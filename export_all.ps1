# Do a recursive descent into all folders lloking for Perforce versioned files.
# Extract all versions and export to git.

Write-Output "Export all ,v files to git"

Get-ChildItem "*,v" -recurse |
ForEach-Object {
    Write-Output $_.FullName
    p4hist -git "$_"
}