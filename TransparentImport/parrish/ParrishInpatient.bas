Attribute VB_Name = "ParrishInpatient"
Option Explicit
'
' Parrish Inpatient
'
' This processes one patient using the Inpatient methodology.
'
' All search functions use exact match for code and result (except when result is null).
' result_like looks for exact match in the result.
' result_list looks for any one of a list of words exactly as the result.
'
Private Const MAX_INDS = 50
Private Const MAX_PROCS = 20

Private Type indicator_data
    checked     As Boolean
    group       As Integer
    also_mark   As String                   'not used at Arnot (handy with user-defined indicators?)
End Type

Private Type procdata                       'not used at Arnot
    pnum    As Integer
    start   As Date
    finish  As Date
    isvalid As Boolean 'used in combining procs
    pindex As Integer 'used in combining procs = the index of procs whose start/finish times are equal to this one
End Type
Private Type ocdata                       'not used at Arnot
    checked As Boolean
    pnum    As Integer
    start   As String
End Type

Private inds(MAX_INDS)          As indicator_data

Private numprocs                As Integer
Private numoutcomes             As Integer
Private procs(MAX_PROCS)        As procdata
Private oc(MAX_PROCS)           As ocdata

Private WHERE_ENCOUNTER         As String
Private AND_UNIT                As String
Private AND_PULL_RANGE          As String
Private AND_OUTCOMES_RANGE      As String
Private AND_ARRIVAL             As String
Private BUCKET_CALC             As String

Private m_pat                   As PatientInfo
Private m_assist_count          As Integer

Private Enum SearchMode
    SearchDefault
    Searchpullrange             'search within the current pull     -- default
    SearchSinceArrival          'search since arrival to the unit
    SearchSinceAdmission        'search everything since admission to the hospital
    SearchOutcomesRange         'search within the current pull + 24 hours before
End Enum

Private Enum CountMode
    CountAll
    CountFirst                  'stop after one is found
End Enum

Private Enum GetValueMode
    GetTotal
    GetMax
    GetLast
End Enum

Private Const FREQUENCY_BUCKET_SIZE = 20    'min

Private Enum Frequencies
    QNONE
    Q4H
    Q2H
    Q1H
    Q30M
End Enum

Private Type fmapRow
    los_high                As Single       'the LOS being testing
    freq(QNONE To Q30M)     As Integer      'the count required for each Q value
End Type

Private m_freq_map(6)       As fmapRow      '1,2,4,8,12,24 hours



'This is the main entry point
'
Public Sub ProcessInpatient(pat As PatientInfo)
    On Error GoTo errHandler
    
    m_pat = pat

    InitGroupsIfNeeded
    SetSQLConstants
    LoadFreqTable
    ResetIndicators
    ResetProcs
    ResetOutcomes

    Check_1_2_3
    Check_4
    Check_5_6
    Check_7
    Check_8
    Check_9_10
    Check_11_12
    Check_13
    Check_14_15_16_17
    Check_18
    Check_19_20
    Check_21
    Check_22
    AtLeastOneADL
    HighestIndicatorInEachGroupWins

    CheckProcs
    CheckOutcomes

    If g_no_output Then Exit Sub
    OutputClass
    OutputProcs
    OutputOutcomes
    Exit Sub
    
errHandler:
    LogError Err.Description & " in " & Err.source, EVENT_CATEGORY_UNEXPECTED
    Exit Sub
    Resume  'debug
End Sub

Private Sub ResetIndicators()
    Dim i As Integer
    
    For i = 1 To MAX_INDS
        inds(i).checked = False
        inds(i).also_mark = ""
    Next i
    
End Sub

Private Sub ResetProcs()
    numprocs = 0
End Sub
Private Sub ResetOutcomes()
    numoutcomes = 0
End Sub

Private Sub InitGroupsIfNeeded()
    On Error GoTo errHandler
    
    Static been_here As Boolean
    Dim rs As New Recordset
    Dim sql As String
    Dim inum As Integer
    
    If been_here Then Exit Sub
    
    'get indicator radio groups from the database
    sql = "SELECT INDICATOR_NUMBER, RADIO_GROUP FROM INDICATOR_DEFINITION WHERE METHODOLOGY_ID=" & m_pat.meth_id
    rs.Open sql, g_cnADO
    
    Do While Not rs.EOF
        inum = rs("INDICATOR_NUMBER")
        If (inum < MAX_INDS) Then
            inds(inum).group = g_dbutil.DBToInteger(rs("RADIO_GROUP"))  'could be null
        End If
        rs.MoveNext
    Loop
    
    rs.Close
    Exit Sub
    
errHandler:
    g_util.ThrowError "InitGroupsIfNeeded"
End Sub

Private Sub LoadFreqTableRow(m_freq As fmapRow, los_high As Single, values As String)
    Dim arr() As String, n As Integer, i As Integer
    
    m_freq.los_high = los_high

    n = g_util.SplitTextOnChar(values, ",", arr(), 0, 0)
    For i = 0 To n - 1
        m_freq.freq(i) = CInt(arr(i))
    Next i
End Sub

Private Sub LoadFreqTable()
                                 ' LOS,   None Q4h Q2h Q1h Q30m
    LoadFreqTableRow m_freq_map(1), 1, "    0,  0,  0,  0,  1"
    LoadFreqTableRow m_freq_map(2), 2, "    0,  0,  0,  1,  2"
    LoadFreqTableRow m_freq_map(3), 4, "    0,  1,  2,  3,  4"
    LoadFreqTableRow m_freq_map(4), 8, "    0,  2,  3,  4,  8"
    LoadFreqTableRow m_freq_map(5), 12, "   0,  2,  4,  6, 12"
    LoadFreqTableRow m_freq_map(6), 9999, " 0,  3,  6, 12, 24"         '24+
    
    'To read this table: if you have at least 6 asessmenets in a 12-hour period, you get Q1h
    '(row 5: LOS=12, column Q1h has a count of 6)
End Sub

Private Function FreqForCount(los_hours As Single, count As Integer) As Frequencies
    Dim i As Integer, j As Integer
    
    For i = 1 To UBound(m_freq_map)                         'go from LOS 1 to 24+
        If (los_hours <= m_freq_map(i).los_high) Then
            For j = Q30M To QNONE Step -1                   'search right to left
                If (count >= m_freq_map(i).freq(j)) Then
                    FreqForCount = j
                    Exit Function
                End If
            Next j
        End If
    Next i
    
    FreqForCount = QNONE
End Function

Private Sub SetSQLConstants()
    'These SQL constants are combined in different ways for searching
    WHERE_ENCOUNTER = " where (encounter_id = " & m_pat.encounter_id & ")"
    AND_UNIT = " and (unit_id = " & m_pat.unit_id & ")"
    AND_PULL_RANGE = " and (event_datetime between " & g_dbutil.SQL_DateTime(m_pat.pull_start) & " and " & g_dbutil.SQL_DateTime(m_pat.pull_finish) & ")"
    AND_ARRIVAL = " and (event_datetime between " & g_dbutil.SQL_DateTime(m_pat.unit_arrival) & " and " & g_dbutil.SQL_DateTime(m_pat.pull_finish) & ")"
    AND_OUTCOMES_RANGE = " and (event_datetime between " & g_dbutil.SQL_DateTime(DateAdd("d", -1, m_pat.pull_start)) & " and " & g_dbutil.SQL_DateTime(m_pat.pull_finish) & ")"
    
    'What bucket number does the event belong to?
    BUCKET_CALC = "DateDiff(minute," & g_dbutil.SQL_DateTime(m_pat.pull_start) & ",event_datetime) / " & FREQUENCY_BUCKET_SIZE
End Sub

'Add this to all chart item queries to start the WHERE clause
'This is where the search depth is set
Private Function WhereBase(Optional search_mode As SearchMode = SearchDefault)
    Dim result As String
    
    Select Case search_mode
    Case Searchpullrange, SearchDefault
        result = WHERE_ENCOUNTER & AND_UNIT & AND_PULL_RANGE    'search within pull range
    Case SearchSinceArrival
        result = WHERE_ENCOUNTER & AND_UNIT & AND_ARRIVAL       'search since arrival to the unit
    Case SearchSinceAdmission
        result = WHERE_ENCOUNTER                                'search since admission to the hospital (any unit)
    Case SearchOutcomesRange
        result = WHERE_ENCOUNTER & AND_UNIT & AND_OUTCOMES_RANGE    'search within pull range+24hrs before
    End Select
    
    'Since we are looking for one patient in one facility, we don't have to filter by TC_source_ID.
    
    WhereBase = result
End Function

Private Function ValueIsAList(result As String) As Boolean
    ValueIsAList = (InStr(1, result, ",") > 0)                'is there a comma-separated list?
End Function

'Look for any of these fields.  Cat/desc/field = exact match.  Result = like match.
Private Function AndSimpleItemFilter(cat As String, code As String, desc As String, field As String, result_like As String) As String
    Dim result As String
    
    If Len(cat) Then result = result & " and category=" & g_dbutil.SQL_String(cat)
    If Len(code) Then result = result & " and code=" & g_dbutil.SQL_String(code)
    If Len(desc) Then result = result & " and description=" & g_dbutil.SQL_String(desc)
    If Len(field) Then result = result & " and field_name=" & g_dbutil.SQL_String(field)
    If Len(result_like) Then result = result & " and result=" & g_dbutil.SQL_String(result_like)

    AndSimpleItemFilter = result
End Function

'Look for a result that contains one of the words in the list
'The only drawback with this function is that we don't know what it found (for audits)
'Because of this, it is only used in as few places.
Private Function AndResultContains(result_list As String) As String
    Dim arr() As String
    Dim i As Integer, n As Integer
    Dim result As String

    If Len(result_list) = 0 Then Exit Function
    
    n = g_util.SplitTextOnChar(result_list, ",", arr(), 1, 0)
    
    result = "and ((1=0)"   ' or (result=this) or (result=that)
    
    For i = 1 To n
        result = result & " or (result=" & g_dbutil.SQL_String(Trim$(arr(i))) & ")"
    Next i
    
    result = result & ")"
    
    AndResultContains = result
End Function

Private Function DescribeSearchDepth(search_mode As SearchMode) As String
    Dim result As String
    
    Select Case search_mode
    Case Searchpullrange, SearchDefault
        'result = "in pull range"
        result = ""                         'be quiet since this is default
    Case SearchSinceArrival
        result = "since arrival to unit"
    Case SearchSinceAdmission
        result = "since admission"
    Case SearchOutcomesRange
        result = "since 24hrs before pull"
    End Select
    
    DescribeSearchDepth = result
End Function

Private Function Describe(cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = Searchpullrange) As String
    Dim sql As String, and_filter As String
    Dim rs As New Recordset
    Dim result As String
   
    If Not g_debug Then Exit Function           'avoid extra overhead if not making a log

    result = "looking for"
    If Len(cat) Then result = result & " cat='" & cat & "'"
    If Len(code) Then result = result & " code='" & code & "'"
    If Len(desc) Then result = result & " desc='" & desc & "'"
    If Len(field) Then result = result & " field='" & field & "'"
    If Len(result_list) Then result = result & " result contains '" & result_list & "'"

    If ValueIsAList(result_list) Then
        and_filter = AndSimpleItemFilter(cat, code, desc, field, "") & AndResultContains(result_list)
    Else
        and_filter = AndSimpleItemFilter(cat, code, desc, field, result_list)
    End If
    
    sql = "select category, code, description, field_name, result from chart_item" & WhereBase(search_mode) & and_filter
    rs.Open sql, g_cnADO
    If rs.EOF Then
        result = result & "; not found " & DescribeSearchDepth(search_mode)
    Else
        result = result & "; found"
        'Add info for columns that were not specified and a value was found
        If (Len(cat) = 0) And Len(rs("category")) Then result = result & " cat='" & rs("category") & "'"
        If (Len(code) = 0) And Len(rs("code")) Then result = result & " code='" & rs("code") & "'"
        If (Len(desc) = 0) And Len(rs("description")) Then result = result & " desc='" & rs("description") & "'"
        If (Len(field) = 0) And Len(rs("field_name")) Then result = result & " field='" & rs("field_name") & "'"
        'Add the complete result found (we searched for a word or words)
        result = result & " result='" & rs("result") & "'"
        'Are there more results?  Just say how many; we aren't going to list them.
        If (rs.RecordCount = 2) Then
            result = result & " (1 more result)"
        ElseIf (rs.RecordCount > 2) Then
            result = result & " (" & rs.RecordCount - 1 & " more results)"
        End If
    End If
    rs.Close
    
    Describe = result
End Function

'Set an indicator for this reason (low level)
Private Sub SetInd(inum As Integer, reason As String)
    If inds(inum).checked And Not g_debug Then Exit Sub       'already set and no log?

    inds(inum).checked = True
    dprint "Set Ind #" & inum & ": " & reason
End Sub

'Clear an indicator for this reason (low level)
Private Sub ClrInd(inum As Integer, reason As String)
    If Not inds(inum).checked And Not g_debug Then Exit Sub   'already clear and no log?
    
    inds(inum).checked = False
    dprint "Clr Ind #" & inum & ": " & reason
End Sub

'Count how many items have the search word in result_like
'This is simple and fast
Private Function CountSimpleResult(cat As String, code As String, desc As String, field As String, result_like As String, Optional search_mode As SearchMode = Searchpullrange, Optional trace As Boolean = True, Optional found_what As String) As Long
    Dim sql As String, and_filter As String
    Dim rs As New Recordset
    Dim count As Long

    and_filter = AndSimpleItemFilter(cat, code, desc, field, result_like)
    sql = "select count(*) from chart_item" & WhereBase(search_mode) & and_filter
    'Debug.Print sql
    rs.Open sql, g_cnADO
    count = rs(0)
    rs.Close
    
    'Normally CountSimpleResult will report what it finds; not eveyone wants this so the trace can be turned off
    found_what = Describe(cat, code, desc, field, result_like, search_mode) & IIf(count > 0, "; count=" & count, "")
    If trace Then dvprint found_what
    
    CountSimpleResult = count
End Function

'Count how many items have a result with one of the words given in the result_list
'Returns a description of what word was found
'This is more powerful than CountSimpleResult but it is slower
Private Function CountResultInList(cat As String, code As String, desc As String, field As String, result_list As String, search_mode As SearchMode, count_mode As CountMode, trace As Boolean, found_what As String) As Integer
    On Error GoTo errHandler
    Dim sql As String, and_filter As String, arr() As String
    Dim rs As New Recordset
    Dim i As Integer, n As Integer, count As Integer

    n = g_util.SplitTextOnChar(result_list, ",", arr(), 1, 0)
    
    'Do not filter by result here; we need all results
    and_filter = AndSimpleItemFilter(cat, code, desc, field, "")
    sql = "select code, result from chart_item" & WhereBase(search_mode) & and_filter
    'Debug.Print sql
    rs.Open sql, g_cnADO
    
    Do While Not rs.EOF
        'Look for each search word in the result (case insensitive)
        For i = 1 To n
            arr(i) = Trim$(arr(i))          'we split on comma so there usually are leading blanks
            If InStr(1, rs("result"), arr(i), vbTextCompare) > 0 Then
                found_what = "found '" & arr(i) & "' in code='" & rs("code") & "' result='" & rs("result") & "'"
                If trace Then dvprint found_what
                count = count + 1
                If count_mode <> CountAll Then Exit Do  'stop counting
                Exit For                                'try next record
            End If
        Next i

        rs.MoveNext
    Loop
    
    If (count > 0) Then
        'We already printed what was found; maybe add how many?
        If trace And (count_mode = CountAll) Then dvprint "found " & count & " total"
    Else
        found_what = Describe(cat, code, desc, field, result_list, search_mode)       'not found
        If trace Then dvprint found_what
    End If
    
    rs.Close
    
    CountResultInList = count
    Exit Function
    
errHandler:
    g_util.ThrowError "CountResultInList"
    Resume   'debug
End Function

Private Function CountResultContains(cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = Searchpullrange, Optional trace As Boolean = True, Optional found_what As String) As Integer
    If ValueIsAList(result_list) Then
        CountResultContains = CountResultInList(cat, code, desc, field, result_list, search_mode, CountAll, trace, found_what)
    Else
        CountResultContains = CountSimpleResult(cat, code, desc, field, result_list, search_mode, trace, found_what)
    End If
End Function

Private Function ResultContains(cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = Searchpullrange, Optional trace As Boolean = True, Optional found_what As String) As Boolean
    'tracing can be turned off and you can ask for what was found instead of having it printed
    If ValueIsAList(result_list) Then
        ResultContains = (CountResultInList(cat, code, desc, field, result_list, search_mode, CountFirst, trace, found_what) > 0)
    Else
        ResultContains = (CountSimpleResult(cat, code, desc, field, result_list, search_mode, trace, found_what) > 0)
    End If
End Function


Private Function CountResultNotInList(cat As String, code As String, desc As String, field As String, result_list As String, search_mode As SearchMode, count_mode As CountMode, trace As Boolean, found_what As String) As Integer
    On Error GoTo errHandler
    Dim sql As String, and_filter As String, arr() As String
    Dim rs As New Recordset
    Dim i As Integer, n As Integer, count As Integer
    Dim found_one As Boolean

    n = g_util.SplitTextOnChar(result_list, ",", arr(), 1, 0)
    
    'Do not filter by result here; we need all results
    and_filter = AndSimpleItemFilter(cat, code, desc, field, "")
    sql = "select code, result from chart_item" & WhereBase(search_mode) & and_filter
    'Debug.Print sql
    rs.Open sql, g_cnADO
    
    Do While Not rs.EOF
        'Look for each search word in the result (case insensitive)
        found_one = False
        
        For i = 1 To n
            arr(i) = Trim$(arr(i))          'we split on comma so there usually are leading blanks
            If InStr(1, rs("result"), arr(i), vbTextCompare) > 0 Then
                found_what = "found '" & arr(i) & "' in code='" & rs("code") & "' result='" & rs("result") & "' -- ignore this"
                If trace Then dvprint found_what
                found_one = True
                Exit For
            End If
        Next i

        If (Not found_one) Then
            'none of the words were found - good!
            found_what = Describe(cat, code, desc, field, "", search_mode) & " -- does not contain '" & result_list & "'"
            If trace Then dvprint found_what
            count = count + 1
            If count_mode <> CountAll Then Exit Do
        End If

        rs.MoveNext
    Loop
    
    If (count > 0) Then
        'We already printed what was found; maybe show the total?
        If trace And (count_mode = CountAll) Then dvprint "found " & count & " total"
    ElseIf (rs.RecordCount > 0) Then
        'We already printed what we ignored
    Else
        'Print what did not exist
        found_what = Describe(cat, code, desc, field, "", search_mode)      'not found
        If trace Then dvprint found_what
    End If
    
    rs.Close
    
    CountResultNotInList = count
    Exit Function
    
errHandler:
    g_util.ThrowError "CountResultNotInList"
    Resume   'debug
End Function

Private Function CountResultDoesNotContain(cat As String, code As String, desc As String, field As String, result_list As String, found_what As String, Optional search_mode As SearchMode = Searchpullrange, Optional trace As Boolean = True) As Integer
    CountResultDoesNotContain = CountResultNotInList(cat, code, desc, field, result_list, search_mode, CountAll, trace, found_what)
End Function

Private Function ResultDoesNotContain(cat As String, code As String, desc As String, field As String, result_list As String, found_what As String, Optional search_mode As SearchMode = Searchpullrange, Optional trace As Boolean = True) As Boolean
    ResultDoesNotContain = (CountResultNotInList(cat, code, desc, field, result_list, search_mode, False, trace, found_what) > 0)
End Function

'Set the indicator if the result contains one of the words in the result_list
Private Sub SetIndIfResultContains(inum As Integer, cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = Searchpullrange)
    Dim found_what As String
    'avoid more queries if the indicator is already set
    If inds(inum).checked Then Exit Sub
    
    'Turn trace off for ResultContains() so the result can be placed on the "SetInd#" line below
    If ResultContains(cat, code, desc, field, result_list, search_mode, False, found_what) Then
        SetInd inum, found_what             'echo here
    Else
        dvprint found_what                  'and here
    End If
End Sub

'Set the indicator if the result does not contain any of the words in result_list
Private Sub SetIndIfResultDoesNotContain(inum As Integer, cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = Searchpullrange)
    Dim found_what As String
    'avoid more queries if the indicator is already set
    If inds(inum).checked Then Exit Sub
    
    'Turn trace off for ResultDoesNotContain() and echo what was set below with SetInd
    If ResultDoesNotContain(cat, code, desc, field, result_list, found_what, search_mode, False) Then
        SetInd inum, found_what             'echo here
    Else
        dvprint found_what                  'and here
    End If
End Sub

'Clear the indicator if the result contains on of the words in the result_list
Private Sub ClrIndIfResultContains(inum As Integer, cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = Searchpullrange)
    Dim found_what As String
    'avoid more queries if the indicator is already clear
    If Not inds(inum).checked Then Exit Sub
    
    'Turn trace off for ResultContains() and echo what was set below with SetInd
    If ResultContains(cat, code, desc, field, result_list, search_mode, False, found_what) Then
        ClrInd inum, found_what             'echo here
    Else
        dvprint found_what                  'and here
    End If
End Sub


'These slightly smaller functions are meant for places where you aren't really looking for a chart result
Private Function Exists(cat As String, Optional code As String, Optional desc As String, Optional field As String, Optional result_list As String, Optional search_mode As SearchMode = Searchpullrange, Optional trace As Boolean = True, Optional found_what As String) As Boolean
    Exists = (CountResultContains(cat, code, desc, field, result_list, search_mode, trace, found_what) > 0)
End Function

Private Sub SetIndIfFound(inum As Integer, cat As String, Optional code As String, Optional desc As String, Optional field As String, Optional result_list As String, Optional search_mode As SearchMode = Searchpullrange)
    SetIndIfResultContains inum, cat, code, desc, field, result_list, search_mode
End Sub

Private Sub ClrIndIfFound(inum As Integer, cat As String, Optional code As String, Optional desc As String, Optional field As String, Optional result_list As String, Optional search_mode As SearchMode = Searchpullrange)
    ClrIndIfResultContains inum, cat, code, desc, field, result_list, search_mode
End Sub


'Get the max/total value from a result (usually in the middle of the text)
Private Function GetIntValue(get_mode As GetValueMode, cat As String, code As String, desc As String, field As String, result_like As String, Optional search_mode As SearchMode = Searchpullrange) As Integer
    Dim sql As String, and_filter As String, arr() As String, msg As String
    Dim rs As New Recordset
    Dim result As Integer, i As Integer, n As Integer, value As Integer
    Dim found_one As Boolean

    and_filter = AndSimpleItemFilter(cat, code, desc, field, result_like)
    sql = "select result from chart_item" & WhereBase(search_mode) & and_filter
    'Debug.Print sql
    rs.Open sql, g_cnADO
    
    'Look for a number in the result
    
    Do While Not rs.EOF
        n = g_util.SplitTextOnChar(rs(0), " ", arr(), 1, 0)
        For i = 1 To n
            'Look for 1st character numeric; IsNumeric("60min") will fail so look at 1st char only
            If IsNumeric(Left$(arr(i), 1)) Then
                value = val(arr(i))                         'Use Val; CInt will error on "60min"
                Select Case get_mode
                Case GetMax
                    result = g_util.Max(value, result)      'max
                Case GetTotal
                    result = result + value                 'total
                Case GetLast
                    result = value                          'last
                End Select
                
                'print what we are searching for (the first time)
                If Not found_one Then
                    dvprint Describe(cat, code, desc, field, result_like, search_mode)
                End If
                found_one = True
                'print each value found
                dvprint "  found numeric value " & result
                'Keep going in case there are more
            End If
        Next i
        rs.MoveNext
    Loop
    
    rs.Close
    
    If Not found_one Then
        'show what was not found
        If g_verbose Then dprint Describe(cat, code, desc, field, result_like, search_mode)
    End If
    
    GetIntValue = result
End Function

Private Function GetMaxValue(cat As String, code As String, desc As String, field As String, result_like As String, Optional search_mode As SearchMode = Searchpullrange) As Integer
    GetMaxValue = GetIntValue(GetMax, cat, code, desc, field, result_like, search_mode)
End Function

Private Function GetTotalValue(cat As String, code As String, desc As String, field As String, result_like As String, Optional search_mode As SearchMode = Searchpullrange) As Integer
    GetTotalValue = GetIntValue(GetTotal, cat, code, desc, field, result_like, search_mode)
End Function

'Get a result; returns True if found with return_result set
Private Function GetResult(cat As String, code As String, desc As String, field As String, return_result As String, Optional search_mode As SearchMode = Searchpullrange) As Boolean
    Dim rs As New Recordset
    Dim sql As String

    sql = "select result from chart_item" & WhereBase(search_mode) & AndSimpleItemFilter(cat, code, desc, field, "")
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        return_result = rs(0) & ""
    Else
        return_result = ""
    End If
    rs.Close

    dvprint Describe(cat, code, desc, field, "", search_mode)
    GetResult = (Len(return_result) > 0)
End Function
Private Function GetResultOfLatest(cat As String, code As String, desc As String, field As String, return_result As String, Optional search_mode As SearchMode = Searchpullrange) As Boolean
    Dim rs As New Recordset
    Dim sql As String
    Dim evdt As Date
    Dim done As Boolean

    sql = "select event_datetime,result from chart_item" & WhereBase(search_mode) & AndSimpleItemFilter(cat, code, desc, field, "")
    sql = sql & " order by event_datetime desc"
    rs.Open sql, g_cnADO
    
    return_result = ""
    done = False
    
    If Not rs.EOF Then evdt = rs(0)
    Do While Not rs.EOF And Not done
        If evdt = rs(0) Then
            return_result = return_result & rs(1) & ","
        Else
            done = True
        End If
        rs.MoveNext
    Loop
    rs.Close

    dvprint Describe(cat, code, desc, field, "", search_mode)
    GetResultOfLatest = (Len(return_result) > 0)

End Function

Private Sub Check_1_2_3()
    On Error GoTo errHandler

    dvprint "---------------"
    dvprint "1. ADL Self"
    dvprint "2. ADL Assist"
    dvprint "3. ADL Complete"
    dvprint "---------------"
    
    SetADLCompleteWhenAge "<=3"
    
' only use NSADLx
'    SetIndIfResultContains 2, "", "OLS", "", "", "1,2"
'
'    If ResultContains("", "OLS", "", "", "1,2") Or _
'       ResultContains("", "OLDA68", "", "", "Y") Then
'        SetIndIfResultContains 3, "", "OLDIET02", "", "", "T,TF"
'    End If
    
    'SetIndIfResultContains 3, "", "OLPA123", "", "", "O"
    SetIndIfResultContains 3, "", "1004910", "", "", "1,2"
    SetIndIfResultContains 3, "", "1003200", "", "", "1,2"
    SetIndIfResultContains 3, "", "1003300", "", "", "1,2"
    
    If inds(3).checked Then Exit Sub
    
    SetIndIfResultContains 3, "", "NSADLC", "", "", "Y"
    SetIndIfResultContains 2, "", "NSADLP", "", "", "Y"
    SetIndIfResultContains 1, "", "NSADLS", "", "", "Y"
    
    
' only use NSADLx
'    SetIndIfResultContains 2, "", "OLS", "", "", "3"
'    SetIndIfResultContains 2, "", "OLDA69", "", "", "Y"
'    SetIndIfResultContains 2, "", "OLDIET02", "", "", "T,TF,A"
    
    If inds(2).checked Then Exit Sub
    
' only use NSADLx
'    SetIndIfResultContains 1, "", "OLS", "", "", "4"
'    SetIndIfResultContains 1, "", "OLDA70", "", "", "Y"
'    SetIndIfResultContains 1, "", "OLDIET02", "", "", "S"
    
'OLS: Use the most recent, not just since admission 3/23/11
    
    Exit Sub
errHandler:
    g_util.ThrowError "Check_1_2_3"
    Resume  'debug
End Sub


Private Sub Check_4()
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "4. ADL Rehab"
    dvprint "---------------"
    
    SetIndIfResultContains 4, "", "AMPUEX", "", "", "", SearchSinceAdmission
    SetIndIfResultContains 4, "", "AMPULEX", "", "", "", SearchSinceAdmission
    SetIndIfFound 4, "", "STR" 'added stroke 3/23/11
    SetIndIfFound 4, "", "CVAHEM"
    SetIndIfFound 4, "", "CVAISC"
    SetIndIfFound 4, "", "CVA"
    SetIndIfResultContains 4, "", "OLACUITY14", "", "", "Y"
    SetIndIfFound 4, "", "TOTALHIPP"
    SetIndIfFound 4, "", "ORIFHIP"
    SetIndIfFound 4, "", "TOTALKNEE"
    SetIndIfFound 4, "", "44000"
    SetIndIfFound 4, "", "1013401"
    SetIndIfFound 4, "", "HXMO"
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_4"
    Resume  'debug
End Sub


Private Sub Check_5_6()
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "5. ADL 2-3 Caregivers"
    dvprint "6. ADL 4 or more Caregivers"
    dvprint "---------------"
    
    SetIndIfResultContains 6, "", "OLACUITY63", "", "", "Y"
    SetIndIfResultContains 5, "", "OLACUITY62", "", "", "Y"
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_5_6"
    Resume  'debug
End Sub

Private Sub Check_7()
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "7. Communication"
    dvprint "---------------"

    SetIndIfResultContains 7, "", "OLACUITY72", "", "", "Y", SearchSinceAdmission
    SetIndIfResultContains 7, "", "OLEDUCB", "", "", "L", SearchSinceAdmission
    SetIndIfResultContains 7, "", "OLED065", "", "", "N", SearchSinceAdmission
    SetIndIfFound 7, "", "497400"
    SetIndIfFound 7, "", "497600"
    SetIndIfFound 7, "", "497800"
    SetIndIfResultContains 7, "", "497500", "", "", "", SearchSinceAdmission
    SetIndIfResultContains 7, "", "EDMTRACH", "", "", "Y"
    'SetIndIfResultContains 7, "", "OLETT001", "", "", "Y"
    SetIndIfResultContains 7, "", "NSNE033", "", "", "Y"
    'SetIndIfResultContains 7, "", "OLPA123", "", "", "O"
    SetIndIfFound 7, "", "HXDEAF", "", "", "", SearchSinceAdmission
    SetIndIfFound 7, "", "HXCVAA", "", "", "", SearchSinceAdmission
    SetIndIfFound 7, "", "HXAPHASIA", "", "", "", SearchSinceAdmission
    SetIndIfFound 7, "", "HXBLIND", "", "", "", SearchSinceAdmission
    SetIndIfFound 7, "", "HXVISL", "", "", "", SearchSinceAdmission
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_7"
    Resume  'debug
End Sub

Private Sub Check_8()
    On Error GoTo errHandler
    Dim tmp As String

    dvprint "---------------"
    dvprint "8. Cognitive Support"
    dvprint "---------------"

    SetIndIfResultContains 8, "", "OLFALL31", "", "", "Y"
    SetIndIfResultContains 8, "", "OLFALL32", "", "", "Y"
    SetIndIfResultContains 8, "", "OLFALL33", "", "", "Y"
    SetIndIfResultContains 8, "", "OLFALL34", "", "", "Y"
    'SetIndIfResultContains 8, "", "OLFALL35", "", "", "Y"
    SetIndIfResultContains 8, "", "OLFALL36", "", "", "Y"
    SetIndIfResultContains 8, "", "OLFALL37", "", "", "Y"
    SetIndIfResultContains 8, "", "OLAMB70", "", "", "Y", SearchSinceAdmission
    
    SetIndIfFound 8, "", "HXDEV", "", "", "", SearchSinceAdmission
    SetIndIfFound 8, "", "HXWAS", "", "", "", SearchSinceAdmission
    SetIndIfFound 8, "", "HXWASM", "", "", "", SearchSinceAdmission
    SetIndIfFound 8, "", "HXAUT", "", "", "", SearchSinceAdmission
    SetIndIfFound 8, "", "HXADD", "", "", "", SearchSinceAdmission
    SetIndIfFound 8, "", "HXADDH", "", "", "", SearchSinceAdmission
    SetIndIfFound 8, "", "HXPOSIT", "", "", "", SearchSinceAdmission
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_8"
    Resume  'debug
End Sub

Private Sub Check_9_10()
    On Error GoTo errHandler
    Dim tmp As String

    dvprint "---------------"
    dvprint "9.  Behavior/Emotional Management"
    dvprint "10. Behavior/Emotional Mgmt - q 1 Hour"
    dvprint "---------------"

    SetIndIfFound 9, "", "582000"
    SetIndIfFound 9, "", "1015501"
    SetIndIfFound 10, "", "582100"
    SetIndIfFound 10, "", "1012201"
    SetIndIfResultContains 10, "", "OLFALL36", "", "", "Y"
    SetIndIfResultContains 10, "", "OLFALL37", "", "", "Y"

    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_9_10"
    Resume  'debug
End Sub


Private Sub Check_11_12()
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "11. Safety Management - q 2 Hours"
    dvprint "12. Safety Management - q 30 Minutes"
    dvprint "---------------"

    '
    ' 12.
    '
    SetIndIfResultContains 12, "", "OLFALL36", "", "", "Y"
    SetIndIfResultContains 12, "", "OLFALL37", "", "", "Y"
    If InStr(1, m_pat.unit_name, "ICU", vbTextCompare) Then
        SetIndIfFound 12, "", "OVERDOSE", "", "", "", SearchSinceAdmission
        SetIndIfFound 12, "", "OVERDOSEM", "", "", "", SearchSinceAdmission
    End If
    SetIndIfFound 12, "", "1012201"
    SetIndIfFound 12, "", "OLAMBPRO02"
    SetIndIfFound 12, "", "OLAMBPRO03"
    SetIndIfResultContains 12, "", "OLAMBPROG8", "", "", "Y"
    SetIndIfResultContains 12, "", "EDMSUI", "", "", "Y"
    SetIndIfResultContains 12, "", "EDMKIL", "", "", "Y"
    SetIndIfResultContains 12, "", "EDMPLAN", "", "", "Y"
    
    If inds(12).checked Then Exit Sub
    
    '
    ' 11.
    '
    SetIndIfResultContains 11, "", "OLSAFETY01", "", "", "2"
'    SetIndIfResultContains 11, "", "OLFALL31", "", "", "Y"
'    SetIndIfResultContains 11, "", "OLFALL34", "", "", "Y"
    SetIndIfFound 11, "", "HXDEV", "", "", "", SearchSinceAdmission
    SetIndIfFound 11, "", "HXWAS", "", "", "", SearchSinceAdmission
    SetIndIfFound 11, "", "HXWASM", "", "", "", SearchSinceAdmission
    SetIndIfFound 11, "", "HXAUT", "", "", "", SearchSinceAdmission
    SetIndIfFound 11, "", "1015501"
    SetIndIfFound 11, "", "268000"
    SetIndIfFound 11, "", "SUICIDE"
    SetIndIfFound 11, "", "SUICIDEI"
    SetIndIfResultContains 11, "", "SUICIDER", "", "", "Y"
    SetIndIfResultDoesNotContain 11, "", "EDMSUI", "", "", "N"
    SetIndIfResultDoesNotContain 11, "", "EDMKIL", "", "", "N"
    SetIndIfResultDoesNotContain 11, "", "EDMPLAN", "", "", "N"
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_11_12"
    Resume  'debug
End Sub

Private Sub Check_13()
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "13. Isolation"
    dvprint "---------------"
    
    SetIndIfResultContains 13, "", "OEISO", "", "", "A,AC,C,CN,D,DC,NE,RAD,ZCH,ZCHA,ZCHAC,ZCHC,ZCHCN,ZCHD,ZCHDC,ZCHNE"
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_13"
    Resume  'debug
End Sub

Private Sub CheckAssessment(count As Integer, ct14 As Integer, ct15 As Integer, ct16 As Integer, ct17 As Integer, desc As String, Optional startat As Integer = 17)
    Dim los_hours As Single

    If (inds(17).checked) Then Exit Sub             'skip if highest already checked
    If (count = 0) Then Exit Sub                    'skip if none

    'Here's the mathematical way:
    '    freq_min = g_util.DivideWithoutError(m_pat.range, count)
    'The book, however, says that you did this frequency for the majority of the time period.
    'This could mean that you did it for only 51% of the period.  Use a lookup table instead.
    
    'los_hours = m_pat.range / 60#
    
    If count >= ct17 And startat = 17 Then  'default startat is set to 17 in argument def.
        SetInd 17, desc
    ElseIf count >= ct16 And startat >= 16 Then
        SetInd 16, desc
    ElseIf count >= ct15 Then
        SetInd 15, desc
    ElseIf count >= ct14 Then
        SetInd 14, desc
    End If

End Sub

Private Sub Check_14_15_16_17()
    Dim found_what As String
    Dim qmins As Integer
    Dim ct As Integer
    Dim return_result As String
    
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "14. Assessment q4h"
    dvprint "15. Assessment q2h"
    dvprint "16. Assessment q1h"
    dvprint "17. Assessment q30min"
    dvprint "---------------"
    
    If GetIntValue(GetMax, "", "OLACUITY03", "", "", "") > 6 Then
        qmins = GetIntValue(GetMax, "", "OLACUITY02", "", "", "")
        If qmins = 5 Or qmins = 10 Or qmins = 15 Or qmins = 30 Then
            SetInd 17, "OLACUITY03 > 6 and qmins=" & qmins
        End If
    End If
    
    If inds(17).checked Then Exit Sub
    
    SetIndIfResultContains 17, "", "OLACUITY11", "", "", "Y"
    SetIndIfResultContains 17, "", "OLACUITY06", "", "", "Q15"
    'SetIndIfFound 17, "", "CYT100"
    'SetIndIfFound 17, "", "CYT200"
    
    If inds(17).checked Then Exit Sub
    
    'SetIndIfFound 17, "", "PIT"
    'SetIndIfFound 17, "", "PIT20RM"
    SetIndIfFound 17, "", "CARBO250"
    SetIndIfFound 17, "", "SUB2"
    SetIndIfFound 17, "", "MAGJ"
    
    If inds(17).checked Then Exit Sub

    ct = CountSimpleResult("", "1004610", "", "", "", , , found_what)
    CheckAssessment ct, 4, 6, 8, 9, found_what
    ct = CountSimpleResult("", "1010800", "", "", "", , , found_what)
    CheckAssessment ct, 2, 3, 4, 5, found_what
    ct = CountSimpleResult("", "1006100", "", "", "", , , found_what)
    CheckAssessment ct, 2, 3, 4, 5, found_what
    ct = CountSimpleResult("", "693000", "", "", "", , , found_what)
    CheckAssessment ct, 2, 3, 4, 5, found_what

    
    ct = CountSimpleResult("", "1000435", "", "", "", , , found_what)
    CheckAssessment ct, 2, 3, 6, 12, found_what
    ct = CountSimpleResult("", "1000426", "", "", "", , , found_what)
    CheckAssessment ct, 2, 3, 6, 12, found_what
    ct = CountSimpleResult("", "1000428", "", "", "", , , found_what)
    CheckAssessment ct, 2, 3, 6, 12, found_what
    ct = CountSimpleResult("", "1000429", "", "", "", , , found_what)
    CheckAssessment ct, 2, 3, 6, 12, found_what
    
    ct = CountSimpleResult("", "1018510", "", "", "", , , found_what)
    CheckAssessment ct, 2, 4, 6, 8, found_what
    ct = CountSimpleResult("", "1000990", "", "", "", , , found_what)
    CheckAssessment ct, 2, 4, 6, 8, found_what
    
    If inds(17).checked Then Exit Sub
    ct = CountSimpleResult("", "668000", "", "", "", , , found_what)
    CheckAssessment ct, 2, 4, 6, 8, found_what
    ct = CountSimpleResult("", "668100", "", "", "", , , found_what)
    CheckAssessment ct, 2, 4, 6, 8, found_what
    ct = CountSimpleResult("", "668200", "", "", "", , , found_what)
    CheckAssessment ct, 2, 4, 6, 8, found_what
    
    If inds(17).checked Then Exit Sub
    ct = CountSimpleResult("", "1011110", "", "", "", , , found_what)
    CheckAssessment ct, 2, 4, 6, 8, found_what
    ct = CountSimpleResult("", "1001090", "", "", "", , , found_what)
    CheckAssessment ct, 4, 6, 8, 9, found_what

    If inds(17).checked Then Exit Sub
    ct = CountSimpleResult("", "1011610", "", "", "", , , found_what)
    If ct > 12 Then SetInd 17, found_what
    
    If inds(17).checked Then Exit Sub
    ct = CountSimpleResult("", "OLINCIS042", "", "", "Y", , , found_what)
    CheckAssessment ct, 2, 4, 6, 12, found_what
    ct = CountSimpleResult("", "OLWOUND001", "", "", "Y", , , found_what)
    CheckAssessment ct, 2, 4, 6, 12, found_what
    ct = CountSimpleResult("", "OLSK230", "", "", "Y", , , found_what)
    CheckAssessment ct, 2, 4, 6, 12, found_what
    
    If inds(17).checked Then Exit Sub
    SetIndIfFound 16, "", "1001072"
    'If pt is in unit ICU, then automatically give q1=indicator 16
    If InStr(1, m_pat.unit_name, "ICU", vbTextCompare) Then
        SetInd 16, "Unit is ICU - Automatically set assessments at Q1"
    End If
    
    If GetResultOfLatest("", "NURBSCHEC3", "", "", return_result, SearchSinceAdmission) Then
        If InStr(1, return_result, "Q1") Then
            SetInd 16, "NURBSCHEC3+Q1"
        ElseIf InStr(1, return_result, "Q2") Then
            SetInd 15, "NURBSCHEC3+Q2"
        ElseIf InStr(1, return_result, "A") Then
            SetInd 14, "NURBSCHEC3+A"
        ElseIf InStr(1, return_result, "Q4") Then
            SetInd 14, "NURBSCHEC3+Q4"
        ElseIf InStr(1, return_result, "Q6") Then
            SetInd 14, "NURBSCHEC3+Q6"
        End If
    End If
    
    If inds(16).checked Then Exit Sub
    SetIndIfResultContains 16, "", "OLD001", "", "", "I"
'    SetIndIfResultContains 16, "", "NURBSCHEC3", "", "", "Q1"
    SetIndIfResultContains 16, "", "OLACUITY06", "", "", "Q1"
    SetIndIfResultContains 16, "", "OLINCIS040", "", "", "*SEE NOTE"

    If inds(16).checked Then Exit Sub
    ct = CountSimpleResult("", "1001454", "", "", "", , , found_what)
    If ct >= 6 Then
        SetInd 16, found_what
    End If

    If inds(16).checked Then Exit Sub
    ct = CountSimpleResult("", "1004700", "", "", "", , , found_what)
    CheckAssessment ct, 2, 4, 6, 8, found_what, 16
    ct = CountSimpleResult("", "1004400", "", "", "", , , found_what)
    CheckAssessment ct, 2, 4, 6, 8, found_what, 16
    ct = CountSimpleResult("", "2000205", "", "", "", , , found_what)
    CheckAssessment ct, 4, 6, 8, 10, found_what, 16
    ct = CountSimpleResult("", "2000165", "", "", "", , , found_what)
    CheckAssessment ct, 2, 4, 6, 8, found_what, 16

    If inds(16).checked Then Exit Sub
    ct = CountSimpleResult("", "OLNSAPNEA", "", "", "", , , found_what)
    If ct >= 12 Then
        SetInd 16, found_what
    ElseIf ct >= 3 Then
        SetInd 15, found_what
    ElseIf ct >= 2 Then
        SetInd 14, found_what
    End If
    
    ct = CountSimpleResult("", "OLMURPH9", "", "", "Y", , , found_what)
    If ct >= 2 Then
        SetInd 16, found_what
    ElseIf ct = 1 Then
        SetInd 15, found_what
    End If
    
    If inds(16).checked Or inds(15).checked Then Exit Sub
    
    SetIndIfResultContains 16, "", "OLACUITY05", "", "", "Y"
    ct = CountSimpleResult("", "344000", "", "", "", , , found_what)
    If ct >= 3 Then
        SetInd 15, found_what
    ElseIf ct >= 1 Then
        SetInd 14, found_what
    End If
    If ct >= 1 Then
        SetInd 19, found_what
    End If

    If inds(15).checked Then Exit Sub
    
'    SetIndIfResultContains 15, "", "NURBSCHEC3", "", "", "Q2"
'    SetIndIfResultContains 14, "", "NURBSCHEC3", "", "", "A"
'    SetIndIfResultContains 14, "", "NURBSCHEC3", "", "", "Q4"
'    SetIndIfResultContains 14, "", "NURBSCHEC3", "", "", "Q6"
    SetIndIfResultContains 14, "", "1013400", "", "", "Y"

    If inds(14).checked Then Exit Sub
    
    ct = CountSimpleResult("", "OLACUITY07", "", "", "Q4", , , found_what)
    If ct >= 2 Then
        SetInd 14, found_what
    End If
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_14_15_16_17"
    Resume  'debug
End Sub

Private Sub Check_18()
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "18. Medication Preparation >= 20 Minutes"
    dvprint "---------------"
    
    SetIndIfResultContains 18, "", "OLDIET", "", "", "NPO,TPN,TUBE"
    SetIndIfResultContains 18, "", "OLACUITY14", "", "", "Y"
    
    SetIndIfFound 18, "", "NURI59"
    'SetIndIfFound 18, "", "NURI53"
    SetIndIfFound 18, "", "1011122"
    SetIndIfFound 18, "", "402998"
    SetIndIfResultContains 18, "", "NSGI019", "", "", "Y"
    SetIndIfResultContains 18, "", "NSGI027", "", "", "Y"
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_18"
    Resume  'debug
End Sub

Private Sub CheckWound(total As Integer)
    If (inds(20).checked) Then Exit Sub             'skip if highest already checked

    If (total = 0) Then
        'skip it
    ElseIf (total >= 30) Then
        SetInd 20, "wound >= 30 min"
    Else
        SetInd 19, "wound < 30 min"
    End If
End Sub

Private Sub Check_19_20()
    Dim return_result As String
    Dim ol15, ol16 As Boolean
    
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "19. Wound/Injury Mgmt"
    dvprint "20. Wound/Injury Mgmt >= 30 Minutes"
    dvprint "---------------"
    
    If Exists("", "OLACUITY15", , , "Y") Then
        If ResultContains("", "OLIV232", "", "", "A,B,C,G,H,ID,IS,M,NEO,PAC,PIC,P,Q,TE,TEMPDIAL,U") Then
            ol15 = True
            SetInd 19, "OLIV232 + qualifying result"
        End If
    End If
    
    If Exists("", "OLACUITY16", , , "Y") Then
        If ResultContains("", "OLIV247", "", "", "A,B,C,G,H,ID,IS,M,NEO,PAC,PIC,P,Q,TE,TEMPDIAL,U") Then
            ol16 = inds(19).checked
            SetInd 19, "OLIV247 + qualifying result"
        End If
    End If
    
    If ol15 And ol16 Then
        SetInd 20, "OLACUITY15+ and OLACUITY16+OLIV247"
    End If
    
    SetIndIfResultContains 20, "", "OLMURPH9", "", "", "Y"
    SetIndIfResultContains 20, "", "EDMTRACH", "", "", "Y"

    If inds(20).checked Then Exit Sub
    SetIndIfResultContains 20, "", "OLSK115", "", "", "3,4,E"
    SetIndIfResultContains 20, "", "OLSK130", "", "", "3,4,E"
    SetIndIfResultContains 20, "", "OLSK145", "", "", "3,4,E"
    SetIndIfResultContains 20, "", "OLSK160", "", "", "3,4,E"
    
    If inds(20).checked Then Exit Sub
    SetIndIfResultContains 20, "", "NURBFC", "", "", "R"
    SetIndIfResultContains 20, "", "NUROUTNG", "", "", "RE,SA"
    SetIndIfResultContains 20, "", "NUROUTEM01", "", "", "RE,SA"
    
    If GetResult("", "OLINCIS001", "", "", return_result) Then
        If IsNumeric(return_result) Then
            If return_result >= 2 Then
                SetInd 20, "OLINCIS001 return was #incisions=" & return_result
            End If
        End If
    End If
    
    If inds(20).checked Then Exit Sub
    If GetResult("", "OLSK72", "", "", return_result) Then
        If IsNumeric(return_result) Then
            If return_result >= 2 Then
                SetInd 20, "OLSK72 return was #wounds=" & return_result
            ElseIf return_result = 1 Then
                SetInd 19, "OLSK72 return was #wounds=" & return_result
            End If
        End If
    End If
    
    If inds(20).checked Then Exit Sub
    If GetResult("", "OLSK127", "", "", return_result) Then
        If IsNumeric(return_result) Then
            If return_result >= 3 Then
                SetInd 20, "OLSK127 return was #pressulcers=" & return_result
            ElseIf return_result >= 1 Then
                SetInd 19, "OLSK127 return was #pressulcers=" & return_result
            End If
        End If
    End If
    
    If inds(20).checked Then Exit Sub
    
    SetIndIfResultContains 20, "", "OLINCIS002", "", "", "DEH,NDAP,DR"
    SetIndIfResultContains 20, "", "OLINCIS028", "", "", "DEH,NDAP,DR"
    SetIndIfResultContains 20, "", "OLINCIS034", "", "", "DEH,NDAP,DR"
    SetIndIfResultContains 20, "", "OLINCIS005", "", "", "DEH,NDAP,DR"
    SetIndIfResultContains 20, "", "OLINCIS029", "", "", "DEH,NDAP,DR"
    SetIndIfResultContains 20, "", "OLINCIS035", "", "", "DEH,NDAP,DR"
    
    If inds(20).checked Then Exit Sub
    SetIndIfResultContains 20, "", "OLINCIS007", "", "", "DEH,NDAP,DR"
    SetIndIfResultContains 20, "", "OLINCIS030", "", "", "DEH,NDAP,DR"
    SetIndIfResultContains 20, "", "OLINCIS036", "", "", "DEH,NDAP,DR"
    SetIndIfResultContains 20, "", "OLINCIS009", "", "", "DEH,NDAP,DR"
    SetIndIfResultContains 20, "", "OLINCIS031", "", "", "DEH,NDAP,DR"
    SetIndIfResultContains 20, "", "OLINCIS037", "", "", "DEH,NDAP,DR"

    If inds(20).checked Then Exit Sub
    SetIndIfResultContains 20, "", "OLINCIS011", "", "", "DEH,NDAP,DR"
    SetIndIfResultContains 20, "", "OLINCIS032", "", "", "DEH,NDAP,DR"
    SetIndIfResultContains 20, "", "OLINCIS038", "", "", "DEH,NDAP,DR"
    SetIndIfResultContains 20, "", "OLINCIS013", "", "", "DEH,NDAP,DR"
    SetIndIfResultContains 20, "", "OLINCIS033", "", "", "DEH,NDAP,DR"
    SetIndIfResultContains 20, "", "OLINCIS039", "", "", "DEH,NDAP,DR"

    If inds(20).checked Or inds(19).checked Then Exit Sub
    
    SetIndIfResultContains 19, "", "OLINCIS003", "", "", "COLN,ILEAN,ILEON,URON"
    SetIndIfResultContains 19, "", "OLINCIS004", "", "", "COLN,ILEAN,ILEON,URON"
    SetIndIfResultContains 19, "", "OLINCIS006", "", "", "COLN,ILEAN,ILEON,URON"
    SetIndIfResultContains 19, "", "OLINCIS008", "", "", "COLN,ILEAN,ILEON,URON"
    SetIndIfResultContains 19, "", "OLINCIS010", "", "", "COLN,ILEAN,ILEON,URON"
    SetIndIfResultContains 19, "", "OLINCIS012", "", "", "COLN,ILEAN,ILEON,URON"
    


    SetIndIfFound 19, "", "402100"
    SetIndIfFound 19, "", "688000"
    
    
    If inds(19).checked Then Exit Sub
    If Exists("", "OLACUITY17", , , "Y") Then
        SetIndIfResultContains 19, "", "OLIV255", "", "", "A,B,C,G,H,ID,IS,M,NEO,PAC,PIC,P,Q,TE,TEMPDIAL,U"
    End If
    
    If inds(19).checked Then Exit Sub
    If Exists("", "OLACUITY18", , , "Y") Then
        SetIndIfResultContains 19, "", "OLIV263", "", "", "A,B,C,G,H,ID,IS,M,NEO,PAC,PIC,P,Q,TE,TEMPDIAL,U"
    End If
    
    If inds(19).checked Then Exit Sub

    SetIndIfFound 19, "", "ABDPERINRESS", "", "", "", SearchSinceAdmission
    SetIndIfFound 19, "", "NURI59"
    SetIndIfFound 19, "", "NURO80"
    SetIndIfFound 19, "", "NURO65"
    SetIndIfFound 19, "", "NURO66"
    If inds(19).checked Then Exit Sub
    SetIndIfFound 19, "", "NURO63"
    SetIndIfFound 19, "", "NURO64"
    SetIndIfFound 19, "", "NUROA060"
    
    If inds(19).checked Then Exit Sub
    SetIndIfResultContains 19, "", "NSGI019", "", "", "Y"
    SetIndIfResultContains 19, "", "OLACUITY09", "", "", "Y"
    SetIndIfResultContains 19, "", "NSGI027", "", "", "Y"
    SetIndIfFound 19, "", "693000"
    If inds(19).checked Then Exit Sub
    'SetIndIfFound 19, "", "402000"
    SetIndIfFound 19, "", "1013400"
    'SetIndIfFound 19, "", "402200"
    SetIndIfFound 19, "", "187201"
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_19_20"
    Resume  'debug
End Sub


Private Sub Check_21()
    On Error GoTo errHandler
    
    Dim mins As Integer
    Dim chart_result As String
    
    dvprint "---------------"
    dvprint "21. Healthcare Mgmt Education >= 1 Hour"
    dvprint "---------------"
    
    SetIndIfFound 21, "", "1011122"
    SetIndIfFound 21, "", "560800"
    SetIndIfFound 21, "", "560400"
    SetIndIfFound 21, "", "560425"
    SetIndIfFound 21, "", "560450"
    SetIndIfFound 21, "", "580475"
    SetIndIfFound 21, "", "1019400"
    SetIndIfFound 21, "", "1019425"
    SetIndIfFound 21, "", "1019450"
    SetIndIfFound 21, "", "1019475"
    SetIndIfFound 21, "", "1019600"
    SetIndIfFound 21, "", "1019605"
    SetIndIfFound 21, "", "1019610"
    SetIndIfFound 21, "", "1019615"
    SetIndIfFound 21, "", "560570"
    SetIndIfFound 21, "", "560620"
    SetIndIfFound 21, "", "560520"
    SetIndIfFound 21, "", "560329"
    SetIndIfFound 21, "", "560300"
    SetIndIfFound 21, "", "560310"
    SetIndIfFound 21, "", "560320"
    SetIndIfFound 21, "", "1016500"
    SetIndIfResultContains 21, "", "OLACUITY75", "", "", "Y"
    SetIndIfResultContains 21, "", "OLACUITY29", "", "", "Y"
    SetIndIfResultContains 21, "", "OLACUITY52", "", "", "Y"
    SetIndIfResultContains 21, "", "OLACUITY53", "", "", "Y"
    SetIndIfResultContains 21, "", "OLACUITY08", "", "", "Y"

    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_21"
    Resume  'debug
End Sub

Private Sub Check_22()
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "22. 1 to 1 Physiological Interv. >= 2 Hours"
    dvprint "---------------"

    SetIndIfFound 22, "", "404500"
    SetIndIfFound 22, "", "404600"
    SetIndIfResultContains 22, "", "OLACUITY11", "", "", "Y"
    SetIndIfResultContains 22, "", "OLACUITY64", "", "", "Y"

    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_22"
    Resume  'debug
End Sub
Private Function VerifyProcTime(ptime As String, returndt As Date, Optional afterdt As Date = 0) As Boolean
    Dim ok As Boolean
    Dim effdate As String
    Dim efftime As String

    ok = True
    ptime = Trim$(ptime)
    If Len(ptime) > 4 Then
        ok = False
        dvprint ("Procedure time incorrectly formatted: '" & ptime & "'")
    End If
    
    If Not ok Then Exit Function
    
    If Len(ptime) < 4 Then
        dvprint ("Procedure time will be pre-padded with zeroes: '" & ptime & "'")
        ptime = String$(4 - Len(ptime), "0") & ptime
    End If
    
    If IsNumeric(ptime) Then
        If ptime < "2400" And Mid$(ptime, 3, 2) <= "59" Then
            If afterdt = 0 Then 'treat ptime relative to g_effdt
                effdate = Format$(g_effdt, "yyyymmdd")
                efftime = Format$(g_effdt, "hhnn")
                If ptime <= efftime Then
                    returndt = g_util.CDateEx(effdate & ptime)
                Else
                    returndt = g_util.CDateEx(Format$(DateAdd("d", -1, g_util.DateOnly(g_effdt)), "yyyymmdd") & ptime)
                End If
            Else 'treat ptime so that it should be after afterdt
                effdate = Format$(afterdt, "yyyymmdd")
                efftime = Format$(afterdt, "hhnn")
                If ptime >= efftime Then
                    returndt = g_util.CDateEx(effdate & ptime)
                Else
                    returndt = g_util.CDateEx(Format$(DateAdd("d", 1, g_util.DateOnly(afterdt)), "yyyymmdd") & ptime)
                End If
            End If
        Else
            ok = False
            dvprint ("Procedure time not valid military time: '" & ptime & "'")
        End If
    Else
        ok = False
        dvprint ("Procedure time is non-numeric: '" & ptime & "'")
    End If
    
    VerifyProcTime = ok

End Function

Private Sub ProcessProc(pnum As Integer, code1 As String, code2 As String, code3 As String)
    Dim stime As String
    Dim etime As String
    Dim psdt As Date
    Dim pedt As Date
    Dim mins As Integer

    If Exists("", code1, , , "Y") Then
        If GetResult("", code2, "", "", stime) Then
            'stime is HHMM only.  assume it's the nearest HHMM previous to g_effdt.
            ' say g_effdt = 201009180700 and stime=2300.  then pstime=201009172300
            If VerifyProcTime(stime, psdt) Then
                If GetResult("", code3, "", "", etime) Then
                    If VerifyProcTime(etime, pedt, psdt) Then
                        numprocs = numprocs + 1
                        With procs(numprocs)
                            .pnum = pnum
                            .start = psdt
                            .finish = pedt
                        End With
                        dvprint ("Set Proc " & pnum)
                        If code1 = "OLACUITY21" Then
                            mins = DateDiff("n", psdt, pedt)
                            If mins >= 60 Then
                                SetInd 21, "OLACUITY21 with open-ended time"
                            Else
                                dvprint ("OLACUITY21 is marked but duration is only " & mins & " minutes.")
                            End If
                        End If
                    Else
                        numprocs = numprocs + 1
                        With procs(numprocs)
                            .pnum = pnum
                            .start = psdt
                            .finish = 0
                        End With
                        dvprint ("Set Proc " & pnum & " using open-ended out time: " & code3 & " was invalid format='" & etime & "'")
                        If code1 = "OLACUITY21" Then
                            SetInd 21, "OLACUITY21 with open-ended time"
                        End If
                    End If
                Else 'default to end of day
                    numprocs = numprocs + 1
                    With procs(numprocs)
                        .pnum = pnum
                        .start = psdt
                        .finish = 0
                    End With
                    dvprint ("Set Proc " & pnum & " using open-ended out time: " & code3 & " not found")
                    If code1 = "OLACUITY21" Then
                        SetInd 21, "OLACUITY21 with open-ended time"
                    End If
                End If
            End If
        Else
            dvprint ("Procedure was not processed: " & code2 & " not found")
        End If
    End If


End Sub

Private Sub CheckProcs()
    Dim return_result As String
    Dim proc8, proc9 As Boolean
    
    On Error GoTo errHandler
    dvprint "---------------"
    dvprint "P1. 1-1 safety observation by non-RN"
    dvprint "---------------"
    
    ProcessProc 1, "OLAMBPROG8", "OLAMBPRO02", "OLAMBPR03"

    dvprint "---------------"
    dvprint "P2. Off unit accompanied by RN"
    dvprint "---------------"
    
    ProcessProc 2, "OLACUITY25", "OLACUITY30", "OLACUITY31"
    
    dvprint "---------------"
    dvprint "P3. Off unit accompanied by non-RN"
    dvprint "---------------"
    
    ProcessProc 3, "OLACUITY20", "OLACUITY32", "OLACUITY33"
    
    dvprint "---------------"
    dvprint "P4. Patient/family education by RN"
    dvprint "---------------"
    
    ProcessProc 4, "OLACUITY21", "OLACUITY45", "OLACUITY46"

    dvprint "---------------"
    dvprint "P5. Extensive wound management by RN"
    dvprint "---------------"
    
    ProcessProc 5, "OLACUITY22", "OLACUITY34", "OLACUITY35"

    dvprint "---------------"
    dvprint "P6. Extensive wound management by non-RN"
    dvprint "---------------"
    
    ProcessProc 6, "OLACUITY23", "OLACUITY36", "OLACUITY37"

    dvprint "---------------"
    dvprint "P7. Coordination of care by RN"
    dvprint "---------------"
    
    ProcessProc 7, "OLACUITY24", "OLACUITY43", "OLACUITY44"

    dvprint "---------------"
    dvprint "P8&P9. 1-1 or 2-1 by RN at bedside"
    dvprint "---------------"

    'olacuity59 and olacuity60 and ptunitmatchescode on both
    proc8 = False
    proc9 = False
    If GetResult("", "OLACUITY59", "", "", return_result) Then
        If PtUnitMatchesCode(return_result) Then
            ProcessProc 8, "OLACUITY26", "OLACUITY38", "OLACUITY39"
            If procs(numprocs).pnum = 8 Then proc8 = True
            If GetResult("", "OLACUITY60", "", "", return_result) Then
                If PtUnitMatchesCode(return_result) Then
                    ProcessProc 9, "OLACUITY27", "OLACUITY41", "OLACUITY42"
                    If procs(numprocs).pnum = 9 Then proc9 = True
                End If
            End If
        End If
    End If
    
    If proc9 Then
        If proc8 Then procs(numprocs - 1) = procs(numprocs)
        With procs(numprocs)
            .pindex = 0
            .start = 0
            .finish = 0
            .isvalid = False
            .pnum = 0
        End With
        numprocs = numprocs - 1
    End If
    
'    dvprint "---------------"
'    dvprint "P8. 1-1 by RN at bedside"
'    dvprint "---------------"
'
'    'olacuity59 and ptunitmatchescode
'    If GetResult("", "OLACUITY59", "", "", return_result) Then
'        If PtUnitMatchesCode(return_result) Then
'            ProcessProc 8, "OLACUITY26", "OLACUITY38", "OLACUITY39"
'        End If
'    End If

    
    Exit Sub

errHandler:
    g_util.ThrowError "CheckProcs"
    Resume  'debug
End Sub
Private Sub CheckOutcomes()
    Dim return_result As String
    
    'How to handle multiple outcomes per pt, each with different times?
    dvprint "---------------"
    dvprint "Outcomes: FALL"
    dvprint "---------------"
    If GetResult("", "FALL", "", "", return_result, SearchOutcomesRange) Then
        numoutcomes = numoutcomes + 1
        With oc(numoutcomes)
            .checked = True
            .pnum = 1
            .start = return_result
        End With
    End If
    
    dvprint "---------------"
    dvprint "Outcomes: MED"
    dvprint "---------------"
    If GetResult("", "MED", "", "", return_result, SearchOutcomesRange) Then
        numoutcomes = numoutcomes + 1
        With oc(numoutcomes)
            .checked = True
            .pnum = 2
            .start = return_result
        End With
    End If
    
    dvprint "---------------"
    dvprint "Outcomes: PROC"
    dvprint "---------------"
    If GetResult("", "PROC", "", "", return_result, SearchOutcomesRange) Then
        numoutcomes = numoutcomes + 1
        With oc(numoutcomes)
            .checked = True
            .pnum = 21
            .start = return_result
        End With
    End If
    
    Exit Sub

errHandler:
    g_util.ThrowError "CheckOutcomes"
    Resume  'debug
End Sub


Private Sub AtLeastOneADL()
    If Not (inds(1).checked Or inds(2).checked Or inds(3).checked) Then
        'Note: Heather wants "#2 ADL - Assist" to be the default.  (90% of patients)
        SetInd 2, "at least one ADL"
    End If
End Sub

Private Sub HighestIndicatorInEachGroupWins()
    Dim i As Integer
    Dim g As Integer
    Dim highest_is_on As Boolean
    Dim ind_list As String

    dvprint "---------------"
    dprint "Select highest indicator in each group"
    
    g = 0
    highest_is_on = False
    For i = MAX_INDS To 1 Step -1
        If (inds(i).group > 0) Then
            If (inds(i).group <> g) Then
                'this is a new group
                g = inds(i).group
                highest_is_on = inds(i).checked
            Else
                'same group
                If highest_is_on Then
                    inds(i).checked = False             'uncheck a lower number
                Else
                    highest_is_on = inds(i).checked     'save this one
                End If
            End If
        End If
    Next i
    
    'Echo the indicators for an audit (no classification will be saved)
    If g_debug And g_no_output Then
        For i = 1 To MAX_INDS
            If inds(i).checked Then ind_list = ind_list & "," & i
        Next i
        dprint "Final list = " & Mid$(ind_list, 2)
    End If

End Sub

Private Sub OutputClass()
    Dim outstr As String, ind_list As String
    Dim i As Integer
    Dim txarea As String

    txarea = ""
    If m_pat.unit_id = 108 Then 'surg/ortho/peds
        If m_pat.age <= 16 Then txarea = "Pediatrics"
    End If
        
    outstr = g_util.FixedWidth("", 8)                                       '(facility code)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.unit_name, 16)
    outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(unit code)
    outstr = outstr & "|" & g_util.FixedWidth(txarea, 16)                       '(area code)
    outstr = outstr & "|" & g_util.FixedWidth("", 8)                        '(class date - give datetime instead)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.acct, 20)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.last_name, 32)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.first_name, 32)
    outstr = outstr & "|" & g_util.FixedWidth("", 32)                       '(middle)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.room, 8)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.bed, 4)
    outstr = outstr & "|" & Format$(m_pat.pull_finish, "yyyymmddhhnn")      'class datetime
    outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(login)
    outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(employee)
    outstr = outstr & "|" & g_util.FixedWidth(CStr(m_pat.meth_id), 4)
    outstr = outstr & "|" & g_util.FixedWidth("C", 1)                       'record type = class
    outstr = outstr & "|" & g_util.FixedWidth("", 4)                        '(stage)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.TC_source_id, 4)        'TC source ID
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.range, 4)               'TC pull range
    outstr = outstr & "|"
    outstr = g_util.FixedWidth(outstr, 294)
    outstr = outstr & "|" & Format$(m_pat.effective, "yyyymmddhhnn")        'IN
    outstr = g_util.FixedWidth(outstr, 377)
    outstr = outstr & "|"
    
    For i = 1 To MAX_INDS
        If (inds(i).checked) Then
            outstr = outstr & "Y"
            ind_list = ind_list & "," & i
        Else
            outstr = outstr & "N"
        End If
    Next i
    ind_list = Mid$(ind_list, InStr(1, ind_list, ",") + 1)                'strip leading comma
    
    Print #outfile, outstr
    
    AddLogEntry EVENT_TYPE_INFO, "Classified: " & ind_list, EVENT_CATEGORY_PROCESSED
End Sub

Private Sub OutputProcs()
    Dim i, j As Integer
    Dim s, f As Date
    Dim outstr As String, proc_list As String
    
    For i = 1 To numprocs
        procs(i).isvalid = True
    Next i
    For i = 1 To numprocs - 1
        If procs(i).isvalid Then
        For j = i + 1 To numprocs
            If procs(j).isvalid And procs(j).start = procs(i).start And procs(j).finish = procs(i).finish Then   'this can be combined.
                procs(j).isvalid = False
                procs(j).pindex = i
            End If
        Next j
        End If
    Next i

    For i = 1 To numprocs
        If procs(i).isvalid Then
        outstr = g_util.FixedWidth("", 8)                                       '(facility code)
        outstr = outstr & "|" & g_util.FixedWidth(m_pat.unit_name, 16)
        outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(unit code)
        outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(area code)
        outstr = outstr & "|" & g_util.FixedWidth("", 8)                        '(class date - give datetime instead)
        outstr = outstr & "|" & g_util.FixedWidth(m_pat.acct, 20)
        outstr = outstr & "|" & g_util.FixedWidth(m_pat.last_name, 32)
        outstr = outstr & "|" & g_util.FixedWidth(m_pat.first_name, 32)
        outstr = outstr & "|" & g_util.FixedWidth("", 32)                       '(middle)
        outstr = outstr & "|" & g_util.FixedWidth(m_pat.room, 8)
        outstr = outstr & "|" & g_util.FixedWidth(m_pat.bed, 4)
        outstr = outstr & "|" & Format$(procs(i).start, "yyyymmddhhnn")       '204 procdt
        outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(login)
        outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(employee)
        outstr = outstr & "|" & g_util.FixedWidth(CStr(m_pat.meth_id), 4)
        outstr = outstr & "|" & g_util.FixedWidth("P", 1)                       'record type = class
        outstr = outstr & "|"
        outstr = outstr & Space$(294 - Len(outstr))
        outstr = outstr & "|" & Format$(procs(i).start, "yyyymmddhhnn")       '296 procdt in
        outstr = outstr & Space$(346 - Len(outstr))
        If procs(i).finish = 0 Then
            outstr = outstr & "|" & Space$(12)
        Else
            outstr = outstr & "|" & Format$(procs(i).finish, "yyyymmddhhnn")      '348 procdt out
        End If
        outstr = g_util.FixedWidth(outstr, 377)
        outstr = outstr & "|NNNNNNNNN"
        Mid$(outstr, 378 + procs(i).pnum, 1) = "Y"
        proc_list = proc_list & "," & procs(i).pnum
        For j = i + 1 To numprocs
            If Not procs(j).isvalid And procs(j).pindex = i Then
                Mid$(outstr, 378 + procs(j).pnum, 1) = "Y"
                proc_list = proc_list & "," & procs(j).pnum
            End If
        Next j
        proc_list = Mid$(proc_list, InStr(1, proc_list, ",") + 1)          'strip leading comma
        
        Print #outfile, outstr
        
        AddLogEntry EVENT_TYPE_INFO, "Procedure: " & proc_list, EVENT_CATEGORY_PROCESSED
        End If 'isvalid
    Next i

End Sub
Private Sub OutputOutcomes()
    Dim i, j As Integer
    Dim s, f As Date
    Dim outstr As String
    Dim octime As String

    If numoutcomes = 0 Then Exit Sub
    
    For i = 1 To numoutcomes
        If (oc(i).checked) Then
            outstr = g_util.FixedWidth("", 8)
            outstr = outstr & "|" & g_util.FixedWidth(m_pat.unit_name, 16)
            outstr = outstr & "|" & g_util.FixedWidth("", 16)
            outstr = outstr & "|" & g_util.FixedWidth("", 16)
            outstr = outstr & "|" & g_util.FixedWidth(oc(i).start, 12)
            outstr = outstr & "|" & g_util.FixedWidth(m_pat.acct, 20)
            outstr = outstr & "|" & g_util.FixedWidth(oc(i).pnum, 3)
            Print #outfile2, outstr 'Print line to outcomesindicator.TXT
        End If
    Next i
End Sub

Private Sub SetADLCompleteWhenAge(agecond As String)  ' agecond example: "<=3"
    Dim sql As String
    Dim rs As New Recordset

'
'select case when round(age_at_admission,0,1) <=55 then 1 else 0 end from encounter where encounter_id=6990

    sql = "select case when round(age_at_admission,0,1) " & agecond & " then 1 else 0 end from encounter " & WHERE_ENCOUNTER
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        If rs(0) = 1 Then
            SetInd 3, "Age <=3 years"
        End If
    End If
    rs.Close

End Sub

Private Function PtUnitMatchesCode(code As String) As Boolean

'2 = ICU
'3 = Surg/Ortho/Peds
'3W= Women's Center
'4 = Cardiovascular
'5 = Med/Oncology
'N = Nursery
    Select Case code
    Case "2"
        PtUnitMatchesCode = (m_pat.unit_id = 106) '"2ICU")
    Case "3"
        PtUnitMatchesCode = (m_pat.unit_id = 108) '"3E" Or UCase$(m_pat.unit_name) = "3N" Or UCase$(m_pat.unit_name) = "3S")
    Case "3W"
        PtUnitMatchesCode = (m_pat.unit_id = 107) '"3W")
    Case "4"
        PtUnitMatchesCode = (m_pat.unit_id = 109)  '"4N" Or UCase$(m_pat.unit_name) = "4S" Or UCase$(m_pat.unit_name) = "4W")
    Case "5"
        PtUnitMatchesCode = (m_pat.unit_id = 110)  '"5N" Or UCase$(m_pat.unit_name) = "5S" Or UCase$(m_pat.unit_name) = "5W")
    Case "N"
        PtUnitMatchesCode = (m_pat.unit_id = 111)  '"3WNSY")
    End Select

End Function



'CHANGES FOR 7/13
'Add changes for 4/27 in mapping table.
'Also make NURBSCHEC3 Persistent.
'have Justin push out to all machines
'how to make icons smaller?

'select classification_event_id from classification_event
'where timestamp = (select max(timestamp) from classification_event)

