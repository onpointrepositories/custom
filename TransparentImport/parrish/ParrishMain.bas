Attribute VB_Name = "ParrishMain"
Option Explicit
'
' Parrish transparent classification
'
' Each unit must set the "use transparent" flag.
'
' This is designed to be run as a background job, so never stop for user input -- that means no error MsgBox.
' All communication is done through log files and the event log; it is OK to print to the console.
'
' In order to be version independent, this program does not use any AcuityPlus dlls.
' Some AcuityPlus utility classes have been copied here.
'
' In order to work with the AcuityPlus Patient Selection and Transparent import:
'
'   * The program name must be TransparentMapping.exe, placed in AcuityPlus\bin
'   * The audit file is called TransparentAudit.log, placed in AcuityPlus\log
'   * The output file is Transparent.txt, placed in AcuityPlus\load_me
'   * Events are saved in the database event log
'
Public g_cnADO          As ADODB.Connection             'main DB connection
Public g_util           As New PFSUtility               'utility functions
Public g_dbutil         As New PFSDBUtility
Public g_event          As New PFSEventLog
Public g_display        As New PFSDisplayProperties
Public g_command        As String                       'command line (for db utility)
Public g_abort          As Boolean

Private Const TRANSP_FILENAME = "Transparent.txt"       'THE output file
Private Const TRANSP_AUDIT_LOG = "TransparentAudit.log" 'The debug/audit log
Private Const CHART_ITEM_LIFE = 30                      'days in the chart_item table
Private Const OUTCOMES_FILENAME = "OutcomesIndicator.txt"   'outcomes output file


'c:\FTP ISACUITYTEST\transparency\acuity_plus_problem_list
'c:\FTP ISACUITYTEST\transparency\acuity_plus_queries
'c:\FTP ISACUITYTEST\transparency\acuity_plus_interventions___meds
'c:\FTP ISACUITYTEST\transparency\outcomes_yyyymmddhhnn.txt
Private Const INPUT_TESTPATH = "c:\FTP ISACUITYTEST\transparency\"
Private Const INPUT_PRODPATH = "c:\FTP ISACUITYPLUS\transparency\"
Private Const INPUT_QFILE = "acuity_plus_queries"
Private Const INPUT_IFILE = "acuity_plus_interventions___meds"
Private Const INPUT_PFILE = "acuity_plus_problem_list"
Private Const INPUT_OFILE = "outcomes"

Private Const DEFAULT_RANGE = 1440                      '24 hrs in minutes; overridden by -range

Public Type PatientInfo
    unit_id             As Long
    encounter_id        As Long
    meth_id             As Long
    last_name           As String
    first_name          As String
    acct                As String
    age                 As Single                   'age (in years) at admission
    unit_name           As String
    room                As String
    bed                 As String
    unit_arrival        As Date
    effective           As Date                     'patient specific (may be > g_effdt)
    pull_start          As Date                     'patient specific (may be > g_pull_start)
    pull_finish         As Date                     'patient specific (may be < g_pull_finish)
    range               As Integer                  'patient specific (may be < g_range)
    TC_source_id        As Integer
End Type

Public g_debug          As Boolean
Public g_verbose        As Boolean                  'verbose debug output?
Public g_no_output      As Boolean      'if no ouput, then no input files either

Public g_effdt          As Date                     'effective datetime

Public outfile          As Integer
Public outfile2          As Integer
Public dbugfile         As Integer

Public gLogUnitID       As Long
Public gLogEncounterID  As Long
Public gLogSourceText   As String

Public g_pull_start     As Date
Public g_pull_finish    As Date                     'pull datetime
Public g_range          As Long

Private winpfspath      As String                   'path of winpfs
Private winlogpath      As String                   'path of winpfs\log
Private winloadpath     As String                   'path of winpfs\load_me
Private outfilename     As String
Private outfile2name    As String
Private dbugname        As String

Private nowdt           As Date
Private debug_acct      As String                   'acct filter
Private debug_unit      As String                   'unit filter
Private debug_unit_id   As Long

Private inputqfile      As Integer
Private inputifile      As Integer
Private inputpfile      As Integer
Private inputofile      As Integer
Private inputqname      As String
Private inputiname      As String
Private inputpname      As String
Private inputoname      As String

Sub Main()
    On Error GoTo errHandler
    
    frmMain.Show vbModeless                         'progress window
    DoEvents
    
    Set g_cnADO = g_dbutil.NewRemoteConnection
    
    gLogSourceText = Command$
    LogInfo "Begin mapping and translation", EVENT_CATEGORY_STARTUP_SHUTDOWN
    gLogSourceText = ""
    
    ParseCommandLine
    OpenOutputFiles
    OpenInputFiles
    WriteDataToCI
    DeleteInputFiles
    Process
    CloseOutputFiles
    DeleteOldLogs
    DeleteOldChartItems

    LogInfo "Mapping complete", EVENT_CATEGORY_STARTUP_SHUTDOWN
    g_cnADO.Close
normalExit:
    Unload frmMain
    Exit Sub

errHandler:
    LogError Err.Description & " in " & Err.source
    LogInfo "Unexpected error - shut down", EVENT_CATEGORY_STARTUP_SHUTDOWN
    GoTo normalExit
    Resume  'debug
End Sub

Private Sub ParseCommandLine()
    On Error GoTo errHandler

    Dim argc As Integer, argv() As String, subarg() As String, tag As String, value As String
    Dim i As Integer, n As Integer
    Dim effdate As String, efftime As String
    Dim pulldate As String, pulltime As String
    Dim nowdate As String, nowtime As String
    
    '-effdate=yyyymmdd   This is the date which, in combination with the
    '                   -efftime time is used to specify the classification In time.
    '                   If not specified, -effdate is assumed to be the current date.
    'Special value:  -effdate=yesterday
    
    '-efftime=hhmm      Used to set the classification In time.
    '                   If not specified, then -efftime is assumed to be the current time.
        
    '-pulldate=yyyymmdd This is the date/time of the data pull time.
    '                   If not specified, then this is assumed to be the -effdate.
    '-pulltime=hhmm     This is the time starting from which the pull is to
    '                   look backwards from.
    '                   If not specified, then this time is assumed to be the -efftime.
    
    '-range=nnnn        This is the number of minutes backwards from the pull time
    '                   that defines valid g_range of charting events.
    '                   If not specified, this will default to 1440 (24 hours)

    '-debug             echo debug/audit info; save in log file
    '-brief             Minimal debug info -- sets -debug
    '-verbose           Lots of debugging info -- sets -debug
    '-n                 No output to transparent.txt - debug only
    '-audit             sets -debug and -n
    '
    '-acct=12345        Process this patient only
    '-unit=5N           Process this unit only
    '-unit_id=1234      process this unit only
    
    nowdt = Now
    nowdate = Format$(nowdt, "yyyymmdd")
    nowtime = Format$(nowdt, "hhnn")
    
    g_verbose = True

    argc = g_util.SplitTextOnChar(LCase$(Command$), " ", argv(), 1, 1)

    For i = 1 To argc
        n = g_util.SplitTextOnChar(argv(i), "=", subarg(), 1, 2)        '<tag>=<value>
        tag = subarg(1)
        value = subarg(2)
        
        Select Case tag
        Case "-acct"
            debug_acct = value
            
        Case "-audit"
            g_debug = True
            g_no_output = True
        
        Case "-brief"
            g_verbose = False
            g_debug = True
        
        Case "-debug"
            g_debug = True
        
        Case "-effdate"
            If (value = "yesterday") Then
                effdate = Format(DateAdd("d", -1, Date), "yyyymmdd")
            Else
                effdate = value
            End If
        
        Case "-efftime"
            efftime = Left$(value, 4)
            efftime = String$(4 - Len(efftime), "0") & efftime
        
        Case "-n"
            g_no_output = True

        Case "-pulldate"
            pulldate = value
        
        Case "-pulltime"
            pulltime = Left$(value, 4)
            pulltime = String$(4 - Len(pulltime), "0") & pulltime

        Case "-range"
            g_range = CInt(value)
            
        Case "-unit"
            debug_unit = value
        Case "-unit_id"
            debug_unit_id = CLng(value)
            
        Case "-verbose"
            g_verbose = True
            g_debug = True

        Case Else
            LogWarning "Unexpected argument: " & tag
        End Select
    Next i

    If Len(effdate) = 0 Then effdate = nowdate
    If Len(efftime) = 0 Then efftime = nowtime
    
    'Note: pulldate defaults to effdate, not nowdate
    If Len(pulldate) = 0 Then pulldate = effdate
    If Len(pulltime) = 0 Then pulltime = efftime

    If g_range = 0 Then g_range = DEFAULT_RANGE
    
    'Note: the log file isn't open yet so these just go to the screen
    Debug.Print "Command line: " & Command$
    Debug.Print "effdate=" & effdate
    Debug.Print "efftime=" & efftime
    Debug.Print "pulldate=" & pulldate
    Debug.Print "pulltime=" & pulltime
    Debug.Print "range=" & g_range
    
    g_effdt = g_util.CDateEx(effdate & efftime)
    
    g_pull_finish = g_util.CDateEx(pulldate & pulltime)     'for chart item queries
    g_pull_start = DateAdd("n", -g_range, g_pull_finish)
    Exit Sub
    
errHandler:
    g_util.ThrowError "ParseCommandLine"
    Resume  'debug
End Sub


Private Sub Process()
    On Error GoTo errHandler
    
    Dim pat As PatientInfo
    Dim rs As New Recordset
    Dim sql As String
    Dim count As Integer

    dprint ""
    dprint "Pull at:   " & g_pull_finish & " (back to " & g_pull_start & "; range=" & g_range & ")"
    dprint "Effective: " & g_effdt
        
    '
    ' Make a list of all patients with chart items during the pull period.
    ' Include only units that have the "use transparent" flag set.
    ' Limit to one patient if -acct is given.  Limit to one unit with -unit.
    ' Left join encounter_location - the patient may be discharged at pull time.
    ' Look for the latest patient arrival to the unit before the pull time; ignore transfers within the unit.
    ' Look for a patient departure (if any) before the pull time; ignore transfers within the unit.
    sql = "" & _
        " SELECT LIST.UNIT_ID, LIST.ENCOUNTER_ID, LIST.TC_SOURCE_ID, PARAM.METHODOLOGY_ID," & _
        "     UNIT.NAME AS UNIT, EL.ROOM, EL.BED, EL.DATETIME_IN AS BED_ARRIVAL," & _
        "     ARRIVE.DATETIME_IN AS UNIT_ARRIVAL, DEPART.DATETIME_OUT AS UNIT_DEPARTURE," & _
        "     E.ACCT_NUMBER, P.LAST_NAME, P.FIRST_NAME," & _
        "     P.DOB, E.ADMISSION_DATETIME, E.REGISTRATION_DATETIME" & _
        " FROM (" & _
        "     SELECT DISTINCT CI.UNIT_ID, CI.ENCOUNTER_ID, CI.TC_SOURCE_ID" & _
        "     FROM CHART_ITEM AS CI" & _
        "     INNER JOIN UNIT ON (UNIT.UNIT_ID = CI.UNIT_ID)" & _
        "     WHERE UNIT.USE_TRANSPARENT_CLASSIFICATION='Y'" & _
        "     AND EVENT_DATETIME BETWEEN " & g_dbutil.SQL_DateTime(g_pull_start) & " AND " & g_dbutil.SQL_DateTime(g_pull_finish) & _
        " ) AS LIST"
    sql = sql & _
        " INNER JOIN UNIT ON (UNIT.UNIT_ID = LIST.UNIT_ID)" & _
        " INNER JOIN UNIT_PARAM AS PARAM ON (PARAM.UNIT_ID = UNIT.UNIT_ID) AND (" & g_dbutil.SQL_DateTime(g_pull_finish) & " BETWEEN PARAM.EFFECTIVE_REPORT_DATE AND EXPIRATION_REPORT_DATE)" & _
        " INNER JOIN ENCOUNTER AS E ON (E.ENCOUNTER_ID = LIST.ENCOUNTER_ID)" & _
        " INNER JOIN PERSON AS P ON (P.PERSON_ID = E.PERSON_ID)" & _
        " LEFT  JOIN ENCOUNTER_LOCATION AS EL ON (EL.ENCOUNTER_ID = E.ENCOUNTER_ID) AND (" & g_dbutil.SQL_DateTime(g_pull_finish) & " BETWEEN EL.EFFECTIVE_DATETIME_IN AND EL.EFFECTIVE_DATETIME_OUT)" & _
        " INNER JOIN (" & _
        "    SELECT WORKING_UNIT_ID AS UNIT_ID, ENCOUNTER_ID, MAX(DATETIME_IN) AS DATETIME_IN" & _
        "    FROM ENCOUNTER_LOCATION AS EL" & _
        "    WHERE WORKING_UNIT_ID IN (SELECT UNIT_ID FROM UNIT WHERE USE_TRANSPARENT_CLASSIFICATION='Y')" & _
        "    AND DATETIME_IN < " & g_dbutil.SQL_DateTime(g_pull_finish) & _
        "    AND IS_TRANSFER_IN='Y'" & _
        "    GROUP BY WORKING_UNIT_ID, ENCOUNTER_ID" & _
        " ) AS ARRIVE ON (ARRIVE.UNIT_ID = UNIT.UNIT_ID) AND (ARRIVE.ENCOUNTER_ID = E.ENCOUNTER_ID)" & _
        " LEFT JOIN (" & _
        "    SELECT WORKING_UNIT_ID AS UNIT_ID, ENCOUNTER_ID, MAX(DATETIME_OUT) AS DATETIME_OUT" & _
        "    FROM ENCOUNTER_LOCATION AS EL" & _
        "    WHERE WORKING_UNIT_ID IN (SELECT UNIT_ID FROM UNIT WHERE USE_TRANSPARENT_CLASSIFICATION='Y')" & _
        "    AND DATETIME_OUT < " & g_dbutil.SQL_DateTime(g_pull_finish) & _
        "    AND ((NEXT_WORKING_UNIT_ID<>WORKING_UNIT_ID) OR (NEXT_WORKING_UNIT_ID IS NULL))" & _
        "    GROUP BY WORKING_UNIT_ID, ENCOUNTER_ID" & _
        " ) AS DEPART ON (DEPART.UNIT_ID = UNIT.UNIT_ID) AND (DEPART.ENCOUNTER_ID = E.ENCOUNTER_ID)" & _
        " WHERE 1=1"
    
    If Len(debug_acct) Then
        sql = sql & " AND E.ACCT_NUMBER=" & g_dbutil.SQL_String(debug_acct)
    End If
    If Len(debug_unit) Then
        sql = sql & " AND UNIT.NAME=" & g_dbutil.SQL_String(debug_unit)
    End If
    If (debug_unit_id > 0) Then
        sql = sql & " AND UNIT.UNIT_ID=" & debug_unit_id
    End If
    sql = sql & " ORDER BY UNIT.NAME, P.LAST_NAME, P.FIRST_NAME, E.ACCT_NUMBER"
    
    Debug.Print sql
    rs.Open sql, g_cnADO
    count = 0

    Do While Not rs.EOF
        'Package the patient info (don't use unit/encounter objects)
        With pat
            .unit_id = rs("UNIT_ID")
            .encounter_id = rs("ENCOUNTER_ID")
            .meth_id = rs("METHODOLOGY_ID")
            .acct = rs("ACCT_NUMBER")
            .last_name = rs("LAST_NAME") & ""
            .first_name = rs("FIRST_NAME") & ""
            .unit_name = rs("UNIT") & ""
            .room = rs("ROOM") & ""
            .bed = rs("BED") & ""
            
            If Not IsNull(rs("DOB")) And Not IsNull(rs("ADMISSION_DATETIME")) Then
                .age = DateDiff("n", rs("DOB"), rs("ADMISSION_DATETIME")) / 60# / 24# / 365.24219
            ElseIf Not IsNull(rs("DOB")) And Not IsNull(rs("REGISTRATION_DATETIME")) Then
                .age = DateDiff("n", rs("DOB"), rs("REGISTRATION_DATETIME")) / 60# / 24# / 365.24219
            Else
                .age = 0
            End If
            
            .unit_arrival = rs("UNIT_ARRIVAL")
            .effective = g_util.Max(g_effdt, .unit_arrival)
            .pull_start = g_util.Max(g_pull_start, .unit_arrival)
            .pull_finish = g_util.Min(g_pull_finish, rs("UNIT_DEPARTURE"))
            If (.pull_finish < .pull_start) Then
                .pull_finish = g_pull_finish
            End If
            .range = DateDiff("n", .pull_start, .pull_finish)
            .TC_source_id = g_dbutil.DBToInteger(rs("TC_SOURCE_ID"))
        End With
        
        'Save ids for log entries
        gLogUnitID = pat.unit_id
        gLogEncounterID = pat.encounter_id
    
        'Process this patient
        count = count + 1
        frmMain.SetProgress "Processing patient " & count & " of " & rs.RecordCount
        
        dvprint ""
        dprint String$(80, "=")
        dprint "Patient " & count & " of " & rs.RecordCount & ": " & _
            pat.last_name & ", " & pat.first_name & _
            " in " & pat.unit_name & " " & pat.room & " " & pat.bed & _
            " acct=" & pat.acct & " age=" & Round(pat.age, 2)
        dprint "Here from " & pat.pull_start & " to " & pat.pull_finish & _
            "  (LOS=" & Round(pat.range / 60, 2) & ")"

        
        'Most sites will have one source of chart items
        'If there are multiple sources that need different mapping, this is the place to do it
        Select Case pat.TC_source_id
'        Case 123
'            Select Case pat.meth_id
'            Case METH_ID_INPATIENT
'                ProcessInpatient_123 pat
'            Case Else
'                LogWarning "Methodology " & pat.meth_id & " in unit " & pat.unit_name & " is not supported"
'            End Select
        
        Case Else

            Select Case pat.meth_id
            Case METH_ID_INPATIENT
                ProcessInpatient pat
            Case Else
                LogWarning "Methodology " & pat.meth_id & " in unit " & pat.unit_name & " is not supported"
            End Select

        End Select
        
        gLogUnitID = 0
        gLogEncounterID = 0

        If g_abort Then Exit Do
        
        rs.MoveNext
    Loop
    
    rs.Close
    
    If (count < 1) Then
        dprint ""
        If Len(debug_acct) Then
            LogWarning "The selected patient has no chart items in the given time range", EVENT_CATEGORY_UNEXPECTED
        Else
            LogWarning "No chart items found to process - have the unit(s) been enabled for transparent classification?", EVENT_CATEGORY_UNEXPECTED
        End If
    End If
    
    Exit Sub

errHandler:
    g_util.ThrowError "Process"
    Resume  'debug
End Sub

'debug print
Public Sub dprint(s As String)
    If Not g_debug Then Exit Sub
    
    Debug.Print s                           'add to debug window
    'It would be nice if we could print to the cmd window, but it takes some effort

    If dbugfile <> 0 Then
        Print #dbugfile, s                  'add to debug log file
    End If
End Sub

'debug verbose print
Public Sub dvprint(s As String)
    If g_verbose Then dprint s
End Sub

Private Sub OpenOutputFiles()
    On Error GoTo errHandler
    
    If Not g_util.GetKeyValue(HKEY_LOCAL_MACHINE, "SOFTWARE\Quadramed\WinPFS", "Directory", winpfspath) Then
        winpfspath = App.Path & "\.."
    End If

    winlogpath = winpfspath & "\log"
    winloadpath = winpfspath & "\load_me"
    If Not g_util.DirExists(winlogpath) Then
        MkDir$ winlogpath
    End If

    'dbugname = winlogpath & "\" & TRANSP_DEBUG & Format$(nowdt, "mmdd") & ".log"
    dbugname = winlogpath & "\" & TRANSP_AUDIT_LOG
    
    If g_debug Then
        dbugfile = FreeFile
        Open dbugname For Output As #dbugfile                   'overwrite existing
        Print #dbugfile, String(80, "=")
        Print #dbugfile, "TRANSPARENT MAPPING AUDIT FILE                       Time=" & nowdt
    End If
    
    If g_no_output Then Exit Sub

    outfilename = winloadpath & "\" & TRANSP_FILENAME           'load_me\transparent.txt
    outfile = FreeFile
    Open outfilename For Append As #outfile                     'add to existing
    
    outfile2name = winloadpath & "\" & OUTCOMES_FILENAME           'load_me\outcomesindicator.txt
    outfile2 = FreeFile
    Open outfile2name For Append As #outfile2                     'add to existing
    
    Exit Sub

errHandler:
    g_util.ThrowError "OpenOutputFiles"
    Resume  'debug
End Sub
Private Sub CloseOutputFiles()
    If (outfile <> 0) Then Close #outfile
    If (outfile2 <> 0) Then Close #outfile2
    
    If (dbugfile <> 0) Then
        Close #dbugfile
        dbugfile = 0                                            'stop writing to debug file
    End If
End Sub
Private Sub OpenInputFiles()
    Dim p As String
    On Error GoTo errHandler
    
    If g_no_output Then Exit Sub 'if no ouput, then no input
    
    'There are 3 input files:  QUERIES, INTERVENTIONS, PROBLEMS
'    inputqname = winloadpath & "\" & INPUT_QFILE
'    inputiname = winloadpath & "\" & INPUT_IFILE
'    inputpname = winloadpath & "\" & INPUT_PFILE
    If g_util.DirExists(INPUT_TESTPATH) Then p = INPUT_TESTPATH
    If g_util.DirExists(INPUT_PRODPATH) Then p = INPUT_PRODPATH

    inputqname = p & INPUT_QFILE
    inputiname = p & INPUT_IFILE
    inputpname = p & INPUT_PFILE
    inputoname = p & INPUT_OFILE & "_" & Format$(g_effdt, "YYYYMMddHHmm") & ".TXT"

    
    CheckInFile inputqname, inputqfile
    CheckInFile inputiname, inputifile
    CheckInFile inputpname, inputpfile
    CheckInFile inputoname, inputofile
    
'    inputqfile = FreeFile
'    Open inputqname For Input As #inputqfile
'    inputifile = FreeFile
'    Open inputiname For Input As #inputifile
'    inputpfile = FreeFile
'    Open inputpname For Input As #inputpfile
    
    Exit Sub

errHandler:
    g_util.ThrowError "OpenInputFiles"
    Resume  'debug
End Sub

Private Sub CheckInFile(n As String, f As Integer)

    If g_util.FileExists(n) Then
        f = FreeFile
        Open n For Input As #f
    Else
        f = 0
    End If

End Sub

Private Sub DeleteInputFiles()
    If g_no_output Then Exit Sub
    
    If inputqfile <> 0 Then
        Close #inputqfile
        Name inputqname As inputqname & "_" & Format$(nowdt, "MMddHHmm") & ".TXT"
    End If
    If inputifile <> 0 Then
        Close #inputifile
        Name inputiname As inputiname & "_" & Format$(nowdt, "MMddHHmm") & ".TXT"
    End If
    If inputpfile <> 0 Then
        Close #inputpfile
        Name inputpname As inputpname & "_" & Format$(nowdt, "MMddHHmm") & ".TXT"
    End If
    If inputofile <> 0 Then
        Close #inputofile
        Name inputoname As inputoname & "_" & Format$(nowdt, "MMddHHmm") & ".TXT"
    End If
    
End Sub

Private Sub WriteDataToCI()
    Dim hdr As String
    Dim sql As String
    Dim s As String
    Dim buf As String
    Dim rows() As String
    Dim cols() As String
    Dim maxrow As Integer
    Dim iRow As Integer
    Dim skip As Boolean
    Dim UnitID As Long
    Dim encid As Long
    Dim dupevdt, dupcode, dupres As String
    
'CREATE TABLE dbo.CHART_ITEM (
'     UNIT_ID              int NOT NULL,
'     EVENT_DATETIME       datetime NOT NULL,
'     ENCOUNTER_ID         int NOT NULL,
'     CODE                 varchar(32) NOT NULL,
'       CATEGORY             varchar(64) NULL,
'       DESCRIPTION          varchar(128) NULL,
'       FIELD_NAME           varchar(64) NULL,
'       FREQUENCY_HOURS      real NULL,
'       PROCEDURE_START      datetime NULL,
'       PROCEDURE_FINISH     datetime NULL,
'       PROCEDURE_HOURS      real NULL,
'     RESULT               varchar(512) NULL,
'       EXPIRATION_DATETIME  datetime NULL,
'       SOURCE_TEXT          varchar(2048) NULL,
'       TC_SOURCE_ID         smallint NULL,
'     TIMESTAMP            datetime NOT NULL,
'       Primary key(unit_id, EVENT_DATETIME, encounter_id, code)
')

'=QUERY FILE=
'TCQUERY201007191039
'"00006593"|"GRADY,PATIENT1"|"4S"|"400"|"1"
'"201007160800"|"OLPA123"||||"P"
'"201007161200"|"OLPA123"||||"P"
'"201007161205"|"OLACUITY52"||||"Y"
'"201007161205"|"OLACUITY62"||||"Y"
'"201007161205"|"OLDA69"||||"Y"
'"201007161154"|"OEISO"||||"S"    no ass triggered.
'"00006601"|"GREGORY,PATIENT2"|"4S"|"401"|"1"
'"201007160800"|"OLAMBPROG8"||||"N"
'"201007161200"|"OLAMBPROG8"||||"N"
'"201007161302"|"OLDA69"||||"Y"
    On Error GoTo QUERIESerrHandler
    If inputqfile <> 0 Then
    s = Input(LOF(inputqfile), inputqfile)
    rows() = Split(s, vbCrLf)
    maxrow = UBound(rows)
    skip = True
    For iRow = 0 To maxrow
        buf = rows(iRow)
        buf = Replace(buf, """", "")
        buf = Replace(buf, "'", "")
        cols() = Split(buf, "|")
        If UBound(cols) < 2 Then 'skip this row
        ElseIf UBound(cols) >= 2 Then
            If cols(2) <> "" Then 'header- relies on check cols(2) for non-null
                UnitID = GetUnitIDFromUnitName(cols(2))
                encid = GetEncounterIDFromAcct(cols(0))
                skip = False
                If UnitID = 0 Or encid = 0 Then
                    skip = True
                Else
                    hdr = "insert into chart_item (unit_id,encounter_id,tc_source_id,timestamp,event_datetime,code,result,source_text) values ("
                    hdr = hdr & UnitID & ","
                    hdr = hdr & encid & ","
                    hdr = hdr & "1,"   'tc_source_id = 1
                End If
            ElseIf Not skip Then 'detail
                sql = hdr & g_dbutil.SQL_DateTime(g_effdt) & ","   'timestamp
                dupevdt = g_dbutil.SQL_DateTime(g_util.CDateEx(cols(0)))
                dupcode = g_dbutil.SQL_String(cols(1))
                dupres = g_dbutil.SQL_String(cols(5)) 'result
                sql = sql & dupevdt & ","  'event_datetime yyyymmddhhnn
                sql = sql & dupcode & "," 'code
                sql = sql & dupres & "," 'result
                sql = sql & g_dbutil.SQL_String(buf) & ")"
                g_cnADO.Execute sql
            End If
        End If
    Next iRow
    End If

'=INTERVENTIONS FILE=
'TCINTERVENTION201007191056
'"00006593"|"GRADY,PATIENT1"|"4S"|"400"|"1"
'"201007161000"|"1014960"||||
'"00006601"|"GREGORY,PATIENT2"|"4S"|"401"|"1"
'"201007160800"|"668000"||||
'"201007160800"|"1000426"||||
'|"MAGJ"||||                       med
'|"PIT"||||                        med
'"201007161200"|"668000"||||
'"201007161200"|"1000426"||||
'"201007161254"|"1011610"||||
'"201007161600"|"668000"||||
'"00006619"|"LYLE,PATIENT3"|"3S"|"311"|"1"
'"201007160800"|"668000"||||
'"201007160800"|"2000165"||||
'"201007161000"|"2000165"||||
    On Error GoTo errHandler
    If inputifile <> 0 Then
    s = Input(LOF(inputifile), inputifile)
    rows() = Split(s, vbCrLf)
    maxrow = UBound(rows)
    skip = True
    For iRow = 0 To maxrow
        buf = rows(iRow)
        buf = Replace(buf, """", "")
        buf = Replace(buf, "'", "")
        cols() = Split(buf, "|")
        If UBound(cols) < 2 Then 'skip this row
        ElseIf UBound(cols) >= 2 Then
            If cols(2) <> "" Then 'header- relies on check cols(2) for non-null
                UnitID = GetUnitIDFromUnitName(cols(2))
                encid = GetEncounterIDFromAcct(cols(0))
                skip = False
                If UnitID = 0 Or encid = 0 Then
                    skip = True
                Else
                    hdr = "insert into chart_item (unit_id,encounter_id,tc_source_id,timestamp,event_datetime,code,result,source_text) values ("
                    hdr = hdr & UnitID & ","
                    hdr = hdr & encid & ","
                    hdr = hdr & "1,"
                End If
            ElseIf Not skip Then 'detail
                sql = hdr & g_dbutil.SQL_DateTime(g_effdt) & ","   'timestamp
                If cols(0) = "" Then 'med
                    dupevdt = g_dbutil.SQL_DateTime(g_effdt)
                Else  'intervention
                    dupevdt = g_dbutil.SQL_DateTime(g_util.CDateEx(cols(0)))
                End If
                dupcode = g_dbutil.SQL_String(Replace(cols(1), "'", ""))
                dupres = "''" 'result
                sql = sql & dupevdt & ","  'event_datetime yyyymmddhhnn
                sql = sql & dupcode & ","  'code
                sql = sql & dupres & "," 'result
                sql = sql & g_dbutil.SQL_String(buf) & ")" 'source text
                g_cnADO.Execute sql
            End If
        End If
    Next iRow
    End If

'=PROBLEMS FILE=
'TCPROBLEMS201007191100
'"00004846"|"PITT,BRAD"|"4S"|"404"|"1"
'|"TOTALHIPP"||||
'"00005785"|"ACUITY,MIKE"|"5N"|"548"|"1"
'|"OUTPTBLOOD"||||
'|"HXDEV"||||
'"00005876"|"ACUITY,ORTHO"|"2ICU"|"203"|"1"
'|"HXWAS"||||
'"00005975"|"BABA,ALI"|"3W"|"326"|"1"
'|"CVA"||||
    If inputpfile <> 0 Then
    s = Input(LOF(inputpfile), inputpfile)
    rows() = Split(s, vbCrLf)
    maxrow = UBound(rows)
    skip = True
    For iRow = 0 To maxrow
        buf = rows(iRow)
        buf = Replace(buf, """", "")
        buf = Replace(buf, "'", "")
        cols() = Split(buf, "|")
        If UBound(cols) < 2 Then 'skip this row
        ElseIf UBound(cols) >= 2 Then
            If cols(2) <> "" Then 'header- relies on check cols(2) for non-null
                UnitID = GetUnitIDFromUnitName(cols(2))
                encid = GetEncounterIDFromAcct(cols(0))
                skip = False
                If UnitID = 0 Or encid = 0 Then
                    skip = True
                Else
                    hdr = "insert into chart_item (unit_id,encounter_id,tc_source_id,timestamp,event_datetime,code,result,source_text) values ("
                    hdr = hdr & UnitID & ","
                    hdr = hdr & encid & ","
                    hdr = hdr & "1,"
                End If
            ElseIf Not skip Then 'detail
                sql = hdr & g_dbutil.SQL_DateTime(g_effdt) & ","   'timestamp
                'no event time of problem item in data file.  assume it occurred 1 minute ago...
                dupevdt = g_dbutil.SQL_DateTime(g_effdt)
                dupcode = g_dbutil.SQL_String(cols(1))
                dupres = "''" 'result
                sql = sql & dupevdt & ","  'event_datetime yyyymmddhhnn
                sql = sql & dupcode & ","  'code
                sql = sql & dupres & "," 'result
                sql = sql & g_dbutil.SQL_String(buf) & ")" 'source text
                g_cnADO.Execute sql
            End If
        End If
    Next iRow
    End If
    
' OUTCOMES FILE
'26215665|GRIFFIN,LANDON|3N|348|1|201105150530|FALL
'26191510|ANDERSON,MARILYN W|4N|445|1|201105150100|PROC
'26214882|ELENDT,DESTEENA LOY|3S|301|1|201105151130|MED
'
'Or output could be:
'
'Report: ACUITY PLUS OUTCOMES DAYS was run on 05/15/11 at 7:04am
'No records found.

    If inputofile <> 0 Then
    s = Input(LOF(inputofile), inputofile)
    rows() = Split(s, vbCrLf)
    maxrow = UBound(rows)
    For iRow = 0 To maxrow
        buf = rows(iRow)
        buf = Replace(buf, """", "")
        buf = Replace(buf, "'", "")
        cols() = Split(buf, "|")
        If UBound(cols) < 2 Then 'skip this row
        ElseIf UBound(cols) >= 2 Then
            UnitID = GetUnitIDFromUnitName(cols(2))
            encid = GetEncounterIDFromAcct(cols(0))
            If UnitID = 0 Or encid = 0 Then
                skip = True
            Else
                hdr = "insert into chart_item (unit_id,encounter_id,tc_source_id,timestamp,event_datetime,code,result,source_text) values ("
                hdr = hdr & UnitID & ","
                hdr = hdr & encid & ","
                hdr = hdr & "1,"
                sql = hdr & g_dbutil.SQL_DateTime(g_effdt) & ","   'timestamp
                dupevdt = g_dbutil.SQL_DateTime(g_util.CDateEx(cols(5)))
                dupcode = g_dbutil.SQL_String(cols(6))
                dupres = g_dbutil.SQL_String(cols(5)) 'place event time in result so can use result return function
                sql = sql & dupevdt & ","  '
                sql = sql & dupcode & ","  'code fall med or proc
                sql = sql & dupres & "," 'event_dt
                sql = sql & g_dbutil.SQL_String(buf) & ")" 'source text
                g_cnADO.Execute sql
            End If
        End If
    Next iRow
    End If
    
    
    Exit Sub
    
QUERIESerrHandler:   'if dup is found, update the record because result may be different, and then move on
    If InStr(1, Err.Description, "duplicate", vbTextCompare) > 0 Then
        sql = "update chart_item set code=" & dupcode & ",result=" & dupres & ",source_text='" & buf & "' where "
        sql = sql & "unit_id=" & UnitID & " and encounter_id=" & encid & " and event_datetime=" & dupevdt & " and code=" & dupcode
        g_cnADO.Execute sql
        Resume Next
    Else
        LogError Err.Description & " in " & Err.source
        LogInfo "Unexpected error - shut down", EVENT_CATEGORY_STARTUP_SHUTDOWN
        Exit Sub
    End If
    
    
errHandler:   'if dup found, move on because only key elements are being used here.
    If InStr(1, Err.Description, "duplicate", vbTextCompare) > 0 Then
        Resume Next
    Else
        LogError Err.Description & " in " & Err.source
        LogInfo "Unexpected error - shut down", EVENT_CATEGORY_STARTUP_SHUTDOWN
    End If

End Sub
Private Function GetUnitIDFromUnitName(s As String) As Long
    Dim rs As New Recordset
    Dim sql As String
    
    GetUnitIDFromUnitName = 0
    sql = "select unit_id from unit_alias where unit_alias_name='" & s & "'"
    rs.Open sql, g_cnADO
    
    If Not rs.EOF Then
        If Not IsNull(rs(0)) Then
            GetUnitIDFromUnitName = rs(0)
        End If
    End If
    
    rs.Close
    Set rs = Nothing

End Function
Private Function GetEncounterIDFromAcct(s As String) As Long
    Dim rs As New Recordset
    Dim sql As String

    GetEncounterIDFromAcct = 0
    sql = "select encounter_id from encounter where acct_number='" & s & "'"
    rs.Open sql, g_cnADO
    
    If Not rs.EOF Then
        If Not IsNull(rs(0)) Then
            GetEncounterIDFromAcct = rs(0)
        End If
    End If
    
    rs.Close
    Set rs = Nothing

End Function

Private Sub DeleteOldLogs()
    'There are no more log files; history has been added to the event log
End Sub

Private Sub DeleteOldChartItems()
    On Error GoTo errHandler
    
    Dim sql As String
    Dim dt As Date

    dt = DateAdd("d", -CHART_ITEM_LIFE, Date)
    sql = "DELETE FROM CHART_ITEM WHERE EVENT_DATETIME < " & g_dbutil.SQL_DateTime(dt)
    'Debug.Print sql
    g_cnADO.Execute sql
    Exit Sub
    
errHandler:
    g_util.ThrowError "DeleteOldChartItems"
    Resume  'debug
End Sub




Public Sub AddLogEntry(event_type As EventLogType, msg As String, _
    Optional event_category As EventLogCategory = EVENT_CATEGORY_NONE _
)
    On Error GoTo errHandler

    dprint msg                      'copy to debug log

    g_event.AddEventLogEntry EVENT_SOURCE_TRANSPARENT_MAPPING, event_type, event_category, _
        msg, gLogSourceText, 0, gLogUnitID, gLogEncounterID
    Exit Sub
    
errHandler:
    'no message boxes allowed (this is a command-line program)
    Debug.Print Err.Description
End Sub

Public Sub LogInfo(s As String, Optional event_category As EventLogCategory = EVENT_CATEGORY_NONE)
    AddLogEntry EVENT_TYPE_INFO, s, event_category
End Sub

Public Sub LogWarning(s As String, Optional event_category As EventLogCategory = EVENT_CATEGORY_VALIDATION)
    AddLogEntry EVENT_TYPE_WARNING, s, event_category
End Sub

Public Sub LogError(s As String, Optional event_category As EventLogCategory = EVENT_CATEGORY_VALIDATION)
    AddLogEntry EVENT_TYPE_ERROR, s, event_category
End Sub

