﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;                     // for LINQ; add a reference
using System.Data.SqlClient;                // for SqlConnection
using System.IO;                            // for Directory, Path, Stream
using PfsShared;                            // add a reference to Shared2 project

// ============================================================================
// ED Visit transparent mapping -- GOES HERE --
// Mayo Arizona
// ============================================================================
// This processes one patient.  Only one database query is used.  :)
//
// All search functions use exact match for category, description and field name.
// Codes and results can be a single word or a list of words.
// Codes default to exact match; prefix with CODE_CODE_LIKE_PREFIX if desired (single word only)
// Results default to like matching; prefix with RESULT_EXACT_MATCH_PREFIX if desired (single word only)
//
// All searches are case insensitive.
//
//
namespace TransparentMapping
{
    class EDVisit
    {
        private const int MAX_INDS = 50;
        private const int MAX_PROCS = 20;

        private const string CODE_LIKE_PREFIX = "%!";       // use to make codes %like%
        private const string EXACT_MATCH_PREFIX = "&!";     // use to make desc/result exact match
        private const string CHAR_COMMA = "||";             // use to insert literal comma in word list
        private const string NOT_PREFIX = "!!";             // use to insert literal comma in word list

        private struct indicator_data {
            public bool     is_checked;     
            public int      radio_group;       
        }

        private struct proc_data {                      
            public int      procedure_number;
            public DateTime start;   
            public DateTime finish;
        }

        // These are database CHART_ITEMs for this patient
        private CHART_ITEM[] _chart_items_since_admission;
        private CHART_ITEM[] _chart_items_since_unit_arrival;
        private CHART_ITEM[] _chart_items_during_pull_period;
        private CHART_ITEM[] _chart_items_pull_period_plus;
        // These are database PROCEDURE_EVENTs for this patient
        private PROCEDURE_EVENT[] _procedure_events;

        private indicator_data[] _inds;
        private List<proc_data> _procs;
        private List<proc_data> _outcomes;

        private const int DEFAULT_BUCKET_SIZE = 20;       //min

        private PatientInfo _pat;
        private bool adl23;
        private bool adl4;
        private bool tubefeed;
        private bool coma;
        private int  _bucket_size = DEFAULT_BUCKET_SIZE;
        private bool is_default;
        private bool isEDonly = false;


        private enum SearchDepth {
            SearchDefault,
            SearchPullRange,            //search within the current pull     -- default
            SearchSinceArrival,         //search since arrival to the unit
            SearchSinceAdmission,        //search everything since admission to the hospital
            SearchPullPlus
        }

        private enum CountMode {
            CountAll,
            CountFirst                  //stop after one is found
        }

        private enum GetValueMode {
            GetTotal,
            GetMax,
            GetLast
        }

        enum Frequencies {
            QNONE,
            Q4H,
            Q2H,
            Q1H,
            Q15M
        }

        private struct fmapRow {
            public double   los_high;                       //the LOS being testing
            public int[]    freq;                           //the count required for each Q value
        }
        private List<fmapRow> _freq_map;                    //1,2,4,8,12,24 hours

     
        //
        // This is the main entry point
        //
        public void ProcessPatient(PatientInfo pat)
        {
            _pat = pat;

            InitIndicators(); // sets is_default
            InitProcs();
            isEDonly = OnlyHasED();
            if (! is_default)
                {
                LoadFreqTable();
                LoadPatientChart();
                Check_1();
                Check_234();
                Check_5();
                Check_67();
                Check_89();
                Check_1011();
                Check_1213();
                Check_14();
                Check_15();
                Check_16();
                Check_17();
                Check_181920();
            }

            AtLeastOneADL();
            HighestIndicatorInEachGroupWins();

            //if (!is_default)
            //{
            //    CheckProcs();
            //    CheckOutcomes();
            //}

            if (Program.g_no_output) return;
            OutputClass();
            //OutputProcs();
            //OutputOutcomes();
        }

       
        private void InitIndicators()
        {
            // Make a new (empty) indicator array
            _inds = new indicator_data[MAX_INDS + 1];           // This 1 based so add one
            is_default = false;
            //if (_pat.los_hours <= 4.0) {
            //    is_default = true;
            //    Program.VerboseAudit("Patient was here 4 hrs or less. Will receive default indicators " + _pat.default_inds_str);
            //    foreach (var ind in _pat.default_inds) {
            //        if (ind <= _inds.GetUpperBound(0)) {
            //            _inds[ind].is_checked = true;
            //        }
            //    }
            //}

            adl23 = false;
            adl4 = false;
            tubefeed = false;
            coma = false;

            // get indicator radio groups from the database
            // ** (This database access can be replaced once we have a C# methodolgy cache)
            var db = PFSUtility.NewPfsDataContext();
            var query = from idef in db.INDICATOR_DEFINITIONs
                        where (idef.METHODOLOGY_ID == _pat.meth_id)
                        select idef;
            foreach (var idef in query) {
                if (idef.INDICATOR_NUMBER <= _inds.GetUpperBound(0)) {
                    // (convert nulls to zero)
                    _inds[idef.INDICATOR_NUMBER].radio_group = PFSUtility.DBToInt(idef.RADIO_GROUP);
                }
            }
        }

        private void InitProcs()
        {
            _procs = new List<proc_data>();
            _outcomes = new List<proc_data>();
        }

        private fmapRow LoadFreqTableRow(double los_high, string values)
        {
            fmapRow fmrow;
            
            fmrow.los_high = los_high;
            fmrow.freq = new int[5];

            var arr = values.Split(',');
            for (int i = 0; i <= fmrow.freq.GetUpperBound(0) ; i++ )
            {
                fmrow.freq[i] = arr[i].ToInteger();
            }
            return fmrow;
        }

        // These are the number of times you need to do something during a certain time period (LOS)
        // in order for it to be considered one of the frequencies listed.
        private void LoadFreqTable()
        {
            _freq_map = new List<fmapRow>();
            //                              LOS,  None Q4h Q2h Q1h Q15m
            _freq_map.Add(LoadFreqTableRow(1, "    0,  0,  0,  1,  2"));
            _freq_map.Add(LoadFreqTableRow(2, "    0,  0,  1,  1,  4"));
            _freq_map.Add(LoadFreqTableRow(4, "    0,  1,  2,  2,  8"));
            _freq_map.Add(LoadFreqTableRow(6, "    0,  1,  2,  3, 12"));
            _freq_map.Add(LoadFreqTableRow(8, "    0,  1,  2,  4, 16"));
            _freq_map.Add(LoadFreqTableRow(12, "   0,  2,  4,  6, 24"));
            _freq_map.Add(LoadFreqTableRow(16, "   0,  3,  5,  8, 32"));
            _freq_map.Add(LoadFreqTableRow(24, "   0,  4,  8, 12, 48"));
            _freq_map.Add(LoadFreqTableRow(36, "   0,  4,  8, 18, 72"));
            _freq_map.Add(LoadFreqTableRow(48, "   0,  4,  8, 24, 96"));
            //New freq table 2/5/14
//q4	q2	q1	q30     q30
//            Non-ICU	ICU & SD
// 4	8	15	29	    36
// 3	5	9	17	    24
// 2	4	7	13	    19
// 2	3	5	10	    13

            //to read this table: if you have at least 6 asessmenets in a 12-hour period, you get Q1h
            //(LOS=12, column Q1h has a count of 6)
        }

        private Frequencies FreqForCount(double los_hours, int count) 
        {
            foreach(var fmrow in _freq_map) {
                if (los_hours <= fmrow.los_high) {
                    // Option: pro-rate the count upward if the LOS is less than the row we are interested in.
                    //         This will bump the count to what it might have been at the full LOS.
                    // Note: truncate the result; rounding inflates the value too much.
                    int prorated_count = (int)((fmrow.los_high / los_hours) * count);

                    // foreach goes low to high; go from high to low instead
                    for (int j = (int)Frequencies.Q15M; (j >= (int)Frequencies.QNONE); j--) { //search right to left
                        if (prorated_count >= fmrow.freq[j]) {
                            return (Frequencies)j;
                        }
                    } // next j
                }
            }
            
            return Frequencies.QNONE;
        }

        //
        // Load patient chart from the database (one DB access)
        // Load everything since admission
        //
        private void LoadPatientChart()
        {
            // Get the entire patient chart (all units and dates for this patient)
            var db = PFSUtility.NewPfsDataContext();
            var query = from item in db.CHART_ITEMs
                        where (item.ENCOUNTER_ID == _pat.encounter_id)
                        select item;
            // Save the result
            _chart_items_since_admission = query.ToArray();

            // Convert all results to lower case for case insensitive comparisons
            foreach (var item in _chart_items_since_admission) {
                if (item.CATEGORY != null) item.CATEGORY = item.CATEGORY.ToLower();
                if (item.CODE != null) item.CODE = item.CODE.ToLower();
                if (item.DESCRIPTION != null) item.DESCRIPTION = item.DESCRIPTION.ToLower();
                if (item.FIELD_NAME != null) item.FIELD_NAME = item.FIELD_NAME.ToLower();
                if (item.RESULT != null) item.RESULT = item.RESULT.ToLower();
            }

            // Prepare two more versions of the chart
            var query2 = from item in _chart_items_since_admission
                    where(item.EVENT_DATETIME >= _pat.unit_arrival) && (item.EVENT_DATETIME <= _pat.pull_finish)
                    select item;
            _chart_items_since_unit_arrival = query2.ToArray();

            query2 = from item in _chart_items_since_admission
                     where (item.EVENT_DATETIME >= _pat.pull_start) && (item.EVENT_DATETIME <= _pat.pull_finish)
                     select item;
            _chart_items_during_pull_period = query2.ToArray();
            query2 = from item in _chart_items_since_admission
                     where (item.EVENT_DATETIME >= _pat.pull_start) && (item.EVENT_DATETIME <= _pat.pull_finish.AddHours(4))
                     select item;
            _chart_items_pull_period_plus = query2.ToArray();
        }

        // Delay looking for patient procedures until they are wanted
        private void LoadPatientProceduresIfNeeded()
        {
            if (_procedure_events != null) return;

            var db = PFSUtility.NewPfsDataContext();
            var query = from proc in db.PROCEDURE_EVENTs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                        select proc;
            _procedure_events = query.ToArray();
        }



        // Started a new chart item query (of a certain depth)
        // Default is the chart during the pull range.  Options for unit arrival and admission to hospital.
        private IEnumerable<CHART_ITEM> StartNewQuery()
        {
            if (isEDonly)
                return StartNewQuery(SearchDepth.SearchSinceAdmission);
            else
                return StartNewQuery(SearchDepth.SearchDefault);
        }
        private IEnumerable<CHART_ITEM> StartNewQuery(SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;

            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    return (from item in _chart_items_during_pull_period select item);
                case SearchDepth.SearchSinceArrival:
                    return (from item in _chart_items_since_unit_arrival select item);
                case SearchDepth.SearchSinceAdmission:
                    return (from item in _chart_items_since_admission select item);
                case SearchDepth.SearchPullPlus:
                    return (from item in _chart_items_pull_period_plus select item);
            }
            return null;
        }

        // Is this search term a word or a list of words?
        private bool ValueIsAList(string s) 
        {
            return (s != null) && s.Contains(',');                //is this a comma-separated list?
        }

        // Break apart a wordlist and prepare its members
        private string[] SplitOnCommaAndPrepareElements(string s)
        {
            // If a word list has a space after each comma, we will get leading blanks in each term
            var arr = s.Split(',');
            for (int i = 0; i <= arr.GetUpperBound(0); i++) {
                arr[i] = arr[i].Trim();                         // get rid of leading blanks
                arr[i] = arr[i].Replace(CHAR_COMMA, ",");       // insert a literal comma into search word
                arr[i] = arr[i].ToLower();                      // convert for case-insensitive comparisons
            }
            return arr;
        }

        // Add a basic chart item filter to a LINQ query
        //
        // cat = exact match
        // code_list = exact match; optional "like" match
        // desc_list = "like" match; optional exact match
        // field = exact match
        // result_list = "like" match; optional exact match
        //
        // These are comma-separated lists.  
        // Search for a literal comma by using CHAR_COMMA in the string.
        private IEnumerable<CHART_ITEM> AndItemFilter(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list) 
        {
            // Converted all values to lower case for case-insensitive comparisons.
            if (!String.IsNullOrEmpty(cat)) {
                cat = cat.ToLower();
                query = query.Where(e => e.CATEGORY == cat);
            }

            if (!String.IsNullOrEmpty(code_list)) {
                code_list = code_list.ToLower();
                if (code_list.Left(2) == EXACT_MATCH_PREFIX) {          // exact match override?
                    query = query.Where(e => e.CODE == code_list.Substring(2));
                } else if (code_list.Left(2) == CODE_LIKE_PREFIX)
                {            // override for "like" match?
                    query = query.Where(e => e.CODE.Contains(code_list.Substring(2)));
                } else if (ValueIsAList(code_list)) {
                    query = AndCodeInList(query, code_list);            // find one of the words
                } else {
                    query = query.Where(e => e.CODE.Contains(code_list));      // find this word
                }
            }
            
            if (!String.IsNullOrEmpty(desc_list)) {
                desc_list = desc_list.ToLower();
                if (desc_list.Left(2) == EXACT_MATCH_PREFIX) {          // exact match override?
                    query = query.Where(e => e.DESCRIPTION == desc_list.Substring(2));
                } else if (ValueIsAList(desc_list)) {
                    query = AndDescriptionInList(query, desc_list);
                } else {
                    desc_list = desc_list.Replace(CHAR_COMMA, ",");     // look for literal comma requests
                    query = query.Where(e => e.DESCRIPTION.Contains(desc_list));
                }
            }
            
            if (!String.IsNullOrEmpty(field)) {
                field = field.ToLower();
                query = query.Where(e => e.FIELD_NAME == field);
            }
            
            if (!String.IsNullOrEmpty(result_list)) {
                result_list = result_list.ToLower();                        // make all results case insensitve
                if (result_list.Left(2) == EXACT_MATCH_PREFIX) {     // exact match override?
                    query = query.Where(e => e.RESULT == result_list.Substring(2));
                } else if (ValueIsAList(result_list)) {
                    query = AndResultInList(query, result_list);
                } else {
                    result_list = result_list.Replace(CHAR_COMMA, ",");     // look for literal comma requests
                    query = query.Where(e => e.RESULT.Contains(result_list));
                }
            }
            return query;
        }

        // Look for a code that is one of the words in the list.
        private IEnumerable<CHART_ITEM> AndCodeInList(IEnumerable<CHART_ITEM> query, string code_list) 
        {
            if (String.IsNullOrEmpty(code_list)) return query;
            
            var arr = SplitOnCommaAndPrepareElements(code_list);
            return query.Where(e => e.CODE.ContainsAny(arr));  // use like match. Exact match = arr.Contains(e.CODE));   
        }

        // Look for a description that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndDescriptionInList(IEnumerable<CHART_ITEM> query, string desc_list) 
        {
            if (String.IsNullOrEmpty(desc_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(desc_list);
            return query.Where(e => e.DESCRIPTION.ContainsAny(arr));    // "like" match
        }

        // Look for a result that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultInList(IEnumerable<CHART_ITEM> query, string result_list) 
        {
            if (String.IsNullOrEmpty(result_list)) return query;
            
            var arr = SplitOnCommaAndPrepareElements(result_list);
            return query.Where(e => e.RESULT.ContainsAny(arr));         // "like" match
        }

        // Look for a result that does not contain one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultNotInList(IEnumerable<CHART_ITEM> query, string result_list)
        {
            if (String.IsNullOrEmpty(result_list)) return query;
            
            var arr = SplitOnCommaAndPrepareElements(result_list);
            return query.Where(e => !e.RESULT.ContainsAny(arr));        // "like" match
        }

        private string DescribeSearchDepth(SearchDepth search_depth) 
        {
            string result = "";
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            
            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    //result = "in pull range"
                    result = "";                         //be quiet since this is default
                    break;
                case SearchDepth.SearchSinceArrival:
                    result = "since arrival to unit";
                    break;
                case SearchDepth.SearchSinceAdmission:
                    result = "since admission";
                    break;
                case SearchDepth.SearchPullPlus:
                    result = "in LOS range plus 4 hours after";
                    break;
            }
            
            return result;
        }

        // Describe a query that has already been run
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Make a simple query and describe the results
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            var arr = query.ToArray();
            return Describe(arr, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private string LookingFor(string so_far, string what, string op, string value)
        {
            string result;
            if (String.IsNullOrEmpty(value)) return so_far;

            if (so_far == "")
                result = "looking for " + what;
            else
                result = so_far + " and " + what;

            if (value.Left(2) == EXACT_MATCH_PREFIX)
                result += "='" + value.Substring(2) + "'";
            else if (value.Left(2) == CODE_LIKE_PREFIX)
                result += " contains " + "'" + value.Substring(2) + "'";
            else if (ValueIsAList(value))
                result += " in '" + value + "'";
            else
                result += op + "'" + value + "'";
            
            return result;
        }
        private string Describe(CHART_ITEM[] arr, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            string result = "";
            result = LookingFor(result, "cat", "=", cat);
            result = LookingFor(result, "code", "=", code_list);
            result = LookingFor(result, "desc", " contains ", desc_list);
            result = LookingFor(result, "field", "=", field);
            result = LookingFor(result, "result", " contains ", result_list);

            if (arr.Count() == 0) {
                result += "; not found " + DescribeSearchDepth(search_depth);
            } else {
                var e = arr[0];
                result = "FOUND: " + result + "; found";
                // We might have searched for a pattern or word list in several fields - show what was found
                if (e.CATEGORY != null)     result += " cat='" + e.CATEGORY + "'";
                if (e.CODE != null)         result += " code='" + e.CODE + "'";
                if (e.DESCRIPTION != null)  result += " desc='" + e.DESCRIPTION + "'";
                if (e.FIELD_NAME != null)   result += " field='" + e.FIELD_NAME + "'";
                if (e.RESULT != null)       result += " result='" + e.RESULT + "'";
                // Are there more results?  Just say how many; we aren't going to list them.
                if (arr.Count() == 2) {
                    result += " (1 more result)";
                } else if (arr.Count() > 2) {
                    result += " (" + (arr.Count() - 1) + " more results)";
                }
            }
            
            return result;
        }


        //Set an indicator for this reason (low level)
        private void SetInd(int inum, string reason)
        {
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Set Ind #" + inum + ": " + reason);          //already set - repeat for verbose only
            } else {
                _inds[inum].is_checked = true;
                Program.Audit("Set Ind #" + inum + ": " + reason);
            }
        }

        //Clear an indicator for this reason (low level)
        private void ClrInd(int inum, string reason)
        {
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Clr Ind #" + inum + ": " + reason);          //already clear - repeat verbose only
            } else {
                _inds[inum].is_checked = false;
                Program.Audit("Clr Ind #" + inum + ": " + reason);
            }
        }

        // Count how many items meet the conditions
        // All items are listed
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountItems(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            int count = query.Count();

            // always return what was found
            found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
            // echo the result?
            if (trace) Program.VerboseAudit(found_what);
            
            return count;
        }

        // Count how many items meet the conditions
        // This has the option to list every entry or give a total
        private int CountResultInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what="";

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);

            var arr = SplitOnCommaAndPrepareElements(result_list);

            foreach (var item in query) {
                // Figure out which of the search words were found
                var s = item.RESULT.ContainsWhich(arr);
                found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' code='" + item.CODE + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "'";
                if (trace) Program.VerboseAudit(found_what);
                count++;
                if (count_mode != CountMode.CountAll) break;        //stop counting
            }
            
            if (count > 0) {
                //We already printed what was found; maybe add how many?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }
            
            return count;
        }

        // Count how many items contain ALL results in list
        private int CountResultInListAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what = "";

            var arr = SplitOnCommaAndPrepareElements(result_list);

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            for (int i = 0; i <= arr.GetUpperBound(0); i++)
            {
                if (arr[i].StartsWith(NOT_PREFIX))
                {
                    query = query.Where(e => !e.RESULT.Contains(arr[i].Substring(2)));
                }
                else
                {
                    query = query.Where(e => e.RESULT.Contains(arr[i]));
                }
            }

            count = query.Count();

            if (count > 0)
            {
                found_what = "found item with all results in '" + result_list + "' without those prefixed by !!";
                if (trace) Program.VerboseAudit(found_what);
            }
            else
            {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            if (ValueIsAList(result_list))
            {
                return CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what);
            } else {
                return CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what);
            }
        }

        // Is there an item with this result?
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list) 
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth) 
        {
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContains(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what) 
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            if (ValueIsAList(result_list))
            {
                return (CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            } else {
                return (CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
            }
        }

        // Result contains all in list
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            if (ValueIsAList(result_list))
            {
                return (CountResultInListAll(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            }
            else
            {
                found_what = "Not valid for non-list results.";
                return false;
            }
        }
        // Count how many items meet the conditions, with none of the results in result_list.
        // This has the option to say how many were found rather than list all.
        private int CountResultNotInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what) 
        {
            int count = 0, rec_count = 0;
            bool found_one;

            found_what = "";
            var arr = SplitOnCommaAndPrepareElements(result_list);         // also deals with CHAR_COMMA
            
            //Do not filter by result here; we need all results
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");
            
            foreach(var item in query) {
                rec_count++;
                //Look for each search word in the result
                found_one = false;
                
                foreach (string s in arr) {
                    if (String.Equals(item.RESULT, s)) {
                        found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "' -- ignore this";
                        Program.VerboseAudit(found_what);
                        found_one = true;
                        break;
                    }
                }

                if (! found_one) {
                    //none of the words were found - good!
                    found_what = Describe(cat, code_list, desc_list, field, "", search_depth) + " -- does not contain '" + result_list + "'";
                    Program.VerboseAudit(found_what);
                    count++;
                    if (count_mode != CountMode.CountAll) break;
                }
            }
            
            if (count > 0) {
                //We already printed what was found; maybe show the total?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else if (rec_count > 0) {
                //We already printed what we ignored
            } else {
                //Print what did not exist
                found_what = Describe(cat, code_list, desc_list, field, "", search_depth);      //not found
                Program.VerboseAudit(found_what);
            }
            
            return count;
        }

        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, string found_what) 
        {
            // Search in the pull range with trace on
            return CountResultDoesNotContain(cat, code_list, desc_list, field, result_list, out found_what, SearchDepth.SearchDefault, true);
        }
        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, out string found_what, SearchDepth search_depth, bool trace)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            return CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0;
        }

        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            return (CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0);
        }

        // Set the indicator if the conditions are met
        //
        private bool SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);

        }
        private bool SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            bool b = false;
            //avoid more queries if the indicator is already set
            if (_inds[inum].is_checked) return true;

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what)) {
                SetInd(inum, found_what);                          //echo here - found
                b = true;
            } else {
                Program.VerboseAudit(found_what);                  //and here - not found
            }
            return b;
        }
        // Special contains all
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContainsAll(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already set
            if (_inds[inum].is_checked) return;

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                          //echo here - found
            }
            else
            {
                Program.VerboseAudit(found_what);                  //and here - not found
            }
        }
        // Set the indicator if the result does not contain any of the words in result_list
        //
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultDoesNotContain(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {   
            string found_what;
            //avoid more queries if the indicator is already set
            if (_inds[inum].is_checked) return;

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            //Turn trace off for ResultDoesNotContain() and echo what was set below with SetInd
            if (ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                           //echo here - not found
            } else {
                Program.VerboseAudit(found_what);                   //and here - found
            }
        }

        // Clear the indicator if the result contains one of the words in the result_list
        //
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already clear
            if (! _inds[inum].is_checked) return;

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            //Turn trace off for ResultContains() and echo what was set below with SetInd
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what)) {
                ClrInd(inum, found_what);                           //echo here - found
            } else {
                Program.VerboseAudit(found_what);                   //and here - not found
            }
        }


        //These slightly smaller functions are meant for places where you aren't really looking for a chart result
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            return (CountResultContains(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
        }

        // Variations with optional parameters (can switch to optional parameters in VS 2010)
        //
        private void SetIndIfFound(int inum, string cat)
        {
            SetIndIfResultContains(inum, cat, "", "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list)
        {
            SetIndIfResultContains(inum, cat, code_list, "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private void ClrIndIfFound(int inum, string cat)
        {
            ClrIndIfResultContains(inum, cat, "","","","", SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }


        // Get the max/total value from a result (usually in the middle of the text)
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetIntValue(get_mode, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            int value;
            int result = 0;
            bool found_one = false;

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            
            //Look for a number in the result
            
            foreach(var item in query) {
                // RESULT might be null so convert to empty string so Split won't blow up
                var arr = PFSUtility.DBToString(item.RESULT).Split(' ');
                foreach (string s in arr) {
                    //Look for 1st character numeric; IsNumeric("60min") will fail so look at 1st char only
                    if (s.Left(1).IsNumeric()) {
                        value = (int)s.Val();                           //Use Val; ToInteger will error on "60min"
                        switch (get_mode) {
                            case GetValueMode.GetMax:
                                result = Math.Max(value, result);       //max
                                break;
                            case GetValueMode.GetTotal:
                                result += value;                        //total
                                break;
                            case GetValueMode.GetLast:
                                result = value;                         //last
                                break;
                        }
                        
                        // print what we are searching for (the first time)
                        if (! found_one) {
                            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
                        }
                        found_one = true;
                        // print each value found
                        Program.VerboseAudit("  found numeric value " + result);
                        //Keep going in case there are more
                    }
                } 
            }
            
            if (! found_one) {
                //show what was not found
                Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
            }
            
            return result;
        }

        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetMaxValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            return GetIntValue(GetValueMode.GetMax, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetTotalValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            return GetIntValue(GetValueMode.GetTotal, cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Get a result; returns true if found with return_result
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result)
        {
            return GetResult(cat, code_list, desc_list, field, out return_result, SearchDepth.SearchDefault);
        }
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0) {
                return_result = PFSUtility.DBToString(query.First().RESULT);
            } else {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }
        private void GetResultforOther(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "Other (comment)");
            return_result="";
            foreach (var item in query)
            {
                if (!String.Equals(item.RESULT, "Other (comment)"))
                {
                    return_result = item.RESULT;
                    //Program.VerboseAudit(found_what);
                    break;
                }

            }
            return;
        }

        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, out string return_result, out DateTime return_evdt)
        {
            return GetResultAndEVDT(cat, code_list, desc_list, field, out return_result, out return_evdt, SearchDepth.SearchDefault);
        }
        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, out string return_result, out DateTime return_evdt, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0)
            {
                return_result = PFSUtility.DBToString(query.First().RESULT);
                return_evdt = PFSUtility.DBToDateTime(query.First().EVENT_DATETIME);
            }
            else
            {
                return_result = "";
                return_evdt = DateTime.MinValue;
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }


        //======================================================================================
        // Mapping starts here
        //======================================================================================

        private void Check_1()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Visit 1. Initial Assessment > 20 min");
            Program.VerboseAudit("---------------");

            //SetIndIfResultContains(1, "", "552634311", "", "", "Ambulance");
            //SetIndIfResultContains(1, "", "552634311", "", "", "Helicopter");
            //SetIndIfResultContains(1, "", "552634311", "", "", "Code Response Team");
            //SetIndIfResultContains(1, "", "552634311", "", "", "Police");
            //SetIndIfResultContains(1, "", "552634311", "", "", "Rescue");
            //SetIndIfResultContains(1, "", "1220195663", "", "", "Skilled Nursing Facility");
            //SetIndIfResultContains(1, "", "292718628", "", "", "Level 1");
            //SetIndIfResultContains(1, "", "292718628", "", "", "Level 2");
            //SetIndIfResultContains(1, "", "897429469", "", "", "Yes");
            //SetIndIfResultContains(1, "", "322738049", "", "", "Yes");
            //SetIndIfResultContains(1, "", "500037079", "", "", "No");

            SetIndIfResultContains(1, "", "", "Mode of Arrival", "", "Ambulance",SearchDepth.SearchPullPlus);
            SetIndIfResultContains(1, "", "", "Mode of Arrival", "", "Helicopter", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(1, "", "", "Mode of Arrival", "", "Code Response Team", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(1, "", "", "Mode of Arrival", "", "Police", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(1, "", "", "Mode of Arrival", "", "Rescue", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(1, "", "", "Patient Arrived from", "", "Skilled Nursing Facility", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(1, "", "", "Acuity", "", "Level 1", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(1, "", "", "Acuity", "", "Level 2", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(1, "", "", "Pt thoughts of harming themselves others", "", "Yes", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(1, "", "", "Possible abuse - ED question", "", "Yes", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(1, "", "", "Feel safe at home", "", "No", SearchDepth.SearchPullPlus);
        }


        private void Check_234()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Visit 2. ADL - Self Care");
            Program.VerboseAudit("ED Visit 3. ADL - Assist");
            Program.VerboseAudit("ED Visit 4. ADL - Extended");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(3, "", "17076211", "", "", "Ambulatory with Assistive Device", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(3, "", "17076211", "", "", "Cane", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(3, "", "17076211", "", "", "Crutches", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(3, "", "17076211", "", "", "Walker", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(3, "", "17076211", "", "", "Wheelchair", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(3, "", "17389", "", "", "Lethargic", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(3, "", "97036799", "", "", "Confused", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(3, "", "97036799", "", "", "Lethargic", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(3,"","17343783","","","");
            SetIndIfResultContains(3,"","209315142","","","Barrier Cream (Stool)");
            SetIndIfResultContains(3,"","209315142","","","Barrier Cream (Urine)");
            SetIndIfResultContains(3,"","209315142","","","Brief Changed");
            SetIndIfResultContains(3,"","209315142","","","Dri-Flo Changed");
            SetIndIfResultContains(3,"","209315142","","","Dry/not soiled");
            SetIndIfResultContains(3,"","209315142","","","Peri-care");
            SetIndIfResultContains(3,"","209315142","","","Peri-wash");
            SetIndIfResultContains(3,"","209315142","","","Stool");
            SetIndIfResultContains(3,"","209315142","","","Urine");
            SetIndIfResultContains(3,"","209315142","","","Other");
            SetIndIfResultContains(3,"","5613420","","","Bedpan");
            SetIndIfResultContains(3,"","5613420","","","Bedside Commode");
            SetIndIfResultContains(3,"","5613420","","","Foley");
            SetIndIfResultContains(3,"","5613420","","","Incontinent");
            SetIndIfResultContains(3,"","5613420","","","Rectal tube");
            SetIndIfResultContains(3,"","5613420","","","with assistance");
            SetIndIfResultContains(3,"","5613422","","","Ambulatory with Assistive Device");
            SetIndIfResultContains(3,"","5613422","","","Lift Assist Device");
            SetIndIfResultContains(3,"","5613422","","","Mobilize with 1 person assist");
            SetIndIfResultContains(3,"","5613422","","","Mobilize with 2-3 person assist");
            SetIndIfResultContains(3,"","5613422","","","Mobilize with >3 person assist");
            SetIndIfResultContains(3,"","5613423","","","Mobilize with 1 person assist");
            SetIndIfResultContains(3,"","5613423","","","Mobilize with 2-3 person assist");
            SetIndIfResultContains(3,"","5613423","","","Mobilize with >3 person assist");
            SetIndIfResultContains(3,"","5613416","","","Assist");
            SetIndIfResultContains(3,"","5613457","","","Assisted with Feeding");
            SetIndIfResultContains(3,"","5613457","","","Aspiration Precautions Observed");
            SetIndIfResultContains(3,"","335132705","","","Score of 5 or greater");
            SetIndIfResultContains(3,"","305006247","","","Yes");
//            "SetIndIfResultContains(3,"""",""1. Check for orders.IV_ind
//            2. Order_detail.OE_field_display_value = IV and also check order_detail OE_field_meaning =rxroute"","""","""",""Medication Documentation"");"
            SetIndIfResultContains(3,"","303693631","","","");

            SetIndIfResultContains(3, "", "", "Baseline Mobility Status", "", "Ambulatory with Assistive Device,Cane,Crutches,Walker,Wheelchair", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(3, "", "", "Level of consciousness", "", "Lethargic", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(3, "", "", "Neuro Behavior", "", "Confused,Lethargic", SearchDepth.SearchPullPlus);

            if (_pat.age < 4.0) SetInd(4,"Patient age is less than 4 = [ " + _pat.age.ToString() + " ]");
            SetIndIfResultContains(4, "", "17076211", "", "", "Bedbound", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(4, "", "17076211", "", "", "Total Assist", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(4, "", "17076211", "", "", "Unable to Assess", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(4, "", "292718628", "", "", "Level 1", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(4, "", "17389", "", "", "Stuporous", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(4, "", "17389", "", "", "Comatose", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(4,"","5613416","","","Complete/Total Care");
            SetIndIfResultContains(4,"","5613457","","","Fed");

            SetIndIfResultContains(4, "", "", "Baseline Mobility Status", "", "Bedbound,Total Assist,Unable to Assess", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(4, "", "", "Acuity", "", "Level 1", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(4, "", "", "Level of consciousness", "", "Stuporous,Comatose", SearchDepth.SearchPullPlus);


        }


        private void Check_5()
        {
            
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Visit 5. Communication Support");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(5, "", "92408462", "", "", "Yes", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(5, "", "92408462", "", "", "ATT line explained to patient", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(5, "", "92408462", "", "", "Patient/Family refused interpreter", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(5, "", "92408465", "", "", "Hearing Problem Uncompensated", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(5, "", "82329083", "", "", "Incomprehensible Speech", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(5, "", "82329083", "", "", "Unable to Speak", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(5, "", "82329083", "", "", "Unable to Assess", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(5, "", "92408463", "", "", "Vision Problem Uncompensated", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(5, "", "552433758", "", "", "Reading", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(5, "", "552433758", "", "", "Interpreter Needed", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(5, "", "481831381", "", "", "Mild to moderate aphasia = 1");
            SetIndIfResultContains(5, "", "481831381", "", "", "Severe aphasia = 2");
            SetIndIfResultContains(5, "", "481831381", "", "", "Mute = 3");
            SetIndIfResultContains(5, "", "481831406", "", "", "Mild to moderate dysarthria = 1");
            SetIndIfResultContains(5, "", "481831406", "", "", "Near unintelligible or worse = 2");
            SetIndIfResultContains(5, "", "7812343", "", "", "Difficulty speaking");
            SetIndIfResultContains(5, "", "7812343", "", "", "Expressive aphasia");
            SetIndIfResultContains(5, "", "7812343", "", "", "Inappropriate speech");
            SetIndIfResultContains(5, "", "7812343", "", "", "Incomprehensible sounds");
            SetIndIfResultContains(5, "", "7812343", "", "", "Receptive aphasia");
            SetIndIfResultContains(5, "", "7812343", "", "", "Artificial Airway");
            SetIndIfResultContains(5, "", "7812343", "", "", "Slurred");
            SetIndIfResultContains(5, "", "7812343", "", "", "Repetitive Speech");
            SetIndIfResultContains(5, "", "5613405", "", "", "Bivona Tube");
            SetIndIfResultContains(5, "", "5613405", "", "", "ET Tube");
            SetIndIfResultContains(5, "", "5613405", "", "", "Lary Tube");
            SetIndIfResultContains(5, "", "5613405", "", "", "Shiley Tube");
            SetIndIfResultContains(5, "", "17076154", "", "", "Difficulty Speaking");
            SetIndIfResultContains(5, "", "17076152", "", "", "Hearing Loss");
            SetIndIfResultContains(5, "", "17389", "", "", "Comatose", SearchDepth.SearchPullPlus);

            SetIndIfResultContains(5, "", "", "Interpreter Needed", "", "Yes", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(5, "", "", "Interpreter Needed", "", "ATT line explained to patient", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(5, "", "", "Interpreter Needed", "", "Patient/Family refused interpreter", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(5, "", "", "Hearing Problems", "", "Hearing Problem Uncompensated", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(5, "", "", "Speech Problems", "", "Incomprehensible Speech", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(5, "", "", "Speech Problems", "", "Unable to Speak", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(5, "", "", "Speech Problems", "", "Unable to Assess", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(5, "", "", "Vision Problems", "", "Vision Problem Uncompensated", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(5, "", "", "Barriers to Learning", "", "Reading", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(5, "", "", "Barriers to Learning", "", "Interpreter Needed", SearchDepth.SearchPullPlus);


        }


        private void Check_67()
        {
            string res;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Visit 6. Safety Management - q30min");
            Program.VerboseAudit("ED Visit 7. Safety Management - 1 to 1");
            Program.VerboseAudit("---------------");

            if (_inds[7].is_checked) return;

            SetIndIfResultContains(6, "", "97036799", "", "", "Agitated", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(6, "", "7812626", "", "", "Combative");
            SetIndIfResultContains(6, "", "7812626", "", "", "Hallucinations");
            SetIndIfResultContains(6, "", "7812626", "", "", "Hostile");
            SetIndIfResultContains(6, "", "7812626", "", "", "Violent");

            string codelist = "303419785,892363904,303419710,303419650,303419891,303419836,414141054";
            bool line_doc = (ResultContains("", codelist, "", "", ""));

            if (ResultContains("", "", "Confusion/Disorientation Hendrich", "", "Yes"))
            {
                if (GetResult("", "335132705", "", "", out res, SearchDepth.SearchPullPlus))
                {
                    if (res.Left(1).IsNumeric())
                    {
                        int value = (int)res.Val();
                        if (value >= 5) SetInd(6, "Confusion + Fall score(DTA)=" + res);
                    }
                }
                if (GetResult("", "Fall Risk Score", "", "", out res))
                {
                    if (res.Left(1).IsNumeric())
                    {
                        int value = (int)res.Val();
                        if (value >= 5) SetInd(6, "Confusion + Fall score=" + res);
                    }
                }

                if (line_doc) SetInd(6, "Confusion + Documented Line");
            }

            if (_pat.age < 6)
            {
                if (line_doc) SetInd(6, "Age <=5 + Documented Line");
                if (ResultContains("", "17343783", "", "", "")) SetInd(6, "Age <=5 + ECG HR");
            }

            SetIndIfResultContains(6, "", "322387963", "", "", "High Risk for Injury", SearchDepth.SearchPullPlus);

            SetIndIfResultContains(7, "", "187381794", "", "", "Yes");
            SetIndIfResultContains(7, "", "124578403", "", "", "");
            SetIndIfResultContains(7, "", "104631951", "", "", "");
            SetIndIfResultContains(7, "", "5613460", "", "", "Sitter");

            SetIndIfResultContains(6, "", "", "Neuro Behavior", "", "Agitated", SearchDepth.SearchPullPlus);
            //SetIndIfResultContains(6, "", "", "Confusion/Disorientation Hendrich", "", "Yes + Hendrich II Fall Risk Score > 5");
            //SetIndIfResultContains(6, "", "", "Confusion/Disorientation Hendrich", "", "Yes + Documentation of a line");
            SetIndIfResultContains(6, "", "", "Nursing Dx-High Risk for Injury", "", "High Risk for Injury", SearchDepth.SearchPullPlus);
            //SetIndIfResultContains(6, "", "", "", "", "Age 5 and under + Documentation of a line");
            //SetIndIfResultContains(6, "", "", "", "", "Age 5 and under + Documentation of an ECG HR");

            SetIndIfResultContains(7, "", "", "Pt thoughts of harming themselves others", "", "Yes");


        }

        private void Check_89()
        {
            string reslist;
            string other_str;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Visit 8. Behavior/Emotional Mgt");
            Program.VerboseAudit("ED Visit 9. Behavior/Emotional Mgt >= 30min");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(8, "", "", "Nursing Dx-Alteration in Thought Process", "", "Alteration in Thought Process", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(8, "", "", "ED Education Topics", "", "Bereavement/End of Life Care", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(8, "", "", "ED Education Topics", "", "Crisis Support", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(8, "", "", "ED Peds Behavioral Issues", "", "Yes", SearchDepth.SearchPullPlus);

            SetIndIfResultContains(9, "", "", "Pt thoughts of harming themselves others", "", "Yes");
            SetIndIfResultContains(9, "", "", "Contacted Social Worker or MHA", "", "Yes", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(9, "", "", "Feel safe at home", "", "No", SearchDepth.SearchPullPlus);
            //SetIndIfResultContains(9, "", "", "", "", "Placed during the ED visit");
            //SetIndIfResultContains(9, "", "", "", "", "Placed during the ED visit");
            SetIndIfResultContains(9, "", "", "Nursing Dx-Impaired Effective Coping", "", "Impaired/Ineffective Coping", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(9, "", "", "Nursing Dx-Decreased Cardiac Output", "", "Decreased Cardiac Output related to card", SearchDepth.SearchPullPlus);

            SetIndIfResultContains(8, "", "7812626", "", "", "Combative");
            SetIndIfResultContains(8, "", "7812626", "", "", "Agitated");
            SetIndIfResultContains(8, "", "7812626", "", "", "Hallucinations");
            SetIndIfResultContains(8, "", "7812626", "", "", "Hostile");
            SetIndIfResultContains(8, "", "7812626", "", "", "Violent");
            SetIndIfResultContains(8, "", "7812625", "", "", "Ineffective");
            SetIndIfResultContains(8, "", "1228232355", "", "", "Ineffective");
            SetIndIfResultContains(8, "", "1249837227", "", "", "");
            SetIndIfResultContains(8, "", "322387606", "", "", "Alteration in Thought Process", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(8, "", "805513177", "", "", "Bereavement/End of Life Care", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(8, "", "805513177", "", "", "Crisis Support", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(8, "", "800940576", "", "", "Yes", SearchDepth.SearchPullPlus);

            SetIndIfResultContains(9, "", "187381794", "", "", "Yes");
            SetIndIfResultContains(9, "", "316346213", "", "", "Violent/Self Destructive Behavior");
            SetIndIfResultContains(9, "", "322738364", "", "", "Yes", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(9, "", "500036603", "", "", "No", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(9, "", "80747987", "", "", "Placed during the ED visit");
            SetIndIfResultContains(9, "", "346727232", "", "", "Placed during the ED visit");
            SetIndIfResultContains(9, "", "322387606", "", "", "Impaired/Ineffective Coping related to fear and uncertainty about disease process", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(9, "", "322387606", "", "", "Decreased Cardiac Output related to cardiac/respiratory arrest", SearchDepth.SearchPullPlus);
        }

        private void Check_1011()
        {
            string reslist;
            List<int> buckets;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Visit 10. Fluid Management");
            Program.VerboseAudit("ED Visit 11. Fluid Management - q1hr");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(10, "", "97037219", "", "", "");
            SetIndIfResultContains(10, "", "97037271", "", "", "");
            SetIndIfResultContains(10, "", "393464372", "", "", "Scanned Amount of Urine");

            //SetIndIfResultContains(11, "", "n/a", "", "", "Lasix medication order and or documentation of lasix");
            //SetIndIfResultContains(11, "", "Order", "", "", "");
            //SetIndIfResultContains(11, "", "Medication", "", "", "");
            SetIndIfResultContains(11, "", "481751481", "", "", "");
            SetIndIfResultContains(11, "", "481752177", "", "", "");
            SetIndIfResultContains(11, "", "304307996", "", "", "");

            SetIndIfResultContains(10, "", EXACT_MATCH_PREFIX + "MED", ";ROUTE=IV", "", "");

            SetBucketSize(60);
            buckets = new List<int>();
            AddBuckets(buckets, "", EXACT_MATCH_PREFIX + "MED", ";ROUTE=IV", "", "");
            AddBuckets(buckets, "", "", ";IO=OUTPUT", "", "");
            int ct = CountBuckets(buckets);
            if (IsQ1Hour(ct)) SetInd(11, "Intake+Output count=" + ct);

        }

        private void Check_1213()
        {
            string codelist, found_what;
            int ct;
            List<int> buckets;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Visit 12. Physiologic Assessment q1hr");
            Program.VerboseAudit("ED Visit 13. Physiologic Assessment q15min");
            Program.VerboseAudit("---------------");

            SetBucketSize(15);
            buckets = new List<int>();
            codelist = "118054,117590,11789,117531,1734783,117635,305994900,117638,";
            codelist += "141615,142506593,12499648,117498,117496,117584,305994913,";
            codelist += "305994918,3059991317,292718281,305995220,7812336,18802339,";
            codelist += "3055994957,7812341,7812343,7812342,9278691,9278652,481831474,";
            codelist += "7812371,7812372,7812373,7812374,7812375,7812376,7812377,";
            codelist += "7812464,7812465,7812466,7812467,7812468,7812469,7812470,";
            codelist += "7812365,18802341,800940383,7812493,18802342,7812494,7812495,";
            codelist += "302915656,758359245,325754577,302915761,302915771,592033203,";
            codelist += "590233208,592033213,345372930,302916056,303447877,302916076,";
            codelist += "758629716,758608948,758642066,758669207";
            AddBuckets(buckets, "", codelist, "", "");
            codelist = "Non-Invasive SBP,Non-Invasive DBP,Heart Rate,Respiratory Rate,SPO2";
            AddBuckets(buckets, "", "", codelist, "");
            ct = CountBuckets(buckets);
            if (IsQ15(ct))
                SetInd(13, "All Physiologic Assessments count=" + ct);

            if (_inds[13].is_checked) return;

            SetIndIfResultContains(12, "", "118054", "", "", "");
            SetIndIfResultContains(12, "", "117590", "", "", "");
            SetIndIfResultContains(12, "", "11789", "", "", "");
            SetIndIfResultContains(12, "", "117531", "", "", "");
            SetIndIfResultContains(12, "", "1734783", "", "", "");
            SetIndIfResultContains(12, "", "117635", "", "", "");
            SetIndIfResultContains(12, "", "305994900", "", "", "");
            SetIndIfResultContains(12, "", "117638", "", "", "");
            SetIndIfResultContains(12, "", "141615", "", "", "");
            SetIndIfResultContains(12, "", "142506593", "", "", "");
            SetIndIfResultContains(12, "", "12499648", "", "", "");
            SetIndIfResultContains(12, "", "117498", "", "", "");
            SetIndIfResultContains(12, "", "117496", "", "", "");
            SetIndIfResultContains(12, "", "117584", "", "", "");
            SetIndIfResultContains(12, "", "305994913", "", "", "");
            SetIndIfResultContains(12, "", "305994918", "", "", "");
            SetIndIfResultContains(12, "", "3059991317", "", "", "");
            SetIndIfResultContains(12, "", "292718281", "", "", "");
            SetIndIfResultContains(12, "", "305995220", "", "", "");
            SetIndIfResultContains(12, "", "7812336", "", "", "");
            SetIndIfResultContains(12, "", "18802339", "", "", "");
            SetIndIfResultContains(12, "", "3055994957", "", "", "");
            SetIndIfResultContains(12, "", "7812341", "", "", "");
            SetIndIfResultContains(12, "", "7812343", "", "", "");
            SetIndIfResultContains(12, "", "7812342", "", "", "");
            SetIndIfResultContains(12, "", "9278691", "", "", "");
            SetIndIfResultContains(12, "", "9278652", "", "", "");
            SetIndIfResultContains(12, "", "481831474", "", "", "");
            SetIndIfResultContains(12, "", "7812371", "", "", "");
            SetIndIfResultContains(12, "", "7812372", "", "", "");
            SetIndIfResultContains(12, "", "7812373", "", "", "");
            SetIndIfResultContains(12, "", "7812374", "", "", "");
            SetIndIfResultContains(12, "", "7812375", "", "", "");
            SetIndIfResultContains(12, "", "7812376", "", "", "");
            SetIndIfResultContains(12, "", "7812377", "", "", "");
            SetIndIfResultContains(12, "", "7812464", "", "", "");
            SetIndIfResultContains(12, "", "7812465", "", "", "");
            SetIndIfResultContains(12, "", "7812466", "", "", "");
            SetIndIfResultContains(12, "", "7812467", "", "", "");
            SetIndIfResultContains(12, "", "7812468", "", "", "");
            SetIndIfResultContains(12, "", "7812469", "", "", "");
            SetIndIfResultContains(12, "", "7812470", "", "", "");
            SetIndIfResultContains(12, "", "7812365", "", "", "");
            SetIndIfResultContains(12, "", "18802341", "", "", "");
            SetIndIfResultContains(12, "", "800940383", "", "", "");
            SetIndIfResultContains(12, "", "7812493", "", "", "");
            SetIndIfResultContains(12, "", "18802342", "", "", "");
            SetIndIfResultContains(12, "", "7812494", "", "", "");
            SetIndIfResultContains(12, "", "7812495", "", "", "");
            SetIndIfResultContains(12, "", "302915656", "", "", "");
            SetIndIfResultContains(12, "", "758359245", "", "", "");
            SetIndIfResultContains(12, "", "325754577", "", "", "");
            SetIndIfResultContains(12, "", "302915761", "", "", "");
            SetIndIfResultContains(12, "", "302915771", "", "", "");
            SetIndIfResultContains(12, "", "592033203", "", "", "");
            SetIndIfResultContains(12, "", "590233208", "", "", "");
            SetIndIfResultContains(12, "", "592033213", "", "", "");
            SetIndIfResultContains(12, "", "345372930", "", "", "");
            SetIndIfResultContains(12, "", "302916056", "", "", "");
            SetIndIfResultContains(12, "", "303447877", "", "", "");
            SetIndIfResultContains(12, "", "302916076", "", "", "");
            SetIndIfResultContains(12, "", "758629716", "", "", "");
            SetIndIfResultContains(12, "", "758608948", "", "", "");
            SetIndIfResultContains(12, "", "758642066", "", "", "");
            SetIndIfResultContains(12, "", "758669207", "", "", "");

            SetIndIfResultContains(12, "", "", "Non-Invasive SBP", "", "", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(12, "", "", "Non-Invasive DBP", "", "", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(12, "", "", "Heart Rate", "", "", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(12, "", "", "Respiratory Rate", "", "", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(12, "", "", "SPO2", "", "", SearchDepth.SearchPullPlus);

        }

        // (this isn't really a Q1h count -- it is just a count) Make this a q1hr count
        private int ReturnQ1HrCount(string code_list, string result_list)
        {
            var buckets = new List<int>();
            SetBucketSize(60);
            AddBuckets(buckets, "", code_list, "", "", result_list);
            return CountBuckets(buckets);
        }

        private bool IsQ1Hour(int count)
        {
            return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q1H);
        }
        private bool IsQ15(int count)
        {
            return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q15M);
        }

        // Use this if counting safety buckets
        //private void CheckSafety(int count, string desc)
        //{
        //    if (_inds[13].is_checked) return;             //skip if highest already checked
        //    if (count == 0) return;
        //    switch (FreqForCount(_pat.los_hours, count))
        //    {
        //    case Frequencies.Q30M:
        //        SetInd(13, desc + " q30min");
        //        break;
        //    case Frequencies.Q1H:
        //    case Frequencies.Q2H:
        //        SetInd(12, desc + " q2h");
        //        break;
        //    default:
        //        Program.VerboseAudit(desc + ": " + count + " in " + Math.Round(_pat.los_hours) + " hours is not enough");
        //        break;
        //    }
        //}

        private void Check_14()
        {    

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Visit 14. Medication Management");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(14, "", EXACT_MATCH_PREFIX+"MED", "", "", "");
        }

        //private void CheckAssessment(int count, string desc)
        //{
        //    if (_inds[18].is_checked) return;          //skip if highest already checked
        //    if (count == 0) return;                    //skip if none
            
        //    // This should work the same as the original code:
        //    switch (FreqForCount(_pat.los_hours, count)) {
        //        case Frequencies.Q30M:
        //            SetInd(18, desc);
        //            break;
        //        case Frequencies.Q1H:
        //            SetInd(17, desc);
        //            break;
        //        case Frequencies.Q2H:
        //            SetInd(16, desc);
        //            break;
        //        case Frequencies.Q4H:
        //            SetInd(15, desc);
        //            break;
        //        default:
        //            break;
        //    }

        //}

        private void Check_15()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Visit 15. Wound/Injury Management");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(15,"","303692843","","","");
            SetIndIfResultContains(15,"","303692914","","","");
            SetIndIfResultContains(15,"","303693055","","","");
            SetIndIfResultContains(15,"","303419740","","","Dressing Change");
            SetIndIfResultContains(15,"","303419675","","","Dressing Change");
            SetIndIfResultContains(15,"","303419805","","","Dressing Change");
            SetIndIfResultContains(15,"","303419861","","","Dressing Change");
            SetIndIfResultContains(15,"","303419916","","","Dressing Change");
            SetIndIfResultContains(15,"","305056499","","","");
            SetIndIfResultContains(15,"","17076217","","","");
            SetIndIfResultContains(15,"","305142088","","","APD - All Purpose Drain");
            SetIndIfResultContains(15,"","305142088","","","Biliary");
            SetIndIfResultContains(15,"","305142088","","","Blake");
            SetIndIfResultContains(15,"","305142088","","","CAPD");
            SetIndIfResultContains(15,"","305142088","","","Drain, Constavac");
            SetIndIfResultContains(15,"","305142088","","","Davol");
            SetIndIfResultContains(15,"","305142088","","","G-J Tube");
            SetIndIfResultContains(15,"","305142088","","","G-Tube");
            SetIndIfResultContains(15,"","305142088","","","Hemovac");
            SetIndIfResultContains(15,"","305142088","","","JP");
            SetIndIfResultContains(15,"","305142088","","","J-Tube");
            SetIndIfResultContains(15,"","305142088","","","PEG tube");
            SetIndIfResultContains(15,"","305142088","","","Pelvic Drain");
            SetIndIfResultContains(15,"","305142088","","","Pendrose Drain");
            SetIndIfResultContains(15,"","305142088","","","Pericardial");
            SetIndIfResultContains(15,"","305142088","","","T-Tube");
            SetIndIfResultContains(15,"","305142088","","","Other");
            SetIndIfResultContains(15,"","305056424","","","");
            SetIndIfResultContains(15,"","645764172","","","1 = Erythema");
            SetIndIfResultContains(15,"","645764172","","","2 = Pain");
            SetIndIfResultContains(15,"","645764172","","","3 = Pain");
            SetIndIfResultContains(15,"","645764172","","","4 = Pain");
            SetIndIfResultContains(15,"","645764177","","","1 = Skin blanched");
            SetIndIfResultContains(15,"","645764177","","","2 = Skin blanched");
            SetIndIfResultContains(15,"","645764177","","","3 = Skin blanched");
            SetIndIfResultContains(15,"","645764177","","","4 = Skin tight");
            SetIndIfResultContains(15,"","320562063","","","Ileal conduit");
            SetIndIfResultContains(15,"","320562063","","","nephrostomy tube");
            SetIndIfResultContains(15,"","320562063","","","suprapubic catheter");
            SetIndIfResultContains(15,"","30505631","","","");
            SetIndIfResultContains(15,"","316346132","","","");
            SetIndIfResultContains(15,"","305056439","","","");
            SetIndIfResultContains(15,"","302916311","","","");
            SetIndIfResultContains(15,"","17076150","","","Analgesic drops");
            SetIndIfResultContains(15,"","17076150","","","Fluoresceine Stain");
            SetIndIfResultContains(15,"","17076150","","","Antibiotic drops");
            SetIndIfResultContains(15,"","17076150","","","Glycerine");
            SetIndIfResultContains(15,"","17076150","","","Ice Applied");
            SetIndIfResultContains(15,"","17076150","","","Irrigation With");
            SetIndIfResultContains(15,"","17076150","","","Lidocaine");
            SetIndIfResultContains(15,"","17076150","","","Normal Saline");
            SetIndIfResultContains(15,"","17076150","","","Patched");
            SetIndIfResultContains(15,"","17076150","","","Pressure Applied");
            SetIndIfResultContains(15,"","14813153","","","Yes");
            SetIndIfResultContains(15,"","14813153","","","Controlled");
            SetIndIfResultContains(15,"","14813154","","","");
            SetIndIfResultContains(15,"","305995275","","","");
            SetIndIfResultContains(15,"","7812543","","","Bruising");
            SetIndIfResultContains(15,"","7812543","","","Lesion");
            SetIndIfResultContains(15,"","7812543","","","Peeling");
            SetIndIfResultContains(15,"","7812543","","","Rash");
            SetIndIfResultContains(15,"","7812543","","","Redness");
            SetIndIfResultContains(15,"","305995245","","","Bleeding Gums");
            SetIndIfResultContains(15,"","305995245","","","Lip Ulcers");
            SetIndIfResultContains(15,"","305995245","","","Oral Abcess(es)");
            SetIndIfResultContains(15,"","305995245","","","Oral Rashes");
            SetIndIfResultContains(15,"","305995245","","","Oral Ulcers");
            SetIndIfResultContains(15,"","305995250","","","Ulcers");
            SetIndIfResultContains(15,"","305995250","","","White Coating");
            SetIndIfResultContains(15,"","7812620","","","2 = Mod pain");
            SetIndIfResultContains(15,"","7812620","","","3 = Severe pain");
            SetIndIfResultContains(15,"","7812620","","","4 = Life-threatening");
            SetIndIfResultContains(15,"","7812537","","","");
            SetIndIfResultContains(15,"","5613405","","","");
            SetIndIfResultContains(15,"","5613336","","","Transcutaneous");
            SetIndIfResultContains(15,"","5613336","","","Transvenous");
            SetIndIfResultContains(15,"","303693441","","","");
            SetIndIfResultContains(15,"","303693536","","","Erythema");
            SetIndIfResultContains(15,"","303693536","","","Peeling");
            SetIndIfResultContains(15,"","303693536","","","Lesion");
            SetIndIfResultContains(15,"","5613437","","","");

            SetIndIfResultContains(15,"","304015194","","","");
            SetIndIfResultContains(15,"","303692985","","","");
            SetIndIfResultContains(15,"","303693120","","","");

    }


        private void Check_16()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Visit 16. Urgent Intervention > 1 staff");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(16, "", "", "Acuity", "", "Level 1", SearchDepth.SearchPullPlus);
            //SetIndIfResultContains(16, "", "", "", "", "");
            //SetIndIfResultContains(16, "", "", "", "", "");
            //SetIndIfResultContains(16, "", "", "", "", "");
            SetIndIfResultContains(16, "", "", "Nursing Dx-Decreased Cardiac Output", "", "Decreased Cardiac Output related to card", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(16, "", "", "Patient disposition", "", "Expired", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(16, "", "", "Patient disposition", "", "Admit to Cath Lab or ICU", SearchDepth.SearchPullPlus);
            //SetIndIfResultContains(16, "", "", "Kathleen To determine", "", "Any Value (need to determine if this will come from Iview)");



            SetIndIfResultContains(16, "", "292718628", "", "", "Level 1", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(16, "", "285079089", "", "", "");
            SetIndIfResultContains(16, "", "305973488", "", "", "");
            SetIndIfResultContains(16, "", "1121279935", "", "", "");
            SetIndIfResultContains(16, "", "322387606", "", "", "Decreased Cardiac Output related to cardiac/respiratory arrest", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(16, "", "498518319", "", "", "Expired", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(16, "", "498518320", "", "", "Admit to Cath Lab or ICU", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(16, "", "117586", "", "", "");
            SetIndIfResultContains(16, "", "5613402", "", "", "Bi-Level Positive Airway Pressure");
            SetIndIfResultContains(16, "", "5613402", "", "", "Bag-Valve");
            SetIndIfResultContains(16, "", "5613402", "", "", "Continuous Positive Airway Pressure");
            SetIndIfResultContains(16, "", "5613402", "", "", "Mechanical Ventilator");
            SetIndIfResultContains(16, "", "180571496", "", "", "", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(16, "", "5613336", "", "", "Transcutaneous");
            SetIndIfResultContains(16, "", "5613336", "", "", "Transvenous");
            SetIndIfResultContains(16, "", "481831474", "", "", "", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(16, "", "1120348623", "", "", "", SearchDepth.SearchPullPlus);

        }

        private void Check_17()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Visit 17. Educational Needs");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(17, "", "", "ED Education Topics", "", "Anticoagulation Education", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(17, "", "", "ED Education Topics", "", "Crutch Training", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(17, "", "", "ED Education Topics", "", "Equipment Application", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(17, "", "", "ED Education Topics", "", "Splint care", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(17, "", "", "ED Education Topics", "", "Stroke", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(17, "", "", "ED Education Topics", "", "Urinary Catheter", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(17, "", "", "ED Education Topics", "", "Walker Training", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(17, "", "", "ED Education Topics", "", "Diabetes", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(17, "", "", "ED Education Topics", "", "Wound Care", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(17, "", "", "ED Education Topics", "", "Bereavement/End of Life Care", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(17, "", "", "ED Education Topics", "", "Crisis Support", SearchDepth.SearchPullPlus);

            SetIndIfResultContains(17, "", "800940576", "", "", "Anticoagulation Education", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(17, "", "800940576", "", "", "Crutch Training", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(17, "", "800940576", "", "", "Equipment Application", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(17, "", "800940576", "", "", "Splint care", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(17, "", "800940576", "", "", "Stroke", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(17, "", "800940576", "", "", "Urinary Catheter", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(17, "", "800940576", "", "", "Walker Training", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(17, "", "800940576", "", "", "Diabetes", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(17, "", "800940576", "", "", "Wound Care", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(17, "", "800940576", "", "", "Bereavement/End of Life Care", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(17, "", "800940576", "", "", "Crisis Support", SearchDepth.SearchPullPlus);

        }

        private void Check_181920()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Visit 18. Admitted");
            Program.VerboseAudit("ED Visit 19. Transferred to external Facility");
            Program.VerboseAudit("ED Visit 20. Expired");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(18, "", "", "Patient disposition", "", "Admit", SearchDepth.SearchPullPlus);
            //SetIndIfResultContains(18, "", "", "Patient disposition", "", "Admit to OR");
            //SetIndIfResultContains(18, "", "", "Patient disposition", "", "Admit to Cath Lab or ICU");
            SetIndIfResultContains(19, "", "", "Patient disposition", "", "Assisted Living/Cust Care-No Skilled Services", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(19, "", "", "Patient disposition", "", "Cancer Center or Children", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(19, "", "", "Patient disposition", "", "Critical Access Hospital", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(19, "", "", "Patient disposition", "", "Federal Hospital", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(19, "", "", "Patient disposition", "", "Hospice", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(19, "", "", "Patient disposition", "", "Long Term Specialty Care Hospital", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(19, "", "", "Patient disposition", "", "Transfer to Hosp", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(19, "", "", "Patient disposition", "", "Acute Rehab Unit", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(20, "", "", "Patient disposition", "", "Expired", SearchDepth.SearchPullPlus);


            SetIndIfResultContains(18, "", "498518319", "", "", "Admit", SearchDepth.SearchPullPlus);
            //SetIndIfResultContains(18, "", "498518319", "", "", "Admit to OR");
            //SetIndIfResultContains(18, "", "498518319", "", "", "Admit to Cath Lab or ICU");
            SetIndIfResultContains(18, "", "537695013", "", "", "", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(18, "", "315952539", "", "", "", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(18, "", "420011761", "", "", "", SearchDepth.SearchPullPlus);

            SetIndIfResultContains(19, "", "498518319", "", "", "Assisted Living/Cust Care-No Skilled Services", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(19, "", "498518319", "", "", "Cancer Center or Children", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(19, "", "498518319", "", "", "Critical Access Hospital", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(19, "", "498518319", "", "", "Federal Hospital", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(19, "", "498518319", "", "", "Hospice", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(19, "", "498518319", "", "", "Long Term Specialty Care Hospital", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(19, "", "498518319", "", "", "Transfer to Hosp", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(19, "", "498518319", "", "", "Acute Rehab Unit", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(19, "", "322178549", "", "", "", SearchDepth.SearchPullPlus);

            SetIndIfResultContains(20, "", "498518319", "", "", "Expired", SearchDepth.SearchPullPlus);
            SetIndIfResultContains(20, "", "531429162", "", "", "", SearchDepth.SearchPullPlus);


        }

        //private bool IsICU()
        //{
        //    switch (_pat.unit_name)
        //    {
        //        case "ICU":
        //        case "3BR":
        //        case "CTIC":
        //        case "NIC":
        //        case "PCU":
        //        case "TICU":
        //        case "CCB":
        //        case "RICU":
        //        case "CCUS":
        //            return true;
        //        default:
        //            return false;
        //    }
        //}

        //private bool IsTele()
        //{
        //    switch (_pat.unit_name)
        //    {
        //        case "4EST":
        //        case "2NW":
        //        case "3AE":
        //        case "5MEH":
        //        case "5NW":
        //        case "6MEH":
        //        case "6NW":
        //        case "7MEH":
        //        case "B4W":
        //        case "B5S":
        //        case "B5W":
        //        case "2NOB":
        //        case "CPLX":
        //        case "TELMS":
        //        case "MAN4":
        //            return true;
        //        default:
        //            return false;
        //    }
        //}


        //private void CountAssessments(int bucket_size)
        //{
        //    int ct;
        //    string codelist;
        //    string reslist;
        //    List<int> buckets;

        //    SetBucketSize(bucket_size);

        //    buckets = new List<int>();

        //    codelist = "97036712,118064,118066,118058,118132,305991507,118520,14305,142506594";
        //    codelist += ",12293217,118193,118068,118011,118146,306182838,306182805,305991322";
        //    codelist += ",118498,29718286,306327425,131172918,353080778";
        //    AddBuckets(buckets, "", codelist, "", "");
        //    reslist = "APD - All Purpose Drain (AZ),Biliary,Blake,CAPD,Drain,Constavac,Davol,G-J Tube,G-Tube,Hemovac";
        //    reslist += ",JP,J-Tube,PEG tube,Pelvic Drain,Pendrose Drain,Pericardial,T-Tube,Other";
        //    AddBuckets(buckets, "", "305142088", "", "", reslist);
        //    codelist = "305433923,633686082,305433661,481751506,481752199,304541034,316381370,305434227";
        //    codelist += ",316380803,119127,119141,304541048,7847308,18674661,306327593,7849724";
        //    codelist += ",7849861,7849891,7851959,18674834,7852147,12293217,30600122,7852257";
        //    codelist += ",306001049,7853949,7931900,7852980,7852983,7853157,7853190,7931902";
        //    codelist += ",7853207,7903597,7853235,7903643,7853418,7853426,7853430,7866522";
        //    codelist += ",18674837,7866633,14197072,7932422";
        //    AddBuckets(buckets, "", codelist, "", "");
        //    AddBuckets(buckets, "", "7868669", "", "", "Endotracheal,Nasal,Nasal Tracheal Suction,Oral Suction,Tracheal Suction");
        //    codelist = "7870437,7904435,18674857,7932312,18674862,7907957,18674869";
        //    codelist += ",303031350,325754541,303031329,532319071,345591779,304015183";
        //    codelist += ",345603431,303722796,303722810,303722826,303722841,303722857";
        //    codelist += ",303722871,303722885,303722900,790715,305433789,383694315";
        //    codelist += ",383695492,303722345,303693371,303722782";
        //    AddBuckets(buckets, "", codelist, "", "");
            
        //    ct = CountBuckets(buckets);
        //    CheckAssessment(ct, "All assessments count=" + ct);
        //    if (_inds[18].is_checked) return;


        //    //buckets = new List<int>();
        //    //codelist = "MH_ORD_Respiratory";
        //    //AddBuckets(buckets, "", codelist, "", "");
        //    //ct = CountBuckets(buckets);
        //    //CheckAssessment(ct, "Pulmonary=" + ct);
        //    //if (_inds[18].is_checked) return;

        //    //buckets = new List<int>();
        //    //codelist = "MH_ORD_VitalSigns";
        //    //AddBuckets(buckets, "", codelist, "", "");
        //    //ct = CountBuckets(buckets);
        //    //CheckAssessment(ct, "VS=" + ct);
        //    //if (_inds[18].is_checked) return;

        //    //buckets = new List<int>();
        //    //codelist = "CARDRHYTHM";
        //    //AddBuckets(buckets, "", codelist, "", "");
        //    //ct = CountBuckets(buckets);
        //    //CheckAssessment(ct, "CardRhythm=" + ct);
        //    //if (_inds[18].is_checked) return;

        //    //buckets = new List<int>();
        //    //codelist = "A_GlasgowComaScl,SC_S_ASMT_FregNeuroChks,A_MHPNeuroTube";
        //    //AddBuckets(buckets, "", codelist, "", "");
        //    //ct = CountBuckets(buckets);
        //    //CheckAssessment(ct, "Neuro=" + ct);
        //    //if (_inds[18].is_checked) return;

        //    //buckets = new List<int>();
        //    //codelist = "A_MHMPain,A_MHPain,A_Pain";
        //    //AddBuckets(buckets, "", codelist, "", "");
        //    //ct = CountBuckets(buckets);
        //    //CheckAssessment(ct, "Pain=" + ct);
        //    //if (_inds[18].is_checked) return;


        //}

        //=====================================================================
        // Group like assessments into buckets of time
        // Each bucket counts as one assessment in the frequency count

        private void SetBucketSize(int minutes)
        {
            _bucket_size = minutes;
        }

        //Add to the list of bucket numbers (redundant buckets are fine)
        private void AddBuckets(List<int> bucket_list, string cat, string code_list, string desc, string field)
        {
            AddBuckets(bucket_list, cat, code_list, desc, field, "");
        }
        private void AddBuckets(List<int> bucket_list, string cat, string code_list, string desc, string field, string result_list)
        {
            // Assign (virtual) bucket numbers to each assessment.  
            // Every x minutes after that is a new bucket.  Bucket zero = pull_start time.

            // get the chart items for the assessments
            var query = StartNewQuery(SearchDepth.SearchPullPlus);
            query = AndItemFilter(query, cat, code_list, desc, field, result_list);

            // This step is needed for those who want to count the # within a bucket, like Shands #24.
            // This will have no effect on those that count buckets.
            var query2 = (from item in query select new { item.EVENT_DATETIME }).Distinct();

            // figure out what buckets the events belong to
            var query3 = from item in query2
                         select new {
                            bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size)
                         };
            // Add to the list
            foreach (var item in query3) {
                bucket_list.Add(item.bucket);
            }

            // This is just for auditing: Say what we are looking for and how many were found
            if (!String.IsNullOrEmpty(result_list)) {
                // print each word and if it was found or not
                int i = CountResultContains(cat, code_list, desc, field, result_list);
            } else {
                // print how many were found
                Program.VerboseAudit(Describe(cat, code_list, desc, field, ""));
            }
        }

        private int CountBuckets(List<int> bucket_list)       // bucket list: (ha ha)
        {
            int result = bucket_list.Distinct().Count();
            if (result > 0) Program.VerboseAudit(result + " unique");
            return result;
        }




        private void AtLeastOneADL()
        {
            Program.Audit("---------------");
            if (!(_inds[2].is_checked || _inds[3].is_checked || _inds[4].is_checked)) {
                // Make "#2 ADL - Assist" the default.  (90% of patients)
                SetInd(2, "Defaulting to ADL Self due to lack of documentation.");
            }
        }

        private void HighestIndicatorInEachGroupWins()
        {
            int i, g;
            bool highest_is_on;
            string ind_list;

            Program.Audit("Select highest indicator in each group");
            
            g = 0;
            highest_is_on = false;
            // Go from bottom up - highest (lowest) indicator in each group wins
            for (i = MAX_INDS; (i >= 1); i--) {
                if (_inds[i].radio_group > 0) {
                    if (_inds[i].radio_group != g) {
                        //this is a new group
                        g = _inds[i].radio_group;
                        highest_is_on = _inds[i].is_checked;
                    } else {
                        //same group
                        if (highest_is_on) {
                            _inds[i].is_checked = false;             //uncheck a lower number
                        } else {
                            highest_is_on = _inds[i].is_checked;     //save this one
                        }
                    }
                }
            } // next i
            
            //Echo the indicators for an Audit (no classification will be saved)
            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++) {
                if (_inds[i].is_checked) ind_list += "," + i;
            } // next i

            Program.Audit("Final list = " + ind_list.Substring(1));
            Program.Audit("---------------");
        }


        private void CheckProcs()
        {
            //CheckProc_1();
            //CheckProc_2();
            CheckProc_3();
            CheckProc_4();
            CheckProc_5();
            CheckProc_6();
            //CheckProc_7();
            CheckProc_8();
            CheckProc_9();
            CheckProc_10();
            CheckProc_11();

        }

        private void DoProc(int pnum, string code)
        {
            double mins = 0;
            string found_what;
            DateTime evdt;
            DateTime enddt = DateTime.MinValue;

            if (GetResultAndEVDT("", code, "", "", out found_what, out evdt))
            {
                mins = 60.0 * found_what.ToDouble();
                enddt = evdt.AddMinutes(mins);

                if (ProcExistsInDB(pnum, evdt, enddt))
                {
                    Program.Audit("Procedure " + pnum+ ": already exists");
                }
                else
                {
                    if (!QueuedProcOverlaps(pnum, evdt, enddt))
                    {
                        var proc = new proc_data();
                        proc.procedure_number = pnum;
                        proc.start = evdt;
                        proc.finish = enddt;
                        _procs.Add(proc);
                        Program.Audit("Procedure " + pnum + ": Found " + code + " between " + evdt + " and " + enddt);
                    }
                }

            }

        }

        private bool OnlyHasED()
        {
            var db = PFSUtility.NewPfsDataContext();
            var query = from el in db.ENCOUNTER_LOCATIONs
                        join u in db.UNITs on el.UNIT_ID equals u.UNIT_ID
                        where (el.ENCOUNTER_ID == _pat.encounter_id)
                        where (u.IS_ED.ToString().ToUpper() == "N")
                        select new { u.NAME };
            return (query.Count() == 0);
        }

        private bool ProcExistsInDB(int pnum, DateTime startdt, DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && (proc.PROCEDURE_DATETIME == startdt)
                            && (proc.DEPARTURE_DATETIME == enddt)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new { proc.PROCEDURE_EVENT_ID };
            return (query.Count() > 0);
        }

        private bool QueuedProcOverlaps(int pnum, DateTime startdt, DateTime enddt)
        {
            bool overlap = false;

            proc_data[] pary = _procs.ToArray();
            for (int i = 0; i <= pary.GetUpperBound(0); i++)
            {
                if (pary[i].procedure_number == pnum)
                {
                    //overlap if   p.start between stardt and enddt
                    //             p.finish between startdt and enddt
                    //             startdt >= p.start and enddt <= p.finish
                    if ((pary[i].start >= startdt) && (pary[i].start < enddt) ||
                        (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
                        (startdt >= pary[i].start) && (enddt <= pary[i].finish))
                    {
                        overlap = true;
                    }

                    if ((pary[i].start >= startdt) && (pary[i].start < enddt) && (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
                        (pary[i].start < startdt) && (pary[i].finish > startdt) && (pary[i].finish < enddt)
                        )
                    {
                        // the proc in the list is completely contained in startdt/endt pair
                        // change this proc to be the startdt/enddt
                        // overlap is still true, but the list item will be updated with the encompassing times
                        //Program.Audit("Procedure " + pnum + ": " + startdt + " - " + enddt + " will trump " + pary[i].start + " - " + pary[i].finish);
                        pary[i].start = startdt;
                        pary[i].finish = enddt;
                    }
                }
            }
            _procs = pary.ToList();
            return overlap;
        }


        private void CheckProc_1()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P1. 1-1 safety observation by RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_2()
        {
            string nowstr;
            string toddtstr;
            string yesdtstr;
            string timea ="";
            DateTime timea_startdt, timea_enddt;
            DateTime timeb_startdt, timeb_enddt;
            DateTime nowdt = _pat.pull_finish;              // "now" is pull time
            DateTime yesdt;
            
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P2. 1-1 safety observation by non-RN");
            Program.VerboseAudit("---------------");

            nowstr = nowdt.ToString("yyyyMMddHHmm");
            yesdt = nowdt.AddDays(-1);
            toddtstr = nowdt.ToString("yyyyMMdd");
            yesdtstr = yesdt.ToString("yyyyMMdd");
            
            //when is now? (yesterday/today)
            //yes 7am -- yes 7p  -- tod 7a -- tod 7p
            //                                   A                   B
            //if nowdt >= tod7pm then check  tod 7am-tod 7pm and tod 7pm-tom 7a
            //if nowdt >= tod7am then check  yes 7p - tod 7a     tod 7a-tod 7p
            //if nowdt >= yes7pm then check  yes 7a-yes 7p       yes 7p-tod 7a
            if (nowstr.CompareTo(toddtstr + "1900") >= 0) {
                timea = toddtstr + "0700";
            } else if (nowstr.CompareTo(toddtstr + "0700") >= 0) {
                timea = yesdtstr + "1900";
            } else if (nowstr.CompareTo(yesdtstr + "1900") >= 0) {
                timea = yesdtstr + "0700";
            }
            
            timea_startdt = PFSUtility.ISOToDateTime(timea);
            timea_enddt = timea_startdt.AddHours(12);
            MaybeAddSitter(timea_startdt, timea_enddt);

            timeb_startdt = timea_enddt;
            timeb_enddt = timeb_startdt.AddHours(12);
            MaybeAddSitter(timeb_startdt, timeb_enddt);
        }

        private void MaybeAddSitter(DateTime startdt, DateTime enddt)
        {
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);    // add custom time range below
            query = query.Where(e => (e.EVENT_DATETIME >= startdt) && (e.EVENT_DATETIME < enddt));
            query = AndCodeInList(query, "Sitter");
            query = AndResultInList(query,"continued, initiated");
            query = AndResultNotInList(query, "discontinued");

            if (query.Count() > 0) {
                if (ProcExists(2, startdt, enddt)) {
                    Program.Audit("Procedure 2: already exists");
                } else {
                    var proc = new proc_data();
                    proc.procedure_number = 2;
                    proc.start = startdt;
                    proc.finish = enddt;
                    _procs.Add(proc);
                    Program.Audit("Procedure 2: Found Sitter between " + startdt + " and " + enddt);
                }
            }
            
        }

        private bool ProcExists(int pnum, DateTime startdt, DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && (proc.PROCEDURE_DATETIME == startdt)
                            && (proc.DEPARTURE_DATETIME == enddt)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new {proc.PROCEDURE_EVENT_ID};
            return (query.Count() > 0);
        }


        private void CheckProc_3()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P3. Off unit accompanied by RN");
            Program.VerboseAudit("---------------");
            DoProc(3, "A_MHAcuOffUnit");
        }

        private void CheckProc_4()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P4. Off unit accompanied by non-RN");
            Program.VerboseAudit("---------------");
            DoProc(4, "A_MHAcuOffUNonRN");
        }

        private void CheckProc_5()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P5. Patient/family education by RN");
            Program.VerboseAudit("---------------");
            DoProc(5, "A_MHAcuPtFamEduc");
        }

        private void CheckProc_6()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P6. Extensive wound management by RN");
            Program.VerboseAudit("---------------");
            DoProc(6, "A_MHAcuExtensive");
        }

        private void CheckProc_7()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P7. Extensive wound management by non-RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_8()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P8. Coordination of care by RN");
            Program.VerboseAudit("---------------");
            DoProc(8, "A_MHAcuCoordinat");
        }

        private void CheckProc_9()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P9 1:1 RN at bedside");
            Program.VerboseAudit("---------------");
            DoProc(9, "A_MHAcu1:1byURN");
        }

        private void CheckProc_10()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P10. 1:1 non-RN at bedside");
            Program.VerboseAudit("---------------");
            DoProc(10, "A_MhAcu1:1UNonRN");
        }

        private void CheckProc_11()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P11. 2:1 by RN at bedside");
            Program.VerboseAudit("---------------");
            DoProc(11, "A_MHAcu2:1by URN");
        }

        private void CheckOutcomes()
        {
            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = AndItemFilter(query, "", "A_MHPostFalltime", "", "", "");
            foreach (var ch in query)
            {
                var outc = new proc_data();
                outc.procedure_number = 1;
                outc.start = ch.EVENT_DATETIME;
                _outcomes.Add(outc);
                Program.Audit("Outcomes 1: Found A_MHPostFalltime at " + outc.start);
            }
        }
        

        private const string DATETIME_FORMAT = "yyyyMMddHHmm";              // ISO Date/Time w/o seconds

        private void OutputClass()
        {
            string outstr, ind_list, desc, str_pull_dt;
            int i, tc_event_id;

            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         212 242 304
            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1
            //1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901 3456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
            //1       |10 5            |                |                |        |10023121779         |ALEXANDER                       |GLORIA                          |                                |0508    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289530  |           |201403030400                                                                      |NNYNNNNYYNNYNNNYNNNYYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//1       |10 5            |                |                |        |10024544908         |ARMSTRONG                       |AARON                           |                                |0535    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289531  |           |201403030400                                                                      |NYNNYNNNNNNYNNNYNNNNYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//|0547    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289532  |           |201403030400                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//|0547    |A   |201403030700|                |201403030700    |18  |C|    |3040|1440|18289532  |           |201403030700                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN

            if (Program.g_is_test)
                tc_event_id = 9999;
            else
                tc_event_id = PFSUtility.NextGID();                         //get a unique id for this class
            str_pull_dt = _pat.pull_finish.ToString(DATETIME_FORMAT);

            outstr = _pat.facilty_code.FixedWidth(8);                       //(facility code)
            outstr += "|" + _pat.unit_name.FixedWidth(16);
            outstr += "|" + "".FixedWidth(16);                               //(unit code)
            outstr += "|" + "".FixedWidth(16);                               //(area code)
            outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
            outstr += "|" + _pat.acct.FixedWidth(20);
            outstr += "|" + _pat.last_name.FixedWidth(32);
            outstr += "|" + _pat.first_name.FixedWidth(32);
            outstr += "|" + _pat.middle_name.FixedWidth(32);
            outstr += "|" + _pat.room.FixedWidth(8);
            outstr += "|" + _pat.bed.FixedWidth(4);
          outstr += "|" + str_pull_dt.FixedWidth(12);                      //class datetime (could change)
            outstr += "|" + "".FixedWidth(16);                               //(login)
          outstr += "|" + str_pull_dt.FixedWidth(16);                      //(employee)/(pull datetime)
            outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
            outstr += "|" + "C".FixedWidth(1);                               //record type = class
            outstr += "|" + "".FixedWidth(4);                                //(stage)
            outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
            outstr += "|" + _pat.range.ToString().FixedWidth(4);             //TC pull range
            outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
            outstr += "|";
            outstr = outstr.FixedWidth(294);
          outstr += "|" + _pat.unit_arrival.ToString(DATETIME_FORMAT);        //IN
            outstr = outstr.FixedWidth(377);
            outstr += "|";
            
            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++) {
                if (_inds[i].is_checked) {
                    outstr += "Y";
                    ind_list += "," + i;
                } else {
                    outstr += "N";
                }
            } // next i
            ind_list = ind_list.Substring(1);                           //strip leading comma
  
            Program.outfile.WriteLine(outstr);                          //output to transparent.txt

            Program.Audit("");
            desc = "Classified: " + ind_list;
            if (Program.g_is_test) {
                Program.Audit(desc);
            } else {
                //Save the selected indicators plus both Program.Audits in the event log; link with tc_event_id
                PFSEventLog.AddTransparentMappingEventLogEntry(
                    desc, Program.gLogUnitID, Program.gLogEncounterID,
                    tc_event_id, Program.gLogMapperVersion,
                    Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
            }
        }

        private void OutputProcs()
        {
            int i;
            string outstr, proc_list, desc;
            int tc_event_id;

            foreach(var proc in _procs) {
                if (Program.g_is_test)
                    tc_event_id = 9999;
                else
                    tc_event_id = PFSUtility.NextGID();                         //get a unique id for this proc

                outstr = _pat.facilty_code.FixedWidth(8);
                outstr += "|" + _pat.unit_name;                                 //10
                outstr = outstr.FixedWidth(68);
                outstr += "|" + _pat.acct.FixedWidth(20);                       //90
                outstr += "|" + _pat.last_name.FixedWidth(32);
                outstr += "|" + _pat.first_name.FixedWidth(32);
                outstr += "|" + _pat.middle_name.FixedWidth(32);
                outstr = outstr.FixedWidth(202);
                outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //204 proc dt
                outstr = outstr.FixedWidth(254);
                outstr += "|P";                                                 //256 procedure type record
                outstr += "|" + "".FixedWidth(4);                               //(stage)
                outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);     //TC source ID
                outstr += "|" + _pat.range.ToString().FixedWidth(4);            //TC pull range
                outstr += "|" + tc_event_id.ToString().FixedWidth(10);          //TC event ID
                outstr += "|";
                outstr = outstr.FixedWidth(294);
                outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //296 procdt in
                outstr = outstr.FixedWidth(346);
                outstr += "|" + proc.finish.ToString(DATETIME_FORMAT);          //348 procdt out
                outstr = outstr.FixedWidth(377);
                outstr += "|";
                
                proc_list = "";
                for (i = 1; (i < MAX_PROCS); i++) {
                    if (proc.procedure_number == i) {
                        outstr += "Y";
                        proc_list += "," + i;
                    } else {
                        outstr += "N";
                    }
                } // next i
                proc_list = proc_list.Substring(1);                             //strip leading comma

                Program.outfile.WriteLine(outstr);                              //output to transparent.txt

                desc = "Procedures: " + proc_list;
                if (Program.g_is_test) {
                    Program.Audit(desc);
                } else {
                    //Save the selected procedures plus both Program.Audits in the event log; link with tc_event_id
                    //(this assumes the procedure Program.Audit is mixed in with the class Program.Audit)
                    PFSEventLog.AddTransparentMappingEventLogEntry(desc, Program.gLogUnitID, Program.gLogEncounterID,
                        tc_event_id, Program.gLogMapperVersion,
                        Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
                }
            } // next proc
        }

        private void OutputOutcomes()
        {
            string outstr;

            foreach (var oc in _outcomes)
            {
                outstr = "".FixedWidth(9) + _pat.unit_name.FixedWidth(16);       // unitname
                outstr = outstr.FixedWidth(60);
                outstr += oc.start.ToString(DATETIME_FORMAT);                 //event_datetime
                outstr = outstr.FixedWidth(73);
                outstr += _pat.acct.FixedWidth(20);                            //acct
                outstr = outstr.FixedWidth(94);
                outstr += oc.procedure_number;                                //outcome indicator
                Program.out2file.WriteLine(outstr);                          //output to outcomesindicator.txt
            }

        }


    }
}
