﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;                     // for LINQ; add a reference
using System.Data.SqlClient;                // for SqlConnection
using System.IO;                            // for Directory, Path, Stream
using PfsShared;                            // add a reference to Shared2 project

// ============================================================================
// Inpatient 2.0 transparent mapping -- GOES HERE --
// Lawrence
// ============================================================================
// This processes one patient.  Only one database query is used.  :)
//
// All search functions use exact match for category, description and field name.
// Codes and results can be a single word or a list of words.
// Codes default to exact match; prefix with CODE_CODE_LIKE_PREFIX if desired (single word only)
// Results default to like matching; prefix with RESULT_EXACT_MATCH_PREFIX if desired (single word only)
//
// All searches are case insensitive.
//
//
namespace TransparentMapping
{
    class Inpatient2
    {
        private const int MAX_INDS = 50;
        private const int MAX_PROCS = 20;

        private const string CODE_LIKE_PREFIX = "%!";       // use to make codes %like%
        private const string EXACT_MATCH_PREFIX = "&!";     // use to make desc/result exact match
        private const string CHAR_COMMA = "||";             // use to insert literal comma in word list
        private const string NOT_PREFIX = "!!";             // use to insert literal comma in word list

        private struct indicator_data {
            public bool     is_checked;     
            public int      radio_group;       
        }

        private struct proc_data {                      
            public int      procedure_number;
            public DateTime start;   
            public DateTime finish;
        }

        // These are database CHART_ITEMs for this patient
        private CHART_ITEM[] _chart_items_since_admission;
        private CHART_ITEM[] _chart_items_since_unit_arrival;
        private CHART_ITEM[] _chart_items_during_pull_period;
        // These are database PROCEDURE_EVENTs for this patient
        private PROCEDURE_EVENT[] _procedure_events;

        private indicator_data[] _inds;
        private List<proc_data> _procs;
        private List<proc_data> _outcomes;

        private const int DEFAULT_BUCKET_SIZE = 20;       //min

        private PatientInfo _pat;
        private bool adl23;
        private bool adl4;
        private bool tubefeed;
        private bool coma;
        private int  _bucket_size = DEFAULT_BUCKET_SIZE;
        private bool is_default;
        private bool give_default_inds = false;


        private enum SearchDepth {
            SearchDefault,
            SearchPullRange,            //search within the current pull     -- default
            SearchSinceArrival,         //search since arrival to the unit
            SearchSinceAdmission        //search everything since admission to the hospital
        }

        private enum CountMode {
            CountAll,
            CountFirst                  //stop after one is found
        }

        private enum GetValueMode {
            GetTotal,
            GetMax,
            GetLast
        }

        enum Frequencies {
            QNONE,
            Q4H,
            Q2H,
            Q1H,
            Q30M
        }

        private struct fmapRow {
            public double   los_high;                       //the LOS being testing
            public int[]    freq;                           //the count required for each Q value
        }
        private List<fmapRow> _freq_map;                    //1,2,4,8,12,24 hours
        private struct gBucket
        {
            public int bucket;
            public string code;
            public DateTime evdt;
        }

     
        //
        // This is the main entry point
        //
        public void ProcessPatient(PatientInfo pat)
        {
            _pat = pat;

            InitIndicators(); // sets is_default
            InitProcs();
            if (! is_default)
                {
                LoadFreqTable();
                LoadPatientChart();
                Check_1_2_3_4();
                Check_5();
                Check_6_7();
                Check_8();
                Check_9();
                Check_10_11();
                Check_12_13();
                Check_14();
                Check_15_16_17_18();
                Check_19();
                Check_20();
                Check_21_22();
                Check_23();
                Check_24();
            }
            AtLeastOneADL();
            HighestIndicatorInEachGroupWins();

            if (!is_default)
            {
                CheckProcs();
                //CheckOutcomes();
            }

            if (Program.g_no_output) return;
            bool def_in_past4hrs;
            if (def_in_past4hrs = DefaultInPast4hrs())
            {
                Program.VerboseAudit("default found def ptype=" + _pat.default_ptype);
                give_default_inds = (_pat.default_ptype > DeterminePtypeOfIndicators());
            }
            OutputClass();
            OutputProcs();
            //OutputOutcomes();
        }

       
        private void InitIndicators()
        {
            // Make a new (empty) indicator array
            _inds = new indicator_data[MAX_INDS + 1];           // This 1 based so add one
            is_default = false;
            //if (_pat.los_hours <= 4.0) {
            //    is_default = true;
            //    Program.VerboseAudit("Patient was here 4 hrs or less. Will receive default indicators " + _pat.default_inds_str);
            //    foreach (var ind in _pat.default_inds) {
            //        if (ind <= _inds.GetUpperBound(0)) {
            //            _inds[ind].is_checked = true;
            //        }
            //    }
            //}

            adl23 = false;
            adl4 = false;
            tubefeed = false;
            coma = false;

            // get indicator radio groups from the database
            // ** (This database access can be replaced once we have a C# methodolgy cache)
            var db = PFSUtility.NewPfsDataContext();
            var query = from idef in db.INDICATOR_DEFINITIONs
                        where (idef.METHODOLOGY_ID == _pat.meth_id)
                        select idef;
            foreach (var idef in query) {
                if (idef.INDICATOR_NUMBER <= _inds.GetUpperBound(0)) {
                    // (convert nulls to zero)
                    _inds[idef.INDICATOR_NUMBER].radio_group = PFSUtility.DBToInt(idef.RADIO_GROUP);
                }
            }
        }

        private void InitProcs()
        {
            _procs = new List<proc_data>();
            _outcomes = new List<proc_data>();
        }

        private fmapRow LoadFreqTableRow(double los_high, string values)
        {
            fmapRow fmrow;
            
            fmrow.los_high = los_high;
            fmrow.freq = new int[5];

            var arr = values.Split(',');
            for (int i = 0; i <= fmrow.freq.GetUpperBound(0) ; i++ )
            {
                fmrow.freq[i] = arr[i].ToInteger();
            }
            return fmrow;
        }

        // These are the number of times you need to do something during a certain time period (LOS)
        // in order for it to be considered one of the frequencies listed.
        private void LoadFreqTable()
        {
            _freq_map = new List<fmapRow>();
            //                              LOS,  None Q4h Q2h Q1h Q30m
            _freq_map.Add(LoadFreqTableRow(1, "    0,  0,  0,  1,  2"));
            _freq_map.Add(LoadFreqTableRow(2, "    0,  0,  1,  2,  3"));
            _freq_map.Add(LoadFreqTableRow(4, "    0,  1,  2,  3,  5"));
            _freq_map.Add(LoadFreqTableRow(6, "    0,  1,  2,  4,  8"));
            _freq_map.Add(LoadFreqTableRow(8, "  0,  1,  2,  6, 10"));
            _freq_map.Add(LoadFreqTableRow(12, " 0,  2,  4,  8, 15"));
            _freq_map.Add(LoadFreqTableRow(16, "   0,  3,  5,  9, 20"));
            _freq_map.Add(LoadFreqTableRow(24, "   0,  4,  8, 15, 29"));
            _freq_map.Add(LoadFreqTableRow(9999, " 0,  4,  8, 15, 29"));
//New freq table 2/5/14
//q4	q2	q1	q30     q30
//            Non-ICU	ICU & SD
// 4	8	15	29	    36
// 3	5	9	17	    24
// 2	4	7	13	    19
// 2	3	5	10	    13

            //to read this table: if you have at least 6 asessmenets in a 12-hour period, you get Q1h
            //(LOS=12, column Q1h has a count of 6)
        }

        private Frequencies FreqForCount(double los_hours, int count) 
        {
            foreach(var fmrow in _freq_map) {
                if (los_hours <= fmrow.los_high) {
                    // Option: pro-rate the count upward if the LOS is less than the row we are interested in.
                    //         This will bump the count to what it might have been at the full LOS.
                    // Note: truncate the result; rounding inflates the value too much.
                    int prorated_count = (int)((fmrow.los_high / los_hours) * count);

                    // foreach goes low to high; go from high to low instead
                    for (int j = (int)Frequencies.Q30M; (j >= (int)Frequencies.QNONE); j--) { //search right to left
                        if (prorated_count >= fmrow.freq[j]) {
                            return (Frequencies)j;
                        }
                    } // next j
                }
            }
            
            return Frequencies.QNONE;
        }

        //
        // Load patient chart from the database (one DB access)
        // Load everything since admission
        //
        private void LoadPatientChart()
        {
            // Get the entire patient chart (all units and dates for this patient)
            var db = PFSUtility.NewPfsDataContext();
            var query = from item in db.CHART_ITEMs
                        where (item.ENCOUNTER_ID == _pat.encounter_id)
                        select item;
            // Save the result
            _chart_items_since_admission = query.ToArray();

            // Convert all results to lower case for case insensitive comparisons
            foreach (var item in _chart_items_since_admission) {
                if (item.CATEGORY != null) item.CATEGORY = item.CATEGORY.ToLower();
                if (item.CODE != null) item.CODE = item.CODE.ToLower();
                if (item.DESCRIPTION != null) item.DESCRIPTION = item.DESCRIPTION.ToLower();
                if (item.FIELD_NAME != null) item.FIELD_NAME = item.FIELD_NAME.ToLower();
                if (item.RESULT != null) item.RESULT = item.RESULT.ToLower();
            }

            // Prepare two more versions of the chart
            var query2 = from item in _chart_items_since_admission
                    where(item.EVENT_DATETIME >= _pat.unit_arrival) && (item.EVENT_DATETIME < _pat.pull_finish)
                    select item;
            _chart_items_since_unit_arrival = query2.ToArray();

            query2 = from item in _chart_items_since_admission
                     where (item.EVENT_DATETIME >= _pat.pull_start) && (item.EVENT_DATETIME < _pat.pull_finish)
                     select item;
            _chart_items_during_pull_period = query2.ToArray();
        }

        // Delay looking for patient procedures until they are wanted
        private void LoadPatientProceduresIfNeeded()
        {
            if (_procedure_events != null) return;

            var db = PFSUtility.NewPfsDataContext();
            var query = from proc in db.PROCEDURE_EVENTs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                        select proc;
            _procedure_events = query.ToArray();
        }



        // Started a new chart item query (of a certain depth)
        // Default is the chart during the pull range.  Options for unit arrival and admission to hospital.
        private IEnumerable<CHART_ITEM> StartNewQuery()
        {
            return StartNewQuery(SearchDepth.SearchDefault);
        }
        private IEnumerable<CHART_ITEM> StartNewQuery(SearchDepth search_depth)
        {
            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    return (from item in _chart_items_during_pull_period select item);
                case SearchDepth.SearchSinceArrival:
                    return (from item in _chart_items_since_unit_arrival select item);
                case SearchDepth.SearchSinceAdmission:
                    return (from item in _chart_items_since_admission select item);
            }
            return null;
        }

        // Is this search term a word or a list of words?
        private bool ValueIsAList(string s) 
        {
            return (s != null) && s.Contains(',');                //is this a comma-separated list?
        }

        // Break apart a wordlist and prepare its members
        private string[] SplitOnCommaAndPrepareElements(string s)
        {
            // If a word list has a space after each comma, we will get leading blanks in each term
            var arr = s.Split(',');
            for (int i = 0; i <= arr.GetUpperBound(0); i++) {
                arr[i] = arr[i].Trim();                         // get rid of leading blanks
                arr[i] = arr[i].Replace(CHAR_COMMA, ",");       // insert a literal comma into search word
                arr[i] = arr[i].ToLower();                      // convert for case-insensitive comparisons
            }
            return arr;
        }

        // Add a basic chart item filter to a LINQ query
        //
        // cat = exact match
        // code_list = exact match; optional "like" match
        // desc_list = "like" match; optional exact match
        // field = exact match
        // result_list = "like" match; optional exact match
        //
        // These are comma-separated lists.  
        // Search for a literal comma by using CHAR_COMMA in the string.
        private IEnumerable<CHART_ITEM> AndItemFilter(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list) 
        {
            // Converted all values to lower case for case-insensitive comparisons.
            if (!String.IsNullOrEmpty(cat))
            {
                cat = cat.ToLower();
                query = query.Where(e => e.CATEGORY == cat);
            }

            if (!String.IsNullOrEmpty(code_list)) {
                code_list = code_list.ToLower();
                if (code_list.Left(2) == EXACT_MATCH_PREFIX) {          // exact match override?
                    query = query.Where(e => e.CODE == code_list.Substring(2));
                } else if (code_list.Left(2) == CODE_LIKE_PREFIX)
                {            // override for "like" match?
                    query = query.Where(e => e.CODE.Contains(code_list.Substring(2)));
                } else if (ValueIsAList(code_list)) {
                    query = AndCodeInList(query, code_list);            // find one of the words
                } else {
                    query = query.Where(e => e.CODE.Contains(code_list));      // find this word
                }
            }
            
            if (!String.IsNullOrEmpty(desc_list)) {
                desc_list = desc_list.ToLower();
                if (desc_list.Left(2) == EXACT_MATCH_PREFIX) {          // exact match override?
                    query = query.Where(e => e.DESCRIPTION == desc_list.Substring(2));
                } else if (ValueIsAList(desc_list)) {
                    query = AndDescriptionInList(query, desc_list);
                } else {
                    desc_list = desc_list.Replace(CHAR_COMMA, ",");     // look for literal comma requests
                    query = query.Where(e => e.DESCRIPTION.Contains(desc_list));
                }
            }
            
            //if (!String.IsNullOrEmpty(field)) {
            //    field = field.ToLower();
            //    query = query.Where(e => e.FIELD_NAME == field);
            //}
            
            if (!String.IsNullOrEmpty(result_list)) {
                result_list = result_list.ToLower();                        // make all results case insensitve
                if (result_list.Left(2) == EXACT_MATCH_PREFIX) {     // exact match override?
                    query = query.Where(e => e.RESULT == result_list.Substring(2));
                } else if (ValueIsAList(result_list)) {
                    query = AndResultInList(query, result_list);
                } else {
                    result_list = result_list.Replace(CHAR_COMMA, ",");     // look for literal comma requests
                    query = query.Where(e => e.RESULT.Contains(result_list));
                }
            }
            return query;
        }

        // Look for a code that is one of the words in the list.
        private IEnumerable<CHART_ITEM> AndCodeInList(IEnumerable<CHART_ITEM> query, string code_list) 
        {
            if (String.IsNullOrEmpty(code_list)) return query;
            
            var arr = SplitOnCommaAndPrepareElements(code_list);
            return query.Where(e => e.CODE.ContainsAny(arr));  // use like match. Exact match = arr.Contains(e.CODE));   
        }

        // Look for a description that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndDescriptionInList(IEnumerable<CHART_ITEM> query, string desc_list) 
        {
            if (String.IsNullOrEmpty(desc_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(desc_list);
            return query.Where(e => e.DESCRIPTION.ContainsAny(arr));    // "like" match
        }

        // Look for a result that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultInList(IEnumerable<CHART_ITEM> query, string result_list) 
        {
            if (String.IsNullOrEmpty(result_list)) return query;
            
            var arr = SplitOnCommaAndPrepareElements(result_list);
            return query.Where(e => e.RESULT.ContainsAny(arr));         // "like" match
        }

        // Look for a result that does not contain one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultNotInList(IEnumerable<CHART_ITEM> query, string result_list)
        {
            if (String.IsNullOrEmpty(result_list)) return query;
            
            var arr = SplitOnCommaAndPrepareElements(result_list);
            return query.Where(e => !e.RESULT.ContainsAny(arr));        // "like" match
        }

        private string DescribeSearchDepth(SearchDepth search_depth) 
        {
            string result = "";
            
            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    //result = "in pull range"
                    result = "";                         //be quiet since this is default
                    break;
                case SearchDepth.SearchSinceArrival:
                    result = "since arrival to unit";
                    break;
                case SearchDepth.SearchSinceAdmission:
                    result = "since admission";
                    break;
            }
            
            return result;
        }

        // Describe a query that has already been run
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Make a simple query and describe the results
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            var arr = query.ToArray();
            return Describe(arr, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private string LookingFor(string so_far, string what, string op, string value)
        {
            string result;
            if (String.IsNullOrEmpty(value)) return so_far;

            if (so_far == "")
                result = "looking for " + what;
            else
                result = so_far + " and " + what;

            if (value.Left(2) == EXACT_MATCH_PREFIX)
                result += "='" + value.Substring(2) + "'";
            else if (value.Left(2) == CODE_LIKE_PREFIX)
                result += " contains " + "'" + value.Substring(2) + "'";
            else if (ValueIsAList(value))
                result += " in '" + value + "'";
            else
                result += op + "'" + value + "'";
            
            return result;
        }
        private string Describe(CHART_ITEM[] arr, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string result = "";
            result = LookingFor(result, "cat", "=", cat);
            result = LookingFor(result, "code", "=", code_list);
            result = LookingFor(result, "desc", " contains ", desc_list);
            result = LookingFor(result, "field", "=", field);
            result = LookingFor(result, "result", " contains ", result_list);

            if (arr.Count() == 0) {
                result += "; not found " + DescribeSearchDepth(search_depth);
            } else {
                var e = arr[0];
                result = "FOUND: " + result + "; found";
                // We might have searched for a pattern or word list in several fields - show what was found
                if (e.CATEGORY != null)     result += " cat='" + e.CATEGORY + "'";
                if (e.CODE != null)         result += " code='" + e.CODE + "'";
                if (e.DESCRIPTION != null)  result += " desc='" + e.DESCRIPTION + "'";
                if (e.FIELD_NAME != null)   result += " field='" + e.FIELD_NAME + "'";
                if (e.RESULT != null)       result += " result='" + e.RESULT + "'";
                // Are there more results?  Just say how many; we aren't going to list them.
                if (arr.Count() == 2) {
                    result += " (1 more result)";
                } else if (arr.Count() > 2) {
                    result += " (" + (arr.Count() - 1) + " more results)";
                }
            }
            
            return result;
        }


        //Set an indicator for this reason (low level)
        private void SetInd(int inum, string reason)
        {
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Set Ind #" + inum + ": " + reason);          //already set - repeat for verbose only
            } else {
                _inds[inum].is_checked = true;
                Program.Audit("Set Ind #" + inum + ": " + reason);
            }
        }

        //Clear an indicator for this reason (low level)
        private void ClrInd(int inum, string reason)
        {
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Clr Ind #" + inum + ": " + reason);          //already clear - repeat verbose only
            } else {
                _inds[inum].is_checked = false;
                Program.Audit("Clr Ind #" + inum + ": " + reason);
            }
        }

        // Count how many items meet the conditions
        // All items are listed
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountItems(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            int count = query.Count();

            // always return what was found
            found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
            // echo the result?
            if (trace) Program.VerboseAudit(found_what);
            
            return count;
        }

        // Count how many items meet the conditions
        // This has the option to list every entry or give a total
        private int CountResultInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what="";
 
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);

            var arr = SplitOnCommaAndPrepareElements(result_list);

            foreach (var item in query) {
                // Figure out which of the search words were found
                var s = item.RESULT.ContainsWhich(arr);
                found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' code='" + item.CODE + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "'";
                if (trace) Program.VerboseAudit(found_what);
                count++;
                if (count_mode != CountMode.CountAll) break;        //stop counting
            }
            
            if (count > 0) {
                //We already printed what was found; maybe add how many?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }
            
            return count;
        }

        // Count how many items contain ALL results in list
        private int CountResultInListAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what = "";

            var arr = SplitOnCommaAndPrepareElements(result_list);

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            for (int i = 0; i <= arr.GetUpperBound(0); i++)
            {
                if (arr[i].StartsWith(NOT_PREFIX))
                {
                    query = query.Where(e => !e.RESULT.Contains(arr[i].Substring(2)));
                }
                else
                {
                    query = query.Where(e => e.RESULT.Contains(arr[i]));
                }
            }

            count = query.Count();

            if (count > 0)
            {
                found_what = "found item with all results in '" + result_list + "' without those prefixed by !!";
                if (trace) Program.VerboseAudit(found_what);
            }
            else
            {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (ValueIsAList(result_list)) {
                return CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what);
            } else {
                return CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what);
            }
        }

        // Is there an item with this result?
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list) 
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth) 
        {
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContains(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what) 
        {
            if (ValueIsAList(result_list)) {
                return (CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            } else {
                return (CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
            }
        }

        // Result contains all in list
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (ValueIsAList(result_list))
            {
                return (CountResultInListAll(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            }
            else
            {
                found_what = "Not valid for non-list results.";
                return false;
            }
        }
        // Count how many items meet the conditions, with none of the results in result_list.
        // This has the option to say how many were found rather than list all.
        private int CountResultNotInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what) 
        {
            int count = 0, rec_count = 0;
            bool found_one;

            found_what = "";
            var arr = SplitOnCommaAndPrepareElements(result_list);         // also deals with CHAR_COMMA
            
            //Do not filter by result here; we need all results
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");
            
            foreach(var item in query) {
                rec_count++;
                //Look for each search word in the result
                found_one = false;
                
                foreach (string s in arr) {
                    if (String.Equals(item.RESULT, s)) {
                        found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "' -- ignore this";
                        Program.VerboseAudit(found_what);
                        found_one = true;
                        break;
                    }
                }

                if (! found_one) {
                    //none of the words were found - good!
                    found_what = Describe(cat, code_list, desc_list, field, "", search_depth) + " -- does not contain '" + result_list + "'";
                    Program.VerboseAudit(found_what);
                    count++;
                    if (count_mode != CountMode.CountAll) break;
                }
            }
            
            if (count > 0) {
                //We already printed what was found; maybe show the total?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else if (rec_count > 0) {
                //We already printed what we ignored
            } else {
                //Print what did not exist
                found_what = Describe(cat, code_list, desc_list, field, "", search_depth);      //not found
                Program.VerboseAudit(found_what);
            }
            
            return count;
        }

        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, string found_what) 
        {
            // Search in the pull range with trace on
            return CountResultDoesNotContain(cat, code_list, desc_list, field, result_list, out found_what, SearchDepth.SearchDefault, true);
        }
        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, out string found_what, SearchDepth search_depth, bool trace)
        {
            return CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0;
        }

        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            return (CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0);
        }

        // Set the indicator if the conditions are met
        //
        private bool SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);

        }
        private bool SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            bool b = false;
            //avoid more queries if the indicator is already set
            if (_inds[inum].is_checked) return true;
            
            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what)) {
                SetInd(inum, found_what);                          //echo here - found
                b = true;
            } else {
                Program.VerboseAudit(found_what);                  //and here - not found
            }
            return b;
        }
        // Special contains all
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContainsAll(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already set
            if (_inds[inum].is_checked) return;

            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                          //echo here - found
            }
            else
            {
                Program.VerboseAudit(found_what);                  //and here - not found
            }
        }
        // Set the indicator if the result does not contain any of the words in result_list
        //
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultDoesNotContain(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {   
            string found_what;
            //avoid more queries if the indicator is already set
            if (_inds[inum].is_checked) return;
            
            //Turn trace off for ResultDoesNotContain() and echo what was set below with SetInd
            if (ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                           //echo here - not found
            } else {
                Program.VerboseAudit(found_what);                   //and here - found
            }
        }

        // Clear the indicator if the result contains one of the words in the result_list
        //
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already clear
            if (! _inds[inum].is_checked) return;
            
            //Turn trace off for ResultContains() and echo what was set below with SetInd
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what)) {
                ClrInd(inum, found_what);                           //echo here - found
            } else {
                Program.VerboseAudit(found_what);                   //and here - not found
            }
        }


        //These slightly smaller functions are meant for places where you aren't really looking for a chart result
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            return (CountResultContains(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
        }

        // Variations with optional parameters (can switch to optional parameters in VS 2010)
        //
        private void SetIndIfFound(int inum, string cat)
        {
            SetIndIfResultContains(inum, cat, "", "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list)
        {
            SetIndIfResultContains(inum, cat, code_list, "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private void ClrIndIfFound(int inum, string cat)
        {
            ClrIndIfResultContains(inum, cat, "","","","", SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }


        // Get the max/total value from a result (usually in the middle of the text)
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetIntValue(get_mode, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            int value;
            int result = 0;
            bool found_one = false;

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            
            //Look for a number in the result
            
            foreach(var item in query) {
                // RESULT might be null so convert to empty string so Split won't blow up
                var arr = PFSUtility.DBToString(item.RESULT).Split(' ');
                foreach (string s in arr) {
                    //Look for 1st character numeric; IsNumeric("60min") will fail so look at 1st char only
                    if (s.Left(1).IsNumeric()) {
                        value = (int)s.Val();                           //Use Val; ToInteger will error on "60min"
                        switch (get_mode) {
                            case GetValueMode.GetMax:
                                result = Math.Max(value, result);       //max
                                break;
                            case GetValueMode.GetTotal:
                                result += value;                        //total
                                break;
                            case GetValueMode.GetLast:
                                result = value;                         //last
                                break;
                        }
                        
                        // print what we are searching for (the first time)
                        if (! found_one) {
                            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
                        }
                        found_one = true;
                        // print each value found
                        Program.VerboseAudit("  found numeric value " + result);
                        //Keep going in case there are more
                    }
                } 
            }
            
            if (! found_one) {
                //show what was not found
                Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
            }
            
            return result;
        }

        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetMaxValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return GetIntValue(GetValueMode.GetMax, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetTotalValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return GetIntValue(GetValueMode.GetTotal, cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Get a result; returns true if found with return_result
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result)
        {
            return GetResult(cat, code_list, desc_list, field, out return_result, SearchDepth.SearchDefault);
        }
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0) {
                return_result = PFSUtility.DBToString(query.First().RESULT);
            } else {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }
        private void GetResultforOther(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "Other (comment)");
            return_result="";
            foreach (var item in query)
            {
                if (!String.Equals(item.RESULT, "Other (comment)"))
                {
                    return_result = item.RESULT;
                    //Program.VerboseAudit(found_what);
                    break;
                }

            }
            return;
        }

        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, out string return_result, out DateTime return_evdt)
        {
            return GetResultAndEVDT(cat, code_list, desc_list, field, out return_result, out return_evdt, SearchDepth.SearchDefault);
        }
        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, out string return_result, out DateTime return_evdt, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0)
            {
                return_result = PFSUtility.DBToString(query.First().RESULT);
                return_evdt = PFSUtility.DBToDateTime(query.First().EVENT_DATETIME);
            }
            else
            {
                return_result = "";
                return_evdt = DateTime.MinValue;
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }


        //======================================================================================
        // Mapping starts here
        //======================================================================================

        private void Check_1_2_3_4()
        {
            //bool mob3 = false;
            bool mob4 = false;

            bool feed2 = false;
            bool bath2 = false;
            bool toil2 = false;
            bool mob2 = false;

            //bool feed3 = false;
            //bool feed4 = false;
            bool hyg3 = false;
            bool hyg4 = false;
            string found_what;
            bool adl1 = false;
            bool adl2 = false;
            bool adl3 = false;
            bool adl4 = false;
            bool adl5 = false;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("1. ADL Self");
            Program.VerboseAudit("2. ADL Assist");
            Program.VerboseAudit("3. ADL Extended");
            Program.VerboseAudit("4. ADL Complete");
            Program.VerboseAudit("---------------");
            
            // 4. COMPLETE CARE

            if (_pat.age < 4.0) {
                SetInd(4,"Age <=3 years");
            }
            if (_inds[4].is_checked) return;
//header tells Start and End time range otherwise 8 hours when steady ICU
            bool totbath = Exists("ADL001", "FND007", "", "N502", "");
            totbath |= Exists("ADL001", "MCK_QMR707", "", "N502", "");
            totbath |= Exists("ADL001", "MCK_QMR707", "", "BTH001", "");

            bool totfed = Exists("ADL001", "FND763", "", "N505", "");
            totfed |= Exists("ADL001", "FND027", "", "N505", "");
            totfed |= Exists("ADL001", "FND763", "", "EAT001", "");
            totfed |= Exists("ADL001", "FND027", "", "EAT001", "");
            totfed |= Exists("NUT001", "FND296", "", "DIT004", "");
            totfed |= Exists("NUT001", "FND027", "", "DIT002", "");
            totfed |= Exists("NUT001", "FND104899", "", "DTPH", "");
            totfed |= Exists("GTOSTA", "FND767", "", "FED003", "");
            totfed |= Exists("GTOSTA", "FND766", "", "FED003", "");
            totfed |= Exists("GTOSTA", "FND101052", "", "FED003", "");
            totfed |= Exists("GTOSTA", "FND100061", "", "FED003", "");
            totfed |= Exists("GTOSTA", "FND100055", "", "FED003", "");
            totfed |= Exists("GTOSTA", "FND940", "", "FED003", "");
            totfed |= Exists("GTOSTA", "FND766", "", "FED003", "");
            totfed |= Exists("GTOSTA", "FND100061", "", "FED003", "");
            totfed |= Exists("GTOSTA", "FND767", "", "FED003", "");
            totfed |= Exists("GTOSTA", "FND100055", "", "FED003", "");
            totfed |= Exists("GTOSTA", "FND940", "", "FED003", "");

            if (totbath && totfed) 
                SetInd(4,"Total Feeding and Total Bathing found.");

            SetIndIfResultContains(4, "NUR001", "MCK_QMR658", "", "N446", "");
            SetIndIfResultContains(4, "NUR001", "FND335", "", "N446", "");
            SetIndIfResultContains(4, "EKOS", "FND335", "", "N447", "");


            //SetIndIfResultContains(4, "ADL001", "FND007", "", "N503", "");
            //SetIndIfResultContains(4, "ADL001", "FND007", "", "N504", "");
            //SetIndIfResultContains(4, "ADL001", "FND007", "", "N506", "");
            //SetIndIfResultContains(4, "ADL001", "FND014", "", "N504", "");
            //SetIndIfResultContains(4, "ADL001", "FND015", "", "N504", "");
            //SetIndIfResultContains(4, "ADL001", "FND016", "", "N504", "");
            //SetIndIfResultContains(4, "ADL001", "FND007", "", "ORL001", "");
            //SetIndIfResultContains(4, "ADL001", "FND007", "", "GRM001", "");
            //SetIndIfResultContains(4, "ADL001", "FND007", "", "BWL001", "");
            //SetIndIfResultContains(4, "ADL001", "FND014", "", "BWL001", "");
            //SetIndIfResultContains(4, "ADL001", "FND015", "", "BWL001", "");
            //SetIndIfResultContains(4, "ADL001", "FND016", "", "BWL001", "");
            //SetIndIfResultContains(4, "NUT001", "", "", "DIT005", "");
            //SetIndIfResultContains(4, "GTOSTA", "FND697", "", "OST012", "");
            //SetIndIfResultContains(4, "GTOSTA", "FND696", "", "OST012", "");
            //SetIndIfResultContains(4, "GTOSTA", "FND844", "", "OST012", "");
            //SetIndIfResultContains(4, "GTOSTA", "FND698", "", "OST012", "");
            //SetIndIfResultContains(4, "GTOSTA", "FN101052", "", "OST012", "");
            //SetIndIfResultContains(4, "GTOSTA", "FND100117", "", "OST012", "");
            //SetIndIfResultContains(4, "GTOSTA", "FND697", "", "OST012", "");
            //SetIndIfResultContains(4, "GTOSTA", "FND696", "", "OST012", "");
            //SetIndIfResultContains(4, "GTOSTA", "FND844", "", "OST012", "");
            //SetIndIfResultContains(4, "GTOSTA", "FND698", "", "OST012", "");
            //SetIndIfResultContains(4, "GTOSTA", "FND101052", "", "OST012", "");
            //SetIndIfResultContains(4, "GTOSTA", "FND100117", "", "OST012", "");
            //SetIndIfResultContains(4, "GNU001", "FND108386", "", "FOLE9", "");


            bool grm = false;
            bool bth = false;
            bool eat = false;
            bool toi = false;
            bool mob = false;

            grm = grm || SetIndIfResultContains(2, "ADL001", "MCK_QMR700", "", "N503", "");
            grm = grm || SetIndIfResultContains(2, "ADL001", "FND008", "", "N503", "");
            grm = grm || SetIndIfResultContains(2, "ADL001", "MCK_QMR700", "", "GRM001", "");
            grm = grm || SetIndIfResultContains(2, "ADL001", "FND101055", "", "N34", "");
            grm = grm || SetIndIfResultContains(2, "ADL001", "FND101056", "", "N34", "");

            bth = bth || SetIndIfResultContains(2, "ADL001", "FND008", "", "N502", "");
            bth = bth || SetIndIfResultContains(2, "ADL001", "FND001", "", "N502", "");
            bth = bth || SetIndIfResultContains(2, "ADL001", "FND102744", "", "BTH001", "");
            bth = bth || SetIndIfResultContains(2, "ADL001", "FND001", "", "BTH001", "");
            bth = bth || SetIndIfResultContains(2, "ADL001", "FND101053", "", "N34", "");
            bth = bth || SetIndIfResultContains(2, "ADL001", "FND101059", "", "N34", "");

            eat = eat || SetIndIfResultContains(2, "ADL001", "MCK_QMR702", "", "N505", "");
            eat = eat || SetIndIfResultContains(2, "ADL001", "FND008", "", "N506", "");
            eat = eat || SetIndIfResultContains(2, "ADL001", "FND100247", "", "ORL001", "");
            eat = eat || SetIndIfResultContains(2, "ADL001", "MCK_QMR702", "", "EAT001", "");
            eat = eat || SetIndIfResultContains(2, "ADL001", "FND101054", "", "N34", "");
            eat = eat || SetIndIfResultContains(2, "ADL001", "FND110725", "", "N34", "");
            eat = eat || SetIndIfResultContains(2, "NUT001", "FND296", "", "SWAL02", "");
            eat = eat || SetIndIfResultContains(2, "NUT001", "FND105559", "", "SWAL01", "");

            toi = toi || SetIndIfResultContains(2, "ADL001", "MCK_QMR703", "", "BWL001", "");
            toi = toi || SetIndIfResultContains(2, "ADL001", "FND046", "", "BWL001", "");
            toi = toi || SetIndIfResultContains(2, "ADL001", "FND010", "", "BWL001", "");
            toi = toi || SetIndIfResultContains(2, "ADL001", "FND012", "", "BWL001", "");
            toi = toi || SetIndIfResultContains(2, "ADL001", "MCK_QMR703", "", "N504", "");
            toi = toi || SetIndIfResultContains(2, "ADL001", "FND101058", "", "N34", "");

            mob = mob || SetIndIfResultContains(2, "NUR004", "FND101590", "", "N507", "");
            mob = mob || SetIndIfResultContains(2, "MSC001", "FND208", "", "MUSCACT", "");
            mob = mob || SetIndIfResultContains(2, "MSC001", "FND208", "", "N420", "");

            int num_adl_items = (grm ? 1 : 0) + (bth ? 1 : 0) + (eat ? 1 : 0) + (toi ? 1 : 0) + (mob ? 1 : 0);
            if (num_adl_items >= 3) SetInd(3, "At least 3 categories of ADL partial were found = " + num_adl_items);

            SetIndIfResultContains(1, "ADL001", "MCK_QMR704", "", "N502", "");
            SetIndIfResultContains(1, "ADL001", "MCK_QMR699", "", "N503", "");
            SetIndIfResultContains(1, "ADL001", "FND005", "", "N504", "");
            SetIndIfResultContains(1, "ADL001", "FND761", "", "N505", "");
            SetIndIfResultContains(1, "ADL001", "FND005", "", "N506", "");
            SetIndIfResultContains(1, "ADL001", "MCK_QMR705", "", "N502", "");
            SetIndIfResultContains(1, "ADL001", "FND006", "", "N504", "");
            SetIndIfResultContains(1, "ADL001", "FND006", "", "N505", "");
            SetIndIfResultContains(1, "ADL001", "FND006", "", "N506", "");
            SetIndIfResultContains(1, "NUR004", "FND109909", "", "N507", "");
            SetIndIfResultContains(1, "NUR004", "MCK_QMR685", "", "N507", "");
            SetIndIfResultContains(1, "ADL001", "FND005", "", "ORL001", "");
            SetIndIfResultContains(1, "ADL001", "MCK_QMR699", "", "GRM001", "");
            SetIndIfResultContains(1, "ADL001", "MCK_QMR704", "", "BTH001", "");
            SetIndIfResultContains(1, "ADL001", "FND761", "", "EAT001", "");
            SetIndIfResultContains(1, "ADL001", "FND005", "", "BWL001", "");
            SetIndIfResultContains(1, "ADL001", "MCK_QMR705", "", "BTH001", "");
            SetIndIfResultContains(1, "ADL001", "FND006", "", "EAT001", "");
            SetIndIfResultContains(1, "ADL001", "FND011", "", "BWL001", "");
            SetIndIfResultContains(1, "MSC001", "FND207", "", "MUSCACT", "");
            SetIndIfResultContains(1, "MSC001", "FND207", "", "N420", "");



            //int num_adl_items = (feed2 ? 1 : 0) + (bath2 ? 1 : 0) + (toil2 ? 1 : 0) + (mob2 ? 1 : 0);
            //if (num_adl_items >= 4)
            //{
            //    SetInd(3, "Four or more Partial ADL found among Feeding,Bathing,Toileting,Mobility");
            //}


            //int num3_items = (feed3 ? 1 : 0) + (bath3 ? 1 : 0) + (toil3 ? 1 : 0) + (mob3 ? 1 : 0) + (dress3 ? 1 : 0);
            //if (num3_items >= 4)
            //{
            //    SetInd(3, "Four or more Partial ADL found among Feeding,Bathing,Toileting,Mobility,Dressing");
            //}


            if (!_inds[2].is_checked && !_inds[3].is_checked && !_inds[4].is_checked)
            {
                SetInd(4, "No ADL documentation found - defaulting to ADL Complete");
            }


        }


        private void Check_5()
        {
            string reslist;
            string found_what;
            
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("5. ADL Rehab");
            Program.VerboseAudit("---------------");

            //if (IsRehab()) SetInd(5, "Unit is RHB");
            //SetIndIfResultContains(5, "", "303419250", "", "", "Bowel Retraining");
            //SetIndIfResultContains(5, "", "303419250", "", "", "Bladder Retraining");
            //SetIndIfResultContains(5, "", "5613457", "", "", "ADL Rehabilitative");
            //SetIndIfResultContains(5, "", "5613416", "", "", "ADL Rehabilitative");
            //SetIndIfResultContains(5, "", "5613420", "", "", "ADL Rehabilitative");
            //SetIndIfResultContains(5, "", "5613422", "", "", "ADL Rehabilitative");
            //SetIndIfResultContains(5, "", "216958660", "", "", "ADL Rehabilitative");
            //SetIndIfResultContains(5, "", "303419038", "", "", "Nursing");

        }


        private void Check_6_7()
        {    
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("6. ADL 2-3 Caregivers");
            Program.VerboseAudit("7. ADL 4 or more Caregivers");
            Program.VerboseAudit("---------------");

            if (Exists("", "FND111561", "", "", ""))
            {
                SetIndIfResultContains(7, "ADL001", "FND007", "", "N502", "");
                SetIndIfResultContains(7, "ADL001", "FND007", "", "N503", "");
                SetIndIfResultContains(7, "ADL001", "FND007", "", "N504", "");
                SetIndIfResultContains(7, "ADL001", "FND007", "", "N506", "");
                SetIndIfResultContains(7, "ADL001", "MCK_QMR707", "", "N502", "");
                SetIndIfResultContains(7, "ADL001", "FND014", "", "N504", "");
                SetIndIfResultContains(7, "ADL001", "FND007", "", "ORL001", "");
                SetIndIfResultContains(7, "ADL001", "FND007", "", "GRM001", "");
                SetIndIfResultContains(7, "ADL001", "MCK_QMR707", "", "BTH001", "");
                SetIndIfResultContains(7, "ADL001", "FND007", "", "BWL001", "");
                SetIndIfResultContains(7, "ADL001", "FND014", "", "BWL001", "");
                SetIndIfResultContains(7, "ADL001", "MCK_QMR703", "", "N504", "");
                SetIndIfResultContains(7, "NUR004", "FND101590", "", "N507", "");
                SetIndIfResultContains(7, "ADL001", "FND008", "", "N502", "");
                SetIndIfResultContains(7, "ADL001", "FND102744", "", "BTH001", "");
                SetIndIfResultContains(7, "ADL001", "MCK_QMR703", "", "BWL001", "");
                SetIndIfResultContains(7, "ADL001", "FND010", "", "BWL001", "");
                SetIndIfResultContains(7, "ADL001", "FND012", "", "BWL001", "");
                SetIndIfResultContains(7, "ADL001", "FND101058", "", "N34", "");
                SetIndIfResultContains(7, "ADL001", "FND101053", "", "N34", "");
                SetIndIfResultContains(7, "MSC001", "FND208", "", "MUSCACT", "");
                SetIndIfResultContains(7, "MSC001", "FND208", "", "N420", "");
                SetIndIfResultContains(7, "NUR004", "FND101590", "", "N507", "");
                SetIndIfResultContains(7, "MSC001", "FND209", "", "N420", "");
            }

            if (Exists("", "FND111560", "", "", ""))
            {
                SetIndIfResultContains(6, "ADL001", "FND007", "", "GRM001", "");
                SetIndIfResultContains(6, "ADL001", "MCK_QMR707", "", "BTH001", "");
                SetIndIfResultContains(6, "ADL001", "FND007", "", "BWL001", "");
                SetIndIfResultContains(6, "ADL001", "FND014", "", "BWL001", "");
                SetIndIfResultContains(6, "ADL001", "MCK_QMR703", "", "N504", "");
                SetIndIfResultContains(6, "NUR004", "FND101590", "", "N507", "");
                SetIndIfResultContains(6, "ADL001", "FND008", "", "N502", "");
                SetIndIfResultContains(6, "ADL001", "FND102744", "", "BTH001", "");
                SetIndIfResultContains(6, "ADL001", "MCK_QMR703", "", "BWL001", "");
                SetIndIfResultContains(6, "ADL001", "FND010", "", "BWL001", "");
                SetIndIfResultContains(6, "ADL001", "FND012", "", "BWL001", "");
                SetIndIfResultContains(6, "ADL001", "FND101058", "", "N34", "");
                SetIndIfResultContains(6, "ADL001", "FND101053", "", "N34", "");
                SetIndIfResultContains(6, "MSC001", "FND208", "", "MUSCACT", "");
                SetIndIfResultContains(6, "MSC001", "FND208", "", "N420", "");
                SetIndIfResultContains(6, "MSC001", "FND209", "", "N420", "");
            }
            
        }

        private void Check_8()
        {
            string reslist;
            string other_str;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("8. Communication");
            Program.VerboseAudit("---------------");

            bool coma = false;

            if (coma)
            {
                Program.Audit("Patient is comatose; Communication not applicable.");
                return;
            }
            SetIndIfResultContains(8, "ADM001", "FND104507", "", "OPNU22", "");
            SetIndIfResultContains(8, "", "BLIND", "", "", "");
            SetIndIfResultContains(8, "", "DEAD", "", "", "");
            SetIndIfResultContains(8, "", "HEARING_IMPAIRMENT", "", "", "");
            SetIndIfResultContains(8, "", "INTERPRETER_REQUIRED", "", "", "");
            SetIndIfResultContains(8, "", "MUTE", "", "", "");
            SetIndIfResultContains(8, "", "SPEECH_IMPAIRMENT", "", "", "");
            SetIndIfResultContains(8, "", "UNABLE_TO_COMM", "", "", "");
            SetIndIfResultContains(8, "", "UNABLE_TO_INFER", "", "", "");
            SetIndIfResultContains(8, "", "UNABLE_TO_READ", "", "", "");
            SetIndIfResultContains(8, "", "UNABLE_TO_SPEAK", "", "", "");
            SetIndIfResultContains(8, "", "UNABLE_TO_VERBAL", "", "", "");
            SetIndIfResultContains(8, "", "UNABLE_TO_WRITE", "", "", "");
            SetIndIfResultContains(8, "", "VISION_IMPAIRMENT", "", "", "");
            SetIndIfResultContains(8, "EDU001", "FND10034", "", "EDU011", "");
            SetIndIfResultContains(8, "EDU001", "FND10032", "", "EDU011", "");
            SetIndIfResultContains(8, "EDU001", "FND377", "", "EDU011", "");
            SetIndIfResultContains(8, "EDU001", "FND590", "", "EDU011", "");
            SetIndIfResultContains(8, "EDU001", "FND10034", "", "EDU011", "");
            SetIndIfResultContains(8, "EDU001", "FND10032", "", "EDU011", "");
            SetIndIfResultContains(8, "EDU001", "FND377", "", "EDU011", "");
            SetIndIfResultContains(8, "EDU001", "FND590", "", "EDU011", "");
            SetIndIfResultContains(8, "ADM001", "FND1349", "", "LAN001", "");
            SetIndIfResultContains(8, "NUR001", "MCK_QMR718", "", "SPH001", "");
            SetIndIfResultContains(8, "NUR001", "MCK_QMR673", "", "SPH001", "");
            SetIndIfResultContains(8, "NUR001", "FND10190", "", "SPH001", "");
            SetIndIfResultContains(8, "NUR001", "FND254", "", "SPH001", "");
            SetIndIfResultContains(8, "NUR001", "FND100209", "", "SPH001", "");
            SetIndIfResultContains(8, "NUR001", "MCK_QMR718", "", "SPH001", "");
            SetIndIfResultContains(8, "NUR001", "MCK_QMR673", "", "SPH001", "");
            SetIndIfResultContains(8, "NUR001", "FND254", "", "SPH001", "");
            SetIndIfResultContains(8, "NUR001", "FND10190", "", "SPH001", "");
            SetIndIfResultContains(8, "EKOS", "MCK_QMR718", "", "N483", "");
            SetIndIfResultContains(8, "EKOS", "MCK_QMR673", "", "N483", "");
            SetIndIfResultContains(8, "EKOS", "FND10190", "", "N483", "");
            SetIndIfResultContains(8, "EKOS", "FND254", "", "N483", "");
            SetIndIfResultContains(8, "EKOS", "FND100209", "", "N483", "");
            SetIndIfResultContains(8, "ENT001", "MCK_QMR711", "", "EAR010", "");
            SetIndIfResultContains(8, "ENT001", "FND1059", "", "EAR011", "");
            SetIndIfResultContains(8, "ENT001", "FND1058", "", "EAR100", "");
            SetIndIfResultContains(8, "ENT001", "FND1058", "", "EAR101", "");
            SetIndIfResultContains(8, "ENT001", "MCK_QMR711", "", "EAR010", "");
            SetIndIfResultContains(8, "ENT001", "MCK_QMR711", "", "EAR011", "");
            SetIndIfResultContains(8, "ENT001", "FND1058", "", "EAR100", "");
            SetIndIfResultContains(8, "ENT001", "FND1058", "", "EAR101", "");
            SetIndIfResultContains(8, "RSP001", "FND452", "", "TRC004", "");
            SetIndIfResultContains(8, "RSP001", "FND100168", "", "TRC004", "");
            SetIndIfResultContains(8, "RSP001", "FND983", "", "TRC004", "");
            SetIndIfResultContains(8, "RSP001", "FND100166", "", "TRC004", "");
            SetIndIfResultContains(8, "RSP001", "FND100167", "", "TRC004", "");
            SetIndIfResultContains(8, "RSP001", "FND813", "", "TRC004", "");
            SetIndIfResultContains(8, "RSP001", "FND100179", "", "N512", "");
            SetIndIfResultContains(8, "RSP001", "FND100178", "", "N512", "");
            SetIndIfResultContains(8, "RSP001", "FND100496", "", "N512", "");
            SetIndIfResultContains(8, "RSP001", "FND100253", "", "N512", "");
            SetIndIfResultContains(8, "RSP001", "FND100177", "", "N512", "");
            SetIndIfResultContains(8, "RSP001", "FND100495", "", "N512", "");
            SetIndIfResultContains(8, "RSP001", "FND452", "", "TRC004", "");
            SetIndIfResultContains(8, "RSP001", "FND100168", "", "TRC004", "");
            SetIndIfResultContains(8, "RSP001", "FND983", "", "TRC004", "");
            SetIndIfResultContains(8, "RSP001", "FND100166", "", "TRC004", "");
            SetIndIfResultContains(8, "RSP001", "FND100167", "", "TRC004", "");
            SetIndIfResultContains(8, "RSP001", "FND813", "", "TRC004", "");
            SetIndIfResultContains(8, "RSP003", "FND100224", "", "N37", "");
            SetIndIfResultContains(8, "RSP003", "FND100223", "", "N37", "");
            SetIndIfResultContains(8, "RSP003", "FND100494", "", "N37", "");

        }

        private void Check_9()
        {
            string reslist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("9. Cognitive Support");
            Program.VerboseAudit("---------------");
//FIELD_NAME like 'n62%' and code='mck_total'
            //SetIndIfResultContains(9, "NUR001", "MCK_QMR665", "", "N446", "");
            //SetIndIfResultContains(9, "NUR001", "MCK_QMR656", "", "N446", "");
            //SetIndIfResultContains(9, "NUR001", "FND236", "", "N446", "");
            //SetIndIfResultContains(9, "NUR001", "FND100145", "", "N446", "");
            SetIndIfResultContains(9, "EKOS", "FND236", "", "N447", "");
            SetIndIfResultContains(9, "EKOS", "FND100145", "", "N447", "");
            SetIndIfResultContains(9, "PSY005", "MCK_QMR671", "", "MEM001", "");
            SetIndIfResultContains(9, "PSY005", "MCK_QMR672", "", "MEM001", "");
            SetIndIfResultContains(9, "NUR001", "MCK_QMR665", "", "RFR001", "");
            SetIndIfResultContains(9, "NUR001", "FND236", "", "RFR001", "");
            SetIndIfResultContains(9, "NUR001", "FND100132", "", "NEUROACT", "");
            SetIndIfResultContains(9, "NUR001", "FND100133", "", "NEUROACT", "");
            SetIndIfResultContains(9, "EKOS", "FND100132", "", "N408", "");
            SetIndIfResultContains(9, "EKOS", "FND100133", "", "N408", "");
            //SetIndIfResultContains(9, "EKOS", "FND236", "", "N447", "");
            //SetIndIfResultContains(9, "WITHDR", "FND105979", "", "N75", "");
            //SetIndIfResultContains(9, "WITHDR", "FND105798", "", "N75", "");
            //SetIndIfResultContains(9, "WITHDR", "FND105739", "", "ATIVAN1", "");
            //SetIndIfResultContains(9, "WITHDR", "FND105743", "", "ATIVAN1", "");
            //SetIndIfResultContains(9, "WITHDR", "FND105746", "", "ATIVAN1", "");
            //SetIndIfResultContains(9, "WITHDR", "FND105747", "", "ATIVAN1", "");
            //SetIndIfResultContains(9, "WITHDR", "FND105755", "", "ATIVAN1", "");
            //SetIndIfResultContains(9, "NARC1", "FND105807", "", "N74", "");
            //SetIndIfResultContains(9, "NARC1", "FND105743", "", "NARCW", "");
            SetIndIfResultContains(9, "WITHDR", "FND111961", "", "N623", "acute panic");
            SetIndIfResultContains(9, "WITHDR", "FND111965", "", "N623", "paces back and forth");
            SetIndIfResultContains(9, "WITHDR", "FND105746", "", "N623", "severe hallucination");
            SetIndIfResultContains(9, "WITHDR", "FND111983", "", "N623", "severe hallucination");
            SetIndIfResultContains(9, "WITHDR", "FND111984", "", "N623", "continuous hallucination");

            SetIndIfResultContains(9, "WITHDR", "FND111998", "", "N623", "severe hallucination");
            SetIndIfResultContains(9, "WITHDR", "FND111975", "", "N623", "severe hallucination");
            SetIndIfResultContains(9, "WITHDR", "FND111965", "", "N623", "paces back and forth");
            SetIndIfResultContains(9, "WITHDR", "FND111992", "", "N623", "continuous hallucination");
            SetIndIfResultContains(9, "WITHDR", "FND105755", "", "N623", "disoriented");
            SetIndIfResultContains(9, "NARC1", "FND111946", "", "N625", "patient is so irritable");

            int v = 0;
            v = GetMaxValue("WITHDR", "MCK_TOTAL", "", "N623", "");
            if (v >= 10)
            {
                SetInd(9, "CIWA Assessment Score=" + v);
                SetInd(12, "CIWA Assessment Score=" + v);
            }
            v = GetMaxValue("NARC1", "MCK_TOTAL", "", "N624", "");
            if (v >= 20)
            {
                SetInd(9, "COWS Assessment Score=" + v);
                SetInd(12, "COWS Assessment Score=" + v);
            }


        }

        private void Check_10_11()
        {
            string reslist, found_what;
            int ct;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("10. Behavior/Emotional Management");
            Program.VerboseAudit("11. Behavior/Emotional Mgmt - q 1 Hour");
            Program.VerboseAudit("---------------");

            if (ResultContains("PSY001", "FND620", "", "EMT001", "Depressed"))
                SetIndIfResultContains(10, "", "FND111563", "", "", "Patient needs extra encouragement");
            SetIndIfResultContains(10, "PSY001", "FND623", "", "EMT001", "");
            SetIndIfResultContains(10, "PSY001", "FND711", "", "EMT001", "");
            SetIndIfResultContains(10, "PSY001", "FND622", "", "EMT001", "");
            SetIndIfResultContains(10, "PSY001", "FND879", "", "EMT001", "");
            SetIndIfResultContains(10, "PSY001", "FND626", "", "EMT001", "");
            SetIndIfResultContains(10, "NUR001", "FND711", "", "N446", "");
            SetIndIfResultContains(10, "EKOS", "FND711", "", "N447", "");
            SetIndIfResultContains(10, "NUR001", "FND711", "", "N446", "");
            SetIndIfResultContains(10, "PSY001", "FND623", "", "EMT001", "");
            SetIndIfResultContains(10, "PSY001", "FND711", "", "EMT001", "");
            SetIndIfResultContains(10, "PSY001", "FND622", "", "EMT001", "");
            SetIndIfResultContains(10, "PSY001", "FND879", "", "EMT001", "");
            SetIndIfResultContains(10, "PSY001", "FND626", "", "EMT001", "");
            SetIndIfResultContains(10, "PSY001", "FND109931", "", "DEPSCREEN", "");
            SetIndIfResultContains(10, "PSY001", "FND109931", "", "DEPSCREEN", "");

            var buckets = new List<gBucket>();
            SetBucketSize(60);
            AddBuckets(buckets, "", "FND111563", "", "", "Patient needs extra encouragement");
            AddBuckets(buckets, "PSY001", "FND623", "", "EMT001", "");
            AddBuckets(buckets, "PSY001", "FND711", "", "EMT001", "");
            AddBuckets(buckets, "PSY001", "FND622", "", "EMT001", "");
            AddBuckets(buckets, "PSY001", "FND879", "", "EMT001", "");
            AddBuckets(buckets, "PSY001", "FND626", "", "EMT001", "");
            AddBuckets(buckets, "NUR001", "FND711", "", "N446", "");
            AddBuckets(buckets, "EKOS", "FND711", "", "N447", "");
            AddBuckets(buckets, "NUR001", "FND711", "", "N446", "");
            AddBuckets(buckets, "PSY001", "FND623", "", "EMT001", "");
            AddBuckets(buckets, "PSY001", "FND711", "", "EMT001", "");
            AddBuckets(buckets, "PSY001", "FND622", "", "EMT001", "");
            AddBuckets(buckets, "PSY001", "FND879", "", "EMT001", "");
            AddBuckets(buckets, "PSY001", "FND626", "", "EMT001", "");
            AddBuckets(buckets, "PSY001", "FND109931", "", "DEPSCREEN", "");
            AddBuckets(buckets, "PSY001", "FND109931", "", "DEPSCREEN", "");
            ct = CountBuckets(buckets);
            if (IsQ1Hour(ct)) SetInd(11, "Item count = " + ct);
   
        }

        // (this isn't really a Q1h count -- it is just a count) Make this a q1hr count
        private int ReturnQ1HrCount(string code_list, string result_list)
        {
            var buckets = new List<gBucket>();
            SetBucketSize(60);
            AddBuckets(buckets, "", code_list, "", "", result_list);
            return CountBuckets(buckets);
        }

        private bool IsQ1Hour(int count)
        {
            return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q1H);
        }
        private bool IsQ2Hour(int count)
        {
            return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q2H);
        }
        private bool IsQ30Min(int count)
        {
            return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q30M);
        }


        // Use this if counting safety buckets
        //private void CheckSafety(int count, string desc)
        //{
        //    if (_inds[13].is_checked) return;             //skip if highest already checked
        //    if (count == 0) return;
        //    switch (FreqForCount(_pat.los_hours, count))
        //    {
        //    case Frequencies.Q30M:
        //        SetInd(13, desc + " q30min");
        //        break;
        //    case Frequencies.Q1H:
        //    case Frequencies.Q2H:
        //        SetInd(12, desc + " q2h");
        //        break;
        //    default:
        //        Program.VerboseAudit(desc + ": " + count + " in " + Math.Round(_pat.los_hours) + " hours is not enough");
        //        break;
        //    }
        //}

        private void Check_12_13()
        {
            bool suppress13=false;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("12. Safety Management - q 2 Hours");
            Program.VerboseAudit("13. Safety Management - q 30 Minutes");
            Program.VerboseAudit("---------------");

            SetBucketSize(60);
            var buckets = new List<gBucket>();

            if (Exists("", "FND106812,FND106813", "", "", ""))
            {
                AddBuckets(buckets, "", "FND111267,FND111268,FND111827", "", "", "");
            }

            AddBuckets(buckets, "", "FND109305,FND101444", "", "", "");

            if (Exists("", "FND112049,FND112056", "", "", ""))
            {
                AddBuckets(buckets, "", "FND112050,FND112051,FND112052,FND112053,FND112054,FND112055", "", "", "");
            }

            if (Exists("", "FND106816,FND106817", "", "", ""))
            {
                AddBuckets(buckets, "", "FND111267,FND111268,FND111827", "", "", "");
            }

            int ct = CountBuckets(buckets);
            if (IsQ30Min(ct)) 
            {
                SetInd(13, "Item count = " + ct);
                return;
            }
            if (IsQ2Hour(ct)) SetInd(12, "Item count = " + ct);


            buckets = new List<gBucket>();
            AddBuckets(buckets, "", "FND109943,FND109944,FND109945,FND109946,FND109947", "", "SITTER1", "");
            AddBuckets(buckets, "", "FND106751", "", "SUI11", "");
            ct = CountBuckets(buckets);
            if (IsQ30Min(ct))
            {
                SetInd(13, "Sitter freq count = " + ct);
                return;
            }

        }

        private void Check_14()
        {    
            string reslist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("14. Isolation");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(14, "ADL001", "FND10484", "", "ISO0101", "");
            SetIndIfResultContains(14, "ADL001", "FND106924", "", "ISO0101", "");
            SetIndIfResultContains(14, "ADL001", "FND10482", "", "ISO0101", "");
            SetIndIfResultContains(14, "ADL001", "FND106491", "", "ISO0101", "");
            SetIndIfResultContains(14, "ADL001", "FND10484", "", "ISO0101", "");
            SetIndIfResultContains(14, "ADL001", "FND106924", "", "ISO0101", "");
            SetIndIfResultContains(14, "ADL001", "FND10482", "", "ISO0101", "");
            SetIndIfResultContains(14, "ADL001", "FND106491", "", "ISO0101", "");
            SetIndIfResultContains(14, "", "C-DIFF", "", "", "");
            SetIndIfResultContains(14, "", "ESBL", "", "", "");
            SetIndIfResultContains(14, "", "ESBL1", "", "", "");
            SetIndIfResultContains(14, "", "INFLUENZA", "", "", "");
            SetIndIfResultContains(14, "", EXACT_MATCH_PREFIX+"M", "", "", "");
            SetIndIfResultContains(14, "", EXACT_MATCH_PREFIX + "M1", "", "", "");
            SetIndIfResultContains(14, "", EXACT_MATCH_PREFIX + "M2", "", "", "");
            SetIndIfResultContains(14, "", EXACT_MATCH_PREFIX + "M4", "", "", "");
            SetIndIfResultContains(14, "", "MRSA1 - Contact", "", "", "");
            SetIndIfResultContains(14, "", "MRSA2 - Contact", "", "", "");
            SetIndIfResultContains(14, "", "MRSA4 - Contact", "", "", "");
            SetIndIfResultContains(14, "", "MDRO", "", "", "");
            SetIndIfResultContains(14, "", EXACT_MATCH_PREFIX + "T", "", "", "");
            SetIndIfResultContains(14, "", EXACT_MATCH_PREFIX + "V", "", "", "");
            SetIndIfResultContains(14, "", EXACT_MATCH_PREFIX + "V1", "", "", "");
            SetIndIfResultContains(14, "", EXACT_MATCH_PREFIX + "V2", "", "", "");
            SetIndIfResultContains(14, "", "PRECONT", "", "", "");
            SetIndIfResultContains(14, "", "PREDP", "", "", "");
            SetIndIfResultContains(14, "", "PRERV", "", "", "");
            SetIndIfResultContains(14, "", "PREAB", "", "", "");

        }

        private void CheckAssessment(int count, string desc)
        {
            if (_inds[18].is_checked) return;          //skip if highest already checked
            if (count == 0) return;                    //skip if none
            
            // This should work the same as the original code:
            switch (FreqForCount(_pat.los_hours, count)) {
                case Frequencies.Q30M:
                    SetInd(18, desc);
                    break;
                case Frequencies.Q1H:
                    SetInd(17, desc);
                    break;
                case Frequencies.Q2H:
                    SetInd(16, desc);
                    break;
                case Frequencies.Q4H:
                    SetInd(15, desc);
                    break;
                default:
                    break;
            }

        }

        private void ShowBuckets(List<gBucket> buckets, int start, int last)
        {
            string s = "";
            Program.VerboseAudit("start bucket=" + start + "  last bucket=" + last);
            foreach (var e in buckets)
            {
                if ((e.bucket >= start) && (e.bucket <= last))
                {
                    s = "(" + e.bucket + "," + e.code + "," + e.evdt.ToString() + ")";
                    Program.VerboseAudit("VS item:=" + s);
                }
            }
        }


        private void Check_15_16_17_18()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("15. Assessment q4h");
            Program.VerboseAudit("16. Assessment q2h");
            Program.VerboseAudit("17. Assessment q1h");
            Program.VerboseAudit("18. Assessment q30min");
            Program.VerboseAudit("---------------");

            CountAssessments(30);               // always do q30 buckets--but use different freq tables for icu vs non-icu.

        }

        private bool IsRehab()
        {
            switch (_pat.unit_name)
            {
                case "RHB":
                    return true;
                default:
                    return false;
            }
        }


        //private bool IsICU()
        //{
        //    switch (_pat.unit_name)
        //    {
        //        case "ICU":
        //        case "3BR":
        //        case "CTIC":
        //        case "NIC":
        //        case "PCU":
        //        case "TICU":
        //        case "CCB":
        //        case "RICU":
        //        case "CCUS":
        //            return true;
        //        default:
        //            return false;
        //    }
        //}

        //private bool IsTele()
        //{
        //    switch (_pat.unit_name)
        //    {
        //        case "4EST":
        //        case "2NW":
        //        case "3AE":
        //        case "5MEH":
        //        case "5NW":
        //        case "6MEH":
        //        case "6NW":
        //        case "7MEH":
        //        case "B4W":
        //        case "B5S":
        //        case "B5W":
        //        case "2NOB":
        //        case "CPLX":
        //        case "TELMS":
        //        case "MAN4":
        //            return true;
        //        default:
        //            return false;
        //    }
        //}


        private void CountAssessments(int bucket_size)
        {
            int ct;
            string codelist;
            string reslist;
            List<gBucket> buckets;

            SetBucketSize(bucket_size);

            buckets = new List<gBucket>();
            codelist = "BP,Pulse,O2SAT,Resp,ARTERIAL,CVP,Temp";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "VS count=" + ct);
//            ShowBuckets(buckets);
            if (_inds[18].is_checked) return;

            buckets = new List<gBucket>();
            codelist = "FND229,FND782,FND737,FND256,FND229,FND782,FND737,FND256,FND229";
            codelist += ",FND782,FND737,FND256,FND229,FND782,FND737,FND256,FND229,FND782";
            codelist += ",FND737,FND256,FND229,FND782,FND737,FND256,FND229,FND782,FND737";
            codelist += ",FND256,FND229,FND782,FND737,FND256,FND1330,FND1000029,FND10462";
            codelist += ",FND10461,FND1334,FND10465,FND103304,FND1333,FND100032,FND100034";
            codelist += ",FND100033,FND100244,FND100248,FND100245,FND1331,FND10459,FND10460";
            codelist += ",FND10467,FND103303,FND109527,FND10468,FND101050,FND103306,FND038";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Cardio count=" + ct);
            if (_inds[18].is_checked) return;

            buckets = new List<gBucket>();
            codelist = "MCK_QMR666,MCK_QMR667,MCK_QMR668,MCK_QMR669,MCK_QMR718,MCK_QMR719,MCK_QMR673";
            codelist += ",FND254,FND10190,FND100209,FND100162,FND572,FND571,FND336,FND695,FND482";
            codelist += ",FND100148,FND100149,FND100151,FND100152,FND100150,FND100153,FND222";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Neuro/EKOS count=" + ct);
            if (_inds[18].is_checked) return;

            buckets = new List<gBucket>();
            codelist = "FND780,FND106668,FND100012,FND278,FND102706,FND102707,FND706,FND256,FND020";
            codelist += ",FND019,FND109645,FND109646,FND109647,FND109648,FND109649,FND109650,FND109651";
            codelist += ",FND109652,FND109653,FND109654,FND100224,FND100223,FND100494,FND038,FND753";
            codelist += ",FND754,FND756,FND510,FND10332,FND10333,FND100496,FND100495,FND481,FND482";
            codelist += ",FND709,FND260,FND708,FND274,FND275,FND267,FND265,FND646,FND710,FND647";
            codelist += ",FND743,FND742,FND750,FND017,FND009,FND100093,FND100094";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Respiratory/Vent count=" + ct);
            if (_inds[18].is_checked) return;

            buckets = new List<gBucket>();
            codelist = "Oral1,IVFluid1,IVFluid2,IVFluid3,Tube1,TPN,PRBC,Gtube,FTFlush,IVFluid29,IVFluid21";
            codelist += ",IVFluid27,IVFluid19,IVFluid30,IVFluid26,IVFluid28,IVFluid20,IVFluid25,IVFluid23";
            codelist += ",IVFluid9,IVFluid8,IVFluid17,Other3,IVFluid18,Other2,IVFluid15,Other1,IVFluid12";
            codelist += ",IVFluid6,IVFluid7,Voided,Ucatheter,Ctube1,Emesis1,ColosBM,Drain1,BM,Urine,Other5";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "I&O count=" + ct);
            if (_inds[18].is_checked) return;


            SetIndIfResultContains(18, "", "RXORDER", "Dopamine", "", "");
            SetIndIfResultContains(18, "", "RXORDER", "Phenylephrine", "", "");
            SetIndIfResultContains(18, "", "RXORDER", "Dobutamine", "", "");
            SetIndIfResultContains(18, "", "RXORDER", "Norepenephrine", "", "");
            SetIndIfResultContains(18, "", "RXORDER", "Vasopressin", "", "");
            SetIndIfResultContains(18, "", "RXORDER", "Epinephrine", "", "");
            SetIndIfResultContains(18, "", "RXORDER", "Nitroglycerin", "", "");
            SetIndIfResultContains(18, "", "RXORDER", "Nitroprusside", "", "");
            SetIndIfResultContains(18, "", "RXORDER", "Amiodarone", "", "");
            SetIndIfResultContains(18, "", "RXORDER", "Diltiazem", "", "");
            SetIndIfResultContains(18, "", "RXORDER", "Labetalol", "", "");
            SetIndIfResultContains(18, "", "RXORDER", "propofol", "", "");
            SetIndIfResultContains(18, "", "RXORDER", "precedex", "", "");
            

//Dopamine 800mg/500mL D5W
//Phenylephrine 60mg/250mL
//Dobutamine 500mg/250mL D5W
//Norepenephrine 8mg/250mL D5W
//Vasopressin 35units/250mL
//Epinephrine 4mg/250mL D5W
//Nitroglycerin 50mg/250mL D5W
//Nitroprusside 50mg/250mL D5W
//Amiodarone 150mg/100mL D5W
//Diltiazem 100mg/100mL D5W
//Labetalol 400mg/300mL D5W
//propofol
//precedex


        }

        //=====================================================================
        // Group like assessments into buckets of time
        // Each bucket counts as one assessment in the frequency count

        private void SetBucketSize(int minutes)
        {
            _bucket_size = minutes;
        }

        //Add to the list of bucket numbers (redundant buckets are fine)
        private void AddBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field)
        {
            AddBuckets(bucket_list, cat, code_list, desc, field, "");
        }
        private void AddBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field, string result_list)
        {
            // Assign (virtual) bucket numbers to each assessment.  
            // Every x minutes after that is a new bucket.  Bucket zero = pull_start time.

            // get the chart items for the assessments
            var query = StartNewQuery();
            query = AndItemFilter(query, cat, code_list, desc, field, result_list);

            // This step is needed for those who want to count the # within a bucket, like Shands #24.
            // This will have no effect on those that count buckets.
            //var query2 = (from item in query select new { item.EVENT_DATETIME }).Distinct();
            var query2 = (from item in query select new { item.EVENT_DATETIME, item.CODE });

            // figure out what buckets the events belong to
            //var query3 = from item in query2
            //             select new {
            //                bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size)
            //             };
            //// Add to the list
            //foreach (var item in query3) {
            //    bucket_list.Add(item.bucket);
            //}

            var query3 = from item in query2
                         select new
                         {
                             bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                             code = item.CODE,
                             evdt = item.EVENT_DATETIME
                         };
            // Add to the list
            foreach (var item in query3)
            {
                var b = new gBucket();
                b.bucket = item.bucket;
                b.code = item.code;
                b.evdt = item.evdt;
                bucket_list.Add(b);
            }

            // This is just for auditing: Say what we are looking for and how many were found
            if (!String.IsNullOrEmpty(result_list)) {
                // print each word and if it was found or not
                int i = CountResultContains(cat, code_list, desc, field, result_list);
            } else {
                // print how many were found
                Program.VerboseAudit(Describe(cat, code_list, desc, field, ""));
            }
        }

        private int CountBuckets(List<gBucket> bucket_list)       // bucket list: (ha ha)
        {
            //int result = bucket_list.Distinct().Count();
            //if (result > 0) Program.VerboseAudit(result + " unique");
            //return result;
            int x = -99;
            int result = 0;
            //int result = bucket_list.Distinct().Count();
            var query = from b in bucket_list
                        orderby b.bucket ascending
                        select b;
            foreach (var b in query)
            {
                if (x != b.bucket)
                {
                    result++;
                    x = b.bucket;
                }
            }
            if (result > 0) Program.VerboseAudit(result + " unique");
            return result;
        }


        private void Check_19()
        {
            string codelist;
            int ct;
            string found_what;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("19. Vascular Access Site Mgt q1 Hour");
            Program.VerboseAudit("---------------");

            codelist = "IVFLUID,TPN,PRBC,Other3,Other2";
            if (Exists("", codelist, "", "", ""))
            {
                SetIndIfResultContains(19, "PSY001", "FND109946", "", "SITTER1", "");
                SetIndIfResultContains(19, "PSY005", "FND109946", "", "SITTER1", "");
                SetIndIfResultContains(19, "NUR001", "MCK_QMR665", "", "N446", "");
                SetIndIfResultContains(19, "NUR001", "FND236", "", "N446", "");
                SetIndIfResultContains(19, "EKOS", "FND236", "", "N447", "");
                SetIndIfResultContains(19, "NUR001", "MCK_QMR665", "", "RFR001", "");
                SetIndIfResultContains(19, "NUR001", "FND236", "", "RFR001", "");
                SetIndIfResultContains(19, "EKOS", "FND236", "", "N447", "");
            }

        }

        private void Check_20()
        {
            string medlist;
            
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("20. Medication Activity >= 20 minutes");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(20, "", "RBC1", "", "", "");
            SetIndIfResultContains(20, "", "RBC2", "", "", "");
            SetIndIfResultContains(20, "", "RBC3", "", "", "");
            SetIndIfResultContains(20, "", "RBC4", "", "", "");
            SetIndIfResultContains(20, "", "PLAT1", "", "", "");
            SetIndIfResultContains(20, "", "PLAT2", "", "", "");
            SetIndIfResultContains(20, "", "PLAT3", "", "", "");
            SetIndIfResultContains(20, "", "PLAT4", "", "", "");
            SetIndIfResultContains(20, "", "PLAS1", "", "", "");
            SetIndIfResultContains(20, "", "PLAS2", "", "", "");
            SetIndIfResultContains(20, "", "PLAS3", "", "", "");
            SetIndIfResultContains(20, "", "PLAS4", "", "", "");
            SetIndIfResultContains(20, "", "CRYO1", "", "", "");
            SetIndIfResultContains(20, "", "CRYO2", "", "", "");
            SetIndIfResultContains(20, "", "CRYO3", "", "", "");

            //exclude any IV named Sodium Chloride with 3ml + q8h. 
            //2017-08-29 08:18:00.000
            //Sodium Chloride 0.9%; 
            //Dose=3 ML;Route=IV;Freq=Q8H
            SetIndIfResultContains(20, "", "RXORDER", "Insulin", "", "Route=IV");
            SetIndIfResultContains(20, "", "RXORDER", "Heparin", "", "Route=IV");

            int ct = CountResultContains("", "RXORDER", "", "", "Route=ORAL");
            if (ct >= 8) SetInd(20, "Oral med count=" + ct);

            CountIVs();

        }

        private void CountIVs()
        {
            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = AndCodeInList(query, "RXORDER");
            query = AndResultInList(query, "Route=IV");
            query = query.Where(e => !e.RESULT.Contains("freq=q8h"));
            query = query.Where(e => !e.DESCRIPTION.Contains("sodium chloride"));
            query = query.Where(e => !e.DESCRIPTION.Contains("flush"));
            int ct = query.Count();
            if (ct >= 4)
                SetInd(20, "IV med count=" + ct);
        }

        private void Check_21_22()
        {
            string codelist;
            
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("21. Wound/Injury Mgmt");
            Program.VerboseAudit("22. Wound/Injury Mgmt >= 30 Minutes");
            Program.VerboseAudit("---------------");

            if (Exists("SKINAS", "FND110713", "", "", ""))
            {
                SetIndIfResultContains(21, "", "FND110585", "", "", "");
                SetIndIfResultContains(21, "", "FND110869", "", "", "");
                SetIndIfResultContains(21, "", "FND110586", "", "", "");
                SetIndIfResultContains(21, "", "FND110843", "", "", "");
                SetIndIfResultContains(21, "", "FND110589", "", "", "");
                SetIndIfResultContains(21, "", "FND110590", "", "", "");

                SetIndIfResultContains(22, "", "FND110591", "", "", "");
            }

            if (Exists("SKINAS", "FND106059", "", "", ""))
            {
                SetIndIfResultContains(21, "", "FND110586", "", "", "");

                SetIndIfResultContains(22, "", "FND110591", "", "", "");
            }

            if (_inds[21].is_checked || _inds[22].is_checked) return;

            SetIndIfResultContains(21, "GTOSTA", "FND100119", "", "N35", "");
            SetIndIfResultContains(21, "GTOSTA", "FND100120", "", "N35", "");
            SetIndIfResultContains(21, "GTOSTA", "FND804", "", "N35", "");
            SetIndIfResultContains(21, "GTOSTA", "FND10049", "", "N35", "");
            SetIndIfResultContains(21, "GTOSTA", "FND100121", "", "N35", "");
            SetIndIfResultContains(21, "GTOSTA", "FND100122", "", "N35", "");
            SetIndIfResultContains(21, "GNU001", "FND189", "", "MAL001", "");
            SetIndIfResultContains(21, "GNU001", "FND189", "", "FEM001", "");
            SetIndIfResultContains(21, "GNU001", "FND189", "", "N166", "");
            SetIndIfResultContains(21, "RESP001", "FND100177", "", "N512", "");
            SetIndIfResultContains(21, "RSP001", "FND102583", "", "CHSTTUB", "");
            SetIndIfResultContains(21, "RSP001", "FND102584", "", "CHSTTUB", "");
            SetIndIfResultContains(21, "RSP001", "FND102585", "", "CHSTTUB", "");
            SetIndIfResultContains(21, "RSP001", "FND102586", "", "CHSTTUB", "");
            SetIndIfResultContains(21, "RSP001", "FND904", "", "CHE001", "");
            SetIndIfResultContains(21, "RSP001", "FND905", "", "CHE001", "");
            SetIndIfResultContains(21, "GST001", "FND201", "", "EME001", "");
            SetIndIfResultContains(21, "GST001", "FND100095", "", "EME001", "");
            SetIndIfResultContains(21, "GST001", "FND201", "", "EME001", "");
            SetIndIfResultContains(21, "GST001", "FND100095", "", "EME001", "");
            SetIndIfResultContains(21, "GST001", "FND341", "", "158089", "");
            SetIndIfResultContains(21, "GTOSTA", "FND201", "", "158089", "");
            SetIndIfResultContains(21, "GST001", "FND341", "", "158089", "");
            SetIndIfResultContains(21, "GTOSTA", "FND201", "", "158089", "");
            SetIndIfResultContains(21, "", "IV_ACT_DRESSING_CHG", "", "", "");
            SetIndIfResultContains(21, "", "ADAPTER", "", "", "");
            SetIndIfResultContains(21, "", "CENTRAL_LINE", "", "", "");
            SetIndIfResultContains(21, "", "GROSHONG", "", "", "");
            SetIndIfResultContains(21, "", "HICKMAN", "", "", "");
            SetIndIfResultContains(21, "", "MIDLINE", "", "", "");
            SetIndIfResultContains(21, "", "NON_TUNNELED", "", "", "");
            SetIndIfResultContains(21, "", "PICC", "", "", "");
            SetIndIfResultContains(21, "", "POWER_PORT", "", "", "");
            SetIndIfResultContains(21, "GNU001", "FND108391", "", "FOLE11", "");
            SetIndIfResultContains(21, "GNU001", "FND108392", "", "FOLE11", "");
            SetIndIfResultContains(21, "", "INICBI", "", "", "");
            SetIndIfResultContains(21, "", "MAINCBI", "", "", "");


            //SetIndIfResultContains(22, "SKINAS", "FND110591", "", "SKINAS12", "");
            //SetIndIfResultContains(22, "SKINAS", "FND110591", "", "SKINAS13", "");
            //SetIndIfResultContains(22, "SKINAS", "FND110591", "", "SKINAS15", "");
            //SetIndIfResultContains(22, "SKINAS", "FND110591", "", "SKINAS11", "");
            //SetIndIfResultContains(22, "SKINAS", "FND106059,FND110591", "", "SKINAS30", "");
            //SetIndIfResultContains(22, "SKINAS", "FND106059,FND110591", "", "SKINAS30", "");
            //SetIndIfResultContains(22, "SKINAS", "FND106059,FND110591", "", "SKINAS32", "");
            //SetIndIfResultContains(22, "SKINAS", "FND106059,FND110591", "", "SKINAS31", "");
            //SetIndIfResultContains(22, "SKINAS", "FND106059,FND110591", "", "SKINAS29", "");
            //SetIndIfResultContains(22, "SKINAS", "FND106059,FND110591", "", "SKINAS28", "");
            //SetIndIfResultContains(22, "SKINAS", "FND106059,FND110591", "", "SKINAS32", "");
            //SetIndIfResultContains(22, "SKINAS", "FND106059,FND110591", "", "SKINAS28", "");
            //SetIndIfResultContains(22, "SKINAS", "FND106059,FND110591", "", "SKINAS31", "");
            //SetIndIfResultContains(22, "SKINAS", "FND106059,FND110591", "", "SKINAS29", "");
            //SetIndIfResultContains(22, "SKINAS", "FND110591", "", "SKINAS14", "");

        }



        // Use this if you can total the education time
        //private void CheckEducation(int total)
        //{
        //    if (_inds[23].is_checked) return;             //skip if already checked

        //    if (total >= 60) {
        //        SetInd(23, "education >= 60 min");
        //    }
        //}

        int EducMins(string nursecode, string timecode)
        {
            string res;
            int val = 0;
            if (ResultContains("", nursecode, "", "", "Nursing"))
            {
                if (GetResult("", timecode, "", "", out res))
                {
                    if (res.Left(1).IsNumeric())
                    {
                        val += (int)res.Val();
                    }
                }
            }
            return val;
        }

        private void Check_23()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("23. Healthcare Mgmt Education >= 1 Hour");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(23, "", "FND100950,FND829,FND015,FND0825,FND100405,FND10073", "", "", "");

        }

        private void Check_24()
        {
            string codelist,s;
            int startbucket = -1, endbucket = -1;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("24. 1 to 1 Physiological Interv. >= 2 Hours");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(24, "", "HYPBLK", "", "", "");
            SetIndIfResultContains(24, "", "HYPICE", "", "", "");
            SetIndIfResultContains(24, "", "HYPRAI", "", "", "");
            SetIndIfResultContains(24, "", "VITSIGH", "", "", "");
            SetIndIfResultContains(24, "", "IHCP", "", "", "");
            SetIndIfResultContains(24, "", "IHRWP", "", "", "");

//9 sets of vital signs in 2 hours (cardiovascular VS)
//pulse
//resp
//bp
//o2
//temp
            codelist = "BP,Pulse,O2SAT,Resp,Temp";

            SetBucketSize(60);                                  // set one hour buckets
            var buckets = new List<gBucket>();                      // list of bucket numbers (for each match)
            AddBuckets(buckets, "", codelist, "", "", "");
            // Instead of counting the number of buckets to get the frequency,
            // we want to see if any two hours in a row had 9 or more assessments in the bucket.
            // Make a query that returns each bucket number and its item count.
            //var query = buckets.GroupBy(x => x.bucket)                 // group by the hour (x is an int)
            //                   .Select(g => new { Hour = g.Key, Count = g.Count() })
            //                   .OrderBy(e => e.Hour);
            var query = buckets.GroupBy(x => x.bucket)                 // group by the hour (x is an int)
                               .Select(g => new { Hour = g.Key, Count = g.Select(e => e.evdt).Distinct().Count() })
                               .OrderBy(e => e.Hour);
            var arr = query.ToArray();

            s = "";
            foreach (var e in arr) {
                s += ",(" + e.Hour + "," + e.Count + ")";
            }
            if (s.Left(1) == ",") s = s.Substring(1);
            Program.VerboseAudit("VS hourly counts for 1:1 Phys Interv: " + s);

            for (int i = 0; (i < arr.GetUpperBound(0)); i++) {
                // Are these two hours in a row? (beware missing hours)
                if (arr[i].Hour == arr[i + 1].Hour - 1) {
                    // Add these two consecutive hours
                    if (arr[i].Count + arr[i+1].Count >= 9) {
                        startbucket = arr[i].Hour;
                        endbucket = arr[i + 1].Hour;
                        SetInd(24, "9 or more VS in 2 hours.");
                        break;
                    }
                }
            }

            if (endbucket >= 0) ShowBuckets(buckets,startbucket,endbucket);

        }


        private void AtLeastOneADL()
        {
            Program.Audit("---------------");
            if (!(_inds[1].is_checked || _inds[2].is_checked || _inds[3].is_checked || _inds[4].is_checked)) {
                // Make "#2 ADL - Assist" the default.  (90% of patients)
                SetInd(2, "Defaulting to ADL Partial due to lack of documentation.");
            }
        }

        private void HighestIndicatorInEachGroupWins()
        {
            int i, g;
            bool highest_is_on;
            string ind_list;

            Program.Audit("Select highest indicator in each group");
            
            g = 0;
            highest_is_on = false;
            // Go from bottom up - highest (lowest) indicator in each group wins
            for (i = MAX_INDS; (i >= 1); i--) {
                if (_inds[i].radio_group > 0) {
                    if (_inds[i].radio_group != g) {
                        //this is a new group
                        g = _inds[i].radio_group;
                        highest_is_on = _inds[i].is_checked;
                    } else {
                        //same group
                        if (highest_is_on) {
                            _inds[i].is_checked = false;             //uncheck a lower number
                        } else {
                            highest_is_on = _inds[i].is_checked;     //save this one
                        }
                    }
                }
            } // next i
            
            //Echo the indicators for an Audit (no classification will be saved)
            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++) {
                if (_inds[i].is_checked) ind_list += "," + i;
            } // next i

            Program.Audit("Final list = " + ind_list.Substring(1));
            Program.Audit("---------------");
        }


        private void CheckProcs()
        {
            //CheckProc_1();
            CheckProc_2();
            CheckProc_3();
            //CheckProc_4();
            //CheckProc_5();
            CheckProc_67();
            //CheckProc_8();
            CheckProc_91011();

        }

        //private void DoProc(int pnum, string code, string result, string sttime_code, string fntime_code)
        //{
        //    double mins = 0;
        //    string found_res;
        //    DateTime evdt = DateTime.MinValue;
        //    DateTime enddt = DateTime.MinValue;

        //    var query = StartNewQuery(SearchDepth.SearchDefault);
        //    query = AndCodeInList(query, code);
        //    query = AndResultInList(query, result);
        //    query = query.OrderBy(e => e.EVENT_DATETIME);
        //    foreach (var ci in query)
        //    { //pick the earliest one if multiple
        //        if (evdt == DateTime.MinValue) evdt = ci.EVENT_DATETIME;
        //    }

        //    DateTime st_time = GetResultTime(sttime_code, evdt);
        //    DateTime en_time = GetResultTime(fntime_code, evdt);

        //    if (st_time == DateTime.MinValue || en_time == DateTime.MinValue || st_time>=en_time)
        //    {
        //        Program.Audit("Data for Activity " + pnum + " has invalid datetime(s):  start="+st_time.ToString()+"  finish="+en_time.ToString());
        //        return;
        //    }

        //    if (ProcExistsInDB(pnum, st_time, en_time))
        //    {
        //        Program.Audit("Activity " + pnum+ ": already exists");
        //    }
        //    else
        //    {
        //        if (!QueuedProcOverlaps(pnum, st_time, en_time))
        //        {
        //            var proc = new proc_data();
        //            proc.procedure_number = pnum;
        //            proc.start = evdt;
        //            proc.finish = enddt;
        //            _procs.Add(proc);
        //            Program.Audit("Activity " + pnum + ": Found " + code + " between " + evdt + " and " + enddt);
        //        }
        //    }

        //}


        DateTime GetResultTime(string code, DateTime evdt, int offset)
        { //offset is the starting pos of yyyyMMddHHmm in the result string
            string res = "";
            string s;
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = AndCodeInList(query, code);
            query = query.Where(e => (e.EVENT_DATETIME == evdt));
            if (query.Count() == 0)
            {
                Program.VerboseAudit("Time not found for DTA=" + code + " at time="+evdt.ToString());
                return DateTime.MinValue;
            }
            foreach (var ci in query)
            {
                res = ci.RESULT;
            }
            if (res.Length >= 12)
            {
                s = res.Substring(offset, 12);
                Program.Audit("time string=" + s);
            }
            else
                return DateTime.MinValue;
            
            if (s.Length == 12)
            {
                string format = "yyyyMMddHHmm";
                return DateTime.ParseExact(s, format, System.Globalization.CultureInfo.InvariantCulture);
            }
            else
                return DateTime.MinValue;

        }

        private bool ProcExistsInDB(int pnum, DateTime startdt, DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && (proc.PROCEDURE_DATETIME == startdt)
                            && (proc.DEPARTURE_DATETIME == enddt)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new { proc.PROCEDURE_EVENT_ID };
            return (query.Count() > 0);
        }

        private bool QueuedProcOverlaps(int pnum, DateTime startdt, DateTime enddt)
        {
            bool overlap = false;

            proc_data[] pary = _procs.ToArray();
            for (int i = 0; i <= pary.GetUpperBound(0); i++)
            {
                if (pary[i].procedure_number == pnum)
                {
                    //overlap if   p.start between stardt and enddt
                    //             p.finish between startdt and enddt
                    //             startdt >= p.start and enddt <= p.finish
                    if ((pary[i].start >= startdt) && (pary[i].start < enddt) ||
                        (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
                        (startdt >= pary[i].start) && (enddt <= pary[i].finish))
                    {
                        overlap = true;
                    }

                    if ((pary[i].start >= startdt) && (pary[i].start < enddt) && (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
                        (pary[i].start < startdt) && (pary[i].finish > startdt) && (pary[i].finish < enddt)
                        )
                    {
                        // the proc in the list is completely contained in startdt/endt pair
                        // change this proc to be the startdt/enddt
                        // overlap is still true, but the list item will be updated with the encompassing times
                        //Program.Audit("Procedure " + pnum + ": " + startdt + " - " + enddt + " will trump " + pary[i].start + " - " + pary[i].finish);
                        pary[i].start = startdt;
                        pary[i].finish = enddt;
                    }
                }
            }
            _procs = pary.ToList();
            return overlap;
        }


        //private void CheckProc_1()
        //{
        //    Program.VerboseAudit("---------------");
        //    Program.VerboseAudit("A1. 1-1 safety observation by RN");
        //    Program.VerboseAudit("---------------");
        //}

        private void CheckProc_2()
        {
            //string nowstr;
            //string toddtstr;
            //string yesdtstr;
            //string timea ="";
            //DateTime timea_startdt, timea_enddt;
            //DateTime timeb_startdt, timeb_enddt;
            //DateTime nowdt = _pat.pull_finish;              // "now" is pull time
            //DateTime yesdt;
            
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A2. 1-1 safety observation by non-RN");
            Program.VerboseAudit("---------------");

//305006247   Sitter
//305006262   Yes
//  SetIndIfResultContains(21, "GTOSTA", "FND100119", "", "SITTER1", "");
//FND109943
//FND109944
//FND109945
//FND109946
//FND109947
            DateTime stdt, endt, prevstdt=DateTime.MinValue;

            var query = StartNewQuery(SearchDepth.SearchPullRange);
            query = query.Where(e => ((e.CODE.ToLower() == "fnd109943") || (e.CODE.ToLower() == "fnd109944") || (e.CODE.ToLower() == "fnd109945") || (e.CODE.ToLower() == "fnd109946") || (e.CODE.ToLower() == "fnd109947")));
            query = query.Where(e => (e.FIELD_NAME.ToLower().Trim() == "sitter1"));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            if (query.Count() > 0)
            {
                foreach (var item in query)
                {

                    Program.VerboseAudit("before getstartcode=" + item.EVENT_DATETIME.ToString());
                    GetSitterStartEnd(item.EVENT_DATETIME, out stdt, out endt);
                    Program.VerboseAudit("code=" + item.CODE + " evtime=" + item.EVENT_DATETIME.ToString() + " start="+stdt.ToString() + " end="+endt.ToString());

                    if (stdt != prevstdt)
                    {
                        prevstdt = stdt;
                        if (!ProcExists(2, stdt, endt))
                        {
                            var proc = new proc_data();
                            proc.procedure_number = 2;
                            proc.start = stdt;
                            proc.finish = endt;
                            _procs.Add(proc);
                            Program.Audit("Activity 2: Found Sitter between " + stdt.ToString() + " and " + endt.ToString());
                        }
                    }
                }
            }
        }

        private void GetSitterStartEnd(DateTime evdt, out DateTime stdt, out DateTime endt)
        {
            stdt = DateTime.MinValue;
            endt = DateTime.MinValue;
            Program.VerboseAudit("Hour=" + evdt.Hour.ToString() + " evdt.Date=" + evdt.Date.ToString() + " evdt.Date.Add(2)=" + evdt.Date.AddHours(2).ToString());
            if ((evdt.Hour >= 7) && (evdt.Hour < 15)) // hour is 7..14
            {
                stdt = evdt.Date.AddHours(7);
                endt = evdt.Date.AddHours(15);
            }
            if ((evdt.Hour >= 15) && (evdt.Hour < 23)) // hour is 15..22
            {
                stdt = evdt.Date.AddHours(15);
                endt = evdt.Date.AddHours(23);
            }
            if ((evdt.Hour == 23))
            {
                stdt = evdt.Date.AddHours(23);
                endt = evdt.Date.AddHours(30);
            }
            if  (evdt.Hour < 7) // hour is 0..6
            {
                stdt = evdt.Date.AddHours(-1);
                endt = evdt.Date.AddHours(7);
            }
        }

        private bool ProcExists(int pnum, DateTime startdt, DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && (proc.PROCEDURE_DATETIME >= startdt)
                            && (proc.DEPARTURE_DATETIME <= enddt)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new {proc.PROCEDURE_EVENT_ID};
            return (query.Count() > 0);
        }

        private void CheckProc_3()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A3. Off unit accompanied by RN");
            Program.VerboseAudit("---------------");

            CheckProc_3q();
        }

        //private void CheckProc_4()
        //{
        //    Program.VerboseAudit("---------------");
        //    Program.VerboseAudit("A4. Off unit accompanied by non-RN");
        //    Program.VerboseAudit("---------------");

        //    CheckProc_34(4, "PCA/PCT");
        //}

        private void CheckProc_3q()
        {
            string codelist = "";
            codelist = "CAB,CAP,C8300,C8288,C8296,C8290";
            codelist += ",CBX,C7010,C7015,CCS,CCHW,CDSSCT";
            codelist += ",CCAB,CCAP,CCYAS,C4280,CFBW,CFNAWG";
            codelist += ",C3301,CGDR,CGUID,CHCSWO,CHSNWO,CHDW,CLLW,CLUW";
            codelist += ",CLEXAM,CBXLIV,CTLCRYO,CTLRFA,CLSWOW,CLSP,CNKW,CNDBX,CNTUBE,CORW";
            codelist += ",CPW,C6065,C3200,C6050,C8370,C5615,CRLW";
            codelist += ",CRUW,CSL,CSNW,CTBW,C6045,CTSWOW";
            codelist += ",CTSP,CTSPW";
            codelist += "," + EXACT_MATCH_PREFIX + "MA";
            codelist += ",MAP,MAC,MLAL,MLAR";
            codelist += ",MBBO,MBMNC,MHC," + EXACT_MATCH_PREFIX + "MH,MBCACN,MBCNC,MBB,MCA,MCS";
            codelist += ",4732010,MCC," + EXACT_MATCH_PREFIX + "MC,MUEL,MUER";
            codelist += ",MENT,MFC,MLNFL,MLNFR";
            codelist += ",MUFL,MUFR,MUBB,MUHL,MUHR";
            codelist += ",MBAN,MBCV,MBCA,MBAVNC";
            codelist += ",MLHL,MLHR";
            codelist += ",MUNUL,MUNUR,MLKL,MLKR";
            codelist += ",MLJ,MLN";
            codelist += ",MLC," + EXACT_MATCH_PREFIX + "ML,MRCP,MOC,MOFN,MPC,MPCO," + EXACT_MATCH_PREFIX + "MP";
            codelist += ",MUSL,MUSR";
            codelist += ",MSTNC,MLNTL,MLNTR";
            codelist += ",MTSC," + EXACT_MATCH_PREFIX + "MTS,MTMJ,MUB,MUC," + EXACT_MATCH_PREFIX + "MU,MUN,MUWL";
            codelist += ",MUWR,MBA,MBV";

            DateTime evdt = DateTime.MinValue;
            DateTime prevevdt = DateTime.MinValue;
            
            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = AndCodeInList(query, codelist);
            query = query.Where(e => e.UNIT_ID != -1);
            query = query.Where(e => e.ORDER_CONTROL == "nw");
            query = query.OrderBy(e => e.EVENT_DATETIME);
            if (query.Count() == 0) return;
            foreach (var ci in query)
            { //starting from earliest and no duplicate start times
                if (ci.EVENT_DATETIME != prevevdt)
                {
                    prevevdt = ci.EVENT_DATETIME;
                    if (VerifyOrderStatus(ci.ORDER_ID, ci.EVENT_DATETIME))
                    {
                        if (!ProcExists(3, ci.EVENT_DATETIME, ci.EVENT_DATETIME.AddHours(1)))
                        {
                            var proc = new proc_data();
                            proc.procedure_number = 3;
                            proc.start = ci.EVENT_DATETIME;
                            proc.finish = ci.EVENT_DATETIME.AddHours(1);
                            _procs.Add(proc);
                            Program.Audit("Activity 3: Found Off-unit activity order = " + ci.CODE + " descript=" + ci.DESCRIPTION+ " at: " + ci.EVENT_DATETIME.ToString());
                        }
                    }
                }
            }

            //DateTime st_time = GetResultTime(sttime_code, evdt, 0);
            //DateTime en_time = GetResultTime(fntime_code, evdt, 0);

            //if (st_time == DateTime.MinValue || en_time == DateTime.MinValue)
            //{
            //    return;
            //}
            //if (st_time >= en_time)
            //{
            //    Program.Audit("Data for Activity " + pnum + " has invalid datetime(s):  start=" + st_time.ToString() + "  finish=" + en_time.ToString());
            //    return;
            //}

            //if (ProcExistsInDB(pnum, st_time, en_time))
            //{
            //    Program.Audit("Activity " + pnum + ": already exists");
            //}
            //else
            //{
            //    if (!QueuedProcOverlaps(pnum, st_time, en_time))
            //    {
            //        var proc = new proc_data();
            //        proc.procedure_number = pnum;
            //        proc.start = st_time;
            //        proc.finish = en_time;
            //        _procs.Add(proc);
            //        Program.Audit("Activity " + pnum + ": Found " + code + "  start=" + st_time.ToString() + "  end=" + en_time.ToString());
            //    }
            //}
        }

        private bool VerifyOrderStatus(string ordid, DateTime evdt)
        {
            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = query.Where(e => e.ORDER_ID == ordid.ToLower());
            query = query.Where(e => e.EVENT_DATETIME == evdt);
            query = query.Where(e => e.ORDER_CONTROL == "sc");
            query = query.Where(e => e.UNIT_ID != -1);
            return (query.Count() > 0);
        }

        private void CheckProc_5()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A5. Patient/family education by RN");
            Program.VerboseAudit("---------------");
            //1375032589  educ start time in result
            //1375032603  educ end time in result
            string res="";
            string res2="";
            int pnum = 5;
            string code = "1375032519";
            DateTime evdt = DateTime.MinValue;
            string sttime_code = "1375032589";
            string fntime_code = "1375032603";
            DateTime st_time, en_time;

            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = AndCodeInList(query, sttime_code);
            query = query.OrderBy(e => e.EVENT_DATETIME);
            if (query.Count() == 0) return;
            foreach (var ci in query)
            { //pick the earliest one if multiple
                if (evdt == DateTime.MinValue)
                {
                    evdt = ci.EVENT_DATETIME;
                    res = ci.RESULT;
                }
            }
            Program.Audit("Education start time=" + res);

            var query2 = StartNewQuery(SearchDepth.SearchDefault);
            query2 = AndCodeInList(query2, fntime_code);
            query2 = query2.Where(e => e.EVENT_DATETIME == evdt);
            query2 = query2.OrderBy(e => e.EVENT_DATETIME);
            if (query2.Count() == 0) return;
            foreach (var ci in query2)
            { 
                res2 = ci.RESULT;
            }
            Program.Audit("Education end time=" + res2);

            string format = "yyyyMMddHHmm";
            st_time = DateTime.ParseExact(res, format, System.Globalization.CultureInfo.InvariantCulture);
            en_time = DateTime.ParseExact(res2, format, System.Globalization.CultureInfo.InvariantCulture);
            if (ProcExistsInDB(pnum, st_time, en_time))
            {
                Program.Audit("Activity " + pnum + ": already exists");
            }
            else
            {
                if (!QueuedProcOverlaps(pnum, st_time, en_time))
                {
                    var proc = new proc_data();
                    proc.procedure_number = pnum;
                    proc.start = st_time;
                    proc.finish = en_time;
                    _procs.Add(proc);
                    Program.Audit("Activity " + pnum + ": Found " + code + "  start=" + st_time.ToString() + "  end=" + en_time.ToString());
                }
            }

        }

        private void CheckProc_67()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A6. Extensive wound management by RN");
            Program.VerboseAudit("A7. Extensive wound management by non-RN");
            Program.VerboseAudit("---------------");

//307297004
//27719009

            int pnum = 6;
            string codelist = "307297004,27719009";
            DateTime evdt = DateTime.MinValue;
            DateTime prevevdt = DateTime.MinValue;

            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = AndCodeInList(query, codelist);
            query = query.Where(e => e.UNIT_ID != -1);
            query = query.OrderBy(e => e.EVENT_DATETIME);
            if (query.Count() == 0) return;
            foreach (var ci in query)
            { //pick the earliest one if multiple
                if (ci.EVENT_DATETIME != prevevdt)
                {
                    prevevdt = ci.EVENT_DATETIME;
                    if (!ProcExists(6, ci.EVENT_DATETIME, ci.EVENT_DATETIME.AddHours(1)))
                    {
                        var proc = new proc_data();
                        proc.procedure_number = 6;
                        proc.start = ci.EVENT_DATETIME;
                        proc.finish = ci.EVENT_DATETIME.AddHours(1);
                        _procs.Add(proc);
                        Program.Audit("Activity 6: Found Extensive Wound Mgt activity = " + ci.CODE + " descript=" + ci.DESCRIPTION + " at: " + ci.EVENT_DATETIME.ToString());
                    }
                }
            }

            //int cgtype = GetCaregiverType("1375033639", evdt); // 0=none 1=RN  2=nonRN
            //if (cgtype == 0) return;
            //if (cgtype == 1) pnum = 6;
            //if (cgtype == 1) pnum = 7;

            //DateTime st_time = GetResultTime(sttime_code, evdt, 2);
            //DateTime en_time = GetResultTime(fntime_code, evdt, 2);

            //if (st_time == DateTime.MinValue || en_time == DateTime.MinValue)
            //{
            //    return;
            //}
            //if (st_time >= en_time)
            //{
            //    Program.Audit("Data for Activity " + pnum + " has invalid datetime(s):  start=" + st_time.ToString() + "  finish=" + en_time.ToString());
            //    return;
            //}

            //if (ProcExistsInDB(pnum, st_time, en_time))
            //{
            //    Program.Audit("Activity " + pnum + ": already exists");
            //}
            //else
            //{
            //    if (!QueuedProcOverlaps(pnum, st_time, en_time))
            //    {
            //        var proc = new proc_data();
            //        proc.procedure_number = pnum;
            //        proc.start = st_time;
            //        proc.finish = en_time;
            //        _procs.Add(proc);
            //        Program.Audit("Activity " + pnum + ": Found " + code + "  start=" + st_time.ToString() + "  end=" + en_time.ToString());
            //    }
            //}


        }

        private int GetCaregiverType(string code, DateTime evdt)
        {
            //1375033639		1:1 RN   or Non-RN
            string r = "";

            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = AndCodeInList(query, code);
            query = query.Where(e => e.EVENT_DATETIME == evdt);
            query = query.OrderBy(e => e.EVENT_DATETIME);
            if (query.Count() == 0) return 0;
            foreach (var ci in query)
            { //pick the earliest one if multiple
                if (r == "") r = ci.RESULT;
            }
            if (r.Trim() == "1:1 RN") return 1;
            if (r.Trim() == "Non-RN") return 2;
            return 0;

        }

        private void CheckProc_8()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A8. Coordination of care by RN");
            Program.VerboseAudit("---------------");
            
            int pnum = 8;
            string code = "1375032519";
            DateTime evdt = DateTime.MinValue;
            string sttime_code = "1375032547";
            string fntime_code = "1375032561";

            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = AndCodeInList(query, code);
            query = AndResultInList(query, "Coordination of care");
            query = query.OrderBy(e => e.EVENT_DATETIME);
            if (query.Count() == 0) return;
            foreach (var ci in query)
            { //pick the earliest one if multiple
                if (evdt == DateTime.MinValue) evdt = ci.EVENT_DATETIME;
            }

            DateTime st_time = GetResultTime(sttime_code, evdt, 2);
            DateTime en_time = GetResultTime(fntime_code, evdt, 2);

            if (st_time == DateTime.MinValue || en_time == DateTime.MinValue)
            {
                return;
            }
            if (st_time >= en_time)
            {
                Program.Audit("Data for Activity " + pnum + " has invalid datetime(s):  start=" + st_time.ToString() + "  finish=" + en_time.ToString());
                return;
            }

            if (ProcExistsInDB(pnum, st_time, en_time))
            {
                Program.Audit("Activity " + pnum + ": already exists");
            }
            else
            {
                if (!QueuedProcOverlaps(pnum, st_time, en_time))
                {
                    var proc = new proc_data();
                    proc.procedure_number = pnum;
                    proc.start = st_time;
                    proc.finish = en_time;
                    _procs.Add(proc);
                    Program.Audit("Activity " + pnum + ": Found " + code + "  start=" + st_time.ToString() + "  end=" + en_time.ToString());
                }
            }

        }

        private void CheckProc_91011()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A9 1:1 RN at bedside");
            Program.VerboseAudit("A10 1:1 Non-RN at bedside");
            Program.VerboseAudit("A11 2:1 RN at bedside");
            Program.VerboseAudit("---------------");

//9:
//FND104183
//FND105595
//FND102559
//FND103806
//FND105596
//FND105597
//FND102551
//FND101806

//11:
//ROTOPRO
//IHCP
//IHRWP
//Primary: FND104183 Secondary: FND111717
//Primary: FND105595 Secondary: FND111717
//Primary: FND102559 Secondary: FND111717
//Primary: FND103806 Secondary: FND111717
//Primary: FND105596 Secondary: FND111717
//Primary: FND105597 Secondary: FND111717
//Primary: FND102551 Secondary: FND111717
//Primary: FND101806 Secondary: FND111717

            string codelist = "FND104183,FND105595,FND102559,FND103806,FND105596,FND105597,FND102551,FND101806";
            string codelist2 = ",ROTOPRO,IHCP,IHRWP";

            int pnum = 9;
            DateTime evdt = DateTime.MinValue;
            DateTime prevevdt = DateTime.MinValue;

            //is there an activity?
            //if so, then get evdt of activity
            //see if there are st_code and en_code at evdt
            //see if there is stf_code at evdt
            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = AndCodeInList(query, codelist+codelist2);
            query = query.Where(e => e.UNIT_ID != -1);
            query = query.OrderBy(e => e.EVENT_DATETIME);
            if (query.Count() == 0) return;
            foreach (var ci in query)
            { //pick the earliest one if multiple
                if (ci.EVENT_DATETIME != prevevdt)
                {
                    prevevdt = ci.EVENT_DATETIME;
                    if (codelist2.ToLower().IndexOf(ci.CODE.ToLower()) != -1)
                    {
                        //add proc 11
                        AddProc911(11, ci.CODE, ci.EVENT_DATETIME);
                    }
                    else // in codelist1
                    {
                        if (ExistsSecondary(ci.EVENT_DATETIME, "FND111717"))
                        {
                            //add proc 11
                            AddProc911(11, ci.CODE, ci.EVENT_DATETIME);
                        }
                        else
                        {
                            //add proc 9
                            AddProc911(9, ci.CODE, ci.EVENT_DATETIME);
                        }
                    }

                }
            }

            //    if (evdt == DateTime.MinValue) 
            //    {
            //        evdt = ci.EVENT_DATETIME;
            //        activity = ci.RESULT;
            //    }
            //}
            //Program.Audit("Activity found = " + activity + " at event time=" + evdt.ToString());

            //DateTime st_time = GetResultTime(st_code, evdt, 2);
            //DateTime en_time = GetResultTime(en_code, evdt, 2);
            //Program.Audit("Start time=" + st_time.ToString() + "  End time="+en_time.ToString());

            //evdt = DateTime.MinValue;
            //var query2 = StartNewQuery(SearchDepth.SearchDefault);
            //query2 = AndCodeInList(query2, stf_code);
            //query2 = query2.Where(e => e.EVENT_DATETIME == evdt);
            //if (query2.Count() == 0) return;
            //foreach (var ci in query)
            //{ //pick the earliest one if multiple
            //    if (evdt == DateTime.MinValue)
            //    {
            //        evdt = ci.EVENT_DATETIME;
            //        staff = ci.RESULT;
            //    }
            //}
            //Program.Audit("Staff involved=" + staff);

            //if (staff == "1:1 RN") pnum = 9;
            //if (staff == "Non-RN") pnum = 10;
            //if (staff == "2:1 RN") pnum = 11;

            //if (ProcExistsInDB(pnum, st_time, en_time))
            //{
            //    Program.Audit("Activity " + pnum + ": already exists");
            //}
            //else
            //{
            //    if (!QueuedProcOverlaps(pnum, st_time, en_time))
            //    {
            //        var proc = new proc_data();
            //        proc.procedure_number = pnum;
            //        proc.start = st_time;
            //        proc.finish = en_time;
            //        _procs.Add(proc);
            //        Program.Audit("Activity " + pnum + ": Found " + code + "  start=" + st_time.ToString() + "  end=" + en_time.ToString());
            //    }
            //}
        }

        private bool ExistsSecondary(DateTime evdt, string code)
        {
            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = AndCodeInList(query, code);
            query = query.Where(e => e.EVENT_DATETIME == evdt);
            return (query.Count() > 0);
        }

        private void AddProc911(int pnum, string code, DateTime evdt)
        {
            if (!ProcExists(pnum, evdt, evdt.AddHours(1)))
            {
                var proc = new proc_data();
                proc.procedure_number = pnum;
                proc.start = evdt;
                proc.finish = evdt.AddHours(1);
                _procs.Add(proc);
                Program.Audit("Activity " + pnum + ": Found Activity at bedside = " + code + " at: " + evdt.ToString());
            }
        }

        private void CheckOutcomes()
        {
            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = AndItemFilter(query, "", "A_MHPostFalltime", "", "", "");
            foreach (var ch in query)
            {
                var outc = new proc_data();
                outc.procedure_number = 1;
                outc.start = ch.EVENT_DATETIME;
                _outcomes.Add(outc);
                Program.Audit("Outcomes 1: Found A_MHPostFalltime at " + outc.start);
            }
        }
        

        private const string DATETIME_FORMAT = "yyyyMMddHHmm";              // ISO Date/Time w/o seconds

        private void OutputClass()
        {
            string outstr, ind_list, desc, str_pull_dt;
            int i, tc_event_id;

            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         212 242 304
            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1
            //1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901 3456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
            //1       |10 5            |                |                |        |10023121779         |ALEXANDER                       |GLORIA                          |                                |0508    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289530  |           |201403030400                                                                      |NNYNNNNYYNNYNNNYNNNYYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//1       |10 5            |                |                |        |10024544908         |ARMSTRONG                       |AARON                           |                                |0535    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289531  |           |201403030400                                                                      |NYNNYNNNNNNYNNNYNNNNYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//|0547    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289532  |           |201403030400                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//|0547    |A   |201403030700|                |201403030700    |18  |C|    |3040|1440|18289532  |           |201403030700                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN

            if (Program.g_is_test)
                tc_event_id = 9999;
            else
                tc_event_id = PFSUtility.NextGID();                         //get a unique id for this class
            str_pull_dt = _pat.pull_finish.ToString(DATETIME_FORMAT);

            outstr = _pat.facilty_code.FixedWidth(8);                       //(facility code)
            outstr += "|" + _pat.unit_name.FixedWidth(16);
            outstr += "|" + "".FixedWidth(16);                               //(unit code)
            outstr += "|" + "".FixedWidth(16);                               //(area code)
            outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
            outstr += "|" + _pat.acct.FixedWidth(20);
            outstr += "|" + _pat.last_name.FixedWidth(32);
            outstr += "|" + _pat.first_name.FixedWidth(32);
            outstr += "|" + _pat.middle_name.FixedWidth(32);
            outstr += "|" + _pat.room.FixedWidth(8);
            outstr += "|" + _pat.bed.FixedWidth(4);
          outstr += "|" + str_pull_dt.FixedWidth(12);                      //class datetime (could change)
            outstr += "|" + "".FixedWidth(16);                               //(login)
          outstr += "|" + str_pull_dt.FixedWidth(16);                      //(employee)/(pull datetime)
            outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
            outstr += "|" + "C".FixedWidth(1);                               //record type = class
            outstr += "|" + "".FixedWidth(4);                                //(stage)
            outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
            outstr += "|" + _pat.range.ToString().FixedWidth(4);             //TC pull range
            outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
            outstr += "|";
            outstr = outstr.FixedWidth(294);
          outstr += "|" + _pat.effective.ToString(DATETIME_FORMAT);        //IN
            outstr = outstr.FixedWidth(377);
            outstr += "|";
            
            ind_list = "";

            if (give_default_inds)
            {
                Program.VerboseAudit("Patient will receive default indicators " + _pat.default_inds_str);
                for (i = 1; (i <= MAX_INDS); i++)
                {
                    _inds[i].is_checked = false;
                }
                foreach (var ind in _pat.default_inds)
                {
                    if (ind <= _inds.GetUpperBound(0))
                    {
                        _inds[ind].is_checked = true;
                    }
                }
            }


            for (i = 1; (i <= MAX_INDS); i++) {
                if (_inds[i].is_checked) {
                    outstr += "Y";
                    ind_list += "," + i;
                } else {
                    outstr += "N";
                }
            } // next i
            ind_list = ind_list.Substring(1);                           //strip leading comma
  
            Program.outfile.WriteLine(outstr);                          //output to transparent.txt

            Program.Audit("");
            desc = "Classified: " + ind_list;
            if (Program.g_is_test) {
                Program.Audit(desc);
            } else {
                //Save the selected indicators plus both Program.Audits in the event log; link with tc_event_id
                PFSEventLog.AddTransparentMappingEventLogEntry(
                    desc, Program.gLogUnitID, Program.gLogEncounterID,
                    tc_event_id, Program.gLogMapperVersion,
                    Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
            }
        }

        private void OutputProcs()
        {
            int i;
            string outstr, proc_list, desc;
            int tc_event_id;

            foreach(var proc in _procs) {
                if (Program.g_is_test)
                    tc_event_id = 9999;
                else
                    tc_event_id = PFSUtility.NextGID();                         //get a unique id for this proc

                outstr = _pat.facilty_code.FixedWidth(8);
                outstr += "|" + _pat.unit_name;                                 //10
                outstr = outstr.FixedWidth(68);
                outstr += "|" + _pat.acct.FixedWidth(20);                       //90
                outstr += "|" + _pat.last_name.FixedWidth(32);
                outstr += "|" + _pat.first_name.FixedWidth(32);
                outstr += "|" + _pat.middle_name.FixedWidth(32);
                outstr = outstr.FixedWidth(202);
                outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //204 proc dt
                outstr = outstr.FixedWidth(254);
                outstr += "|P";                                                 //256 procedure type record
                outstr += "|" + "".FixedWidth(4);                               //(stage)
                outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);     //TC source ID
                outstr += "|" + _pat.range.ToString().FixedWidth(4);            //TC pull range
                outstr += "|" + tc_event_id.ToString().FixedWidth(10);          //TC event ID
                outstr += "|";
                outstr = outstr.FixedWidth(294);
                outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //296 procdt in
                outstr = outstr.FixedWidth(346);
                outstr += "|" + proc.finish.ToString(DATETIME_FORMAT);          //348 procdt out
                outstr = outstr.FixedWidth(377);
                outstr += "|";
                
                proc_list = "";
                for (i = 1; (i < MAX_PROCS); i++) {
                    if (proc.procedure_number == i) {
                        outstr += "Y";
                        proc_list += "," + i;
                    } else {
                        outstr += "N";
                    }
                } // next i
                proc_list = proc_list.Substring(1);                             //strip leading comma

                Program.outfile.WriteLine(outstr);                              //output to transparent.txt

                desc = "Procedures: " + proc_list;
                if (Program.g_is_test) {
                    Program.Audit(desc);
                } else {
                    //Save the selected procedures plus both Program.Audits in the event log; link with tc_event_id
                    //(this assumes the procedure Program.Audit is mixed in with the class Program.Audit)
                    PFSEventLog.AddTransparentMappingEventLogEntry(desc, Program.gLogUnitID, Program.gLogEncounterID,
                        tc_event_id, Program.gLogMapperVersion,
                        Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
                }
            } // next proc
        }

        private void OutputOutcomes()
        {
            string outstr;

            foreach (var oc in _outcomes)
            {
                outstr = "".FixedWidth(9) + _pat.unit_name.FixedWidth(16);       // unitname
                outstr = outstr.FixedWidth(60);
                outstr += oc.start.ToString(DATETIME_FORMAT);                 //event_datetime
                outstr = outstr.FixedWidth(73);
                outstr += _pat.acct.FixedWidth(20);                            //acct
                outstr = outstr.FixedWidth(94);
                outstr += oc.procedure_number;                                //outcome indicator
                Program.out2file.WriteLine(outstr);                          //output to outcomesindicator.txt
            }

        }

        private bool DefaultInPast4hrs()
        {
            bool def = false;

            var db = PFSUtility.NewPfsDataContext();

            var query = from ce in db.CLASSIFICATION_EVENTs
                        where (ce.ENCOUNTER_ID == _pat.encounter_id)
                        where (ce.EFFECTIVE_DATETIME_IN >= _pat.pull_finish.AddMinutes(-(4 * 60)))
                        orderby (ce.EFFECTIVE_DATETIME_IN)
                        select ce;

            int ct = query.Count();
            if (ct == 0) return def;
            bool first = true;
            foreach (var byid in query)
            {
                if (first)
                {
                    first = false;
                    def = (byid.CLASSIFIED_BY_ID == -2); // this is a default
                    Program.VerboseAudit("ceid=" + byid.CLASSIFICATION_EVENT_ID + "  byid=" + byid.CLASSIFIED_BY_ID + "  exists=" + def.ToString());
                }
            }
            return def;
        }

        private int DeterminePtypeOfIndicators()
        {
            int i;
            var pscore = 0.0;
            int pt_type = 6;
            var indlist = new List<int>();
            string indstr = "";

            Program.VerboseAudit("Determine patient type of triggered indicators");

            for (i = 1; (i <= MAX_INDS); i++)
            {
                if (_inds[i].is_checked)
                {
                    indlist.Add(i);
                    indstr += i.ToString() + ",";
                }
            }

            var db = PFSUtility.NewPfsDataContext();
            var query_ind_def = from ind_def in db.INDICATOR_DEFINITIONs
                                where (ind_def.METHODOLOGY_ID == 20) &&
                                  indlist.Contains(ind_def.INDICATOR_NUMBER)
                                select new
                                {
                                    ind_def.WEIGHT
                                };
            foreach (var wgts in query_ind_def)
            {
                pscore += wgts.WEIGHT;
            }
            Program.VerboseAudit("indicators=" + indstr);
            Program.VerboseAudit("score=" + pscore.ToString());

            var query_ptype = from ptype in db.PATIENT_TYPEs
                              where (ptype.METHODOLOGY_ID == 20)
                              orderby ptype.PATIENT_TYPE1
                              select new
                              {
                                  ptype.PATIENT_TYPE1,
                                  ptype.POINTS_HIVAL
                              };

            foreach (var ptypes in query_ptype)
            {
                //                Program.DebugTrace("type,hival=" + ptypes.PATIENT_TYPE1.ToString() + "," + ptypes.POINTS_HIVAL.ToString(), "");
                if (pscore <= ptypes.POINTS_HIVAL)
                {
                    if (pt_type > ptypes.PATIENT_TYPE1)
                    {
                        pt_type = ptypes.PATIENT_TYPE1;
                    }
                }
            }
            Program.VerboseAudit("patient type=" + pt_type.ToString());

            return pt_type;
        }


    }
}
