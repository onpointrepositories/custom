10/10/2014 2:23:06 PM [I] A02 - Transfer ACUITYUPGR, PATA AAA: Acct=142210208388, MRN=85-33-84-2, Type='I', Location=3NE   10/10/2014 1:16:00 PM
MSH|^~\&|HL7SCMADM2|NIH|SCM||20141010142254||ADT^A02|ADT9006016553601100|P|2.3
EVN|A02|201410101422|201410101422
PID|||85-33-84-2^^^MRN||ACUITYUPGR^PATA^AAA||19561031|M|||2341 MAIN^^ANN^NJ^23412^USA||^^^^^(234)^123-6767^^Home||English|||142210208388^^^Account
PV1||I|3NE^^^CC||||H0ASW000^Ackerman^Hans|||I PD|||||13-CC-0099||2000574448^Joly^Joanna|I||||||||||||||||||||||||||201410101316


10/10/2014 2:28:05 PM [I] R01 - Unsolicited Observation: ACUITYUPGR, PATA AAA: Acct=142210208388, MRN=8533842, Type='I', Location=3NE   10/10/2014 1:16:00 PM
MSH|^~\&|SCM|SCM|HL7ACP||20141010142524||ORU^R01|9000249420003960|P|2.3
PID|||8533842^^^MRN||ACUITYUPGR^PATA^AAA||19561031|Male|||2341 MAIN^^ANN^NJ^23412^USA^Home||^^^^^234^123-6767^^Home||English|||142210208388^^^Account|U-NK-NOWN
PV1||Inpatient|3NE^^^CC||||H0ASW000^Ackerman^Hans|||I PD|||||13-CC-0099||2000574448^Joly^Joanna|Inpatient|142210208388^^^Account|||||||||||||||||||||||||20141010131600
OBR|1||9002899372002040^SCM|DACPFS70003^02 Nursing Assessment^ACPNURS|||20141010142400|||||||||||||||20141010142517|||R|||||||IETIN&Etin&Inna&
OBX|1|TX|NURS Neuro Level of Consciousness 1^NURS Neuro Level of Consciousness 1^ACPNURS|1|Lethargic|||N|||R|||20141010142400
OBX|2|TX|NURS Neuro Oriented To^NURS Neuro Oriented To^ACPNURS|2|Place|||N|||R|||20141010142400
OBX|3|TX|NURS Physical Safety Risks Due To^NURS Physical Safety Risks Due To^ACPNURS|3|Age|||N|||R|||20141010142400
OBX|4|TX|NURS Physical Safety Devices^NURS Physical Safety Devices^ACPNURS|4|Helmet|||N|||R|||20141010142400
OBX|5|TX|NURS Assistance Levels^NURS Assistance Levels^ACPNURS|5|Partial|||N|||R|||20141010142400
OBX|6|TX|NURS Med/Surg Activities^NURS Med/Surg Activities^ACPNURS|6|1:1 safety observation by non-RN|||N|||R|||20141010142400
OBX|7|TX|NURS Med/Surg Activites Freq^NURS Med/Surg Activites Freq^ACPNURS|7|2 hours|||N|||R|||20141010142400


10/10/2014 2:32:07 PM [I] R01 - Unsolicited Observation: ACUITYUPGR, PATA AAA: Acct=142210208388, MRN=8533842, Type='I', Location=3NE   10/10/2014 1:16:00 PM
MSH|^~\&|SCM|SCM|HL7ACP||20141010143205||ORU^R01|9000249420103960|P|2.3
PID|||8533842^^^MRN||ACUITYUPGR^PATA^AAA||19561031|Male|||2341 MAIN^^ANN^NJ^23412^USA^Home||^^^^^234^123-6767^^Home||English|||142210208388^^^Account|U-NK-NOWN
PV1||Inpatient|3NE^^^CC||||H0ASW000^Ackerman^Hans|||I PD|||||13-CC-0099||2000574448^Joly^Joanna|Inpatient|142210208388^^^Account|||||||||||||||||||||||||20141010131600
OBR|1||9002899372002040^SCM|DACPFS70003^02 Nursing Assessment^ACPNURS|||20141010142400|||||||||||||||20141010143154|||C|||||||IETIN&Etin&Inna&
OBX|1|TX|NURS Neuro Level of Consciousness 1^NURS Neuro Level of Consciousness 1^ACPNURS|1|Lethargic|||N|||R|||20141010142400
OBX|2|TX|NURS Physical Safety Risks Due To^NURS Physical Safety Risks Due To^ACPNURS|2|Age|||N|||R|||20141010142400
OBX|3|TX|NURS Physical Safety Devices^NURS Physical Safety Devices^ACPNURS|3|Helmet|||N|||R|||20141010142400
OBX|4|TX|NURS Assistance Levels^NURS Assistance Levels^ACPNURS|4|Partial|||N|||R|||20141010142400
OBX|5|TX|NURS Med/Surg Activities^NURS Med/Surg Activities^ACPNURS|5|1:1 safety observation by non-RN|||N|||R|||20141010142400
OBX|6|TX|NURS Med/Surg Activites Freq^NURS Med/Surg Activites Freq^ACPNURS|6|2 hours|||N|||R|||20141010142400


10/10/2014 2:33:36 PM [I] R01 - Unsolicited Observation: ACUITYUPGR, PATA AAA: Acct=142210208388, MRN=8533842, Type='I', Location=3NE   10/10/2014 1:16:00 PM
MSH|^~\&|SCM|SCM|HL7ACP||20141010143335||ORU^R01|9000249420203960|P|2.3
PID|||8533842^^^MRN||ACUITYUPGR^PATA^AAA||19561031|Male|||2341 MAIN^^ANN^NJ^23412^USA^Home||^^^^^234^123-6767^^Home||English|||142210208388^^^Account|U-NK-NOWN
PV1||Inpatient|3NE^^^CC||||H0ASW000^Ackerman^Hans|||I PD|||||13-CC-0099||2000574448^Joly^Joanna|Inpatient|142210208388^^^Account|||||||||||||||||||||||||20141010131600
OBR|1||9002899372002040^SCM|DACPFS70003^02 Nursing Assessment^ACPNURS|||20141010142400|||||||||||||||20141010143329|||C|||||||IETIN&Etin&Inna&


10/10/2014 2:45:31 PM [I] R01 - Unsolicited Observation: ACUITYUPGR, PATA AAA: Acct=142210208388, MRN=8533842, Type='I', Location=3NE   10/10/2014 1:16:00 PM
MSH|^~\&|SCM|SCM|HL7ACP||20141010144526||ORU^R01|9000249420303960|P|2.3
PID|||8533842^^^MRN||ACUITYUPGR^PATA^AAA||19561031|Male|||2341 MAIN^^ANN^NJ^23412^USA^Home||^^^^^234^123-6767^^Home||English|||142210208388^^^Account|U-NK-NOWN
PV1||Inpatient|3NE^^^CC||||H0ASW000^Ackerman^Hans|||I PD|||||13-CC-0099||2000574448^Joly^Joanna|Inpatient|142210208388^^^Account|||||||||||||||||||||||||20141010131600
OBR|1||9002899372302040^SCM|DACPFS70004^03 Nursing Treatment and Care^ACPNURS|||20141010142400|||||||||||||||20141010144515|||R|||||||IETIN&Etin&Inna&
OBX|1|TX|NURS Mobility Activities^NURS Mobility Activities^ACPNURS|1|Ambulated around unit|||N|||R|||20141010142400
OBX|2|TX|NURS Activity Assistance1^NURS Activity Assistance1^ACPNURS|2|Partial|||N|||R|||20141010142400
OBX|3|TX|NURS Activity Assistance1^NURS Activity Assistance1^ACPNURS|2|With 2 staff|||N|||R|||20141010142400


10/10/2014 2:48:19 PM [I] R01 - Unsolicited Observation: ACUITYUPGR, PATA AAA: Acct=142210208388, MRN=8533842, Type='I', Location=3NE   10/10/2014 1:16:00 PM
MSH|^~\&|SCM|SCM|HL7ACP||20141010144816||ORU^R01|9000249420403960|P|2.3
PID|||8533842^^^MRN||ACUITYUPGR^PATA^AAA||19561031|Male|||2341 MAIN^^ANN^NJ^23412^USA^Home||^^^^^234^123-6767^^Home||English|||142210208388^^^Account|U-NK-NOWN
PV1||Inpatient|3NE^^^CC||||H0ASW000^Ackerman^Hans|||I PD|||||13-CC-0099||2000574448^Joly^Joanna|Inpatient|142210208388^^^Account|||||||||||||||||||||||||20141010131600
OBR|1||9002899372302040^SCM|DACPFS70004^03 Nursing Treatment and Care^ACPNURS|||20141010142400|||||||||||||||20141010144808|||C|||||||IETIN&Etin&Inna&
OBX|1|TX|NURS Mobility Activities^NURS Mobility Activities^ACPNURS|1|Ambulated around unit|||N|||R|||20141010142400
OBX|2|TX|NURS Activity Assistance1^NURS Activity Assistance1^ACPNURS|2|Partial|||N|||C|||20141010142400
OBX|3|TX|NURS Activity Assistance1^NURS Activity Assistance1^ACPNURS|2|With 1 staff|||N|||C|||20141010142400


10/10/2014 2:50:08 PM [I] R01 - Unsolicited Observation: ACUITYUPGR, PATA AAA: Acct=142210208388, MRN=8533842, Type='I', Location=3NE   10/10/2014 1:16:00 PM
MSH|^~\&|SCM|SCM|HL7ACP||20141010145006||ORU^R01|9000249420503960|P|2.3
PID|||8533842^^^MRN||ACUITYUPGR^PATA^AAA||19561031|Male|||2341 MAIN^^ANN^NJ^23412^USA^Home||^^^^^234^123-6767^^Home||English|||142210208388^^^Account|U-NK-NOWN
PV1||Inpatient|3NE^^^CC||||H0ASW000^Ackerman^Hans|||I PD|||||13-CC-0099||2000574448^Joly^Joanna|Inpatient|142210208388^^^Account|||||||||||||||||||||||||20141010131600
OBR|1||9002899372502040^SCM|DACPFS70004^03 Nursing Treatment and Care^ACPNURS|||20141010144900|||||||||||||||20141010145001|||R|||||||IETIN&Etin&Inna&
OBX|1|TX|NURS Food/Fluid Interventions^NURS Food/Fluid Interventions^ACPNURS|1|Fluids encouraged|||N|||R|||20141010144900
OBX|2|TX|NURS Diet Assistance^NURS Diet Assistance^ACPNURS|2|Partial|||N|||R|||20141010144900
OBX|3|TX|NURS Bowel Nursing Interventions^NURS Bowel Nursing Interventions^ACPNURS|3|Urinal used|||N|||R|||20141010144900
OBX|4|TX|NURS Bowel Nursing Interventions^NURS Bowel Nursing Interventions^ACPNURS|3|Checked for impaction|||N|||R|||20141010144900
OBX|5|TX|NURS Mobility Activities^NURS Mobility Activities^ACPNURS|4|Ambulated in room|||N|||R|||20141010144900


10/10/2014 2:53:01 PM [I] R01 - Unsolicited Observation: ACUITYUPGR, PATA AAA: Acct=142210208388, MRN=8533842, Type='I', Location=3NE   10/10/2014 1:16:00 PM
MSH|^~\&|SCM|SCM|HL7ACP||20141010145257||ORU^R01|9000249420603960|P|2.3
PID|||8533842^^^MRN||ACUITYUPGR^PATA^AAA||19561031|Male|||2341 MAIN^^ANN^NJ^23412^USA^Home||^^^^^234^123-6767^^Home||English|||142210208388^^^Account|U-NK-NOWN
PV1||Inpatient|3NE^^^CC||||H0ASW000^Ackerman^Hans|||I PD|||||13-CC-0099||2000574448^Joly^Joanna|Inpatient|142210208388^^^Account|||||||||||||||||||||||||20141010131600
OBR|1||9002899372602040^SCM|DACPSN70026^Patient Education Note^ACPNURS|||20141010145100|||||||||||||||20141010145252|||R|||||||IETIN&Etin&Inna&
OBX|1|TX|NIH Patient Ed Instruction Duration^NIH Patient Ed Instruction Duration^ACPNURS|1|Less than 1 hour|||N|||R|||20141010145100
OBX|2|TX|NIH Patient Ed Instructions Given To^NIH Patient Ed Instructions Given To^ACPNURS|2|Patient|||N|||R|||20141010145100
OBX|3|TX|NIH Patient Ed Methods Used^NIH Patient Ed Methods Used^ACPNURS|3|Patient Research Education Group|||N|||R|||20141010145100
OBX|4|TX|NIH Patient Ed Diet^NIH Patient Ed Diet^ACPNURS|4|Decreasing phosphorus intake|||N|||R|||20141010145100
OBX|5|TX|NIH Patient Ed Drug-nutrient Interaction^NIH Patient Ed Drug-nutrient Interaction^ACPNURS|5|Lithium and sodium and caffeine|||N|||R|||20141010145100
OBX|6|TX|NIH Patient Ed Validation Content^NIH Patient Ed Validation Content^ACPNURS|6|Medication|||N|||R|||20141010145100
OBX|7|TX|NIH Patient Ed Comprehension Level^NIH Patient Ed Comprehension Level^ACPNURS|7|Good|||N|||R|||20141010145100
OBX|8|TX|NIH Patient Ed Expected Adherence^NIH Patient Ed Expected Adherence^ACPNURS|8|Good|||N|||R|||20141010145100


10/10/2014 2:54:19 PM [I] R01 - Unsolicited Observation: ACUITYUPGR, PATA AAA: Acct=142210208388, MRN=8533842, Type='I', Location=3NE   10/10/2014 1:16:00 PM
MSH|^~\&|SCM|SCM|HL7ACP||20141010145417||ORU^R01|9000249420703960|P|2.3
PID|||8533842^^^MRN||ACUITYUPGR^PATA^AAA||19561031|Male|||2341 MAIN^^ANN^NJ^23412^USA^Home||^^^^^234^123-6767^^Home||English|||142210208388^^^Account|U-NK-NOWN
PV1||Inpatient|3NE^^^CC||||H0ASW000^Ackerman^Hans|||I PD|||||13-CC-0099||2000574448^Joly^Joanna|Inpatient|142210208388^^^Account|||||||||||||||||||||||||20141010131600
OBR|1||9002899372602040^SCM|DACPSN70026^Patient Education Note^ACPNURS|||20141010145100|||||||||||||||20141010145407|||C|||||||IETIN&Etin&Inna&
OBX|1|TX|NIH Patient Ed Instruction Duration^NIH Patient Ed Instruction Duration^ACPNURS|1|Greater than 1 hour|||N|||C|||20141010145100
OBX|2|TX|NIH Patient Ed Instructions Given To^NIH Patient Ed Instructions Given To^ACPNURS|2|Patient|||N|||C|||20141010145100
OBX|3|TX|NIH Patient Ed Instructions Given To^NIH Patient Ed Instructions Given To^ACPNURS|2|Family|||N|||C|||20141010145100
OBX|4|TX|NIH Patient Ed Methods Used^NIH Patient Ed Methods Used^ACPNURS|3|Patient Research Education Group|||N|||R|||20141010145100
OBX|5|TX|NIH Patient Ed Diet^NIH Patient Ed Diet^ACPNURS|4|Decreasing phosphorus intake|||N|||R|||20141010145100
OBX|6|TX|NIH Patient Ed Drug-nutrient Interaction^NIH Patient Ed Drug-nutrient Interaction^ACPNURS|5|Lithium and sodium and caffeine|||N|||R|||20141010145100
OBX|7|TX|NIH Patient Ed Validation Content^NIH Patient Ed Validation Content^ACPNURS|6|Medication|||N|||R|||20141010145100
OBX|8|TX|NIH Patient Ed Comprehension Level^NIH Patient Ed Comprehension Level^ACPNURS|7|Fair|||N|||C|||20141010145100
OBX|9|TX|NIH Patient Ed Expected Adherence^NIH Patient Ed Expected Adherence^ACPNURS|8|Good|||N|||R|||20141010145100


10/10/2014 2:57:02 PM [I] R01 - Unsolicited Observation: ACUITYUPGR, PATA AAA: Acct=142210208388, MRN=8533842, Type='I', Location=3NE   10/10/2014 1:16:00 PM
MSH|^~\&|SCM|SCM|HL7ACP||20141010145657||ORU^R01|9000249420803960|P|2.3
PID|||8533842^^^MRN||ACUITYUPGR^PATA^AAA||19561031|Male|||2341 MAIN^^ANN^NJ^23412^USA^Home||^^^^^234^123-6767^^Home||English|||142210208388^^^Account|U-NK-NOWN
PV1||Inpatient|3NE^^^CC||||H0ASW000^Ackerman^Hans|||I PD|||||13-CC-0099||2000574448^Joly^Joanna|Inpatient|142210208388^^^Account|||||||||||||||||||||||||20141010131600
OBR|1||9002899372602040^SCM|DACPSN70026^Patient Education Note^ACPNURS|||20141010145100|||||||||||||||20141010145654|||C|||||||IETIN&Etin&Inna&


10/10/2014 2:57:02 PM [I] R01 - Unsolicited Observation: ACUITYUPGR, PATA AAA: Acct=142210208388, MRN=8533842, Type='I', Location=3NE   10/10/2014 1:16:00 PM
MSH|^~\&|SCM|SCM|HL7ACP||20141010145657||ORU^R01|9000249420903960|P|2.3
PID|||8533842^^^MRN||ACUITYUPGR^PATA^AAA||19561031|Male|||2341 MAIN^^ANN^NJ^23412^USA^Home||^^^^^234^123-6767^^Home||English|||142210208388^^^Account|U-NK-NOWN
PV1||Inpatient|3NE^^^CC||||H0ASW000^Ackerman^Hans|||I PD|||||13-CC-0099||2000574448^Joly^Joanna|Inpatient|142210208388^^^Account|||||||||||||||||||||||||20141010131600
OBR|1||9002899372602040^SCM|DACPSN70026^Patient Education Note^ACPNURS|||20141010145100|||||||||||||||20141010145654|||C|||||||IETIN&Etin&Inna&



1. add fs
2. delete single item on fs
3.  delete time col fs
4.  add new differente fs
5.  modify item in fs
6.  add new time col on fs.
7.  Struc Note add: 
8.  mod/del on SN
9.  Del SN 

Modification has C but Deletion is omission
So need to see how this would work -- will need the info on the OBR to do this.