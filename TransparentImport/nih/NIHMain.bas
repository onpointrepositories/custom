Attribute VB_Name = "NIHMain"
Option Explicit
'
' NIH transparent classification
'-verbose -efftime=0700 -effdate=yesterday -pulltime=0700 -pulldate=today -range=1440
' Each unit must set the "use transparent" flag.
'
' This is designed to be run as a background job, so never stop for user input -- that means no error MsgBox.
' All communication is done through log files and the event log; it is OK to print to the console.
'
' In order to be version independent, this program does not use any AcuityPlus dlls.
' Some AcuityPlus utility classes have been copied here.
'
' In order to work with the AcuityPlus Patient Selection and Transparent import:
'
'   * The program name must be TransparentMapping.exe, placed in AcuityPlus\bin
'   * The audit file is called TransparentAudit.log, placed in AcuityPlus\log
'   * The output file is Transparent.txt, placed in AcuityPlus\load_me
'   * Events are saved in the database event log
'
Public g_cnADO          As ADODB.Connection             'main DB connection
Public g_util           As New PFSUtility               'utility functions
Public g_dbutil         As New PFSDBUtility
Public g_event          As New PFSEventLog
Public g_display        As New PFSDisplayProperties
Public g_command        As String                       'command line (for db utility)
Public g_abort          As Boolean

Private Const TRANSP_FILENAME = "Transparent.txt"       'THE output file
Private Const TRANSP_AUDIT_LOG = "TransparentAudit.log" 'The debug/audit log
Private Const CHART_ITEM_LIFE = 180                      'days in the chart_item table EXCEPT for Order_Status<>''
Private Const OUTCOMES_FILENAME = "OutcomesIndicator.txt"   'outcomes output file

Private Const INPUT_PRODPATH = "c:\program files\quadramed\acuityplus\load_me\"
Private Const INPUT_IFILE = "tcquery."
'C:\Documents and Settings\kmasumoto\Desktop\CLIENT FOLDERS\Mission
Private Const DEFAULT_RANGE = 1440                      '24 hrs in minutes; overridden by -range
Public Const MAX_LOA = 10
Public Const MAX_LOC = 10

Public Type LOAtype
    startdt As Date
    enddt As Date
    Cancel As Boolean
    retain As Boolean
    locidx As Integer
End Type
Public Type LOAtypePrecision
    startdt As String
    enddt As String
    timestamp As String
    Cancel As Boolean
    retain As Boolean
End Type
Public Type LocType
    UnitID As Long
    indt As Date
    outdt As Date
    remove As Boolean
End Type
Public Type ClassType
    UnitID As Long
    indt As Date
    outdt As Date
    is_dnc As Boolean
    source As Integer
End Type

Public Type PatientInfo
    unit_id             As Long
    encounter_id        As Long
    meth_id             As Long
    last_name           As String
    first_name          As String
    acct                As String
    age                 As Single                   'age (in years) at admission
    unit_name           As String
    room                As String
    bed                 As String
    unit_arrival        As Date
    effective           As Date                     'patient specific (may be > g_effdt)
    pull_start          As Date                     'patient specific (may be > g_pull_start)
    pull_finish         As Date                     'patient specific (may be < g_pull_finish)
    range               As Integer                  'patient specific (may be < g_range)
    TC_source_id        As Integer
    num_loa             As Integer
    loa(MAX_LOA)        As LOAtype
'    intloa_startdt      As Date
'    intloa_finishdt     As Date
'    outdt               As Date
    num_loc             As Integer
    loc(MAX_LOC)        As LocType
    num_class           As Integer
    Class(MAX_LOC)  As ClassType
End Type

Public g_debug          As Boolean
Public g_verbose        As Boolean                  'verbose debug output?
Public g_no_output      As Boolean      'if no ouput, then no input files either

Public g_effdt          As Date                     'effective datetime

Public outfile          As Integer
Public dbugfile         As Integer

Public gLogUnitID       As Long
Public gLogEncounterID  As Long
Public gLogSourceText   As String

Public g_pull_start     As Date
Public g_pull_finish    As Date                     'pull datetime
Public g_range          As Long

Private winpfspath      As String                   'path of winpfs
Private winlogpath      As String                   'path of winpfs\log
Private winloadpath     As String                   'path of winpfs\load_me
Private outfilename     As String
Private dbugname        As String

Private nowdt           As Date
Private debug_acct      As String                   'acct filter
Private debug_unit      As String                   'unit filter
Private debug_unit_id   As Long
Public called_from_server As Boolean
Public g_verbose_audit  As String
Public g_brief_audit    As String

Sub Main()
    Dim s As String
    On Error GoTo errHandler
    
    frmMain.Show vbModeless                         'progress window
    DoEvents
    
    Set g_cnADO = g_dbutil.NewRemoteConnection
    
    gLogSourceText = Command$
    LogInfo "Begin mapping and translation", EVENT_CATEGORY_STARTUP_SHUTDOWN 'deletes old event_log records
    gLogSourceText = ""
    
    ParseCommandLine
    OpenOutputFiles
    'look at chart items for past 30 days for STARTLOA. if no finishloa then
    '   create dnc record with in time = startloa and out time = tomorrow 7am.
    '   if finishloa found then
    '   create dnc record with in time = startloa and out time = finishloa.
'    If called_from_server Then ProcessDNC 'for entire db, not just those with chart items.
    Process
    CloseOutputFiles
    DeleteOldLogs
    DeleteOldChartItems

    LogInfo "Mapping complete", EVENT_CATEGORY_STARTUP_SHUTDOWN
    g_cnADO.Close
normalExit:
    Unload frmMain
    Exit Sub

errHandler:
    LogError Err.Description & " in " & Err.source
    LogInfo "Unexpected error - shut down", EVENT_CATEGORY_STARTUP_SHUTDOWN
    GoTo normalExit
    Resume  'debug
End Sub

Private Sub ParseCommandLine()
    On Error GoTo errHandler

    Dim argc As Integer, argv() As String, subarg() As String, tag As String, value As String
    Dim i As Integer, n As Integer
    Dim effdate As String, efftime As String
    Dim pulldate As String, pulltime As String
    Dim nowdate As String, nowtime As String
    
    '-effdate=yyyymmdd   This is the date which, in combination with the
    '                   -efftime time is used to specify the classification In time.
    '                   If not specified, -effdate is assumed to be the current date.
    'Special value:  -effdate=yesterday
    
    '-efftime=hhmm      Used to set the classification In time.
    '                   If not specified, then -efftime is assumed to be the current time.
        
    '-pulldate=yyyymmdd This is the date/time of the data pull time.
    '                   If not specified, then this is assumed to be the -effdate.
    '-pulltime=hhmm     This is the time starting from which the pull is to
    '                   look backwards from.
    '                   If not specified, then this time is assumed to be the -efftime.
    
    '-range=nnnn        This is the number of minutes backwards from the pull time
    '                   that defines valid g_range of charting events.
    '                   If not specified, this will default to 1440 (24 hours)

    '-debug             echo debug/audit info; save in log file
    '-brief             Minimal debug info -- sets -debug
    '-verbose           Lots of debugging info -- sets -debug
    '-n                 No output to transparent.txt - debug only
    '-audit             sets -debug and -n
    '
    '-acct=12345        Process this patient only
    '-unit=5N           Process this unit only
    '-unit_id=1234      process this unit only
    
'==================
'        pull_dt = rs("classification_datetime")
'        eff_dt = rs("effective_datetime_in")
'        range = g_dbutil.DBToInteger(rs("TC_pull_range"))
'        If (range = 0) Then range = 1440
'    cmd = "..\bin\" & TRANSPARENT_MAPPING_EXE_NAME
'    If verbose Then
'        cmd = cmd & " -verbose"
'    Else
'        cmd = cmd & " -brief"
'    End If
'    cmd = cmd & " -n"                               'no output to transparent.txt
'    cmd = cmd & " -unit_id=" & cdlUnit.code(cboUnit.Text)
'    cmd = cmd & " -acct=" & cboAcct.Text
'    cmd = cmd & " -pulldate=" & Format$(pull_dt, "yyyymmdd")
'    cmd = cmd & " -pulltime=" & Format$(pull_dt, "hhnn")
'    cmd = cmd & " -effdate=" & Format$(eff_dt, "yyyymmdd")
'    cmd = cmd & " -efftime=" & Format$(eff_dt, "hhnn")
'    cmd = cmd & " -range=" & range
'
'==================
    
    nowdt = Now
    nowdate = Format$(nowdt, "yyyymmdd")
    nowtime = Format$(nowdt, "hhnn")
    
    g_verbose = True

    argc = g_util.SplitTextOnChar(LCase$(Command$), " ", argv(), 1, 1)

    For i = 1 To argc
        n = g_util.SplitTextOnChar(argv(i), "=", subarg(), 1, 2)        '<tag>=<value>
        tag = subarg(1)
        value = subarg(2)
        
        Select Case tag
        Case "-server"    'must be first arg
            called_from_server = True
        
        Case "-acct"
            debug_acct = value
            
        Case "-audit"
            g_debug = True
            g_no_output = True
        
        Case "-brief"
            g_verbose = False
            g_debug = True
        
        Case "-debug"
            g_debug = True
        
        Case "-effdate"
            If (value = "yesterday") Then
                effdate = Format(DateAdd("d", -1, Date), "yyyymmdd")
            Else
                effdate = value
            End If
        
        Case "-efftime"
            efftime = Left$(value, 4)
            efftime = String$(4 - Len(efftime), "0") & efftime
        
        Case "-n"
            g_no_output = True

        Case "-pulldate"
            If (value = "today") Then
                pulldate = Format(Date, "yyyymmdd")
            Else
                pulldate = value
            End If
        
        Case "-pulltime"
            pulltime = Left$(value, 4)
            pulltime = String$(4 - Len(pulltime), "0") & pulltime

        Case "-range"
            g_range = CInt(value)
            
        Case "-unit"
            debug_unit = value
        Case "-unit_id"
            debug_unit_id = CLng(value)
            
        Case "-verbose"
            g_verbose = True
            g_debug = True

        Case Else
            LogWarning "Unexpected argument: " & tag
        End Select
    Next i
    
    If Not called_from_server Then
        'adjust times
        'need to do this for 7am class because the pull from todays 7am goes into yesterday 7am
        'so need to adjust pulldatetime. effdate doesnt matter b/c not creating a class.
        g_range = 1440
        If efftime < "0700" Then 'the report date is the calendar day before
            'set the calendar date back by 1 day
            'effdt of 2014 0101 0333 means that the pull was done on 2014 0101 0700 going back to 2013 1231 0700
            'so take the effdate 1 day back for use in setting pulldate
            effdate = Format(DateAdd("n", -g_range, g_util.CDateEx(effdate & efftime)), "yyyymmdd")
        End If
        efftime = "0700"
        'set pulldate by adding 1 day
        pulldate = Format(DateAdd("n", g_range, g_util.CDateEx(effdate & efftime)), "yyyymmdd")
        pulltime = "0700"
'        MsgBox "pulldate=" & pulldate
'        MsgBox "pulltime=" & pulltime
        
'        If efftime = "0700" Then
'            pulldate = Format(DateAdd("n", g_range, g_util.CDateEx(effdate & efftime)), "yyyymmdd")
'            pulltime = Format(DateAdd("n", g_range, g_util.CDateEx(effdate & efftime)), "hhnn")
'        End If
    End If

    If Len(effdate) = 0 Then effdate = nowdate
    If Len(efftime) = 0 Then efftime = nowtime
    
    'Note: pulldate defaults to effdate, not nowdate
    If Len(pulldate) = 0 Then pulldate = effdate
    If Len(pulltime) = 0 Then pulltime = efftime

    If g_range = 0 Then g_range = DEFAULT_RANGE
    
    'Note: the log file isn't open yet so these just go to the screen
    Debug.Print "Command line: " & Command$
    Debug.Print "effdate=" & effdate
    Debug.Print "efftime=" & efftime
    Debug.Print "pulldate=" & pulldate
    Debug.Print "pulltime=" & pulltime
    Debug.Print "range=" & g_range
    
    g_effdt = g_util.CDateEx(effdate & efftime)
    
    g_pull_finish = g_util.CDateEx(pulldate & pulltime)     'for chart item queries
    g_pull_start = DateAdd("n", -g_range, g_pull_finish)
    
    If Not called_from_server Then
'        If g_range <> 1440 Then
'            g_pull_start = g_pull_finish
'            g_pull_finish = DateAdd("n", g_range, g_pull_finish)
'        End If
    End If
    Exit Sub
    
errHandler:
    g_util.ThrowError "ParseCommandLine"
    Resume  'debug
End Sub


Private Sub Process()
    On Error GoTo errHandler
    
    Dim pat As PatientInfo
    Dim rs As New Recordset
    Dim sql As String
    Dim count As Integer
    Dim suppress_class As Boolean
    Dim i As Integer
    Dim prev_encid As Long

    prev_encid = 0
    dprint ""
        
    '
    ' Make a list of all patients with chart items during the pull period.
    ' Include only units that have the "use transparent" flag set.
    ' Limit to one patient if -acct is given.  Limit to one unit with -unit.
    ' Left join encounter_location - the patient may be discharged at pull time.
    ' Look for the latest patient arrival to the unit before the pull time; ignore transfers within the unit.
    ' Look for a patient departure (if any) before the pull time; ignore transfers within the unit.
    sql = "" & _
        " SELECT LIST.UNIT_ID, LIST.ENCOUNTER_ID, PARAM.METHODOLOGY_ID," & _
        "     UNIT.NAME AS UNIT, EL.ROOM, EL.BED, EL.DATETIME_IN AS BED_ARRIVAL," & _
        "     ARRIVE.DATETIME_IN AS UNIT_ARRIVAL, DEPART.DATETIME_OUT AS UNIT_DEPARTURE," & _
        "     E.ACCT_NUMBER, P.LAST_NAME, P.FIRST_NAME," & _
        "     P.DOB, E.ADMISSION_DATETIME, E.REGISTRATION_DATETIME" & _
        " FROM (" & _
        "     SELECT DISTINCT ELLIST.WORKING_UNIT_ID as UNIT_ID, ELLIST.ENCOUNTER_ID" & _
        "     FROM ENCOUNTER_LOCATION AS ELLIST" & _
        "     INNER JOIN UNIT ON (UNIT.UNIT_ID = ELLIST.WORKING_UNIT_ID)" & _
        "     WHERE UNIT.USE_TRANSPARENT_CLASSIFICATION='Y'" & _
        "     AND (ELLIST.EFFECTIVE_DATETIME_IN BETWEEN " & g_dbutil.SQL_DateTime(g_pull_start) & " AND " & g_dbutil.SQL_DateTime(g_pull_finish) & " OR " & _
        "          ELLIST.EFFECTIVE_DATETIME_OUT BETWEEN " & g_dbutil.SQL_DateTime(g_pull_start) & " AND " & g_dbutil.SQL_DateTime(g_pull_finish) & " OR " & _
        "          (ELLIST.EFFECTIVE_DATETIME_IN <" & g_dbutil.SQL_DateTime(g_pull_start) & " AND " & _
        "           ELLIST.EFFECTIVE_DATETIME_OUT >" & g_dbutil.SQL_DateTime(g_pull_finish) & "))" & _
        " ) AS LIST"
    sql = sql & _
        " INNER JOIN UNIT ON (UNIT.UNIT_ID = LIST.UNIT_ID)" & _
        " INNER JOIN UNIT_PARAM AS PARAM ON (PARAM.UNIT_ID = UNIT.UNIT_ID) AND (" & g_dbutil.SQL_DateTime(g_pull_finish) & " BETWEEN PARAM.EFFECTIVE_DATETIME AND PARAM.EXPIRATION_DATETIME)" & _
        " INNER JOIN ENCOUNTER AS E ON (E.ENCOUNTER_ID = LIST.ENCOUNTER_ID)" & _
        " INNER JOIN PERSON AS P ON (P.PERSON_ID = E.PERSON_ID)" & _
        " LEFT JOIN ENCOUNTER_LOCATION AS EL ON (EL.ENCOUNTER_ID = E.ENCOUNTER_ID) AND (" & g_dbutil.SQL_DateTime(g_pull_finish) & " BETWEEN EL.EFFECTIVE_DATETIME_IN AND EL.EFFECTIVE_DATETIME_OUT)" & _
        " INNER JOIN (" & _
        "    SELECT WORKING_UNIT_ID AS UNIT_ID, ENCOUNTER_ID, MAX(DATETIME_IN) AS DATETIME_IN" & _
        "    FROM ENCOUNTER_LOCATION AS EL" & _
        "    WHERE WORKING_UNIT_ID IN (SELECT UNIT_ID FROM UNIT WHERE USE_TRANSPARENT_CLASSIFICATION='Y')" & _
        "    AND DATETIME_IN < " & g_dbutil.SQL_DateTime(g_pull_finish) & _
        "    AND IS_TRANSFER_IN='Y'" & _
        "    GROUP BY WORKING_UNIT_ID, ENCOUNTER_ID" & _
        " ) AS ARRIVE ON (ARRIVE.UNIT_ID = UNIT.UNIT_ID) AND (ARRIVE.ENCOUNTER_ID = E.ENCOUNTER_ID)" & _
        " LEFT JOIN (" & _
        "    SELECT WORKING_UNIT_ID AS UNIT_ID, ENCOUNTER_ID, MAX(DATETIME_OUT) AS DATETIME_OUT" & _
        "    FROM ENCOUNTER_LOCATION AS EL" & _
        "    WHERE WORKING_UNIT_ID IN (SELECT UNIT_ID FROM UNIT WHERE USE_TRANSPARENT_CLASSIFICATION='Y')" & _
        "    AND DATETIME_OUT < " & g_dbutil.SQL_DateTime(g_pull_finish) & _
        "    AND ((NEXT_WORKING_UNIT_ID<>WORKING_UNIT_ID) OR (NEXT_WORKING_UNIT_ID IS NULL))" & _
        "    GROUP BY WORKING_UNIT_ID, ENCOUNTER_ID" & _
        " ) AS DEPART ON (DEPART.UNIT_ID = UNIT.UNIT_ID) AND (DEPART.ENCOUNTER_ID = E.ENCOUNTER_ID)" & _
        " WHERE 1=1"
    
    If Len(debug_acct) Then
        sql = sql & " AND E.ACCT_NUMBER=" & g_dbutil.SQL_String(debug_acct)
    End If
    If Len(debug_unit) Then
        sql = sql & " AND UNIT.NAME=" & g_dbutil.SQL_String(debug_unit)
    End If
    If (debug_unit_id > 0) Then
        sql = sql & " AND UNIT.UNIT_ID=" & debug_unit_id
    End If
    sql = sql & " ORDER BY LIST.ENCOUNTER_ID"
    
'    Debug.Print sql
'    dvprint sql
    rs.Open sql, g_cnADO
    count = 0

    Do While Not rs.EOF
        'Package the patient info (don't use unit/encounter objects)
        g_verbose_audit = ""
        g_brief_audit = ""
    dprint "TRANSPARENT MAPPING AUDIT                     Time=" & nowdt
    dprint "Pull at:   " & g_pull_finish & " (back to " & g_pull_start & "; range=" & g_range & ")"
    dprint "Effective: " & g_effdt
        With pat
            .unit_id = rs("UNIT_ID")
            .encounter_id = rs("ENCOUNTER_ID")
            .meth_id = rs("METHODOLOGY_ID")
            .acct = rs("ACCT_NUMBER")
            .last_name = rs("LAST_NAME") & ""
            .first_name = rs("FIRST_NAME") & ""
            .unit_name = rs("UNIT") & ""
            .room = rs("ROOM") & ""
            .bed = rs("BED") & ""
            
            If Not IsNull(rs("DOB")) And Not IsNull(rs("ADMISSION_DATETIME")) Then
                .age = DateDiff("n", rs("DOB"), rs("ADMISSION_DATETIME")) / 60# / 24# / 365.24219
            ElseIf Not IsNull(rs("DOB")) And Not IsNull(rs("REGISTRATION_DATETIME")) Then
                .age = DateDiff("n", rs("DOB"), rs("REGISTRATION_DATETIME")) / 60# / 24# / 365.24219
            Else
                .age = 0
            End If
            
            .unit_arrival = rs("UNIT_ARRIVAL")
            .effective = g_util.Max(g_effdt, .unit_arrival)
            .pull_start = g_pull_start  'g_util.Max(g_pull_start, .unit_arrival)
            .pull_finish = g_pull_finish  'util.Min(g_pull_finish, rs("UNIT_DEPARTURE"))
            If (.pull_finish < .pull_start) Then
                .pull_finish = g_pull_finish
            End If
            .range = DateDiff("n", .pull_start, .pull_finish)
            .TC_source_id = 1 'g_dbutil.DBToInteger(rs("TC_SOURCE_ID"))
            .num_loa = 0
            .num_loc = 0
            .num_class = 0
'            MsgBox "m_pat.pull_start=" & .pull_start
'            MsgBox "m_pat.pull_finish=" & .pull_finish
'            MsgBox "m_pat.range=" & .range
'            .intloa_startdt = 0
'            .intloa_finishdt = 0
'            .outdt = 0
        End With
        
        'Save ids for log entries
        gLogUnitID = pat.unit_id
        gLogEncounterID = pat.encounter_id
        
    If (pat.encounter_id <> prev_encid) Then 'proceed
        prev_encid = pat.encounter_id
    
        'Process this patient
        count = count + 1
        frmMain.SetProgress "Processing patient " & count & " of " & rs.RecordCount
        
        dprint String$(80, "=")
        dprint "Patient " & count & " of " & rs.RecordCount & ": " & _
            pat.last_name & ", " & pat.first_name & _
            " in " & pat.unit_name & " " & pat.room & " " & pat.bed & _
            " acct=" & pat.acct & " age=" & Round(pat.age, 2)
        dprint ""
'        dprint "Here from " & pat.pull_start & " to " & pat.pull_finish & _
'            "  (LOS=" & Round(pat.range / 60, 2) & ")"

        If called_from_server Then
            suppress_class = False
            'pat.effective = g_pull_start
'            pat.outdt = g_pull_finish
'            dvprint "g_pull_start=" & g_dbutil.SQL_DateTime(g_pull_start)
'            dvprint "g_pull_finish=" & g_dbutil.SQL_DateTime(g_pull_finish)
'            dvprint "pat.effective=" & g_dbutil.SQL_DateTime(pat.effective)
'            dvprint "pat.pull_start=" & g_dbutil.SQL_DateTime(pat.pull_start)
'            dvprint "pat.pull_finish=" & g_dbutil.SQL_DateTime(pat.pull_finish)
'            dvprint "pat.outdt=" & g_dbutil.SQL_DateTime(pat.outdt)
'            If LOAForEntireClassRange(pat, i) Then
'                suppress_class = True
'                CreateDNCForEntireRange pat, i
'                'is there another dnc that overlaps with this one?
'                '     Then it"s b/c there was xfer to another unit.
'                '     End that dnc at the new location intime and modify the chart_item StartLOA record.
'                dvprint "DNC-Suppressing classifications for patient #" & count
'            End If
        End If
        GetAllLocationsForPeriod pat
        SetLOARanges pat
        OutlineClassifications pat
        If called_from_server Then PrintClassifications pat
        
    Else
        suppress_class = True
    End If ' prev_encid
           
        If Not suppress_class Then
            'Most sites will have one source of chart items
            'If there are multiple sources that need different mapping, this is the place to do it
            Select Case pat.TC_source_id
    '        Case 123
    '            Select Case pat.meth_id
    '            Case METH_ID_INPATIENT
    '                ProcessInpatient_123 pat
    '            Case Else
    '                LogWarning "Methodology " & pat.meth_id & " in unit " & pat.unit_name & " is not supported"
    '            End Select
            
            Case Else
    
                Select Case pat.meth_id
                Case 18, 20
                    ProcessInpatient pat
                Case 17
                    ProcessMentalHealth pat
                Case Else
                    LogWarning "Methodology " & pat.meth_id & " in unit " & pat.unit_name & " is not supported"
                End Select
    
            End Select
        
        End If 'suppress_class
        
        gLogUnitID = 0
        gLogEncounterID = 0

        If g_abort Then Exit Do
        
        rs.MoveNext
    Loop
    
    rs.Close
    
    If (count < 1) Then
        dprint ""
        If Len(debug_acct) Then
            LogWarning "The selected patient has no chart items in the given time range", EVENT_CATEGORY_UNEXPECTED
        Else
            LogWarning "No chart items found to process - have the unit(s) been enabled for transparent classification?", EVENT_CATEGORY_UNEXPECTED
        End If
    End If
    
    Exit Sub

errHandler:
    g_util.ThrowError "Process"
    Resume  'debug
End Sub

'debug print
Public Sub dprint(s As String)
    
'    Debug.Print s                           'add to debug window
    g_verbose_audit = g_verbose_audit & s & vbCrLf
    g_brief_audit = g_brief_audit & s & vbCrLf
    'It would be nice if we could print to the cmd window, but it takes some effort

    If dbugfile <> 0 Then
        Print #dbugfile, s                  'add to debug log file
    End If
End Sub

'debug verbose print
Public Sub dvprint(s As String)
    g_verbose_audit = g_verbose_audit & s & vbCrLf
    'If g_verbose Then dprint s
    If dbugfile <> 0 Then
        Print #dbugfile, s                  'add to debug log file
    End If
End Sub

Private Sub OpenOutputFiles()
    On Error GoTo errHandler
    
    If Not g_util.GetKeyValue(HKEY_LOCAL_MACHINE, "SOFTWARE\Quadramed\WinPFS", "Directory", winpfspath) Then
        winpfspath = App.Path & "\.."
    End If

    winlogpath = winpfspath & "\log"
    winloadpath = winpfspath & "\load_me"
    If Not g_util.DirExists(winlogpath) Then
        MkDir$ winlogpath
    End If

    'dbugname = winlogpath & "\" & TRANSP_DEBUG & Format$(nowdt, "mmdd") & ".log"
    If called_from_server Then
        dbugname = winlogpath & "\" & Format$(nowdt, "mmddhhnnss") & TRANSP_AUDIT_LOG
    Else
        dbugname = winlogpath & "\" & TRANSP_AUDIT_LOG
    End If
    
    If g_debug Then
        dbugfile = FreeFile
        Open dbugname For Output As #dbugfile                   'overwrite existing
        Print #dbugfile, String(80, "=")
        Print #dbugfile, "TRANSPARENT MAPPING AUDIT FILE                       Time=" & nowdt
    End If
    
    If g_no_output Then Exit Sub

    outfilename = winloadpath & "\" & TRANSP_FILENAME           'load_me\transparent.txt
    outfile = FreeFile
    Open outfilename For Append As #outfile                     'add to existing
    
    Exit Sub

errHandler:
    g_util.ThrowError "OpenOutputFiles"
    Resume  'debug
End Sub
Private Sub CloseOutputFiles()
    If (outfile <> 0) Then Close #outfile
    
    If (dbugfile <> 0) Then
        Close #dbugfile
        dbugfile = 0                                            'stop writing to debug file
    End If
End Sub

'Private Function ConvertDate(d As String) As String
'
''2012-07-10 13:53:00.000
'
'    ConvertDate = Mid$(d, 1, 4) & Mid$(d, 6, 2) & Mid$(d, 9, 2) & Mid$(d, 12, 2) & Mid$(d, 15, 2) & Mid$(d, 18, 2)
'End Function


Private Sub DeleteOldLogs()
    'There are no more log files; history has been added to the event log
End Sub

Private Sub DeleteOldChartItems()
    On Error GoTo errHandler
    
    Dim sql As String
    Dim dt As Date

    dt = DateAdd("d", -CHART_ITEM_LIFE, Date)
    sql = "DELETE FROM CHART_ITEM WHERE EVENT_DATETIME < " & g_dbutil.SQL_DateTime(dt)
    sql = sql & " and (order_control is null or order_control='' or order_control='X')"
    'Debug.Print sql
    g_cnADO.Execute sql
    Exit Sub
    
errHandler:
    g_util.ThrowError "DeleteOldChartItems"
    Resume  'debug
End Sub


Public Sub AddLogEntry(event_type As EventLogType, msg As String, _
    Optional event_category As EventLogCategory = EVENT_CATEGORY_NONE _
)
    On Error GoTo errHandler

    dprint msg                      'copy to debug log

    g_event.AddEventLogEntry EVENT_SOURCE_TRANSPARENT_MAPPING, event_type, event_category, _
        msg, gLogSourceText, 0, gLogUnitID, gLogEncounterID
    Exit Sub
    
errHandler:
    'no message boxes allowed (this is a command-line program)
    Debug.Print Err.Description
End Sub

Public Sub LogInfo(s As String, Optional event_category As EventLogCategory = EVENT_CATEGORY_NONE)
    AddLogEntry EVENT_TYPE_INFO, s, event_category
End Sub

Public Sub LogWarning(s As String, Optional event_category As EventLogCategory = EVENT_CATEGORY_VALIDATION)
    AddLogEntry EVENT_TYPE_WARNING, s, event_category
End Sub

Public Sub LogError(s As String, Optional event_category As EventLogCategory = EVENT_CATEGORY_VALIDATION)
    AddLogEntry EVENT_TYPE_ERROR, s, event_category
End Sub

'Private Sub ProcessDNC()
'    On Error GoTo errHandler
'
'    Dim pat As PatientInfo
'    Dim rs As New Recordset
'    Dim rs2 As New Recordset
'    Dim sql As String, sql2 As String, sql3 As String
'    Dim count As Integer
'    Dim indtstr As String, outdtstr As String
'    Dim nowdate As String, nowtime As String
'    Dim minfinishdt As Date
'
'    'Get the Latest Start_LOA - assume every start_loa has a finish
'    sql = "select encounter_id,event_datetime,unit_id from chart_item where timestamp>dateadd(dd,-30,getdate()) and code='@start_loa'"
'    sql = sql & " and order_control<>'X' order by encounter_id,event_datetime,unit_id"
'    rs.Open sql, g_cnADO, adOpenDynamic, adLockOptimistic
'    count = 0
'
'    Do While Not rs.EOF
'        'Package the patient info (don't use unit/encounter objects)
'
'        'Save ids for log entries
'        gLogUnitID = rs(2)
'        gLogEncounterID = rs(0)
'
'        'Process this patient
'        count = count + 1
'        frmMain.SetProgress "Processing DNC patient " & count & " of " & rs.RecordCount
'
'        indtstr = Format$(rs(1), "yyyymmdd") & Format$(rs(1), "hhnn")
'
'        'Look for the finish for this start
'        sql2 = "select min(event_datetime) from chart_item where encounter_id=" & gLogEncounterID
'        sql2 = sql2 & " and code='@finish_loa' and event_datetime>=" & g_dbutil.SQL_DateTime(rs(1))
'        sql2 = sql2 & " and unit_id=" & gLogUnitID & " and order_control<>'X'"
'        rs2.Open sql2, g_cnADO
'
'        If IsNull(rs2(0)) Then 'no finish
'            'create the DNC using rs(1) as the InTime and 7am as the OutTime
'            'dateadd(dd,1,rs(1))
'            nowdate = Format$(nowdt, "yyyymmdd")
'            nowtime = Format$(nowdt, "hhnn")
'
'            If nowtime >= "0700" Then 'it is between 07:00 and 23:59. make the date tomorrow
'                outdtstr = Format$(DateAdd("d", 1, nowdt), "yyyymmdd") & "0700"
'            Else 'it is betwn 00:00 and 06:59.  make the date today
'                outdtstr = nowdate & "0700"
'            End If
'        Else 'found a match so get rid of these records
'            'create the DNC using rs(1) as the InTime and rs2(0) as the OutTime
'            minfinishdt = rs2(0)
'            outdtstr = Format$(minfinishdt, "yyyymmdd") & Format$(minfinishdt, "hhnn")
'
'            sql3 = "update chart_item set order_control='X' where encounter_id=" & gLogEncounterID
'            sql3 = sql3 & " and code='@start_loa' and event_datetime=" & g_dbutil.SQL_DateTime(rs(1))
'            sql3 = sql3 & " and unit_id=" & gLogUnitID
'            g_cnADO.Execute sql3
'            sql3 = "update chart_item set order_control='X' where encounter_id=" & gLogEncounterID
'            sql3 = sql3 & " and code='@finish_loa' and event_datetime=" & g_dbutil.SQL_DateTime(minfinishdt)
'            sql3 = sql3 & " and unit_id=" & gLogUnitID
'            g_cnADO.Execute sql3
'        End If
'        rs2.Close
'
'        OutputDNC rs(0), rs(2), indtstr, outdtstr  'encid, unitid, in, out
'
'        rs.MoveNext
'    Loop
'
'    rs.Close
'
'    Exit Sub
'errHandler:
'    Debug.Print Err.Description
'End Sub

'PROCESSDNC logic
'all patients in last 30 days with loa
'
'select encounter_id,event_datetime from chart_item where timestamp>dateadd(dd,-30,getdate()) and code='@start_loa'
'
'
'    select from chart_item where encounter_id=rs(0) and code='@finish_loa' and event_datetime>rs(1)
'
'    if any of these don't have a finish_loa then continue dnc
'
'        if record count = 0 then continue dnc
'
'        if record count
'
'if any of these have a finish_loa and then create dnc

Private Sub CreateDNCForEntireRange(pat As PatientInfo, idx As Integer)
    Dim indtstr As String
    Dim outdtstr As String
    Dim indt As Date, outdt As Date, newindt As Date
    Dim classid As Long, newunitid As Long, UnitID As Long
    
        indt = pat.loa(idx).startdt
        UnitID = pat.unit_id
        If AnyOverlappingDNCExists(pat.encounter_id, indt, classid) Then
            dvprint "AnyDNC classid=" & classid
            'get datetime in of new location pat.unit_id
            LocDTIn pat.encounter_id, indt, newindt, newunitid
            'end the dnc that was found by AnyDNCexists, at newindt
            UpdateDNCOutTime classid, indt, newindt   ' newindt is the outtime of the dnc
            'Modify StartLOA chart_item time from pat.loa(idx).startdt to newindt
            UpdateStartLOA pat.encounter_id, indt, newindt 'indt is the original dncstart
            'Remove pat.loa(idx) and adjust num loa in ary.
            pat.loa(idx).Cancel = True
            CrunchLOA pat
            
            indt = newindt
            UnitID = newunitid
        End If
        
        indtstr = Format$(indt, "yyyymmddhhnn")
        'create the DNC using rs(1) as the InTime and 7am as the OutTime
        'dateadd(dd,1,rs(1))
        If pat.loa(idx).enddt = 0 Then
            outdt = pat.pull_finish
        Else
            outdt = pat.loa(idx).enddt
        End If
        outdtstr = Format$(outdt, "yyyymmddhhnn")
        dvprint "Adding DNC record: In=" & indtstr & " Out=" & outdtstr
        OutputDNC pat.encounter_id, UnitID, indt, outdt  'encid, unitid, newin, out
            

End Sub
Private Sub UpdateStartLOA(encid As Long, olddt As Date, newindt As Date)
    Dim sql As String
    
    sql = "update chart_item set event_datetime=" & g_dbutil.SQL_DateTime(newindt)
    sql = sql & " where encounter_id=" & encid
    sql = sql & " and event_datetime=" & g_dbutil.SQL_DateTime(olddt)
dvprint "UpdateStartLOA"
dvprint sql
    g_cnADO.Execute sql
End Sub

Private Sub UpdateDNCOutTime(classid As Long, indt As Date, outdt As Date)
    Dim sql As String
    
    sql = "update classification_event set datetime_out=" & g_dbutil.SQL_DateTime(outdt)
    sql = sql & ",effective_datetime_out=" & g_dbutil.SQL_DateTime(outdt)
    sql = sql & ",report_date2=" & g_dbutil.SQL_Date(outdt)
    sql = sql & ",los_hours=" & DateDiff("n", indt, outdt) / 60#
    sql = sql & ",finish_date=" & g_dbutil.SQL_Date(outdt)
    sql = sql & " where classification_event_id=" & classid
dvprint "UpdateDNCOutTime"
dvprint sql
    g_cnADO.Execute sql

End Sub
Private Sub LocDTIn(encid As Long, indt As Date, newindt As Date, newunitid As Long)
    Dim rs As New Recordset
    Dim sql As String
    
    newindt = 0
    sql = "select min(effective_datetime_in) from encounter_location where encounter_id=" & encid
'    sql = sql & " and working_unit_id=" & UnitID
    sql = sql & " and effective_datetime_in>=" & g_dbutil.SQL_DateTime(indt)
'dvprint "LocDTIn"
'dvprint sql
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        If Not IsNull(rs(0)) Then
            newindt = rs(0)
        End If
    End If
    rs.Close
    
    If newindt <> 0 Then
        sql = "select working_unit_id from encounter_location where encounter_id=" & encid
        sql = sql & " and effective_datetime_in=" & g_dbutil.SQL_DateTime(newindt)
        rs.Open sql, g_cnADO
        If Not rs.EOF Then newunitid = rs(0)
        rs.Close
    End If

End Sub
Private Function AnyOverlappingDNCExists(encid As Long, indt As Date, classid As Long) As Boolean
    Dim rs As New Recordset
    Dim sql As String
    Dim origIn As Date, origOut As Date
'This needs to be refined to only return TRUE overlaps, not qhere datetime in just equals.
    sql = "select classification_event_id,datetime_in,effective_datetime_out from classification_event where encounter_id=" & encid
    sql = sql & " and methodology_id=-1"
    sql = sql & " and datetime_in=" & g_dbutil.SQL_DateTime(indt)
dvprint "AnyDNC exists"
dvprint sql
    rs.Open sql, g_cnADO
    If rs.EOF Then
        AnyOverlappingDNCExists = False
        classid = 0
    Else
        If IsNull(rs(0)) Then
            AnyOverlappingDNCExists = False
            classid = 0
        Else
            classid = rs(0)
            origIn = rs(1)
            origOut = rs(2)
            AnyOverlappingDNCExists = (CheckOverlapDNC(encid, classid, origIn, origOut) > 0)
        End If
    End If
    rs.Close
dvprint "AnyDNC going to return class id=" & classid

End Function
Private Function CheckOverlapDNC(encid As Long, classid As Long, indt As Date, outdt As Date) As Long
    Dim rs As New Recordset
    Dim sql As String
'This needs to be refined to only return TRUE overlaps, not qhere datetime in just equals.
    sql = "select classification_event_id from classification_event where encounter_id=" & encid
    sql = sql & " and methodology_id=-1 and classification_event_id<>" & classid
    sql = sql & " and ((datetime_in>" & g_dbutil.SQL_DateTime(indt) & " and datetime_in<" & g_dbutil.SQL_DateTime(outdt) & ") "
    sql = sql & " or (datetime_out>" & g_dbutil.SQL_DateTime(indt) & " and datetime_out<" & g_dbutil.SQL_DateTime(outdt) & ") "
    sql = sql & " or (datetime_in<=" & g_dbutil.SQL_DateTime(indt) & " and datetime_out>=" & g_dbutil.SQL_DateTime(outdt) & "))"
dvprint "CheckOverlapDNC"
dvprint sql
    rs.Open sql, g_cnADO
    If rs.EOF Then
        CheckOverlapDNC = 0
    Else
        If IsNull(rs(0)) Then
            CheckOverlapDNC = 0
        Else
            CheckOverlapDNC = rs(0)
        End If
    End If
    rs.Close
'dvprint "CheckOverlapDNC going to return class id=" & rs(0)

End Function
Private Function DNCExists(encid As Long, UnitID As Long, indt As Date) As Boolean
    Dim rs As New Recordset
    Dim sql As String
    
    sql = "select count(*) from classification_event where encounter_id=" & encid
    sql = sql & " and unit_id=" & UnitID & " and methodology_id=-1"
    sql = sql & " and datetime_in=" & g_dbutil.SQL_DateTime(indt)
dvprint "DNC exists"
dvprint sql
    rs.Open sql, g_cnADO
    DNCExists = (rs(0) > 0)
    rs.Close

End Function
Private Sub DNCUpdate(encid As Long, UnitID As Long, indt As Date, outdt As Date)
    Dim sql As String
    
    sql = "update classification_event set datetime_out=" & g_dbutil.SQL_DateTime(outdt)
    sql = sql & " ,effective_datetime_out=" & g_dbutil.SQL_DateTime(outdt)
    sql = sql & " ,report_date2=" & g_dbutil.SQL_Date(outdt)
    sql = sql & " ,los_hours=" & DateDiff("n", indt, outdt) / 60#
    sql = sql & " ,finish_date=" & g_dbutil.SQL_Date(outdt)
    sql = sql & " where encounter_id=" & encid
    sql = sql & " and unit_id=" & UnitID & " and methodology_id=-1"
    sql = sql & " and datetime_in=" & g_dbutil.SQL_DateTime(indt)
dvprint "DNC Update"
dvprint sql
    g_cnADO.Execute sql

End Sub
Public Sub OutputDNC(encid As Long, UnitID As Long, indt As Date, outdt As Date)
    Dim dncstr As String
    Dim rs As New Recordset
    Dim sql As String
    Dim acct As String
    Dim uname As String, indtstr As String, outdtstr As String
    
    indtstr = Format$(indt, "yyyymmddhhnn")
    outdtstr = Format$(outdt, "yyyymmddhhnn")
    
'    If DNCExists(encid, UnitID, indt) Then
'        DNCUpdate encid, UnitID, indt, outdt
'        Exit Sub
'    End If
    
    sql = "select acct_number from encounter where encounter_id=" & encid
    rs.Open sql, g_cnADO
    If Not rs.EOF Then acct = rs(0)
    rs.Close
    
    sql = "select name from unit where unit_id=" & UnitID
    rs.Open sql, g_cnADO
    If Not rs.EOF Then uname = rs(0)
    rs.Close
    
    dncstr = g_util.FixedWidth("", 8)                                       '(facility code)
    dncstr = dncstr & "|" & g_util.FixedWidth(uname, 16)
    dncstr = dncstr & "|" & g_util.FixedWidth("", 16)                       '(unit code)
    dncstr = dncstr & "|" & g_util.FixedWidth("", 16)                       '(area code)
    dncstr = dncstr & "|" & g_util.FixedWidth("", 8)                        '(class date - give datetime instead)
    dncstr = dncstr & "|" & g_util.FixedWidth(acct, 20)
    dncstr = dncstr & "|" & g_util.FixedWidth("", 32)
    dncstr = dncstr & "|" & g_util.FixedWidth("", 32)
    dncstr = dncstr & "|" & g_util.FixedWidth("", 32)                       '(middle)
    dncstr = dncstr & "|" & g_util.FixedWidth("", 8)
    dncstr = dncstr & "|" & g_util.FixedWidth("", 4)
    dncstr = dncstr & "|" & indtstr                                        'class datetime
    dncstr = dncstr & "|" & g_util.FixedWidth("", 16)                       '(login)
    dncstr = dncstr & "|" & g_util.FixedWidth("", 16)                       '(employee)
    dncstr = dncstr & "|" & g_util.FixedWidth("", 4)
    dncstr = dncstr & "|" & g_util.FixedWidth("D", 1)                       'record type = DNC
    dncstr = dncstr & "|" & g_util.FixedWidth("", 4)                        '(stage)
    dncstr = dncstr & "|" & g_util.FixedWidth("1", 4)              'TC source ID
    dncstr = dncstr & "|" & g_util.FixedWidth("", 4)               'TC pull range
    dncstr = dncstr & "|"
    dncstr = g_util.FixedWidth(dncstr, 294)
    dncstr = dncstr & "|" & indtstr                                         'IN
    dncstr = g_util.FixedWidth(dncstr, 346)
    dncstr = dncstr & "|" & outdtstr                                        'OUT
    dncstr = g_util.FixedWidth(dncstr, 377)
    dncstr = dncstr & "|NNNNY"  ' on pass indicator 5
    Print #outfile, dncstr
    
    'AddLogEntry EVENT_TYPE_INFO, "DNC: " & "ind 5", EVENT_CATEGORY_PROCESSED


End Sub
'Private Function SetEffectiveTimeWhenDNC(encid As Long, ByRef effdt As Date) As Boolean
'    'if there is a LOA finish between g_pull_start and g_pull_finish
'    ' in order to find the correct IN time
'    ' then set pat.effective = LOA finish
'    On Error GoTo errHandler
'
'    Dim rs As New Recordset
'    Dim sql As String
'
'    sql = "select max(event_datetime) from chart_item where encounter_id=" & encid
'    sql = sql & "  and code='@finish_loa' and event_datetime between " & g_dbutil.SQL_DateTime(g_pull_start) & " and " & g_dbutil.SQL_DateTime(g_pull_finish)
'    rs.Open sql, g_cnADO
'    If Not IsNull(rs(0)) Then
'        effdt = rs(0)
'        SetEffectiveTimeWhenDNC = True
'    End If
'
'    rs.Close
'
'    Exit Function
'errHandler:
'    Debug.Print Err.Description
'End Function
'
'Private Function SetOutTimeWhenDNC(encid As Long, effdt As Date, ByRef outdt As Date) As Boolean
'    'if there is a LOA Start between effective (set by SetEffective) and g_pull_finish
'    ' in order to find the correct OUT time
'    ' then set pat.outdt = LOA Start
'    On Error GoTo errHandler
'
'    Dim rs As New Recordset
'    Dim sql As String
'
'    sql = "select max(event_datetime) from chart_item where encounter_id=" & encid
'    sql = sql & "  and code='@start_loa' and event_datetime between " & g_dbutil.SQL_DateTime(effdt) & " and " & g_dbutil.SQL_DateTime(g_pull_finish)
'    rs.Open sql, g_cnADO
'    If Not IsNull(rs(0)) Then
'        outdt = rs(0)
'        SetOutTimeWhenDNC = True
'    End If
'
'    rs.Close
'
'    Exit Function
'errHandler:
'    Debug.Print Err.Description
'End Function

'Private Function PatientDNCForEntireClassRange(encid As Long, ByRef effdt As Date) As Boolean
'    On Error GoTo errHandler
'
'    Dim rs As New Recordset
'    Dim sql As String
'    Dim loastart As Date, loafinish As Date
'    'takes care of DNC cases 1 and 4
'    '                 --+            --+
'    '   yesterday 7am   | 1            |
'    '                   |              |
'    '                 --+              | 4
'    '                                  |
'    '                 --+              |
'    '                   | 2,2a,2b...   |
'    '                 --+              |
'    '                                  |
'    '                 --+              |
'    '                   | 3            |
'    '  today 7am        |              |
'    '                 --+            --+
'    '
'
'  'case 1 & 4
'
''  ================================================================================
''Patient 8 of 123: WEEKS, JACKSON in 1NW 11676 11676A acct=130424286447 age=11.9
''Here from 4/27/2013 7:00:00 AM to 4/28/2013 7:00:00 AM  (LOS=24)
''g_pull_start='20130427 07:00:00'
''g_pull_finish='20130428 07:00:00'
''pat.effective='20130427 07:00:00'
''pat.outdt='20130428 07:00:00'
''Suppressing classifications for patient #8
''final pat.effective='20130428 09:20:00'
''final pat.intloa_start='18991230 00:00:00'
''final pat.intloa_finish='18991230 00:00:00'
''final pat.outdt='20130428 07:00:00'
'
'    PatientDNCForEntireClassRange = False
'    sql = "select max(event_datetime) from chart_item where encounter_id=" & encid
'    sql = sql & " and order_control<>'X' and code='@start_loa' and event_datetime<=" & g_dbutil.SQL_DateTime(g_pull_start)
'    rs.Open sql, g_cnADO
'    If Not IsNull(rs(0)) Then
'        loastart = rs(0)
'        dvprint "loastart=" & g_dbutil.SQL_DateTime(loastart)
'        rs.Close
'        'inspect the time range between loa start and pull start
'        sql = "select min(event_datetime) from chart_item where encounter_id=" & encid
'        sql = sql & "  and order_control<>'X' and code='@finish_loa' and event_datetime>" & g_dbutil.SQL_DateTime(loastart)
'        rs.Open sql, g_cnADO
'        If Not IsNull(rs(0)) Then
'            loafinish = rs(0)
'            dvprint "loafinish=" & g_dbutil.SQL_DateTime(loafinish)
'            rs.Close
'            If loafinish <= g_pull_start Then Exit Function
'        Else
'            rs.Close
'            Exit Function
'        End If
'
'        sql = "select min(event_datetime) from chart_item where encounter_id=" & encid
'        sql = sql & "  and order_control<>'X' and code='@finish_loa' and event_datetime>" & g_dbutil.SQL_DateTime(g_pull_start)
'        rs.Open sql, g_cnADO
'        If Not IsNull(rs(0)) Then
'            effdt = rs(0)
'            If effdt >= g_pull_finish Then
'                PatientDNCForEntireClassRange = True
'            End If
'        Else 'no finish time yet, so entire range = true
'            PatientDNCForEntireClassRange = True
'        End If
'        rs.Close
'    Else
'        'effdt = g_pull_start
'        rs.Close
'    End If
'
'    Exit Function
'errHandler:
'    Debug.Print Err.Description
'End Function
Private Function LOAForEntireClassRange(pat As PatientInfo, idx As Integer) As Boolean
    Dim i As Integer
    
dvprint "LOAForEntireClassRange: " & pat.num_loa
    For i = 1 To pat.num_loa
dprint "loa start=" & pat.loa(i).startdt & "  loa end=" & pat.loa(i).enddt & "  pullstart=" & pat.pull_start & "  pullfin=" & pat.pull_finish
        If (pat.loa(i).startdt <= pat.pull_start) And (pat.loa(i).enddt >= pat.pull_finish) Then
            LOAForEntireClassRange = True
            idx = i
        End If
    Next i
End Function
'Private Sub CheckDNC23(encid As Long, ByRef startdt As Date, ByRef finishdt As Date, ByRef outdt As Date)
'    On Error GoTo errHandler
'
'    Dim rs As New Recordset
'    Dim sql As String
'    'takes care of DNC cases 1 and 4
'    '                 --+            --+
'    '   yesterday 7am   | 1            |
'    '                   |              |
'    '                 --+              | 4
'    '                                  |
'    '                 --+              |
'    '                   | 2,2a,2b...   |
'    '                 --+              |
'    '                                  |
'    '                 --+              |
'    '                   | 3            |
'    '  today 7am        |              |
'    '                 --+            --+
'    '
'
''case 2 & 3 -- assume a max of 1 internal loa
'    sql = "select min(event_datetime) from chart_item where encounter_id=" & encid
'    sql = sql & " and order_control<>'X' and code='@start_loa' and event_datetime>=" & g_dbutil.SQL_DateTime(g_pull_start)
'    sql = sql & " and event_datetime<=" & g_dbutil.SQL_DateTime(g_pull_finish)
'    rs.Open sql, g_cnADO
'    If Not IsNull(rs(0)) Then
'        startdt = rs(0)
'        rs.Close
'        sql = "select min(event_datetime) from chart_item where encounter_id=" & encid
'        sql = sql & " and order_control<>'X' and code='@finish_loa' and event_datetime>" & g_dbutil.SQL_DateTime(startdt)
'        sql = sql & " and event_datetime<=" & g_dbutil.SQL_DateTime(g_pull_finish)
'        rs.Open sql, g_cnADO
'        If Not IsNull(rs(0)) Then
'            finishdt = rs(0)
'            If finishdt > startdt Then XOutLOA encid, startdt, finishdt
'            If finishdt = g_pull_finish Then 'this is actually case 3
'                outdt = startdt
'                startdt = 0
'                finishdt = 0
'            End If
'        Else 'no finish time yet, so startdt is actually the outtime of class in case 3.
'            outdt = startdt
'            startdt = 0
'            finishdt = 0
'        End If
'        rs.Close
'    Else
'        startdt = 0
'        finishdt = 0
'        rs.Close
'    End If
'
'    Exit Sub
'errHandler:
'    Debug.Print Err.Description
'End Sub

Public Sub XOutLOA(encid As Long, startdt As Date, finishdt As Date)
    Dim sql As String
    
    
    sql = "update chart_item set order_control='X' where encounter_id=" & encid
    sql = sql & " and code='@start_loa' and event_datetime=" & g_dbutil.SQL_DateTime(startdt)
dvprint sql
    g_cnADO.Execute sql
    sql = "update chart_item set order_control='X' where encounter_id=" & encid
    sql = sql & " and code='@finish_loa' and event_datetime=" & g_dbutil.SQL_DateTime(finishdt)
dvprint sql
    g_cnADO.Execute sql
    
    
End Sub
Private Sub SetLOARanges(pat As PatientInfo)
    On Error GoTo errHandler
    
    Dim rs As New Recordset
    Dim rs2 As New Recordset
    Dim sql As String, sql2 As String
    Dim startloa(MAX_LOA) As LOAtypePrecision
    Dim finishloa(MAX_LOA) As LOAtypePrecision
    Dim numstartloa As Integer, numendloa As Integer, i As Integer, j As Integer
    Dim done As Boolean

'there shouldnt be 2 starts in a row
'-- choose the later timestamp and cancel the earlier one
'ONLY check for starts less than the finish time--dont want to do future dncs.
    numstartloa = 0
    sql = "select convert(varchar,timestamp,20) from chart_item where encounter_id=" & pat.encounter_id
    sql = sql & " and order_control<>'X' and code='@start_loa' and event_datetime<" & g_dbutil.SQL_DateTime(g_pull_finish)
    sql = sql & " order by timestamp"
'dvprint sql
    rs.Open sql, g_cnADO
    Do While Not rs.EOF
        numstartloa = numstartloa + 1
        startloa(numstartloa).startdt = rs(0)
        rs.MoveNext
    Loop
    rs.Close
    
    'now cancel the earlier starts
    For i = 1 To numstartloa - 1
        sql = "select count(*) from chart_item where encounter_id=" & pat.encounter_id
        sql = sql & " and order_control<>'X' and code='@finish_loa'"
        sql = sql & " and convert(varchar,timestamp,20) between '" & startloa(i).startdt & "' and '" & startloa(i + 1).startdt & "'"
'dvprint sql
        rs.Open sql, g_cnADO
        If rs(0) = 0 Then 'no finish between the starts-cancel the earlier one
            startloa(i).Cancel = True
        End If
        rs.Close
    Next i

'do the same for finishes
'there shouldnt be 2 finishes in a row
    numendloa = 0
    sql = "select convert(varchar,timestamp,20) from chart_item where encounter_id=" & pat.encounter_id
    sql = sql & " and order_control<>'X' and code='@finish_loa' order by timestamp"
'dvprint sql
    rs.Open sql, g_cnADO
    Do While Not rs.EOF
        numendloa = numendloa + 1
        finishloa(numendloa).enddt = rs(0)
        rs.MoveNext
    Loop
    rs.Close
    
    'now cancel the earlier finishes
    For i = 1 To numendloa - 1
        sql = "select count(*) from chart_item where encounter_id=" & pat.encounter_id
        sql = sql & " and order_control<>'X' and code='@start_loa'"
        sql = sql & " and convert(varchar,timestamp,20) between '" & finishloa(i).enddt & "' and '" & finishloa(i + 1).enddt & "'"
'dvprint sql
        rs.Open sql, g_cnADO
        If rs(0) = 0 Then 'no start between the finishes-cancel the earlier one
            finishloa(i).Cancel = True
        End If
        rs.Close
    Next i
    
'do the actual cancels in the db.
    For i = 1 To numstartloa
        If startloa(i).Cancel Then
            sql = "update chart_item set order_control='X' where encounter_id=" & pat.encounter_id
            sql = sql & " and code='@start_loa' and convert(varchar,timestamp,20)='" & startloa(i).startdt & "'"
'dvprint sql
            g_cnADO.Execute sql
        End If
    Next i

    For i = 1 To numendloa
        If finishloa(i).Cancel Then
            sql = "update chart_item set order_control='X' where encounter_id=" & pat.encounter_id
            sql = sql & " and code='@finish_loa' and convert(varchar,timestamp,20)='" & finishloa(i).enddt & "'"
'dvprint sql
            g_cnADO.Execute sql
        End If
    Next i

'Now there are no consecutive starts and no consec finishes;
'  each start can be paired with its finish if it exists
    sql = "select event_datetime,timestamp from chart_item where encounter_id=" & pat.encounter_id
    sql = sql & " and order_control<>'X' and code='@start_loa' order by timestamp"
'dvprint sql
    rs.Open sql, g_cnADO
    Do While Not rs.EOF
        pat.num_loa = pat.num_loa + 1
        pat.loa(pat.num_loa).startdt = rs(0)
'dvprint "Start " & pat.num_loa & "=" & pat.loa(pat.num_loa).startdt
        sql2 = "select min(event_datetime) from chart_item where encounter_id=" & pat.encounter_id
        sql2 = sql2 & "  and order_control<>'X' and code='@finish_loa' and timestamp>=" & g_dbutil.SQL_DateTime(rs(1))
'dvprint sql2
        rs2.Open sql2, g_cnADO
        If IsNull(rs2(0)) Then
            pat.loa(pat.num_loa).retain = True 'still havent received returnloa
            If pat.loa(pat.num_loa).startdt >= g_pull_finish Then
                pat.loa(pat.num_loa).enddt = DateAdd("h", 24, g_pull_finish)
            Else
                pat.loa(pat.num_loa).enddt = g_pull_finish
            End If
        ElseIf rs2(0) > g_pull_finish Then
            pat.loa(pat.num_loa).retain = True 'want to end the dnc at 7am and have the partial dnc start at 7am tomorrow
            pat.loa(pat.num_loa).enddt = g_pull_finish
        Else
            pat.loa(pat.num_loa).retain = False
            pat.loa(pat.num_loa).enddt = rs2(0)
        End If
'dvprint "Finish " & pat.num_loa & "=" & pat.loa(pat.num_loa).enddt
        rs2.Close
        rs.MoveNext
    Loop
    rs.Close
    
    'now reconcile the pairs
    'cancel the pairs where start = finish
    For i = 1 To pat.num_loa
        If pat.loa(i).startdt = pat.loa(i).enddt Then
            pat.loa(i).Cancel = True
            sql = "update chart_item set order_control='X' where encounter_id=" & pat.encounter_id
            sql = sql & " and code='@start_loa' and event_datetime=" & g_dbutil.SQL_DateTime(pat.loa(i).startdt)
'dvprint sql
            g_cnADO.Execute sql
            sql = "update chart_item set order_control='X' where encounter_id=" & pat.encounter_id
            sql = sql & " and code='@finish_loa' and event_datetime=" & g_dbutil.SQL_DateTime(pat.loa(i).enddt)
'dvprint sql
            g_cnADO.Execute sql
        Else
'            dvprint "StartDNC=" & pat.loa(i).startdt & " EndDNC=" & pat.loa(i).enddt
        End If
    Next i
    
    'now crunch the ary so we dont have cancels
    CrunchLOA pat
    
       
    Exit Sub
errHandler:
    Debug.Print Err.Description
End Sub

Private Sub CrunchLOA(pat As PatientInfo)
    Dim i As Integer
    Dim num_done As Integer
    
'    dvprint "CrunchLOA"
    num_done = 0
    For i = 1 To pat.num_loa
        If Not pat.loa(i).Cancel Then
            If (pat.loa(i).startdt >= g_pull_start And pat.loa(i).startdt <= g_pull_finish) Or _
               (pat.loa(i).enddt >= g_pull_start And pat.loa(i).enddt <= g_pull_finish) Or _
               (pat.loa(i).startdt <= g_pull_start And pat.loa(i).enddt >= g_pull_finish) Then
                num_done = num_done + 1
                pat.loa(num_done) = pat.loa(i)
    dvprint "Pass/DNC: " & g_dbutil.SQL_DateTime(pat.loa(num_done).startdt) & g_dbutil.SQL_DateTime(pat.loa(num_done).enddt)
            End If
        End If
    Next i
    pat.num_loa = num_done
    
'    dvprint "END CrunchLOA"

End Sub

Private Sub GetAllLocationsForPeriod(pat As PatientInfo)
    Dim rs As New Recordset
    Dim sql As String
    Dim num_removed As Integer, lastgoodidx As Integer, i As Integer
    Dim num_done As Integer, lastadj As Integer

    sql = "select working_unit_id,effective_datetime_in,effective_datetime_out from encounter_location where encounter_id=" & pat.encounter_id
    sql = sql & " and (effective_datetime_out between " & g_dbutil.SQL_DateTime(g_pull_start) & " and " & g_dbutil.SQL_DateTime(g_pull_finish)
    sql = sql & " or effective_datetime_in between " & g_dbutil.SQL_DateTime(g_pull_start) & " and " & g_dbutil.SQL_DateTime(g_pull_finish)
    sql = sql & " or (effective_datetime_in <= " & g_dbutil.SQL_DateTime(g_pull_start) & " and effective_datetime_out >= " & g_dbutil.SQL_DateTime(g_pull_finish) & "))"
    sql = sql & " order by effective_datetime_in"
'dvprint sql
    pat.num_loc = 0
    rs.Open sql, g_cnADO
    Do While Not rs.EOF
        pat.num_loc = pat.num_loc + 1
        pat.loc(pat.num_loc).UnitID = rs(0)
        If pat.num_loc = 1 Then
            If rs(1) <= g_pull_start Then
                pat.loc(pat.num_loc).indt = g_pull_start
            Else
                pat.loc(pat.num_loc).indt = rs(1)
            End If
        Else
            pat.loc(pat.num_loc).indt = rs(1)
        End If
        If rs(2) <= g_pull_finish Then
            pat.loc(pat.num_loc).outdt = rs(2)
        Else
            pat.loc(pat.num_loc).outdt = g_pull_finish
        End If
'        dvprint pat.loc(pat.num_loc).UnitID & ": " & g_dbutil.SQL_DateTime(pat.loc(pat.num_loc).indt) & g_dbutil.SQL_DateTime(pat.loc(pat.num_loc).outdt)
        rs.MoveNext
    Loop
    rs.Close
    
    num_removed = 0
    'where there are adjacent same-locations, mark the duplicates as remove.
    For i = 1 To pat.num_loc
        pat.loc(i).remove = False
        If i >= 2 Then
        If pat.loc(i - 1).UnitID = pat.loc(i).UnitID Then
            pat.loc(i).remove = True
            num_removed = num_removed + 1
'            dvprint "Remove: " & i
        End If
        End If
    Next i
    
    'where there are several adjacent same-unit locations, need to combine them into 1 loc record.
    '  make the outdt of that record be the latest outdt
'1011  --    0731  1:1/1
'-1    0731  1301  2:2/'911   1301  1345  3: 3/3
'911   1345  ++    R  4: 3/3  3=4

    If pat.num_loc > 1 Then
        For i = 1 To pat.num_loc
            If Not pat.loc(i).remove Then
                lastgoodidx = i
                lastadj = i
            End If
            If i > 1 Then
                If pat.loc(i).remove Then
                    If lastadj = i - 1 Then
                        pat.loc(lastgoodidx).outdt = pat.loc(i).outdt
                        lastadj = i
'                        dvprint "change outdt of: " & lastgoodidx & " to " & i & ": " & g_dbutil.SQL_DateTime(pat.loc(i).outdt)
                    End If
                End If
            End If
        Next i
    End If
    
    num_done = 0
    'Finally remove all the duplicates (remove=true)
    '1=1,2=2,3=3
    For i = 1 To pat.num_loc
        If Not pat.loc(i).remove Then
            num_done = num_done + 1
            pat.loc(num_done) = pat.loc(i)
' dvprint pat.loc(num_done).UnitID & ": " & g_dbutil.SQL_DateTime(pat.loc(num_done).indt) & g_dbutil.SQL_DateTime(pat.loc(num_done).outdt)
        End If
    Next i
    pat.num_loc = pat.num_loc - num_removed
    
    'print the locations for debug
    For i = 1 To pat.num_loc
        dvprint "Loc " & i & ": " & pat.loc(i).UnitID & " in=" & g_dbutil.SQL_DateTime(pat.loc(i).indt) & " out=" & g_dbutil.SQL_DateTime(pat.loc(i).outdt)
    Next i
    
End Sub

Private Sub AddClass(src As Integer, pat As PatientInfo, isdnc As Boolean, UnitID As Long, indt As Date, outdt As Date)
    pat.num_class = pat.num_class + 1
    pat.Class(pat.num_class).UnitID = UnitID
    pat.Class(pat.num_class).indt = indt
    pat.Class(pat.num_class).outdt = outdt
    pat.Class(pat.num_class).is_dnc = isdnc
    pat.Class(pat.num_class).source = src
End Sub
Private Sub PrintClassifications(pat As PatientInfo)
    Dim i As Integer
    
    With pat
    For i = 1 To .num_class
        dvprint "Class: " & i & " src=" & .Class(i).source & " isdnc=" & .Class(i).is_dnc & " unit=" & .Class(i).UnitID & " in/out=" & g_dbutil.SQL_DateTime(.Class(i).indt) & g_dbutil.SQL_DateTime(.Class(i).outdt)
    Next i
    End With
    
End Sub
Private Sub OutlineClassifications(pat As PatientInfo)
    Dim i As Integer
    Dim j As Integer
    Dim does_not_intersect As Boolean 'does not intersect with the LOA period
    
    For i = 1 To pat.num_loc
        does_not_intersect = True
        For j = 1 To pat.num_loa
            If (pat.loa(j).startdt >= pat.loc(i).indt And pat.loa(j).startdt <= pat.loc(i).outdt) Or _
               (pat.loa(j).enddt >= pat.loc(i).indt And pat.loa(j).startdt <= pat.loc(i).outdt) Then
                pat.loa(j).locidx = i
                does_not_intersect = False
            Else
                pat.loa(j).locidx = 0
            End If
        Next j
        For j = 1 To pat.num_loa
            If j >= 2 Then
'            if dnc(j-1).inlocidx = i then
'                CLASS = DE(j-1) to DS(j)
                If pat.loa(j - 1).locidx = i Then
                    AddClass 1, pat, False, pat.loc(i).UnitID, _
                             pat.loa(j - 1).enddt, pat.loa(j).startdt
                End If
            End If
            If pat.loa(j).locidx = i Then
'            if DS(j) and DE(j) btwn LS(i) and LE(i) then
'                if (j-1>=1 and dnc(j-1).inlocidx<>i) or (j=1) then
'                    CLASS = LS(i) to DS(j)
'                CLASSDNC = DS(j) to DE(j)
                If (pat.loa(j).startdt >= pat.loc(i).indt And pat.loa(j).startdt <= pat.loc(i).outdt) And _
                   (pat.loa(j).enddt >= pat.loc(i).indt And pat.loa(j).startdt <= pat.loc(i).outdt) Then
                    If (j >= 2 And pat.loa(j - 1).locidx <> i) Or (j = 1) Then
                        AddClass 2, pat, False, pat.loc(i).UnitID, _
                                 pat.loc(i).indt, pat.loa(j).startdt
                    End If
                    AddClass 3, pat, True, pat.loc(i).UnitID, _
                             pat.loa(j).startdt, pat.loa(j).enddt
                ElseIf (pat.loa(j).startdt >= pat.loc(i).indt And pat.loa(j).startdt <= pat.loc(i).outdt) Then
'            elseif DS(j) btwn LS(i) and LE(i) then
'                CLASSDNC = DS(j) to LE(i)
                    AddClass 4, pat, True, pat.loc(i).UnitID, _
                             pat.loa(j).startdt, pat.loc(i).outdt
                ElseIf (pat.loa(j).enddt >= pat.loc(i).indt And pat.loa(j).enddt <= pat.loc(i).outdt) Then
'            elseif DE(j) btwn LS(i) and LE(i) then
'                CLASSDNC = LS(i) to DE(j)
'                CLASS = DE(j) to LE(i)
                    AddClass 5, pat, True, pat.loc(i).UnitID, _
                             pat.loc(i).indt, pat.loa(j).enddt
'                    If (j + 1 <= pat.num_loa) And pat.loa(j + 1).locidx = i Then
'                        'dont add a class here
'                    ElseIf pat.loa(j).enddt < pat.loc(i).outdt Then
'                        AddClass 6, pat, False, pat.loc(i).UnitID, _
'                                 pat.loa(j).enddt, pat.loc(i).outdt
'                    End If
                ElseIf pat.loa(pat.num_loa).startdt <= pat.loc(i).indt And _
                       pat.loa(pat.num_loa).enddt >= pat.loc(i).outdt Then
                    AddClass 7, pat, True, pat.loc(i).UnitID, _
                             pat.loc(i).indt, pat.loc(i).outdt
                End If
            End If
        Next j
        If does_not_intersect Then
            AddClass 8, pat, False, pat.loc(i).UnitID, _
                     pat.loc(i).indt, pat.loc(i).outdt
        ElseIf (pat.num_loa > 0) Then
'    if DE(num dnc) < LE(i) then CLASS = DE(num dnc) to LE(i)
            If pat.loa(pat.num_loa).enddt < pat.loc(i).outdt Then
                AddClass 9, pat, False, pat.loc(i).UnitID, _
                         pat.loa(pat.num_loa).enddt, pat.loc(i).outdt
            End If
        End If
    Next i
End Sub
'Generate Class accounting for DNC and locations
'Setup locations array
'Setup dnc array - adjacent dncs must be combined into 1 dnc record. Ex: if dnc 7-9 and 9-11 then sh be 7-11.
'loop locations i
'    loop dncs j
'        if DS(j) or DE(j) btwn LS(i) and LE(i) then
'        dnc(j).inlocidx = i
'    Else
'        dnc(j).inlocidx = 0
'    Next j
'    loop dncs j
'        If j - 1 >= 1 Then
'            if dnc(j-1).inlocidx = i then
'                CLASS = DE(j-1) to DS(j)
'        If dnc(j).inlocidx = i Then
'            if DS(j) and DE(j) btwn LS(i) and LE(i) then
'                if (j-1>=1 and dnc(j-1).inlocidx<>i) or (j=1) then
'                    CLASS = LS(i) to DS(j)
'                CLASSDNC = DS(j) to DE(j)
'            elseif DS(j) btwn LS(i) and LE(i) then
'                CLASSDNC = DS(j) to LE(i)
'            elseif DE(j) btwn LS(i) and LE(i) then
'                CLASSDNC = LS(i) to DE(j)
'                if (j+1 <= num_dnc) and dnc(j+1).inlocidx=i then dont add a class here.
'                else
'                    CLASS = DE(j) to LE(i)
'           elseif DS(j) < LS(i)
'                CLASSDNC = LS(i) to LE(i)
'        endif idx=i
'    Next j
'    if DE(num dnc) < LE(i) then CLASS = DE(num dnc) to LE(i)
'next i
                
'Examples:
'Locations =
'   Unit A 7am - 11am
'   Unit B 11am - 6pm
'   Unit C 6pm - 7am
'DNCs =
'   9a - 2p
'   8p - 10p
'compare Loc 1 with DNC 1 => Class:  A  7am-9am
'                         => DNC Class: A  9am-11am
'compare Loc 1 with DNC 2 =>
'
'compare Loc 2 with DNC 1 => DNC Class:  B  11am-2pm
'                         => Class:  B  2pm - 6pm
'compare Loc 2 with DNC 2 =>
'
'compare Loc 3 with DNC 1 =>
'compare Loc 3 with DNC 2 => Class:  C  6pm-8pm
'                         => DNC Class:  C  8pm-10pm
'                         => Class:  C 10pm - 7am
'
'
'Locations =
'   Unit A 7am - 11am
'   Unit B 11am - 6pm
'   Unit C 6pm - 7am
'DNCs =
'   9a - 10p
'
'compare Loc 1 with DNC 1 => Class:  A  7am-9am
'                         => DNC Class: A  9am-11am
'compare Loc 1 with DNC 2 =>
'
'compare Loc 2 with DNC 1 => DNC Class:  B  11am-2pm
'                         => Class:  B  2pm - 6pm
'compare Loc 2 with DNC 2 =>
'
'compare Loc 3 with DNC 1 =>
'compare Loc 3 with DNC 2 => Class:  C  6pm-8pm
'                         => DNC Class:  C  8pm-10pm
'                         => Class:  C 10pm - 7am



