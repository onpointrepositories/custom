Attribute VB_Name = "NIHInpatient"
Option Explicit
'
' NIH Inpatient (IP 2.0) [copied from Shands]
'
' This processes one patient using the Inpatient methodology.
'
' All search functions use exact match for code.
'
' result_like looks for LIKE matches in the result, except where EXACT_MATCH_PREFIX is pre-appended to target.
' result_list looks for any one of a list of words exactly as the result.
'
Private Const MAX_INDS = 50
Private Const MAX_PROCS = 48

Private Const EXACT_MATCH_PREFIX = "&!"
Private Const LIKE_PREFIX = "%!"
Private Const CHAR_COMMA = "||"


Private Type indicator_data
    checked     As Boolean
    group       As Integer
    also_mark   As String                   'not used at Arnot (handy with user-defined indicators?)
End Type

Private Type procdata                       'not used at Arnot
    pnum    As Integer
    start   As Date
    finish  As Date
    isvalid As Boolean 'used in combining procs
    pindex As Integer 'used in combining procs = the index of procs whose start/finish times are equal to this one
End Type
Private Type ocdata                       'not used at Arnot
    checked As Boolean
    pnum    As Integer
    start   As String
End Type
Public Type bucket_type
    startdt As Date
    enddt As Date
    count As Integer
End Type
    
Public num_buckets             As Integer
Public bucket(48)             As bucket_type
    

Private inds(MAX_INDS)          As indicator_data

Private numprocs                As Integer
Private numoutcomes             As Integer
Private procs(MAX_PROCS)        As procdata
'Private oc(MAX_PROCS)           As ocdata

Private WHERE_ENCOUNTER         As String
Private AND_UNIT                As String
Private AND_PULL_RANGE          As String
Private AND_OUTCOMES_RANGE      As String
Private AND_ARRIVAL             As String
Private BUCKET_CALC             As String

Private m_pat                   As PatientInfo
Private m_assist_count          As Integer

Private adl23                   As Boolean
Private adl4                    As Boolean
Private tubefeed                As Boolean
Private morse                   As Integer


Private Enum SearchMode
    SearchDefault
    SearchPullRange             'search within the current pull     -- default
    SearchSinceArrival          'search since arrival to the unit
    SearchSinceAdmission        'search everything since admission to the hospital
    SearchOutcomesRange         'search within the current pull + 24 hours before
    SearchAssessments
End Enum

Private Enum CountMode
    CountAll
    CountFirst                  'stop after one is found
End Enum

Private Enum GetValueMode
    GetTotal
    GetMax
    GetLast
End Enum

Private Const FREQUENCY_BUCKET_SIZE = 20    'min

Private Enum Frequencies
    QNONE
    Q4H
    Q2H
    Q1H
    Q30M
End Enum

Private Type fmapRow
    los_high                As Single       'the LOS being testing
    freq(QNONE To Q30M)     As Integer      'the count required for each Q value
End Type

Private m_freq_map(6)       As fmapRow      '1,2,4,8,12,24 hours
Private outstr As String
Private ind_list As String
Public adl_default As Boolean
Public no_assess_items As Boolean



'This is the main entry point
'
Public Sub ProcessInpatient(pat As PatientInfo)
    On Error GoTo errHandler
    
    m_pat = pat
'    frmMain.SetProgress "Processing acct: " & m_pat.acct
    InitGroupsIfNeeded
    SetSQLConstants
    LoadFreqTable
    ResetIndicators
    ResetProcs
'    ResetOutcomes
    If called_from_server Then
        'check if pt on loa for entire range so as not to create class.
    End If

    Check_1234
    Check_5
    Check_67
    Check_8
    Check_9
    Check_1011
    Check_1213
    Check_14
    Check_15161718
    Check_19
    Check_20
    Check_2122
    Check_23
    Check_24
    'CheckCustom
    HighestIndicatorInEachGroupWins

    CheckProcs
'    CheckOutcomes

    If g_no_output Then Exit Sub
    SetClassString
    OutputDNCandClass
    OutputProcs
'    OutputOutcomes
    Exit Sub
    
errHandler:
    LogError Err.Description & " in " & Err.source, EVENT_CATEGORY_UNEXPECTED
    Exit Sub
    Resume  'debug
End Sub

Private Sub ResetIndicators()
    Dim i As Integer
    
    For i = 1 To MAX_INDS
        inds(i).checked = False
        inds(i).also_mark = ""
    Next i
    
    adl23 = False
    adl4 = False
    tubefeed = False
    morse = 0
    ind_list = ""
    adl_default = False
    no_assess_items = True
    
End Sub

Private Sub ResetProcs()
    numprocs = 0
End Sub
Private Sub ResetOutcomes()
    numoutcomes = 0
End Sub

Private Sub InitGroupsIfNeeded()
    On Error GoTo errHandler
    
    Static been_here As Boolean
    Dim rs As New Recordset
    Dim sql As String
    Dim inum As Integer
    
    If been_here Then Exit Sub
    
    'get indicator radio groups from the database
    sql = "SELECT INDICATOR_NUMBER, RADIO_GROUP FROM INDICATOR_DEFINITION WHERE METHODOLOGY_ID=" & m_pat.meth_id
    rs.Open sql, g_cnADO
    
    Do While Not rs.EOF
        inum = rs("INDICATOR_NUMBER")
        If (inum < MAX_INDS) Then
            inds(inum).group = g_dbutil.DBToInteger(rs("RADIO_GROUP"))  'could be null
        End If
        rs.MoveNext
    Loop
    
    rs.Close
    Exit Sub
    
errHandler:
    g_util.ThrowError "InitGroupsIfNeeded"
End Sub

Private Sub LoadFreqTableRow(m_freq As fmapRow, los_high As Single, values As String)
    Dim arr() As String, n As Integer, i As Integer
    
    m_freq.los_high = los_high

    n = g_util.SplitTextOnChar(values, ",", arr(), 0, 0)
    For i = 0 To n - 1
        m_freq.freq(i) = CInt(arr(i))
    Next i
End Sub

Private Sub LoadFreqTable()
                                 ' LOS,   None Q4h Q2h Q1h Q30m
    LoadFreqTableRow m_freq_map(1), 1, "    0,  0,  0,  0,  1"
    LoadFreqTableRow m_freq_map(2), 2, "    0,  0,  0,  1,  2"
    LoadFreqTableRow m_freq_map(3), 4, "    0,  1,  2,  3,  4"
    LoadFreqTableRow m_freq_map(4), 8, "    0,  2,  3,  4,  8"
    LoadFreqTableRow m_freq_map(5), 12, "   0,  2,  4,  6, 12"
    LoadFreqTableRow m_freq_map(6), 9999, " 0,  3,  6, 12, 24"         '24+
    
    'To read this table: if you have at least 6 asessmenets in a 12-hour period, you get Q1h
    '(row 5: LOS=12, column Q1h has a count of 6)
End Sub

Private Function FreqForCount(los_hours As Single, count As Integer) As Frequencies
    Dim i As Integer, j As Integer
    
    For i = 1 To UBound(m_freq_map)                         'go from LOS 1 to 24+
        If (los_hours <= m_freq_map(i).los_high) Then
            For j = Q30M To QNONE Step -1                   'search right to left
                If (count >= m_freq_map(i).freq(j)) Then
                    FreqForCount = j
                    Exit Function
                End If
            Next j
        End If
    Next i
    
    FreqForCount = QNONE
End Function

Private Sub SetSQLConstants()
    'These SQL constants are combined in different ways for searching
    WHERE_ENCOUNTER = " where (encounter_id = " & m_pat.encounter_id & ")"
    AND_UNIT = " and (unit_id = " & m_pat.unit_id & ")"
    AND_PULL_RANGE = " and (event_datetime between " & g_dbutil.SQL_DateTime(m_pat.pull_start) & " and " & g_dbutil.SQL_DateTime(m_pat.pull_finish) & ")"
    AND_ARRIVAL = " and (event_datetime between " & g_dbutil.SQL_DateTime(m_pat.unit_arrival) & " and " & g_dbutil.SQL_DateTime(m_pat.pull_finish) & ")"
    AND_OUTCOMES_RANGE = " and (event_datetime between " & g_dbutil.SQL_DateTime(DateAdd("d", -1, m_pat.pull_start)) & " and " & g_dbutil.SQL_DateTime(m_pat.pull_finish) & ")"
    
    'What bucket number does the event belong to?
    BUCKET_CALC = "DateDiff(minute," & g_dbutil.SQL_DateTime(m_pat.pull_start) & ",event_datetime) / " & FREQUENCY_BUCKET_SIZE
End Sub

'Add this to all chart item queries to start the WHERE clause
'This is where the search depth is set
Private Function WhereBase(Optional search_mode As SearchMode = SearchDefault)
    Dim result As String
    
    Select Case search_mode
    Case SearchPullRange, SearchDefault
        result = WHERE_ENCOUNTER & AND_UNIT & AND_PULL_RANGE    'search within pull range
    Case SearchSinceArrival
        result = WHERE_ENCOUNTER & AND_UNIT & AND_ARRIVAL       'search since arrival to the unit
    Case SearchSinceAdmission
        result = WHERE_ENCOUNTER                                'search since admission to the hospital (any unit)
    Case SearchOutcomesRange
        result = WHERE_ENCOUNTER & AND_UNIT & AND_OUTCOMES_RANGE    'search within pull range+24hrs before
    Case SearchAssessments
        result = WHERE_ENCOUNTER & AND_UNIT     'search within 12 hour range
    End Select
    
    'Since we are looking for one patient in one facility, we don't have to filter by TC_source_ID.
    
    WhereBase = result
End Function

Private Function ValueIsAList(result As String) As Boolean
    ValueIsAList = (InStr(1, result, ",") > 0)                'is there a comma-separated list?
End Function

'Look for any of these fields.  Cat/desc/field = exact match.  Result = like match.
Private Function AndSimpleItemFilter(cat As String, code As String, desc As String, field As String, result_like As String) As String
    Dim result As String
    Dim pos As Integer
    
    If Len(cat) Then result = result & " and category=" & g_dbutil.SQL_String(cat)
    If Len(code) Then
        If Mid$(code, 1, 2) = LIKE_PREFIX Then
            result = result & " and code like " & g_dbutil.SQL_String(Mid$(code, 3, Len(code) - 2))
        ElseIf ValueIsAList(code) Then
            result = result & " and code in " & FormatCodeList(code)
        Else
            result = result & " and code=" & g_dbutil.SQL_String(code)
        End If
    End If
    If Len(desc) Then
        If ValueIsAList(desc) Then
            result = result & " and " & FormatDescList(desc)
        Else
            result = result & " and description like '%" & desc & "%'"
        End If
    End If
    If Len(field) Then result = result & " and field_name=" & g_dbutil.SQL_String(field)
    If Len(result_like) Then
        pos = InStr(1, result_like, CHAR_COMMA) 'when looking for a comma in the result
        If pos > 0 Then
            result_like = Mid$(result_like, 1, pos - 1) & "," & Mid$(result_like, pos + 2, Len(result_like) - pos - 1)
        End If

        If InStr(result_like, EXACT_MATCH_PREFIX) = 1 Then 'exact match
            result_like = Mid$(result_like, 3, Len(result_like) - Len(EXACT_MATCH_PREFIX))
            result = result & " and result= " & g_dbutil.SQL_String(result_like)
        Else
            result = result & " and result like '%" & Trim$(result_like) & "%'"
        End If
    End If

    AndSimpleItemFilter = result
End Function
Private Function FormatCodeList(code_list As String) As String
    Dim arr() As String
    Dim i As Integer, n As Integer
    Dim result As String

    If Len(code_list) = 0 Then Exit Function
    
    n = g_util.SplitTextOnChar(code_list, ",", arr(), 1, 0)
    
    result = "("
    
    For i = 1 To n
        result = result & g_dbutil.SQL_String(Trim$(arr(i)))
        If i < n Then result = result & ","
    Next i
    
    result = result & ")"
    
    FormatCodeList = result
End Function
Private Function FormatDescList(desc_list As String) As String
    Dim arr() As String
    Dim i As Integer, n As Integer
    Dim result As String

    If Len(desc_list) = 0 Then Exit Function
    
    n = g_util.SplitTextOnChar(desc_list, ",", arr(), 1, 0)
' want:  description like 'xxxx%' or description like 'yyyy%' or description like 'zzz%'
    result = ""
    
    For i = 1 To n
        result = result & "description like '" & Trim$(arr(i)) & "%' or "
    Next i
    
    result = Mid$(result, 1, Len(result) - 4) 'remove last " or"
    
    result = "(" & result & ")"
    
    FormatDescList = result
End Function

'Look for a result that contains one of the words in the list
'The only drawback with this function is that we don't know what it found (for audits)
'Because of this, it is only used in as few places.
Private Function AndResultContains(result_list As String) As String
    Dim arr() As String
    Dim i As Integer, n As Integer
    Dim result As String
    Dim pos As Integer

    If Len(result_list) = 0 Then Exit Function
    
    n = g_util.SplitTextOnChar(result_list, ",", arr(), 1, 0)
    
    result = "and ((1=0)"   ' or (result=this) or (result=that)
    
    For i = 1 To n
        pos = InStr(1, arr(i), CHAR_COMMA)
        If pos > 0 Then
            arr(i) = Mid$(arr(i), 1, pos - 1) & "," & Mid(arr(i), pos + 2, Len(arr(i)) - pos - 1)
        End If
        result = result & " or (result like '" & "%" & Trim$(arr(i)) & "%')"
    Next i
    
    result = result & ")"
    
    AndResultContains = result
End Function

Private Function DescribeSearchDepth(search_mode As SearchMode) As String
    Dim result As String
    
    Select Case search_mode
    Case SearchPullRange, SearchDefault
        'result = "in pull range"
        result = ""                         'be quiet since this is default
    Case SearchSinceArrival
        result = "since arrival to unit"
    Case SearchSinceAdmission
        result = "since admission"
    Case SearchOutcomesRange
        result = "since 24hrs before pull"
    End Select
    
    DescribeSearchDepth = result
End Function

Private Function Describe(cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = SearchPullRange) As String
    Dim sql As String, and_filter As String
    Dim rs As New Recordset
    Dim result As String
   
    If Not g_debug Then Exit Function           'avoid extra overhead if not making a log

    result = "looking for"
    If Len(cat) Then result = result & " cat='" & cat & "'"
    If Len(code) Then result = result & " code='" & code & "'"
    If Len(desc) Then result = result & " desc='" & desc & "'"
    If Len(field) Then result = result & " field='" & field & "'"
    If Len(result_list) Then result = result & " result contains '" & result_list & "'"

    If ValueIsAList(result_list) Then
        and_filter = AndSimpleItemFilter(cat, code, desc, field, "") & AndResultContains(result_list)
    Else
        and_filter = AndSimpleItemFilter(cat, code, desc, field, result_list)
    End If
    
    sql = "select code, description, result from chart_item" & WhereBase(search_mode) & and_filter
    rs.Open sql, g_cnADO
    If rs.EOF Then
        result = result & "; not found " & DescribeSearchDepth(search_mode)
    Else
        result = result & "; found"
        'Add info for columns that were not specified and a value was found
        If (Len(code) = 0 Or ValueIsAList(code)) And Len(rs("code")) Then result = result & " code='" & rs("code") & "'"
        If (Len(desc) = 0) And Len(rs("description")) Then result = result & " desc='" & rs("description") & "'"
        'Add the complete result found (we searched for a word or words)
        result = result & " result='" & rs("result") & "'"
        'Are there more results?  Just say how many; we aren't going to list them.
        If (rs.RecordCount = 2) Then
            result = result & " (1 more result)"
        ElseIf (rs.RecordCount > 2) Then
            result = result & " (" & rs.RecordCount - 1 & " more results)"
        End If
    End If
    rs.Close
    
    Describe = result
End Function

'Set an indicator for this reason (low level)
Private Sub SetInd(inum As Integer, reason As String)
    If inds(inum).checked And Not g_debug Then Exit Sub       'already set and no log?

    inds(inum).checked = True
    dprint "Set Ind #" & inum & ": " & reason
End Sub

'Clear an indicator for this reason (low level)
Private Sub ClrInd(inum As Integer, reason As String)
    If Not inds(inum).checked And Not g_debug Then Exit Sub   'already clear and no log?
    
    inds(inum).checked = False
    dprint "Clr Ind #" & inum & ": " & reason
End Sub

'Count how many items have the search word in result_like
'This is simple and fast
Private Function CountSimpleResult(cat As String, code As String, desc As String, field As String, result_like As String, Optional search_mode As SearchMode = SearchPullRange, Optional trace As Boolean = True, Optional found_what As String) As Long
    Dim sql As String, and_filter As String
    Dim rs As New Recordset
    Dim count As Long
    Dim pos As Integer

    and_filter = AndSimpleItemFilter(cat, code, desc, field, result_like)
    sql = "select count(*) from chart_item" & WhereBase(search_mode) & and_filter
'    dvprint sql
    'Debug.Print sql
    rs.Open sql, g_cnADO
    count = rs(0)
    rs.Close
    
    'Normally CountSimpleResult will report what it finds; not eveyone wants this so the trace can be turned off
    found_what = Describe(cat, code, desc, field, result_like, search_mode) & IIf(count > 0, "; count=" & count, "")
    If trace Then dvprint found_what
    
    CountSimpleResult = count
End Function
Private Function CountUniqueCodesInList(codelist As String, retlist As String) As Long
    Dim sql As String, and_filter As String
    Dim rs As New Recordset
    Dim count As Long

    retlist = ""
    sql = "select distinct(code) from chart_item" & WhereBase & " and code in (" & codelist & ")"
    rs.Open sql, g_cnADO
    Do While Not rs.EOF
        If Not IsNull(rs(0)) Then
            count = count + 1
            retlist = retlist & rs(0) & " "
        End If
        rs.MoveNext
    Loop
    rs.Close
    
    'Normally CountSimpleResult will report what it finds; not eveyone wants this so the trace can be turned off
    
    CountUniqueCodesInList = count
End Function
Private Function CountCodeHitsInList(codelist As String, retlist As String) As Long
    Dim sql As String, and_filter As String
    Dim rs As New Recordset
    Dim count As Long

    retlist = ""
    sql = "select count(code),code from chart_item" & WhereBase & " and code in (" & codelist & ")"
    sql = sql & " group by code having count(code)>=2"
    rs.Open sql, g_cnADO
    Do While Not rs.EOF
        If Not IsNull(rs(0)) And Not IsNull(rs(1)) Then
            count = count + 1
            retlist = retlist & rs(1) & " = " & rs(0) & " times; "
        End If
        rs.MoveNext
    Loop
    rs.Close
    If Len(retlist) Then retlist = Mid$(retlist, 1, Len(retlist) - 2) & "."
    'Normally CountSimpleResult will report what it finds; not eveyone wants this so the trace can be turned off
    
    CountCodeHitsInList = count
End Function

'Count how many items have a result with one of the words given in the result_list
'Returns a description of what word was found
'This is more powerful than CountSimpleResult but it is slower
Private Function CountResultInList(cat As String, code As String, desc As String, field As String, result_list As String, search_mode As SearchMode, count_mode As CountMode, trace As Boolean, found_what As String) As Integer
    On Error GoTo errHandler
    Dim sql As String, and_filter As String, arr() As String
    Dim rs As New Recordset
    Dim i As Integer, n As Integer, count As Integer
    Dim pos As Long
    
    n = g_util.SplitTextOnChar(result_list, ",", arr(), 1, 0)
    
    'Do not filter by result here; we need all results
    and_filter = AndSimpleItemFilter(cat, code, desc, field, "")
    sql = "select code,result from chart_item" & WhereBase(search_mode) & and_filter
    'Debug.Print sql
    rs.Open sql, g_cnADO
    Do While Not rs.EOF
        'Look for each search word in the result (case insensitive)
        For i = 1 To n
            arr(i) = Trim$(arr(i))          'we split on comma so there usually are leading blanks
            
            pos = InStr(1, arr(i), CHAR_COMMA)
            If pos > 0 Then
                arr(i) = Mid$(arr(i), 1, pos - 1) & "," & Mid(arr(i), pos + 2, Len(arr(i)) - pos - 1)
            End If
            
            If InStr(arr(i), EXACT_MATCH_PREFIX) = 1 Then
                arr(i) = Mid$(arr(i), 3, Len(arr(i)) - Len(EXACT_MATCH_PREFIX))
                pos = (rs("result") = arr(i))
            Else
                pos = InStr(1, rs("result"), arr(i), vbTextCompare)  'bad when looking for "RN" in "Non RN"
            End If
            If pos > 0 Then
                found_what = "found '" & arr(i) & "' in code='" & rs("code") & "' result='" & rs("result") & "'"
                If trace Then dvprint found_what
                count = count + 1
                If count_mode <> CountAll Then Exit Do  'stop counting
                Exit For                                'try next record
            End If
        Next i

        rs.MoveNext
    Loop
    
    If (count > 0) Then
        'We already printed what was found; maybe add how many?
        If trace And (count_mode = CountAll) Then dvprint "found " & count & " total"
    Else
        found_what = Describe(cat, code, desc, field, result_list, search_mode)       'not found
        If trace Then dvprint found_what
    End If
    
    rs.Close
    
    CountResultInList = count
    Exit Function
    
errHandler:
    g_util.ThrowError "CountResultInList"
    Resume   'debug
End Function

Private Function CountResultContains(cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = SearchPullRange, Optional trace As Boolean = True, Optional found_what As String) As Integer
    If ValueIsAList(result_list) Then
        CountResultContains = CountResultInList(cat, code, desc, field, result_list, search_mode, CountAll, trace, found_what)
    Else
        CountResultContains = CountSimpleResult(cat, code, desc, field, result_list, search_mode, trace, found_what)
    End If
End Function

Private Function ResultContains(cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = SearchPullRange, Optional trace As Boolean = True, Optional found_what As String) As Boolean
    'tracing can be turned off and you can ask for what was found instead of having it printed
    If ValueIsAList(result_list) Then
        ResultContains = (CountResultInList(cat, code, desc, field, result_list, search_mode, CountFirst, trace, found_what) > 0)
    Else
        ResultContains = (CountSimpleResult(cat, code, desc, field, result_list, search_mode, trace, found_what) > 0)
    End If
End Function


Private Function CountResultNotInList(cat As String, code As String, desc As String, field As String, result_list As String, search_mode As SearchMode, count_mode As CountMode, trace As Boolean, found_what As String) As Integer
    On Error GoTo errHandler
    Dim sql As String, and_filter As String, arr() As String
    Dim rs As New Recordset
    Dim i As Integer, n As Integer, count As Integer
    Dim found_one As Boolean

    n = g_util.SplitTextOnChar(result_list, ",", arr(), 1, 0)
    
    'Do not filter by result here; we need all results
    and_filter = AndSimpleItemFilter(cat, code, desc, field, "")
    sql = "select code, result from chart_item" & WhereBase(search_mode) & and_filter
    sql = sql & " and result<>'' and result is not null"
    'Debug.Print sql
    rs.Open sql, g_cnADO
    
    Do While Not rs.EOF
        'Look for each search word in the result (case insensitive)
        found_one = False
        
        For i = 1 To n
            arr(i) = Trim$(arr(i))          'we split on comma so there usually are leading blanks
            If InStr(1, rs("result"), arr(i), vbTextCompare) > 0 Then
                found_what = "found '" & arr(i) & "' in code='" & rs("code") & "' result='" & rs("result") & "' -- ignore this"
                If trace Then dvprint found_what
                found_one = True
                Exit For
            End If
        Next i

        If (Not found_one) Then
            'none of the words were found - good!
            found_what = Describe(cat, code, desc, field, rs("result"), search_mode) & " -- does not contain '" & result_list & "'"
            If trace Then dvprint found_what
            count = count + 1
            If count_mode <> CountAll Then Exit Do
        End If

        rs.MoveNext
    Loop
    
    If (count > 0) Then
        'We already printed what was found; maybe show the total?
        If trace And (count_mode = CountAll) Then dvprint "found " & count & " total"
    ElseIf (rs.RecordCount > 0) Then
        'We already printed what we ignored
    Else
        'Print what did not exist
        found_what = Describe(cat, code, desc, field, "", search_mode)      'not found
        If trace Then dvprint found_what
    End If
    
    rs.Close
    
    CountResultNotInList = count
    Exit Function
    
errHandler:
    g_util.ThrowError "CountResultNotInList"
    Resume   'debug
End Function

Private Function CountResultDoesNotContain(cat As String, code As String, desc As String, field As String, result_list As String, found_what As String, Optional search_mode As SearchMode = SearchPullRange, Optional trace As Boolean = True) As Integer
    CountResultDoesNotContain = CountResultNotInList(cat, code, desc, field, result_list, search_mode, CountAll, trace, found_what)
End Function

Private Function ResultDoesNotContain(cat As String, code As String, desc As String, field As String, result_list As String, found_what As String, Optional search_mode As SearchMode = SearchPullRange, Optional trace As Boolean = True) As Boolean
    ResultDoesNotContain = (CountResultNotInList(cat, code, desc, field, result_list, search_mode, False, trace, found_what) > 0)
End Function

'Set the indicator if the result contains one of the words in the result_list
Private Sub SetIndIfResultContains(inum As Integer, cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = SearchPullRange)
    Dim found_what As String
    'avoid more queries if the indicator is already set
    If inds(inum).checked Then Exit Sub
    
    'Turn trace off for ResultContains() so the result can be placed on the "SetInd#" line below
    If ResultContains(cat, code, desc, field, result_list, search_mode, False, found_what) Then
        SetInd inum, found_what             'echo here
    Else
        dvprint found_what                  'and here
    End If
End Sub

'Set the indicator if the result does not contain any of the words in result_list
Private Sub SetIndIfResultDoesNotContain(inum As Integer, cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = SearchPullRange)
    Dim found_what As String
    'avoid more queries if the indicator is already set
    If inds(inum).checked Then Exit Sub
    
    'Turn trace off for ResultDoesNotContain() and echo what was set below with SetInd
    If ResultDoesNotContain(cat, code, desc, field, result_list, found_what, search_mode, False) Then
        SetInd inum, found_what             'echo here
    Else
        dvprint found_what                  'and here
    End If
End Sub

'Clear the indicator if the result contains on of the words in the result_list
Private Sub ClrIndIfResultContains(inum As Integer, cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = SearchPullRange)
    Dim found_what As String
    'avoid more queries if the indicator is already clear
    If Not inds(inum).checked Then Exit Sub
    
    'Turn trace off for ResultContains() and echo what was set below with SetInd
    If ResultContains(cat, code, desc, field, result_list, search_mode, False, found_what) Then
        ClrInd inum, found_what             'echo here
    Else
        dvprint found_what                  'and here
    End If
End Sub


'These slightly smaller functions are meant for places where you aren't really looking for a chart result
Private Function Exists(cat As String, Optional code As String, Optional desc As String, Optional field As String, Optional result_list As String, Optional search_mode As SearchMode = SearchPullRange, Optional trace As Boolean = True, Optional found_what As String) As Boolean
    Exists = (CountResultContains(cat, code, desc, field, result_list, search_mode, trace, found_what) > 0)
End Function

Private Sub SetIndIfFound(inum As Integer, cat As String, Optional code As String, Optional desc As String, Optional field As String, Optional result_list As String, Optional search_mode As SearchMode = SearchPullRange)
    SetIndIfResultContains inum, cat, code, desc, field, result_list, search_mode
End Sub

Private Sub ClrIndIfFound(inum As Integer, cat As String, Optional code As String, Optional desc As String, Optional field As String, Optional result_list As String, Optional search_mode As SearchMode = SearchPullRange)
    ClrIndIfResultContains inum, cat, code, desc, field, result_list, search_mode
End Sub


'Get the max/total value from a result (usually in the middle of the text)
Private Function GetIntValue(get_mode As GetValueMode, cat As String, code As String, desc As String, field As String, result_like As String, Optional search_mode As SearchMode = SearchPullRange) As Integer
    Dim sql As String, and_filter As String, arr() As String, msg As String
    Dim rs As New Recordset
    Dim result As Integer, i As Integer, n As Integer, value As Integer
    Dim found_one As Boolean

    and_filter = AndSimpleItemFilter(cat, code, desc, field, result_like)
    sql = "select result from chart_item" & WhereBase(search_mode) & and_filter
    'Debug.Print sql
    rs.Open sql, g_cnADO
    
    'Look for a number in the result
    
    Do While Not rs.EOF
        n = g_util.SplitTextOnChar(rs(0), " ", arr(), 1, 0)
        For i = 1 To n
            'Look for 1st character numeric; IsNumeric("60min") will fail so look at 1st char only
            If IsNumeric(Left$(arr(i), 1)) Then
                value = val(arr(i))                         'Use Val; CInt will error on "60min"
                Select Case get_mode
                Case GetMax
                    result = g_util.Max(value, result)      'max
                Case GetTotal
                    result = result + value                 'total
                Case GetLast
                    result = value                          'last
                End Select
                
                'print what we are searching for (the first time)
                If Not found_one Then
                    dvprint Describe(cat, code, desc, field, result_like, search_mode)
                End If
                found_one = True
                'print each value found
                dvprint "  found numeric value " & result
                'Keep going in case there are more
            End If
        Next i
        rs.MoveNext
    Loop
    
    rs.Close
    
    If Not found_one Then
        'show what was not found
        If g_verbose Then dprint Describe(cat, code, desc, field, result_like, search_mode)
    End If
    
    GetIntValue = result
End Function

Private Function GetMaxValue(cat As String, code As String, desc As String, field As String, result_like As String, Optional search_mode As SearchMode = SearchPullRange) As Integer
    GetMaxValue = GetIntValue(GetMax, cat, code, desc, field, result_like, search_mode)
End Function

Private Function GetTotalValue(cat As String, code As String, desc As String, field As String, result_like As String, Optional search_mode As SearchMode = SearchPullRange) As Integer
    GetTotalValue = GetIntValue(GetTotal, cat, code, desc, field, result_like, search_mode)
End Function

'Get a result; returns True if found with return_result set
Private Function GetResult(cat As String, code As String, desc As String, field As String, return_result As String, Optional search_mode As SearchMode = SearchPullRange) As Boolean
    Dim rs As New Recordset
    Dim sql As String

    sql = "select result from chart_item" & WhereBase(search_mode) & AndSimpleItemFilter(cat, code, desc, field, "")
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        return_result = rs(0) & ""
    Else
        return_result = ""
    End If
    rs.Close

    dvprint Describe(cat, code, desc, field, "", search_mode)
    GetResult = (Len(return_result) > 0)
End Function
Private Function GetResultOfLatest(cat As String, code As String, desc As String, field As String, return_result As String, Optional search_mode As SearchMode = SearchPullRange) As Boolean
    Dim rs As New Recordset
    Dim sql As String
    Dim evdt As Date
    Dim done As Boolean

    sql = "select event_datetime,result from chart_item" & WhereBase(search_mode) & AndSimpleItemFilter(cat, code, desc, field, "")
    sql = sql & " order by event_datetime desc"
    rs.Open sql, g_cnADO
    
    return_result = ""
    done = False
    
    If Not rs.EOF Then evdt = rs(0)
    Do While Not rs.EOF And Not done
        If evdt = rs(0) Then
            return_result = return_result & rs(1) & ","
        Else
            done = True
        End If
        rs.MoveNext
    Loop
    rs.Close

    dvprint Describe(cat, code, desc, field, "", search_mode)
    GetResultOfLatest = (Len(return_result) > 0)

End Function

Private Sub Check_1234()
    On Error GoTo errHandler
    Dim count As Integer
    Dim act As Boolean
    Dim diet As Boolean
    Dim hyg As Boolean
    Dim found_what As String
    Dim return_result As String
    Dim reslist As String


    dvprint "---------------"
    dvprint "1. ADL Self"
    dvprint "2. ADL Assist"
    dvprint "3. ADL Extended"
    dvprint "4. ADL Complete"
    dvprint "---------------"

'ORDERS
'Value (In ORC/1)    Description
'NW  New order - Make order active in AcuityPlus
'CA  Cancel order - Make order no longer active in AcuityPlus
'DC  Discontinue order - Make order no longer active in AcuityPlus (I think this is only sent in ORC/5)
'HD  Hold order - Suspend order - Ignore
'RL  Release/cancel previous hold - Unsuspend order - Ignore
'XO  Change/update order - Modify order - Ignore

'in data only these order control,order status pairs occur:
'CA DC
'NW IP
'XO CM

'DNC/LOA data
'2013-03-13 16:43:00.000 9293110 959 @START_LOA
'2013-03-13 17:01:00.000 9293110 959 @FINISH_LOA
'2013-03-14 09:32:00.000 9293154 911 @START_LOA
'2013-03-13 10:24:00.000 9293477 963 @START_LOA
'2013-03-14 17:35:00.000 9293477 963 @FINISH_LOA
'2013-03-20 09:22:00.000 9293687 911 @START_LOA
'2013-03-20 11:28:00.000 9293724 959 @START_LOA
    adl_default = False
'COMPLETE CARE
    SetADLCompleteWhenAge "<=3"
    If inds(4).checked Then Exit Sub
    SetIndIfResultContains 4, "", "", "NURSI TX Position/Device", "", "with 3,with 4"
    SetIndIfResultContains 4, "", "", "NSG_IABP Settings", "", ""
    SetIndIfResultContains 4, "", "", "NURSI NMB Train of Four", "", "Neuromuscular Blockade Train of Four"
    SetIndIfResultContains 4, "", "", "NURSI Lumbar Status", "", "Continuous drain, Intermittent drain"
    SetIndIfResultContains 4, "", "", "NURSI Ventric Status", "", "Continuous drain, Intermittent drain"
    SetIndIfResultContains 4, "", "", "NURSI TX Position/Device", "", "with 4"
    SetIndIfResultContains 4, "", "", "NURSI TX Activity Type", "", "up in cardiac chair"
    SetIndIfResultContains 4, "", "", "NURSI ICP", "", ""
    SetIndIfResultContains 4, "", "", "NURSI RASS", "", "+4,+3,+2,+1,-1,-2,-3,-4,-5"
    
    reslist = "Aerosol blow-by, Aerosol face mask, Aerosol face tent, Aerosol trach mask, Aerosol T-piece,"
    reslist = reslist & " BiFlow cannula, CPAP/BiPAP, Non-rebreather mask, Ventilator, Vapo Therm"
    SetIndIfResultContains 4, "", "", "NURSI O2 Delivery Mode", "", reslist

'STILL Need to add Glasgow mappings 1/24/14
'NURSI M Glasgow Eye Opening; NURSI M Glasgow Best Verbal Response; NURSI M Glasgow Best Motor Response; NURSI M Glasgow Coma Scale Total Score
    
    If inds(4).checked Then Exit Sub
    count = 0
    If ResultContains("", "", "NURS Assistance Levels", "", "Full") Then count = count + 1
    If ResultContains("", "", "NURS Mobility Gait", "", "complete") Then count = count + 1
    If count >= 2 Then
        SetInd 4, "Two items found for ADL Complete"
        Exit Sub
    End If
    If ResultContains("", "", "NURS Neuro Level of Consciousness 1", "", "Lethargic, Stuporous, Semi-comatose, Comatose, Drowsy") Then count = count + 1
    If ResultContains("", "", "NURS Bowel Observations", "", "Incontinent of stool") Then count = count + 1
    If count >= 2 Then
        SetInd 4, "Two items found for ADL Complete"
        Exit Sub
    End If
    If ResultContains("", "", "NURS Urinary Observation 1", "", "Incontinence of urine") Then count = count + 1
    If ResultContains("", "", "NURS Bowel Nursing Interventions", "", "Bedpan,diaper/pad changed") Then count = count + 1
    If count >= 2 Then
        SetInd 4, "Two items found for ADL Complete"
        Exit Sub
    End If
    If ResultContains("", "", "NURS Diet Assistance", "", "complete") Then count = count + 1
    If ResultContains("", "", "NURS Hygiene Care Interventions", "", "Bedbath, Tub bath") Then count = count + 1
    If count >= 2 Then
        SetInd 4, "Two items found for ADL Complete"
        Exit Sub
    End If
    If ResultContains("", "", "NURS Skin Hygiene Performed", "", "complete") Then count = count + 1
    If ResultContains("", "", "NURS Oral Care", "", "Performed by RN, Performed by patient family") Then count = count + 1
    If count >= 2 Then
        SetInd 4, "Two items found for ADL Complete"
        Exit Sub
    End If
    If ResultContains("", "", "NURS Food/Fluid Feeding Patterns", "", "Must Be Fed") Then count = count + 1
    If ResultContains("", "", "NURS Toileting", "", "Total dependence with toileting") Then count = count + 1
    If count >= 2 Then
        SetInd 4, "Two items found for ADL Complete"
        Exit Sub
    End If
    If ResultContains("", "", "NURSI TX ICU Bed Type", "", "Crib") Then count = count + 1
    If ResultContains("", "", "NURS_FPH Pediatric Standard Precautions", "", "child attended at all times,crib rails up,hand contact when crib rails are down") Then count = count + 1
    If count >= 2 Then
        SetInd 4, "Two items found for ADL Complete"
        Exit Sub
    End If
    If ResultContains("", "", "NURSI Diet Feeding Assistance", "", "Bottle, Family,Total") Then count = count + 1
    If ResultContains("", "", "NURSI Bowel Observations", "", "Incontinent of stool") Then count = count + 1
    If count >= 2 Then
        SetInd 4, "Two items found for ADL Complete"
        Exit Sub
    End If
    If ResultContains("", "", "NURSI Urine Observations", "", "Incontinent") Then count = count + 1
    If ResultContains("", "", "NURSI Fecal Containment Type", "", "") Then count = count + 1
    If count >= 2 Then
        SetInd 4, "Two items found for ADL Complete"
        Exit Sub
    End If
    If ResultContains("", "", "NURSI Level of Consciousness", "", "Lethargic, Stuporous, Semi-comatose, Comatose, Drowsy") Then count = count + 1
    If ResultContains("", "", "NURSI TX Skin Characteristics", "", "complete") Then count = count + 1
    If count >= 2 Then
        SetInd 4, "Two items found for ADL Complete"
        Exit Sub
    End If
    If ResultContains("", "", "NURSI Motor Neuro-motor Mobility", "", "Paraplegic,Quadriplegic") Then count = count + 1
    If ResultContains("", "", "NURSI TX Activity Assistance", "", "complete") Then count = count + 1
    If ResultContains("", "", "NURS FR Elimination, B%B", "", "incontinence") Then count = count + 1
    If count >= 2 Then
        SetInd 4, "Two items found for ADL Complete"
        Exit Sub
    End If
    
    If inds(4).checked Then Exit Sub
'3
    SetIndIfResultContains 3, "", "", "NURSI TX Position/Device", "", "with 1,with 2"
    SetIndIfResultContains 3, "", "", "NURSI TX W Drain Type of Drain/Suction", "", ""
    SetIndIfResultContains 3, "", "", "NURSI Chest Tube Type", "", ""
    SetIndIfResultContains 3, "", "", "NURSI Pacemaker Type", "", ""
    SetIndIfResultContains 3, "", "", "NURSI TX Beck_s Score", "", ""
    
    
    If ResultContains("", "", "NURS Activity Assistance", "", "with 1 staff, with more than 1 staff, with family") Then act = True
    If Not act Then
        reslist = "Unsteady,Ataxic,Scissor,Spastic,Staggering,Wadding,Drags right foot while walking,"
        reslist = reslist & "Drags left foot while walking,Leans forward while walking,Leans backward while walking"
        If ResultContains("", "", "NURS Mobility Gait", "", reslist) Then act = True
    End If
    If Not act Then
        If ResultContains("", "", "NURSI TX Activity Assistance", "", "Partial") Then act = True
    End If
    If Not act Then
        If ResultContains("", "", "NURS FR Mobility", "", "Requires assistance with ambulation") Then act = True
    End If
    
    If act Then
    
    If ResultContains("", "", "NURS Diet Assistance", "", "partial") Then diet = True
    If Not diet Then
       If ResultContains("", "", "NURSI Diet Feeding Assistance", "", "Partial") Then diet = True
    End If
    
    If diet Then
    
    If ResultContains("", "", "NURS Bowel Nursing Interventions", "", "Assisted to Bathroom, Assisted to Commode") Then hyg = True
    If Not hyg Then
        If ResultContains("", "", "NURS Skin Hygiene Performed", "", "partial,by patient") Then hyg = True
    End If
    If Not hyg Then
        If ResultContains("", "", "NURSI Bowel Interventions", "", "Bedpan used") Then hyg = True
    End If
    If Not hyg Then
        If ResultContains("", "", "NURSI TX Hygiene Care", "", "Partial") Then hyg = True
    End If
    If Not hyg Then
        If ResultContains("", "", "NURSI Falls/Safety Interventions (Equipment)", "", "bedside commode/toileting assist ") Then hyg = True
    End If
    If Not hyg Then
        If ResultContains("", "", "NURS Oral Care", "", "Performed by RN, performed by patient family") Then hyg = True
    End If
    If act And diet And hyg Then SetInd 3, "Activity+Diet+Hygiene Assistance"
    
    If inds(3).checked Then Exit Sub
    
    
    End If 'diet
    End If 'act
'2
    SetIndIfResultContains 2, "", "", "NURS Assistance Levels", "", "Partial"
    SetIndIfResultContains 2, "", "", "NURS Activity Assistance", "", "partial"
    SetIndIfResultContains 2, "", "", "NURSI TX Activity Assistance", "", "Partial"
    reslist = "Ambulated,Ambulated in room,Ambulated to bathroom,Sat on edge of bed,Stood at bedside,"
    reslist = reslist & "Up ad lib,Up in cardiac chair,Up in chair,Up to bedside commode"
    SetIndIfResultContains 2, "", "", "NURSI TX Activity Type", "", reslist
    If inds(2).checked Then Exit Sub
    
    SetIndIfResultContains 2, "", "", "NURS Oral Care", "", "Performed by RN/PCT, Performed by patient family"
    reslist = "Unsteady,Ataxic,Scissor,Spastic,Staggering,Wadding,Drags right foot while walking,"
    reslist = reslist & "Drags left foot while walking,Leans forward while walking,Leans backward while walking"
    SetIndIfResultContains 2, "", "", "NURS Mobility Gait", "", reslist
    
    SetIndIfResultContains 2, "", "", "NURS FR Mobility", "", "Requires assistance with ambulation"
    
    SetIndIfResultContains 2, "", "", "NURS Mobility Weight Bearing", "", "Partially"
    SetIndIfResultContains 2, "", "", "NURS Mobility Devices", "", "Cane, Gait Belt, Walker"
    If inds(2).checked Then Exit Sub
    
    SetIndIfResultContains 2, "", "", "NURS Toileting", "", "Needs partial assistance when toileting"
    SetIndIfResultContains 2, "", "", "NURSI TX Hygiene Care", "", "Partial"
    SetIndIfResultContains 2, "", "", "NURS Elimination Stoma Type", "", ""
    If inds(2).checked Then Exit Sub
    
    SetIndIfResultContains 2, "", "", "NURS Urinary Catheters Elimination", "", ""
    SetIndIfResultContains 2, "", "", "NURS Bowel Nursing Interventions", "", "Assisted to Bathroom, Assisted to Commode"
    SetIndIfResultContains 2, "", "", "NURS Skin Hygiene Performed", "", "partial"
    If inds(2).checked Then Exit Sub

    SetIndIfResultContains 2, "", "", "NURS Diet Assistance", "", "Partial"
    SetIndIfResultContains 2, "", "", "NURS Diet Interventions", "", ""
    SetIndIfResultContains 2, "", "", "NURS Food/Fluid Feeding Patterns", "", " Needs Assisstance"
    SetIndIfResultContains 2, "", "", "NURSI Diet Feeding Assistance", "", "Partial"
    SetIndIfResultContains 2, "", "", "NURSI Diet Type", "", "tube feeding"
    If inds(2).checked Then Exit Sub
    
    SetIndIfResultContains 2, "", "", "NURS_EN_Delivery Method", "", ""
    SetIndIfResultContains 2, "", "", "NURSI GI Stoma Location", "", ""
    SetIndIfResultContains 2, "", "", "NURSI GU Stoma Type", "", ""
    SetIndIfResultContains 2, "", "", "NURS_FPH Hypotension/Vertigo/Dizziness", "", "Called for assistance before"
    If inds(2).checked Then Exit Sub

'1
    SetIndIfResultContains 1, "", "", "NURS Assistance Levels", "", "None"
    SetIndIfResultContains 1, "", "", "NURSI TX Activity Assistance", "", "Self"
    SetIndIfResultContains 1, "", "", "NURS Activity Assistance", "", "Self"
    If inds(1).checked Then Exit Sub
    SetIndIfResultContains 1, "", "", "NUS Skin Hygiene Performed", "", "Self"
    SetIndIfResultContains 1, "", "", "NURS Diet Assistance", "", "self"
    SetIndIfResultContains 1, "", "", "NURS Oral Care", "", "performed by patient"
    If inds(1).checked Then Exit Sub
    SetIndIfResultContains 1, "", "", "NURS Food/Fluid Feeding Patterns", "", "Eats Independently"
    SetIndIfResultContains 1, "", "", "NURS Toileting", "", "Patient performs independent toileting"
    SetIndIfResultContains 1, "", "", "NURSI Diet Feeding Assistance", "", "Self"
    SetIndIfResultContains 1, "", "", "NURSI TX Skin Characteristics", "", "Self"

    If Not (inds(1).checked Or inds(2).checked Or inds(3).checked Or inds(4).checked) Then
        If IsICU Then
            SetInd 4, "Defaulting to Complete in ICU due to lack of documentation."
        Else
            SetInd 1, "Defaulting to self due to lack of documentation."
            adl_default = True
        End If
    End If
    
    Exit Sub
errHandler:
    g_util.ThrowError "Check_1234"
    Resume  'debug
End Sub
Private Sub SetADLCompleteWhenAge(agecond As String)  ' agecond example: "<=3"
    Dim sql As String
    Dim rs As New Recordset

'
'select case when round(age_at_admission,0,1) <=55 then 1 else 0 end from encounter where encounter_id=6990

    sql = "select case when round(age_at_admission,0,1) " & agecond & " then 1 else 0 end from encounter " & WHERE_ENCOUNTER
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        If rs(0) = 1 Then
            SetInd 4, "Age <=3 years"
        End If
    End If
    rs.Close

End Sub


Private Sub Check_5()
    On Error GoTo errHandler
    Dim reslist As String
    
    dvprint "---------------"
    dvprint "5. ADL Rehab"
    dvprint "---------------"
    
SetIndIfResultContains 5, "", "", "Nurs Mobility Range of Motion", "", ""
SetIndIfResultContains 5, "", "", "NURSI TX Exercise Performed", "", ""
SetIndIfResultContains 5, "", "", "NURS FAST Screen Swallowing", "", "Yes"
    If inds(5).checked Then Exit Sub
SetIndIfResultContains 5, "", "", "NURS Respiratory Cough Description", "", "Ineffective, unable to cough"
SetIndIfResultContains 5, "", "", "NURS Food/Fluid Food Intake Obs", "", "Gag Reflex Absent "
    reslist = "Unsteady,Ataxic,Scissor,Spastic,Staggering,Wadding,Drags right foot while walking,"
    reslist = reslist & "Drags left foot while walking,Leans forward while walking,Leans backward while walking"
SetIndIfResultContains 5, "", "", "NURS Mobility Gait", "", reslist
    If inds(5).checked Then Exit Sub
SetIndIfResultContains 5, "", "", "NURS Food/Fluid Interventions", "", "RN assist with swallowing precautions"
SetIndIfResultContains 5, "", "", "NURS Mobility Positioning Aids", "", "foot board, multipodus boots"
SetIndIfResultContains 5, "", "", "NURSI Diet Interventions", "", "RN assist with swallowing precautions"
    If inds(5).checked Then Exit Sub
SetIndIfResultContains 5, "", "", "NURSI Musculoskeletal Observations", "", ""
SetIndIfResultContains 5, "", "", "NURSI CN Glossopharyngeal/Vagus", "", "Gag Reflex Absent, Difficulty Swallowing, Cough Absent"
SetIndIfResultContains 5, "", "", "NURSI TX Pressure Ulcer Prevention", "", "Boots, multi-podus"
    If inds(5).checked Then Exit Sub
SetIndIfResultContains 5, "", "", "NURS Mobility Ambulation Tolerance", "", "Dizziness, Fatigue, Pain, Shortness of Breath, Weakness"
SetIndIfResultContains 5, "", "", "NURS Mobility Devices", "", ""
SetIndIfResultContains 5, "", "", "NURS Mobility Weight Bearing", "", "Partially, Unable"
    
    If inds(5).checked Then Exit Sub
SetIndIfResultContains 5, "", "", "NURS Pressure Sore Prevention", "", "Rehab consult orderded, Seen by rehab"
SetIndIfResultContains 5, "", "", "NURS Supports/Binders 1", "", ""
SetIndIfResultContains 5, "", "", "RMD_Brief Service Note", "", "Subject seen today for services indicated below"
SetIndIfResultContains 5, "", "", "RMDPD Plan", "", "Continue as per pulmonary rehabilitation plan of care"
SetIndIfResultContains 5, "", "", "REHAB Encounter Service", "", "Occupational Therapy, Physiatry, Physical therapy"
  
  
SetIndIfResultContains 5, "", "", "NURS Respiratory Tracheostomy Tube Type", "", ""
SetIndIfResultContains 5, "", "", "NURSI Tracheostomy Tube Type", "", ""
SetIndIfResultContains 5, "", "", "NURSI Trach Speaking Valve", "", ""
SetIndIfResultContains 5, "", "", "NURS Respiratory Time Speaking Valve", "", ""

  
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_5"
    Resume  'debug
End Sub


Private Sub Check_67()
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "6. ADL 2-3 Caregivers"
    dvprint "7. ADL 4 or more Caregivers"
    dvprint "---------------"
    
SetIndIfResultContains 7, "", "", "NURS Skin Hygiene Performed", "", "With 4 or more staff"
SetIndIfResultContains 7, "", "", "NURS Activity Assistance1", "", "With 4 or more staff"
    If inds(7).checked Then Exit Sub
SetIndIfResultContains 7, "", "", "NURSI TX Position/Device", "", "With 4 or more staff"
SetIndIfResultContains 7, "", "", "NURSI TX Hygiene Care", "", "With 4 or more staff"
SetIndIfResultContains 7, "", "", "NURSI TX Assist/Caregivers", "", "With 4 or more staff"

If inds(7).checked Then Exit Sub
SetIndIfResultContains 6, "", "", "NURS Skin Hygiene Performed", "", "with 2, with 3"
SetIndIfResultContains 6, "", "", "NURS Activity Assistance1", "", "with 2, with 3"
If inds(6).checked Then Exit Sub
SetIndIfResultContains 6, "", "", "NURSI TX Position/Device", "", "with 2, with 3"
SetIndIfResultContains 6, "", "", "NURSI TX Hygiene Care", "", "with 2, with 3"
SetIndIfResultContains 6, "", "", "NURSI TX Activity Assistance", "", "with 2, with 3"
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_67"
    Resume  'debug
End Sub

Private Sub Check_8()
    Dim reslist As String
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "8. Communication"
    dvprint "---------------"
    
SetIndIfResultContains 8, "", "", "NURS Neuro Speech Deficits/Abnormalities", "", ""
SetIndIfResultContains 8, "", "", "NURS Physical Safety Risks Due To", "", "Communication"
    If inds(8).checked Then Exit Sub
SetIndIfResultContains 8, "", "", "NURS Respiratory Tracheostomy Tube Type", "", ""
SetIndIfResultContains 8, "", "", "NURS_FPH Fall Risk Due To", "", "Visual Impairment, Hearing Impairment"
SetIndIfResultContains 8, "", "", "NURS Primary Language Fluency", "", "Spanish, Greek, Vietnamese, Korean, Creole, Arabic, Mandarin, Italian, German, Hebrew, French, American Sign Language, Other", SearchSinceAdmission
    If inds(8).checked Then Exit Sub
SetIndIfResultContains 8, "", "", "NURS Teach/Learning Barriers Patient", "", "Non-English speaking, Illiteracy, Vision Impairment, Hearing Impairment, Dyslexia, Aphasia", SearchSinceAdmission
SetIndIfResultContains 8, "", "", "NURS FAT Mobility 2", "", "yes"
SetIndIfResultContains 8, "", "", "NURS FAST Screen Speech", "", "Yes"
    If inds(8).checked Then Exit Sub
SetIndIfResultContains 8, "", "", "NURSI Falls/Safety Interventions (Equipment)", "", "Translation phone"
SetIndIfResultContains 8, "", "", "NURSI Tracheostomy Tube Type", "", ""
SetIndIfResultContains 8, "", "", "NURSI Speech", "", "Abnormal rate of speech, Expressive aphasia, Receptive Aphasia, Incomprehensible, Slurred"
    If inds(8).checked Then Exit Sub
SetIndIfResultContains 8, "", "", "CC_Resp Airway Type", "", ""
SetIndIfResultContains 8, "", "", "NURSI Barrier Assessment", "", "Artificial Airway, Aphasia, Language Barriers, Speech Impairment, Oral Deformity, Mute, Hard of Hearing/Deaf, Disease process"
SetIndIfResultContains 8, "", "", "NURS_FPH Visual/Hearing Impairment", "", ""
SetIndIfResultContains 8, "", "", "NURSI NIV Mask Type", "", "face mask"

    SetIndIfResultContains 8, "", "", "NURS FR Mobility", "", "Uncompensated visual/auditory impairment"
SetIndIfResultContains 8, "", "", "NURSI Falls/Safety Interventions (Equipment)", "", "Translation phone"
SetIndIfResultContains 8, "", "", "NURSI NIV O2 Mode", "", ""
SetIndIfResultContains 8, "", "", "NURS_FPH Visual/Hearing Impairment", "", ""

    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_8"
    Resume  'debug
End Sub

Private Sub Check_9()
    On Error GoTo errHandler
    Dim found_what As String
    Dim reslist As String
    Dim cogimp As Boolean

    dvprint "---------------"
    dvprint "9. Cognitive Support"
    dvprint "---------------"
    
If ResultContains("", "", "NURS_FPH Cognitive Impairment/Disorientation", "", "") Then
    cogimp = True
End If
If cogimp Then
    SetIndIfResultContains 9, "", "", "NURS Neuro Disoriented To", "", ""
    SetIndIfResultContains 9, "", "", "NURS Physical Safety Risks Due To", "", "Cognitive Ability"
    If inds(9).checked Then Exit Sub
    SetIndIfResultContains 9, "", "", "NURS Neuro Level of Consciousness 1", "", "Lethargic, Stuporous, Semi-comatose, Comatose, Drowsy"
    SetIndIfResultContains 9, "", "", "NURS Glasgow Best Verbal Response", "", "4,3,2"
    SetIndIfResultContains 9, "", "", "NURS Neuro Memory - Short Term", "", "Impaired, Absent"
    If inds(9).checked Then Exit Sub
    SetIndIfResultContains 9, "", "", "NURS Neuro Memory - Long Term", "", "Impaired, Absent"
    SetIndIfResultContains 9, "", "", "NURSI Level of Consciousness", "", "Lethargic, Obtunded, Stuporous, Sedated, Comatose, Drowsy"
    SetIndIfResultContains 9, "", "", "NURSI Glasgow Best Verbal Response", "", "4, 3, 2"
    SetIndIfResultContains 9, "", "", "NURSI Mentation", "", "Confused, Hallucinating, Immediate recall loss, Short term memory loss, Long term memory loss, Unable to assess"
End If
    If ResultContains("", "", "NURS Psych Perceptual Disturbances", "", "observed") Then
        SetIndIfResultContains 9, "", "", "NURS Psych Perceptual Disturbances", "", "intervention"
    End If
    If inds(9).checked Then Exit Sub
    If ResultContains("", "", "NURS Psych thought Content Disorder", "", "observed") Then
        SetIndIfResultContains 9, "", "", "NURS Psych thought Content Disorder", "", "intervention"
    End If
'SetIndIfResultDoesNotContain 9, "", "", "NURSI Oriented To ", "", "Person, Place, Time, Situation"
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_9"
    Resume  'debug
End Sub

Private Sub Check_1011()
    On Error GoTo errHandler
    Dim reslist As String
    Dim ct As Integer
    Dim found_what As String
    Dim return_result As String
    Dim numbuckets As Integer

    dvprint "---------------"
    dvprint "10. Behavior/Emotional Management"
    dvprint "11. Behavior/Emotional Mgmt - q 1 Hour"
    dvprint "---------------"
    
SetIndIfResultContains 11, "", "", "NURS Psych Observation Status", "", ""
If inds(11).checked Then Exit Sub

If ResultContains("", "", "NURS Psych Action Taken", "", "") And _
   ResultContains("", "", "NURS Psych Support Frequency2", "", "every 1 hour") Then
    
    SetIndIfResultContains 11, "", "", "NURS Psych Overt Behavior", "", "Combativeness, Agitation, Restlessness, Pacing, Purposeless activity"
    SetIndIfResultContains 11, "", "", "NURS Patient Appears", "", "Angry, Anxious, Clinging to parent, Crying/tearful, Combative, Depressed, Difficult to engage, Restless, Talkative, Uncooperative,Withdrawn"
    
End If
If inds(11).checked Then Exit Sub

If ResultContains("", "", "NURSI Psych Action Taken", "", "") And _
   ResultContains("", "", "NURSI Psych Support Frequency", "", "every 1 hour") Then
    
    SetIndIfResultContains 11, "", "", "NURSI Patient Appears", "", "Angry, Anxious, Clinging to parent, Crying/tearful, Combative, Depressed, Difficult to engage, Restless, Talkative, Uncooperative,Withdrawn"
    
End If

If inds(11).checked Then Exit Sub

SetIndIfResultContains 10, "", "", "NURSI RASS", "", "combative, agitated, restless"
If inds(10).checked Then Exit Sub
reslist = "Inappropriate to situation, Agitated, Angry, Anxious, Combative, Euphoric, Frantic sucking of hands, Irritable, Flat affect, Labile affect, Paranoid, Picking at times, Restless, Sad, Uncooperative, Withdrawn"
SetIndIfResultContains 10, "", "", "NURSI Behavior", "", reslist
If inds(10).checked Then Exit Sub
If ResultContains("", "", "NURS Psych Action Taken", "", "") And _
   ResultContains("", "", "NURS Psych Support Frequency", "", "Every 8 hours, Every 4 hours, Every 2 hours") Then
    
    SetIndIfResultContains 10, "", "", "NURS Appearance", "", "Behavior Inappropriate for situation"
    SetIndIfResultContains 10, "", "", "NURS Psych Overt Behavior", "", "Combativeness, Agitation, Restlessness, Pacing, Purposeless activity"
    SetIndIfResultContains 10, "", "", "NURS Patient Appears", "", "Angry, Anxious, Clinging to parent, Crying/tearful, Combative, Depressed, Difficult to engage, Restless, Talkative, Uncooperative,Withdrawn"
    
End If
If inds(10).checked Then Exit Sub
If ResultContains("", "", "NURSI Psych Action Taken", "", "") And _
   ResultContains("", "", "NURSI Psych Support Frequency", "", "Every 8 hours, Every 4 hours, Every 2 hours") Then
    
    SetIndIfResultContains 10, "", "", "NURSI Patient Appears", "", "Angry, Anxious, Clinging to parent, Crying/tearful, Combative, Depressed, Difficult to engage, Restless, Talkative, Uncooperative,Withdrawn"
    
End If

    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_1011"
    Resume  'debug
End Sub


Private Sub Check_1213()
    On Error GoTo errHandler
    Dim ct As Integer
    Dim found_what As String
    Dim reslist As String
    
    dvprint "---------------"
    dvprint "12. Safety Management - q 2 Hours"
    dvprint "13. Safety Management - q 30 Minutes"
    dvprint "---------------"
    
If CheckOrder("NSG_IABP") Then
    SetInd 13, ""
    SetInd 18, "Iabp"
End If
If inds(13).checked Then Exit Sub
'SetIndIfResultContains 13, "", "", "NURS Physical Safety Observational", "", "Patient kept under continuous eye contact"
If ResultContains("", "", "NURS Physical Safety Observation Status", "", "Every 30 minutes, Every 15 minutes") And _
  ResultContains("", "", "NURS Physical Safety Risks Due To", "", "") Then
  SetInd 13, "Observation freq q15 min or q30 min with Safety Risk."
End If
  
SetIndIfResultContains 13, "", "", "NURS Electrode Types", "", ""
If inds(13).checked Then Exit Sub
SetIndIfResultContains 13, "", "", "NURS_FPH Seizures", "", ""
'SetIndIfResultContains 13, "", "", "NURS Restraint Types", "", ""
'SetIndIfResultContains 13, "", "", "NURS Restraint Eye Contact Med/Surg ICU", "", "Done"
If inds(13).checked Then Exit Sub
SetIndIfResultContains 13, "", "", "NURS Restraint Eye Contact Med/Surg ICU", "", "Done"
SetIndIfResultContains 13, "", "", "NURS_FPH Cognitive Impairment/Disorientation", "", "Sitter Present"
SetIndIfResultContains 13, "", "", "NURS_FPH Seizures", "", "every 30 minutes, every 15 minutes"
If inds(13).checked Then Exit Sub
SetIndIfResultContains 13, "", "", "NURS_FPH Pediatric Standard Precautions", "", ""
SetIndIfResultContains 13, "", "", "NURSI EEG Electrode Types", "", ""
SetIndIfResultContains 13, "", "", "NURSI CRRT Mode", "", "SCUF, CVVH, CVVHDF"
If inds(13).checked Then Exit Sub
SetIndIfResultContains 13, "", "", "CC_Resp Airway Type", "", "endotracheal"
SetIndIfResultContains 13, "", "", "NURSI RASS", "", "+4,+3,+2,+1,-1,-2,-3,-4,-5"
SetIndIfResultContains 13, "", "", "NURSI ICP", "", ""
SetIndIfResultContains 13, "", "", "NURSI Ventric Status", "", "Continuous drain, Intermittent drain"
If inds(13).checked Then Exit Sub

'SetIndIfResultContains 12, "", "", "NURS Physical Safety Observation Status", "", "Hourly"
SetIndIfResultContains 12, "", "", "NURS Psych Elopement Risk", "", "Yes"
If ResultContains("", "", "NURS Physical Safety Risks Due To", "", "") Then
    SetIndIfResultContains 12, "", "", "NURS Physical Safety Observational", "", "Patient maintained under close observation"
End If
'SetIndIfResultContains 12, "", "", "NURS_FPH Fall Risk Due To", "", ""
If inds(12).checked Then Exit Sub
If ResultContains("", "", "NURS_FPH Communication of Falls Risk Prevention Plan", "", "") Then
    SetIndIfResultContains 12, "", "", "NURS FR Risk Level", "", "High Risk"
    SetIndIfResultContains 12, "", "", "NURSI Mentation", "", "Confused, Hallucinating, Immediate recall loss, Short term memory loss, Long term memory loss"
    SetIndIfResultContains 12, "", "", "NURSI Behavior", "", "Agitated, angry, anxious, combative, paranoid, picking at items, uncooperative, restless"
    SetIndIfResultContains 12, "", "", "NURS FR Patient Care Equipment", "", "tether patient"
    SetIndIfResultContains 12, "", "", "NURS FR Bones", "", ""
    SetIndIfResultContains 12, "", "", "NURS FR Complications Risk", "", ""
    SetIndIfResultContains 12, "", "", "NURS FR Bleeding Risk", "", ""
End If
If inds(12).checked Then Exit Sub
'SetIndIfResultContains 12, "", "", "NURS FAT Falls Risk Level", "", "High Risk"
'SetIndIfResultContains 12, "", "", "NURSI Falls/Safety Interventions (Equipment)", "", "Falling star magnet on door"
'SetIndIfResultContains 12, "", "", "NURSI Falls/Safety Interventions (Equipment)", "", "Falling star on chart"
'If inds(12).checked Then Exit Sub
SetIndIfResultContains 12, "", "", "NURSI Seizure Interventions", "", "Safety Evaluated and Maintained"
SetIndIfResultContains 12, "", "", "NURS_FPH Cognitive Impairment/Disorientation", "", "bed alarms on, siderails up x 4, room near nurses station"
SetIndIfResultContains 12, "", "", "NURS_FPH Seizures", "", "observed every 60 minutes"
'SetIndIfResultDoesNotContain 12, "", "", "NURSI Level of Cooperation", "", "cooperative"
If inds(12).checked Then Exit Sub


    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_1213"
    Resume  'debug
End Sub

Private Sub Check_14()
    On Error GoTo errHandler
    Dim found_what As String
    Dim reslist As String
    
    dvprint "---------------"
    dvprint "14. Isolation"
    dvprint "---------------"

'look for latest code like NSG_IABP with order_control of NW or XO
'if there is an order_control with CA then stop

    If CheckOrder("Epid_AIRB") Then SetInd 14, ""
    If inds(14).checked Then Exit Sub
'    If CheckOrder("Epid_AFB") Then SetInd 14, ""
'    If inds(14).checked Then Exit Sub
'    If CheckOrder("Epid_CNS") Then SetInd 14, ""
'    If inds(14).checked Then Exit Sub
    If CheckOrder("Epid_Resp") Then SetInd 14, ""
    If inds(14).checked Then Exit Sub
    If CheckOrder("Epid_Spec") Then SetInd 14, ""
    If inds(14).checked Then Exit Sub
    If CheckOrder("Epid_Cont") Then SetInd 14, ""
    If inds(14).checked Then Exit Sub
    If CheckOrder("Epid_Enhance") Then SetInd 14, ""
    If inds(14).checked Then Exit Sub
    If CheckOrder("Epid_Custom") Then SetInd 14, ""
    If inds(14).checked Then Exit Sub
    
    If Not IsoDC("Epid_AIRB,Epid_Resp,Epid_Spec,Epid_Cont,Epid_Enhance,Epid_Custom") Then
        If IsoInPrevClass Then SetInd 14, "Isolation - rolled from previous classification"
    End If

    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_14"
    Resume  'debug
End Sub

Private Sub CheckAssessment(count As Integer, desc As String)
    Dim los As Single
    Dim p As Single

    If (inds(18).checked) Then Exit Sub             'skip if highest already checked
    If (count = 0) Then Exit Sub                    'skip if none
    
    los = DateDiff("n", m_pat.pull_start, m_pat.pull_finish)
    desc = desc & "; LOS=" & los
    p = 1# * los / 1440  ' pro-rate factor 0 < p <= 1
    If p < 1# Then desc = desc & "; LOS factor=" & p

    If count >= g_util.Max(16, CInt(36 * p)) Then
        SetInd 18, desc & " with count of " & count
    ElseIf count >= g_util.Max(8, CInt(24 * p)) Then
        SetInd 17, desc & " with count of " & count
    ElseIf count >= g_util.Max(4, CInt(12 * p)) Then
        SetInd 16, desc & " with count of " & count
    ElseIf count >= g_util.Max(2, CInt(6 * p)) Then
        SetInd 15, desc & " with count of " & count
    End If

End Sub

Private Sub Check_15161718()
    Dim found_what As String
    Dim qmins As Integer
    Dim ct As Integer
    Dim ct1 As Integer
    Dim ct2 As Integer
    Dim ct3 As Integer
    Dim ct4 As Integer
    Dim ct5 As Integer
    Dim desclist As String
    Dim reslist As String
    Dim return_result As String
    
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "15. Assessment q4h"
    dvprint "16. Assessment q2h"
    dvprint "17. Assessment q1h"
    dvprint "18. Assessment q30min"
    dvprint "---------------"
    
    If inds(18).checked Then 'could have been triggered by Iabp
        dvprint "Iabp order triggered indicator 18."
        Exit Sub
    End If
'Pain
SetupBuckets (30)
    desclist = "NURS Pain,NURS Pain CNVI At Rest Total"  ' Presence of Pain,NURS Pain Cries Total,NURS Pain FLACC Total,Nurs Pain Comfort Total"
    PutIntoBuckets (desclist)
PrintBucketFreq "Pain buckets="
ct = CountBuckets
ClearBuckets
    CheckAssessment ct, "Pain=" & ct
    

'symptom mgt
'NURS Symptom Assessment Reason
    desclist = "NURS Symptom Assessment Reason,NURS Beck"
    PutIntoBuckets (desclist)
PrintBucketFreq "Symptom Assessment buckets="
ct = CountBuckets
ClearBuckets
    CheckAssessment ct, "Symptom Assessment=" & ct


'temp
'NURS Temperature
'NURSI Temperature
'NURSI TX Temp Reg Device Status  result=on
    desclist = "NURS Temperature,NURSI Temperature,NURSI TX Temp Reg Device,NURS Post Procedure Temperature,NURS Braden Risk Assessment,NURS Skin Characteristics"
    reslist = " , ,on, , , "
    PutIntoBucketsWithResults desclist, reslist
PrintBucketFreq "Temperature/Skin buckets="
ct = CountBuckets
ClearBuckets
    CheckAssessment ct, "Temperature/Skin=" & ct

' heart rate and rhythm
    desclist = "NURS Heart Rate/Pulse,NURSI Heart Rate/Pulse,NURSI Heart Rhythm,NURSI Pacemaker Type,NURS Circulation Heart Rhythm"
    reslist = " , , , trans, "
    PutIntoBucketsWithResults desclist, reslist
PrintBucketFreq "Heart Rate and Rhythm buckets="
ct = CountBuckets
ClearBuckets
    CheckAssessment ct, "Heart Rate and Rhythm=" & ct

'BP
    desclist = "NURS Blood Pressure,NURSI NIBP,NURSI ABP,NURS Post Procedure Heart Rate/Pulse,NURS Post Procedure Blood Pressure Diastolic,NURS Post Procedure Blood Pressure Systolic"
    PutIntoBuckets (desclist)
PrintBucketFreq "Blood Pressure buckets="
ct = CountBuckets
ClearBuckets
    CheckAssessment ct, "Blood Pressure=" & ct

'Pulmonary
    desclist = "NURS Respiratory,NURSI Resp Depth/Rhythm,NURSI Respiratory Rate,NURSI O2,NURS Post Procedure Respiratory Rate,NURS Sleep Respiration"
    PutIntoBuckets (desclist)
PrintBucketFreq "Pulmonary buckets="
ct = CountBuckets
ClearBuckets
    CheckAssessment ct, "Pulmonary=" & ct

'Vascular
    desclist = "NURS Circulation Capillary Refill,NURS Circulation Pulse Assessment,NURS Post Procedure Heart Rate/Pulse"
    PutIntoBuckets (desclist)
PrintBucketFreq "Vascular buckets="
ct = CountBuckets
ClearBuckets
    CheckAssessment ct, "Vascular=" & ct

'telemetry
    desclist = "NURS Circulation Cardiac Monitor,NURS Circulation Monitor Status,NURS Circulation Cardiac Monitor Interventions,NURS Circulation Rhythm Strip Interpretation"
    PutIntoBuckets (desclist)
PrintBucketFreq "Telemtry buckets="
ct = CountBuckets
ClearBuckets
    CheckAssessment ct, "Telemetry=" & ct

'wound
    desclist = "NURS Procedure Site Observations"
    PutIntoBuckets (desclist)
PrintBucketFreq "Wound buckets="
ct = CountBuckets
ClearBuckets
    CheckAssessment ct, "Wound=" & ct
    
'gastro
    desclist = "NURS Food/Fluid Food Intake Obs"
    PutIntoBuckets (desclist)
PrintBucketFreq "Gastro buckets="
ct = CountBuckets
ClearBuckets
    CheckAssessment ct, "Gastro=" & ct

'Neuro
    desclist = "NURS Neuro Level of Consciousness,NURS Neuro Oriented To,NURS Electrode Types,NURSI Level of Consciousness,NURSI Oriented To,NURSI ICP,NURSI EEG Electrode Types,NURSI RASS,NURS FS Level of Consciousness"
    PutIntoBuckets (desclist)
PrintBucketFreq "Neuro buckets="
ct = CountBuckets
ClearBuckets
    CheckAssessment ct, "Neuro=" & ct

'Genitourinary
    desclist = "NURS Urinary Post Void Residu Measure By,NURS CBI Color of Drainage,NURSI Bladder Mean,NURSI CBI Color of Drainage,NURS Urine Specific Gravity Reading,NURS Urine Hemastix Results"
    reslist = "bladder scan, , , , , "
    PutIntoBucketsWithResults desclist, reslist
PrintBucketFreq "Genitourinary buckets="
ct = CountBuckets
ClearBuckets
    CheckAssessment ct, "Genitourinary=" & ct

'Fluid Balance I&O
    desclist = "io_intake,io_output,NURSI CRRT,NURSI CRRT,NURSI Lumbar Status,NURSI Lumbar Status"
    reslist = " , , started, in progress, continuous, intermittent"
    PutIntoBucketsWithResults desclist, reslist
PrintBucketFreq "Fluid Balance I&O buckets="
ct = CountBuckets
ClearBuckets
    CheckAssessment ct, "Fluid Balance I&O=" & ct

'Glucose
    desclist = "NURS BGM Glucose Reading,NURS BGM Extreme Glucose Reading,NURSI BBGM Result"
    PutIntoBuckets (desclist)
PrintBucketFreq "Glucose buckets="
ct = CountBuckets
ClearBuckets
    CheckAssessment ct, "Glucose=" & ct
'==============
    desclist = "NURS Pain"  ' Presence of Pain,NURS Pain Cries Total,NURS Pain FLACC Total,Nurs Pain Comfort Total"
    PutIntoBuckets (desclist)
    desclist = "NURS Symptom Assessment Reason,NURS Beck"
    PutIntoBuckets (desclist)
    desclist = "NURS Temperature,NURSI Temperature,NURSI TX Temp Reg Device,NURS Post Procedure Temperature,NURS Braden Risk Assessment,NURS Skin Characteristics"
    reslist = " , ,on, , , "
    PutIntoBucketsWithResults desclist, reslist
    desclist = "NURS Heart Rate/Pulse,NURSI Heart Rate/Pulse,NURSI Heart Rhythm,NURSI Pacemaker Type,NURS Circulation Heart Rhythm"
    reslist = " , , , trans, "
    PutIntoBucketsWithResults desclist, reslist
    desclist = "NURS Blood Pressure,NURSI NIBP,NURSI ABP,NURS Post Procedure Heart Rate/Pulse,NURS Post Procedure Blood Pressure Diastolic,NURS Post Procedure Blood Pressure Systolic"
    PutIntoBuckets (desclist)
    desclist = "NURS Respiratory,NURSI Resp Depth/Rhythm,NURSI Respiratory Rate,NURSI O2,NURS Post Procedure Respiratory Rate"
    PutIntoBuckets (desclist)
    desclist = "NURS Circulation Capillary Refill,NURS Circulation Pulse Assessment,NURS Post Procedure Heart Rate/Pulse"
    PutIntoBuckets (desclist)
    desclist = "NURS Circulation Cardiac Monitor,NURS Circulation Monitor Status,NURS Circulation Cardiac Monitor Interventions,NURS Circulation Rhythm Strip Interpretation"
    PutIntoBuckets (desclist)
    desclist = "NURS Procedure Site Observations"
    PutIntoBuckets (desclist)
    desclist = "NURS Food/Fluid Food Intake Obs"
    PutIntoBuckets (desclist)
    desclist = "NURS Neuro Level of Consciousness,NURS Neuro Oriented To,NURS Electrode Types,NURSI Level of Consciousness,NURSI Oriented To,NURSI ICP,NURSI EEG Electrode Types,NURSI RASS,NURS FS Level of Consciousness"
    PutIntoBuckets (desclist)
    desclist = "NURS Urinary Post Void Residu Measure By,NURS CBI Color of Drainage,NURSI Bladder Mean,NURSI CBI Color of Drainage,NURS Urine Specific Gravity Reading,NURS Urine Hemastix Results"
    reslist = "bladder scan, , , , , "
    PutIntoBucketsWithResults desclist, reslist
    desclist = "io_intake,io_output,NURSI CRRT,NURSI CRRT,NURSI Lumbar Status,NURSI Lumbar Status"
    reslist = " , , started, in progress, continuous, intermittent"
    PutIntoBucketsWithResults desclist, reslist
    desclist = "NURS BGM Glucose Reading,NURS BGM Extreme Glucose Reading,NURSI BBGM Result"
    PutIntoBuckets (desclist)
PrintBucketFreq "Combined="
ct = CountBuckets
ClearBuckets
    CheckAssessment ct, "Combined=" & ct

'==============
    desclist = ""
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_15161718"
    Resume  'debug
End Sub

Private Sub Check_19()
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "19. Vascular Access Site Mgt q1 Hour"
    dvprint "---------------"
    

        
SetIndIfResultContains 19, "", "", "NURS Chemo/Bio Site Check1", "", "every 1 hour"
SetIndIfResultContains 19, "", "", "NURS VADCD Catheter Type1", "", "Arterial, cordis, PA catheter, A Sheath, V Sheath"
SetIndIfResultContains 19, "", "", "NURS VADP Site Check", "", "every hour"
If inds(19).checked Then Exit Sub
SetIndIfResultContains 19, "", "", "NURS VADPIV Site Check", "", "every hour"
SetIndIfResultContains 19, "", "", "NURS VADC Site Check", "", "every hour"
SetIndIfResultContains 19, "", "", "NURS VADCT Site Check", "", "every hour"
If inds(19).checked Then Exit Sub
SetIndIfResultContains 19, "", "", "NURSI PAP Systolic", "", ""
SetIndIfResultContains 19, "", "", "NURSI ABP", "", ""
SetIndIfResultContains 19, "", "", "NURSI CRRT Mode", "", "SCUF, CVVH, CVVHDF"
SetIndIfResultContains 19, "", "", "NURSI Site Access", "", ""
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_19"
    Resume  'debug
End Sub
Private Sub Check_20()
    Dim found_what As String
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "20. Medication Activity >= 20 minutes"
    dvprint "---------------"
    
SetIndIfResultContains 20, "", "", "NURS Vital Sign Reason", "", "transfusion"
SetIndIfResultContains 20, "", "", "NURS Chemo Pre-Administration Prep", "", ""
SetIndIfResultContains 20, "", "", "NURS Pain Pharmacologic Interventions Provided", "", _
    "Continuous analgesic infusion started,Continuous analgesic infusion increased,Continuous analgesic infusion decreased"
If inds(20).checked Then Exit Sub
SetIndIfResultContains 20, "", "", "DTM Blood Component", "", ""
SetIndIfResultContains 20, "", "", "DTM Cell Therapy Product", "", ""
SetIndIfResultContains 20, "", "", "NURS Cell Products", "", ""
If inds(20).checked Then Exit Sub
SetIndIfResultContains 20, "", "", "NURSI Vital Sign Reason", "", "transfusion"
'SetIndIfResultContains 20, "", "", "NURS Pain Pharmacologic Interventions Provided", "", "Continuous pain infusion started"
'SetIndIfResultContains 20, "", "", "NURSI Pain Pharmacologic Interventions Provided", "", "Continuous pain infusion started"
SetIndIfResultContains 20, "", "", "NURSI CRRT", "", "Started, Primed with heparin,reprimed with saline,Primed with saline only"

SetIndIfResultContains 20, "", "", "NURS_EN_Interventions", "", "Medication given"
SetIndIfResultContains 20, "", "", "NURSI RASS", "", "4,3,2,1"
'SetIndIfResultContains 20, "", "", "NURS Pain Pharmacologic Interventions Provided", "", "Continuous pain infusion"
SetIndIfResultContains 20, "", "", "DTM Transfusion Reaction Symptoms", "", ""
SetIndIfResultContains 20, "", "", "NURSI CRRTStatus", "", "Started, Primed with heparin,reprimed with saline,Primed with saline only"
SetIndIfResultContains 20, "", "", "io_intake_colloid_albumin_5", "", ""
SetIndIfResultContains 20, "", "", "io_intake_colloid_albumin_25", "", ""
SetIndIfResultContains 20, "", "", "io_intake_drip_ped", "", ""
SetIndIfResultContains 20, "", "", "io_intake_drip_adlt", "", ""
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_20"
    Resume  'debug
End Sub

'Private Sub CheckWound(total As Integer)
'    If (inds(20).checked) Then Exit Sub             'skip if highest already checked
'
'    If (total = 0) Then
'        'skip it
'    ElseIf (total >= 30) Then
'        SetInd 20, "wound >= 30 min"
'    Else
'        SetInd 19, "wound < 30 min"
'    End If
'End Sub

Private Sub Check_2122()
    Dim return_result As String
    Dim codelist As String
    Dim found_what As String
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "21. Wound/Injury Mgmt"
    dvprint "22. Wound/Injury Mgmt >= 30 Minutes"
    dvprint "---------------"

SetIndIfResultContains 22, "", "", "NURS Wound Care Total", "", "Greater than 30 minutes"
SetIndIfResultContains 22, "", "", "NURSI Wound CareTotal", "", "Greater than 30 minutes"
SetIndIfResultContains 22, "", "", "NURSI Site Pressure", "", ""
If inds(22).checked Then Exit Sub

    If CheckVadCDCNT(found_what) Then SetInd 22, found_what
    
If inds(22).checked Then Exit Sub
    
'NURS VADC Catheter Type 1  non-tunneled or lumen
'NURS VADC Removal Date
    If CheckVadC(found_what) Then SetInd 22, found_what

If inds(22).checked Then Exit Sub

    If CheckWounds(found_what) Then SetInd 21, found_what
    
If inds(21).checked Then Exit Sub

SetIndIfResultContains 21, "", "", "NURS Incision _ Type of Dressing", "", "Foam, Gauze, Hydrogel, Hydrocolloid, Non-stick gauze, Transparent film, Wet to dry, Wet to moist, Silicone, Alginate"
SetIndIfResultContains 21, "", "", "NURS Incision Drain _ Drainage Odor", "", "Malodorous"
SetIndIfResultContains 21, "", "", "NURS Wound Drain _ Drainage Odor", "", "Malodorous"

If inds(21).checked Then Exit Sub
SetIndIfResultContains 21, "", "", "NURS Pressure Ulcer _ Type of Dressing", "", "Foam, Gauze, Hydrogel, Hydrocolloid, Non-stick gauze, Transparent film, Wet to dry, Wet to moist, Silicone, Alginate"
If inds(21).checked Then Exit Sub

SetIndIfResultContains 21, "", "", "NURS VADCT Site Care", "", "Yes"
SetIndIfResultContains 21, "", "", "NURS VADCNT Site Care", "", "Yes"
If inds(21).checked Then Exit Sub
SetIndIfResultContains 21, "", "", "NURS VADCP Site Care", "", "Yes"
SetIndIfResultContains 21, "", "", "NURS VADCD Site Care", "", "Yes"
SetIndIfResultContains 21, "", "", "NURS VADC Site Care", "", "Yes"
If inds(21).checked Then Exit Sub

SetIndIfResultContains 21, "", "", "NURS Pain CAP Insertion Site", "", ""
SetIndIfResultContains 21, "", "", "NURS Elimination Stoma Type", "", ""
SetIndIfResultContains 21, "", "", "NURS El Stoma Interventions", "", ""
If inds(21).checked Then Exit Sub
SetIndIfResultContains 21, "", "", "NURS Color Stool", "", "gross blood noted, tarry stools noted"
SetIndIfResultContains 21, "", "", "NURS Circulation Bleeding Obs Status", "", "active bleeding"
SetIndIfResultContains 21, "", "", "NURS Skin Characteristics", "", "fissured, excoriated, wet desquamation, dry desquamation"
If inds(21).checked Then Exit Sub
SetIndIfResultContains 21, "", "", "NURS Respiratory Artificial Airway Maint", "", ""
SetIndIfResultContains 21, "", "", "NURS Respiratory Tracheostomy Tube Type", "", ""
SetIndIfResultContains 21, "", "", "NURS CT Interventions", "", ""
If inds(21).checked Then Exit Sub
SetIndIfResultContains 21, "", "", "NURS CT Tube Type", "", ""
SetIndIfResultContains 21, "", "", "NURS Tubes/Drains Interventions", "", "Esophagostomy, gastrostomy, jejunostomy"
SetIndIfResultContains 21, "", "", "NURS Oral Care", "", "Lip hydration, Oral hydration, Mouth Care"
SetIndIfResultContains 21, "", "", "NURSI CBI Color of Drainage", "", ""
SetIndIfResultContains 21, "", "", "io_output_dr_lumbar", "", ""
If inds(21).checked Then Exit Sub

SetIndIfResultContains 21, "", "", "NURSI CBI Color of Drainage", "", ""
SetIndIfResultContains 21, "", "", "NURSI GU Stoma Type", "", ""
SetIndIfResultContains 21, "", "", "NURSI GU Stoma Care", "", ""
If inds(21).checked Then Exit Sub
SetIndIfResultContains 21, "", "", "NURSI Urine Drain Interventiosns", "", "Dressing changed, site cleansed"
SetIndIfResultContains 21, "", "", "NURSI GI Stoma Type", "", ""
SetIndIfResultContains 21, "", "", "NURSI GI Tube Type", "", "G-tube, PEG, J-tube, G-J tube"
If inds(21).checked Then Exit Sub
SetIndIfResultContains 21, "", "", "NURSI Tracheostomy Tube Type", "", ""
SetIndIfResultContains 21, "", "", "NURSI Tracheostomy Care", "", ""
SetIndIfResultContains 21, "", "", "NURSI TX I Care 1", "", ""
If inds(21).checked Then Exit Sub
SetIndIfResultContains 21, "", "", "NURSI TX W Drain Type of Drain/Suction 1", "", ""
SetIndIfResultContains 21, "", "", "NURSI TX Skin Characteristics", "", "rash, cracked, fissured, excoriated, wet desquamation, dry desquamation"
SetIndIfResultContains 21, "", "", "NURSI TX Drain Intervention 1", "", ""
If inds(21).checked Then Exit Sub
SetIndIfResultContains 21, "", "", "NURSI TX I Drain Type of Drain/Suction 1", "", ""
SetIndIfResultContains 21, "", "", "NURSI Site Access", "", ""
SetIndIfResultContains 21, "", "", "NURSI TX P Care 1", "", ""
If inds(21).checked Then Exit Sub
SetIndIfResultContains 21, "", "", "NURSI Chest Tube Dressing", "", ""
SetIndIfResultContains 21, "", "", "NURSI Epidural Dressing", "", ""
If inds(21).checked Then Exit Sub
SetIndIfResultContains 21, "", "", "NURSI Tracheostomy Care", "", ""
SetIndIfResultContains 21, "", "", "NURSI Bowel Stool Color/Consistency", "", "gross blood, tarry stool"
SetIndIfResultContains 21, "", "", "NURSI Lumbar Dressing", "", ""
If inds(21).checked Then Exit Sub
SetIndIfResultContains 21, "", "", "NURSI Ventric Dressing", "", ""
SetIndIfResultContains 21, "", "", "NURSI EEG Electrode Types", "", ""
If inds(21).checked Then Exit Sub

SetIndIfResultContains 21, "", "", "NURSI TX I Location", "", ""
SetIndIfResultContains 21, "", "", "NURSI TX I Sensation", "", ""
SetIndIfResultContains 21, "", "", "NURSI TX I Appearance", "", ""
SetIndIfResultContains 21, "", "", "NURSI TX I Length", "", ""
If inds(21).checked Then Exit Sub
SetIndIfResultContains 21, "", "", "NURSI TX I Width", "", ""
SetIndIfResultContains 21, "", "", "NURSI TX I Depth", "", ""
SetIndIfResultContains 21, "", "", "NURSI TX I Dressing", "", ""
SetIndIfResultContains 21, "", "", "NURSI TX I Closure", "", ""
If inds(21).checked Then Exit Sub
SetIndIfResultContains 21, "", "", "NURSI TX I Drainage", "", ""
SetIndIfResultContains 21, "", "", "NURSI TX I Care", "", ""
SetIndIfResultContains 21, "", "", "NURSI TX I Drsg Type", "", "Foam, Gauze, Hydrogel, Hydrocolloid, Non-stick gauze, Transparent film, Wet to dry, Wet to moist, Silicone, Alginate"
SetIndIfResultContains 21, "", "", "NURSI TX I Neg Pressure Wound Therapy Setting", "", ""
If inds(21).checked Then Exit Sub
SetIndIfResultContains 21, "", "", "NURSI TX I Drain Type of Drain/Suction", "", ""
SetIndIfResultContains 21, "", "", "NURSI TX I Drain Intervention", "", ""
SetIndIfResultContains 21, "", "", "NURSI TX I Drain Odor", "", ""
If inds(21).checked Then Exit Sub

SetIndIfResultContains 21, "", "", "NURSI TX P Location", "", ""
SetIndIfResultContains 21, "", "", "NURSI TX P Sensation", "", ""
SetIndIfResultContains 21, "", "", "NURSI TX P Wound Bed", "", ""
SetIndIfResultContains 21, "", "", "NURSI TX P Length", "", ""
If inds(21).checked Then Exit Sub
SetIndIfResultContains 21, "", "", "NURSI TX P Width", "", ""
SetIndIfResultContains 21, "", "", "NURSI TX P Depth", "", ""
SetIndIfResultContains 21, "", "", "NURSI TX P Ulcer Staging/Description", "", ""
SetIndIfResultContains 21, "", "", "NURSI TX P Peri-wound", "", ""
If inds(21).checked Then Exit Sub
SetIndIfResultContains 21, "", "", "NURSI TX P Dressing", "", ""
SetIndIfResultContains 21, "", "", "NURSI TX P Drainage", "", ""
SetIndIfResultContains 21, "", "", "NURSI TX P Care", "", ""
SetIndIfResultContains 21, "", "", "NURSI TX P Drsg Type", "", "Foam, Gauze, Hydrogel, Hydrocolloid, Non-stick gauze, Transparent film, Wet to dry, Wet to moist, Silicone, Alginate"
SetIndIfResultContains 21, "", "", "NURSI TX P Neg Pressure Wound Therapy Setting", "", ""
If inds(21).checked Then Exit Sub

SetIndIfResultContains 21, "", "", "NURSI TX W Location", "", ""
SetIndIfResultContains 21, "", "", "NURSI TX W Sensation", "", ""
SetIndIfResultContains 21, "", "", "NURSI TX W Bed", "", ""
SetIndIfResultContains 21, "", "", "NURSI TX W Length", "", ""
If inds(21).checked Then Exit Sub
SetIndIfResultContains 21, "", "", "NURSI TX W Width", "", ""
SetIndIfResultContains 21, "", "", "NURSI TX W Depth", "", ""
SetIndIfResultContains 21, "", "", "NURSI TX W Closure", "", ""
SetIndIfResultContains 21, "", "", "NURSI TX W Drainage", "", ""
If inds(21).checked Then Exit Sub
SetIndIfResultContains 21, "", "", "NURSI TX W Care", "", ""
SetIndIfResultContains 21, "", "", "NURSI TX W Drsg Type", "", "Foam, Gauze, Hydrogel, Hydrocolloid, Non-stick gauze, Transparent film, Wet to dry, Wet to moist, Silicone, Alginate"
SetIndIfResultContains 21, "", "", "NURSI TX W Neg Pressure Wound Therapy Setting", "", ""
SetIndIfResultContains 21, "", "", "NURSI TX W Drain Type of Drain/Suction", "", ""
SetIndIfResultContains 21, "", "", "NURSI TX W Drain Intervention", "", ""
SetIndIfResultContains 21, "", "", "NURSI TX W Drain Odor", "", ""

    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_2122"
    Resume  'debug
End Sub


Private Sub Check_23()
    Dim fnd284 As Boolean
    Dim fnd295 As Boolean
    
    On Error GoTo errHandler
    
    Dim mins As Integer
    Dim chart_result As String
    
    dvprint "---------------"
    dvprint "23. Healthcare Mgmt Education >= 1 Hour"
    dvprint "---------------"

SetIndIfResultContains 23, "", "", "NIH Patient Ed Instruction Duration", "", "Greater than 1 hour"
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_23"
    Resume  'debug
End Sub

Private Sub Check_24()
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "24. 1 to 1 Physiological Interv. >= 2 Hours"
    dvprint "---------------"

    SetIndIfResultContains 24, "", "", "NURS_AC Intervention", "", "Yes"
    SetIndIfResultContains 24, "", "", "NSG_IABP Settings", "", ""
    SetIndIfResultContains 24, "", "", "DTM Blood Component", "", ""

    If inds(24).checked Then Exit Sub
    
'    If CountWithin2Hours("NURS Heart Rate/Pulse") >= 8 Then
'        SetInd 24, "Found at least 8 of NURS Heart Rate/Pulse within 2 hours."
'    End If
'    If inds(24).checked Then Exit Sub
'
'    If CountWithin2Hours("NURS Blood Pressure Systolic") >= 8 Then
'        SetInd 24, "Found at least 8 of NURS Blood Pressure Systolic within 2 hours."
'    End If
'    If inds(24).checked Then Exit Sub
'    If CountWithin2Hours("NURS Blood Pressure Diastolic") >= 8 Then
'        SetInd 24, "Found at least 8 of NURS Blood Pressure Diastolic within 2 hours."
'    End If
'    If inds(24).checked Then Exit Sub
'    If CountWithin2Hours("NURS Respiratory Rate") >= 8 Then
'        SetInd 24, "Found at least 8 of NURS Respiratory Rate within 2 hours."
'    End If
'    If inds(24).checked Then Exit Sub
'    If CountWithin2Hours("NURS Respiratory Oxygen Saturation") >= 8 Then
'        SetInd 24, "Found at least 8 of NURS Respiratory Oxygen Saturation within 2 hours."
'    End If
'    If inds(24).checked Then Exit Sub
'    If CountWithin2Hours("NURS Neuro Level of Consciousness") >= 8 Then
'        SetInd 24, "Found at least 8 of NURS Neuro Level of Consciousness within 2 hours."
'    End If
'    If inds(24).checked Then Exit Sub
'    If CountWithin2Hours("NURS Neuro Oriented To") >= 8 Then
'        SetInd 24, "Found at least 8 of NURS Neuro Oriented To within 2 hours."
'    End If
'    If inds(24).checked Then Exit Sub
'
'    If CountWithin2Hours("NURSI Temperature") >= 8 Then
'        SetInd 24, "Found at least 8 of NURSI Temperature within 2 hours."
'    End If
'    If inds(24).checked Then Exit Sub
'    If CountWithin2Hours("NURSI Heart Rate/Pulse") >= 8 Then
'        SetInd 24, "Found at least 8 of NURSI Heart Rate/Pulse within 2 hours."
'    End If
'    If inds(24).checked Then Exit Sub
'    If CountWithin2Hours("NURSI NIBP Systolic") >= 8 Then
'        SetInd 24, "Found at least 8 of NURSI NIBP Systolic within 2 hours."
'    End If
'    If inds(24).checked Then Exit Sub
'    If CountWithin2Hours("NURSI NIBP Diastolic") >= 8 Then
'        SetInd 24, "Found at least 8 of NURSI NIBP Diastolic within 2 hours."
'    End If
'    If inds(24).checked Then Exit Sub
'    If CountWithin2Hours("NURSI ABP Systolic") >= 8 Then
'        SetInd 24, "Found at least 8 of NURSI ABP Systolic within 2 hours."
'    End If
'    If inds(24).checked Then Exit Sub
'    If CountWithin2Hours("NURSI ABP Diastolic") >= 8 Then
'        SetInd 24, "Found at least 8 of NURSI ABP Diastolic within 2 hours."
'    End If
'    If inds(24).checked Then Exit Sub
'    If CountWithin2Hours("NURSI Respiratory Rate") >= 8 Then
'        SetInd 24, "Found at least 8 of NURSI Respiratory Rate within 2 hours."
'    End If
'    If inds(24).checked Then Exit Sub
'    If CountWithin2Hours("NURSI O2 Saturation") >= 8 Then
'        SetInd 24, "Found at least 8 of NURSI O2 Saturation within 2 hours."
'    End If
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_24"
    Resume  'debug
End Sub
'Private Sub CheckCustom()
'    On Error GoTo errHandler
'
'    dvprint "---------------"
'    dvprint "33. Central Line"
'    dvprint "34. Ventilator"
'    dvprint "35. Foley Catheter"
'    dvprint "---------------"
'
'    SetIndIfFound 33, "", "CENTRAL_LINE,GROSHONG,HICKMAN,IMPLANTED_PORT,PICC,POWER_PORT,ADAPTER"
'    If Not inds(33).checked Then
'        'until discontinued
'        SetIndIfFound 33, "", "CENTRL,CRLDSG,PICCARE"
'    End If
'
'    If Not inds(33).checked Then
'        SetIndIfFound 33, "", "FND103633,FND103481"
'    End If
'
'    If Not inds(33).checked Then
'        SetIndIfFound 33, "", "FND103311,FND103809,FND103338,FND103339,FND103340,FND103341,FND103342,FND103343,FND103344"
'    End If
'
'    If Not inds(33).checked Then
'    If Exists("", "FND102293") Then
'        If Exists("", "FND10338,FND10339,FND10340,FND10341,FND10342,FND10343,FND10344") Then
'            SetInd 33, "FND102293"
'        End If
'    End If
'    End If
'
'    SetIndIfFound 34, "", "FND10386,FND10387,FND10388,FND10390,FND10391,VENTCHG,VENT1ST"
'    If Not inds(34).checked Then
'        SetIndIfFound 34, "", "FND10360,FND10361,FND10456,FND103633,FND101679,FND103618,FND103621,FND103622"
'    End If
'
'
'    If Not Exists("", "FND107250") Then   'FND107250 negates 35
'    SetIndIfFound 35, "", "FOLEY,Ucatheter,FOLEYIR"
'    If Not inds(35).checked Then
'        SetIndIfFound 35, "", "FND188,CATH1"
'    End If
'    If Not inds(35).checked Then
'        SetIndIfFound 35, "", "FND100323,FND100321,FND100322,FND104310,FND104309"
'    End If
'    If Not inds(35).checked Then
'        SetIndIfFound 35, "", "FND104315,FND102318,FND100594,FND100325,FND100326"
'    End If
'    If Not inds(35).checked Then
'        SetIndIfFound 35, "", "FND107399,FND107400,FND107401,FND107402,FND107403"
'    End If
'    If Not inds(35).checked Then
'        SetIndIfFound 35, "", "FND107404,FND107405,FND038"
'    End If
'    End If 'negate
'
'
'    Exit Sub
'
'errHandler:
'    g_util.ThrowError "CheckCustom"
'    Resume  'debug
'End Sub

Private Sub ProcessProc(pnum As Integer, res As String)
    Dim sql As String
    Dim rs As New Recordset
    Dim rs2 As New Recordset
    Dim f As String
    Dim done As Boolean

    sql = "select distinct(event_datetime) from chart_item " & WhereBase(SearchPullRange)
    sql = sql & " and description='NURS Med/Surg Activities'"
    sql = sql & " and result like '%" & res & "%'"
    rs.Open sql, g_cnADO
    Do While Not rs.EOF
        done = False
        sql = "select result from chart_item " & WhereBase(SearchPullRange)
        sql = sql & " and description='NURS Med/Surg Activites Freq'"
        sql = sql & " and event_datetime=" & g_dbutil.SQL_DateTime(rs(0))
        rs2.Open sql, g_cnADO
        Do While Not rs2.EOF And Not done
            f = rs2(0)
    dvprint "NURS Med/Surg Activities Freq=" & f
            numprocs = numprocs + 1
            procs(numprocs).pnum = pnum
            procs(numprocs).start = rs(0)
            procs(numprocs).finish = DateAdd("h", CInt(Mid$(f, 1, 1)), rs(0))
            done = True
        Loop
        rs2.Close
        rs.MoveNext
    Loop
    rs.Close
    
    sql = "select distinct(event_datetime) from chart_item " & WhereBase(SearchPullRange)
    sql = sql & " and description='NURSI Hour Activities'"
    sql = sql & " and result like '%" & res & "%'"
    rs.Open sql, g_cnADO
    Do While Not rs.EOF
        done = False
        sql = "select result from chart_item " & WhereBase(SearchPullRange)
        sql = sql & " and description='NURSI Hour Activities Freq'"
        sql = sql & " and event_datetime=" & g_dbutil.SQL_DateTime(rs(0))
        rs2.Open sql, g_cnADO
        Do While Not rs2.EOF And Not done
            f = rs2(0)
    dvprint "NURSI Hour Activities Freq=" & f
            numprocs = numprocs + 1
            procs(numprocs).pnum = pnum
            procs(numprocs).start = rs(0)
            procs(numprocs).finish = DateAdd("h", CInt(Mid$(f, 1, 1)), rs(0))
            done = True
        Loop
        rs2.Close
        rs.MoveNext
    Loop
    rs.Close

End Sub
'
Private Sub CheckProcs()
    Dim return_result As String
    Dim proc8, proc9 As Boolean
'1:1 Safety observation by RN
'1:1 Safety Observation by Non-RN
'Off unit accompanied by RN
'Off unit accompanied by non-RN
'Patient/family education by RN
'Extensive wound management by RN
'Extensive wound management by non-RN
'Coordination of care by RN
'1:1 by RN at the bedside
'1:1 by non-RN at the bedside
'2:1 by RN at the bedside


    On Error GoTo errHandler
    dvprint "---------------"
    dvprint "P1. 1-1 safety observation by RN"
    dvprint "---------------"

    ProcessProc 1, "1:1 Safety observation by RN"

    dvprint "---------------"
    dvprint "P2. 1-1 safety observation by non-RN"
    dvprint "---------------"

    ProcessProc 2, "1:1 Safety observation by non-%RN"

    dvprint "---------------"
    dvprint "P3. Off unit accompanied by RN"
    dvprint "---------------"

    ProcessProc 3, "Off unit accompanied by RN"

    dvprint "---------------"
    dvprint "P4. Off unit accompanied by non-RN"
    dvprint "---------------"

    ProcessProc 4, "Off unit accompanied by non-RN"

    dvprint "---------------"
    dvprint "P5. Patient/family education by RN"
    dvprint "---------------"

    ProcessProc 5, "Patient/family education by RN"

    dvprint "---------------"
    dvprint "P6. Extensive wound management by RN"
    dvprint "---------------"

    ProcessProc 6, "Extensive wound management by RN"

    dvprint "---------------"
    dvprint "P7. Extensive wound management by non-RN"
    dvprint "---------------"

    ProcessProc 7, "Extensive wound management by non-RN"

    dvprint "---------------"
    dvprint "P8. Coordination of care by RN"
    dvprint "---------------"

    ProcessProc 8, "Coordination of care by RN"

    dvprint "---------------"
    dvprint "P9. 1-1 by RN at bedside"
    dvprint "---------------"
    
    ProcessProc 9, "1:1 by RN at the bedside"

    dvprint "---------------"
    dvprint "P10. 1-1 by non-RN at bedside"
    dvprint "---------------"
    
    ProcessProc 10, "1:1 by non-RN at the bedside"
    
    dvprint "---------------"
    dvprint "P11. 2-1 by RN at bedside"
    dvprint "---------------"
    
    ProcessProc 11, "2:1 by RN at the bedside"
    
    Exit Sub

errHandler:
    g_util.ThrowError "CheckProcs"
    Resume  'debug
End Sub
'Private Sub CheckOutcomes()
'    Dim return_result As String
'
'    'How to handle multiple outcomes per pt, each with different times?
'    dvprint "---------------"
'    dvprint "Outcomes: FALL"
'    dvprint "---------------"
'    If GetResult("", "FALL", "", "", return_result, SearchOutcomesRange) Then
'        numoutcomes = numoutcomes + 1
'        With oc(numoutcomes)
'            .checked = True
'            .pnum = 1
'            .start = return_result
'        End With
'    End If
'
'    dvprint "---------------"
'    dvprint "Outcomes: MED"
'    dvprint "---------------"
'    If GetResult("", "MED", "", "", return_result, SearchOutcomesRange) Then
'        numoutcomes = numoutcomes + 1
'        With oc(numoutcomes)
'            .checked = True
'            .pnum = 2
'            .start = return_result
'        End With
'    End If
'
'    dvprint "---------------"
'    dvprint "Outcomes: PROC"
'    dvprint "---------------"
'    If GetResult("", "PROC", "", "", return_result, SearchOutcomesRange) Then
'        numoutcomes = numoutcomes + 1
'        With oc(numoutcomes)
'            .checked = True
'            .pnum = 21
'            .start = return_result
'        End With
'    End If
'
'    Exit Sub
'
'errHandler:
'    g_util.ThrowError "CheckOutcomes"
'    Resume  'debug
'End Sub


'Private Sub AtLeastOneADL()
'    If Not (inds(1).checked Or inds(2).checked Or inds(3).checked) Then
'        'Note: Heather wants "#2 ADL - Assist" to be the default.  (90% of patients)
'        SetInd 2, "at least one ADL"
'    End If
'End Sub

Private Sub HighestIndicatorInEachGroupWins()
    Dim i As Integer
    Dim g As Integer
    Dim highest_is_on As Boolean
    Dim ind_list As String

    dvprint "---------------"
    dprint "Select highest indicator in each group"
    
    g = 0
    highest_is_on = False
    For i = MAX_INDS To 1 Step -1
        If (inds(i).group > 0) Then
            If (inds(i).group <> g) Then
                'this is a new group
                g = inds(i).group
                highest_is_on = inds(i).checked
            Else
                'same group
                If highest_is_on Then
                    inds(i).checked = False             'uncheck a lower number
                Else
                    highest_is_on = inds(i).checked     'save this one
                End If
            End If
        End If
    Next i
    
    'Echo the indicators for an audit (no classification will be saved)
    If g_debug And g_no_output Then
        For i = 1 To MAX_INDS
            If inds(i).checked Then ind_list = ind_list & "," & i
        Next i
        dprint "Final list = " & Mid$(ind_list, 2)
    End If

End Sub
Private Sub SetClassString()
    Dim i As Integer
    Dim txarea As String
    Dim adl1only As String

    txarea = ""
    adl1only = ""
'    If m_pat.unit_id = 108 Then 'surg/ortho/peds
'        If m_pat.age <= 16 Then txarea = "Pediatrics"
'    End If
            
    outstr = g_util.FixedWidth("1", 8)                                       '(facility code)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.unit_name, 16)
    outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(unit code)
    outstr = outstr & "|" & g_util.FixedWidth(txarea, 16)                       '(area code)
    outstr = outstr & "|" & g_util.FixedWidth("", 8)                        '(class date - give datetime instead)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.acct, 20)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.last_name, 32)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.first_name, 32)
    outstr = outstr & "|" & g_util.FixedWidth("", 32)                       '(middle)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.room, 8)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.bed, 4)
    outstr = outstr & "|" & Format$(m_pat.effective, "yyyymmddhhnn")      'class datetime
    outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(login)
    outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(employee)
    outstr = outstr & "|" & g_util.FixedWidth(CStr(m_pat.meth_id), 4)
    outstr = outstr & "|" & g_util.FixedWidth("C", 1)                       'record type = class
    outstr = outstr & "|" & g_util.FixedWidth("", 4)                        '(stage)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.TC_source_id, 4)        'TC source ID
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.range, 4)               'TC pull range
    outstr = outstr & "|"
    outstr = g_util.FixedWidth(outstr, 294)
    outstr = outstr & "|" & Format$(m_pat.effective, "yyyymmddhhnn")        'IN
    outstr = g_util.FixedWidth(outstr, 346)
'    If m_pat.outdt <> 0 Then
'        outstr = outstr & "|" & Format$(m_pat.outdt, "yyyymmddhhnn")                '348 = OUT
'    End If
    outstr = g_util.FixedWidth(outstr, 377)
    outstr = outstr & "|"
    
    For i = 1 To MAX_INDS
        adl1only = adl1only & "N"
        If (inds(i).checked) Then
            outstr = outstr & "Y"
            ind_list = ind_list & "," & i
        Else
            outstr = outstr & "N"
        End If
    Next i
    Mid$(adl1only, 1) = "Y"
    If adl_default And no_assess_items And Mid$(outstr, 379, MAX_INDS) = adl1only Then
        Mid$(outstr, 379) = "N"
        ind_list = ""
    Else
        ind_list = Mid$(ind_list, InStr(1, ind_list, ",") + 1)                'strip leading comma
    End If
End Sub
Private Sub OutputDNCandClass()
    Dim i As Integer
    Dim numloa_LT_start As Integer
    
dvprint "OutputDNCandCLASS"

    If m_pat.num_loa <= 0 Then
        Print #outfile, outstr
    Else
dvprint "num_loa=" & m_pat.num_loa
        For i = 1 To m_pat.num_loa
            dvprint "i=" & i
            If m_pat.loa(i).startdt <= m_pat.pull_start Then
dvprint "numloa_LT_start=" & numloa_LT_start
                If numloa_LT_start > 0 Then 'need to add a class here before the next dnc.
dvprint "add class before next dnc"
                    Mid$(outstr, 204, 12) = Format$(m_pat.loa(i - 1).enddt, "yyyymmddhhnn") 'class
                    Mid$(outstr, 296, 12) = Format$(m_pat.loa(i - 1).enddt, "yyyymmddhhnn") 'in
                    Mid$(outstr, 348, 12) = Format$(m_pat.loa(i).startdt, "yyyymmddhhnn") 'out
                    Print #outfile, outstr
                End If
                numloa_LT_start = numloa_LT_start + 1
                OutputDNC m_pat.encounter_id, _
                          m_pat.unit_id, _
                          m_pat.loa(i).startdt, _
                          m_pat.loa(i).enddt
            ElseIf m_pat.loa(i).startdt > m_pat.pull_start Then
 dvprint "startdt>pull start"
                If i - 1 >= 1 Then 'use the enddt of i-1 as the start of the class
                    If m_pat.loa(i - 1).enddt < m_pat.loa(i).startdt Then 'dont want zero-length class
                        Mid$(outstr, 204, 12) = Format$(m_pat.loa(i - 1).enddt, "yyyymmddhhnn") 'class
                        Mid$(outstr, 296, 12) = Format$(m_pat.loa(i - 1).enddt, "yyyymmddhhnn") 'in
                        Mid$(outstr, 348, 12) = Format$(m_pat.loa(i).startdt, "yyyymmddhhnn") 'out
                        Print #outfile, outstr
                    End If
                Else 'use the pull start as the start of the class
                    If m_pat.pull_start < m_pat.loa(i).startdt Then 'dont want zero-length class
                        Mid$(outstr, 204, 12) = Format$(m_pat.pull_start, "yyyymmddhhnn") 'class
                        Mid$(outstr, 296, 12) = Format$(m_pat.pull_start, "yyyymmddhhnn") 'in
                        Mid$(outstr, 348, 12) = Format$(m_pat.loa(i).startdt, "yyyymmddhhnn") 'out
                        Print #outfile, outstr
                    End If
                End If
                OutputDNC m_pat.encounter_id, _
                          m_pat.unit_id, _
                          m_pat.loa(i).startdt, _
                          m_pat.loa(i).enddt
                
            End If
            If Not m_pat.loa(i).retain Then
                XOutLOA m_pat.encounter_id, m_pat.loa(i).startdt, m_pat.loa(i).enddt
            End If
        Next i
        
        If m_pat.loa(m_pat.num_loa).enddt < m_pat.pull_finish Then
            Mid$(outstr, 204, 12) = Format$(m_pat.loa(m_pat.num_loa).enddt, "yyyymmddhhnn") 'class
            Mid$(outstr, 296, 12) = Format$(m_pat.loa(m_pat.num_loa).enddt, "yyyymmddhhnn") 'in
            Mid$(outstr, 348, 12) = Format$(m_pat.pull_finish, "yyyymmddhhnn") 'out
            Print #outfile, outstr
        End If
    End If
    
    AddLogEntry EVENT_TYPE_INFO, "Classified: " & ind_list, EVENT_CATEGORY_PROCESSED

End Sub
'Private Sub OutputClass()
'    Dim outstr As String, ind_list As String
'    Dim i As Integer
'    Dim txarea As String
'
'    txarea = ""
''    If m_pat.unit_id = 108 Then 'surg/ortho/peds
''        If m_pat.age <= 16 Then txarea = "Pediatrics"
''    End If
'
'    outstr = g_util.FixedWidth("1", 8)                                       '(facility code)
'    outstr = outstr & "|" & g_util.FixedWidth(m_pat.unit_name, 16)
'    outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(unit code)
'    outstr = outstr & "|" & g_util.FixedWidth(txarea, 16)                       '(area code)
'    outstr = outstr & "|" & g_util.FixedWidth("", 8)                        '(class date - give datetime instead)
'    outstr = outstr & "|" & g_util.FixedWidth(m_pat.acct, 20)
'    outstr = outstr & "|" & g_util.FixedWidth(m_pat.last_name, 32)
'    outstr = outstr & "|" & g_util.FixedWidth(m_pat.first_name, 32)
'    outstr = outstr & "|" & g_util.FixedWidth("", 32)                       '(middle)
'    outstr = outstr & "|" & g_util.FixedWidth(m_pat.room, 8)
'    outstr = outstr & "|" & g_util.FixedWidth(m_pat.bed, 4)
'    outstr = outstr & "|" & Format$(m_pat.effective, "yyyymmddhhnn")      'class datetime
'    outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(login)
'    outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(employee)
'    outstr = outstr & "|" & g_util.FixedWidth(CStr(m_pat.meth_id), 4)
'    outstr = outstr & "|" & g_util.FixedWidth("C", 1)                       'record type = class
'    outstr = outstr & "|" & g_util.FixedWidth("", 4)                        '(stage)
'    outstr = outstr & "|" & g_util.FixedWidth(m_pat.TC_source_id, 4)        'TC source ID
'    outstr = outstr & "|" & g_util.FixedWidth(m_pat.range, 4)               'TC pull range
'    outstr = outstr & "|"
'    outstr = g_util.FixedWidth(outstr, 294)
'    outstr = outstr & "|" & Format$(m_pat.effective, "yyyymmddhhnn")        'IN
'    outstr = g_util.FixedWidth(outstr, 346)
'    If m_pat.outdt <> 0 Then
'        outstr = outstr & "|" & Format$(m_pat.outdt, "yyyymmddhhnn")                'OUT
'    End If
'    outstr = g_util.FixedWidth(outstr, 377)
'    outstr = outstr & "|"
'
'    For i = 1 To MAX_INDS
'        If (inds(i).checked) Then
'            outstr = outstr & "Y"
'            ind_list = ind_list & "," & i
'        Else
'            outstr = outstr & "N"
'        End If
'    Next i
'    ind_list = Mid$(ind_list, InStr(1, ind_list, ",") + 1)                'strip leading comma
'
'    If m_pat.intloa_startdt = 0 Then 'finishdt is also zero
'        Print #outfile, outstr
'    Else
'        'class before dnc
'        Mid$(outstr, 348, 12) = Format$(m_pat.intloa_startdt, "yyyymmddhhnn") 'out = startdt
'        Print #outfile, outstr
'
'        'class after dnc
'        Mid$(outstr, 204, 12) = Format$(m_pat.intloa_finishdt, "yyyymmddhhnn") 'in = finishdt
'        Mid$(outstr, 296, 12) = Format$(m_pat.intloa_finishdt, "yyyymmddhhnn") 'in = finishdt
'        Mid$(outstr, 348, 12) = Space$(12) 'outdt
'        Print #outfile, outstr
'    End If
'
'    AddLogEntry EVENT_TYPE_INFO, "Classified: " & ind_list, EVENT_CATEGORY_PROCESSED
'End Sub

Private Sub OutputProcs()
    Dim i, j As Integer
    Dim s, f As Date
    Dim outstr As String, proc_list As String

    For i = 1 To numprocs
        procs(i).isvalid = True
    Next i
    For i = 1 To numprocs - 1
        If procs(i).isvalid Then
        For j = i + 1 To numprocs
            If procs(j).isvalid And procs(j).start = procs(i).start And procs(j).finish = procs(i).finish Then   'this can be combined.
                procs(j).isvalid = False
                procs(j).pindex = i
            End If
        Next j
        End If
    Next i

    For i = 1 To numprocs
        If procs(i).isvalid Then
        outstr = g_util.FixedWidth("1", 8)                                       '(facility code)
        outstr = outstr & "|" & g_util.FixedWidth(m_pat.unit_name, 16)
        outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(unit code)
        outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(area code)
        outstr = outstr & "|" & g_util.FixedWidth("", 8)                        '(class date - give datetime instead)
        outstr = outstr & "|" & g_util.FixedWidth(m_pat.acct, 20)
        outstr = outstr & "|" & g_util.FixedWidth(m_pat.last_name, 32)
        outstr = outstr & "|" & g_util.FixedWidth(m_pat.first_name, 32)
        outstr = outstr & "|" & g_util.FixedWidth("", 32)                       '(middle)
        outstr = outstr & "|" & g_util.FixedWidth(m_pat.room, 8)
        outstr = outstr & "|" & g_util.FixedWidth(m_pat.bed, 4)
        outstr = outstr & "|" & Format$(procs(i).start, "yyyymmddhhnn")       '204 procdt
        outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(login)
        outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(employee)
        outstr = outstr & "|" & g_util.FixedWidth(CStr(m_pat.meth_id), 4)
        outstr = outstr & "|" & g_util.FixedWidth("P", 1)                       'record type = class
        outstr = outstr & "|"
        outstr = outstr & Space$(294 - Len(outstr))
        outstr = outstr & "|" & Format$(procs(i).start, "yyyymmddhhnn")       '296 procdt in
        outstr = outstr & Space$(346 - Len(outstr))
        If procs(i).finish = 0 Then
            outstr = outstr & "|" & Space$(12)
        Else
            outstr = outstr & "|" & Format$(procs(i).finish, "yyyymmddhhnn")      '348 procdt out
        End If
        outstr = g_util.FixedWidth(outstr, 377)
        outstr = outstr & "|NNNNNNNNNNN"
        Mid$(outstr, 378 + procs(i).pnum, 1) = "Y"
        proc_list = proc_list & "," & procs(i).pnum
        For j = i + 1 To numprocs
            If Not procs(j).isvalid And procs(j).pindex = i Then
                Mid$(outstr, 378 + procs(j).pnum, 1) = "Y"
                proc_list = proc_list & "," & procs(j).pnum
            End If
        Next j
        proc_list = Mid$(proc_list, InStr(1, proc_list, ",") + 1)          'strip leading comma

        Print #outfile, outstr

        AddLogEntry EVENT_TYPE_INFO, "Procedure: " & proc_list, EVENT_CATEGORY_PROCESSED
        End If 'isvalid
    Next i

End Sub
Private Sub OutputOutcomes()
'    Dim i, j As Integer
'    Dim s, f As Date
'    Dim outstr As String
'    Dim octime As String
'
'    If numoutcomes = 0 Then Exit Sub
'
'    For i = 1 To numoutcomes
'        If (oc(i).checked) Then
'            outstr = g_util.FixedWidth("", 8)
'            outstr = outstr & "|" & g_util.FixedWidth(m_pat.unit_name, 16)
'            outstr = outstr & "|" & g_util.FixedWidth("", 16)
'            outstr = outstr & "|" & g_util.FixedWidth("", 16)
'            outstr = outstr & "|" & g_util.FixedWidth(oc(i).start, 12)
'            outstr = outstr & "|" & g_util.FixedWidth(m_pat.acct, 20)
'            outstr = outstr & "|" & g_util.FixedWidth(oc(i).pnum, 3)
'            Print #outfile2, outstr 'Print line to outcomesindicator.TXT
'        End If
'    Next i
End Sub


Private Function ReturnAssessCount(desclist As String) As Integer
    Dim sql As String
    Dim rs As New Recordset
    Dim ct As Integer
    Dim done_ct As Integer
    Dim mintime As Date
    Dim maxtime As Date
    Dim done As Boolean
    
    ReturnAssessCount = 0

    sql = "select count(distinct(event_datetime)) from chart_item " & WhereBase & AndSimpleItemFilter("", "", desclist, "", "")
    sql = sql & " and event_datetime is not null"
    'dvprint sql
    rs.Open sql, g_cnADO
'    dvprint sql

    If Not rs.EOF Then
        If IsNull(rs(0)) Then
            ct = 0
            rs.Close
            Exit Function
        Else
            ct = rs(0)
            rs.Close
        End If
    Else
        ct = rs(0)
        rs.Close
        Exit Function
    End If
    
    ReturnAssessCount = ct
    
End Function
Private Function ReturnAssessCountWithResults(desclist As String, reslist As String) As Integer
    Dim sql As String
    Dim rs As New Recordset
    Dim ct As Integer
    Dim darr() As String
    Dim rarr() As String
    Dim nd As Integer, nr As Integer, i As Integer
    
    ReturnAssessCountWithResults = 0
    
    nd = g_util.SplitTextOnChar(desclist, ",", darr(), 1, 0)
    nr = g_util.SplitTextOnChar(reslist, ",", rarr(), 1, 0)
    If nd <> nr Then Exit Function
    
    sql = " and ("
    For i = 1 To nd
        If Len(Trim$(rarr(i))) > 0 Then
            sql = sql & "(description like '" & Trim$(darr(i)) & "%' and result like '%" & Trim$(rarr(i)) & "%') or "
        Else
            sql = sql & "(description like '" & Trim$(darr(i)) & "%') or "
        End If
    Next i
    sql = Mid$(sql, 1, Len(sql) - 4) 'remove last " or "
    sql = sql & ")"

    sql = "select count(distinct(event_datetime)) from chart_item " & WhereBase & sql
    sql = sql & " and event_datetime is not null"
    'dvprint sql
    rs.Open sql, g_cnADO
'    dvprint sql

    If Not rs.EOF Then
        If IsNull(rs(0)) Then
            ct = 0
            rs.Close
            Exit Function
        Else
            ct = rs(0)
            rs.Close
        End If
    Else
        ct = rs(0)
        rs.Close
        Exit Function
    End If
    
    ReturnAssessCountWithResults = ct
    
End Function

'Private Function SumEventDateTime(code As String, res As String) As Date
'    Dim sql As String
'    Dim sql2 As String
'    Dim total As Integer
'    Dim rs As New Recordset
'    Dim rs2 As New Recordset
'
'    sql = "select event_datetime from chart_item " & WhereBase
'    sql = sql & " and code=" & g_dbutil.SQL_String(code) & " and result=" & g_dbutil.SQL_String(res) & " and event_datetime is not null"
'    rs.Open sql, g_cnADO
'
'    Do While Not rs.EOF
'        If Not IsNull(rs(0)) Then
'            sql2 = "select sum(result) from chart_item " & WhereBase
'            sql2 = sql & " and code=" & g_dbutil.SQL_String(code) & " and event_datetime=" & g_dbutil.SQL_Date(rs(0))
'            rs2.Open sql2, g_cnADO
'            If Not rs2.EOF Then
'                If Not IsNull(rs2(0)) Then
'                    If IsNumeric(rs2(0)) Then
'                        total = total + rs2(0)
'                    End If
'                End If
'            End If
'        End If
'        rs.MoveNext
'    Loop
'    rs2.Close
'    rs.Close
'    SumEventDateTime = total
'End Function


'Private Function ReturnMaxCount(codelist As String, reslist As String, ByRef numbuckets As Integer) As Integer 'needs quotes in lists!
'    Dim sql As String
'    Dim rs As New Recordset
'    Dim ct As Integer
'    Dim done_ct As Integer
'    Dim mintime As Date
'    Dim maxtime As Date
'    Dim done As Boolean
'    Dim i As Integer
'    Dim j As Integer
'    Dim totals(12) As Boolean
'    Dim bucket(24) As buckettype
'
'    ReturnMaxCount = 0
'    numbuckets = 0
'
'    'do an initial count to see if warrants continuing
'    sql = "select count(*) from chart_item " & WhereBase
'    sql = sql & " and code in (" & codelist & ")"
'    sql = sql & " and result in (" & reslist & ")"
'    sql = sql & " and event_datetime is not null"
'    rs.Open sql, g_cnADO
'    If Not rs.EOF Then
'        If rs(0) = 0 Then
'            rs.Close
'            Exit Function
'        End If
'    End If
'    rs.Close
'
'    'g_pull_start for g_range mins
'    'need to return number of hours between start and finish to get  hits/hour because start may be pt arrival time
'    '1. determine number of hours btwn start and finish.  sets the number of buckets
'    '2.
'    numbuckets = 24
'    For i = 1 To 24
'        bucket(i).start = DateAdd("h", i - 1, g_pull_start)
'        bucket(i).finish = DateAdd("n", 59, bucket(i).start)
'        If bucket(i).finish >= g_pull_finish Then
'            numbuckets = i 'may be less than 24 buckets
'            Exit For
'        End If
'    Next i
'
'    sql = "select "
'    For i = 1 To numbuckets
'        sql = sql & "sum(case when event_datetime between " & bucket(i).start & " and " & bucket(i).finish & " then 1 else 0 end) as range" & i & ","
'    Next i
'    sql = Mid$(sql, 1, Len(sql) - 1) 'remove comma
'    sql = sql & " from chart_item " & WhereBase
'    sql = sql & " and code in (" & codelist & ")"
'    sql = sql & " and result in (" & reslist & ")"
'    sql = sql & " and event_datetime is not null"
'    rs.Open sql, g_cnADO
'
'    ct = 0
'    For i = 1 To numbuckets
'        bucket(i).count = rs(i - 1)
'        If bucket(i).count > 0 Then ct = ct + 1
'    Next i
'    rs.Close
'
'    ReturnMaxCount = ct
'
'    'do an initial count to see if warrants continuing; set the mintime of all codes
''    sql = "select sum(case when event_datetime between CONVERT(VARCHAR(10),GETDATE(),111) + ' 06:00' and CONVERT(VARCHAR(10),GETDATE(),111) + ' 06:59' then 1 else 0 end) as range24,"
''    sql = sql & "sum(case when event_datetime between CONVERT(VARCHAR(10),GETDATE(),111) + ' 05:00' and CONVERT(VARCHAR(10),GETDATE(),111) + ' 05:59' then 1 else 0 end) as range23,"
'
'End Function

Private Function CheckOrder(isocode As String) As Boolean
    Dim sql As String
    Dim rs As New Recordset
    Dim rs2 As New Recordset
    Dim isodt As Date
    Dim check As Boolean, done_check As Boolean
    
    done_check = False
    check = False
    sql = "select event_datetime,result from chart_item " & WhereBase(SearchSinceAdmission)
    sql = sql & " and code like '" & isocode & "%'"
'    sql = sql & " and (description like 'reason for isolation%' or description like 'trigger%')"
    sql = sql & " and order_control in ('NW','XO')"
    dvprint sql
    rs.Open sql, g_cnADO
    Do While Not rs.EOF
        check = True
        sql = "select count(*) from chart_item " & WhereBase(SearchSinceAdmission)
        sql = sql & " and event_datetime>" & g_dbutil.SQL_DateTime(rs(0))
        sql = sql & " and code like '" & isocode & "%'"
        sql = sql & " and result=" & g_dbutil.SQL_String(rs(1))
        sql = sql & " and order_control = 'CA'"
        dvprint sql
        rs2.Open sql, g_cnADO
        If rs2(0) = 0 Then
            dvprint "Order for: " & isocode & "-" & rs(1) & " found as New or Continued on " & g_dbutil.SQL_DateTime(rs(0))
        Else
            check = False 'found a cancel order
        End If
        rs2.Close
        done_check = check Or done_check
        rs.MoveNext
    Loop
    rs.Close
    
    CheckOrder = done_check

End Function
Private Function IsoDC(isocodelist As String) As Boolean
    Dim sql As String
    Dim rs As New Recordset
    Dim DC As Boolean
    Dim arr() As String, n As Integer, i As Integer, s As String
    
    s = "("
    n = g_util.SplitTextOnChar(isocodelist, ",", arr(), 0, 0)
    For i = 0 To n - 1
        arr(i) = "code like '" & arr(i) & "%'"
        If i < n - 1 Then arr(i) = arr(i) & " or "
        s = s & arr(i)
    Next i
    s = s & ")"
    
    DC = False
    sql = "select count(*) from chart_item " & WhereBase
    sql = sql & " and (order_control in ('DC','CA'))"
    sql = sql & " and " & s
    dvprint sql
    rs.Open sql, g_cnADO
    If rs(0) > 0 Then DC = True
    rs.Close
    
    IsoDC = DC

End Function


Private Function CheckVadCDCNT(ByRef found_what As String) As Boolean
    Dim sql As String
    Dim rs As New Recordset
    Dim rs2 As New Recordset
    Dim dt As Date
    Dim desc As String
    Dim done As Boolean

    CheckVadCDCNT = False
    done = False
    found_what = ""
    
    sql = "select description,max(event_datetime) from chart_item " & WhereBase(SearchSinceAdmission)
    sql = sql & " and (description like 'NURS VADCNT Catheter Type%' or description like 'NURS VADCD Catheter Type%') group by description"
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
    Do While Not rs.EOF And Not done
        desc = rs(0)
        If InStr(1, desc, "VADCNT", vbTextCompare) > 0 Then
            desc = "NURS VADCNT Removal Date" & Mid$(desc, Len(desc), 1)
        Else
            desc = "NURS VADCD Removal Date" & Mid$(desc, Len(desc), 1)
        End If
        sql = "select max(event_datetime) from chart_item " & WhereBase(SearchSinceAdmission)
        sql = sql & " and description=" & g_dbutil.SQL_String(desc)
        sql = sql & " and event_datetime>" & g_dbutil.SQL_DateTime(rs(1))
        rs2.Open sql, g_cnADO
        If Not IsNull(rs2(0)) Then
            CheckVadCDCNT = True
            found_what = "Found " & rs(0) & " with Removal Date of " & rs2(0)
            done = True
        End If
        rs2.Close
        
        rs.MoveNext
    Loop
    End If 'not eof
    
    rs.Close

End Function


Private Function CheckVadC(ByRef found_what As String) As Boolean
    Dim sql As String
    Dim rs As New Recordset
    Dim rs2 As New Recordset
    Dim dt As Date
    Dim desc As String
    Dim done As Boolean

    CheckVadC = False
    done = False
    found_what = ""
    
    sql = "select description,max(event_datetime) from chart_item " & WhereBase(SearchSinceAdmission)
    sql = sql & " and description like 'NURS VADC Catheter Type%'"
    sql = sql & " and (result like '%non-tunneled%' or result like '%lumen%') group by description"
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
    Do While Not rs.EOF And Not done
        desc = rs(0)
        desc = "NURS VADC Removal Date" & Mid$(desc, Len(desc) - 1, 2)
        sql = "select max(event_datetime) from chart_item " & WhereBase(SearchSinceAdmission)
        sql = sql & " and description=" & g_dbutil.SQL_String(desc)
        sql = sql & " and event_datetime>" & g_dbutil.SQL_DateTime(rs(1))
        rs2.Open sql, g_cnADO
        If Not IsNull(rs2(0)) Then
            CheckVadC = True
            found_what = "Found " & rs(0) & " with Removal Date of " & rs2(0)
            done = True
        End If
        rs2.Close
        
        rs.MoveNext
    Loop
    End If 'not eof
    
    rs.Close

End Function

Private Function CheckWounds(ByRef found_what As String) As Boolean
    Dim sql As String
    Dim rs As New Recordset
    Dim dt As Date
    Dim desc As String
    Dim done As Boolean

    CheckWounds = False
    done = False
    found_what = ""
    
    sql = "select description from chart_item " & WhereBase()
    sql = sql & " and description like 'NURS Incision %'"
    sql = sql & " and description not like 'NURS Incision _ Type of Dressing'"
    sql = sql & " and description not like 'NURS Incision Drain _ Drainage Odor'"
    rs.Open sql, g_cnADO
    
    If Not rs.EOF Then
    If Not IsNull(rs(0)) Then
        CheckWounds = True
        found_what = rs(0)
        rs.Close
        Exit Function
    End If
    End If
    
    rs.Close
    
    sql = "select description from chart_item " & WhereBase()
    sql = sql & " and description like 'NURS Wound %'"
    sql = sql & " and description not like 'NURS Wound Drain _ Drainage Odor'"
    rs.Open sql, g_cnADO
    
    If Not rs.EOF Then
    If Not IsNull(rs(0)) Then
        CheckWounds = True
        found_what = rs(0)
        rs.Close
        Exit Function
    End If
    End If
    
    rs.Close

    sql = "select description from chart_item " & WhereBase()
    sql = sql & " and description like 'NURS Pressure Ulcer %'"
    sql = sql & " and description not like 'NURS Pressure Ulcer _ Type of Dressing'"
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
    If Not IsNull(rs(0)) Then
        CheckWounds = True
        found_what = rs(0)
        rs.Close
        Exit Function
    End If
    End If
    
    rs.Close
    
End Function

Private Function CountWithin2Hours(desc As String) As Integer
    Dim sql As String
    Dim rs As New Recordset
    Dim rs2 As New Recordset
    Dim evdt As Date
    Dim endevdt As Date
    Dim done As Boolean
    Dim count As Integer

    CountWithin2Hours = 0
    done = False
    
    sql = "select count(distinct(event_datetime)) from chart_item " & WhereBase()
    sql = sql & " and description like '" & desc & "%'"
    rs.Open sql, g_cnADO
    count = rs(0)
    rs.Close
    
    If count < 8 Then Exit Function
    
    sql = "select distinct(event_datetime) from chart_item " & WhereBase()
    sql = sql & " and description like '" & desc & "%'"
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
    Do While Not rs.EOF And Not done
        evdt = rs(0)
        endevdt = DateAdd("h", 2, evdt)
        sql = "select count(distinct(event_datetime)) from chart_item " & WhereBase()
        sql = sql & " and description like '" & desc & "%'"
        sql = sql & " and event_datetime between " & g_dbutil.SQL_DateTime(evdt) & " and " & g_dbutil.SQL_DateTime(endevdt)
        rs2.Open sql, g_cnADO
        count = rs2(0)
        If count >= 8 Then
            done = True
            CountWithin2Hours = count
        End If
        rs2.Close
        
        rs.MoveNext
    Loop
    End If
    rs.Close

End Function
Private Sub SetupBuckets(bucketsize As Integer)
    Dim i As Integer
'b1 = pull_start to +70
'b2=  b1 end
' 700 - 1022  range = 3h 22min = 202mins   202/70=2 -> 3 buckets
' 700:700+70=810, 810:810+70=920,  920:finish
    num_buckets = g_range \ bucketsize
    If (g_range Mod bucketsize) <> 0 Then num_buckets = num_buckets + 1
    For i = 1 To num_buckets
        bucket(i).startdt = DateAdd("n", (i - 1) * bucketsize, m_pat.pull_start)
        bucket(i).count = 0
    Next i
    For i = 1 To num_buckets - 1
        bucket(i).enddt = bucket(i + 1).startdt
    Next i
    bucket(num_buckets).enddt = m_pat.pull_finish

'    For i = 1 To num_buckets
'        dprint i & ": " & bucket(i).startdt
'    Next i

End Sub

Private Sub PutIntoBuckets(desclist As String)
    Dim sql As String, and_filter As String
    Dim rs As New Recordset
    Dim count As Long, i As Integer
    Dim pos As Integer
    Dim s As String
    
    s = ""
    For i = 1 To num_buckets
        s = s & "sum(case when event_datetime>=" & g_dbutil.SQL_DateTime(bucket(i).startdt) & " and "
        If i < num_buckets Then
            s = s & "event_datetime<" & g_dbutil.SQL_DateTime(bucket(i).enddt) & " then 1 else 0 end) as range" & i & ","
        Else
            s = s & "event_datetime=" & g_dbutil.SQL_DateTime(bucket(i).enddt) & " then 1 else 0 end) as range" & i & ","
        End If
    Next i
    s = Mid$(s, 1, Len(s) - 1)  'remove last comma

    sql = "select " & s
    sql = sql & " from (select distinct(event_datetime) from chart_item " & WhereBase() & AndSimpleItemFilter("", "", desclist, "", "") & ") as Q"
    rs.Open sql, g_cnADO
    If Not IsNull(rs(0)) Then
        For i = 1 To num_buckets
            bucket(i).count = bucket(i).count + rs(i - 1)
        Next i
    End If
        
    rs.Close

End Sub
Private Sub PutIntoBucketsWithResults(desclist As String, reslist As String)
    Dim sql As String, qsql As String
    Dim rs As New Recordset
    Dim count As Long, i As Integer
    Dim pos As Integer
    Dim s As String
    Dim darr() As String
    Dim rarr() As String
    Dim nd As Integer, nr As Integer
       
    nd = g_util.SplitTextOnChar(desclist, ",", darr(), 1, 0)
    nr = g_util.SplitTextOnChar(reslist, ",", rarr(), 1, 0)
    If nd <> nr Then Exit Sub
    
    qsql = " and ("
    For i = 1 To nd
        If Len(Trim$(rarr(i))) > 0 Then
            qsql = qsql & "(description like '" & Trim$(darr(i)) & "%' and result like '%" & Trim$(rarr(i)) & "%') or "
        Else
            qsql = qsql & "(description like '" & Trim$(darr(i)) & "%') or "
        End If
    Next i
    qsql = Mid$(qsql, 1, Len(qsql) - 4) 'remove last " or "
    qsql = qsql & ")"
    
    s = ""
    For i = 1 To num_buckets
        s = s & "sum(case when event_datetime>=" & g_dbutil.SQL_DateTime(bucket(i).startdt) & " and "
        If i < num_buckets Then
            s = s & "event_datetime<" & g_dbutil.SQL_DateTime(bucket(i).enddt) & " then 1 else 0 end) as range" & i & ","
        Else
            s = s & "event_datetime=" & g_dbutil.SQL_DateTime(bucket(i).enddt) & " then 1 else 0 end) as range" & i & ","
        End If
    Next i
    s = Mid$(s, 1, Len(s) - 1)  'remove last comma

    sql = "select " & s
    sql = sql & " from (select distinct(event_datetime) from chart_item " & WhereBase() & qsql & ") as Q"

    rs.Open sql, g_cnADO
    If Not IsNull(rs(0)) Then
        For i = 1 To num_buckets
            bucket(i).count = bucket(i).count + rs(i - 1)
        Next i
    End If
        
    rs.Close

End Sub
Private Function CheckBucketFreq(ByRef found_what As String) As Boolean
    Dim i As Integer, j As Integer, adj As Integer
    Dim rangelen As Integer
    Dim rangefilled As Boolean
    
    adj = 0
    rangelen = num_buckets \ 2
    If (num_buckets Mod 2 > 0) Then
        rangelen = rangelen + 1
        adj = 1
    End If
    For i = 1 To rangelen + adj
        rangefilled = True
        For j = 1 To rangelen
            If i + j - 1 <= num_buckets Then
                rangefilled = rangefilled And (bucket(i + j - 1).count > 0)
            End If
        Next j
        If rangefilled Then i = rangelen + 2
    Next i
    If rangefilled Then found_what = found_what & " found in at least half of LOS."
    CheckBucketFreq = rangefilled

End Function
Public Sub PrintBucketFreq(desc As String)
    Dim i As Integer
    Dim s As String
    
    s = desc
    For i = 1 To num_buckets
        s = s & bucket(i).count & ", "
    Next i
    dvprint s

End Sub

Public Function CountBuckets() As Integer
    Dim i As Integer, ct As Integer
    
    ct = 0
    For i = 1 To num_buckets
        If bucket(i).count > 0 Then ct = ct + 1
    Next i
    If ct > 0 Then no_assess_items = False
    CountBuckets = ct

End Function
Public Sub ClearBuckets()
    Dim i As Integer
    
    For i = 1 To num_buckets
        bucket(i).count = 0
    Next i

End Sub

Private Function IsICU() As Boolean
    Dim sql As String
    Dim rs As New Recordset

    IsICU = False
    sql = "select is_icu from unit where unit_id=" & m_pat.unit_id
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        If IsNull(rs(0)) Then
        ElseIf rs(0) = "" Or rs(0) = "N" Then
        ElseIf rs(0) = "Y" Then IsICU = True
        End If
    End If
    rs.Close
End Function
Private Function IsoInPrevClass() As Boolean
    Dim sql As String
    Dim rs As New Recordset

    sql = "select count(*) from indicator_answer where indicator_number=14 and classification_event_id="
    sql = sql & " (select max(classification_event_id) from classification_event where encounter_id=" & m_pat.encounter_id
    sql = sql & "  and classification_datetime="
    sql = sql & "  (select max(classification_datetime) from classification_event where encounter_id=" & m_pat.encounter_id
    sql = sql & "))"
    dvprint sql
    rs.Open sql, g_cnADO
    IsoInPrevClass = (rs(0) > 0)
    rs.Close

End Function
 
