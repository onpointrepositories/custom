Attribute VB_Name = "NIHMentalHealth"
Option Explicit
'
' NIH MH (IP 2.0) [copied from Shands]
'
' This processes one patient using the Inpatient methodology.
'
' All search functions use exact match for code.
'
' result_like looks for LIKE matches in the result, except where EXACT_MATCH_PREFIX is pre-appended to target.
' result_list looks for any one of a list of words exactly as the result.
'
Private Const MAX_INDS = 50
Private Const MAX_PROCS = 48
Private Const MAX_PSYCHINTERV = 40

Private Const EXACT_MATCH_PREFIX = "&!"
Private Const LIKE_PREFIX = "%!"
Private Const CHAR_COMMA = "||"


Private Type indicator_data
    checked     As Boolean
    group       As Integer
    also_mark   As String                   'not used at Arnot (handy with user-defined indicators?)
End Type

Private Type procdata                       'not used at Arnot
    pnum    As Integer
    start   As Date
    finish  As Date
    isvalid As Boolean 'used in combining procs
    pindex As Integer 'used in combining procs = the index of procs whose start/finish times are equal to this one
End Type
Private Type ocdata                       'not used at Arnot
    checked As Boolean
    pnum    As Integer
    start   As String
End Type
Private Type buckettype
    start As Date
    finish As Date
    count As Integer
End Type
Private Type buckettype2
    evdt As Date
    qvalue As Single  'q8 = 8, q4= 4, q2= 2, q1=1, q30m=0.5
' look across 24 hours
'  q8 = 16 hours  16 x 8 + 4 x 1 + .5 x 4 = 134 / 24 = 5.x = q8
'  q1 = 4 hours
'  q30m = 4 hours
' q30=16h, q8=8h:  0.5x16 + 8x8 = 72/24=3 = q4
' q30=20h, q8=4h:  10 + 32 = 42/24=1.x  q2
' sched: q8 = 5.0-8
'        q4 = 3.0 - 4.9
'        q2 = 1.5 - 2.9
'        q1 = .75 - 1.49
'        q30 = 0.5 - .749
End Type
    

Private inds(MAX_INDS)          As indicator_data

Private numprocs                As Integer
Private numoutcomes             As Integer
Private procs(MAX_PROCS)        As procdata
Private oc(MAX_PROCS)           As ocdata

Private WHERE_ENCOUNTER         As String
Private AND_UNIT                As String
Private AND_PULL_RANGE          As String
Private AND_OUTCOMES_RANGE      As String
Private AND_ARRIVAL             As String
Private BUCKET_CALC             As String

Private m_pat                   As PatientInfo
Private m_assist_count          As Integer

Private adl23                   As Boolean
Private adl4                    As Boolean
Private tubefeed                As Boolean
Private morse                   As Integer


Private Enum SearchMode
    SearchDefault
    SearchPullRange             'search within the current pull     -- default
    SearchSinceArrival          'search since arrival to the unit
    SearchSinceAdmission        'search everything since admission to the hospital
    SearchOutcomesRange         'search within the current pull + 24 hours before
    SearchAssessments
End Enum

Private Enum CountMode
    CountAll
    CountFirst                  'stop after one is found
End Enum

Private Enum GetValueMode
    GetTotal
    GetMax
    GetLast
End Enum

Private Const FREQUENCY_BUCKET_SIZE = 20    'min

Private Enum Frequencies
    QNONE
    Q4H
    Q2H
    Q1H
    Q30M
End Enum

Private Type fmapRow
    los_high                As Single       'the LOS being testing
    freq(QNONE To Q30M)     As Integer      'the count required for each Q value
End Type

Private m_freq_map(6)       As fmapRow      '1,2,4,8,12,24 hours
Private psyval(MAX_PSYCHINTERV) As buckettype2
Private numpsy As Integer
Private psyinterv_filter   As String
Private outstr As String
Private ind_list As String
Private patlos As Integer


'This is the main entry point
'
Public Sub ProcessMentalHealth(pat As PatientInfo)
    On Error GoTo errHandler
    
    m_pat = pat
'    frmMain.SetProgress "Processing acct: " & m_pat.acct
    InitGroupsIfNeeded
    SetSQLConstants
    LoadFreqTable
    ResetIndicators
    ResetProcs
    SetLOS
'    ResetOutcomes

    Check_MH123
    Check_MH4
    Check_MH56
    Check_MH78
    Check_MHBeh
    Check_MH1415
    Check_MHAssess
    Check_MH19
    Check_MH20
    Check_MH21
    Check_MH22
    
    'CheckCustom
    HighestIndicatorInEachGroupWins

    CheckProcs
'    CheckOutcomes

    If g_no_output Then Exit Sub
    SetClassString
    OutputDNCandClass
    OutputProcs
'    OutputOutcomes
    Exit Sub
    
errHandler:
    LogError Err.Description & " in " & Err.source, EVENT_CATEGORY_UNEXPECTED
    Exit Sub
    Resume  'debug
End Sub

Private Sub ResetIndicators()
    Dim i As Integer
    
    For i = 1 To MAX_INDS
        inds(i).checked = False
        inds(i).also_mark = ""
    Next i
    
    adl23 = False
    adl4 = False
    tubefeed = False
    morse = 0
    ind_list = ""
    adl_default = False
    no_assess_items = True
End Sub

Private Sub ResetProcs()
    numprocs = 0
End Sub
Private Sub ResetOutcomes()
    numoutcomes = 0
End Sub

Private Sub InitGroupsIfNeeded()
    On Error GoTo errHandler
    
    Static been_here As Boolean
    Dim rs As New Recordset
    Dim sql As String
    Dim inum As Integer
    
    If been_here Then Exit Sub
    
    'get indicator radio groups from the database
    sql = "SELECT INDICATOR_NUMBER, RADIO_GROUP FROM INDICATOR_DEFINITION WHERE METHODOLOGY_ID=" & m_pat.meth_id
    rs.Open sql, g_cnADO
    
    Do While Not rs.EOF
        inum = rs("INDICATOR_NUMBER")
        If (inum < MAX_INDS) Then
            inds(inum).group = g_dbutil.DBToInteger(rs("RADIO_GROUP"))  'could be null
        End If
        rs.MoveNext
    Loop
    
    rs.Close
    Exit Sub
    
errHandler:
    g_util.ThrowError "InitGroupsIfNeeded"
End Sub

Private Sub LoadFreqTableRow(m_freq As fmapRow, los_high As Single, values As String)
    Dim arr() As String, n As Integer, i As Integer
    
    m_freq.los_high = los_high

    n = g_util.SplitTextOnChar(values, ",", arr(), 0, 0)
    For i = 0 To n - 1
        m_freq.freq(i) = CInt(arr(i))
    Next i
End Sub

Private Sub LoadFreqTable()
                                 ' LOS,   None Q4h Q2h Q1h Q30m
    LoadFreqTableRow m_freq_map(1), 1, "    0,  0,  0,  0,  1"
    LoadFreqTableRow m_freq_map(2), 2, "    0,  0,  0,  1,  2"
    LoadFreqTableRow m_freq_map(3), 4, "    0,  1,  2,  3,  4"
    LoadFreqTableRow m_freq_map(4), 8, "    0,  2,  3,  4,  8"
    LoadFreqTableRow m_freq_map(5), 12, "   0,  2,  4,  6, 12"
    LoadFreqTableRow m_freq_map(6), 9999, " 0,  3,  6, 12, 24"         '24+
    
    'To read this table: if you have at least 6 asessmenets in a 12-hour period, you get Q1h
    '(row 5: LOS=12, column Q1h has a count of 6)
End Sub

Private Function FreqForCount(los_hours As Single, count As Integer) As Frequencies
    Dim i As Integer, j As Integer
    
    For i = 1 To UBound(m_freq_map)                         'go from LOS 1 to 24+
        If (los_hours <= m_freq_map(i).los_high) Then
            For j = Q30M To QNONE Step -1                   'search right to left
                If (count >= m_freq_map(i).freq(j)) Then
                    FreqForCount = j
                    Exit Function
                End If
            Next j
        End If
    Next i
    
    FreqForCount = QNONE
End Function

Private Sub SetSQLConstants()
    'These SQL constants are combined in different ways for searching
    WHERE_ENCOUNTER = " where (encounter_id = " & m_pat.encounter_id & ")"
    AND_UNIT = " and (unit_id = " & m_pat.unit_id & ")"
    AND_PULL_RANGE = " and (event_datetime between " & g_dbutil.SQL_DateTime(m_pat.pull_start) & " and " & g_dbutil.SQL_DateTime(m_pat.pull_finish) & ")"
    AND_ARRIVAL = " and (event_datetime between " & g_dbutil.SQL_DateTime(m_pat.unit_arrival) & " and " & g_dbutil.SQL_DateTime(m_pat.pull_finish) & ")"
    AND_OUTCOMES_RANGE = " and (event_datetime between " & g_dbutil.SQL_DateTime(DateAdd("d", -1, m_pat.pull_start)) & " and " & g_dbutil.SQL_DateTime(m_pat.pull_finish) & ")"
    
    'What bucket number does the event belong to?
    BUCKET_CALC = "DateDiff(minute," & g_dbutil.SQL_DateTime(m_pat.pull_start) & ",event_datetime) / " & FREQUENCY_BUCKET_SIZE
End Sub

'Add this to all chart item queries to start the WHERE clause
'This is where the search depth is set
Private Function WhereBase(Optional search_mode As SearchMode = SearchDefault)
    Dim result As String
    
    Select Case search_mode
    Case SearchPullRange, SearchDefault
        result = WHERE_ENCOUNTER & AND_PULL_RANGE    'search within pull range
    Case SearchSinceArrival
        result = WHERE_ENCOUNTER & AND_ARRIVAL        'search since arrival to the unit
    Case SearchSinceAdmission
        result = WHERE_ENCOUNTER                                'search since admission to the hospital (any unit)
    Case SearchOutcomesRange
        result = WHERE_ENCOUNTER & AND_OUTCOMES_RANGE    'search within pull range+24hrs before
    Case SearchAssessments
        result = WHERE_ENCOUNTER      'search within 12 hour range
    End Select
    
    'Since we are looking for one patient in one facility, we don't have to filter by TC_source_ID.
    
    WhereBase = result
End Function

Private Function ValueIsAList(result As String) As Boolean
    ValueIsAList = (InStr(1, result, ",") > 0)                'is there a comma-separated list?
End Function

'Look for any of these fields.  Cat/desc/field = exact match.  Result = like match.
Private Function AndSimpleItemFilter(cat As String, code As String, desc As String, field As String, result_like As String) As String
    Dim result As String
    Dim pos As Integer
    
    If Len(cat) Then result = result & " and category=" & g_dbutil.SQL_String(cat)
    If Len(code) Then
        If Mid$(code, 1, 2) = LIKE_PREFIX Then
            result = result & " and code like " & g_dbutil.SQL_String(Mid$(code, 3, Len(code) - 2))
        ElseIf ValueIsAList(code) Then
            result = result & " and code in " & FormatCodeList(code)
        Else
            result = result & " and code=" & g_dbutil.SQL_String(code)
        End If
    End If
    If Len(desc) Then
        If ValueIsAList(desc) Then
            result = result & " and " & FormatDescList(desc)
        Else
            result = result & " and description like '" & desc & "%'"
        End If
    End If
    If Len(field) Then result = result & " and field_name=" & g_dbutil.SQL_String(field)
    If Len(result_like) Then
        pos = InStr(1, result_like, CHAR_COMMA) 'when looking for a comma in the result
        If pos > 0 Then
            result_like = Mid$(result_like, 1, pos - 1) & "," & Mid$(result_like, pos + 2, Len(result_like) - pos - 1)
        End If

        If InStr(result_like, EXACT_MATCH_PREFIX) = 1 Then 'exact match
            result_like = Mid$(result_like, 3, Len(result_like) - Len(EXACT_MATCH_PREFIX))
            result = result & " and result= " & g_dbutil.SQL_String(result_like)
        Else
            result = result & " and result like '%" & Trim$(result_like) & "%'"
        End If
    End If

    AndSimpleItemFilter = result
End Function
Private Function FormatCodeList(code_list As String) As String
    Dim arr() As String
    Dim i As Integer, n As Integer
    Dim result As String

    If Len(code_list) = 0 Then Exit Function
    
    n = g_util.SplitTextOnChar(code_list, ",", arr(), 1, 0)
    
    result = "("
    
    For i = 1 To n
        result = result & g_dbutil.SQL_String(Trim$(arr(i)))
        If i < n Then result = result & ","
    Next i
    
    result = result & ")"
    
    FormatCodeList = result
End Function
Private Function FormatDescList(desc_list As String) As String
    Dim arr() As String
    Dim i As Integer, n As Integer
    Dim result As String

    If Len(desc_list) = 0 Then Exit Function
    
    n = g_util.SplitTextOnChar(desc_list, ",", arr(), 1, 0)
' want:  description like 'xxxx%' or description like 'yyyy%' or description like 'zzz%'
    result = ""
    
    For i = 1 To n
        result = result & "description like '" & Trim$(arr(i)) & "%' or "
    Next i
    
    result = Mid$(result, 1, Len(result) - 4) 'remove last " or"
    
    result = "(" & result & ")"
    
    FormatDescList = result
End Function

'Look for a result that contains one of the words in the list
'The only drawback with this function is that we don't know what it found (for audits)
'Because of this, it is only used in as few places.
Private Function AndResultContains(result_list As String) As String
    Dim arr() As String
    Dim i As Integer, n As Integer
    Dim result As String
    Dim pos As Integer

    If Len(result_list) = 0 Then Exit Function
    
    n = g_util.SplitTextOnChar(result_list, ",", arr(), 1, 0)
    
    result = "and ((1=0)"   ' or (result=this) or (result=that)
    
    For i = 1 To n
        pos = InStr(1, arr(i), CHAR_COMMA)
        If pos > 0 Then
            arr(i) = Mid$(arr(i), 1, pos - 1) & "," & Mid(arr(i), pos + 2, Len(arr(i)) - pos - 1)
        End If
        result = result & " or (result like '" & "%" & Trim$(arr(i)) & "%')"
    Next i
    
    result = result & ")"
    
    AndResultContains = result
End Function

Private Function DescribeSearchDepth(search_mode As SearchMode) As String
    Dim result As String
    
    Select Case search_mode
    Case SearchPullRange, SearchDefault
        'result = "in pull range"
        result = ""                         'be quiet since this is default
    Case SearchSinceArrival
        result = "since arrival to unit"
    Case SearchSinceAdmission
        result = "since admission"
    Case SearchOutcomesRange
        result = "since 24hrs before pull"
    End Select
    
    DescribeSearchDepth = result
End Function

Private Function Describe(cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = SearchPullRange) As String
    Dim sql As String, and_filter As String
    Dim rs As New Recordset
    Dim result As String
   
    If Not g_debug Then Exit Function           'avoid extra overhead if not making a log

    result = "looking for"
    If Len(cat) Then result = result & " cat='" & cat & "'"
    If Len(code) Then result = result & " code='" & code & "'"
    If Len(desc) Then result = result & " desc='" & desc & "'"
    If Len(field) Then result = result & " field='" & field & "'"
    If Len(result_list) Then result = result & " result contains '" & result_list & "'"

    If ValueIsAList(result_list) Then
        and_filter = AndSimpleItemFilter(cat, code, desc, field, "") & AndResultContains(result_list)
    Else
        and_filter = AndSimpleItemFilter(cat, code, desc, field, result_list)
    End If
    
    sql = "select code, description, result from chart_item" & WhereBase(search_mode) & and_filter
    rs.Open sql, g_cnADO
    If rs.EOF Then
        result = result & "; not found " & DescribeSearchDepth(search_mode)
    Else
        result = result & "; found"
        'Add info for columns that were not specified and a value was found
        If (Len(code) = 0 Or ValueIsAList(code)) And Len(rs("code")) Then result = result & " code='" & rs("code") & "'"
        If (Len(desc) = 0) And Len(rs("description")) Then result = result & " desc='" & rs("description") & "'"
        'Add the complete result found (we searched for a word or words)
        result = result & " result='" & rs("result") & "'"
        'Are there more results?  Just say how many; we aren't going to list them.
        If (rs.RecordCount = 2) Then
            result = result & " (1 more result)"
        ElseIf (rs.RecordCount > 2) Then
            result = result & " (" & rs.RecordCount - 1 & " more results)"
        End If
    End If
    rs.Close
    
    Describe = result
End Function

'Set an indicator for this reason (low level)
Private Sub SetInd(inum As Integer, reason As String)
    If inds(inum).checked And Not g_debug Then Exit Sub       'already set and no log?

    inds(inum).checked = True
    dprint "Set Ind #" & inum & ": " & reason
End Sub

'Clear an indicator for this reason (low level)
Private Sub ClrInd(inum As Integer, reason As String)
    If Not inds(inum).checked And Not g_debug Then Exit Sub   'already clear and no log?
    
    inds(inum).checked = False
    dprint "Clr Ind #" & inum & ": " & reason
End Sub

'Count how many items have the search word in result_like
'This is simple and fast
Private Function CountSimpleResult(cat As String, code As String, desc As String, field As String, result_like As String, Optional search_mode As SearchMode = SearchPullRange, Optional trace As Boolean = True, Optional found_what As String) As Long
    Dim sql As String, and_filter As String
    Dim rs As New Recordset
    Dim count As Long
    Dim pos As Integer

    and_filter = AndSimpleItemFilter(cat, code, desc, field, result_like)
    sql = "select count(*) from chart_item" & WhereBase(search_mode) & and_filter
'    dvprint sql
    'Debug.Print sql
    rs.Open sql, g_cnADO
    count = rs(0)
    rs.Close
    
    'Normally CountSimpleResult will report what it finds; not eveyone wants this so the trace can be turned off
    found_what = Describe(cat, code, desc, field, result_like, search_mode) & IIf(count > 0, "; count=" & count, "")
    If trace Then dvprint found_what
    
    CountSimpleResult = count
End Function
Private Function CountUniqueCodesInList(codelist As String, retlist As String) As Long
    Dim sql As String, and_filter As String
    Dim rs As New Recordset
    Dim count As Long

    retlist = ""
    sql = "select distinct(code) from chart_item" & WhereBase & " and code in (" & codelist & ")"
    rs.Open sql, g_cnADO
    Do While Not rs.EOF
        If Not IsNull(rs(0)) Then
            count = count + 1
            retlist = retlist & rs(0) & " "
        End If
        rs.MoveNext
    Loop
    rs.Close
    
    'Normally CountSimpleResult will report what it finds; not eveyone wants this so the trace can be turned off
    
    CountUniqueCodesInList = count
End Function
Private Function CountCodeHitsInList(codelist As String, retlist As String) As Long
    Dim sql As String, and_filter As String
    Dim rs As New Recordset
    Dim count As Long

    retlist = ""
    sql = "select count(code),code from chart_item" & WhereBase & " and code in (" & codelist & ")"
    sql = sql & " group by code having count(code)>=2"
    rs.Open sql, g_cnADO
    Do While Not rs.EOF
        If Not IsNull(rs(0)) And Not IsNull(rs(1)) Then
            count = count + 1
            retlist = retlist & rs(1) & " = " & rs(0) & " times; "
        End If
        rs.MoveNext
    Loop
    rs.Close
    If Len(retlist) Then retlist = Mid$(retlist, 1, Len(retlist) - 2) & "."
    'Normally CountSimpleResult will report what it finds; not eveyone wants this so the trace can be turned off
    
    CountCodeHitsInList = count
End Function

'Count how many items have a result with one of the words given in the result_list
'Returns a description of what word was found
'This is more powerful than CountSimpleResult but it is slower
Private Function CountResultInList(cat As String, code As String, desc As String, field As String, result_list As String, search_mode As SearchMode, count_mode As CountMode, trace As Boolean, found_what As String) As Integer
    On Error GoTo errHandler
    Dim sql As String, and_filter As String, arr() As String
    Dim rs As New Recordset
    Dim i As Integer, n As Integer, count As Integer
    Dim pos As Long
    
    n = g_util.SplitTextOnChar(result_list, ",", arr(), 1, 0)
    
    'Do not filter by result here; we need all results
    and_filter = AndSimpleItemFilter(cat, code, desc, field, "")
    sql = "select code,result from chart_item" & WhereBase(search_mode) & and_filter
    'Debug.Print sql
    rs.Open sql, g_cnADO
    Do While Not rs.EOF
        'Look for each search word in the result (case insensitive)
        For i = 1 To n
            arr(i) = Trim$(arr(i))          'we split on comma so there usually are leading blanks
            
            pos = InStr(1, arr(i), CHAR_COMMA)
            If pos > 0 Then
                arr(i) = Mid$(arr(i), 1, pos - 1) & "," & Mid(arr(i), pos + 2, Len(arr(i)) - pos - 1)
            End If
            
            If InStr(arr(i), EXACT_MATCH_PREFIX) = 1 Then
                arr(i) = Mid$(arr(i), 3, Len(arr(i)) - Len(EXACT_MATCH_PREFIX))
                pos = (rs("result") = arr(i))
            Else
                pos = InStr(1, rs("result"), arr(i), vbTextCompare)  'bad when looking for "RN" in "Non RN"
            End If
            If pos > 0 Then
                found_what = "found '" & arr(i) & "' in code='" & rs("code") & "' result='" & rs("result") & "'"
                If trace Then dvprint found_what
                count = count + 1
                If count_mode <> CountAll Then Exit Do  'stop counting
                Exit For                                'try next record
            End If
        Next i

        rs.MoveNext
    Loop
    
    If (count > 0) Then
        'We already printed what was found; maybe add how many?
        If trace And (count_mode = CountAll) Then dvprint "found " & count & " total"
    Else
        found_what = Describe(cat, code, desc, field, result_list, search_mode)       'not found
        If trace Then dvprint found_what
    End If
    
    rs.Close
    
    CountResultInList = count
    Exit Function
    
errHandler:
    g_util.ThrowError "CountResultInList"
    Resume   'debug
End Function

Private Function CountResultContains(cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = SearchPullRange, Optional trace As Boolean = True, Optional found_what As String) As Integer
    If ValueIsAList(result_list) Then
        CountResultContains = CountResultInList(cat, code, desc, field, result_list, search_mode, CountAll, trace, found_what)
    Else
        CountResultContains = CountSimpleResult(cat, code, desc, field, result_list, search_mode, trace, found_what)
    End If
End Function

Private Function ResultContains(cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = SearchPullRange, Optional trace As Boolean = True, Optional found_what As String) As Boolean
    'tracing can be turned off and you can ask for what was found instead of having it printed
    If ValueIsAList(result_list) Then
        ResultContains = (CountResultInList(cat, code, desc, field, result_list, search_mode, CountFirst, trace, found_what) > 0)
    Else
        ResultContains = (CountSimpleResult(cat, code, desc, field, result_list, search_mode, trace, found_what) > 0)
    End If
End Function


Private Function CountResultNotInList(cat As String, code As String, desc As String, field As String, result_list As String, search_mode As SearchMode, count_mode As CountMode, trace As Boolean, found_what As String) As Integer
    On Error GoTo errHandler
    Dim sql As String, and_filter As String, arr() As String
    Dim rs As New Recordset
    Dim i As Integer, n As Integer, count As Integer
    Dim found_one As Boolean

    n = g_util.SplitTextOnChar(result_list, ",", arr(), 1, 0)
    
    'Do not filter by result here; we need all results
    and_filter = AndSimpleItemFilter(cat, code, desc, field, "")
    sql = "select code, result from chart_item" & WhereBase(search_mode) & and_filter
    sql = sql & " and result<>'' and result is not null"
    'Debug.Print sql
    rs.Open sql, g_cnADO
    
    Do While Not rs.EOF
        'Look for each search word in the result (case insensitive)
        found_one = False
        
        For i = 1 To n
            arr(i) = Trim$(arr(i))          'we split on comma so there usually are leading blanks
            If InStr(1, rs("result"), arr(i), vbTextCompare) > 0 Then
                found_what = "found '" & arr(i) & "' in code='" & rs("code") & "' result='" & rs("result") & "' -- ignore this"
                If trace Then dvprint found_what
                found_one = True
                Exit For
            End If
        Next i

        If (Not found_one) Then
            'none of the words were found - good!
            found_what = Describe(cat, code, desc, field, "", search_mode) & " -- does not contain '" & result_list & "'"
            If trace Then dvprint found_what
            count = count + 1
            If count_mode <> CountAll Then Exit Do
        End If

        rs.MoveNext
    Loop
    
    If (count > 0) Then
        'We already printed what was found; maybe show the total?
        If trace And (count_mode = CountAll) Then dvprint "found " & count & " total"
    ElseIf (rs.RecordCount > 0) Then
        'We already printed what we ignored
    Else
        'Print what did not exist
        found_what = Describe(cat, code, desc, field, "", search_mode)      'not found
        If trace Then dvprint found_what
    End If
    
    rs.Close
    
    CountResultNotInList = count
    Exit Function
    
errHandler:
    g_util.ThrowError "CountResultNotInList"
    Resume   'debug
End Function

Private Function CountResultDoesNotContain(cat As String, code As String, desc As String, field As String, result_list As String, found_what As String, Optional search_mode As SearchMode = SearchPullRange, Optional trace As Boolean = True) As Integer
    CountResultDoesNotContain = CountResultNotInList(cat, code, desc, field, result_list, search_mode, CountAll, trace, found_what)
End Function

Private Function ResultDoesNotContain(cat As String, code As String, desc As String, field As String, result_list As String, found_what As String, Optional search_mode As SearchMode = SearchPullRange, Optional trace As Boolean = True) As Boolean
    ResultDoesNotContain = (CountResultNotInList(cat, code, desc, field, result_list, search_mode, False, trace, found_what) > 0)
End Function

'Set the indicator if the result contains one of the words in the result_list
Private Sub SetIndIfResultContains(inum As Integer, cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = SearchPullRange)
    Dim found_what As String
    'avoid more queries if the indicator is already set
    If inds(inum).checked Then Exit Sub
    
    'Turn trace off for ResultContains() so the result can be placed on the "SetInd#" line below
    If ResultContains(cat, code, desc, field, result_list, search_mode, False, found_what) Then
        SetInd inum, found_what             'echo here
    Else
        dvprint found_what                  'and here
    End If
End Sub

'Set the indicator if the result does not contain any of the words in result_list
Private Sub SetIndIfResultDoesNotContain(inum As Integer, cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = SearchPullRange)
    Dim found_what As String
    'avoid more queries if the indicator is already set
    If inds(inum).checked Then Exit Sub
    
    'Turn trace off for ResultDoesNotContain() and echo what was set below with SetInd
    If ResultDoesNotContain(cat, code, desc, field, result_list, found_what, search_mode, False) Then
        SetInd inum, found_what             'echo here
    Else
        dvprint found_what                  'and here
    End If
End Sub

'Clear the indicator if the result contains on of the words in the result_list
Private Sub ClrIndIfResultContains(inum As Integer, cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = SearchPullRange)
    Dim found_what As String
    'avoid more queries if the indicator is already clear
    If Not inds(inum).checked Then Exit Sub
    
    'Turn trace off for ResultContains() and echo what was set below with SetInd
    If ResultContains(cat, code, desc, field, result_list, search_mode, False, found_what) Then
        ClrInd inum, found_what             'echo here
    Else
        dvprint found_what                  'and here
    End If
End Sub


'These slightly smaller functions are meant for places where you aren't really looking for a chart result
Private Function Exists(cat As String, Optional code As String, Optional desc As String, Optional field As String, Optional result_list As String, Optional search_mode As SearchMode = SearchPullRange, Optional trace As Boolean = True, Optional found_what As String) As Boolean
    Exists = (CountResultContains(cat, code, desc, field, result_list, search_mode, trace, found_what) > 0)
End Function

Private Sub SetIndIfFound(inum As Integer, cat As String, Optional code As String, Optional desc As String, Optional field As String, Optional result_list As String, Optional search_mode As SearchMode = SearchPullRange)
    SetIndIfResultContains inum, cat, code, desc, field, result_list, search_mode
End Sub

Private Sub ClrIndIfFound(inum As Integer, cat As String, Optional code As String, Optional desc As String, Optional field As String, Optional result_list As String, Optional search_mode As SearchMode = SearchPullRange)
    ClrIndIfResultContains inum, cat, code, desc, field, result_list, search_mode
End Sub


'Get the max/total value from a result (usually in the middle of the text)
Private Function GetIntValue(get_mode As GetValueMode, cat As String, code As String, desc As String, field As String, result_like As String, Optional search_mode As SearchMode = SearchPullRange) As Integer
    Dim sql As String, and_filter As String, arr() As String, msg As String
    Dim rs As New Recordset
    Dim result As Integer, i As Integer, n As Integer, value As Integer
    Dim found_one As Boolean

    and_filter = AndSimpleItemFilter(cat, code, desc, field, result_like)
    sql = "select result from chart_item" & WhereBase(search_mode) & and_filter
    'Debug.Print sql
    rs.Open sql, g_cnADO
    
    'Look for a number in the result
    
    Do While Not rs.EOF
        n = g_util.SplitTextOnChar(rs(0), " ", arr(), 1, 0)
        For i = 1 To n
            'Look for 1st character numeric; IsNumeric("60min") will fail so look at 1st char only
            If IsNumeric(Left$(arr(i), 1)) Then
                value = val(arr(i))                         'Use Val; CInt will error on "60min"
                Select Case get_mode
                Case GetMax
                    result = g_util.Max(value, result)      'max
                Case GetTotal
                    result = result + value                 'total
                Case GetLast
                    result = value                          'last
                End Select
                
                'print what we are searching for (the first time)
                If Not found_one Then
                    dvprint Describe(cat, code, desc, field, result_like, search_mode)
                End If
                found_one = True
                'print each value found
                dvprint "  found numeric value " & result
                'Keep going in case there are more
            End If
        Next i
        rs.MoveNext
    Loop
    
    rs.Close
    
    If Not found_one Then
        'show what was not found
        If g_verbose Then dprint Describe(cat, code, desc, field, result_like, search_mode)
    End If
    
    GetIntValue = result
End Function

Private Function GetMaxValue(cat As String, code As String, desc As String, field As String, result_like As String, Optional search_mode As SearchMode = SearchPullRange) As Integer
    GetMaxValue = GetIntValue(GetMax, cat, code, desc, field, result_like, search_mode)
End Function

Private Function GetTotalValue(cat As String, code As String, desc As String, field As String, result_like As String, Optional search_mode As SearchMode = SearchPullRange) As Integer
    GetTotalValue = GetIntValue(GetTotal, cat, code, desc, field, result_like, search_mode)
End Function

'Get a result; returns True if found with return_result set
Private Function GetResult(cat As String, code As String, desc As String, field As String, return_result As String, Optional search_mode As SearchMode = SearchPullRange) As Boolean
    Dim rs As New Recordset
    Dim sql As String

    sql = "select result from chart_item" & WhereBase(search_mode) & AndSimpleItemFilter(cat, code, desc, field, "")
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        return_result = rs(0) & ""
    Else
        return_result = ""
    End If
    rs.Close

    dvprint Describe(cat, code, desc, field, "", search_mode)
    GetResult = (Len(return_result) > 0)
End Function
Private Function GetResultOfLatest(cat As String, code As String, desc As String, field As String, return_result As String, Optional search_mode As SearchMode = SearchPullRange) As Boolean
    Dim rs As New Recordset
    Dim sql As String
    Dim evdt As Date
    Dim done As Boolean

    sql = "select event_datetime,result from chart_item" & WhereBase(search_mode) & AndSimpleItemFilter(cat, code, desc, field, "")
    sql = sql & " order by event_datetime desc"
    rs.Open sql, g_cnADO
    
    return_result = ""
    done = False
    
    If Not rs.EOF Then evdt = rs(0)
    Do While Not rs.EOF And Not done
        If evdt = rs(0) Then
            return_result = return_result & rs(1) & ","
        Else
            done = True
        End If
        rs.MoveNext
    Loop
    rs.Close

    dvprint Describe(cat, code, desc, field, "", search_mode)
    GetResultOfLatest = (Len(return_result) > 0)

End Function

Private Sub Check_MH123()
    On Error GoTo errHandler
    Dim count As Integer
    Dim act As Boolean
    Dim diet As Boolean
    Dim hyg As Boolean
    Dim found_what As String

    dvprint "---------------"
    dvprint "MH 1. ADL Self"
    dvprint "MH 2. ADL Partial"
    dvprint "MH 3. ADL Extended"
    dvprint "---------------"

    adl_default = False
'3
    act = ResultContains("", "", "NURS Activity Assistance", "", "with 1 staff, with more than 1 staff, with family")
    If Not act Then
        act = ResultContains("", "", "NURS Mobility Gait", "", "Unsteady, Ataxic, Scissor, Spastic, Staggering, Waddling, Drags right foot while walking, Drags left foot while walking, Leans forward while walking, Leans backward while walking")
    End If
    
    If act Then
    
    diet = ResultContains("", "", "NURS Diet Assistance", "", "partial")
    
    If diet Then
    
    hyg = ResultContains("", "", "NURS Bowel Nursing Interventions", "", "Assisted to Bathroom, Assisted to Commode")
    If Not hyg Then
        hyg = ResultContains("", "", "NURS Skin Hygiene Performed", "", "partial,complete")
    End If
    If act And diet And hyg Then SetInd 3, "Activity+Diet+Hygiene Assistance"
    
    If inds(3).checked Then Exit Sub
    
    
    End If 'diet
    End If 'act
'2
SetIndIfResultContains 2, "", "", "NURS Diet Assistance", "", "partial"
SetIndIfResultContains 2, "", "", "NURS FR Mobility", "", "Requires assistance with ambulation"
SetIndIfResultContains 2, "", "", "NURS Assistance Levels", "", "Partial"
SetIndIfResultContains 2, "", "", "NURS Activity Assistance", "", "partial"
SetIndIfResultContains 2, "", "", "NURS Bowel Nursing Interventions", "", "Assisted to Bathroom, Assisted to Commode, bedpan used, urinal used"
If inds(2).checked Then Exit Sub
SetIndIfResultContains 2, "", "", "NURS Skin Hygiene Performed", "", "partial"
SetIndIfResultContains 2, "", "", "NURS Oral Care", "", "Performed by patient family, performed by RN/PCT"
SetIndIfResultContains 2, "", "", "NURS Food/Fluid Feeding Patterns", "", "Needs Assistance"
SetIndIfResultContains 2, "", "", "NURS Toileting", "", "Needs partial assistance when toileting"

If inds(2).checked Then Exit Sub

'1
SetIndIfResultContains 1, "", "", "NURS Assistance Levels", "", "None"
SetIndIfResultContains 1, "", "", "NURS Activity Assistance", "", "Self"
SetIndIfResultContains 1, "", "", "NURS Skin Hygiene Performed", "", "self"
If inds(1).checked Then Exit Sub
SetIndIfResultContains 1, "", "", "NURS Oral Care", "", "Performed by patient"
SetIndIfResultContains 1, "", "", "NURS Food/Fluid Feeding Patterns", "", "Eats independently"
SetIndIfResultContains 1, "", "", "NURS Toileting", "", "Patient performs independent toileting"


    If Not (inds(1).checked Or inds(2).checked Or inds(3).checked) Then
        SetInd 1, "Defaulting to self due to lack of documentation."
        adl_default = True
    End If
    
    Exit Sub
errHandler:
    g_util.ThrowError "Check_MH123"
    Resume  'debug
End Sub
Private Sub Check_MH4()
    On Error GoTo errHandler

    dvprint "---------------"
    dvprint "MH 4. ADL Supervision"
    dvprint "---------------"

SetIndIfResultContains 4, "", "", "NURS Skin Hygiene Performed", "", "By patient with staff supervision"
SetIndIfResultContains 4, "", "", "NURS Psych Interventions", "", "Supervised use of sharp objects"
SetIndIfResultContains 4, "", "", "NURS Psych Mood Interventions", "", "Supervised use of sharp objects"
SetIndIfResultContains 4, "", "", "NURS Psych Perception Interventions", "", "Supervised use of sharp objects"
    
    Exit Sub
errHandler:
    g_util.ThrowError "Check_MH4"
    Resume  'debug
End Sub
Private Sub Check_MH56()
    On Error GoTo errHandler

    dvprint "---------------"
    dvprint "MH 5. Cognitive Support"
    dvprint "MH 6. Cognitive Support q1hr"
    dvprint "---------------"

SetIndIfResultContains 6, "", "", "NURS Psych Cognitive IntervenFreq", "", "Every 1 hour, Every 30 minutes"
If inds(6).checked Then Exit Sub
SetIndIfResultContains 6, "", "", "NURS CIWA Nausea and Vomiting", "", "1,2,3,4,5,6,7"
SetIndIfResultContains 6, "", "", "NURS CIWA Tremor", "", "1,2,3,4,5,6,7"
SetIndIfResultContains 6, "", "", "NURS CIWA Paroxysmal Sweats", "", "1,2,3,4,5,6,7"
If inds(6).checked Then Exit Sub
SetIndIfResultContains 6, "", "", "NURS CIWA Anxiety", "", "1,2,3,4,5,6,7"
SetIndIfResultContains 6, "", "", "NURS CIWA Agitation", "", "1,2,3,4,5,6,7"
SetIndIfResultContains 6, "", "", "NURS CIWA Tactile Disturbances", "", "1,2,3,4,5,6,7"
If inds(6).checked Then Exit Sub
SetIndIfResultContains 6, "", "", "NURS CIWA Auditory Disturbances", "", "1,2,3,4,5,6,7"
SetIndIfResultContains 6, "", "", "NURS CIWA Visual Disturbances", "", "1,2,3,4,5,6,7"
SetIndIfResultContains 6, "", "", "NURS CIWA Headache, Fullness in Head", "", "1,2,3,4,5,6,7"
SetIndIfResultContains 6, "", "", "NURS CIWA Orientation and Clouding of Sensorium", "", "1,2,3,4"
If inds(6).checked Then Exit Sub
    
SetIndIfResultContains 5, "", "", "NURS Neuro Disoriented To", "", ""
SetIndIfResultContains 5, "", "", "NURS Neuro Level of Consciousness", "", "Lethargic, Stuporous, Semi-comatose, Comatose, Drowsy, Unable to assess"
SetIndIfResultContains 5, "", "", "NURS Physical Safety Risks Due To", "", "cognitive ability"
If inds(5).checked Then Exit Sub
SetIndIfResultContains 5, "", "", "NURS Glasgow Best Verbal Response", "", "4,3,2"
SetIndIfResultContains 5, "", "", "NURS Psych Perceptual Disturbances", "", ""
SetIndIfResultContains 5, "", "", "NURS Psych thought Content Disorder", "", ""
If inds(5).checked Then Exit Sub
SetIndIfResultContains 5, "", "", "NURS Neuro Memory - Immediate Recall", "", "Impaired , Absent"
SetIndIfResultContains 5, "", "", "NURS Neuro Memory - Long Term", "", "Impaired, Absent"
SetIndIfResultContains 5, "", "", "NURS Neuro Memory - Short Term", "", "Impaired, Absent"
SetIndIfResultContains 5, "", "", "NURS Psych Perception Interventions", "", "Reality Orient"
    
    Exit Sub
errHandler:
    g_util.ThrowError "Check_MH56"
    Resume  'debug
End Sub
Private Sub Check_MH78()
    On Error GoTo errHandler

    dvprint "---------------"
    dvprint "MH 7. Safety q15min"
    dvprint "MH 8. Safety q5min"
    dvprint "---------------"

If ResultContains("", "", "NURS Psych Observation Status", "", "Every 5 min, eye contact, arm") Then
    SetIndIfResultContains 7, "", "", "NURS Psych Observ Interven", "", "Explained restriction, reinforced safety"
End If
SetIndIfResultContains 8, "", "", "NURS Psych Observation Status Off", "", "1:1"
SetIndIfResultContains 8, "", "", "NURS Suicide Current Self Harm", "", "Self injury, Self harm thoughts, Active suicidal ideation, Wishes of death, Suicide attempt"

If inds(8).checked Then Exit Sub

If ResultContains("", "", "NURS Physical Safety Observation Status", "", "Every 15 minutes") Then
    SetIndIfResultContains 7, "", "", "NURS Physical Safety Observational", "", "Patient maintained under close observation"
End If
If ResultContains("", "", "NURS Psych Observation Status", "", "Every 15 minutes") Then
    SetIndIfResultContains 7, "", "", "NURS Psych Observ Interven", "", "Explained restriction, reinforced safety"
End If

    
    Exit Sub
errHandler:
    g_util.ThrowError "Check_MH78"
    Resume  'debug
End Sub
Private Sub Check_MHBeh()
    On Error GoTo errHandler

    dvprint "---------------"
    dvprint "MH  9. Behavior/Emotional"
    dvprint "MH 10. Behavior/Emotional q4"
    dvprint "MH 11. Behavior/Emotional q2"
    dvprint "MH 12. Behavior/Emotional q1"
    dvprint "MH 13. Behavior/Emotional q30min"
    dvprint "---------------"

'
'If ResultContains("", "", "NURS Psych Interventions", "", "") Then
'    If ResultContains("", "", "NURS Psych Gen Interven Freq", "", "Every 8 hours") Then
'        SetInd 9, "NURS Psych Gen Interven Freq + Every 8 hours"
'    ElseIf ResultContains("", "", "NURS Psych Gen Interven Freq", "", "Every 4 hours") Then
'        SetInd 10, "NURS Psych Gen Interven Freq + Every 4 hours"
'    ElseIf ResultContains("", "", "NURS Psych Gen Interven Freq", "", "Every 2 hours") Then
'        SetInd 11, "NURS Psych Gen Interven Freq + Every 2 hours"
'    ElseIf ResultContains("", "", "NURS Psych Gen Interven Freq", "", "Every 1 hour") Then
'        SetInd 12, "NURS Psych Gen Interven Freq + Every 1 hour"
'    ElseIf ResultContains("", "", "NURS Psych Gen Interven Freq", "", "Every 30 minutes") Then
'        SetInd 13, "NURS Psych Gen Interven Freq + Every 30 min"
'    End If
'End If
'If inds(13).checked Then Exit Sub
'
'If ResultContains("", "", "NURS Psych Mood Interventions", "", "") Then
'    If ResultContains("", "", "Nurs Psych Mood Interven Freq", "", "Every 8 hours") Then
'        SetInd 9, "NURS Psych Mood Interven Freq + Every 8 hours"
'    ElseIf ResultContains("", "", "Nurs Psych Mood Interven Freq", "", "Every 4 hours") Then
'        SetInd 10, "NURS Psych Mood Interven Freq + Every 4 hours"
'    ElseIf ResultContains("", "", "Nurs Psych Mood Interven Freq", "", "Every 2 hours") Then
'        SetInd 11, "NURS Psych Mood Interven Freq + Every 2 hours"
'    ElseIf ResultContains("", "", "Nurs Psych Mood Interven Freq", "", "Every 1 hour") Then
'        SetInd 12, "NURS Psych Mood Interven Freq + Every 1 hour"
'    ElseIf ResultContains("", "", "Nurs Psych Mood Interven Freq", "", "Every 30 minutes") Then
'        SetInd 13, "NURS Psych Mood Interven Freq + Every 30 min"
'    End If
'End If
'
'If inds(13).checked Then Exit Sub
'If ResultContains("", "", "NURS Psych Thought Interventions", "", "") Then
'    If ResultContains("", "", "Nurs Psych Thought Interven Freq", "", "Every 8 hours") Then
'        SetInd 9, "NURS Psych Thought Interven Freq + Every 8 hours"
'    ElseIf ResultContains("", "", "Nurs Psych Thought Interven Freq", "", "Every 4 hours") Then
'        SetInd 10, "NURS Psych Thought Interven Freq + Every 4 hours"
'    ElseIf ResultContains("", "", "Nurs Psych Thought Interven Freq", "", "Every 2 hours") Then
'        SetInd 11, "NURS Psych Thought Interven Freq + Every 2 hours"
'    ElseIf ResultContains("", "", "Nurs Psych Thought Interven Freq", "", "Every 1 hour") Then
'        SetInd 12, "NURS Psych Thought Interven Freq + Every 1 hour"
'    ElseIf ResultContains("", "", "Nurs Psych Thought Interven Freq", "", "Every 30 minutes") Then
'        SetInd 13, "NURS Psych Thought Interven Freq + Every 30 minutes"
'    End If
'End If
'If inds(13).checked Then Exit Sub
'
'If ResultContains("", "", "NURS Psych Cognitive Interventions", "", "") Then
'    If ResultContains("", "", "Nurs Psych Cognitive IntervenFreq", "", "Every 8 hours") Then
'        SetInd 9, "NURS Psych Cognitive Interven Freq + Every 8 hours"
'    ElseIf ResultContains("", "", "Nurs Psych Cognitive IntervenFreq", "", "Every 4 hours") Then
'        SetInd 10, "NURS Psych Cognitive Interven Freq + Every 4 hours"
'    ElseIf ResultContains("", "", "Nurs Psych Cognitive IntervenFreq", "", "Every 2 hours") Then
'        SetInd 11, "NURS Psych Cognitive Interven Freq + Every 2 hours"
'    ElseIf ResultContains("", "", "Nurs Psych Cognitive IntervenFreq", "", "Every 1 hour") Then
'        SetInd 12, "NURS Psych Cognitive Interven Freq + Every 1 hour"
'    ElseIf ResultContains("", "", "Nurs Psych Cognitive IntervenFreq", "", "Every 30 minutes") Then
'        SetInd 13, "NURS Psych Cognitive Interven Freq + Every 30 minutes"
'    End If
'End If
'If inds(13).checked Then Exit Sub
'
'If ResultContains("", "", "NURS Psych Therapeutic Interventions", "", "") Then
'    If ResultContains("", "", "Nurs Psych Therapeutic IntervenFreq", "", "Every 8 hours") Then
'        SetInd 9, "NURS Psych Therapeutic Interven Freq + Every 8 hours"
'    ElseIf ResultContains("", "", "Nurs Psych Therapeutic IntervenFreq", "", "Every 4 hours") Then
'        SetInd 10, "NURS Psych Therapeutic Interven Freq + Every 4 hours"
'    ElseIf ResultContains("", "", "Nurs Psych Therapeutic IntervenFreq", "", "Every 2 hours") Then
'        SetInd 11, "NURS Psych Therapeutic Interven Freq + Every 2 hours"
'    ElseIf ResultContains("", "", "Nurs Psych Therapeutic IntervenFreq", "", "Every 1 hour") Then
'        SetInd 12, "NURS Psych Therapeutic Interven Freq + Every 1 hour"
'    ElseIf ResultContains("", "", "Nurs Psych Therapeutic IntervenFreq", "", "Every 30 minutes") Then
'        SetInd 13, "NURS Psych Therapeutic Interven Freq + Every 30 minutes"
'    End If
'End If
'
    numpsy = 0
    psyinterv_filter = ""
If ResultContains("", "", "NURS Psych Interventions", "", "") Then
    AddIntoSearch "NURS Psych Gen Interven Freq"
End If
If ResultContains("", "", "NURS Psych Mood Interventions", "", "") Then
    AddIntoSearch "Nurs Psych Mood Interven Freq"
End If
If ResultContains("", "", "NURS Psych Thought Interventions", "", "") Then
    AddIntoSearch "Nurs Psych Thought Interven Freq"
End If
If ResultContains("", "", "NURS Psych Cognitive Interventions", "", "") Then
    AddIntoSearch "Nurs Psych Cognitive IntervenFreq"
End If
If ResultContains("", "", "NURS Psych Therapeutic Interventions", "", "") Then
    AddIntoSearch "Nurs Psych Therapeutic IntervenFreq"
End If
    
    If psyinterv_filter = "" Then Exit Sub
    psyinterv_filter = " and (" & psyinterv_filter & ") and (result like '%every [8421] h%' or result like '%every 30 m%')"
    CheckPsyIntervFreq
    
    Exit Sub
errHandler:
    g_util.ThrowError "Check_MHBeh"
    Resume  'debug
End Sub
Private Sub AddIntoSearch(interv As String)
       
    If psyinterv_filter = "" Then
        psyinterv_filter = " description = '" & interv & "'"
    Else
        psyinterv_filter = psyinterv_filter & " or description = '" & interv & "'"
    End If
End Sub
Private Sub CheckPsyIntervFreq()
    Dim sql As String
    Dim rs As New Recordset
    Dim qtot As Single
    Dim i As Integer
    Dim q As String
    'm_pat.pull_start, pull_finish
'Private psyval(MAX_PSYCHINTERV) As buckettype2
'Private numpsy As Integer
    
    sql = "select event_datetime,result from chart_item" & WhereBase() & psyinterv_filter
    sql = sql & " order by event_datetime"
    'Debug.Print sql
'    dprint sql
    rs.Open sql, g_cnADO
    Do While Not rs.EOF
        numpsy = numpsy + 1
        psyval(numpsy).evdt = rs(0)
        q = rs(1)
        If InStr(1, q, "8 h", vbTextCompare) > 0 Then
            psyval(numpsy).qvalue = 8
        ElseIf InStr(1, q, "4 h", vbTextCompare) > 0 Then
            psyval(numpsy).qvalue = 4
        ElseIf InStr(1, q, "2 h", vbTextCompare) > 0 Then
            psyval(numpsy).qvalue = 2
        ElseIf InStr(1, q, "1 h", vbTextCompare) > 0 Then
            psyval(numpsy).qvalue = 1
        ElseIf InStr(1, q, "30 m", vbTextCompare) > 0 Then
            psyval(numpsy).qvalue = 0.5
        End If
        rs.MoveNext
    Loop
    rs.Close
    
    If numpsy = 1 Then
        qtot = psyval(1).qvalue * patlos
    Else
        qtot = psyval(1).qvalue * LOSBeforeEvent(psyval(1).evdt)
        For i = 1 To numpsy 'weighted qvalue x mins, where first qvalue starts at beginning.
            dprint i & ": " & psyval(i).evdt & " v=" & psyval(i).qvalue
            If i < numpsy Then
                qtot = qtot + psyval(i).qvalue * DateDiff("n", psyval(i).evdt, psyval(i + 1).evdt)
            Else  ' If i = numpsy Then
                qtot = qtot + psyval(i).qvalue * LOSAfterEvent(psyval(1).evdt)
            End If
        Next i
    End If
    MapPsy qtot

End Sub
Private Function LOSBeforeEvent(evdt As Date) As Integer
    Dim los As Integer, i As Integer
    los = 0
    For i = 1 To m_pat.num_loc
        If m_pat.loc(i).outdt <= evdt Then
            los = los + DateDiff("n", m_pat.loc(i).indt, m_pat.loc(i).outdt)
        ElseIf m_pat.loc(i).indt < evdt Then
            los = los + DateDiff("n", m_pat.loc(i).indt, evdt)
        End If
    Next i
    LOSBeforeEvent = los
End Function
Private Function LOSAfterEvent(evdt As Date) As Integer
    Dim los As Integer, i As Integer
    los = 0
    For i = 1 To m_pat.num_loc
        If m_pat.loc(i).indt >= evdt Then
            los = los + DateDiff("n", m_pat.loc(i).indt, m_pat.loc(i).outdt)
        ElseIf m_pat.loc(i).outdt > evdt Then
            los = los + DateDiff("n", evdt, m_pat.loc(i).outdt)
        End If
    Next i
    LOSAfterEvent = los
End Function

Private Sub MapPsy(qtotal As Single)
    Dim qlos As Single, qtot As Single, i As Integer
    Dim s As String
' sched: q8 = 5.0-8
'        q4 = 3.0 - 4.9
'        q2 = 1.5 - 2.9
'        q1 = .75 - 1.49
'        q30 = 0.5 - .749
    qlos = patlos  'DateDiff("n", m_pat.pull_start, m_pat.pull_finish)
    qtot = qtotal / qlos
    For i = 1 To numpsy
        dprint psyval(i).evdt & ": q " & psyval(i).qvalue & " hr."
    Next i
    s = "Weighted total=" & qtotal & " divided by los=" & qlos & " =" & qtot
    If qtot < 0.75 Then
        SetInd 13, s & ": qualifies for Q30m [0 - 0.75)"
    ElseIf qtot >= 0.75 And qtot < 1.5 Then
        SetInd 12, s & ": qualifies for Q1h [0.75 - 1.5)"
    ElseIf qtot >= 1.5 And qtot < 3 Then
        SetInd 11, s & ": qualifies for Q2h [1.5 - 3.0)"
    ElseIf qtot >= 3 And qtot < 5 Then
        SetInd 10, s & ": qualifies for Q4h [3.0 - 5.0)"
    ElseIf qtot >= 5 Then
        SetInd 9, s & ": qualifies for Q8h [5.0+)"
    End If
    

End Sub
Private Sub Check_MH1415()
    On Error GoTo errHandler
    Dim ct As Integer
    Dim desclist As String
    Dim reslist As String

    dvprint "---------------"
    dvprint "MH 14. Medication Mgt q4"
    dvprint "MH 15. Medication Mgt q2"
    dvprint "---------------"
    
    desclist = "NURS Symptom Assessment Reason,Nurs Symptom Intervention,NURS Pain Reason For Assessment"
    reslist = "Reasessment after medication intervention,medication given as ordered,Reassessment following intervention"
    ct = ReturnAssessCountWithResults(desclist, reslist)
    
    If ct >= 6 Then
        SetInd 15, "Medication Mgt count = " & ct
    ElseIf ct >= 3 Then
        SetInd 14, "Medication Mgt count = " & ct
    End If
    
    Exit Sub
errHandler:
    g_util.ThrowError "Check_MH1415"
    Resume  'debug
End Sub


Private Sub SetADLCompleteWhenAge(agecond As String)  ' agecond example: "<=3"
    Dim sql As String
    Dim rs As New Recordset

'
'select case when round(age_at_admission,0,1) <=55 then 1 else 0 end from encounter where encounter_id=6990

    sql = "select case when round(age_at_admission,0,1) " & agecond & " then 1 else 0 end from encounter " & WHERE_ENCOUNTER
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        If rs(0) = 1 Then
            SetInd 4, "Age <=3 years"
        End If
    End If
    rs.Close

End Sub



Private Sub CheckAssessment(count As Integer, desc As String)
    Dim los As Single
    Dim p As Single
'    If (inds(18).checked) Then Exit Sub             'skip if highest already checked
'    If (count = 0) Then Exit Sub                    'skip if none
'
'    If count >= g_util.Max(4, CInt(12 * (g_range / 1440))) Then
'        SetInd 18, desc & " with count of " & count
'    ElseIf count >= g_util.Max(2, CInt(6 * (g_range / 1440))) Then
'        SetInd 17, desc & " with count of " & count
'    ElseIf count >= g_util.Max(1, CInt(3 * (g_range / 1440))) Then
'        SetInd 16, desc & " with count of " & count
'    End If
    
    If (inds(18).checked) Then Exit Sub             'skip if highest already checked
    If (count = 0) Then Exit Sub                    'skip if none
    
    los = patlos   'DateDiff("n", m_pat.pull_start, m_pat.pull_finish)
    desc = desc & "; LOS=" & los
    p = 1# * los / 1440  ' pro-rate factor 0 < p <= 1
    If p < 1# Then desc = desc & "; LOS factor=" & p

    If count >= g_util.Max(8, CInt(24 * p)) Then
        SetInd 18, desc & " with count of " & count
    ElseIf count >= g_util.Max(4, CInt(12 * p)) Then
        SetInd 17, desc & " with count of " & count
    ElseIf count >= g_util.Max(2, CInt(6 * p)) Then
        SetInd 16, desc & " with count of " & count
    End If

End Sub
Private Sub Check_MHAssess()
    Dim found_what As String
    Dim qmins As Integer
    Dim ct As Integer
    Dim ct1 As Integer
    Dim ct2 As Integer
    Dim ct3 As Integer
    Dim ct4 As Integer
    Dim ct5 As Integer
    Dim desclist As String
    Dim reslist As String
    Dim return_result As String
    
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "MH 16. Assessment q4h"
    dvprint "MH 17. Assessment q2h"
    dvprint "MH 18. Assessment q1h"
    dvprint "---------------"
    
SetupBuckets (30)
'Pain
    desclist = "NURS Pain"  ' Presence of Pain,NURS Pain Cries Total,NURS Pain FLACC Total,Nurs Pain Comfort Total"
PutIntoBuckets (desclist)
PrintBucketFreq "Pain buckets="
ct = CountBuckets
ClearBuckets
CheckAssessment ct, "Pain=" & ct
    

'symptom mgt
'NURS Symptom Assessment Reason
    desclist = "NURS Symptom Assessment Reason"
    PutIntoBuckets (desclist)
PrintBucketFreq "Symptom Assessment buckets="
ct = CountBuckets
ClearBuckets
    CheckAssessment ct, "Symptom Assessment=" & ct


'temp
    desclist = "NURS Temperature,NURSI Temperature,NURSI TX Temp Reg Device,NURS Beck,NURS Braden,NURS Skin"
    reslist = " , ,on, , , "
    PutIntoBucketsWithResults desclist, reslist
PrintBucketFreq "Temperature/Skin buckets="
ct = CountBuckets
ClearBuckets
    CheckAssessment ct, "Temperature/Skin=" & ct

' heart rate and rhythm
    desclist = "NURS Heart Rate/Pulse,NURSI Heart Rate/Pulse,NURSI Heart Rhythm,NURSI Pacemaker Type,NURS Circulation Heart Rhythm"
    reslist = " , , , trans, "
    PutIntoBucketsWithResults desclist, reslist
PrintBucketFreq "Heart Rate and Rhythm buckets="
ct = CountBuckets
ClearBuckets
    CheckAssessment ct, "Heart Rate=" & ct

'Neuro
    desclist = "NURS Neuro Level of Consciousness,NURS Neuro Oriented To"
    PutIntoBuckets (desclist)
PrintBucketFreq "Neuro buckets="
ct = CountBuckets
ClearBuckets
    CheckAssessment ct, "Neuro=" & ct

'Fluid Balance I&O
    desclist = "io_intake,io_output"
    PutIntoBuckets (desclist)
PrintBucketFreq "I&O buckets="
ct = CountBuckets
ClearBuckets
    CheckAssessment ct, "Fluid Balance I&O=" & ct

'Glucose
    desclist = "NURS BGM Glucose Reading,NURS BGM Extreme Glucose Reading"
    PutIntoBuckets (desclist)
PrintBucketFreq "Glucose buckets="
ct = CountBuckets
ClearBuckets
    CheckAssessment ct, "Glucose=" & ct
    
'Cardiac
    desclist = "NURS Circulation Capillary Refill,NURS Circulation Pulse Assessment,NURS Circulation Heart Rhythm"
    PutIntoBuckets (desclist)
PrintBucketFreq "Cardiac buckets="
ct = CountBuckets
ClearBuckets
    CheckAssessment ct, "Cardiac=" & ct
    
'BP
    desclist = "NURS Blood Pressure"
    PutIntoBuckets (desclist)
PrintBucketFreq "BP buckets="
ct = CountBuckets
ClearBuckets
    CheckAssessment ct, "Blood Pressure=" & ct

'Pulmonary
    desclist = "NURS Respiratory Rate,NURS Respiratory Oxygen Saturation,NURS Sleep Respiration"
    PutIntoBuckets (desclist)
PrintBucketFreq "Pulmonary buckets="
ct = CountBuckets
ClearBuckets
    CheckAssessment ct, "Pulmonary=" & ct

'Blood alcohol
    desclist = "NURS Breath  Alcohol Level"
    PutIntoBuckets (desclist)
PrintBucketFreq "Blood Alcohol buckets="
ct = CountBuckets
ClearBuckets
    CheckAssessment ct, "Blood alcohol=" & ct

'================
    desclist = "NURS Pain"  ' Presence of Pain,NURS Pain Cries Total,NURS Pain FLACC Total,Nurs Pain Comfort Total"
PutIntoBuckets (desclist)
    desclist = "NURS Symptom Assessment Reason"
    PutIntoBuckets (desclist)
    desclist = "NURS Temperature,NURSI Temperature,NURSI TX Temp Reg Device,NURS Beck,NURS Braden,NURS Skin"
    reslist = " , ,on, , , "
    PutIntoBucketsWithResults desclist, reslist
    desclist = "NURS Heart Rate/Pulse,NURSI Heart Rate/Pulse,NURSI Heart Rhythm,NURSI Pacemaker Type,NURS Circulation Heart Rhythm"
    reslist = " , , , trans, "
    PutIntoBucketsWithResults desclist, reslist
    desclist = "NURS Neuro Level of Consciousness,NURS Neuro Oriented To"
    PutIntoBuckets (desclist)
    desclist = "io_intake,io_output"
    PutIntoBuckets (desclist)
    desclist = "NURS BGM Glucose Reading,NURS BGM Extreme Glucose Reading"
    PutIntoBuckets (desclist)
    desclist = "NURS Circulation Capillary Refill,NURS Circulation Pulse Assessment,NURS Circulation Heart Rhythm"
    PutIntoBuckets (desclist)
    desclist = "NURS Blood Pressure"
    PutIntoBuckets (desclist)
    desclist = "NURS Respiratory Rate,NURS Respiratory Oxygen Saturation"
    PutIntoBuckets (desclist)
    desclist = "NURS Breath  Alcohol Level"
    PutIntoBuckets (desclist)
PrintBucketFreq "MH Combined buckets="
ct = CountBuckets
ClearBuckets
    CheckAssessment ct, "MH Combined=" & ct
'================
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_MHAssess"
    Resume  'debug
End Sub
Private Sub Check_MH19()
    Dim rs As New Recordset
    Dim sql As String
    On Error GoTo errHandler

    dvprint "---------------"
    dvprint "MH 19. Fluid Mgt"
    dvprint "---------------"

'SetIndIfResultContains 19, "", "", "NURS Weight", "", ""
SetIndIfResultContains 19, "", "", "io_intake", "", ""
If inds(19).checked Then Exit Sub

    sql = "select count(*) from chart_item " & WhereBase(SearchPullRange)
    sql = sql & " and description like '%io_output%' and description not like '%stool%'"
    rs.Open sql, g_cnADO
    If rs(0) > 0 Then SetInd 19, "IO not stool"
    rs.Close
    
    Exit Sub
errHandler:
    g_util.ThrowError "Check_MH19"
    Resume  'debug
End Sub
Private Sub Check_MH20()
    Dim found_what As String
    On Error GoTo errHandler

    dvprint "---------------"
    dvprint "MH 20. Wound/Injury Mgt"
    dvprint "---------------"
    
    If CheckWounds(found_what) Then SetInd 20, found_what
    
If inds(20).checked Then Exit Sub
    

SetIndIfResultContains 20, "", "", "NURS Incision _ Type of Dressing", "", "Foam, Gauze, Hydrogel, Hydrocolloid, Non-stick gauze, Transparent film, Wet to dry, Wet to moist, Silicone, Alginate"
SetIndIfResultContains 20, "", "", "NURS Incision Drain _ Drainage Odor", "", "Malodorous"
SetIndIfResultContains 20, "", "", "NURS Wound Drain _ Drainage Odor", "", "Malodorous"

If inds(20).checked Then Exit Sub
SetIndIfResultContains 20, "", "", "NURS Pressure Ulcer _ Type of Dressing", "", "Foam, Gauze, Hydrogel, Hydrocolloid, Non-stick gauze, Transparent film, Wet to dry, Wet to moist, Silicone, Alginate"

SetIndIfResultContains 20, "", "", "NURS VADCT Site Care", "", "Yes"
SetIndIfResultContains 20, "", "", "NURS VADCNT Site Care", "", "Yes"
If inds(20).checked Then Exit Sub
SetIndIfResultContains 20, "", "", "NURS VADCP Site Care", "", "Yes"
SetIndIfResultContains 20, "", "", "NURS VADCD Site Care", "", "Yes"
SetIndIfResultContains 20, "", "", "NURS VADC Site Care", "", "Yes"

    Exit Sub
errHandler:
    g_util.ThrowError "Check_MH20"
    Resume  'debug
End Sub
Private Function CheckWounds(ByRef found_what As String) As Boolean
    Dim sql As String
    Dim rs As New Recordset
    Dim dt As Date
    Dim desc As String
    Dim done As Boolean

    CheckWounds = False
    done = False
    found_what = ""
    
    sql = "select description from chart_item " & WhereBase()
    sql = sql & " and description like 'NURS Incision %'"
    sql = sql & " and description not like 'NURS Incision _ Type of Dressing'"
    sql = sql & " and description not like 'NURS Incision Drain _ Drainage Odor'"
    rs.Open sql, g_cnADO
    
    If Not rs.EOF Then
        CheckWounds = True
        found_what = rs(0)
        rs.Close
        Exit Function
    End If
    
    rs.Close
    
    sql = "select description from chart_item " & WhereBase()
    sql = sql & " and description like 'NURS Wound %'"
    sql = sql & " and description not like 'NURS Wound Drain _ Drainage Odor'"
    rs.Open sql, g_cnADO
    
    If Not rs.EOF Then
        CheckWounds = True
        found_what = rs(0)
        rs.Close
        Exit Function
    End If
    
    rs.Close

    sql = "select description from chart_item " & WhereBase()
    sql = sql & " and description like 'NURS Pressure Ulcer %'"
    sql = sql & " and description not like 'NURS Pressure Ulcer _ Type of Dressing'"
    rs.Open sql, g_cnADO
    
    If Not rs.EOF Then
        CheckWounds = True
        found_what = rs(0)
        rs.Close
        Exit Function
    End If
    
    rs.Close
    
End Function
Private Sub Check_MH21()
    On Error GoTo errHandler

    dvprint "---------------"
    dvprint "MH 21. Patient/Family Educ >= 1Hr by RN"
    dvprint "---------------"

SetIndIfResultContains 21, "", "", "NIH Patient Ed Instruction Duration", "", "Greater than 1 Hour"

    
    Exit Sub
errHandler:
    g_util.ThrowError "Check_MH21"
    Resume  'debug
End Sub
Private Sub Check_MH22()
    On Error GoTo errHandler

    dvprint "---------------"
    dvprint "MH 22. Coordination of Care >= 1Hr by RN"
    dvprint "---------------"

'SetIndIfResultContains 22, "", "", "NURS_NCN Contact Call Duration", "", "Greater than one Hour"
SetIndIfResultContains 22, "", "", "NURS Telephone Call Duration", "", "Greater than 1 Hour"
SetIndIfResultContains 22, "", "", "NURS Care Conference Duration", "", "Greater than 1 Hour"
    
    Exit Sub
errHandler:
    g_util.ThrowError "Check_MH22"
    Resume  'debug
End Sub


Private Sub ProcessProc(pnum As Integer, res As String)
    Dim sql As String
    Dim rs As New Recordset
    Dim rs2 As New Recordset
    Dim f As String
    Dim done As Boolean

    sql = "select distinct(event_datetime) from chart_item " & WhereBase(SearchPullRange)
    sql = sql & " and description='NURS Psych Events'"
    sql = sql & " and result like '%" & res & "%'"
    rs.Open sql, g_cnADO
    Do While Not rs.EOF
        done = False
        sql = "select result from chart_item " & WhereBase(SearchPullRange)
        sql = sql & " and description='NURS Psych Hour ActivitiesFreq'"
        sql = sql & " and event_datetime=" & g_dbutil.SQL_DateTime(rs(0))
        rs2.Open sql, g_cnADO
        Do While Not rs2.EOF And Not done
            f = rs2(0)
    dvprint "NURS Psych Hour Activities Freq=" & f
            numprocs = numprocs + 1
            procs(numprocs).pnum = pnum
            procs(numprocs).start = rs(0)
            procs(numprocs).finish = DateAdd("h", CInt(Mid$(f, 1, 1)), rs(0))
            done = True
        Loop
        rs2.Close
        rs.MoveNext
    Loop
    rs.Close

End Sub
'
Private Sub CheckProcs()

    On Error GoTo errHandler
    dvprint "---------------"
    dvprint "MP1. 1-1 safety observation by RN"
    dvprint "---------------"

    ProcessProc 1, "1:1 safety observation by RN"

    dvprint "---------------"
    dvprint "MP2. 1-1 safety observation by non-RN"
    dvprint "---------------"

    ProcessProc 2, "1:1 Safety observation by non-RN"

    dvprint "---------------"
    dvprint "MP3. Off unit accompanied by RN"
    dvprint "---------------"

    ProcessProc 3, "Off unit accompanied by RN"

    dvprint "---------------"
    dvprint "MP4. Off unit accompanied by non-RN"
    dvprint "---------------"

    ProcessProc 4, "Off unit accompanied by non-RN"

    dvprint "---------------"
    dvprint "MP5. 1:1 Continuous Intervention by RN"
    dvprint "---------------"

    ProcessProc 5, "1:1 Continuous Intervention by RN"

    dvprint "---------------"
    dvprint "MP6. 1:1 Continuous Intervention by non-RN"
    dvprint "---------------"

    ProcessProc 6, "1:1 Continuous Intervention by non-RN"

    dvprint "---------------"
    dvprint "MP7. 2:1 Continuous Intervention by RN"
    dvprint "---------------"

    ProcessProc 7, "2:1 Continuous Intervention by RN"

    Exit Sub

errHandler:
    g_util.ThrowError "CheckProcs"
    Resume  'debug
End Sub
'Private Sub CheckOutcomes()
'    Dim return_result As String
'
'    'How to handle multiple outcomes per pt, each with different times?
'    dvprint "---------------"
'    dvprint "Outcomes: FALL"
'    dvprint "---------------"
'    If GetResult("", "FALL", "", "", return_result, SearchOutcomesRange) Then
'        numoutcomes = numoutcomes + 1
'        With oc(numoutcomes)
'            .checked = True
'            .pnum = 1
'            .start = return_result
'        End With
'    End If
'
'    dvprint "---------------"
'    dvprint "Outcomes: MED"
'    dvprint "---------------"
'    If GetResult("", "MED", "", "", return_result, SearchOutcomesRange) Then
'        numoutcomes = numoutcomes + 1
'        With oc(numoutcomes)
'            .checked = True
'            .pnum = 2
'            .start = return_result
'        End With
'    End If
'
'    dvprint "---------------"
'    dvprint "Outcomes: PROC"
'    dvprint "---------------"
'    If GetResult("", "PROC", "", "", return_result, SearchOutcomesRange) Then
'        numoutcomes = numoutcomes + 1
'        With oc(numoutcomes)
'            .checked = True
'            .pnum = 21
'            .start = return_result
'        End With
'    End If
'
'    Exit Sub
'
'errHandler:
'    g_util.ThrowError "CheckOutcomes"
'    Resume  'debug
'End Sub


'Private Sub AtLeastOneADL()
'    If Not (inds(1).checked Or inds(2).checked Or inds(3).checked) Then
'        'Note: Heather wants "#2 ADL - Assist" to be the default.  (90% of patients)
'        SetInd 2, "at least one ADL"
'    End If
'End Sub

Private Sub HighestIndicatorInEachGroupWins()
    Dim i As Integer
    Dim g As Integer
    Dim highest_is_on As Boolean
    Dim ind_list As String

    dvprint "---------------"
    dprint "Select highest indicator in each group"
    
    g = 0
    highest_is_on = False
    For i = MAX_INDS To 1 Step -1
        If (inds(i).group > 0) Then
            If (inds(i).group <> g) Then
                'this is a new group
                g = inds(i).group
                highest_is_on = inds(i).checked
            Else
                'same group
                If highest_is_on Then
                    inds(i).checked = False             'uncheck a lower number
                Else
                    highest_is_on = inds(i).checked     'save this one
                End If
            End If
        End If
    Next i
    
    'Echo the indicators for an audit (no classification will be saved)
    If g_debug And g_no_output Then
        For i = 1 To MAX_INDS
            If inds(i).checked Then ind_list = ind_list & "," & i
        Next i
        dprint "Final list = " & Mid$(ind_list, 2)
    End If

End Sub
Private Sub SetClassString()
    Dim i As Integer
    Dim txarea As String
    Dim adl1only As String

    txarea = ""
    adl1only = ""
'    If m_pat.unit_id = 108 Then 'surg/ortho/peds
'        If m_pat.age <= 16 Then txarea = "Pediatrics"
'    End If
            
    outstr = g_util.FixedWidth("1", 8)                                       '(facility code)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.unit_name, 16)
    outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(unit code)
    outstr = outstr & "|" & g_util.FixedWidth(txarea, 16)                       '(area code)
    outstr = outstr & "|" & g_util.FixedWidth("", 8)                        '(class date - give datetime instead)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.acct, 20)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.last_name, 32)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.first_name, 32)
    outstr = outstr & "|" & g_util.FixedWidth("", 32)                       '(middle)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.room, 8)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.bed, 4)
    outstr = outstr & "|" & Format$(m_pat.effective, "yyyymmddhhnn")      'class datetime
    outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(login)
    outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(employee)
    outstr = outstr & "|" & g_util.FixedWidth(CStr(m_pat.meth_id), 4)
    outstr = outstr & "|" & g_util.FixedWidth("C", 1)                       'record type = class
    outstr = outstr & "|" & g_util.FixedWidth("", 4)                        '(stage)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.TC_source_id, 4)        'TC source ID
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.range, 4)               'TC pull range
    outstr = outstr & "|"
    outstr = g_util.FixedWidth(outstr, 294)
    outstr = outstr & "|" & Format$(m_pat.effective, "yyyymmddhhnn")        'IN
    outstr = g_util.FixedWidth(outstr, 346)
'    If m_pat.outdt <> 0 Then
'        outstr = outstr & "|" & Format$(m_pat.outdt, "yyyymmddhhnn")                '348 = OUT
'    End If
    outstr = g_util.FixedWidth(outstr, 377)
    outstr = outstr & "|"
    
    For i = 1 To MAX_INDS
'        adl1only = adl1only & "N"
        If (inds(i).checked) Then
            outstr = outstr & "Y"
            ind_list = ind_list & "," & i
        Else
            outstr = outstr & "N"
        End If
    Next i
'    Mid$(adl1only, 1) = "Y"
'    If adl_default And no_assess_items And Mid$(outstr, 379, MAX_INDS) = adl1only Then
'        Mid$(outstr, 379) = "N"
'        ind_list = ""
'    Else
'        ind_list = Mid$(ind_list, InStr(1, ind_list, ",") + 1)                'strip leading comma
'    End If
     ind_list = Mid$(ind_list, InStr(1, ind_list, ",") + 1)                'strip leading comma
End Sub
Private Sub OutputDNCandClass()
    Dim i As Integer
    Dim numloa_LT_start As Integer

    For i = 1 To m_pat.num_class
        If m_pat.Class(i).UnitID <> -1 Then
            If m_pat.Class(i).is_dnc Then
                OutputDNC m_pat.encounter_id, m_pat.Class(i).UnitID, m_pat.Class(i).indt, m_pat.Class(i).outdt
            Else
                Mid$(outstr, 10, 16) = g_util.FixedWidth(GetUnitName(m_pat.Class(i).UnitID), 16) 'unitname
                Mid$(outstr, 204, 12) = Format$(m_pat.Class(i).indt, "yyyymmddhhnn") 'class
                Mid$(outstr, 296, 12) = Format$(m_pat.Class(i).indt, "yyyymmddhhnn") 'in
                Mid$(outstr, 348, 12) = Format$(m_pat.Class(i).outdt, "yyyymmddhhnn") 'out
                Print #outfile, outstr
            End If
        End If
    Next i

    For i = 1 To m_pat.num_loa
        If Not m_pat.loa(i).retain Then
            XOutLOA m_pat.encounter_id, m_pat.loa(i).startdt, m_pat.loa(i).enddt
        End If
    Next i

    
    AddLogEntry EVENT_TYPE_INFO, "Classified: " & ind_list, EVENT_CATEGORY_PROCESSED

End Sub

'Private Sub OutputClass()
'    Dim outstr As String, ind_list As String
'    Dim i As Integer
'    Dim txarea As String
'
'    txarea = ""
''    If m_pat.unit_id = 108 Then 'surg/ortho/peds
''        If m_pat.age <= 16 Then txarea = "Pediatrics"
''    End If
'
'    outstr = g_util.FixedWidth("1", 8)                                       '(facility code)
'    outstr = outstr & "|" & g_util.FixedWidth(m_pat.unit_name, 16)
'    outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(unit code)
'    outstr = outstr & "|" & g_util.FixedWidth(txarea, 16)                       '(area code)
'    outstr = outstr & "|" & g_util.FixedWidth("", 8)                        '(class date - give datetime instead)
'    outstr = outstr & "|" & g_util.FixedWidth(m_pat.acct, 20)
'    outstr = outstr & "|" & g_util.FixedWidth(m_pat.last_name, 32)
'    outstr = outstr & "|" & g_util.FixedWidth(m_pat.first_name, 32)
'    outstr = outstr & "|" & g_util.FixedWidth("", 32)                       '(middle)
'    outstr = outstr & "|" & g_util.FixedWidth(m_pat.room, 8)
'    outstr = outstr & "|" & g_util.FixedWidth(m_pat.bed, 4)
'    outstr = outstr & "|" & Format$(m_pat.effective, "yyyymmddhhnn")      'class datetime
'    outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(login)
'    outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(employee)
'    outstr = outstr & "|" & g_util.FixedWidth(CStr(m_pat.meth_id), 4)
'    outstr = outstr & "|" & g_util.FixedWidth("C", 1)                       'record type = class
'    outstr = outstr & "|" & g_util.FixedWidth("", 4)                        '(stage)
'    outstr = outstr & "|" & g_util.FixedWidth(m_pat.TC_source_id, 4)        'TC source ID
'    outstr = outstr & "|" & g_util.FixedWidth(m_pat.range, 4)               'TC pull range
'    outstr = outstr & "|"
'    outstr = g_util.FixedWidth(outstr, 294)
'    outstr = outstr & "|" & Format$(m_pat.effective, "yyyymmddhhnn")        'IN
'    outstr = g_util.FixedWidth(outstr, 346)
'    If m_pat.outdt <> 0 Then
'        outstr = outstr & "|" & Format$(m_pat.outdt, "yyyymmddhhnn")                'OUT
'    End If
'    outstr = g_util.FixedWidth(outstr, 377)
'    outstr = outstr & "|"
'
'    For i = 1 To MAX_INDS
'        If (inds(i).checked) Then
'            outstr = outstr & "Y"
'            ind_list = ind_list & "," & i
'        Else
'            outstr = outstr & "N"
'        End If
'    Next i
'    ind_list = Mid$(ind_list, InStr(1, ind_list, ",") + 1)                'strip leading comma
'
'    If m_pat.intloa_startdt = 0 Then 'finishdt is also zero
'        Print #outfile, outstr
'    Else
'        'class before dnc
'        Mid$(outstr, 348, 12) = Format$(m_pat.intloa_startdt, "yyyymmddhhnn") 'out = startdt
'        Print #outfile, outstr
'
'        'class after dnc
'        Mid$(outstr, 204, 12) = Format$(m_pat.intloa_finishdt, "yyyymmddhhnn") 'in = finishdt
'        Mid$(outstr, 296, 12) = Format$(m_pat.intloa_finishdt, "yyyymmddhhnn") 'in = finishdt
'        Mid$(outstr, 348, 12) = Space$(12) 'outdt
'        Print #outfile, outstr
'    End If
'
'    AddLogEntry EVENT_TYPE_INFO, "Classified: " & ind_list, EVENT_CATEGORY_PROCESSED
'End Sub

Private Sub OutputProcs()
    Dim i, j As Integer
    Dim s, f As Date
    Dim outstr As String, proc_list As String

    For i = 1 To numprocs
        procs(i).isvalid = True
    Next i
    For i = 1 To numprocs - 1
        If procs(i).isvalid Then
        For j = i + 1 To numprocs
            If procs(j).isvalid And procs(j).start = procs(i).start And procs(j).finish = procs(i).finish Then   'this can be combined.
                procs(j).isvalid = False
                procs(j).pindex = i
            End If
        Next j
        End If
    Next i

    For i = 1 To numprocs
        If procs(i).isvalid Then
        outstr = g_util.FixedWidth("1", 8)                                       '(facility code)
        outstr = outstr & "|" & g_util.FixedWidth(UnitForProcStart(procs(i).start), 16) ' m_pat.unit_name, 16)
        outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(unit code)
        outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(area code)
        outstr = outstr & "|" & g_util.FixedWidth("", 8)                        '(class date - give datetime instead)
        outstr = outstr & "|" & g_util.FixedWidth(m_pat.acct, 20)
        outstr = outstr & "|" & g_util.FixedWidth(m_pat.last_name, 32)
        outstr = outstr & "|" & g_util.FixedWidth(m_pat.first_name, 32)
        outstr = outstr & "|" & g_util.FixedWidth("", 32)                       '(middle)
        outstr = outstr & "|" & g_util.FixedWidth(m_pat.room, 8)
        outstr = outstr & "|" & g_util.FixedWidth(m_pat.bed, 4)
        outstr = outstr & "|" & Format$(procs(i).start, "yyyymmddhhnn")       '204 procdt
        outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(login)
        outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(employee)
        outstr = outstr & "|" & g_util.FixedWidth(CStr(m_pat.meth_id), 4)
        outstr = outstr & "|" & g_util.FixedWidth("P", 1)                       'record type = class
        outstr = outstr & "|"
        outstr = outstr & Space$(294 - Len(outstr))
        outstr = outstr & "|" & Format$(procs(i).start, "yyyymmddhhnn")       '296 procdt in
        outstr = outstr & Space$(346 - Len(outstr))
        If procs(i).finish = 0 Then
            outstr = outstr & "|" & Space$(12)
        Else
            outstr = outstr & "|" & Format$(procs(i).finish, "yyyymmddhhnn")      '348 procdt out
        End If
        outstr = g_util.FixedWidth(outstr, 377)
        outstr = outstr & "|NNNNNNN"
        Mid$(outstr, 378 + procs(i).pnum, 1) = "Y"
        proc_list = proc_list & "," & procs(i).pnum
        For j = i + 1 To numprocs
            If Not procs(j).isvalid And procs(j).pindex = i Then
                Mid$(outstr, 378 + procs(j).pnum, 1) = "Y"
                proc_list = proc_list & "," & procs(j).pnum
            End If
        Next j
        proc_list = Mid$(proc_list, InStr(1, proc_list, ",") + 1)          'strip leading comma

        Print #outfile, outstr

        AddLogEntry EVENT_TYPE_INFO, "Procedure: " & proc_list, EVENT_CATEGORY_PROCESSED
        End If 'isvalid
    Next i

End Sub
Private Sub OutputOutcomes()
'    Dim i, j As Integer
'    Dim s, f As Date
'    Dim outstr As String
'    Dim octime As String
'
'    If numoutcomes = 0 Then Exit Sub
'
'    For i = 1 To numoutcomes
'        If (oc(i).checked) Then
'            outstr = g_util.FixedWidth("", 8)
'            outstr = outstr & "|" & g_util.FixedWidth(m_pat.unit_name, 16)
'            outstr = outstr & "|" & g_util.FixedWidth("", 16)
'            outstr = outstr & "|" & g_util.FixedWidth("", 16)
'            outstr = outstr & "|" & g_util.FixedWidth(oc(i).start, 12)
'            outstr = outstr & "|" & g_util.FixedWidth(m_pat.acct, 20)
'            outstr = outstr & "|" & g_util.FixedWidth(oc(i).pnum, 3)
'            Print #outfile2, outstr 'Print line to outcomesindicator.TXT
'        End If
'    Next i
End Sub


Private Function ReturnAssessCount(desclist As String) As Integer
    Dim sql As String
    Dim rs As New Recordset
    Dim ct As Integer
    Dim done_ct As Integer
    Dim mintime As Date
    Dim maxtime As Date
    Dim done As Boolean
    
    ReturnAssessCount = 0

    sql = "select count(distinct(event_datetime)) from chart_item " & WhereBase & AndSimpleItemFilter("", "", desclist, "", "")
    sql = sql & " and event_datetime is not null"
    'dvprint sql
    rs.Open sql, g_cnADO
'    dvprint sql

    If Not rs.EOF Then
        If IsNull(rs(0)) Then
            ct = 0
            rs.Close
            Exit Function
        Else
            ct = rs(0)
            rs.Close
        End If
    Else
        ct = rs(0)
        rs.Close
        Exit Function
    End If
    
    ReturnAssessCount = ct
    
End Function
Private Function ReturnAssessCountWithResults(desclist As String, reslist As String) As Integer
    Dim sql As String
    Dim rs As New Recordset
    Dim ct As Integer
    Dim darr() As String
    Dim rarr() As String
    Dim nd As Integer, nr As Integer, i As Integer
    
    ReturnAssessCountWithResults = 0
    
    nd = g_util.SplitTextOnChar(desclist, ",", darr(), 1, 0)
    nr = g_util.SplitTextOnChar(reslist, ",", rarr(), 1, 0)
    If nd <> nr Then Exit Function
    
    sql = " and ("
    For i = 1 To nd
        If Len(Trim$(rarr(i))) > 0 Then
            sql = sql & "(description like '" & Trim$(darr(i)) & "%' and result like '%" & Trim$(rarr(i)) & "%') or "
        Else
            sql = sql & "(description like '" & Trim$(darr(i)) & "%') or "
        End If
    Next i
    sql = Mid$(sql, 1, Len(sql) - 4) 'remove last " or "
    sql = sql & ")"

    sql = "select count(distinct(event_datetime)) from chart_item " & WhereBase & sql
    sql = sql & " and event_datetime is not null"
    'dvprint sql
    rs.Open sql, g_cnADO
'    dvprint sql

    If Not rs.EOF Then
        If IsNull(rs(0)) Then
            ct = 0
            rs.Close
            Exit Function
        Else
            ct = rs(0)
            rs.Close
        End If
    Else
        ct = rs(0)
        rs.Close
        Exit Function
    End If
    
    ReturnAssessCountWithResults = ct
    
End Function
Private Sub SetupBuckets(bucketsize As Integer)
    Dim i As Integer
'b1 = pull_start to +70
'b2=  b1 end
' 700 - 1022  range = 3h 22min = 202mins   202/70=2 -> 3 buckets
' 700:700+70=810, 810:810+70=920,  920:finish
    num_buckets = g_range \ bucketsize
dprint "setupbucket: m_pat.pull_start=" & m_pat.pull_start
dprint "setupbucket: num_buckets=" & num_buckets
dprint "setupbucket: g_range=" & g_range
    If (g_range Mod bucketsize) <> 0 Then num_buckets = num_buckets + 1
    For i = 1 To num_buckets
        bucket(i).startdt = DateAdd("n", (i - 1) * bucketsize, m_pat.pull_start)
        bucket(i).count = 0
    Next i
    For i = 1 To num_buckets - 1
        bucket(i).enddt = bucket(i + 1).startdt
    Next i
    bucket(num_buckets).enddt = m_pat.pull_finish

'    For i = 1 To num_buckets
'        dprint i & ": " & bucket(i).startdt
'    Next i

End Sub
Private Sub PutIntoBuckets(desclist As String)
    Dim sql As String, and_filter As String
    Dim rs As New Recordset
    Dim count As Long, i As Integer
    Dim pos As Integer
    Dim s As String

    s = ""
    For i = 1 To num_buckets
        s = s & "sum(case when event_datetime>=" & g_dbutil.SQL_DateTime(bucket(i).startdt) & " and "
        If i < num_buckets Then
            s = s & "event_datetime<" & g_dbutil.SQL_DateTime(bucket(i).enddt) & " then 1 else 0 end) as range" & i & ","
        Else
            s = s & "event_datetime=" & g_dbutil.SQL_DateTime(bucket(i).enddt) & " then 1 else 0 end) as range" & i & ","
        End If
    Next i
    s = Mid$(s, 1, Len(s) - 1)  'remove last comma

    sql = "select " & s
    sql = sql & " from (select distinct(event_datetime) from chart_item " & WhereBase() & AndSimpleItemFilter("", "", desclist, "", "") & ") as Q"

    rs.Open sql, g_cnADO
    If Not IsNull(rs(0)) Then
        For i = 1 To num_buckets
            bucket(i).count = bucket(i).count + rs(i - 1)
        Next i
    End If
        
    rs.Close

End Sub
Private Sub PutIntoBucketsWithResults(desclist As String, reslist As String)
    Dim sql As String, qsql As String
    Dim rs As New Recordset
    Dim count As Long, i As Integer
    Dim pos As Integer
    Dim s As String
    Dim darr() As String
    Dim rarr() As String
    Dim nd As Integer, nr As Integer

       
    nd = g_util.SplitTextOnChar(desclist, ",", darr(), 1, 0)
    nr = g_util.SplitTextOnChar(reslist, ",", rarr(), 1, 0)
    If nd <> nr Then Exit Sub
    
    qsql = " and ("
    For i = 1 To nd
        If Len(Trim$(rarr(i))) > 0 Then
            qsql = qsql & "(description like '" & Trim$(darr(i)) & "%' and result like '%" & Trim$(rarr(i)) & "%') or "
        Else
            qsql = qsql & "(description like '" & Trim$(darr(i)) & "%') or "
        End If
    Next i
    qsql = Mid$(qsql, 1, Len(qsql) - 4) 'remove last " or "
    qsql = qsql & ")"
    
    s = ""
    For i = 1 To num_buckets
        s = s & "sum(case when event_datetime>=" & g_dbutil.SQL_DateTime(bucket(i).startdt) & " and "
        If i < num_buckets Then
            s = s & "event_datetime<" & g_dbutil.SQL_DateTime(bucket(i).enddt) & " then 1 else 0 end) as range" & i & ","
        Else
            s = s & "event_datetime=" & g_dbutil.SQL_DateTime(bucket(i).enddt) & " then 1 else 0 end) as range" & i & ","
        End If
    Next i
    s = Mid$(s, 1, Len(s) - 1)  'remove last comma

    sql = "select " & s
    sql = sql & " from (select distinct(event_datetime) from chart_item " & WhereBase() & qsql & ") as Q"

    rs.Open sql, g_cnADO
    If Not IsNull(rs(0)) Then
        For i = 1 To num_buckets
            bucket(i).count = bucket(i).count + rs(i - 1)
        Next i
    End If
        
    rs.Close

End Sub

Private Sub SetLOS()
    Dim i As Integer

    patlos = 0
    For i = 1 To m_pat.num_class
        If m_pat.Class(i).UnitID <> -1 And Not m_pat.Class(i).is_dnc Then
            patlos = patlos + DateDiff("n", m_pat.Class(i).indt, m_pat.Class(i).outdt)
        End If
    Next i

End Sub
Private Function GetUnitName(UnitID As Long) As String
    Dim sql As String
    Dim rs As New Recordset

    sql = "select name from unit where unit_id=" & UnitID
    rs.Open sql, g_cnADO
    GetUnitName = rs(0)
    rs.Close

End Function

Private Function UnitForProcStart(sdt As Date) As String
    Dim i As Integer
    Dim name As String

    name = ""
    For i = 1 To m_pat.num_class
        If sdt >= m_pat.Class(i).indt And sdt <= m_pat.Class(i).outdt Then
            name = GetUnitName(m_pat.Class(i).UnitID)
        End If
    Next i
    If name = "" Then name = m_pat.unit_name
    UnitForProcStart = name

End Function


