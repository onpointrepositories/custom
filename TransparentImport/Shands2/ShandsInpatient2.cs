﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;                     // for LINQ; add a reference
using System.Data.SqlClient;                // for SqlConnection
using System.IO;                            // for Directory, Path, Stream
using PfsShared;                            // add a reference to Shared2 project
//
// Shands Inpatient 2.0 transparent mapping
//
// This processes one patient.  Only one database query is used.  :)
//
// All search functions use exact match for category, description and field name.
// Codes and results can be a single word or a list of words.
// Codes default to exact match; prefix with CODE_CODE_LIKE_PREFIX if desired (single word only)
// Results default to like matching; prefix with RESULT_EXACT_MATCH_PREFIX if desired (single word only)
//
// All searches are case insensitive.
//
namespace Shands2
{
    class ShandsInpatient2
    {
        private const int MAX_INDS = 50;
        private const int MAX_PROCS = 20;

        private const string CODE_LIKE_PREFIX = "%!";       // use to make codes '%like%' (match anywhere)
        private const string START_WITH_PREFIX = "%@";       // use to make codes 'like%' (match start)
        private const string EXACT_MATCH_PREFIX = "&!";     // use to make desc/result exact match
        private const string CHAR_COMMA = "||";             // use to insert literal comma in word list

        private struct indicator_data {
            public bool     is_checked;     
            public int      radio_group;       
        }

        private struct proc_data {                      
            public int      procedure_number;
            public DateTime start;   
            public DateTime finish;
        }

        // These are database CHART_ITEMs for this patient
        private CHART_ITEM[] _chart_items_since_admission;
        private CHART_ITEM[] _chart_items_since_unit_arrival;
        private CHART_ITEM[] _chart_items_during_pull_period;
        // These are database PROCEDURE_EVENTs for this patient
        private PROCEDURE_EVENT[] _procedure_events;

        private indicator_data[] _inds;
        private List<proc_data> _procs;

        private const int DEFAULT_BUCKET_SIZE = 20;       //min

        private PatientInfo _pat;
        private bool adl23;
        private bool adl4;
        private bool tubefeed;
        private int  morse;
        private int  _bucket_size = DEFAULT_BUCKET_SIZE;
        private bool is_default;


        private enum SearchDepth {
            SearchDefault,
            SearchPullRange,            //search within the current pull     -- default
            SearchSinceArrival,         //search since arrival to the unit
            SearchSinceAdmission        //search everything since admission to the hospital
        }

        private enum CountMode {
            CountAll,
            CountFirst                  //stop after one is found
        }

        private enum GetValueMode {
            GetTotal,
            GetMax,
            GetLast
        }

        enum Frequencies {
            QNONE,
            Q4H,
            Q2H,
            Q1H,
            Q30M
        }

        private struct fmapRow {
            public double   los_high;                       //the LOS being testing
            public int[]    freq;                           //the count required for each Q value
        }
        private List<fmapRow> _freq_map;                    //1,2,4,8,12,24 hours

     
        //
        // This is the main entry point
        //
        public void ProcessPatient(PatientInfo pat)
        {
            _pat = pat;

            InitIndicators(); // sets is_default
            InitProcs();
            if (! is_default)
                {
                LoadFreqTable();
                LoadPatientChart();
                Check_1_2_3_4();
                //adl23 = (_inds[2].is_checked || _inds[3].is_checked);
                //adl4 = (_inds[4].is_checked);
                Check_5();
                Check_6_7();
                Check_8();
                Check_9();
                Check_10_11();
                Check_12_13();
                Check_14();
                Check_15_16_17_18();
                Check_19();
                Check_20();
                Check_21_22();
                Check_23();
                Check_24();
            }
            AtLeastOneADL();
            HighestIndicatorInEachGroupWins();

            if (!is_default)
            {
                CheckProcs();
            }

            if (Program.g_no_output) return;
            OutputClass();
            OutputProcs();
        }

       
        private void InitIndicators()
        {
            // Make a new (empty) indicator array
            _inds = new indicator_data[MAX_INDS + 1];           // This 1 based so add one
            is_default = false;
            if (_pat.los_hours <= 4.0) {
                is_default = true;
                Program.VerboseAudit("Patient was here 4 hrs or less. Will receive default indicators " + _pat.default_inds_str);
                foreach (var ind in _pat.default_inds) {
                    if (ind <= _inds.GetUpperBound(0)) {
                        _inds[ind].is_checked = true;
                    }
                }
                if (_pat.age < 4.0) _inds[4].is_checked = true;
            }

            adl23 = false;
            adl4 = false;
            tubefeed = false;
            morse = 0;

            // get indicator radio groups from the database
            // ** (This database access can be replaced once we have a C# methodolgy cache)
            var db = PFSUtility.NewPfsDataContext();
            var query = from idef in db.INDICATOR_DEFINITIONs
                        where (idef.METHODOLOGY_ID == _pat.meth_id)
                        select idef;
            foreach (var idef in query) {
                if (idef.INDICATOR_NUMBER <= _inds.GetUpperBound(0)) {
                    // (convert nulls to zero)
                    _inds[idef.INDICATOR_NUMBER].radio_group = PFSUtility.DBToInt(idef.RADIO_GROUP);
                }
            }
        }

        private void InitProcs()
        {
            _procs = new List<proc_data>();
        }

        private fmapRow LoadFreqTableRow(double los_high, string values)
        {
            fmapRow fmrow;
            
            fmrow.los_high = los_high;
            fmrow.freq = new int[5];

            var arr = values.Split(',');
            for (int i = 0; i <= fmrow.freq.GetUpperBound(0) ; i++ )
            {
                fmrow.freq[i] = arr[i].ToInteger();
            }
            return fmrow;
        }

        // These are the number of times you need to do something during a certain time period (LOS)
        // in order for it to be considered one of the frequencies listed.
        private void LoadFreqTable()
        {
            _freq_map = new List<fmapRow>();
            //                              LOS,  None Q4h Q2h Q1h Q30m
            _freq_map.Add(LoadFreqTableRow(1, "    0,  0,  0,  1,  2"));
            _freq_map.Add(LoadFreqTableRow(2, "    0,  0,  1,  2,  3"));
            _freq_map.Add(LoadFreqTableRow(4, "    0,  1,  2,  3,  5"));
            _freq_map.Add(LoadFreqTableRow(6, "    0,  1,  2,  4,  8"));
            _freq_map.Add(LoadFreqTableRow(8, "  0,  1,  2,  6, 10"));
            _freq_map.Add(LoadFreqTableRow(12, " 0,  2,  4,  8, 15"));
            //_freq_map.Add(LoadFreqTableRow(16, "   0,  3,  5,  9, 20"));
            //_freq_map.Add(LoadFreqTableRow(24, "   0,  4,  8, 15, 29"));
            _freq_map.Add(LoadFreqTableRow(9999, " 0,  4,  8, 15, 29"));
//New freq table 2/5/14
//q4	q2	q1	q30     q30
//            Non-ICU	ICU & SD
// 4	8	15	29	    36
// 3	5	9	17	    24
// 2	4	7	13	    19
// 2	3	5	10	    13

            //to read this table: if you have at least 6 asessmenets in a 12-hour period, you get Q1h
            //(LOS=12, column Q1h has a count of 6)
        }

        private Frequencies FreqForCount(double los_hours, int count) 
        {
            foreach(var fmrow in _freq_map) {
                if (los_hours <= fmrow.los_high) {
                    // Option: pro-rate the count upward if the LOS is less than the row we are interested in.
                    //         This will bump the count to what it might have been at the full LOS.
                    // Note: truncate the result; rounding inflates the value too much.
                    int prorated_count = (int)((fmrow.los_high / los_hours) * count);

                    // foreach goes low to high; go from high to low instead
                    for (int j = (int)Frequencies.Q30M; (j >= (int)Frequencies.QNONE); j--) { //search right to left
                        if (prorated_count >= fmrow.freq[j]) {
                            return (Frequencies)j;
                        }
                    } // next j
                }
            }
            
            return Frequencies.QNONE;
        }

        //
        // Load patient chart from the database (one DB access)
        // Load everything since admission
        //
        private void LoadPatientChart()
        {
            // Get the entire patient chart (all units and dates for this patient)
            var db = PFSUtility.NewPfsDataContext();
            var query = from item in db.CHART_ITEMs
                        where (item.ENCOUNTER_ID == _pat.encounter_id)
                        select item;
            // Save the result
            _chart_items_since_admission = query.ToArray();

            // Convert all results to lower case for case insensitive comparisons
            foreach (var item in _chart_items_since_admission) {
                if (item.CATEGORY != null) item.CATEGORY = item.CATEGORY.ToLower();
                if (item.CODE != null) item.CODE = item.CODE.ToLower();
                if (item.DESCRIPTION != null) item.DESCRIPTION = item.DESCRIPTION.ToLower();
                if (item.FIELD_NAME != null) item.FIELD_NAME = item.FIELD_NAME.ToLower();
                if (item.RESULT != null) item.RESULT = item.RESULT.ToLower();
            }

            // Prepare two more versions of the chart
            var query2 = from item in _chart_items_since_admission
                    where(item.EVENT_DATETIME >= _pat.unit_arrival) && (item.EVENT_DATETIME <= _pat.pull_finish)
                    select item;
            _chart_items_since_unit_arrival = query2.ToArray();

            query2 = from item in _chart_items_since_admission
                     where (item.EVENT_DATETIME >= _pat.pull_start) && (item.EVENT_DATETIME <= _pat.pull_finish)
                     select item;
            _chart_items_during_pull_period = query2.ToArray();
        }

        // Delay looking for patient procedures until they are wanted
        private void LoadPatientProceduresIfNeeded()
        {
            if (_procedure_events != null) return;

            var db = PFSUtility.NewPfsDataContext();
            var query = from proc in db.PROCEDURE_EVENTs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                        select proc;
            _procedure_events = query.ToArray();
        }



        // Started a new chart item query (of a certain depth)
        // Default is the chart during the pull range.  Options for unit arrival and admission to hospital.
        private IEnumerable<CHART_ITEM> StartNewQuery()
        {
            return StartNewQuery(SearchDepth.SearchDefault);
        }
        private IEnumerable<CHART_ITEM> StartNewQuery(SearchDepth search_depth)
        {
            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    return (from item in _chart_items_during_pull_period select item);
                case SearchDepth.SearchSinceArrival:
                    return (from item in _chart_items_since_unit_arrival select item);
                case SearchDepth.SearchSinceAdmission:
                    return (from item in _chart_items_since_admission select item);
            }
            return null;
        }

        // Is this search term a word or a list of words?
        private bool ValueIsAList(string s) 
        {
            return (s != null) && s.Contains(',');                //is this a comma-separated list?
        }

        // Break apart a wordlist and prepare its members
        private string[] SplitOnCommaAndPrepareElements(string s)
        {
            // If a word list has a space after each comma, we will get leading blanks in each term
            var arr = s.Split(',');
            for (int i = 0; i <= arr.GetUpperBound(0); i++) {
                arr[i] = arr[i].Trim();                         // get rid of leading blanks
                arr[i] = arr[i].Replace(CHAR_COMMA, ",");       // insert a literal comma into search word
                arr[i] = arr[i].ToLower();                      // convert for case-insensitive comparisons
            }
            return arr;
        }

        // Add a basic chart item filter to a LINQ query
        //
        // cat = exact match
        // code_list = exact match; optional "like" match
        // desc_list = "like" match; optional exact match
        // field = exact match
        // result_list = "like" match; optional exact match
        //
        // These are comma-separated lists.  
        // Search for a literal comma by using CHAR_COMMA in the string.
        private IEnumerable<CHART_ITEM> AndItemFilter(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list) 
        {
            // Converted all values to lower case for case-insensitive comparisons.
            if (!String.IsNullOrEmpty(cat)) {
                cat = cat.ToLower();
                query = query.Where(e => e.CATEGORY == cat);
            }

            if (!String.IsNullOrEmpty(code_list)) {
                code_list = code_list.ToLower();
                if (code_list.Left(2) == CODE_LIKE_PREFIX) {            // override for "%like%" match?
                    query = query.Where(e => e.CODE.Contains(code_list.Substring(2)));
                } else if (ValueIsAList(code_list)) {
                    query = AndCodeInList(query, code_list);            // find one of the words
                } else {
                    query = query.Where(e => e.CODE == code_list);      // find this word
                }
            }
            
            if (!String.IsNullOrEmpty(desc_list)) {
                desc_list = desc_list.ToLower();
                if (desc_list.Left(2) == EXACT_MATCH_PREFIX) {          // exact match override?
                    query = query.Where(e => e.DESCRIPTION == desc_list.Substring(2));
                } else if (ValueIsAList(desc_list)) {
                    query = AndDescriptionInList(query, desc_list);
                } else {
                    desc_list = desc_list.Replace(CHAR_COMMA, ",");     // look for literal comma requests
                    query = query.Where(e => e.DESCRIPTION.Contains(desc_list));
                }
            }
            
            if (!String.IsNullOrEmpty(field)) {
                field = field.ToLower();
                query = query.Where(e => e.FIELD_NAME == field);
            }
            
            if (!String.IsNullOrEmpty(result_list)) {
                result_list = result_list.ToLower();                        // make all results case insensitve
                if (result_list.Left(2) == EXACT_MATCH_PREFIX) {     // exact match override?
                    query = query.Where(e => e.RESULT == result_list.Substring(2));
                } else if (result_list.Left(2) == START_WITH_PREFIX)  {    // override for "like%" match?
                    query = query.Where(e => e.RESULT.StartsWith(result_list.Substring(2)));
                } else if (ValueIsAList(result_list)) {
                    query = AndResultInList(query, result_list);                
                } else  {
                    result_list = result_list.Replace(CHAR_COMMA, ",");     // look for literal comma requests
                    query = query.Where(e => e.RESULT.Contains(result_list));
                }
            }
            return query;
        }

        // Look for a code that is one of the words in the list.
        private IEnumerable<CHART_ITEM> AndCodeInList(IEnumerable<CHART_ITEM> query, string code_list) 
        {
            if (String.IsNullOrEmpty(code_list)) return query;
            
            var arr = SplitOnCommaAndPrepareElements(code_list);
            return query.Where(e => arr.Contains(e.CODE));              // exact match
        }

        // Look for a description that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndDescriptionInList(IEnumerable<CHART_ITEM> query, string desc_list) 
        {
            if (String.IsNullOrEmpty(desc_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(desc_list);
            return query.Where(e => e.DESCRIPTION.ContainsAny(arr));    // "like" match
        }

        // Look for a result that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultInList(IEnumerable<CHART_ITEM> query, string result_list) 
        {
            if (String.IsNullOrEmpty(result_list)) return query;
            
            var arr = SplitOnCommaAndPrepareElements(result_list);
            return query.Where(e => e.RESULT.ContainsAny(arr));         // "like" match
        }

        // Look for a result that does not contain one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultNotInList(IEnumerable<CHART_ITEM> query, string result_list)
        {
            if (String.IsNullOrEmpty(result_list)) return query;
            
            var arr = SplitOnCommaAndPrepareElements(result_list);
            return query.Where(e => !e.RESULT.ContainsAny(arr));        // "like" match
        }

        private string DescribeSearchDepth(SearchDepth search_depth) 
        {
            string result = "";
            
            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    //result = "in pull range"
                    result = "";                         //be quiet since this is default
                    break;
                case SearchDepth.SearchSinceArrival:
                    result = "since arrival to unit";
                    break;
                case SearchDepth.SearchSinceAdmission:
                    result = "since admission";
                    break;
            }
            
            return result;
        }

        // Describe a query that has already been run
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Make a simple query and describe the results
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            var arr = query.ToArray();
            return Describe(arr, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private string LookingFor(string so_far, string what, string op, string value)
        {
            string result;
            if (String.IsNullOrEmpty(value)) return so_far;

            if (so_far == "")
                result = "looking for " + what;
            else
                result = so_far + " and " + what;

            if (value.Left(2) == EXACT_MATCH_PREFIX)
                result += "='" + value.Substring(2) + "'";
            else if (value.Left(2) == CODE_LIKE_PREFIX)
                result += " contains " + "'" + value.Substring(2) + "'";
            else if (value.Left(2) == START_WITH_PREFIX)
                result += " starts with " + "'" + value.Substring(2) + "'";
            else if (ValueIsAList(value))
                result += " in '" + value + "'";
            else
                result += op + "'" + value + "'";
            
            return result;
        }
        private string Describe(CHART_ITEM[] arr, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string result = "";
            result = LookingFor(result, "cat", "=", cat);
            result = LookingFor(result, "code", "=", code_list);
            result = LookingFor(result, "desc", " contains ", desc_list);
            result = LookingFor(result, "field", "=", field);
            result = LookingFor(result, "result", " contains ", result_list);

            if (arr.Count() == 0) {
                result += "; not found " + DescribeSearchDepth(search_depth);
            } else {
                var e = arr[0];
                result = "FOUND: " + result + "; found";
                // We might have searched for a pattern or word list in several fields - show what was found
                if (e.CATEGORY != null)     result += " cat='" + e.CATEGORY + "'";
                if (e.CODE != null)         result += " code='" + e.CODE + "'";
                if (e.DESCRIPTION != null)  result += " desc='" + e.DESCRIPTION + "'";
                if (e.FIELD_NAME != null)   result += " field='" + e.FIELD_NAME + "'";
                if (e.RESULT != null)       result += " result='" + e.RESULT + "'";
                // Are there more results?  Just say how many; we aren't going to list them.
                if (arr.Count() == 2) {
                    result += " (1 more result)";
                } else if (arr.Count() > 2) {
                    result += " (" + (arr.Count() - 1) + " more results)";
                }
            }
            
            return result;
        }


        //Set an indicator for this reason (low level)
        private void SetInd(int inum, string reason)
        {
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Set Ind #" + inum + ": " + reason);          //already set - repeat for verbose only
            } else {
                _inds[inum].is_checked = true;
                Program.Audit("Set Ind #" + inum + ": " + reason);
            }
        }

        //Clear an indicator for this reason (low level)
        private void ClrInd(int inum, string reason)
        {
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Clr Ind #" + inum + ": " + reason);          //already clear - repeat verbose only
            } else {
                _inds[inum].is_checked = false;
                Program.Audit("Clr Ind #" + inum + ": " + reason);
            }
        }

        // Count how many items meet the conditions
        // All items are listed
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountItems(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            int count = query.Count();

            // always return what was found
            found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
            // echo the result?
            if (trace) Program.VerboseAudit(found_what);
            
            return count;
        }

        // Count how many items meet the conditions
        // This has the option to list every entry or give a total
        private int CountResultInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what="";
 
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);

            var arr = SplitOnCommaAndPrepareElements(result_list);

            foreach (var item in query) {
                // Figure out which of the search words were found
                var s = item.RESULT.ContainsWhich(arr);
                found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' code='" + item.CODE + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "'";
                if (trace) Program.VerboseAudit(found_what);
                count++;
                if (count_mode != CountMode.CountAll) break;        //stop counting
            }
            
            if (count > 0) {
                //We already printed what was found; maybe add how many?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }
            
            return count;
        }

        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (ValueIsAList(result_list)) {
                return CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what);
            } else {
                return CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what);
            }
        }

        // Is there an item with this result?
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list) 
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth) 
        {
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContains(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what) 
        {
            if (ValueIsAList(result_list)) {
                return (CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            } else {
                return (CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
            }
        }

        // Count how many items meet the conditions, with none of the results in result_list.
        // This has the option to say how many were found rather than list all.
        private int CountResultNotInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what) 
        {
            int count = 0, rec_count = 0;
            bool found_one;

            found_what = "";
            var arr = SplitOnCommaAndPrepareElements(result_list);         // also deals with CHAR_COMMA
            
            //Do not filter by result here; we need all results
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");
            
            foreach(var item in query) {
                rec_count++;
                //Look for each search word in the result
                found_one = false;
                
                foreach (string s in arr) {
                    //if (String.Equals(item.RESULT, s)) {
                    if (item.RESULT.Contains(s)) {
                        found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "' -- ignore this";
                        Program.VerboseAudit(found_what);
                        found_one = true;
                        break;
                    }
                }

                if (! found_one) {
                    //none of the words were found - good!
                    found_what = Describe(cat, code_list, desc_list, field, "", search_depth) + " -- does not contain '" + result_list + "'";
                    Program.VerboseAudit(found_what);
                    count++;
                    if (count_mode != CountMode.CountAll) break;
                }
            }
            
            if (count > 0) {
                //We already printed what was found; maybe show the total?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else if (rec_count > 0) {
                //We already printed what we ignored
            } else {
                //Print what did not exist
                found_what = Describe(cat, code_list, desc_list, field, "", search_depth);      //not found
                Program.VerboseAudit(found_what);
            }
            
            return count;
        }

        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, string found_what) 
        {
            // Search in the pull range with trace on
            return CountResultDoesNotContain(cat, code_list, desc_list, field, result_list, out found_what, SearchDepth.SearchDefault, true);
        }
        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, out string found_what, SearchDepth search_depth, bool trace)
        {
            return CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0;
        }

        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, string found_what)
        {
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, out found_what, SearchDepth.SearchDefault, true);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, out string found_what, SearchDepth search_depth, bool trace)
        {
            return (CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0);
        }

        // Set the indicator if the conditions are met
        //
        private void SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already set
            if (_inds[inum].is_checked) return;
            
            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what)) {
                SetInd(inum, found_what);                          //echo here - found
            } else {
                Program.VerboseAudit(found_what);                  //and here - not found
            }
        }

        // Set the indicator if the result does not contain any of the words in result_list
        //
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultDoesNotContain(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {   
            string found_what;
            //avoid more queries if the indicator is already set
            if (_inds[inum].is_checked) return;
            
            //Turn trace off for ResultDoesNotContain() and echo what was set below with SetInd
            if (ResultDoesNotContain(cat, code_list, desc_list, field, result_list, out found_what, search_depth, false)) {
                SetInd(inum, found_what);                           //echo here - not found
            } else {
                Program.VerboseAudit(found_what);                   //and here - found
            }
        }

        // Clear the indicator if the result contains one of the words in the result_list
        //
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already clear
            if (! _inds[inum].is_checked) return;
            
            //Turn trace off for ResultContains() and echo what was set below with SetInd
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what)) {
                ClrInd(inum, found_what);                           //echo here - found
            } else {
                Program.VerboseAudit(found_what);                   //and here - not found
            }
        }


        //These slightly smaller functions are meant for places where you aren't really looking for a chart result
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            return (CountResultContains(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
        }

        // Variations with optional parameters (can switch to optional parameters in VS 2010)
        //
        private void SetIndIfFound(int inum, string cat)
        {
            SetIndIfResultContains(inum, cat, "", "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list)
        {
            SetIndIfResultContains(inum, cat, code_list, "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private void ClrIndIfFound(int inum, string cat)
        {
            ClrIndIfResultContains(inum, cat, "","","","", SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }


        // Get the max/total value from a result (usually in the middle of the text)
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetIntValue(get_mode, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            int value;
            int result = 0;
            bool found_one = false;

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            
            //Look for a number in the result
            
            foreach(var item in query) {
                // RESULT might be null so convert to empty string so Split won't blow up
                var arr = PFSUtility.DBToString(item.RESULT).Split(' ');
                foreach (string s in arr) {
                    //Look for 1st character numeric; IsNumeric("60min") will fail so look at 1st char only
                    if (s.Left(1).IsNumeric()) {
                        value = (int)s.Val();                           //Use Val; ToInteger will error on "60min"
                        switch (get_mode) {
                            case GetValueMode.GetMax:
                                result = Math.Max(value, result);       //max
                                break;
                            case GetValueMode.GetTotal:
                                result += value;                        //total
                                break;
                            case GetValueMode.GetLast:
                                result = value;                         //last
                                break;
                        }
                        
                        // print what we are searching for (the first time)
                        if (! found_one) {
                            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
                        }
                        found_one = true;
                        // print each value found
                        Program.VerboseAudit("  found numeric value " + result);
                        //Keep going in case there are more
                    }
                } 
            }
            
            if (! found_one) {
                //show what was not found
                Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
            }
            
            return result;
        }

        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetMaxValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return GetIntValue(GetValueMode.GetMax, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetTotalValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return GetIntValue(GetValueMode.GetTotal, cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Get a result; returns true if found with return_result
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result)
        {
            return GetResult(cat, code_list, desc_list, field, out return_result, SearchDepth.SearchDefault);
        }
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0) {
                return_result = PFSUtility.DBToString(query.First().RESULT);
            } else {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }
        private void GetResultforOther(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "Other (comment)");
            return_result="";
            foreach (var item in query)
            {
                if (!String.Equals(item.RESULT, "Other (comment)"))
                {
                    return_result = item.RESULT;
                    //Program.VerboseAudit(found_what);
                    break;
                }

            }
            return;
        }

        //======================================================================================
        // Mapping starts here
        //======================================================================================

        private void Check_1_2_3_4()
        {
            bool mob3 = false;
            bool mob4 = false;
            bool feed3 = false;
            bool feed4 = false;
            bool hyg3 = false;
            bool hyg4 = false;
            string found_what;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("1. ADL Self");
            Program.VerboseAudit("2. ADL Assist");
            Program.VerboseAudit("3. ADL Extended");
            Program.VerboseAudit("4. ADL Complete");
            Program.VerboseAudit("---------------");
            
            // 4. COMPLETE CARE

            adl23 = ResultContains("", "LOAMOBILITY", "", "", "assistance by 2-3 caregivers") || 
                    ResultContains("", "HYGIENEASSISTANCE", "", "", "ADL assistance by 2-3 caregivers");
            adl4 = ResultContains("", "LOAMOBILITY", "", "", "assistance by 4 or more caregivers") || 
                   ResultContains("", "HYGIENEASSISTANCE", "", "", "ADL assistance by 4 or more caregivers");
            tubefeed = ResultContains("", "FEEDING", "", "", "Tube Feeding");
            if (GetResult("", "MORSESCORE", "", "", out found_what)) {
                morse = found_what.ToInteger();
            }
   
            if (_pat.age < 4.0) {
                SetInd(4,"Age <=3 years");
            }
            if (_inds[4].is_checked) return;
    
            feed4 = ResultContains("", "FEEDING", "", "", "Total Assist") || tubefeed || 
                    ResultContains("", "FEEDING", "", "", "NPO");
            hyg4 = ResultContains("", "HYGIENEASSISTANCE", "", "", START_WITH_PREFIX + "Dependent") ||
                   ResultContains("", "HYGIENEASSISTANCE", "", "", ";Dependent");
            if (!hyg4)
            {
                hyg4 = ResultContains("", "HYGIENEASSISTANCE", "", "", "Maximum assist");
            }
            
            mob4 = ResultContains("", "LOAMOBILITY", "", "", "Dependent" + CHAR_COMMA + " patient does less than 25%");
            if (feed4 && hyg4) {
                SetInd(4, "Total Feed+Dependent Hyg");
            }
    
      //    if (ResultContains("", "FEEDING", "", "", "Total Assist") && _
      //         (ResultContains("", "HYGIENEASSISTANCE", "", "", "Maximum assist") || _
      //          ResultContains("", "HYGIENEASSISTANCE", "", "", EXACT_MATCH_PREFIX + "Dependent")) {
      //            SetInd 4, "Total Feeding + Max/Dep Hygiene"
      //    }
                
            if (_inds[4].is_checked) return;
    
      //    SetIndIfResultContains 4, "", "FEEDING", "", "", "Total Assist"
      //    SetIndIfResultContains 4, "", "LOAMOBILITY", "", "", "Maximum assist" + CHAR_COMMA + " patient does 25-49%"
      //    SetIndIfResultContains 4, "", "LOAMOBILITY", "", "", "Dependent" + CHAR_COMMA + " patient does less than 25%"
          //SetIndIfResultContains 4, "", "HYGIENEASSISTANCE", "", "", "Maximum assist,Dependent"
      //    SetIndIfResultContains 4, "", "HYGCONFACTORS", "", "", "Impaired Mobility,Obesity,Restraints,Risk of Dislodging LDAs,Unable to Follow Commands,Other (Comment)"

            // 3. EXTENDED CARE

            SetIndIfResultContains(3, "", "GISYMPTOMS", "", "", "Incontinence");
            SetIndIfResultContains(3, "", "GUSYMPTOMS", "", "", "Incontinence");
    
            if (feed4) feed3 = true;
            if (! feed3) feed3 = ResultContains("", "FEEDING", "", "", "Needs Assist");
            
            if (hyg4) hyg3 = true;
            if (! hyg3) {
                hyg3 = ResultContains("", "HYGIENEASSISTANCE", "", "", "Minimal assist,Moderate assist,Maximum assist");
            }
            if (! hyg3) hyg3 = ResultContains("", "HYGIENEASSISTANCE", "", "", "ADL assistance by 2-3 caregivers,ADL assistance by 4 or more caregivers");
           
            if (mob4) mob3 = true;
            if (! mob3) mob3 = ResultContains("", "LOAMOBILITY", "", "", "Minimal assist,Moderate assist,Maximum assist") || adl23 || adl4;
            if (! mob3) mob3 = ResultContains("", "LOAMOBILITY", "", "", START_WITH_PREFIX + "Dependent");
            if (!mob3) mob3 = ResultContains("", "LOAMOBILITY", "", "", ";Dependent");
            if (!mob3)
            {
                if (morse >= 45) {
                    Program.VerboseAudit("Morse score=" + morse);
                    mob3 = true;
                }
            }
    
            if (feed3 && hyg3 && mob3) {
                SetInd(3, "Feed Assist + Hygiene Assist + Mobility Assist");
            }
        //    } else {if (feed3 && mob3) {
        //        SetInd 3, "Feed Assist + Mobility Assist"
        //    } else {if (hyg3 && mob3) {
        //        SetInd 3, "Hygiene Assist + Mobility Assist"
    
            if (_inds[3].is_checked) return;

            // 2. ASSIST

            if (feed3 || feed4) SetInd(2, "Feed Assist");
            if (adl23 || adl4)  SetInd(2, "ADL assist by 2+");
            if (mob3 || mob4)   SetInd(2, "Mobility assist");
            if (hyg3 || hyg4)   SetInd(2, "Hygiene assist");
            SetIndIfResultContains(2, "", "ASSISTIVEDEVICE", "", "", "Cane,Crutches,walker,Gait belt,Hand held,lift,Sling,Wheelchair");
            SetIndIfResultContains(2, "", "30450102", "", "", "Cane,Crutches,walker,Gait belt,Hand held,lift,Sling,Wheelchair");
            
            //SetIndIfResultContains 2, "", "LOAMOBILITY", "", "", "Moderate assist,maximum assist"
            SetIndIfResultContains(2, "", "ACTCONFACTORS", "", "", "Impaired Mobility,Obesity,Restraints");
            SetIndIfResultContains(2, "", "ACTCONFACTORS", "", "", "Risk of Dislodging LDAs,Unable to Follow Commands,Other (Comment)");
            SetIndIfResultContains(2, "", "FEEDING", "", "", "Needs set up");
            //SetIndIfResultContains 2, "", "DIETTYPE", "", "", "Tube feeding"
            
            if (_inds[2].is_checked) return;
    
            // 1. SELF

            SetIndIfResultContains(1, "", "LOAMOBILITY", "", "", "Independent");
            SetIndIfResultContains(1, "", "LOAMOBILITY", "", "", "Minimal assist" + CHAR_COMMA + " patient does 75");

        }


        private void Check_5()
        {
            string reslist; 
            
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("5. ADL Rehab");
            Program.VerboseAudit("---------------");

            reslist = "Decreased knowledge of therapeutic intervention,Impaired bed mobility,Impaired transfer ability,Impaired gait,Impaired wheelchair mobility,Decreased ability to ascend/descend stairs,Decreased ability to negotiate curb,Decreased aerobic capacity,Decreased UE ROM,Decreased LE ROM,Decreased UE strength,Decreased LE strength,Decreased sensation,Decreased skin integrity,Decreased ADL status,Impaired neurodevelopment (comment),Abnormal tone,Impaired balance,Impaired judgement,Impaired cognition,Impaired vision,Impaired coordination,Increased pain,Increased risk for falls,Other (comment)";
            SetIndIfResultContains(5, "", "PTASSESSMENT", "", "", reslist);

            reslist = "Decreased ability to perform ADLs,Decreased knowledge self care,Decreased knowledge of therapeutic intervention,Decreased UE ROM,Decreased LE ROM,Decreased UE strength,Decreased LE strength,Decreased sensation,Decreased skin integrity,Impaired judgement,Impaired cognition,Impaired vision,Impaired coordination,Abnormal tone,Impaired neurodevelopment,Impaired feeding,Impaired balance,Impaired bed mobility,Impaired transfer mobility,Decreased aerobic capacity,Increased pain,Increased risk for falls";
            SetIndIfResultContains(5, "", "OTASSESSMENT", "", "", reslist);
            SetIndIfResultContains(5, "", "ACTIVITY", "", "", "Assistance with relearning ADL activities");

            SetIndIfResultContains(5, "", "ACTIVITY", "", "", "Assistance with relearning ADL activities");
            SetIndIfResultContains(5, "", "ACTIVITY", "", "", "Therapy consulted via MD order");
            SetIndIfResultContains(5, "", "LOAMOBILITY", "", "", "Assistance with re-learning ADL activities");
            SetIndIfResultContains(5, "", "FEEDING", "", "", "Able to feed self with adaptive devices");
        }


        private void Check_6_7()
        {    
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("6. ADL 2-3 Caregivers");
            Program.VerboseAudit("7. ADL 4 or more Caregivers");
            Program.VerboseAudit("---------------");

            if (adl4) SetIndIfResultContains(7, "", "ACTCONFACTORS", "", "", "Impaired Mobility,Obesity,Restraints,Risk of Dislodging LDA,Unable to follow commands,Other (comments)");
            if (_inds[7].is_checked) return;
            if (adl23) SetIndIfResultContains(6, "", "ACTCONFACTORS", "", "", "Impaired Mobility,Obesity,Restraints,Risk of Dislodging LDA,Unable to follow commands,Other (comments)");
            SetIndIfResultContains(6, "", "THERMOREGULATION", "", "", "ECMO");
            SetIndIfResultContains(6, "", "OXYGENDEVICE", "", "", "Oscillator");
            if (_pat.unit_name.Length >= 4)
            {
                if (_pat.unit_name.Substring(0, 4) == "NICU")
                {
                    SetIndIfResultContains(6, "", "SECUREDAT", "", "", "");
                }
            }
        }

        private void Check_8()
        {
            string reslist;
            string other_str;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("8. Communication");
            Program.VerboseAudit("---------------");

            if (ResultContains("", "LEVELOFCONSCIOUSNESS", "", "", "Unresponsive,Pharmaceutically")) return;

            if (ResultContains("", "RIGHTEYE", "", "", "Blind,(uncorrected)", SearchDepth.SearchSinceAdmission)) 
            {
                SetIndIfResultContains(8, "", "LEFTEYE", "", "", "Blind,(uncorrected)", SearchDepth.SearchSinceAdmission);
            }
            if (ResultContains("", "RIGHTEAR", "", "", "Deaf,(uncorrected)", SearchDepth.SearchSinceAdmission))
            {
                SetIndIfResultContains(8, "", "LEFTEAR", "", "", "Deaf,(uncorrected)", SearchDepth.SearchSinceAdmission);
            }
            SetIndIfResultContains(8, "", "BARRIERSCOMMUNICATION", "", "", "Vision,Hearing,Language,Literacy", SearchDepth.SearchSinceAdmission);
            GetResultforOther("", "BARRIERSCOMMUNICATION", "", "", out other_str, SearchDepth.SearchSinceAdmission);
            if (!String.Equals(other_str, ""))
            {
                SetInd(8, "Found Comm Barrier Other comment: " + other_str);
            }

            reslist = "Difficulty talking,trach";
            SetIndIfResultContains(8, "", "VOICE", "", "", reslist);
            //    reslist = "Slurred,Delayed responses,aphasia,Incomprehensible,Uses written communication"
        //    SetIndIfResultContains 8, "", "SPEECH", "", "", reslist
        //    reslist = "Sign Language,Communication board,Interpreter,Intubated"
        //    SetIndIfResultContains 8, "", "SPEECH", "", "", reslist
        //    reslist = "Trached,Nods/gestures appropriately,Augmentative Device"
        //    SetIndIfResultContains 8, "", "SPEECH", "", "", reslist
        //    reslist = "Difficulty talking,trach"

            if (_inds[8].is_checked) return;
            reslist = "Slurred,Delayed responses,aphasia,Incomprehensible,Uses written communication,";
            reslist += "Sign Language,Communication board,Interpreter,Intubated,";
            reslist += "Trached,Nods/gestures appropriately,Augmentative Device";
            // Note: check since admission
            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = AndItemFilter(query, "", "SPEECH", "", "", reslist);
            query = AndResultNotInList(query, "clear,appropriate for");
            if (query.Count() > 0)
            {
                string found_what = Describe(query, "", "SPEECH", "", "", reslist);
                SetInd(8, "Found SPEECH: " + found_what);
            }

        //    'if (ResultDoesNotContain(cat, code, desc, field, result_list, found_what, search_mode, false) {
        //    SetIndIfResultDoesNotContain 8, "", "LANGUAGE", "", "", "English,Primary Language"
        //    if (inds(8).is_checked) return
        }

        private void Check_9()
        {
            string reslist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("9. Cognitive Support");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(9, "", "ORIENTATION", "", "", "Disoriented");
            
            reslist = "Poor judgement,Poor safety awareness,Poor attention / concentration,Unable to follow commands";
            SetIndIfResultContains(9, "", "COGNITION", "", "", reslist);

            if (_inds[9].is_checked) return;

            if (! ResultContains("", "COGNITION", "", "", "No short term memory loss")) {
                if (ResultContains("", "COGNITION", "", "", "short term memory loss")) {
                    SetInd(9, "Found Short term memory loss.");
                }
            }
        }

        private void Check_10_11()
        {
            string reslist, found_what;
            int ct;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("10. Behavior/Emotional Management");
            Program.VerboseAudit("11. Behavior/Emotional Mgmt - q 1 Hour");
            Program.VerboseAudit("---------------");

            SetIndIfResultDoesNotContain(10, "", "INFANTBEHAVIOR", "", "", "Sleeping,Other (comm");
    
            if (GetResult("", "NASTOTALSCORE", "", "", out found_what)) {
                if (found_what.IsNumeric()) {
                    if (found_what.ToInteger() >= 8) {
                        SetInd(11, "NAS Total Score=" + found_what);
                    } else {
                        SetInd(10, "NAS Total Score=" + found_what);
                    }
                }
            }
            if (_inds[11].is_checked) return;

            if (!HasCalmCooperative("PATIENTBEHAVIORS"))
            {
                //suppress PATIENTBEHAVIORS from triggereing
                reslist = "Aggressive physically,Aggressive verbally,Depressed affect,Hallucinations,";
                reslist += "Homicidal,Hyperactivity,Non -consolable,not interactive,";
                reslist += "Suicidal,Tearful,Uncooperative,Withdrawn";
                ct = ReturnQ1HrCount("PATIENTBEHAVIORS", reslist);
                if (12 / _pat.los_hours * ct >= 7)
                {
                    SetInd(11, "Patient Behavior count of " + ct + " in a range of " + Math.Round(_pat.los_hours) + " hours");
                }
            }

            if (_inds[11].is_checked) return;

            //if (ResultContains("", "OBSERVEDFEELINGS,PATIENTBEHAVIORS,FAMILYBEHAVIORS,VISITORBEHAVIORS", "", "", "calm,appropriate for age,appropriate for situation"))
            //{
            //    return;
            //}
            //if (ResultContains("", "OBSERVEDFEELINGS,PATIENTBEHAVIORS,FAMILYBEHAVIORS,VISITORBEHAVIORS", "", "", EXACT_MATCH_PREFIX + "cooperative"))
            //{
            //    return;
            //}

            if (!HasCalmCooperative("OBSERVEDFEELINGS"))
            {
                //suppress OBSERVEDFEELINGS from triggereing
                reslist = "anger,agitated,apathetic,combative,confused,denial,fearful,frustrated,hopeless,irritable,pessimistic,stressed,tearful,unrealistic";
                SetIndIfResultContains(10, "", "OBSERVEDFEELINGS", "", "", reslist);
                if (_inds[10].is_checked) return;
            }

            reslist = "Aggressive physically,Aggressive verbally,Depressed affect,Hallucinations,Homicidal,Hyperactivity,Non -consolable,not interactive,Suicidal,Tearful,Uncooperative,Withdrawn";
            if (!HasCalmCooperative("PATIENTBEHAVIORS"))
            {
                //suppress PATIENTBEHAVIORS from triggereing
                SetIndIfResultContains(10, "", "PATIENTBEHAVIORS", "", "", reslist);
            }
            if (_inds[10].is_checked) return;
            if (!HasCalmCooperative("FAMILYBEHAVIORS"))
            {
                SetIndIfResultContains(10, "", "FAMILYBEHAVIORS", "", "", reslist);
            }

            if (_inds[10].is_checked) return;
            if (!HasCalmCooperative("VISITORBEHAVIORS"))
            {
                SetIndIfResultContains(10, "", "VISITORBEHAVIORS", "", "", reslist);
            }
            if (_inds[10].is_checked) return;
            
            SetIndIfResultContains(10, "", "PRECAUTIONS", "", "", "Suicide");
        }

        private bool HasCalmCooperative(string codelist)
        {
            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = AndCodeInList(query, codelist);
            query = AndResultInList(query, "calm,cooperative");
            query = AndResultNotInList(query, "uncooperative");

            return (query.Count() > 0);

        }

        // (this isn't really a Q1h count -- it is just a count) Make this a q1hr count
        private int ReturnQ1HrCount(string code_list, string result_list)
        {
            var buckets = new List<int>();
            SetBucketSize(60);
            AddBuckets(buckets, "", code_list, "", "", result_list);
            return CountBuckets(buckets);
        }

        private bool IsQ1Hour(int count)
        {
            return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q1H);
        }

        // Use this if counting safety buckets
        //private void CheckSafety(int count, string desc)
        //{
        //    if (_inds[13].is_checked) return;             //skip if highest already checked
        //    if (count == 0) return;
        //    switch (FreqForCount(_pat.los_hours, count))
        //    {
        //    case Frequencies.Q30M:
        //        SetInd(13, desc + " q30min");
        //        break;
        //    case Frequencies.Q1H:
        //    case Frequencies.Q2H:
        //        SetInd(12, desc + " q2h");
        //        break;
        //    default:
        //        Program.VerboseAudit(desc + ": " + count + " in " + Math.Round(_pat.los_hours) + " hours is not enough");
        //        break;
        //    }
        //}

        private void Check_12_13()
        {
            string found_what;
            string reslist;
            bool suppress13=false;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("12. Safety Management - q 2 Hours");
            Program.VerboseAudit("13. Safety Management - q 30 Minutes");
            Program.VerboseAudit("---------------");

            reslist = "Initiated,Continued";
            if (ResultContains("", "SITTER", "", "", reslist))  {
                suppress13=true;
                Program.VerboseAudit("Safety q30 (indicator 13) will be suppressed due to Sitter.");
            }


            if (! suppress13) {
                SetIndIfResultContains(13, "", "RESTRAINTBEHAVIORV", "", "", "No");
                reslist = "Start,Continued";
                if (_inds[13].is_checked) return;
                SetIndIfResultContains(13, "", "VIOLENTMITTRT", "", "", reslist);
                SetIndIfResultContains(13, "", "VIOLENTMITTLFT", "", "", reslist);
                SetIndIfResultContains(13, "", "VIOLENTWRISTRT", "", "", reslist);
                if (_inds[13].is_checked) return;
                SetIndIfResultContains(13, "", "VIOLENTWRISTLFT", "", "", reslist);
                SetIndIfResultContains(13, "", "VIOLENTANKLERT", "", "", reslist);
                SetIndIfResultContains(13, "", "VIOLENTANKLELFT", "", "", reslist);
                SetIndIfResultContains(13, "", "VIOLENTROLLBELT", "", "", reslist);
                SetIndIfResultContains(13, "", "VIOLENTVEST", "", "", reslist);
                if (_inds[13].is_checked) return;
                SetIndIfResultContains(13, "", "VIOLENTSECLUSION", "", "", reslist);
                SetIndIfResultContains(13, "", "VIOLENTLAPWAIST", "", "", reslist);
                SetIndIfResultContains(13, "", "VIOLENTCHEST", "", "", reslist);
                SetIndIfResultContains(13, "", "VIOLENTFOURWAY", "", "", reslist);
                if (_inds[13].is_checked) return;
                SetIndIfResultContains(13, "", "VIOLENTOTHER", "", "", reslist);
                SetIndIfResultContains(13, "", "VIOLENTPHYSICALHOLD", "", "", reslist);
                
                if (_inds[13].is_checked) return;
            }

            reslist = "Start,Continued";
            SetIndIfResultContains(12, "", "NONVIOLENTENCLOSUREBED", "", "", reslist);
            reslist = "Adjusting CPAP prongs,Adjusting bilimask,adjusting lines";
            if (ResultContains("", "FREQUENTINTERVENTIONS", "", "", reslist, SearchDepth.SearchDefault, false, out found_what)) {
                if (! suppress13)  SetIndIfResultContains(13, "", "FREQUENTINTERVENTIONS", "", "", "at least every 30");
                if (!_inds[13].is_checked) {
                    SetInd(12, "Frequent interventions: " + found_what);
                }
            } else {
                Program.VerboseAudit(found_what);               // say what was not found
            }
            
            if (_inds[13].is_checked) return;
            if (_inds[12].is_checked) return;

            SetIndIfResultContains(12, "", "OVERRIDEFALLFLOWSHEET", "", "", ""); //new Jun7 2021.
            if (GetResult("", "FALLRISKSCORE", "", "", out found_what)) {
                if (found_what.IsNumeric()) {
                    if (found_what.ToInteger() >= 51) {
                        SetInd(12, "Fall Risk Score=" + found_what);
                    }
                }
            }

            if (morse >= 45) SetInd(12, "Morse Score=" + morse);
            if (_inds[12].is_checked) return;
            if (GetResult("", "GRAFPIFSCORE", "", "", out found_what)) {
                if (found_what.IsNumeric()) {
                    if (found_what.ToInteger() >= 2) {
                        SetInd(12, "GRAF-PIF Score=" + found_what);
                    }
                }
            }

            if (_inds[12].is_checked) return;
            //reslist = "Aspiration,Bleeding,Critical airway,Fracture,Fragile Bones,Latex,Neutropenic,Precaution Sign Posted,Reflux,Shunt,Silo,Sternal,Other (Comment)";
            //SetIndIfResultContains(12, "", "NICUPRECAUTIONS", "", "", reslist);
            //if (_inds[12].is_checked) return;

            SetIndIfResultContains(12, "", "PRECAUTIONS", "", "", "Assault,Elopement,Suicide,Victim of violence");
            SetIndIfResultContains(12, "", "PEDSPRECAUTIONS", "", "", "Assault,Elopement,Suicide,Victim of violence");
            //SetIndIfResultContains(12, "", "PRECAUTIONS", "", "", "Critical Airway,Elopement,Epidural,Extremity,Latex,Pica");
            //SetIndIfResultContains(12, "", "PRECAUTIONS", "", "", "Seizure,Spinal,sternal,Open sternal,Suicide,Victim of violence,Other (Comment)");
            
            if (_inds[12].is_checked) return;
            
            SetIndIfResultContains(12, "", "RESTRAINTBEHAVIORNV", "", "", "No");
            reslist = "Start,Continued";
            SetIndIfResultContains(12, "", "NONVIOLENTMITTRT", "", "", reslist);
            SetIndIfResultContains(12, "", "NONVIOLENTMITTLFT", "", "", reslist);
            SetIndIfResultContains(12, "", "NONVIOLENTWRISTRT", "", "", reslist);
            if (_inds[12].is_checked) return;
            SetIndIfResultContains(12, "", "NONVIOLENTWRISTLFT", "", "", reslist);
            SetIndIfResultContains(12, "", "NONVIOLENTANKLERT", "", "", reslist);
            SetIndIfResultContains(12, "", "NONVIOLENTANKLELFT", "", "", reslist);
            SetIndIfResultContains(12, "", "NONVIOLENTVEST", "", "", reslist);
            if (_inds[12].is_checked) return;
            SetIndIfResultContains(12, "", "NONVIOLENTLAPBELT", "", "", reslist);
            SetIndIfResultContains(12, "", "NONVIOLENTROLLBELT", "", "", reslist);
            SetIndIfResultContains(12, "", "GERICHAIR", "", "", reslist);
            SetIndIfResultContains(12, "", "LOCKEDLIMB", "", "", reslist);
            if (_inds[12].is_checked) return;
            SetIndIfResultContains(12, "", "UNIVERSALCHAIR", "", "", reslist);
            SetIndIfResultContains(12, "", "UNIVERSALBED", "", "", reslist);
            SetIndIfResultContains(12, "", "NONVIOLENTOTHER", "", "", reslist);
    
        }

        private void Check_14()
        {    
            string reslist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("14. Isolation");
            Program.VerboseAudit("---------------");
            
            // changed to "since arrival" 3/29
            reslist = "Contact,Enhanced contact,Droplet,Enteric,Airborne,Strict,Special,Neutropenic,Radiation,Compromised host";
            SetIndIfResultContains(14, "", "ISOLATIONPRECAUTIONS", "", "", reslist);
        }

        private void CheckAssessment(int count, string desc)
        {
            if (_inds[18].is_checked) return;          //skip if highest already checked
            if (count == 0) return;                    //skip if none
            
            // This should work the same as the original code:
            switch (FreqForCount(_pat.los_hours, count)) {
                case Frequencies.Q30M:
                    SetInd(18, desc);
                    break;
                case Frequencies.Q1H:
                    SetInd(17, desc);
                    break;
                case Frequencies.Q2H:
                    SetInd(16, desc);
                    break;
                case Frequencies.Q4H:
                    SetInd(15, desc);
                    break;
                default:
                    break;
            }

        }

        private void Check_15_16_17_18()
        {
            string codelist;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("15. Assessment q4h");
            Program.VerboseAudit("16. Assessment q2h");
            Program.VerboseAudit("17. Assessment q1h");
            Program.VerboseAudit("18. Assessment q30min");
            Program.VerboseAudit("---------------");
            
            //direct triggers for NICU
            SetIndIfResultContains(18, "", "ADDITIONALINFANTMONITORING", "", "", "Cooling");
            SetIndIfResultContains(18, "", "VENTNOPPMOBS", "", "", "");
            if (ResultContains("", EXACT_MATCH_PREFIX + "LVAD", "", "", "Heartmate II,Heartware,CentriMag"))
            {
                if (ResultContains("", "LVADFLOW", "", "", ""))
                {
                    codelist = "VASOPRESSIN, NOREPINEPRINE, DOPAMINE, EPINEPHRINE, PROPOFOL, LABETALOL, PHENYLEPHRINE";
                    if (ResultContains("", codelist, "", "", ""))
                    {
                        SetInd(18,"LVAD + LVADFLOW + specific med");
                        SetInd(24,"LVAD + LVADFLOW + specific med");
                    }
                }
            }
            if (ResultContains("", EXACT_MATCH_PREFIX + "RVAD", "", "", "Heartmate II,Heartware,CentriMag"))
            {
                if (ResultContains("", "RVADFLOW", "", "", ""))
                {
                    codelist = "VASOPRESSIN, NOREPINEPRINE, DOPAMINE, EPINEPHRINE, PROPOFOL, LABETALOL, PHENYLEPHRINE";
                    if (ResultContains("", codelist, "", "", ""))
                    {
                        SetInd(18,"LVAD + LVADFLOW + specific med");
                        SetInd(24,"RVAD + RVADFLOW + specific med");
                    }
                }
            }

            
            if (_inds[18].is_checked) return;
            SetIndIfResultContains(17, "", "APNEA", "", "", "Yes,No");
            SetIndIfResultContains(17, "", "BRADYCARDIA", "", "", "");
            SetIndIfResultContains(17, "", "PEDSFEEDINGROUTE", "", "", "Continuous");
            
            //direct trigger for minimum Q2
            SetIndIfResultContains(16, "", "LVAD,RVAD", "", "", "");

            // ct events q 12 hours - count distinct event times!
            // select count(distinct(times)) where eventcode in ()
            //select min(time) where code in () and time>=pull-24
            //add 12 hours
            //select count(distinct(times)) where code in () and time between min and min+12
            //returnmaxcount(codeslist,n) in the past n hours
            //3=15, 6=16, 12=17, 24=18

            //if (IsSpecialAssessUnit()) {
            //    CountAssessments(30);               // special q30 buckets
            //} else {
            //    CountAssessments(1);                // tiny buckets = count distinct times
            //}
            CountAssessments(30);               // always do q30 buckets--but use different freq tables for icu vs non-icu.

        }

        //private bool IsSpecialAssessUnit()
        //{
        //    //These are the units at Shands to put the assessments into a bucket where there is only a count of 1 q 30 minutes,
        //    //regardless of the number of assessments completed - so all pain assessments in a 30 minute time frame = 1,
        //    //all pulmonary assessments in a 30 minute time frame, all cardiac assessments in a 30 minute time frame - and so on!
        //    //
        //    //55, 54, 65, 64, 75, 74, 95nt,95,35,44,45,42,6w,6e,5w,8e, 11ms, 10-5
        //    //155,115,100,170,190,130,7197814, 95? , 35? ,7197814,7112825,7112819, 7112816 , 220 ,7112826,7112756,2511461,7112818
        //    if (_pat.is_ICU) return true;
        //    switch(_pat.unit_id) {
        //        case 155: case 115: case 100: case 170: case 190: case 130: case 380: case 350: 
        //        case 14047404: case 7197814: case 7112825: case 7112819: case 7112816: 
        //        case 220: case 7112826: case 7112756: case 2511461: case 7112818:
        //            return true;
        //        default:
        //            return false;
        //    }
                
                    
        //    //name             unit_id
        //    //---------------- -----------
        //    //SICU 1
        //    //65 MS 100
        //    //54 MS 115
        //    //74 MS 130
        //    //55 MS 155
        //    //64 MS 170
        //    //75 MS 190
        //    //UA6E 220
        //    //MICU 235
        //    //25 IC 250
        //    //NICU 2           265
        //    //NICU 3           280
        //    //BICU 305
        //    //BMTU 350
        //    //94 IMC 365
        //    //MBU 380
        //    //4 PEDS 395
        //    //PICU 410
        //    //52 PY 435
        //    //Vista South      450
        //    //old Vista West   480
        //    //old Vista East   495
        //    //Other 496
        //    //MSS AGH          497
        //    //Rehab Nursing    514682
        //    //oldVista North   515047
        //    //MSO AGH          531947
        //    //MS AGH           531962
        //    //CARD AGH         531982
        //    //SACC AGH         532002
        //    //IMC AGH          532017
        //    //SICU AGH         532032
        //    //AICU AGH         532047
        //    //MBU AGH          532062
        //    //NICU AGH         532077
        //    //Vista West       532642
        //    //Vista East       532657
        //    //Vista North      532672
        //    //Trauma 1504622
        //    //TEST AGH         1698032
        //    //Other 1752602
        //    //11 MS 2511461
        //    //10 SI 3232907
        //    //45 MS 3232908
        //    //PEDS 3355356
        //    //PEDS 3355376
        //    //PEDs 7           3355391
        //    //PICU 3355406
        //    //LD AGH           4501805
        //    //Test MBU @Shands 4504840
        //    //Test LD Shands A 4504842
        //    //82 NS 5216839
        //    //CARD 5 AGH       5278326
        //    //L&D              6576074
        //    //8E               7112756
        //    //44 PEDS 7112757
        //    //4E               7112758
        //    //4 W 7112759
        //    //5E               7112815
        //    //6 W 7112816
        //    //11 5             7112817
        //    //10 5             7112818
        //    //42 PEDS 7112819
        //    //45 PEDS 7112825
        //    //5 W 7112826
        //    //24 IC 7112827
        //    //NT95 7197814
        //    //Transparent 12546789
        //    //32 MS 14047404

        //}

        // Count number of buckets; similar assessments in the same bucket count as one
        //
        private void CountAssessments(int bucket_size)
        {
            int ct;
            string codelist;
            List<int> buckets;

            SetBucketSize(bucket_size);

            buckets = new List<int>();
            codelist = "CORETEMP,TEMPERATURE,TEMPSOURCE";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Temperature=" + ct);
            if (_inds[18].is_checked) return;

            buckets = new List<int>();
            codelist = "ORALNASALETCO2,RESPIRATION,SPO22PREDUCTAL";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Pulmonary=" + ct);
            if (_inds[18].is_checked) return;

            buckets = new List<int>();
            codelist = "HEARTRATE,BLOODPRESSURE2,HEARTRATEMONITOR,BP,MAP2,BLOODPRESSURE3,BLOODPRESSURE4,OBFHRDOPPLER,LVAD";
            codelist += ",ARTLINEBP,ARTLINEBP2,ARTLINEBP3,IABFREQUENCY,SWANGANZ,OBFHRDOPPLER,DIASTOLIC_BP,SYSTOLIC_BP";
            codelist += ",CBIFOLEYOUTPUT,PRNREASON,PRNASSESSINTERVENTIONS,PRNRESPONSEINTERVENTIONS";
            codelist += ",LEFTEJECTIONBERLINHEART,LCOTAH,PURGEFLOWRATEIMPELLA";
            //removed CARDRHYTHM for
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Cardiac=" + ct);
            if (_inds[18].is_checked) return;
            



            buckets = new List<int>();
            codelist = "CARDRHYTHM";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "CardRhythm=" + ct);
            if (_inds[18].is_checked) return;

            buckets = new List<int>();
            codelist = "BLADDERPRESSURE,ABDOMGIRTH,BLADDERSCAN";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Intra-Abdominal=" + ct);
            if (_inds[18].is_checked) return;

            buckets = new List<int>();
            codelist = "CATHSITECHECK,EPIDURALSITEASSESS,DOPPFLAPCHECK";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Wound=" + ct);
            if (_inds[18].is_checked) return;

            buckets = new List<int>();
            codelist = "BISRANGE,TRAINOFFOUR,CHARTINGTYPE,EEGMONITORINGTYPE";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Neuro=" + ct);
            if (_inds[18].is_checked) return;

            buckets = new List<int>();
            codelist = "DVPRSPAINSCALE,ANVPSPAINSCORE,PAINSCALE0TO10,NIPSSCORE,NPASSSCORE,PEDSPAINASSESSFLACC,PAINWONGBAKERFACESSCORE";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Pain=" + ct);
            if (_inds[18].is_checked) return;

                        buckets = new List<int>();
                        codelist = "WAT1TOTAL";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Withdrawal=" + ct);
            if (_inds[18].is_checked) return;

                        buckets = new List<int>();
                        codelist = "SNAKEBITESITEA";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Snake Bite=" + ct);

        }

        //=====================================================================
        // Group like assessments into buckets of time
        // Each bucket counts as one assessment in the frequency count

        private void SetBucketSize(int minutes)
        {
            _bucket_size = minutes;
        }

        //Add to the list of bucket numbers (redundant buckets are fine)
        private void AddBuckets(List<int> bucket_list, string cat, string code_list, string desc, string field)
        {
            AddBuckets(bucket_list, cat, code_list, desc, field, "");
        }
        private void AddBuckets(List<int> bucket_list, string cat, string code_list, string desc, string field, string result_list)
        {
            // Assign (virtual) bucket numbers to each assessment.  
            // Every x minutes after that is a new bucket.  Bucket zero = pull_start time.

            // get the chart items for the assessments
            var query = StartNewQuery();
            query = AndItemFilter(query, cat, code_list, desc, field, result_list);

            // This step is needed for those who want to count the # within a bucket, like Shands #24.
            // This will have no effect on those that count buckets.
            var query2 = (from item in query select new { item.EVENT_DATETIME }).Distinct();

            // figure out what buckets the events belong to
            var query3 = from item in query2
                         select new {
                            bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size)
                         };
            // Add to the list
            foreach (var item in query3) {
                bucket_list.Add(item.bucket);
            }

            // This is just for auditing: Say what we are looking for and how many were found
            if (!String.IsNullOrEmpty(result_list)) {
                // print each word and if it was found or not
                int i = CountResultContains(cat, code_list, desc, field, result_list);
            } else {
                // print how many were found
                Program.VerboseAudit(Describe(cat, code_list, desc, field, ""));
            }
        }

        private int CountBuckets(List<int> bucket_list)       // bucket list: (ha ha)
        {
            int result = bucket_list.Distinct().Count();
            if (result > 0) Program.VerboseAudit(result + " unique");
            return result;
        }


        private void Check_19()
        {
            string codelist;
            int ct;
            string found_what;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("19. Vascular Access Site Mgt q1 Hour");
            Program.VerboseAudit("---------------");

            if (!ResultContains("", "PRECAUTIONS", "", "", "Cytotoxic")) return;  //this indicator requires cytotoxic 2/5/14

            codelist = CODE_LIKE_PREFIX + "LINESITEASSESS";
            ct = ReturnQ1HrCount(codelist, "");
            if  (12 / _pat.los_hours * ct >= 6) {
                SetInd(19, "Line Site count of " + ct + " in a range of " + Math.Round(_pat.los_hours) + " hours.");
            }

        //added 4/5 Vascular triggered when:
        //age < 17 and under with periph iv
            if (_pat.age >= 17) return;
            
            if (_pat.unit_name.Substring(0,4) == "NICU") {
                if (ResultContains("", "LINESTATUS", "", "", "infusing", SearchDepth.SearchDefault, false, out found_what)) {
                    SetInd(17, "NICU: Line Status Infusing" + found_what);
                    SetInd(19, "NICU: Line Status Infusing" + found_what);
                }
            }
            //BELOW DRSGINTERVENTION and DRSGSTATUS commented out 2/5/14.
            //codelist = CODE_LIKE_PREFIX + "DRSGINTERVENTION";
            //if (ResultContains("", codelist, "", "", "", SearchDepth.SearchDefault, false, out found_what)) {
            //    Program.VerboseAudit(found_what);
            //    if (!String.IsNullOrEmpty(found_what) && found_what.Contains("peripheral iv")) {
            //        SetInd(19, "Age<17: " + found_what);
            //    }
            //}
            
            //codelist = CODE_LIKE_PREFIX + "DRSGSTATUS";
            //if (ResultContains("", codelist, "", "", "", SearchDepth.SearchDefault, false, out found_what)) {
            //    Program.VerboseAudit(found_what);
            //    if (!String.IsNullOrEmpty(found_what) && found_what.Contains("peripheral iv")) {
            //        SetInd(19, "Age<17: " + found_what);
            //    }
            //}

        //    ct = ReturnAssessCount(codelist)
        //    if (g_range = 1440) {
        //        if (ct >= 12) { SetInd 19, "LINESITEASSESS Count=" + ct + " for 24 hours."
        //    } else {if (g_range = 600) {
        //        if (ct >= 5) { SetInd 19, "LINESITEASSESS Count=" + ct + " for 10 hours."
        //    } else {if (g_range = 240) {
        //        if (ct >= 2) { SetInd 19, "LINESITEASSESS Count=" + ct + " for 4 hours."
        //    }
            
        //    SetIndIfFound 19, "", CODE_LIKE_PREFIX + "LINESITEASSESS"
        //for (24 hours, count is 12+; for 4 hour pull 2+; for 9 hour pull 5+
        }

        private void Check_20()
        {
            string medlist;
            
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("20. Medication Activity >= 20 minutes");
            Program.VerboseAudit("---------------");
            
            SetIndIfResultContains(20, "", "PATIENTBEHAVIORS", "", "", "Difficulty taking medications");
            SetIndIfResultContains(20, "", "THROAT", "", "", "swallow");
            if (tubefeed) SetInd(20, "Tube Feeding");
            SetIndIfResultContains(20, "", "PRECAUTIONS", "", "", "Cytotoxic");
            SetIndIfResultContains(20, "", "PEDSPRECAUTIONS", "", "", "Cytotoxic");
            if (_inds[20].is_checked) return;
            medlist = "ABCIXIMABVOLUME,AMIODARONEVOLUME,ARGATROBANVOLUME,ATRACURIUMVOLUME,BIVALIRUDINVOLUME,DEXMEDETOMIDINEVOLUME,";
            medlist += "DILTIAZEMVOLUME,DOBUTAMINEVOLUME,DOPAMINEVOLUME,EPIDURALINTAKE,EPINEPHRINEVOLUME,EPOPROSTENOLVOLUME,";
            medlist += "EPTIFIBATIDEVOLUME,ESMOLOLVOLUME,FENTANYLVOLUME,HEPARINVOLUME,HYDROMORPHONEVOLUME,INSULINVOLUME,";
            medlist += "ISOPROTERENOLVOLUME,KETAMINEVOLUME,LABETALOLVOLUME,LIDOCAINEVOLUME,LORAZEPAMVOLUME,MAGNESIUMVOLUME,";
            medlist += "MIDAZOLAMVOLUME,MILRINONEVOLUME,NESIRITIDEVOLUME,NITROGLYCERINVOLUME,NITROPRUSSIDEVOLUME,NOREPINEPHRINEVOLUME,";
            medlist += "OXYTOCINVOLUME,PANCURONIUMVOLUME,PHENYLEPHRINEVOLUME,PROCAINAMIDEVOLUME,PROPOFOLVOLUME,TIROFIBANVOLUME,";
            medlist += "TREPROSTINILVOLUME,VASOPRESSINVOLUME,VECURONIUMVOLUME,TPNVOLUME,BLOODPRODUCTVOLUME,ALTEPLASEVOLUME";
            SetIndIfResultContains(20, "", medlist, "", "", "");
        }

        // Use this if you can total the wound care time
        //private void CheckWound(int total)
        //{
        //    if (_inds[22].is_checked) return;             //skip if highest already checked
        //    if (total == 0) {
        //        //skip it
        //    } else if (total >= 30) {
        //        SetInd(22, "wound >= 30 min");
        //    } else {
        //        SetInd(21, "wound < 30 min");
        //    }
        //}

        private void Check_21_22()
        {
            string codelist;
            
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("21. Wound/Injury Mgmt");
            Program.VerboseAudit("22. Wound/Injury Mgmt >= 30 Minutes");
            Program.VerboseAudit("---------------");
            
            CheckNICUSkinIntegrity();

            if (ResultContains("", "TOTALTIMEREQWOUNDCARE", "", "", "Greater than or equal to 30"))
            {
                SetIndIfFound(22, "", "CONFACWNDCARETIME");
            }
            if (_inds[22].is_checked) return;
            
            CheckDressing();
            if (_inds[21].is_checked) return;
            
            codelist = "OBFUNDUS,CATHSITECHECK";
            SetIndIfFound(21, "", codelist);
            if (_inds[21].is_checked) return;
            
            codelist = CODE_LIKE_PREFIX + "WOUNDSITECLOSURE";
            SetIndIfFound(21, "", codelist);
            
        //    codelist = CODE_LIKE_PREFIX + "DRSGINTERVENTION"
        //    SetIndIfFound 21, "", codelist
        //    codelist = CODE_LIKE_PREFIX + "DRSGSTATUS"
        //    SetIndIfFound 21, "", codelist
            codelist = CODE_LIKE_PREFIX + "OSTOMYSITEASSESS";
            SetIndIfFound(21, "", codelist);
            if (_inds[21].is_checked) return;
            
            CheckSkinIntegrity();
            if (_inds[21].is_checked) return;

            codelist = "OBNEWBORNUMBCARE,OBNEWBORNCIRCCARE,OBPERIAPPEAR,CBIRRIGANT";
            SetIndIfFound(21, "", codelist);

            //DRSGINTERVENTION& 938
            //DRSGSTATUS& 1004
            //  DOPPFLAPCHECK& 3091
            //  EPIDURALSITEASSESS& 1001
            //  LINESITEASSESS& 100
            //OSTOMYSITEASSESS& 1006
            //WOUNDSITECLOSURE& 1023
        }

        private void CheckNICUSkinIntegrity()
        {
            string codelist = "NEWBORNSKININTEGRITY";
            string reslist = "Abrasion, elastic turgor, melanocytic nevus, stork bite, Blister," +
                "Electrode mark, Excoriation, Forceps mark, Hemorrhoids, Infiltration, Laceration," +
                "Newborn Rash, Petechia, Rash, Redness, Swelling, Tear, Other (comment), Weeping";
            // Note: check since admission
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = AndItemFilter(query,"",codelist,"","",reslist);
            query = AndResultNotInList(query, "intact");
            if (query.Count() > 0) {
                string found_what = Describe(query, "", codelist, "", "", reslist);
                SetInd(21, "Found (without intact): " + found_what);
            }
        }

        private void CheckSkinIntegrity()
        {
            string codelist = "SKININTEGRITY";
            string reslist = "Abrasion, Burn, Denuded, Electrode mark, Erythema, Excoriation, Fragile," +
                "Hematoma, Indurated, Laceration, Lesion, Macerated, Petechia, Rash, Redness, Tear, Weeping";
            // Note: check since admission
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = AndItemFilter(query,"",codelist,"","",reslist);
            query = AndResultNotInList(query, "intact");
            if (query.Count() > 0) {
                string found_what = Describe(query, "", codelist, "", "", reslist);
                SetInd(21, "Found (without intact): " + found_what);
            }
        }

        private void CheckDressing()
        {
            string codelist = "DRSGINTERVENTION";
            string desclist =
                "Single Lumen, Double Lumen, Triple Lumen, Quadruple Lumen" +      // LDA%lumen
                "chamber port, LDA art line, LDA hemodialysis cath," +
                "LDA intraosseous line, LDA introducer, LDA neuraxial catheter, LDA pa catheter," +
                "LDA paravertebral catheter, LDA perineural catheter, LDA neuraxial catheter," +
                "LDA SH IP, LDA sub q catheter, LDA transthoracic line, LDA umbilical artery cath," +
                "OB LDA epidural catheter";
            // Note: check since admission
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = AndItemFilter(query, "", codelist, desclist, "", "");
            if (query.Count() > 0) {
                string found_what = Describe(query, "", codelist, desclist, "", "");
                SetInd(21, found_what);
            }
        }

        // Use this if you can total the education time
        //private void CheckEducation(int total)
        //{
        //    if (_inds[23].is_checked) return;             //skip if already checked

        //    if (total >= 60) {
        //        SetInd(23, "education >= 60 min");
        //    }
        //}

        private void Check_23()
            {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("23. Healthcare Mgmt Education >= 1 Hour");
            Program.VerboseAudit("---------------");

            if (ResultContains("", "EXTDCTEACHLIFESTYLECHANGE", "", "", "Yes"))
            {
                SetIndIfResultContains(23, "", "EDPERTAINTO", "", "", "1,2,3");
            }
        }

        private void Check_24()
        {
            string s;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("24. 1 to 1 Physiological Interv. >= 2 Hours");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(24, "", "ECMOBYPASSHOUR", "", "", "");
            SetIndIfResultContains(24, "", "VENTNOPPMOBS", "", "", "");

            if (_inds[24].is_checked) return;
            if (!_pat.is_ICU) return;

            string codelist = "CORETEMP,TEMPERATURE,";
            codelist += "RESPIRATION,SPO22PREDUCTAL,";
            codelist += "HEARTRATE,BLOODPRESSURE2,HEARTRATEMONITOR,BP,MAP2,BLOODPRESSURE3,BLOODPRESSURE4,OBFHRDOPPLER,";
            codelist += "ARTLINEBP,ARTLINEBP2,ARTLINEBP3,IABFREQUENCY,SWANGANZ,CARDRHYTHM";

            SetBucketSize(60);                                  // set one hour buckets
            var buckets = new List<int>();                      // list of bucket numbers (for each match)
            AddBuckets(buckets, "", codelist, "", "", "");
            // Instead of counting the number of buckets to get the frequency,
            // we want to see if any two hours in a row had 9 or more assessments in the bucket.
            // Make a query that returns each bucket number and its item count.
            var query = buckets.GroupBy(x => x)                 // group by the hour (x is an int)
                               .Select(g => new { Hour = g.Key, Count = g.Count() })
                               .OrderBy(e => e.Hour);
            var arr = query.ToArray();

            s = "";
            foreach (var e in arr) {
                s += ",(" + e.Hour + "," + e.Count + ")";
            }
            if (s.Left(1) == ",") s = s.Substring(1);
            Program.VerboseAudit("VS hourly counts for 1:1 Phys Interv: " + s);

            for (int i = 0; (i < arr.GetUpperBound(0)); i++) {
                // Are these two hours in a row? (beware missing hours)
                if (arr[i].Hour == arr[i + 1].Hour - 1) {
                    // Add these two consecutive hours
                    if (arr[i].Count + arr[i+1].Count >= 9) {
                        SetInd(24, "9 or more VS in 2 hours.");
                        break;
                    }
                }
            }

            if (query.Count() == 0) {
                Program.VerboseAudit("No documentation found for VS.");
            }
        }


        private void AtLeastOneADL()
        {
            Program.Audit("---------------");
            if (!(_inds[1].is_checked || _inds[2].is_checked || _inds[3].is_checked || _inds[4].is_checked)) {
                // Make "#2 ADL - Assist" the default.  (90% of patients)
                SetInd(2, "Defaulting to partial care due to lack of documentation.");
            }
        }

        private void HighestIndicatorInEachGroupWins()
        {
            int i, g;
            bool highest_is_on;
            string ind_list;

            Program.Audit("Select highest indicator in each group");
            
            g = 0;
            highest_is_on = false;
            // Go from bottom up - highest (lowest) indicator in each group wins
            for (i = MAX_INDS; (i >= 1); i--) {
                if (_inds[i].radio_group > 0) {
                    if (_inds[i].radio_group != g) {
                        //this is a new group
                        g = _inds[i].radio_group;
                        highest_is_on = _inds[i].is_checked;
                    } else {
                        //same group
                        if (highest_is_on) {
                            _inds[i].is_checked = false;             //uncheck a lower number
                        } else {
                            highest_is_on = _inds[i].is_checked;     //save this one
                        }
                    }
                }
            } // next i
            
            //Echo the indicators for an Audit (no classification will be saved)
            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++) {
                if (_inds[i].is_checked) ind_list += "," + i;
            } // next i

            Program.Audit("Final list = " + ind_list.Substring(1));
            Program.Audit("---------------");
        }


        private void CheckProcs()
        {
            CheckProc_1();
            CheckProc_2();
            CheckProc_3();
            CheckProc_4();
            CheckProc_5();
            CheckProc_6();
            CheckProc_7();
            CheckProc_9();
            CheckProc_10();
            CheckProc_11();
        }

        private void CheckProc_1()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P1. 1-1 safety observation by RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_2()
        {
            string nowstr;
            string toddtstr;
            string yesdtstr;
            string timea ="";
            DateTime timea_startdt, timea_enddt;
            DateTime timeb_startdt, timeb_enddt;
            DateTime nowdt = _pat.pull_finish;              // "now" is pull time
            DateTime yesdt;
            
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P2. 1-1 safety observation by non-RN");
            Program.VerboseAudit("---------------");

            nowstr = nowdt.ToString("yyyyMMddHHmm");
            yesdt = nowdt.AddDays(-1);
            toddtstr = nowdt.ToString("yyyyMMdd");
            yesdtstr = yesdt.ToString("yyyyMMdd");
            
            //when is now? (yesterday/today)
            //yes 7am -- yes 7p  -- tod 7a -- tod 7p
            //                                   A                   B
            //if nowdt >= tod7pm then check  tod 7am-tod 7pm and tod 7pm-tom 7a
            //if nowdt >= tod7am then check  yes 7p - tod 7a     tod 7a-tod 7p
            //if nowdt >= yes7pm then check  yes 7a-yes 7p       yes 7p-tod 7a
            if (nowstr.CompareTo(toddtstr + "1900") >= 0) {
                timea = toddtstr + "0700";
            } else if (nowstr.CompareTo(toddtstr + "0700") >= 0) {
                timea = yesdtstr + "1900";
            } else if (nowstr.CompareTo(yesdtstr + "1900") >= 0) {
                timea = yesdtstr + "0700";
            }
            
            timea_startdt = PFSUtility.ISOToDateTime(timea);
            timea_enddt = timea_startdt.AddHours(12);
            MaybeAddSitter(timea_startdt, timea_enddt);

            timeb_startdt = timea_enddt;
            timeb_enddt = timeb_startdt.AddHours(12);
            MaybeAddSitter(timeb_startdt, timeb_enddt);
        }

        private void MaybeAddSitter(DateTime startdt, DateTime enddt)
        {
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);    // add custom time range below
            query = query.Where(e => (e.EVENT_DATETIME >= startdt) && (e.EVENT_DATETIME < enddt));
            query = AndCodeInList(query, "Sitter");
            query = AndResultInList(query,"continued, initiated");
            query = AndResultNotInList(query, "discontinued");

            if (query.Count() > 0) {
                if (ProcExists(2, startdt, enddt)) {
                    Program.Audit("Procedure 2: already exists");
                } else {
                    var proc = new proc_data();
                    proc.procedure_number = 2;
                    proc.start = startdt;
                    proc.finish = enddt;
                    _procs.Add(proc);
                    Program.Audit("Procedure 2: Found Sitter between " + startdt + " and " + enddt);
                }
            }
            
        }

        private bool ProcExists(int pnum, DateTime startdt, DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && (proc.PROCEDURE_DATETIME == startdt)
                            && (proc.DEPARTURE_DATETIME == enddt)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new {proc.PROCEDURE_EVENT_ID};
            return (query.Count() > 0);
        }


        private void CheckProc_3()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P3. Off unit accompanied by RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_4()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P4. Off unit accompanied by non-RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_5()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P5. Patient/family education by RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_6()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P6. Extensive wound management by RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_7()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P7. Extensive wound management by non-RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_8()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P8. Coordination of care by RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_9()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P9 1:1 RN at bedside");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_10()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P10. 1:1 non-RN at bedside");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_11()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P11. 2:1 by RN at bedside");
            //Program.VerboseAudit("---------------");
        }

        
        private const string DATETIME_FORMAT = "yyyyMMddHHmm";              // ISO Date/Time w/o seconds

        private void OutputClass()
        {
            string outstr, ind_list, desc, str_pull_dt, outstr0700;
            int i, tc_event_id;
            string txarea = "";

            txarea = FindTxArea(_pat.effective);
            
            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         212 242 304
            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1
            //1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901 3456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
            //1       |10 5            |                |                |        |10023121779         |ALEXANDER                       |GLORIA                          |                                |0508    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289530  |           |201403030400                                                                      |NNYNNNNYYNNYNNNYNNNYYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//1       |10 5            |                |                |        |10024544908         |ARMSTRONG                       |AARON                           |                                |0535    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289531  |           |201403030400                                                                      |NYNNYNNNNNNYNNNYNNNNYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//|0547    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289532  |           |201403030400                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//|0547    |A   |201403030700|                |201403030700    |18  |C|    |3040|1440|18289532  |           |201403030700                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN

            if (Program.g_is_test)
                tc_event_id = 9999;
            else
                tc_event_id = PFSUtility.NextGID();                         //get a unique id for this class
            str_pull_dt = _pat.pull_finish.ToString(DATETIME_FORMAT);

            outstr = _pat.facilty_code.FixedWidth(8);                       //(facility code)
            outstr += "|" + _pat.unit_name.FixedWidth(16);
            outstr += "|" + "".FixedWidth(16);                               //(unit code)
            outstr += "|" + txarea.FixedWidth(16);//"".FixedWidth(16);      //(area code)
            outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
            outstr += "|" + _pat.acct.FixedWidth(20);
            outstr += "|" + _pat.last_name.FixedWidth(32);
            outstr += "|" + _pat.first_name.FixedWidth(32);
            outstr += "|" + _pat.middle_name.FixedWidth(32);
            outstr += "|" + _pat.room.FixedWidth(8);
            outstr += "|" + _pat.bed.FixedWidth(4);
            outstr += "|" + str_pull_dt.FixedWidth(12);                      //class datetime (could change)
            outstr += "|" + "".FixedWidth(16);                               //(login)
            outstr += "|" + str_pull_dt.FixedWidth(16);                      //(employee)/(pull datetime)
            outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
            outstr += "|" + "C".FixedWidth(1);                               //record type = class
            outstr += "|" + "".FixedWidth(4);                                //(stage)
            outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
            outstr += "|" + _pat.range.ToString().FixedWidth(4);             //TC pull range
            outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
            outstr += "|";
            outstr = outstr.FixedWidth(294);
            outstr += "|" + _pat.effective.ToString(DATETIME_FORMAT);        //IN
            outstr = outstr.FixedWidth(377);
            outstr += "|";
            
            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++) {
                if (_inds[i].is_checked) {
                    outstr += "Y";
                    ind_list += "," + i;
                } else {
                    outstr += "N";
                }
            } // next i
            ind_list = ind_list.Substring(1);                           //strip leading comma
  
            Program.outfile.WriteLine(outstr);                          //output to transparent.txt
            if (Program.g_make0700)
            {  //212 242 304
                char[] buffer = outstr.ToCharArray();
                buffer[212 - 1 + 0] = '0';
                buffer[212 - 1 + 1] = '7';
                buffer[212 - 1 + 2] = '0';
                buffer[212 - 1 + 3] = '0';

                buffer[242 - 1 + 0] = '0';
                buffer[242 - 1 + 1] = '7';
                buffer[242 - 1 + 2] = '0';
                buffer[242 - 1 + 3] = '0';

                buffer[304 - 1 + 0] = '0';
                buffer[304 - 1 + 1] = '7';
                buffer[304 - 1 + 2] = '0';
                buffer[304 - 1 + 3] = '0';
                outstr0700 = new string(buffer);
                Program.outfile7am.WriteLine(outstr0700);                          //output to transparent0700.txt
            }
            // This next part is only for the 11am.  if there's no 7am class
            // then create one using the 11am indicators.
            //This can happen if admit is after 4am and before 7am. 20150701hhmm
            if (str_pull_dt.Substring(8,4) == "1100" && NoClassAt7am(str_pull_dt))
            {
                char[] buff11 = outstr.ToCharArray();
                buff11[212 - 1 + 0] = '0';
                buff11[212 - 1 + 1] = '7';
                buff11[212 - 1 + 2] = '0';
                buff11[212 - 1 + 3] = '0';

                buff11[242 - 1 + 0] = '0';
                buff11[242 - 1 + 1] = '7';
                buff11[242 - 1 + 2] = '0';
                buff11[242 - 1 + 3] = '0';

                buff11[304 - 1 + 0] = '0';
                buff11[304 - 1 + 1] = '7';
                buff11[304 - 1 + 2] = '0';
                buff11[304 - 1 + 3] = '0';
                outstr0700 = new string(buff11);
                Program.outfile.WriteLine(outstr0700);                          //output to transparent.txt
            }

            Program.Audit("");
            desc = "Classified: " + ind_list;
            if (Program.g_is_test) {
                Program.Audit(desc);
            } else {
                //Save the selected indicators plus both Program.Audits in the event log; link with tc_event_id
                PFSEventLog.AddTransparentMappingEventLogEntry(
                    desc, Program.gLogUnitID, Program.gLogEncounterID,
                    tc_event_id, Program.gLogMapperVersion,
                    Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
            }
        }

        private string FindTxArea(DateTime dt)
        {
            string tx = "";
            var db = PFSUtility.NewPfsDataContext();
            var query = from el in db.ENCOUNTER_LOCATIONs
                        where (el.ENCOUNTER_ID == _pat.encounter_id)
                        where (el.EFFECTIVE_DATETIME_IN <= dt)
                        where (el.EFFECTIVE_DATETIME_OUT >= dt)
                        select el;
            foreach (var item in query)
            {
                tx = item.HOSPITAL_SERVICE_LVC.Trim();
            }
            if (tx == "OBS" || tx == "GYN" || tx == "NUR")
                return tx;
            else
                return "";
        }

        private bool NoClassAt7am(string str_dt)
        {
            // 
            //_pat.encounter_id
            //_pat.unit_id
            //var query = from ce in _class
            //            from ans in proc.PROCEDURE_ANSWERs
            //            where (proc.ENCOUNTER_ID == _pat.encounter_id)
            //                && (proc.PROCEDURE_DATETIME == startdt)
            //                && (proc.DEPARTURE_DATETIME == enddt)
            //                && (ans.PROCEDURE_NUMBER == pnum)
            //            select new {proc.PROCEDURE_EVENT_ID};
            //return (query.Count() > 0);
            string str7am = str_dt.Substring(0, 8) + "0700";
            DateTime str7amdt = PFSUtility.ISOToDateTime(str7am);
            var db = PFSUtility.NewPfsDataContext();
            var query = from ce in db.CLASSIFICATION_EVENTs
                        where (ce.ENCOUNTER_ID == _pat.encounter_id)
                           && (ce.UNIT_ID == _pat.unit_id)
                           && (ce.EFFECTIVE_DATETIME_IN == str7amdt)
                        select new { ce.CLASSIFICATION_EVENT_ID };
            return (query.Count() == 0);
        }

        private void OutputProcs()
        {
            int i;
            string outstr, proc_list, desc;
            int tc_event_id;

            foreach(var proc in _procs) {
                if (Program.g_is_test)
                    tc_event_id = 9999;
                else
                    tc_event_id = PFSUtility.NextGID();                         //get a unique id for this proc

                outstr = _pat.facilty_code.FixedWidth(8);
                outstr += "|" + _pat.unit_name;                                 //10
                outstr = outstr.FixedWidth(68);
                outstr += "|" + _pat.acct.FixedWidth(20);                       //90
                outstr += "|" + _pat.last_name.FixedWidth(32);
                outstr += "|" + _pat.first_name.FixedWidth(32);
                outstr += "|" + _pat.middle_name.FixedWidth(32);
                outstr = outstr.FixedWidth(202);
                outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //204 proc dt
                outstr = outstr.FixedWidth(254);
                outstr += "|P";                                                 //256 procedure type record
                outstr += "|" + "".FixedWidth(4);                               //(stage)
                outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);     //TC source ID
                outstr += "|" + _pat.range.ToString().FixedWidth(4);            //TC pull range
                outstr += "|" + tc_event_id.ToString().FixedWidth(10);          //TC event ID
                outstr += "|";
                outstr = outstr.FixedWidth(294);
                outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //296 procdt in
                outstr = outstr.FixedWidth(346);
                outstr += "|" + proc.finish.ToString(DATETIME_FORMAT);          //348 procdt out
                outstr = outstr.FixedWidth(377);
                outstr += "|";
                
                proc_list = "";
                for (i = 1; (i < MAX_PROCS); i++) {
                    if (proc.procedure_number == i) {
                        outstr += "Y";
                        proc_list += "," + i;
                    } else {
                        outstr += "N";
                    }
                } // next i
                proc_list = proc_list.Substring(1);                             //strip leading comma

                Program.outfile.WriteLine(outstr);                              //output to transparent.txt

                desc = "Procedures: " + proc_list;
                if (Program.g_is_test) {
                    Program.Audit(desc);
                } else {
                    //Save the selected procedures plus both Program.Audits in the event log; link with tc_event_id
                    //(this assumes the procedure Program.Audit is mixed in with the class Program.Audit)
                    PFSEventLog.AddTransparentMappingEventLogEntry(desc, Program.gLogUnitID, Program.gLogEncounterID,
                        tc_event_id, Program.gLogMapperVersion,
                        Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
                }
            } // next proc
        }


    }
}
