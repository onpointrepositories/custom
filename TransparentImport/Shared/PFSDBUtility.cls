VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PFSDBUtility"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'
' PFSDBUtility: Low-level database utilities
'
' The database may contain null values.  If these values are not going
' to be stored in variant datatypes they need to be convert to zero,
' false, or null string.
' String constants in SQL commands must be quoted with single quotes.
' Imbedded single quotes must be quoted.  For example, O'Donnell becomes 'O''Donnell'.
' Date constants in SQL commands must be quoted and possibly run through
' a conversion function before the database will accept them.

Public Enum DatabaseTypeEnum
    DBT_UNKNOWN
    DBT_ACCESS
    DBT_SQL_SERVER
    DBT_ORACLE
End Enum

Public Enum DriverTypeEnum
    DRV_ODBC
    DRV_OLEDB
End Enum

'
' Default database name, user and password for opening database connections
'
Private Const PFS_REMOTE_DSN = "WinPFS"
Private Const PFS_REMOTE_UID = "WinPFS"
Private Const PFS_REMOTE_PWD = "pfs4you"

Private Const PFS_LOCAL_MDB = ""                'no default
Private Const PFS_LOCAL_UID = "Admin"
Private Const PFS_LOCAL_PWD = ""

Private Const GID_SEQUENCE_NAME = "GID"
Private Const GID_DEFAULT_BLOCKSIZE = 5

Private gid_blocksize   As Integer
Private gid_next_id     As Long
Private gid_last_id     As Long

'
' Database to application conversions:
'

Function DBToBoolean(ByVal v As Variant) As Boolean
    Dim s As String
    ' We are expecting Y/N or NULL.  Also accept y/n and 1/0.
    If IsNull(v) Then
        DBToBoolean = False
    Else
        s = CStr(v)
        DBToBoolean = (s = "Y" Or s = "y" Or s = "1")
    End If
End Function

Function DBToDouble(ByVal v As Variant) As Double
    If IsNumeric(v) Then
        DBToDouble = CDbl(v)
    Else
        DBToDouble = 0
    End If
End Function

Function DBToInteger(ByVal v As Variant) As Integer
    If IsNumeric(v) Then
        DBToInteger = CInt(v)
    Else
        DBToInteger = 0
    End If
End Function

Function DBToLong(ByVal v As Variant) As Long
    If IsNumeric(v) Then
        DBToLong = CLng(v)
    Else
        DBToLong = 0
    End If
End Function

Function DBToSingle(ByVal v As Variant) As Single
    If IsNumeric(v) Then
        DBToSingle = CSng(v)
    Else
        DBToSingle = 0
    End If
End Function

Function DBToString(ByVal v As Variant) As String
    If IsNull(v) Then
        DBToString = ""
    Else
        DBToString = CStr(v)
    End If
End Function

'
' Application to database conversions:
'

Function BooleanToDB(ByVal v As Boolean) As String
    ' We are storing boolean values as Y/N in a CHAR(1) field.
    If (v) Then
        BooleanToDB = "Y"
    Else
        BooleanToDB = "N"
    End If
End Function

Function IDToDB(ByVal id As Long) As Variant
    IDToDB = ZeroToNull(id)
End Function

Function LongToDB(ByVal v As Variant) As Variant
    If IsNumeric(v) Then
        LongToDB = CLng(v)
    Else
        LongToDB = Null
    End If
End Function

Function StringToDB(ByVal v As Variant) As Variant
    ' Convert null string to NULL (for the database)
    If Len(v) <> 0 Then
        StringToDB = CStr(v)
    Else
        StringToDB = Null
    End If
End Function

Function ZeroToNull(ByVal v As Variant) As Variant
    ' Convert a zero back to NULL (for the database)
    If v <> 0 Then
        ZeroToNull = v
    Else
        ZeroToNull = Null
    End If
End Function


'
' SQL Command utilities (used to create SQL commands)
'

Public Function SQL_Boolean(ByVal val As Boolean) As String
    SQL_Boolean = "'" & BooleanToDB(val) & "'"
End Function


Private Function SQL_DateAndTime(ByVal v As Variant, ByVal with_date As Boolean, ByVal with_time As Boolean, Optional database_type As DatabaseTypeEnum) As String
    Dim fmt As String

    If IsMissing(database_type) Or (database_type = 0) Then
        database_type = DatabaseType(g_cnADO)
    End If

    If IsDate(v) Then
        Select Case database_type
        Case DBT_ACCESS
            'Access always wants us-english style dates in pound signs
            If (with_date) Then fmt = fmt & "mm/dd/yyyy"
            If (with_time) Then fmt = fmt & " hh:nn:ss"
            SQL_DateAndTime = "#" & Format$(CDate(v), fmt) & "#"
        Case Else
            'SQL Server will accept ISO YYYYMMDD so we can avoid regional settings
            If (with_date) Then fmt = fmt & "yyyymmdd"
            If (with_time) Then fmt = fmt & " hh:nn:ss"
            SQL_DateAndTime = "'" & Format$(CDate(v), fmt) & "'"
        End Select
    Else
        SQL_DateAndTime = "NULL"
    End If
End Function

Function SQL_Date(ByVal v As Variant, Optional database_type As DatabaseTypeEnum) As String
    SQL_Date = SQL_DateAndTime(v, True, False, database_type)
End Function

Function SQL_DateTime(ByVal v As Variant, Optional database_type As DatabaseTypeEnum) As String
    SQL_DateTime = SQL_DateAndTime(v, True, True, database_type)
End Function

Function SQL_Time(ByVal v As Variant, Optional database_type As DatabaseTypeEnum) As String
    SQL_Time = SQL_DateAndTime(v, False, True, database_type)
End Function

'Insert a number, or NULL
Function SQL_Number(ByVal v As Variant) As String
    If IsNumeric(v) Then
        SQL_Number = CStr(v)
    Else
        SQL_Number = "NULL"
    End If
End Function

'
' Quote a string and quote imbedded apostrophies.  Return "NULL" for null values.
'
Function SQL_String(ByVal s As Variant) As String
    On Error GoTo errHandler
    Dim c As Integer
    Dim n As Long                               'allow long text
    Dim txt As String, ch As String

    If IsNull(s) Then
        SQL_String = "NULL"
        Exit Function
    End If

    txt = Replace$(s, "'", "''")                'quote all apostrohpies
    
    'Look for control characters; some may cause trouble unless changed to 'char(n)' format.
    
    For c = 1 To 31
        ch = Chr$(c)
        Select Case ch
        Case vbCr, vbLf, vbTab
            'OK - keep these inline
        Case Else
            'Look for additional control characters - some have caused an "unclosed quote" error
            n = InStr(1, txt, ch)
            If (n > 0) Then
                'Problem: when we use the + operator we are limited to the VARCHAR max of 8000 chars.
                'If the string length exceeds this, the command will blow up.
                If Len(s) < 8000 Then
                    'Option 1: preserve the control chars
                    txt = Replace$(txt, ch, "' + CHAR(" & Asc(ch) & ") + '")
                Else
                    'Option 2: kill the control characters
                    txt = Replace$(txt, ch, "?")
                End If
            End If
        End Select
    Next c
    
    SQL_String = "'" & txt & "'"
    Exit Function
    
errHandler:
    SQL_String = "'" & txt & "'"
End Function

'return the appropriate string concatenation operator according to the database type
Function SQL_StringAppend() As String
    
    Select Case g_dbutil.DatabaseType(g_cnADO)
    
        Case DBT_ACCESS
            SQL_StringAppend = " & "
        Case DBT_SQL_SERVER
            SQL_StringAppend = " + "
        Case DBT_ORACLE
            SQL_StringAppend = " || "
        Case Else
            SQL_StringAppend = " + "
            
    End Select

End Function

' Convert a zero value back to NULL (for the database)
Function SQL_ZeroToNull(ByVal v As Variant) As String
    If IsNumeric(v) And (v <> 0) Then
        SQL_ZeroToNull = CStr(v)
    Else
        SQL_ZeroToNull = "NULL"
    End If
End Function


'
' Open an Access database
' (returns a new connection object)
' Note: this is still used in DBUpgrade.cls
'
Function NewLocalConnection( _
    Optional opt_dsn As Variant, _
    Optional opt_uid As Variant, _
    Optional opt_pwd As Variant _
) As Connection
    Dim dsn As String, uid As String, pwd As String
    Dim result As New Connection
    
    On Error GoTo errHandler
    
    dsn = IIf(IsMissing(opt_dsn), PFS_LOCAL_MDB, opt_dsn)
    uid = IIf(IsMissing(opt_uid), PFS_LOCAL_UID, opt_uid)
    pwd = IIf(IsMissing(opt_pwd), PFS_LOCAL_PWD, opt_pwd)
    
    Set result = New Connection
    With result
        .ConnectionString = "DBQ=" & dsn & ";driver={Microsoft Access Driver (*.mdb)}"
        .CursorLocation = adUseClient
        .Open
    End With
    
    Set NewLocalConnection = result
    Exit Function
errHandler:
    Err.Raise PFSERR_VALIDATION, "", Err.Description & ": " & dsn
End Function

'
' Open a connection to the database server
' (returns a new connection object)
'
Function NewRemoteConnection( _
    Optional opt_dsn As Variant, _
    Optional opt_uid As Variant, _
    Optional opt_pwd As Variant, _
    Optional opt_UseServerCursor As Boolean = False, _
    Optional opt_driver As DriverTypeEnum = DRV_ODBC _
) As Connection
    Dim dsn As String, uid As String, pwd As String
    Dim driver As DriverTypeEnum
    Dim result As Connection

    On Error GoTo errHandler
    
    'This used to be a time-consuming process but it has been made much more effecient.
    '
    'For best performance, each active recordset should have its own connection.
    'More than one recordset open on the same connection works, but not always well.
    Debug.Print
    Debug.Print "*** OPENING DATABASE CONNECTION"
    
    dsn = IIf(IsMissing(opt_dsn), PFS_REMOTE_DSN, opt_dsn)
    uid = IIf(IsMissing(opt_uid), PFS_REMOTE_UID, opt_uid)
    pwd = IIf(IsMissing(opt_pwd), PFS_REMOTE_PWD, opt_pwd)
    
    driver = opt_driver
    If InStr(1, g_command, "-odbc") > 0 Then
        driver = DRV_ODBC
    End If
    If InStr(1, g_command, "-oledb") > 0 Then
        driver = DRV_OLEDB
    End If

    Set result = New Connection
    With result
        .ConnectionString = MakeConnectionString(dsn, uid, pwd, driver)
        Debug.Print .ConnectionString

        'Use the client cursor by default to avoid ADO bugs
        'Besides that, the client cursor is more effecient.
        If opt_UseServerCursor = True Then
            .CursorLocation = adUseServer
        Else
            .CursorLocation = adUseClient
        End If
        
        'The default connect timeout is 15 seconds; sometimes 30 isn't enough
        .ConnectionTimeout = 40
        .Open
    
        'The default command timeout is 30 seconds; sometimes 60 isn't enough
        .CommandTimeout = 180
    End With
    
    Debug.Print
    Set NewRemoteConnection = result
    Exit Function
errHandler:
    Err.Raise PFSERR_VALIDATION, "", "Unable to open database '" & dsn & "': " & Err.Description
End Function

'(this is used in report notes)
Public Function DefaultConnectionString() As String
    DefaultConnectionString = MakeConnectionString(PFS_REMOTE_DSN, PFS_REMOTE_UID, PFS_REMOTE_PWD, DRV_ODBC)
End Function

Private Function MakeConnectionString(dsn As String, uid As String, pwd As String, driver As DriverTypeEnum) As String
    Dim result As String

    '2005: as long as each ODBC recordset has its own connection, there no longer appears
    'to be any difference in performance between OLE-DB and ODBC.

    Select Case driver
    Case DRV_OLEDB
        result = MakeOLEDBConnectionString(dsn, uid, pwd)
        If Len(result) = 0 Then
            result = MakeConnectionString(dsn, uid, pwd, DRV_ODBC)
        End If
    Case Else
        'Standard ODBC connection
        result = "DSN=" & dsn & ";uid=" & uid & ";pwd=" & pwd & ";"
    End Select

    MakeConnectionString = result
End Function

Private Function MakeOLEDBConnectionString(dsn As String, uid As String, pwd As String) As String
    On Error GoTo errHandler
    Dim result As String, server_name As String, default_db As String
    Dim regkey As String, subkey As String, KeyVal As String

    regkey = "SOFTWARE\ODBC\odbc.ini\WinPFS"
    
    subkey = "Server"
    If g_util.GetKeyValue(HKEY_LOCAL_MACHINE, regkey, subkey, KeyVal) Then
        server_name = KeyVal
    Else
        'Bail out, use ODBC and display error message
        Exit Function
    End If
    
    subkey = "Database"
    If g_util.GetKeyValue(HKEY_LOCAL_MACHINE, regkey, subkey, KeyVal) Then
        default_db = KeyVal
    Else
        default_db = "pfs"
    End If

    result = "Provider=sqloledb" & _
        ";Data Source=" & server_name & _
        ";Initial Catalog=" & default_db & _
        ";User Id=" & uid & _
        ";Password=" & pwd & _
        ";Packet Size=32767" & _
        ";"
    MakeOLEDBConnectionString = result
    Exit Function
    
errHandler:
    g_util.ThrowError "MakeOLEDBConnectionString"
    Resume  'debug
End Function


'
' A recordset may report a fatal error even though the connection object says
' everything is all right.
'
Public Function IsFatalDatabaseError(ByVal errnum As Long, ByVal errdesc As String) As Boolean
    Dim result As Boolean
    Dim desc As String

    'There appear to be multiple error numbers for the same error text, or one
    'number with many meanings.  Skip the number and examine the error description.
    desc = LCase$(errdesc)
    If InStr(1, desc, "communication link") > 0 Or _
       InStr(1, desc, "network") > 0 Or _
       InStr(1, desc, "timeout") > 0 Or _
       InStr(1, desc, "connectionread") > 0 Or _
       InStr(1, desc, "connectionwrite") > 0 Or _
       InStr(1, desc, "terminated") > 0 _
    Then
        result = True
    End If

    IsFatalDatabaseError = result
End Function


'
' Database type
' (this is a quick and dirty version to figure out the database type)
'
Public Function DatabaseType(ByVal cnADO As Connection) As DatabaseTypeEnum
    DatabaseType = DBT_UNKNOWN
    If (cnADO Is Nothing) Then Exit Function
    
    If InStr(1, LCase$(cnADO.ConnectionString), ".mdb") Then
        DatabaseType = DBT_ACCESS
    Else
        DatabaseType = DBT_SQL_SERVER
    End If
End Function

'
' Database Name
'
Public Function DatabaseName(ByVal cnADO As Connection) As String
    If (cnADO Is Nothing) Then Exit Function
    DatabaseName = cnADO.Properties("Current Catalog")
End Function

'
' Return the name of the server we are connected to
'
Function ServerName(ByVal cnADO As Connection) As String
    On Error GoTo errHandler
    Dim n As Integer
    Dim regkey As String, subkey As String, KeyVal As String, dsn As String
    Dim result As String

    If (cnADO Is Nothing) Then
        ServerName = "(nothing)"
        Exit Function
    End If

    '
    ' This section works with ADO 1.0 - 2.1
    '
    n = InStr(1, cnADO.ConnectionString, "SERVER=")
    If (n > 0) Then
        result = g_util.StringUpTo(Mid$(cnADO.ConnectionString, n + 7), ";")
    End If
    If Len(result) = 0 Then
        n = InStr(cnADO.ConnectionString, "DBQ=")
        If (n > 0) Then
            'Show an Access database filename
            result = g_util.StringUpTo(Mid$(cnADO.ConnectionString, n + 4), ";")
        End If
    End If
    
    '
    ' This section works with ADO 2.5 on
    '
    If Len(result) = 0 Then
        n = InStr(1, cnADO.ConnectionString, "Data Source=")
        If (n > 0) Then
            result = g_util.StringUpTo(Mid$(cnADO.ConnectionString, n + 12), ";")
        End If
    End If
    
    If Len(result) = 0 Then
        n = InStr(1, cnADO.ConnectionString, "Address=")
        If (n > 0) Then
            result = g_util.StringUpTo(Mid$(cnADO.ConnectionString, n + 8), ";")
        End If
    End If
    
    If Len(result) = 0 Then
        'None of the above?  Try getting the data source name
        n = InStr(1, cnADO.ConnectionString, "DSN=")
        If (n > 0) Then
            dsn = g_util.StringUpTo(Mid$(cnADO.ConnectionString, n + 4), ";")
        Else
            'In Vista the connection string has only "Provider=MSDASQL.1;" - that's all
            'Fortunately we can still get the DSN this way:
            dsn = cnADO.Properties("Data Source Name").value
        End If
        
        If (Len(dsn) > 0) Then
            'Get the server name from the ODBC configuration
            regkey = "SOFTWARE\ODBC\odbc.ini\" & dsn
            subkey = "Server"
            If g_util.GetKeyValue(HKEY_LOCAL_MACHINE, regkey, subkey, KeyVal) Then
                ServerName = KeyVal
                Exit Function
            End If
        End If
    End If
    
    'Strip off any extended information like TCP port number, i.e. "server,1433"
    result = g_util.StringUpTo(result, ",")
    ServerName = result
    Exit Function

errHandler:
    ServerName = "(Unknown)"
End Function

'
' Return the server's current date & time
'
Function ServerDateTime(ByVal cnADO As Connection) As Date
    On Error GoTo errHandler
    Dim rs As New Recordset
    rs.Open "SELECT GETDATE()", cnADO
    ServerDateTime = rs(0)
    rs.Close
    Exit Function
errHandler:
    g_util.ThrowError "ServerDateTime"
End Function

Function ServerDate(ByVal cnADO As Connection) As Date
    ServerDate = DateValue(ServerDateTime(cnADO))
End Function

Function ServerTime(ByVal cnADO As Connection) As Date
    ServerTime = TimeValue(ServerDateTime(cnADO))
End Function


'
' Return the workstation local date & time, using the server clock
' We are doing this because the workstation time is often wrong.
' This also eliminates a security hole by letting the user pick a "current" date that isn't.
Function LocalDateTime(ByVal cnADO As Connection) As Date
    On Error GoTo errHandler
    Dim rs As New Recordset
    Dim dt As Date
    Dim KeyVal As String
    
    rs.Open "SELECT GETUTCDATE()", cnADO
    dt = rs(0)
    rs.Close
    
    If g_util.GetKeyValue(HKEY_LOCAL_MACHINE, "System\CurrentControlSet\Control\TimeZoneInformation", "ActiveTimeBias", KeyVal) Then
        'subtract your local time zone offset (in minutes) from the UTC time
        LocalDateTime = DateAdd("n", -CInt(KeyVal), dt)
    Else
        LocalDateTime = Now()
    End If
    Exit Function
errHandler:
    'SQL 7.0 does not support UTC time; be nice and keep working
    'g_util.ThrowError "ServerDateTime"
    LocalDateTime = Now()
End Function

Function LocalDate(ByVal cnADO As Connection) As Date
    LocalDate = DateValue(LocalDateTime(cnADO))
End Function

Function LocalTime(ByVal cnADO As Connection) As Date
    LocalTime = TimeValue(LocalDateTime(cnADO))
End Function


'
' Get the current database version
' Returns -1 if there is no version yet.
'
Public Function DatabaseVersion(ByVal cnADO As Connection) As Double
    Dim rs As New Recordset, sql As String
    Dim result As Double
    
    On Error GoTo errHandler
    
    result = -1
    
    sql = "SELECT MAX(DATABASE_REVISION) FROM SYS_REVISION"
    rs.Open sql, cnADO
    
    If Not rs.EOF Then
        'MAX returns NULL if the table is empty
        If Not IsNull(rs(0)) Then
            result = rs(0)
        End If
    End If
    
    rs.Close
    Set rs = Nothing
    
    DatabaseVersion = result
    Exit Function

errHandler:
    If IsFatalDatabaseError(Err.Number, Err.Description) Then
        g_util.ThrowError
    End If
    'The sys_revision table does not exist yet
    DatabaseVersion = -1
    Exit Function
End Function


'
' Row ID Numbers:
' This is a global allocation scheme (one sequence of numbers for all tables)
'
' NextGID() will always return the next unique global id no matter what.
' You can reduce overhead by reserving GIDs if you know how many you will need.
'
' These routines require g_cnADO: a global database connection

Public Function NextGID() As Long
    Dim result As Long
    
    If (gid_next_id <= gid_last_id And gid_next_id <> 0) Then    'more ids reserved?
        result = gid_next_id                'get the next one
    Else
        'Use the default block size unless a told otherwise by ReserveGIDs
        If (gid_blocksize < 1) Then gid_blocksize = GID_DEFAULT_BLOCKSIZE
        'Get a new block of numbers
        result = AllocateIDs(GID_SEQUENCE_NAME, gid_blocksize)
        'Save the last valid number
        gid_last_id = result + gid_blocksize - 1
        gid_blocksize = 0                   'revert to default for next time
    End If
    gid_next_id = result + 1                'bump for next time
    NextGID = result
End Function

Public Sub ReserveGIDs(ByVal num_ids As Integer)
    gid_blocksize = num_ids                 'save for when needed
End Sub

'
' AllocateIDs can keep track of any number of sequences, by name
'
Public Function AllocateIDs(ByVal sequence_name As String, ByVal num_ids As Integer) As Long
    'Allocate a block of IDs from the SYS_SEQUENCE table
    Dim rs As New Recordset
    Dim sql As String, next_id As Long
    
retry:
    On Error GoTo errHandler
    
    sql = "SELECT * FROM SYS_SEQUENCE WHERE SEQUENCE_NAME=" & SQL_String(sequence_name)
    Debug.Print sql
    rs.Open sql, g_cnADO, adOpenDynamic, adLockPessimistic
    
    If rs.EOF Then
        rs.AddNew                               'first time ever
        rs("SEQUENCE_NAME") = sequence_name     'save the new sequence name
        next_id = 1                             'always start at one, not zero.
    Else
        next_id = rs("NEXT_VALUE")              'get the next id
    End If
    
    rs("NEXT_VALUE") = next_id + num_ids        'save next available id for others
    rs.Update                                   'update and release
    rs.Close
    
    Set rs = Nothing
    AllocateIDs = next_id
    Exit Function
errHandler:
    'adLockPessimistic lock should have locked this record but it didn't
    If (Err.Number = SQL_ERROR_CANNOT_LOCATE_ROW) Then
        'Can't locate row for updating: it was modified.  Try again.
        On Error Resume Next
        'This causes an error even though rs is open.  "resume next" doesn't help either.
        'rs.Close
        Set rs = New Recordset                  'allocate a new one
        Debug.Print "SYS_SEQUENCE is busy; try again"
        g_util.Sleep 250
        Resume retry
    End If
    Err.Raise Err.Number, "AllocateIDs", Err.Description
    Resume  'debug
End Function


'====================================================================================
' MISC FUNCTIONS SHARED AMONG VARIOUS CLASSES
'====================================================================================

' Add a request to recalculate report totals for this unit and date
'
Public Sub AddRecalcUnitDate(ByVal unit_id As Long, ByVal report_date As Date)
    Dim sql As String

    sql = "INSERT INTO RPT_RECALC_UNIT_DATE (" & _
          "    UNIT_ID, REPORT_DATE" & _
          ") VALUES (" & _
               unit_id & "," & g_dbutil.SQL_Date(report_date) & _
          ")"

    'We will get an insert error if this unit and date are already there
    On Error GoTo eraseTheRecalcDate
    Debug.Print sql
    g_cnADO.Execute sql
    Exit Sub
    
eraseTheRecalcDate:
    'If it is already in the recalc unit date table someone might be in the process
    'of recalculating it (or we just got an error during a calculation).  Clear the
    'recalc date/time to keep this record in the table until next time.
    sql = "UPDATE RPT_RECALC_UNIT_DATE" & _
         " SET    RECALC_DATETIME = NULL" & _
         " WHERE  UNIT_ID = " & unit_id & _
         " AND    REPORT_DATE = " & g_dbutil.SQL_Date(report_date)
    On Error Resume Next
    g_cnADO.Execute sql
End Sub


'
' Return the name of the day definition id in the UNIT table for a given date.
' The column names are SUNDAY_ID, MONDAY_ID, ... SATURDAY_ID.
'
Friend Function DayDefColumnName(ByVal this_date As Date) As String
    'Do not use Format$ to get the day name: it fails if you aren't in
    'an English-speaking region (like Iceland).
    'DayDefColumnName = UCase$(Format$(this_date, "dddd")) & "_ID"
    Select Case Weekday(this_date)
    Case vbSunday:      DayDefColumnName = "SUNDAY_ID"
    Case vbMonday:      DayDefColumnName = "MONDAY_ID"
    Case vbTuesday:     DayDefColumnName = "TUESDAY_ID"
    Case vbWednesday:   DayDefColumnName = "WEDNESDAY_ID"
    Case vbThursday:    DayDefColumnName = "THURSDAY_ID"
    Case vbFriday:      DayDefColumnName = "FRIDAY_ID"
    Case vbSaturday:    DayDefColumnName = "SATURDAY_ID"
    End Select
End Function


'Fix parameter expiration dates after one or more effective date has been added or changed.
'Returns True if changes were made.
'
Private Function FixGenericParameterExpiration( _
    ByVal table As String, ByVal pk_column As String, _
    ByVal parent_column As String, ByVal parent_id As Long _
) As Boolean
    On Error GoTo errHandler

    Dim sql As String
    Dim rs As New Recordset
    Dim param() As Variant
    Dim count As Integer, iRow As Integer
    Dim expiration_datetime As Date
    Dim effective_report_date As Date
    Dim expiration_report_date As Date
    Dim result As Boolean, changed As Boolean

    result = False

    sql = "SELECT " & _
               pk_column & "," & _
         "     EFFECTIVE_DATETIME,    EXPIRATION_DATETIME," & _
         "     EFFECTIVE_REPORT_DATE, EXPIRATION_REPORT_DATE" & _
         " FROM " & table & _
         " WHERE " & parent_column & "=" & parent_id & _
         " ORDER BY EFFECTIVE_DATETIME"
    Debug.Print sql
    rs.Open sql, g_cnADO, adOpenStatic, adLockOptimistic
    count = rs.RecordCount

    If count > 0 Then
        'Set the effective report date.
        'The effective report date is always the date part of the effective date/time.
        rs.MoveFirst
        For iRow = 0 To count - 1
            effective_report_date = g_util.DateOnly(rs("EFFECTIVE_DATETIME"))
            
            If g_util.TimeNE(rs("EFFECTIVE_REPORT_DATE"), effective_report_date) Then
                rs("EFFECTIVE_REPORT_DATE") = effective_report_date
                rs.Update
                result = True
            End If
            
            rs.MoveNext
        Next iRow
                
        'Create a zero-based matrix of values.  Note that the matrix is column
        'major: param(cols, rows).
        rs.MoveFirst
        param = rs.GetRows
        
        'Now calculate all but the last expiration date
        rs.MoveFirst
        For iRow = 0 To count - 2
            changed = False
            
            'The current expiration date/time should be one second less than the next
            'effective date/time.  The SQL BETWEEN operator is inclusive so the expiration
            'time must not equal the next effective time.
            expiration_datetime = DateAdd("s", -1, param(1, iRow + 1))
            If g_util.TimeNE(rs("EXPIRATION_DATETIME"), expiration_datetime) Then
                rs("EXPIRATION_DATETIME") = expiration_datetime
                changed = True
            End If
            
            'The expiration report date is the day before the next effective report date.
            expiration_report_date = DateAdd("d", -1, g_util.DateOnly(param(3, iRow + 1)))
            If g_util.TimeNE(rs("EXPIRATION_REPORT_DATE"), expiration_report_date) Then
                rs("EXPIRATION_REPORT_DATE") = expiration_report_date
                changed = True
            End If
            
            If changed Then             '(rs.Status = adRecModified) didn't work
                rs.Update
                result = True
            End If
            
            rs.MoveNext
        Next iRow

        rs.MoveLast
        'The last expiration date should always be well in the future.
        '(We could make the last expiration date NULL but that would complicate
        ' all SQL commands.  This way we can always look for parameters for a date
        ' where the date is between effective and expiration dates.)
        
        'expiration_datetime = DateAdd("yyyy", 50, ServerDate(g_cnADO))
        expiration_datetime = DateAdd("yyyy", 50, rs("EFFECTIVE_DATETIME"))
        
        If rs("EXPIRATION_DATETIME") <> expiration_datetime Then
            rs("EXPIRATION_DATETIME") = expiration_datetime
            rs("EXPIRATION_REPORT_DATE") = g_util.DateOnly(expiration_datetime)
            rs.Update
            result = True
        End If
        
        
    End If

    rs.Close
    FixGenericParameterExpiration = result
    Exit Function
    
errHandler:
    g_util.ThrowError "FixGenericParameterExpiration"
    Resume  'debug
End Function

'
' Fix Enterprise expiration dates after effective dates have been added,
' deleted or changed.
'
Public Function FixEnterpriseParameterExpiration(ByVal enterprise_id As Long) As Boolean
    FixEnterpriseParameterExpiration = _
        FixGenericParameterExpiration( _
            "ENTERPRISE_PARAM", "ENTERPRISE_PARAM_ID", "ENTERPRISE_ID", enterprise_id)
End Function

'
' Fix Facility Parameter expiration dates after effective dates have been added,
' deleted or changed.
'
Public Function FixFacilityParameterExpiration(ByVal facility_id As Long) As Boolean
    FixFacilityParameterExpiration = _
        FixGenericParameterExpiration( _
            "FACILITY_PARAM", "FACILITY_PARAM_ID", "FACILITY_ID", facility_id)
End Function

'
' Fix Unit Parameter expiration dates after effective dates have been added,
' deleted or changed.
'
Public Function FixUnitParameterExpiration(ByVal unit_id As Long) As Boolean
    FixUnitParameterExpiration = _
        FixGenericParameterExpiration( _
            "UNIT_PARAM", "UNIT_PARAM_ID", "UNIT_ID", unit_id)
End Function


'
'Get a user preference.  Return the default if the key is not found.
'
Public Function GetUserPreference(user_id As Long, key As String, default As String) As String
    Dim rs As New Recordset, sql As String

    sql = "SELECT PREFERENCE_VALUE FROM USER_PREFERENCE" & _
          " WHERE USER_ID = " & user_id & _
          " AND   PREFERENCE_KEY = " & g_dbutil.SQL_String(key)
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        GetUserPreference = rs(0) & ""
    Else
        GetUserPreference = default
    End If
    rs.Close
End Function

'
'Set a user preference
'
Public Sub SetUserPreference(user_id As Long, key As String, value As String)
    Dim sql As String

    If (user_id = 0) Or Len(key) = 0 Then Exit Sub

    sql = "DELETE FROM USER_PREFERENCE" & _
          " WHERE USER_ID = " & user_id & _
          " AND   PREFERENCE_KEY = " & g_dbutil.SQL_String(key)
    On Error Resume Next
    g_cnADO.Execute sql
    On Error GoTo 0

    sql = "INSERT INTO USER_PREFERENCE (" & _
          "     USER_ID, PREFERENCE_KEY, PREFERENCE_VALUE" & _
          ") VALUES (" & _
          "     " & user_id & "," & _
                g_dbutil.SQL_String(key) & "," & _
                g_dbutil.SQL_String(value) & _
          ")"
    g_cnADO.Execute sql

End Sub


