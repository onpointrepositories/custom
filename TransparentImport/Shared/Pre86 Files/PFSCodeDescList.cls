VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PFSCodeDescList"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'
' This class makes it easy to manipulate code/description lookup lists.
' Data is pulled from g_cnADO.
' Note: All codes are converted to strings.
'
' Sample code:
'
'   Dim cdl as PFSCodeDescList, aCode as String
'
'   cdl.LoadLVC LVC_ADMISSION_CODE, cdlShowCodeWithDesc
'   cdl.FillComboBox cboAdmitCodes
'
'   aCode = cdl.code (cboAdmitCodes.Text)   'Get the code for selected item
'

'These options can be combined (they are bit flags)
Public Enum cdlOptions
    cdlNoOptionSelected = 0                 'Default is to show description only
    cdlShowCodeWithDesc = 1                 'Display code and description
    cdlShowCodeOnly = 2                     'Display code only
    
    cdlInsertAllChoice = 4                  'Add (all) to the list
    cdlInsertNoneChoice = 8                 'Add (none) to the list
    cdlInsertNullStringChoice = 16          'Add a null string to the list
    
    cdlOrderByCode = 32                     'Order by code
    cdlOrderByDesc = 64                     'Order by description
    
    cdlLookupByCodeOnly = 128               'This option allows non-unique descriptions
End Enum

Private m_Code      As Collection           'list of codes
Private m_Desc      As Collection           'list of descriptions
Private m_options   As cdlOptions

Private Sub Class_Initialize()
    Set m_Code = New Collection
    Set m_Desc = New Collection
End Sub

Private Sub Class_Terminate()
    Set m_Code = Nothing
    Set m_Desc = Nothing
End Sub


Public Sub reset()
    Set m_Code = New Collection
    Set m_Desc = New Collection
    m_options = cdlNoOptionSelected
End Sub

'
' Add a code/description pair to the list
'
Friend Sub AddCodeDesc(ByVal code As String, ByVal desc As String)
    On Error GoTo errHandler
    
    'Make it so you can look up code by desc and vice versa
    'Be sure to add strings, not variants or numbers.  (numbers will be mistaken for an index)
    If (m_options And cdlLookupByCodeOnly) Then
        'Don't add to lookup by code
    Else
        m_Code.Add code, desc                   'item=code, key=desc
    End If
    
    m_Desc.Add desc, code                       'item=desc, key=code
    Exit Sub

errHandler:
    'exit quietly
    Exit Sub
End Sub

Private Sub AddOrderClause(ByRef sql As String, ByVal code_col As String, ByVal desc_col As String, ByVal options As Integer)
    If (options And cdlShowCodeWithDesc) Then
        'We're showing codes: order by code
        options = (options Or cdlOrderByCode)
    End If
    
    If (options And cdlOrderByCode) Then
        sql = sql & " ORDER BY " & code_col
    ElseIf (options And cdlOrderByCode) Then
        sql = sql & " ORDER BY " & desc_col
    Else
        sql = sql & " ORDER BY SEQUENCE"
    End If
End Sub
    
'
' Find a code given the description, or index (1-n)
'
Public Function code(ByRef desc As Variant) As String
    On Error GoTo errHandler
    code = m_Code(desc)
    Exit Function
    
errHandler:
    code = ""                                   'invalid index or description
    Exit Function
End Function

'
' Find a description given the code, or index (1-n)
' NOTE: Use Tools->Procedure Attributes to make this the default method
'
Public Function desc(ByRef code As Variant) As String
Attribute desc.VB_UserMemId = 0
    On Error GoTo errHandler
    desc = m_Desc(code)
    Exit Function
    
errHandler:
    desc = ""                                   'invalid index or code
    Exit Function
End Function

'
'Return the description if available, otherwise return the code
'
Public Function DescOrCode(ByRef code As Variant) As String
    On Error GoTo errHandler
    DescOrCode = m_Desc(code)
    Exit Function
errHandler:
    DescOrCode = code
    Exit Function
End Function

'
' Find the index of a code (1-n)
'
Public Function IndexOfCode(ByVal code As String) As Long
    Dim i As Long
    
    On Error GoTo errHandler

    For i = 1 To m_Code.count
        If m_Code.item(i) = code Then
            IndexOfCode = i
            Exit Function
        End If
    Next i
    IndexOfCode = -1
    Exit Function
    
errHandler:
    IndexOfCode = -1
    Exit Function
End Function
'
' Find the code of an index
'
Public Function CodeOfIndex(ByVal index As Long) As String
    On Error GoTo errHandler

    CodeOfIndex = m_Code.item(index)
    Exit Function
errHandler:
    CodeOfIndex = "0"
End Function

'
' Find the listbox index of a code (0 - n-1)
'
Public Function ListIndexOfCode(ByVal code As String) As Long
    ListIndexOfCode = IndexOfCode(code) - 1
End Function

'
' Fill a Combo Box
'(declare ByVal so SecurityCodeDescList will compile)
Public Sub FillComboBox(ByVal cbo As ComboBox)
    FillComboOrList cbo
End Sub

'
' Fill a ListBox
'(declare ByVal so SecurityCodeDescList will compile)
Public Sub FillListBox(ByVal lst As ListBox)
    FillComboOrList lst
End Sub

Private Sub FillComboOrList(ByRef ctl As Control)
    'Fill a combo box or a list box with the descriptions
    Dim textItem As Variant
    With ctl
        .Clear                                  'clear the list
        For Each textItem In m_Desc
            .AddItem textItem                   'add items
        Next
        If .ListCount > 0 Then .ListIndex = 0   'select first item
    End With
End Sub

'
' Fill an ActiveBar ComboList
' (Note: declare ctl as Variant so projects without ActiveBar will compile)
'
Public Sub FillComboList(ByRef ctl As Variant)
    'Fill a combo box or a list box with the descriptions
    Dim textItem As Variant
    With ctl
        .Clear                                  'clear the list
        For Each textItem In m_Desc
            .AddItem textItem                   'add items
        Next
'       If .ListCount > 0 Then .ListIndex = 0   'can't do this
    End With
End Sub

'
' Return the number of items in the list
'
Public Function ItemCount() As Integer
    ItemCount = m_Code.count
End Function

'
' Load the Code/Desc list using the SQL command provided.
' The command can do anything as long as the first two columns are
' code and description.  An optional third column will be added to the description.
'
Public Sub LoadSQL(ByVal sql As String, Optional ByVal options As cdlOptions = cdlNoOptionSelected)
    
    Dim rs As New Recordset
    Dim code As String, desc As String
    
    Me.reset
    m_options = options
    
    'Add optional entries for a null code
    If (options And cdlInsertAllChoice) Then
        AddCodeDesc "", "(all)"
    ElseIf (options And cdlInsertNoneChoice) Then
        AddCodeDesc "", "(none)"
    ElseIf (options And cdlInsertNullStringChoice) Then
        AddCodeDesc "", ""
    End If
    
    'Debug.Print sql
    rs.Open sql, g_cnADO
    
    Do While Not rs.eof
        code = rs(0) & ""
        desc = rs(1) & ""
        If Len(desc) = 0 Then
            desc = "(null)"
        End If
        If (rs.Fields.count = 3) Then
            If Not IsNull(rs(2)) Then
                'add an extended description
                desc = desc & "  (" & rs(2) & ")"
            End If
        End If

        If (options And cdlShowCodeWithDesc) Then
            AddCodeDesc code, code & " - " & desc
        ElseIf (options And cdlShowCodeOnly) Then
            AddCodeDesc code, code
        Else
            AddCodeDesc code, desc
        End If
        rs.moveNext
    Loop
    
    rs.Close
    Set rs = Nothing
End Sub


'
' Load the Code/Desc list using the value item list provided.
' (Note: declare valitems as Variant so projects without TrueDBGrid will compile)
'
Public Sub LoadValueItems(ByRef valitems As Variant)
    Dim i As Integer
    
    Me.reset
    
    For i = 0 To valitems.count - 1
        AddCodeDesc valitems(i).value, valitems(i).DisplayValue
    Next i
End Sub

'
' Load a lookup value code list
'
Public Sub LoadLVC(ByVal lvc_key As Integer, Optional ByVal options As cdlOptions = cdlNoOptionSelected)
    Dim where_clause As String
      
    where_clause = "LOOKUP_DEFINITION_KEY = " & lvc_key
    
    LoadCodeDesc "LOOKUP_VALUE", "LOOKUP_CODE", "LOOKUP_DESCRIPTION", where_clause, options
End Sub

'
' Load a table with code and desc
' This is similar to LoadSQL, only it will add an ORDER BY clause for you.
'
Public Sub LoadCodeDesc( _
    ByVal table As String, ByVal code_col As String, ByVal desc_col As String, _
    Optional ByVal where_clause As String, _
    Optional ByVal options As cdlOptions = cdlNoOptionSelected _
)
    Dim sql As String
    
    sql = "SELECT " & code_col & ", " & desc_col & " FROM " & table
    If Len(where_clause) > 0 Then
        sql = sql & " WHERE " & where_clause
    End If
    
    AddOrderClause sql, code_col, desc_col, options
    LoadSQL sql, options
End Sub


Private Function AddCodeToString(s As String, code As String)
    Dim result As String
    
    result = s
    If Len(result) > 0 Then
        result = result & ","
    End If
    
    'All codes have been stored as strings but SQL will care.
    'If it looks numeric, treat it as numeric.
    'Look out for lists with "(none)" choices and remove them here.
    If IsNumeric(code) Then
        result = result & code                      ' 123,456,789
    ElseIf Len(code) > 0 Then
        result = result & "'" & code & "'"          ' 'RN','LPN','NA'
    End If
    
    AddCodeToString = result
End Function

'
' Return a comma-separated list of ids from the selected list items.
' Returns a zero if no items are selected (to avoid SQL syntax errors).
'
'(declare ByVal so SecurityCodeDescList will compile)
Public Function GetSelectedIDString(ByVal lst As ListBox) As String
    Dim result As String
    Dim i As Integer
    
    For i = 0 To lst.ListCount - 1
        If lst.Selected(i) Then
            result = AddCodeToString(result, m_Code(lst.list(i)))
        End If
    Next i
    
    If Len(result) = 0 Then result = "0"

    GetSelectedIDString = result
End Function

'
' Return a comma-separated list of all ids in the list
'
Public Function GetIDString() As String
    Dim result As String, code As Variant

    For Each code In m_Code
        result = AddCodeToString(result, CStr(code))
    Next code

    GetIDString = result
End Function


'
' Return an array of ids from selected list items (1-n).
' Returns a zero dimensioned array if no items are selected.
'
'(declare ByVal so SecurityCodeDescList will compile)
Public Sub GetSelectedIDArray(ByVal lst As ListBox, ByRef arr() As String)
    Dim result As String
    Dim i As Integer, count As Integer
    
    ReDim arr(0 To lst.SelCount)

    For i = 0 To lst.ListCount - 1
        If lst.Selected(i) Then
            count = count + 1
            arr(count) = m_Code(lst.list(i))
        End If
    Next i
End Sub


'
' Select all items in the list with matching codes
'
'(declare ByVal so SecurityCodeDescList will compile)
Public Sub SelectIDString(ByVal lst As ListBox, ByRef id_list As String)
    Dim arr() As String
    
    'warning: this requires g_util!
    g_util.SplitTextOnChar id_list, ",", arr(), 1, 0
    SelectIDArray lst, arr()
End Sub


'
' Select all items in the list with matching codes
'
'(declare ByVal so SecurityCodeDescList will compile)
Public Sub SelectIDArray(ByVal lst As ListBox, ByRef arr() As String)
    Dim i As Integer, idx As Integer
    
    For i = 0 To lst.ListCount - 1
        lst.Selected(i) = False
    Next i
    
    For i = 1 To UBound(arr)
        idx = ListIndexOfCode(arr(i))
        If (idx >= 0) Then
            lst.Selected(idx) = True
        End If
    Next i
End Sub

'
'
'(declare ByVal so SecurityCodeDescList will compile)
Public Sub SelectComboBoxItem(ByVal cbo As ComboBox, ByVal code As String)
    Dim idx As Integer
    
    idx = ListIndexOfCode(code)
    If (idx >= 0) And (idx < cbo.ListCount) Then
        cbo.ListIndex = idx
    End If
End Sub



'
' Add support for a For...Each loop.
' Note: Use Tools->Procedure Attributes to hide this member and set its ID to -4.
'
Public Function NewEnum() As IUnknown
Attribute NewEnum.VB_MemberFlags = "40"
    Set NewEnum = m_Desc.[_NewEnum]
End Function

