VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PFSColor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'
'PFSColor: control on-screen colors
'
'Set a color scheme this will tell you how to color different things.
'

'Do not renumber these!  They must match values saved in the database.
Public Enum PFSColorScheme
    CS_DEFAULT = 0
    CS_PASTEL = 1
End Enum

'These are things we want to color
Public Enum PFSColorEnum
    'Patient Selection
    COLOR_PATSEL_GRID_DEFAULT
    COLOR_PATSEL_GRID_HISTORICAL_DATE
    COLOR_PATSEL_NOT_CLASSIFIED
    COLOR_PATSEL_CLASSIFICATION_OUT_OF_DATE
    COLOR_PATSEL_DEFAULT_CLASSIFICATION
    COLOR_PATSEL_DO_NOT_CLASSIFY
    COLOR_PATSEL_PROC_OLD
    COLOR_PATSEL_PROC_NO_DEPARTURE
    'Staffing
    COLOR_ACTUAL_STAFFING_GRID
    COLOR_SCHEDULED_STAFFING_GRID
    'Classification
    COLOR_GENERIC_METHODOLOGY_COLOR1
    COLOR_GENERIC_METHODOLOGY_COLOR2
    COLOR_GRAYED_OUT_BACKCOLOR
    COLOR_GRAYED_OUT_FORECOLOR
    COLOR_NORMAL_FORECOLOR
    COLOR_OUTCOMES
    'Monitor
    COLOR_MONITOR_METHODOLOGY_COLOR1
    COLOR_MONITOR_METHODOLOGY_COLOR2
    'Generic
    COLOR_REQUIRED_FIELD
    'Inter-Rater Test
    COLOR_TEST_NOT_ASSIGNED
    COLOR_TEST_ASSIGNED_NOT_STARTED
    COLOR_TEST_STARTED
    COLOR_TEST_COMPLETE
    COLOR_TEST_FAILED
    COLOR_DIFF_TEST_ASSIGNED
    
    COLOR_COUNT
End Enum

'NOTE: VB Color constants are in BGR order, not RGB

'For Saturated colors use vbRed, vbYellow, etc.
Private Const WC_GRAY = &H808080
Private Const WC_ORANGE = &H4080FF

'Medium light colors
Private Const WC_MED_GRAY = &HC0C0C0
Private Const WC_MED_RED = &H4040FF                 '8080FF looks like salmon; make darker
Private Const WC_MED_YELLOW = &H80FFFF
Private Const WC_MED_ORANGE = &H80C0FF
Private Const WC_MED_GREEN = &H80FF80
Private Const WC_MED_CYAN = &HFFFF80
Private Const WC_MED_BLUE = &HFF8080
Private Const WC_MED_MAGENTA = &HFF80FF

'Light colors (pastel)
Private Const WC_LIGHT_GREY = &HE0E0E0
Private Const WC_LIGHT_RED = &HC0C0FF               'pink
Private Const WC_LIGHT_ORANGE = &HC0E0FF
Private Const WC_LIGHT_YELLOW = &HC0FFFF
Private Const WC_LIGHT_GREEN = &HC0FFC0
Private Const WC_LIGHT_CYAN = &HFFFFC0
Private Const WC_LIGHT_BLUE = &HFFC0C0
Private Const WC_LIGHT_MAGENTA = &HFFC0FF

Private m_color_scheme      As String
Private m_colors()          As Long


Private Sub Class_Initialize()
    Me.ColorScheme = CS_DEFAULT
End Sub


Public Property Get ColorScheme() As PFSColorScheme
    ColorScheme = m_color_scheme
End Property

Public Property Let ColorScheme(newval As PFSColorScheme)
    m_color_scheme = newval
    LoadColorScheme
End Property

Public Function Color(index As PFSColorEnum) As Long
    Color = m_colors(index)
End Function


Private Sub LoadColorScheme()
    ReDim m_colors(0 To COLOR_COUNT)

    'These colors could be loaded from the database but for now they are
    'hard-coded.  If we ever want the users to be able to customize their
    'colors, we could put these in the database instead.
    m_colors(COLOR_GRAYED_OUT_BACKCOLOR) = WC_LIGHT_GREY
    m_colors(COLOR_GRAYED_OUT_FORECOLOR) = vbGrayText
    m_colors(COLOR_NORMAL_FORECOLOR) = vbWindowText
    
    Select Case m_color_scheme
    Case CS_DEFAULT
        m_colors(COLOR_PATSEL_GRID_DEFAULT) = vbWindowBackground
        'm_colors(COLOR_PATSEL_GRID_HISTORICAL_DATE) = vbCyan       'too dark
        m_colors(COLOR_PATSEL_GRID_HISTORICAL_DATE) = WC_MED_CYAN
        m_colors(COLOR_PATSEL_NOT_CLASSIFIED) = vbRed
        m_colors(COLOR_PATSEL_CLASSIFICATION_OUT_OF_DATE) = vbYellow
        m_colors(COLOR_PATSEL_DEFAULT_CLASSIFICATION) = WC_MED_MAGENTA
        m_colors(COLOR_PATSEL_DO_NOT_CLASSIFY) = WC_ORANGE
        m_colors(COLOR_PATSEL_PROC_OLD) = vbGrayText
        m_colors(COLOR_PATSEL_PROC_NO_DEPARTURE) = vbRed

        'm_colors(COLOR_ACTUAL_STAFFING_GRID) = vbGreen             'too dark
        m_colors(COLOR_ACTUAL_STAFFING_GRID) = WC_MED_GREEN
        m_colors(COLOR_SCHEDULED_STAFFING_GRID) = vbWindowBackground
    
        m_colors(COLOR_GENERIC_METHODOLOGY_COLOR1) = vbWhite
        'm_colors(COLOR_GENERIC_METHODOLOGY_COLOR2) = vbCyan        'too dark
        m_colors(COLOR_GENERIC_METHODOLOGY_COLOR2) = WC_MED_CYAN
        m_colors(COLOR_REQUIRED_FIELD) = vbYellow
        
        m_colors(COLOR_MONITOR_METHODOLOGY_COLOR1) = vbWhite
        m_colors(COLOR_MONITOR_METHODOLOGY_COLOR2) = WC_MED_GREEN
        m_colors(COLOR_OUTCOMES) = WC_MED_YELLOW
        
        m_colors(COLOR_TEST_NOT_ASSIGNED) = vbWhite
        m_colors(COLOR_TEST_ASSIGNED_NOT_STARTED) = WC_MED_YELLOW
        m_colors(COLOR_TEST_STARTED) = WC_MED_ORANGE
        m_colors(COLOR_TEST_COMPLETE) = WC_MED_GREEN
        m_colors(COLOR_TEST_FAILED) = WC_MED_RED
        m_colors(COLOR_DIFF_TEST_ASSIGNED) = WC_MED_GRAY

    Case CS_PASTEL
        m_colors(COLOR_PATSEL_GRID_DEFAULT) = vbWindowBackground
        m_colors(COLOR_PATSEL_GRID_HISTORICAL_DATE) = WC_LIGHT_CYAN
        m_colors(COLOR_PATSEL_NOT_CLASSIFIED) = WC_MED_RED
        m_colors(COLOR_PATSEL_CLASSIFICATION_OUT_OF_DATE) = WC_MED_YELLOW
        m_colors(COLOR_PATSEL_DEFAULT_CLASSIFICATION) = WC_LIGHT_MAGENTA
        m_colors(COLOR_PATSEL_DO_NOT_CLASSIFY) = WC_MED_ORANGE
        m_colors(COLOR_PATSEL_PROC_OLD) = vbGrayText
        m_colors(COLOR_PATSEL_PROC_NO_DEPARTURE) = WC_MED_RED

        m_colors(COLOR_ACTUAL_STAFFING_GRID) = WC_LIGHT_GREEN
        m_colors(COLOR_SCHEDULED_STAFFING_GRID) = vbWindowBackground

        m_colors(COLOR_GENERIC_METHODOLOGY_COLOR1) = vbWhite
        m_colors(COLOR_GENERIC_METHODOLOGY_COLOR2) = WC_LIGHT_CYAN
        m_colors(COLOR_REQUIRED_FIELD) = WC_MED_YELLOW

        m_colors(COLOR_MONITOR_METHODOLOGY_COLOR1) = vbWhite
        m_colors(COLOR_MONITOR_METHODOLOGY_COLOR2) = WC_LIGHT_GREEN
        
        m_colors(COLOR_OUTCOMES) = WC_LIGHT_YELLOW
        
        m_colors(COLOR_TEST_NOT_ASSIGNED) = vbWhite
        m_colors(COLOR_TEST_ASSIGNED_NOT_STARTED) = WC_LIGHT_YELLOW
        m_colors(COLOR_TEST_STARTED) = WC_LIGHT_ORANGE
        m_colors(COLOR_TEST_COMPLETE) = WC_LIGHT_GREEN
        m_colors(COLOR_TEST_FAILED) = WC_LIGHT_RED
        m_colors(COLOR_DIFF_TEST_ASSIGNED) = WC_LIGHT_GREY

    End Select
End Sub


'Decode graph colors from lookup list #60
'
Public Sub GetGraphColorsFromLookupCode(code As String, arr() As Long)
    Dim s As String
    Dim n As Integer

    'Each code has an ordering, dot, and color code(s): nn.CCCC
    'Each color code is one character.  Multiple colors represent a gradient or
    'rotating colors.

    
    n = InStr(1, code, ".")
    If (n > 0) Then
        s = Mid$(code, n + 1)
    Else
        s = code
    End If
    
    If Len(s) = 0 Then s = "1"              'default red if nothing given
    
    ReDim arr(0 To Len(s))
    For n = 1 To Len(s)
        arr(n) = GraphColorFromLookupCode(Mid$(s, n, 1))
    Next n
End Sub

Public Function GraphColorFromLookupCode(code As String) As Long
    Dim result As Long
    
    Select Case UCase$(code)
    'basic DOS 16 colors
    Case "0":   result = vbBlack
    Case "1":   result = vbRed
    Case "2":   result = vbYellow
    Case "3":   result = vbGreen
    Case "4":   result = vbCyan
    Case "5":   result = vbBlue
    Case "6":   result = vbMagenta
    Case "7":   result = WC_GRAY
    Case "8":   result = WC_MED_GRAY
    Case "9":   result = WC_LIGHT_RED
    Case "A":   result = WC_LIGHT_YELLOW
    Case "B":   result = WC_LIGHT_GREEN
    Case "C":   result = WC_LIGHT_CYAN
    Case "D":   result = WC_LIGHT_BLUE
    Case "E":   result = WC_LIGHT_MAGENTA
    Case "F":   result = vbWhite
    'extended colors
    Case "G":   result = WC_ORANGE
    Case "H":   result = WC_LIGHT_ORANGE
    Case Else
        result = vbBlack
    End Select

    GraphColorFromLookupCode = result
End Function
