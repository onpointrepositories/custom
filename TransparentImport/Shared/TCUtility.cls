VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "TCUtility"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'
' Transparent classification utilities
'
Const DNLD_FNAME = "TCQUERY"                'prefix of import filename

Public Function GetAllImportFilenames(import_path As String, fnames() As String) As Integer
    On Error GoTo errHandler
    'The folder location is determined by the first line of the MSDICT dictionary.
    Const INITIAL_BLOCK_SIZE = 24
    Dim i As Integer
    Dim infname As String, wildcard As String
    
    ReDim fnames(0 To INITIAL_BLOCK_SIZE) As String
    i = 0
    
    wildcard = import_path + "\" & DNLD_FNAME & "*.TXT"
    infname = Dir$(wildcard) 'returns ONLY the filename.  For wildcards.
    While infname <> ""
        i = i + 1
        If (i > UBound(fnames)) Then
            ReDim Preserve fnames(0 To i + INITIAL_BLOCK_SIZE) As String
        End If
        fnames(i) = infname
        infname = Dir$
    Wend
    
    BubbleSort fnames, i
    
    GetAllImportFilenames = i
    Exit Function
errHandler:
    g_util.ThrowError "", "Scanning for query files; path = " & wildcard
End Function


Public Sub BubbleSort(arr As Variant, numEls As Integer)
    Dim lLoop1 As Integer
    Dim lLoop2 As Integer
    Dim lTemp As Variant
    
    For lLoop1 = numEls To LBound(arr) Step -1
       For lLoop2 = LBound(arr) + 1 To lLoop1
           If arr(lLoop2 - 1) > arr(lLoop2) Then
              lTemp = arr(lLoop2 - 1)
              arr(lLoop2 - 1) = arr(lLoop2)
              arr(lLoop2) = lTemp
           End If
       Next lLoop2
   Next lLoop1
End Sub


