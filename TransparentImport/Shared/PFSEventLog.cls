VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PFSEventLog"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'
' PFSEventLog: Add to the event log
'
' IF YOU ADD TO THESE LISTS, BE SURE TO ADD TO THE DATABASE (LOOKUP_VALUE) AS WELL.
' DO NOT CHANGE ANY EXISTING NUMBERS!
'
Const MAX_EVENT_LIFE_IN_DAYS = 45

Const UNCLOSED_QUOTE_WARNING = "(unclosed quote)"

Public Enum EventLogSource                  'Lookup list #66
    EVENT_SOURCE_NONE = 0
    EVENT_SOURCE_HL7 = 1
    EVENT_SOURCE_BATCH_IMPORT = 2
    EVENT_SOURCE_BATCH_EXPORT = 3
    EVENT_SOURCE_REPORT_CALCULATIONS = 4    '(from AutoReport.exe and audit report)
    EVENT_SOURCE_TRANSPARENT_IMPORT = 5     'batch import
    EVENT_SOURCE_TRANSPARENT_MAPPING = 6    'mapping only (generates an import file)
    EVENT_SOURCE_DATA_CONSISTENCY = 7
End Enum

Public Enum EventLogType                    'Lookup list #67
    EVENT_TYPE_NONE = 0
    EVENT_TYPE_INFO = 1                     'generic message
    EVENT_TYPE_WARNING = 2
    EVENT_TYPE_ERROR = 3
    EVENT_TYPE_REJECT = 4
End Enum

Public Enum EventLogCategory                'Lookup list #68
    EVENT_CATEGORY_NONE = 0                 'generic category
    EVENT_CATEGORY_STARTUP_SHUTDOWN = 1     'startup or shutdown message
    EVENT_CATEGORY_VALIDATION = 2           'a validation error
    EVENT_CATEGORY_UNEXPECTED = 3           'an unexpected (internal) error
    EVENT_CATEGORY_PROCESSED = 4            'a processed message
End Enum

Private lastEventDate   As Date



'
' Add an item to the event log
'
Public Sub DoAddEventLogEntry( _
    ByVal EventSource As EventLogSource, _
    ByVal EventType As EventLogType, _
    ByVal EventCategory As EventLogCategory, _
    ByRef Description As Variant, _
    ByRef SourceText As Variant, _
    ByVal TCPPort As Long, _
    ByVal UnitID As Long, _
    ByVal EncounterID As Long, _
    ByVal EventID As Long, _
    ByRef MapperVersion As Variant, _
    ByRef BriefAudit As Variant, _
    ByRef VerboseAudit As Variant _
)
    On Error GoTo errHandler
    
    Dim sql As String
    Dim src As Variant
    
    If Len(Description) = 0 Then Exit Sub           'don't log empty messages

    'Source text often has a trailing cr/lf - strip it here.
    'Convert null SourceText to null string or StripCrLf will blow up (so will Left$)
    src = g_util.StripCrLf(SourceText & "")

    sql = "INSERT INTO EVENT_LOG (" & _
         "      TIMESTAMP, EVENT_SOURCE, EVENT_TYPE, EVENT_CATEGORY," & _
         "      DESCRIPTION, SOURCE_TEXT, TCP_PORT, UNIT_ID, ENCOUNTER_ID," & _
         "      TC_EVENT_ID, MAPPER_VERSION, BRIEF_AUDIT, VERBOSE_AUDIT" & _
         " ) VALUES (" & _
                "GETDATE()," & _
                EventSource & "," & _
                EventType & "," & _
                EventCategory & "," & _
                g_dbutil.SQL_String(Left$(Description, 1024)) & "," & _
                g_dbutil.SQL_String(Left$(src, 4096)) & "," & _
                g_dbutil.SQL_ZeroToNull(TCPPort) & "," & _
                g_dbutil.SQL_ZeroToNull(UnitID) & "," & _
                g_dbutil.SQL_ZeroToNull(EncounterID) & "," & _
                g_dbutil.SQL_ZeroToNull(EventID) & "," & _
                g_dbutil.SQL_String(Left$(MapperVersion & "", 32)) & "," & _
                g_dbutil.SQL_String(BriefAudit) & "," & _
                g_dbutil.SQL_String(VerboseAudit) & _
         " )"
'    Debug.Print "Event source=" & EventSource & " type=" & EventType & " cat=" & EventCategory & _
'        " desc='" & Description & "' source='" & Left$((SourceText & ""), 16) & "...'" & _
'        " tcp=" & TCPPort & " unit=" & UnitID & " enc=" & EncounterID
    'Debug.Print sql
    g_cnADO.Execute sql
    
    '
    'Remove old events each time the date changes -
    'This is for the HL7 interface which runs continuously without startup/shutdown.
    '
    If (year(lastEventDate) > 1900) And (lastEventDate <> Date) Then
        RemoveOldEvents
        lastEventDate = Date
    End If
    
    Exit Sub

errHandler:
    'Do not throw the error because this is the place where errors get recorded so it gets recursive.
    'Exit quietly instead.
    
    If g_dbutil.IsFatalDatabaseError(Err.Number, Err.Description) Then
        'do not try to re-open the connection           [7666]
        Exit Sub
    End If
    
    If InStr(1, Err.Description, SQL_INSTR_DUPLICATE_KEY, vbTextCompare) Then
        'Try again with a new timestamp (.1 sec delay is fine)
        g_util.Sleep 10
        Resume
    End If
    If InStr(1, Err.Description, "timeout", vbTextCompare) Then
        'Try again if there is a timeout.  A search or delete may have locked the table? [7431]
        g_util.Sleep 500
        Resume
    End If
    If InStr(1, Err.Description, "UNIT_ID") Then
        'We were given an invalid unit_id - try again without it
        AddEventLogEntry EventSource, EventType, EventCategory, Description, src, TCPPort, 0, EncounterID
        Exit Sub
    End If
    If InStr(1, Err.Description, "ENCOUNTER_ID") Then
        'We were given an invalid encounter_id - try again without it
        AddEventLogEntry EventSource, EventType, EventCategory, Description, src, TCPPort, UnitID, 0
        Exit Sub
    End If
    If InStr(1, Err.Description, "unclosed quotation", vbTextCompare) Then
        'Either description or source got an unclosed quote error; most likely the source
        If (SourceText <> UNCLOSED_QUOTE_WARNING) Then
            'Try again without the source string; replace it with a warning
            AddEventLogEntry EventSource, EventType, EventCategory, _
                Description, UNCLOSED_QUOTE_WARNING, TCPPort, UnitID, EncounterID
        ElseIf (Description <> UNCLOSED_QUOTE_WARNING) Then
            'Try again without the description (should never get here)
            AddEventLogEntry EventSource, EventType, EventCategory, _
                UNCLOSED_QUOTE_WARNING, UNCLOSED_QUOTE_WARNING, TCPPort, UnitID, EncounterID
        End If
        Exit Sub
    End If
    
    'MsgBox "Error " & Err.Number & " Desc=" & Err.Description & vbCrLf & vbCrLf & "sql=" & sql
    
    Exit Sub
    
    Resume      'debug
End Sub

'Generic (original) Add Log Entry
Public Sub AddEventLogEntry( _
    ByVal EventSource As EventLogSource, _
    ByVal EventType As EventLogType, _
    ByVal EventCategory As EventLogCategory, _
    ByRef Description As Variant, _
    ByRef SourceText As Variant, _
    ByVal TCPPort As Long, _
    ByVal UnitID As Long, _
    ByVal EncounterID As Long _
)
    DoAddEventLogEntry EventSource, EventType, EventCategory, _
        Description, SourceText, TCPPort, UnitID, EncounterID, _
        0, Null, Null, Null
End Sub

'Transparent classification specific entry
Public Sub AddTransparentMappingEventLogEntry( _
    ByRef Description As Variant, _
    ByVal UnitID As Long, _
    ByVal EncounterID As Long, _
    ByVal EventID As Long, _
    ByRef MapperVersion As String, _
    ByRef BriefAudit As String, _
    ByRef VerboseAudit As String _
)
    DoAddEventLogEntry EVENT_SOURCE_TRANSPARENT_MAPPING, EVENT_TYPE_INFO, EVENT_CATEGORY_PROCESSED, _
        Description, Null, 0, UnitID, EncounterID, _
        EventID, MapperVersion, BriefAudit, VerboseAudit
End Sub


'
' Batch interfaces should call this during startup or shutdown.
' HL7 calls this  (indirectly) each midnight.
'
Public Sub RemoveOldEvents()
    On Error GoTo errHandler
    
    Dim sql As String
    Dim dt As Date

    If InStr(1, g_command, "-keepoldevents") Then Exit Sub

    dt = DateAdd("d", -MAX_EVENT_LIFE_IN_DAYS, Date)
    sql = "DELETE FROM EVENT_LOG WHERE TIMESTAMP < " & g_dbutil.SQL_Date(dt)
    Debug.Print sql
    g_cnADO.Execute sql
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "RemoveOldEvents"
End Sub
