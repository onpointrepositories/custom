﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Xml;
//using System.Xml.Linq;
using System.Data.SqlClient;                // for SqlConnection
using System.IO;                            // for Path
using PfsShared;

//
// Orders Data Reader for Lawrence 
//     (and any other places that need Orders to be read into chart_items)
// To be run hourly.  
// Looks for all event_log data that are non-rejected O01 messages.
//    get acct num from PID-18
//        look for Order ID for this patient.
//        if it exists, then check if the timestamp is different from the eventlog timestamp
//        if timestamp different, then add this order into chart items
//
// This is a console program, so no message boxes!
//

namespace OrdersReader
{
    static class Program
    {
        const string ORDERS_AUDIT_PATH = "C:\\Users\\Public\\AcuityPlus\\Log\\";
        const string ORDERS_AUDIT_FILE = "OrdersAudit.log";
        public static bool _debug;

        static string _path;
        static string _file;
        static bool _norename;
        static bool _nodelete;
        static bool _pause;
        static bool g_log = false;
        static int g_lookback = 2; //hours
        public static StreamWriter  logfile;
        static int seq = 1;

        static void Main(string[] args)
        {
            try
            {
                ParseCommandLine(args);
                OpenLogFile();
                ReadInOrders();
                CloseLogFile();
            }
            catch(Exception e)
            {
                Console.WriteLine("Unexpected Error: {0}", e.Message);
                Console.WriteLine("{0}", e.StackTrace);
                LogUnexpectedError(e.Message, e.StackTrace);
            }

            if (_debug || _pause)
            {
                Console.WriteLine("\n");
                Console.Write("Press any key...");
                Console.ReadKey();                          // only in "pause" mode
            }
        }

        static void ParseCommandLine(string[] args)
        {
            string value;

            foreach (var arg in args)
            {
                var arr = arg.Split('=');
                value = (arr.GetUpperBound(0) > 0) ? arr[1] : "";
                
                switch(arr[0])
                {
                    case "-debug":
                        _debug = true;
                        break;
                    case "-log":                                // create TransparentAudit.log
                        g_log = true;                           // (verbose audit)
                        break;

                    case "-pause":
                        _pause = true;
                        break;
                    case "-lookback":
                        // looks back number of hours in event log 
                        g_lookback = value.ToInteger();
                        break;
                    default:
                        if (arr[0].Left(1) != "-")
                        {
                            _path = arr[0];                 // assume it is a path
                        }
                        else
                        {
                            Console.WriteLine("unexpected argument: {0}", arg);
                        }
                        break;
                }
            }
        }

        static void OpenLogFile()
        {
            if (!g_log) return;
            // re-write new file
            DateTime nowdt = DateTime.Now;
            string nowdate;
            string nowtime;
            nowdate = nowdt.ToString("yyyyMMdd");
            nowtime = nowdt.ToString("HHmm");
            logfile = new StreamWriter(Path.Combine(ORDERS_AUDIT_PATH, nowdate + nowtime + ORDERS_AUDIT_FILE));
        }

        static void CloseLogFile()
        {
            if (!g_log) return;
            if (logfile != null) {
                logfile.Close();
                logfile = null;
            }

        }

        static void ReadInOrders()
        {
            DateTime nowdt = DateTime.Now;
            string or_code;
            string or_desc;
            string or_id; 
            DateTime or_evdt;
            string or_ctrl;
            string or_status;

            var db = PFSUtility.NewPfsDataContext();
            var query = from item in db.EVENT_LOGs
                        where (item.DESCRIPTION.Contains("O01"))
                        where (item.EVENT_SOURCE == 1)
                        where (item.EVENT_TYPE == 1)
                        where (item.EVENT_CATEGORY == 4)
                        where (item.TCP_PORT == 5300)
                        where (item.TIMESTAMP >= nowdt.AddHours(-2))
                        select item;
            if (query.Count() == 0) return;
            foreach (var ev in query)
            {
                if (EncounterExists((int)ev.ENCOUNTER_ID))
                {
                    GetOrderData(ev.SOURCE_TEXT, out or_code, out or_desc, out or_id, out or_evdt, out or_ctrl, out or_status);

                    if (!OrderExists((int)ev.ENCOUNTER_ID, or_id, or_code, or_evdt, or_ctrl))
                    {
                        AddOrder((int)ev.ENCOUNTER_ID, or_code, or_desc, or_id, or_evdt, or_ctrl, or_status);
                    }
                }
            }

        }

        static bool EncounterExists(int encid)
        {
            var db = PFSUtility.NewPfsDataContext();
            var query = from item in db.ENCOUNTERs
                        where (item.ENCOUNTER_ID == encid)
                        select item;
            return (query.Count() > 0);
        }

        static bool OrderExists(int encid, string or_id, string or_code, DateTime or_evdt, string or_ctrl)
        {
            if (logfile != null)
                logfile.WriteLine("OrderExists: encid=" + encid + "  orid=" + or_id + "  orcode=" + or_code + "  orctrl=" + or_ctrl);
            var db = PFSUtility.NewPfsDataContext();
            var query = from item in db.CHART_ITEMs
                        where (item.ENCOUNTER_ID == encid)
                        where (item.ORDER_ID == or_id)
                        where (item.CODE == or_code)
                        where (item.ORDER_CONTROL == or_ctrl)
                        select item;
            return (query.Count() > 0);
        }

        static void GetOrderData(string source, 
                                 out string orcode,
                                 out string ordesc,
                                 out string orid, 
                                 out DateTime orevdt, 
                                 out string orctrl,
                                 out string orstatus)
        {
        // Get the HL7MessageSegments within a message
            int ct = 0;
            int fct = 0;

            orcode = "";
            ordesc = "";
            orid = "";
            orevdt = DateTime.MinValue;
            orctrl = "";
            orstatus = "";

            char HL7SegmentTerminator = (char)0x0D;
            var arr = source.Split(HL7SegmentTerminator);
            foreach (var seg in arr)
            {
                if (seg.StartsWith("ORC|")) 
                {
                    var segarr = seg.Split('|');
                    ct = 0;
                    foreach (var field in segarr)
                    {
                        if (ct == 1) orctrl = field;
                        if (ct == 2) orid = field;
                        if (ct == 9) orevdt = PFSUtility.ISOToDateTime(field); 

                        if (logfile != null) 
                            if (field.Trim() != "")
                                logfile.WriteLine(ct + ": " + field);
                        ct++;
                    }
                }
                else if (seg.StartsWith("OBR|"))
                {
                    var segarr = seg.Split('|');
                    ct = 0;
                    foreach (var field in segarr)
                    {
                        if (ct == 4)
                        {
                            var fldarr = field.Split('^');
                            fct = 1;
                            foreach (var subfld in fldarr)
                            {
                                if (fct == 1) orcode = subfld;
                                if (fct == 2) ordesc = subfld.Replace('\'',' ');
                                fct++;
                            }
                        }
                        if (logfile != null)
                            if (field.Trim() != "")
                                logfile.WriteLine(ct + ": " + field);
                        ct++;
                    }
                }

            }
            if (logfile != null)
            {
                logfile.WriteLine("ctrl  =" + orctrl);
                logfile.WriteLine("id    =" + orid);
                logfile.WriteLine("code  =" + orcode);
                logfile.WriteLine("desc  =" + ordesc);
                logfile.WriteLine("evdt  =" + orevdt.ToString());
                logfile.WriteLine("status=" + orstatus);
            }


        }

        static void AddOrder(int encid,
                             string orcode,
                             string ordesc,
                             string orid,
                             DateTime orevdt,
                             string orctrl,
                             string orstatus)
        {
//        void SaveChartItem(PfsDataContext pfs)
            if (logfile != null)
                logfile.WriteLine("AddOrder: encid=" + encid + "  orcode=" + orcode + "  ordesc=" + ordesc + "  orid=" + orid + "  orctrl=" + orctrl + "  evdt=" + orevdt.ToString());
            var pfs = PFSUtility.NewPfsDataContext();
            var ci = new CHART_ITEM();

            string format = "yyyyMMddHHmm";
            ci.EVENT_DATETIME = orevdt;
            ci.ENCOUNTER_ID = encid;
            ci.UNIT_ID = LookupUnitID(encid,orevdt);
            ci.CODE = orcode;
            ci.DESCRIPTION = ordesc;
            ci.ORDER_ID = orid;
            ci.ORDER_CONTROL = orctrl;
            ci.TIMESTAMP = DateTime.Now;
            ci.SEQUENCE = (short)seq++;

            pfs.CHART_ITEMs.InsertOnSubmit(ci);
            SubmitChanges(pfs);
        }
        static int LookupUnitID(int encid, DateTime dt)
        {
            int uid = -1;
            var db = PFSUtility.NewPfsDataContext();
            var query = from item in db.ENCOUNTER_LOCATIONs
                        where (item.ENCOUNTER_ID == encid)
                        where (item.EFFECTIVE_DATETIME_IN <= dt)
                        where (item.EFFECTIVE_DATETIME_OUT >= dt)
                        select item;
            if (query.Count() > 0)
                foreach (var el in query)
                {
                    uid = el.UNIT_ID;
                }
            return uid;
        }

        static void SubmitChanges(PfsDataContext pfs)
        {
            bool moreToSubmit = true;
            do
            {
                try
                {
                    pfs.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
                    moreToSubmit = false;
                }
                catch (System.Data.Linq.ChangeConflictException)
                {
                    foreach (System.Data.Linq.ObjectChangeConflict occ in pfs.ChangeConflicts)
                    {
                        // All database values overwrite current values with 
                        //values from database
                        occ.Resolve(System.Data.Linq.RefreshMode.OverwriteCurrentValues);
                    }
                }
            }
            while (moreToSubmit);

        }

        static void ProcessFile(string pathname)
        {
            //var reader = new DataReader();

            //Console.WriteLine("Processing {0}", pathname);
            //if (reader.process(pathname)) {

            //    string newname = string.Format(@"{0}\{1}_{2:yyyyMMddHHmmss}.TXT",
            //        Path.GetDirectoryName(pathname),
            //        Path.GetFileNameWithoutExtension(pathname),
            //        DateTime.Now);

            //    if (_norename) {
            //        DebugTrace("Would rename to {0}", newname);
            //    } else {
            //        // rename the processed file
            //        DebugTrace("Rename to {0}", newname);
            //        File.Delete(newname);               // in case it already exists (no error if not)
            //        File.Move(pathname, newname);       // rename
            //    }
            //}
        }

        static void DeleteTheseOldFiles(string pattern)
        {
            //string[] files = Directory.GetFiles(_path, pattern);

            //foreach (string file in files)
            //{
            //    FileInfo fi = new FileInfo(file);
            //    if (fi.LastWriteTime < DateTime.Now.AddDays(-MAX_FILE_LIFE_IN_DAYS))
            //    {
            //        if (_nodelete)
            //            DebugTrace("Would delete {0}", file);
            //        else
            //        {
            //            DebugTrace("Delete {0}", file);
            //            fi.Delete();
            //        }
            //    }
            //}
        }
        
        public static void DebugTrace(string format, params object[] values)
        {
            if (_debug)
            {
                Console.WriteLine(format, values);
            }
        }

        public static void LogInfo(string msg, string source)
        {
            PFSEventLog.AddEventLogEntry(
                PFSEventLog.EventLogSource.EVENT_SOURCE_TRANSPARENT_MAPPING,
                PFSEventLog.EventLogType.EVENT_TYPE_INFO,
                PFSEventLog.EventLogCategory.EVENT_CATEGORY_PROCESSED,
                msg, source, 0, 0, 0);
        }

        public static void LogValidationError(string msg, string source)
        {
            Console.WriteLine(msg);
            PFSEventLog.AddEventLogEntry(
                PFSEventLog.EventLogSource.EVENT_SOURCE_TRANSPARENT_MAPPING,
                PFSEventLog.EventLogType.EVENT_TYPE_ERROR,
                PFSEventLog.EventLogCategory.EVENT_CATEGORY_VALIDATION,
                msg, source, 0, 0, 0);
        }

        public static void LogUnexpectedError(string msg, string source)
        {
            Console.WriteLine(msg);
            PFSEventLog.AddEventLogEntry(
                PFSEventLog.EventLogSource.EVENT_SOURCE_TRANSPARENT_MAPPING,
                PFSEventLog.EventLogType.EVENT_TYPE_ERROR,
                PFSEventLog.EventLogCategory.EVENT_CATEGORY_UNEXPECTED,
                msg, source, 0, 0, 0);
        }
    }
}
