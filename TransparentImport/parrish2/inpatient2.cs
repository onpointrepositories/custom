﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;                     // for LINQ; add a reference
using System.Data.SqlClient;                // for SqlConnection
using System.IO;                            // for Directory, Path, Stream
using PfsShared;                            // add a reference to Shared2 project

// ============================================================================
// Inpatient 2.0 transparent mapping -- GOES HERE --
// (The code below is a sample from Sharp)
// ============================================================================
// This processes one patient.  Only one database query is used.  :)
//
// All search functions use exact match for category, description and field name.
// Codes and results can be a single word or a list of words.
// Codes default to exact match; prefix with CODE_CODE_LIKE_PREFIX if desired (single word only)
// Results default to like matching; prefix with RESULT_EXACT_MATCH_PREFIX if desired (single word only)
//
// All searches are case insensitive.
//
//
namespace TransparentMapping
{
    class Inpatient2
    {
        private const int MAX_INDS = 50;
        private const int MAX_PROCS = 20;

        private const string CODE_LIKE_PREFIX = "%!";       // use to make codes %like%
        private const string EXACT_MATCH_PREFIX = "&!";     // use to make desc/result exact match
        private const string CHAR_COMMA = "||";             // use to insert literal comma in word list
        private const string NOT_PREFIX = "!!";             // use to insert literal comma in word list

        private struct indicator_data {
            public bool     is_checked;     
            public int      radio_group;       
        }

        private struct proc_data {                      
            public int      procedure_number;
            public DateTime start;   
            public DateTime finish;
        }

        // These are database CHART_ITEMs for this patient
        private CHART_ITEM[] _chart_items_since_admission;
        private CHART_ITEM[] _chart_items_since_unit_arrival;
        private CHART_ITEM[] _chart_items_during_pull_period;
        private CHART_ITEM[] _chart_items_past24hrs;
        // These are database PROCEDURE_EVENTs for this patient
        private PROCEDURE_EVENT[] _procedure_events;

        private indicator_data[] _inds;
        private List<proc_data> _procs;
        private List<proc_data> _outcomes;

        private const int DEFAULT_BUCKET_SIZE = 20;       //min

        private PatientInfo _pat;
        private bool adl23;
        private bool adl4;
        private bool tubefeed;
        private bool coma;
        private int  _bucket_size = DEFAULT_BUCKET_SIZE;
        private bool is_default;


        private enum SearchDepth {
            SearchDefault,
            SearchPullRange,            //search within the current pull     -- default
            SearchPast24Hrs,
            SearchSinceArrival,         //search since arrival to the unit
            SearchSinceAdmission        //search everything since admission to the hospital
        }

        private enum CountMode {
            CountAll,
            CountFirst                  //stop after one is found
        }

        private enum GetValueMode {
            GetTotal,
            GetMax,
            GetLast
        }

        enum Frequencies {
            QNONE,
            Q4H,
            Q2H,
            Q1H,
            Q30M
        }

        private struct fmapRow {
            public double   los_high;                       //the LOS being testing
            public int[]    freq;                           //the count required for each Q value
        }
        private List<fmapRow> _freq_map;                    //1,2,4,8,12,24 hours

     
        //
        // This is the main entry point
        //
        public void ProcessPatient(PatientInfo pat)
        {
            bool elig_for_default = false;
            bool no7am_exists = false;
            _pat = pat;

            InitIndicators(); // sets is_default
            InitProcs();
            if (! is_default)
                {
                LoadFreqTable();
                LoadPatientChart();
                Check_1_2_3_4();
                Check_5();
                Check_6_7();
                Check_8();
                Check_9();
                Check_10_11();
                Check_12_13();
                Check_14();
                Check_15_16_17_18();
                Check_19();
                Check_20();
                Check_21_22();
                Check_23();
                Check_24();
            }
            AtLeastOneADL();
            HighestIndicatorInEachGroupWins();

            //if (!is_default)
            //{
            CheckProcs();
            //    CheckOutcomes();
            //}

            if (_pat.default_ptype > DeterminePtypeOfIndicators(_pat.meth_id))
            { // if the default pt type is higher than the pt type of this class
                // then if there is a default classification in the past 16 hrs
                // then make these indicators the default indicators.
                elig_for_default = true;
                Program.VerboseAudit("Eligible for default indicators");
            }


            if (Program.g_no_output) return;

            //foreach (var ploc in Program.patloclist)
            //{
            //    Audit("unitid=" + ploc.unit_id + " start= " + ploc.in_time + " end=" + ploc.out_time);
            //}
            const string DATETIME_FORMAT = "yyyyMMddHHmm";
            const string DATE_FORMAT = "yyyyMMdd";
            const string TIME_FORMAT = "HHmm";
            string str_date = Program.g_pull_finish.ToString(DATE_FORMAT); //tod7am or tod7pm
            string str_time = Program.g_pull_finish.ToString(TIME_FORMAT);
            string str_yest_date = Program.g_pull_finish.AddDays(-1).ToString(DATE_FORMAT);
            DateTime start_dt;
            int save_unit_id = -1;
            int save_methid = -1;
            bool is_7am = false;
            if (str_time == "0700")
            {
                is_7am = true;
            }
            start_dt = Program.g_pull_finish;

            bool did_this_pt=false;
            foreach (var ploc in Program.patloclist)
            {
                if (!did_this_pt)
                {
                    if ((ploc.unit_id != -1) && (ploc.methid == 18))
                    {
                        if (ploc.out_time >= start_dt)
                        {
                            did_this_pt = true;
                            if (is_7am)
                                OutputClass(Convert.ToInt32(elig_for_default), ploc.unit_id);
                            else
                                OutputClass(0, ploc.unit_id);
                        }
                    }
                }
            } // for


            OutputProcs();
            //OutputOutcomes();
        }

        private string GetUnitName(int unitid)
        {
            string result = "";
            //Program.VerboseAudit("GetUnitName");
            var db = PFSUtility.NewPfsDataContext();
            var query = from u in db.UNITs
                        where (u.UNIT_ID == unitid)
                        select u.NAME;
            foreach (var urec in query)
            {
                result = urec.ToString();
            }
            Program.VerboseAudit("Unit=" + result);
            return result;
        }
       
        private void InitIndicators()
        {
            // Make a new (empty) indicator array
            _inds = new indicator_data[MAX_INDS + 1];           // This 1 based so add one
            is_default = false;
            //if (_pat.los_hours <= 4.0) {
            //    is_default = true;
            //    Program.VerboseAudit("Patient was here 4 hrs or less. Will receive default indicators " + _pat.default_inds_str);
            //    foreach (var ind in _pat.default_inds) {
            //        if (ind <= _inds.GetUpperBound(0)) {
            //            _inds[ind].is_checked = true;
            //        }
            //    }
            //}

            adl23 = false;
            adl4 = false;
            tubefeed = false;
            coma = false;

            // get indicator radio groups from the database
            // ** (This database access can be replaced once we have a C# methodolgy cache)
            var db = PFSUtility.NewPfsDataContext();
            var query = from idef in db.INDICATOR_DEFINITIONs
                        where (idef.METHODOLOGY_ID == _pat.meth_id)
                        select idef;
            foreach (var idef in query) {
                if (idef.INDICATOR_NUMBER <= _inds.GetUpperBound(0)) {
                    // (convert nulls to zero)
                    _inds[idef.INDICATOR_NUMBER].radio_group = PFSUtility.DBToInt(idef.RADIO_GROUP);
                }
            }
        }

        private void InitProcs()
        {
            _procs = new List<proc_data>();
            _outcomes = new List<proc_data>();
        }

        private fmapRow LoadFreqTableRow(double los_high, string values)
        {
            fmapRow fmrow;
            
            fmrow.los_high = los_high;
            fmrow.freq = new int[5];

            var arr = values.Split(',');
            for (int i = 0; i <= fmrow.freq.GetUpperBound(0) ; i++ )
            {
                fmrow.freq[i] = arr[i].ToInteger();
            }
            return fmrow;
        }

        // These are the number of times you need to do something during a certain time period (LOS)
        // in order for it to be considered one of the frequencies listed.
        private void LoadFreqTable()
        {
            _freq_map = new List<fmapRow>();
            //                              LOS,  None Q4h Q2h Q1h Q30m  col formula=LOS/(2 x q): q=4,2,1,0.5
            _freq_map.Add(LoadFreqTableRow(1, "    0,  1, 99,  99, 99"));
            _freq_map.Add(LoadFreqTableRow(2, "    0,  1,  2,  99, 99"));
            _freq_map.Add(LoadFreqTableRow(4, "    0,  1,  2,  4,  99"));
            _freq_map.Add(LoadFreqTableRow(6, "    0,  2,  3,  5,   6"));
            _freq_map.Add(LoadFreqTableRow(8, "    0,  3,  5,  7,   8"));
            _freq_map.Add(LoadFreqTableRow(12, "   0,  3,  6,  9,  12"));
            //_freq_map.Add(LoadFreqTableRow(16, "   0,  3,  5,  9, 20"));
            //_freq_map.Add(LoadFreqTableRow(24, "   0,  4,  8, 15, 29"));
            //_freq_map.Add(LoadFreqTableRow(9999, " 0,  4,  8, 15, 29"));

            //to read this table: if you have at least 6 asessmenets in a 12-hour period, you get Q1h
            //(LOS=12, column Q1h has a count of 6)
        }

        private Frequencies FreqForCount(double los_hours, int count) 
        {
            foreach(var fmrow in _freq_map) {
                if (los_hours <= fmrow.los_high) {
                    // Option: pro-rate the count upward if the LOS is less than the row we are interested in.
                    //         This will bump the count to what it might have been at the full LOS.
                    // Note: truncate the result; rounding inflates the value too much.
                    int prorated_count = (int)((fmrow.los_high / los_hours) * count);

                    // foreach goes low to high; go from high to low instead
                    for (int j = (int)Frequencies.Q30M; (j >= (int)Frequencies.QNONE); j--) { //search right to left
                        if (prorated_count >= fmrow.freq[j]) {
                            return (Frequencies)j;
                        }
                    } // next j
                }
            }
            
            return Frequencies.QNONE;
        }

        //
        // Load patient chart from the database (one DB access)
        // Load everything since admission
        //
        private void LoadPatientChart()
        {
            // Get the entire patient chart (all units and dates for this patient)
            var db = PFSUtility.NewPfsDataContext();
            var query = from item in db.CHART_ITEMs
                        where (item.ENCOUNTER_ID == _pat.encounter_id)
                        select item;
            // Save the result
            _chart_items_since_admission = query.ToArray();

            // Convert all results to lower case for case insensitive comparisons
            foreach (var item in _chart_items_since_admission) {
                if (item.CATEGORY != null) item.CATEGORY = item.CATEGORY.ToLower();
                if (item.CODE != null) item.CODE = item.CODE.ToLower();
                if (item.DESCRIPTION != null) item.DESCRIPTION = item.DESCRIPTION.ToLower();
                if (item.FIELD_NAME != null) item.FIELD_NAME = item.FIELD_NAME.ToLower();
                if (item.RESULT != null) item.RESULT = item.RESULT.ToLower();
            }

            // Prepare 3 more versions of the chart
            var query2 = from item in _chart_items_since_admission
                    where(item.EVENT_DATETIME >= _pat.unit_arrival) && (item.EVENT_DATETIME <= _pat.pull_finish)
                    select item;
            _chart_items_since_unit_arrival = query2.ToArray();

            query2 = from item in _chart_items_since_admission
                     where (item.EVENT_DATETIME >= _pat.pull_start) && (item.EVENT_DATETIME <= _pat.pull_finish)
                     select item;
            _chart_items_during_pull_period = query2.ToArray();

            query2 = from item in _chart_items_since_admission
                         where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-24))
                         select item;
            _chart_items_past24hrs = query2.ToArray();
        }

        // Delay looking for patient procedures until they are wanted
        private void LoadPatientProceduresIfNeeded()
        {
            if (_procedure_events != null) return;

            var db = PFSUtility.NewPfsDataContext();
            var query = from proc in db.PROCEDURE_EVENTs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                        select proc;
            _procedure_events = query.ToArray();
        }



        // Started a new chart item query (of a certain depth)
        // Default is the chart during the pull range.  Options for unit arrival and admission to hospital.
        private IEnumerable<CHART_ITEM> StartNewQuery()
        {
            return StartNewQuery(SearchDepth.SearchDefault);
        }
        private IEnumerable<CHART_ITEM> StartNewQuery(SearchDepth search_depth)
        {
            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    return (from item in _chart_items_during_pull_period select item);
                case SearchDepth.SearchPast24Hrs:
                    return (from item in _chart_items_past24hrs select item);
                case SearchDepth.SearchSinceArrival:
                    return (from item in _chart_items_since_unit_arrival select item);
                case SearchDepth.SearchSinceAdmission:
                    return (from item in _chart_items_since_admission select item);
            }
            return null;
        }

        // Is this search term a word or a list of words?
        private bool ValueIsAList(string s) 
        {
            return (s != null) && s.Contains(',');                //is this a comma-separated list?
        }

        // Break apart a wordlist and prepare its members
        private string[] SplitOnCommaAndPrepareElements(string s)
        {
            // If a word list has a space after each comma, we will get leading blanks in each term
            var arr = s.Split(',');
            for (int i = 0; i <= arr.GetUpperBound(0); i++) {
                arr[i] = arr[i].Trim();                         // get rid of leading blanks
                arr[i] = arr[i].Replace(CHAR_COMMA, ",");       // insert a literal comma into search word
                arr[i] = arr[i].ToLower();                      // convert for case-insensitive comparisons
            }
            return arr;
        }

        // Add a basic chart item filter to a LINQ query
        //
        // cat = exact match
        // code_list = exact match; optional "like" match
        // desc_list = "like" match; optional exact match
        // field = exact match
        // result_list = "like" match; optional exact match
        //
        // These are comma-separated lists.  
        // Search for a literal comma by using CHAR_COMMA in the string.
        private IEnumerable<CHART_ITEM> AndItemFilter(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list) 
        {
            // Converted all values to lower case for case-insensitive comparisons.
            if (!String.IsNullOrEmpty(cat)) {
                cat = cat.ToLower();
                if (cat.Left(2) == CODE_LIKE_PREFIX)
                    query = query.Where(e => e.CATEGORY.Contains(cat.Substring(2)));
                else
                    query = query.Where(e => e.CATEGORY == cat);
            }

            if (!String.IsNullOrEmpty(code_list)) {
                code_list = code_list.ToLower();
                if (code_list.Left(2) == EXACT_MATCH_PREFIX) {          // exact match override?
                    query = query.Where(e => e.CODE == code_list.Substring(2));
                } else if (code_list.Left(2) == CODE_LIKE_PREFIX)
                {            // override for "like" match?
                    query = query.Where(e => e.CODE.Contains(code_list.Substring(2)));
                } else if (ValueIsAList(code_list)) {
                    query = AndCodeInList(query, code_list);            // find one of the words
                } else {
                    query = query.Where(e => e.CODE.Contains(code_list));      // find this word
                }
            }
            
            if (!String.IsNullOrEmpty(desc_list)) {
                desc_list = desc_list.ToLower();
                if (desc_list.Left(2) == EXACT_MATCH_PREFIX) {          // exact match override?
                    query = query.Where(e => e.DESCRIPTION == desc_list.Substring(2));
                } else if (ValueIsAList(desc_list)) {
                    query = AndDescriptionInList(query, desc_list);
                } else {
                    desc_list = desc_list.Replace(CHAR_COMMA, ",");     // look for literal comma requests
                    query = query.Where(e => desc_list.Contains(e.DESCRIPTION));
                }
            }
            
            if (!String.IsNullOrEmpty(field)) {
                //field = field.ToLower();
                //query = query.Where(e => e.FIELD_NAME.Contains(field));
                field = field.ToLower();  //treat field as a list of field_names
                if (field.Left(2) == EXACT_MATCH_PREFIX)
                {          // exact match override?
                    query = query.Where(e => e.FIELD_NAME == field.Substring(2));
                }
                else if (field.Left(2) == CODE_LIKE_PREFIX)
                {            // override for "like" match?
                    query = query.Where(e => e.FIELD_NAME.Contains(field.Substring(2)));
                }
                else if (ValueIsAList(field))
                {
                    query = AndFieldInList(query, field);            // find one of the words
                }
                else
                {
                    query = query.Where(e => e.FIELD_NAME.Contains(field));      // find this word
                }
            }
            
            if (!String.IsNullOrEmpty(result_list)) {
                result_list = result_list.ToLower();                        // make all results case insensitve
                if (result_list.Left(2) == EXACT_MATCH_PREFIX) {     // exact match override?
                    query = query.Where(e => e.RESULT == result_list.Substring(2));
                } else if (ValueIsAList(result_list)) {
                    query = AndResultInList(query, result_list);
                } else {
                    result_list = result_list.Replace(CHAR_COMMA, ",");     // look for literal comma requests
                    query = query.Where(e => e.RESULT.Contains(result_list));
                }
            }
            return query;
        }

        // Look for a code that is one of the words in the list.
        private IEnumerable<CHART_ITEM> AndCodeInList(IEnumerable<CHART_ITEM> query, string code_list) 
        {
            if (String.IsNullOrEmpty(code_list)) return query;
            
            var arr = SplitOnCommaAndPrepareElements(code_list);
            return query.Where(e => arr.Any(e.CODE.Contains));  // use like match. Exact match = arr.Contains(e.CODE));   
            /* or a bit longer:  
             *  return query.Where(e => arr.Any(x => e.CODE.Contains(x))); */
        }

        // Look for a description that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndDescriptionInList(IEnumerable<CHART_ITEM> query, string desc_list) 
        {
            if (String.IsNullOrEmpty(desc_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(desc_list);
            return query.Where(e => arr.Any(e.DESCRIPTION.Contains));    // "like" match
        }

        // Look for a code that is one of the words in the list.
        private IEnumerable<CHART_ITEM> AndFieldInList(IEnumerable<CHART_ITEM> query, string field)
        {
            if (String.IsNullOrEmpty(field)) return query;

            var arr = SplitOnCommaAndPrepareElements(field);
            return query.Where(e => arr.Any(e.FIELD_NAME.Contains));  // use like match. Exact match = arr.Contains(e.CODE));   
        }
        // Look for a result that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultInList(IEnumerable<CHART_ITEM> query, string result_list) 
        {
            if (String.IsNullOrEmpty(result_list)) return query;
            
            var arr = SplitOnCommaAndPrepareElements(result_list);
            return query.Where(e => arr.Any(e.RESULT.Contains));         // "like" match
        }

        // Look for a result that does not contain one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultNotInList(IEnumerable<CHART_ITEM> query, string result_list)
        {
            if (String.IsNullOrEmpty(result_list)) return query;
            
            var arr = SplitOnCommaAndPrepareElements(result_list);
            return query.Where(e => !arr.Any(e.RESULT.Contains));        // "like" match
        }

        private string DescribeSearchDepth(SearchDepth search_depth) 
        {
            string result = "";
            
            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    //result = "in pull range"
                    result = "";                         //be quiet since this is default
                    break;
                case SearchDepth.SearchSinceArrival:
                    result = "since arrival to unit";
                    break;
                case SearchDepth.SearchSinceAdmission:
                    result = "since admission";
                    break;
            }
            
            return result;
        }

        // Describe a query that has already been run
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Make a simple query and describe the results
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            var arr = query.ToArray();
            return Describe(arr, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private string LookingFor(string so_far, string what, string op, string value)
        {
            string result;
            if (String.IsNullOrEmpty(value)) return so_far;

            if (so_far == "")
                result = "looking for " + what;
            else
                result = so_far + " and " + what;

            if (value.Left(2) == EXACT_MATCH_PREFIX)
                result += "='" + value.Substring(2) + "'";
            else if (value.Left(2) == CODE_LIKE_PREFIX)
                result += " contains " + "'" + value.Substring(2) + "'";
            else if (ValueIsAList(value))
                result += " in '" + value + "'";
            else
                result += op + "'" + value + "'";
            
            return result;
        }
        private string Describe(CHART_ITEM[] arr, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string result = "";
            result = LookingFor(result, "cat", "=", cat);
            result = LookingFor(result, "code", "=", code_list);
            result = LookingFor(result, "desc", " contains ", desc_list);
            result = LookingFor(result, "field", "=", field);
            result = LookingFor(result, "result", " contains ", result_list);

            if (arr.Count() == 0) {
                result += "; not found " + DescribeSearchDepth(search_depth);
            } else {
                var e = arr[0];
                result = "FOUND: " + result + "; found";
                // We might have searched for a pattern or word list in several fields - show what was found
                if (e.CATEGORY != null)     result += " cat='" + e.CATEGORY + "'";
                if (e.CODE != null)         result += " code='" + e.CODE + "'";
                if (e.DESCRIPTION != null)  result += " desc='" + e.DESCRIPTION + "'";
                if (e.FIELD_NAME != null)   result += " field='" + e.FIELD_NAME + "'";
                if (e.RESULT != null)       result += " result='" + e.RESULT + "'";
                // Are there more results?  Just say how many; we aren't going to list them.
                if (arr.Count() == 2) {
                    result += " (1 more result)";
                } else if (arr.Count() > 2) {
                    result += " (" + (arr.Count() - 1) + " more results)";
                }
            }
            
            return result;
        }


        //Set an indicator for this reason (low level)
        private void SetInd(int inum, string reason)
        {
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Set Ind #" + inum + ": " + reason);          //already set - repeat for verbose only
            } else {
                _inds[inum].is_checked = true;
                Program.Audit("Set Ind #" + inum + ": " + reason);
            }
        }

        //Clear an indicator for this reason (low level)
        private void ClrInd(int inum, string reason)
        {
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Clr Ind #" + inum + ": " + reason);          //already clear - repeat verbose only
            } else {
                _inds[inum].is_checked = false;
                Program.Audit("Clr Ind #" + inum + ": " + reason);
            }
        }

        // Count how many items meet the conditions
        // All items are listed
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountItems(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            int count = query.Count();

            // always return what was found
            found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
            // echo the result?
            if (trace) Program.VerboseAudit(found_what);
            
            return count;
        }

        // Count how many items meet the conditions
        // This has the option to list every entry or give a total
        private int CountResultInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what="";
 
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);

            var arr = SplitOnCommaAndPrepareElements(result_list);

            foreach (var item in query) {
                // Figure out which of the search words were found
                var s = item.RESULT.ContainsWhich(arr);
                found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' code='" + item.CODE + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "'";
                if (trace) Program.VerboseAudit(found_what);
                count++;
                if (count_mode != CountMode.CountAll) break;        //stop counting
            }
            
            if (count > 0) {
                //We already printed what was found; maybe add how many?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }
            
            return count;
        }

        // Count how many items contain ALL results in list
        private int CountResultInListAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what = "";

            var arr = SplitOnCommaAndPrepareElements(result_list);

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            for (int i = 0; i <= arr.GetUpperBound(0); i++)
            {
                if (arr[i].StartsWith(NOT_PREFIX))
                {
                    query = query.Where(e => !e.RESULT.Contains(arr[i].Substring(2)));
                }
                else
                {
                    query = query.Where(e => e.RESULT.Contains(arr[i]));
                }
            }

            count = query.Count();

            if (count > 0)
            {
                found_what = "found item with all results in '" + result_list + "' without those prefixed by !!";
                if (trace) Program.VerboseAudit(found_what);
            }
            else
            {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (ValueIsAList(result_list)) {
                return CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what);
            } else {
                return CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what);
            }
        }

        // Is there an item with this result?
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list) 
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth) 
        {
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContains(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what) 
        {
            if (ValueIsAList(result_list)) {
                return (CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            } else {
                return (CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
            }
        }

        // Result contains all in list
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (ValueIsAList(result_list))
            {
                return (CountResultInListAll(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            }
            else
            {
                found_what = "Not valid for non-list results.";
                return false;
            }
        }
        // Count how many items meet the conditions, with none of the results in result_list.
        // This has the option to say how many were found rather than list all.
        private int CountResultNotInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what) 
        {
            int count = 0, rec_count = 0;
            bool found_one;

            found_what = "";
            var arr = SplitOnCommaAndPrepareElements(result_list);         // also deals with CHAR_COMMA
            
            //Do not filter by result here; we need all results
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");
            
            foreach(var item in query) {
                rec_count++;
                //Look for each search word in the result
                found_one = false;
                
                foreach (string s in arr) {
                    if (String.Equals(item.RESULT, s)) {
                        found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "' -- ignore this";
                        Program.VerboseAudit(found_what);
                        found_one = true;
                        break;
                    }
                }

                if (! found_one) {
                    //none of the words were found - good!
                    found_what = Describe(cat, code_list, desc_list, field, "", search_depth) + " -- does not contain '" + result_list + "'";
                    Program.VerboseAudit(found_what);
                    count++;
                    if (count_mode != CountMode.CountAll) break;
                }
            }
            
            if (count > 0) {
                //We already printed what was found; maybe show the total?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else if (rec_count > 0) {
                //We already printed what we ignored
            } else {
                //Print what did not exist
                found_what = Describe(cat, code_list, desc_list, field, "", search_depth);      //not found
                Program.VerboseAudit(found_what);
            }
            
            return count;
        }

        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, string found_what) 
        {
            // Search in the pull range with trace on
            return CountResultDoesNotContain(cat, code_list, desc_list, field, result_list, out found_what, SearchDepth.SearchDefault, true);
        }
        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, out string found_what, SearchDepth search_depth, bool trace)
        {
            return CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0;
        }

        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            return (CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0);
        }

        // Set the indicator if the conditions are met
        //
        private bool SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);

        }
        private bool SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            bool b = false;
            //avoid more queries if the indicator is already set
            if (_inds[inum].is_checked) return true;
            
            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what)) {
                SetInd(inum, found_what);                          //echo here - found
                b = true;
            } else {
                Program.VerboseAudit(found_what);                  //and here - not found
            }
            return b;
        }
        // Special contains all
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContainsAll(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already set
            if (_inds[inum].is_checked) return;

            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                          //echo here - found
            }
            else
            {
                Program.VerboseAudit(found_what);                  //and here - not found
            }
        }
        // Set the indicator if the result does not contain any of the words in result_list
        //
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultDoesNotContain(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {   
            string found_what;
            //avoid more queries if the indicator is already set
            if (_inds[inum].is_checked) return;
            
            //Turn trace off for ResultDoesNotContain() and echo what was set below with SetInd
            if (ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                           //echo here - not found
            } else {
                Program.VerboseAudit(found_what);                   //and here - found
            }
        }

        // Clear the indicator if the result contains one of the words in the result_list
        //
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already clear
            if (! _inds[inum].is_checked) return;
            
            //Turn trace off for ResultContains() and echo what was set below with SetInd
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what)) {
                ClrInd(inum, found_what);                           //echo here - found
            } else {
                Program.VerboseAudit(found_what);                   //and here - not found
            }
        }


        //These slightly smaller functions are meant for places where you aren't really looking for a chart result
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            return (CountResultContains(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
        }

        // Variations with optional parameters (can switch to optional parameters in VS 2010)
        //
        private void SetIndIfFound(int inum, string cat)
        {
            SetIndIfResultContains(inum, cat, "", "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list)
        {
            SetIndIfResultContains(inum, cat, code_list, "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private void ClrIndIfFound(int inum, string cat)
        {
            ClrIndIfResultContains(inum, cat, "","","","", SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }


        // Get the max/total value from a result (usually in the middle of the text)
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetIntValue(get_mode, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            int value;
            int result = 0;
            bool found_one = false;

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            
            //Look for a number in the result
            
            foreach(var item in query) {
                // RESULT might be null so convert to empty string so Split won't blow up
                var arr = PFSUtility.DBToString(item.RESULT).Split(' ');
                foreach (string s in arr) {
                    //Look for 1st character numeric; IsNumeric("60min") will fail so look at 1st char only
                    if (s.Left(1).IsNumeric()) {
                        value = (int)s.Val();                           //Use Val; ToInteger will error on "60min"
                        switch (get_mode) {
                            case GetValueMode.GetMax:
                                result = Math.Max(value, result);       //max
                                break;
                            case GetValueMode.GetTotal:
                                result += value;                        //total
                                break;
                            case GetValueMode.GetLast:
                                result = value;                         //last
                                break;
                        }
                        
                        // print what we are searching for (the first time)
                        if (! found_one) {
                            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
                        }
                        found_one = true;
                        // print each value found
                        Program.VerboseAudit("  found numeric value " + result);
                        //Keep going in case there are more
                    }
                } 
            }
            
            if (! found_one) {
                //show what was not found
                Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
            }
            
            return result;
        }

        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetMaxValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return GetIntValue(GetValueMode.GetMax, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetTotalValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return GetIntValue(GetValueMode.GetTotal, cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Get a result; returns true if found with return_result
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result)
        {
            return GetResult(cat, code_list, desc_list, field, out return_result, SearchDepth.SearchDefault);
        }
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0) {
                return_result = PFSUtility.DBToString(query.First().RESULT);
            } else {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }
        private void GetResultforOther(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "Other (comment)");
            return_result="";
            foreach (var item in query)
            {
                if (!String.Equals(item.RESULT, "Other (comment)"))
                {
                    return_result = item.RESULT;
                    //Program.VerboseAudit(found_what);
                    break;
                }

            }
            return;
        }

        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, out string return_result, out DateTime return_evdt)
        {
            return GetResultAndEVDT(cat, code_list, desc_list, field, out return_result, out return_evdt, SearchDepth.SearchDefault);
        }
        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, out string return_result, out DateTime return_evdt, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0)
            {
                return_result = PFSUtility.DBToString(query.First().RESULT);
                return_evdt = PFSUtility.DBToDateTime(query.First().EVENT_DATETIME);
            }
            else
            {
                return_result = "";
                return_evdt = DateTime.MinValue;
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }


        //======================================================================================
        // Mapping starts here
        //======================================================================================

        private void Check_1_2_3_4()
        {
            string found_what;
            SearchDepth s;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("1. ADL Self");
            Program.VerboseAudit("2. ADL Assist");
            Program.VerboseAudit("3. ADL Extended");
            Program.VerboseAudit("4. ADL Complete");
            Program.VerboseAudit("---------------");
            
            if (_pat.age <= 4.0) SetInd(4,"Age <=4 years");
            if (_inds[4].is_checked) return;

            if (Program.g_pull_finish.ToString("hhmm") == "0700")
                s = SearchDepth.SearchPast24Hrs;
            else
                s = SearchDepth.SearchPullRange;

            bool feed4 = ResultContains("", "APS.FEED", "", "0500030,1750011", "Total,Maximum",s);
            bool feed2 = ResultContains("", "APS.FEED", "", "0500030,1750011", "Standby,Minimum,Moderate",s);
            bool feed1 = ResultContains("", "APS.FEED", "", "0500030,1750011", "Independent",s);

            bool act4 = ResultContains("", "APS.ACTABL", "", "1750002", "Total,Maximum", s);
            bool act2 = ResultContains("", "APS.ACTABL", "", "1750002", "Standby,Minimum,Moderate,Contact Guard,person", s);
            bool act1 = ResultContains("", "APS.ACTABL", "", "1750002", "Independent", s);

            bool hyg4 = ResultContains("", "APS.HYGBTH", "", "1750002", "Total,Maximum",s);
            bool hyg2 = ResultContains("", "APS.HYGBTH", "", "1750002", "Standby,Minimum,Moderate,Contact Guard,person",s);
            bool hyg1 = ResultContains("", "APS.HYGBTH", "", "1750002", "Independent",s);

            bool npo = ResultContains(CODE_LIKE_PREFIX + "diets", "NPO", "", "", "", s);

            if ((feed4 || npo) && hyg4) SetInd(4, "Total Feeding and Bathing");
            if (_inds[4].is_checked) return;

            bool toi4 = ResultContains("", "OT.TOILET", "", "1750002", "Total,Maximum", s);
            bool toi2 = ResultContains("", "OT.TOILET", "", "1750002", "Standby,Minimum,Moderate,Contact Guard,person", s);
            bool toi1 = ResultContains("", "OT.TOILET", "", "1750002", "Independent", s);

            bool guvoid = ResultContains("", "GU.VOID", "", "0500030", "Incontinence pads", s);
            bool iovoid = ResultContains("", "IO.VOID", "", "0500030", "", s);

            bool ionbm = ResultContains("", "IO.NBM", "", "0500030", "", s);
            bool gusx = ResultContains("", "GU.SX", "", "3300002", "Incontinent", s);
            bool gibp = ResultContains("", "GI.BP", "", "3300001", "Incontinent", s);
            bool gibmf = ResultContains("", "GI.BMF", "", "3300001", "Every 1-2", s);

            bool incont = (guvoid && iovoid) || ((gusx || gibp || gibmf) && ionbm);

            if ((feed4 || feed2 || npo) && (act4 || act2) && (hyg4 || hyg2) &&
                (toi4 || toi2 || incont))
                SetInd(3, "Dependent care for all: eating,bathing,toileting,activity");
            if (_inds[3].is_checked) return;
            if (feed4 || feed2 || npo || act4 || act2 || hyg4 || hyg2 ||
                toi4 || toi2 || incont)
                SetInd(2, "Partial care for some of: eating,bathing,toileting,activity");
            if (_inds[2].is_checked) return;
            if (feed1 || act1 || hyg1) SetInd(1, "Independent in at least one of: feeding,bathing,activity");
            if (!_inds[1].is_checked) SetInd(2, "No ADL documentation found - Defaulting to Partial");

            // Madhu

            bool canfeed1 = ResultContains("", "APS.FEED2", "", "0500030", "Independent", s);
            bool canfeed2 = ResultContains("", "APS.FEED2", "", "0500030", "Stand-by Assist, Mimimal Assist, Moderate Assist", s);
            bool canfeed4 = ResultContains("", "APS.FEED2", "", "0500030", "Maximal Assist", s);
            canfeed4 |= ResultContains("", "APS.FEED2", "", "0500030", EXACT_MATCH_PREFIX+"Dependent", s);

            bool canact1 = ResultContains("", "APS.ACTABL2", "", "1750002", "Independent", s);
            bool canact2 = ResultContains("", "APS.ACTABL2", "", "1750002", "Stand-by Assist, Mimimal Assist, Moderate Assist", s);
            bool canact4 = ResultContains("", "APS.ACTABL2", "", "1750002", "Maximal Assist, Dependent", s);

            bool canbath1 = ResultContains("", "APS.HYGBTH3", "", "1750002", "Independent", s);
            bool canbath2 = ResultContains("", "APS.HYGBTH3", "", "1750002", "Stand-by Assist, Mimimal Assist, Moderate Assist", s);
            bool canbath4 = ResultContains("", "APS.HYGBTH3", "", "1750002", "Maximal Assist", s);
            canbath4 |= ResultContains("", "APS.HYGBTH3", "", "1750002", EXACT_MATCH_PREFIX+"Dependent", s);

            if (canfeed4 && canbath4) SetInd(4, "Total Feeding and Bathing");
            if (_inds[4].is_checked) return;

            bool cantoi1 = ResultContains("", "APS.TOILET2", "", "1750002", "Independent", s);
            bool cantoi2 = ResultContains("", "APS.TOILET2", "", "1750002", "Stand-by Assist, Mimimal Assist, Moderate Assist", s);
            bool cantoi4 = ResultContains("", "APS.TOILET2", "", "1750002", "Maximal Assist", s);
            cantoi4 |= ResultContains("", "APS.TOILET2", "", "1750002", EXACT_MATCH_PREFIX+"Dependent", s);

            bool canvoid = ResultContains("", "GU.VOID2", "", "0500030", "Incontinence Pads", s);
            bool numvoid = ResultContains("", "IO.VOID", "", "0500030", "", s);

            bool numbm = ResultContains("", "IO.NBM", "", "", "", s);

            bool gusymp = ResultContains("", "GU.SX2", "", "", "Incontinent", s);
            bool bpatt = ResultContains("", "GI.BP2", "", "", "Incontinent", s);
            bool bmov = ResultContains("", "GI.BMF", "", "", "Every 1-2 Hours", s);

            bool incontnew = (canvoid && numvoid) || ((gusymp || bpatt || bmov) && numbm);

            if ((canfeed4 || canfeed2) && (canact4 || canact2) && (canbath4 || canbath2) && (cantoi4 || cantoi2 || incontnew))
                SetInd(3, "Dependent care for all: eating,bathing,toileting,activity");
            if (_inds[3].is_checked) return;

            if (canfeed2 || canact2 || canbath2 || toi2 || incontnew)
                SetInd(2, "Partial care for some of: eating,bathing,toileting,activity");
            if (_inds[2].is_checked) return;

            if (canfeed1 || canact1 || canbath1 || cantoi1) SetInd(1, "Independent in at least one of: feeding,bathing,activity");
            if (!_inds[1].is_checked) SetInd(2, "No ADL documentation found - Defaulting to Partial");

        }


        private void Check_5()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("5. ADL Rehab");
            Program.VerboseAudit("---------------");

            SetIndIfResultDoesNotContain(5, "", "OT.DRESS", "", "OT", "Independent");
            SetIndIfResultContains(5, "", "OT.FEED", "", "OT", "");
            SetIndIfResultContains(5, "", "OT.BATH", "", "OT", "");
            SetIndIfResultContains(5, "", "OT.TOILET", "", "OT", "");
            SetIndIfResultDoesNotContain(5, "", "ST.ORAL", "", "", "WFL");
            SetIndIfResultDoesNotContain(5, "", "ST.PHARY", "", "", "WFL");
            SetIndIfResultContains(5, "", "ST.TX.DATE", "", "", "");
            SetIndIfResultContains(5, "", "ST.EVAL.DATE", "", "", "");
            SetIndIfResultContains(5, "", "PT.TX.DATE", "", "", "");
            SetIndIfResultContains(5, "", "EVAL.START.TIME", "", "", "");
            SetIndIfResultContains(5, "", "EVAL.DATE", "", "", "");
            SetIndIfResultContains(5, "", "EVAL.OT.DATE", "", "", "");
            SetIndIfResultContains(5, "", "OT.TX.DATE", "", "", "");

            // Madhu

            SetIndIfResultDoesNotContain(5, "", "ST.THINOS", "", "", "WFL");
            SetIndIfResultDoesNotContain(5, "", "ST.NECTOS", "", "", "WFL");
            SetIndIfResultDoesNotContain(5, "", "ST.HONEOS", "", "", "WFL");
            SetIndIfResultDoesNotContain(5, "", "ST.PUREOS", "", "", "WFL");
            SetIndIfResultDoesNotContain(5, "", "ST.MECHOS", "", "", "WFL");
            SetIndIfResultDoesNotContain(5, "", "ST.REGOS", "", "", "WFL");
            SetIndIfResultDoesNotContain(5, "", "ST.THINPS", "", "", "WFL");
            SetIndIfResultDoesNotContain(5, "", "ST.NECTPS", "", "", "WFL");
            SetIndIfResultDoesNotContain(5, "", "ST.HONEPS", "", "", "WFL");
            SetIndIfResultDoesNotContain(5, "", "ST.PUREPS", "", "", "WFL");
            SetIndIfResultDoesNotContain(5, "", "ST.MECHPS", "", "", "WFL");
            SetIndIfResultDoesNotContain(5, "", "ST.REGPS", "", "", "WFL");
                                                       
        }


        private void Check_6_7()
        {    
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("6. ADL 2-3 Caregivers");
            Program.VerboseAudit("7. ADL 4 or more Caregivers");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(6, "", "APS.ACTABL", "", "", "2-3");
            SetIndIfResultContains(6, "", "APS.HYGBTH", "", "", "2-3");
            SetIndIfResultContains(6, "", "OT.TOILET", "", "", "2-3");
            SetIndIfResultContains(6, "", "OT.FEED", "", "", "2-3");
            SetIndIfResultContains(6, "", "OT.DRESS", "", "", "2-3");
            SetIndIfResultContains(6, "", "OT.BATH", "", "", "2-3");
            SetIndIfResultContains(7, "", "APS.ACTABL", "", "", "4");
            SetIndIfResultContains(7, "", "APS.HYGBTH", "", "", "4");
            SetIndIfResultContains(7, "", "OT.TOILET", "", "", "4");
            SetIndIfResultContains(7, "", "OT.FEED", "", "", "4");
            SetIndIfResultContains(7, "", "OT.DRESS", "", "", "4");
            SetIndIfResultContains(7, "", "OT.BATH", "", "", "4");

            // Madhu
            SetIndIfResultContains(6, "", "APS.ACTABL2", "", "", "2-3");
            SetIndIfResultContains(6, "", "APS.HYGBTH3", "", "", "2-3");
            SetIndIfResultContains(6, "", "APS.TOILET2", "", "", "2-3");

            SetIndIfResultContains(7, "", "APS.ACTABL2", "", "", "4");
            SetIndIfResultContains(7, "", "APS.HYGBTH3", "", "", "4");
            SetIndIfResultContains(7, "", "APS.TOILET2", "", "", "4");

        }

        private void Check_8()
        {
            string reslist;
            string other_str;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("8. Communication");
            Program.VerboseAudit("---------------");

            if (Exists("", "Neuro.ORIE", "", "", "Comatose") ||
                Exists("", "Neuro.C", "", "", "Unable to Comprehend") ||
                Exists("", "Neuro.CSV", "", "", "Incomprehensible,None") ||
                Exists("", "ED.NIH.1A", "", "", "Comatose,Stuporous") ||
                Exists("", "Neuro.LOC", "", "", "Comatose,Unresponsive"))
            {
                coma = true;
                Program.VerboseAudit("Patient is comatose or other non-communicative state");
                return;
            }

            // Madhu

            if (Exists("", "NEURO.VCA2", "", "", "Non-verbal/Mute") ||
              Exists("", "APS.HEAR2", "", "", "Use of Hearing Aid") ||
              Exists("", "Neuro.LOC2", "", "", "Comatose,Stuperous"))
            {
                coma = true;
                Program.VerboseAudit("Patient is comatose or other non-communicative state");
                return;
            }
 //  Madhu end


            SetIndIfResultContains(8, "", "GEN.IR", "", "", "Y", SearchDepth.SearchSinceAdmission);
            //SetIndIfResultContains(8, "", "APS.HEAR", "", "", "Deaf,Hard of hearing", SearchDepth.SearchSinceAdmission);
            SetIndIfResultContains(8, "", "HEENT.VD", "", "", "Blind", SearchDepth.SearchSinceAdmission);
            SetIndIfResultContains(8, "", "ED.R.ENT.T", "", "", "Aphonia", SearchDepth.SearchSinceAdmission);
            SetIndIfResultContains(8, "", "CODE.STROKE", "", "", "Difficulty speaking,Aphasia,Sudden Visual Loss", SearchDepth.SearchSinceAdmission);
            SetIndIfResultContains(8, "", "NEURO.SPEECH", "", "", "aphasia,garbled,ventilated,unclear", SearchDepth.SearchSinceAdmission);
            SetIndIfResultContains(8, "", "ED.NIH.9", "", "", "Severe aphasia,mute/global aphasia", SearchDepth.SearchSinceAdmission);
            SetIndIfResultContains(8, "", "NEURO.COMT", "", "", "Interpreter", SearchDepth.SearchSinceAdmission);
            SetIndIfResultContains(8, "", "GEN.AR", "", "", "No", SearchDepth.SearchSinceAdmission);
            SetIndIfResultContains(8, "", "GEN.AW", "", "", "No", SearchDepth.SearchSinceAdmission);
            SetIndIfResultContains(8, "", "GEN.CCS", "", "", "Unable to read,Unable to write,Unable to follow commands", SearchDepth.SearchSinceAdmission);

            // Madhu
            SetIndIfResultContains(8, "", "NEURO.UNABLEAS", "", "", "", SearchDepth.SearchSinceAdmission);
            SetIndIfResultContains(8, "", "HEENT.EYSX2", "", "", "Blindness", SearchDepth.SearchSinceAdmission);
            SetIndIfResultContains(8, "", "ED.R.ENT.T2", "", "", "Aphonia", SearchDepth.SearchSinceAdmission);
            SetIndIfResultContains(8, "", "SPEECH.PATTERN", "", "", "aphasia,garbled,unclear", SearchDepth.SearchSinceAdmission);


        }

        private void Check_9()
        {
            string reslist;
            bool person = false;
            bool place = false;
            bool otime = false;
            bool situation = false;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("9. Cognitive Support");
            Program.VerboseAudit("---------------");

            if (coma)
            {
                Program.VerboseAudit("Patient is comatose or other non-communicative state");
                return;
            }

            if (Exists("", "NEURO.UNABLEAS", "", "", "chronically impaired, Intubated/sedated, unresponsive") ||
               Exists("", "NEURO.C", "", "", "Unable to comprehend") ||
               Exists("", "NEURO.CSV", "", "", "Incomprehensible, None") ||
               Exists("", "ED.NIH.1A", "", "", "Comatose, Stuporous") ||
               Exists("", "NEURO.LOC2", "", "", "Comatose, Unresponsive") ||
               Exists("", "NEURO.CSV", "", "", "Incomprehensible, None"))
            {
                coma = true;
                Program.VerboseAudit("Patient is comatose or other non-communicative state"); // not sure about the verbose here
                return;
            }

            if (ResultContains("", "MENTAL", "impaired", "", ""))
            {
                if (ResultContains("", "NEURO.ORIE", "", "", "person"))
                {
                    Program.VerboseAudit("Oriented to PERSON");
                    person = true;
                }
                if (ResultContains("", "NEURO.ORIE", "", "", "place"))
                {
                    Program.VerboseAudit("Oriented to PLACE");
                    place = true;
                }
                if (ResultContains("", "NEURO.ORIE", "", "", "time"))
                {
                    Program.VerboseAudit("Oriented to TIME");
                    otime = true;
                }
                if (ResultContains("", "NEURO.ORIE", "", "", "situation"))
                {
                    Program.VerboseAudit("Oriented to SITUATION");
                    situation = true;
                }
                if (!(person && place && otime && situation))
                    SetInd(9, "Not oriented in at least one aspect");
                SetIndIfResultContains(9, "", "NEURO.ORIEN", "", "", "No");
                SetIndIfResultContains(9, "", "NEURO.CSV", "", "", "Confused");
            }

            SetIndIfResultContains(9, "", "NEURO.C", "", "", "Unable to comprehend");

            if (ResultContains("", "APS.FRM", "", "", "Y"))
                SetIndIfResultContains(9, "", "FALL.", "", "", "Y");
            //SetIndIfResultContains(9, "", "FALL.BED", "", "", "Yes");
            //SetIndIfResultContains(9, "", "FALL.TABS", "", "", "Yes");
            //SetIndIfResultContains(9, "", "FALL.FREQ", "", "", "Yes");
            //SetIndIfResultContains(9, "", "FALL.ROUNDS", "", "", "Yes");
            //SetIndIfResultContains(9, "", "FALL.ASSIST", "", "", "Yes");
            //SetIndIfResultContains(9, "", "FALL.ALONE", "", "", "Yes");

            // SetIndIfResultContains(9, "", "MENTAL", "", "", "");

            // Madhu

             

              SetIndIfResultContains(9, "", "NEURO.NOTORIENT", "", "", "");
              SetIndIfResultContains(9, "", "APS.FRMR", "", "", "Yes");
            //SetIndIfResultContains(9, "", "", "", "", "Mental Status");

        }

        private void Check_10_11()
        {
            string reslist, found_what;
            int ct;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("10. Behavior/Emotional Management");
            Program.VerboseAudit("11. Behavior/Emotional Mgmt - q 1 Hour");
            Program.VerboseAudit("---------------");
            
            SetIndIfResultContains(11, "", "RESTVIOL", "", "", "");
            SetIndIfResultContains(11, "", "REST.VINIT,REST.NVINIT", "", "", "");
//            SetIndIfResultContains(11, "", "REST.DC", "", "", "");
            SetIndIfResultContains(11, "", "REST.NVDC", "", "", "");
            SetIndIfResultContains(11, "", "REST.VIOL", "", "", "");
            
            reslist = "aggessive,combative,suspicious,resistive to care,crying,uncooperative,belligerent,fearful";
            SetIndIfResultContains(10, "", "NEURO.B", "", "", reslist);
            reslist = "Angry,depressed,Fearful,Hostile,Sad,Suspicious,Nervous,Withdrawn,Apprehensive";
            SetIndIfResultContains(10, "", "NEURO.AFFE", "", "", reslist);

            SetIndIfResultContains(10, "", "PSY.DELU", "", "", "Present");
            SetIndIfResultContains(10, "", "PSY.HALL", "", "", "Present");
            SetIndIfResultContains(10, "", "PSY.AB.TH", "", "", "Present");

            SetIndIfResultContains(10, "", "THPA", "", "", "");
            SetIndIfResultContains(10, "", "ANX", "", "", "");
            SetIndIfResultContains(10, "", "SINI", "", "", "");
            SetIndIfResultContains(10, "", "HOPE", "", "", "");
            SetIndIfResultContains(10, "", "HR.SUICIDE", "", "", "");

            // Madhu

            string reslist2, reslist3, reslist4, reslist5, reslist6;
            reslist2 = "Agressive,Anxious,Confused,Crying,Fearful,Guarded,Implusive,Restless,Suspicous,Uncooperative";
            reslist3 = "Being Controlled,Bizarre,Grandiose,Paranoid Ideation,Self-Ideation";
            reslist4 = "Auditory,Command,Olfactory,Tactile,Visual";
            reslist5 = "Circumstantial,Disorganized,Disoriented,Flight of Ideas,Goal Oriented,Loose Associations,Tangential,Thought Blocking";
            reslist6 = "Angry,Anxious,Apathetic,Apprehensive,Blunted,Depressed,Elated,Euphoric,Fearful,Flat,Hostile,Labile,Nervous,Sad,Suspicious,Withdrawn";

            SetIndIfResultContains(10, "", "NEURO.BEH", "", "", reslist2);
            SetIndIfResultContains(10, "", "NEURO.AFFE2", "", "", reslist6);
            SetIndIfResultContains(10, "", "PSY.DEL2", "", "", reslist3);
            SetIndIfResultContains(10, "", "PSY.HALLUC", "", "", reslist4);
            SetIndIfResultContains(10, "", "PSY.THOUGHT", "", "", reslist5);
            SetIndIfResultContains(11, "", "REST.STATUS2", "", "", "Application, Monitor, Discontinue");
            

        }




        // (this isn't really a Q1h count -- it is just a count) Make this a q1hr count
        private int ReturnQ1HrCount(string code_list, string result_list)
        {
            var buckets = new List<int>();
            SetBucketSize(60);
            AddBuckets(buckets, "", code_list, "", "", result_list);
            return CountBuckets(buckets);
        }

        private bool IsQ1Hour(int count)
        {
            return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q1H);
        }

        // Use this if counting safety buckets
        //private void CheckSafety(int count, string desc)
        //{
        //    if (_inds[13].is_checked) return;             //skip if highest already checked
        //    if (count == 0) return;
        //    switch (FreqForCount(_pat.los_hours, count))
        //    {
        //    case Frequencies.Q30M:
        //        SetInd(13, desc + " q30min");
        //        break;
        //    case Frequencies.Q1H:
        //    case Frequencies.Q2H:
        //        SetInd(12, desc + " q2h");
        //        break;
        //    default:
        //        Program.VerboseAudit(desc + ": " + count + " in " + Math.Round(_pat.los_hours) + " hours is not enough");
        //        break;
        //    }
        //}

        private void Check_12_13()
        {
            bool suppress13=false;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("12. Safety Management - q 2 Hours");
            Program.VerboseAudit("13. Safety Management - q 30 Minutes");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(13, "", "APS.SSD", "", "", "");
            SetIndIfResultContains(13, "", "APS.SST", "", "", "");
            SetIndIfResultContains(13, "", "APS.SED", "", "", ""); // Madhu
            SetIndIfResultContains(13, "", "APS.SET", "", "", ""); // Madhu
            if (_inds[13].is_checked) return;

            SetIndIfResultContains(12, "", "FALL.RISK", "", "", "Y");
            //SetIndIfResultContains(12, "", "FALL.ROUNDS", "", "", "Yes");
            SetIndIfResultContains(12, "", "RESTNV", "", "", "");
            SetIndIfResultContains(12, "", "REST.MEDICAL", "", "", "");
            SetIndIfResultContains(12, "", "REST.NVDC", "", "", "");
            SetIndIfResultContains(12, "", "REST.NVINIT", "", "", "");
            // Madhu
            SetIndIfResultContains(12, "", "APS.FRMR", "", "", "Yes");
            SetIndIfResultContains(12, "", "REST.STATUS2", "", "", "Application, Monitor");
            SetIndIfResultContains(12, "", "REST.STATUS2", "", "", "Discontinue");

            if (_inds[12].is_checked) return;
            int value = 0;
            string return_result = "";
            if (GetResult("","APS.FRMTS", "","", out return_result)) 
            {
                if (return_result.IsNumeric())
                {
                    value = (int)return_result.Val();
                    if (value >= 1) SetInd(12, "High Risk Total Score=" + value);
                }
            }
                
        }

        private void Check_14()
        {    
            string reslist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("14. Isolation");
            Program.VerboseAudit("---------------");

            SetIndIfResultDoesNotContain(14, "", "GEN.ISO2", "", "", "standard,chemo");
            SetIndIfResultContains(14, "", "GEN.ISO3", "", "", "Contact, Droplet, Airborne, Chemo, Neutropenic, Contact Plus");  // Madhu
        }

        private void CheckAssessment(int count, string desc)
        {
            if (_inds[18].is_checked) return;          //skip if highest already checked
            if (count == 0) return;                    //skip if none
            
            // This should work the same as the original code:
            switch (FreqForCount(_pat.los_hours, count)) {
                case Frequencies.Q30M:
                    SetInd(18, desc);
                    break;
                case Frequencies.Q1H:
                    SetInd(17, desc);
                    break;
                case Frequencies.Q2H:
                    SetInd(16, desc);
                    break;
                case Frequencies.Q4H:
                    SetInd(15, desc);
                    break;
                default:
                    break;
            }

        }

        private void Check_15_16_17_18()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("15. Assessment q4h");
            Program.VerboseAudit("16. Assessment q2h");
            Program.VerboseAudit("17. Assessment q1h");
            Program.VerboseAudit("18. Assessment q30min");
            Program.VerboseAudit("---------------");

            int value = 0;
            string return_result = "";
            if (GetResult("", "NB.FINN.TOT", "", "", out return_result))
            {
                if (return_result.IsNumeric())
                {
                    value = (int)return_result.Val();
                    if (value >= 8) 
                        SetInd(16, "Finnegan Score=" + value);
                    else
                        SetInd(15, "Finnegan Score=" + value);
                }
            }

            CountAssessments(60);               // always do q30 buckets--but use different freq tables for icu vs non-icu.

        }

        private bool IsICU()
        {
            switch (_pat.unit_name)
            {
                case "ICU":
                case "3BR":
                case "CTIC":
                case "NIC":
                case "PCU":
                case "TICU":
                case "CCB":
                case "RICU":
                case "CCUS":
                    return true;
                default:
                    return false;
            }
        }

        private bool IsTele()
        {
            switch (_pat.unit_name)
            {
                case "4EST":
                case "2NW":
                case "3AE":
                case "5MEH":
                case "5NW":
                case "6MEH":
                case "6NW":
                case "7MEH":
                case "B4W":
                case "B5S":
                case "B5W":
                case "2NOB":
                case "CPLX":
                case "TELMS":
                case "MAN4":
                    return true;
                default:
                    return false;
            }
        }


        private void CountAssessments(int bucket_size)
        {
            int ct;
            string codelist;
            List<int> buckets;

            SetBucketSize(bucket_size);

            buckets = new List<int>();
            codelist = "250015,SHIFT.ASSESS";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Assessment=" + ct);
            if (_inds[18].is_checked) return;

            buckets = new List<int>();
            //codelist = "VS.TEMP,VS.PULSE,VS.BP,VS.RESP,RESP.POSAT,VS,LD.VS,NB.VS";
            codelist = "VS.TEMP,VS.PULSE,VS.BP,CARD,PED.CARD,PED.NB.CARD,NB.CARDREASSESS,TRAIN-OF-4,500100,CARD.DEFIB";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Cardio=" + ct);
            if (_inds[18].is_checked) return;

            buckets = new List<int>();
            codelist = "VS.RESP,RESP.POSAT,RESP,PED.RESP,PED.NB.RESP,NB.RESPASSESS,3000390,APNEA.MON,RT.APNEA,3000430,3000252,RT.CPAP,RT.TREAT,PCS.IS,3000130,3000060,RT.TRACH";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Resp=" + ct);
            if (_inds[18].is_checked) return;

            buckets = new List<int>();
            codelist = "NEURO,PED.NEURO,PED.NB.NEURO,NB.NEUROREASSE";
//            codelist = "PCA.SD,PCA.ST,PCA.DISD,PCA.DIST,PCA.DCHG,PAIN.PCA,PCA.INITIAL,PAIN.PCA.CHANGE";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Neuro=" + ct);
            if (_inds[18].is_checked) return;

            buckets = new List<int>();
            AddBuckets(buckets, "", EXACT_MATCH_PREFIX+"GI", "", "");
//            codelist = "PED.GI,PED.NB.GI,NEWBORN.ELIM,NEWBORN.FEED,NEWBORN.GASTRO";
            codelist = "PED.GI,PED.NB.GI,NEWBORN.GASTRO";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "GI=" + ct);
            if (_inds[18].is_checked) return;

            buckets = new List<int>();
//            codelist = "INT.WOUND,POST.CATH";
            AddBuckets(buckets, "", EXACT_MATCH_PREFIX + "GU", "", "");
            codelist = "PED.GU,PED.NB.GU";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "GU=" + ct);
            if (_inds[18].is_checked) return;

            buckets = new List<int>();
            AddBuckets(buckets, "", EXACT_MATCH_PREFIX+"REP", "", "");
            codelist = "REP.M,PED.REP,PED.NB.REP,NEWBORN.REP";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Reproductive=" + ct);
            if (_inds[18].is_checked) return;

            buckets = new List<int>();
            codelist = "CARD.CSM,PED.NV,PED.NB.NV,NEWBORN.EXTREMITY";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "MS/NV=" + ct);
            if (_inds[18].is_checked) return;

            buckets = new List<int>();
            AddBuckets(buckets, "", EXACT_MATCH_PREFIX + "HEENT", "", "");
            codelist = "PED.HEENT,PED.NB.HEENT,NEWBORN.HEENT";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "HEENT=" + ct);
            if (_inds[18].is_checked) return;

            buckets = new List<int>();
            AddBuckets(buckets, "", EXACT_MATCH_PREFIX + "PAIN", "", "");
            codelist = "PAIN.NIPS,PAIN.PCA,PCA.SD,PCA.DCHG,MAR.PAIN";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Pain=" + ct);
            if (_inds[18].is_checked) return;

            buckets = new List<int>();
            AddBuckets(buckets, "", EXACT_MATCH_PREFIX + "INT", "", ""); 
            AddBuckets(buckets, "", EXACT_MATCH_PREFIX + "PP", "", "");
            codelist = "PED.NB.SKIN,NEWBORN.SKIN,INT.WOUND,POST.CATH";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "INT/WOUND=" + ct);
            if (_inds[18].is_checked) return;

            buckets = new List<int>();
            codelist = "NUTR.BG,NB.HYPO,TITRATE.DO,TITRATION";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Meds=" + ct);
            if (_inds[18].is_checked) return;

        }

        //=====================================================================
        // Group like assessments into buckets of time
        // Each bucket counts as one assessment in the frequency count

        private void SetBucketSize(int minutes)
        {
            _bucket_size = minutes;
        }

        //Add to the list of bucket numbers (redundant buckets are fine)
        private void AddBuckets(List<int> bucket_list, string cat, string code_list, string desc, string field)
        {
            AddBuckets(bucket_list, cat, code_list, desc, field, "");
        }
        private void AddBuckets(List<int> bucket_list, string cat, string code_list, string desc, string field, string result_list)
        {
            // Assign (virtual) bucket numbers to each assessment.  
            // Every x minutes after that is a new bucket.  Bucket zero = pull_start time.

            // get the chart items for the assessments
            var query = StartNewQuery();
            query = AndItemFilter(query, cat, code_list, desc, field, result_list);

            // This step is needed for those who want to count the # within a bucket, like Shands #24.
            // This will have no effect on those that count buckets.
            var query2 = (from item in query select new { item.EVENT_DATETIME }).Distinct();

            // figure out what buckets the events belong to
            var query3 = from item in query2
                         select new {
                            bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size)
                         };
            // Add to the list
            foreach (var item in query3) {
                bucket_list.Add(item.bucket);
            }

            // This is just for auditing: Say what we are looking for and how many were found
            if (!String.IsNullOrEmpty(result_list)) {
                // print each word and if it was found or not
                int i = CountResultContains(cat, code_list, desc, field, result_list);
            } else {
                // print how many were found
                Program.VerboseAudit(Describe(cat, code_list, desc, field, ""));
            }
        }

        private int CountBuckets(List<int> bucket_list)       // bucket list: (ha ha)
        {
            int result = bucket_list.Distinct().Count();
            if (result > 0) Program.VerboseAudit(result + " unique");
            return result;
        }


        private void Check_19()
        {
            int ct;
            List<int> buckets;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("19. Vascular Access Site Mgt q1 Hour");
            Program.VerboseAudit("---------------");

            SetBucketSize(60);
            buckets = new List<int>();
            AddBuckets(buckets, "", "IV.2", "", "");
            ct = CountBuckets(buckets);
            if (ct >= 6)
                SetInd(19, "IV.2 count = " + ct);
        }

        private void Check_20()
        {
            string medlist;
            
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("20. Medication Activity >= 20 minutes");
            Program.VerboseAudit("---------------");
            
            //int value = 0;
            //string return_result = "";
            //if (GetResult("", "LRBC.UNIT", "", "", out return_result))
            //{
            //    if (return_result.IsNumeric())
            //    {
            //        value = (int)return_result.Val();
            //        if (value >= 2) 
            //            SetInd(20, "Transfuse units=" + value);
            //    }
            //}
            SetIndIfResultContains(20, "", "FFP.UNIT", "", "", "");
            SetIndIfResultContains(20, "", "PLTPHER.DOSE", "", "", "");
            //SetIndIfResultContains(20, "", "TX.PLASMA", "", "", "");
            //SetIndIfResultContains(20, "", "TX.PLT", "", "", "");
            SetIndIfResultContains(20, "", "GEN.ISO2", "", "", "chemo");
            if (Exists("", "GI.TUBES4", "", "", ""))
                SetIndIfResultContains(20, "", "GI.MEDPREP", "", "", "");
            // 8/8/2018 ... Madhu

            SetIndIfResultContains(20, "", "LRBC.UNIT", "", "", "");
            SetIndIfResultContains(20, "", "GI.TUBES4", "", "", "");
           
           // SetIndIfResultContains(20, "", "", "", "", "10 or greater"); // contains no other info



        }

        private void Check_21_22()
        {
            string codelist;
            
            
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("21. Wound/Injury Mgmt");
            Program.VerboseAudit("22. Wound/Injury Mgmt >= 30 Minutes");
            Program.VerboseAudit("---------------");


           

            SetIndIfResultContains(22, "", "WC.TIME", "", "", "");

            SetIndIfResultContains(21, "", "INT.WOUND", "", "", "Present");
            SetIndIfResultContains(21, "", "INT.DRAIN", "", "", "Present");
            SetIndIfResultContains(21, "", "WOUND.TYPE", "", "", "Incision, Burn, Stasis Ulcer, Pressure Ulcer, Diabetic Foot Ulcer, Amputation, Skin Tear, Skin Flap, Abrasion, Avulsion, Maceration, Laceration, Puncture, Contusion, Blister, Graft, Dehiscence, Gangrenegi, Abscess, Friction/shear Injury");
            SetIndIfResultContains(21, "", "GI.ST.CARE", "", "", "");
            SetIndIfResultContains(21, "", "GI.STCA", "", "", "");
            SetIndIfResultContains(21, "", "INF.CORDCARE", "", "", "");
            SetIndIfResultContains(21, "", "RESP.CTDSG", "", "", " changed, dry and intact, reinforced, drainage circled");
            SetIndIfResultContains(21, "", "RESP.TRACHC", "", "", "Y");
            SetIndIfResultContains(21, "", "GI.SX", "", "", "Blood in stool, Rectal Bleeding, Hemorrhoids, Blood Tinged, Bright Red Blood, Blood Clots, Coffee Grounds, Fecal Matter");
            SetIndIfResultContains(21, "", "REP.COM", "", "", "");
            SetIndIfResultContains(21, "", "REP.UTP", "", "", "");
            SetIndIfResultContains(21, "", "GU.CIFT", "", "", "");
            SetIndIfResultContains(21, "", "GU.CATHPT", "", "", "irrigat,clamped");
            SetIndIfResultContains(21, "", "WOUND.DRAIN.DSG", "", "", "changed,reinforced, drainage circled");
            SetIndIfResultContains(21, "", "WOUND.DRAI", "", "", "");
            SetIndIfResultContains(21, "", "REP.PCIRCA", "", "", "");
            SetIndIfResultContains(21, "", "NB.CIR.CARE", "", "", "");

            // Madhu
            SetIndIfResultContains(21, "", "INTEG.WOUND", "", "", "Abrasion, Abscess, Amputation, Avulsion, Burn, Cellulitis, Contusion, Dehiscence, Diabetic Ulcer, Excoriation, Friction/Shear Injury, Gangrene, Graft, Incision, Laceration, Maceration, Pressure Ulcer, Puncture, Skin Flap, Skin Tear, Status Ulcer");
            SetIndIfResultContains(21, "", "REP.PCIRCA", "", "", "Yes");
            SetIndIfResultContains(21, "", "NB.CIR.CARE", "", "", "Yes");
            SetIndIfResultContains(21, "", "WOUND.PICTAKEN", "", "", "Yes"); // ID=2100,2101,2102


            /*
               string res_list = "";
            if (GetResult("", "GI.ST.CARE", "", "", out res_list))
            {
                if (!string.IsNullOrEmpty(res_list))
                {
                    SetIndIfResultContains(21, "", "GI.ST.CARE", "", "", res_list);

                }
                else
                    SetIndIfResultContains(21, "", "GI.ST.CARE", "", "", "");

            }
            */
            
            //SetIndIfResultContains(21, "", "GI.ST.CARE", "", "", "Accept any Response");
            SetIndIfResultContains(21, "", "GI.STCA", "", "", "");
            SetIndIfResultContains(21, "", "INF.CORDCARE", "", "", "");
            SetIndIfResultContains(21, "", "RESP.CTDSG", "", "", "CHANGED,REINFORCED");
            SetIndIfResultContains(21, "", "RESP.TRACHC", "","","YES");
            SetIndIfResultContains(21, "", "RESP.TRTC", "", "", "YES");
            SetIndIfResultContains(21, "", "GI.SX2", "", "", "HEMORRHOIDS,RECTAL BLEEDING");
            SetIndIfResultContains(21, "", "REP.COM", "", "", "");
            SetIndIfResultContains(21, "", "REP.UTP", "", "", "");
            SetIndIfResultContains(21, "", "GU.CIFT", "", "", "");
            SetIndIfResultContains(21, "", "GU.CATHPT", "", "", "Continuous Irrigation,Irrigated");
            SetIndIfResultContains(21, "", "WOUND.DRAINDRES", "", "", "Changed, reinforced");
            SetIndIfResultContains(21, "", "WOUND.DRAI", "", "", "");
           // SetIndIfResultContains(21, "", "WOUND.DRAINDRES", "", "", "Both Wound Vac + Dressing Status Changed");
            SetIndIfResultContains(21, "", "", "", "", "Wound Vac");

        }
       



        // Use this if you can total the education time
        //private void CheckEducation(int total)
        //{
        //    if (_inds[23].is_checked) return;             //skip if already checked

        //    if (total >= 60) {
        //        SetInd(23, "education >= 60 min");
        //    }
        //}

        private void Check_23()
            {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("23. Healthcare Mgmt Education >= 1 Hour");
            Program.VerboseAudit("---------------");

            int tot = GetTotalValue("", "TEACH.TIME", "", "", "");
            if (tot >= 45)
                SetInd(23, "Teaching time total in 12 hours = " + tot + " mins");
            }

        private void Check_24()
        {
            string s;
            string return_result;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("24. 1 to 1 Physiological Interv. >= 2 Hours");
            Program.VerboseAudit("---------------");

            //SetIndIfResultContains(24, "", "PLUS1HRACTIVITIES", "", "", "");
            if (GetResult("", "OEST", "", "", out return_result))
                Program.VerboseAudit("OEST result is:" + return_result);
            if (GetResult("", "OESTP", "", "", out return_result))
                Program.VerboseAudit("OESTP result is:" + return_result);

            ////A_Pulse, A_BP, A_MHRespRate, A_PulseOx   count of 9 in 2 hours
            //codelist = "ACS,AMI,PN,CARD.CSM,INT,HEENT,GI,GU,REP,REP.M,NUTR.BG,PED.CARDIAC,PES.HEENT,RESP.MVC,RESP.BIPAP,RESP.IS,RESP.SPUTUM,RESP.TRACH,TOF";

            //SetBucketSize(60);                                  // set one hour buckets
            //var buckets = new List<int>();                      // list of bucket numbers (for each match)
            //AddBuckets(buckets, "", codelist, "", "", "");
            //// Instead of counting the number of buckets to get the frequency,
            //// we want to see if any two hours in a row had 9 or more assessments in the bucket.
            //// Make a query that returns each bucket number and its item count.
            //var query = buckets.GroupBy(x => x)                 // group by the hour (x is an int)
            //                   .Select(g => new { Hour = g.Key, Count = g.Count() })
            //                   .OrderBy(e => e.Hour);
            //var arr = query.ToArray();

            //s = "";
            //foreach (var e in arr)
            //{
            //    s += ",(" + e.Hour + "," + e.Count + ")";
            //}
            //if (s.Left(1) == ",") s = s.Substring(1);
            //Program.VerboseAudit("Assessment hourly counts for 1:1 Phys Interv: " + s);

            //for (int i = 0; (i < arr.GetUpperBound(0)); i++)
            //{
            //    // Are these two hours in a row? (beware missing hours)
            //    if (arr[i].Hour == arr[i + 1].Hour - 1)
            //    {
            //        // Add these two consecutive hours
            //        if (arr[i].Count + arr[i + 1].Count >= 9)
            //        {
            //            SetInd(24, "9 or more Assess in 2 hours.");
            //            break;
            //        }
            //    }
            //}

            ////if (query.Count() == 0)
            ////{
            ////    Program.VerboseAudit("No documentation found for VS items: A_Pulse, A_BP, A_MHRespRate, A_PulseOx");
            ////}
        }


        private void AtLeastOneADL()
        {
            Program.Audit("---------------");
            if (!(_inds[1].is_checked || _inds[2].is_checked || _inds[3].is_checked || _inds[4].is_checked)) {
                // Make "#2 ADL - Assist" the default.  (90% of patients)
                SetInd(2, "Defaulting to partial care due to lack of documentation.");
            }
        }

        private void HighestIndicatorInEachGroupWins()
        {
            int i, g;
            bool highest_is_on;
            string ind_list;

            Program.Audit("Select highest indicator in each group");
            
            g = 0;
            highest_is_on = false;
            // Go from bottom up - highest (lowest) indicator in each group wins
            for (i = MAX_INDS; (i >= 1); i--) {
                if (_inds[i].radio_group > 0) {
                    if (_inds[i].radio_group != g) {
                        //this is a new group
                        g = _inds[i].radio_group;
                        highest_is_on = _inds[i].is_checked;
                    } else {
                        //same group
                        if (highest_is_on) {
                            _inds[i].is_checked = false;             //uncheck a lower number
                        } else {
                            highest_is_on = _inds[i].is_checked;     //save this one
                        }
                    }
                }
            } // next i
            
            //Echo the indicators for an Audit (no classification will be saved)
            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++) {
                if (_inds[i].is_checked) ind_list += "," + i;
            } // next i

            Program.Audit("Final list = " + ind_list.Substring(1));
            Program.Audit("---------------");
        }


        private void CheckProcs()
        {
            //CheckProc_1();
            CheckProc_2();
            CheckProc_3();
            CheckProc_4();
            //CheckProc_5();
            //CheckProc_6();
            CheckProc_7();
            CheckProc_8();
            CheckProc_9();
            CheckProc_10();
            CheckProc_11();

        }

        private void DoProc(int pnum, string code)
        {
            double mins = 0;
            string found_what;
            DateTime evdt;
            DateTime enddt = DateTime.MinValue;

            if (GetResultAndEVDT("", code, "", "", out found_what, out evdt))
            {
                if (found_what.IsNumeric())
                    mins = found_what.ToDouble();
                if (mins >= 60)
                {
                    enddt = evdt.AddMinutes(mins);

                    if (ProcExistsInDB(pnum, evdt, enddt))
                    {
                        Program.Audit("Procedure " + pnum + ": already exists");
                    }
                    else
                    {
                        if (!QueuedProcOverlaps(pnum, evdt, enddt))
                        {
                            var proc = new proc_data();
                            proc.procedure_number = pnum;
                            proc.start = evdt;
                            proc.finish = enddt;
                            _procs.Add(proc);
                            Program.Audit("Procedure " + pnum + ": Found " + code + " between " + evdt + " and " + enddt);
                        }
                    }
                }

            }

        }

        private bool ProcExistsInDB(int pnum, DateTime startdt, DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && (proc.PROCEDURE_DATETIME == startdt)
                            && (proc.DEPARTURE_DATETIME == enddt)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new { proc.PROCEDURE_EVENT_ID };
            return (query.Count() > 0);
        }

        private bool QueuedProcOverlaps(int pnum, DateTime startdt, DateTime enddt)
        {
            bool overlap = false;

            proc_data[] pary = _procs.ToArray();
            for (int i = 0; i <= pary.GetUpperBound(0); i++)
            {
                if (pary[i].procedure_number == pnum)
                {
                    //overlap if   p.start between stardt and enddt
                    //             p.finish between startdt and enddt
                    //             startdt >= p.start and enddt <= p.finish
                    if ((pary[i].start >= startdt) && (pary[i].start < enddt) ||
                        (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
                        (startdt >= pary[i].start) && (enddt <= pary[i].finish))
                    {
                        overlap = true;
                    }

                    if ((pary[i].start >= startdt) && (pary[i].start < enddt) && (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
                        (pary[i].start < startdt) && (pary[i].finish > startdt) && (pary[i].finish < enddt)
                        )
                    {
                        // the proc in the list is completely contained in startdt/endt pair
                        // change this proc to be the startdt/enddt
                        // overlap is still true, but the list item will be updated with the encompassing times
                        //Program.Audit("Procedure " + pnum + ": " + startdt + " - " + enddt + " will trump " + pary[i].start + " - " + pary[i].finish);
                        pary[i].start = startdt;
                        pary[i].finish = enddt;
                    }
                }
            }
            _procs = pary.ToList();
            return overlap;
        }


        private void CheckProc_1()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P1. 1-1 safety observation by RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_2()
        {
            string nowstr;
            string toddtstr;
            string yesdtstr;
            string timea ="";
            string sdstr = "", ststr = "", edstr = "", etstr = ""; //Start Date,Start Time,End Date,End Time strings
            DateTime sdevdt,stevdt,edevdt,etevdt;

            DateTime timea_startdt, timea_enddt;
            DateTime timeb_startdt, timeb_enddt;
            DateTime nowdt = _pat.pull_finish;              // "now" is pull time
            DateTime yesdt;
            
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P2. 1-1 safety observation by non-RN");
            Program.VerboseAudit("---------------");

            GetResultAndEVDT("", "APS.SSD", "", "SITTER.BED", out sdstr, out sdevdt);
            GetResultAndEVDT("", "APS.SST", "", "SITTER.BED", out ststr, out stevdt);
            GetResultAndEVDT("", "APS.SED", "", "SITTER.BED", out edstr, out edevdt);
            GetResultAndEVDT("", "APS.SET", "", "SITTER.BED", out etstr, out etevdt);
            if (sdevdt != DateTime.MinValue && stevdt != DateTime.MinValue && edevdt != DateTime.MinValue && etevdt != DateTime.MinValue)
            {
                if ((sdevdt == stevdt) && (edevdt == etevdt))
                {
                    LapsedTime(sdstr, ststr, edstr, etstr);
                }
            }

            //nowstr = nowdt.ToString("yyyyMMddHHmm");
            //yesdt = nowdt.AddDays(-1);
            //toddtstr = nowdt.ToString("yyyyMMdd");
            //yesdtstr = yesdt.ToString("yyyyMMdd");
            
            ////when is now? (yesterday/today)
            ////yes 7am -- yes 7p  -- tod 7a -- tod 7p
            ////                                   A                   B
            ////if nowdt >= tod7pm then check  tod 7am-tod 7pm and tod 7pm-tom 7a
            ////if nowdt >= tod7am then check  yes 7p - tod 7a     tod 7a-tod 7p
            ////if nowdt >= yes7pm then check  yes 7a-yes 7p       yes 7p-tod 7a
            //if (nowstr.CompareTo(toddtstr + "1900") >= 0) {
            //    timea = toddtstr + "0700";
            //} else if (nowstr.CompareTo(toddtstr + "0700") >= 0) {
            //    timea = yesdtstr + "1900";
            //} else if (nowstr.CompareTo(yesdtstr + "1900") >= 0) {
            //    timea = yesdtstr + "0700";
            //}
            
            //timea_startdt = PFSUtility.ISOToDateTime(timea);
            //timea_enddt = timea_startdt.AddHours(12);
            //MaybeAddSitter(timea_startdt, timea_enddt);

            //timeb_startdt = timea_enddt;
            //timeb_enddt = timeb_startdt.AddHours(12);
            //MaybeAddSitter(timeb_startdt, timeb_enddt);
        }

        private void LapsedTime(string sdstr, string ststr, string edstr, string etstr)
        {
            if (sdstr != null && ststr != null && edstr != null && etstr != null)
            {
                DateTime sdt = PFSUtility.ISODateToDateTime(sdstr + ststr);
                DateTime edt = PFSUtility.ISODateToDateTime(edstr + etstr);
                if (edt > sdt)
                {
                    double mindiff = PFSUtility.DateDiffInMinutes(sdt, edt);
                    if (mindiff >= 60)
                        MaybeAddSitter(sdt, edt);
                }
            }
            return;
        }


        private void MaybeAddSitter(DateTime startdt, DateTime enddt)
        {
            //var query = StartNewQuery(SearchDepth.SearchSinceAdmission);    // add custom time range below
            //query = query.Where(e => (e.EVENT_DATETIME >= startdt) && (e.EVENT_DATETIME < enddt));
            //query = AndCodeInList(query, "Sitter");
            //query = AndResultInList(query,"continued, initiated");
            //query = AndResultNotInList(query, "discontinued");

            //if (query.Count() > 0) {
                if (ProcExists(2, startdt, enddt)) {
                    Program.Audit("Procedure 2: already exists");
                } else {
                    var proc = new proc_data();
                    proc.procedure_number = 2;
                    proc.start = startdt;
                    proc.finish = enddt;
                    _procs.Add(proc);
                    Program.Audit("Procedure 2: Found Sitter between " + startdt + " and " + enddt);
                }
            //}
            
        }

        private bool ProcExists(int pnum, DateTime startdt, DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && (proc.PROCEDURE_DATETIME == startdt)
                            && (proc.DEPARTURE_DATETIME == enddt)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new {proc.PROCEDURE_EVENT_ID};
            return (query.Count() > 0);
        }


        private void CheckProc_3()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P3. Off unit accompanied by RN");
            Program.VerboseAudit("---------------");
            DoProc(3, "OFFUNITRN");
        }

        private void CheckProc_4()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P4. Off unit accompanied by non-RN");
            Program.VerboseAudit("---------------");
            DoProc(4, "OFFUNITBYNONRN");
        }

        private void CheckProc_5()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P5. Patient/family education by RN");
            Program.VerboseAudit("---------------");
            DoProc(5, "A_MHAcuPtFamEduc");
        }

        private void CheckProc_6()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P6. Extensive wound management by RN");
            Program.VerboseAudit("---------------");
            DoProc(6, "A_MHAcuExtensive");
        }

        private void CheckProc_7()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P7. Extensive wound management by non-RN");
            Program.VerboseAudit("---------------");
            DoProc(7, "EXTWOMGNRN");
        }

        private void CheckProc_8()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P8. Coordination of care by RN");
            Program.VerboseAudit("---------------");
            DoProc(8, "COCARN");
        }

        private void CheckProc_9()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P9 1:1 RN at bedside");
            Program.VerboseAudit("---------------");
            DoProc(9, "BEPRRN");
        }

        private void CheckProc_10()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P10. 1:1 non-RN at bedside");
            Program.VerboseAudit("---------------");
            DoProc(10, "BEPRNNRN");
        }

        private void CheckProc_11()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P11. 2:1 by RN at bedside");
            Program.VerboseAudit("---------------");
            DoProc(11, "BEPR2RN");
        }

        //private void CheckOutcomes()
        //{
        //    var query = StartNewQuery(SearchDepth.SearchDefault);
        //    query = AndItemFilter(query, "", "A_MHPostFalltime", "", "", "");
        //    foreach (var ch in query)
        //    {
        //        var outc = new proc_data();
        //        outc.procedure_number = 1;
        //        outc.start = ch.EVENT_DATETIME;
        //        _outcomes.Add(outc);
        //        Program.Audit("Outcomes 1: Found A_MHPostFalltime at " + outc.start);
        //    }
        //}
        

        private const string DATETIME_FORMAT = "yyyyMMddHHmm";              // ISO Date/Time w/o seconds

        private void OutputClass(int elig_for_default, int unitid)
        {
            string outstr, ind_list, desc, str_pull_dt;
            int i, tc_event_id;
            string unitname;

            unitname = GetUnitName(unitid);

            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         212 242 304
            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1
            //1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901 3456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
            //1       |10 5            |                |                |        |10023121779         |ALEXANDER                       |GLORIA                          |                                |0508    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289530  |           |201403030400                                                                      |NNYNNNNYYNNYNNNYNNNYYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//1       |10 5            |                |                |        |10024544908         |ARMSTRONG                       |AARON                           |                                |0535    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289531  |           |201403030400                                                                      |NYNNYNNNNNNYNNNYNNNNYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//|0547    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289532  |           |201403030400                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//|0547    |A   |201403030700|                |201403030700    |18  |C|    |3040|1440|18289532  |           |201403030700                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN

            if (Program.g_is_test)
                tc_event_id = 9999;
            else
                tc_event_id = PFSUtility.NextGID();                         //get a unique id for this class
            str_pull_dt = _pat.pull_finish.ToString(DATETIME_FORMAT);

            outstr = _pat.facilty_code.FixedWidth(8);                       //(facility code)
            outstr += "|" + _pat.unit_name.FixedWidth(16);
            outstr += "|" + "".FixedWidth(16);                               //(unit code)
            outstr += "|" + "".FixedWidth(16);                               //(area code)
            outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
            outstr += "|" + _pat.acct.FixedWidth(20);
            outstr += "|" + _pat.last_name.FixedWidth(32);
            outstr += "|" + _pat.first_name.FixedWidth(32);
            outstr += "|" + _pat.middle_name.FixedWidth(32);
            outstr += "|" + _pat.room.FixedWidth(8);
            outstr += "|" + _pat.bed.FixedWidth(4);
          outstr += "|" + str_pull_dt.FixedWidth(12);                      //class datetime (could change)
            outstr += "|" + "".FixedWidth(16);                               //(login)
          outstr += "|" + str_pull_dt.FixedWidth(16);                      //(employee)/(pull datetime)
            outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
            outstr += "|" + "C".FixedWidth(1);                               //record type = class
            outstr += "|" + "".FixedWidth(4);                                //(stage)
            outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
            outstr += "|" + _pat.range.ToString().FixedWidth(4);             //TC pull range
            outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
            outstr += "|";
            outstr = outstr.FixedWidth(294);
          //outstr += "|" + _pat.effective.ToString(DATETIME_FORMAT);        //IN
            outstr += "|" + str_pull_dt.FixedWidth(12);        //IN
          outstr = outstr.FixedWidth(377);
            outstr += "|";

            if (elig_for_default == 1)
                if (EarliestLocationInPast4hrs(unitid))
                { //make all is_checked = false and then mark defaults
                    Program.VerboseAudit("Patient will receive default indicators " + _pat.default_inds_str);
                    for (i = 1; (i <= MAX_INDS); i++)
                    {
                        _inds[i].is_checked = false;
                    }
                    foreach (var ind in _pat.default_inds)
                    {
                        if (ind <= _inds.GetUpperBound(0))
                        {
                            _inds[ind].is_checked = true;
                        }
                    }
                }
            


            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++) {
                if (_inds[i].is_checked) {
                    outstr += "Y";
                    ind_list += "," + i;
                } else {
                    outstr += "N";
                }
            } // next i
            ind_list = ind_list.Substring(1);                           //strip leading comma
  
            Program.outfile.WriteLine(outstr);                          //output to transparent.txt

            Program.Audit("");
            desc = "Classified: " + ind_list;
            if (Program.g_is_test) {
                Program.Audit(desc);
            } else {
                //Save the selected indicators plus both Program.Audits in the event log; link with tc_event_id
                PFSEventLog.AddTransparentMappingEventLogEntry(
                    desc, Program.gLogUnitID, Program.gLogEncounterID,
                    tc_event_id, Program.gLogMapperVersion,
                    Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
            }
        }

        private bool EarliestLocationInPast4hrs(int unitid)
        {
            DateTime min_dt = DateTime.MinValue;
            //get min enc loc effdt
            var db = PFSUtility.NewPfsDataContext();

            var query = from el in db.ENCOUNTER_LOCATIONs
                        where (el.ENCOUNTER_ID == _pat.encounter_id)
                        select new
                        {
                            el.EFFECTIVE_DATETIME_IN
                        };
            min_dt = PFSUtility.DBToDateTime(query.Min(x => x.EFFECTIVE_DATETIME_IN));
            if (System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(min_dt, Program.g_pull_finish) <= 240)
            {
                Program.VerboseAudit("Earliest location time is within the past 4 hours");
                return true;
            }

            return false;

        }


        private void OutputProcs()
        {
            int i;
            string outstr, proc_list, desc;
            int tc_event_id;

            foreach(var proc in _procs) {
                if (Program.g_is_test)
                    tc_event_id = 9999;
                else
                    tc_event_id = PFSUtility.NextGID();                         //get a unique id for this proc

                outstr = _pat.facilty_code.FixedWidth(8);
                outstr += "|" + _pat.unit_name;                                 //10
                outstr = outstr.FixedWidth(68);
                outstr += "|" + _pat.acct.FixedWidth(20);                       //90
                outstr += "|" + _pat.last_name.FixedWidth(32);
                outstr += "|" + _pat.first_name.FixedWidth(32);
                outstr += "|" + _pat.middle_name.FixedWidth(32);
                outstr = outstr.FixedWidth(202);
                outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //204 proc dt
                outstr = outstr.FixedWidth(254);
                outstr += "|P";                                                 //256 procedure type record
                outstr += "|" + "".FixedWidth(4);                               //(stage)
                outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);     //TC source ID
                outstr += "|" + _pat.range.ToString().FixedWidth(4);            //TC pull range
                outstr += "|" + tc_event_id.ToString().FixedWidth(10);          //TC event ID
                outstr += "|";
                outstr = outstr.FixedWidth(294);
                outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //296 procdt in
                outstr = outstr.FixedWidth(346);
                outstr += "|" + proc.finish.ToString(DATETIME_FORMAT);          //348 procdt out
                outstr = outstr.FixedWidth(377);
                outstr += "|";
                
                proc_list = "";
                for (i = 1; (i < MAX_PROCS); i++) {
                    if (proc.procedure_number == i) {
                        outstr += "Y";
                        proc_list += "," + i;
                    } else {
                        outstr += "N";
                    }
                } // next i
                proc_list = proc_list.Substring(1);                             //strip leading comma

                Program.outfile.WriteLine(outstr);                              //output to transparent.txt

                desc = "Procedures: " + proc_list;
                if (Program.g_is_test) {
                    Program.Audit(desc);
                } else {
                    //Save the selected procedures plus both Program.Audits in the event log; link with tc_event_id
                    //(this assumes the procedure Program.Audit is mixed in with the class Program.Audit)
                    PFSEventLog.AddTransparentMappingEventLogEntry(desc, Program.gLogUnitID, Program.gLogEncounterID,
                        tc_event_id, Program.gLogMapperVersion,
                        Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
                }
            } // next proc
        }

        private void OutputOutcomes()
        {
            string outstr;

            foreach (var oc in _outcomes)
            {
                outstr = "".FixedWidth(9) + _pat.unit_name.FixedWidth(16);       // unitname
                outstr = outstr.FixedWidth(60);
                outstr += oc.start.ToString(DATETIME_FORMAT);                 //event_datetime
                outstr = outstr.FixedWidth(73);
                outstr += _pat.acct.FixedWidth(20);                            //acct
                outstr = outstr.FixedWidth(94);
                outstr += oc.procedure_number;                                //outcome indicator
                Program.out2file.WriteLine(outstr);                          //output to outcomesindicator.txt
            }

        }
        private int DeterminePtypeOfIndicators(int methid)
        {
            int i;
            var pscore = 0.0;
            int pt_type = 6;
            var indlist = new List<int>();
            string indstr = "";

            Program.VerboseAudit("Determine patient type of triggered indicators");

            for (i = 1; (i <= MAX_INDS); i++)
            {
                if (_inds[i].is_checked)
                {
                    indlist.Add(i);
                    indstr += i.ToString() + ",";
                }
            }

            var db = PFSUtility.NewPfsDataContext();
            var query_ind_def = from ind_def in db.INDICATOR_DEFINITIONs
                                where (ind_def.METHODOLOGY_ID == methid) &&
                                  indlist.Contains(ind_def.INDICATOR_NUMBER)
                                select new
                                {
                                    ind_def.WEIGHT
                                };
            foreach (var wgts in query_ind_def)
            {
                pscore += wgts.WEIGHT;
            }
            Program.VerboseAudit("indicators=" + indstr);
            Program.VerboseAudit("score=" + pscore.ToString());

            var query_ptype = from ptype in db.PATIENT_TYPEs
                              where (ptype.METHODOLOGY_ID == methid)
                              orderby ptype.PATIENT_TYPE1
                              select new
                              {
                                  ptype.PATIENT_TYPE1,
                                  ptype.POINTS_HIVAL
                              };

            foreach (var ptypes in query_ptype)
            {
                //                Program.DebugTrace("type,hival=" + ptypes.PATIENT_TYPE1.ToString() + "," + ptypes.POINTS_HIVAL.ToString(), "");
                if (pscore <= ptypes.POINTS_HIVAL)
                {
                    if (pt_type > ptypes.PATIENT_TYPE1)
                    {
                        pt_type = ptypes.PATIENT_TYPE1;
                    }
                }
            }
            Program.VerboseAudit("patient type=" + pt_type.ToString());

            return pt_type;

        }

        


    }
}
