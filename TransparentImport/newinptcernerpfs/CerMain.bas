Attribute VB_Name = "CerMain"
Option Explicit
'The location of the input is determined by the first line of the dictionary.
'
' This is the main module for CerPFS
' File is formatted as:
'    <Header>
'       <Event>
'       <Event>
'         ...   This should be the NEWInptCerPFS
'         ...   This should be the NEWInptCerPFS
'         ...   This should be the NEWInptCerPFS
'         ...   This should be the NEWInptCerPFS
'         ...   This should be the NEWInptCerPFS
'
Const TRANSP_FILENAME = "Transparent.txt"
Const TRANSP_INLOG_FILE_LIFE = 5 'days
Const TRANSP_OUTLOG_FILE_LIFE = 30 'days
Const DNLD_FNAME = "CEWINPFS"
Const PILOTUNITS_FNAME = "PILOT.UNT"

'HEADER RECORD
Const MAX_INDICATORS = 50
Const MAX_PROCS = 10

Const START_ACCT_NUM = 1
Const LEN_ACCT_NUM = 20

Const START_PTNAME = 41 '21
Const LEN_PTNAME = 30

Const START_UNIT = 21 '51
Const LEN_UNIT = 10

Const START_RM = 31 '61
Const LEN_RM = 5

Const START_BED = 36 '66
Const LEN_BED = 5

'EVENT RECORD
Const START_EVENT = 44
Const LEN_EVENT = 16

Const START_RESULT = 78
Const LEN_RESULT = 240

Const START_DATETIME = 60
Const LEN_DATETIME = 17

Const RANGE_DENOMINATOR = 24 * 60   'For proportioning the count of Med/Pulm/Cardio

Const MAX_UNIQUE_CHART_TIMES = 60

Dim g_util As New RegistryUtils

Dim infn As String
Dim fnames() As String
Dim outfn As String
Dim inlogfile As Integer
Dim inlogname As String
Dim outlogfile As Integer
Dim outlogname As String
Dim dbugfile As Integer
Dim dbugname As String

Private Type cerindicator
  indwinpfs As Integer  'winpfs indic number associated with this cerner event
  eventid As String     'Event_CD of this indicator
  chartkey As String    'look for this key in charted result
  check_freq As Boolean 'flag to check frequency
  freq_basis As Integer 'frequency required for this indicator (in minutes)
  act_freq As Integer   'calculated frequency from data (in minutes)
  last_datetime As String 'last date time of event found in this patient
  num_found As Integer  'number of times this was found in this patient
  also_mark As String   'if this indicator is marked, then also mark these.
  charttime(MAX_UNIQUE_CHART_TIMES) As String
End Type

Private Type indicator_data
    checked As Boolean
    also_mark As String
End Type

Dim dicary() As cerindicator
Dim dicnum As Integer
Dim inDirPath As String  'path of input file
Dim winpfspath As String 'path of winpfs
Dim winlogpath As String 'path of winpfs\log
Dim winloadpath As String ' path of winpfs\load_me

Dim datafile As Integer
Dim outfile As Integer

Dim inds(MAX_INDICATORS) As indicator_data
Dim grps(MAX_INDICATORS) As Integer

Dim lastname As String
Dim firstname As String
Dim unitname As String
Dim roomname As String
Dim bedname As String
Dim acctnum As String
Dim classdt As String
Dim classdate As String
Dim classtime As String
Dim nowdt As Date
Dim intime As String
Dim saveintime As String
'Dim event21793372 As Boolean

Dim range As Single  'number of minutes in the scope of time for freq.
Dim dbugon As Boolean
Dim suppressDbugLog As Boolean
Dim alterdateon As Boolean
Dim alterdate As String
Dim pulltimeon As Boolean
Dim pulltime As String
Dim pulldateon As Boolean
Dim pulldate As String
Dim telemetry As Boolean
Dim emus As Boolean
Dim nimcu As Boolean
Dim unitnum As Integer
Dim unitary() As String
Dim classinon As Boolean
Dim classindt As String
Dim classouton As Boolean
Dim classoutdt As String
Dim pt_age As Single 'in years

Dim SeeRestraint As Boolean
Dim SeeksRNAssist As Boolean
Dim totnumprocs As Integer
Private Type proctype
    pnum As Integer
    s As String
    e As String
    isdup As Boolean
End Type
Dim procs(MAX_PROCS) As proctype


Sub Main()
    Const RANGE_PARAM = "-range="
    Const EFFECTIVE_PARAM = "-effective="
    Const DBUG_ON = "debug"
    Const ALTER_DATE = "-date="
    Const PULL_TIME = "-pulltime="
    Const PULL_DATE = "-pulldate="
    Const CLASS_INDT = "-classindt="   'pos 296 in the import file
    Const CLASS_OUTDT = "-classoutdt=" 'pos 348 in the import file
    Dim n As Integer
    Dim i As Integer
    Dim cmdLine As String
    Dim epos As Integer
    Dim rpos As Integer
    Dim effective As String
    Dim h As String
    Dim t As Date
    Dim adpos As Integer
    
    '-effective=hhmm  This is the time at which the classification is effective.
    '                 The date associated with this time is taken from Now.
    '                 If not specified, then this -effective time is assumed to be Now.
    
    '-pulltime=hhmm  This is a time, on Now's date, to specify the starting point at which
    '                the pull is to look backwards from.  If not specified,
    '                Then this time is assumed to be the effective time.
    '-pulldate=yyyymmdd This is the date of the pulltime.
    
    '-date=yyyymmdd   This is the date which, in combination with the
    '                 -effective time is used to specify the effective datetime.
    '                 In the absence of this parameter, Now's date is used.
    'Special value:  -date=yesterday means Now's yesterday.
    
    '-range=nnnn  This is the number of minutes backwards from the pull time
    '             that defines valid charting events.
    
    nowdt = Now
    cmdLine = LCase(Command$)
    
    rpos = InStr(cmdLine, RANGE_PARAM)
    epos = InStr(cmdLine, EFFECTIVE_PARAM)
    
    dbugon = (InStr(cmdLine, DBUG_ON) > 0)
    suppressDbugLog = False
    
    alterdateon = (InStr(cmdLine, ALTER_DATE) > 0)
    If alterdateon Then
        adpos = InStr(cmdLine, ALTER_DATE)
        If UCase$(Mid$(cmdLine, adpos + Len(ALTER_DATE), 8)) = "YESTERDA" Then
            alterdate = Format(DateAdd("d", -1, g_util.DateOnly(nowdt)), "yyyymmdd")
        Else
            alterdate = Mid$(cmdLine, adpos + 6, 8) 'yyyymmdd
        End If
    End If
    
    pulltimeon = (InStr(cmdLine, PULL_TIME) > 0)
    If pulltimeon Then
        pulltime = Mid$(cmdLine, InStr(cmdLine, PULL_TIME) + Len(PULL_TIME), 4)
    End If
    
    pulldateon = (InStr(cmdLine, PULL_DATE) > 0)
    If pulldateon Then
        pulldate = Mid$(cmdLine, InStr(cmdLine, PULL_DATE) + Len(PULL_DATE), 8)
    End If
    
    classinon = (InStr(cmdLine, CLASS_INDT) > 0)
    If classinon Then
        classindt = Mid$(cmdLine, InStr(cmdLine, CLASS_INDT) + Len(CLASS_INDT), 12)
    End If
    
    classouton = (InStr(cmdLine, CLASS_OUTDT) > 0)
    If classouton Then
        classoutdt = Mid$(cmdLine, InStr(cmdLine, CLASS_OUTDT) + Len(CLASS_OUTDT), 12)
    End If
    

    If epos > 0 Then
        effective = Mid$(cmdLine, InStr(cmdLine, EFFECTIVE_PARAM) + Len(EFFECTIVE_PARAM), 4)
    End If
    
    If rpos > 0 Then
        range = val(Mid$(cmdLine, InStr(cmdLine, RANGE_PARAM) + Len(RANGE_PARAM), 4))
    End If
    
    If range <= 0 Then
        range = 480
    End If
    
    intime = ""
    
    If (LoadDictionary) Then
        'InitGroups
        MakeLogFiles
        dprint -1, 0, "alterdate=" & alterdate
        dprint -1, 0, "pulltime=" & pulltime
        dprint -1, 0, "pulldate=" & pulldate
        dprint -1, 0, "effective=" & effective
        dprint -1, 0, "range=" & range
        If IsNull(effective) Then
            effective = ""
        Else
            ' command parameter needs to be in form hhmm ONLY
            If IsNumeric(effective) Then
                If Len(effective) < 4 Then
                    If Len(effective) = 1 Then
                        effective = "000" & effective
                    ElseIf Len(effective) = 2 Then
                        effective = "00" & effective
                    Else
                        effective = "0" & effective
                    End If
                Else
                    effective = Mid$(effective, 1, 4)
                End If
                effective = Mid$(effective, 1, 2) & ":" & Mid$(effective, 3, 2)
                t = CDate(effective)
                If IsDate(t) Then
                    effective = Mid$(effective, 1, 2) & Mid$(effective, 4, 2)
                    i = year(g_util.DateOnly(FileDateTime(outlogname)))
                    intime = CStr(i) & Mid$(outlogname, Len(outlogname) - Len("mmdd.log") + 1, Len("mmdd")) & effective
                    If alterdateon Then
                        intime = alterdate & Mid$(intime, 9, 4)
                    End If
                    saveintime = intime
                End If
            End If
        End If
        n = GetAllInputFilenames
'        i = 0      ONLY PROCESS THE LATEST FILE 11/4/05
        i = n - 1  'ONLY PROCESS THE LATEST FILE 11/4/05
        While i < n
            i = i + 1
            infn = inDirPath + "\" + fnames(i)
            Process
        Wend
        CloseLogFiles
        DeleteOldLogs
    End If

End Sub
Private Function LoadDictionary() As Boolean
    Dim dicfn As String
    Dim dicfile As Integer
    Dim buf As String
    Dim unitfn As String
    Dim unitfile As Integer

    dicnum = 0
    dicfile = FreeFile
    dicfn = App.Path & "\" & "INPTMETH.DAT"
    Open dicfn For Input As #dicfile
    While Not EOF(dicfile)
        dicnum = dicnum + 1
        If (dicnum = 1) Then
            Line Input #dicfile, inDirPath
        Else
            Line Input #dicfile, buf
            ReDim Preserve dicary(0 To dicnum - 1)
            dicary(dicnum - 1).indwinpfs = val(Mid$(buf, 1, 2))
            dicary(dicnum - 1).eventid = Trim$(Mid$(buf, 4, 17))
            dicary(dicnum - 1).chartkey = Trim$(Mid$(buf, 38, 75))
            dicary(dicnum - 1).check_freq = (Mid$(buf, 3, 1) = "*")
            dicary(dicnum - 1).freq_basis = val(Trim$(Mid$(buf, 22, 4)))
            dicary(dicnum - 1).also_mark = Trim$(Mid$(buf, 28, 10))
        End If
    Wend
    Close #dicfile
    dicnum = dicnum - 1
    
    unitnum = 0
    unitfile = FreeFile
    unitfn = App.Path & "\" & PILOTUNITS_FNAME
    Open unitfn For Input As #unitfile
    While Not EOF(unitfile)
        Line Input #unitfile, buf
        If Trim$(buf) <> "" Then
            unitnum = unitnum + 1
            ReDim Preserve unitary(0 To unitnum)
            unitary(unitnum) = UCase$(Trim$(Mid$(buf, 1, 16)))
        End If
    Wend
    Close #unitfile
    
    LoadDictionary = True
End Function

Private Function GetAllInputFilenames() As Integer
    'The location is determined by the first line of the dictionary.
    Const INITIAL_BLOCK_SIZE = 24
    Dim i As Integer
    Dim infname As String
    
    outfn = winloadpath & "\" & TRANSP_FILENAME
    
    ReDim fnames(0 To INITIAL_BLOCK_SIZE) As String
    i = 0
    infname = Dir$(inDirPath + "\" & DNLD_FNAME & ".*") 'returns ONLY the filename.  For wildcards.
    While infname <> ""
        If InStr(infname, ".txt") = 0 And InStr(infname, ".dat") = 0 Then
            i = i + 1
            If (i > UBound(fnames)) Then
                ReDim Preserve fnames(0 To i + INITIAL_BLOCK_SIZE) As String
            End If
            fnames(i) = infname
        End If
        infname = Dir$
    Wend
    
    BubbleSort fnames, i
    
    GetAllInputFilenames = i
    
End Function

Private Sub BubbleSort(arr As Variant, numEls As Integer)
    Dim lLoop1 As Integer
    Dim lLoop2 As Integer
    Dim lTemp As Variant
    
    For lLoop1 = numEls To LBound(arr) Step -1
       For lLoop2 = LBound(arr) + 1 To lLoop1
           If arr(lLoop2 - 1) > arr(lLoop2) Then
              lTemp = arr(lLoop2 - 1)
              arr(lLoop2 - 1) = arr(lLoop2)
              arr(lLoop2) = lTemp
           End If
       Next lLoop2
   Next lLoop1
End Sub

Private Sub DoPatientSummary()
    Dim highest_is_on As Boolean
    Dim g As Integer
    Dim i As Integer
    Dim p As Integer
    
    'Here is where you now have to go through the frequencies to
    'determine which indicators to mark for this pt.
    If Not ValidUnit(unitname) Then
        Exit Sub
    End If
    'First, set the act_freq to 1440 if num_found is 1
    'If .num_found > 0 AND .act_freq=0 then change .act_freq to 1440.
    'Leave oral care score alone.  Oral care score is in act_freq.
    For i = 1 To dicnum
        If dicary(i).num_found = 1 And dicary(i).eventid <> "21793956" And dicary(i).eventid <> "530719" Then
            dicary(i).act_freq = 1440
        End If
    Next i
    
    telemetry = CheckTelemetry
    emus = CheckEMUS
    nimcu = CheckNIMCU
    CheckUltrafiltration
    CheckRestraintSheet
    CheckSeeksRNAssist
    Check7
    Check8
    Check9
    Check10
    Check11_12
    Check14
    Check15
    Check16
    Check17
    CheckIntakeOutput
    Check19
    Check20
    Check21
    DoProcedures
    
    'Next, loop the dictionary to find the indicator.
    For i = 1 To dicnum
        If dicary(i).num_found > 0 Then
            p = FindClosestFreq(i)
            If (p > 0) Then
            If Check3Filter(i) Then
                dprint 3, 0, "passed Check3Filter"
                If Check3 Then
                    inds(3).checked = True
                    dprint 3, 1, "passed Check3"
                End If
            End If
'            If Check6Filter(i) Then
'                dprint 6, 1, "passed Check6Filter"
'                inds(6).checked = True
'                'If event21793372 And InComa Then
'                If InComa Then
'                    inds(6).checked = False
'                    dprint 6, 0, "event 21793372 and In Coma"
'                End If
'            End If
'            If Check11Filter(i) Then
'                If dicary(i).eventid = "22124174" And UCase$(dicary(i).chartkey) = "FEEDING TUBE RESIDUAL CHE" Then
'                    g = CheckBlood
'                    If g = 11 Then
'                        inds(11).checked = True
'                    ElseIf g >= 12 Then
'                        inds(12).checked = True
'                    End If
'                Else
'                    inds(dicary(p).indwinpfs).checked = True
'                End If
'                dprint 11, 1, "passed Check11Filter"
'            End If
            If (dicary(i).eventid = "200748") Then 'respiratory rate
'              Future dictionary changes: if 200748 is used for a different
'              winpfs indicator other than 17-19, then this check must change.
                inds(dicary(p).indwinpfs).checked = True 'inds(dicary(p).indwinpfs).checked Or CheckVitals(True)
                dprint dicary(p).indwinpfs, CInt(inds(dicary(p).indwinpfs).checked), "from CheckBP"
'            ElseIf (dicary(i).eventid = "92663") Then
'                g = CheckBlood
'                If g > 0 Then
'                    inds(11).checked = True
'                    dprint 11, 1, " by I&O alone, but still can be overridden by mutuality."
'                End If
'            ElseIf (dicary(p).indwinpfs = 12) Then
'                If dicary(i).eventid = "21793665" And _
'                   (UCase$(dicary(i).chartkey) = "INTERMITTENT" Or _
'                    UCase$(dicary(i).chartkey) = "CONTINUOUS" Or _
'                    UCase$(dicary(i).chartkey) = "BOLUS") Then
'                    g = CheckBlood
'                    If g >= 12 Then
'                        inds(12).checked = True
'                        dprint 12, 1, "passed I&O Intermitt,Cont,or Bolus with indicator=" & g
'                    End If
'                Else
'                    g = CheckBlood
'                    If g > 0 Then
'                        inds(g).checked = True
'                        dprint 12, 1, "passed I&O with indicator=" & g
'                    End If
'                End If
'            ElseIf (dicary(p).indwinpfs = 27) Then
'                inds(27).checked = True
'                If InComa And Not FamilyPresent Then
'                    inds(27).checked = False
'                End If
'            ElseIf (dicary(p).indwinpfs = 21) Then
'                inds(21).checked = True
'                If dicary(i).eventid = "21793264" And _
'                   (UCase$(dicary(i).chartkey) = "DEVELOPMENTAL CARE" Or _
'                    UCase$(dicary(i).chartkey) = "BASIC NEWBORN" Or _
'                    UCase$(dicary(i).chartkey) = "INFANT BONDING") Then
'                    inds(21).checked = Not PatientOrChildONLY
'                End If
'                If (InComa Or Confused) And Not FamilyPresent Then
'                    inds(21).checked = False
'                End If
            ElseIf dicary(p).eventid = "21793085" Then 'pulse rate
                If CheckBP Then
                    inds(dicary(p).indwinpfs).checked = True
                    dprint dicary(p).indwinpfs, 1, "passed pulse+CheckBP"
                End If
'            ElseIf dicary(p).eventid = "28153581" Then
'                If CheckOralCare Then
'                    inds(dicary(p).indwinpfs).checked = True
'                    dprint dicary(p).indwinpfs, 1, "passed CheckOralCare"
'                End If
'            ElseIf dicary(p).eventid = "530719" Then
'                If CheckBraden Then
'                    inds(dicary(p).indwinpfs).checked = True
'                    dprint dicary(p).indwinpfs, 1, "passed CheckBraden"
'                End If
'            ElseIf (dicary(i).chartkey = "Dressing Changed") Then
''                If CheckCentral(i) > 0 Then
''                    inds(25).checked = True
''                Else
''                    inds(24).checked = True
''                End If
'                If CheckCentral(i) > 0 Then
'                    inds(24).checked = True
'                    dprint 24, 1, "passed Dressing Changed with CheckCentral"
'                End If
            ElseIf Not (dicary(p).indwinpfs = 3 Or dicary(p).indwinpfs = 7) Then
                inds(dicary(p).indwinpfs).checked = True
                dprint dicary(p).indwinpfs, 1, "via fall through"
            End If
            
            If (dicary(p).num_found > 0 And Not inds(dicary(p).indwinpfs).checked) Then
                dprint dicary(p).indwinpfs, -1, "***FAILED TO TURN ON***"
            End If
            
            If inds(dicary(p).indwinpfs).checked Then
                If inds(dicary(p).indwinpfs).also_mark <> "" Then
                    'Only do the also mark if it originally arose from an also-mark ind.
                    ParseAlsoMark (inds(dicary(p).indwinpfs).also_mark)
                End If
            End If
            End If
        End If
    Next i
    
    If InComa Then
        inds(7).checked = False
        inds(8).checked = False
    End If
    If pt_age <= 3 Then
        inds(7).checked = False
        inds(8).checked = False
    End If
    
    '03/23/06: ADL 2 is the minimum as decided in phone mtg on 3/22/06
    If Not (inds(1).checked Or inds(2).checked Or inds(3).checked) Then
        inds(2).checked = True
        dprint 2, 1, "via AtLeastOneADL"
    End If
    
    'Now, if there are any mutually exclusive indicators marked
    'then remove the lesser ones.
    g = 0
    For i = MAX_INDICATORS To 1 Step -1
        If (grps(i) > 0) Then
            If (grps(i) <> g) Then
                g = grps(i)
                highest_is_on = inds(i).checked
            Else
                If highest_is_on Then
                    inds(i).checked = False
                Else
                    highest_is_on = inds(i).checked
                End If
            End If
        End If
    Next i
    
    'Make sure there is at least 1 ADL marked
    '3/23/06: No longer need this
    'AtLeastOneADL
        
End Sub

Private Sub Process()
    Dim on_orders As Boolean
    
    Dim togo As Long, toread As Long
    Dim blocksize As Long
    Dim buf As String
    Dim p As Integer
    Dim i As Integer
    Dim start As Integer
    Dim d As String
    Dim formloading As Boolean
    Dim s As String
    Dim rows() As String
    Dim maxrow As Long
    Dim irow As Long
    Dim isodt As String
    Dim sNowdt As String
    Dim charting As String
    Dim prevcharting As String
    Dim res As String
    Dim cond1 As Boolean
    Dim cond2 As Boolean
    Dim rangedate As Date
    Dim rangedt As String
    Dim intimedate As Date
    Dim n As Integer
    Dim dupfound As Boolean
    Dim eventid As String
    Dim eventtime As String
    
    On Error GoTo errProcess
        
    formloading = True
'    cmdProcess.Enabled = False
'    cmdOutput.Enabled = False
    
    datafile = FreeFile
    Open infn For Input As #datafile
    s = Input(LOF(datafile), datafile)
    rows() = Split(s, vbCrLf)
    maxrow = UBound(rows)
    
    outfile = FreeFile
    Open outfn For Append As #outfile
    
    pt_age = 21
    
    For irow = 0 To maxrow
        buf = rows(irow)
'       Print #inlogfile, buf
        
        If Trim$(Mid$(buf, 1, 1)) = Chr$(12) Then 'do nothing -- pg break
        ElseIf Trim$(Mid$(buf, 1, 4)) = "" Then 'Event record
' There may be more than 1 dictionary entry with the same
' event code but with a different frequency.  So use the first occurrence
' of this event to keep the running frequency.  Then at then end,
' locate the appropriate indicator for the calculated frequency.
            isodt = CerDateToISO(Trim$(Mid$(buf, START_DATETIME, LEN_DATETIME)))
            If pulltimeon Then
                If pulldateon Then
                    intimedate = DateSerial(Mid$(pulldate, 1, 4), Mid$(pulldate, 5, 2), Mid$(pulldate, 7, 2)) _
                               + TimeSerial(Mid$(pulltime, 1, 2), Mid$(pulltime, 3, 2), 0)
                Else
                    intimedate = g_util.DateOnly(nowdt) + TimeSerial(Mid$(pulltime, 1, 2), Mid$(pulltime, 3, 2), 0)
                End If
            Else
                intimedate = DateSerial(Mid$(intime, 1, 4), Mid$(intime, 5, 2), Mid$(intime, 7, 2)) _
                            + TimeSerial(Mid$(intime, 9, 2), Mid$(intime, 11, 2), 0)
            End If
            'dprint -1, 0, "intimedate=" & intimedate
            rangedate = DateAdd("n", -range, intimedate) ' range minutes before intime
            rangedt = Format(rangedate, "yyyymmddhhnn")
            'dprint -1, 0, "rangedt=" & rangedt
            eventid = Trim$(Mid(buf, START_EVENT, LEN_EVENT))
            If rangedt > isodt And eventid <> "11111" Then   'rangedt time comes from -effective=
                dprint -2, 0, buf
            Else
                If ValidUnit(unitname) Then dprint -1, 0, buf
'            sNowdt = Format(nowdt, "yyyymmddhhnn")
'            If isodt > classdt And sNowdt >= isodt Then
'                classdt = CerDateToISO(Trim$(Mid$(buf, START_DATETIME, LEN_DATETIME)))
'            End If
            If eventid = "21793956" Then 'Oral care score
                charting = " "
                p = FoundInList("21793956", charting)
                dicary(p).num_found = 1
                dicary(p).act_freq = val(Trim$(Mid(buf, START_RESULT, LEN_RESULT))) 'score
                dprint dicary(p).indwinpfs, -1, "num found = 1, freq=" & dicary(p).act_freq
'            ElseIf eventid = "530719" Then 'Braden score
'                charting = " "
'                p = FoundInList("530719", charting)
'                dicary(p).num_found = 1
'                dicary(p).act_freq = val(Trim$(Mid(buf, START_RESULT, LEN_RESULT))) 'score
'                dprint dicary(p).indwinpfs, -1, "num found = 1, freq=" & dicary(p).act_freq
            ElseIf eventid = "22124177" Then
                res = Trim$(Mid(buf, START_RESULT, LEN_RESULT))
                SeeRestraint = SeeRestraint Or InStr(1, res, "See Restraint Sheet", vbTextCompare) > 0
            ElseIf eventid = "11111" Then 'pt age
                res = Trim$(Mid(buf, START_RESULT, LEN_RESULT))
                If InStr(1, res, "years", vbTextCompare) > 0 Then
                    res = Replace(res, "years", "", , , vbTextCompare) 'remove years
                    pt_age = CInt(res)
                ElseIf InStr(1, res, "months", vbTextCompare) > 0 Then
                    res = Replace(res, "months", "", , , vbTextCompare) 'remove months, divide by 12
                    pt_age = CInt(res) \ 12
                Else
                    pt_age = 1
                End If
                dprint -2, 0, "PATIENT AGE=" & pt_age
            End If ' oral care
            'event21793372 = event21793372 Or (eventid = "21793372") 'coma
            charting = Trim$(Mid(buf, START_RESULT, LEN_RESULT))
            prevcharting = charting
            p = FoundInList(eventid, charting)
            While p > 0
                If dicary(p).check_freq Then
                    If dicary(p).num_found = 0 Then
                        dicary(p).num_found = 1
                        dicary(p).last_datetime = Trim$(Mid(buf, START_DATETIME, LEN_DATETIME))
                        If EventIsProcedureTime(eventid) Then
                            dicary(p).charttime(dicary(p).num_found) = Trim$(Mid(buf, START_RESULT + 2, 12))
                        Else
                            dicary(p).charttime(dicary(p).num_found) = Trim$(Mid(buf, START_DATETIME, LEN_DATETIME))
                        End If
                        dicary(p).act_freq = 0
                        'If this is the only occurrence, then it will be changed
                        'to 1440 at summary time.
                        dprint dicary(p).indwinpfs, -1, "num found = 1, freq=" & dicary(p).act_freq & " Item=" & p
                    Else
                        dupfound = False
                        If (p = 4) Then 'charting = "Intake / Output / Shift Care" And eventid = "92663")
                            If dicary(p).num_found <= MAX_UNIQUE_CHART_TIMES Then
                            For n = 1 To dicary(p).num_found
                                If EventIsProcedureTime(eventid) Then
                                    eventtime = Trim$(Mid(buf, START_RESULT + 2, 12))
                                Else
                                    eventtime = Trim$(Mid(buf, START_DATETIME, LEN_DATETIME))
                                End If
                                If dicary(p).charttime(n) = eventtime Then
                                    dupfound = True
                                    dprint dicary(p).indwinpfs, -1, "Duplicate Rejected" & dicary(p).act_freq & " Item=" & p
                                End If
                            Next n
                            End If
                        End If
                        If Not dupfound Then
                            'CalcFrequency sets act_freq
                            dicary(p).num_found = dicary(p).num_found + 1
                            If dicary(p).num_found <= MAX_UNIQUE_CHART_TIMES Then
                                If EventIsProcedureTime(eventid) Then
                                    dicary(p).charttime(dicary(p).num_found) = Trim$(Mid(buf, START_RESULT + 2, 12))
                                Else
                                    dicary(p).charttime(dicary(p).num_found) = Trim$(Mid(buf, START_DATETIME, LEN_DATETIME))
                                End If
                            End If
                            CalcFrequency p
                            dprint dicary(p).indwinpfs, -1, "num found =" & dicary(p).num_found & ", freq=" & dicary(p).act_freq & " Item=" & p
                        End If
                    End If
                Else
                    inds(dicary(p).indwinpfs).checked = True
                    dprint dicary(p).indwinpfs, 1, ""
                    If dicary(p).also_mark <> "" Then
                        ParseAlsoMark (dicary(p).also_mark)
                        dprint dicary(p).indwinpfs, 1, " Also-mark=" & dicary(p).also_mark
                    End If
                End If
                'Get the also-mark from the dictionary since this is its source.
                If dicary(p).also_mark <> "" Then
                    inds(dicary(p).indwinpfs).also_mark = dicary(p).also_mark
                End If
                
                If prevcharting = charting Then
                    p = 0
                Else
                    p = FoundInList(eventid, charting)
                End If
            Wend
            End If
        Else 'Header record
            If (Not formloading) Then 'end of the previous pt.  Output now.
                DoPatientSummary
                classdate = Mid$(classdt, 1, 8)
                classtime = Mid$(classdt, 9, 4)
                AssembleOutput
            End If
'            dprint -1, 0, buf
            formloading = False
            InitIndicators
            ResetDictionaryFields
            ParsePatientInfo (buf)
            If ValidUnit(unitname) Then dprint -1, 0, buf
            InitGroups
            classdt = ""
            classdate = ""  'Mid$(buf, 21, 4) & Mid$(buf, 15, 2) & Mid$(buf, 18, 2)
            classtime = ""  'Mid$(buf, 26, 4)
            intime = saveintime
            'event21793372 = False
            telemetry = False
            emus = False
            nimcu = False
            pt_age = 21
            SeeRestraint = False
            SeeksRNAssist = False
        End If
        

        DoEvents
    Next

    DoPatientSummary
    classdate = Mid$(classdt, 1, 8)
    classtime = Mid$(classdt, 9, 4)
    AssembleOutput

    Close #datafile
    Close #outfile
    ' dont delete infn Kill infn in this program because we need it for the next program
    
    Exit Sub
errProcess:
        
End Sub




Private Function FoundInList(id As String, ByRef s As String) As Integer
    Dim i As Integer
    Dim p As Integer

    FoundInList = 0
    For i = 1 To dicnum
        If (id = dicary(i).eventid) Then
            If (Trim$(dicary(i).chartkey) <> "") Then
                p = InStr(1, s, dicary(i).chartkey, vbTextCompare)
                If (p > 0) Then
                    FoundInList = i
                    s = Replace(s, dicary(i).chartkey, "", , , vbTextCompare)
                    dprint 0, 0, "Dictionary item found=" & i & " id=" & id & " charting=" & s & " mapping to=" & dicary(i).indwinpfs
                    Exit For
                End If
            Else 'don't care about matching chartkey
                FoundInList = i
                dprint 0, 0, "Dictionary item found=" & i & " id=" & id & " charting=%don't care%" & " mapping to=" & dicary(i).indwinpfs
                Exit For
            End If
        End If
    Next i
End Function

Private Sub InitIndicators()
    Dim i As Integer
    
    For i = 1 To MAX_INDICATORS
        inds(i).checked = False
        inds(i).also_mark = ""
    Next i
    
    totnumprocs = 0
    Erase procs
    
End Sub
Private Sub InitGroups()
    Dim i As Integer
    
    For i = 1 To MAX_INDICATORS
        grps(i) = 0
    Next i
    
    For i = 1 To 3
        grps(i) = 1
    Next i
    For i = 5 To 6
        grps(i) = 2
    Next i
    For i = 9 To 10
        grps(i) = 3
    Next i
    For i = 11 To 12
        grps(i) = 4
    Next i
    For i = 14 To 17
        grps(i) = 5
    Next i
    For i = 19 To 20
        grps(i) = 6
    Next i

End Sub

Private Function ItemExists(col As Collection, key As String) As Boolean
    Dim dummy As Variant
    On Error Resume Next
    dummy = col.Item(key)
    ItemExists = (Err <> 5)
End Function

Private Sub AtLeastOneADL()

    If Not (inds(1).checked Or inds(2).checked Or inds(3).checked Or inds(4).checked) Then
        inds(1).checked = True
        dprint 1, 1, "via AtLeastOneADL"
    End If
End Sub

Private Sub ParseAlsoMark(s As String)
    Dim comma_pos As Integer
    Dim ind As Integer
    
    'for example:   29,31  is the also_mark string for RESTRAINTS
    
    Do
        comma_pos = InStr(s, ",")
        If (comma_pos = 0) Then
            ind = val(s)
        Else
            ind = val(Mid$(s, 1, comma_pos - 1))
            s = Mid$(s, comma_pos + 1, Len(s) - comma_pos)
        End If
        
        inds(ind).checked = True
        dprint ind, 1, "via Also-mark"
        
    Loop Until comma_pos = 0
End Sub

Private Sub ParsePatientInfo(s As String)
    Dim fullname As String
    Dim commapos As Integer
    
    acctnum = Trim$(Mid$(s, START_ACCT_NUM, LEN_ACCT_NUM))
    
    fullname = Trim$(Mid$(s, START_PTNAME, LEN_PTNAME))
    commapos = InStr(fullname, ",")
    If commapos = 0 Then
        lastname = fullname
        firstname = ""
    Else
        lastname = Mid$(fullname, 1, commapos - 1)
        firstname = Mid$(fullname, commapos + 1, Len(fullname) - commapos)
    End If
    
    unitname = Trim$(Mid$(s, START_UNIT, LEN_UNIT))
    roomname = Trim$(Mid$(s, START_RM, LEN_RM))
'    If UCase$(unitname) = "A3E" Then
'    Select Case UCase$(roomname)
'        Case "A301", "A302", "A303", "A304", "A305", "A306", "A307", "A308", "A309", "A310"
'            unitname = "FBC"
'    End Select
'    End If
    bedname = Trim$(Mid$(s, START_BED, LEN_BED))
    
    
End Sub


Private Sub AssembleOutput()
    Dim outstr As String
    Dim i As Integer
    Dim din As Date
    Dim tin As Date
    Dim ok As Boolean
    
'    ok = False
'    Select Case UCase$(unitname)
'        Case "A4W", "A6M", "A9M"
'            ok = True
'    End Select
    ok = ValidUnit(unitname)
    
    If Not ok Then
        Exit Sub
    End If
    
    outstr = Space$(9) & unitname
    outstr = outstr & Space$(26 - Len(outstr))
    outstr = outstr & unitname '27
    If telemetry And (UCase$(unitname) = "A9M" Or UCase$(unitname) = "M9") Then 'm9
        outstr = outstr & Space$(43 - Len(outstr))
        outstr = outstr & "Telemetry"
    ElseIf emus And (UCase$(unitname) = "A4E" Or UCase$(unitname) = "E4") Then  'e4
        outstr = outstr & Space$(43 - Len(outstr))
        outstr = outstr & "EMUS"
    ElseIf nimcu Then
        Select Case UCase$(roomname)
            Case "A405", "A406", "A407", "A408", "A409", "A410"
                outstr = outstr & Space$(43 - Len(outstr))
                outstr = outstr & "NIMCU"
        End Select
    End If
    If UCase$(unitname) = "H6M" Then
        If inds(19).checked Then
            inds(19).checked = False
            inds(18).checked = True
        End If
        If inds(22).checked Then
            inds(22).checked = False
            inds(21).checked = True
        End If
    End If
    
    'Dont allow 8 or 9 to be marked for some unitsNICU, STIC, MSIC, and CVIC
    Select Case UCase$(unitname)
        Case "NICU", "STIC", "MSIC", "CVIC"
'            inds(8).checked = False  should only be for safety mgt 9,10
            inds(9).checked = False
            inds(10).checked = False
    End Select
    
    outstr = outstr & Space$(60 - Len(outstr))
    outstr = outstr & Mid$(intime, 1, 8) & Space$(1) '61
'    outstr = outstr & classdate & Space$(1) '61
    outstr = outstr & acctnum & Space$(20 - Len(acctnum) + 1) '70
    outstr = outstr & lastname & Space$(32 - Len(lastname) + 1) '91
    outstr = outstr & firstname & Space$(32 - Len(firstname) + 1) '124
    outstr = outstr & Space$(32 + 1) '157
    outstr = outstr & roomname & Space$(8 - Len(roomname) + 1) '190
    outstr = outstr & bedname & Space$(4 - Len(bedname) + 1) '199
    outstr = outstr & intime & Space$(1) '204
'    outstr = outstr & classdate & classtime & Space$(1)
    outstr = outstr & Space$(78 + 1) '217 + 79 = 296
'    If (Not IsNull(intime) And Not IsNull(classdate) And Not IsNull(classtime)) Then
'        If (Mid$(intime, 1, 8) > classdate) Then
'            tin = TimeSerial(val(Mid$(intime, 9, 2)), val(Mid$(intime, 11, 2)), 0)
'            intime = classdate
'            din = DateSerial(val(Mid$(intime, 1, 4)), val(Mid$(intime, 5, 2)), val(Mid$(intime, 7, 2)))
'            intime = Format(din + tin, "yyyymmddhhnn")
'        End If
'        If intime > (classdate & classtime) Then
'            din = DateSerial(val(Mid$(intime, 1, 4)), val(Mid$(intime, 5, 2)), val(Mid$(intime, 7, 2)))
'            tin = TimeSerial(val(Mid$(intime, 9, 2)), val(Mid$(intime, 11, 2)), 0)
'            din = DateAdd("d", -1, din)
'            intime = Format(din + tin, "yyyymmddhhnn")
'        End If
'    End If
    If Not classinon Then '296 + 13 = 309
        outstr = outstr & intime & Space$(12 - Len(intime) + 1)
    Else
        outstr = outstr & classindt & Space$(12 - Len(classindt) + 1)
    End If
    If Not classouton Then
        outstr = outstr & Space$(70) 'go directly to 379 indicators
    Else
        outstr = outstr & Space$(39) '309 + 39 = 348
        outstr = outstr & classoutdt & Space$(12 - Len(classoutdt)) '360
        outstr = outstr & Space$(19)
    End If
    For i = 1 To MAX_INDICATORS
        If (inds(i).checked) Then
            outstr = outstr & "Y"
        Else
            outstr = outstr & "N"
        End If
    Next i
    
    Print #outfile, outstr
    Print #outlogfile, outstr
End Sub


Private Sub CalcFrequency(p As Integer)
    Dim n As Integer
    Dim r As Single
    
    r = range / RANGE_DENOMINATOR
' Use 8 hours as basis
    Select Case dicary(p).indwinpfs
        Case 14 To 22 'indicators 14-22 are to be counted instead of freq.
            If dicary(p).num_found >= 4 * r And dicary(p).num_found <= 12 * r Then
                dicary(p).act_freq = 240
            ElseIf dicary(p).num_found > 12 * r And dicary(p).num_found <= 24 * r Then
                dicary(p).act_freq = 60
            ElseIf dicary(p).num_found > 24 * r Then
                dicary(p).act_freq = 30
            Else
                dicary(p).act_freq = 480
            End If
        Case Else   ' Other values.
            dicary(p).act_freq = range / dicary(p).num_found 'denom will never be zero
    End Select

End Sub

Private Function CerDateToISO(d As String) As String
    Dim century As String
    ' Yields a string of the format yyyymmddhhnn
    
    If val(Mid$(d, 7, 2)) <= 50 Then
        century = "20"
    Else
        century = "19"
    End If
    CerDateToISO = century & Mid$(d, 7, 2) & Mid$(d, 1, 2) & Mid$(d, 4, 2) _
     & Mid$(d, 10, 2) & Mid$(d, 13, 2)

End Function
Private Function NowDateToISO(d As String) As String
    Dim century As String
    Dim yslash, dslash, mcolon, m, day, h As Integer
    Dim ys, ms, ds, hs, ns As String
    ' Yields a string of the format yyyymmddhhnn
    yslash = InStrRev(d, "/")
    dslash = InStr(d, "/")
    mcolon = InStr(d, ":")
    
    ys = Mid$(d, yslash + 1, 4)
        
    ms = Mid$(d, 1, dslash - 1)
    m = val(ms)
    If m < 10 Then ms = "0" & ms
    
    ds = Mid$(d, dslash + 1, yslash - dslash - 1)
    day = val(ds)
    If day < 10 Then ds = "0" & ds
    
    hs = Mid$(d, mcolon - 2, 2)
    h = val(hs)
    If InStr(d, "PM") > 0 Then
        If h < 12 Then h = h + 12
    ElseIf InStr(d, "AM") > 0 Then
        If h = 12 Then h = 0
    End If
    hs = Trim$(str(h))
    If h = 0 Then
        hs = "00"
    ElseIf h < 10 Then
        hs = "0" & hs
    End If
    
    ns = Mid$(d, mcolon + 1, 2)
    
    NowDateToISO = ys & ms & ds & hs & ns

End Function

Private Sub ResetDictionaryFields()
    Dim i As Integer
    
    For i = 1 To dicnum
        dicary(i).num_found = 0
        dicary(i).act_freq = 0
        dicary(i).last_datetime = ""
    Next i

End Sub

Private Function FindClosestFreq(p As Integer) As Integer
    Dim i As Integer
    Dim keep As Integer
    
    keep = 0
    For i = 1 To dicnum
        If dicary(i).eventid = dicary(p).eventid And dicary(i).chartkey = dicary(p).chartkey Then
            If dicary(p).act_freq <= dicary(i).freq_basis Then
                keep = i
            End If
        End If
    Next i
    
    FindClosestFreq = keep
    
End Function

Private Function CheckBP() As Boolean
    Dim e193099 As Integer  'index of 193099 in dictionary
    Dim e193101 As Integer  '  "      193100    "
    Dim charting As String
    
    suppressDbugLog = True
    
    CheckBP = False
    charting = " "
    e193099 = FoundInList("193099", charting)
    charting = " "
    e193101 = FoundInList("193101", charting)
    charting = " "
'    e200748 = FoundInList("200748", charting)
'    If isRespiratory Then
'        If dicary(e193099).num_found > 0 And dicary(e193101).num_found > 0 And _
'            dicary(e200748).num_found > 0 Then
'            CheckVitals = True
'        End If
'    ElseIf dicary(e193099).act_freq = dicary(e193101).act_freq Then
'            CheckVitals = True
'    End If
    If dicary(e193099).num_found > 0 And dicary(e193101).num_found > 0 Then
        CheckBP = True
    End If
    suppressDbugLog = False
    
End Function

Private Function Check3Filter(i As Integer) As Boolean

    Check3Filter = _
       dicary(i).eventid = "21793395" And UCase$(dicary(i).chartkey) = "TOTAL" Or _
       pt_age < 3

End Function

Private Function Check3() As Boolean
    Dim bathtotal As Integer
    Dim eatingtotal As Integer
    Dim interm, cont As Integer
    Dim bolus As Integer
    Dim ped1, ped2, ped3 As Integer
    Dim charting As String
    suppressDbugLog = True
    
' 3 *22143402                          Breast
' 3 *22143402                          Bottle
' 3 *22143402                          NPO After 12

    
    Check3 = False
    charting = "total"
    bathtotal = FoundInList("21793395", charting) 'requires EATING total
    charting = "total"
    eatingtotal = FoundInList("21793401", charting) 'requires bath total
    charting = "intermittent"
    interm = FoundInList("21793665", charting) 'requires bath total
    charting = "continuous"
    cont = FoundInList("21793665", charting) 'doesnt require anything else
    charting = "bolus"
    bolus = FoundInList("21793665", charting) 'requires bath total
    charting = "breast"
    ped1 = FoundInList("22143402", charting)
    charting = "bottle"
    ped2 = FoundInList("22143402", charting)
    charting = "npo after 12"
    ped3 = FoundInList("22143402", charting)
    If dicary(bathtotal).num_found > 0 And _
       (dicary(eatingtotal).num_found > 0 Or dicary(interm).num_found > 0 Or dicary(bolus).num_found > 0 Or _
        dicary(ped1).num_found > 0 Or dicary(ped2).num_found > 0 Or dicary(ped3).num_found > 0) Then
        Check3 = True
        suppressDbugLog = False
        dprint 3, 1, "ADL Complete due to bath total + other"
    End If
    If dicary(cont).num_found > 0 Then
        Check3 = True
        suppressDbugLog = False
        dprint 3, 1, "Continuous fluids"
    End If
    If pt_age < 3 Then
        Check3 = True
        suppressDbugLog = False
        dprint 3, 1, "ADL Complete due to age=" & str(pt_age)
    End If
    suppressDbugLog = False

End Function

Private Sub Check7()
    Dim charting As String
    Dim b1, b2, b3, b4, b5, b6 As Integer
    
    If InComa Then Exit Sub
    If pt_age <= 3 Then Exit Sub
    
    charting = "bilateral"
    b1 = FoundInList("256585", charting)
    charting = "hearing aid"
    b2 = FoundInList("256585", charting)
    charting = "diminished"
    b3 = FoundInList("256585", charting)
    charting = "deaf"
    b4 = FoundInList("256585", charting)
    
    If dicary(b1).num_found > 0 And dicary(b2).num_found = 0 And (dicary(b3).num_found > 0 Or dicary(b4).num_found > 0) Then
        inds(7).checked = True
        dprint 7, 0, "256585 hearing"
    End If
    
    charting = "diminished"
    b1 = FoundInList("128357936", charting)
    charting = "hearing aid"
    b2 = FoundInList("128357936", charting)
    
    If dicary(b1).num_found > 0 And dicary(b2).num_found = 0 Then
        inds(7).checked = True
        dprint 7, 0, "128357936 hearing"
    End If
    
    
    charting = "bilateral"
    b1 = FoundInList("289254", charting)
    charting = "Loss of Vision"
    b2 = FoundInList("289254", charting)
    charting = "Blind"
    b3 = FoundInList("289254", charting)
    charting = "Prosthetic"
    b4 = FoundInList("289254", charting)
    charting = "Contacts"
    b5 = FoundInList("289254", charting)
    charting = "Glasses"
    b6 = FoundInList("289254", charting)
    
    If dicary(b1).num_found > 0 And (dicary(b2).num_found > 0 Or dicary(b3).num_found > 0 Or dicary(b4).num_found > 0) _
       And Not (dicary(b5).num_found > 0 Or dicary(b6).num_found > 0) Then
        inds(7).checked = True
        dprint 7, 0, "256585 vision"
    End If
    
    charting = "left"
    b1 = FoundInList("38896463", charting)
    charting = "right"
    b2 = FoundInList("38896463", charting)
    If dicary(b1).num_found > 0 And dicary(b2).num_found > 0 Then
        inds(7).checked = True
        dprint 7, 0, "38896463 left+right vision"
    End If

    charting = "TRACHEOSTOMY"
    b1 = FoundInList("21793446", charting)
    charting = "VENTILATOR"
    b2 = FoundInList("193845", charting)
    If dicary(b1).num_found > 0 Or dicary(b2).num_found > 0 Then
        inds(7).checked = True
        dprint 7, 0, "21793446 trach or 193845 vent"
    End If
                
End Sub


Private Function InComa() As Boolean
    Dim coma As Integer
    Dim semicoma As Integer
    Dim charting As String
    
    suppressDbugLog = True
    charting = "comatose"
    coma = FoundInList("21793372", charting)
    charting = "semicomatose"
    semicoma = FoundInList("21793372", charting)
    InComa = dicary(coma).num_found > 0 Or dicary(semicoma).num_found > 0
    suppressDbugLog = False
    
End Function

'Private Function Check7Filter(i As Integer) As Boolean
'    Dim charting As String
'    Dim unresp As Integer
'
'    suppressDbugLog = True
'    Check7Filter = False
'    If dicary(i).eventid = "21793689" And _
'        (UCase$(dicary(i).chartkey) = "CONFUSED" Or _
'         UCase$(dicary(i).chartkey) = "DISORIENTED" Or _
'         UCase$(dicary(i).chartkey) = "COMBATIVE") Then
'        charting = "unresponsive"
'        unresp = FoundInList("21793689", charting)
'        Check7Filter = (dicary(unresp).num_found = 0)
'    End If
'    suppressDbugLog = False
'End Function

'Private Function Check11Filter(i As Integer) As Boolean
'
'    Check11Filter = _
'        dicary(i).eventid = "22124174" And UCase$(dicary(i).chartkey) = "FEEDING TUBE RESIDUAL CHE" And dicary(i).num_found > 0 Or _
'        dicary(i).eventid = "282664" And dicary(i).num_found > 0 And _
'            (UCase$(dicary(i).chartkey) = "BED" Or _
'             UCase$(dicary(i).chartkey) = "SLING" Or _
'             UCase$(dicary(i).chartkey) = "UPRIGHT")
'
'End Function

' Commented out per 11/30/05 review - not a valid criterion
'
'Private Function Check11() As Boolean
'    Dim ivaccess As Integer
'    Dim charting As String
'
'    charting = "yes"
'    ivaccess = FoundInList("28179262", charting)
'    Check11 = dicary(ivaccess).num_found > 0
'
'End Function

Private Function GetIO() As Integer
    Dim io As Integer
    Dim charting As String
' 0 *73035390          1440            Intake / Output / Shift Care

    suppressDbugLog = True
    GetIO = 0
    charting = "INTAKE / OUTPUT / SHIFT CARE"
    io = FoundInList("73035390", charting)
    If dicary(io).num_found > 0 Then
        If dicary(io).act_freq <= 30 Then
            GetIO = 17
        ElseIf dicary(io).act_freq <= 60 Then
            GetIO = 16
        ElseIf dicary(io).act_freq <= 120 Then
            GetIO = 15
        ElseIf dicary(io).act_freq <= 240 Then
            GetIO = 14
        End If
    End If
    suppressDbugLog = False
    
End Function

'Private Function CheckOralCare() As Boolean
'    Dim oralscore As Integer
'    Dim charting As String
'
'    suppressDbugLog = True
'    charting = " "
'    oralscore = FoundInList("21793956", charting)
'    CheckOralCare = dicary(oralscore).act_freq > 6
'    suppressDbugLog = False
'End Function

'Private Function CheckCentral(i As Integer) As Integer
'    Dim central As Integer
'    Dim charting As String
'
'    suppressDbugLog = True
'    charting = "central"
'    If (dicary(i).eventid = "28161419") Then
'        central = FoundInList("28161401", charting)
'    ElseIf (dicary(i).eventid = "28185338") Then
'        central = FoundInList("28185323", charting)
'    ElseIf (dicary(i).eventid = "28185368") Then
'        central = FoundInList("28185356", charting)
'    ElseIf (dicary(i).eventid = "28185398") Then
'        central = FoundInList("28185389", charting)
'    ElseIf (dicary(i).eventid = "28308753") Then
'        central = FoundInList("28308759", charting)
'    ElseIf (dicary(i).eventid = "28179318") Then
'        central = FoundInList("28179293", charting)
'    End If
'
'    CheckCentral = dicary(central).num_found
'    suppressDbugLog = False
'
'End Function

'Private Function CheckBraden() As Boolean
'    Dim braden As Integer
'    Dim charting As String
'
'    suppressDbugLog = True
'    charting = " "
'    braden = FoundInList("530719", charting)
'    CheckBraden = dicary(braden).act_freq <= 18
'    suppressDbugLog = False
'End Function

'Public Function Rename_File(ByVal Location As String, ByVal name As String, ByVal changeto As String) As Boolean
'
'    On Error GoTo ErrorHandler
'
'    If FileExists(Location & name) Then
'        If FileExists(Location & changeto) Then
'            Kill Location & changeto
'            Name Location & name As Location & changeto
'            Rename_File = True
'            Exit Function
'        Else
'            Rename_File = FileExists(Location & changeto)
'            Exit Function
'        End If
'    End If
'ErrorHandler:
' Err.Clear
'End Function

Private Sub MakeLogFiles()
    Const HKEY_LOCAL_MACHINE = &H80000002
    Dim dt As Variant
    
    If Not g_util.GetKeyValue(HKEY_LOCAL_MACHINE, "SOFTWARE\Quadramed\WinPFS", "Directory", winpfspath) Then
        winpfspath = App.Path
    End If

    If g_util.DirExists(winpfspath) Then
        winlogpath = winpfspath & "\log"
        winloadpath = winpfspath & "\load_me"
        If Not g_util.DirExists(winpfspath) Then
            MkDir$ (winlogpath)
        End If
    Else
        winlogpath = App.Path
        winloadpath = winlogpath
    End If
    
    dt = nowdt
    inlogname = winlogpath & "\TranspIn_" & Format$(dt, "mmdd") & ".log"
    outlogname = winlogpath & "\TranspOut_" & Format$(dt, "mmdd") & ".log"
    dbugname = winlogpath & "\DbugTransp_" & Format$(dt, "mmdd") & ".log"
    
'    inlogfile = FreeFile
'    Open inlogname For Append As #inlogfile
'    Print #inlogfile, "**** WinPFS Transparent Classification Input    Time=" & nowdt & " ****"
    
    outlogfile = FreeFile
    Open outlogname For Append As #outlogfile
    Print #outlogfile, "**** WinPFS Transparent Classification Output    Time=" & nowdt & " ****"
    
    If dbugon Then
        dbugfile = FreeFile
        Open dbugname For Append As #dbugfile
        Print #dbugfile, "**TRANSPARENT TRANSLATION DEBUGGING MODE**"
    End If
    
End Sub

'Private Sub SetIntimeFromCommand()
'    Dim i As Integer
'    Dim h As String
'    Dim t As Date
'
'    ' command parameter needs to be in form hh:mm ONLY
'    intime = ""
'    h = Command$
'    If IsDate(h) Then
'        t = g_util.DateTimeOrNull(h)
'        If Not IsNull(t) Then
'            h = Format$(t, "hhNN")
'            i = year(g_util.DateOnly(FileDateTime(inlogname)))
'            intime = CStr(i) & Mid$(inlogname, Len(inlogname) - Len("mmdd.log") + 1, Len("mmdd")) & h
'        End If
'    End If
'
'End Sub

Private Sub CloseLogFiles()

'    Close inlogfile
    Close outlogfile
    If dbugon Then
        Close dbugfile
    End If

    'inlogfile's dt = mid$(inlogname,len(inlogname)-len("mmdd.txt")+1,len("mmddhh.txt"))
'    FileCopy outfn, winlogpath & "\TranspOut_" & Mid$(inlogname, Len(inlogname) - Len("mmddhh.txt") + 1, Len("mmddhh.txt"))

End Sub

Private Sub DeleteOldLogs()
    Dim temp As String
    Dim i As Integer
    Dim dt As Variant

    ' Delete old log files
    ' (allow the interface to be down for up to 5 days)
    dt = DateAdd("d", -TRANSP_INLOG_FILE_LIFE, nowdt)
    For i = 1 To 5
        temp = winlogpath & "\TranspIn_" & Format$(dt, "mmdd") & ".log"
        If g_util.FileExists(temp) Then
            Kill temp
        End If
        dt = DateAdd("d", -1, dt)
    Next i

    dt = DateAdd("d", -TRANSP_OUTLOG_FILE_LIFE, nowdt)
    For i = 1 To 5
        temp = winlogpath & "\TranspOut_" & Format$(dt, "mmdd") & ".log"
        If g_util.FileExists(temp) Then
            Kill temp
        End If
        dt = DateAdd("d", -1, dt)
    Next i

End Sub


Private Sub dprint(ind As Integer, status As Integer, s As String)
    Dim sstat As String

    If Not dbugon Or suppressDbugLog Then
        Exit Sub
    End If
    
    If ind = -1 Then
        Print #dbugfile, s
    ElseIf ind = -2 Then
        If ValidUnit(unitname) Then
        Print #dbugfile, "The following event was Out of Range and was rejected."
        Print #dbugfile, s
        End If
    ElseIf ind = 0 Then
        If ValidUnit(unitname) Then Print #dbugfile, "    " & s
    Else
        If ValidUnit(unitname) Then
        If (status = 1) Or (status = -1) Then
            sstat = "-TURNED ON-"
        Else
            sstat = "   "
        End If
        Print #dbugfile, "    Indicator " & ind & sstat & s
        End If
    End If
    
End Sub

Private Function CheckTelemetry() As Boolean
    Dim tel1 As Integer
    Dim tel2 As Integer
    Dim charting As String
'20*272342            1440            Telemetry
'20*282736            1440            Telemetry

    suppressDbugLog = True
    CheckTelemetry = False
    charting = "Telemetry"
    tel1 = FoundInList("272342", charting)
    tel2 = FoundInList("282736", charting)
    If dicary(tel1).num_found > 0 Or dicary(tel2).num_found > 0 Then
        CheckTelemetry = True
    End If
    suppressDbugLog = False
End Function

Private Function CheckEMUS() As Boolean
    Dim e As Integer
    Dim charting As String
    
    suppressDbugLog = True
    CheckEMUS = False
    charting = "EMUS Protocol"
    e = FoundInList("22124177", charting)
    If dicary(e).num_found > 0 Then
        CheckEMUS = True
    End If
    suppressDbugLog = False
End Function
Private Function CheckNIMCU() As Boolean
    Dim e As Integer
    Dim charting As String
    
    suppressDbugLog = True
    CheckNIMCU = False
    charting = "NIMCU"
    e = FoundInList("22124177", charting)
    If dicary(e).num_found > 0 Then
        CheckNIMCU = True
    End If
    suppressDbugLog = False
End Function


Private Function ValidUnit(u As String) As Boolean
    Dim i As Integer
    
    For i = 1 To unitnum
        If UCase$(u) = unitary(i) Then
            ValidUnit = True
            Exit For
        End If
    Next i

End Function

Private Function FamilyPresent() As Boolean
    Dim c As Integer
    Dim fm As Integer
    Dim f As Integer
    Dim p As Integer
    Dim so As Integer
    Dim s As Integer
    Dim o As Integer
    Dim charting As String
    
    suppressDbugLog = True
    FamilyPresent = False
    charting = "Child"
    c = FoundInList("21793267", charting)
    charting = "Family Member"
    fm = FoundInList("21793267", charting)
    charting = "Friend"
    f = FoundInList("21793267", charting)
    charting = "Parent"
    p = FoundInList("21793267", charting)
    charting = "Significant other"
    so = FoundInList("21793267", charting)
    charting = "Spouse"
    s = FoundInList("21793267", charting)
    charting = "Other"
    o = FoundInList("21793267", charting)
    
    If dicary(c).num_found > 0 Or dicary(fm).num_found > 0 Or dicary(f).num_found > 0 Or _
        dicary(p).num_found > 0 Or dicary(so).num_found > 0 Or dicary(s).num_found > 0 Or _
        dicary(o).num_found > 0 Then
        FamilyPresent = True
    End If
    suppressDbugLog = False

End Function

Private Function PatientOrChildONLY() As Boolean
    Dim c As Integer
    Dim pt As Integer
    Dim fm As Integer
    Dim f As Integer
    Dim p As Integer
    Dim so As Integer
    Dim s As Integer
    Dim o As Integer
    Dim charting As String
    
    suppressDbugLog = True
    PatientOrChildONLY = False
    charting = "Child"
    c = FoundInList("21793267", charting)
    charting = "Patient"
    pt = FoundInList("21793267", charting)
    charting = "Family Member"
    fm = FoundInList("21793267", charting)
    charting = "Friend"
    f = FoundInList("21793267", charting)
    charting = "Parent"
    p = FoundInList("21793267", charting)
    charting = "Significant other"
    so = FoundInList("21793267", charting)
    charting = "Spouse"
    s = FoundInList("21793267", charting)
    charting = "Other"
    o = FoundInList("21793267", charting)
    
    If (dicary(c).num_found > 0 Or dicary(pt).num_found > 0) And _
        Not (dicary(fm).num_found > 0 Or dicary(f).num_found > 0 Or _
        dicary(p).num_found > 0 Or dicary(so).num_found > 0 Or dicary(s).num_found > 0 Or _
        dicary(o).num_found > 0) Then
        PatientOrChildONLY = True
    End If
    suppressDbugLog = False

End Function

Private Function Confused() As Boolean
    Dim c As Integer
    Dim d As Integer
    Dim charting As String
    
    suppressDbugLog = True
    Confused = False
    c = FoundInList("21793689", "Confused")
    d = FoundInList("21793689", "Disoriented")
    If dicary(c).num_found > 0 Or dicary(d).num_found > 0 Then
        Confused = True
    End If
    suppressDbugLog = False

End Function
Private Sub CheckUltrafiltration()
    Dim i As Integer
    Dim charting As String
    
    suppressDbugLog = True
    charting = "ultrafi"
    i = FoundInList("22124177", charting)
    
    If dicary(i).num_found > 0 Then
        inds(11).checked = True
        inds(18).checked = True
        inds(21).checked = True
        inds(41).checked = True
    End If
    suppressDbugLog = False
            
End Sub

Private Sub CheckRestraintSheet()
    Dim i As Integer
    Dim charting As String
    
    suppressDbugLog = True
    If SeeRestraint Then
        inds(23).checked = True
        inds(30).checked = True
        inds(31).checked = True
        
 '       8 *22124229                          Safety mgmt
        charting = "Safety mgmt"
        i = FoundInList("22124229", charting)

        If dicary(i).num_found > 0 Then
            inds(8).checked = True
        End If
    End If
    suppressDbugLog = False

End Sub

Private Sub CheckSeeksRNAssist()
    Dim i, i2, i3, i4 As Integer
    Dim charting As String
    
    suppressDbugLog = True
    charting = "Seeks RN Assist"
    i = FoundInList("72957277", charting)
    
'30*21793189                          Anxious
'30*21793189                          Confusion
'30*21793189                          Crying
    If dicary(i).num_found > 0 Then
        charting = "Anxious"
        i2 = FoundInList("21793189", charting)
        charting = "Confusion"
        i3 = FoundInList("21793189", charting)
        charting = "Crying"
        i4 = FoundInList("21793189", charting)
        
        If dicary(i2).num_found > 0 Or dicary(i3).num_found > 0 Or dicary(i4).num_found > 0 Then
            inds(30).checked = True
        End If
    End If
    suppressDbugLog = False


End Sub

Private Sub Check8()
    Dim i1, i2, i3, i4, i5, i6, i7 As Integer
    Dim charting As String
    
' 8 *22124177                35        Alcohol Protocol
' 8 *22142590                          Autism
' 8 *21793689          1440            Confused
' 8 *21793689          1440            Disoriented
' 8 *21793689          1440            Combative
    suppressDbugLog = True
    If pt_age <= 3 Then Exit Sub
    
    charting = "alcohol protocol"
    i1 = FoundInList("22124177", charting)
    
    If dicary(i1).num_found > 0 Then
        inds(14).checked = True
        inds(35).checked = True
    End If
        
    If Not InComa Then
        charting = "confused"
        i3 = FoundInList("21793689", charting)
        charting = "disoriented"
        i4 = FoundInList("21793689", charting)
        charting = "combative"
        i5 = FoundInList("21793689", charting)
        
        If dicary(i1).num_found > 0 Or dicary(i3).num_found > 0 Or dicary(i4).num_found > 0 Or dicary(i5).num_found > 0 Then
            inds(8).checked = True
        End If
        
        charting = "Cognitive/Sensory Impairment"
        i3 = FoundInList("251577", charting)
        If dicary(i3).num_found > 0 Then inds(8).checked = True
        
        If inds(8).checked Then Exit Sub
        
        If pt_age >= 3 And Not InComa Then
            charting = "orient x 3"
            i1 = FoundInList("289219", charting)
            i2 = FoundInList("128054493", charting)
            i3 = FoundInList("21793372", charting)
            i3 = FoundInList("128365968", charting)
            If dicary(i1).num_found = 0 And dicary(i2).num_found = 0 And dicary(i3).num_found = 0 And dicary(i3).num_found = 0 Then
                charting = "seizure like"
                i1 = FoundInList("289219", charting)
                charting = "hallucination"
                i2 = FoundInList("289219", charting)
                charting = "receptive aphasia"
                i3 = FoundInList("289219", charting)
                charting = "sedated"
                i4 = FoundInList("21793372", charting)
                charting = "tracheostomy"
                i5 = FoundInList("128054493", charting)
                charting = "confusion/disorientation"
                i6 = FoundInList("128365968", charting)
                charting = "confusion/disorientation"
                i7 = FoundInList("289219", charting)
                If dicary(i1).num_found > 0 Or dicary(i2).num_found Or dicary(i3).num_found > 0 Or dicary(i4).num_found > 0 Or dicary(i5).num_found > 0 Or dicary(i6).num_found > 0 Or dicary(i7).num_found > 0 Then
                    inds(8).checked = True
                End If
            End If
        
        End If
    
    End If
    charting = "autism"
    i2 = FoundInList("22142590", charting)
    If dicary(i2).num_found > 0 And pt_age > 3 Then
        inds(8).checked = True
    End If
    
    suppressDbugLog = False

End Sub

Private Sub Check9()
    Dim i1, i2, i3, i4, i, ibeg, iend As Integer
    Dim charting As String
    
' 9 *21793189                          Anxious
' 9 *21793189                          Agitated
' 9 *21793189                          Crying
' 9 *21793189                          Combative
' 9 *21793189                          Fearful
' 9 *21793189                          Hostile
' 9 *21793189                          Depressed
' 9 *21793189                          Withdrawn
' 9 *72957277                          Disruptive
' 9 *72957277                          Requires repetitive
' 9 *72957277                          Requires comforting
'  0 *72957277                          Seeks RN assistance q 30-60
    suppressDbugLog = True
    
    If SeeRestraint Then
        inds(9).checked = True
        Exit Sub
    End If
    
    charting = "disruptive"
    i1 = FoundInList("72957277", charting)
    charting = "Requires repetitive"
    i2 = FoundInList("72957277", charting)
    charting = "Requires comforting"
    i3 = FoundInList("72957277", charting)
'    charting = "Seeks RN assistance q 30-60"
'    i4 = FoundInList("72957277", charting)
        
    If dicary(i1).num_found > 0 Or dicary(i2).num_found > 0 Or dicary(i3).num_found > 0 Then
'        charting = "anxious"
'        ibeg = FoundInList("21793189", charting)
'        charting = "hostile"
'        iend = FoundInList("21793189", charting)
'        For i = ibeg To iend
'            If dicary(i).num_found > 0 Then
'                inds(9).checked = True
'            End If
'        Next i
         inds(9).checked = True
    End If
    
    If dicary(i2).num_found > 0 Then
        charting = "depressed"
        ibeg = FoundInList("21793189", charting)
        charting = "withdrawn"
        iend = FoundInList("21793189", charting)
        For i = ibeg To iend
            If dicary(i).num_found > 0 Then
                inds(9).checked = True
            End If
        Next i
    End If
    
    suppressDbugLog = False
End Sub
Private Sub Check10()
    Dim i1, i2, i3, i4, i, ibeg, iend As Integer
    Dim charting As String
    
'  0 *72957277                          Seeks RN assistance q 30-60
    suppressDbugLog = True
    
    charting = "Seeks RN assistance q 30-60"
    i1 = FoundInList("72957277", charting)
        
    If dicary(i1).num_found > 0 Then
'        charting = "anxious"
'        i2 = FoundInList("21793189", charting)
'        charting = "crying"
'        i3 = FoundInList("21793189", charting)
'' 10*289219                            Confusion/Disorientation
'        charting = "Confusion/Disorientation"
'        i4 = FoundInList("21793189", charting)
'        If dicary(i2).num_found > 0 Or dicary(i3).num_found > 0 Or dicary(i4).num_found > 0 Then
        inds(10).checked = True
    End If
    
    suppressDbugLog = False
End Sub
Private Sub Check11_12()
    Dim i, i1, i2, i3, i4 As Integer
    Dim charting As String
    Dim minfalls As Integer
    Dim turnon As Boolean
    Dim lateston As String
    Dim count As Integer
    
'11*21793649                          Implemented
'11*21793649                          Continued
'11*21793649                          Discontinued
'11*21793649                          N/A
'11*61186687                          Implemented
'11*61186687                          Continued
'11*61186687                          Discontinued
'11*61186687                          N/A
    If inds(12).checked Then Exit Sub

    If SeeRestraint Then
        charting = "Safety mgmt q 2"
        i1 = FoundInList("22124229", charting)
        
        If dicary(i1).num_found > 0 Then
            inds(11).checked = True
        End If
        
    End If
    
    If Not inds(11).checked Then
        count = 0
        charting = "Safety mgmt q 2"
        i1 = FoundInList("123098909", charting)
        charting = "Restraints for safety"
        i2 = FoundInList("128365989", charting)
        charting = "Safety net bed"
        i3 = FoundInList("123099119", charting)
        If dicary(i1).num_found > 0 Then count = count + 1
        If dicary(i2).num_found > 0 Then count = count + 1
        If dicary(i3).num_found > 0 Then count = count + 1
        If SeeRestraint Then count = count + 1
        If count >= 2 Then
            inds(11).checked = True
            dprint 11, 0, "at least one pair of Safety and Restraints"
        End If
            
    End If

    
If Not inds(11).checked Then
    
    If range = 1440 Then
        minfalls = 2
    Else
        minfalls = 1
    End If
    
    turnon = False
    charting = "Implemented"
    i1 = FoundInList("21793649", charting)
    charting = "Continued"
    i2 = FoundInList("21793649", charting)
    If dicary(i1).num_found + dicary(i2).num_found >= minfalls Then
        turnon = True
        'find latest charttime of this
        lateston = ""
        If dicary(i1).num_found > 0 Then
            For i = 1 To dicary(i1).num_found
                If dicary(i1).charttime(i) > lateston Then
                    lateston = dicary(i1).charttime(i)
                End If
            Next i
        End If
        If dicary(i2).num_found > 0 Then
            For i = 1 To dicary(i2).num_found
                If dicary(i2).charttime(i) > lateston Then
                    lateston = dicary(i2).charttime(i)
                End If
            Next i
        End If
    End If
    If turnon Then dprint 11, 1, "Latest time for implemented/continued=" & lateston
    
    If turnon Then 'see if we should turn it off
        charting = "Discontinued"
        i3 = FoundInList("21793649", charting)
        If dicary(i3).num_found > 0 Then
            'need to check if this came later than impplemented/continued
            For i = 1 To dicary(i3).num_found
                If dicary(i3).charttime(i) > lateston Then
                    turnon = False
                    dprint 11, 1, "Latest time turns Off (discontinued)=" & dicary(i3).charttime(i)
                End If
            Next i
        End If
        charting = "N/A"
        i4 = FoundInList("21793649", charting)
        If dicary(i4).num_found > 0 Then
            'need to check if this came later than impplemented/continued
            For i = 1 To dicary(i4).num_found
                If dicary(i4).charttime(i) > lateston Then
                    turnon = False
                    dprint 11, 1, "Later time turns Off (na)=" & dicary(i4).charttime(i)
                End If
            Next i
        End If
    End If
    
    If turnon Then inds(11).checked = True
    
    turnon = False
    charting = "Implemented"
    i1 = FoundInList("61186687", charting)
    charting = "Continued"
    i2 = FoundInList("61186687", charting)
    If dicary(i1).num_found + dicary(i2).num_found >= minfalls Then
        turnon = True
        'find latest charttime of this
        lateston = ""
        If dicary(i1).num_found > 0 Then
            For i = 1 To dicary(i1).num_found
                If dicary(i1).charttime(i) > lateston Then
                    lateston = dicary(i1).charttime(i)
                End If
            Next i
        End If
        If dicary(i2).num_found > 0 Then
            For i = 1 To dicary(i2).num_found
                If dicary(i2).charttime(i) > lateston Then
                    lateston = dicary(i2).charttime(i)
                End If
            Next i
        End If
    End If
    If turnon Then dprint 11, 1, "Latest time for 61186687implemented/continued=" & lateston
    
    If turnon Then 'see if we should turn it off
        charting = "Discontinued"
        i3 = FoundInList("61186687", charting)
        If dicary(i3).num_found > 0 Then
            'need to check if this came later than impplemented/continued
            For i = 1 To dicary(i3).num_found
                If dicary(i3).charttime(i) > lateston Then
                    turnon = False
                    dprint 11, 1, "Latest time turns Off (61186687discontinued)=" & dicary(i3).charttime(i)
                End If
            Next i
        End If
        charting = "N/A"
        i4 = FoundInList("61186687", charting)
        If dicary(i4).num_found > 0 Then
            'need to check if this came later than impplemented/continued
            For i = 1 To dicary(i4).num_found
                If dicary(i4).charttime(i) > lateston Then
                    turnon = False
                    dprint 11, 1, "Later time turns Off (61186687na)=" & dicary(i4).charttime(i)
                End If
            Next i
        End If
    End If
    
    If turnon Then inds(11).checked = True
        
End If
    suppressDbugLog = False
End Sub

Private Sub Check14()
    Dim i1, i2, i3, i4, i, ibeg, iend, pulse, resp, fpulse, fresp, oralscore, io As Integer
    Dim charting As String
    
'22142965 Bili with   pulse,rr at q4
    suppressDbugLog = True
    
    charting = "Bili"
    i1 = FoundInList("22142965", charting)
    
    If dicary(i1).num_found > 0 Then
        charting = ""
        pulse = FoundInList("21793085", charting)
        charting = ""
        resp = FoundInList("200748", charting)
        
        fpulse = dicary(pulse).act_freq
        fresp = dicary(resp).act_freq
        
        If fpulse = 0 Or fresp = 0 Then
        Else
            If fpulse > fresp Then
                fresp = fpulse
            Else
                fpulse = fresp
            End If
            'at this point, fresp=fpulse
            If fresp <= 240 Then
                inds(14).checked = True
                dprint 14, 1, " Bili + pulse/rr"
           End If
        End If
    End If
    
'22143886          720             Linen/Pads Changed
    charting = "Linen/Pads"
    i1 = FoundInList("22143886", charting)
    If dicary(i1).num_found >= 6 Then
        inds(14).checked = True
        dprint 14, 1, " Linen changes >=6"
    End If
   
' 14*28153581          1440            Comatose
' 14*28153581                          Immuno-compromised
' 14*28153581                          Medical/Physical conditio
' 14*28153581                          Medications with mouth dr
' 14*28153581                          NPO for >24 Hrs
' 14*28153581                          Total Care dependency
' 14*28153581                          Ventilator Dependency
    charting = " "
    oralscore = FoundInList("21793956", charting)
    If dicary(oralscore).act_freq >= 11 And dicary(oralscore).act_freq <= 15 Then
        charting = "Comatose"
        ibeg = FoundInList("28153581", charting)
        charting = "Ventilator Dependency"
        iend = FoundInList("28153581", charting)
        For i = ibeg To iend
            If dicary(i).num_found > 0 Then
                inds(14).checked = True
                dprint 14, 1, dicary(i).chartkey & " + oral score 11-15"
            End If
        Next i
    End If
    
' 0 *73035390          1440            Intake / Output / Shift Care
    i = GetIO
    If i = 14 Then
        inds(14).checked = True
        dprint 14, 1, " I/O at q4"
    ElseIf i = 15 Then
        inds(15).checked = True
        dprint 15, 1, " I/O at q2"
    ElseIf i >= 16 Then
        inds(16).checked = True
        dprint 16, 1, " I/O at q1"
    End If
'    If i = 15 Then
'        inds(i).checked = True
'        dprint 15, 1, " I/O at q2"
'    End If
   
' 22124174  FEEDING TUBE RESIDUAL
    charting = "Feeding Tube Residual"
    i = FoundInList("22124174", charting)
    If dicary(i).num_found > 0 Then
        inds(14).checked = True
        dprint 14, 1, " Feeding Tube Residual"
    End If
    
'267758  Emerson   q4
    charting = "Emerson"
    i1 = FoundInList("267758", charting)
    If dicary(i1).num_found > 0 Then
        If dicary(i1).act_freq <= 240 Then
            inds(14).checked = True
            dprint 14, 1, " Emerson at q4"
        End If
    End If

'21793643 Yes  (incentive spirometry q4)
    charting = "Yes"
    i1 = FoundInList("267758", charting)
    If dicary(i1).num_found > 0 Then
        If dicary(i1).act_freq <= 240 Then
            inds(14).checked = True
            dprint 14, 1, " IncentiveSpirometry at q4"
        End If
    End If

'193374  Pulmonary 14-17
    charting = ""
    i1 = FoundInList("193374", charting)
    If dicary(i1).num_found > 0 Then
        If dicary(i1).act_freq <= 30 Then
            inds(17).checked = True
            dprint 17, 1, " LungSounds at q30"
        ElseIf dicary(i1).act_freq <= 60 Then
            inds(16).checked = True
            dprint 16, 1, " LungSounds at q1"
        ElseIf dicary(i1).act_freq <= 120 Then
            inds(15).checked = True
            dprint 15, 1, " LungSounds at q2"
        ElseIf dicary(i1).act_freq <= 240 Then
            inds(14).checked = True
            dprint 14, 1, " LungSounds at q4"
        End If
    End If
    
' 14*193845            1440            Blow-by Oxygen
' 14*193845            1440            Face Mask BIPAP
' 14*193845            1440            Nasal BIPAP
' 14*193845            1440            Nasal CPAP
' 14*193845            1440            Nasal Cannula
' 14*193845            1440            Non Rebreather Mask
' 14*193845            1440            Simple Mask
    charting = "Blow-by Oxygen"
    ibeg = FoundInList("193845", charting)
    charting = "Simple Mask"
    iend = FoundInList("193845", charting)
    For i = ibeg To iend
        If dicary(i).num_found > 0 Then
            inds(14).checked = True
            dprint 14, 1, dicary(i).chartkey
        End If
    Next i

' 14*193845            1440            Aerosol Trach Collar
    charting = "Aerosol Trach Collar"
    i1 = FoundInList("193845", charting)
    If dicary(i1).num_found > 0 Then
        If dicary(i1).act_freq <= 240 Then
            inds(14).checked = True
            dprint 14, 1, " Aerosol Trach Collar at q4"
        End If
    End If

' 14*22124174          240             Trach/Stoma Care
' 14*22124174          240             Cough and Deep Breathe
' 14*22124174          240             Incentive Spirometry
' 14*22124174          240             Pulse Oximeter, Spot Chec
' 14*22124174          240             Aerosol Updraft
' 14*22124174          240             Suction
' 14*22124174          240             Turn from Side to Side
    charting = "Trach/Stoma Care"
    ibeg = FoundInList("22124174", charting)
    charting = "Turn from Side to Side"
    iend = FoundInList("22124174", charting)
    For i = ibeg To iend
        If dicary(i).num_found > 0 Then
            inds(14).checked = True
            dprint 14, 1, dicary(i).chartkey
        End If
    Next i
    
' 14*22143886          240             Trach/Stoma Care
    charting = "Trach/Stoma Care"
    i1 = FoundInList("193845", charting)
    If dicary(i1).num_found > 0 Then
        If dicary(i1).act_freq <= 240 Then
            inds(14).checked = True
            dprint 14, 1, " Trach/Stoma Care at q4"
        End If
    End If
    
' *79132886
    charting = ""
    i1 = FoundInList("79132886", charting)
    If dicary(i1).num_found > 0 Then
        If dicary(i1).act_freq <= 240 Then
            inds(14).checked = True
            dprint 14, 1, " Nutrition and shift care IO 79132886 at q4"
        End If
    End If

    
' 14*272342            1440            Telemetry
' 14*282736            1440            Telemetry
    charting = "Telemetry"
    i1 = FoundInList("272342", charting)
    If dicary(i1).num_found > 0 Then
        inds(14).checked = True
        dprint 14, 1, " 272342 Telemetry"
    End If
    charting = "Telemetry"
    i1 = FoundInList("282736", charting)
    If dicary(i1).num_found > 0 Then
        inds(14).checked = True
        dprint 14, 1, " 282736 Telemetry"
    End If

' 14*193357            240             Click
' 14*193357            240             Muffled
' 14*193357            240             Murmur
' 14*193357            240             Rub
' 14*193357            240             S1S2
' 14*193357            240             S3
' 14*193357            240             S4
' 14*21793192          240             Cyanotic
' 14*21793192          240             > 3 seconds
' 14*21793192          240             Dusky
' 14*21793192          240             Pale
    charting = "Click"
    ibeg = FoundInList("193357", charting)
    charting = "Pale"
    iend = FoundInList("21793192", charting)
    For i = ibeg To iend
        If dicary(i).num_found > 0 Then
            inds(14).checked = True
            dprint 14, 1, dicary(i).chartkey
            If dicary(i).act_freq <= 120 Then
                inds(15).checked = True
                dprint 15, 1, dicary(i).chartkey
            End If
        End If
    Next i

' 14*282664            1440            Upright
    charting = "Upright"
    i1 = FoundInList("193845", charting)
    If dicary(i1).num_found > 0 Then
        inds(14).checked = True
        dprint 14, 1, " Upright"
    End If
    
' 14*22933819          1440            Peritoneal Dialysis
' 14*22143407          1440            Continuous Tube
' 14*28308762          1440            Peripheral Venous Cathete
    io = GetIO
    If io >= 14 Then
        charting = "Peritoneal Dialysis"
        ibeg = FoundInList("22933819", charting)
        charting = "Peripheral Venous Cathete"
        iend = FoundInList("28308762", charting)
        For i = ibeg To iend
            If dicary(i).num_found > 0 Then
                If io >= 16 Then
                    inds(16).checked = True
                ElseIf io >= 14 Then
                    inds(14).checked = True
                End If
                dprint 14, 1, dicary(i).chartkey
            End If
        Next i
    
    End If
' 3 *21793665          1440  12        Intermittent
' 3 *21793665                          Continuous
' 3 *21793665          1440  12        Bolus
    If io >= 14 Then
        charting = "Intermittent"
        ibeg = FoundInList("21793665", charting)
        charting = "Bolus"
        iend = FoundInList("21793665", charting)
        For i = ibeg To iend
            If dicary(i).num_found > 0 Then
                If io >= 15 Then
                    inds(15).checked = True
                ElseIf io >= 14 Then
                    inds(14).checked = True
                End If
                dprint 14, 1, dicary(i).chartkey
            End If
        Next i
    
    End If
    
'14*128054523                         over the needle
'14*128054523                         butterfly
    If io >= 14 Then
        charting = "over the needle"
        i1 = FoundInList("99274505", charting)
        If dicary(i1).num_found > 0 Then inds(14).checked = True
        
        charting = "butterfly"
        i2 = FoundInList("99274505", charting)
        If dicary(i2).num_found > 0 Then inds(14).checked = True
    End If
    
    suppressDbugLog = False
End Sub
Private Sub Check15()
    Dim i1, i2, i3, i4, i, ibeg, iend, resp, fresp, io, pulse, fpulse As Integer
    Dim charting As String
    
    suppressDbugLog = True
    
    If inds(15).checked Or inds(16).checked Or inds(17).checked Then
        Exit Sub
    End If
  
' 14*193845            1440            Blow-by Oxygen
' 14*193845            1440            Face Mask BIPAP
' 14*193845            1440            Nasal BIPAP
' 14*193845            1440            Nasal CPAP
' 14*193845            1440            Nasal Cannula
' 14*193845            1440            Non Rebreather Mask
' 14*193845            1440            Simple Mask
' 14*193845            1440            Aerosol Trach Collar

    charting = ""
    resp = FoundInList("200748", charting)
    fresp = dicary(resp).act_freq
    
    If fresp <= 240 And fresp >= 120 Then
        charting = "Blow-by Oxygen"
        ibeg = FoundInList("193845", charting)
        charting = "Aerosol Trach Collar"
        iend = FoundInList("193845", charting)
        For i = ibeg To iend
            If dicary(i).num_found > 0 Then
                inds(15).checked = True
                dprint 15, 1, dicary(i).chartkey
            End If
        Next i
        
        inds(15).checked = True
        dprint 15, 1, " RR q2-q4"
    End If

' 14*22124174          240             Trach/Stoma Care
' 14*22124174          240             Cough and Deep Breathe
' 14*22124174          240             Incentive Spirometry
' 14*22124174          240             Pulse Oximeter, Spot Chec
' 14*22124174          240             Aerosol Updraft
' 14*22124174          240             Suction
' 14*22124174          240             Turn from Side to Side
    charting = "Trach/Stoma Care"
    ibeg = FoundInList("22124174", charting)
    charting = "Turn from Side to Side"
    iend = FoundInList("22124174", charting)
    For i = ibeg To iend
        If dicary(i).num_found > 0 Then
            If dicary(i).act_freq <= 240 And dicary(i).act_freq > 120 Then
                inds(15).checked = True
                dprint 15, 1, dicary(i).chartkey
            End If
        End If
    Next i
    
' 14*22143886          240             Trach/Stoma Care
    charting = "Trach/Stoma Care"
    i1 = FoundInList("193845", charting)
    If dicary(i1).num_found > 0 Then
        If dicary(i1).act_freq <= 120 Then
            inds(15).checked = True
            dprint 15, 1, " Trach/Stoma Care at q2"
        End If
    End If
    
' 22124174  FEEDING TUBE RESIDUAL
    io = GetIO
    charting = "Feeding Tube Residual"
    i = FoundInList("22124174", charting)
    If dicary(i).num_found > 0 And io = 15 Then
        inds(15).checked = True
        dprint 15, 1, " Feeding Tube Residual"
    End If
    
' 3 *21793665          1440  12        Intermittent
' 3 *21793665                          Continuous
' 3 *21793665          1440  12        Bolus
    If io = 15 Then
        charting = "Intermittent"
        ibeg = FoundInList("21793665", charting)
        charting = "Bolus"
        iend = FoundInList("21793665", charting)
        For i = ibeg To iend
            If dicary(i).num_found > 0 Then
                inds(15).checked = True
                dprint 15, 1, dicary(i).chartkey
            End If
        Next i
    End If
    
    charting = ""
    pulse = FoundInList("21793085", charting)
    fpulse = dicary(pulse).act_freq
    If fpulse <= 120 And fpulse > 60 Then
            inds(15).checked = True
            dprint 15, 1, " Pulse q1-q2"
    End If

' 14*193357            240             Click
' 14*193357            240             Muffled
' 14*193357            240             Murmur
' 14*193357            240             Rub
' 14*193357            240             S1S2
' 14*193357            240             S3
' 14*193357            240             S4
' 14*21793192          240             Cyanotic
' 14*21793192          240             > 3 seconds
' 14*21793192          240             Dusky
' 14*21793192          240             Pale
    charting = "Click"
    ibeg = FoundInList("193357", charting)
    charting = "Pale"
    iend = FoundInList("21793192", charting)
    For i = ibeg To iend
        If dicary(i).num_found > 0 And dicary(i).act_freq <= 120 Then
            inds(15).checked = True
            dprint 15, 1, dicary(i).chartkey
        End If
    Next i

    suppressDbugLog = False
End Sub

Private Sub Check16()
    Dim i1, i2, i3, i4, i, ibeg, iend, pulse, fpulse As Integer
    Dim charting As String
    
    suppressDbugLog = True
    
    If inds(16).checked Or inds(17).checked Then
        Exit Sub
    End If
    
  
' 14*193845            1440            Blow-by Oxygen
' 14*193845            1440            Face Mask BIPAP
' 14*193845            1440            Nasal BIPAP
' 14*193845            1440            Nasal CPAP
' 14*193845            1440            Nasal Cannula
' 14*193845            1440            Non Rebreather Mask
' 14*193845            1440            Simple Mask
' 14*193845            1440            Aerosol Trach Collar
    charting = "Blow-by Oxygen"
    ibeg = FoundInList("193845", charting)
    charting = "Aerosol Trach Collar"
    iend = FoundInList("193845", charting)
    For i = ibeg To iend
        If dicary(i).num_found > 0 Then
        If dicary(i).act_freq <= 60 And dicary(i).act_freq > 30 Then
            inds(16).checked = True
            dprint 16, 1, dicary(i).chartkey
        End If
        End If
    Next i
    
' 14*22124174          240             Incentive Spirometry
' 14*22124174          240             Pulse Oximeter, Spot Chec
' 14*22124174          240             Aerosol Updraft
' 14*22124174          240             Suction
    charting = "Incentive Spirometry"
    ibeg = FoundInList("22124174", charting)
    charting = "Suction"
    iend = FoundInList("22124174", charting)
    For i = ibeg To iend
        If dicary(i).num_found > 0 Then
            If dicary(i1).act_freq <= 60 Then
                inds(16).checked = True
                dprint 16, 1, dicary(i).chartkey
            End If
        End If
    Next i
    
'pulse bp
    charting = ""
    pulse = FoundInList("21793085", charting)
    fpulse = dicary(pulse).act_freq
    If fpulse <= 60 And fpulse > 30 Then
            inds(16).checked = True
            dprint 16, 1, " Pulse q30-q1"
    End If

    charting = "Telemetry"
    i1 = FoundInList("272342", charting)
    If dicary(i1).num_found > 0 Then
    If dicary(i1).act_freq <= 60 Then
        inds(16).checked = True
        dprint 16, 1, " 272342 Telemetry q1"
    End If
    End If

    
' 16*22143826          60              Transduced ICP Monitoring
    charting = "Transduced ICP Monitoring"
    i1 = FoundInList("22143826", charting)
    If dicary(i1).num_found > 0 Then
    If dicary(i1).act_freq <= 60 Then
        inds(16).checked = True
        dprint 16, 1, " Transduced ICP q1"
    End If
    End If
    
    


    suppressDbugLog = False
End Sub
Private Sub Check17()
    Dim i1, i2, i3, i4, i, ibeg, iend, pulse, fpulse As Integer
    Dim charting As String
    
    suppressDbugLog = True
    
    If inds(17).checked Then
        Exit Sub
    End If
  
' 14*193845            1440            Blow-by Oxygen
' 14*193845            1440            Face Mask BIPAP
' 14*193845            1440            Nasal BIPAP
' 14*193845            1440            Nasal CPAP
' 14*193845            1440            Nasal Cannula
' 14*193845            1440            Non Rebreather Mask
' 14*193845            1440            Simple Mask
' 14*193845            1440            Aerosol Trach Collar
    charting = "Blow-by Oxygen"
    ibeg = FoundInList("193845", charting)
    charting = "Aerosol Trach Collar"
    iend = FoundInList("193845", charting)
    For i = ibeg To iend
        If dicary(i).num_found > 0 And dicary(i).act_freq <= 30 Then
            inds(17).checked = True
            dprint 17, 1, dicary(i).chartkey
        End If
    Next i
    
' 14*22124174          240             Pulse Oximeter, Spot Chec
' 14*22124174          240             Aerosol Updraft
    charting = "Pulse Oximeter, Spot Chec"
    ibeg = FoundInList("22124174", charting)
    charting = "Aerosol Updraft"
    iend = FoundInList("22124174", charting)
    For i = ibeg To iend
        If dicary(i).num_found > 0 Then
            If dicary(i1).act_freq <= 30 Then
                inds(17).checked = True
                dprint 17, 1, dicary(i).chartkey
            End If
        End If
    Next i
    
    charting = ""
    i1 = FoundInList("200748", charting)
    If dicary(i1).num_found > 0 And dicary(i1).act_freq <= 30 Then
        inds(17).checked = True
        dprint 17, 1, " RR at q30"
    End If

    charting = "Telemetry"
    i1 = FoundInList("272342", charting)
    If dicary(i1).num_found > 0 Then
    If dicary(i1).act_freq <= 30 Then
        inds(17).checked = True
        dprint 17, 1, " 272342 Telemetry q30"
    End If
    End If
    
' 16*22143826          60              Transduced ICP Monitoring
    charting = "Transduced ICP Monitoring"
    i1 = FoundInList("22143826", charting)
    If dicary(i1).num_found > 0 Then
    If dicary(i1).act_freq <= 30 Then
        inds(17).checked = True
        dprint 17, 1, " Transduced ICP q30"
    End If
    End If
    
'pulse bp
    charting = ""
    pulse = FoundInList("21793085", charting)
    fpulse = dicary(pulse).act_freq
    If fpulse <= 30 And fpulse > 0 Then
            inds(17).checked = True
            dprint 17, 1, " Pulse q30"
    End If


    suppressDbugLog = False
End Sub
Private Sub CheckIntakeOutput()  'for 14-17
    Dim i, j, k, istart, iend, count, tempcount As Integer
    Dim charting As String
    Dim times() As String
    Dim timeisthere As Boolean
    
    charting = ""
    istart = FoundInList("IOSTART", charting) + 1
    charting = ""
    iend = FoundInList("IOEND", charting) - 1
    
    suppressDbugLog = False
    count = 0
    For i = istart To iend
        If dicary(i).num_found > 0 Then
            For j = 1 To dicary(i).num_found
                If count > 0 Then
                    tempcount = count
                    timeisthere = False
                    For k = 1 To tempcount
                        If times(k) = dicary(i).charttime(j) Then
                            timeisthere = True
                        End If
                    Next k
                    If Not timeisthere Then
                        count = count + 1
                        ReDim Preserve times(0 To count)
                        times(count) = dicary(i).charttime(j)
                        dprint 14, 1, " Intake & Output count=" & count & " Time=" & times(count)
                    End If
                Else
                    count = 1
                    ReDim times(0 To 1)
                    times(1) = dicary(i).charttime(j)
                    dprint 14, 1, " Intake & Output count=" & count & " Time=" & times(count)
                End If
            Next j
        End If
    Next i
    
    If count >= 24 * range / RANGE_DENOMINATOR + 1 Then
        inds(17).checked = True
        dprint 17, 1, " Intake & Output count=" & count
    ElseIf count >= 12 * range / RANGE_DENOMINATOR Then
        inds(16).checked = True
        dprint 16, 1, " Intake & Output count=" & count
    ElseIf count >= 6 * range / RANGE_DENOMINATOR Then
        inds(15).checked = True
        dprint 15, 1, " Intake & Output count=" & count
    ElseIf count >= 3 * range / RANGE_DENOMINATOR Then
        inds(14).checked = True
        dprint 14, 1, " Intake & Output count=" & count
    End If
    
End Sub

Private Sub Check19()
    Dim i1, i2, i, ibeg, iend As Integer
    Dim charting As String

'0 *128054461                         Assess
'0 *128054461                         Start
    charting = "Assess"
    i1 = FoundInList("128054461", charting)
    charting = "Start"
    i2 = FoundInList("128054461", charting)
    
    If dicary(i1).num_found > 0 Or dicary(i2).num_found > 0 Then
        charting = "Dressing change"
        ibeg = FoundInList("99274752", charting)
        charting = "Infiltrated"
        iend = FoundInList("99274712", charting)
        For i = ibeg To iend
            If dicary(i).num_found > 0 Then
                inds(19).checked = True
            End If
        Next i
    End If

End Sub

Private Sub Check20()
    Dim i1 As Integer
    Dim j As Integer
    Dim charting As String
    Dim i1a As Integer
    Dim i1b As Integer
    Dim p1num As Integer
    Dim p1anum As Integer
    Dim p1bnum As Integer
    Dim nd As String
    Dim cd As String
    Dim sd As String
    Dim ed As String
    
    charting = "Yes"
    i1 = FoundInList("170954768", charting)
    p1num = dicary(i1).num_found
    If (p1num > 0) Then
        charting = ""
        i1a = FoundInList("170955166", charting)
        p1anum = dicary(i1a).num_found
        i1b = FoundInList("170955224", charting)
        p1bnum = dicary(i1b).num_found
        'Apply other constraints as described in mapping table.
        If p1anum > p1bnum Then 'there are only p1bnum endtimes
            p1anum = p1bnum
        ElseIf p1anum < p1bnum Then 'there are only p1anum start times
            p1bnum = p1anum
        End If
        For j = 1 To p1anum  'at this point p1anum=p1bnum=min(of initial p1anum,p1bnum)
            'make a procedure for each unique time
            cd = CerDateToISO(dicary(i1).charttime(j))
            If Mid$(nd, 1, 8) = Mid$(cd, 1, 8) Then 'same date
                sd = dicary(i1a).charttime(j)
                ed = dicary(i1b).charttime(j)
                If sd <> ed And (sd <> "" And ed <> "") Then
                    i1 = DateDiff("n", Str2Date(sd), Str2Date(ed)) '>= 30 Then
                    If i1 >= 30 Then
                        inds(20).checked = True
                    End If
                End If
            End If
        Next j
    End If

End Sub


Private Sub Check21()
    Dim i1, i2, i3, i4, i, ibeg, iend, pulse, fpulse, ed, edstart, edend As Integer
    Dim charting As String
    Dim eligible As Boolean
    
    suppressDbugLog = True
    
    If inds(21).checked Then
        Exit Sub
    End If
  
' 21*21793264          1440            Safety
' 21*21793264          1440            Invasive equipment
' 21*21793264          1440            Hygiene
' 21*21793264          1440            Invasive line
' 21*21793264          1440            Lab test
' 21*21793264          1440            Mechanical vent
' 21*21793264          1440            Medications, inp
' 21*21793264          1440            Medications, take
' 21*21793264          1440            Pain management
' 21*21793264          1440            Physical limit
' 21*21793264          1440            Skin care
' 21*21793264          1440            Surgical proc
' 21*21793264          1440            Smoking cessation
' 21*21793264          1440            Treatment proc
' 21*21793264          1440            Cast Care
' 21*21793264          1440            Trach Care
' 21*21793264          1440            Wound/Incision Care
' 21*21793264          1440            Chronic disease
' 21*21793264          1440            Restraints
' 21*21793264          1440            Diabetic
    
'0 *72957355                          Yes
'0 *72957358
'0 *72957361

    eligible = False
    charting = "Yes"
    ed = FoundInList("72957355", charting)
    charting = ""
    edstart = FoundInList("72957358", charting)
    charting = ""
    edend = FoundInList("72957361", charting)
    If dicary(ed).num_found > 0 Then  'removed requirement for start/end times 11/22/10
        eligible = True
    End If

    charting = "Basic Newborn"
    i1 = FoundInList("21793264", charting)
    charting = "Infant Bonding"
    i2 = FoundInList("21793264", charting)
    charting = "Developmental care"
    i3 = FoundInList("21793264", charting)
    If dicary(i1).num_found > 0 Or dicary(i2).num_found > 0 Or dicary(i3).num_found > 0 Then
        If InComa Then
            If FamilyPresent Then
                If Not PatientOrChildONLY Then
                    If dicary(i1).num_found > 0 Then
                        inds(21).checked = True
                    ElseIf eligible Then
                        inds(21).checked = True
                    End If
                End If
            End If
        Else
            If dicary(i1).num_found > 0 Then
                inds(21).checked = True
            ElseIf eligible Then
                inds(21).checked = True
            End If
        End If
    End If

    charting = "Medications, take"
    ibeg = FoundInList("21793264", charting)
    charting = "Diabetes"
    iend = FoundInList("21793264", charting)
    For i = ibeg To iend
        If dicary(i).num_found > 0 Then
            If InComa Then
                If FamilyPresent Then
                    inds(21).checked = True
                    dprint 21, 1, dicary(i).chartkey
                End If
            Else
                    inds(21).checked = True
                    dprint 21, 1, dicary(i).chartkey
            End If
        End If
    Next i
    
    If eligible Then
    charting = "Safety"
    ibeg = FoundInList("21793264", charting)
    charting = "Restraints"
    iend = FoundInList("21793264", charting)
    For i = ibeg To iend
        If dicary(i).num_found > 0 Then
            If InComa Then
                If FamilyPresent Then
                    inds(21).checked = True
                    dprint 21, 1, dicary(i).chartkey
                End If
            Else
                    inds(21).checked = True
                    dprint 21, 1, dicary(i).chartkey
            End If
        End If
    Next i
    End If
    


    suppressDbugLog = False
End Sub


Private Sub DoProcedures()
    Dim i1, i2, j As Integer
    Dim i1a As Integer
    Dim i1b As Integer
    Dim p1num As Integer
    Dim p2num As Integer
    Dim p1anum As Integer
    Dim p1bnum As Integer
    Dim charting As String
    Dim outstr As String
    Dim d As String
    Dim nd As String
    Dim cd As String
    Dim sd As String
    Dim ed As String
    Dim s_hr, s_mn, e_hr, e_mn As Integer
    
    suppressDbugLog = True
'2 P1
' 0 *72957280  unit based sitter
' 0 *72957283
' 0 *72957286
' 0 *72957280  unit based sitter
' 0 *72957289
' 0 *72957292
'P2
' 0 *72957319  yes
' 0 *72957322
' 0 *72957325
'P3
' 0 *72957328  yes
' 0 *72957331
' 0 *72957334
'P4
' 0 *72957355  yes
' 0 *72957358
' 0 *72957361
'P5
' 0 *72957301  yes
' 0 *72957304
' 0 *72957307
'P6
' 0 *72957310  yes
' 0 *72957313
' 0 *72957316
'P7
' 0 *72957337
' 0 *72957340
'P8
' 0 *72957343
' 0 *72957346
'P9
' 0 *72957349
' 0 *72957352


  
    d = CDate(nowdt)
    nd = NowDateToISO(d)


    'P1
    charting = "Unit based sitter"
    i1 = FoundInList("72957280", charting)
    charting = "1:1 safety observation-non rn"
    i2 = FoundInList("123098909", charting)
    p1num = dicary(i1).num_found
    p2num = dicary(i2).num_found
    If (p1num + p2num > 0) Then
        charting = ""
        i1a = FoundInList("72957283", charting)
        p1anum = dicary(i1a).num_found
        i1b = FoundInList("72957286", charting)
        p1bnum = dicary(i1b).num_found
        'Apply other constraints as described in mapping table.
        If p1anum > p1bnum Then 'there are only p1bnum endtimes
            p1anum = p1bnum
        ElseIf p1anum < p1bnum Then 'there are only p1anum start times
            p1bnum = p1anum
        End If
        For j = 1 To p1anum  'at this point p1anum=p1bnum=min(of initial p1anum,p1bnum)
            'make a procedure for each unique time
            cd = CerDateToISO(dicary(i1).charttime(j))
            If Mid$(nd, 1, 8) = Mid$(cd, 1, 8) Then 'same date
                sd = dicary(i1a).charttime(j)
                ed = dicary(i1b).charttime(j)
                If sd <> ed And (sd <> "") Then  'allow ed to go to end of day
                    i1 = DateDiff("n", Str2Date(sd), Str2Date(ed)) '>= 60 Then
                    If i1 >= 60 Or ed = "" Then
                        AddProc 1, sd, ed
                    End If
                End If
            End If
        Next j
    End If
        
    charting = "Yes"
    i1 = FoundInList("72957319", charting)
    p1num = dicary(i1).num_found
    If (p1num > 0) Then
        charting = ""
        i1a = FoundInList("72957322", charting)
        p1anum = dicary(i1a).num_found
        i1b = FoundInList("72957325", charting)
        p1bnum = dicary(i1b).num_found
        'Apply other constraints as described in mapping table.
        If p1anum > p1bnum Then 'there are only p1bnum endtimes
            p1anum = p1bnum
        ElseIf p1anum < p1bnum Then 'there are only p1anum start times
            p1bnum = p1anum
        End If
        For j = 1 To p1anum  'at this point p1anum=p1bnum=min(of initial p1anum,p1bnum)
            'make a procedure for each unique time
            cd = CerDateToISO(dicary(i1).charttime(j))
            If Mid$(nd, 1, 8) = Mid$(cd, 1, 8) Then 'same date
                sd = dicary(i1a).charttime(j)
                ed = dicary(i1b).charttime(j)
                If sd <> ed And (sd <> "") Then  'allow ed to go to end of day
                    i1 = DateDiff("n", Str2Date(sd), Str2Date(ed)) '>= 60 Then
                    If i1 >= 60 Or ed = "" Then
                        AddProc 2, sd, ed
                    End If
                End If
            End If
        Next j
    End If
   
    charting = "Yes"
    i1 = FoundInList("72957328", charting)
    p1num = dicary(i1).num_found
     If (p1num > 0) Then
        charting = ""
        i1a = FoundInList("72957331", charting)
        p1anum = dicary(i1a).num_found
        i1b = FoundInList("72957334", charting)
        p1bnum = dicary(i1b).num_found
        'Apply other constraints as described in mapping table.
        If p1anum > p1bnum Then 'there are only p1bnum endtimes
            p1anum = p1bnum
        ElseIf p1anum < p1bnum Then 'there are only p1anum start times
            p1bnum = p1anum
        End If
        For j = 1 To p1anum  'at this point p1anum=p1bnum=min(of initial p1anum,p1bnum)
            'make a procedure for each unique time
            cd = CerDateToISO(dicary(i1).charttime(j))
            If Mid$(nd, 1, 8) = Mid$(cd, 1, 8) Then 'same date
                sd = dicary(i1a).charttime(j)
                ed = dicary(i1b).charttime(j)
                If sd <> ed And (sd <> "" And ed <> "") Then
                    i1 = DateDiff("n", Str2Date(sd), Str2Date(ed)) '>= 60 Then
                    If i1 >= 60 Then
                        AddProc 3, sd, ed
                    End If
                End If
            End If
        Next j
    End If
   
    charting = "Yes"
    i1 = FoundInList("72957355", charting)
    p1num = dicary(i1).num_found
     If (p1num > 0) Then
        charting = ""
        i1a = FoundInList("72957358", charting)
        p1anum = dicary(i1a).num_found
        i1b = FoundInList("72957361", charting)
        p1bnum = dicary(i1b).num_found
        'Apply other constraints as described in mapping table.
        If p1anum > p1bnum Then 'there are only p1bnum endtimes
            p1anum = p1bnum
        ElseIf p1anum < p1bnum Then 'there are only p1anum start times
            p1bnum = p1anum
        End If
        For j = 1 To p1anum  'at this point p1anum=p1bnum=min(of initial p1anum,p1bnum)
            'make a procedure for each unique time
            cd = CerDateToISO(dicary(i1).charttime(j))
            If Mid$(nd, 1, 8) = Mid$(cd, 1, 8) Then 'same date
                sd = dicary(i1a).charttime(j)
                ed = dicary(i1b).charttime(j)
                If sd <> ed And (sd <> "" And ed <> "") Then
                    i1 = DateDiff("n", Str2Date(sd), Str2Date(ed)) '>= 60 Then
                    If i1 >= 60 Then
                        AddProc 4, sd, ed
                    End If
                End If
            End If
        Next j
    End If
    
    charting = "Yes"
    i1 = FoundInList("72957301", charting)
    p1num = dicary(i1).num_found
     If (p1num > 0) Then
        charting = ""
        i1a = FoundInList("72957304", charting)
        p1anum = dicary(i1a).num_found
        i1b = FoundInList("72957307", charting)
        p1bnum = dicary(i1b).num_found
        'Apply other constraints as described in mapping table.
        If p1anum > p1bnum Then 'there are only p1bnum endtimes
            p1anum = p1bnum
        ElseIf p1anum < p1bnum Then 'there are only p1anum start times
            p1bnum = p1anum
        End If
        For j = 1 To p1anum  'at this point p1anum=p1bnum=min(of initial p1anum,p1bnum)
            'make a procedure for each unique time
            cd = CerDateToISO(dicary(i1).charttime(j))
            If Mid$(nd, 1, 8) = Mid$(cd, 1, 8) Then 'same date
                sd = dicary(i1a).charttime(j)
                ed = dicary(i1b).charttime(j)
                If sd <> ed And (sd <> "" And ed <> "") Then
                    i1 = DateDiff("n", Str2Date(sd), Str2Date(ed)) '>= 60 Then
                    If i1 >= 60 Then
                        AddProc 5, sd, ed
                    End If
                End If
            End If
        Next j
    End If
    
    charting = "Yes"
    i1 = FoundInList("72957310", charting)
    p1num = dicary(i1).num_found
     If (p1num > 0) Then
        charting = ""
        i1a = FoundInList("72957313", charting)
        p1anum = dicary(i1a).num_found
        i1b = FoundInList("72957316", charting)
        p1bnum = dicary(i1b).num_found
        'Apply other constraints as described in mapping table.
        If p1anum > p1bnum Then 'there are only p1bnum endtimes
            p1anum = p1bnum
        ElseIf p1anum < p1bnum Then 'there are only p1anum start times
            p1bnum = p1anum
        End If
        For j = 1 To p1anum  'at this point p1anum=p1bnum=min(of initial p1anum,p1bnum)
            'make a procedure for each unique time
            cd = CerDateToISO(dicary(i1).charttime(j))
            If Mid$(nd, 1, 8) = Mid$(cd, 1, 8) Then 'same date
                sd = dicary(i1a).charttime(j)
                ed = dicary(i1b).charttime(j)
                If sd <> ed And (sd <> "" And ed <> "") Then
                    i1 = DateDiff("n", Str2Date(sd), Str2Date(ed)) '>= 60 Then
                    If i1 >= 60 Then
                        AddProc 6, sd, ed
                    End If
                End If
            End If
        Next j
    End If
    
    
'    Coordination -- RN 1:1 Date/Time Start 72957343        08/17/10 21:14:00 0:2010081714300000:0.000000:0:0
'    Coordination -- RN 1:1 Date/Time Start 72957343        08/17/10 21:14:00
'    Coordination -- RN 1:1 Date/Time Start 72957343        08/17/10 15:48:00 0:2010081714300000:0.000000:0:0
'    Coordination -- RN 1:1 Date/Time End   72957346        08/17/10 21:14:00 0:2010081715480000:0.000000:0:0
'    Coordination -- RN 1:1 Date/Time End   72957346        08/17/10 21:14:00
'    Coordination -- RN 1:1 Date/Time End   72957346        08/17/10 15:48:00 0:2010081715480000:0.000000:0:0
    
    'sh be btwn 37 and 40.  If 37 not current date, then donot apply.  40-37 must be >=60 mins
        charting = ""
        i1a = FoundInList("72957337", charting)
        p1anum = dicary(i1a).num_found
        i1b = FoundInList("72957340", charting)
        p1bnum = dicary(i1b).num_found
        'Apply other constraints as described in mapping table.
        If p1anum > p1bnum Then 'there are only p1bnum endtimes
            p1anum = p1bnum
        ElseIf p1anum < p1bnum Then 'there are only p1anum start times
            p1bnum = p1anum
        End If
        For j = 1 To p1anum  'at this point p1anum=p1bnum=min(of initial p1anum,p1bnum)
            'make a procedure for each unique time
            cd = dicary(i1a).charttime(j)
            If Mid$(nd, 1, 8) = Mid$(cd, 1, 8) Then 'same date
                sd = dicary(i1a).charttime(j)
                ed = dicary(i1b).charttime(j)
                If sd <> ed And (sd <> "" And ed <> "") Then
                    i1 = DateDiff("n", Str2Date(sd), Str2Date(ed)) '>= 60 Then
                    If i1 >= 60 Then
                        AddProc 7, sd, ed
                    End If
                End If
            End If
        Next j
    
    'sh be btwn 43 and 46.  If 37 not current date, then donot apply.  40-37 must be >=60 mins
    charting = ""
    i1a = FoundInList("72957343", charting)
    p1anum = dicary(i1a).num_found
    i1b = FoundInList("72957346", charting)
    p1bnum = dicary(i1b).num_found
        If p1anum > p1bnum Then 'there are only p1bnum endtimes
            p1anum = p1bnum
        ElseIf p1anum < p1bnum Then 'there are only p1anum start times
            p1bnum = p1anum
        End If
        For j = 1 To p1anum  'at this point p1anum=p1bnum=min(of initial p1anum,p1bnum)
            cd = dicary(i1a).charttime(j)
            If Mid$(nd, 1, 8) = Mid$(cd, 1, 8) Then 'same date
                sd = dicary(i1a).charttime(j)
                ed = dicary(i1b).charttime(j)
                If sd <> ed And (sd <> "" And ed <> "") Then
                    i1 = DateDiff("n", Str2Date(sd), Str2Date(ed)) '>= 60 Then
                    If i1 >= 60 Then
                        AddProc 8, sd, ed
                    End If
                End If
            End If
        Next j
    
    'sh be btwn 49 and 52.  If 37 not current date, then donot apply.  40-37 must be >=60 mins
    charting = ""
    i1a = FoundInList("72957349", charting)
    p1anum = dicary(i1a).num_found
    i1b = FoundInList("72957352", charting)
    p1bnum = dicary(i1b).num_found
        If p1anum > p1bnum Then 'there are only p1bnum endtimes
            p1anum = p1bnum
        ElseIf p1anum < p1bnum Then 'there are only p1anum start times
            p1bnum = p1anum
        End If
        For j = 1 To p1anum  'at this point p1anum=p1bnum=min(of initial p1anum,p1bnum)
            cd = dicary(i1a).charttime(j)
            If Mid$(nd, 1, 8) = Mid$(cd, 1, 8) Then 'same date
                sd = dicary(i1a).charttime(j)
                ed = dicary(i1b).charttime(j)
                If sd <> ed And (sd <> "" And ed <> "") Then
                    i1 = DateDiff("n", Str2Date(sd), Str2Date(ed)) '>= 60 Then
                    If i1 >= 60 Then
                        AddProc 9, sd, ed
                    End If
                End If
            End If
         Next j
         
    CombineProcs
    For j = 1 To totnumprocs
        If Not procs(j).isdup Then  'only do unique start/end times
            OutputProcs j, procs(j).s, procs(j).e
        End If
    Next j

End Sub

Private Sub OutputProcs(index As Integer, procin As String, procout As String)
    Dim outstr As String
    Dim i As Integer
    Dim pstr As String

'    outstr = Space$(9) & unitname
'    outstr = outstr & Space$(26 - Len(outstr))
'    outstr = outstr & unitname '27
'    outstr = outstr & Space$(40)
'    outstr = outstr & acctnum & Space$(20 - Len(acctnum) + 1) '70
'    outstr = outstr & lastname & Space$(32 - Len(lastname) + 1) '91
'    outstr = outstr & firstname & Space$(32 - Len(firstname) + 1) '124
'    outstr = outstr & Space$(32 + 1) '157
'    outstr = outstr & Space$(8 + 1)  '190
'    outstr = outstr & Space$(4 + 1)  '199
'    outstr = outstr & procin & Space$(1) '204
'    outstr = outstr & Space$(39) & "P"  '257
'    outstr = outstr & Space$(39)
'    outstr = outstr & procin  '360
'    outstr = outstr & Space$(40)
'    outstr = outstr & procout  '360
'    outstr = outstr & Space$(19)
    
    outstr = Space$(9) & unitname '10 unit
    outstr = outstr & Space$(43 - Len(outstr))
    outstr = outstr & "" '44 tx area
    outstr = outstr & Space$(69 - Len(outstr))
    outstr = outstr & acctnum  '70 acct
    outstr = outstr & Space$(90 - Len(outstr))
    outstr = outstr & lastname & " " & firstname '91 last first
    outstr = outstr & Space$(203 - Len(outstr))
    outstr = outstr & procin '204 procdt
    outstr = outstr & Space$(255 - Len(outstr))
    outstr = outstr & "p"    '256 procedure type record
    outstr = outstr & Space$(295 - Len(outstr))
    outstr = outstr & procin '296 procdt in
    outstr = outstr & Space$(347 - Len(outstr))
    outstr = outstr & procout '348 procdt out
    outstr = outstr & Space$(378 - Len(outstr))
    
    pstr = "NNNNNNNNN"
    For i = index To totnumprocs
        If procs(index).s = procs(i).s And procs(index).e = procs(i).e Then
            Mid$(pstr, procs(i).pnum, 1) = "Y"
        End If
    Next i
    outstr = outstr & pstr

    Print #outfile, outstr
    Print #outlogfile, outstr
End Sub


Private Function EventIsProcedureTime(e As String) As Boolean
    
    EventIsProcedureTime = False
    Select Case e
        Case "72957283", "72957286", "72957289", "72957292", "72957322", "72957325", "72957331", "72957334", "72957358", "72957361", _
        "72957304", "72957307", "72957313", "72957316", "72957337", "72957340", "72957343", "72957346", "72957349", "72957352", _
        "170955224", "170955166"   'these last 2 are for ind 20, not procedures
            EventIsProcedureTime = True
    End Select
                                                         
End Function

Private Function Str2Date(s As String) As Date
's is like 201008261559
    Str2Date = DateSerial(Mid$(s, 1, 4), Mid$(s, 5, 2), Mid$(s, 7, 2)) _
                + TimeSerial(Mid$(s, 9, 2), Mid$(s, 11, 2), 0)

End Function

Private Sub AddProc(pnum As Integer, s As String, e As String)
    Dim i As Integer
    Dim dupfound As Boolean
    
    If totnumprocs = 0 Then
        totnumprocs = totnumprocs + 1
        If totnumprocs <= MAX_PROCS Then
        procs(totnumprocs).pnum = pnum
        procs(totnumprocs).s = s
        procs(totnumprocs).e = e
        End If
    Else
        dupfound = False
        For i = 1 To totnumprocs
            If procs(i).pnum = pnum And procs(i).s = s And procs(i).e = e Then dupfound = True
        Next i
        If Not dupfound Then
            totnumprocs = totnumprocs + 1
            If totnumprocs <= MAX_PROCS Then
            procs(totnumprocs).pnum = pnum
            procs(totnumprocs).s = s
            procs(totnumprocs).e = e
            End If
        End If
    End If

End Sub

Private Sub CombineProcs()
    Dim i, j As Integer
    
    For i = 1 To totnumprocs
        For j = i + 1 To totnumprocs
            If procs(i).s = procs(j).s And procs(i).e = procs(j).e Then
                procs(j).isdup = True  'is start and end time dup
            End If
        Next j
    Next i

End Sub
