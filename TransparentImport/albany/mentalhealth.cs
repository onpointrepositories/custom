﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;                     // for LINQ; add a reference
using System.Data.SqlClient;                // for SqlConnection
using System.IO;                            // for Directory, Path, Stream
using PfsShared;                            // add a reference to Shared2 project

// ============================================================================
// ALBANY Mental Health transparent mapping -- GOES HERE --
// (The code below is a sample from Sharp)
// ============================================================================
// This processes one patient.  Only one database query is used.  :)
//
// All search functions use exact match for category, description and field name.
// Codes and results can be a single word or a list of words.
// Codes default to exact match; prefix with CODE_CODE_LIKE_PREFIX if desired (single word only)
// Results default to like matching; prefix with RESULT_EXACT_MATCH_PREFIX if desired (single word only)
//
// All searches are case insensitive.
//
//
namespace TransparentMapping
{
    class MentalHealth
    {
        private const int MAX_INDS = 50;
        private const int MAX_PROCS = 20;

        private const string CODE_LIKE_PREFIX = "%!";       // use to make codes %like%
        private const string EXACT_MATCH_PREFIX = "&!";     // use to make desc/result exact match
        private const string CHAR_COMMA = "||";             // use to insert literal comma in word list
        private const string NOT_PREFIX = "!!";             // use to insert literal comma in word list

        private struct indicator_data {
            public bool     is_checked;     
            public int      radio_group;       
        }

        private struct proc_data {                      
            public int      procedure_number;
            public DateTime start;   
            public DateTime finish;
        }

        // These are database CHART_ITEMs for this patient
        private CHART_ITEM[] _chart_items_since_admission;
        private CHART_ITEM[] _chart_items_since_unit_arrival;
        private CHART_ITEM[] _chart_items_during_pull_period;
        private CHART_ITEM[] _chart_items_inprev24hrs;
        // These are database PROCEDURE_EVENTs for this patient
        private PROCEDURE_EVENT[] _procedure_events;

        private indicator_data[] _inds;
        private List<proc_data> _procs;

        private const int DEFAULT_BUCKET_SIZE = 20;       //min

        private PatientInfo _pat;
        private int  _bucket_size = DEFAULT_BUCKET_SIZE;
        private bool is_default;


        private enum SearchDepth {
            SearchDefault,
            SearchPullRange,            //search within the current pull     -- default
            Search24Hrs,               // search prev 12 or 24 hrs
            SearchSinceArrival,         //search since arrival to the unit
            SearchSinceAdmission        //search everything since admission to the hospital
        }

        private enum CountMode {
            CountAll,
            CountFirst                  //stop after one is found
        }

        private enum GetValueMode {
            GetTotal,
            GetMax,
            GetLast
        }

        enum Frequencies {
            QNONE,
            Q4H,
            Q2H,
            Q1H,
            Q30M
        }

        private struct fmapRow {
            public double   los_high;                       //the LOS being testing
            public int[]    freq;                           //the count required for each Q value
        }
        private List<fmapRow> _freq_map;                    //1,2,4,8,12,24 hours

     
        //
        // This is the main entry point
        //
        public void ProcessPatient(PatientInfo pat)
        {
            _pat = pat;

            InitIndicators(); // sets is_default
            InitProcs();
            if (! is_default)
                {
                LoadFreqTable();
                LoadPatientChart();
                Check_1_2_3();
                Check_4();
                Check_5_6();
                Check_7_8();
                Check_9_10_11_12_13();
                Check_14_15();
                Check_16_17_18();
                Check_19();
                Check_20();
                Check_21();
                Check_22();
                //Check_UD();
            }
            //AtLeastOneADL();

            if (!is_default)
            {
                //CheckProcs();
            }
            HighestIndicatorInEachGroupWins();

            if (Program.g_no_output) return;
            OutputClass();
            //OutputProcs();
        }

       
        private void InitIndicators()
        {
            // Make a new (empty) indicator array
            _inds = new indicator_data[MAX_INDS + 1];           // This 1 based so add one
            is_default = false;
            //if (_pat.los_hours <= 4.0) {
            //    is_default = true;
            //    Program.VerboseAudit("Patient was here 4 hrs or less. Will receive default indicators " + _pat.default_inds_str);
            //    foreach (var ind in _pat.default_inds) {
            //        if (ind <= _inds.GetUpperBound(0)) {
            //            _inds[ind].is_checked = true;
            //        }
            //    }
            //}


            // get indicator radio groups from the database
            // ** (This database access can be replaced once we have a C# methodolgy cache)
            var db = PFSUtility.NewPfsDataContext();
            var query = from idef in db.INDICATOR_DEFINITIONs
                        where (idef.METHODOLOGY_ID == _pat.meth_id)
                        select idef;
            foreach (var idef in query) {
                if (idef.INDICATOR_NUMBER <= _inds.GetUpperBound(0)) {
                    // (convert nulls to zero)
                    _inds[idef.INDICATOR_NUMBER].radio_group = PFSUtility.DBToInt(idef.RADIO_GROUP);
                }
            }
        }

        private void InitProcs()
        {
            _procs = new List<proc_data>();
        }

        private fmapRow LoadFreqTableRow(double los_high, string values)
        {
            fmapRow fmrow;
            
            fmrow.los_high = los_high;
            fmrow.freq = new int[5];

            var arr = values.Split(',');
            for (int i = 0; i <= fmrow.freq.GetUpperBound(0) ; i++ )
            {
                fmrow.freq[i] = arr[i].ToInteger();
            }
            return fmrow;
        }

        // These are the number of times you need to do something during a certain time period (LOS)
        // in order for it to be considered one of the frequencies listed.
        private void LoadFreqTable()
        {
            _freq_map = new List<fmapRow>();
            //                              LOS,  None Q4h Q2h Q1h Q30m
            //_freq_map.Add(LoadFreqTableRow(1, "    0,  0,  0,  4,  8"));
            //_freq_map.Add(LoadFreqTableRow(2, "    0,  1,  2,  4,  8"));
            _freq_map.Add(LoadFreqTableRow(4, "    0,  1,  2,  4, 99"));
            _freq_map.Add(LoadFreqTableRow(6, "    0,  1,  2,  4, 99"));
            _freq_map.Add(LoadFreqTableRow(8, "  0,  1,  2,  6, 99"));
            _freq_map.Add(LoadFreqTableRow(12, " 0,  2,  4,  8, 99"));
            _freq_map.Add(LoadFreqTableRow(16, "   0,  3,  5,  9, 20"));
            _freq_map.Add(LoadFreqTableRow(24, "   0,  4,  8, 15, 29"));
            _freq_map.Add(LoadFreqTableRow(9999, " 0,  4,  8, 15, 29"));
//New freq table 2/5/14
//q4	q2	q1	q30     q30
//            Non-ICU	ICU & SD
// 4	8	15	29	    36
// 3	5	9	17	    24
// 2	4	7	13	    19
// 2	3	5	10	    13

            //to read this table: if you have at least 6 asessmenets in a 12-hour period, you get Q1h
            //(LOS=12, column Q1h has a count of 6)
        }

        private Frequencies FreqForCount(double los_hours, int count) 
        {
            if (los_hours < 4 && count >= 1)
                return Frequencies.Q4H;
            else
                foreach(var fmrow in _freq_map) {
                    if (los_hours <= fmrow.los_high) {
                        // Option: pro-rate the count upward if the LOS is less than the row we are interested in.
                        //         This will bump the count to what it might have been at the full LOS.
                        // Note: truncate the result; rounding inflates the value too much.
                        int prorated_count = (int)((fmrow.los_high / los_hours) * count);

                        // foreach goes low to high; go from high to low instead
                        for (int j = (int)Frequencies.Q1H; (j >= (int)Frequencies.QNONE); j--) { //search right to left
                            if (prorated_count >= fmrow.freq[j]) {
                                return (Frequencies)j;
                            }
                        } // next j
                    }
            }
            
            return Frequencies.QNONE;
        }

        //
        // Load patient chart from the database (one DB access)
        // Load everything since admission
        //
        private void LoadPatientChart()
        {
            // Get the entire patient chart (all units and dates for this patient)
            var db = PFSUtility.NewPfsDataContext();
            var query = from item in db.CHART_ITEMs
                        where (item.ENCOUNTER_ID == _pat.encounter_id)
                        select item;
            // Save the result
            _chart_items_since_admission = query.ToArray();

            // Convert all results to lower case for case insensitive comparisons
            foreach (var item in _chart_items_since_admission) {
                if (item.CATEGORY != null) item.CATEGORY = item.CATEGORY.ToLower();
                if (item.CODE != null) item.CODE = item.CODE.ToLower();
                if (item.DESCRIPTION != null) item.DESCRIPTION = item.DESCRIPTION.ToLower();
                if (item.FIELD_NAME != null) item.FIELD_NAME = item.FIELD_NAME.ToLower();
                if (item.RESULT != null) item.RESULT = item.RESULT.ToLower();
            }

            // Prepare two more versions of the chart
            var query2 = from item in _chart_items_since_admission
                    where(item.EVENT_DATETIME >= _pat.unit_arrival) && (item.EVENT_DATETIME <= _pat.pull_finish)
                    select item;
            _chart_items_since_unit_arrival = query2.ToArray();

            int lookbackhrs = -24;
            if (Program.g_pull_finish.Hour == 19) lookbackhrs = -12;
            Program.VerboseAudit("Once-a-day documentation look back hours = " + lookbackhrs);
            query2 = from item in _chart_items_since_admission
                     where (item.EVENT_DATETIME >= _pat.pull_start.AddHours(lookbackhrs)) && (item.EVENT_DATETIME <= _pat.pull_finish)
                     select item;
            _chart_items_inprev24hrs = query2.ToArray();
            query2 = from item in _chart_items_since_admission
                     where (item.EVENT_DATETIME >= _pat.pull_start) && (item.EVENT_DATETIME <= _pat.pull_finish)
                     select item;
            _chart_items_during_pull_period = query2.ToArray();
        }

        // Delay looking for patient procedures until they are wanted
        private void LoadPatientProceduresIfNeeded()
        {
            if (_procedure_events != null) return;

            var db = PFSUtility.NewPfsDataContext();
            var query = from proc in db.PROCEDURE_EVENTs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                        select proc;
            _procedure_events = query.ToArray();
        }



        // Started a new chart item query (of a certain depth)
        // Default is the chart during the pull range.  Options for unit arrival and admission to hospital.
        private IEnumerable<CHART_ITEM> StartNewQuery()
        {
            return StartNewQuery(SearchDepth.SearchDefault);
        }
        private IEnumerable<CHART_ITEM> StartNewQuery(SearchDepth search_depth)
        {
            if (search_depth == SearchDepth.SearchDefault && _pat.los_hours < 12 && _pat.was_in_ED)
                search_depth = SearchDepth.Search24Hrs;
            switch (search_depth)
            {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    return (from item in _chart_items_during_pull_period select item);
                case SearchDepth.Search24Hrs:
                    return (from item in _chart_items_inprev24hrs select item);
                case SearchDepth.SearchSinceArrival:
                    return (from item in _chart_items_since_unit_arrival select item);
                case SearchDepth.SearchSinceAdmission:
                    return (from item in _chart_items_since_admission select item);
            }
            return null;
        }

        // Is this search term a word or a list of words?
        private bool ValueIsAList(string s) 
        {
            return (s != null) && s.Contains(',');                //is this a comma-separated list?
        }

        // Break apart a wordlist and prepare its members
        private string[] SplitOnCommaAndPrepareElements(string s)
        {
            // If a word list has a space after each comma, we will get leading blanks in each term
            var arr = s.Split(',');
            for (int i = 0; i <= arr.GetUpperBound(0); i++) {
                arr[i] = arr[i].Trim();                         // get rid of leading blanks
                arr[i] = arr[i].Replace(CHAR_COMMA, ",");       // insert a literal comma into search word
                arr[i] = arr[i].ToLower();                      // convert for case-insensitive comparisons
            }
            return arr;
        }

        // Add a basic chart item filter to a LINQ query
        //
        // cat = exact match
        // code_list = exact match; optional "like" match
        // desc_list = "like" match; optional exact match
        // field = exact match
        // result_list = "like" match; optional exact match
        //
        // These are comma-separated lists.  
        // Search for a literal comma by using CHAR_COMMA in the string.
        private IEnumerable<CHART_ITEM> AndItemFilter(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list) 
        {
            // Converted all values to lower case for case-insensitive comparisons.
            if (!String.IsNullOrEmpty(cat)) {
                cat = cat.ToLower();
                query = query.Where(e => e.CATEGORY == cat);
            }

            if (!String.IsNullOrEmpty(code_list)) {
                code_list = code_list.ToLower();
                if (code_list.Left(2) == EXACT_MATCH_PREFIX) {          // exact match override?
                    query = query.Where(e => e.CODE == code_list.Substring(2));
                } else if (code_list.Left(2) == CODE_LIKE_PREFIX)
                {            // override for "like" match?
                    query = query.Where(e => e.CODE.Contains(code_list.Substring(2)));
                } else if (ValueIsAList(code_list)) {
                    query = AndCodeInList(query, code_list);            // find one of the words
                } else {
                    query = query.Where(e => e.CODE.Contains(code_list));      // find this word
                }
            }
            
            if (!String.IsNullOrEmpty(desc_list)) {
                desc_list = desc_list.ToLower();
                if (desc_list.Left(2) == EXACT_MATCH_PREFIX) {          // exact match override?
                    query = query.Where(e => e.DESCRIPTION == desc_list.Substring(2));
                } else if (ValueIsAList(desc_list)) {
                    query = AndDescriptionInList(query, desc_list);
                } else {
                    desc_list = desc_list.Replace(CHAR_COMMA, ",");     // look for literal comma requests
                    query = query.Where(e => e.DESCRIPTION.Contains(desc_list));
                }
            }
            
            if (!String.IsNullOrEmpty(field)) {
                field = field.ToLower();
                query = query.Where(e => e.FIELD_NAME == field);
            }
            
            if (!String.IsNullOrEmpty(result_list)) {
                result_list = result_list.ToLower();                        // make all results case insensitve
                if (result_list.Left(2) == EXACT_MATCH_PREFIX) {     // exact match override?
                    query = query.Where(e => e.RESULT == result_list.Substring(2));
                } else if (ValueIsAList(result_list)) {
                    query = AndResultInList(query, result_list);
                } else {
                    result_list = result_list.Replace(CHAR_COMMA, ",");     // look for literal comma requests
                    query = query.Where(e => e.RESULT.Contains(result_list));
                }
            }
            return query;
        }

        // Look for a code that is one of the words in the list.
        private IEnumerable<CHART_ITEM> AndCodeInList(IEnumerable<CHART_ITEM> query, string code_list) 
        {
            if (String.IsNullOrEmpty(code_list)) return query;
            
            var arr = SplitOnCommaAndPrepareElements(code_list);
            return query.Where(e => arr.Any(e.CODE.ToLower().Contains)); //this means:  are any of the arr items contained in the CODE
        }

        // Look for a description that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndDescriptionInList(IEnumerable<CHART_ITEM> query, string desc_list) 
        {
            if (String.IsNullOrEmpty(desc_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(desc_list);
            return query.Where(e => arr.Any(e.DESCRIPTION.ToLower().Contains));    // "like" match
        }

        // Look for a result that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultInList(IEnumerable<CHART_ITEM> query, string result_list) 
        {
            if (String.IsNullOrEmpty(result_list)) return query;
            
            var arr = SplitOnCommaAndPrepareElements(result_list);
            return query.Where(e => arr.Any(e.RESULT.ToLower().Contains));         // "like" match
        }

        // Look for a result that does not contain one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultNotInList(IEnumerable<CHART_ITEM> query, string result_list)
        {
            if (String.IsNullOrEmpty(result_list)) return query;
            
            var arr = SplitOnCommaAndPrepareElements(result_list);
            return query.Where(e => !arr.Any(e.RESULT.ToLower().Contains));        // "like" match
        }

        private string DescribeSearchDepth(SearchDepth search_depth) 
        {
            string result = "";
            
            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    //result = "in pull range"
                    result = "";                         //be quiet since this is default
                    break;
                case SearchDepth.Search24Hrs:
                    result = "in previous 24 hours";
                    break;
                case SearchDepth.SearchSinceArrival:
                    result = "since arrival to unit";
                    break;
                case SearchDepth.SearchSinceAdmission:
                    result = "since admission";
                    break;
            }
            
            return result;
        }

        // Describe a query that has already been run
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Make a simple query and describe the results
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            var arr = query.ToArray();
            return Describe(arr, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private string LookingFor(string so_far, string what, string op, string value)
        {
            string result;
            if (String.IsNullOrEmpty(value)) return so_far;

            if (so_far == "")
                result = "looking for " + what;
            else
                result = so_far + " and " + what;

            if (value.Left(2) == EXACT_MATCH_PREFIX)
                result += "='" + value.Substring(2) + "'";
            else if (value.Left(2) == CODE_LIKE_PREFIX)
                result += " contains " + "'" + value.Substring(2) + "'";
            else if (ValueIsAList(value))
                result += " in '" + value + "'";
            else
                result += op + "'" + value + "'";
            
            return result;
        }
        private string Describe(CHART_ITEM[] arr, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string result = "";
            result = LookingFor(result, "cat", "=", cat);
            result = LookingFor(result, "code", "=", code_list);
            result = LookingFor(result, "desc", " contains ", desc_list);
            result = LookingFor(result, "field", "=", field);
            result = LookingFor(result, "result", " contains ", result_list);

            if (arr.Count() == 0) {
                result += "; not found " + DescribeSearchDepth(search_depth);
            } else {
                var e = arr[0];
                result = "FOUND: " + result + "; found";
                // We might have searched for a pattern or word list in several fields - show what was found
                if (e.CATEGORY != null)     result += " cat='" + e.CATEGORY + "'";
                if (e.CODE != null)         result += " code='" + e.CODE + "'";
                if (e.DESCRIPTION != null)  result += " desc='" + e.DESCRIPTION + "'";
                if (e.FIELD_NAME != null)   result += " field='" + e.FIELD_NAME + "'";
                if (e.RESULT != null)       result += " result='" + e.RESULT + "'";
                // Are there more results?  Just say how many; we aren't going to list them.
                if (arr.Count() == 2) {
                    result += " (1 more result)";
                } else if (arr.Count() > 2) {
                    result += " (" + (arr.Count() - 1) + " more results)";
                }
            }
            
            return result;
        }


        //Set an indicator for this reason (low level)
        private void SetInd(int inum, string reason)
        {
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Set Ind #" + inum + ": " + reason);          //already set - repeat for verbose only
            } else {
                _inds[inum].is_checked = true;
                Program.Audit("Set Ind #" + inum + ": " + reason);
            }
        }

        //Clear an indicator for this reason (low level)
        private void ClrInd(int inum, string reason)
        {
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Clr Ind #" + inum + ": " + reason);          //already clear - repeat verbose only
            } else {
                _inds[inum].is_checked = false;
                Program.Audit("Clr Ind #" + inum + ": " + reason);
            }
        }

        // Count how many items meet the conditions
        // All items are listed
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountItems(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            int count = query.Count();

            // always return what was found
            found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
            // echo the result?
            if (trace) Program.VerboseAudit(found_what);
            
            return count;
        }

        // Count how many items meet the conditions
        // This has the option to list every entry or give a total
        private int CountResultInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what="";
 
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);

            var arr = SplitOnCommaAndPrepareElements(result_list);

            foreach (var item in query) {
                // Figure out which of the search words were found
                var s = item.RESULT.ContainsWhich(arr);
                found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' code='" + item.CODE + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "'";
                if (trace) Program.VerboseAudit(found_what);
                count++;
                if (count_mode != CountMode.CountAll) break;        //stop counting
            }
            
            if (count > 0) {
                //We already printed what was found; maybe add how many?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }
            
            return count;
        }

        // Count how many items contain ALL results in list
        private int CountResultInListAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what = "";

            var arr = SplitOnCommaAndPrepareElements(result_list);

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            for (int i = 0; i <= arr.GetUpperBound(0); i++)
            {
                if (arr[i].StartsWith(NOT_PREFIX))
                {
                    query = query.Where(e => !e.RESULT.Contains(arr[i].Substring(2)));
                }
                else
                {
                    query = query.Where(e => e.RESULT.Contains(arr[i]));
                }
            }

            count = query.Count();

            if (count > 0)
            {
                found_what = "found item with all results in '" + result_list + "' without those prefixed by !!";
                if (trace) Program.VerboseAudit(found_what);
            }
            else
            {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (ValueIsAList(result_list)) {
                return CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what);
            } else {
                return CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what);
            }
        }

        // Is there an item with this result?
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list) 
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth) 
        {
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContains(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what) 
        {
            if (ValueIsAList(result_list)) {
                return (CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            } else {
                return (CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
            }
        }

        // Result contains all in list
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (ValueIsAList(result_list))
            {
                return (CountResultInListAll(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            }
            else
            {
                found_what = "Not valid for non-list results.";
                return false;
            }
        }
        // Count how many items meet the conditions, with none of the results in result_list.
        // This has the option to say how many were found rather than list all.
        private int CountResultNotInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what) 
        {
            int count = 0, rec_count = 0;
            bool found_one;

            found_what = "";
            var arr = SplitOnCommaAndPrepareElements(result_list);         // also deals with CHAR_COMMA
            
            //Do not filter by result here; we need all results
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");
            
            foreach(var item in query) {
                rec_count++;
                //Look for each search word in the result
                found_one = false;
                
                foreach (string s in arr) {
                    //if (String.Equals(item.RESULT, s)) 
                    if (item.RESULT.Contains(s))
                    {
                        found_what = "found '" + s +  "' result='" + item.RESULT + "' -- ignore this";
                        Program.VerboseAudit(found_what);
                        found_one = true;
                        break;
                    }
                }

                if (! found_one) {
                    //none of the words were found - good!
                    found_what = Describe(cat, code_list, desc_list, field, "", search_depth) + " -- does not contain '" + result_list + "'";
                    Program.VerboseAudit(found_what);
                    count++;
                    if (count_mode != CountMode.CountAll) break;
                }
            }
            
            if (count > 0) {
                //We already printed what was found; maybe show the total?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else if (rec_count > 0) {
                //We already printed what we ignored
            } else {
                //Print what did not exist
                found_what = Describe(cat, code_list, desc_list, field, "", search_depth);      //not found
                Program.VerboseAudit(found_what);
            }
            
            return count;
        }

        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, string found_what) 
        {
            // Search in the pull range with trace on
            return CountResultDoesNotContain(cat, code_list, desc_list, field, result_list, out found_what, SearchDepth.SearchDefault, true);
        }
        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, out string found_what, SearchDepth search_depth, bool trace)
        {
            return CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0;
        }

        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            return (CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0);
        }

        // Set the indicator if the conditions are met
        //
        private void SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already set
            if (_inds[inum].is_checked) return;
            
            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what)) {
                SetInd(inum, found_what);                          //echo here - found
            } else {
                Program.VerboseAudit(found_what);                  //and here - not found
            }
        }
        // Special contains all
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContainsAll(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already set
            if (_inds[inum].is_checked) return;

            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                          //echo here - found
            }
            else
            {
                Program.VerboseAudit(found_what);                  //and here - not found
            }
        }
        // Set the indicator if the result does not contain any of the words in result_list
        //
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultDoesNotContain(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {   
            string found_what;
            //avoid more queries if the indicator is already set
            if (_inds[inum].is_checked) return;
            
            //Turn trace off for ResultDoesNotContain() and echo what was set below with SetInd
            if (ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                           //echo here - not found
            } else {
                Program.VerboseAudit(found_what);                   //and here - found
            }
        }

        // Clear the indicator if the result contains one of the words in the result_list
        //
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already clear
            if (! _inds[inum].is_checked) return;
            
            //Turn trace off for ResultContains() and echo what was set below with SetInd
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what)) {
                ClrInd(inum, found_what);                           //echo here - found
            } else {
                Program.VerboseAudit(found_what);                   //and here - not found
            }
        }


        //These slightly smaller functions are meant for places where you aren't really looking for a chart result
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            return (CountResultContains(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
        }

        // Variations with optional parameters (can switch to optional parameters in VS 2010)
        //
        private void SetIndIfFound(int inum, string cat)
        {
            SetIndIfResultContains(inum, cat, "", "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list)
        {
            SetIndIfResultContains(inum, cat, code_list, "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private void ClrIndIfFound(int inum, string cat)
        {
            ClrIndIfResultContains(inum, cat, "","","","", SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }


        // Get the max/total value from a result (usually in the middle of the text)
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetIntValue(get_mode, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            int value;
            int result = 0;
            bool found_one = false;

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            
            //Look for a number in the result
            
            foreach(var item in query) {
                // RESULT might be null so convert to empty string so Split won't blow up
                var arr = PFSUtility.DBToString(item.RESULT).Split(' ');
                foreach (string s in arr) {
                    //Look for 1st character numeric; IsNumeric("60min") will fail so look at 1st char only
                    if (s.Left(1).IsNumeric()) {
                        value = (int)s.Val();                           //Use Val; ToInteger will error on "60min"
                        switch (get_mode) {
                            case GetValueMode.GetMax:
                                result = Math.Max(value, result);       //max
                                break;
                            case GetValueMode.GetTotal:
                                result += value;                        //total
                                break;
                            case GetValueMode.GetLast:
                                result = value;                         //last
                                break;
                        }
                        
                        // print what we are searching for (the first time)
                        if (! found_one) {
                            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
                        }
                        found_one = true;
                        // print each value found
                        Program.VerboseAudit("  found numeric value " + result);
                        //Keep going in case there are more
                    }
                } 
            }
            
            if (! found_one) {
                //show what was not found
                Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
            }
            
            return result;
        }

        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetMaxValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return GetIntValue(GetValueMode.GetMax, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetTotalValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return GetIntValue(GetValueMode.GetTotal, cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Get a result; returns true if found with return_result
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result)
        {
            return GetResult(cat, code_list, desc_list, field, out return_result, SearchDepth.SearchDefault);
        }
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0) {
                return_result = PFSUtility.DBToString(query.First().RESULT);
            } else {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }

        private void GetResultforOther(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "Other (comment)");
            return_result="";
            foreach (var item in query)
            {
                if (!String.Equals(item.RESULT, "Other (comment)"))
                {
                    return_result = item.RESULT;
                    //Program.VerboseAudit(found_what);
                    break;
                }

            }
            return;
        }

        //======================================================================================
        // Mapping starts here
        //======================================================================================

        private void Check_1_2_3()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 1. ADL Self");
            Program.VerboseAudit("MH 2. ADL Partial");
            Program.VerboseAudit("MH 3. ADL Extended");
            Program.VerboseAudit("---------------");
            
            SetIndIfResultContains(3, "", "GROOMING", "", "", "Complete");
            SetIndIfResultContains(3, "", "MHTOILET", "", "", "Complete");
            SetIndIfResultContains(2, "", "GROOMING", "", "", "Assisted");
            SetIndIfResultContains(2, "", "MHTOILET", "", "", "Assisted");
            SetIndIfResultContains(1, "", "GROOMING", "", "", "Independent");
            SetIndIfResultContains(1, "", "MHTOILET", "", "", "Independent");
            if (!_inds[1].is_checked && !_inds[2].is_checked && !_inds[3].is_checked)
                SetInd(1,"Defaulting to ADL Self");

        }


        private void Check_4()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 4. ADL Supervision");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(4, "", "MHADL", "", "", "");
        
        }


        private void Check_5_6()
        {    
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 5. Cognitive Support");
            Program.VerboseAudit("MH 6. Cognitive Support q 1 hr");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(6, "", "MHCOG", "", "", "q1h");
            SetIndIfResultContains(5, "", "MHCOG", "", "", "");
        }

        private void Check_7_8()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 7. Safety Mgt q 15 min");
            Program.VerboseAudit("MH 8. Safety Mgt q  5 min");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(8, "", "OBSSTATUS", "", "", "Constant observation,Suicide");
            //SetIndIfResultContains(8, "", "SECLUDE", "", "", "");

            SetIndIfResultContains(7, "", "OBSSTATUS", "", "", "q15");
            SetIndIfResultContains(7, "", "RESTRAINTS", "", "", "Violent (4pt or 5pt)");

            if (!_inds[7].is_checked && !_inds[8].is_checked)
            {
                if (StandardOrderIsActive("NUR0195"))
                    SetInd(7, "NUR0195 is active within this time range");
                if (StandardOrderIsActive("NUR0415"))
                    SetInd(8, "NUR0415 is active within this time range");
                //if (StandardOrderIsActive("NUR0421"))
                //    SetInd(8, "NUR0421 is active within this time range");

            }
            
        }

        private void Check_9_10_11_12_13()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH  9. Behavior/Emotional Mgt");
            Program.VerboseAudit("MH 10. Behavior/Emotional Mgt q 4 hr");
            Program.VerboseAudit("MH 11. Behavior/Emotional Mgt q 2 hr");
            Program.VerboseAudit("MH 12. Behavior/Emotional Mgt q 1 hr");
            Program.VerboseAudit("MH 13. Behavior/Emotional Mgt q 30 min");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(9, "", "MHINTERV", "", "", "");
            SetIndIfResultContains(10, "", "MHINTERV", "", "", "q4h");
            SetIndIfResultContains(11, "", "MHINTERV", "", "", "q2h");
            SetIndIfResultContains(12, "", "MHINTERV", "", "", "q1h");
            SetIndIfResultContains(13, "", "MHINTERV", "", "", "q30");
            SetIndIfResultContains(13, "", "PHYSAGRES", "", "", "");
            SetIndIfResultContains(13, "", "VERBTHREAT", "", "", "");
            SetIndIfResultContains(13, "", "RESTRAINTS", "", "", "Violent (4pt or 5pt)");
            if (StandardOrderIsActive("NUR0415"))
                SetInd(13, "NUR0415 is active within this time range");
        }

        private void Check_14_15()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 14. Medication Mgt q 4 hr");
            Program.VerboseAudit("MH 15. Medication Mgt q 2 hr");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(14, "", "E2MEDFREQ", "", "", "q4h");
            SetIndIfResultContains(15, "", "E2MEDFREQ", "", "", "q1h,q2h");
    
        }

        // (this isn't really a Q1h count -- it is just a count) Make this a q1hr count
        private int ReturnQ1HrCount(string code_list, string result_list)
        {
            var buckets = new List<int>();
            SetBucketSize(60);
            AddBuckets(buckets, "", code_list, "", "", result_list);
            return CountBuckets(buckets);
        }

        private bool IsQ1Hour(int count)
        {
            return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q1H);
        }

        // Use this if counting safety buckets
        //private void CheckSafety(int count, string desc)
        //{
        //    if (_inds[13].is_checked) return;             //skip if highest already checked
        //    if (count == 0) return;
        //    switch (FreqForCount(_pat.los_hours, count))
        //    {
        //    case Frequencies.Q30M:
        //        SetInd(13, desc + " q30min");
        //        break;
        //    case Frequencies.Q1H:
        //    case Frequencies.Q2H:
        //        SetInd(12, desc + " q2h");
        //        break;
        //    default:
        //        Program.VerboseAudit(desc + ": " + count + " in " + Math.Round(_pat.los_hours) + " hours is not enough");
        //        break;
        //    }
        //}


        private void CheckFallsScore(string code, int minval)
        {
            int result = 0;
            int value = 0;

            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = AndItemFilter(query, "", code, "", "", "");
            query = query.Where(e => e.EVENT_DATETIME >= Program.g_pull_finish.AddHours(-24));
            query = query.Where(e => e.EVENT_DATETIME <= Program.g_pull_finish);
            
            //Look for a number in the result

            foreach (var item in query)
            {
                // RESULT might be null so convert to empty string so Split won't blow up
                var arr = PFSUtility.DBToString(item.RESULT).Split(' ');
                foreach (string s in arr)
                {
                    //Look for 1st character numeric; IsNumeric("60min") will fail so look at 1st char only
                    if (s.Left(1).IsNumeric())
                    {
                        value = (int)s.Val();                           //Use Val; ToInteger will error on "60min"
                        result = Math.Max(value, result);       //max
                    }
                }
            }
            if (result > minval)
            {
                SetInd(12, code + " Fall Risk Score = " + result);
            }
        }

        private void Check_14()
        {    
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("14. Isolation");
            Program.VerboseAudit("---------------");

            //SetIndIfResultContains(14, "", "ISOPRE", "", "", "",SearchDepth.SearchSinceAdmission);
            //SetIndIfResultContains(14, "", "NUR0416", "", "", "", SearchDepth.SearchSinceAdmission);
            if (StandardOrderIsActive("ISOPRE"))
                SetInd(14, "ISOPRE is active within this time range");
            if (StandardOrderIsActive("NUR0416"))
                SetInd(14, "NUR0416 is active within this time range");
            if (StandardOrderIsActive("NUR0703"))
                SetInd(14, "NUR0703 is active within this time range");
        }

        private void CheckAssessment(int count, string desc)
        {
            if (_inds[18].is_checked) return;          //skip if highest already checked
            if (count == 0) return;                    //skip if none
            
            // This should work the same as the original code:
            switch (FreqForCount(_pat.los_hours, count)) {
                case Frequencies.Q1H:
                    SetInd(18, desc);
                    break;
                case Frequencies.Q2H:
                    SetInd(17, desc);
                    break;
                case Frequencies.Q4H:
                    SetInd(16, desc);
                    break;
                default:
                    break;
            }

        }

        private void Check_16_17_18()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 16. Physio Assessment q4h");
            Program.VerboseAudit("MH 17. Physio Assessment q2h");
            Program.VerboseAudit("MH 18. Physio Assessment q1h");
            Program.VerboseAudit("---------------");

//Grouping:
//Pulmonary - RESPIRATIONS, O2SAT, O2SAT2
//Cardiovascular - PULSE, BP1
//Neurological - COLDTAN & COLTMAN            

            CountAssessments(30);               // always do q30 buckets--but use different freq tables for icu vs non-icu.

        }


        private void CountAssessments(int bucket_size)
        {
            int ct;
            string codelist;
            List<int> buckets;

            SetBucketSize(bucket_size);

            buckets = new List<int>();
            codelist = "RESPIRATIONS,O2SAT,O2SAT2";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Pulmonary=" + ct);
            if (_inds[18].is_checked) return;

            buckets = new List<int>();
            codelist = "PULSE,BP1";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Cardio=" + ct);
            if (_inds[18].is_checked) return;

            buckets = new List<int>();
            codelist = "COLDTAN,COLTMAN";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Neuro=" + ct);
            if (_inds[18].is_checked) return;

        }

        //=====================================================================
        // Group like assessments into buckets of time
        // Each bucket counts as one assessment in the frequency count

        private void SetBucketSize(int minutes)
        {
            _bucket_size = minutes;
        }

        //Add to the list of bucket numbers (redundant buckets are fine)
        private void AddBuckets(List<int> bucket_list, string cat, string code_list, string desc, string field)
        {
            AddBuckets(bucket_list, cat, code_list, desc, field, "");
        }
        private void AddBuckets(List<int> bucket_list, string cat, string code_list, string desc, string field, string result_list)
        {
            // Assign (virtual) bucket numbers to each assessment.  
            // Every x minutes after that is a new bucket.  Bucket zero = pull_start time.

            // get the chart items for the assessments
            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = AndItemFilter(query, cat, code_list, desc, field, result_list);
            query = query.Where(e => e.EVENT_DATETIME >= _pat.pull_start && e.EVENT_DATETIME <= _pat.pull_finish);

            // This step is needed for those who want to count the # within a bucket, like Shands #24.
            // This will have no effect on those that count buckets.
            var query2 = (from item in query select new { item.EVENT_DATETIME }).Distinct();

            // figure out what buckets the events belong to
            var query3 = from item in query2
                         select new {
                            bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size)
                         };
            // Add to the list
            foreach (var item in query3) {
                bucket_list.Add(item.bucket);
            }

            // This is just for auditing: Say what we are looking for and how many were found
            if (!String.IsNullOrEmpty(result_list)) {
                // print each word and if it was found or not
                int i = CountResultContains(cat, code_list, desc, field, result_list);
            } else {
                // print how many were found
                Program.VerboseAudit(Describe(cat, code_list, desc, field, ""));
            }
        }
        private void AddOrderBuckets(List<int> bucket_list, string order_list)
        {
            // Assign (virtual) bucket numbers to each assessment.  
            // Every x minutes after that is a new bucket.  Bucket zero = pull_start time.

            // get the chart items for the assessments
            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = AndItemFilter(query, "", order_list, "", "", "");
            query = query.Where(e => e.ORDER_STATUS.Contains("AC") || e.ORDER_STATUS.Contains("IP"));
            query = query.Where(e => e.EVENT_DATETIME >= _pat.pull_start && e.EVENT_DATETIME <= _pat.pull_finish);

            //get the list of order_ids that are not DCed
            var order_id_list = new List<string>();
            foreach (var c in query)
            {
                //look at this order_id.  Is there one with DC?
                var query_dc = StartNewQuery(SearchDepth.SearchDefault);
                query_dc = query_dc.Where(e => e.EVENT_DATETIME >= _pat.pull_start && e.EVENT_DATETIME <= _pat.pull_finish);
                query_dc = AndItemFilter(query_dc, "", order_list, "", "", "");
                query_dc = query_dc.Where(e => e.ORDER_ID == c.ORDER_ID);
                query_dc = query_dc.Where(e => e.ORDER_STATUS.Contains("DC"));
                int ctdc = query_dc.Count();
                if (ctdc == 0)
                {  // no dc exists, add this to the list
                    order_id_list.Add(c.ORDER_ID);
                    Program.VerboseAudit("Adding orderid=" + c.ORDER_ID);
                }
            }

            var query_nw = StartNewQuery(SearchDepth.SearchDefault);
            query_nw = query_nw.Where(e => e.EVENT_DATETIME >= _pat.pull_start && e.EVENT_DATETIME <= _pat.pull_finish);
            query_nw = AndItemFilter(query_nw, "", order_list, "", "", "");
            query_nw = query_nw.Where(e => e.ORDER_STATUS.Contains("AC") || e.ORDER_STATUS.Contains("IP"));
            query_nw = query_nw.Where(e => order_id_list.Contains(e.ORDER_ID));
            Program.VerboseAudit("Count of orders=" + query_nw.Count().ToString());

            // This step is needed for those who want to count the # within a bucket, like Shands #24.
            // This will have no effect on those that count buckets.
            var query2 = (from item in query_nw select new { item.EVENT_DATETIME }).Distinct();

            // figure out what buckets the events belong to
            Program.VerboseAudit("Bucket size=" + _bucket_size.ToString());
            var query3 = from item in query2
                         select new
                         {
                             bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size)
                         };
            // Add to the list
            foreach (var item in query3)
            {
                Program.VerboseAudit("Adding this bucket=" + item.bucket.ToString());
                bucket_list.Add(item.bucket);
            }

                // print how many were found`
                Program.VerboseAudit(Describe("", order_list, "", "", ""));
        }

        private int CountBuckets(List<int> bucket_list)       // bucket list: (ha ha)
        {
            int result = bucket_list.Distinct().Count();
            if (result > 0) Program.VerboseAudit(result + " unique");
            return result;
        }


        private void Check_19()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 19. Fluid Management");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(19, "", "NUR0057", "", "", "");

        }

        private void Check_20()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 20. Wound/Injury Management");
            Program.VerboseAudit("---------------");

            string codelist = "PRSULDDT,DRSCOND,WNDDT";
            SetIndIfResultContains(20, "", codelist, "", "", "");
        }

        private void Check_21()
        {
            string codelist;
            
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 21. Patient/Family Education >= 1Hr by RN");
            Program.VerboseAudit("---------------");

            codelist = "MEDED,INFECT,WRITWARF,OTHACOAG,INJECTANTI,PAINMGT";
            SetIndIfResultContains(21, "", codelist, "", "", "");

        }

        private void Check_22()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 22. Coordination of Care >= 1Hr by RN");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(22, "", "MHCCARE", "", "", "");

        }

        




        private void Check_UD()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("UD. User-defined indicators");
            //Program.VerboseAudit("---------------");

        }

        private bool StandardOrderIsActive(string ocode)
        {
            bool result = false;
            bool mid_result;
            var query_nw = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query_nw = AndItemFilter(query_nw, "", ocode, "", "", "");
            query_nw = query_nw.Where(e => e.ORDER_STATUS.Contains("AC") || e.ORDER_STATUS.Contains("IP"));
            query_nw = query_nw.Where(e => e.EVENT_DATETIME <= Program.g_pull_finish);
            if (query_nw.Count() > 0)  //these are the orders that have been started since admission
            {
                foreach (var c in query_nw)
                {
                    mid_result = true;
                    //look at this order_id.  Is there one with DC?  
                    var query_dc = StartNewQuery(SearchDepth.SearchDefault);
                    query_dc = AndItemFilter(query_dc, "", ocode, "", "", "");
                    query_dc = query_dc.Where(e => e.ORDER_ID == c.ORDER_ID);
                    query_dc = query_dc.Where(e => e.ORDER_STATUS.Contains("DC"));
                    query_dc = query_dc.Where(e => e.EVENT_DATETIME <= Program.g_pull_start); 
                    int ctdc = query_dc.Count();
                    if (ctdc > 0)
                    {  // dc exists before this pull.  dont triger indicator
                        mid_result = false;
                    }
                    result = result || mid_result;
                } //end foreach
            }
            return result;
        }

//        private void CheckOrder(string ocode, out int freq_out)
//        {
//            DateTime evdt1 = DateTime.MinValue;
//            DateTime evdt2 = DateTime.MinValue;
//            int count = 0;
//            int high_freq = -1;

//            //order_id 7820510
//            //order_control  NW SC
//            //order_status  AC  DC
//            //order_timing  Q4H DAILY
//            //result  NUR0550
//// Q15 MIN DAILY, Q30 MIN DAILY, Q1H DAILY, Q2H DAILY, Q4H DAILY, Q6H DAILY, Q8H DAILY, 2XDAY DAILY, 3XDAY DAILY, CONTINUOUS_ DAILY
//            //find latest time of attnd_safety since admission that is not discontinued
//            // order_control=X means discontinue already processed
//            var query_nw = StartNewQuery(SearchDepth.SearchDefault);
//            query_nw = AndItemFilter(query_nw, "", ocode, "", "", "");
//            query_nw = query_nw.Where(e => e.ORDER_STATUS.Contains("AC") || e.ORDER_STATUS.Contains("IP"));

//            //get the list of order_id to use
//            var order_id_list = new List<string>();
//            foreach (var c in query_nw)
//            {
//                //look at this order_id.  Is there one with DC?
//                var query_dc = StartNewQuery(SearchDepth.SearchDefault);
//                query_dc = AndItemFilter(query_dc, "", ocode, "", "", "");
//                query_dc = query_dc.Where(e => e.ORDER_ID == c.ORDER_ID); 
//                query_dc = query_dc.Where(e => e.ORDER_STATUS.Contains("DC"));
//                int ctdc = query_dc.Count();
//                if (ctdc == 0) 
//                {  // no dc exists, add this to the list
//                    order_id_list.Add(c.ORDER_ID);
//                }
                
//            }

//            query_nw = StartNewQuery(SearchDepth.SearchDefault);
//            query_nw = AndItemFilter(query_nw, "", ocode, "", "", "");
//            query_nw = query_nw.Where(e => e.ORDER_STATUS.Contains("AC") || e.ORDER_STATUS.Contains("IP"));
//            query_nw = query_nw.Where(e => order_id_list.Contains(e.ORDER_ID));

//            var q15 = query_nw.Where(e => e.ORDER_TIMING.Contains("Q15 MIN DAILY"));
//            var q30 = query_nw.Where(e => e.ORDER_TIMING.Contains("Q30 MIN DAILY"));
//            var q1  = query_nw.Where(e => e.ORDER_TIMING.Contains("Q1H DAILY"));
//            var q2  = query_nw.Where(e => e.ORDER_TIMING.Contains("Q2H DAILY"));
//            var q4  = query_nw.Where(e => e.ORDER_TIMING.Contains("Q4H DAILY"));
//            int ct15 = q15.Count();
//            int ct30 = q30.Count();
//            int ct1 = q1.Count();
//            int ct2 = q2.Count();
//            int ct4 = q4.Count();

//            double[] dur = new double[5];
//            dur[4] = ct15 * 0.25;
//            dur[3] = ct30 * 0.5;
//            dur[2] = ct1;
//            dur[1] = ct2 * 2;
//            dur[0] = ct4 * 4;

//            string[] freq = new string[5];
//            freq[4] = "Q15 MIN DAILY";
//            freq[3] = "Q30 MIN DAILY";
//            freq[2] = "Q1H DAILY";
//            freq[1] = "Q2H DAILY";
//            freq[0] = "Q4H DAILY";

//            double max_dur = dur.Max();
//            int max_idx = dur.ToList().IndexOf(dur.Max());

//            if (max_dur > 0)
//                Program.VerboseAudit(ocode + " was found with a frequency of " + freq[max_idx] + " and duration of " + max_dur + " hours");

//            if (max_dur >= 2)
//            {
//                if (max_idx == 4)  //q15 should be limited to q30
//                    freq_out = 4;
//                else
//                    freq_out = max_idx + 1;
//            }
//            else
//                freq_out = -1;
               
//            //if (query_nw.Count() > 0)
//            //{
//            //    foreach (var item_nw in query_nw)
//            //    {
//            //        var query_dc = StartNewQuery(SearchDepth.SearchSinceAdmission);
//            //        query_dc = AndItemFilter(query_dc, "", ocode, "", "", "");
//            //        query_nw = query_nw.Where(e => !e.ORDER_CONTROL.Contains("X"));
//            //        query_dc = query_dc.Where(e => e.ORDER_ID == item_nw.ORDER_ID);
//            //        query_dc = query_dc.Where(e => e.ORDER_STATUS.Contains("DC") || e.ORDER_STATUS.Contains("CA") || e.ORDER_STATUS.Contains("CM")); //DC CA CM
//            //        count = query_dc.Count();
//            //        if (count > 0)
//            //        {
//            //            foreach (var item_dc in query_dc)
//            //            {
//            //                Program.VerboseAudit(ocode + " found as " + item_nw.ORDER_STATUS + " on " + item_nw.EVENT_DATETIME.ToString() + " and " + item_dc.ORDER_STATUS + " on " + item_dc.EVENT_DATETIME.ToString());
//            //                UpdateDBOrder(item_nw.ORDER_ID);
//            //            }
//            //        }
//            //        else
//            //        {
//            //            Program.VerboseAudit(ocode + " found as " + item_nw.ORDER_STATUS + " on " + item_nw.EVENT_DATETIME.ToString() + " with Freq = " + item_nw.ORDER_TIMING);
//            //            if (item_nw.ORDER_TIMING == "Q15 MIN DAILY" || item_nw.ORDER_TIMING == "Q30 MIN DAILY")
//            //            {
//            //                if (high_freq < 4) high_freq = 4;
//            //            }
//            //            else if (item_nw.ORDER_TIMING == "Q1H DAILY")
//            //            {
//            //                if (high_freq < 3) high_freq = 3;
//            //            }
//            //            else if (item_nw.ORDER_TIMING == "Q2H DAILY")
//            //            {
//            //                if (high_freq < 2) high_freq = 2;
//            //            }
//            //            else if (item_nw.ORDER_TIMING == "Q4H DAILY")
//            //            {
//            //                if (high_freq < 1) high_freq = 1;
//            //            }
//            //            else
//            //            {
//            //                if (high_freq < 0) high_freq = 0;
//            //            }

//            //        }
//            //    } //foreach
//            //}
//            //freq = high_freq;
//        }




        private void AtLeastOneADL()
        {
            Program.Audit("---------------");
            if (!(_inds[1].is_checked || _inds[2].is_checked || _inds[3].is_checked || _inds[4].is_checked)) {
                // Make "#2 ADL - Assist" the default.  (90% of patients)
                SetInd(2, "Defaulting to partial care due to lack of documentation.");
            }
        }

        private void HighestIndicatorInEachGroupWins()
        {
            int i, g;
            bool highest_is_on;
            string ind_list;

            Program.Audit("Select highest indicator in each group");
            
            g = 0;
            highest_is_on = false;
            // Go from bottom up - highest (lowest) indicator in each group wins
            for (i = MAX_INDS; (i >= 1); i--) {
                if (_inds[i].radio_group > 0) {
                    if (_inds[i].radio_group != g) {
                        //this is a new group
                        g = _inds[i].radio_group;
                        highest_is_on = _inds[i].is_checked;
                    } else {
                        //same group
                        if (highest_is_on) {
                            _inds[i].is_checked = false;             //uncheck a lower number
                        } else {
                            highest_is_on = _inds[i].is_checked;     //save this one
                        }
                    }
                }
            } // next i
            
            //Echo the indicators for an Audit (no classification will be saved)
            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++) {
                if (_inds[i].is_checked) ind_list += "," + i;
            } // next i

            Program.Audit("Final list = " + ind_list.Substring(1));
            Program.Audit("---------------");
        }


        private void CheckProcs()
        {
            //CheckProc_1();
            CheckProc_2();
            //CheckProc_3();
            //CheckProc_4();
            //CheckProc_5();
            //CheckProc_6();
            //CheckProc_7();
            //CheckProc_9();
            //CheckProc_10();
            //CheckProc_11();
        }

        private void CheckProc_1()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P1. 1-1 safety observation by RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_2()
        {
            DateTime start_dt;
            DateTime end_dt;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P2. 1-1 safety observation by non-RN");
            Program.VerboseAudit("---------------");

            var query_nw = StartNewQuery(SearchDepth.SearchDefault);
            query_nw = AndItemFilter(query_nw, "", "BEDCOMP", "", "", "");

            foreach (var item_nw in query_nw)
            {
                int hrs = (int)item_nw.RESULT.Val();
                if (hrs >= 6)
                {
                    SetInd(13, "BEDCOMP hours=" + hrs);
                }
                if (item_nw.EVENT_DATETIME >= _pat.pull_start && item_nw.EVENT_DATETIME < _pat.pull_finish)
                    start_dt = _pat.pull_start;
                else
                    start_dt = _pat.pull_finish;
                start_dt = item_nw.EVENT_DATETIME;
                end_dt = start_dt.AddHours(hrs);
                Program.VerboseAudit("Found BEDCOMP time start=" + item_nw.EVENT_DATETIME.ToString() + " hours=" + hrs + " Activity start=" + start_dt.ToString());
                MaybeAddSitter(start_dt,end_dt);
            }

        }

        private void MaybeAddSitter(DateTime startdt, DateTime enddt)
        {
                if (ProcExists(2, startdt, enddt)) {
                    Program.Audit("Procedure 2: already exists");
                } else {
                    var proc = new proc_data();
                    proc.procedure_number = 2;
                    proc.start = startdt;
                    proc.finish = enddt;
                    _procs.Add(proc);
                }
        }

        private bool ProcExists(int pnum, DateTime startdt, DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && (proc.PROCEDURE_DATETIME == startdt)
                            && (proc.DEPARTURE_DATETIME == enddt)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new {proc.PROCEDURE_EVENT_ID};
            return (query.Count() > 0);
        }


        private void CheckProc_3()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P3. Off unit accompanied by RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_4()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P4. Off unit accompanied by non-RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_5()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P5. Patient/family education by RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_6()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P6. Extensive wound management by RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_7()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P7. Extensive wound management by non-RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_8()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P8. Coordination of care by RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_9()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P9 1:1 RN at bedside");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_10()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P10. 1:1 non-RN at bedside");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_11()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P11. 2:1 by RN at bedside");
            //Program.VerboseAudit("---------------");
        }

        
        private const string DATETIME_FORMAT = "yyyyMMddHHmm";              // ISO Date/Time w/o seconds

        private void OutputClass()
        {
            string outstr, ind_list, desc, str_pull_dt;
            int i, tc_event_id;

            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         212 242 304
            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1
            //1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901 3456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
            //1       |10 5            |                |                |        |10023121779         |ALEXANDER                       |GLORIA                          |                                |0508    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289530  |           |201403030400                                                                      |NNYNNNNYYNNYNNNYNNNYYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//1       |10 5            |                |                |        |10024544908         |ARMSTRONG                       |AARON                           |                                |0535    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289531  |           |201403030400                                                                      |NYNNYNNNNNNYNNNYNNNNYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//|0547    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289532  |           |201403030400                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//|0547    |A   |201403030700|                |201403030700    |18  |C|    |3040|1440|18289532  |           |201403030700                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN

            if (Program.g_is_test)
                tc_event_id = 9999;
            else
                tc_event_id = PFSUtility.NextGID();                         //get a unique id for this class
            str_pull_dt = _pat.pull_finish.ToString(DATETIME_FORMAT);

            outstr = _pat.facilty_code.FixedWidth(8);                       //(facility code)
            outstr += "|" + _pat.unit_name.FixedWidth(16);
            outstr += "|" + "".FixedWidth(16);                               //(unit code)
            outstr += "|" + "".FixedWidth(16);                               //(area code)
            outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
            outstr += "|" + _pat.acct.FixedWidth(20);
            outstr += "|" + _pat.last_name.FixedWidth(32);
            outstr += "|" + _pat.first_name.FixedWidth(32);
            outstr += "|" + _pat.middle_name.FixedWidth(32);
            outstr += "|" + _pat.room.FixedWidth(8);
            outstr += "|" + _pat.bed.FixedWidth(4);
          outstr += "|" + str_pull_dt.FixedWidth(12);                      //class datetime (could change)
            outstr += "|" + "".FixedWidth(16);                               //(login)
          outstr += "|" + str_pull_dt.FixedWidth(16);                      //(employee)/(pull datetime)
            outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
            outstr += "|" + "C".FixedWidth(1);                               //record type = class
            outstr += "|" + "".FixedWidth(4);                                //(stage)
            outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
            outstr += "|" + _pat.range.ToString().FixedWidth(4);             //TC pull range
            outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
            outstr += "|";
            outstr = outstr.FixedWidth(294);
          outstr += "|" + _pat.effective.ToString(DATETIME_FORMAT);        //IN
            outstr = outstr.FixedWidth(377);
            outstr += "|";
            
            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++) {
                if (_inds[i].is_checked) {
                    outstr += "Y";
                    ind_list += "," + i;
                } else {
                    outstr += "N";
                }
            } // next i
            ind_list = ind_list.Substring(1);                           //strip leading comma
  
            Program.outfile.WriteLine(outstr);                          //output to transparent.txt

            Program.Audit("");
            desc = "Classified: " + ind_list;
            if (Program.g_is_test) {
                Program.Audit(desc);
            } else {
                //Save the selected indicators plus both Program.Audits in the event log; link with tc_event_id
                PFSEventLog.AddTransparentMappingEventLogEntry(
                    desc, Program.gLogUnitID, Program.gLogEncounterID,
                    tc_event_id, Program.gLogMapperVersion,
                    Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
            }
        }

        private void OutputProcs()
        {
            int i;
            string outstr, proc_list, desc;
            int tc_event_id;

            foreach(var proc in _procs) {
                if (Program.g_is_test)
                    tc_event_id = 9999;
                else
                    tc_event_id = PFSUtility.NextGID();                         //get a unique id for this proc

                outstr = _pat.facilty_code.FixedWidth(8);
                outstr += "|" + _pat.unit_name;                                 //10
                outstr = outstr.FixedWidth(68);
                outstr += "|" + _pat.acct.FixedWidth(20);                       //90
                outstr += "|" + _pat.last_name.FixedWidth(32);
                outstr += "|" + _pat.first_name.FixedWidth(32);
                outstr += "|" + _pat.middle_name.FixedWidth(32);
                outstr = outstr.FixedWidth(202);
                outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //204 proc dt
                outstr = outstr.FixedWidth(254);
                outstr += "|P";                                                 //256 procedure type record
                outstr += "|" + "".FixedWidth(4);                               //(stage)
                outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);     //TC source ID
                outstr += "|" + _pat.range.ToString().FixedWidth(4);            //TC pull range
                outstr += "|" + tc_event_id.ToString().FixedWidth(10);          //TC event ID
                outstr += "|";
                outstr = outstr.FixedWidth(294);
                outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //296 procdt in
                outstr = outstr.FixedWidth(346);
                outstr += "|" + proc.finish.ToString(DATETIME_FORMAT);          //348 procdt out
                outstr = outstr.FixedWidth(377);
                outstr += "|";
                
                proc_list = "";
                for (i = 1; (i < MAX_PROCS); i++) {
                    if (proc.procedure_number == i) {
                        outstr += "Y";
                        proc_list += "," + i;
                    } else {
                        outstr += "N";
                    }
                } // next i
                proc_list = proc_list.Substring(1);                             //strip leading comma

                Program.outfile.WriteLine(outstr);                              //output to transparent.txt

                desc = "Procedures: " + proc_list;
                if (Program.g_is_test) {
                    Program.Audit(desc);
                } else {
                    //Save the selected procedures plus both Program.Audits in the event log; link with tc_event_id
                    //(this assumes the procedure Program.Audit is mixed in with the class Program.Audit)
                    PFSEventLog.AddTransparentMappingEventLogEntry(desc, Program.gLogUnitID, Program.gLogEncounterID,
                        tc_event_id, Program.gLogMapperVersion,
                        Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
                }
            } // next proc
        }


    }
}
