Attribute VB_Name = "RoperHistorical"
Option Explicit
'The location of the input is determined by the first line of the dictionary.
'
' This is the main module for SiePFS
' File is formatted as:
'    <Header>
'       <Event>
'       <Event>
'         ...
'

Const TRANSP_FILENAME = "Transparent.txt"
Const TRANSP_INLOG_FILE_LIFE = 10 'days
Const TRANSP_OUTLOG_FILE_LIFE = 10 'days
Const TRANSP_DEBUG_FILE_LIFE = 10 'days
Const MSDIC_FNAME = "MSDICT.DAT"
Const MHDIC_FNAME = "MHDICT.DAT"
Const DNLD_FNAME = "TCQUERY"
Const VALIDMS_FNAME = "VALIDMS.UNT"
Const VALIDMH_FNAME = "VALIDMH.UNT"
Const PERDATA_FNAME = "PERSIST.DAT"

'HEADER RECORD
Const MAX_INDICATORS = 50

Const START_ACCT_NUM = 1
Const LEN_ACCT_NUM = 20

Const START_PTNAME = 21
Const LEN_PTNAME = 30

Const START_UNIT = 51
Const LEN_UNIT = 10

Const START_RM = 61
Const LEN_RM = 10

Const START_BED = 71
Const LEN_BED = 5

Const START_FACILITY = 76
Const LEN_FAC = 5

'Const START_TX = 56
'Const LEN_TX = 16

Const START_CLASS_DT = 81
Const LEN_DT = 14



'EVENT RECORD
Const START_EVENT_DT = 5
'Const LEN_DT = 14

Const START_EVENT = 20
Const LEN_EVENT = 30

Const START_EVENT_DSC = 50
Const LEN_EVENT_DSC = 25

Const START_FREQ = 75
Const LEN_FREQ = 10

Const START_RESULT = 116
Const LEN_RESULT = 75

Const MAX_RANGE = 1440  '24 hrs in minutes; this is overridden by the -range arg.

Const RANGE_FACTOR = 24 * 60   'For proportioning the count of Med/Pulm/Cardio

Const MAX_UNIQUE_CHART_TIMES = 20

Dim infn As String
Dim fnames() As String
Dim outfn As String
Dim csvfile As Integer
Dim csvname As String
Dim dbugfile As Integer
Dim dbugname As String

Private Type indicator
  indwinpfs As Integer  'winpfs indic number associated with this cerner event
  eventid As String     'Event_CD of this indicator
  chartkey As String    'look for this key in charted result
  gavefreq As Boolean   'file supplied the frequency; ignore subsequent freqs
  check_freq As Boolean 'flag to check frequency
  freq_basis As Integer 'frequency required for this indicator (in minutes)
  act_freq As Integer   'calculated frequency from data (in minutes)
  last_datetime As String 'last date time of event found in this patient
  num_found As Integer  'number of times this was found in this patient
  also_mark As String   'if this indicator is marked, then also mark these.
  'charttime(MAX_UNIQUE_CHART_TIMES) As String       commented out and in code to save mem space 9/26/07
  one_row_find As Single
  persist As Boolean  'flag if this charting element persists
  perlen As Single    'for how long does it persist? in minutes
  icupersist As Boolean 'persist for regardless of icu, represented by & in dictionary
End Type

Private Type indicator_data
    checked As Boolean
    also_mark As String
End Type

Private Type event_wildcharting
    eventid As String 'event id
    count As Single
End Type

Private Type ProcessedInfo
    a As String     'acctnum
    already_did_persist As Boolean
End Type

Dim dicary() As indicator
Dim mhdicary() As indicator
Dim dicnum As Integer
Dim mhdicnum As Integer
Dim inDirPath As String  'path of input file
Dim winpfspath As String 'path of winpfs
Dim winlogpath As String 'path of winpfs\log
Dim winloadpath As String ' path of winpfs\load_me

Dim datafile As Integer
Dim outfile As Integer

Dim inds(MAX_INDICATORS) As indicator_data
Dim grps(MAX_INDICATORS) As Integer
Dim mhinds(MAX_INDICATORS) As indicator_data
Dim mhgrps(MAX_INDICATORS) As Integer

Dim lastname As String
Dim firstname As String
Dim unitname As String
Dim roomname As String
Dim bedname As String
Dim acctnum As String
Dim classdt As String
Dim classdate As String
Dim classtime As String
Dim nowdt As Date
Dim intime As String
Dim saveintime As String

Dim range As Single  'number of minutes in the scope of time for freq.
Dim dbugon As Boolean
Dim suppressDbugLog As Boolean
Dim effdateon As Boolean
Dim effdate As String
Dim efftimeon As Boolean
Dim efftime As String
Dim pulltimeon As Boolean
Dim pulltime As String
Dim pulldateon As Boolean
Dim pulldate As String

Dim event6931 As Boolean
Dim event33474 As Boolean
Dim event16966 As Boolean
Dim event21430 As Boolean
Dim count18098 As Single
Dim event33765 As Boolean
Dim val33775 As Single
Dim event33775 As Boolean
Dim dt33775 As String
Dim val35286 As Single
Dim dt35286 As String
Dim event35286 As Boolean
Dim count5805 As Single
Dim count5806 As Single
Dim countwild(20) As event_wildcharting

Dim MSunitary() As String
Dim MHunitary() As String
Dim unitnum As Integer
Dim mhunitnum As Integer

Dim acctnumdirectory() As ProcessedInfo
Dim numacct As Single
Dim telemetry As Boolean

Dim acct As String
Dim unit As String
Dim name As String
Dim room As String

Dim x90117 As Boolean
Dim ps As Integer
Dim pe As Integer
Dim s As String
Dim e As String
    Dim cmdLine As String


Sub Main()
    Const RANGE_PARAM = "-range="
    Const EFFECTIVE_PARAM = "-efftime="
    Const DBUG_ON = "-debug"
    Const ALTER_DATE = "-effdate="
    Const PULL_TIME = "-pulltime="
    Const PULL_DATE = "-pulldate="
    Dim n As Integer
    Dim i As Integer
    Dim epos As Integer
    Dim rpos As Integer
    Dim effective As String
    Dim h As String
    Dim t As Date
    Dim adpos As Integer
    
    
    '-efftime=hhmm  This is the time at which the classification is effective.
    '                 The date associated with this time is taken from header classdate.
    '                 If not specified, then -efftime is assumed to be the classtime
    '                 from the header.
    '-effdate=yyyymmdd   This is the date which, in combination with the
    '                 -effective time is used to specify the effective datetime.
    '                 If not specified, then -effdate is assumed to be the classdate
    '                 from the header.
    'Special value:  -effdate=yesterday means Now's yesterday.
        
    '-pulltime=hhmm  This is the time starting from which the pull is to
    '                look backwards from.
    '                If not specified, then this time is assumed to be the -effective time.
    '-pulldate=yyyymmdd This is the date of the pulltime.
    '                If not specified, then this is assumed to be Now's date.
    
    '-range=nnnn  This is the number of minutes backwards from the pull time
    '             that defines valid range of charting events.
    
    nowdt = Now
    cmdLine = LCase(Command$)
    ps = InStr(cmdLine, "-s=")
    pe = InStr(cmdLine, "-e=")
    s = Mid$(cmdLine, ps + 3, 12)
    e = Mid$(cmdLine, pe + 3, 12)
    MakeCSVFile
    n = GetAllInputFilenames
    While i < n
        i = i + 1
        infn = inDirPath + "\" + fnames(i)
        ConvertToCSV
    Wend

End Sub

Private Function GetAllInputFilenames() As Integer
    'The location is determined by the first line of the dictionary.
    Const INITIAL_BLOCK_SIZE = 24
    Dim i As Integer
    Dim infname As String
    
    outfn = winloadpath & "\" & TRANSP_FILENAME
    
    ReDim fnames(0 To INITIAL_BLOCK_SIZE) As String
    i = 0
    infname = Dir$(inDirPath + "\" & DNLD_FNAME & "*.TXT") 'returns ONLY the filename.  For wildcards.
    While infname <> ""
        i = i + 1
        If (i > UBound(fnames)) Then
            ReDim Preserve fnames(0 To i + INITIAL_BLOCK_SIZE) As String
        End If
        fnames(i) = infname
        infname = Dir$
    Wend
    
    BubbleSort fnames, i
    
    GetAllInputFilenames = i
    
End Function

Private Sub BubbleSort(arr As Variant, numEls As Integer)
    Dim lLoop1 As Integer
    Dim lLoop2 As Integer
    Dim lTemp As Variant
    
    For lLoop1 = numEls To LBound(arr) Step -1
       For lLoop2 = LBound(arr) + 1 To lLoop1
           If arr(lLoop2 - 1) > arr(lLoop2) Then
              lTemp = arr(lLoop2 - 1)
              arr(lLoop2 - 1) = arr(lLoop2)
              arr(lLoop2) = lTemp
           End If
       Next lLoop2
   Next lLoop1
End Sub


Private Sub ConvertToCSV()
    Dim buf As String
    Dim iRow As Long
    Dim s As String
    Dim rows() As String
    Dim maxrow As Long
    Dim hdr As String
    Dim rec As String
    Dim sql As String
    Dim acctnum As String, name As String, unit As String, room As String, bed As String, facility As String
    Dim perfdatetime As String, event_id As String, event_desc As String, result As String
    
    On Error GoTo errProcess
    
    datafile = FreeFile
    Open infn For Input As #datafile
    s = Input(LOF(datafile), datafile)
    rows() = Split(s, vbCrLf)
    maxrow = UBound(rows)
    
    For iRow = 0 To maxrow
        buf = rows(iRow)
        If Trim$(buf) = "" Then 'blank
        Else
        If Trim$(Mid$(buf, 1, 4)) = "" Then 'Event record
            hdr = Mid$(buf, 5, 12)
            If hdr >= s And hdr <= e Then
                Print #csvfile, buf
            End If
        Else 'Header record
            Print #csvfile, buf
        End If
        End If
    Next

    Close #datafile
    CloseLogFiles
    Exit Sub
errProcess:
        
End Sub

'SAMPLE DATA
'0        1         2         3         4         5         6         7         8         9         0         1         2         3         4         5         6         7
'123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
'0906101310          RANDOLPH CLARENCE O           A5E       0508      A    A    20100211202356
'    20100211192200 6879                          Pulse                                                             106 brachial
'    20100211192200 6880                          Respirations                                                      18
'    20100211192200 6881                          BP                                                                116/84 automated
'    20100211192200 6891                          O2 saturation                                                     99% oxygen
'    20100211192200 33682                         Safety checks                                                     bed low, call bell, patient checked
'    20100211200800 5793                          Breath sounds - All lobes                                         clear
'    20100211200800 5809                          Oxygen per FiO2%                                                  30% trach collar
'    20100211200800 5810                          Resp Invtn RN/RT                                                  inr cnula securd, oxygen, pulse oximeter
'    20100211200800 5820                          Heart Rate/Sound                                                  tachycardic
'    20100211200800 5929                          Orientation/LOC                                                   respds pain only
'    20100211200800 6240                          Diet                                                              npo w/tbfdg tpn
'    20100211200800 9506                          Trach                                                             metal
'    20100211200800 9513                          Trach size                                                        8
'    20100211200800 9535                          Neuro/Musc asmnt                                                  WNL except
'    20100211200800 9647                          Emot/Mntl assmnt                                                  WNL
'    20100211200800 16992                         Activity                                                          bedrest
'    20100211200800 16996                         Interventions                                                     circ checks, oxygen, pulse oximeter, SCDs, telemetry
'    20100211200800 16999                         Interventions                                                     cont to orient, ROM, seizure prec
'    20100211200800 17007                         Rhythm                                                            sinus tach
'    20100211200800 18095                         Pain                                                              unresponsive
'    20100211200800 24360                         Isolation                                                         contact
'    20100211200800 33405                         ADL Level                                                         total care, w/ 2 plus assist
'    20100211200800 33524                         Pulses - +1 Weak                                                  R dors/ped, L dors/ped
'    20100211200800 33549                         Edema - 1+ = 2mm                                                  generalized
'    20100211200800 33682                         Safety checks                                                     bed low, call bell, name band on, patient checked, rails up x2
'    20100211200800 33775                         Score                                                             11
'    20100211200800 34074                         Intvervention                                                     fall risk band, near nur station, safety checks
'    20100211200800 34906                         Taught Whom                                                       family
'    20100211200800 35346                         Is pt fall risk                                                   yes
'    20100211200800 44819                         Med related asmt                                                  assess q 2 hr
'    20100211200800 80582                         Precautions                                                       AMS/dementia, mobility impaird
'    20100211213000 6928                          Tube feeding                                                      150
'    20100211213000 18095                         Pain                                                              unresponsive
'    20100211213000 33682                         Safety checks                                                     patient checked
'    20100211213000 35346                         Is pt fall risk                                                   yes
'    20100211223700 16095                         Turning/Position                                                  hob up, turn rt side
'    20100211223700 33682                         Safety checks                                                     bed low, call bell, patient checked
'    20100211224500 5793                          Breath sounds - All lobes                                         clear
'    20100211224500 5809                          Oxygen per FiO2%                                                  30% trach collar
'    20100211224500 5810                          Resp Invtn RN/RT                                                  inr cnula securd, oxygen, pulse oximeter, suction/oral, suction/trach, trac
'    20100211224500 5820                          Heart Rate/Sound                                                  tachycardic
'    20100211224500 9506                          Trach                                                             metal
'    20100211224500 9513                          Trach size                                                        8
'    20100211224500 9535                          Neuro/Musc asmnt                                                  WNL except
'    20100211224500 18095                         Pain                                                              unresponsive
'    20100211224500 33682                         Safety checks                                                     patient checked
'    20100211224500 35346                         Is pt fall risk                                                   yes
'    20100211230600 33682                         Safety checks                                                     patient checked
'    20100211230600 35346                         Is pt fall risk                                                   yes
'    20100211233500 6879                          Pulse                                                             84 brachial
'    20100211233500 6880                          Respirations                                                      16
'    20100211233500 6881                          BP                                                                103/66 automated
'    20100211233500 6891                          O2 saturation                                                     98% oxygen
'    20100211233500 33682                         Safety checks                                                     bed low, call bell, patient checked
'    20100212010000 18095                         Pain                                                              none
'    20100212010000 33682                         Safety checks                                                     patient checked
'    20100212010000 35346                         Is pt fall risk                                                   yes
'    20100212020100 5820                          Heart Rate/Sound                                                  regular
'    20100212020100 18095                         Pain                                                              none
'    20100212020100 33682                         Safety checks                                                     patient checked
'    20100212020100 35346                         Is pt fall risk                                                   yes
'    20100212025900 6976                          Incont urine                                                      1
'    20100212025900 33682                         Safety checks                                                     bed low, call bell, patient checked
'    20100212033000 18095                         Pain                                                              none
'    20100212033000 33682                         Safety checks                                                     patient checked
'    20100212033000 35346                         Is pt fall risk                                                   yes
'    20100212045000 6928                          Tube feeding                                                      420
'    20100212045000 18095                         Pain                                                              none
'    20100212045000 33682                         Safety checks                                                     patient checked
'    20100212045000 35346                         Is pt fall risk                                                   yes
'0914400017          BROWN ROBERT                  CL1       0305      C    C    20100212001951
'    20100211195700 6240                          Diet                                                              mechanical soft, thicknd liquids
'    20100211195700 6913                          Oral                                                              120
'    20100211195700 6976                          Incont urine                                                      1
'    20100211195700 33682                         Safety checks                                                     bed low, call bell, name band on, patient checked, rails up x2, environmt c
'    20100211220000 6976                          Incont urine                                                      2
'    20100211220000 33682                         Safety checks                                                     bed low, call bell, name band on, Portable alarm, rails up x2
'    20100211220000 34906                         Taught Whom                                                       patient
'    20100211220000 34911                         Activity/Rehab                                                    Ordact continue reinfrc
'    20100211220000 34912                         Safety                                                            Other continue reinfrc
'    20100211220000 34916                         Equipment                                                         O2 continue reinfrc
'    20100212001700 6879                          Pulse                                                             89 brachial
'    20100212001700 6880                          Respirations                                                      18
'    20100212001700 6881                          BP                                                                146/81
'    20100212001700 6891                          O2 saturation                                                     96% oxygen
'    20100212001700 18095                         Pain                                                              none
'    20100212001700 24360                         Isolation                                                         contact
'    20100212001700 33682                         Safety checks                                                     bed low, call bell, name band on, patient checked, rails up x2
'    20100212021300 16095                         Turning/Position                                                  turn rt side
'    20100212021300 33682                         Safety checks                                                     bed low, patient checked
'0916901216          PATTERSON MACK                A5P       0582      A    A    20100212015733
'    20100211200000 33682                         Safety checks                                                     bed alarm on, bed low, call bell, name band on, patient checked
'    20100211210000 33682                         Safety checks                                                     toilet offered, bed alarm on, bed low, call bell, name band on, patient che
'    20100211220000 33682                         Safety checks                                                     toilet offered, bed alarm on, bed low, call bell, name band on, patient che
'    20100211224400 5793                          Breath sounds - All lobes                                         clear
'    20100211224400 5820                          Heart Rate/Sound                                                  regular
'    20100211224400 5929                          Orientation/LOC                                                   obey cmplx cmnd, person, place
'    20100211224400 6240                          Diet                                                              consistent carb, fluid restrict, renal
'    20100211224400 6355                          Behavior                                                          cooperative
'    20100211224400 9535                          Neuro/Musc asmnt                                                  WNL except
'    20100211224400 9647                          Emot/Mntl assmnt                                                  WNL
'    20100211224400 16095                         Turning/Position                                                  hob up, self
'    20100211224400 16992                         Activity                                                          bedrest
'    20100211224400 18095                         Pain                                                              none
'    20100211224400 24360                         Isolation                                                         none
'    20100211224400 33405                         ADL Level                                                         total care
'    20100211224400 33497                         Dialysis access                                                   permacath, right, chest
'    20100211224400 33524                         Pulses - +1 Weak                                                  R dors/ped
'    20100211224400 33525                         Pulses - +2 Normal                                                R radial, L radial
'    20100211224400 33631                         Wound Assessment - Wound 1                                        drsg dry/intact
'    20100211224400 33632                         Wound Assessment - Wound 2                                        clean
'    20100211224400 33647                         Wound Drainage - Wound 1                                          none
'    20100211224400 33648                         Wound Drainage - Wound 2                                          none
'    20100211224400 33682                         Safety checks                                                     toilet offered, bed alarm on, bed low, call bell, name band on, patient che
'    20100211224400 33725                         Wound Care - Wound 2                                              topical tx
'    20100211224400 33766                         Discharge Plan                                                    coord care mtng, need finanl help, lack suprt on DC
'    20100211224400 33775                         Score                                                             18
'    20100211224400 34073                         Fall Risk screen                                                  alt gait/mobiliy
'    20100211224400 34074                         Intvervention                                                     bed alarm on, fall risk band, near nur station, toileting assist, safety ch
'    20100211224400 34906                         Taught Whom                                                       patient
'    20100211224400 34908                         Learning Barrier                                                  none
'    20100211224400 34911                         Activity/Rehab                                                    OrdactLimtns continue reinfrc
'    20100211224400 34912                         Safety                                                            Calhlp continue reinfrc
'    20100211224400 35346                         Is pt fall risk                                                   yes
'    20100211224400 80428                         Type                                                              none
'    20100211224400 80582                         Precautions                                                       mobility impaird, seizure
'    20100211230000 33682                         Safety checks                                                     toilet offered, bed alarm on, bed low, call bell, name band on, patient che
'    20100212000000 6879                          Pulse                                                             70 brachial
'    20100212000000 6880                          Respirations                                                      16
'    20100212000000 6881                          BP                                                                168/98 manual
'    20100212000000 6891                          O2 saturation                                                     98% room air
'    20100212000000 9995                          Weight                                                            98.7kg bed
'    20100212000000 33682                         Safety checks                                                     toilet offered, bed alarm on, bed low, call bell, name band on, patient che
'    20100212010000 33682                         Safety checks                                                     toilet offered, bed alarm on, bed low, call bell, name band on, patient che
'    20100212020000 33682                         Safety checks                                                     toilet offered, bed alarm on, bed low, call bell, name band on, patient che
'    20100212030000 33682                         Safety checks                                                     toilet offered, bed alarm on, bed low, call bell, name band on, patient che
'    20100212040000 6879                          Pulse                                                             72 radial
'    20100212040000 6880                          Respirations                                                      20
'    20100212040000 6881                          BP                                                                147/76 manual
'    20100212040000 6891                          O2 saturation                                                     98% room air
'    20100212040000 33682                         Safety checks                                                     toilet offered, bed alarm on, bed low, call bell, name band on, patient che
'0928801550          GADSDEN CHERYL ANN            CV2       0414      C    C    20100212013533
'    20100211205000 6879                          Pulse                                                             101 brachial
'    20100211205000 6880                          Respirations                                                      20
'    20100211205000 6881                          BP                                                                220/138 automated
'    20100211205000 6891                          O2 saturation                                                     100% room air
'    20100211210000 33682                         Safety checks                                                     patient checked
'    20100211222300 6932                          Urine                                                             1
'    20100211223300 6881                          BP                                                                172/116 automated
'    20100211223400 6928                          Tube feeding                                                      480
'    20100211223400 16966                         IV Fluid                                                          160
'    20100212003300 6879                          Pulse                                                             97 brachial
'    20100212003300 6880                          Respirations                                                      18
'    20100212003300 6881                          BP                                                                158/109 automated
'    20100212003300 6891                          O2 saturation                                                     100% room air
'    20100212003300 33682                         Safety checks                                                     bed low, call bell, name band on, patient checked, rails up x2
'    20100212004500 16095                         Turning/Position                                                  hob up, self
'    20100212004500 18095                         Pain                                                              none
'    20100212004500 18098                         Pain Score/Eval                                                   0(0 NUMERIC PAIN SCL
'    20100212004500 24360                         Isolation                                                         modified contact
'    20100212004500 33682                         Safety checks                                                     bed low, call bell, name band on, patient checked, rails up x2
'    20100212004500 34906                         Taught Whom                                                       patient
'    20100212004500 34908                         Learning Barrier                                                  physical
'    20100212004500 34910                         Medications                                                       current meds
'    20100212004500 34911                         Activity/Rehab                                                    ordered activity, limitations
'    20100212004500 34912                         Safety                                                            call for help
'    20100212004500 34913                         Signs/Symptoms                                                    report symptom
'    20100212004500 34916                         Equipment                                                         call light, feeding pumps
'    20100212004500 34919                         Pain Management                                                   call for PRN med
'    20100212004500 35341                         Sedation level                                                    2=awake alert
'    20100212004500 80428                         Type                                                              modified contact
'    20100212004500 80582                         Precautions                                                       mobility impaird
'    20100212015900 33682                         Safety checks                                                     patient checked
'    20100212020300 33682                         Safety checks                                                     bed low, call bell, name band on, patient checked, rails up x2
'    20100212033400 6879                          Pulse                                                             93 brachial
'    20100212033400 6880                          Respirations                                                      14
'    20100212033400 6881                          BP                                                                151/107 automated
'    20100212033400 6891                          O2 saturation                                                     100% room air
'    20100212033400 33682                         Safety checks                                                     bed low, call bell, name band on, patient checked, rails up x2
'    20100212045100 6931                          Urine                                                             0
'    20100212045100 33682                         Safety checks                                                     bed low, call bell, patient checked, rails up x2



Private Sub MakeCSVFile()
    Dim dicfn As String
    Dim dicfile As Integer

    Const HKEY_LOCAL_MACHINE = &H80000002
    Dim dt As Variant
    
        winpfspath = "D:\"
        winloadpath = winpfspath
        winlogpath = winpfspath

    csvname = winpfspath & "\TCdata.txt"
    
    csvfile = FreeFile
    Open csvname For Append As #csvfile
    
    
    
End Sub

Private Sub CloseLogFiles()

    Close #csvfile

End Sub



