Attribute VB_Name = "BJHInpatient"
Option Explicit
'
' Parrish Inpatient
'
' This processes one patient using the Inpatient methodology.
'
' All search functions use exact match for code and result (except when result is null).
' result_like looks for exact match in the result.
' result_list looks for any one of a list of words exactly as the result.
'
Private Const MAX_INDS = 101
Private Const MAX_PROCS = 20
Private Const COMMACHAR = "[]"   'string equiv to comma, use for replacement in list items with commas.

Private Type indicator_data
    checked     As Boolean
    group       As Integer
    also_mark   As String                   'not used at Arnot (handy with user-defined indicators?)
End Type

Private Type procdata                       'not used at Arnot
    pnum    As Integer
    start   As Date
    finish  As Date
    isvalid As Boolean 'used in combining procs
    pindex As Integer 'used in combining procs = the index of procs whose start/finish times are equal to this one
End Type
Private Type ocdata                       'not used at Arnot
    checked As Boolean
    pnum    As Integer
    start   As String
End Type

Private inds(MAX_INDS)          As indicator_data

Private numprocs                As Integer
Private numoutcomes             As Integer
Private procs(MAX_PROCS)        As procdata
Private oc(MAX_PROCS)           As ocdata

Private WHERE_ENCOUNTER         As String
Private AND_UNIT                As String
Private AND_PULL_RANGE          As String
Private AND_OUTCOMES_RANGE      As String
Private AND_ARRIVAL             As String
Private BUCKET_CALC             As String

Private m_pat                   As PatientInfo
Private m_assist_count          As Integer

Private Enum SearchMode
    SearchDefault
    searchpullrange             'search within the current pull     -- default
    SearchSinceArrival          'search since arrival to the unit
    SearchSinceAdmission        'search everything since admission to the hospital
    SearchOutcomesRange         'search within the current pull + 24 hours before
End Enum

Private Enum TCOperator
    opeq = 1
    oplike = 2
    OPLTE = 3
    OPGTE = 4
    OPNE = 5
    opnotlike = 6
    opnone = 0
End Enum

Private Enum CountMode
    countall
    CountFirst                  'stop after one is found
End Enum

Private Enum GetValueMode
    GetTotal
    GetMax
    GetLast
End Enum

Private Const FREQUENCY_BUCKET_SIZE = 20    'min

Private Enum Frequencies
    QNONE
    Q4H
    Q2H
    Q1H
    Q30M
End Enum

Private Type fmapRow
    los_high                As Single       'the LOS being testing
    freq(QNONE To Q30M)     As Integer      'the count required for each Q value
End Type

Private m_freq_map(6)       As fmapRow      '1,2,4,8,12,24 hours
Private exclusionRange      As String
Private pminRN As Date
Private pminNONRN As Date
Private ONunitRNmins As Integer
Private OFFunitRNmins As Integer
Private OFFunitNONRNmins As Integer





'This is the main entry point
'
Public Sub ProcessInpatient(pat As PatientInfo)
    Dim t1 As Date
    Dim t2 As Date
    Dim tdiff As Integer
    
    On Error GoTo errHandler
    
    m_pat = pat

    InitGroupsIfNeeded
    SetSQLConstants
    LoadFreqTable
    ResetIndicators
    ResetProcs
    ResetOutcomes

    t1 = Now
    Check_1_2_3
    t2 = Now
    tdiff = DateDiff("s", t1, t2)
    dvprint "t01=" & tdiff
    
    t1 = Now
    Check_4
    t2 = Now
    tdiff = DateDiff("s", t1, t2)
    dvprint "t04=" & tdiff
    
    t1 = Now
    Check_5_6
    t2 = Now
    tdiff = DateDiff("s", t1, t2)
    dvprint "t05=" & tdiff
    
    t1 = Now
    Check_7
    t2 = Now
    tdiff = DateDiff("s", t1, t2)
    dvprint "t07=" & tdiff
    
    t1 = Now
    Check_8
    t2 = Now
    tdiff = DateDiff("s", t1, t2)
    dvprint "t08=" & tdiff
    
    t1 = Now
    Check_9_10
    t2 = Now
    tdiff = DateDiff("s", t1, t2)
    dvprint "t09=" & tdiff
    
    t1 = Now
    Check_11_12
    t2 = Now
    tdiff = DateDiff("s", t1, t2)
    dvprint "t11=" & tdiff
    
    t1 = Now
    Check_13
    t2 = Now
    tdiff = DateDiff("s", t1, t2)
    dvprint "t13=" & tdiff
    
    t1 = Now
    Check_14_15_16_17
    t2 = Now
    tdiff = DateDiff("s", t1, t2)
    dvprint "t14=" & tdiff
    
    t1 = Now
    Check_18
    t2 = Now
    tdiff = DateDiff("s", t1, t2)
    dvprint "t18=" & tdiff
    
    t1 = Now
    Check_19_20
    t2 = Now
    tdiff = DateDiff("s", t1, t2)
    dvprint "t19=" & tdiff
    
    t1 = Now
    Check_21
    t2 = Now
    tdiff = DateDiff("s", t1, t2)
    dvprint "t21=" & tdiff
    
    t1 = Now
    Check_22
    t2 = Now
    tdiff = DateDiff("s", t1, t2)
    dvprint "t22=" & tdiff
    
    Check_UserDefined
    AtLeastOneADL
    HighestIndicatorInEachGroupWins

    t1 = Now
    CheckProcs
    t2 = Now
    tdiff = DateDiff("s", t1, t2)
    dvprint "tp=" & tdiff
    
'    CheckOutcomes

    If g_no_output Then Exit Sub
    OutputClass
    OutputProcs
    'OutputOutcomes
    Exit Sub
    
errHandler:
    LogError Err.Description & " in " & Err.source, EVENT_CATEGORY_UNEXPECTED
    Exit Sub
    Resume  'debug
End Sub

Private Sub ResetIndicators()
    Dim i As Integer
    
    For i = 1 To MAX_INDS
        inds(i).checked = False
        inds(i).also_mark = ""
    Next i
    
End Sub

Private Sub ResetProcs()
    numprocs = 0
End Sub
Private Sub ResetOutcomes()
    numoutcomes = 0
End Sub

Private Sub InitGroupsIfNeeded()
    On Error GoTo errHandler
    
    Static been_here As Boolean
    Dim rs As New Recordset
    Dim sql As String
    Dim inum As Integer
    
    If been_here Then Exit Sub
    
    'get indicator radio groups from the database
    sql = "SELECT INDICATOR_NUMBER, RADIO_GROUP FROM INDICATOR_DEFINITION WHERE METHODOLOGY_ID=" & m_pat.meth_id
    rs.Open sql, g_cnADO
    
    Do While Not rs.EOF
        inum = rs("INDICATOR_NUMBER")
        If (inum < MAX_INDS) Then
            inds(inum).group = g_dbutil.DBToInteger(rs("RADIO_GROUP"))  'could be null
        End If
        rs.MoveNext
    Loop
    
    rs.Close
    Exit Sub
    
errHandler:
    g_util.ThrowError "InitGroupsIfNeeded"
End Sub

Private Sub LoadFreqTableRow(m_freq As fmapRow, los_high As Single, values As String)
    Dim arr() As String, n As Integer, i As Integer
    
    m_freq.los_high = los_high

    n = g_util.SplitTextOnChar(values, ",", arr(), 0, 0)
    For i = 0 To n - 1
        m_freq.freq(i) = CInt(arr(i))
    Next i
End Sub

Private Sub LoadFreqTable()
                                 ' LOS,   None Q4h Q2h Q1h Q30m
    LoadFreqTableRow m_freq_map(1), 1, "    0,  0,  0,  0,  1"
    LoadFreqTableRow m_freq_map(2), 2, "    0,  0,  0,  1,  2"
    LoadFreqTableRow m_freq_map(3), 4, "    0,  1,  2,  3,  4"
    LoadFreqTableRow m_freq_map(4), 8, "    0,  2,  3,  4,  8"
    LoadFreqTableRow m_freq_map(5), 12, "   0,  2,  4,  6, 12"
    LoadFreqTableRow m_freq_map(6), 9999, " 0,  3,  6, 12, 24"         '24+
    
    'To read this table: if you have at least 6 asessmenets in a 12-hour period, you get Q1h
    '(row 5: LOS=12, column Q1h has a count of 6)
End Sub

Private Function FreqForCount(los_hours As Single, count As Integer) As Frequencies
    Dim i As Integer, j As Integer
    
    For i = 1 To UBound(m_freq_map)                         'go from LOS 1 to 24+
        If (los_hours <= m_freq_map(i).los_high) Then
            For j = Q30M To QNONE Step -1                   'search right to left
                If (count >= m_freq_map(i).freq(j)) Then
                    FreqForCount = j
                    Exit Function
                End If
            Next j
        End If
    Next i
    
    FreqForCount = QNONE
End Function

Private Sub SetSQLConstants()
    'These SQL constants are combined in different ways for searching
    WHERE_ENCOUNTER = " where (encounter_id = " & m_pat.encounter_id & ")"
    AND_UNIT = " and (unit_id = " & m_pat.unit_id & ")"
    AND_PULL_RANGE = " and (event_datetime between " & g_dbutil.SQL_DateTime(m_pat.pull_start) & " and " & g_dbutil.SQL_DateTime(m_pat.pull_finish) & ")"
    AND_ARRIVAL = " and (event_datetime between " & g_dbutil.SQL_DateTime(m_pat.unit_arrival) & " and " & g_dbutil.SQL_DateTime(m_pat.pull_finish) & ")"
    AND_OUTCOMES_RANGE = " and (event_datetime between " & g_dbutil.SQL_DateTime(DateAdd("d", -1, m_pat.pull_start)) & " and " & g_dbutil.SQL_DateTime(m_pat.pull_finish) & ")"
    
    'What bucket number does the event belong to?
    BUCKET_CALC = "DateDiff(minute," & g_dbutil.SQL_DateTime(m_pat.pull_start) & ",event_datetime) / " & FREQUENCY_BUCKET_SIZE
End Sub

'Add this to all chart item queries to start the WHERE clause
'This is where the search depth is set
Private Function WhereBase(Optional search_mode As SearchMode = SearchDefault)
    Dim result As String
    
    Select Case search_mode
    Case searchpullrange, SearchDefault
        result = WHERE_ENCOUNTER & AND_UNIT & AND_PULL_RANGE    'search within pull range
    Case SearchSinceArrival
        result = WHERE_ENCOUNTER & AND_UNIT & AND_ARRIVAL       'search since arrival to the unit
    Case SearchSinceAdmission
        result = WHERE_ENCOUNTER                                'search since admission to the hospital (any unit)
    Case SearchOutcomesRange
        result = WHERE_ENCOUNTER & AND_UNIT & AND_OUTCOMES_RANGE    'search within pull range+24hrs before
    End Select
    
    'Since we are looking for one patient in one facility, we don't have to filter by TC_source_ID.
    
    WhereBase = result
End Function

Private Function ValueIsAList(result As String) As Boolean
    ValueIsAList = (InStr(1, result, ",") > 0)                'is there a comma-separated list?
End Function

'Look for any of these fields.  Cat/desc/field = exact match.  Result = like match.
Private Function AndSimpleItemFilter(code As String, opcode As TCOperator, desc As String, opdesc As TCOperator, result_like As String, opres As TCOperator) As String
    Dim result As String
    
    If Len(code) Then result = result & " and code" & Op(opcode, code)
    If Len(desc) Then result = result & " and description" & Op(opdesc, desc)
    If Len(result_like) Then result = result & " and result" & Op(opres, result_like)

    AndSimpleItemFilter = result
End Function
Private Function AndSpecialItemFilter(code As String, opcode As TCOperator, desc As String, opdesc As TCOperator, result_like As String, opres As TCOperator) As String
    Dim result As String
    
    If Len(code) Then result = result & " and code" & Op(opcode, code)
    If Len(desc) Then result = result & " and description" & Op(opdesc, desc)
    If Len(result_like) Then result = result & " and substring(result,1,1)" & Op(opres, result_like)

    AndSpecialItemFilter = result
End Function

'Look for a result that contains one of the words in the list
'The only drawback with this function is that we don't know what it found (for audits)
'Because of this, it is only used in as few places.
Private Function AndResultContains(result_list As String, opres As TCOperator) As String
    Dim arr() As String
    Dim i As Integer, n As Integer
    Dim result As String
    
    If Len(result_list) = 0 Then Exit Function
    
    If Len(result_list) = 0 Then Exit Function
    
    n = g_util.SplitTextOnChar(result_list, ",", arr(), 1, 0)
    
    result = "and ((1=0)"   ' or (result=this) or (result=that)
    
    For i = 1 To n
        result = result & " or (result" & Op(opres, Trim$(arr(i))) & ")"
    Next i
    
    result = result & ")"
    
    AndResultContains = result
End Function

Private Function DescribeSearchDepth(search_mode As SearchMode) As String
    Dim result As String
    
    Select Case search_mode
    Case searchpullrange, SearchDefault
        'result = "in pull range"
        result = ""                         'be quiet since this is default
    Case SearchSinceArrival
        result = "since arrival to unit"
    Case SearchSinceAdmission
        result = "since admission"
    Case SearchOutcomesRange
        result = "since 24hrs before pull"
    End Select
    
    DescribeSearchDepth = result
End Function

Private Function Describe(code As String, opcode As TCOperator, desc As String, opdesc As TCOperator, result_list As String, opres As TCOperator, Optional search_mode As SearchMode = searchpullrange) As String
    Dim sql As String, and_filter As String
    Dim rs As New Recordset
    Dim result As String
   
    If Not g_debug Then Exit Function           'avoid extra overhead if not making a log

    result = "looking for"
    If Len(code) Then result = result & " code" & Op(opcode, code)
    If Len(desc) Then result = result & " desc" & Op(opdesc, desc)
    If Len(result_list) Then result = result & " result" & Op(opres, result_list)

    and_filter = AndSimpleItemFilter(code, opcode, desc, opdesc, "", 0) & AndResultContains(result_list, oplike)
    
    sql = "select code, description, result from chart_item" & WhereBase(search_mode) & and_filter
    rs.Open sql, g_cnADO
    If rs.EOF Then
        result = result & "; not found " & DescribeSearchDepth(search_mode)
    Else
        result = result & "; found"
        'Add info for columns that were not specified and a value was found
        If (Len(code) = 0) And Len(rs("code")) Then result = result & " code" & Op(opcode, rs("code"))
        If (Len(desc) = 0) And Len(rs("description")) Then result = result & " desc" & Op(opdesc, rs("description"))
        'Add the complete result found (we searched for a word or words)
        result = result & " result" & Op(opres, rs("result"))
        'Are there more results?  Just say how many; we aren't going to list them.
        If (rs.RecordCount = 2) Then
            result = result & " (1 more result)"
        ElseIf (rs.RecordCount > 2) Then
            result = result & " (" & rs.RecordCount - 1 & " more results)"
        End If
    End If
    rs.Close
    
    Describe = result
End Function

'Set an indicator for this reason (low level)
Private Sub SetInd(inum As Integer, reason As String)
    If inds(inum).checked And Not g_debug Then Exit Sub       'already set and no log?

    inds(inum).checked = True
    dprint "Set Ind #" & inum & ": " & reason
End Sub

'Clear an indicator for this reason (low level)
Private Sub ClrInd(inum As Integer, reason As String)
    If Not inds(inum).checked And Not g_debug Then Exit Sub   'already clear and no log?
    
    inds(inum).checked = False
    dprint "Clr Ind #" & inum & ": " & reason
End Sub

'Count how many items have the search word in result_like
'This is simple and fast
Private Function CountSimpleResult(code As String, opcode As TCOperator, _
                                   desc As String, opdesc As TCOperator, _
                                   result_like As String, opres As TCOperator, _
                                   Optional search_mode As SearchMode = searchpullrange, _
                                   Optional trace As Boolean = True, _
                                   Optional found_what As String) As Long
    Dim sql As String, and_filter As String
    Dim rs As New Recordset
    Dim count As Long

    and_filter = AndSimpleItemFilter(code, opcode, desc, opdesc, result_like, opres)
    sql = "select count(*) from chart_item" & WhereBase(search_mode) & and_filter
    'Debug.Print sql
    rs.Open sql, g_cnADO
    count = rs(0)
    rs.Close
    
    'Normally CountSimpleResult will report what it finds; not eveyone wants this so the trace can be turned off
    found_what = Describe(code, opcode, desc, opdesc, result_like, opres, search_mode) & IIf(count > 0, "; count=" & count, "")
    If trace Then dvprint found_what
    
    CountSimpleResult = count
End Function
Private Function CountSpecialResult(code As String, opcode As TCOperator, _
                                   desc As String, opdesc As TCOperator, _
                                   result_like As String, opres As TCOperator, _
                                   Optional search_mode As SearchMode = searchpullrange, _
                                   Optional trace As Boolean = True, _
                                   Optional found_what As String) As Long
    Dim sql As String, and_filter As String
    Dim rs As New Recordset
    Dim count As Long

    and_filter = AndSpecialItemFilter(code, opcode, desc, opdesc, result_like, opres)
    sql = "select count(*) from chart_item" & WhereBase(search_mode) & and_filter
    'Debug.Print sql
    rs.Open sql, g_cnADO
    count = rs(0)
    rs.Close
    
    'Normally CountSimpleResult will report what it finds; not eveyone wants this so the trace can be turned off
    found_what = Describe(code, opcode, desc, opdesc, result_like, opres, search_mode) & IIf(count > 0, "; count=" & count, "")
    If trace Then dvprint found_what
    
    CountSpecialResult = count
End Function

'Count how many items have a result with one of the words given in the result_list
'Returns a description of what word was found
'This is more powerful than CountSimpleResult but it is slower
Private Function CountResultInList(code As String, opcode As TCOperator, _
                                   desc As String, opdesc As TCOperator, _
                                   result_list As String, opres As TCOperator, _
                                   search_mode As SearchMode, _
                                   count_mode As CountMode, _
                                   trace As Boolean, found_what As String) As Integer
    On Error GoTo errHandler
    Dim sql As String, and_filter As String, arr() As String
    Dim rs As New Recordset
    Dim i As Integer, n As Integer, count As Integer

    n = g_util.SplitTextOnChar(result_list, ",", arr(), 1, 0)
    
    'Do not filter by result here; we need all results
    and_filter = AndSimpleItemFilter(code, opcode, "", opnone, "", opnone)
    sql = "select code,result from chart_item" & WhereBase(search_mode) & and_filter
    'Debug.Print sql
    rs.Open sql, g_cnADO
    
    Do While Not rs.EOF
        'Look for each search word in the result (case insensitive)
        For i = 1 To n
            arr(i) = Trim$(arr(i))          'we split on comma so there usually are leading blanks
            If InStr(1, rs("result"), arr(i), vbTextCompare) > 0 Then
                found_what = "found '" & arr(i) & "' in code" & Op(opcode, rs("code")) & " result" & Op(opres, rs("result"))
                If trace Then dvprint found_what
                count = count + 1
                If count_mode <> countall Then Exit Do  'stop counting
                Exit For                                'try next record
            End If
        Next i
        rs.MoveNext
    Loop
    
    If (count > 0) Then
        'We already printed what was found; maybe add how many?
        If trace And (count_mode = countall) Then dvprint "found " & count & " total"
    Else
        found_what = Describe(code, opcode, desc, opdesc, result_list, opres, search_mode)       'not found
        If trace Then dvprint found_what
    End If
    
    rs.Close
    
    CountResultInList = count
    Exit Function
    
errHandler:
    g_util.ThrowError "CountResultInList"
    Resume   'debug
End Function
Private Function CountDescInList(code As String, opcode As TCOperator, _
                                 desc As String, opdesc As TCOperator, _
                                 Optional search_mode As SearchMode = searchpullrange, _
                                 Optional count_mode As CountMode = countall, _
                                 Optional trace As Boolean = False, _
                                 Optional found_what As String) As Integer
    On Error GoTo errHandler
    Dim sql As String, and_filter As String, arr() As String
    Dim rs As New Recordset
    Dim i As Integer, n As Integer, count As Integer

    n = g_util.SplitTextOnChar(desc, ",", arr(), 1, 0)
    
    'Do not filter by result here; we need all results
    and_filter = AndSimpleItemFilter(code, opcode, "", opnone, "", opnone)
    sql = "select code,description from chart_item" & WhereBase(search_mode) & and_filter
    dvprint sql
    'Debug.Print sql
    rs.Open sql, g_cnADO
    
    Do While Not rs.EOF
        'Look for each search word in the result (case insensitive)
        For i = 1 To n
            arr(i) = Trim$(arr(i))          'we split on comma so there usually are leading blanks
            If InStr(1, rs("description"), arr(i), vbTextCompare) > 0 Then
                found_what = "found '" & arr(i) & "' in code" & Op(opcode, rs("code")) & " desc" & Op(opdesc, rs("description"))
                If trace Then dvprint found_what
                count = count + 1
                If count_mode <> countall Then Exit Do  'stop counting
                Exit For                                'try next record
            End If
        Next i

        rs.MoveNext
    Loop
    
    If (count > 0) Then
        'We already printed what was found; maybe add how many?
        If trace And (count_mode = countall) Then dvprint "found " & count & " total"
    Else
        found_what = Describe(code, opcode, desc, opdesc, "", opnone, search_mode)    'not found
        If trace Then dvprint found_what
    End If
    
    rs.Close
    CountDescInList = count
    Exit Function
    
errHandler:
    g_util.ThrowError "CountDescInList"
    Resume   'debug
End Function
Private Function CountCodesInList(code As String, search_mode As SearchMode, count_mode As CountMode, trace As Boolean, found_what As String) As Integer
    On Error GoTo errHandler
    Dim sql As String, and_filter As String, arr() As String
    Dim rs As New Recordset
    Dim i As Integer, n As Integer, count As Integer

    sql = "select count(distinct(event_datetime)) from chart_item" & WhereBase(search_mode)
    arr = Split(code, ",")
    sql = sql & " and (code like '%" & arr(0) & "%'"
    For i = 1 To UBound(arr)
        sql = sql & " or code like '%" & arr(i) & "%'"
    Next i
    sql = sql & ")" & exclusionRange
    Debug.Print sql
    rs.Open sql, g_cnADO
    
    If Not rs.EOF Then
        count = rs(0)
    Else
        count = 0
    End If
    rs.Close
    
    If (count > 0) Then
        'We already printed what was found; maybe add how many?
        found_what = "Found " & count & " unique times of codes like (" & code & ")."
        If trace And (count_mode = countall) Then dvprint "found " & count & " total"
    Else
        found_what = "No codes in (" & code & ") were found."
        If trace Then dvprint found_what
    End If
    
    
    CountCodesInList = count
    Exit Function
    
errHandler:
    g_util.ThrowError "CountCodesInList"
    Resume   'debug
End Function
Private Function CountUniqueCodesInList(code As String, search_mode As SearchMode, count_mode As CountMode, trace As Boolean, found_what As String) As Integer
    On Error GoTo errHandler
    Dim sql As String, and_filter As String, arr() As String
    Dim rs As New Recordset
    Dim i As Integer, n As Integer, count As Integer

    sql = "select count(distinct(code)) from chart_item" & WhereBase(search_mode)
    arr = Split(code, ",")
    sql = sql & " and (code like '%" & arr(0) & "%'"
    For i = 1 To UBound(arr)
        sql = sql & " or code like '%" & arr(i) & "%'"
    Next i
    sql = sql & ")"
    Debug.Print sql
    rs.Open sql, g_cnADO
    
    If Not rs.EOF Then
        count = rs(0)
    Else
        count = 0
    End If
    rs.Close
    
    If (count > 0) Then
        'We already printed what was found; maybe add how many?
        found_what = "Found " & count & " unique codes like (" & code & ")."
        If trace And (count_mode = countall) Then dvprint "found " & count & " total"
    Else
        found_what = "No codes in (" & code & ") were found."
        If trace Then dvprint found_what
    End If
    
    
    CountUniqueCodesInList = count
    Exit Function
    
errHandler:
    g_util.ThrowError "CountUniqueCodesInList"
    Resume   'debug
End Function

'Private Function CountResultContains(code As String, opcode As TCOperator, desc As String, opdesc As TCOperator, result_list As String, opres As TCOperator, Optional search_mode As SearchMode = Searchpullrange, Optional trace As Boolean = True, Optional found_what As String) As Integer
'
'    If Len(result_list) Then
'        If ValueIsAList(result_list) Then
'            CountResultContains = CountResultInList(code, opcode, desc, opdesc, result_list, opres, search_mode, countall, trace, found_what)
'        Else
'            CountResultContains = CountSimpleResult(code, opcode, "", opnone, result_list, opres, search_mode, trace, found_what)
'        End If
'    ElseIf Len(desc) Then
'        If ValueIsAList(desc) Then
'            CountResultContains = CountDescInList(code, opcode, desc, opdesc, search_mode, countall, trace, found_what)
'        Else
'            CountResultContains = CountSimpleResult(code, opcode, desc, opdesc, "", opnone, search_mode, trace, found_what)
'        End If
'    Else
'        CountResultContains = CountSimpleResult(code, opcode, "", opnone, "", opnone, search_mode, trace, found_what)
'    End If
'End Function

Private Function ResultContains(code As String, opcode As TCOperator, desc As String, opdesc As TCOperator, result_list As String, opres As TCOperator, Optional count As Integer = 0, Optional search_mode As SearchMode = searchpullrange, Optional trace As Boolean = True, Optional found_what As String) As Boolean
    'tracing can be turned off and you can ask for what was found instead of having it printed
    
    'NOTE: result_list and desc CANNOT BOTH be searched for; it's either code+desc or code+result and either desc/result
    'can be lists.
    If InStr(1, code, "BJH Pro ", vbTextCompare) > 0 Then search_mode = SearchSinceAdmission
    If search_mode = SearchSinceAdmission Then dvprint "Since Admission: " & code
    If Len(result_list) Then
        If ValueIsAList(result_list) Then
            ResultContains = (CountResultInList(code, opcode, "", opnone, result_list, opres, search_mode, countall, trace, found_what) > count)
        Else
            ResultContains = (CountSimpleResult(code, opcode, "", opnone, result_list, opres, search_mode, trace, found_what) > count)
        End If
    ElseIf Len(desc) Then
        If ValueIsAList(desc) Then
            ResultContains = (CountDescInList(code, opcode, desc, opdesc, search_mode, countall, trace, found_what) > count)
        Else
            ResultContains = (CountSimpleResult(code, opcode, desc, opdesc, "", opnone, search_mode, trace, found_what) > count)
        End If
    Else
        ResultContains = (CountSimpleResult(code, opcode, "", opnone, "", opnone, search_mode, trace, found_what) > count)
    End If
End Function


'Private Function CountResultNotInList(cat As String, code As String, desc As String, field As String, result_list As String, search_mode As SearchMode, count_mode As CountMode, trace As Boolean, found_what As String) As Integer
Private Function CountResultNotInList(code As String, opcode As TCOperator, _
                                   desc As String, opdesc As TCOperator, _
                                   result_list As String, opres As TCOperator, _
                                   search_mode As SearchMode, _
                                   count_mode As CountMode, _
                                   trace As Boolean, found_what As String) As Integer
    On Error GoTo errHandler
    Dim sql As String, and_filter As String, arr() As String
    Dim rs As New Recordset
    Dim i As Integer, n As Integer, count As Integer
    Dim found_one As Boolean

    n = g_util.SplitTextOnChar(result_list, ",", arr(), 1, 0)

    'Do not filter by result here; we need all results
    and_filter = AndSimpleItemFilter(code, opcode, desc, opdesc, "", opnone)
    sql = "select code, result from chart_item" & WhereBase(search_mode) & and_filter
    'Debug.Print sql
    rs.Open sql, g_cnADO

    Do While Not rs.EOF
        'Look for each search word in the result (case insensitive)
        found_one = False

        For i = 1 To n
            arr(i) = Trim$(arr(i))          'we split on comma so there usually are leading blanks
            arr(i) = Replace(arr(i), COMMACHAR, ",")

            If InStr(1, rs("result"), arr(i), vbTextCompare) > 0 Then
                found_what = "found '" & arr(i) & "' in code='" & rs("code") & "' result='" & rs("result") & "' -- ignore this"
                If trace Then dvprint found_what
                found_one = True
                Exit For
            End If
        Next i

        If (Not found_one) Then
            'none of the words were found - good!
            found_what = Describe(code, opcode, desc, opdesc, result_list, opres, search_mode) & " -- does not contain '" & result_list & "'"
            If trace Then dvprint found_what
            count = count + 1
            If count_mode <> countall Then Exit Do
        End If

        rs.MoveNext
    Loop

    If (count > 0) Then
        'We already printed what was found; maybe show the total?
        If trace And (count_mode = countall) Then dvprint "found " & count & " total"
    ElseIf (rs.RecordCount > 0) Then
        'We already printed what we ignored
    Else
        'Print what did not exist
        found_what = Describe(code, opcode, desc, opdesc, result_list, opres, search_mode)       'not found
        If trace Then dvprint found_what
    End If

    rs.Close

    CountResultNotInList = count
    Exit Function

errHandler:
    g_util.ThrowError "CountResultNotInList"
    Resume   'debug
End Function

'Private Function CountResultDoesNotContain(cat As String, code As String, desc As String, field As String, result_list As String, found_what As String, Optional search_mode As SearchMode = Searchpullrange, Optional trace As Boolean = True) As Integer
'    CountResultDoesNotContain = CountResultNotInList(cat, code, desc, field, result_list, search_mode, CountAll, trace, found_what)
'End Function

'Private Function ResultDoesNotContain(cat As String, code As String, desc As String, field As String, result_list As String, found_what As String, Optional search_mode As SearchMode = Searchpullrange, Optional trace As Boolean = True) As Boolean
Private Function ResultDoesNotContain(code As String, opcode As TCOperator, desc As String, opdesc As TCOperator, result_list As String, opres As TCOperator, Optional count As Integer = 0, Optional search_mode As SearchMode = searchpullrange, Optional trace As Boolean = True, Optional found_what As String) As Boolean
    ResultDoesNotContain = (CountResultNotInList(code, opcode, desc, opdesc, result_list, opres, search_mode, countall, trace, found_what) > 0)
End Function

'Set the indicator if the result contains one of the words in the result_list
Private Sub SetIndIfResultContains(inum As Integer, code As String, opcode As TCOperator, _
                                   desc As String, opdesc As TCOperator, _
                                   result_list As String, opres As TCOperator, _
                                   Optional count As Integer = 0, _
                                   Optional search_mode As SearchMode = searchpullrange)
    Dim found_what As String
    'avoid more queries if the indicator is already set
    If inds(inum).checked Then Exit Sub
    
    'Turn trace off for ResultContains() so the result can be placed on the "SetInd#" line below
    If ResultContains(code, opcode, desc, opdesc, result_list, opres, count, search_mode, False, found_what) Then
        SetInd inum, found_what             'echo here
    Else
        dvprint found_what                  'and here
    End If
End Sub

'Set the indicator if the result does not contain any of the words in result_list
'Private Sub SetIndIfResultDoesNotContain(inum As Integer, cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = Searchpullrange)
'    Dim found_what As String
'    'avoid more queries if the indicator is already set
'    If inds(inum).checked Then Exit Sub
'
'    'Turn trace off for ResultDoesNotContain() and echo what was set below with SetInd
'    If ResultDoesNotContain(cat, code, desc, field, result_list, found_what, search_mode, False) Then
'        SetInd inum, found_what             'echo here
'    Else
'        dvprint found_what                  'and here
'    End If
'End Sub

'Clear the indicator if the result contains on of the words in the result_list
'Private Sub ClrIndIfResultContains(inum As Integer, cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = Searchpullrange)
'    Dim found_what As String
'    'avoid more queries if the indicator is already clear
'    If Not inds(inum).checked Then Exit Sub
'
'    'Turn trace off for ResultContains() and echo what was set below with SetInd
'    If ResultContains(cat, code, desc, field, result_list, search_mode, False, found_what) Then
'        ClrInd inum, found_what             'echo here
'    Else
'        dvprint found_what                  'and here
'    End If
'End Sub


'These slightly smaller functions are meant for places where you aren't really looking for a chart result
'Private Function Exists(cat As String, Optional code As String, Optional desc As String, Optional field As String, Optional result_list As String, Optional search_mode As SearchMode = Searchpullrange, Optional trace As Boolean = True, Optional found_what As String) As Boolean
'    Exists = (CountResultContains(cat, code, desc, field, result_list, search_mode, trace, found_what) > 0)
'End Function

'Private Sub SetIndIfFound(inum As Integer, cat As String, Optional code As String, Optional desc As String, Optional field As String, Optional result_list As String, Optional search_mode As SearchMode = Searchpullrange)
'    SetIndIfResultContains inum, cat, code, desc, field, result_list, search_mode
'End Sub
'
'Private Sub ClrIndIfFound(inum As Integer, cat As String, Optional code As String, Optional desc As String, Optional field As String, Optional result_list As String, Optional search_mode As SearchMode = Searchpullrange)
'    ClrIndIfResultContains inum, cat, code, desc, field, result_list, search_mode
'End Sub


'Get the max/total value from a result (usually in the middle of the text)
'Private Function GetIntValue(get_mode As GetValueMode, code As String, opcode As TCOperator, desc As String, opdesc As TCOperator, result_like As String, opres As TCOperator, Optional search_mode As SearchMode = Searchpullrange) As Integer
'    Dim sql As String, and_filter As String, arr() As String, msg As String
'    Dim rs As New Recordset
'    Dim result As Integer, i As Integer, n As Integer, value As Integer
'    Dim found_one As Boolean
'
'    and_filter = AndSimpleItemFilter(code, opcode, desc, opdesc, result_like, opres)
'    sql = "select result from chart_item" & WhereBase(search_mode) & and_filter
'    'Debug.Print sql
'    rs.Open sql, g_cnADO
'
'    'Look for a number in the result
'
'    Do While Not rs.EOF
'        n = g_util.SplitTextOnChar(rs(0), " ", arr(), 1, 0)
'        For i = 1 To n
'            'Look for 1st character numeric; IsNumeric("60min") will fail so look at 1st char only
'            If IsNumeric(Left$(arr(i), 1)) Then
'                value = val(arr(i))                         'Use Val; CInt will error on "60min"
'                Select Case get_mode
'                Case GetMax
'                    result = g_util.Max(value, result)      'max
'                Case GetTotal
'                    result = result + value                 'total
'                Case GetLast
'                    result = value                          'last
'                End Select
'
'                'print what we are searching for (the first time)
'                If Not found_one Then
'                    dvprint Describe(cat, code, desc, field, result_like, search_mode)
'                End If
'                found_one = True
'                'print each value found
'                dvprint "  found numeric value " & result
'                'Keep going in case there are more
'            End If
'        Next i
'        rs.MoveNext
'    Loop
'
'    rs.Close
'
'    If Not found_one Then
'        'show what was not found
'        If g_verbose Then dprint Describe(cat, code, desc, field, result_like, search_mode)
'    End If
'
'    GetIntValue = result
'End Function

'Private Function GetMaxValue(cat As String, code As String, desc As String, field As String, result_like As String, Optional search_mode As SearchMode = Searchpullrange) As Integer
'    GetMaxValue = GetIntValue(GetMax, cat, code, desc, field, result_like, search_mode)
'End Function
'
'Private Function GetTotalValue(cat As String, code As String, desc As String, field As String, result_like As String, Optional search_mode As SearchMode = Searchpullrange) As Integer
'    GetTotalValue = GetIntValue(GetTotal, cat, code, desc, field, result_like, search_mode)
'End Function

'Get a result; returns True if found with return_result set
'Private Function GetResult(cat As String, code As String, desc As String, field As String, return_result As String, Optional search_mode As SearchMode = Searchpullrange) As Boolean
'    Dim rs As New Recordset
'    Dim sql As String
'
'    sql = "select result from chart_item" & WhereBase(search_mode) & AndSimpleItemFilter(cat, code, desc, field, "")
'    rs.Open sql, g_cnADO
'    If Not rs.EOF Then
'        return_result = rs(0) & ""
'    Else
'        return_result = ""
'    End If
'    rs.Close
'
'    dvprint Describe(code, desc, "", search_mode)
'    GetResult = (Len(return_result) > 0)
'End Function
'Private Function GetResultOfLatest(cat As String, code As String, desc As String, field As String, return_result As String, Optional search_mode As SearchMode = Searchpullrange) As Boolean
'    Dim rs As New Recordset
'    Dim sql As String
'    Dim evdt As Date
'    Dim done As Boolean
'
'    sql = "select event_datetime,result from chart_item" & WhereBase(search_mode) & AndSimpleItemFilter(cat, code, desc, field, "")
'    sql = sql & " order by event_datetime desc"
'    rs.Open sql, g_cnADO
'
'    return_result = ""
'    done = False
'
'    If Not rs.EOF Then evdt = rs(0)
'    Do While Not rs.EOF And Not done
'        If evdt = rs(0) Then
'            return_result = return_result & rs(1) & ","
'        Else
'            done = True
'        End If
'        rs.MoveNext
'    Loop
'    rs.Close
'
'    dvprint Describe(cat, code, desc, field, "", search_mode)
'    GetResultOfLatest = (Len(return_result) > 0)
'
'End Function

'select ind,code,opcode,descr,opdescr,res,opres where ind=1
'rs.open
'if not rs.eof then
'   ind=rs(0)
'   code=rs(1)
'   opcode=rs(2)
'   descr=rs(3)
'   opdescr=rs(4)
'   res=rs(5)
'   opres=rs(6)
'   do while not inds(ind).checked and not rs.eof
'       setindquery(code,opcode,descr,opdescr,res,opres)
'       rs.movenext
'   end 'while

'setindquery:
'SetIndIfResultContains(inum As Integer, ''=cat As String, code As String, opcode as TCOperator, desc As String, opdesc as TCOperator, ''=field As String, result_list As String, opres as TCOperator, Optional search_mode As SearchMode = Searchpullrange)
'

Private Sub SetIndicator(ind As Integer)
    Dim rs As New Recordset
    Dim sql As String
    Dim code As String
    Dim desc As String
    Dim res As String
    Dim opcode As TCOperator
    Dim opdesc As TCOperator
    Dim opres As TCOperator
    Dim count As Integer

    sql = "select ind,code,opcode,descr,opdescr,result,opresult,count from TCDATA where ind=" & ind
    rs.Open sql, g_cnADO
    Do While Not inds(ind).checked And Not rs.EOF
        code = g_dbutil.DBToString(rs(1))
        opcode = g_dbutil.DBToInteger(rs(2))
        desc = g_dbutil.DBToString(rs(3))
        opdesc = g_dbutil.DBToInteger(rs(4))
        res = g_dbutil.DBToString(rs(5))
        opres = g_dbutil.DBToInteger(rs(6))
        count = g_dbutil.DBToInteger(rs(7))
        SetIndIfResultContains ind, code, opcode, desc, opdesc, res, opres, count
        rs.MoveNext
    Loop
    
    rs.Close

End Sub

Private Sub Check_1_2_3()
    Dim bath1 As Boolean
    Dim feed As Boolean
    Dim found_what As String
    Dim found_what2 As String
    
    On Error GoTo errHandler

    dvprint "---------------"
    dvprint "1. ADL Self"
    dvprint "2. ADL Assist"
    dvprint "3. ADL Complete"
    dvprint "---------------"
    
'    opeq = 1
'    oplike = 2
'    OPLTE = 3
'    OPGTE = 4
'    opnone = 0
    'SetIndicator (3)
'    SetIndIfResultContains ind, code, opcode, desc, opdesc, res, opres, count
'if ResultContains(code, opcode, desc, opdesc, result_list, opres, count, search_mode, False, found_what) then

'5/16change for ICU:  check ind2 and 1 first.  Then, if nothing, or if only bath, or if only feed, then make 3.


    SetIndIfResultContains 3, "BJH AS FRA Initial RD", opeq, "FallRisk_Complete paralysis or completely immobilized", opeq, "", opnone
    If inds(3).checked Then Exit Sub
    
    feed = ResultContains("BJH VS Meal Feed SD", opeq, "Feeding_Complete", opeq, "", opnone, , , , found_what)
    If Not feed Then feed = ResultContains("INTAKE-Tube Feeding", oplike, "", opnone, "", opnone, , , , found_what)
    If Not feed Then feed = ResultContains("BJH IO Diet Ordered", opeq, "Ordered_NPO", opeq, "", opnone, , , , found_what)

    bath1 = ResultContains("BJH VS BathAssist", opeq, "complete", oplike, "", opnone, , , , found_what2)
    
    If IsICU Then
        If bath1 Or feed Then SetInd 3, "ICU: " & found_what & found_what2
    Else
        If bath1 And feed Then SetInd 3, found_what & found_what2
    End If

    If inds(3).checked Then Exit Sub
    If bath1 Or feed Then SetInd 2, found_what & found_what2
    SetIndIfResultContains 2, "BJH VS BathAssist", opeq, "assist", oplike, "", opnone
    SetIndIfResultContains 2, "BJH VS PeriAssist", opeq, "assist", oplike, "", opnone
    SetIndIfResultContains 2, "BJH VS Activity Assist", opeq, "assist", oplike, "", opnone
    SetIndIfResultContains 2, "BJH VS Positioning Assist", opeq, "assist", oplike, "", opnone
    SetIndIfResultContains 2, "BJH VS Oral Care", opeq, "assist", oplike, "", opnone
    SetIndIfResultContains 2, "BJH VS Meal Feed SD", opeq, "assist", oplike, "", opnone
    SetIndIfResultContains 2, "INTAKE-Tube Feeding", oplike, "", opnone, "", opnone
    
'    SetIndicator (2)
    If inds(2).checked Then Exit Sub
    SetIndIfResultContains 1, "BJH VS BathAssist", opeq, "self", oplike, "", opnone
    SetIndIfResultContains 1, "BJH VS PeriAssist", opeq, "self", oplike, "", opnone
    SetIndIfResultContains 1, "BJH VS Activity Assist", opeq, "freely", oplike, "", opnone
    'SetIndIfResultContains 1, "BJH VS Positioning Assist", opeq, "self", oplike, "", opnone
    SetIndIfResultContains 1, "BJH VS Oral Care", opeq, "self", oplike, "", opnone
    SetIndIfResultContains 1, "BJH VS Meal Feed SD", opeq, "self", oplike, "", opnone

'    SetIndicator (1)
    If Not inds(1).checked And Not inds(2).checked And Not inds(3).checked Then
        If IsICU Then
            SetInd 3, "Unit is ICU"
        End If
    End If
    
    Exit Sub
errHandler:
    g_util.ThrowError "Check_1_2_3"
    Resume  'debug
End Sub


Private Sub Check_4()
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "4. ADL Rehab"
    dvprint "---------------"

    SetIndIfResultContains 4, "BJH Pro Functional Changes", opeq, "", opnone, "", opnone
    SetIndIfResultContains 4, "BJHEDUNutrDietFeedTopic", opeq, "Swallowing", opeq, "", opnone
    SetIndIfResultContains 4, "BJH AS Neuro INV Activity and Tolerance", opeq, "hip precaution", oplike, "", opnone
    'SetIndicator (4)
    
    Exit Sub
errHandler:
    g_util.ThrowError "Check_4"
    Resume  'debug
End Sub


Private Sub Check_5_6()
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "5. ADL 2-3 Caregivers"
    dvprint "6. ADL 4 or more Caregivers"
    dvprint "---------------"
    
    SetIndIfResultContains 6, "BJH VS BathAssist", opeq, "complete by 4", oplike, "", opnone
    SetIndIfResultContains 6, "BJH VS PeriAssist", opeq, "complete by 4", oplike, "", opnone
    SetIndIfResultContains 6, "BJH VS Activity Assist", opeq, "assist of 4", oplike, "", opnone
    SetIndIfResultContains 6, "BJH VS Positioning Assist", opeq, "assist of 4", oplike, "", opnone
    'SetIndicator (6)
    If inds(6).checked Then Exit Sub
    SetIndIfResultContains 5, "BJH VS BathAssist", opeq, "2-3", oplike, "", opnone
    SetIndIfResultContains 5, "BJH VS PeriAssist", opeq, "2-3", oplike, "", opnone
    SetIndIfResultContains 5, "BJH VS Activity Assist", opeq, "assist of 2-3", oplike, "", opnone
    SetIndIfResultContains 5, "BJH VS Positioning Assist", opeq, "assist of 2-3", oplike, "", opnone
'    SetIndicator (5)
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_5_6"
    Resume  'debug
End Sub

Private Sub Check_7()
    Dim neuspeech As Boolean
    Dim found_what As String
    
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "7. Communication"
    dvprint "---------------"
    
    SetIndIfResultContains 7, "BJH Pro Speech Barriers", opeq, "Speech Limitations", opeq, "", opnone
    SetIndIfResultContains 7, "BJH Pro Primary Speaking LanguageYN", opeq, "Is English Your Preferred Speaking Language_No", opeq, "", opnone
    'SetIndIfResultContains 7, "BJH Pro Primary Speaking LanguageYN", opeq, "Interpreter Needed_Yes", opeq, "", opnone
    SetIndIfResultContains 7, "BJH Pro Reading", opeq, "Do You Have Difficulty Reading_Yes", opeq, "", opnone
    'SetIndIfResultContains 7, "BJH Pro Cognitive Barriers", opeq, "", opeq, "", opnone
    SetIndIfResultContains 7, "BJH AS Psych Communication Barriers", opeq, "", opeq, "", opnone
    'SetIndIfResultContains 7, "BJH AS Psy Behaviors Observations", opeq, "", opeq, "vegetative", oplike
    SetIndIfResultContains 7, "BJH AS Psy Behaviors Observations", opeq, "", opeq, "word salad", oplike
    'SetIndIfResultContains 7, "BJH RT Vent ArtAirway Type", opeq, "", opeq, "", opnone
    'SetIndIfResultContains 7, "BJH VS Acute Pain Tool Used 2", opeq, "", opeq, "non communicative", oplike
    'SetIndIfResultContains 7, "BJH VS Acute Pain Tool Used 2", opeq, "", opeq, "faces", oplike
    SetIndIfResultContains 7, "BJH AS Neuro Vision", opeq, "Vision_Blind Both Eyes", opeq, "", opnone
    SetIndIfResultContains 7, "BJH AS Neuro Hearing", opeq, "Hearing_Impaired hearing bilateral ears Impaired hearing bilateral ears", opeq, "", opnone
    SetIndIfResultContains 7, "BJH AS Resp Communication", opeq, "", opeq, "", opnone
    
    If inds(7).checked Then Exit Sub
    neuspeech = ResultDoesNotContain("BJH Neu Speech", opeq, "", oplike, "clear,fluent", oplike, , , , found_what)
    If neuspeech Then
        SetInd 7, found_what
    End If
    If inds(7).checked Then Exit Sub
    neuspeech = ResultDoesNotContain("BJH Neu NIH Best Language", opeq, "", oplike, "0", oplike, , , , found_what)
    If neuspeech Then
        SetInd 7, found_what
    End If
    If inds(7).checked Then Exit Sub
    neuspeech = ResultDoesNotContain("BJH Neu NIH Dysarthria", opeq, "", oplike, "0", oplike, , , , found_what)
    If neuspeech Then
        SetInd 7, found_what
    End If
    If inds(7).checked Then Exit Sub
    'SetIndIfResultContains 7, "BJH Pro Physical Limitations", opeq, "", opeq, "", opnone
    neuspeech = ResultDoesNotContain("BJH Pro Physical Limitations", opeq, "", oplike, "mobility/dexterity", oplike, , , , found_what)
    If neuspeech Then
        SetInd 7, found_what
    End If
'ASK SARAH
'    SetIndicator (7)
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_7"
    Resume  'debug
End Sub

Private Sub Check_8()
    On Error GoTo errHandler
    Dim found_what As String
    Dim neuspeech As Boolean

    dvprint "---------------"
    dvprint "8. Cognitive Support"
    dvprint "---------------"

    SetIndIfResultContains 8, "BJH AS Neuro Cognition/Memory", opeq, "", opeq, "", opnone
    If inds(8).checked Then Exit Sub
    neuspeech = ResultDoesNotContain("BJH Pro Cognitive Barriers", opeq, "", oplike, "unresponsive", oplike, , , , found_what)
    If neuspeech Then
        SetInd 8, found_what
    End If
 'BJH VS Oriented To SD   except results:  person,place,time,situation,unable to assess
'ASK SARAH
    If inds(8).checked Then Exit Sub
    neuspeech = ResultDoesNotContain("BJH VS Oriented To SD", opeq, "", oplike, "Person" & COMMACHAR & " place" & COMMACHAR & " time and situation,unable to assess", oplike, , , , found_what)
    If neuspeech Then
        SetInd 8, found_what
    End If
    If inds(8).checked Then Exit Sub
    SetIndIfResultContains 8, "BJH VS Orientation x", opeq, "", opeq, "1,2,3", opeq
    SetIndIfResultContains 8, "BJH AS Neuro Orientation x", opeq, "", opeq, "1,2,3", opeq
    SetIndIfResultContains 8, "BJH AS Neuro Cognition/Memory", opeq, "", opeq, "", opeq
    'SetIndIfResultContains 8, "BJH AS Neuro INV Safe Environment", opeq, "", opeq, "", opeq
    SetIndIfResultContains 8, "BJH AS Neuro Reality Testing", opeq, "", opeq, "", opeq
    SetIndIfResultContains 8, "BJH AS CAM Overall Score", opeq, "", opeq, "positive", oplike
 
    If inds(8).checked Then Exit Sub
    neuspeech = ResultDoesNotContain("BJH AS Neuro Orientation", opeq, "", oplike, "person" & COMMACHAR & " place" & COMMACHAR & " time and situation,unable to assess", oplike, , , , found_what)
    If neuspeech Then
        SetInd 8, found_what
    End If
 'BJH VS LOC2 LOC/Cognitive   except: Sedated?, alert, awake, sleeping
'ASK SARAH
'    If inds(8).checked Then Exit Sub
'    neuspeech = ResultDoesNotContain("BJH VS LOC2", opeq, "LOC/Cognitive", opeq, "alert,awake,sleeping", oplike, , , , found_what)
'    If neuspeech Then
'        SetInd 8, found_what
'    End If
'    If inds(8).checked Then Exit Sub
'    neuspeech = ResultDoesNotContain("BJH AS Neuro LOC Cognitive", opeq, "", oplike, "alert,awake,sleeping", oplike, , , , found_what)
'    If neuspeech Then
'        SetInd 8, found_what
'    End If

'BJH Neu1 Best Verbal Response   Best Verbal Response    4                     -  Confused
'BJH Neu1 Best Verbal Response   Best Verbal Response    1                     -  No response
'BJH Neu1 Best Verbal Response   Best Verbal Response    2                     -  Incomprehensible sounds
'BJH Neu1 Best Verbal Response   Best Verbal Response    3                     -  Inappropriate words
    neuspeech = ResultDoesNotContain("BJH Neu1 Best Verbal Response", opeq, "", oplike, "not tested,artificial airway,oriented", oplike, , , , found_what)
    If neuspeech Then
        SetInd 8, found_what
    End If

    SetIndIfResultContains 8, "BJH VS Oriented x", opeq, "", opeq, "1,2,3", opeq
    
    If inds(8).checked Then Exit Sub
    neuspeech = ResultDoesNotContain("BJH Neu NIH Commands", opeq, "", oplike, "0", oplike, , , , found_what)
    If neuspeech Then
        SetInd 8, found_what
    End If
    If inds(8).checked Then Exit Sub
    neuspeech = ResultDoesNotContain("BJH Neu NIH Questions", opeq, "", oplike, "0", oplike, , , , found_what)
    If neuspeech Then
        SetInd 8, found_what
    End If

 '   SetIndicator (8)
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_8"
    Resume  'debug
End Sub

Private Sub Check_9_10()
    On Error GoTo errHandler
    Dim found_what As String
    Dim neuspeech As Boolean
    Dim count As Integer
    

    dvprint "---------------"
    dvprint "9.  Behavior/Emotional Management"
    dvprint "10. Behavior/Emotional Mgmt - q 1 Hour"
    dvprint "---------------"

    'SetIndIfResultContains 10, "BJH VS Precautions", opeq, "", opeq, "suicide", oplike
    SetIndIfResultContains 10, "BJH AS Psy INV Support", opeq, "", opeq, "q1h", oplike
    'SetIndIfResultContains 10, "BJH VS Sitter", opeq, "", opeq, "", oplike
    SetIndIfResultContains 10, "BJH AS Psy INV Monitorng", opeq, "", opeq, "", oplike
'    SetIndicator (10)
    If inds(10).checked Then Exit Sub
    
    'SetIndIfResultContains 9, "BJH Res Restraint Reason", opeq, "", opeq, "", oplike
    SetIndIfResultContains 9, "BJH AS Psy INV Redirection", opeq, "", opeq, "", oplike
    SetIndIfResultContains 9, "BJH AS Psy INV Environment", opeq, "", opeq, "", oplike
    SetIndIfResultContains 9, "BJH AS Psych Verbalized Feelings", opeq, "", opeq, "", oplike
    SetIndIfResultContains 9, "BJH VS Precautions", opeq, "", opeq, "elopement", oplike
    SetIndIfResultContains 9, "BJH AS Psy Precautions", opeq, "", opeq, "elopement", oplike
    SetIndIfResultContains 9, "BJH AS Psy Precautions", opeq, "", opeq, "assault", oplike
    
    If inds(9).checked Then Exit Sub
    neuspeech = ResultDoesNotContain("BJH AS Psy INV Support", opeq, "", oplike, "religious practices supported", oplike, , , , found_what)
    If neuspeech Then
        SetInd 9, found_what
    End If
    If inds(9).checked Then Exit Sub
    neuspeech = ResultDoesNotContain("BJH AS Psy Behaviors Observations", opeq, "", oplike, "calm,pleasant,sociable,cooperative", oplike, , , , found_what)
    If neuspeech Then
        SetInd 9, found_what
    End If
    
    'SetIndicator (9)

    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_9_10"
    Resume  'debug
End Sub


Private Sub Check_11_12()
    Dim count As Integer
    Dim countlow As Integer
    Dim counthi As Integer
    Dim no11or12 As Boolean
    Dim found_what As String
    Dim found_what1 As String
    Dim found_what2 As String
    Dim found_what3 As String
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "11. Safety Management - q 2 Hours"
    dvprint "12. Safety Management - q 30 Minutes"
    dvprint "---------------"


    no11or12 = False
    dprint "Check11_12"
    
    If ResultContains("BJH VS RassSedationScale", opeq, "", opnone, "No response,Unarousable", oplike, , , False, found_what) Then
        no11or12 = True
        dvprint "No reponse or Unarousable: Indicators 11,12 will be suppressed"
    End If
' Removed 4/6/12
'    If ResultContains("BJH AS Fall Risk Low/High Risk Per Protocol", opeq, "Complete Paralysis or completely immobilized", oplike, "", opnone, , , False, found_what) Then
'        no11or12 = True
'        dvprint "Complete paralysis or completely immobilized: Indicators 11,12 will be suppressed"
'    End If

'    BJH AS FRA Total CALC
    
'    count = CountCodesInList("BJH VS Sitter", Searchpullrange, CountAll, False, found_what)
'    If g_range = 720 Then '12 hr pull
'        If count >= 1 Then SetInd 12, found_what
'    ElseIf g_range = 1440 Then '24 hr pull
'        If count >= 2 Then SetInd 12, found_what
'    End If
    
' Removed 4/6/12
'    countlow = CountSimpleResult("BJH AS Fall Risk Score Calc2", opeq, "", opnone, "2", OPLTE, , False, found_what)
'    counthi = CountSimpleResult("BJH AS Fall Risk Score Calc2", opeq, "", opnone, "99", OPLTE, , False, found_what)
'    If Not (countlow = 0 And counthi = 0) Then
'        If countlow >= 1 Then
'            'no11or12 = True
'            dvprint "Fall Risk Score of 2 or lower exists"
'        End If
'        If counthi > countlow Then
'            SetInd 12, found_what
'            dvprint "Fall Risk Score of 3 or higher exists"
'        End If
'    End If

' Added 4/6/12
'    countlow = CountSpecialResult("BJH AS FRA Total CALC", opeq, "", opnone, "6", OPGTE, , False, found_what)
'    If countlow >= 1 Then
'        SetInd 11, found_what
'        dvprint "Fall Risk Score of 6 or greater exists"
'    End If
    
'BJH AS FRA Initial RD with complete IN DICTIONARY  4/6/12
    
'    SetIndicator (12)
    
    'If no11or12 And Not (inds(11).checked Or inds(12).checked) Then
    If no11or12 Then
        inds(11).checked = False
        inds(12).checked = False
        Exit Sub
    End If
    
    SetIndIfResultContains 12, "BJH AS Psy Precautions", opeq, "", opeq, "suicide", oplike
    SetIndIfResultContains 12, "BJH AS Psy Behaviors High Risk Suicide", opeq, "", opeq, "", oplike
    SetIndIfResultContains 12, "BJH VS Precautions", opeq, "", opeq, "suicide", oplike
    SetIndIfResultContains 12, "BJH VS Sitter", opeq, "", opeq, "", oplike
    
    If inds(12).checked Then Exit Sub
    
    SetIndIfResultContains 12, "BJH AS Psy INV Monitorng", opeq, "", opeq, "", oplike
    If inds(12).checked Then Exit Sub
    
'"BJH VS Alarms On Off"
    count = CountSimpleResult("BJH VS Alarms On Off", opeq, "", opnone, "on", oplike, , False, found_what)
    If count >= 48 Then
        SetInd 12, found_what
    ElseIf count >= 12 Then
        SetInd 11, found_what
    End If
    If inds(12).checked Then Exit Sub
    count = CountSimpleResult("BJH VS Alarms Portable On", opeq, "", opnone, "on", oplike, , False, found_what)
    If count >= 48 Then
        SetInd 12, found_what
    ElseIf count >= 12 Then
        SetInd 11, found_what
    End If
    If inds(12).checked Then Exit Sub
    count = CountResultNotInList("BJH VS Bed Alarm", opeq, "", opnone, "off,n/a", opnotlike, searchpullrange, countall, False, found_what)
    If count >= 48 Then
        SetInd 12, found_what
    ElseIf count >= 12 Then
        SetInd 11, found_what
    End If
    If inds(12).checked Then Exit Sub
    
'"BJH AS FRA Initial RD"
    SetIndIfResultContains 12, "BJH AS FRA Initial RD", opeq, "auto high", oplike, "", opnone
'"BJH AS FRA Int Level RD"
    SetIndIfResultContains 12, "BJH AS FRA Int Level RD", opeq, "total_high", oplike, "", opnone

'    count = CountDescInList("BJH Res Restraint Reason", opeq, "Risk of injury,Disruptive/dangerous", oplike, , , , found_what)
'    count = CountSimpleResult("BJH Res Restraint Reason", opeq, "", opnone, "interference with medical treatment", opnotlike, , False, found_what)
'    If g_range = 720 Then '12 hr pull
'        If count >= 1 Then
'            SetInd 12, found_what
'        End If
'    ElseIf g_range = 1440 Then '24 hr pull
'        If count >= 2 Then
'            SetInd 12, found_what
'        End If
'    End If

    If inds(12).checked Then Exit Sub
    If inds(11).checked Then Exit Sub
    

    SetIndIfResultContains 11, "BJH Res Restraint Reason", opeq, "", opeq, "", oplike
    SetIndIfResultContains 11, "BJH AS FRA Initial RD", opeq, "complete paralysis", oplike, "", opnone
    SetIndIfResultContains 11, "BJH AS FRA Int Level RD", opeq, "total_mod", oplike, "", opnone


'Placed in dictionary -- only one count needed for either of these
'    count = CountDescInList("BJH Res Restraint Reason", opeq, "Risk of injury to patient/others", oplike, , , , found_what2) + _
'            CountDescInList("BJH Res Restraint Reason", opeq, "Disruptive/dangerous", oplike, , , , found_what3)
'    found_what = found_what1 & vbCrLf & found_what2 & vbCrLf & found_what3
'    If g_range = 720 Then '12 hr pull
'        If count >= 1 Then SetInd 11, found_what
'        If count >= 2 Then SetInd 12, found_what
'    ElseIf g_range = 1440 Then '24 hr pull
'        If count >= 2 Then SetInd 11, found_what
'        If count >= 4 Then SetInd 12, found_what
'    End If
    
'removed MAR19 2013
'    If inds(12).checked Then Exit Sub
'    count = CountDescInList("BJH AS Fall/Injury Interventionss Confusion", opeq, "Sitter", oplike, , , , found_what)
'    If g_range = 720 Then '12 hr pull
'        If count >= 1 Then SetInd 12, found_what
'    ElseIf g_range = 1440 Then '24 hr pull
'        If count >= 2 Then SetInd 12, found_what
'    End If
'
'    If inds(12).checked Then Exit Sub
'    count = CountDescInList("BJH AS Psy INV Monitorng", opeq, "Psychosocial Intervention Monitoring", oplike, , , , found_what)
'    If g_range = 720 Then '12 hr pull
'        If count >= 1 Then SetInd 12, found_what
'    ElseIf g_range = 1440 Then '24 hr pull
'        If count >= 2 Then SetInd 12, found_what
'    End If
    
'    If inds(12).checked Then Exit Sub
'    count = CountDescInList("BJH VS Restraint Alternatives", oplike, "1:1 Violent Self-destructive", oplike, , , , found_what) + _
'            CountDescInList("BJH Res Restraint Reason", opeq, "Risk of injury to patient/others,Disruptive/dangerous", oplike, , , , found_what1) + _
'            CountSimpleResult("BJH VS Room Safety Check", opeq, "", opnone, "Done", oplike, , False, found_what2) + _
'            CountSimpleResult("BJH VS Patient Safety Rounds", opeq, "", opnone, "Done", oplike, , False, found_what3)
'    found_what = found_what & vbCrLf & found_what1 & vbCrLf & found_what2 & vbCrLf & found_what3
'    If count >= 1 Then SetInd 12, found_what
    
'removed mar19 2013
'    If inds(12).checked Then Exit Sub
'    count = CountDescInList("BJH VS Restraint Alternatives LS", opeq, "staff", oplike, , , , found_what)
'    If g_range = 720 Then '12 hr pull
'        If count >= 24 Then inds(12).checked = True
'    ElseIf g_range = 1440 Then '24 hr pull
'        If count >= 48 Then inds(12).checked = True
'    End If
'
'    If inds(12).checked Then Exit Sub
'    count = CountDescInList("BJH VS Restraint Alternatives LS", opeq, "1:1 violent", oplike, , , , found_what)
'    If g_range = 720 Then '12 hr pull
'        If count >= 24 Then inds(12).checked = True
'    ElseIf g_range = 1440 Then '24 hr pull
'        If count >= 48 Then inds(12).checked = True
'    End If
'
'    If inds(12).checked Or inds(11).checked Then Exit Sub
'    count = CountDescInList("BJH AS Fall Risk Interventions", opeq, "Hourly Rounds in place", oplike, , , , found_what)
'    If g_range = 720 Then '12 hr pull
'        If count >= 1 Then SetInd 11, found_what
'    ElseIf g_range = 1440 Then '24 hr pull
'        If count >= 2 Then SetInd 11, found_what
'    End If
'
'    If inds(12).checked Then Exit Sub
'    count = CountSimpleResult("BJH VS Patient Safety Rounds", opeq, "", opnone, "Done", oplike, , False, found_what)
'    If g_range = 720 Then '12 hr pull
'        If count >= 12 Then
'            SetInd 12, found_what
'        ElseIf count >= 4 Then
'            SetInd 11, found_what
'        End If
'    ElseIf g_range = 1440 Then '24 hr pull
'        If count >= 24 Then
'            SetInd 12, found_what
'        ElseIf count >= 4 Then
'            SetInd 11, found_what
'        End If
'    End If
'
'    If inds(12).checked Then Exit Sub
'    count = CountSimpleResult("BJH VS Room Safety Check", opeq, "", opnone, "Done", oplike, , False, found_what)
'    If g_range = 720 Then '12 hr pull
'        If count >= 12 Then
'            SetInd 12, found_what
'        ElseIf count >= 4 Then
'            SetInd 11, found_what
'        End If
'    ElseIf g_range = 1440 Then '24 hr pull
'        If count >= 24 Then
'            SetInd 12, found_what
'        ElseIf count >= 4 Then
'            SetInd 11, found_what
'        End If
'    End If
'
'    If inds(12).checked Then Exit Sub
'    count = CountDescInList("BJH Res Restraint Reason", opeq, "Risk of injury,Disruptive/dangerous", oplike, , , , found_what)
'    If g_range = 720 Then '12 hr pull
'        If count >= 1 Then
'            SetInd 12, found_what
'        End If
'    ElseIf g_range = 1440 Then '24 hr pull
'        If count >= 2 Then
'            SetInd 12, found_what
'        End If
'    End If
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_11_12"
    Resume  'debug
End Sub

Private Sub Check_13()
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "13. Isolation"
    dvprint "---------------"
    
    SetIndIfResultContains 13, "BJH AS Isolation Status", opeq, "", opeq, "initiated,continued", oplike
'    SetIndicator (13)
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_13"
    Resume  'debug
End Sub

Private Sub SetAssessmentIndicator(count As Integer, found_what As String)
    Dim los_hours As Single

    If (inds(17).checked) Then Exit Sub             'skip if highest already checked
    If (count = 0) Then Exit Sub                    'skip if none

    'Here's the mathematical way:
    '    freq_min = g_util.DivideWithoutError(m_pat.range, count)
    'The book, however, says that you did this frequency for the majority of the time period.
    'This could mean that you did it for only 51% of the period.  Use a lookup table instead.
    
    'los_hours = m_pat.range / 60#
'    If g_range = 720 Then
'        If count >= 24 Then
'            SetInd 17, found_what
'        ElseIf count >= 12 Then
'            SetInd 16, found_what
'        ElseIf count >= 6 Then
'            SetInd 15, found_what
'        ElseIf count >= 3 Then
'            SetInd 14, found_what
'        End If
'    ElseIf g_range = 1440 Then
        If count >= 48 Then
            SetInd 17, found_what
        ElseIf count >= 24 Then
            SetInd 16, found_what
        ElseIf count >= 12 Then
            SetInd 15, found_what
        ElseIf count >= 6 Then
            SetInd 14, found_what
        End If
'    End If

End Sub
Private Sub QAssessSetExclusionTimeRange(c As String)
    Dim rs As New Recordset
    Dim sql As String
    Dim minpfdt, maxpfdt As Date
    Dim and_filter As String
        
    exclusionRange = ""
    minpfdt = #1/1/1900#
    maxpfdt = minpfdt
    
    and_filter = AndSimpleItemFilter(c, oplike, "", opnone, "", opnone)
    sql = "select min(event_datetime) from chart_item" & WhereBase() & and_filter
'    sql = sql & " and perfdt>" & g_dbutil.SQL_DateTime(p)
    rs.Open sql, g_cnADO
    If Not IsNull(rs(0)) Then minpfdt = rs(0)
    rs.Close
    
    If minpfdt = #1/1/1900# Then Exit Sub
    
    sql = "select max(event_datetime) from chart_item" & WhereBase() & and_filter
    rs.Open sql, g_cnADO
    If Not IsNull(rs(0)) Then maxpfdt = rs(0)
    rs.Close
    Set rs = Nothing
    
    If maxpfdt = #1/1/1900# Or (minpfdt = maxpfdt) Then Exit Sub
    
    exclusionRange = " and event_datetime NOT between " & g_dbutil.SQL_DateTime(minpfdt) & " and " & g_dbutil.SQL_DateTime(maxpfdt)

End Sub

Private Sub Check_14_15_16_17()
    Dim found_what As String
    Dim qmins As Integer
    Dim count As Integer
    Dim return_result As String
    Dim codelist As String
    Dim inp As Integer
    Dim out As Integer
    
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "14. Assessment q4h"
    dvprint "15. Assessment q2h"
    dvprint "16. Assessment q1h"
    dvprint "17. Assessment q30min"
    dvprint "---------------"
    
    'QAssessSetExclusionTimeRange ("BJH VS Blood Pump Flow") 'sets exclusionRange due to Dialysis
    
    'count distinct event_datetime where codelist like and exclusionrange.
    count = CountCodesInList("BJH VS Temp", searchpullrange, countall, False, found_what)
    SetAssessmentIndicator count, found_what
   
    If inds(17).checked Then Exit Sub
    count = CountCodesInList("BJH VS Pulse Rate,BJH VS Heart Rate,BJH VS Paced rhythm", searchpullrange, countall, False, found_what)
    SetAssessmentIndicator count, found_what
    
    If inds(17).checked Then Exit Sub
    count = CountCodesInList("BJH VS Noninv BP,BJH VS Art bp,BJH VS IABP Systolic,BJH VS IABP Mean", searchpullrange, countall, False, found_what)
    SetAssessmentIndicator count, found_what
    
    If inds(17).checked Then Exit Sub
    count = CountCodesInList("BJH VS Resp Rate,BJH VS SPO2,BJH VS PulmInter", searchpullrange, countall, False, found_what)
    SetAssessmentIndicator count, found_what

    If inds(17).checked Then Exit Sub
    count = CountCodesInList("BJH VS Noninv BP,BJH VS Art bp,BJH VS IABP Systolic", searchpullrange, countall, False, found_what)
    SetAssessmentIndicator count, found_what

    If inds(17).checked Then Exit Sub
    count = CountCodesInList("BJH VS ICP monitor,BJH VS LOC,BJH VS RassSedationScale", searchpullrange, countall, False, found_what)
    SetAssessmentIndicator count, found_what
 
    If inds(17).checked Then Exit Sub
    count = CountCodesInList("BJH VS Distal Pulse Assessment Site,BJH VS Perf circ temp,BJH VS Cessium pt visualization,BJH VS Flap", searchpullrange, countall, False, found_what)
    SetAssessmentIndicator count, found_what
 


    If inds(17).checked Then Exit Sub
    count = CountCodesInList("BJH VS Blood Pump Flowrate", searchpullrange, countall, False, found_what)
    SetAssessmentIndicator count, found_what
   
    If inds(17).checked Then Exit Sub
    count = CountCodesInList("BJH VS TPE Blood Flow", searchpullrange, countall, False, found_what)
    SetAssessmentIndicator count, found_what
    
    If inds(17).checked Then Exit Sub
    count = CountCodesInList("BJH VS Dialysis", searchpullrange, countall, False, found_what)
    SetAssessmentIndicator count, found_what
    
    If inds(17).checked Then Exit Sub
    count = CountCodesInList("BJH VS PAP,BJH VS Hemo CO,BJH VS Noninv CO Mon Type,BJH VS Bladder Press", searchpullrange, countall, False, found_what)
    SetAssessmentIndicator count, found_what
    
    
    
    If inds(17).checked Then Exit Sub
    count = CountCodesInList("BJH VS ACT,BJH VS Glucose BS", searchpullrange, countall, False, found_what)
    SetAssessmentIndicator count, found_what
    
    
    If inds(17).checked Then Exit Sub
    count = CountCodesInList("BJH VS Pain Intensity,BJH VS Acute Pain Intensity 2,BJH VS Chronic Pain Intensity 2,BJH VS Chest Pain Intensity", searchpullrange, countall, False, found_what)
    SetAssessmentIndicator count, found_what
    
    If inds(17).checked Then Exit Sub
    codelist = "VS-DRIPS"
    count = CountCodesInList(codelist, searchpullrange, countall, False, found_what)
    SetAssessmentIndicator count, found_what
  
    If inds(17).checked Then Exit Sub
'Intake:
    codelist = "INTAKE-,Miscellaneous-Intake"
    inp = CountCodesInList(codelist, searchpullrange, countall, False, found_what)
'Output: "OUTPUT-,Miscellaneous-Output"
    codelist = "OUTPUT-,Miscellaneous-Output"
    out = CountCodesInList(codelist, searchpullrange, countall, False, found_what)
    
    If inp >= out Then
        count = out
    Else
        count = inp
    End If
    SetAssessmentIndicator count, found_what

    If inds(17).checked Then Exit Sub
    codelist = "NET-Dialysis,NET-Hemodialysis,NET-Lavage/Irrigation,NET-Pheresis"
    count = CountCodesInList(codelist, searchpullrange, countall, False, found_what)
    SetAssessmentIndicator count, found_what
    
    exclusionRange = ""
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_14_15_16_17"
    Resume  'debug
End Sub

Private Sub Check_18()
    Dim found_what As String
    Dim qmins As Integer
    Dim count As Integer
    Dim return_result As String
    Dim codelist As String
    Dim inp As Integer
    Dim out As Integer
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "18. Medication Preparation >= 20 Minutes"
    dvprint "---------------"
    
    codelist = "INTAKE-Blood Products"
    count = CountCodesInList(codelist, searchpullrange, countall, False, found_what)
    If count >= 1 Then SetInd 18, found_what

    If inds(18).checked Then Exit Sub
    codelist = "VS-DRIPS"
    count = CountCodesInList(codelist, searchpullrange, countall, False, found_what)
    If count >= 1 Then SetInd 18, found_what

    If inds(18).checked Then Exit Sub
    codelist = "INTAKE-Colloids"
    count = CountCodesInList(codelist, searchpullrange, countall, False, found_what)
    If count >= 1 Then SetInd 18, found_what
    
    If inds(18).checked Then Exit Sub
    codelist = "INTAKE-Transplant"
    count = CountCodesInList(codelist, searchpullrange, countall, False, found_what)
    If count >= 1 Then SetInd 18, found_what
'    SetIndicator (18)

    If inds(18).checked Then Exit Sub
    codelist = "INTAKE-Small Volume Parenteral"
    count = CountCodesInList(codelist, searchpullrange, countall, False, found_what)
    If count >= 1 Then SetInd 18, found_what


    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_18"
    Resume  'debug
End Sub

Private Sub Check_19_20()
    Dim return_result As String
    Dim found_what As String
    Dim count As Integer
    Dim codelist As String
    
    
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "19. Wound/Injury Mgmt"
    dvprint "20. Wound/Injury Mgmt >= 30 Minutes"
    dvprint "---------------"

    SetIndIfResultContains 20, "BJH AS Central Line 1  Type", opeq, "", opeq, "Venous sheath", oplike
    SetIndIfResultContains 20, "BJH AS Central Line 1  Type", opeq, "", opeq, "Arterial sheath", oplike

    SetIndIfResultContains 20, "BJH AS Skin Ulc_ Appearance UDF", oplike, "", opeq, "stage iii", oplike
    SetIndIfResultContains 20, "BJH AS Skin Ulc10 Appearance UDF", opeq, "", opeq, "stage iii", oplike
    
    SetIndIfResultContains 20, "BJH WOs Services All", opeq, "", opeq, "", oplike
    SetIndIfResultContains 20, "BJH WOs Surg Debride Reason", opeq, "", opeq, "", oplike
    SetIndIfResultContains 20, "BJH WOs Surg Debride Explanation", opeq, "", opeq, "", oplike

    SetIndIfResultContains 20, "BJH WOs Wound Negative Pressure Dressing", opeq, "", opeq, "changed,applied", oplike
    
    If inds(20).checked Then Exit Sub
    
'codelist = "BJH AS GI Tube_ Site Assessment,BJH AS Tube Site Assessment,BJH AS Gu Ileal-conduit Stoma Appearance,BJH AS GI Tube_ Site Assessment,"
'codelist = codelist & "BJH AS GI Stoma Appearance,BJH Net Bladder Irrigation,BJH OUT Tube NG,BJH OUT Tube Blakemore,BJH AS Resp Chest Tube,BJH AS Resp Stoma Assessment,"
'codelist = codelist & "BJH AS Skin Int_ Care SD,BJH AS Skin Int10 Care SD,BJH AS Skin Wnd_ Care SD,BJH AS Skin Wnd10 Care SD,BJH AS Skin Ulc_ Care SD,BJH AS Skin Ulc10 Care SD,"
'codelist = codelist & "BJH AS JP Drain _ Site,BJH AS Penrose Drain _ Site,BJH AS Hemovac _ Site,BJH AS Tube/Drain Site,BJH AS GUPerit Cath Site SD,"
'codelist = codelist & "BJH AS IV Phlebitis,BJH AS Skin VAD Site Location,BJH AS Skin Oral Mucosa,BJH AS Neuro Monitor Ventric WDL,BJH AS Neuro Lumbar Drain WDL,"
'codelist = codelist & "BJH AS Neuro Monitor Subdural Drain WDL,BJH AS Neuro Monitor Bolt Site Assessment,BJH AS IV Infiltration Grade,BJH AS Central Line _  Dressing Change,"
'codelist = codelist & "BJH AS GI Tube_ Drainage UDF,BJH AS Skin VAD Dressing,BJH AS Resp Chest Tube Dressing,Dressing Securement"
'
'    count = CountUniqueCodesInList(codelist, Searchpullrange, CountAll, False, found_what)
'    If count >= 4 Then
'        dvprint found_what
'        SetInd 20, "At least 4 different wound codes found."
'    ElseIf count = 0 Then
'        Exit Sub
'    End If
        
    
'    SetIndicator (20)
    If inds(20).checked Then Exit Sub
    SetIndIfResultContains 19, "BJH AS GI Tube_ Site Assessment", oplike, "", opeq, "", oplike
    SetIndIfResultContains 19, "BJH AS Tube Site Assessment", opeq, "", opeq, "", oplike
    SetIndIfResultContains 19, "BJH AS Gu Ileal-conduit Stoma Appearance", opeq, "", opeq, "", oplike
    SetIndIfResultContains 19, "BJH AS GI Tube_ Site Assessment", oplike, "", opeq, "", oplike
    
    If inds(19).checked Then Exit Sub

    SetIndIfResultContains 19, "BJH AS Skin Wnd_ Type SD", oplike, "", oplike, "", oplike
    SetIndIfResultContains 19, "BJH AS Skin Wnd_ Care SD", oplike, "", oplike, "", oplike
    SetIndIfResultContains 19, "BJH AS GI Tube_ Drainage UDF", oplike, "", opeq, "bloody,bright red,dark red", oplike

    SetIndIfResultContains 19, "BJH AS GI Stoma Appearance", opeq, "", opeq, "", oplike

    SetIndIfResultContains 19, "BJH Net Bladder Irrigation", oplike, "", opeq, "", oplike
    SetIndIfResultContains 19, "BJH IO Emesis Description", oplike, "coffee ground_clots noted", oplike, "", oplike

    SetIndIfResultContains 19, "BJH OUT Tube NG", oplike, "", opeq, "", oplike
    SetIndIfResultContains 19, "BJH OUT Tube Blakemore", oplike, "", opeq, "", oplike
    
    If inds(19).checked Then Exit Sub

    SetIndIfResultContains 19, "BJH IO GI Stool Color", opeq, "red_melenic", oplike, "", oplike
    
    SetIndIfResultContains 19, "BJH AS Resp Chest Tube", oplike, "", oplike, "", oplike

    SetIndIfResultContains 19, "BJH AS Resp Stoma Assessment", opeq, "", oplike, "", oplike
    SetIndIfResultContains 19, "BJH AS Skin VAD Dressing", opeq, "", oplike, "dry and intact", oplike

    SetIndIfResultContains 19, "BJH AS Resp Chest Tube Dressing", oplike, "", oplike, "Changed per policy and procedure", oplike

    SetIndIfResultContains 19, "BJH AS Skin Int_ Care SD", oplike, "", oplike, "", oplike
    SetIndIfResultContains 19, "BJH AS Skin Int10 Care SD", opeq, "", oplike, "", oplike
    
    If inds(19).checked Then Exit Sub

    SetIndIfResultContains 19, "BJH AS Skin Wnd10 Care SD", opeq, "", oplike, "", oplike
    SetIndIfResultContains 19, "BJH AS Skin Wnd10 Type SD", opeq, "", oplike, "", oplike

    SetIndIfResultContains 19, "BJH AS Skin Ulc_ Care SD", oplike, "", oplike, "", oplike
    SetIndIfResultContains 19, "BJH AS Skin Ulc10 Care SD", opeq, "", oplike, "", oplike

    SetIndIfResultContains 19, "BJH AS JP Drain _ Site", oplike, "", oplike, "", oplike
    SetIndIfResultContains 19, "BJH AS Penrose Drain _ Site", oplike, "", oplike, "", oplike
    
    If inds(19).checked Then Exit Sub


    SetIndIfResultContains 19, "BJH AS Hemovac _ Site", oplike, "", oplike, "", oplike

    SetIndIfResultContains 19, "BJH AS Tube/Drain Site", opeq, "", oplike, "", oplike
    SetIndIfResultContains 19, "BJH AS GUPerit Cath Site SD", opeq, "", oplike, "", oplike
    
    SetIndIfResultContains 19, "BJH AS Epidural UDF", opeq, "capped_infusing", oplike, "", oplike
    SetIndIfResultContains 19, "BJH AS IV Phlebitis", opeq, "", oplike, "", oplike

    SetIndIfResultContains 19, "BJH AS Skin VAD Site Location", opeq, "", oplike, "", oplike

    If inds(19).checked Then Exit Sub
    codelist = "BJH AS IV Infiltration Grade"
    count = CountCodesInList(codelist, searchpullrange, countall, False, found_what)
    If count >= 4 Then SetInd 19, found_what
    
    SetIndIfResultContains 19, "BJH AS Central Line _  Dressing Change", oplike, "", oplike, "gauze,transparent,hemotatic", oplike
    
    SetIndIfResultContains 19, "BJH AS Midline Catheter _ Dressing Securement", oplike, "", oplike, "gauze,transparent,hemotatic", oplike
    SetIndIfResultContains 19, "BJH AS PICC Line _ Dressing Securement", oplike, "", oplike, "gauze,transparent,hemotatic", oplike
    
    If inds(19).checked Then Exit Sub

    SetIndIfResultContains 19, "BJH AS Subcutaneous Infusion Access _ Dressing Securement", oplike, "", oplike, "gauze,transparent,hemotatic", oplike
    SetIndIfResultContains 19, "BJH AS Pulmonary Artery Catheter Dressing Securement", opeq, "", oplike, "gauze,transparent,hemotatic", oplike
    SetIndIfResultContains 19, "BJH AS Arterial Line _ Dressing Securement", oplike, "", oplike, "gauze,transparent,hemotatic", oplike

    If inds(19).checked Then Exit Sub

    SetIndIfResultContains 19, "BJH AS IABP Dressing Securement", opeq, "", oplike, "per protocol", oplike

    SetIndIfResultContains 19, "BJH AS Skin Oral Mucosa", opeq, "", oplike, "", oplike
    SetIndIfResultContains 19, "BJH AS Neuro Monitor Ventric WDL", oplike, "", oplike, "", oplike
    
    If inds(19).checked Then Exit Sub

    SetIndIfResultContains 19, "BJH AS Neuro Lumbar Drain WDL", oplike, "", oplike, "", oplike
    SetIndIfResultContains 19, "BJH AS Neuro Monitor Subdural Drain WDL", oplike, "", oplike, "", oplike
    SetIndIfResultContains 19, "BJH AS Neuro Monitor Bolt Site Assessment", oplike, "", oplike, "", oplike

'    SetIndicator (19)
    
'    If inds(19).checked Then Exit Sub
'    count = CountSimpleResult("BJH Net Bladder Irrigation", oplike, "", opnone, "", opnone, CountAll, False, found_what)
'    If g_range = 720 Then '12 hr pull
'        If count >= 4 Then SetInd 19, found_what
'    ElseIf g_range = 1440 Then '24 hr pull
'        If count >= 8 Then SetInd 19, found_what
'    End If
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_19_20"
    Resume  'debug
End Sub


Private Sub Check_21()
    On Error GoTo errHandler
    
    Dim mins As Integer
    Dim chart_result As String
    
    dvprint "---------------"
    dvprint "21. Healthcare Mgmt Education >= 1 Hour"
    dvprint "---------------"
    
    SetIndIfResultContains 21, "BJHEDU Diabetes Topic", opeq, "", oplike, "Review Topics,Complications,Blood Glucose Monitoring,Insulin Administration", oplike
    SetIndIfResultContains 21, "BJHEDU Respiratory Topic", opeq, "", oplike, "equipment", oplike
    SetIndIfResultContains 21, "BJHEDUNutrDietFeedTopic", opeq, "", oplike, "Diabetes,GI Issues,Gastric Bypass Diet,Tube Feeding", oplike
    SetIndIfResultContains 21, "BJHEDUEquipMedTopic", opeq, "", oplike, "donning/doffing", oplike
    If inds(19).checked Then Exit Sub

    SetIndIfResultContains 21, "BJHEDUDischPlanTopic", opeq, "", oplike, "lap gastric,open gastric", oplike
    
    SetIndIfResultContains 21, "BJHEDUNutrDietFeedTopic", opeq, "", oplike, "Diabetes,GI Issues,Gastric Bypass Diet,Tube Feeding", oplike
    
    SetIndIfResultContains 21, "BJHEDUDischPlanTopic", opeq, "", oplike, "Aneurysm Discharge,Amputation Discharge,Axillo-Femoral Bypass Discharge,Cardiac Surgery Discharge,Carotid Endarterectomy Discharge", oplike
    If inds(19).checked Then Exit Sub

    
    SetIndIfResultContains 21, "BJHEDUDischPlanTopic", opeq, "", oplike, "Discharge Instructions for Heart Surgery Patients,Endovascular Aneurysm Repair Discharge,Femoral Bypass Discharge,Femoral Endarterectomy Discharge", oplike
    
    SetIndIfResultContains 21, "BJHEDUDischPlanTopic", opeq, "", oplike, "Femoro-Femoral Bypass Discharge,Femoro-Popliteal Bypass Discharge,Groin Exploration Discharge,Care of Your Fistula", oplike
    
    SetIndIfResultContains 21, "BJHEDUDischPlanTopic", opeq, "", oplike, "Leg Revascularization Discharge,Thoracic Outlet Syndrome Discharge,Vein Removal Discharge,Vertebral Artery Bypass Discharge", oplike
    If inds(19).checked Then Exit Sub

    
    SetIndIfResultContains 21, "BJHEDUDischPlanTopic", opeq, "", oplike, "Home Care after Allogeneic BMT,Home Care after Chemotherapy,Home Care after High Dose Chemotherapy,BMT discharge class", oplike
    
    SetIndIfResultContains 21, "BJHEDUDischPlanTopic", opeq, "", oplike, "BMT RECOVERY pamphlet,Quiz for Allogeneic Bone Marrow,Reasons to Call After Bone Marrow,Tips on Central Venous Catheter", oplike
    
    SetIndIfResultContains 21, "BJHEDUDischPlanTopic", opeq, "", oplike, "Liver Transplant Recipient Discharge,Kidney Transplant Donor Discharge,Kidney Transplant Recipient Discharge", oplike
    If inds(19).checked Then Exit Sub

    
    SetIndIfResultContains 21, "BJHEDUDischPlanTopic", opeq, "", oplike, "Anorectal Discharge,Discharge Instructions following Colon,Hernia Surgery Discharge,Inguinal Hernia Repair Discharge", oplike
    SetIndIfResultContains 21, "BJHEDUDischPlanTopic", opeq, "", oplike, "Laproscopic Cholecystectomy Discharge,Laproscopic Nissen Fundoplication Discharge,Postoperative Instructions for Abdominal Operations", oplike
    SetIndIfResultContains 21, "BJHEDUDischPlanTopic", opeq, "", oplike, "Postoperative Instructions for Gastric Bypass Surgery,Bladder Neck Suspension Discharge,Kidney Drainage Tube Placed by Interventional Radiology", oplike
    If inds(19).checked Then Exit Sub

    SetIndIfResultContains 21, "BJHEDUDischPlanTopic", opeq, "", oplike, "Mesenteric/Renal Bypass Discharge,Nephrectomy Discharge,Prostate UltraSound and Needle Biopsy of the Prostate", oplike
    SetIndIfResultContains 21, "BJHEDUDischPlanTopic", opeq, "", oplike, "Radical Retropubic Prostatectomy Discharge,TURP Discharge,Urinary Catheter or Foley Care Discharge,ET RN name", oplike
    SetIndIfResultContains 21, "BJHEDUDischPlanTopic", opeq, "", oplike, "Obtaining Ostomy Supplies After Discharge,Ostomy Resources,Ostomy Supply Order Sheet,Ostomy visitor,Supply dealer", oplike


'    SetIndicator (21)

    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_21"
    Resume  'debug
End Sub

Private Sub Check_22()
    On Error GoTo errHandler
'    Dim i As Integer
'    Dim c(11) As String
'    Dim d(11) As String
'    Dim r(11) As String
    Dim start As Integer
    Dim found_what As String
    Dim found_what2 As String
    Dim count As Integer
    Dim prochrs As Boolean
    Dim procperf As Boolean
    
    dvprint "---------------"
    dvprint "22. 1 to 1 Physiological Interv. >= 2 Hours"
    dvprint "---------------"
    
    '5/30/13 change
    SetIndIfResultContains 22, "Nurse_ProcedureGreaterThan2Hours", opeq, "", oplike, "", oplike
    
    
    ' 4/4/11  make RBCs count = 5 within 2 hours to trigger this. 4/4/11  Look at other TCs to see what they use.
'    SetIndIfResultContains 22, "BJH VS Staff Assistance Procedure Location", opeq, "", oplike, "", oplike
'    SetIndIfResultContains 22, "BJH VS Procedure Duration", opeq, "", oplike, "", oplike
'    SetIndIfResultContains 22, "BJH VS ProcDurat", opeq, "", oplike, "2,3,4,5.,6,7,8", oplike
'
'    prochrs = ResultContains("BJH VS ProcDurat", opeq, "", oplike, "2,3,4,5.,6,7,8", oplike, , , , found_what)
'    procperf = ResultContains("BJH VS ProcDurat", opeq, "", oplike, "performed", oplike, , , , found_what2)
'
'    If prochrs And procperf Then
'        SetInd 22, found_what & "+" & found_what2
'    End If


'    count = CountCodesInList("BJH VS Blood Pump Flow,BJH VS TPE Blood Flow", Searchpullrange, CountAll, False, found_what)
'    If count > 0 Then
'        start = 8
'    Else
'        start = 1
'    End If
'
'    c(1) = "BJH VS Heart Rate"
'    c(2) = "BJH VS Noninv BP mon systolic"
'    c(3) = "BJH VS Noninv BP mean"
'    c(4) = "BJH VS Art bp a line systolic (ABP Label)"
'    c(5) = "BJH VS Art bp a line mean (monitor) (ABP Label)"
'    c(6) = "BJH VS Art bp a line systolic (ART Label)"
'    c(7) = "BJH VS Art bp a line mean (monitor) (ART Label)"
'    c(8) = "BJH IN RBCs"
'    c(9) = "BJH IN Plasma Fresh Frozen"
'
'    d(1) = "Heart Rate"
'    d(2) = "Systolic"
'    d(3) = "Mean"
'    d(4) = "Systolic"
'    d(5) = "Mean"
'    d(6) = "Systolic"
'    d(7) = "Mean"
'    d(8) = "RBCs"
'    d(9) = "Plasma Fresh Frozen"
'
'    r(1) = "isnumeric(result)=1 and result >=60 and result <=100"
'    r(2) = "isnumeric(result)=1 and result >=90 and result <=180"
'    r(3) = "isnumeric(result)=1 and result >=50"
'    r(4) = "isnumeric(result)=1 and result >=90 and result <=180"
'    r(5) = "isnumeric(result)=1 and result >=55"
'    r(6) = "isnumeric(result)=1 and result >=90 and result <=180"
'    r(7) = "isnumeric(result)=1 and result >=55"
'    r(8) = ""
'    r(9) = ""
'
'    For i = start To 9
'        If Not inds(22).checked Then
'            Q22 c(i), d(i), r(i)
'        End If
'    Next i
    
    Exit Sub
errHandler:
    g_util.ThrowError "Check_22"
    Resume  'debug
End Sub

Private Sub Q22(c As String, d As String, res As String)
    Dim p As Date
    Dim done As Boolean
    Dim count As Integer
    Dim pmin As Date
    Dim r As Integer
    
'set p=0.
'Loop
'c=count the number of hits with perdt>p.
'if c>=8 then:
' p=get the perfdt of the earliest one>p, add 2 hours to it, and r=count the number found in this range.
' if r>=8 then  inds(22).checked=true
' else c=c-1
'endloop
    pmin = #1/1/1900#
    count = Q22count(c, d, res, pmin)  'count c,d with perdt > p
    done = False
    While Not done
        If count >= 8 Then
            'p= min perfdt where perfdt > p
            pmin = Q22getminp(c, d, res, pmin)
            If pmin <> #1/1/1900# Then
                'r= count c,d with perfdt between p and p+2
                r = Q22rangecount(c, d, res, pmin)
                If r >= 8 Then
                    SetInd 22, "Count=" & r & " of code=" & c & " descr=" & d & " " & res & " within 2 hours."
                    done = True
                End If
            End If
            count = count - 1
        Else
            done = True
        End If
    Wend
    
End Sub
Private Function Q22count(c As String, d As String, res As String, p As Date) As Integer
    Dim rs As New Recordset
    Dim sql As String
    Dim and_filter As String
    
    Q22count = 0
    
    and_filter = AndSimpleItemFilter(c, opeq, d, oplike, "", opnone)
    sql = "select count(*) from chart_item" & WhereBase() & and_filter
    If res <> "" Then
        sql = sql & " and " & res
    End If
    sql = sql & " and event_datetime>" & g_dbutil.SQL_DateTime(p)
    rs.Open sql, g_cnADO
    If rs.RecordCount > 0 Then Q22count = rs(0)
    rs.Close
    Set rs = Nothing

End Function
Private Function Q22getminp(c As String, d As String, res As String, p As Date) As Date
    Dim rs As New Recordset
    Dim sql As String
    Dim and_filter As String
    
    Q22getminp = #1/1/1900#
    
    and_filter = AndSimpleItemFilter(c, opeq, d, oplike, "", opnone)
    sql = "select min(event_datetime) from chart_item" & WhereBase() & and_filter
    If res <> "" Then
        sql = sql & " and " & res
    End If
    sql = sql & " and event_datetime>" & g_dbutil.SQL_DateTime(p)
    rs.Open sql, g_cnADO
    If Not IsNull(rs(0)) Then Q22getminp = rs(0)
    rs.Close
    Set rs = Nothing

End Function
Private Function Q22rangecount(c As String, d As String, res As String, pmin As Date) As Integer
    Dim rs As New Recordset
    Dim sql As String
    Dim pmax As Date
    Dim and_filter As String
    
    Q22rangecount = 0
    pmax = DateAdd("h", 2, pmin)
    
    and_filter = AndSimpleItemFilter(c, opeq, d, oplike, "", opnone)
    sql = "select count(*) from chart_item" & WhereBase() & and_filter
    If res <> "" Then
        sql = sql & " and " & res
    End If
    sql = sql & " and event_datetime between " & g_dbutil.SQL_DateTime(pmin) & " and " & g_dbutil.SQL_DateTime(pmax)
    rs.Open sql, g_cnADO
    If rs.RecordCount > 0 Then Q22rangecount = rs(0)
    rs.Close
    Set rs = Nothing

End Function


Private Function VerifyProcTime(ptime As String, returndt As Date, Optional afterdt As Date = 0) As Boolean
    Dim ok As Boolean
    Dim effdate As String
    Dim efftime As String

    ok = True
    ptime = Trim$(ptime)
    If Len(ptime) > 4 Then
        ok = False
        dvprint ("Procedure time incorrectly formatted: '" & ptime & "'")
    End If
    
    If Not ok Then Exit Function
    
    If Len(ptime) < 4 Then
        dvprint ("Procedure time will be pre-padded with zeroes: '" & ptime & "'")
        ptime = String$(4 - Len(ptime), "0") & ptime
    End If
    
    If IsNumeric(ptime) Then
        If ptime < "2400" And Mid$(ptime, 3, 2) <= "59" Then
            If afterdt = 0 Then 'treat ptime relative to g_effdt
                effdate = Format$(g_effdt, "yyyymmdd")
                efftime = Format$(g_effdt, "hhnn")
                If ptime <= efftime Then
                    returndt = g_util.CDateEx(effdate & ptime)
                Else
                    returndt = g_util.CDateEx(Format$(DateAdd("d", -1, g_util.DateOnly(g_effdt)), "yyyymmdd") & ptime)
                End If
            Else 'treat ptime so that it should be after afterdt
                effdate = Format$(afterdt, "yyyymmdd")
                efftime = Format$(afterdt, "hhnn")
                If ptime >= efftime Then
                    returndt = g_util.CDateEx(effdate & ptime)
                Else
                    returndt = g_util.CDateEx(Format$(DateAdd("d", 1, g_util.DateOnly(afterdt)), "yyyymmdd") & ptime)
                End If
            End If
        Else
            ok = False
            dvprint ("Procedure time not valid military time: '" & ptime & "'")
        End If
    Else
        ok = False
        dvprint ("Procedure time is non-numeric: '" & ptime & "'")
    End If
    
    VerifyProcTime = ok

End Function
Private Function GetDuration(c As String, p As Date) As String  'at event_time p
    Dim rs As New Recordset
    Dim sql As String
    Dim and_filter As String
    
    GetDuration = ""
    and_filter = AndSimpleItemFilter(c, opeq, "", opnone, "", opnone)
    sql = "select result from chart_item" & WhereBase() & and_filter
    sql = sql & " and event_datetime=" & g_dbutil.SQL_DateTime(p)
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        If Not IsNull(rs(0)) Then GetDuration = rs(0)
    End If
    rs.Close
    Set rs = Nothing

End Function

Private Sub ProcessProc()
    Dim pmintime As String
    Dim stime As String
    Dim etime As String
    Dim pmin As Date
    Dim mins As Integer
    Dim found_what As String
    Dim dur As String

'    If ResultContains("BJH VS Staff Assistance Procedure Location", opeq, "", opnone, "Nurse", oplike, , , False, found_what) Then
'        'if dur time coexists then
'        pmin = QProcgetminp("BJH VS Staff Assistance Procedure Location", "Nurse")
'        pmintime = Format$(pmin, "hhnn")
'        dvprint "Found code=BJH VS Staff Assistance Procedure Location, result=Nurse at " & pmintime
'        pminRN = pmin
'    End If
'    If ResultContains("BJH VS Staff Assistance Procedure Location", opeq, "", opnone, "Technician", oplike, , , False, found_what) Then
'        'if dur time coexists then
'        pmin = QProcgetminp("BJH VS Staff Assistance Procedure Location", "Technician")
'        pmintime = Format$(pmin, "hhnn")
'        dvprint "Found code=BJH VS Staff Assistance Procedure Location, result=Technician at " & pmintime
'        pminNONRN = pmin
'    End If
'    If ResultContains("BJH VS Staff Assistance Procedure Location", opeq, "", opnone, "performed on", oplike, , , False, found_what) Then
'        'if dur time coexists then
'        If QProcgeteqp("BJH VS Staff Assistance Procedure Location", "performed on", pminRN) Then
'            dvprint "Found result=performed ON"
'            dur = GetDuration("BJH VS Procedure Duration", pminRN)
'            If dur = "Less than 30 minutes" Then
'                ONunitRNmins = 0
'            Else
'                ONunitRNmins = val(Mid$(dur, 1, 3)) * 60#
'            End If
'        End If
'    End If
'    If ResultContains("BJH VS Staff Assistance Procedure Location", opeq, "", opnone, "performed off", oplike, , , False, found_what) Then
'        'if dur time coexists then
'        If QProcgeteqp("BJH VS Staff Assistance Procedure Location", "performed off", pminRN) Then
'            dur = GetDuration("BJH VS Procedure Duration", pminRN)
'            If dur = "Less than 30 minutes" Then
'                OFFunitRNmins = 0
'            Else
'                OFFunitRNmins = val(Mid$(dur, 1, 3)) * 60#
'            End If
'        End If
'        If QProcgeteqp("BJH VS Staff Assistance Procedure Location", "performed off", pminNONRN) Then
'            dur = GetDuration("BJH VS Procedure Duration", pminNONRN)
'            If dur = "Less than 30 minutes" Then
'                OFFunitNONRNmins = 0
'            Else
'                OFFunitNONRNmins = val(Mid$(dur, 1, 3)) * 60#
'            End If
'        End If
'    End If

End Sub

Private Sub CheckProcs()
    Dim return_result As String
    Dim proc8, proc9 As Boolean
    Dim pminRNtime As String
    Dim pminNONRNtime As String
    
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "P1. 1-1 safety observation by non-RN"
    dvprint "---------------"
    
    ProcessProc1
    
'    dvprint "---------------"
'    dvprint "Basis for other procedures"
'    dvprint "---------------"
    
    'ProcessProc

    dvprint "---------------"
    dvprint "P2. Off unit accompanied by RN"
    dvprint "---------------"
    
    ProcessProc2
    
'    If OFFunitRNmins > 0 Then
'        If QProcgeteqp("BJH VS Proced Events 02", "", pminRN) Then
'            If pminRN <> #1/1/1900# Then
'                'procedure 2, start=pmin, end=pmin+dur
'                numprocs = numprocs + 1
'                procs(numprocs).pnum = 2
'                procs(numprocs).start = pminRN
'                procs(numprocs).finish = DateAdd("n", OFFunitRNmins, pminRN)
'                pminRNtime = Format$(pminRN, "hhnn")
'                dvprint "Procedure 2 triggered: Found RN Off-unit minutes=" & OFFunitRNmins & " at " & pminRNtime
'            End If
'        End If
'    End If
'2013-01-16 18:45:00 Nurse_ProcedureOffUnit  Nurse_ProcedureOffUnit  3.0 hours 14351 Nurse Procedure performed off nusing unit
'701601977               10500   1993-04-23 00:00:00 ANTHONYJJAIME   2013-01-16 07:00:00 Nurse_ProcedureOffUnit  Nurse_ProcedureOffUnit  Nurse Procedure performed off nusing unit
'701657695               8200 ICU    1952-10-11 00:00:00 THOMASPFOGARTY  2013-01-16 07:24:00 Nurse_ProcedureOffUnit  Nurse_ProcedureOffUnit  0.5 hours Nurse Procedure performed off nusing unit
    
    dvprint "---------------"
    dvprint "P3. Off unit accompanied by non-RN"
    dvprint "---------------"
    
'    If OFFunitNONRNmins > 0 Then
'        If QProcgeteqp("BJH VS Proced Events 02", "", pminNONRN) Then
'            If pminNONRN <> #1/1/1900# Then
'                'procedure 3, start=pmin, end=pmin+dur
'                numprocs = numprocs + 1
'                procs(numprocs).pnum = 3
'                procs(numprocs).start = pminNONRN
'                procs(numprocs).finish = DateAdd("n", OFFunitNONRNmins, pminNONRN)
'                pminNONRNtime = Format$(pminNONRN, "hhnn")
'                dvprint "Procedure 3 triggered: Found Tech Off-unit minutes=" & OFFunitNONRNmins & " at " & pminNONRNtime
'            End If
'        End If
'    End If
    
'    dvprint "---------------"
'    dvprint "P4. Patient/family education by RN"
'    dvprint "---------------"
    
    'ProcessProc 4, "OLACUITY21", "OLACUITY45", "OLACUITY46"

'    dvprint "---------------"
'    dvprint "P5. Extensive wound management by RN"
'    dvprint "---------------"
    
    'ProcessProc 5, "OLACUITY22", "OLACUITY34", "OLACUITY35"

'    dvprint "---------------"
'    dvprint "P6. Extensive wound management by non-RN"
'    dvprint "---------------"
    
    'ProcessProc 6, "OLACUITY23", "OLACUITY36", "OLACUITY37"

'    dvprint "---------------"
'    dvprint "P7. Coordination of care by RN"
'    dvprint "---------------"
    
    'ProcessProc 7, "OLACUITY24", "OLACUITY43", "OLACUITY44"

    dvprint "---------------"
    dvprint "P8&P9. 1-1 or 2-1 by RN at bedside"
    dvprint "---------------"
    ProcessProc8
'701955564               8300 ICU    1901-01-01 00:00:00 JANE-130371DOE  2013-05-21 02:17:00 BJH Code Discovery Time Discovery_Discovery Time    02:15
'9100    1966-06-27 00:00:00 TINAMJONES  2013-05-22 15:38:00 BJH Code Discovery Time Discovery_Discovery Time    15:39
'701944949               9100    1966-06-27 00:00:00 TINAMJONES  2013-05-22 15:38:00 BJH Code Time 22700 Called  Discovery_Time 22700 Called 15:39
'701944949               9100    1966-06-27 00:00:00 TINAMJONES  2013-05-22 15:38:00 BJH Code Time Of First Assist Ventilation   Airway/Breathing_Time of First Assist Ventilation   15:43
'701944949               9100    1966-06-27 00:00:00 TINAMJONES  2013-05-22 15:38:00 BJH Code Time Of First Chest Compressions   Circulation_Time of First Chest Compressions    15:39
'BJH Code Discovery Time
'BJH Code Time 22700 Called
'BJH Code Time Of First Assist Ventilation
'BJH Code Time Of First Chest Compressions
'    If ONunitRNmins > 0 Then
'        If QProcgeteqp("BJH VS Proced Events 02", "", pminRN) Then
'            If pminRN <> #1/1/1900# Then
'                'procedure 2, start=pmin, end=pmin+dur
'                numprocs = numprocs + 1
'                procs(numprocs).pnum = 2
'                procs(numprocs).start = pminRN
'                procs(numprocs).finish = DateAdd("n", ONunitRNmins, pminRN)
'                pminRNtime = Format$(pminRN, "hhnn")
'                dvprint "Procedure 2 triggered: Found RN On-unit minutes=" & ONunitRNmins & " at " & pminRNtime
'            End If
'        End If
'    End If
    
    Exit Sub

errHandler:
    g_util.ThrowError "CheckProcs"
    Resume  'debug
End Sub

Private Function QProcgetminp(c As String, res As String) As Date
    Dim rs As New Recordset
    Dim sql As String
    Dim and_filter As String
    
    QProcgetminp = #1/1/1900#
    
    and_filter = AndSimpleItemFilter(c, opeq, "", opnone, res, oplike)
    sql = "select min(event_datetime) from chart_item" & WhereBase() & and_filter
    sql = sql & " and event_datetime>" & g_dbutil.SQL_DateTime(#1/1/1900#)
    rs.Open sql, g_cnADO
    If Not IsNull(rs(0)) Then QProcgetminp = rs(0)
    rs.Close
    Set rs = Nothing

End Function
Private Function QProcgetmaxp(c As String, res As String) As Date
    Dim rs As New Recordset
    Dim sql As String
    Dim and_filter As String
    
    QProcgetmaxp = DateAdd("d", 1, Date)
    
    and_filter = AndSimpleItemFilter(c, opeq, "", opnone, res, oplike)
    sql = "select max(event_datetime) from chart_item" & WhereBase() & and_filter
    sql = sql & " and event_datetime<" & g_dbutil.SQL_DateTime(DateAdd("d", 1, Date))
    rs.Open sql, g_cnADO
    If Not IsNull(rs(0)) Then QProcgetmaxp = rs(0)
    rs.Close
    Set rs = Nothing

End Function
Private Function QProcgeteqp(c As String, res As String, p As Date) As Boolean
    Dim rs As New Recordset
    Dim sql As String
    Dim and_filter As String
    
    QProcgeteqp = False
    
    and_filter = AndSimpleItemFilter(c, opeq, "", opnone, res, oplike)
    sql = "select count(*) from chart_item" & WhereBase() & and_filter
    sql = sql & " and event_datetime=" & g_dbutil.SQL_DateTime(p)
    rs.Open sql, g_cnADO
    If Not IsNull(rs(0)) Then QProcgeteqp = (rs(0) > 0)
    rs.Close
    Set rs = Nothing

End Function


Private Sub ProcessProc1()
    Dim rs As New Recordset
    Dim sql As String
    Dim nowdt As Date, yesdt As Date
    Dim toddtstr As String, yesdtstr As String
    Dim yes7pmstr As String, tod7amstr As String, tod7pmstr As String
    Dim efftm As String
    '   pull range --------------|
    '<-- 1/3 <---------------------- 1/2
    '3am mid 7pm    3pm   7am    3am mid  7pm   3pm
            
    'yester7am-yester7pm = create yester 7am-7pm
    '>yester7pm =  create yester7pm - today 7am
    
    '   pull range --------------|
    ' <-------------- 1/3 <-----------1/2
    '3pm   7am    3am mid  7pm   3pm
    
    'yester7pm-today7am = create yester 7pm - today 7am
    ' >today 7am = create today 7am-7pm
    
    'IN GENERAL:  eff-20 to eff-8   and >eff-8
    '
    efftm = Format$(g_effdt, "hhnn")
    
    nowdt = Now
    yesdt = DateAdd("d", -1, nowdt)
    toddtstr = Format$(nowdt, "yyyymmdd")
    yesdtstr = Format$(yesdt, "yyyymmdd")
    yes7pmstr = yesdtstr & "1900"
    tod7amstr = toddtstr & "0700"
    tod7pmstr = toddtstr & "1900"
    
    If efftm = "0300" Then
    sql = "select count(distinct(event_datetime)) from chart_item" & WHERE_ENCOUNTER & AND_UNIT & " and code='BJH VS Sitter'"
    sql = sql & " and event_datetime between " & g_dbutil.SQL_DateTime(g_util.CDateEx(yes7pmstr)) & " and " & g_dbutil.SQL_DateTime(g_util.CDateEx(tod7amstr))
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
    If rs(0) >= 6 Then
        numprocs = numprocs + 1
        procs(numprocs).pnum = 1
        procs(numprocs).start = g_util.CDateEx(yes7pmstr)
        procs(numprocs).finish = g_util.CDateEx(tod7amstr)
        dvprint "Procedure 1 triggered: Found at least 6 BJH VS Sitter."
    End If
    End If
    rs.Close
    End If
    
    If efftm = "1500" Then
    sql = "select count(distinct(event_datetime)) from chart_item" & WHERE_ENCOUNTER & AND_UNIT & " and code='BJH VS Sitter'"
    sql = sql & " and event_datetime between " & g_dbutil.SQL_DateTime(g_util.CDateEx(tod7amstr)) & " and " & g_dbutil.SQL_DateTime(g_util.CDateEx(tod7pmstr))
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
    If rs(0) >= 6 Then
        numprocs = numprocs + 1
        procs(numprocs).pnum = 1
        procs(numprocs).start = g_util.CDateEx(tod7amstr)
        procs(numprocs).finish = g_util.CDateEx(tod7pmstr)
        dvprint "Procedure 1 triggered: Found at least 6 BJH VS Sitter."
    End If
    End If
    rs.Close
    End If

    Set rs = Nothing
    

'    If ResultContains("BJH AS Fall/Injury Interventionss Confusion", opeq, "", opnone, "sitter", oplike, , , False, found_what) Then
'        pmin = QProcgetminp("BJH AS Fall/Injury Interventionss Confusion", "Sitter")
'        pmax = QProcgetmaxp("BJH AS Fall/Injury Interventionss Confusion", "Sitter")
'
'        pmindate = Format$(pmin, "yyyymmdd")
'        pmintime = Format$(pmin, "hhnn")
'        pmaxdate = Format$(pmax, "yyyymmdd")
'        pmaxtime = Format$(pmax, "hhnn")
'
'        yesterd = Format$(DateAdd("d", -1, Date), "yyyymmdd")
'        todayd = Format$(Date, "yyyymmdd")
'
'        If pmindate & pmintime >= yesterd & "0700" And pmindate & pmintime < yesterd & "1900" Then 'proc yester 7a7p
'            numprocs = numprocs + 1
'            procs(numprocs).pnum = 1
'            procs(numprocs).start = g_util.CDateEx(yesterd & "0700")
'            procs(numprocs).finish = g_util.CDateEx(yesterd & "1900")
'            dvprint "Procedure 1 triggered: Found code=BJH AS Fall/Injury Interventionss Confusion, result=Sitter at " & pmindate & pmintime
'        End If
'        If pmaxdate & pmaxtime >= yesterd & "1900" And pmaxdate & pmaxtime < todayd & "0700" Then 'proc yester 7p7a
'            numprocs = numprocs + 1
'            procs(numprocs).pnum = 1
'            procs(numprocs).start = g_util.CDateEx(yesterd & "1900")
'            procs(numprocs).finish = g_util.CDateEx(todayd & "0700")
'            dvprint "Procedure 1 triggered: Found code=BJH AS Fall/Injury Interventionss Confusion, result=Sitter at " & pmaxdate & pmaxtime
'        End If
'    End If


End Sub
Private Sub ProcessProc2()
    Dim rs As New Recordset
    Dim sql As String
    Dim hrpos As Integer
    Dim numhrs As Single
    Dim nummins As Integer
    Dim res As String
    Dim efftm As String
'2013-01-16 18:45:00 Nurse_ProcedureOffUnit  Nurse_ProcedureOffUnit  3.0 hours 14351 Nurse Procedure performed off nusing unit
    
    If Format$(g_effdt, "hhnn") <> "0300" Then Exit Sub
    sql = "select event_datetime,result from chart_item" & WhereBase() & " and code='Nurse_ProcedureOffUnit'"
    sql = sql & " and result like '%nurse%' and result like '%performed off%'"
    rs.Open sql, g_cnADO
    
    Do While Not rs.EOF
        If Not IsNull(rs(0)) Then
            res = rs(1)
            hrpos = InStr(1, res, "hour", vbTextCompare)
            If hrpos > 0 Then
                numhrs = CSng(Mid$(res, 1, hrpos - 1))
            Else
                numhrs = 1
            End If
            If numhrs >= 1# Then
                nummins = CInt(numhrs * 60)
                numprocs = numprocs + 1
                procs(numprocs).pnum = 2
                procs(numprocs).start = rs(0)
                procs(numprocs).finish = DateAdd("n", nummins, rs(0))
                dvprint "Procedure 2 triggered: Found code=Nurse_ProcedureOffUnit at " & g_dbutil.SQL_DateTime(rs(0)) & " for " & numhrs & " hours."
            End If
            
        End If
        rs.MoveNext
    Loop
    rs.Close
    
    Set rs = Nothing
    
End Sub
Private Sub ProcessProc8()
    Dim rs As New Recordset
    Dim sql As String
    Dim hrpos As Integer
    Dim numhrs As Single
    Dim nummins As Integer
    Dim res As String
    Dim efftm As String
    
'BJH Code Discovery Time
'BJH Code Time 22700 Called
'BJH Code Time Of First Assist Ventilation
'BJH Code Time Of First Chest Compressions
'9100    1966-06-27 00:00:00 TINAMJONES  2013-05-22 15:38:00 BJH Code Discovery Time Discovery_Discovery Time    15:39
'701944949               9100    1966-06-27 00:00:00 TINAMJONES  2013-05-22 15:38:00 BJH Code Time 22700 Called  Discovery_Time 22700 Called 15:39
'701944949               9100    1966-06-27 00:00:00 TINAMJONES  2013-05-22 15:38:00 BJH Code Time Of First Assist Ventilation   Airway/Breathing_Time of First Assist Ventilation   15:43
'701944949               9100    1966-06-27 00:00:00 TINAMJONES  2013-05-22 15:38:00 BJH Code Time Of First Chest Compressions   Circulation_Time of First Chest Compressions    15:39
    
    
    If Format$(g_effdt, "hhnn") <> "0300" Then Exit Sub
    sql = "select max(event_datetime) from chart_item" & WhereBase() & " and code in "
    sql = sql & " ('BJH Code Discovery Time','BJH Code Time 22700 Called',"
    sql = sql & "'BJH Code Time Of First Assist Ventilation','BJH Code Time Of First Chest Compressions')"
    rs.Open sql, g_cnADO
    
    Do While Not rs.EOF
        If Not IsNull(rs(0)) Then
            numprocs = numprocs + 1
            procs(numprocs).pnum = 8
            procs(numprocs).start = rs(0)
            procs(numprocs).finish = DateAdd("n", 60, rs(0))
            dvprint "Procedure 8 triggered: CODE at " & g_dbutil.SQL_DateTime(rs(0))
        End If
        rs.MoveNext
    Loop
    rs.Close
    
    Set rs = Nothing
    
End Sub


Private Sub CheckOutcomes()
    Dim return_result As String
    
    'How to handle multiple outcomes per pt, each with different times?
'    dvprint "---------------"
'    dvprint "Outcomes: FALL"
'    dvprint "---------------"
'    If GetResult("", "FALL", "", "", return_result, SearchOutcomesRange) Then
'        numoutcomes = numoutcomes + 1
'        With oc(numoutcomes)
'            .checked = True
'            .pnum = 1
'            .start = return_result
'        End With
'    End If
'
'    dvprint "---------------"
'    dvprint "Outcomes: MED"
'    dvprint "---------------"
'    If GetResult("", "MED", "", "", return_result, SearchOutcomesRange) Then
'        numoutcomes = numoutcomes + 1
'        With oc(numoutcomes)
'            .checked = True
'            .pnum = 2
'            .start = return_result
'        End With
'    End If
'
'    dvprint "---------------"
'    dvprint "Outcomes: PROC"
'    dvprint "---------------"
'    If GetResult("", "PROC", "", "", return_result, SearchOutcomesRange) Then
'        numoutcomes = numoutcomes + 1
'        With oc(numoutcomes)
'            .checked = True
'            .pnum = 21
'            .start = return_result
'        End With
'    End If
'
    Exit Sub

errHandler:
    g_util.ThrowError "CheckOutcomes"
    Resume  'debug
End Sub


Private Sub AtLeastOneADL()
    If Not (inds(1).checked Or inds(2).checked Or inds(3).checked) Then
        'Note: Heather wants "#2 ADL - Assist" to be the default.  (90% of patients)
        SetInd 2, "at least one ADL"
    End If
End Sub

Private Sub HighestIndicatorInEachGroupWins()
    Dim i As Integer
    Dim g As Integer
    Dim highest_is_on As Boolean
    Dim ind_list As String

    dvprint "---------------"
    dprint "Select highest indicator in each group"
    
    g = 0
    highest_is_on = False
    For i = MAX_INDS To 1 Step -1
        If (inds(i).group > 0) Then
            If (inds(i).group <> g) Then
                'this is a new group
                g = inds(i).group
                highest_is_on = inds(i).checked
            Else
                'same group
                If highest_is_on Then
                    inds(i).checked = False             'uncheck a lower number
                Else
                    highest_is_on = inds(i).checked     'save this one
                End If
            End If
        End If
    Next i
    
    'Echo the indicators for an audit (no classification will be saved)
    If g_debug And g_no_output Then
        For i = 1 To MAX_INDS
            If inds(i).checked Then ind_list = ind_list & "," & i
        Next i
        dprint "Final list = " & Mid$(ind_list, 2)
    End If

End Sub

Private Sub OutputClass()
    Dim outstr As String, ind_list As String
    Dim i As Integer
    Dim txarea As String

    txarea = ""
    If m_pat.unit_id = 108 Then 'surg/ortho/peds
        If m_pat.age <= 16 Then txarea = "Pediatrics"
    End If
        
    outstr = g_util.FixedWidth("", 8)                                       '(facility code)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.unit_name, 16)
    outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(unit code)
    outstr = outstr & "|" & g_util.FixedWidth(txarea, 16)                       '(area code)
    outstr = outstr & "|" & g_util.FixedWidth("", 8)                        '(class date - give datetime instead)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.acct, 20)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.last_name, 32)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.first_name, 32)
    outstr = outstr & "|" & g_util.FixedWidth("", 32)                       '(middle)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.room, 8)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.bed, 4)
    outstr = outstr & "|" & Format$(m_pat.effective, "yyyymmddhhnn")      'class datetime
    outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(login)
    outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(employee)
    outstr = outstr & "|" & g_util.FixedWidth(CStr(m_pat.meth_id), 4)
    outstr = outstr & "|" & g_util.FixedWidth("C", 1)                       'record type = class
    outstr = outstr & "|" & g_util.FixedWidth("", 4)                        '(stage)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.TC_source_id, 4)        'TC source ID
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.range, 4)               'TC pull range
    outstr = outstr & "|"
    outstr = g_util.FixedWidth(outstr, 294)
    outstr = outstr & "|" & Format$(m_pat.effective, "yyyymmddhhnn")        'IN
    outstr = g_util.FixedWidth(outstr, 377)
    outstr = outstr & "|"
    
    For i = 1 To MAX_INDS
        If (inds(i).checked) Then
            outstr = outstr & "Y"
            ind_list = ind_list & "," & i
        Else
            outstr = outstr & "N"
        End If
    Next i
    ind_list = Mid$(ind_list, InStr(1, ind_list, ",") + 1)                'strip leading comma
    
    Print #outfile, outstr
    
    If Mid$(outstr, 212, 4) = "0300" Then
        Mid$(outstr, 212, 4) = "0700"
        Mid$(outstr, 304, 4) = "0700"
        Print #outfile, outstr
    End If
    
'2013-04-01 02:00:00.000
'2013-03-31 23:00:00.000
'2013-03-31 20:29:00.000
'2013-03-31 12:27:00.000
'2013-03-31 04:30:00.000
'2013-03-31 03:30:00.000
'2013-03-31 02:15:00.000
'2013-03-31 01:30:00.000

'
'         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         200       1         1         1         1         1         1         1         1         1         300       1         1         1         1         1         1         1
'1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
'        |10100           |                |                |        |701435054           |BLANTON                         |SEDERIA                         |                                |10111   |01  |201209240300|                |                |15  |C|    |1   |720 |                      |201209240300                                                                      |NYNNNNYYNNYNNYNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
'        |10100           |                |                |        |701432238           |BOYLE                           |DONNIELLE                       |                                |        |    |201209231745|                |                |15  |C|    |1   |165 |                      |201209240300                                                                      |NYNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
'        |10100           |                |                |        |701439293           |CONTINI                         |JOHN                            |                                |10105   |02  |201209240300|                |                |15  |C|    |1   |720 |                      |201209240300                                                                      |NYNNNNNNNNYNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
    
    AddLogEntry EVENT_TYPE_INFO, "Classified: " & ind_list, EVENT_CATEGORY_PROCESSED
End Sub

Private Sub OutputProcs()
    Dim i, j As Integer
    Dim s, f As Date
    Dim outstr As String, proc_list As String
    
    For i = 1 To numprocs
        procs(i).isvalid = True
    Next i
    For i = 1 To numprocs - 1
        If procs(i).isvalid Then
        For j = i + 1 To numprocs
            If procs(j).isvalid And procs(j).start = procs(i).start And procs(j).finish = procs(i).finish Then   'this can be combined.
                procs(j).isvalid = False
                procs(j).pindex = i
            End If
        Next j
        End If
    Next i

    For i = 1 To numprocs
        If procs(i).isvalid Then
        outstr = g_util.FixedWidth("", 8)                                       '(facility code)
        outstr = outstr & "|" & g_util.FixedWidth(m_pat.unit_name, 16)
        outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(unit code)
        outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(area code)
        outstr = outstr & "|" & g_util.FixedWidth("", 8)                        '(class date - give datetime instead)
        outstr = outstr & "|" & g_util.FixedWidth(m_pat.acct, 20)
        outstr = outstr & "|" & g_util.FixedWidth(m_pat.last_name, 32)
        outstr = outstr & "|" & g_util.FixedWidth(m_pat.first_name, 32)
        outstr = outstr & "|" & g_util.FixedWidth("", 32)                       '(middle)
        outstr = outstr & "|" & g_util.FixedWidth(m_pat.room, 8)
        outstr = outstr & "|" & g_util.FixedWidth(m_pat.bed, 4)
        outstr = outstr & "|" & Format$(procs(i).start, "yyyymmddhhnn")       '204 procdt
        outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(login)
        outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(employee)
        outstr = outstr & "|" & g_util.FixedWidth(CStr(m_pat.meth_id), 4)
        outstr = outstr & "|" & g_util.FixedWidth("P", 1)                       'record type = class
        outstr = outstr & "|"
        outstr = outstr & Space$(294 - Len(outstr))
        outstr = outstr & "|" & Format$(procs(i).start, "yyyymmddhhnn")       '296 procdt in
        outstr = outstr & Space$(346 - Len(outstr))
        If procs(i).finish = 0 Then
            outstr = outstr & "|" & Space$(12)
        Else
            outstr = outstr & "|" & Format$(procs(i).finish, "yyyymmddhhnn")      '348 procdt out
        End If
        outstr = g_util.FixedWidth(outstr, 377)
        outstr = outstr & "|NNNNNNNNN"
        Mid$(outstr, 378 + procs(i).pnum, 1) = "Y"
        proc_list = proc_list & "," & procs(i).pnum
        For j = i + 1 To numprocs
            If Not procs(j).isvalid And procs(j).pindex = i Then
                Mid$(outstr, 378 + procs(j).pnum, 1) = "Y"
                proc_list = proc_list & "," & procs(j).pnum
            End If
        Next j
        proc_list = Mid$(proc_list, InStr(1, proc_list, ",") + 1)          'strip leading comma
        
        Print #outfile, outstr
        
        AddLogEntry EVENT_TYPE_INFO, "Procedure: " & proc_list, EVENT_CATEGORY_PROCESSED
        End If 'isvalid
    Next i

End Sub
Private Sub OutputOutcomes()
    Dim i, j As Integer
    Dim s, f As Date
    Dim outstr As String
    Dim octime As String

    For i = 1 To MAX_PROCS
        If (oc(i).checked) Then
            outstr = g_util.FixedWidth("", 8)
            outstr = outstr & "|" & g_util.FixedWidth(m_pat.unit_name, 16)
            outstr = outstr & "|" & g_util.FixedWidth("", 16)
            outstr = outstr & "|" & g_util.FixedWidth("", 16)
            outstr = outstr & "|" & g_util.FixedWidth(oc(i).start, 12)
            outstr = outstr & "|" & g_util.FixedWidth(m_pat.acct, 20)
            outstr = outstr & "|" & g_util.FixedWidth(oc(i).pnum, 3)
            Print #outfile2, outstr 'Print line to outcomesindicator.TXT
        End If
    Next i
End Sub

Private Sub SetADLCompleteWhenAge(agecond As String)  ' agecond example: "<=3"
    Dim sql As String
    Dim rs As New Recordset

'
'select case when round(age_at_admission,0,1) <=55 then 1 else 0 end from encounter where encounter_id=6990

    sql = "select case when round(age_at_admission,0,1) " & agecond & " then 1 else 0 end from encounter " & WHERE_ENCOUNTER
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        If rs(0) = 1 Then
            SetInd 3, "Age <=3 years"
        End If
    End If
    rs.Close

End Sub

Private Function IsICU() As Boolean
    
    IsICU = (m_pat.unit_id = 3) Or _
            (m_pat.unit_id = 2141) Or _
            (m_pat.unit_id = 2151) Or _
            (m_pat.unit_id = 2161) Or _
            (m_pat.unit_id = 2171) Or _
            (m_pat.unit_id = 2436) Or _
            (m_pat.unit_id = 2446) Or _
            (m_pat.unit_id = 94573495)

End Function



'CHANGES FOR 7/13
'Add changes for 4/27 in mapping table.
'Also make NURBSCHEC3 Persistent.
'have Justin push out to all machines
'how to make icons smaller?

'select classification_event_id from classification_event
'where timestamp = (select max(timestamp) from classification_event)


Private Function Op(opcode As TCOperator, s As String) As String
    If IsNull(s) Then s = ""
    If opcode = opeq Or opcode = opnone Then
        Op = "='" & s & "'"
    ElseIf opcode = oplike Then
        Op = " like '%" & s & "%'"
    ElseIf opcode = OPLTE Then
        Op = "<=" & s
    ElseIf opcode = OPGTE Then
        Op = ">=" & s
    ElseIf opcode = OPNE Then
        Op = "<>" & s
    ElseIf opcode = opnotlike Then
        Op = " not like '%" & s & "%'"
    End If

End Function

Private Sub Check_UserDefined()
    On Error GoTo errHandler
    
    Dim mins As Integer
    Dim chart_result As String
    
    dvprint "---------------"
    dvprint "User-defined indicators 100,101"
    dvprint "---------------"
    
    SetIndicator (100)
    SetIndicator (101)
'100 BJH VS Sitter   1       NULL    Suicide 2
'101 BJH VS Sitter   1       NULL    Safety  2
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_21"
    Resume  'debug
End Sub
