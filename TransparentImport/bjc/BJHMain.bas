Attribute VB_Name = "BJHMain"
Option Explicit
'
' Barnes-Jewish NEW transparent classification for TCTools (copied from Parrish)
'
'1. read data into CI from file TCQuery*.txt
'2. read dictionary into memory.? No need because dictionary is now data table
'3. process mappings using both dictionary and non-dictionary
'
' Each unit must set the "use transparent" flag.
'
' This is designed to be run as a background job, so never stop for user input -- that means no error MsgBox.
' All communication is done through log files and the event log; it is OK to print to the console.
'
' In order to be version independent, this program does not use any AcuityPlus dlls.
' Some AcuityPlus utility classes have been copied here.
'
' In order to work with the AcuityPlus Patient Selection and Transparent import:
'
'   * The program name must be TransparentMapping.exe, placed in AcuityPlus\bin
'   * The audit file is called TransparentAudit.log, placed in AcuityPlus\log
'   * The output file is Transparent.txt, placed in AcuityPlus\load_me
'   * Events are saved in the database event log
'
Public g_cnADO          As ADODB.Connection             'main DB connection
Public g_util           As New PFSUtility               'utility functions
Public g_dbutil         As New PFSDBUtility
Public g_event          As New PFSEventLog
Public g_display        As New PFSDisplayProperties
Public g_command        As String                       'command line (for db utility)
Public g_abort          As Boolean

Private Const TRANSP_FILENAME = "Transparent.txt"       'THE output file
Private Const TRANSP_AUDIT_LOG = "TransparentAudit.log" 'The debug/audit log
Private Const CHART_ITEM_LIFE = 30                      'days in the chart_item table
Private Const OUTCOMES_FILENAME = "OutcomesIndicator.txt"   'outcomes output file
Private Const DNLD_FNAME = "TCQUERY"


'c:\FTP ISACUITYTEST\transparency\acuity_plus_problem_list
'c:\FTP ISACUITYTEST\transparency\acuity_plus_queries
'c:\FTP ISACUITYTEST\transparency\acuity_plus_interventions___meds
'c:\FTP ISACUITYTEST\transparency\outcomes_yyyymmddhhnn.txt
Private Const INPUT_TESTPATH = "c:\FTP ISACUITYTEST\transparency\"
Private Const INPUT_PRODPATH = "c:\FTP ISACUITYPLUS\transparency\"
Private Const INPUT_QFILE = "acuity_plus_queries"
Private Const INPUT_IFILE = "acuity_plus_interventions___meds"
Private Const INPUT_PFILE = "acuity_plus_problem_list"
Private Const INPUT_OFILE = "outcomes"

Private Const DEFAULT_RANGE = 1440                      '24 hrs in minutes; overridden by -range

Public Type PatientInfo
    unit_id             As Long
    encounter_id        As Long
    meth_id             As Long
    last_name           As String
    first_name          As String
    acct                As String
    age                 As Single                   'age (in years) at admission
    unit_name           As String
    room                As String
    bed                 As String
    unit_arrival        As Date
    effective           As Date                     'patient specific (may be > g_effdt)
    pull_start          As Date                     'patient specific (may be > g_pull_start)
    pull_finish         As Date                     'patient specific (may be < g_pull_finish)
    range               As Integer                  'patient specific (may be < g_range)
    TC_source_id        As Integer
End Type

Private Type unitrecord
    unit_id        As Long
    name           As String
End Type

Public g_debug          As Boolean
Public g_verbose        As Boolean                  'verbose debug output?
Public g_no_output      As Boolean      'if no ouput, then no input files either

Public g_effdt          As Date                     'effective datetime

Public outfile          As Integer
Public outfile2          As Integer
Public dbugfile         As Integer

Public gLogUnitID       As Long
Public gLogEncounterID  As Long
Public gLogSourceText   As String

Public g_pull_start     As Date
Public g_pull_finish    As Date                     'pull datetime
Public g_range          As Long

Private winpfspath      As String                   'path of winpfs
Private winlogpath      As String                   'path of winpfs\log
Private winloadpath     As String                   'path of winpfs\load_me
Private outfilename     As String
Private outfile2name    As String
Private dbugname        As String

Private nowdt           As Date
Private debug_acct      As String                   'acct filter
Private debug_unit      As String                   'unit filter
Private debug_unit_id   As Long

Private inputqfile      As Integer
Private inputifile      As Integer
Private inputpfile      As Integer
Private inputofile      As Integer
Private inputqname      As String
Private inputiname      As String
Private inputpname      As String
Private inputoname      As String

Sub Main()
    On Error GoTo errHandler
    
    frmMain.Show vbModeless                         'progress window
    DoEvents
    
    Set g_cnADO = g_dbutil.NewRemoteConnection
    
    gLogSourceText = Command$
    LogInfo "Begin mapping and translation", EVENT_CATEGORY_STARTUP_SHUTDOWN
    gLogSourceText = ""
    
    ParseCommandLine
    OpenOutputFiles
    OpenInputFiles
    WriteDataToCI
    If g_effdt = "0700" Then
        'AddArtificialDefaults
    End If
    
    DeleteInputFiles
    Process
    CloseOutputFiles
    DeleteOldLogs
    DeleteOldChartItems

    LogInfo "Mapping complete", EVENT_CATEGORY_STARTUP_SHUTDOWN
    g_cnADO.Close
normalExit:
    Unload frmMain
    Exit Sub

errHandler:
    LogError Err.Description & " in " & Err.source
    LogInfo "Unexpected error - shut down", EVENT_CATEGORY_STARTUP_SHUTDOWN
    GoTo normalExit
    Resume  'debug
End Sub

Private Sub ParseCommandLine()
    On Error GoTo errHandler

    Dim argc As Integer, argv() As String, subarg() As String, tag As String, value As String
    Dim i As Integer, n As Integer
    Dim effdate As String, efftime As String
    Dim pulldate As String, pulltime As String
    Dim nowdate As String, nowtime As String
    
    '-effdate=yyyymmdd   This is the date which, in combination with the
    '                   -efftime time is used to specify the classification In time.
    '                   If not specified, -effdate is assumed to be the current date.
    'Special value:  -effdate=yesterday
    
    '-efftime=hhmm      Used to set the classification In time.
    '                   If not specified, then -efftime is assumed to be the current time.
        
    '-pulldate=yyyymmdd This is the date/time of the data pull time.
    '                   If not specified, then this is assumed to be the -effdate.
    '-pulltime=hhmm     This is the time starting from which the pull is to
    '                   look backwards from.
    '                   If not specified, then this time is assumed to be the -efftime.
    
    '-range=nnnn        This is the number of minutes backwards from the pull time
    '                   that defines valid g_range of charting events.
    '                   If not specified, this will default to 1440 (24 hours)

    '-debug             echo debug/audit info; save in log file
    '-brief             Minimal debug info -- sets -debug
    '-verbose           Lots of debugging info -- sets -debug
    '-n                 No output to transparent.txt - debug only
    '-audit             sets -debug and -n
    '
    '-acct=12345        Process this patient only
    '-unit=5N           Process this unit only
    '-unit_id=1234      process this unit only
    
    nowdt = Now
    nowdate = Format$(nowdt, "yyyymmdd")
    nowtime = Format$(nowdt, "hhnn")
    
    g_verbose = True

    argc = g_util.SplitTextOnChar(LCase$(Command$), " ", argv(), 1, 1)

    For i = 1 To argc
        n = g_util.SplitTextOnChar(argv(i), "=", subarg(), 1, 2)        '<tag>=<value>
        tag = subarg(1)
        value = subarg(2)
        
        Select Case tag
        Case "-acct"
            debug_acct = value
            
        Case "-audit"
            g_debug = True
            g_no_output = True
        
        Case "-brief"
            g_verbose = False
            g_debug = True
        
        Case "-debug"
            g_debug = True
        
        Case "-effdate"
            If (value = "yesterday") Then
                effdate = Format(DateAdd("d", -1, Date), "yyyymmdd")
            Else
                effdate = value
            End If
        
        Case "-efftime"
            efftime = Left$(value, 4)
            efftime = String$(4 - Len(efftime), "0") & efftime
        
        Case "-n"
            g_no_output = True

        Case "-pulldate"
            If (value = "today") Then
                pulldate = nowdate
            Else
                pulldate = value
            End If
        
        Case "-pulltime"
            pulltime = Left$(value, 4)
            pulltime = String$(4 - Len(pulltime), "0") & pulltime

        Case "-range"
            g_range = CInt(value)
            
        Case "-unit"
            debug_unit = value
        Case "-unit_id"
            debug_unit_id = CLng(value)
            
        Case "-verbose"
            g_verbose = True
            g_debug = True

        Case Else
            LogWarning "Unexpected argument: " & tag
        End Select
    Next i

    If Len(effdate) = 0 Then effdate = nowdate
    If Len(efftime) = 0 Then efftime = nowtime
    
    'Note: pulldate defaults to effdate, not nowdate
    If Len(pulldate) = 0 Then pulldate = effdate
    If Len(pulltime) = 0 Then pulltime = efftime

    If g_range = 0 Then g_range = DEFAULT_RANGE
    
    'Note: the log file isn't open yet so these just go to the screen
    Debug.Print "Command line: " & Command$
    Debug.Print "effdate=" & effdate
    Debug.Print "efftime=" & efftime
    Debug.Print "pulldate=" & pulldate
    Debug.Print "pulltime=" & pulltime
    Debug.Print "range=" & g_range
    
    g_effdt = g_util.CDateEx(effdate & efftime)
    
    g_pull_finish = g_util.CDateEx(pulldate & pulltime)     'for chart item queries
    g_pull_start = DateAdd("n", -g_range, g_pull_finish)
    Exit Sub
    
errHandler:
    g_util.ThrowError "ParseCommandLine"
    Resume  'debug
End Sub

Private Function GetAllInputFilenames() As String
    'The location is determined by the first line of the dictionary.
    Const INITIAL_BLOCK_SIZE = 24
    Dim i As Integer
    Dim infname As String
    Dim fnames() As String
    
'    outfn = winloadpath & "\" & TRANSP_FILENAME
    
    ReDim fnames(0 To INITIAL_BLOCK_SIZE) As String
    i = 0
    infname = Dir$(winloadpath + "\" & DNLD_FNAME & "*.TXT") 'returns ONLY the filename.  For wildcards.
    While infname <> ""
        i = i + 1
        If (i > UBound(fnames)) Then
            ReDim Preserve fnames(0 To i + INITIAL_BLOCK_SIZE) As String
        End If
        fnames(i) = UCase$(infname)
        infname = Dir$
    Wend
    
    BubbleSort fnames, i
    
    GetAllInputFilenames = fnames(i)
    
End Function
Private Sub BubbleSort(arr As Variant, numEls As Integer)
    Dim lLoop1 As Integer
    Dim lLoop2 As Integer
    Dim lTemp As Variant
    
    For lLoop1 = numEls To LBound(arr) Step -1
       For lLoop2 = LBound(arr) + 1 To lLoop1
           If arr(lLoop2 - 1) > arr(lLoop2) Then
              lTemp = arr(lLoop2 - 1)
              arr(lLoop2 - 1) = arr(lLoop2)
              arr(lLoop2) = lTemp
           End If
       Next lLoop2
   Next lLoop1
End Sub



Private Sub Process()
    On Error GoTo errHandler
    
    Dim pat As PatientInfo
    Dim rs As New Recordset
    Dim sql As String
    Dim count As Integer

    dprint ""
    dprint "Pull at:   " & g_pull_finish & " (back to " & g_pull_start & "; range=" & g_range & ")"
    dprint "Effective: " & g_effdt
        
    '
    ' Make a list of all patients with chart items during the pull period.
    ' Include only units that have the "use transparent" flag set.
    ' Limit to one patient if -acct is given.  Limit to one unit with -unit.
    ' Left join encounter_location - the patient may be discharged at pull time.
    ' Look for the latest patient arrival to the unit before the pull time; ignore transfers within the unit.
    ' Look for a patient departure (if any) before the pull time; ignore transfers within the unit.
    sql = "" & _
        " SELECT LIST.UNIT_ID, LIST.ENCOUNTER_ID, LIST.TC_SOURCE_ID, PARAM.METHODOLOGY_ID," & _
        "     UNIT.NAME AS UNIT, EL.ROOM, EL.BED, EL.DATETIME_IN AS BED_ARRIVAL," & _
        "     ARRIVE.DATETIME_IN AS UNIT_ARRIVAL, DEPART.DATETIME_OUT AS UNIT_DEPARTURE," & _
        "     E.ACCT_NUMBER, P.LAST_NAME, P.FIRST_NAME," & _
        "     P.DOB, E.ADMISSION_DATETIME, E.REGISTRATION_DATETIME" & _
        " FROM (" & _
        "     SELECT DISTINCT CI.UNIT_ID, CI.ENCOUNTER_ID, CI.TC_SOURCE_ID" & _
        "     FROM CHART_ITEM AS CI" & _
        "     INNER JOIN UNIT ON (UNIT.UNIT_ID = CI.UNIT_ID)" & _
        "     WHERE UNIT.USE_TRANSPARENT_CLASSIFICATION='Y'" & _
        "     AND EVENT_DATETIME BETWEEN " & g_dbutil.SQL_DateTime(g_pull_start) & " AND " & g_dbutil.SQL_DateTime(g_pull_finish) & _
        " ) AS LIST"
    sql = sql & _
        " INNER JOIN UNIT ON (UNIT.UNIT_ID = LIST.UNIT_ID)" & _
        " INNER JOIN UNIT_PARAM AS PARAM ON (PARAM.UNIT_ID = UNIT.UNIT_ID) AND (" & g_dbutil.SQL_DateTime(g_pull_finish) & " BETWEEN PARAM.EFFECTIVE_REPORT_DATE AND EXPIRATION_REPORT_DATE)" & _
        " INNER JOIN ENCOUNTER AS E ON (E.ENCOUNTER_ID = LIST.ENCOUNTER_ID)" & _
        " INNER JOIN PERSON AS P ON (P.PERSON_ID = E.PERSON_ID)" & _
        " LEFT  JOIN ENCOUNTER_LOCATION AS EL ON (EL.ENCOUNTER_ID = E.ENCOUNTER_ID) AND (" & g_dbutil.SQL_DateTime(g_pull_finish) & " BETWEEN EL.EFFECTIVE_DATETIME_IN AND EL.EFFECTIVE_DATETIME_OUT)" & _
        " INNER JOIN (" & _
        "    SELECT WORKING_UNIT_ID AS UNIT_ID, ENCOUNTER_ID, MAX(DATETIME_IN) AS DATETIME_IN" & _
        "    FROM ENCOUNTER_LOCATION AS EL" & _
        "    WHERE WORKING_UNIT_ID IN (SELECT UNIT_ID FROM UNIT WHERE USE_TRANSPARENT_CLASSIFICATION='Y')" & _
        "    AND DATETIME_IN < " & g_dbutil.SQL_DateTime(g_pull_finish) & _
        "    AND IS_TRANSFER_IN='Y'" & _
        "    GROUP BY WORKING_UNIT_ID, ENCOUNTER_ID" & _
        " ) AS ARRIVE ON (ARRIVE.UNIT_ID = UNIT.UNIT_ID) AND (ARRIVE.ENCOUNTER_ID = E.ENCOUNTER_ID)" & _
        " LEFT JOIN (" & _
        "    SELECT WORKING_UNIT_ID AS UNIT_ID, ENCOUNTER_ID, MAX(DATETIME_OUT) AS DATETIME_OUT" & _
        "    FROM ENCOUNTER_LOCATION AS EL" & _
        "    WHERE WORKING_UNIT_ID IN (SELECT UNIT_ID FROM UNIT WHERE USE_TRANSPARENT_CLASSIFICATION='Y')" & _
        "    AND DATETIME_OUT < " & g_dbutil.SQL_DateTime(g_pull_finish) & _
        "    AND ((NEXT_WORKING_UNIT_ID<>WORKING_UNIT_ID) OR (NEXT_WORKING_UNIT_ID IS NULL))" & _
        "    GROUP BY WORKING_UNIT_ID, ENCOUNTER_ID" & _
        " ) AS DEPART ON (DEPART.UNIT_ID = UNIT.UNIT_ID) AND (DEPART.ENCOUNTER_ID = E.ENCOUNTER_ID)" & _
        " WHERE 1=1"
    
    If Len(debug_acct) Then
        sql = sql & " AND E.ACCT_NUMBER=" & g_dbutil.SQL_String(debug_acct)
    End If
    If Len(debug_unit) Then
        sql = sql & " AND UNIT.NAME=" & g_dbutil.SQL_String(debug_unit)
    End If
    If (debug_unit_id > 0) Then
        sql = sql & " AND UNIT.UNIT_ID=" & debug_unit_id
    End If
    sql = sql & " ORDER BY UNIT.NAME, P.LAST_NAME, P.FIRST_NAME, E.ACCT_NUMBER"
    
    Debug.Print sql
    rs.Open sql, g_cnADO
    count = 0

    Do While Not rs.EOF
        'Package the patient info (don't use unit/encounter objects)
        With pat
            .unit_id = rs("UNIT_ID")
            .encounter_id = rs("ENCOUNTER_ID")
            .meth_id = rs("METHODOLOGY_ID")
            .acct = rs("ACCT_NUMBER")
            .last_name = rs("LAST_NAME") & ""
            .first_name = rs("FIRST_NAME") & ""
            .unit_name = rs("UNIT") & ""
            .room = rs("ROOM") & ""
            .bed = rs("BED") & ""
            
            If Not IsNull(rs("DOB")) And Not IsNull(rs("ADMISSION_DATETIME")) Then
                .age = DateDiff("n", rs("DOB"), rs("ADMISSION_DATETIME")) / 60# / 24# / 365.24219
            ElseIf Not IsNull(rs("DOB")) And Not IsNull(rs("REGISTRATION_DATETIME")) Then
                .age = DateDiff("n", rs("DOB"), rs("REGISTRATION_DATETIME")) / 60# / 24# / 365.24219
            Else
                .age = 0
            End If
            
            .unit_arrival = rs("UNIT_ARRIVAL")
            .effective = g_util.Max(g_effdt, .unit_arrival)
            .pull_start = g_util.Max(g_pull_start, .unit_arrival)
            .pull_finish = g_util.Min(g_pull_finish, rs("UNIT_DEPARTURE"))
            If (.pull_finish < .pull_start) Then
                .pull_finish = g_pull_finish
            End If
            .range = DateDiff("n", .pull_start, .pull_finish)
            .TC_source_id = g_dbutil.DBToInteger(rs("TC_SOURCE_ID"))
        End With
        
        'Save ids for log entries
        gLogUnitID = pat.unit_id
        gLogEncounterID = pat.encounter_id
    
        'Process this patient
        count = count + 1
        frmMain.SetProgress "Processing patient " & count & " of " & rs.RecordCount
        
        dvprint ""
        dprint String$(80, "=")
        dprint "Patient " & count & " of " & rs.RecordCount & ": " & _
            pat.last_name & ", " & pat.first_name & _
            " in " & pat.unit_name & " " & pat.room & " " & pat.bed & _
            " acct=" & pat.acct & " age=" & Round(pat.age, 2)
        dprint "Here from " & pat.pull_start & " to " & pat.pull_finish & _
            "  (LOS=" & Round(pat.range / 60, 2) & ")"

        
        'Most sites will have one source of chart items
        'If there are multiple sources that need different mapping, this is the place to do it
        Select Case pat.TC_source_id
'        Case 123
'            Select Case pat.meth_id
'            Case METH_ID_INPATIENT
'                ProcessInpatient_123 pat
'            Case Else
'                LogWarning "Methodology " & pat.meth_id & " in unit " & pat.unit_name & " is not supported"
'            End Select
        
        Case Else

            Select Case pat.meth_id
            Case METH_ID_INPATIENT
                ProcessInpatient pat
            Case Else
                LogWarning "Methodology " & pat.meth_id & " in unit " & pat.unit_name & " is not supported"
            End Select

        End Select
        
        gLogUnitID = 0
        gLogEncounterID = 0

        If g_abort Then Exit Do
        
        rs.MoveNext
    Loop
    
    rs.Close
    
    If (count < 1) Then
        dprint ""
        If Len(debug_acct) Then
            LogWarning "The selected patient has no chart items in the given time range", EVENT_CATEGORY_UNEXPECTED
        Else
            LogWarning "No chart items found to process - have the unit(s) been enabled for transparent classification?", EVENT_CATEGORY_UNEXPECTED
        End If
    End If
    
    Exit Sub

errHandler:
    g_util.ThrowError "Process"
    Resume  'debug
End Sub

'debug print
Public Sub dprint(s As String)
    If Not g_debug Then Exit Sub
    
    Debug.Print s                           'add to debug window
    'It would be nice if we could print to the cmd window, but it takes some effort

    If dbugfile <> 0 Then
        Print #dbugfile, s                  'add to debug log file
    End If
End Sub

'debug verbose print
Public Sub dvprint(s As String)
    If g_verbose Then dprint s
End Sub

Private Sub OpenOutputFiles()
    On Error GoTo errHandler
    
    If Not g_util.GetKeyValue(HKEY_LOCAL_MACHINE, "SOFTWARE\Quadramed\WinPFS", "Directory", winpfspath) Then
        winpfspath = App.Path & "\.."
    End If

    winlogpath = "c:\users\public\acuityplus\log" ' winpfspath & "\log"
    winloadpath = "c:\users\public\acuityplus\load_me" 'winpfspath & "\load_me"
    If Not g_util.DirExists(winlogpath) Then
        MkDir$ winlogpath
    End If

    'dbugname = winlogpath & "\" & TRANSP_DEBUG & Format$(nowdt, "mmdd") & ".log"
    dbugname = winlogpath & "\" & TRANSP_AUDIT_LOG
    
    If g_debug Then
        dbugfile = FreeFile
        Open dbugname For Output As #dbugfile                   'overwrite existing
        Print #dbugfile, String(80, "=")
        Print #dbugfile, "TRANSPARENT MAPPING AUDIT FILE                       Time=" & nowdt
    End If
    
    If g_no_output Then Exit Sub

    outfilename = winloadpath & "\" & TRANSP_FILENAME           'load_me\transparent.txt
    outfile = FreeFile
    Open outfilename For Append As #outfile                     'add to existing
    
'    outfile2name = winloadpath & "\" & OUTCOMES_FILENAME           'load_me\outcomesindicator.txt
'    outfile2 = FreeFile
'    Open outfile2name For Append As #outfile2                     'add to existing
    
    Exit Sub

errHandler:
    g_util.ThrowError "OpenOutputFiles"
    Resume  'debug
End Sub
Private Sub CloseOutputFiles()
    If (outfile <> 0) Then Close #outfile
'    If (outfile2 <> 0) Then Close #outfile2
    
    If (dbugfile <> 0) Then
        Close #dbugfile
        dbugfile = 0                                            'stop writing to debug file
    End If
End Sub
Private Sub OpenInputFiles()
    Dim i As Integer
    
    On Error GoTo errHandler
    
    If g_no_output Then Exit Sub 'if no ouput, then no input
    
    'i = GetAllInputFilenames
    inputqname = winloadpath + "\" + GetAllInputFilenames
    
    CheckInFile inputqname, inputqfile
    
    Exit Sub

errHandler:
    g_util.ThrowError "OpenInputFiles"
    Resume  'debug
End Sub

Private Sub CheckInFile(n As String, f As Integer)

    If g_util.FileExists(n) Then
        f = FreeFile
        Open n For Input As #f
    Else
        f = 0
    End If

End Sub

Private Sub DeleteInputFiles()
    If g_no_output Then Exit Sub
    
    If inputqfile <> 0 Then
        Close #inputqfile
        Name inputqname As inputqname & "_" & Format$(nowdt, "MMddHHmm") & ".TXT"
    End If
    
End Sub

Private Sub WriteDataToCI()
    Dim hdr As String
    Dim sql As String
    Dim s As String
    Dim buf As String
    Dim rows() As String
    Dim cols() As String
    Dim maxrow As Long
    Dim iRow As Long
    Dim skip As Boolean
    Dim UnitID As Long
    Dim encid As Long
    Dim dupevdt As String
    Dim dupcode As String
    Dim dupdesc As String
    Dim dupres As String
    Dim prev_encid As Long
    Dim seq As Integer
    
'CREATE TABLE dbo.CHART_ITEM (
'     UNIT_ID              int NOT NULL,
'     EVENT_DATETIME       datetime NOT NULL,
'     ENCOUNTER_ID         int NOT NULL,
'     CODE                 varchar(32) NOT NULL,
'       CATEGORY             varchar(64) NULL,
'     DESCRIPTION          varchar(128) NULL,
'       FIELD_NAME           varchar(64) NULL,
'       FREQUENCY_HOURS      real NULL,
'       PROCEDURE_START      datetime NULL,
'       PROCEDURE_FINISH     datetime NULL,
'       PROCEDURE_HOURS      real NULL,
'     RESULT               varchar(512) NULL,
'       EXPIRATION_DATETIME  datetime NULL,
'     SOURCE_TEXT          varchar(2048) NULL,
'       TC_SOURCE_ID         smallint NULL,
'     TIMESTAMP            datetime NOT NULL,
'       Primary key(unit_id, EVENT_DATETIME, encounter_id, code)
')

'SAMPLE DATA
'Tab-delimited
'124590527               10100   1991-05-14 00:00:00.000 BJHCLINDFIVEEFNCTTESTSCMFIVE    2010-05-24 08:00:00.000 BJH IN D5W  D5W 100.00000
'124590527               10100   1991-05-14 00:00:00.000 BJHCLINDFIVEEFNCTTESTSCMFIVE    2010-05-24 08:00:00.000 BJH IN Dietary  Dietary 80.00000
'124590527               10100   1991-05-14 00:00:00.000 BJHCLINDFIVEEFNCTTESTSCMFIVE    2010-05-24 08:00:00.000 BJH IN DOPamine Order Based Drip    DOPamine Order Based Drip   25.00000

'acctnum VarChar(50)
'unit VarChar(50)
'dob datetime
'name varchar(50)
'perfdt datetime
'code VarChar(Max)
'descript VarChar(Max)
'res VarChar(Max)
    On Error GoTo QUERIESerrHandler
    If inputqfile = 0 Then Exit Sub
    
    s = Input(LOF(inputqfile), inputqfile)
    rows() = Split(s, vbCrLf)
    maxrow = UBound(rows)
    skip = True
    For iRow = 0 To maxrow
        frmMain.SetProgress ("Loading " & iRow & " of " & maxrow)
        buf = rows(iRow)
        buf = Replace(buf, "'", "")
        cols() = Split(buf, vbTab)
        
        If Trim$(buf) = "" Then 'skip blank line
        ElseIf UBound(cols) < 7 Then 'skip incomplete lines--all tabs should be present
        Else
            UnitID = GetUnitIDFromUnitName(cols(1))
            encid = GetEncounterIDFromAcct(cols(0))
            If prev_encid <> encid Then
                seq = 0
                prev_encid = encid
            Else
                seq = seq + 1
            End If
            skip = False
            If UnitID = 0 Or encid = 0 Then
                skip = True
            Else
                hdr = "insert into chart_item (sequence,unit_id,encounter_id,tc_source_id,timestamp,event_datetime,code,description,result) values ("
                hdr = hdr & seq & ","
                hdr = hdr & UnitID & ","
                hdr = hdr & encid & ","
                hdr = hdr & "1,"   'tc_source_id = 1
            End If
            If Not skip Then 'detail
                sql = hdr & g_dbutil.SQL_DateTime(g_effdt) & ","   'timestamp
                dupevdt = g_dbutil.SQL_DateTime(g_util.CDateEx(cols(4)))
                dupcode = g_dbutil.SQL_String(cols(5))
                dupdesc = g_dbutil.SQL_String(cols(6))
                If Len(cols(7)) > 512 Then cols(7) = Mid$(cols(7), 1, 512)
                dupres = g_dbutil.SQL_String(cols(7))  'result
                sql = sql & dupevdt & ","  'event_datetime yyyymmddhhnn
                sql = sql & dupcode & "," 'code
                sql = sql & dupdesc & "," 'desc
                sql = sql & dupres & ")" 'result
'                If Len(buf) > 2048 Then buf = Mid$(buf, 1, 2048)
'                sql = sql & g_dbutil.SQL_String(buf) & ")"
                g_cnADO.Execute sql
            End If
        End If
    Next iRow
    
    Exit Sub
    
QUERIESerrHandler:   'if dup is found, update the record because result may be different, and then move on
    Debug.Print iRow
    Debug.Print sql
    If InStr(1, Err.Description, "duplicate", vbTextCompare) > 0 Then
        Resume Next
    Else
        LogError Err.Description & " in " & Err.source
        LogInfo "Unexpected error - shut down", EVENT_CATEGORY_STARTUP_SHUTDOWN
        Exit Sub
    End If
    
    
errHandler:   'if dup found, move on because only key elements are being used here.
    If InStr(1, Err.Description, "duplicate", vbTextCompare) > 0 Then
        Resume Next
    Else
        LogError Err.Description & " in " & Err.source
        LogInfo "Unexpected error - shut down", EVENT_CATEGORY_STARTUP_SHUTDOWN
    End If

End Sub
Private Function GetUnitIDFromUnitName(s As String) As Long
    Dim rs As New Recordset
    Dim sql As String
    
    Select Case UCase$(Trim$(s))
        Case "13100 TOWER PLACE"
            s = "13100"
        Case "4300 OBSERVATION UNIT"
            s = "4300 OU"
        Case "6200 OBSERVATION UNIT"
            s = "6200 OU"
        Case "PACU CRITICAL CARE AREA"
            s = "PACU CCA"
        Case "16400 OBSERVATION UNIT"
            s = "16400 OU"
        Case "16300 OBSERVATION UNIT"
            s = "16300 OU"
        Case "8900 ONCOLOGY"
            s = "89ON" '89ON is a new alias for 3200 6/5/13
        Case "4900 ONCOLOGY"
            s = "49ON"
    End Select
    
    
    GetUnitIDFromUnitName = 0
    sql = "select unit_id from unit_alias where unit_alias_name='" & s & "'"
    sql = sql & " UNION select unit_id from unit where name='" & s & "'"
    rs.Open sql, g_cnADO
    
    If Not rs.EOF Then
        If Not IsNull(rs(0)) Then
            GetUnitIDFromUnitName = rs(0)
        End If
    End If
    
    rs.Close
    Set rs = Nothing

End Function
Private Function GetEncounterIDFromAcct(s As String) As Long
    Dim rs As New Recordset
    Dim sql As String

    GetEncounterIDFromAcct = 0
    sql = "select encounter_id from encounter where acct_number='" & s & "'"
    rs.Open sql, g_cnADO
    
    If Not rs.EOF Then
        If Not IsNull(rs(0)) Then
            GetEncounterIDFromAcct = rs(0)
        End If
    End If
    
    rs.Close
    Set rs = Nothing

End Function

Private Sub DeleteOldLogs()
    'There are no more log files; history has been added to the event log
End Sub

Private Sub DeleteOldChartItems()
    On Error GoTo errHandler
    
    Dim sql As String
    Dim dt As Date

    dt = DateAdd("d", -CHART_ITEM_LIFE, Date)
    sql = "DELETE FROM CHART_ITEM WHERE EVENT_DATETIME < " & g_dbutil.SQL_DateTime(dt)
    'Debug.Print sql
    g_cnADO.Execute sql
    Exit Sub
    
errHandler:
    g_util.ThrowError "DeleteOldChartItems"
    Resume  'debug
End Sub




Public Sub AddLogEntry(event_type As EventLogType, msg As String, _
    Optional event_category As EventLogCategory = EVENT_CATEGORY_NONE _
)
    On Error GoTo errHandler

    dprint msg                      'copy to debug log

    g_event.AddEventLogEntry EVENT_SOURCE_TRANSPARENT_MAPPING, event_type, event_category, _
        msg, gLogSourceText, 0, gLogUnitID, gLogEncounterID
    Exit Sub
    
errHandler:
    'no message boxes allowed (this is a command-line program)
    Debug.Print Err.Description
End Sub

Public Sub LogInfo(s As String, Optional event_category As EventLogCategory = EVENT_CATEGORY_NONE)
    AddLogEntry EVENT_TYPE_INFO, s, event_category
End Sub

Public Sub LogWarning(s As String, Optional event_category As EventLogCategory = EVENT_CATEGORY_VALIDATION)
    AddLogEntry EVENT_TYPE_WARNING, s, event_category
End Sub

Public Sub LogError(s As String, Optional event_category As EventLogCategory = EVENT_CATEGORY_VALIDATION)
    AddLogEntry EVENT_TYPE_ERROR, s, event_category
End Sub

'Private Sub AddArtificialDefaults()
'    Dim rs As New Recordset
'    Dim sql As String
'    Dim i, j, count As Integer
'    Dim upid As Long
'    Dim admp As Integer
'    Dim indstr As String
'    Dim definds() As String
'    Dim outinds, savoutinds As String
'    Dim acct, outstr, savoutstr As String
'    Dim def As Boolean
'    Dim msunitary() As unitrecord
'
'    sql = "select unit_id,name from unit where use_transparent_classification='Y'"
'    rs.Open sql, g_cnADO
'    If Not rs.EOF Then
'        count = rs.RecordCount
'        ReDim msunitary(1 To count)
'        For i = 1 To count
'            msunitary(i).unit_id = rs(0)
'            msunitary(i).name = rs(1)
'            rs.MoveNext
'        Next i
'    End If
'    rs.Close
'
'    outstr = Space$(9) & "" '*unitname  '10
'    outstr = outstr & Space$(26 - Len(outstr)) '26
'    outstr = outstr & "" '*unitname '27
'    outstr = outstr & Space$(60 - Len(outstr)) '60
'    outstr = outstr & Space$(9) '69
'    outstr = outstr & Space$(21) '**acctnum & Space$(20 - Len(acctnum) + 1) '90
'    outstr = outstr & Space$(33) 'lastname & Space$(32 - Len(lastname) + 1) '123
'    outstr = outstr & Space$(33) 'firstname & Space$(32 - Len(firstname) + 1) '156
'    outstr = outstr & Space$(32 + 1) '189
'    outstr = outstr & Space$(9) 'roomname & Space$(8 - Len(roomname) + 1) '198
'    outstr = outstr & Space$(5) 'bedname & Space$(4 - Len(bedname) + 1) '20
'    outstr = outstr & g_pull_finish & Space$(1) 'classdatetime 216
'    outstr = outstr & Space$(78 + 1) '295
'    outstr = outstr & intime & Space$(12 - Len(intime) + 1) '308
'    outstr = outstr & Space$(70) '378
'    savoutstr = outstr
'
'    outinds = ""
'    For j = 1 To MAX_INDICATORS
'        outinds = outinds & "N"
'    Next j
'    savoutinds = outinds
'
'    For i = 1 To count
'        If msunitary(i).unit_id > 0 Then
'        def = True
'        dprint "Artif unit=" & msunitary(i).name
'        outinds = savoutinds 'start out with all N
'        'getdefaultsforthisunit
'        sql = "select up.unit_param_id,up.default_admission_profile from unit_param up inner join unit u on (up.unit_id = u.unit_id)"
'        sql = sql & " where u.unit_id=" & msunitary(i).unit_id
'        sql = sql & " and getdate() between up.effective_datetime and up.expiration_datetime"
'        rs.Open sql, g_cnADO
'        If Not rs.EOF Then
'            If Not (IsNull(rs(0)) Or IsNull(rs(1))) Then
'            upid = rs(0)
'            admp = rs(1)
'            rs.Close
'            sql = "select indicators from patient_profile where unit_param_id=" & upid & " and profile_number=" & admp
'            rs.Open sql, g_cnADO
'            If Not rs.EOF Then
'                indstr = rs(0)
'                rs.Close
'                definds() = Split(indstr, ",") 'indicators are comma-delimited
'                For j = 0 To UBound(definds)
'                    If IsNumeric(definds(j)) Then
'                        Mid$(outinds, definds(j), 1) = "Y"
'                    End If
'                Next j
'            Else
'                def = False
'                dprint "No default admission class profile"
'                rs.Close
'            End If
'            Else
'                def = False
'                dprint "No default admission class-some ids are null"
'                rs.Close
'            End If
'        Else
'            def = False
'            dprint "No default admission class-No unit params"
'            rs.Close
'        End If
'
'        If def Then
'        outstr = savoutstr
'        Mid$(outstr, 10, 16) = msunitary(i).name & Space(16 - Len(msunitary(i).name)) 'now outstr is changed
'        Mid$(outstr, 27, 16) = msunitary(i).name & Space(16 - Len(msunitary(i).name))
'        outstr = outstr & outinds
'        dprint "getting patients..."
'        sql = "select acct_number from encounter where encounter_id in "
'        sql = sql & "(select distinct(el.encounter_id) from encounter_location el "
'        sql = sql & " inner join unit u on (u.unit_id=el.working_unit_id) "
'        sql = sql & "where el.datetime_out is null and dateadd(hh,3,el.datetime_in)>=getdate() and u.unit_id=" & msunitary(i).unit_id & ")"
'        rs.Open sql, g_cnADO
'        If Not rs.EOF Then
'            count = rs.RecordCount
'            dprint "Artificial default patient count=" & count
'            rs.MoveFirst
'            For j = 1 To count
'                acct = rs(0)
'                Mid$(outstr, 70, 20) = acct & Space$(20 - Len(acct)) 'dont assume all accts are same len
'                Print #outfile, outstr
'                Print #outlogfile, outstr
'                rs.MoveNext
'            Next j
'        End If
'        rs.Close
'        End If 'def
'        End If 'msunitary
'    Next i
'
'    Set rs = Nothing
'End Sub

