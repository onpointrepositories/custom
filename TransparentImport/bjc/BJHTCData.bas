Attribute VB_Name = "MainModule"
Option Explicit
'Reads a text file into the TCDATA table.
'TCDATA is the dictionary table in 8.1  (vs TC_DATA is the charting data in 8.0.3.2)
'
'SAMPLE DATA
'Comma-delimited
'1,BJH IN DOPamine Order Based Drip,1,DOPamine Order Based Drip,2,25.00000,1
'

Public g_cnADO      As ADODB.Connection             'main DB connection
Public g_util       As New PFSUtility               'utility classes
Public g_dbutil     As New PFSDBUtility
Public g_command    As String                       'command line

Dim infn As String



Sub Main()
    
    Set g_cnADO = g_dbutil.NewRemoteConnection
    
    infn = "C:\bjhtcdata.txt"
    Process

    Set g_cnADO = Nothing
End Sub


Private Sub Process()
    Dim buf As String
    Dim p As Integer
    Dim i As Long
    Dim formloading As Boolean
    Dim s As String
    Dim rows() As String
    Dim cols() As String
    Dim maxrow As Long
    Dim iRow As Long
    Dim n As Integer
    Dim datafile As Integer
    Dim hdr As String
    Dim count As Integer
    
    On Error GoTo errProcess
        
    formloading = True
'    cmdProcess.Enabled = False
'    cmdOutput.Enabled = False
    datafile = FreeFile
    Open infn For Input As #datafile
    s = Input(LOF(datafile), datafile)
    rows() = Split(s, vbCrLf)
    maxrow = UBound(rows)

    'NEW INSERTS:
    'Write the datafile to TC_DATA table
    For iRow = 0 To maxrow
        buf = rows(iRow)
        buf = Replace(buf, "'", "")
        cols() = Split(buf, vbTab)

'SAMPLE DATA
'Comma-delimited
'1<TAB>BJH IN DOPamine Order Based Drip<TAB>1<TAB>DOPamine,IV2,IV3<TAB>2<TAB>25.00000<TAB>1<TAB>count

'ind int
'code VarChar(max)
'opcode int
'descr varchar(max)
'opdescr int
'result VarChar(Max)
'opresult int
'count int

        If Trim$(buf) = "" Then 'skip blank line
        ElseIf UBound(cols) < 7 Then 'skip incomplete lines
        Else
            hdr = "INSERT INTO TCDATA (ind,code,opcode,descr,opdescr,result,opresult,count) VALUES ("
            hdr = hdr & g_dbutil.SQL_Number(Trim$(cols(0))) & ","  'indicator
            hdr = hdr & g_dbutil.SQL_String(Trim$(cols(1))) & ","  'code
            hdr = hdr & g_dbutil.SQL_Number(Trim$(cols(2))) & ","  'operator for code
            hdr = hdr & g_dbutil.SQL_String(Trim$(cols(3))) & ","  'description or description list
            hdr = hdr & g_dbutil.SQL_Number(Trim$(cols(4))) & ","  'operator for description
            hdr = hdr & g_dbutil.SQL_String(Trim$(cols(5))) & ","  'result or result list
            hdr = hdr & g_dbutil.SQL_Number(Trim$(cols(6))) & ","  'operator for result
            hdr = hdr & g_dbutil.SQL_Number(Trim$(cols(7))) & ")"  'threshold count for triggering indicator (> count)
            g_cnADO.Execute hdr
            count = count + 1
        End If
    Next
    
    MsgBox count & " rows were added to TCDATA"
    Close #datafile
    
    Exit Sub
errProcess:
        
End Sub

