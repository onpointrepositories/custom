Attribute VB_Name = "MainModule"
Option Explicit
'The location of the input is determined by the first line of the dictionary.
'
' This is the main module for SiePFS
' File is formatted as:
'    <Header>
'       <Event>
'       <Event>
'         ...
'
'TESTING parms:
'  -debug -range=720 -effective=0700

Public g_cnADO      As ADODB.Connection             'main DB connection
Public g_util       As New PFSUtility               'utility classes
Public g_dbutil     As New PFSDBUtility
Public g_command    As String                       'command line


Const TRANSP_FILENAME = "Transparent.txt"
Const TRANSP_INLOG_FILE_LIFE = 10 'days
Const TRANSP_OUTLOG_FILE_LIFE = 10 'days
Const TRANSP_DEBUG_FILE_LIFE = 10 'days
Const MSDIC_FNAME = "MSDICT.DAT"
Const MHDIC_FNAME = "MHDICT.DAT"
Const DNLD_FNAME = "TCQUERY"
Const VALIDMS_FNAME = "VALIDMS.UNT"
Const VALIDMH_FNAME = "VALIDMH.UNT"
Const PERDATA_FNAME = "PERSIST.DAT"
Const DBTABLENAME = "TC_DATA"

'HEADER RECORD
Const MAX_INDICATORS = 50
Const MAX_PROC = 10

Const START_ACCT_NUM = 1
Const LEN_ACCT_NUM = 20

Const START_PTNAME = 21
Const LEN_PTNAME = 30

Const START_UNIT = 51
Const LEN_UNIT = 10

Const START_RM = 61
Const LEN_RM = 10

Const START_BED = 71
Const LEN_BED = 5

Const START_FACILITY = 76
Const LEN_FAC = 5

'EVENT RECORD
Const START_EVENT_DT = 5
Const LEN_DT = 14

Const START_EVENT = 20
Const LEN_EVENT = 30

Const START_EVENT_DSC = 50
Const LEN_EVENT_DSC = 25

Const START_FREQ = 75
Const LEN_FREQ = 10

Const START_RESULT = 116
Const LEN_RESULT = 75

Const MAX_RANGE = 1440  '24 hrs in minutes; this is overridden by the -range arg.

Const RANGE_FACTOR = 24 * 60   'For proportioning the count of Med/Pulm/Cardio

Const MAX_UNIQUE_CHART_TIMES = 20
Const MAX_NUMPROCS = 20

Dim infn As String
Dim fnames() As String
Dim inlogname As String
Dim outfn As String
Dim outlogfile As Integer
Dim outlogname As String
Dim dbugfile As Integer
Dim dbugname As String

'Private Type indicator
'  indwinpfs As Integer  'winpfs indic number associated with this cerner event
'  eventid As String     'Event_CD of this indicator
'  chartkey As String    'look for this key in charted result
'  gavefreq As Boolean   'file supplied the frequency; ignore subsequent freqs
'  check_freq As Boolean 'flag to check frequency
'  freq_basis As Integer 'frequency required for this indicator (in minutes)
'  act_freq As Integer   'calculated frequency from data (in minutes)
'  last_datetime As String 'last date time of event found in this patient
'  num_found As Integer  'number of times this was found in this patient
'  also_mark As String   'if this indicator is marked, then also mark these.
'  'charttime(MAX_UNIQUE_CHART_TIMES) As String       commented out and in code to save mem space 9/26/07
'  one_row_find As Single
'  persist As Boolean  'flag if this charting element persists
'  perlen As Single    'for how long does it persist? in minutes
'  icupersist As Boolean 'persist for regardless of icu, represented by & in dictionary
'End Type

Private Type indicator_data
    checked As Boolean
    also_mark As String
End Type

Private Type ProcessedInfo
    a As String     'acctnum
    already_did_persist As Boolean
End Type

Private Type procdata
    pnum    As Integer
    start   As Date
    finish  As Date
    isvalid As Boolean 'used in combining procs
    pindex As Integer 'used in combining procs = the index of procs whose start/finish times are equal to this one
End Type

'Dim dicary() As indicator
'Dim mhdicary() As indicator
Dim dicnum As Integer
Dim mhdicnum As Integer
Dim inDirPath As String  'path of input file
Dim winpfspath As String 'path of winpfs
Dim winlogpath As String 'path of winpfs\log
Dim winloadpath As String ' path of winpfs\load_me

Dim datafile As Integer
Dim outfile As Integer

Dim inds(MAX_INDICATORS) As indicator_data
Dim grps(MAX_INDICATORS) As Integer
Dim mhinds(MAX_INDICATORS) As indicator_data
Dim mhgrps(MAX_INDICATORS) As Integer

Dim FullName As String
Dim lastname As String
Dim firstname As String
Dim unitname As String
Dim roomname As String
Dim bedname As String
Dim acctnum As String
Dim classdt As String
Dim classdate As String
Dim classtime As String
Dim nowdt As Date
Dim intime As String
Dim saveintime As String

Dim range As Single  'number of minutes in the scope of time for freq.
Dim dbugon As Boolean
Dim suppressDbugLog As Boolean
Dim effdateon As Boolean
Dim effdate As String
Dim efftimeon As Boolean
Dim efftime As String
Dim pulltimeon As Boolean
Dim pulltime As String
Dim pulldateon As Boolean
Dim pulldate As String
Dim fntime As String  'file name ending

Dim MSunitary() As String
Dim MHunitary() As String
Dim unitnum As Integer
Dim mhunitnum As Integer

Dim acctnumdirectory() As ProcessedInfo
Dim numacct As Single
Dim telemetry As Boolean

Dim numprocs As Integer
Dim procs(MAX_NUMPROCS) As procdata
Dim gtime As Integer
Dim ptime1, ptime2 As Date

Dim ettube As Boolean
Dim camicu As Boolean
Dim exclusionRange As String



Sub Main()
    Const RANGE_PARAM = "-range="
    Const EFFECTIVE_PARAM = "-effective="
    Const DBUG_ON = "-debug"
    Const ALTER_DATE = "-effdate="
    Const PULL_TIME = "-pulltime="
    Const PULL_DATE = "-pulldate="
    Const FILENAMETIME = "-fntime="
    Dim n As Integer
    Dim i As Integer
    Dim cmdLine As String
    Dim epos As Integer
    Dim rpos As Integer
    Dim effective As String
    Dim h As String
    Dim t As Date
    Dim adpos As Integer
    
    Set g_cnADO = g_dbutil.NewRemoteConnection
    '-efftime=hhmm  This is the time at which the classification is effective.
    '                 The date associated with this time is taken from header classdate.
    '                 If not specified, then -efftime is assumed to be the classtime
    '                 from the header.
    '-effdate=yyyymmdd   This is the date which, in combination with the
    '                 -effective time is used to specify the effective datetime.
    '                 If not specified, then -effdate is assumed to be the classdate
    '                 from the header.
    'Special value:  -effdate=yesterday means Now's yesterday.
        
    '-pulltime=hhmm  This is the time starting from which the pull is to
    '                look backwards from.
    '                If not specified, then this time is assumed to be the -effective time.
    '-pulldate=yyyymmdd This is the date of the pulltime.
    '                If not specified, then this is assumed to be Now's date.
    
    '-range=nnnn  This is the number of minutes backwards from the pull time
    '             that defines valid range of charting events.
    
    nowdt = Now
    cmdLine = LCase(Command$)
    
    rpos = InStr(cmdLine, RANGE_PARAM)
    epos = InStr(cmdLine, EFFECTIVE_PARAM)
    
    dbugon = (InStr(cmdLine, DBUG_ON) > 0)
    suppressDbugLog = False
    
    effdateon = (InStr(cmdLine, ALTER_DATE) > 0)
    If effdateon Then
        adpos = InStr(cmdLine, ALTER_DATE)
        If UCase$(Mid$(cmdLine, adpos + Len(ALTER_DATE), 8)) = "YESTERDA" Then
            effdate = Format(DateAdd("d", -1, g_util.DateOnly(nowdt)), "yyyymmdd")
        Else
            effdate = Mid$(cmdLine, adpos + Len(ALTER_DATE), 8) 'yyyymmdd
        End If
    End If
    
    efftimeon = (InStr(cmdLine, EFFECTIVE_PARAM) > 0)
    
    pulltimeon = (InStr(cmdLine, PULL_TIME) > 0)
    If pulltimeon Then
        pulltime = Mid$(cmdLine, InStr(cmdLine, PULL_TIME) + Len(PULL_TIME), 4)
    End If
    
    pulldateon = (InStr(cmdLine, PULL_DATE) > 0)
    If pulldateon Then
        pulldate = Mid$(cmdLine, InStr(cmdLine, PULL_DATE) + Len(PULL_DATE), 8)
    End If

    If epos > 0 Then
        effective = Mid$(cmdLine, InStr(cmdLine, EFFECTIVE_PARAM) + Len(EFFECTIVE_PARAM), 4)
    End If
    
    If rpos > 0 Then
        range = val(Mid$(cmdLine, InStr(cmdLine, RANGE_PARAM) + Len(RANGE_PARAM), 4))
    End If
    
    If range <= 0 Then
        range = MAX_RANGE
    End If
    
    If (InStr(cmdLine, FILENAMETIME) > 0) Then
        fntime = Mid$(cmdLine, InStr(cmdLine, FILENAMETIME) + Len(FILENAMETIME), 4)
    Else
        fntime = "????"
    End If
'    If effective = "" Then
'        effective = Format(nowdt, "hhnn")
'    End If
    
    intime = ""
    
    If (LoadDictionaries) Then
        InitGroups
        MakeLogFiles
        dprint "range=" & range
        dprint "effdate=" & effdate
        dprint "effective=" & effective
        dprint "pulltime=" & pulltime
        dprint "pulldate=" & pulldate
        If effective <> "" Then
            ' command parameter needs to be in form hhmm ONLY
            If IsNumeric(effective) Then
                If Len(effective) < 4 Then
                    If Len(effective) = 1 Then
                        effective = "000" & effective
                    ElseIf Len(effective) = 2 Then
                        effective = "00" & effective
                    Else
                        effective = "0" & effective
                    End If
                Else
                    effective = Mid$(effective, 1, 4)
                End If
                effective = Mid$(effective, 1, 2) & ":" & Mid$(effective, 3, 2)
                t = CDate(effective)
                If IsDate(t) Then
                    effective = Mid$(effective, 1, 2) & Mid$(effective, 4, 2)
                    'i = year(g_util.DateOnly(FileDateTime(inlogname)))
                    'i = Format(nowdt, "yyyy")
                    'intime = CStr(i) & Mid$(inlogname, Len(inlogname) - Len("mmdd.log") + 1, Len("mmdd")) & effective
                    intime = Format(nowdt, "yyyy") & Mid$(inlogname, Len(inlogname) - Len("mmdd.log") + 1, Len("mmdd")) & effective
                    pulltime = Format(nowdt, "yyyy") & Mid$(inlogname, Len(inlogname) - Len("mmdd.log") + 1, Len("mmdd")) & pulltime
                    If effdateon Then
                        intime = effdate & Mid$(intime, 9, 4)
                    End If
                    saveintime = intime
                End If
            End If
        End If
        n = GetAllInputFilenames
'        i = 0      ONLY PROCESS THE LATEST FILE 11/4/05
        i = n - 1  'ONLY PROCESS THE LATEST FILE 11/4/05
'Open the Persist file, removing old items, and read acctnums into the acctnumdirectory array
        While i < n
            i = i + 1
            infn = winloadpath + "\" + fnames(i)
            dprint "Processing filename=" & fnames(i)
            Process
        Wend
        CloseLogFiles
        DeleteOldLogs
    End If

    Set g_cnADO = Nothing
End Sub
Private Function LoadDictionaries() As Boolean
    Dim dicfn As String
    Dim dicfile As Integer
    Dim buf As String
    Dim unitfn As String
    Dim unitfile As Integer
    
'Valid Med Surg units
    unitnum = 0
    unitfile = FreeFile
    unitfn = App.Path & "\" & VALIDMS_FNAME
    Open unitfn For Input As #unitfile
    While Not EOF(unitfile)
        Line Input #unitfile, buf
        If Trim$(buf) <> "" Then
            unitnum = unitnum + 1
            ReDim Preserve MSunitary(0 To unitnum)
            MSunitary(unitnum) = UCase$(Trim$(Mid$(buf, 1, 16)))
        End If
    Wend
    Close #unitfile
    
'Valid MH units
    mhunitnum = 0
    unitfile = FreeFile
    unitfn = App.Path & "\" & VALIDMH_FNAME
    Open unitfn For Input As #unitfile
    While Not EOF(unitfile)
        Line Input #unitfile, buf
        If Trim$(buf) <> "" Then
            mhunitnum = mhunitnum + 1
            ReDim Preserve MHunitary(0 To mhunitnum)
            MHunitary(mhunitnum) = UCase$(Trim$(Mid$(buf, 1, 16)))
        End If
    Wend
    
'don't need the unit file anymore
    Close #unitfile
    
'init wild charting array
    
    LoadDictionaries = True
    
End Function

Private Function GetAllInputFilenames() As Integer
    'The location is determined by the first line of the dictionary.
    Const INITIAL_BLOCK_SIZE = 24
    Dim i As Integer
    Dim infname As String
    Dim todaystr As String
    
    outfn = winloadpath & "\" & TRANSP_FILENAME
    
    ReDim fnames(0 To INITIAL_BLOCK_SIZE) As String
    i = 0
    
    todaystr = Format$(Date, "yyyymmdd")
    infname = Dir$(winloadpath + "\" & DNLD_FNAME & todaystr & fntime & ".TXT") 'returns ONLY the filename.  For wildcards.
'    infname = Dir$(winloadpath + "\" & DNLD_FNAME & "2011mmdd" & 0700 & ".TXT") 'returns ONLY the filename.  For wildcards.
    dprint "infname=" & infname
    While infname <> ""
        i = i + 1
        If (i > UBound(fnames)) Then
            ReDim Preserve fnames(0 To i + INITIAL_BLOCK_SIZE) As String
        End If
        fnames(i) = UCase$(infname)
        infname = Dir$
    Wend
    
    BubbleSort fnames, i
    
    GetAllInputFilenames = i
    
End Function

Private Sub BubbleSort(arr As Variant, numEls As Integer)
    Dim lLoop1 As Integer
    Dim lLoop2 As Integer
    Dim lTemp As Variant
    
    For lLoop1 = numEls To LBound(arr) Step -1
       For lLoop2 = LBound(arr) + 1 To lLoop1
           If arr(lLoop2 - 1) > arr(lLoop2) Then
              lTemp = arr(lLoop2 - 1)
              arr(lLoop2 - 1) = arr(lLoop2)
              arr(lLoop2) = lTemp
           End If
       Next lLoop2
   Next lLoop1
End Sub

Private Sub Process()
    Dim buf As String
    Dim p As Integer
    Dim i As Long
    Dim formloading As Boolean
    Dim s As String
    Dim rows() As String
    Dim cols() As String
    Dim maxrow As Long
    Dim iRow As Long
    Dim n As Integer
    
    Dim rsacct As New Recordset
    Dim sql As String
    Dim hdr As String
    Dim count As Long
    Dim h As String
    Dim rowcount As Integer
    Dim persistcount As Integer
    On Error GoTo errProcess
        
    formloading = True
'    cmdProcess.Enabled = False
'    cmdOutput.Enabled = False
    dprint "Process"
    datafile = FreeFile
    Open infn For Input As #datafile
    s = Input(LOF(datafile), datafile)
    dprint "Size of input file string=" & Len(s)
    rows() = Split(s, vbCrLf)
    dprint "Now after rows split"
    maxrow = UBound(rows)
    dprint "Max rows=" & UBound(rows)
    
    outfile = FreeFile
    Open outfn For Append As #outfile
    
    
    If Mid$(intime, 9, 4) = "0700" Then
        ptime1 = Now
        AddArtificialDefaults
        ptime2 = Now
        gtime = DateDiff("s", ptime1, ptime2)
        dprint "Time ArtificialDefaults=" & gtime
    End If
    
    sql = "DELETE TC_DATA"
    g_cnADO.Execute sql
    dprint "TC_DATA deleted"
    'NEW INSERTS:
    'Write the datafile to TC_DATA table
    ptime1 = Now
    For iRow = 0 To maxrow
        buf = rows(iRow)
        buf = Replace(buf, "'", "")
        cols() = Split(buf, vbTab)

'SAMPLE DATA
'Tab-delimited
'124590527               10100   1991-05-14 00:00:00.000 BJHCLINDFIVEEFNCTTESTSCMFIVE    2010-05-24 08:00:00.000 BJH IN D5W  D5W 100.00000
'124590527               10100   1991-05-14 00:00:00.000 BJHCLINDFIVEEFNCTTESTSCMFIVE    2010-05-24 08:00:00.000 BJH IN Dietary  Dietary 80.00000
'124590527               10100   1991-05-14 00:00:00.000 BJHCLINDFIVEEFNCTTESTSCMFIVE    2010-05-24 08:00:00.000 BJH IN DOPamine Order Based Drip    DOPamine Order Based Drip   25.00000

'acctnum VarChar(50)
'unit VarChar(50)
'dob datetime
'name varchar(50)
'perfdt datetime
'code VarChar(Max)
'descript VarChar(Max)
'res VarChar(Max)

' LONG UNIT NAMES FROM ECLIPSYS need to be renamed to an alias  10/19/10
'13100 Tower Place: 13100
'4300 Observation Unit: 4300 OU
'5400 Special Care Nursery 5: 5400 SCN 5   on  hold
'5400 Special Care Nursery 6: 5400 SCN 6   on hold
'5400 Special Care Nursery 7: 5400 SCN 7   on hold
'5400 Special Care Nursery 8: 5400 SCN 8   on hold
'6200 Observation Unit: 6200 OU
'PACU Critical Care: Pacu CCA


        If Trim$(buf) = "" Then 'skip blank line
        ElseIf UBound(cols) < 7 Then 'skip incomplete lines
        Else
            Select Case UCase$(Trim$(cols(1)))
                Case "13100 TOWER PLACE"
                    cols(1) = "13100"
                Case "4300 OBSERVATION UNIT"
                    cols(1) = "4300 OU"
                Case "6200 OBSERVATION UNIT"
                    cols(1) = "6200 OU"
                Case "PACU CRITICAL CARE AREA"
                    cols(1) = "PACU CCA"
                Case "16400 OBSERVATION UNIT"
                    cols(1) = "16400 OU"
                Case "16300 OBSERVATION UNIT"
                    cols(1) = "16300 OU"
            End Select
            hdr = "INSERT INTO TC_DATA (acctnum,unit,name,perfdt,code,descript,res) VALUES ("
            hdr = hdr & "'" & Trim$(cols(0)) & "',"
            hdr = hdr & "'" & Trim$(cols(1)) & "',"
            hdr = hdr & "'" & Trim$(cols(3)) & "',"
            hdr = hdr & "'" & Trim$(cols(4)) & "',"
            hdr = hdr & "'" & Trim$(cols(5)) & "',"
            hdr = hdr & "'" & Trim$(cols(6)) & "',"
            hdr = hdr & "'" & Trim$(cols(7)) & "')"
            g_cnADO.Execute hdr
        End If
    Next
    
    ptime2 = Now
    gtime = DateDiff("s", ptime1, ptime2)
    dprint "Time Inserts=" & gtime


    sql = "SELECT DISTINCT(ACCTNUM),UNIT,NAME FROM TC_DATA"
    rsacct.Open sql, g_cnADO
    count = rsacct.RecordCount
    dprint "Total patient count=" & count
    rsacct.MoveFirst
    For i = 1 To count
        InitIndicators
        acctnum = rsacct(0)
        unitname = rsacct(1)
        FullName = rsacct(2)
        dprint FullName & "," & unitname & "," & acctnum
        
        If ValidMSUnit(unitname) Then
            Translate (1)
'        ElseIf ValidMHUnit(unitname) Then
'            Translate (2)
        End If
        If Not rsacct.EOF Then rsacct.MoveNext
    Next i
    rsacct.Close
    Set rsacct = Nothing
    Close #datafile
    Close #outfile
    Erase procs
    
    p = InStrRev(infn, "\")
    Name infn As winlogpath & Mid$(infn, p, Len(infn) - p + 1) 'move data file to ..\log folder
    
    Exit Sub
errProcess:
        
End Sub
Private Sub AddArtificialDefaults()
    Dim rs As New Recordset
    Dim sql As String
    Dim i, j, count As Integer
    Dim upid As Long
    Dim admp As Integer
    Dim indstr As String
    Dim definds() As String
    Dim outinds, savoutinds As String
    Dim acct, outstr, savoutstr As String
    Dim def As Boolean

    
    intime = Mid$(intime, 1, 12)
    outstr = Space$(9) & "" '*unitname  '10
    outstr = outstr & Space$(26 - Len(outstr)) '26
    outstr = outstr & "" '*unitname '27
    outstr = outstr & Space$(60 - Len(outstr)) '60
    outstr = outstr & Mid$(intime, 1, 8) & Space$(1) '69
    outstr = outstr & Space$(21) '**acctnum & Space$(20 - Len(acctnum) + 1) '90
    outstr = outstr & Space$(33) 'lastname & Space$(32 - Len(lastname) + 1) '123
    outstr = outstr & Space$(33) 'firstname & Space$(32 - Len(firstname) + 1) '156
    outstr = outstr & Space$(32 + 1) '189
    outstr = outstr & Space$(9) 'roomname & Space$(8 - Len(roomname) + 1) '198
    outstr = outstr & Space$(5) 'bedname & Space$(4 - Len(bedname) + 1) '20
    outstr = outstr & intime & Space$(1) 'classdatetime 216
    outstr = outstr & Space$(78 + 1) '295
    outstr = outstr & intime & Space$(12 - Len(intime) + 1) '308
    outstr = outstr & Space$(70) '378
    savoutstr = outstr
    
    outinds = ""
    For j = 1 To MAX_INDICATORS
        outinds = outinds & "N"
    Next j
    savoutinds = outinds
    
    For i = 1 To unitnum
        If MSunitary(i) <> "" Then 'note that msunitary is already trimmed and ucase
        def = True
        dprint "Artif unit=" & MSunitary(i)
        outinds = savoutinds 'start out with all N
        'getdefaultsforthisunit
        sql = "select up.unit_param_id,up.default_admission_profile from unit_param up inner join unit u on (up.unit_id = u.unit_id)"
        sql = sql & " inner join unit_alias ua on (ua.unit_id=u.unit_id) where ua.unit_alias_name='" & MSunitary(i) & "'"
        sql = sql & " and getdate() between up.effective_datetime and up.expiration_datetime"
        rs.Open sql, g_cnADO
        If Not rs.EOF Then
            If Not (IsNull(rs(0)) Or IsNull(rs(1))) Then
            upid = rs(0)
            admp = rs(1)
            rs.Close
            sql = "select indicators from patient_profile where unit_param_id=" & upid & " and profile_number=" & admp
            rs.Open sql, g_cnADO
            If Not rs.EOF Then
                indstr = rs(0)
                rs.Close
                definds() = Split(indstr, ",") 'indicators are comma-delimited
                For j = 0 To UBound(definds)
                    If IsNumeric(definds(j)) Then
                        Mid$(outinds, definds(j), 1) = "Y"
                    End If
                Next j
            Else
                def = False
                dprint "No default admission class profile"
                rs.Close
            End If
            Else
                def = False
                dprint "No default admission class-some ids are null"
                rs.Close
            End If
        Else
            def = False
            dprint "No default admission class-No unit params"
            rs.Close
        End If
        
        If def Then
        outstr = savoutstr
        Mid$(outstr, 10, 16) = MSunitary(i) & Space(16 - Len(MSunitary(i))) 'now outstr is changed
        Mid$(outstr, 27, 16) = MSunitary(i) & Space(16 - Len(MSunitary(i)))
        outstr = outstr & outinds
        dprint "getting patients..."
        sql = "select acct_number from encounter where encounter_id in "
        sql = sql & "(select distinct(el.encounter_id) from encounter_location el "
        sql = sql & " inner join unit u on (u.unit_id=el.working_unit_id) "
        sql = sql & " inner join unit_alias ua on (u.unit_id=ua.unit_id) "
        sql = sql & "where el.datetime_out is null and dateadd(hh,3,el.datetime_in)>=getdate() and ua.unit_alias_name='" & MSunitary(i) & "')"
        rs.Open sql, g_cnADO
        If Not rs.EOF Then
            count = rs.RecordCount
            dprint "Artificial default patient count=" & count
            rs.MoveFirst
            For j = 1 To count
                acct = rs(0)
                Mid$(outstr, 70, 20) = acct & Space$(20 - Len(acct)) 'dont assume all accts are same len
                Print #outfile, outstr
                Print #outlogfile, outstr
                rs.MoveNext
            Next j
        End If
        rs.Close
        End If 'def
        End If 'msunitary
    Next i

    Set rs = Nothing
End Sub

Private Sub Translate(meth As Integer)

    If meth = 1 Then 'Inpatient
        ptime1 = Now
        Check1_2_3
        
        Check4
        Check5_6
        Check7
        Check8
        Check9_10
        Check11_12
        Check13
        ptime2 = Now
        gtime = DateDiff("s", ptime1, ptime2)
        dprint "Time inds 1-13=" & gtime
        
        ptime1 = Now
        CheckAssessments
        ptime2 = Now
        gtime = DateDiff("s", ptime1, ptime2)
        dprint "Time inds 14-17=" & gtime
        
        ptime1 = Now
        Check18
        Check19_20
        Check21
        ptime2 = Now
        gtime = DateDiff("s", ptime1, ptime2)
        dprint "Time inds 18-21=" & gtime
        
        ptime1 = Now
        Check22
        ptime2 = Now
        gtime = DateDiff("s", ptime1, ptime2)
        dprint "Time inds 22=" & gtime
        DoProcedures
    ElseIf meth = 2 Then 'PFS/WM Mental Health
'        hCheck1to10
'        hCheck11to20
'        hCheck21to49
    End If
    
    ParsePatientInfo
    AssembleOutput
    
End Sub

Private Sub InitIndicators()
    Dim i As Integer
    
    For i = 1 To MAX_INDICATORS
        inds(i).checked = False
        inds(i).also_mark = ""
        mhinds(i).checked = False
        mhinds(i).also_mark = ""
    Next i
    
    numprocs = 0
    Erase procs
    
    ettube = False
    camicu = False
End Sub
Private Sub InitGroups()
    Dim i As Integer
    
    For i = 1 To MAX_INDICATORS
        grps(i) = 0
    Next i
    
    For i = 1 To 3
        grps(i) = 1
    Next i
    For i = 5 To 6
        grps(i) = 2
    Next i
    For i = 9 To 10
        grps(i) = 3
    Next i
    For i = 11 To 12
        grps(i) = 4
    Next i
    For i = 14 To 17
        grps(i) = 5
    Next i
    For i = 19 To 20
        grps(i) = 6
    Next i
    
    
    For i = 1 To MAX_INDICATORS
        mhgrps(i) = 0
    Next i
    
    For i = 1 To 4
        mhgrps(i) = 1
    Next i
    For i = 9 To 10
        mhgrps(i) = 2
    Next i
    For i = 11 To 13
        mhgrps(i) = 3
    Next i
    For i = 14 To 15
        mhgrps(i) = 4
    Next i
    For i = 22 To 24
        mhgrps(i) = 5
    Next i
    
End Sub

Private Function ItemExists(col As Collection, key As String) As Boolean
    Dim dummy As Variant
    On Error Resume Next
    dummy = col.Item(key)
    ItemExists = (Err <> 5)
End Function

Private Sub AtLeastOneADL()

    If Not (inds(1).checked Or inds(2).checked Or inds(3).checked) Then
        inds(2).checked = True
        dprint "via AtLeastOneADL"
    End If
End Sub
Private Sub mhAtLeastOneADL()

    If Not (mhinds(1).checked Or mhinds(2).checked Or mhinds(3).checked Or mhinds(4).checked) Then
        mhinds(2).checked = True
        dprint "via AtLeastOneADL"
    End If
End Sub



Private Sub ParsePatientInfo()
    Dim commapos As Integer
    
    commapos = InStr(FullName, ",")
    If commapos = 0 Then
        lastname = FullName
        firstname = ""
    Else
        lastname = Mid$(FullName, 1, commapos - 1)
        firstname = Mid$(FullName, commapos + 1, Len(FullName) - commapos)
    End If
    
    
'    If Not efftimeon Then intime = Trim$(Mid$(s, START_CLASS_DT, LEN_DT))
End Sub


Private Sub AssembleOutput()
    Dim outstr As String
    Dim i As Integer
    Dim din As Date
    Dim tin As Date
    Dim ok As Boolean
    Dim g As Integer
    Dim highest_is_on As Boolean
    
    dprint "Assemble output..."
    
    ok = (acctnum <> "") And (unitname <> "")
    
    If Not ok Then
        Exit Sub
    End If
    
    If ValidMSUnit(unitname) Then
        dprint "Valid tc unit..."
        g = 0
        highest_is_on = False
        For i = MAX_INDICATORS To 1 Step -1
            If (grps(i) > 0) Then
                If (grps(i) <> g) Then
                    g = grps(i)
                    highest_is_on = inds(i).checked
                Else
                    If highest_is_on Then
                        inds(i).checked = False
                    Else
                        highest_is_on = inds(i).checked
                    End If
                End If
            End If
        Next i
        AtLeastOneADL
    End If
    If ValidMHUnit(unitname) Then
        g = 0
        highest_is_on = False
        For i = MAX_INDICATORS To 1 Step -1
            If (mhgrps(i) > 0) Then
                If (mhgrps(i) <> g) Then
                    g = mhgrps(i)
                    highest_is_on = mhinds(i).checked
                Else
                    If highest_is_on Then
                        mhinds(i).checked = False
                    Else
                        highest_is_on = mhinds(i).checked
                    End If
                End If
            End If
        Next i
        mhAtLeastOneADL
    End If
    
    intime = Mid$(intime, 1, 12)
    
    outstr = Space$(9) & unitname  '10
    outstr = outstr & Space$(26 - Len(outstr)) '26
    outstr = outstr & unitname '27
    outstr = outstr & Space$(60 - Len(outstr)) '60
    outstr = outstr & Mid$(intime, 1, 8) & Space$(1) '69
'    outstr = outstr & classdate & Space$(1)
    outstr = outstr & acctnum & Space$(20 - Len(acctnum) + 1) '90
    outstr = outstr & lastname & Space$(32 - Len(lastname) + 1) '123
    outstr = outstr & firstname & Space$(32 - Len(firstname) + 1) '156
    outstr = outstr & Space$(32 + 1) '189
    outstr = outstr & roomname & Space$(8 - Len(roomname) + 1) '198
    outstr = outstr & bedname & Space$(4 - Len(bedname) + 1) '20
    outstr = outstr & intime & Space$(1) 'classdatetime 216
'    outstr = outstr & classdate & classtime & Space$(1)
    outstr = outstr & Space$(78 + 1) '295
    outstr = outstr & intime & Space$(12 - Len(intime) + 1) '308
    outstr = outstr & Space$(70) '378
    
    If ValidMSUnit(unitname) Then
        For i = 1 To MAX_INDICATORS
            If (inds(i).checked) Then
                outstr = outstr & "Y"
            Else
                outstr = outstr & "N"
            End If
        Next i
    ElseIf ValidMHUnit(unitname) Then
        For i = 1 To MAX_INDICATORS
            If (mhinds(i).checked) Then
                outstr = outstr & "Y"
            Else
                outstr = outstr & "N"
            End If
        Next i
    End If
    
    Print #outfile, outstr
    Print #outlogfile, outstr
    
    OutputProcs
End Sub
Private Function StrTimeToOutStr(t As String) As String
    Dim yyyy As String
    Dim mm As String
    Dim dd As String
    Dim hh As String
    Dim nn As String
    
        yyyy = Trim$(str(year(g_util.DateOnly(t))))
        mm = Trim$(str(month(g_util.DateOnly(t))))
        If month(g_util.DateOnly(t)) < 10 Then
            mm = "0" & mm
        End If
        dd = Trim$(str(day(g_util.DateOnly(t))))
        If day(g_util.DateOnly(t)) < 10 Then
            dd = "0" & dd
        End If
        hh = Trim$(str(Hour(g_util.TimeOnly(t))))
        If Hour(g_util.TimeOnly(t)) < 10 Then
            hh = "0" & hh
            If Hour(g_util.TimeOnly(t)) = 0 Then hh = "00"
        End If
        nn = Trim$(str(Minute(g_util.TimeOnly(t))))
        If Minute(g_util.TimeOnly(t)) < 10 Then
            nn = "0" & nn
            If Minute(g_util.TimeOnly(t)) = 0 Then nn = "00"
        End If
        StrTimeToOutStr = yyyy & mm & dd & hh & nn

End Function


Private Function CerDateToISO(d As String) As String
    Dim century As String
    ' Yields a string of the format yyyymmddhhnn
    
    If val(Mid$(d, 7, 2)) <= 50 Then
        century = "20"
    Else
        century = "19"
    End If
    CerDateToISO = century & Mid$(d, 7, 2) & Mid$(d, 1, 2) & Mid$(d, 4, 2) _
     & Mid$(d, 10, 2) & Mid$(d, 13, 2)

End Function


Private Sub MakeLogFiles()
    Const HKEY_LOCAL_MACHINE = &H80000002
    Dim dt As Variant
    
    If Not g_util.GetKeyValue(HKEY_LOCAL_MACHINE, "SOFTWARE\Quadramed\WinPFS", "Directory", winpfspath) Then
        winpfspath = App.Path
    End If

    If g_util.DirExists(winpfspath) Then
        winlogpath = winpfspath & "\log"
        winloadpath = winpfspath & "\load_me"
        If Not g_util.DirExists(winpfspath) Then
            MkDir$ (winlogpath)
        End If
    Else
        winlogpath = App.Path
        winloadpath = winlogpath
    End If

' For Temporary Testing
'    winlogpath = "c:\tc\log"
'    winloadpath = "c:\tc\load_me"
    
    
    dt = nowdt
    inlogname = winlogpath & "\TranspIn_" & Format$(dt, "mmdd") & ".log"
    outlogname = winlogpath & "\TranspOut_" & Format$(dt, "mmdd") & ".log"
    dbugname = winlogpath & "\DbugTransp_" & Format$(dt, "mmdd") & ".log"
    
    outlogfile = FreeFile
    Open outlogname For Append As #outlogfile
    Print #outlogfile, "**** WinPFS Transparent Classification Output    Time=" & nowdt & " ****"
    
    If dbugon Then
        dbugfile = FreeFile
        Open dbugname For Append As #dbugfile
        Print #dbugfile, "****TRANSPARENT TRANSLATION DEBUGGING MODE    Time=" & nowdt & " ****"
    End If
    
End Sub

Private Sub CloseLogFiles()

    Close #outlogfile
    If dbugon Then
        Close #dbugfile
    End If

    'inlogfile's dt = mid$(inlogname,len(inlogname)-len("mmdd.txt")+1,len("mmddhh.txt"))
'    FileCopy outfn, winlogpath & "\TranspOut_" & Mid$(inlogname, Len(inlogname) - Len("mmddhh.txt") + 1, Len("mmddhh.txt"))

End Sub

Private Sub DeleteOldLogs()
    Dim temp As String
    Dim i As Integer
    Dim dt As Variant

    ' Delete old log files
    ' (allow the interface to be down for up to 5 days)
    dt = DateAdd("d", -TRANSP_INLOG_FILE_LIFE, nowdt)
    For i = 1 To 5
        temp = winlogpath & "\TranspIn_" & Format$(dt, "mmdd") & ".log"
        If g_util.FileExists(temp) Then
            Kill temp
        End If
        dt = DateAdd("d", -1, dt)
    Next i

    dt = DateAdd("d", -TRANSP_OUTLOG_FILE_LIFE, nowdt)
    For i = 1 To 5
        temp = winlogpath & "\TranspOut_" & Format$(dt, "mmdd") & ".log"
        If g_util.FileExists(temp) Then
            Kill temp
        End If
        dt = DateAdd("d", -1, dt)
    Next i
    
    dt = DateAdd("d", -TRANSP_DEBUG_FILE_LIFE, nowdt)
    For i = 1 To 5
        temp = winlogpath & "\DbugTransp_" & Format$(dt, "mmdd") & ".log"
        If g_util.FileExists(temp) Then
            Kill temp
        End If
        dt = DateAdd("d", -1, dt)
    Next i

End Sub


Private Sub dprint(s As String)
    Dim sstat As String

    If Not dbugon Or suppressDbugLog Then
        Exit Sub
    End If
    
    Print #dbugfile, s
            
End Sub
Private Function ValidMSUnit(u As String) As Boolean
    Dim i As Integer
    
    For i = 1 To unitnum
        If UCase$(u) = MSunitary(i) Then
            ValidMSUnit = True
            Exit For
        End If
    Next i

End Function
Private Function ValidMHUnit(u As String) As Boolean
    Dim i As Integer
    
    For i = 1 To mhunitnum
        If UCase$(u) = MHunitary(i) Then
            ValidMHUnit = True
            Exit For
        End If
    Next i

End Function


Private Sub Check1_2_3()
    Dim lslist As String
    
    dprint "Check1_2_3"
    'If QLSDes("BJH VS BathAssist", "Bath_Complete by 1") Then inds(3).checked = True
    If QLSDes("BJH VS Oral care", "Oral care_Complete by 1") Then inds(3).checked = True
    If QLSDes("BJH VS Meals", "Feed_Complete") Then inds(3).checked = True
    If QLSDesList("BJH AS Fall Risk Low/High Per Protocol", "Complete Paralysis or completely immobilized") Then inds(3).checked = True
    If QLSDes("BJH VS BathAssist", "Bath_Complete by 2-3") Then inds(3).checked = True
    
    If inds(3).checked Then Exit Sub
    If QLSDes("BJH VS BathAssist", "Bath_Assist of 1") Then inds(2).checked = True
    If QLSDes("BJH VS Activity Assist", "Assist of 1") Then inds(2).checked = True
    If QLSDes("BJH VS Positioning Assist", "Assist of 1") Then inds(2).checked = True
    
    If inds(2).checked Then Exit Sub
    If QLSDes("BJH VS Oral care", "Oral care_Assist of 1") Then inds(2).checked = True
    If QLSDes("BJH VS Meals", "Feed_Assist") Then inds(2).checked = True
    If QLSLike("BJH IN TF") Then inds(2).checked = True
    
    If inds(2).checked Then Exit Sub
    If QLSDes("BJH VS BathAssist", "Bath_Self") Then inds(1).checked = True
    If QLSDes("BJH VS Meals", "Feed_Self") Then inds(1).checked = True
    
End Sub


Private Sub Check4()
    Dim lslist As String
    
    dprint "Check4"
    If QLSRes("BJHEDUNutrDietFeedTopic", "Swallowing") Then inds(4).checked = True
    If QLSDesList("BJH AS Neuro INV Activity and Tolerance", "Posterior Hip,Global Hip") Then inds(4).checked = True
    'If QLSDes("BJH AS Neuro INV Activity and Tolerance", "Global Hip") Then inds(4).checked = True
End Sub

Private Sub Check5_6()
    Dim lslist As String

    dprint "Check5_6"
    If QLSDes("BJH VS BathAssist", "4") Then inds(6).checked = True
    If QLSDes("BJH VS Activity Assist", "4") Then inds(6).checked = True
    If QLSDes("BJH VS Positioning Assist", "4") Then inds(6).checked = True
    If QLSDes("BJH VS Peri Care", "4") Then inds(6).checked = True

    If inds(6).checked Then Exit Sub
    If QLSDes("BJH VS BathAssist", "2-3") Then inds(5).checked = True
    If QLSDes("BJH VS PeriAssist", "2-3") Then inds(5).checked = True
    If QLSDes("BJH VS Activity Assist", "2-3") Then inds(5).checked = True
    
    If inds(5).checked Then Exit Sub
    If QLSDes("BJH VS Positioning Assist", "2-3") Then inds(5).checked = True
    If QLSDes("BJH VS Oral care", "2-3") Then inds(5).checked = True
End Sub

Private Sub Check7()
    Dim s As String

    If QLSCodeLikeResLikeListFreq("BJH RT Vent ArtAirway Type", "Endotracheal,Nasotracheal,Tracheostomy,LMA") Then
        inds(7).checked = True
        ettube = True
    End If

    If QLSDes("BJH Pro Speech Barriers", "Speech Limitations") Then inds(7).checked = True
    If QLSDes("BJH Pro Primary Speaking LanguageYN", "Yes") Then inds(7).checked = True
    If QLSDes("BJH Pro Reading", "Yes") Then inds(7).checked = True
    
    If inds(7).checked Then Exit Sub
    If QLSDesList("BJH Pro Physical Limitations", "Deaf,Blind,Hearing Impaired,Visually Impaired") Then inds(7).checked = True
    
    If inds(7).checked Then Exit Sub
    If QLSDesList("BJH VS Pain Tool Used", "Faces,Non Communicative") Then inds(7).checked = True
    
    If inds(7).checked Then Exit Sub
    If QLSDesList("BJH AS Resp Communication", "Electrolarynx,Paper and pencil,Sign board,Speaking valve") Then inds(7).checked = True
    'If QLSDes("BJH Pro Physical Limitations", "Blind") Then inds(7).checked = True
    'If QLSDes("BJH Pro Physical Limitations", "Hearing Impaired") Then inds(7).checked = True
    'If QLSDes("BJH Pro Physical Limitations", "Visually Impaired") Then inds(7).checked = True
    'If QLSDes("BJH Pro Cognitive Barriers", "Confused") Then inds(7).checked = True
    If inds(7).checked Then Exit Sub
    If QLSDesList("BJH Pro Cognitive Barriers", "Difficulty following instructions,Learning problems,Memory problems") Then inds(7).checked = True
    'If QLSDes("BJH Pro Cognitive Barriers", "Learning problems") Then inds(7).checked = True
    'If QLSDes("BJH Pro Cognitive Barriers", "Memory problems") Then inds(7).checked = True
    'If QLSDes("BJH Pro Cognitive Barriers", "Unable to comprehend") Then inds(7).checked = True
    'If QLSDes("BJH Pro Cognitive Barriers", "Unresponsive") Then inds(7).checked = True
    If inds(7).checked Then Exit Sub
    If QLSDesList("BJH Neu Speech", "Expressive Aphasia,Receptive Aphasia,Global Aphasia") Then inds(7).checked = True
    'If QLSDes("BJH Neu Speech", "Receptive Aphasia") Then inds(7).checked = True
    'If QLSDes("BJH Neu Speech", "Global Aphasia") Then inds(7).checked = True
    If inds(7).checked Then Exit Sub
    s = "Decreased Mental Status,Garbled speech,Mentally challenged,Slurred speech,"
    s = s & "Clanging speech,Cognitive impairment due to medical condition,"
    s = s & "Cognitive impairment due to psychiatric illness,"
    s = s & "Confabulation,Flight of ideas,Latency of response,Loose associations,"
    s = s & "Mute,Poverty of speech,Pressured speech,Responds tangentially"
    If QLSCodeLikeResLikeListFreq("BJH AS Psych Comm", s) Then inds(7).checked = True
    
    If inds(7).checked Then Exit Sub
' to be added to 811 data:
'7 BJH AS Neuro Vision 1 both eyes 2
'7 BJH AS Neuro Hearing 1  bilateral 2
'7 BJH AS Neuro Cognition/Memory 1
'7 BJH Pro Comm Learning Barrier 1  speech,cognitive 2
    
    If QLSDes("BJH AS Neuro Vision", "both eyes") Then inds(7).checked = True
    If QLSDes("BJH AS Neuro Hearing", "bilateral") Then inds(7).checked = True
    If QLS("BJH AS Neuro Cognition/Memory") Then inds(7).checked = True
    If QLSDesList("BJH Pro Comm Learning Barrier", "speech,cognitive") Then inds(7).checked = True
    
End Sub

Private Sub Check8()
    Dim lslist As String
    Dim count As Integer
    Dim nacount As Integer

    dprint "Check8"
    If QLSDesList("BJH AS CAMOverall Score", "Positive") Then
        inds(8).checked = True
        camicu = True
    End If
    
    If ettube And camicu Then
        inds(3).checked = True
        inds(5).checked = True
        inds(7).checked = True
        inds(8).checked = True
        dprint "ET tube and CAMICU triggers 3,5,7,8"
    End If
    
    If QLSDes("BJH AS Neuro INV Cognitive Needs", "Cognitive Needs Oriented to") Then inds(8).checked = True
    'If QLSDes("BJH AS Neuro INV Safe Environment", "Performed Interventions") Then inds(8).checked = True
    
    
    '4/4/11 to change: not for does not regard examiner
    If QLSDes("BJH AS Neuro Cognition/Memory", "Cognition/Memory") Then inds(8).checked = True
    
    If inds(8).checked Then Exit Sub
    If QLSDesList("BJH Pro Cognitive Barriers", "Learning problems,Memory problems") Then inds(8).checked = True
    'If QLSDes("BJH Pro Cognitive Barriers", "Learning problems") Then inds(8).checked = True
    'If QLSDes("BJH Pro Cognitive Barriers", "Memory problems") Then inds(8).checked = True
    If QLSDesList("BJH Pro Cognitive Barriers", "Confused,unable to comprehend") Then inds(8).checked = True
    
    If inds(8).checked Then Exit Sub
    If QLSDesList("BJH VS Oriented To", "Person,place,disoriented,unable to assess") Then inds(8).checked = True
    If inds(8).checked Then Exit Sub
    '4/4/11 comment out  OK
    'If QLSDesList("BJH VS LOC", "Level of Consciouness_sedated") Then inds(8).checked = True
    'If inds(8).checked Then Exit Sub
    If QLSDesList("BJH AS Neuro LOC Cognitive", "Unresponsive,lethargic,obtunded,fluctuates") Then inds(8).checked = True
    If inds(8).checked Then Exit Sub
    If QLSDesList("BJH AS Neuro Orientation", "x3,x2,x1,disorientated") Then inds(8).checked = True
    If inds(8).checked Then Exit Sub
    If QLS("BJH AS Neuro INV Cognitive Needs") Then inds(8).checked = True
    
'8 BJH AS Neuro Cognition/Memory 1
    If QLS("BJH AS Neuro Cognition/Memory") Then inds(8).checked = True
   
End Sub

Private Sub Check9_10()
    Dim lslist As String
    Dim count As Integer

    dprint "Check9_10"
    If QLSDes("BJH Res Restraint Reason", "Disruptive/dangerous behavior") Then inds(10).checked = True
    If QLSDesList("BJH AS Psy INV Support", "every 1 hr,patient/family related to Goals of Care") Then inds(10).checked = True
    'If QLSDes("BJH AS Psy INV Support", "patient/family related to Goals of Care") Then inds(10).checked = True
    
    If QLSCodeLikeResLikeListFreq("BJH AS Psy Behaviors Observations", "has harmed self in past, self mutilating") Then inds(10).checked = True
    If QLS("BJH AS Psy INV monitoring") Then inds(10).checked = True
    
    If inds(10).checked Then Exit Sub
    If QLSDes("BJH Res Restraint Reason", "Interference with medical treatment") Then inds(9).checked = True
    
    If inds(9).checked Then Exit Sub
    If QLSDesList("BJH AS Psy INV Support", "Provide Emotional Support to Patient,Provide Emotional Support of Family/Significant Others,Consults and referrals,Encouraged to ventilate feelings,Encouraged to use relaxation techniques,Provided consistent response") Then inds(9).checked = True
    'If QLSDes("BJH AS Psy INV Support", "Provide Emotional Support of Family/Significant Others") Then inds(9).checked = True
    'If QLSDes("BJH AS Psy INV Support", "Consults and referrals") Then inds(9).checked = True
    'If QLSDes("BJH AS Psy INV Support", "Encouraged to ventilate feelings") Then inds(9).checked = True
    'If QLSDes("BJH AS Psy INV Support", "Encouraged to use relaxation techniques") Then inds(9).checked = True
    'If QLSDes("BJH AS Psy INV Support", "Provided consistent response") Then inds(9).checked = True
    If QLS("BJH AS Psy INV Redirection") Then inds(9).checked = True
    If QLSDes("BJH AS Psy INV Environment", "Provided space/environment to gain control") Then inds(9).checked = True
    If Not inds(9).checked Then QBehavior9 'expensive query goes last
    
End Sub
Private Sub QBehavior9()
    Dim count, c1, c2, c3, c4, c5, c6, c7 As Integer
    Dim r As String
    
' comment out for Mar 4 mapping
'    count = QLSDesFreq("BJH AS Psy Behaviors Observations", "Behaviors Observations")
'    c1 = QLSResFreq("BJH AS Psy Behaviors Observations", "Displays passive acceptance")
'    c2 = QLSResFreq("BJH AS Psy Behaviors Observations", "Pleasant")
'    c3 = QLSResFreq("BJH AS Psy Behaviors Observations", "Displays psychomotor retardation")
'    c4 = QLSResFreq("BJH AS Psy Behaviors Observations", "Sociable")
'    c5 = QLSResFreq("BJH AS Psy Behaviors Observations", "Cooperative")
'    c6 = QLSResFreq("BJH AS Psy Behaviors Observations", "Has harmed self in past")
'    c7 = QLSResFreq("BJH AS Psy Behaviors Observations", "Vegetative")
'    If count > c1 + c2 + c3 + c4 + c5 + c6 + c7 Then
'        inds(9).checked = True
'    End If
    r = "acting out sexually,ask many questions,combative,crying,depressed,many needs and requests,"
    r = r & "seclusive,uncooperative,withdrawn,aggressive verbally,aggressive physically,"
    r = r & "harmed others in the past,compulsive behavior,delusional,explosive,grandiose,Hallucinations,"
    r = r & "hyperactive,hyper-sexual,hyper-verbal,labile,manipulative,suspicious,guarded"
    If QLSCodeLikeResLikeListFreq("BJH AS Psy Behaviors Observations", r) Then inds(9).checked = True
    
    'EXCEPT:
    'Displays passive acceptance, Pleasant, Displays psychomotor retardation, Sociable, Cooperative, Has harmed self in past and Vegetative

End Sub

Private Sub Check11_12()
    Dim count As Integer
    Dim no11or12 As Boolean

    no11or12 = False
    dprint "Check11_12"
    
    If QLSDesList("BJH VS RassSediationScale", "No response,Unarousable") Then
        no11or12 = True
        dprint "No reponse or Unarousable"
    End If
    If QLSDesList("BJH AS Fall Risk Low/High Risk Per Protocol", "Complete Paralysis or completely immobilized") Then
        no11or12 = True
        dprint "Complete paralysis or completely immobilized"
    End If
    
    count = QLSCountResValueGT("BJH AS Fall Risk Score Calc2", 2)
    If count > 0 Then 'value is 3 or greater -- trigger 11
        inds(11).checked = True
    ElseIf count = 0 Then 'then value 2 or less.  suppress 11
        no11or12 = True
    End If 'if count=-1 then no records exist for this; continue evaluation
    
    'If no11or12 And Not (inds(11).checked Or inds(12).checked) Then Exit Sub
    If no11or12 Then Exit Sub
    
'Need to make this change - Anything Violent should be able to come through
    count = QLSDesFreq("BJH Res Alternatives", "1:1 Violent Self-destructive") + _
            QLSDesFreq("BJH Res Restraint Reason", "Risk of injury to patient/others") + _
            QLSDesFreq("BJH Res Restraint Reason", "Disruptive/dangerous")
    If range = 720 Then '12 hr pull
        If count >= 6 Then inds(11).checked = True
        If count >= 24 Then inds(12).checked = True
    ElseIf range = 1440 Then '24 hr pull
        If count >= 12 Then inds(11).checked = True
        If count >= 48 Then inds(12).checked = True
    End If
    
    count = QLSDesFreq("BJH AS Fall/Injury Interventionss Confusion", "Sitter")
    If range = 720 Then '12 hr pull
        If count >= 1 Then inds(12).checked = True
    ElseIf range = 1440 Then '24 hr pull
        If count >= 2 Then inds(12).checked = True
    End If
    
    If inds(12).checked Then Exit Sub
    count = QLSDesFreq("BJH AS Psy INV Monitorng", "Psychosocial Intervention Monitoring")
    If range = 720 Then '12 hr pull
        If count >= 2 Then inds(12).checked = True
    ElseIf range = 1440 Then '24 hr pull
        If count >= 3 Then inds(12).checked = True
    End If
    
    If inds(12).checked Then Exit Sub
    count = QLSDesFreq("BJH Res Alternatives", "1:1 Violent Self-destructive") + _
            QLSDesFreq("BJH Res Restraint Reason", "Risk of injury to patient/others") + _
            QLSDesFreq("BJH Res Restraint Reason", "Disruptive/dangerous") + _
            QLSCodeListResListFreq("BJH VS Room Safety Check,BJH VS Patient Safety Rounds", "Done")
    If range = 720 Then '12 hr pull
        If count >= 24 Then inds(12).checked = True
    ElseIf range = 1440 Then '24 hr pull
        If count >= 48 Then inds(12).checked = True
    End If
    
    If inds(12).checked Then Exit Sub
    count = QLSDesFreq("BJH AS Fall Risk Interventions", "Hourly Rounds in place")
    If range = 720 Then '12 hr pull
        If count >= 1 Then inds(11).checked = True
    ElseIf range = 1440 Then '24 hr pull
        If count >= 2 Then inds(11).checked = True
    End If
    
    If inds(12).checked Then Exit Sub
    count = QLSDesFreq("BJH Restraint Alternatives LS", "staff")
    If range = 720 Then '12 hr pull
        If count >= 2 Then inds(12).checked = True
    ElseIf range = 1440 Then '24 hr pull
        If count >= 3 Then inds(12).checked = True
    End If
    
    If inds(11).checked Then Exit Sub
    count = QLSDesFreq("BJH Res Restraint Reason", "Risk of injury") + _
            QLSDesFreq("BJH Res Restraint Reason", "Disruptive/dangerous") + _
            QLSCodeListResListFreq("BJH VS Room Safety Check,BJH VS Patient Safety Rounds", "Done")
    If range = 720 Then '12 hr pull
        If count >= 6 Then inds(11).checked = True
    ElseIf range = 1440 Then '24 hr pull
        If count >= 12 Then inds(11).checked = True
    End If
    
    
End Sub

Private Sub Check13()
    dprint "Check13"
    If QLS("BJH AS Isolation Type") Then inds(13).checked = True
    If QLSDes("BJH AS Isolation Status", "continued") Then inds(13).checked = True

'13 BJH AS Isolation Status 1 continued 2
    
End Sub

Private Sub CheckAssessments()
    Dim count As Integer
    Dim codelist As String
    Dim i, j As Integer
    Dim inp() As String
    Dim out() As String
    Dim inpok As Boolean
    Dim outok As Boolean
'-   Temp of any source
'-   Pulse/BP
'-Hemodynamics
'-   glucose monitoring
'-   Respiratory rate and spo2%

    QAssessSetExclusionTimeRange ("BJH VS Blood Pump Flow") 'sets exclusionRange due to Dialysis
    
    dprint "CheckAssessments"
    count = QLSLikeListFreq("BJH VS Temp,BJH VS Temp IntraType")
    If range = 720 Then '12 hr pull
        If count >= 24 Then
            inds(17).checked = True
        ElseIf count >= 12 Then
            inds(16).checked = True
        ElseIf count >= 6 Then
            inds(15).checked = True
        ElseIf count >= 3 Then
            inds(14).checked = True
        End If
    ElseIf range = 1440 Then '24 hr pull
        If count >= 48 Then
            inds(17).checked = True
        ElseIf count >= 24 Then
            inds(16).checked = True
        ElseIf count >= 12 Then
            inds(15).checked = True
        ElseIf count >= 6 Then
            inds(14).checked = True
        End If
    End If
    dprint "Temp count=" & count
    
    If inds(17).checked Then Exit Sub
'    count = QLSFreq("BJH VS Pulse Rate") + QLSLikeFreq("BJH VS Noninv BP") + _
'    QLSLikeFreq("BJH VS Art bp") + QLSLikeFreq("BJH VS IABP Systolic") + QLSLikeFreq("BJH VS IABP Mean")
    count = QLSLikeListFreq("BJH VS Pulse Rate,BJH VS Heart Rate")
    If range = 720 Then '12 hr pull
        If count >= 24 Then
            inds(17).checked = True
        ElseIf count >= 12 Then
            inds(16).checked = True
        ElseIf count >= 6 Then
            inds(15).checked = True
        ElseIf count >= 3 Then
            inds(14).checked = True
        End If
    ElseIf range = 1440 Then '24 hr pull
        If count >= 48 Then
            inds(17).checked = True
        ElseIf count >= 24 Then
            inds(16).checked = True
        ElseIf count >= 12 Then
            inds(15).checked = True
        ElseIf count >= 6 Then
            inds(14).checked = True
        End If
    End If
    dprint "Heart rate count=" & count
    
    If inds(17).checked Then Exit Sub
    count = QLSLikeListFreq("BJH VS Noninv BP,BJH VS Art bp,BJH VS IABP Systolic,BJH VS IABP Mean")
    If range = 720 Then '12 hr pull
        If count >= 24 Then
            inds(17).checked = True
        ElseIf count >= 12 Then
            inds(16).checked = True
        ElseIf count >= 6 Then
            inds(15).checked = True
        ElseIf count >= 3 Then
            inds(14).checked = True
        End If
    ElseIf range = 1440 Then '24 hr pull
        If count >= 48 Then
            inds(17).checked = True
        ElseIf count >= 24 Then
            inds(16).checked = True
        ElseIf count >= 12 Then
            inds(15).checked = True
        ElseIf count >= 6 Then
            inds(14).checked = True
        End If
    End If
    dprint "BP count=" & count
'BJH VS Pulse Rate
'BJH VS Noninv BP  mon systolic
'BJH VS Noninv BP mon diastolic
'BJH VS Art bp a line systolic (ABP Label)
'BJH VS Art bp mean (monitor)  (ABP Label)
'BJH VS Art bp a line systolic (ART Label)
'BJH VS Art bp mean (monitor)  (ART Label)
'BJH VS IABP Systolic
'BJH VS IABP Mean

    If inds(17).checked Then Exit Sub
'    count = QLSFreq("BJH VS Heart Rate") + QLSFreq("BJH VS Resp Rate") + QLSFreq("BJH VS SPO2")
'    count = QLSLikeListFreq("BJH VS Resp Rate,BJH VS SPO2,BJH VS O2 Mode,BJH VS O2 Flow,BJH VS PulmInter")
    count = QLSLikeListFreq("BJH VS Resp Rate,BJH VS SPO2,BJH VS PulmInter")
    If range = 720 Then '12 hr pull
        If count >= 24 Then
            inds(17).checked = True
        ElseIf count >= 12 Then
            inds(16).checked = True
        ElseIf count >= 6 Then
            inds(15).checked = True
        ElseIf count >= 3 Then
            inds(14).checked = True
        End If
    ElseIf range = 1440 Then '24 hr pull
        If count >= 48 Then
            inds(17).checked = True
        ElseIf count >= 24 Then
            inds(16).checked = True
        ElseIf count >= 12 Then
            inds(15).checked = True
        ElseIf count >= 6 Then
            inds(14).checked = True
        End If
    End If
    dprint "Pulmonary count=" & count

'-BJH VS Heart Rate
'-BJH VS Resp Rate
'BJH VS SPO2
    
    If inds(17).checked Then Exit Sub
    count = QLSLikeListFreq("BJH VS PAP Systolic,BJH VS Hemo CO,BJH VS Noninv CO Mon Type,BJH VS Bladder Press")
    If range = 720 Then '12 hr pull
        If count >= 24 Then
            inds(17).checked = True
        ElseIf count >= 12 Then
            inds(16).checked = True
        ElseIf count >= 6 Then
            inds(15).checked = True
        ElseIf count >= 3 Then
            inds(14).checked = True
        End If
    ElseIf range = 1440 Then '24 hr pull
        If count >= 48 Then
            inds(17).checked = True
        ElseIf count >= 24 Then
            inds(16).checked = True
        ElseIf count >= 12 Then
            inds(15).checked = True
        ElseIf count >= 6 Then
            inds(14).checked = True
        End If
    End If
    dprint "Hemodynamics count=" & count
    
'BJH VS PAP Systolic
'BJH VS Hemo CO
'BJH VS Noninv CO Mon Type
'BJH VS Bladder Press
    
    If inds(17).checked Then Exit Sub
    count = QLSLikeListFreq("BJH VS ACT,BJH VS Glucose BS")
    If range = 720 Then '12 hr pull
        If count >= 24 Then
            inds(17).checked = True
        ElseIf count >= 12 Then
            inds(16).checked = True
        ElseIf count >= 6 Then
            inds(15).checked = True
        ElseIf count >= 3 Then
            inds(14).checked = True
        End If
    ElseIf range = 1440 Then '24 hr pull
        If count >= 48 Then
            inds(17).checked = True
        ElseIf count >= 24 Then
            inds(16).checked = True
        ElseIf count >= 12 Then
            inds(15).checked = True
        ElseIf count >= 6 Then
            inds(14).checked = True
        End If
    End If
    dprint "ACT_GlucoseBS count=" & count
    
'BJH VS ACT
'BJH VS Glucose BS

    If inds(17).checked Then Exit Sub
'    count = QLSFreq("BJH VS ICP monitor") + QLSFreq("BJH VS O2 Mode") + QLSFreq("BJH VS O2 Flow") + _
'QLSFreq("BJH VS Pain Intensity") + QLSFreq("BJH VS Chest Pain Intensity") + QLSFreq("BJH VS Non Comm Pain Care Giver") + _
'QLSFreq("BJH VS LOC") + QLSFreq("BJH VS RassSedationScale") + QLSFreq("BJH VS ProcSiteDrsgAssess") + _
'QLSFreq("BJH VS Distal Pulse Site") + QLSFreq("BJH VS Perf circ temp") + QLSFreq("BJH VS FlapSite") + _
'QLSFreq("BJH VS PulmInter")
    count = QLSLikeListFreq("BJH VS Pain Intensity,BJH VS Chest Pain Intensity,BJH VS Non Comm Pain Care Giver")
    If range = 720 Then '12 hr pull
        If count >= 24 Then
            inds(17).checked = True
        ElseIf count >= 12 Then
            inds(16).checked = True
        ElseIf count >= 6 Then
            inds(15).checked = True
        ElseIf count >= 3 Then
            inds(14).checked = True
        End If
    ElseIf range = 1440 Then '24 hr pull
        If count >= 48 Then
            inds(17).checked = True
        ElseIf count >= 24 Then
            inds(16).checked = True
        ElseIf count >= 12 Then
            inds(15).checked = True
        ElseIf count >= 6 Then
            inds(14).checked = True
        End If
    End If
    dprint "Pain count=" & count

    If inds(17).checked Then Exit Sub
    count = QLSLikeListFreq("BJH VS LOC,BJH VS RassSedationScale,BJH VS ICP monitor")
    If range = 720 Then '12 hr pull
        If count >= 24 Then
            inds(17).checked = True
        ElseIf count >= 12 Then
            inds(16).checked = True
        ElseIf count >= 6 Then
            inds(15).checked = True
        ElseIf count >= 3 Then
            inds(14).checked = True
        End If
    ElseIf range = 1440 Then '24 hr pull
        If count >= 48 Then
            inds(17).checked = True
        ElseIf count >= 24 Then
            inds(16).checked = True
        ElseIf count >= 12 Then
            inds(15).checked = True
        ElseIf count >= 6 Then
            inds(14).checked = True
        End If
    End If
    dprint "Neuro count=" & count

    If inds(17).checked Then Exit Sub
    count = QLSLikeListFreq("BJH VS ProcSiteDrsgAssess,BJH VS Distal Pulse Site,BJH VS Perf circ temp,BJH VS FlapSite")
    If range = 720 Then '12 hr pull
        If count >= 24 Then
            inds(17).checked = True
        ElseIf count >= 12 Then
            inds(16).checked = True
        ElseIf count >= 6 Then
            inds(15).checked = True
        ElseIf count >= 3 Then
            inds(14).checked = True
        End If
    ElseIf range = 1440 Then '24 hr pull
        If count >= 48 Then
            inds(17).checked = True
        ElseIf count >= 24 Then
            inds(16).checked = True
        ElseIf count >= 12 Then
            inds(15).checked = True
        ElseIf count >= 6 Then
            inds(14).checked = True
        End If
    End If
    dprint "Vascular count=" & count

    If inds(17).checked Then Exit Sub
    
    'INTAKE:  Routine Weight, Oral Fluids, Dirps, IV Solutions, Miscellaneous, Small Volume Parental,
    '         PCA/Epidural, Colloids, Blood Products, Transplant Infusions, Parenteral Nutrition, Meals,
    '         Dietary Supplements, Tube Feeding, Diversion Device (intake)
    
'Routine Weight
'Oral Fluids
'Drips
'IV Solutions
'Miscellaneous -intake
'Small Volume Parenteral
'PCA/Epidural
'Colloids
'Blood Products
'Transplant Infusions
'Parenteral Nutrition
'Meals
'Dietary Supplements
'Tube Feeding
'Diversion Device(intake)
    inpok = False
    codelist = "Routine Weight -,Oral Fluids -,Drips -,IV Solutions -,Miscellaneous -intake -,"
    codelist = codelist & "Small Volume Parenteral -,PCA/Epidural -,Colloids -,Blood Products -,"
    codelist = codelist & "Transplant Infusions -,Parenteral Nutrition -,Meals -,"
    codelist = codelist & "Dietary Supplements -,Tube Feeding -,Diversion Device(intake) -"
    inp = QLSLikeListTimes(codelist, inpok)
    
'Voiding
'Catheters
'Diversion Devices
'Urine Description
'Upper GI
'Upper GI Tubes
'Lower GI
'Drains/Chest Tubes
'Blood Loss
'Miscellaneous -Output
    outok = False
    codelist = "Voiding -,Catheters -,Diversion Devices -,Urine Description -,Upper GI -,Upper GI Tubes -,"
    codelist = codelist & "Lower GI -,Drains/Chest Tubes -,Blood Loss -,Miscellaneous -Output -"
    out = QLSLikeListTimes(codelist, outok)
    
    count = 0
    If inpok And outok Then
        For i = 1 To UBound(inp)
            For j = 1 To UBound(out)
                If inp(i) = out(j) Then
                    count = count + 1
                    Exit For
                End If
            Next j
        Next i
        If range = 720 Then '12 hr pull
            If count >= 24 Then
                inds(17).checked = True
            ElseIf count >= 12 Then
                inds(16).checked = True
            ElseIf count >= 6 Then
                inds(15).checked = True
            ElseIf count >= 3 Then
                inds(14).checked = True
            End If
        ElseIf range = 1440 Then '24 hr pull
            If count >= 48 Then
                inds(17).checked = True
            ElseIf count >= 24 Then
                inds(16).checked = True
            ElseIf count >= 12 Then
                inds(15).checked = True
            ElseIf count >= 6 Then
                inds(14).checked = True
            End If
        End If
    End If
    
    dprint "I&O category I&O count=" & count

    
'Hemodialysis
'Dialysis
'Pheresis
'Lavage/Irrigation
    If inds(17).checked Then
        Exit Sub
    End If
    codelist = "Hemodialysis -,Dialysis -,Pheresis -,Lavage/Irrigation -"
    count = QLSLikeListFreq(codelist)
    If range = 720 Then '12 hr pull
        If count >= 24 Then
            inds(17).checked = True
        ElseIf count >= 12 Then
            inds(16).checked = True
        ElseIf count >= 6 Then
            inds(15).checked = True
        ElseIf count >= 3 Then
            inds(14).checked = True
        End If
    ElseIf range = 1440 Then '24 hr pull
        If count >= 48 Then
            inds(17).checked = True
        ElseIf count >= 24 Then
            inds(16).checked = True
        ElseIf count >= 12 Then
            inds(15).checked = True
        ElseIf count >= 6 Then
            inds(14).checked = True
        End If
    End If
    dprint "I&O category 2 count=" & count
    
    
'Upper GI Tests
'Lower GI Tests
'Urine Tests
'Miscellaneous Testing
'Pad count And Description

' Don't need these per Michelle 9/23/10 phone call.
'    If inds(17).checked Then
'        Exit Sub
'    End If
'    codelist = "Upper GI Tests -,Lower GI Tests -,Urine Tests -,Miscellaneous Testing -,Pad count And Description -"
'    count = QLSLikeListFreq(codelist)
'    If range = 720 Then '12 hr pull
'        If count >= 24 Then inds(17).checked = True
'        ElseIf count >= 12 Then inds(16).checked = True
'        ElseIf count >= 6 Then inds(15).checked = True
'        ElseIf count >= 3 Then inds(14).checked = True
'    ElseIf range = 1440 Then '24 hr pull
'        If count >= 48 Then inds(17).checked = True
'        ElseIf count >= 24 Then inds(16).checked = True
'        ElseIf count >= 12 Then inds(15).checked = True
'        ElseIf count >= 6 Then inds(14).checked = True
'    End If
'    dprint "I&O category 4 count=" & count

    exclusionRange = ""
    dprint "END CheckAssessments END"

End Sub

Private Sub Check18()

'Blood Product
    If QLSLike("Blood Product") Then inds(18).checked = True

    
End Sub

Private Sub Check19_20()
    Dim count As Integer
    
    dprint "Check19_20"
    If QLSCodeLikeResLikeListFreq("BJH AS Skin VAD Site Location", "Dressing changed per") Then inds(20).checked = True
    If QLSDesList("BJH AS Central Line 1  Type", "Venous sheath,Arterial sheath") Then inds(20).checked = True
    'If QLSDes("BJH AS Central Line 1  Type", "Arterial sheath") Then inds(20).checked = True
    If QLS("BJH AS CentralLine 1  Discontinuation") Then inds(20).checked = True
    
    If inds(20).checked Then Exit Sub
    If QLSDes("BJH AS Skin Pressure Ulcer Stage", "IV Full Thickness") Then inds(20).checked = True
'    count = QLSCodeNotWithResListFreq("BJH AS Skin Wound Care", "abrasion,blister,contusion,excoriation,lesion,rash,skin tear")
'    If count >= 4 Then inds(20).checked = True
    
    If inds(20).checked Then Exit Sub
    If QLSLike("BJH WOs Surg Debride") Then inds(20).checked = True
    If QLSDesList("BJH WOs Wound Negative Pressure Dressing", "Applied,Changed") Then inds(20).checked = True
    'If QLSDes("BJH WOs Wound Negative Pressure Dressing", "Changed") Then inds(20).checked = True
    If QLSCodeLikeResLikeListFreq("BJH AS Resp Chest Tube Dressing", "Dressing changed per") Then inds(20).checked = True
    
    
    If inds(20).checked Then Exit Sub
    If QLS("BJH AS Skin VAD Site Location") Then inds(19).checked = True
    If QLS("BJH AS GI Tubes Diversion Device Type") Then inds(19).checked = True
    If QLSDes("BJH AS GI Tubes Drainage UDF", "Bloody") Or QLSDes("BJH AS GI Tubes Drainage UDF", "Red") Then
        inds(19).checked = True
    End If
    
    If inds(19).checked Then Exit Sub
    If QLSCodeLikeResLikeListFreq("BJH AS Resp Chest Tube Dressing", "Dry and intact") Then inds(19).checked = True
    If QLSCodeLikeResLikeListFreq("BJH AS GI Stoma Appearance", "denuded,erythematous") Then inds(19).checked = True
    If QLSCodeLikeResLikeListFreq("BJH AS Resp Stoma Treatment", "stoma care done,trach ties changed") Then inds(19).checked = True
    
    If QLS("BJH AS Neuro Monitor Ventric Site Assesment") Then inds(19).checked = True
    If QLS("BJH AS Neuro Lumbar Drain Insertion Site") Then inds(19).checked = True
    If QLS("BJH AS Neuro Monitor Subural Drainage") Then inds(19).checked = True
    If QLSCodeLikeResLikeListFreq("BJH AS Skin Wound Care", "open to air") Then inds(19).checked = True

    If inds(19).checked Then Exit Sub
    If QLS("BJH OUT Tube NG") Then inds(19).checked = True
    If QLS("BJH OUT Tube Blakemore") Then inds(19).checked = True
    If QLSDes("BJH IO GI Stool Color", "Melenic") Then inds(19).checked = True
    If QLSDes("BJH IO GI Stool Color", "Red") Then inds(19).checked = True
    
    If inds(19).checked Then Exit Sub
    If QLSDes("BJH IO Emesis Description", "Coffee-ground") Or QLSDes("BJH IO Emesis Description", "Clots noted") Or _
        QLSDes("BJH IO Emesis Description", "Brown") Then inds(19).checked = True
    If QLS("BJH AS Skin Wound Type") Then inds(19).checked = True
    If QLS("BJH AS Skin Wound MS set") Then inds(19).checked = True
    
    If inds(19).checked Then Exit Sub
    If QLS("BJH AS Skin Pressure Ulcer Location") Then inds(19).checked = True
    If QLS("BJH AS Pressure Ulcer MS set") Then inds(19).checked = True
    If QLSCodeLikeResLikeListFreq("BJH AS Tube/Drain Type", "Blakemore,Davol sump,Hemovac,Jackson-Pratt,Neuro drain,Penrose drain,Pericardial drain") Then inds(19).checked = True
    If QLSLike("BJH AS Jackson Pratt") Then inds(19).checked = True
    
    If inds(19).checked Then Exit Sub
    If QLSLike("BJH AS JPDrain") Then inds(19).checked = True
    If QLSLike("BJH AS Penrose Drain") Then inds(19).checked = True
    If QLSLike("BJH AS Hemovac") Then inds(19).checked = True
    
    If inds(19).checked Then Exit Sub
    If QLS("BJH AS Peritoneal Dialysis Cath Site") Then inds(19).checked = True
    If QLSCodeLikeResLikeListFreq("BJH AS Epidural UDF", "Functioning,Capped,Infusing poorly") Then inds(19).checked = True
    If QLSDesLike("BJH AS Central Line", "Done per protocol") Then inds(19).checked = True
    
    If inds(19).checked Then Exit Sub
    If QLSCodeLikeResLikeListFreq("BJH AS Skin Oral Mucosa", "Grade II,Grade III,Grade IV") Then inds(19).checked = True
        
    If inds(19).checked Then Exit Sub
    count = QLSFreq("BJH Net Bladder Irrigation")
    If range = 720 Then '12 hr pull
        If count >= 6 Then inds(19).checked = True
    ElseIf range = 1440 Then '24 hr pull
        If count >= 12 Then inds(19).checked = True
    End If

End Sub

Private Sub Check21()
    Dim mins As Integer

    dprint "Check21"
    If QLSDesFreq("BJHEDU Diabetes Topic", "Review Topics") >= 3 Then inds(21).checked = True
    If QLSDesList("BJHEDU Diabetes Topic", "Complications,Blood Glucose Monitoring,Insulin Administration") Then inds(21).checked = True
    If QLSDes("BJHEDU Respiratory Topic", "Equipment") Then inds(21).checked = True
    'If QLSDes("BJHEDU Diabetes Topic", "Blood Glucose Monitoring") Then inds(21).checked = True
    'If QLSDes("BJHEDU Diabetes Topic", "Insulin Administration") Then inds(21).checked = True
    
    If inds(21).checked Then Exit Sub
    If QLSDesList("BJHEDUNutrDietFeedTopic", "Diabetes,Gastric Bypass Diet,Tube Feeding") Then inds(21).checked = True
    'If QLSDes("BJHEDUNutrDietFeedTopic", "Gastric Bypass Diet") Then inds(21).checked = True
    'If QLSDes("BJHEDUNutrDietFeedTopic", "Tube Feeding") Then inds(21).checked = True
    
    If inds(21).checked Then Exit Sub
    If QLSDes("BJHEDUEquipMedTopic", "Donning/doffing of othosis/prothesis") Then inds(21).checked = True
    If QLSDesList("BJHEDUDischPlanTopic", "Lap Gastric Bypass Discharge,Open Gastric Bypass Discharge") Then inds(21).checked = True
    'If QLSDes("BJHEDUDischPlanTopic", "Open Gastric Bypass Discharge") Then inds(21).checked = True
    
    If inds(21).checked Then Exit Sub
    If QLSDesList("BJHEDUDischPlanTopic", "Vascular Surgery Topics,Oncolocy Topics,Liver Transplant Topics,Renal Transplant Topics,GI Topics,GU Topics,Wound/Ostomy Topics") Then inds(21).checked = True
    'If QLSDes("BJHEDUDischPlanTopic", "Oncolocy Topics") Then inds(21).checked = True
    'If QLSDes("BJHEDUDischPlanTopic", "Liver Transplant Topics") Then inds(21).checked = True
    'If QLSDes("BJHEDUDischPlanTopic", "Renal Transplant Topics") Then inds(21).checked = True
    'If QLSDes("BJHEDUDischPlanTopic", "GI Topics") Then inds(21).checked = True
    'If QLSDes("BJHEDUDischPlanTopic", "GU Topics") Then inds(21).checked = True
    'If QLSDes("BJHEDUDischPlanTopic", "Wound/Ostomy Topics") Then inds(21).checked = True
    
End Sub

Private Sub Check22()
    Dim i As Integer
    Dim c(11) As String
    Dim d(11) As String
    Dim r(11) As String
    Dim donottrigger As Boolean
    Dim start As Integer
    
    ' 4/4/11  make RBCs count = 5 within 2 hours to trigger this. 4/4/11  Look at other TCs to see what they use.
    
    donottrigger = False
    dprint "Check22"
    
    If QLSLike("BJH VS Blood Pump Flow") Then donottrigger = True
    If QLS("BJH VS TPE Blood Flow") Then donottrigger = True
    If donottrigger Then
        start = 8
    Else
        start = 1
    End If
    
    c(1) = "BJH VS Heart Rate"
    c(2) = "BJH VS Noninv BP mon systolic"
    c(3) = "BJH VS Noninv BP mean"
    c(4) = "BJH VS Art bp a line systolic (ABP Label)"
    c(5) = "BJH VS Art bp a line mean (monitor) (ABP Label)"
    c(6) = "BJH VS Art bp a line systolic (ART Label)"
    c(7) = "BJH VS Art bp a line mean (monitor) (ART Label)"
    c(8) = "BJH IN RBCs"
    c(9) = "BJH IN Plasma Fresh Frozen"
    
    d(1) = "Heart Rate"
    d(2) = "Systolic"
    d(3) = "Mean"
    d(4) = "Systolic"
    d(5) = "Mean"
    d(6) = "Systolic"
    d(7) = "Mean"
    d(8) = "RBCs"
    d(9) = "Plasma Fresh Frozen"
    
    r(1) = "isnumeric(res)=1 and res >=60 and res <=100"
    r(2) = "isnumeric(res)=1 and res >=90 and res <=180"
    r(3) = "isnumeric(res)=1 and res >=50"
    r(4) = "isnumeric(res)=1 and res >=90 and res <=180"
    r(5) = "isnumeric(res)=1 and res >=55"
    r(6) = "isnumeric(res)=1 and res >=90 and res <=180"
    r(7) = "isnumeric(res)=1 and res >=55"
    r(8) = ""
    r(9) = ""
    
    For i = start To 9
        If Not inds(22).checked Then
            Q22 c(i), d(i), r(i)
        End If
    Next i

    
End Sub

Private Sub Q22(c As String, d As String, res As String)
    Dim p As Date
    Dim done As Boolean
    Dim count As Integer
    Dim pmin As Date
    Dim r As Integer
    
'set p=0.
'Loop
'c=count the number of hits with perdt>p.
'if c>=8 then:
' p=get the perfdt of the earliest one>p, add 2 hours to it, and r=count the number found in this range.
' if r>=8 then  inds(22).checked=true
' else c=c-1
'endloop
    pmin = #1/1/1900#
    count = Q22count(c, d, res, pmin)  'count c,d with perdt > p
    done = False
    While Not done
        If count >= 8 Then
            'p= min perfdt where perfdt > p
            pmin = Q22getminp(c, d, res, pmin)
            If pmin <> #1/1/1900# Then
                'r= count c,d with perfdt between p and p+2
                r = Q22rangecount(c, d, res, pmin)
                If r >= 8 Then
                    inds(22).checked = True
                    done = True
                End If
            End If
            count = count - 1
        Else
            done = True
        End If
    Wend
    
End Sub
Private Function Q22count(c As String, d As String, res As String, p As Date) As Integer
    Dim rs As New Recordset
    Dim sql As String
    
    Q22count = 0
    sql = "select count(*) from " & DBTABLENAME & " where acctnum='" & acctnum & "' and code='" & c & "' and descript like " & "'%" & d & "%'"
    If res <> "" Then
        sql = sql & " and " & res
    End If
    sql = sql & " and perfdt>" & g_dbutil.SQL_DateTime(p)
    dprint sql
    rs.Open sql, g_cnADO
    If rs.RecordCount > 0 Then Q22count = rs(0)
    rs.Close
    Set rs = Nothing

End Function
Private Function Q22getminp(c As String, d As String, res As String, p As Date) As Date
    Dim rs As New Recordset
    Dim sql As String
    
    Q22getminp = #1/1/1900#
    sql = "select min(perfdt) from " & DBTABLENAME & " where acctnum='" & acctnum & "' and code='" & c & "' and descript like " & "'%" & d & "%'"
    If res <> "" Then
        sql = sql & " and " & res
    End If
    sql = sql & " and perfdt>" & g_dbutil.SQL_DateTime(p)
    dprint sql
    rs.Open sql, g_cnADO
    If Not IsNull(rs(0)) Then Q22getminp = rs(0)
    rs.Close
    Set rs = Nothing

End Function
Private Function Q22rangecount(c As String, d As String, res As String, pmin As Date) As Integer
    Dim rs As New Recordset
    Dim sql As String
    Dim pmax As Date
    
    Q22rangecount = 0
    pmax = DateAdd("h", 2, pmin)
    sql = "select count(*) from " & DBTABLENAME & " where acctnum='" & acctnum & "' and code='" & c & "' and descript like " & "'%" & d & "%'"
    If res <> "" Then
        sql = sql & " and " & res
    End If
    sql = sql & " and perfdt between " & g_dbutil.SQL_DateTime(pmin) & " and " & g_dbutil.SQL_DateTime(pmax)
    dprint sql
    rs.Open sql, g_cnADO
    If rs.RecordCount > 0 Then Q22rangecount = rs(0)
    rs.Close
    Set rs = Nothing

End Function

Private Function QLS(code As String) As Boolean
    Dim rs As New Recordset
    Dim sql As String
    
    'dprint "QLS"
'  select * from tc_data where patient_id=x and patresultslabelseq='123456'
    sql = "select * from " & DBTABLENAME & " where acctnum='" & acctnum & "' and code='" & code & "'"
    rs.Open sql, g_cnADO
    QLS = (rs.RecordCount > 0)
    rs.Close
    Set rs = Nothing
'    dprint "end QLabelSeqs"
End Function
Private Function QLSLike(code As String) As Boolean
    Dim rs As New Recordset
    Dim sql As String
    
'    dprint "QLSLike"
'  select * from tc_data where patient_id=x and patresultslabelseq='123456'
    sql = "select count(*) from " & DBTABLENAME & " where acctnum='" & acctnum & "' and code like '%" & code & "%'"
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        QLSLike = (rs(0) > 0)
    Else
        QLSLike = False
    End If
    rs.Close
    Set rs = Nothing
'    dprint "end QLabelSeqs"
End Function
Private Function QLSLikeFreq(code As String) As Integer
    Dim rs As New Recordset
    Dim sql As String
    
'    dprint "QLSLikeFreq"
    QLSLikeFreq = 0
'  select * from tc_data where patient_id=x and patresultslabelseq='123456'
    sql = "select count(*) from " & DBTABLENAME & " where acctnum='" & acctnum & "' and code like '%" & code & "%'"
    rs.Open sql, g_cnADO
    If rs.RecordCount > 0 Then QLSLikeFreq = rs(0)
    rs.Close
    Set rs = Nothing
'    dprint "end QLabelSeqs"
End Function
Private Function QLSLikeListFreq(codelist As String) As Integer
    Dim rs As New Recordset
    Dim sql As String
    Dim i As Integer
    Dim c() As String
    
'    dprint "QLSLikeListFreq"
    QLSLikeListFreq = 0
    sql = "select count(distinct(perfdt)) from " & DBTABLENAME & " where acctnum='" & acctnum & "' and ("
    c = Split(codelist, ",")
    sql = sql & "code like '%" & c(0) & "%'"
    For i = 1 To UBound(c)
        sql = sql & " or code like '%" & c(i) & "%'"
    Next i
    sql = sql & ")" & exclusionRange
    rs.Open sql, g_cnADO
    If rs.RecordCount > 0 Then QLSLikeListFreq = rs(0)
    rs.Close
    Set rs = Nothing
'    dprint "end QLabelSeqs"
End Function
Private Function QLSCodeListResListFreq(codelist As String, reslist As String) As Integer
    Dim rs As New Recordset
    Dim sql As String
    Dim i As Integer
    Dim c() As String
    
'    dprint "QLSLikeListFreq"
    QLSCodeListResListFreq = 0
    sql = "select count(distinct(perfdt)) from " & DBTABLENAME & " where acctnum='" & acctnum & "' and ("
    c = Split(codelist, ",")
    sql = sql & "code like '%" & c(0) & "%'"
    For i = 1 To UBound(c)
        sql = sql & " or code like '%" & c(i) & "%'"
    Next i
    sql = sql & ") and ("
    
    c = Split(reslist, ",")
    sql = sql & "res like " & "'%" & c(0) & "%'"
    For i = 1 To UBound(c)
        sql = sql & " or res like '%" & c(i) & "%'"
    Next i
    sql = sql & ")"
    
    rs.Open sql, g_cnADO
    If rs.RecordCount > 0 Then QLSCodeListResListFreq = rs(0)
    rs.Close
    Set rs = Nothing
'    dprint "end QLabelSeqs"
End Function

Private Function QLSLikeListTimes(codelist As String, timesexist As Boolean) As String()
    Dim rs As New Recordset
    Dim sql As String
    Dim i As Integer
    Dim c() As String
    Dim t() As String
    
'    dprint "QLSLikeListFreq"
    sql = "select distinct(perfdt) from " & DBTABLENAME & " where acctnum='" & acctnum & "' and ("
    c = Split(codelist, ",")
    sql = sql & "code like '%" & c(0) & "%'"
    For i = 1 To UBound(c)
        sql = sql & " or code like '%" & c(i) & "%'"
    Next i
    sql = sql & ")" & exclusionRange
    rs.Open sql, g_cnADO
    timesexist = False
    If Not rs.EOF Then
        For i = 1 To rs.RecordCount
            ReDim Preserve t(0 To i)
            t(i) = g_dbutil.DBToString(rs(0))
            rs.MoveNext
        Next i
        timesexist = True
    End If
    rs.Close
    Set rs = Nothing
    QLSLikeListTimes = t
'    dprint "end QLabelSeqs"
End Function

Private Function QLSDes(code As String, des As String) As Boolean
    Dim rs As New Recordset
    Dim sql As String
    
'    dprint "QLSDes"
'  select * from tc_data where patient_id=x and result_value like '%sa2%' and patresultslabelseq='123456'
    sql = "select count(*) from " & DBTABLENAME & " where acctnum='" & acctnum & "' and code='" & code & "' and descript like '%" & des & "%'"
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        QLSDes = (rs(0) > 0)
    Else
        QLSDes = False
    End If
    rs.Close

    Set rs = Nothing
'    dprint "end QLegLabs"
End Function

Private Function QLSDesList(code As String, deslist As String) As Boolean
    Dim rs As New Recordset
    Dim sql As String
    Dim d() As String
    Dim i As Integer
    
'    dprint "QLSDes"
'  select * from tc_data where patient_id=x and result_value like '%sa2%' and patresultslabelseq='123456'
    d = Split(deslist, ",")
    sql = "select count(*) from " & DBTABLENAME & " where acctnum='" & acctnum & "' and code='" & code & "' and ("
    sql = sql & "descript like '%" & d(0) & "%'"
    For i = 1 To UBound(d)
        sql = sql & " or descript like '%" & d(i) & "%'"
    Next i
    sql = sql & ")"
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        QLSDesList = (rs(0) > 0)
    Else
        QLSDesList = False
    End If
    rs.Close

    Set rs = Nothing
'    dprint "end QLegLabs"
End Function

Private Function QLSRes(code As String, res As String) As Boolean
    Dim rs As New Recordset
    Dim sql As String
    
'    dprint "QLSRes"
'  select * from tc_data where patient_id=x and result_value like '%sa2%' and patresultslabelseq='123456'
    sql = "select count(*) from " & DBTABLENAME & " where acctnum='" & acctnum & "' and code='" & code & "' and res like '%" & res & "%'"
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        QLSRes = (rs(0) > 0)
    Else
        QLSRes = False
    End If
    rs.Close

    Set rs = Nothing
'    dprint "end QLegLabs"
End Function

Private Function QLSDesLike(code As String, des As String) As Boolean
    Dim rs As New Recordset
    Dim sql As String
    
'    dprint "QLSDesLike"
'  select * from tc_data where patient_id=x and result_value like '%sa2%' and patresultslabelseq='123456'
    sql = "select count(*) from " & DBTABLENAME & " where acctnum='" & acctnum & "' and code like '%" & code & "%' and descript like '%" & des & "%'"
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        QLSDesLike = (rs(0) > 0)
    Else
        QLSDesLike = False
    End If
    rs.Close

    Set rs = Nothing
'    dprint "end QLegLabs"
End Function

Private Function QLSList(lslist As String) As Boolean
    Dim rs As New Recordset
    Dim sql As String
    
'    dprint "QLSlist"
'  select * from tc_data where patient_id=x and patresultslabelseq='123456'
    sql = "select count(*) from " & DBTABLENAME & " where acctnum='" & acctnum & "' and labseq in (" & lslist & ")"
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        QLSList = (rs(0) > 0)
    Else
        QLSList = False
    End If
    rs.Close
    Set rs = Nothing
'    dprint "end QLabelSeqs"
End Function
Private Function QLSListRes(lslist As String, res As String) As Boolean
    Dim rs As New Recordset
    Dim sql As String
    
'    dprint "QLSlistres"
'  select * from tc_data where patient_id=x and patresultslabelseq='123456'
    sql = "select count(*) from " & DBTABLENAME & " where acctnum='" & acctnum & "' and labseq in (" & lslist & ") and result like " & "'%" & res & "%'"
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        QLSListRes = (rs(0) > 0)
    Else
        QLSListRes = False
    End If
    rs.Close
    Set rs = Nothing
'    dprint "end QLabelSeqs"
End Function
'Private Function QLSCountValue(ls As Long, r As String) As Integer
'    Dim rs As New Recordset
'    Dim sql As String
'
''    dprint "QLSValue"
'' select * from tc_data where patient_id=x and patresultslabelseq='123456'
'    sql = "select count(distinct(perftime+result)) from " & DBTABLENAME & " where acctnum='" & acctnum & "' and labseq=" & CStr(ls) & " and result" & r
'    rs.Open sql, g_cnADO
'    If rs.RecordCount > 0 Then
'        QLSCountValue = CInt(rs(0))
'    Else
'        QLSCountValue = 0
'    End If
'    rs.Close
'    Set rs = Nothing
''    dprint "end QLabelSeqs"
'End Function

Private Function QLSDesFreq(code As String, d As String) As Integer
    Dim rs As New Recordset
    Dim sql As String
    
    QLSDesFreq = 0
'  select * from tc_data where patient_id=x and patresultslabelseq='123456'
    sql = "select count(*) from " & DBTABLENAME & " where acctnum='" & acctnum & "' and code='" & code & "' and descript like " & "'%" & d & "%'"
    rs.Open sql, g_cnADO
    If rs.RecordCount > 0 Then QLSDesFreq = rs(0)
    rs.Close
    Set rs = Nothing
'    dprint "end QLabelSeqs"

End Function
Private Function QLSResFreq(code As String, d As String) As Integer
    Dim rs As New Recordset
    Dim sql As String
    
    QLSResFreq = 0
'  select * from tc_data where patient_id=x and patresultslabelseq='123456'
    sql = "select count(*) from " & DBTABLENAME & " where acctnum='" & acctnum & "' and code='" & code & "' and res like " & "'%" & d & "%'"
    rs.Open sql, g_cnADO
    If rs.RecordCount > 0 Then QLSResFreq = rs(0)
    rs.Close
    Set rs = Nothing
'    dprint "end QLabelSeqs"

End Function
Private Function QLSCountResValueGT(code As String, dv As Integer) As Integer
    Dim rs As New Recordset
    Dim sql As String
    Dim s As String
    Dim newv As Single
    Dim i As Integer
    Dim count As Integer
    
    count = -1
'  select * from tc_data where patient_id=x and patresultslabelseq='123456'
    sql = "select res from " & DBTABLENAME & " where acctnum='" & acctnum & "' and code='" & code & "'"
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        count = 0
        For i = 1 To rs.RecordCount
            s = g_dbutil.DBToString(rs(0))
            If IsNumeric(s) Then
                newv = CSng(s)
                If newv > dv Then count = count + 1
            End If
            rs.MoveNext
        Next i
    End If
    rs.Close
    QLSCountResValueGT = count
    Set rs = Nothing
'    dprint "end QLabelSeqs"

End Function
Private Function QLSResString(code As String) As String
    Dim rs As New Recordset
    Dim sql As String
    Dim s As String
    Dim i As Integer
    
    s = ""
'  select * from tc_data where patient_id=x and patresultslabelseq='123456'
    sql = "select res from " & DBTABLENAME & " where acctnum='" & acctnum & "' and code='" & code & "'"
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        s = g_dbutil.DBToString(rs(0))
    End If
    rs.Close
    QLSResString = s
    Set rs = Nothing
'    dprint "end QLabelSeqs"

End Function
Private Function QLSResStringAtPerftm(code As String, p As Date) As String
    Dim rs As New Recordset
    Dim sql As String
    Dim s As String
    Dim i As Integer
    
    s = ""
'  select * from tc_data where patient_id=x and patresultslabelseq='123456'
    sql = "select res from " & DBTABLENAME & " where acctnum='" & acctnum & "' and code='" & code & "'"
    sql = sql & " and perfdt=" & g_dbutil.SQL_DateTime(p)
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        s = g_dbutil.DBToString(rs(0))
    End If
    rs.Close
    QLSResStringAtPerftm = s
    Set rs = Nothing
'    dprint "end QLabelSeqs"

End Function


Private Function QLSCodeLikeResLikeListFreq(code As String, reslist As String) As Integer
    Dim rs As New Recordset
    Dim sql As String
    Dim r() As String
    Dim i As Integer
    
'    dprint "QLSCodeLikeResLikeListFreq"
    QLSCodeLikeResLikeListFreq = 0
    r = Split(reslist, ",")
    sql = "select count(*) from " & DBTABLENAME & " where acctnum='" & acctnum & "' and code like '%" & code & "%' and ("
    sql = sql & "res like " & "'%" & r(0) & "%'"
    For i = 1 To UBound(r)
        sql = sql & " or res like '%" & r(i) & "%'"
    Next i
    sql = sql & ")"
    rs.Open sql, g_cnADO
    If rs.RecordCount > 0 Then QLSCodeLikeResLikeListFreq = rs(0)
    rs.Close
    Set rs = Nothing
'    dprint "end QLabelSeqs"

End Function

Private Function QLSFreq(code As String) As Integer
    Dim rs As New Recordset
    Dim sql As String
    
    QLSFreq = 0
'  select * from tc_data where patient_id=x and patresultslabelseq='123456'
    sql = "select count(*) from " & DBTABLENAME & " where acctnum='" & acctnum & "' and code='" & code & "'"
    rs.Open sql, g_cnADO
    If rs.RecordCount > 0 Then
        QLSFreq = rs(0)
    End If
    rs.Close
    Set rs = Nothing
'    dprint "end QLabelSeqs"

End Function

Private Function QLSListFreq(lslist As String, freq As Integer) As Boolean
    Dim ls() As String
    Dim maxls As Integer
    Dim i As Integer
    
    QLSListFreq = False
    ls() = Split(lslist, ",")
    maxls = UBound(ls)
    For i = 0 To maxls
        If QLSFreq(ls(i)) >= freq Then
            QLSListFreq = True
            Exit For
            'i = maxls + 1
        End If
    Next i

End Function


Private Function QLSListCount(lslist As String) As Integer
    Dim ls() As String
    Dim maxls As Integer
    Dim i As Integer
    Dim c As Integer
    
    c = 0
    ls() = Split(lslist, ",")
    maxls = UBound(ls)
    For i = 0 To maxls
        c = c + QLSFreq(ls(i))
    Next i
    QLSListCount = c

End Function


Private Function QLSCodeNotWithResListFreq(code As String, reslist As String) As Integer
    Dim rs As New Recordset
    Dim sql As String
    Dim r() As String
    Dim i As Integer
    
'    dprint "QLSCodeNotWithResListFreq"
    QLSCodeNotWithResListFreq = 0
    r = Split(reslist, ",")
    sql = "select count(distinct perfdt) from " & DBTABLENAME & " where acctnum='" & acctnum & "' and code like '%" & code & "%' and "
    sql = sql & "res not like " & "'%" & r(0) & "%'"
    For i = 1 To UBound(r)
        sql = sql & " and res not like '%" & r(i) & "%'"
    Next i
    rs.Open sql, g_cnADO
    If rs.RecordCount > 0 Then QLSCodeNotWithResListFreq = rs(0)
    rs.Close
    Set rs = Nothing
'    dprint "end QLabelSeqs"

End Function


Private Sub DoProcedures()
    Dim dur As String
    Dim d As Integer
    Dim pmin As Date
    Dim pmax As Date
    Dim byRN As Boolean
    Dim bynonRN As Boolean
    Dim onunit As Boolean
    Dim offunit As Boolean
    Dim pmindate As String
    Dim pmintime As String
    Dim pmaxdate As String
    Dim pmaxtime As String
    Dim yesterd As String
    Dim todayd As String
    Dim pminRN As Date
    Dim pminNONRN As Date
    Dim onunitRNmins As Integer
    Dim offunitRNmins As Integer
    Dim offunitNONRNmins As Integer
    Dim timestr As String
    
        
    dprint "CheckProcedures"
    
    dur = QLSResString("BJH VS Procedure Duration")
    If dur = "Less than 30 minutes" Then
        d = 0
    Else
        d = val(Mid$(dur, 1, 3)) * 60#
    End If

If Mid$(intime, 9, 4) = "0700" Then
    If QLSResFreq("BJH AS Fall/Injury Interventionss Confusion", "Sitter") Then 'either 7a-7p or 7p-7a
        dprint "Found sitter"
        pmin = #1/1/1900#
        pmin = QProcgetminp("BJH AS Fall/Injury Interventionss Confusion", "Sitter", pmin)
        dprint "pmin=" & pmin
        pmax = DateAdd("d", 1, Date)
        pmax = QProcgetmaxp("BJH AS Fall/Injury Interventionss Confusion", "Sitter", pmax)
        dprint "pmax=" & pmax
        
        pmindate = Format$(pmin, "yyyymmdd")
        pmintime = Format$(pmin, "hhnn")
        pmaxdate = Format$(pmax, "yyyymmdd")
        pmaxtime = Format$(pmax, "hhnn")
        
        yesterd = Format$(DateAdd("d", -1, Date), "yyyymmdd")
        todayd = Format$(Date, "yyyymmdd")
        
        If pmindate & pmintime >= yesterd & "0700" And pmindate & pmintime < yesterd & "1900" Then 'proc yester 7a7p
            dprint "Adding 7am-7pm proc"
            numprocs = numprocs + 1
            procs(numprocs).pnum = 1
            procs(numprocs).start = g_util.CDateEx(yesterd & "0700")
            procs(numprocs).finish = g_util.CDateEx(yesterd & "1900")
        End If
        If pmaxdate & pmaxtime >= yesterd & "1900" And pmaxdate & pmaxtime < todayd & "0700" Then 'proc yester 7p7a
            dprint "Adding 7PM-7AM proc"
            numprocs = numprocs + 1
            procs(numprocs).pnum = 1
            procs(numprocs).start = g_util.CDateEx(yesterd & "1900")
            procs(numprocs).finish = g_util.CDateEx(todayd & "0700")
        End If
        
'if pull is 0700 then possibles are:  yesterday 7am-7pm, yesterday 7pm-7am
        'if Min Between yesterday 7am and 7pm  then day procedure
        'if max between yesterday 7pm and (today) 7am  then night proc
        'could be both

'only need to do this procs on 7am pull because it will have data for past 24 hrs.

'if pull is 1500 then possibles are:  today 7am-7pm only
        'if Min Between yesterday 7am and 7pm  then day procedure
        
'if pull is 0300 then possibles are:  today 7pm-7am only
        
    End If
    
'BJH VS Staff Assistance Procedure Location
'9/13/11 duration should be tied by event_datetime
'11/6/11 key off nurse and technician -- see if duration times coexist at same perf time
'        if so, then use those duration times

'Restraint reason rem 11/08/11 but need to fix.
'    dprint "Procedure duration=" & d
'    If d < 60# Then Exit Sub
'
'    If QProc1("BJH Res Restraint Reason", "Risk of injury") Then
'        If d >= 1# Then
'            pmin = #1/1/1900#
'            pmin = QProcgetminp("BJH Res Restraint Reason", "Risk of injury", pmin)
'            'procedure 1, start=pmin, end=pmin+dur
'            numprocs = numprocs + 1
'            procs(numprocs).pnum = 1
'            procs(numprocs).start = pmin
'            procs(numprocs).finish = DateAdd("n", d, pmin)
'        End If
'    End If
    
    If QLSResFreq("BJH VS Staff Assistance Procedure Location", "Nurse") Then
        'if dur time coexists then
        pmin = QProcgetminp("BJH VS Staff Assistance Procedure Location", "Nurse", pmin)
        pminRN = pmin
    End If
    If QLSResFreq("BJH VS Staff Assistance Procedure Location", "Technician") Then
        'if dur time coexists then
        pmin = QProcgetminp("BJH VS Staff Assistance Procedure Location", "Technician", pmin)
        pminNONRN = pmin
    End If
    If QLSResFreq("BJH VS Staff Assistance Procedure Location", "performed on") Then
        'if dur time coexists then
        If QProcgeteqp("BJH VS Staff Assistance Procedure Location", "performed on", pminRN) Then
            dur = QLSResStringAtPerftm("BJH VS Procedure Duration", pminRN)
            If dur = "Less than 30 minutes" Then
                onunitRNmins = 0
            Else
                onunitRNmins = val(Mid$(dur, 1, 3)) * 60#
            End If
        End If
    End If
    If QLSResFreq("BJH VS Staff Assistance Procedure Location", "performed off") Then
        'if dur time coexists then
        If QProcgeteqp("BJH VS Staff Assistance Procedure Location", "performed off", pminRN) Then
            dur = QLSResStringAtPerftm("BJH VS Procedure Duration", pminRN)
            If dur = "Less than 30 minutes" Then
                offunitRNmins = 0
            Else
                offunitRNmins = val(Mid$(dur, 1, 3)) * 60#
            End If
        End If
        
        If QProcgeteqp("BJH VS Staff Assistance Procedure Location", "performed off", pminNONRN) Then
            dur = QLSResStringAtPerftm("BJH VS Procedure Duration", pminNONRN)
            If dur = "Less than 30 minutes" Then
                offunitNONRNmins = 0
            Else
                offunitNONRNmins = val(Mid$(dur, 1, 3)) * 60#
            End If
        End If
    End If
    If onunitRNmins > 0 Then
        If QLS("BJH VS Proced Events 02") Then
            If QProcgeteqp("BJH VS Proced Events 02", "", pminRN) Then
                If pminRN <> #1/1/1900# Then
                    'procedure 8, start=pmin, end=pmin+dur
                    numprocs = numprocs + 1
                    procs(numprocs).pnum = 8
                    procs(numprocs).start = pminRN
                    procs(numprocs).finish = DateAdd("n", onunitRNmins, pminRN)
                End If
            End If
        End If
    End If
    If offunitRNmins > 0 Then
        If QLS("BJH VS Proced Events 02") Then
            If QProcgeteqp("BJH VS Proced Events 02", "", pminRN) Then
                If pminRN <> #1/1/1900# Then
                    'procedure 2, start=pmin, end=pmin+dur
                    numprocs = numprocs + 1
                    procs(numprocs).pnum = 2
                    procs(numprocs).start = pminRN
                    procs(numprocs).finish = DateAdd("n", offunitRNmins, pminRN)
                End If
            End If
        End If
    End If
    If offunitNONRNmins > 0 Then
        If QLS("BJH VS Proced Events 02") Then
            If QProcgeteqp("BJH VS Proced Events 02", "", pminNONRN) Then
                If pminNONRN <> #1/1/1900# Then
                    'procedure 3, start=pmin, end=pmin+dur
                    numprocs = numprocs + 1
                    procs(numprocs).pnum = 3
                    procs(numprocs).start = pminNONRN
                    procs(numprocs).finish = DateAdd("n", offunitNONRNmins, pminNONRN)
                End If
            End If
        End If
    End If

End If '0700
    

End Sub
Private Function QProc1(c As String, d As String) As Boolean
    Dim p As Date
    Dim done As Boolean
    Dim count As Integer
    Dim pmin As Date
    Dim r As Integer
    
'set p=0.
'Loop
'c=count the number of hits with perdt>p.
'if c>=8 then:
' p=get the perfdt of the earliest one>p, add 2 hours to it, and r=count the number found in this range.
' if r>=8 then  inds(22).checked=true
' else c=c-1
'endloop
    pmin = #1/1/1900#
    count = QProccount(c, d, pmin)   'count c,d with perdt > p
    done = False
    While Not done
        If count >= 16 Then
            'p= min perfdt where perfdt > p
            pmin = QProcgetminp(c, d, pmin)
            If pmin <> #1/1/1900# Then
                'r= count c,d with perfdt between p and p+2
                r = QProcrangecount(c, d, pmin)
                If r >= 16 Then
                    QProc1 = True
                    done = True
                End If
            End If
            count = count - 1
        Else
            done = True
        End If
    Wend
    
End Function
Private Function QProccount(c As String, d As String, p As Date) As Integer
    Dim rs As New Recordset
    Dim sql As String
    
    QProccount = 0
    sql = "select count(*) from " & DBTABLENAME & " where acctnum='" & acctnum & "' and code='" & c & "' and res like " & "'%" & d & "%'"
    sql = sql & " and perfdt>" & g_dbutil.SQL_DateTime(p)
    rs.Open sql, g_cnADO
    If rs.RecordCount > 0 Then QProccount = rs(0)
    rs.Close
    Set rs = Nothing

End Function
Private Function QProcgetminp(c As String, d As String, p As Date) As Date
    Dim rs As New Recordset
    Dim sql As String
    
    QProcgetminp = #1/1/1900#
    sql = "select min(perfdt) from " & DBTABLENAME & " where acctnum='" & acctnum & "' and code='" & c & "' and res like " & "'%" & d & "%'"
    sql = sql & " and perfdt>" & g_dbutil.SQL_DateTime(p)
    rs.Open sql, g_cnADO
    If Not IsNull(rs(0)) Then QProcgetminp = rs(0)
    rs.Close
    Set rs = Nothing

End Function
Private Function QProcgetmaxp(c As String, d As String, p As Date) As Date
    Dim rs As New Recordset
    Dim sql As String
    
    QProcgetmaxp = p
    sql = "select max(perfdt) from " & DBTABLENAME & " where acctnum='" & acctnum & "' and code='" & c & "' and res like " & "'%" & d & "%'"
    sql = sql & " and perfdt<" & g_dbutil.SQL_DateTime(p)
    rs.Open sql, g_cnADO
    If Not IsNull(rs(0)) Then QProcgetmaxp = rs(0)
    rs.Close
    Set rs = Nothing

End Function
Private Function QProcgeteqp(c As String, d As String, p As Date) As Boolean
    Dim rs As New Recordset
    Dim sql As String
    
    QProcgeteqp = False
    sql = "select count(*) from " & DBTABLENAME & " where acctnum='" & acctnum & "' and code='" & c & "'"
    If d <> "" Then sql = sql & "and res like " & "'%" & d & "%'"
    sql = sql & " and perfdt=" & g_dbutil.SQL_DateTime(p)
    rs.Open sql, g_cnADO
    If Not IsNull(rs(0)) Then QProcgeteqp = (rs(0) > 0)
    rs.Close
    Set rs = Nothing

End Function

Private Function QProcgetminpList(c As String, reslist As String, p As Date) As Date
    Dim rs As New Recordset
    Dim sql As String
    Dim r() As String
    Dim i As Integer
    
    QProcgetminpList = #1/1/1900#
    r = Split(reslist, ",")
    sql = "select min(perfdt) from " & DBTABLENAME & " where acctnum='" & acctnum & "' and code='" & c & "'"
    If reslist <> "" Then
        sql = sql & " and (res like " & "'%" & r(0) & "%'"
        For i = 1 To UBound(r)
            sql = sql & " or res like '%" & r(i) & "%'"
        Next i
        sql = sql & ")"
    End If
    sql = sql & " and perfdt=" & g_dbutil.SQL_DateTime(p)
    rs.Open sql, g_cnADO
    If Not IsNull(rs(0)) Then QProcgetminpList = rs(0)
    rs.Close
    Set rs = Nothing

End Function

Private Function QProcrangecount(c As String, d As String, pmin As Date) As Integer
    Dim rs As New Recordset
    Dim sql As String
    Dim pmax As Date
    
    QProcrangecount = 0
    pmax = DateAdd("h", 8, pmin)
    sql = "select count(*) from " & DBTABLENAME & " where acctnum='" & acctnum & "' and code='" & c & "' and res like " & "'%" & d & "%'"
    sql = sql & " and perfdt between " & g_dbutil.SQL_DateTime(pmin) & " and " & g_dbutil.SQL_DateTime(pmax)
    rs.Open sql, g_cnADO
    If rs.RecordCount > 0 Then QProcrangecount = rs(0)
    rs.Close
    Set rs = Nothing

End Function

Private Sub OutputProcs()
    Dim i, j As Integer
    Dim s, f As Date
    Dim outstr As String, proc_list As String
    
    For i = 1 To numprocs
        procs(i).isvalid = True
    Next i
    For i = 1 To numprocs - 1
        If procs(i).isvalid Then
        For j = i + 1 To numprocs
            If procs(j).isvalid And procs(j).start = procs(i).start And procs(j).finish = procs(i).finish Then   'this can be combined.
                procs(j).isvalid = False
                procs(j).pindex = i
            End If
        Next j
        End If
    Next i

    For i = 1 To numprocs
        If procs(i).isvalid Then
        outstr = g_util.FixedWidth("", 8)                                       '(facility code)
        outstr = outstr & "|" & g_util.FixedWidth(unitname, 16)
        outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(unit code)
        outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(area code)
        outstr = outstr & "|" & g_util.FixedWidth("", 8)                        '(class date - give datetime instead)
        outstr = outstr & "|" & g_util.FixedWidth(acctnum, 20)
        outstr = outstr & "|" & g_util.FixedWidth(lastname, 32)
        outstr = outstr & "|" & g_util.FixedWidth(firstname, 32)
        outstr = outstr & "|" & g_util.FixedWidth("", 32)                       '(middle)
        outstr = outstr & "|" & g_util.FixedWidth(roomname, 8)
        outstr = outstr & "|" & g_util.FixedWidth(bedname, 4)
        outstr = outstr & "|" & Format$(procs(i).start, "yyyymmddhhnn")       '204 procdt
        outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(login)
        outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(employee)
        outstr = outstr & "|" & g_util.FixedWidth("15", 4)
        outstr = outstr & "|" & g_util.FixedWidth("P", 1)                       'record type = class
        outstr = outstr & "|"
        outstr = outstr & Space$(294 - Len(outstr))
        outstr = outstr & "|" & Format$(procs(i).start, "yyyymmddhhnn")       '296 procdt in
        outstr = outstr & Space$(346 - Len(outstr))
        If procs(i).finish = 0 Then
            outstr = outstr & "|" & Space$(12)
        Else
            outstr = outstr & "|" & Format$(procs(i).finish, "yyyymmddhhnn")      '348 procdt out
        End If
        outstr = g_util.FixedWidth(outstr, 377)
        outstr = outstr & "|NNNNNNNNN"
        Mid$(outstr, 378 + procs(i).pnum, 1) = "Y"
        proc_list = proc_list & "," & procs(i).pnum
        For j = i + 1 To numprocs
            If Not procs(j).isvalid And procs(j).pindex = i Then
                Mid$(outstr, 378 + procs(j).pnum, 1) = "Y"
                proc_list = proc_list & "," & procs(j).pnum
            End If
        Next j
        proc_list = Mid$(proc_list, InStr(1, proc_list, ",") + 1)          'strip leading comma
        
        Print #outfile, outstr
        Print #outlogfile, outstr
        
        dprint "Procedure: " & procs(i).pnum
        End If 'isvalid
    Next i

End Sub

Private Sub QAssessSetExclusionTimeRange(c As String)
    Dim rs As New Recordset
    Dim sql As String
    Dim minpfdt, maxpfdt As Date
        
    exclusionRange = ""
    minpfdt = #1/1/1900#
    maxpfdt = minpfdt
    
    sql = "select min(perfdt) from " & DBTABLENAME & " where acctnum='" & acctnum & "' and code like '%" & c & "%'"
'    sql = sql & " and perfdt>" & g_dbutil.SQL_DateTime(p)
    rs.Open sql, g_cnADO
    If Not IsNull(rs(0)) Then minpfdt = rs(0)
    rs.Close
    
    If minpfdt = #1/1/1900# Then Exit Sub
    
    sql = "select max(perfdt) from " & DBTABLENAME & " where acctnum='" & acctnum & "' and code like '%" & c & "%'"
    rs.Open sql, g_cnADO
    If Not IsNull(rs(0)) Then maxpfdt = rs(0)
    rs.Close
    Set rs = Nothing
    
    If maxpfdt = #1/1/1900# Or (minpfdt = maxpfdt) Then Exit Sub
    
    exclusionRange = " and perfdt NOT between " & g_dbutil.SQL_DateTime(minpfdt) & " and " & g_dbutil.SQL_DateTime(maxpfdt)

End Sub

