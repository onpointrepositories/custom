::
:: Action: Renames transparent.txt to tcqueryyyyymmddhhnn.txt
::
echo on
set d=%date%
set t=%time%
set b=%t:~-11,1%
set h=%t:~-11,2%
if "%b%"==" " set h=0%t:~-10,1%
set f=tcquery%d:~-4,4%%d:~-10,2%%d:~-7,2%%h%%t:~-8,2%.txt
ren transparent.txt %f%

