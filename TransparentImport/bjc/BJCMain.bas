Attribute VB_Name = "MainModule"
Option Explicit
'The location of the input is determined by the first line of the dictionary.
'
' This is the main module for SiePFS
' File is formatted as:
'    <Header>
'       <Event>
'       <Event>
'         ...
'
Public g_cnADO      As adodb.Connection             'main DB connection
Public g_util       As New PFSUtility               'utility classes
Public g_dbutil     As New PFSDBUtility
Public g_command    As String                       'command line


Const TRANSP_FILENAME = "Transparent.txt"
Const TRANSP_INLOG_FILE_LIFE = 10 'days
Const TRANSP_OUTLOG_FILE_LIFE = 10 'days
Const TRANSP_DEBUG_FILE_LIFE = 10 'days
Const MSDIC_FNAME = "MSDICT.DAT"
Const MHDIC_FNAME = "MHDICT.DAT"
Const DNLD_FNAME = "TCQUERY"
Const VALIDMS_FNAME = "VALIDMS.UNT"
Const VALIDMH_FNAME = "VALIDMH.UNT"
Const PERDATA_FNAME = "PERSIST.DAT"
Const DBTABLENAME = "TC_DATA"

'HEADER RECORD
Const MAX_INDICATORS = 50

Const START_ACCT_NUM = 1
Const LEN_ACCT_NUM = 20

Const START_PTNAME = 21
Const LEN_PTNAME = 30

Const START_UNIT = 51
Const LEN_UNIT = 10

Const START_RM = 61
Const LEN_RM = 10

Const START_BED = 71
Const LEN_BED = 5

Const START_FACILITY = 76
Const LEN_FAC = 5

'EVENT RECORD
Const START_EVENT_DT = 5
Const LEN_DT = 14

Const START_EVENT = 20
Const LEN_EVENT = 30

Const START_EVENT_DSC = 50
Const LEN_EVENT_DSC = 25

Const START_FREQ = 75
Const LEN_FREQ = 10

Const START_RESULT = 116
Const LEN_RESULT = 75

Const MAX_RANGE = 1440  '24 hrs in minutes; this is overridden by the -range arg.

Const RANGE_FACTOR = 24 * 60   'For proportioning the count of Med/Pulm/Cardio

Const MAX_UNIQUE_CHART_TIMES = 20

Dim infn As String
Dim fnames() As String
Dim inlogname As String
Dim outfn As String
Dim outlogfile As Integer
Dim outlogname As String
Dim dbugfile As Integer
Dim dbugname As String

Private Type indicator
  indwinpfs As Integer  'winpfs indic number associated with this cerner event
  eventid As String     'Event_CD of this indicator
  chartkey As String    'look for this key in charted result
  gavefreq As Boolean   'file supplied the frequency; ignore subsequent freqs
  check_freq As Boolean 'flag to check frequency
  freq_basis As Integer 'frequency required for this indicator (in minutes)
  act_freq As Integer   'calculated frequency from data (in minutes)
  last_datetime As String 'last date time of event found in this patient
  num_found As Integer  'number of times this was found in this patient
  also_mark As String   'if this indicator is marked, then also mark these.
  'charttime(MAX_UNIQUE_CHART_TIMES) As String       commented out and in code to save mem space 9/26/07
  one_row_find As Single
  persist As Boolean  'flag if this charting element persists
  perlen As Single    'for how long does it persist? in minutes
  icupersist As Boolean 'persist for regardless of icu, represented by & in dictionary
End Type

Private Type indicator_data
    checked As Boolean
    also_mark As String
End Type

Private Type ProcessedInfo
    a As String     'acctnum
    already_did_persist As Boolean
End Type

Dim dicary() As indicator
Dim mhdicary() As indicator
Dim dicnum As Integer
Dim mhdicnum As Integer
Dim inDirPath As String  'path of input file
Dim winpfspath As String 'path of winpfs
Dim winlogpath As String 'path of winpfs\log
Dim winloadpath As String ' path of winpfs\load_me

Dim datafile As Integer
Dim outfile As Integer

Dim inds(MAX_INDICATORS) As indicator_data
Dim grps(MAX_INDICATORS) As Integer
Dim mhinds(MAX_INDICATORS) As indicator_data
Dim mhgrps(MAX_INDICATORS) As Integer

Dim fullname As String
Dim lastname As String
Dim firstname As String
Dim unitname As String
Dim roomname As String
Dim bedname As String
Dim acctnum As String
Dim classdt As String
Dim classdate As String
Dim classtime As String
Dim nowdt As Date
Dim intime As String
Dim saveintime As String

Dim range As Single  'number of minutes in the scope of time for freq.
Dim dbugon As Boolean
Dim suppressDbugLog As Boolean
Dim effdateon As Boolean
Dim effdate As String
Dim efftimeon As Boolean
Dim efftime As String
Dim pulltimeon As Boolean
Dim pulltime As String
Dim pulldateon As Boolean
Dim pulldate As String

Dim event6931 As Boolean
Dim event33474 As Boolean
Dim event16966 As Boolean
Dim event21430 As Boolean
Dim count18098 As Single
Dim event33765 As Boolean
Dim val33775 As Single
Dim event33775 As Boolean
Dim dt33775 As String
Dim val35286 As Single
Dim dt35286 As String
Dim event35286 As Boolean
Dim count5805 As Single
Dim count5806 As Single

Dim MSunitary() As String
Dim MHunitary() As String
Dim unitnum As Integer
Dim mhunitnum As Integer

Dim acctnumdirectory() As ProcessedInfo
Dim numacct As Single
Dim telemetry As Boolean

Dim self89444 As Boolean
Dim b90117 As Boolean
Dim b92420 As Boolean
Dim b95017 As Boolean
Dim b95016 As Boolean
Dim b94816 As Boolean
Dim b94815 As Boolean
Dim b93033 As Boolean


Sub Main()
    Const RANGE_PARAM = "-range="
    Const EFFECTIVE_PARAM = "-efftime="
    Const DBUG_ON = "-debug"
    Const ALTER_DATE = "-effdate="
    Const PULL_TIME = "-pulltime="
    Const PULL_DATE = "-pulldate="
    Dim n As Integer
    Dim i As Integer
    Dim cmdLine As String
    Dim epos As Integer
    Dim rpos As Integer
    Dim effective As String
    Dim h As String
    Dim t As Date
    Dim adpos As Integer
    
    Set g_cnADO = g_dbutil.NewRemoteConnection
    '-efftime=hhmm  This is the time at which the classification is effective.
    '                 The date associated with this time is taken from header classdate.
    '                 If not specified, then -efftime is assumed to be the classtime
    '                 from the header.
    '-effdate=yyyymmdd   This is the date which, in combination with the
    '                 -effective time is used to specify the effective datetime.
    '                 If not specified, then -effdate is assumed to be the classdate
    '                 from the header.
    'Special value:  -effdate=yesterday means Now's yesterday.
        
    '-pulltime=hhmm  This is the time starting from which the pull is to
    '                look backwards from.
    '                If not specified, then this time is assumed to be the -effective time.
    '-pulldate=yyyymmdd This is the date of the pulltime.
    '                If not specified, then this is assumed to be Now's date.
    
    '-range=nnnn  This is the number of minutes backwards from the pull time
    '             that defines valid range of charting events.
    
    nowdt = Now
    cmdLine = LCase(Command$)
    
    rpos = InStr(cmdLine, RANGE_PARAM)
    epos = InStr(cmdLine, EFFECTIVE_PARAM)
    
    dbugon = (InStr(cmdLine, DBUG_ON) > 0)
    suppressDbugLog = False
    
    effdateon = (InStr(cmdLine, ALTER_DATE) > 0)
    If effdateon Then
        adpos = InStr(cmdLine, ALTER_DATE)
        If UCase$(Mid$(cmdLine, adpos + Len(ALTER_DATE), 8)) = "YESTERDA" Then
            effdate = Format(DateAdd("d", -1, g_util.DateOnly(nowdt)), "yyyymmdd")
        Else
            effdate = Mid$(cmdLine, adpos + Len(ALTER_DATE), 8) 'yyyymmdd
        End If
    End If
    
    efftimeon = (InStr(cmdLine, EFFECTIVE_PARAM) > 0)
    
    pulltimeon = (InStr(cmdLine, PULL_TIME) > 0)
    If pulltimeon Then
        pulltime = Mid$(cmdLine, InStr(cmdLine, PULL_TIME) + Len(PULL_TIME), 4)
    End If
    
    pulldateon = (InStr(cmdLine, PULL_DATE) > 0)
    If pulldateon Then
        pulldate = Mid$(cmdLine, InStr(cmdLine, PULL_DATE) + Len(PULL_DATE), 8)
    End If

    If epos > 0 Then
        effective = Mid$(cmdLine, InStr(cmdLine, EFFECTIVE_PARAM) + Len(EFFECTIVE_PARAM), 4)
    End If
    
    If rpos > 0 Then
        range = val(Mid$(cmdLine, InStr(cmdLine, RANGE_PARAM) + Len(RANGE_PARAM), 4))
    End If
    
    If range <= 0 Then
        range = MAX_RANGE
    End If
    
'    If effective = "" Then
'        effective = Format(nowdt, "hhnn")
'    End If
    
    intime = ""
    
    'infn = "C:\Documents and Settings\kmasumoto\Desktop\TC\BJ TC\bjcmt0625.txt"
    infn = "C:\Documents and Settings\kmasumoto\Desktop\TC\BJ TC\transparent2.txt"
    Process

    Set g_cnADO = Nothing
End Sub
Private Function LoadDictionaries() As Boolean
    Dim dicfn As String
    Dim dicfile As Integer
    Dim buf As String
    Dim unitfn As String
    Dim unitfile As Integer
    
    'Special dictionary characters:
    '  * = check freq or other things before triggering indicator
    '  # = persist only
    '  @ = persist and check freq
    dicnum = 0
    dicfile = FreeFile
    dicfn = App.Path & "\" & MSDIC_FNAME
    Open dicfn For Input As #dicfile
    While Not EOF(dicfile)
        dicnum = dicnum + 1
        If (dicnum = 1) Then
            Line Input #dicfile, inDirPath
        Else
            Line Input #dicfile, buf
            ReDim Preserve dicary(0 To dicnum - 1)
'            dicary(dicnum - 1).indwinpfs = val(Mid$(buf, 1, 2))
'            dicary(dicnum - 1).eventid = Trim$(Mid$(buf, 4, 17))
'            dicary(dicnum - 1).chartkey = Trim$(Mid$(buf, 38, 75))
'            dicary(dicnum - 1).check_freq = (Mid$(buf, 3, 1) = "*")
'            dicary(dicnum - 1).freq_basis = val(Trim$(Mid$(buf, 22, 4)))
'            dicary(dicnum - 1).also_mark = Trim$(Mid$(buf, 28, 10))
            dicary(dicnum - 1).indwinpfs = val(Mid$(buf, 1, 2))
            dicary(dicnum - 1).check_freq = (Mid$(buf, 3, 1) = "*") Or (Mid$(buf, 3, 1) = "@") Or (Mid$(buf, 3, 1) = "&")
            dicary(dicnum - 1).eventid = Trim$(Mid$(buf, 4, 30))
            dicary(dicnum - 1).freq_basis = val(Trim$(Mid$(buf, 34, 4)))
            dicary(dicnum - 1).also_mark = Trim$(Mid$(buf, 39, 10))
            dicary(dicnum - 1).chartkey = Trim$(Mid$(buf, 50, 75))
            dicary(dicnum - 1).persist = (Mid$(buf, 3, 1) = "#") Or (Mid$(buf, 3, 1) = "@") Or (Mid$(buf, 3, 1) = "&") Or (Mid$(buf, 3, 1) = "$")
            If dicary(dicnum - 1).persist Then dicary(dicnum - 1).perlen = 1440
            dicary(dicnum - 1).icupersist = (Mid$(buf, 3, 1) = "&") Or (Mid$(buf, 3, 1) = "$")
            ' * + # = @
            ' * + # + icu = @ + icu = &
            ' # + icu = $
        End If
    Wend
    Close #dicfile
    dicnum = dicnum - 1
    
'Valid Med Surg units
    unitnum = 0
    unitfile = FreeFile
    unitfn = App.Path & "\" & VALIDMS_FNAME
    Open unitfn For Input As #unitfile
    While Not EOF(unitfile)
        Line Input #unitfile, buf
        If Trim$(buf) <> "" Then
            unitnum = unitnum + 1
            ReDim Preserve MSunitary(0 To unitnum)
            MSunitary(unitnum) = UCase$(Trim$(Mid$(buf, 1, 16)))
        End If
    Wend
    Close #unitfile
    
    mhdicnum = 0
    dicfile = FreeFile
    dicfn = App.Path & "\" & MHDIC_FNAME
    Open dicfn For Input As #dicfile
    While Not EOF(dicfile)
        mhdicnum = mhdicnum + 1
        Line Input #dicfile, buf
        ReDim Preserve mhdicary(0 To mhdicnum - 1)
        mhdicary(mhdicnum - 1).indwinpfs = val(Mid$(buf, 1, 2))
        mhdicary(mhdicnum - 1).check_freq = (Mid$(buf, 3, 1) = "*") Or (Mid$(buf, 3, 1) = "@")
        mhdicary(mhdicnum - 1).eventid = Trim$(Mid$(buf, 4, 30))
        mhdicary(mhdicnum - 1).freq_basis = val(Trim$(Mid$(buf, 34, 4)))
        mhdicary(mhdicnum - 1).also_mark = Trim$(Mid$(buf, 39, 10))
        mhdicary(mhdicnum - 1).chartkey = Trim$(Mid$(buf, 50, 75))
        mhdicary(mhdicnum - 1).persist = (Mid$(buf, 3, 1) = "#") Or (Mid$(buf, 3, 1) = "@")
        If mhdicary(mhdicnum - 1).persist Then mhdicary(mhdicnum - 1).perlen = 1440
    Wend
    Close #dicfile
    mhdicnum = mhdicnum - 1
        
'Valid MH units
    mhunitnum = 0
    unitfile = FreeFile
    unitfn = App.Path & "\" & VALIDMH_FNAME
    Open unitfn For Input As #unitfile
    While Not EOF(unitfile)
        Line Input #unitfile, buf
        If Trim$(buf) <> "" Then
            mhunitnum = mhunitnum + 1
            ReDim Preserve MHunitary(0 To mhunitnum)
            MHunitary(mhunitnum) = UCase$(Trim$(Mid$(buf, 1, 16)))
        End If
    Wend
    
'don't need the unit file anymore
    Close #unitfile
    
'init wild charting array
    
    LoadDictionaries = True
    
End Function

Private Function GetAllInputFilenames() As Integer
    'The location is determined by the first line of the dictionary.
    Const INITIAL_BLOCK_SIZE = 24
    Dim i As Integer
    Dim infname As String
    
    outfn = winloadpath & "\" & TRANSP_FILENAME
    
    ReDim fnames(0 To INITIAL_BLOCK_SIZE) As String
    i = 0
    infname = Dir$(inDirPath + "\" & DNLD_FNAME & "*.TXT") 'returns ONLY the filename.  For wildcards.
    While infname <> ""
        i = i + 1
        If (i > UBound(fnames)) Then
            ReDim Preserve fnames(0 To i + INITIAL_BLOCK_SIZE) As String
        End If
        fnames(i) = infname
        infname = Dir$
    Wend
    
    BubbleSort fnames, i
    
    GetAllInputFilenames = i
    
End Function

Private Sub BubbleSort(arr As Variant, numEls As Integer)
    Dim lLoop1 As Integer
    Dim lLoop2 As Integer
    Dim lTemp As Variant
    
    For lLoop1 = numEls To LBound(arr) Step -1
       For lLoop2 = LBound(arr) + 1 To lLoop1
           If arr(lLoop2 - 1) > arr(lLoop2) Then
              lTemp = arr(lLoop2 - 1)
              arr(lLoop2 - 1) = arr(lLoop2)
              arr(lLoop2) = lTemp
           End If
       Next lLoop2
   Next lLoop1
End Sub

Private Sub DoPatientSummary()
    Dim highest_is_on As Boolean
    Dim g As Integer
    Dim i As Integer
    Dim p As Integer
    
'    'Here is where you now have to go through the frequencies to
'    'determine which indicators to mark for this pt.
'
'    'Special ADL override:  3 is higher than 4.
'    If inds(3).checked Then inds(4).checked = False
'    If mhinds(3).checked Then mhinds(4).checked = False
'
'    'set the act_freq to 1440 if num_found is >= 1
''    For i = 1 To dicnum
''        If dicary(i).num_found >= 1 Then
''            dicary(i).act_freq = 1440
''        End If
''    Next i
'
'    If ValidMSUnit(unitname) Then
'        Check1
'        Check2_3_4
'        Check6
'        Check7
'        Check8
'        Check9
''        Check11
'        Check12_13
'        Check14_15_16
'        Check17_18_19
'        Check20_21_22
'        Check23
'        Check24_25_26
'        Check27
'        Check28
'        Check29
'        Check42
'        Check44
'
'        MSGlobalTurnOffs
'
'    'Next, loop the dictionary to find the indicator according to frequency.
''    For i = 1 To dicnum
''        If dicary(i).num_found > 0 Then
''            p = FindClosestFreq(i)
''            If (p > 0) Then
''                inds(dicary(p).indwinpfs).checked = True
''                dprint dicary(p).indwinpfs, 1, "via fall through"
''
''                If (dicary(p).num_found > 0 And Not inds(dicary(p).indwinpfs).checked) Then
''                    dprint dicary(p).indwinpfs, -1, "***FAILED TO TURN ON***"
''                End If
''
''                If inds(dicary(p).indwinpfs).checked Then
''                    If inds(dicary(p).indwinpfs).also_mark <> "" Then
''                        'Only do the also mark if it originally arose from an also-mark ind.
''                        ParseAlsoMark (inds(dicary(p).indwinpfs).also_mark)
''                    End If
''                End If
''            End If
''        End If
''    Next i
'
'    '03/23/06: ADL 2 is the minimum as decided in phone mtg on 3/22/06
''    If Not (inds(1).checked Or inds(2).checked Or inds(3).checked Or inds(4).checked) Then
''        dprint 2, 1, "via AtLeastOneADL"
''    End If
'    'inds(2).checked = True
    
    'Now, if there are any mutually exclusive indicators marked
    'then remove the lesser ones.
'    g = 0
'    highest_is_on = False
'    For i = MAX_INDICATORS To 1 Step -1
'        If (grps(i) > 0) Then
'            If (grps(i) <> g) Then
'                g = grps(i)
'                highest_is_on = inds(i).checked
'            Else
'                If highest_is_on Then
'                    inds(i).checked = False
'                Else
'                    highest_is_on = inds(i).checked
'                End If
'            End If
'        End If
'    Next i
'
'    'Make sure there is at least 1 ADL marked
'    AtLeastOneADL
'
'    End If
'
'    If ValidMHUnit(unitname) Then
'        mhCheck1
'        mhCheck2_3_4
'        mhCheck6
'        mhCheck7
'        mhCheck8
'        mhCheck9
'        mhCheck10
'        mhCheck11
'        mhCheck12
'        mhCheck14_15
'        mhCheck16
'        mhCheck17
'        mhCheck18
'        mhCheck19
'        mhCheck20
'        mhCheck21
'        mhCheck22_23
'        mhCheck26
'        mhCheck29
'
'    'Now, if there are any mutually exclusive indicators marked
'    'then remove the lesser ones.
'        g = 0
'        highest_is_on = False
'        For i = MAX_INDICATORS To 1 Step -1
'            If (mhgrps(i) > 0) Then
'                If (mhgrps(i) <> g) Then
'                    g = mhgrps(i)
'                    highest_is_on = mhinds(i).checked
'                Else
'                    If highest_is_on Then
'                        mhinds(i).checked = False
'                    Else
'                        highest_is_on = mhinds(i).checked
'                    End If
'                End If
'            End If
'        Next i
'
'    mhAtLeastOneADL
'
'    End If
        
End Sub

Private Sub Process()
    Dim buf As String
    Dim p As Integer
    Dim i As Long
    Dim formloading As Boolean
    Dim s As String
    Dim rows() As String
    Dim cols() As String
    Dim maxrow As Long
    Dim iRow As Long
    Dim n As Integer
    
    Dim sql As String
    Dim hdr As String
    Dim count As Long
    Dim h As String
    Dim rowcount As Integer
    Dim persistcount As Integer
    On Error GoTo errprocess
        
    formloading = True
'    cmdProcess.Enabled = False
'    cmdOutput.Enabled = False
    dprint "Process"
    datafile = FreeFile
    Open infn For Input As #datafile
    s = Input$(LOF(datafile), datafile)
    rows() = Split(s, vbCrLf)
    maxrow = UBound(rows)
    
    Close #datafile
    
    'PERSISTENCE:  Only some labseqs persist.
    'delete tc_data where labseq not in (persist labseq list)
    'delete tc_data where diff > 24 hrs between intime and perftime
    'new inserts
    'ignore patients where only persist records exist
    'datediff(hh,performdate," & g_dbutil.SQL_DateTime(nowdt) & ") <= 24
    'convert(datetime,performdate)


    'NEW INSERTS:
    'Write the datafile to TC_DATA table
    For iRow = 0 To maxrow
        buf = rows(iRow)
        cols() = Split(buf, vbTab)
        'hdr = "INSERT INTO BJCMT (ind,code,descript,freq,note) VALUES ("
        hdr = "INSERT INTO BJCdata (acctnum,name,perfdt,code,descript) VALUES ("
        hdr = hdr & "'" & Trim$(cols(0)) & "',"
        hdr = hdr & "'" & Trim$(cols(3)) & "',"
        hdr = hdr & "'" & Trim$(cols(4)) & "',"
        hdr = hdr & "'" & Trim$(cols(5)) & "',"
        hdr = hdr & "'" & Trim$(cols(6)) & "')"
        
'        hdr = hdr & cols(0) & ","
'        hdr = hdr & "'" & Trim$(cols(1)) & "',"
'        hdr = hdr & "'" & Trim$(cols(2)) & "',"
'        hdr = hdr & "'" & Trim$(cols(3)) & "',"
'        hdr = hdr & "'" & Trim$(cols(4)) & "')"
        
        g_cnADO.Execute hdr
    Next

    
errprocess:
    Close #datafile
    Err.Raise Err.Number, Err.source, Err.Description
        
End Sub
Private Sub Translate(meth As Integer)

    If meth = 1 Then 'Inpatient
        Check1_2_3
        Check4
        Check5_6
        Check7
        Check8
        Check9_10
        Check11_12
        Check13
        CheckAssessments
        Check18
        Check19_20
        Check21
        Check22
        CheckCustom
    ElseIf meth = 2 Then 'PFS/WM Mental Health
        hCheck1to10
        hCheck11to20
        hCheck21to49
    End If
    
    ParsePatientInfo
    AssembleOutput
    
End Sub


Private Function DicIndex(id As String, ByRef s As String) As Integer
    Dim i As Integer
    Dim p As Integer
    
    ' inds(i).num_found is set only for dictionary items with asterisk (*)
    ' in column 3 of the dictionary file.

'    DicIndex = 0
'    For i = 1 To dicnum
'        If (id = dicary(i).eventid) Then
'            If (Trim$(dicary(i).chartkey) <> "") Then
'                p = InStr(1, s, dicary(i).chartkey, vbTextCompare)
'                If (p > 0) Then
'                    DicIndex = i
'                    'Remove the charting text in prep for another eval.
'                    s = Replace(s, dicary(i).chartkey, "", , , vbTextCompare)
'                    dprint 0, 0, "Dictionary item found=" & i & " id=" & id & " charting=" & s & " mapping to=" & dicary(i).indwinpfs
'                    Exit For
'                End If
'            Else 'don't care about matching chartkey
'                DicIndex = i
'                dprint 0, 0, "Dictionary item found=" & i & " id=" & id & " charting=%don't care%" & " mapping to=" & dicary(i).indwinpfs
'                Exit For
'            End If
'        End If
'    Next i
End Function
Private Function mhDicIndex(id As String, ByRef s As String) As Integer
    Dim i As Integer
    Dim p As Integer
    
'    mhDicIndex = 0
'    For i = 1 To mhdicnum
'        If (id = mhdicary(i).eventid) Then
'            If (Trim$(mhdicary(i).chartkey) <> "") Then
'                p = InStr(1, s, mhdicary(i).chartkey, vbTextCompare)
'                If (p > 0) Then
'                    mhDicIndex = i
'                    'Remove the charting text in prep for another eval.
'                    s = Replace(s, mhdicary(i).chartkey, "", , , vbTextCompare)
'                    dprint 0, 0, "Dictionary item found=" & i & " id=" & id & " charting=" & s & " mapping to=" & mhdicary(i).indwinpfs
'                    Exit For
'                End If
'            Else 'don't care about matching chartkey
'                mhDicIndex = i
'                dprint 0, 0, "Dictionary item found=" & i & " id=" & id & " charting=%don't care%" & " mapping to=" & mhdicary(i).indwinpfs
'                Exit For
'            End If
'        End If
'    Next i
End Function

Private Sub InitIndicators()
    Dim i As Integer
    
    For i = 1 To MAX_INDICATORS
        inds(i).checked = False
        inds(i).also_mark = ""
        mhinds(i).checked = False
        mhinds(i).also_mark = ""
    Next i
    
    self89444 = False
    b90117 = False
    b92420 = False
    b95017 = False
    b95016 = False
    b94816 = False
    b94815 = False
    b93033 = False
End Sub
Private Sub InitGroups()
    Dim i As Integer
    
    For i = 1 To MAX_INDICATORS
        grps(i) = 0
    Next i
    
    For i = 1 To 3
        grps(i) = 1
    Next i
    For i = 5 To 6
        grps(i) = 2
    Next i
    For i = 9 To 10
        grps(i) = 3
    Next i
    For i = 11 To 12
        grps(i) = 4
    Next i
    For i = 14 To 17
        grps(i) = 5
    Next i
    For i = 19 To 20
        grps(i) = 6
    Next i
    
    
    For i = 1 To MAX_INDICATORS
        mhgrps(i) = 0
    Next i
    
    For i = 1 To 4
        mhgrps(i) = 1
    Next i
    For i = 9 To 10
        mhgrps(i) = 2
    Next i
    For i = 11 To 13
        mhgrps(i) = 3
    Next i
    For i = 14 To 15
        mhgrps(i) = 4
    Next i
    For i = 22 To 24
        mhgrps(i) = 5
    Next i
    
End Sub

Private Function ItemExists(col As Collection, key As String) As Boolean
    Dim dummy As Variant
    On Error Resume Next
    dummy = col.Item(key)
    ItemExists = (Err <> 5)
End Function

Private Sub AtLeastOneADL()

    If Not (inds(1).checked Or inds(2).checked Or inds(3).checked) Then
        inds(2).checked = True
        dprint "via AtLeastOneADL"
    End If
End Sub
Private Sub mhAtLeastOneADL()

    If Not (mhinds(1).checked Or mhinds(2).checked Or mhinds(3).checked Or mhinds(4).checked) Then
        mhinds(2).checked = True
        dprint "via AtLeastOneADL"
    End If
End Sub

Private Sub ParseAlsoMark(s As String)
    Dim comma_pos As Integer
    Dim ind As Integer
    
    'for example:   29,31  is the also_mark string for RESTRAINTS
    
    Do
        comma_pos = InStr(s, ",")
        If (comma_pos = 0) Then
            ind = val(s)
        Else
            ind = val(Mid$(s, 1, comma_pos - 1))
            s = Mid$(s, comma_pos + 1, Len(s) - comma_pos)
        End If
        
        inds(ind).checked = True
        dprint "via Also-mark"
        
    Loop Until comma_pos = 0
End Sub
Private Sub mhParseAlsoMark(s As String)
    Dim comma_pos As Integer
    Dim ind As Integer
    
    'for example:   29,31  is the also_mark string for RESTRAINTS
    
    Do
        comma_pos = InStr(s, ",")
        If (comma_pos = 0) Then
            ind = val(s)
        Else
            ind = val(Mid$(s, 1, comma_pos - 1))
            s = Mid$(s, comma_pos + 1, Len(s) - comma_pos)
        End If
        
        mhinds(ind).checked = True
        dprint "via Also-mark"
        
    Loop Until comma_pos = 0
End Sub


Private Sub ParsePatientInfo()
    Dim commapos As Integer
    
    commapos = InStr(fullname, ",")
    If commapos = 0 Then
        lastname = fullname
        firstname = ""
    Else
        lastname = Mid$(fullname, 1, commapos - 1)
        firstname = Mid$(fullname, commapos + 1, Len(fullname) - commapos)
    End If
    
    
'    If Not efftimeon Then intime = Trim$(Mid$(s, START_CLASS_DT, LEN_DT))
End Sub


Private Sub AssembleOutput()
    Dim outstr As String
    Dim i As Integer
    Dim din As Date
    Dim tin As Date
    Dim ok As Boolean
    Dim g As Integer
    Dim highest_is_on As Boolean
    
    ok = (acctnum <> "") And (unitname <> "")
'    ok = False
'    Select Case UCase$(unitname)
'        Case "A4W", "A6M", "A9M"
'            ok = True
'    End Select
    
    If Not ok Then
        Exit Sub
    End If
    If ValidMSUnit(unitname) Then
        g = 0
        highest_is_on = False
        For i = MAX_INDICATORS To 1 Step -1
            If (grps(i) > 0) Then
                If (grps(i) <> g) Then
                    g = grps(i)
                    highest_is_on = inds(i).checked
                Else
                    If highest_is_on Then
                        inds(i).checked = False
                    Else
                        highest_is_on = inds(i).checked
                    End If
                End If
            End If
        Next i
        AtLeastOneADL
    End If
    If ValidMHUnit(unitname) Then
        g = 0
        highest_is_on = False
        For i = MAX_INDICATORS To 1 Step -1
            If (mhgrps(i) > 0) Then
                If (mhgrps(i) <> g) Then
                    g = mhgrps(i)
                    highest_is_on = mhinds(i).checked
                Else
                    If highest_is_on Then
                        mhinds(i).checked = False
                    Else
                        highest_is_on = mhinds(i).checked
                    End If
                End If
            End If
        Next i
        mhAtLeastOneADL
    End If
    
    If unitname = "A7H" Then
        unitname = "A7T"
    ElseIf unitname = "CL2" Or unitname = "CL3" Or unitname = "CL4" Then
        unitname = "CL1"
    ElseIf unitname = "CV2" Then
        unitname = "CV1"
    ElseIf unitname = "CP2" Then
        unitname = "CP1"
    ElseIf unitname = "CPD" Then
        unitname = "CLD"
    End If
    
    'Add postfix letter onto acct num.
    acctnum = acctnum & Mid$(unitname, 1, 1)
    
    intime = Mid$(intime, 1, 12)
    
    outstr = Space$(9) & unitname  '10
    outstr = outstr & Space$(26 - Len(outstr)) '26
    outstr = outstr & unitname '27
    If telemetry Then
        outstr = outstr & Space$(43 - Len(outstr))
        outstr = outstr & "telemetry"
    End If
    outstr = outstr & Space$(60 - Len(outstr)) '60
    outstr = outstr & Mid$(intime, 1, 8) & Space$(1) '69
'    outstr = outstr & classdate & Space$(1)
    outstr = outstr & acctnum & Space$(20 - Len(acctnum) + 1) '90
    outstr = outstr & lastname & Space$(32 - Len(lastname) + 1) '123
    outstr = outstr & firstname & Space$(32 - Len(firstname) + 1) '156
    outstr = outstr & Space$(32 + 1) '189
    outstr = outstr & roomname & Space$(8 - Len(roomname) + 1) '198
    outstr = outstr & bedname & Space$(4 - Len(bedname) + 1) '20
    outstr = outstr & intime & Space$(1) 'classdatetime 216
'    outstr = outstr & classdate & classtime & Space$(1)
    outstr = outstr & Space$(78 + 1) '295
    outstr = outstr & intime & Space$(12 - Len(intime) + 1) '308
    outstr = outstr & Space$(70) '378
    
    If ValidMSUnit(unitname) Then
        For i = 1 To MAX_INDICATORS
            If (inds(i).checked) Then
                outstr = outstr & "Y"
            Else
                outstr = outstr & "N"
            End If
        Next i
    ElseIf ValidMHUnit(unitname) Then
        For i = 1 To MAX_INDICATORS
            If (mhinds(i).checked) Then
                outstr = outstr & "Y"
            Else
                outstr = outstr & "N"
            End If
        Next i
    End If
    
    Print #outfile, outstr
    Print #outlogfile, outstr
End Sub


Private Sub CalcFrequency(p As Integer)
    Dim n As Integer
    Dim r As Single
    
'    r = range / RANGE_DENOMINATOR
    If ValidMSUnit(unitname) Then
        dicary(p).act_freq = range / dicary(p).num_found 'denom will never be zero
    ElseIf ValidMHUnit(unitname) Then
        mhdicary(p).act_freq = range / mhdicary(p).num_found 'denom will never be zero
    End If
' Use 8 hours as basis
'    Select Case dicary(p).indwinpfs
'        Case 14 To 22 'indicators 14-22 are to be counted instead of freq.
'            If dicary(p).num_found >= 4 * r And dicary(p).num_found <= 12 * r Then
'                dicary(p).act_freq = 240
'            ElseIf dicary(p).num_found > 12 * r And dicary(p).num_found <= 24 * r Then
'                dicary(p).act_freq = 60
'            ElseIf dicary(p).num_found > 24 * r Then
'                dicary(p).act_freq = 30
'            Else
'                dicary(p).act_freq = 480
'            End If
'        Case Else   ' Other values.
'            dicary(p).act_freq = range / dicary(p).num_found 'denom will never be zero
'    End Select

End Sub

Private Function CerDateToISO(d As String) As String
    Dim century As String
    ' Yields a string of the format yyyymmddhhnn
    
    If val(Mid$(d, 7, 2)) <= 50 Then
        century = "20"
    Else
        century = "19"
    End If
    CerDateToISO = century & Mid$(d, 7, 2) & Mid$(d, 1, 2) & Mid$(d, 4, 2) _
     & Mid$(d, 10, 2) & Mid$(d, 13, 2)

End Function

Private Sub ResetDictionaryFields()
    Dim i As Integer
    
    For i = 1 To dicnum
        dicary(i).num_found = 0
        dicary(i).act_freq = 0
        dicary(i).last_datetime = ""
        dicary(i).gavefreq = False
        dicary(i).one_row_find = 0
    Next i

    For i = 1 To mhdicnum
        mhdicary(i).num_found = 0
        mhdicary(i).act_freq = 0
        mhdicary(i).last_datetime = ""
        mhdicary(i).gavefreq = False
        mhdicary(i).one_row_find = 0
    Next i
End Sub

Private Sub MakeLogFiles()
    Const HKEY_LOCAL_MACHINE = &H80000002
    Dim dt As Variant
    
    If Not g_util.GetKeyValue(HKEY_LOCAL_MACHINE, "SOFTWARE\Quadramed\WinPFS", "Directory", winpfspath) Then
        winpfspath = App.Path
    End If

    If g_util.DirExists(winpfspath) Then
        winlogpath = winpfspath & "\log"
        winloadpath = winpfspath & "\load_me"
        If Not g_util.DirExists(winpfspath) Then
            MkDir$ (winlogpath)
        End If
    Else
        winlogpath = App.Path
        winloadpath = winlogpath
    End If

' For Temporary Testing
'    winlogpath = "c:\tc\log"
'    winloadpath = "c:\tc\load_me"
    
    
    dt = nowdt
    inlogname = winlogpath & "\TranspIn_" & Format$(dt, "mmdd") & ".log"
    outlogname = winlogpath & "\TranspOut_" & Format$(dt, "mmdd") & ".log"
    dbugname = winlogpath & "\DbugTransp_" & Format$(dt, "mmdd") & ".log"
    
    outlogfile = FreeFile
    Open outlogname For Append As #outlogfile
    Print #outlogfile, "**** WinPFS Transparent Classification Output    Time=" & nowdt & " ****"
    
    If dbugon Then
        dbugfile = FreeFile
        Open dbugname For Append As #dbugfile
        Print #dbugfile, "****TRANSPARENT TRANSLATION DEBUGGING MODE    Time=" & nowdt & " ****"
    End If
    
End Sub

Private Sub CloseLogFiles()

    Close #outlogfile
    If dbugon Then
        Close #dbugfile
    End If

    'inlogfile's dt = mid$(inlogname,len(inlogname)-len("mmdd.txt")+1,len("mmddhh.txt"))
'    FileCopy outfn, winlogpath & "\TranspOut_" & Mid$(inlogname, Len(inlogname) - Len("mmddhh.txt") + 1, Len("mmddhh.txt"))

End Sub

Private Sub DeleteOldLogs()
    Dim temp As String
    Dim i As Integer
    Dim dt As Variant

    ' Delete old log files
    ' (allow the interface to be down for up to 5 days)
    dt = DateAdd("d", -TRANSP_INLOG_FILE_LIFE, nowdt)
    For i = 1 To 5
        temp = winlogpath & "\TranspIn_" & Format$(dt, "mmdd") & ".log"
        If g_util.FileExists(temp) Then
            Kill temp
        End If
        dt = DateAdd("d", -1, dt)
    Next i

    dt = DateAdd("d", -TRANSP_OUTLOG_FILE_LIFE, nowdt)
    For i = 1 To 5
        temp = winlogpath & "\TranspOut_" & Format$(dt, "mmdd") & ".log"
        If g_util.FileExists(temp) Then
            Kill temp
        End If
        dt = DateAdd("d", -1, dt)
    Next i
    
    dt = DateAdd("d", -TRANSP_DEBUG_FILE_LIFE, nowdt)
    For i = 1 To 5
        temp = winlogpath & "\DbugTransp_" & Format$(dt, "mmdd") & ".log"
        If g_util.FileExists(temp) Then
            Kill temp
        End If
        dt = DateAdd("d", -1, dt)
    Next i

End Sub


Private Sub dprint(s As String)
    Dim sstat As String

    If Not dbugon Or suppressDbugLog Then
        Exit Sub
    End If
    
    Print #dbugfile, s
'    ElseIf ind = -2 Then
'        Print #dbugfile, "The following event was Out of Range and was rejected."
'        Print #dbugfile, s
'    ElseIf ind = -3 Then
'        Print #dbugfile, "The following persistent event was before 7am and was rejected."
'        Print #dbugfile, s
'    ElseIf ind = 0 Then
'        Print #dbugfile, "    " & s
'    Else
'        If (status = 1) Or (status = -1) Then
'            sstat = "-TURNED ON-"
'        Else
'            sstat = "   "
'        End If
'        Print #dbugfile, "    Indicator " & ind & sstat & s
'    End If
            
End Sub
Private Function ValidMSUnit(u As String) As Boolean
    Dim i As Integer
    
    For i = 1 To unitnum
        If UCase$(u) = MSunitary(i) Then
            ValidMSUnit = True
            Exit For
        End If
    Next i

End Function
Private Function ValidMHUnit(u As String) As Boolean
    Dim i As Integer
    
    For i = 1 To mhunitnum
        If UCase$(u) = MHunitary(i) Then
            ValidMHUnit = True
            Exit For
        End If
    Next i

End Function


Private Sub Check1_2_3()
    Dim lslist As String
    
    dprint "Check1_2_3"
    If QLSRes(89444, "self") Then
        inds(1).checked = True
        self89444 = True
    End If
    If QLSRes(89444, "partial") Then inds(2).checked = True
    If QLSRes(89444, "complete") Then inds(3).checked = True
    
    If Not inds(3).checked Then
        lslist = "96747,96752,96753,96754,96755,96748,96757,96758,96749,96750,96751,92031"
        If QLSList(lslist) Then inds(3).checked = True
    End If
    
    If QLSRes(90117, "respds pain only") Then b90117 = True
    If QLSRes(90117, "paralyzed") Then b90117 = True
    If QLSRes(90117, "anesth") Then b90117 = True
    If QLSRes(90117, "no respon to pain") Then b90117 = True
    If QLSRes(90117, "unable to assess") Then b90117 = True
    If QLSRes(90117, "sedated") Then b90117 = True
    If b90117 Then inds(3).checked = True
    
End Sub


Private Sub Check4()
    Dim lslist As String
    
    dprint "Check4"
    If IsRehabUnit Then
        inds(4).checked = True
    Else
    
    If Not (inds(1).checked And Not (inds(2).checked Or inds(3).checked)) Then ' if not ind 1 selected
        lslist = "89447,89446,89445,89449,89448,20441,35725,36620,23230,35677,36604,22067"
        If QLSList(lslist) Then inds(4).checked = True
        
        If Not inds(4).checked Then
            lslist = "36438,36564,36256,36204,33073,35738,36913,37061"
            If QLSList(lslist) Then inds(4).checked = True
        End If
    End If
    End If 'if isrehab
'89447,89446,89445,89449,89448,20441,35725,36620,23230,35677,36604,22067
'36438,36564,36256,36204,33073,35738,36913,37061
'89438,89441,89435,89447,89449,89448,90746,89437 with x4
'89438,89441,89435,89447,89449,89448,90746,89437 with x2 or x3
'unique list 4,5,6
'89447,89446,89445,89449,89448,20441,35725,36620,23230,35677,36604,22067,
'36438,36564,36256,36204,33073,35738,36913,37061
'89438,89441,89435,90746,89437

End Sub

Private Sub Check5_6()
    Dim lslist As String

    dprint "Check5_6"
    lslist = "89438,89441,89435,89447,89449,89448,90746,89437"
    If QLSListRes(lslist, "x4") Then inds(6).checked = True
    
    If Not inds(6).checked Then
        lslist = "89438,89441,89435,89447,89449,89448,90746,89437"
        If QLSListRes(lslist, "x2") Then inds(5).checked = True
        If QLSListRes(lslist, "x3") Then inds(5).checked = True
        If QLSRes(94021, "yes") Then inds(5).checked = True
    End If

End Sub

Private Sub Check7()
    Dim lslist As String

    dprint "Check7"
    If QLSRes(92420, "4=") Then b92420 = True
    If QLSRes(92420, "5=") Then b92420 = True
    If QLSRes(92420, "6=") Then b92420 = True
    
    If QLSRes(95017, "patient") Then b95017 = True

    If QLSRes(95016, "family") Then b95016 = True
    If QLSRes(95016, "other") Then b95016 = True
    If QLSRes(95016, "friend") Then b95016 = True
    
    If Not ((b95017 And b90117) Or b92420 Or b95016) Then
        lslist = "95016,96067"
        If QLSListRes(lslist, "language") Then inds(7).checked = True
        If QLSListRes(lslist, "hearing") Then inds(7).checked = True
        If QLSListRes(lslist, "reading") Then inds(7).checked = True
        If QLSRes(96059, "yes") Then inds(7).checked = True
    End If
    
    If Not (b90117 Or b92420 Or inds(7).checked) Then
        lslist = "80582,24359,24363,24364,98509"
        If QLSListRes(lslist, "impaired") Then inds(7).checked = True
        If QLSListRes(lslist, "communication barr") Then inds(7).checked = True
    End If
        
    If Not (b90117 Or b92420 Or inds(7).checked) Then
        If QLSRes(90134, "aphasia") Then inds(7).checked = True
        If QLSRes(90134, "Dysarthria") Then inds(7).checked = True
        If QLSRes(90134, "Dysphagia") Then inds(7).checked = True
        If QLSRes(90134, "Slurred") Then inds(7).checked = True
        If QLSRes(90134, "Trach") Then inds(7).checked = True
        If QLSRes(90134, "ETT") Then inds(7).checked = True
    End If
    
End Sub

Private Sub Check8()
    Dim lslist As String
    Dim count As Integer
    Dim nacount As Integer

    dprint "Check8"
    If Not (b90117 Or b92420) Then
        lslist = "93043,93028,93029,93030,93031,93033,94984"
        If QLSList(lslist) Then inds(8).checked = True
        If QLSRes(34073, "alt ment") Then inds(8).checked = True
        If QLSCountValue(93027, " like '%bed alarm on%' and result like '%near nur station%' and result like '%toileting assist%'") > 0 Then
            ' inds(8).checked = True  removed per 5/19 mt changes
            inds(11).checked = True
        End If
        If QLSRes(80582, "slf destr behave") Or QLSRes(80582, "AMS") Then
            inds(8).checked = True
            inds(9).checked = True
        End If
        If QLSRes(93032, "cont harm") Then
            inds(8).checked = True
            inds(12).checked = True
        End If
        nacount = QLSResFreq(93022, "NA")   '93022 with NA
        count = QLSFreq("93022")  'all 93022
        If count > nacount Then ' if equal, then ALL are NA -- don't trigger if ALL NA.
            inds(8).checked = True
            inds(11).checked = True
        End If
    End If
    
    If Not ((b95017 And b90117) Or b92420) Then
        lslist = "95016,96067"
        If QLSListRes(lslist, "cognitive") Then inds(8).checked = True
    
        If QLSCountValue(94528, ">8") >= CountForPull(1) Then
            inds(8).checked = True
            inds(9).checked = True
            inds(11).checked = True
        End If
    
        If QLSRes(93032, "cont harm") Then inds(9).checked = True
    End If

    If Not (b90117 Or b92420 Or b95016 Or inds(8).checked) Then
        If QLSRes(90118, "Confused") Then inds(8).checked = True
        If QLSRes(90118, "obey simple") Then inds(8).checked = True
        If QLSRes(90118, "short term") Then inds(8).checked = True
    End If
    
End Sub

Private Sub Check9_10()
    Dim lslist As String
    Dim count As Integer

    dprint "Check9_10"
    If QLS(94816) Then b94816 = True
    If QLSRes(94815, "wnl except") Then b94815 = True
    
    If Not ((b95017 And b90117) Or b92420) Then
    If b94816 And b94815 Then
        If QLSRes(94819, "once a shift") Then inds(9).checked = True
        If QLSRes(94819, "every hr") Then inds(10).checked = True
    End If
    End If
    
    If QLSRes(93033, "initl nonviolent") Then b93033 = True
    
    If Not ((b95017 And b90117) Or b92420) Then
        If QLSRes(93033, "initial violent") Then
            inds(10).checked = True
            inds(12).checked = True
        End If
        
        If b93033 Then
            inds(9).checked = True
            inds(11).checked = True
        End If
    End If
    
    If QLSRes(94822, "every hr") Then
        If QLSRes(94821, "limit setting") Then inds(10).checked = True
        If QLSRes(94821, "encourage expression") Then inds(10).checked = True
        If QLSRes(94821, "active listen") Then inds(10).checked = True
    End If
    If QLSRes(94822, "once a shift") And Not inds(10).checked Then
        If QLSRes(94821, "limit setting") Then inds(9).checked = True
        If QLSRes(94821, "encourage expression") Then inds(9).checked = True
        If QLSRes(94821, "active listen") Then inds(9).checked = True
    End If
    
    If Not ((b95017 And b90117) Or b92420) Then
        If QLSRes(94824, "yes") Then
            inds(9).checked = True
            inds(11).checked = True
        End If
    End If
    
    If Not inds(10).checked Then
    
    If QLSRes(92241, "hostile") Then inds(9).checked = True
    If QLSRes(92241, "depressed") Then inds(9).checked = True
    If QLSRes(92241, "flat affect") Then inds(9).checked = True
    If QLSRes(91778, "family conf") Then inds(9).checked = True
    If QLSRes(91776, "yes") Then inds(9).checked = True

    If Not ((b95017 And b90117) Or b92420) Then
        If QLS(93029) Then
            inds(9).checked = True
            inds(11).checked = True
        End If
    End If
    
    If Not (b90117 Or b92420) Then
        If QLS(93030) Then
            inds(9).checked = True
            inds(11).checked = True
        End If
        
        If QLSRes(94984, "initl nonviolent") Then
            inds(9).checked = True
        End If
        If QLSRes(94984, "initial violent") Then
            inds(10).checked = True
        End If
        
        If QLSRes(98484, "Start") Or QLSRes(98484, "ongoing") Then
                If QLSRes(98484, "LPN") Or QLSRes(98484, "PCT") Or QLSRes(98484, "RN") Then
                    inds(12).checked = True
                End If
        End If
        
        If QLS(93031) Then
            inds(10).checked = True
            inds(12).checked = True
        End If
        

    End If
    
    If QLSRes(94816, "Start") Or QLSRes(94816, "ongoing") Then
            If QLSRes(94816, "LPN") Or QLSRes(94816, "PCT") Or QLSRes(94816, "RN") Then
                inds(10).checked = True
            End If
    End If

    End If 'not

    If Not ((b95017 And b90117) Or b92420) Then
        If QLSRes(94825, "yes curr") Then
            inds(10).checked = True
            inds(12).checked = True
        End If
        
        If QLSRes(93616, "Start") Or QLSRes(93616, "ongoing") Then
                If QLSRes(93616, "LPN") Or QLSRes(93616, "PCT") Or QLSRes(93616, "RN") Then
                    inds(10).checked = True
                    inds(12).checked = True
                End If
        End If
    End If
    
    count = QLSCountValue(94528, ">15")
    If count >= CountForPull(3) Then
        If Not ((b95017 And b90117) Or b92420) Then
            inds(10).checked = True
            inds(12).checked = True
        End If
        inds(16).checked = True
    End If
    
End Sub

Private Sub Check11_12()
    Dim lslist As String

    dprint "Check11_12"
    If Not (b90117 Or b92420) Then
        If QLS(95949) Then inds(11).checked = True
        If QLS(94984) Then inds(11).checked = True
        
        lslist = "24359,24363,24364"
        If QLSListRes(lslist, "slf destr") Then inds(11).checked = True
    End If
    
    If Not ((b95017 And b90117) Or b92420) And Not inds(11).checked Then
        If QLSRes(94818, "ciwa") Then inds(11).checked = True
    End If
    
    If Not (b90117 Or b92420) Then
        If Not inds(11).checked Then
            If QLSRes(90118, "Confused") Then inds(11).checked = True
            If QLSRes(90118, "obey simple") Then inds(11).checked = True
            If QLSRes(90118, "short term") Then inds(11).checked = True
        End If
    
        If QLSRes(95946, "Start") Or QLSRes(95946, "ongoing") Then
                If QLSRes(95946, "LPN") Or QLSRes(95946, "PCT") Or QLSRes(95946, "RN") Then
                    inds(12).checked = True
                End If
        End If
    End If
    
    If Not (b90117 Or b92420) And Not inds(11).checked Then
        If QLSCountValue(93027, " like '%bed alarm on%' and result like '%near nur station%' and result like '%toileting assist%'") > 0 Then
            If QLSRes(80582, "AMS") Or QLSRes(34073, "alt ment") Then '90118+confused already triggers this.
                inds(11).checked = True
            End If
        End If
    End If
    
End Sub

Private Sub Check13()
    Dim lslist As String
    
    dprint "Check13"
    lslist = "24360,98507"
    If QLSListRes(lslist, "airborne") Then inds(13).checked = True
    If QLSListRes(lslist, "contact") Then inds(13).checked = True
    If QLSListRes(lslist, "droplet") Then inds(13).checked = True
    If QLSListRes(lslist, "protective") Then inds(13).checked = True
End Sub

Private Sub CheckAssessments()
    Dim count As Integer
    Dim lslist As String
    
    
    dprint "CheckAssessments1"
    If QLS(43022) Then inds(17).checked = True
    If QLSRes(89081, "crrt") Then inds(17).checked = True
    If QLS(92030) Then inds(17).checked = True
    
    
    lslist = "91794,91647,89751,91738,91746,91754,91762,89848,94529,90194,91778,89788,91642,90182,89803,89794,91730"
    If QLSListRes(lslist, "1:1 RN=1hr") Or QLSListRes(lslist, "1:1 RN=2hr") Then
        inds(17).checked = True
    End If
    If Not inds(17).checked Then
        If QLSListRes(lslist, "1:1 RN>1hr") Or QLSListRes(lslist, "1:1 RN>2hr") Then
            inds(17).checked = True
        End If
    End If
    
    dprint "CheckAssessments2"
    If QLSFreq("91794") >= 28 Then inds(17).checked = True
    
    If QLSRes(91763, "30 min") Then
        'inds(17).checked = True
    ElseIf QLSRes(91763, "q 1") Then
        inds(16).checked = True
    ElseIf QLSRes(91763, "q 2") Then
        inds(15).checked = True
    ElseIf QLSRes(91763, "q 4") Then
        inds(14).checked = True
    End If
    
    If QLSRes(91119, "cath lab") Then
        inds(16).checked = True
        inds(19).checked = True
    End If
    
    
    dprint "CheckAssessments3"
    If Not (inds(17).checked Or inds(16).checked) Then
    If QLSRes(89801, "done") Then inds(16).checked = True
    
    count = QLSResFreq(90194, "drain") + QLSResFreq(90194, "drsg") + QLSResFreq(90194, "discontinue")
    If count >= 5 Then inds(14).checked = True
    If count >= 7 Then inds(15).checked = True
    If count >= 16 Then inds(16).checked = True
    
    count = QLSResFreq(90182, "Neuro") + QLSResFreq(90182, "room dark") + QLSResFreq(90182, "seizure") + _
       QLSResFreq(90182, "24 hr EEG") + QLSResFreq(90182, "teaching") + QLSResFreq(90182, "MD notified")
    If count >= 5 Then inds(14).checked = True
    If count >= 7 Then inds(15).checked = True
    If count >= 16 Then inds(16).checked = True
    
    dprint "CheckAssessments4"
    If Not inds(16).checked Then
        count = QLSCountValue(94528, ">=8 and result<=15")
        If count >= CountForPull(2) Then
            inds(15).checked = True
        End If
    End If
    If Not (inds(16).checked Or inds(15).checked) Then
        count = QLSCountValue(94528, ">=0 and result<=7")
        If count >= CountForPull(1) Then
            inds(14).checked = True
        End If
    End If
    
    End If 'not 17
    
'    lslist = "6879,6880,6881,6891,9993,9994,89751,89794,89808,89813,89817,89822,89823,89828,89833,89834,89839,"
'    lslist = lslist & "89844,89845,89848,90116,90118,90120,90124,90134,90182,90192,90194,90327,90328,90334,91619,91620,"
'    lslist = lslist & "91642,91721,91730,91745,91753,91794,92229,92233,92234,92237,92243,94536,96232,96233,96244,96724"
'    If QLSListFreq(lslist, 28) Then
'        inds(17).checked = True
'    ElseIf QLSListFreq(lslist, 16) Then
'        inds(16).checked = True
'    ElseIf QLSListFreq(lslist, 7) Then
'        inds(15).checked = True
'    ElseIf QLSListFreq(lslist, 5) Then
'        inds(14).checked = True
'    End If
    dprint "CheckAssessments5"
    If Not inds(17).checked Then
    dprint "CheckAssessments ListFreq17"
    lslist = "6879,6880,6881,6891,9993,9994,89751,89788,89794,89808,89812,89813,89817,89822,89823,89828,89833,89834,89839,"
    lslist = lslist & "89844,89845,89848,90116,90117,90118,90119,90120,90123,90124,90134,90192,90327,90328,90334,91619,91620,91633,"
    lslist = lslist & "91635,91642,91647,91721,91730,91737,91745,91753,91761,91794,92229,92233,92234,92237,92243,92421,92424,92428,"
    lslist = lslist & "92526,92535,92541,94536,96232,96233,96244,96245,96724"
    If QLSListFreq(lslist, CountForPull(4)) Then inds(17).checked = True
    End If


    If Not (inds(17).checked Or inds(16).checked) Then
    dprint "CheckAssessments ListFreq16"
    lslist = "6879,6880,6881,6891,9993,9994,89025,89029,89039,89040,89051,89052,89061,89071,89746,89747,89749,89750,89751,"
    lslist = lslist & "89754,89788,89794,89808,89812,89813,89817,89822,89823,89828,89829,89833,89834,89839,89844,89845,89848,90116,"
    lslist = lslist & "90117,90118,90119,90120,90123,90124,90132,90134,90155,90157,90159,90161,90171,90172,90173,90174,90175,90192,"
    lslist = lslist & "90327,90328,90333,90334,91619,91620,91621,91622,91623,91624,91625,91633,91635,91636,91642,91647,91721,91730,"
    lslist = lslist & "91734,91737,91742,91745,91750,91753,91758,91761,91781,91787,91793,91794,92229,92233,92234,92237,92243,92421,"
    lslist = lslist & "92423,92428,92526,92535,92541,93629,93630,93631,94536,96232,96233,96244,96245,96724"
    If QLSListFreq(lslist, CountForPull(3)) Then inds(16).checked = True
    End If


    If Not (inds(17).checked Or inds(16).checked Or inds(15).checked) Then
    dprint "CheckAssessments ListFreq15"
    lslist = "6881,89025,89039,89040,89746,89747,89749,89750,89751,89752,89753,89828,89833,89834,89839,89844,89845,89848,90116,"
    lslist = lslist & "90117,90120,90124,90132,90157,90161,90172,90173,90174,90192,90333,90334,91619,91620,91621,91625,91633,91635,91636,"
    lslist = lslist & "91637,91647,91721,91734,91737,91742,91745,91750,91753,91758,91761,91781,91782,91783,91784,91785,91786,91787,91793,"
    lslist = lslist & "91794,91801,91808,91915,91922,92229,92233,92237,92243,92526,92535,92541,93041,93627,93629,93630,93631,93632,93633,"
    lslist = lslist & "93725,93735,94536,96244,96245,96713,96724,"
    lslist = lslist & "6879,6880,6891,89051,89052,89061,89071,89754,89788,89808,89812,89813,89817,89822,89823,90118,90119,90123,90155,"
    lslist = lslist & "90159,90171,90175,90327,90328,91622,91623,91624,91639,91642,91928,92421,92422,92426,92428,93733,96232,96233,"
    lslist = lslist & "96715,96716,96719,96720,96723,"
    lslist = lslist & "9993,9994,89029,89762,89794,90134,91730,92234,96714,96718,96721"
    If QLSListFreq(lslist, CountForPull(2)) Then inds(15).checked = True
    End If
    
    
    If Not (inds(17).checked Or inds(16).checked Or inds(15).checked Or inds(14).checked) Then
    dprint "CheckAssessments ListFreq14"
    lslist = "91787,91794,91620,91647,91621,96724,94536,93631,93630,93629,92233,91782,91786,91784,91785,91783,6881,92227,"
    lslist = lslist & "91637,90174,89750,89749,89751,89747,91737,91734,91745,91742,91753,91750,91761,91758,90192,91793,91721,89025,"
    lslist = lslist & "89746,90116,91619,91781,93627,93725,90132,92229,89039,89040,96713,90173,90128,91801,91808,89752,93633,93632,"
    lslist = lslist & "89848,89753,89826,89829,90157,93731,89825,89827,89828,90172,89845,96244,96245,89842,89846,89841,89843,90333,"
    lslist = lslist & "89831,89835,89830,89832,89837,89840,90161,93735,89836,89838,90334,89839,89844,90124,90120,89833,89834,92051,"
    lslist = lslist & "91915,91922,91636,90117,92237,92243,91625,"
    lslist = lslist & "90175,96723,89052,89051,6891,96715,92426,90118,89061,89071,89788,89763,92421,6879,89806,"
    lslist = lslist & "89809,90155,93729,89805,89807,89808,90171,89823,96232,96233,89820,89824,89819,89821,89810,"
    lslist = lslist & "89814,89916,89811,89815,89818,90159,93733,89915,89816,90327,90328,89817,89822,90123,90119,"
    lslist = lslist & "89812,89813,91928,96716,96719,6880,96720,89754,91624,91623,91622,91639,91642,89794,96721,"
    lslist = lslist & "96718,90134,96718,91790,96714,89762,9993,9994,91730,92221,89029,92234,92222,92053"
    If QLSListFreq(lslist, CountForPull(1)) Then inds(14).checked = True 'if freq of any item in this list >= 5 then
    End If

    dprint "END CheckAssessments END"

End Sub

Private Sub Check18()

    If QLSRes(91763, "med prep >20 min") Then inds(18).checked = True
    
End Sub

Private Sub Check19_20()
    Dim lslist As String
    
    dprint "Check19_20"

    lslist = "92233,92237,92243,92234"
    If QLSListFreq(lslist, CountForPull(4)) Then inds(20).checked = True 'if freq of any item in this list >=c
    
    lslist = "89423,89522,89531,89540,89549,89558,89566,89576,94537"
    If QLSListRes(lslist, "1:1 RN=1hr") Or QLSListRes(lslist, "1:1 RN=2hr") Then
        inds(20).checked = True
    End If
    If Not inds(20).checked Then
        If QLSListRes(lslist, "1:1 RN>1hr") Or QLSListRes(lslist, "1:1 RN>2hr") Then
            inds(20).checked = True
        End If
    End If

    If Not inds(20).checked Then
    lslist = "92655,89063,89073,96243,89323,89334,89344,89356,89367,89378,89389,89400"
    If QLSListRes(lslist, ">1hr cont") Or QLSListRes(lslist, ">30min cont") Then inds(20).checked = True
    End If
    
    If Not inds(20).checked Then
    If QLSRes(89801, "done") Then
        If QLSRes(89803, "1:1 RN=1hr") Or QLSRes(89803, "1:1 RN=2hr") Then inds(20).checked = True
        If QLSRes(89803, "1:1 RN>1hr") Or QLSRes(89803, "1:1 RN>2hr") Then inds(20).checked = True
    End If
    End If


If Not inds(20).checked Then
    lslist = "90560,90570,92233,90891,92041,89423,89522,89531,89540,89549,89558,89566,89576,90192,94538,90877,96244,"
    lslist = lslist & "96245,92233,92233,92243,90543,90838,90851,90863,96232,96233,89804,89794,96740,92219,89323,89334,89344,"
    lslist = lslist & "89356,89367,89378,89389,89400"
    If QLSList(lslist) Then inds(19).checked = True
    
    If Not inds(19).checked Then
        lslist = "89120,89121,89122,89326,89327,89328,89345,89337,89338,89348,89349,89350,89359,89360,89361,89370,"
        lslist = lslist & "89371,89372,89381,89382,89383,89392,89393,89394"
        If QLSList(lslist) Then inds(19).checked = True
    End If
    
    If Not inds(19).checked Then
        lslist = "91738,91746,91754,91762"
        If QLSListRes(lslist, "drsg") Or QLSListRes(lslist, "discontinue") Then
            inds(19).checked = True
        End If
    
        If QLSListRes("93761,93759", "pin care") Then inds(19).checked = True
        If QLSRes(89081, "Drsg") Or QLSRes(89081, "Press") Then inds(19).checked = True
        If QLSListRes("91635,89054,89048", "site") Then inds(19).checked = True
        If QLSRes(89048, "changed") Then inds(19).checked = True
        
        lslist = "91915,91922,90194,89795"
        If QLSListRes(lslist, "drsg") Then inds(19).checked = True
        If QLSRes(91730, "cannula") Or QLSRes(91730, "trach") Then inds(19).checked = True
        
        If Not inds(19).checked Then
        lslist = "89319,89334,89333,89344,89343,89356,89355,89367,89366,89378,89377,89389,89388,89400,89399"
        If QLSListRes(lslist, "drsg") Then inds(19).checked = True
        If QLSListRes(lslist, "open to air") Then inds(19).checked = True
        If QLSListRes(lslist, "press") Then inds(19).checked = True
        If QLSListRes(lslist, "staples remove") Then inds(19).checked = True
        If QLSListRes(lslist, "steri strip") Then inds(19).checked = True
        If QLSListRes(lslist, "other") Then inds(19).checked = True
        End If
    End If 'not 19
End If 'not 20

End Sub

Private Sub Check21()
    Dim mins As Integer

    dprint "Check21"

    If Not (b90117 Or b92420 Or b95017 Or b95016) Then

'    If QLSRes(94983, "15 min") Or QLSRes(94983, "30 min") Or _
'       QLSRes(94983, "45 min") Or QLSRes(94983, "60 min") Then inds(21).checked = True
    mins = 61 * QLSResFreq("94983", "> 1 h")
    If mins >= 60 Then
        inds(21).checked = True
    Else
        mins = mins + 60 * QLSResFreq("94983", "60 min")
        If mins >= 60 Then
            inds(21).checked = True
        Else
            mins = mins + 45 * QLSResFreq("94983", "45 min")
            If mins >= 60 Then
                inds(21).checked = True
            Else
                mins = mins + 30 * QLSResFreq("94983", "30 min")
                If mins >= 60 Then
                    inds(21).checked = True
                Else
                    mins = mins + 15 * QLSResFreq("94983", "15 min")
                    If mins >= 60 Then inds(21).checked = True
                End If
            End If
        End If
    End If
            
' The queries above are more efficient than this:
'    If 15 * QLSresFreq("94983", "15 min") + 30 * QLSresFreq("94983", "30 min") + _
'       45 * QLSresFreq("94983", "45 min") + 60 * QLSresFreq("94983", "60 min") + 61 * QLSresFreq("94983", "> 1 hour") > 60 Then
'       inds(21).checked = True
'    End If
       
    End If
    
End Sub

Private Sub Check22()
    Dim lslist As String
    
    dprint "Check22"
    If QLS(92031) Then inds(22).checked = True
    
    If Not inds(22).checked Then
    lslist = "91794,91647,95981,89751,91738,91746,91754,91762,95971,96262,96269,96276,96283,89032,94829,"
    lslist = lslist & "89848,94537,94529,90194,89788,96118,91642,90182,89803,96084,91730"
    If QLSListRes(lslist, "2hr") Then inds(22).checked = True
    End If
    
End Sub

Private Sub CheckCustom()
    Dim lslist As String
    
    dprint "CheckCustom"
    If QLS(90184) Then inds(24).checked = True

    lslist = "93043,93033"
    If QLSList(lslist) Then inds(33).checked = True
    
    If QLSRes(93034, "left") Then inds(33).checked = True
    If QLSRes(93034, "right") Then inds(33).checked = True
    If QLSRes(93034, "wrist") Then inds(33).checked = True
    If QLSRes(93034, "ankle") Then inds(33).checked = True
    If QLSRes(93034, "mitten") Then inds(33).checked = True
    If QLSRes(93034, "leather") Then inds(33).checked = True
    If QLSRes(93034, "belt") Then inds(33).checked = True
    If QLSRes(93034, "vest") Then inds(33).checked = True
    If QLSRes(93034, "enclosure") Then inds(33).checked = True

    If QLSListRes("90893,92044", "multi lumen cath") Then inds(35).checked = True
    
    If QLSList("90838,90841,90844,90851,90853,90856") Then inds(37).checked = True

    If QLSList("90543,90545,90547") Then inds(38).checked = True

    If QLSRes(89075, "see iv") Then inds(39).checked = True
    If QLSRes(33497, "cath") Then inds(39).checked = True
    If QLSRes(90882, "tunel") Or QLSRes(90882, "tunnel") Then inds(39).checked = True
    
    
    CheckVentilator
    
    If QLSList("6933") Then inds(44).checked = True
    If Not inds(44).checked Then
        If Not QLSList("89033,89034,89035") Then
            If QLSList("89039,89040,89041") Then inds(44).checked = True
        End If
    End If
    
    If QLSListRes("90893,92044", "hickman") Then inds(46).checked = True
    
    If QLSList("90863,90865,90872") Then inds(47).checked = True
    
    If QLSListRes("90893,92044", "cordis") Then inds(48).checked = True

    If QLSListRes("90893,92044", "groshong") Then inds(49).checked = True

End Sub

Private Sub CheckVentilator()

    dprint "CheckVent"
    If QLS(97015) Then inds(42).checked = True
    If QLSRes(91646, "ET tube") Then inds(42).checked = True
    If QLSRes(91721, "intubated") Then inds(42).checked = True
    
    If Not inds(42).checked Then
    If QLSList("86125,86131,86134,91717,91718,91719,97005,97011,97014") Then inds(42).checked = True
    End If
    
    If QLS(97012) Then
    If QLSList("91723,91725,97009") Then inds(42).checked = True
    End If
    
'    If Not inds(42).checked Then
'        If QLSRes(96995, "pap") Or QLSRes(96995, "vent") Then
'            If QLS(91723) Then inds(42).checked = True
'        End If
'    End If

End Sub

Private Function IsSpecialICU() As Boolean
    
    Select Case UCase$(unitname)
        Case "A7I", "A5T", "A2T", "A5B", "C2I", "D2A", "D2B"
            IsSpecialICU = True
    End Select

End Function

Private Function IsRehabUnit() As Boolean
    
    Select Case UCase$(unitname)
        Case "A8P", "A3I", "A3W"
            IsRehabUnit = True
    End Select

End Function


Private Function CountForPull(sched As Integer) As Integer
    Dim c As Integer

    dprint "CountForPull" & sched
    If range >= 1440 Then '24 hrs
        Select Case sched
            Case 1: c = 5  'A
            Case 2: c = 7  'B
            Case 3: c = 16 'C
            Case 4:        'D
                c = 28
                If IsSpecialICU Then
                    c = 48
                End If
        End Select
    ElseIf range >= 720 Then '12 hrs
        Select Case sched
            Case 1: c = 3  'A
            Case 2: c = 4  'B
            Case 3: c = 8  'C
            Case 4:        'D
                c = 14
                If IsSpecialICU Then
                    c = 26
                End If
        End Select
    ElseIf range >= 480 Then '8 hrs
        Select Case sched
            Case 1: c = 2  'A
            Case 2: c = 3  'B
            Case 3: c = 6  'C
            Case 4:        'D
                c = 12
                If IsSpecialICU Then
                    c = 20
                End If
        End Select
    End If
    
    CountForPull = c

End Function

Private Sub DeleteOldPersistData()
    Dim perfn As String
    Dim perfile As Integer
    Dim newperfn As String
    Dim newperfile As Integer
    Dim buf As String
    Dim mins As Single
    Dim pdt As String
    Dim acct As String

    
'    perfn = App.Path & "\" & PERDATA_FNAME          'Persist.dat
'    If g_util.FileExists(perfn) Then
'
'    perfile = FreeFile
'    Open perfn For Input As #perfile 'this file needs to exist before it can be opened
'
'    newperfn = App.Path & "\new" & PERDATA_FNAME
'    newperfile = FreeFile
'    Open newperfn For Append As #newperfile
'
'    If intime = "" Then intime = Format(Now, "yyyymmddhhnn")
'    While Not EOF(perfile)
'        Line Input #perfile, buf
'        pdt = Trim$(Mid$(buf, 1, LEN_DT)) 'this is the persist data line time
'        mins = DateDiff("n", DateSerial(Mid$(pdt, 1, 4), Mid$(pdt, 5, 2), Mid$(pdt, 7, 2)) + _
'                        TimeSerial(Mid$(pdt, 9, 2), Mid$(pdt, 11, 2), "00"), _
'                        DateSerial(Mid$(intime, 1, 4), Mid$(intime, 5, 2), Mid$(intime, 7, 2)) + _
'                        TimeSerial(Mid$(intime, 9, 2), Mid$(intime, 11, 2), "00"))
'        'compare to intime to determin 24 hour diff
'        'if buf's time is 24hrs+ old, then skip it
'        'else
'        If mins <= 1440 Then
'            Print #newperfile, buf
'            acct = Trim$(Mid$(buf, LEN_DT + 1, LEN_ACCT_NUM))
'            If Not AcctNumInPersistFile(acct) Then
'                numacct = numacct + 1
'                PublishAcctNum acct
'            End If
'        End If
'    Wend
'    Close #perfile
'    Close #newperfile
'    Kill perfn
'    Name newperfn As perfn
'
'    Else
'        dprint 0, -1, PERDATA_FNAME & " not found."
'    End If
    
End Sub
Private Function AcctNumInPersistFile(ByRef a As String) As Boolean
    ' only the acctnum array; file is not necessarily open
    Dim i As Single
    Dim r As String
    
'    AcctNumInPersistFile = False
'    For i = 1 To numacct
'        If a = acctnumdirectory(i).a Then
'            AcctNumInPersistFile = True
'            Exit For
'        End If
'    Next i
End Function

Private Function DidProcessPersist(ByRef a As String) As Boolean
    ' only the acctnum array; file is not necessarily open
    Dim i As Single
    Dim r As String
    
'    DidProcessPersist = False
'    For i = 1 To numacct
'        If a = acctnumdirectory(i).a Then
'            DidProcessPersist = acctnumdirectory(i).already_did_persist
'            Exit For
'        End If
'    Next i
End Function

Private Sub SetAlreadyDidPersist(ByRef a As String)
    ' only the acctnum array; file is not necessarily open
    Dim i As Single
    Dim r As String
    
'    For i = 1 To numacct
'        If a = acctnumdirectory(i).a Then
'            acctnumdirectory(i).already_did_persist = True
'            Exit For
'        End If
'    Next i
End Sub

Private Sub PublishAcctNum(ByRef a As String)
'    ReDim Preserve acctnumdirectory(0 To numacct)
'    acctnumdirectory(numacct).a = a
End Sub

Private Sub AddPersistLine(ByRef b As String)
    Dim pdt As String
    Dim perfn As String
    Dim perfile As Integer
    
'    'parse out event time
'    pdt = Trim$(Mid$(b, START_EVENT_DT, LEN_DT))
'    pdt = pdt & Space(LEN_DT - Len(pdt)) & acctnum & Space(LEN_ACCT_NUM - Len(acctnum)) & b
'
'    perfn = App.Path & "\" & PERDATA_FNAME          'Persist.dat
'    perfile = FreeFile
'    Open perfn For Append As #perfile
'
'    Print #perfile, pdt
'
'    Close #perfile
    
End Sub

Private Sub GetNextPersistItemForThisAcctNum(ByRef pidx As Single, ByRef b As String) 'return the event string b
    Dim perfn As String
    Dim perfile As Integer
    Dim ppos As Single
    Dim a As String
    
'    perfn = App.Path & "\" & PERDATA_FNAME          'Persist.dat
'    perfile = FreeFile
'    Open perfn For Input As #perfile
'
'    ppos = 0
'    While Not EOF(perfile) And ppos < pidx
'        Line Input #perfile, b
'        ' parse off the acct num from b and assign a to it.
'        a = Trim$(Mid$(b, LEN_DT + 1, LEN_ACCT_NUM))
'        If a = acctnum Then
'            ppos = ppos + 1
'            If ppos = pidx Then
'                b = Mid$(b, LEN_DT + LEN_ACCT_NUM + 1, START_RESULT + LEN_RESULT) 'start from after acctnum for entire length of event line
'            End If
'        End If
'        If EOF(perfile) Then
'            b = ""
'            SetAlreadyDidPersist (acctnum)
'        End If
'    Wend
'
'    Close #perfile

End Sub


Private Sub MSGlobalTurnOffs()
    Dim charting As String
    Dim i, i1, i2 As Integer
    
'    charting = "4="
'    i1 = DicIndex("35341", charting)
'    charting = "6="
'    i2 = DicIndex("35341", charting)
'
'    For i = i1 To i2
'        If dicary(i).num_found > 0 Then
'             inds(6).checked = False
'             inds(7).checked = False
'             inds(8).checked = False
'             inds(9).checked = False
'             inds(27).checked = False
'             inds(28).checked = False
'             inds(29).checked = False
'             dprint 6, 1, "Turned off 6,7,8,9,27,28,29 by 35341 4=,5=,6="
'        End If
'    Next i

End Sub

Private Function QLS(ls As Long) As Boolean
    Dim rs As New Recordset
    Dim sql As String
    
    dprint "QLS"
'  select * from tc_data where patient_id=x and patresultslabelseq='123456'
    sql = "select * from " & DBTABLENAME & " where acctnum='" & acctnum & "' and labseq=" & CStr(ls)
    rs.Open sql, g_cnADO
    QLS = (rs.RecordCount > 0)
    rs.Close
    Set rs = Nothing
'    dprint "end QLabelSeqs"
End Function
Private Function QLSRes(ls As Long, res As String) As Boolean
    Dim rs As New Recordset
    Dim sql As String
    
    dprint "QLSres"
'  select * from tc_data where patient_id=x and result_value like '%sa2%' and patresultslabelseq='123456'
    sql = "select * from " & DBTABLENAME & " where acctnum='" & acctnum & "' and labseq=" & CStr(ls) & " and result like " & "'%" & res & "%'"
    rs.Open sql, g_cnADO
    QLSRes = (rs.RecordCount > 0)
    rs.Close

    Set rs = Nothing
'    dprint "end QLegLabs"
End Function
Private Function QLSList(lslist As String) As Boolean
    Dim rs As New Recordset
    Dim sql As String
    
    dprint "QLSlist"
'  select * from tc_data where patient_id=x and patresultslabelseq='123456'
    sql = "select * from " & DBTABLENAME & " where acctnum='" & acctnum & "' and labseq in (" & lslist & ")"
    rs.Open sql, g_cnADO
    QLSList = (rs.RecordCount > 0)
    rs.Close
    Set rs = Nothing
'    dprint "end QLabelSeqs"
End Function
Private Function QLSListRes(lslist As String, res As String) As Boolean
    Dim rs As New Recordset
    Dim sql As String
    
    dprint "QLSlistres"
'  select * from tc_data where patient_id=x and patresultslabelseq='123456'
    sql = "select * from " & DBTABLENAME & " where acctnum='" & acctnum & "' and labseq in (" & lslist & ") and result like " & "'%" & res & "%'"
    rs.Open sql, g_cnADO
    QLSListRes = (rs.RecordCount > 0)
    rs.Close
    Set rs = Nothing
'    dprint "end QLabelSeqs"
End Function
Private Function QLSCountValue(ls As Long, r As String) As Integer
    Dim rs As New Recordset
    Dim sql As String
    
    dprint "QLSValue"
' select * from tc_data where patient_id=x and patresultslabelseq='123456'
    sql = "select count(distinct(perftime+result)) from " & DBTABLENAME & " where acctnum='" & acctnum & "' and labseq=" & CStr(ls) & " and result" & r
    rs.Open sql, g_cnADO
    If rs.RecordCount > 0 Then
        QLSCountValue = CInt(rs(0))
    Else
        QLSCountValue = 0
    End If
    rs.Close
    Set rs = Nothing
'    dprint "end QLabelSeqs"
End Function

Private Function QLSResFreq(ls As Long, r As String) As Integer
    Dim rs As New Recordset
    Dim sql As String
    
    QLSResFreq = 0
'  select * from tc_data where patient_id=x and patresultslabelseq='123456'
    sql = "select count(distinct(perftime+result)) from " & DBTABLENAME & " where acctnum='" & acctnum & "' and labseq=" & CStr(ls) & " and result like " & "'%" & r & "%'"
    rs.Open sql, g_cnADO
    If rs.RecordCount > 0 Then QLSResFreq = rs(0)
    rs.Close
    Set rs = Nothing
'    dprint "end QLabelSeqs"

End Function
Private Function QLSFreq(ls As String) As Integer
    Dim rs As New Recordset
    Dim sql As String
    
    QLSFreq = 0
'  select * from tc_data where patient_id=x and patresultslabelseq='123456'
    sql = "select count(distinct(perftime+result)) from " & DBTABLENAME & " where acctnum='" & acctnum & "' and labseq=" & ls
    rs.Open sql, g_cnADO
    If rs.RecordCount > 0 Then
        QLSFreq = rs(0)
    End If
    rs.Close
    Set rs = Nothing
'    dprint "end QLabelSeqs"

End Function

Private Function QLSListFreq(lslist As String, freq As Integer) As Boolean
    Dim ls() As String
    Dim maxls As Integer
    Dim i As Integer
    
    QLSListFreq = False
    ls() = Split(lslist, ",")
    maxls = UBound(ls)
    For i = 0 To maxls
        If QLSFreq(ls(i)) >= freq Then
            QLSListFreq = True
            Exit For
            'i = maxls + 1
        End If
    Next i

End Function


Private Function QLSListCount(lslist As String) As Integer
    Dim ls() As String
    Dim maxls As Integer
    Dim i As Integer
    Dim c As Integer
    
    c = 0
    ls() = Split(lslist, ",")
    maxls = UBound(ls)
    For i = 0 To maxls
        c = c + QLSFreq(ls(i))
    Next i
    QLSListCount = c

End Function

Private Sub hCheck1to10()
    Dim lslist As String
    
    dprint "hCheck1to10"
    If QLSRes(89444, "self") Then
        mhinds(1).checked = True
        self89444 = True
    End If
    If QLSRes(89444, "partial") Then mhinds(2).checked = True
    If QLSRes(89444, "complete") Then mhinds(3).checked = True
    
    If QLS(92031) Then
        mhinds(3).checked = True
        mhinds(15).checked = True
        mhinds(16).checked = True
        mhinds(17).checked = True
        mhinds(18).checked = True
    End If
    If QLSRes(90117, "respds pain only") Then b90117 = True
    If QLSRes(90117, "paralyzed") Then b90117 = True
    If QLSRes(90117, "anesth") Then b90117 = True
    If QLSRes(90117, "no respon to pain") Then b90117 = True
    If QLSRes(90117, "unable to assess") Then b90117 = True
    If QLSRes(90117, "sedated") Then b90117 = True
    If b90117 Then mhinds(3).checked = True
    
    If Not (mhinds(1).checked And Not (mhinds(2).checked Or mhinds(3).checked)) Then
        If QLSList("89445,89446,89447,89448,89449") Then mhinds(4).checked = True
        If Not mhinds(4).checked Then
            lslist = "36438,36564,36256,36204,33073,35738,36913,37061"
            If QLSList(lslist) Then mhinds(4).checked = True
        End If
    End If
    
    If mhinds(4).checked And mhinds(3).checked Then mhinds(4).checked = False

    If Not mhinds(5).checked Then
        lslist = "89438,89441,89435,89447,89449,89448,90746,89437"
        If QLSListRes(lslist, "x2") Then mhinds(5).checked = True
        If QLSListRes(lslist, "x3") Then mhinds(5).checked = True
        If QLSListRes(lslist, "x4") Then mhinds(5).checked = True
        If QLSRes(94021, "yes") Then mhinds(5).checked = True
    End If
    
    If QLSCountValue(94024, "<16") >= 1 Then mhinds(6).checked = True
    
    If QLS(97811) Then mhinds(7).checked = True
    
    lslist = "24359,24363,24364"
    If QLSListRes(lslist, "impaired") Then mhinds(8).checked = True
    If QLSListRes(lslist, "communication barr") Then mhinds(8).checked = True
    
    If QLSRes(92420, "4=") Then b92420 = True
    If QLSRes(92420, "5=") Then b92420 = True
    If QLSRes(92420, "6=") Then b92420 = True
    
    If Not (b90117 Or b92420 Or mhinds(8).checked) Then
        If QLSRes(90134, "aphasia") Then mhinds(8).checked = True
        If QLSRes(90134, "Dysarthria") Then mhinds(8).checked = True
        If QLSRes(90134, "Dysphagia") Then mhinds(8).checked = True
        If QLSRes(90134, "Slurred") Then mhinds(8).checked = True
        If QLSRes(90134, "Trach") Then mhinds(8).checked = True
        If QLSRes(90134, "ETT") Then mhinds(8).checked = True
        
        
        If Not mhinds(8).checked Then
            If QLSRes(95016, "language") Then mhinds(8).checked = True
            If QLSRes(95016, "hearing") Then mhinds(8).checked = True
            If QLSRes(95016, "reading") Then mhinds(8).checked = True
        End If
    End If
    
    If QLSRes(96059, "yes") Then mhinds(8).checked = True
    If QLSRes(96067, "language") Then mhinds(8).checked = True
    If QLSRes(96067, "hearing") Then mhinds(8).checked = True
    If QLSRes(96067, "reading") Then mhinds(8).checked = True

    If QLSRes(98484, "Start") Or QLSRes(98484, "ongoing") Then
            If QLSRes(98484, "LPN") Or QLSRes(98484, "PCT") Or QLSRes(98484, "RN") Then
                mhinds(10).checked = True
                mhinds(13).checked = True
            End If
    End If
    
    If QLSCountValue(94528, ">12") >= 4 Then mhinds(10).checked = True
    
    If Not mhinds(10).checked Then
        If QLSRes(94864, "every hr") Or QLSRes(94864, "1:1") Then
            If QLS(94818) Then mhinds(10).checked = True
        End If
    
        If QLSRes(94827, "Start") Or QLSRes(94827, "ongoing") Then
                If QLSRes(94827, "LPN") Or QLSRes(94827, "PCT") Or QLSRes(94827, "RN") Then mhinds(10).checked = True
        End If
    End If 'not10
    
    If Not mhinds(10).checked Then
    If QLSRes(94857, "auditory") Or QLSRes(94857, "visual") Or QLSRes(94857, "tactile") Or QLSRes(94857, "other") Then
        mhinds(10).checked = True
        mhinds(11).checked = True
    End If
    End If 'not10
    
    If Not (mhinds(11).checked Or mhinds(10).checked) Then
    
    If QLSRes(34073, "alt ment") Then mhinds(9).checked = True
    'If QLSRes(93027, "bed alarm on") And QLSRes(93027, "near nur station") And QLSRes(93027, "toileting assist") Then
    If QLSCountValue(93027, " like '%bed alarm on%' and result like '%near nur station%' and result like '%toileting assist%'") > 0 Then
        mhinds(9).checked = True
    End If

    If QLSRes(90118, "Confused") Then mhinds(9).checked = True
    If QLSRes(90118, "obey simple") Then mhinds(9).checked = True
    If QLSRes(90118, "short term") Then mhinds(9).checked = True
    
    If Not mhinds(9).checked Then
    If QLSList("93028,94856") Then mhinds(9).checked = True
    If QLSCountValue(94528, ">8") >= CountForPull(1) Then mhinds(9).checked = True
    If Not (b90117 Or b92420) Then
        If QLSListRes("95016,96067", "cognitive") Then mhinds(9).checked = True
    End If
    End If 'not9
    End If 'not 10,11

End Sub

Private Sub hCheck11to20()
    Dim count As Integer
    Dim lslist As String

    dprint "hcheck11to20"
    If QLSCountValue(94528, ">24") >= CountForPull(3) Then mhinds(13).checked = True

    If Not (b90117 Or b92420) Then
        If QLSRes(94827, "LPN") Or QLSRes(94827, "PCT") Or QLSRes(94827, "RN") Then mhinds(13).checked = True
    End If

    If QLSRes(94864, "1:1 cont") Then mhinds(13).checked = True

    If QLSRes(95946, "Start") Or QLSRes(95946, "ongoing") Then
        If QLSRes(95946, "LPN") Or QLSRes(95946, "PCT") Or QLSRes(95946, "RN") Then mhinds(13).checked = True
    End If
    
    If QLS(95952) Then mhinds(13).checked = True
    
    If Not mhinds(13).checked Then
    If QLSCountValue(94528, ">=16 and result<=24") >= CountForPull(2) Then mhinds(12).checked = True
    End If
    
    If Not (mhinds(13).checked Or mhinds(12).checked Or mhinds(11).checked) Then
    
    If QLSList("95949,95951") Then mhinds(11).checked = True

    If QLSListRes("24359,24363,24364", "slf destr behave") Then mhinds(11).checked = True
    
    If Not (b90117 Or b92420) Then
        'If QLSRes(93027, "bed alarm on") And QLSRes(93027, "near nur station") And QLSRes(93027, "toileting assist") Then
        If QLSCountValue(93027, " like '%bed alarm on%' and result like '%near nur station%' and result like '%toileting assist%'") > 0 Then
            mhinds(11).checked = True
        End If
        If QLS(94826) Then mhinds(11).checked = True
        If QLSRes(96068, "lvl2 sh") Then mhinds(11).checked = True
    End If

    If Not mhinds(11).checked Then
        If QLSCountValue(94528, "<15") >= 1 Then mhinds(11).checked = True
    
        If QLSRes(94818, "ciwa") Then mhinds(11).checked = True
        If QLSRes(94824, "yes") Then mhinds(11).checked = True
        If QLSRes(94825, "yes curr") Then mhinds(11).checked = True
        If QLSRes(95945, "initiated") Then mhinds(11).checked = True
    End If ' not 11
    
    End If 'not 11,12,13

    dprint "hcheck11to20 B"
    
    If Not mhinds(15).checked Then
    If QLSRes(91763, "q 2") Then mhinds(15).checked = True
    If QLSCountValue(94528, ">8") >= CountForPull(2) Then mhinds(15).checked = True
    End If 'not 15
    
    If Not (mhinds(15).checked Or mhinds(14).checked) Then
    If QLSRes(91763, "q 4") Then mhinds(14).checked = True
    
    count = QLSCountValue(94528, ">=0 and result<=8")
    If count >= CountForPull(1) Then
        mhinds(14).checked = True
    End If

    If QLSRes(94851, ">20") Then mhinds(14).checked = True
    End If 'not 14,15
    
    dprint "hcheck11to20 C"
    If Not mhinds(16).checked Then
        If QLSList("9995,94536") Then mhinds(16).checked = True
        If Not mhinds(16).checked Then
        If range >= 1440 Then '7am pull
            count = 3
        Else
            count = 1
        End If
        If QLSListCount("6913,6928,6930,16966,35597,35598") >= count And _
           QLSListCount("6931,6933,6934,6948,6952,6953,6962,6976,6978,12557") >= count Then
            mhinds(16).checked = True
        End If
        End If
    End If
    
    dprint "hcheck11to20 D"
    If QLS(92030) Then
        mhinds(17).checked = True
        mhinds(18).checked = True
    End If
    If Not mhinds(17).checked Then
        If QLSRes(96118, "1:1 RN=1hr") Or QLSRes(96118, "1:1 RN>1hr") Then mhinds(17).checked = True
        dprint "before dizziness"
        count = QLSCountValue(96327, " like '%dizziness%'")
        dprint "after dizziness" & count
        If count >= CountForPull(1) Then
            mhinds(17).checked = True
        End If
    End If
    If Not mhinds(17).checked Then
        lslist = "6880,6881,6891,6897,89746,89747,89749,89750,89751,89752,89754,89762,89805,89806,89807,"
        lslist = lslist & "89808,89809,89810,89811,89812,89813,89814,89815,89815,89816,89817,89818,89819,"
        lslist = lslist & "89820,89821,89822,89823,89824,89825,89826,89827,89828,89829,89830,89831,89831,"
        lslist = lslist & "89832,89833,89834,89835,89835,89836,89838,89839,89840,89841,89842,89843,89844,"
        lslist = lslist & "89845,89846,89848,89915,89916,90327,90328,90333,90334,91619,91620,91621,91622,"
        lslist = lslist & "91623,91624,91625,91636,91637,91639,91642,91647,91730,91734,91737,91738,91742,"
        lslist = lslist & "91745,91746,93629,93630,93631,93632,93633,93725,93729,93731,93733,93735"
        If QLSListFreq(lslist, CountForPull(1)) Then mhinds(17).checked = True
    End If
    
    
    dprint "hcheck11to20 E"
    lslist = "90116,90117,90118,90119,90120,90123,90124,90128,90132,90134,90155,90157,90159,90161,90171,"
    lslist = lslist & "90172,90173,90174,90175,90182,90326,94528"
    If QLSListFreq(lslist, CountForPull(1)) Then mhinds(18).checked = True

    lslist = "89045,89048,89064,89074,89323,89334,89344,89356,89367,89378,89389,89400,89423,89522,89531,"
    lslist = lslist & "89540,89549,89558,89566,89576,90184,90838,90851,90863,90877,90891,92041,94538"
    If QLSList(lslist) Then mhinds(19).checked = True
    
    If Not mhinds(19).checked Then
    If QLSRes(89054, "site care") Then mhinds(19).checked = True
    If QLSRes(90134, "trach") Then mhinds(19).checked = True
    If QLSRes(91730, "trach") Or QLSRes(91730, "cannula") Then mhinds(19).checked = True
    End If
    
    If Not mhinds(19).checked Then
    If QLSListRes("91738,91746", "drsg") Or QLSListRes("91738,91746", "dc") Then mhinds(19).checked = True
    If QLSListRes("91915,91922", "drsg rein") Then mhinds(19).checked = True
    If QLSListRes("93759,93761", "pin care") Then mhinds(19).checked = True
    End If
    
    dprint "hcheck11to20 F"
    If Not mhinds(19).checked Then
    If QLSRes(94919, "neous trach") Or QLSRes(94919, "drain") Or QLSRes(94919, "thora") Then mhinds(19).checked = True
    If QLSRes(94920, "marrow") Or QLSRes(94920, "line insert") Or QLSRes(94920, "pericar") Or QLSRes(94920, "pacemaker") Then mhinds(19).checked = True
    If QLSRes(94921, "gastric") Or QLSRes(94921, "paracen") Or QLSRes(94921, "gi drain") Then mhinds(19).checked = True
    End If
    
    If Not mhinds(19).checked Then
    If QLSRes(94922, "neled cath") Then mhinds(19).checked = True
    If QLSRes(94923, "halo") Or QLSRes(94923, "csf") Then mhinds(19).checked = True
    If QLSRes(94924, "skin biop") Or QLSRes(94924, "synovial") Then mhinds(19).checked = True
    End If
    
    If Not mhinds(19).checked Then
    If QLSRes(89081, "drsg") Or QLSRes(89081, "press") Then mhinds(19).checked = True
    If QLSRes(89086, "drsg") Or QLSRes(89086, "site care") Then mhinds(19).checked = True
    End If
    
    If Not mhinds(19).checked Then
    lslist = "89319,89333,89343,89355,89366,89377,89388,89399"
    If QLSListRes(lslist, "drsg") Or QLSListRes(lslist, "open to air") Or QLSListRes(lslist, "press") Then mhinds(19).checked = True
    If QLSListRes(lslist, "staple") Or QLSListRes(lslist, "steri strip") Or QLSListRes(lslist, "other") Then mhinds(19).checked = True
    End If

    
    dprint "hcheck11to20 G"
    If QLS(96026) Then mhinds(20).checked = True
    If Not (b90117 Or b92420 Or mhinds(20).checked) Then
        If QLS(94917) And QLS(94918) Then mhinds(20).checked = True
        If QLS(94926) And QLS(94927) Then mhinds(20).checked = True
        If QLS(94939) And QLS(94941) Then mhinds(20).checked = True
        If QLS(94940) And QLS(94941) Then mhinds(20).checked = True
        If QLS(94945) And QLS(94952) Then mhinds(20).checked = True
        If Not mhinds(20).checked Then
        If QLS(94973) And QLS(94974) Then mhinds(20).checked = True
        If QLS(95022) And QLS(95023) Then mhinds(20).checked = True
        If QLS(95027) And QLS(95029) Then mhinds(20).checked = True
        If QLS(95028) And QLS(95029) Then mhinds(20).checked = True
        If QLS(95097) And QLS(95098) Then mhinds(20).checked = True
        End If
    End If

End Sub

Private Sub hCheck21to49()
    Dim count As Integer
    Dim lslist As String

    dprint "hCheck21to49"
    If QLSRes(95017, "patient") Then b95017 = True
    If QLSRes(95016, "family") Then b95016 = True
    If QLSRes(95016, "other") Then b95016 = True
    If QLSRes(95016, "friend") Then b95016 = True
    
    If Not ((b95017 And b90117) Or b92420 Or b95016) Then
        If QLS(95025) And QLS(95026) Then mhinds(21).checked = True
        If QLS(95028) And QLS(95031) Then mhinds(21).checked = True
        If QLS(95029) And QLS(95031) Then mhinds(21).checked = True
        If QLS(95030) And QLS(95031) Then mhinds(21).checked = True
        If Not mhinds(22).checked Then
        If QLS(95033) And QLS(95034) Then mhinds(21).checked = True
        If QLS(95033) And QLS(95035) Then mhinds(21).checked = True
        If QLS(95035) And QLS(95037) Then mhinds(21).checked = True
        If QLS(95036) And QLS(95037) Then mhinds(21).checked = True
        End If
        If Not mhinds(22).checked Then
        If QLS(95038) And QLS(95041) Then mhinds(21).checked = True
        If QLS(95043) And QLS(95044) Then mhinds(21).checked = True
        If QLS(95047) And QLS(95048) Then mhinds(21).checked = True
        If QLS(95049) And QLS(95052) Then mhinds(21).checked = True
        If QLS(95050) And QLS(95052) Then mhinds(21).checked = True
        End If
        If Not mhinds(22).checked Then
        If QLS(95051) And QLS(95052) Then mhinds(21).checked = True
        If QLS(95054) And QLS(95055) Then mhinds(21).checked = True
        If QLS(95056) And QLS(95059) Then mhinds(21).checked = True
        If QLS(95057) And QLS(95059) Then mhinds(21).checked = True
        If QLS(95058) And QLS(95059) Then mhinds(21).checked = True
        End If
        If Not mhinds(22).checked Then
        If QLS(95060) And QLS(95063) Then mhinds(21).checked = True
        If QLS(95061) And QLS(95063) Then mhinds(21).checked = True
        If QLS(95062) And QLS(95063) Then mhinds(21).checked = True
        If QLS(95064) And QLS(95065) Then mhinds(21).checked = True
        End If
        If Not mhinds(22).checked Then
        If QLS(95066) And QLS(95068) Then mhinds(21).checked = True
        If QLS(95067) And QLS(95068) Then mhinds(21).checked = True
        If QLS(95069) And QLS(95074) Then mhinds(21).checked = True
        If QLS(95076) And QLS(95077) Then mhinds(21).checked = True
        End If
        If Not mhinds(22).checked Then
        If QLS(95078) And QLS(95080) Then mhinds(21).checked = True
        If QLS(95079) And QLS(95080) Then mhinds(21).checked = True
        If QLS(95216) And QLS(95215) Then mhinds(21).checked = True
        If QLS(95218) And QLS(95219) Then mhinds(21).checked = True
        End If
    End If
    dprint "hCheck21to49 B"
    
    lslist = "92219,92221,92222,92233,92234,92237,92239,92243"
    If QLSList(lslist) Then mhinds(24).checked = True
        
    If Not (b92420 Or b90117 Or mhinds(24).checked) Then
    If QLSRes(98484, "Start") Or QLSRes(98484, "ongoing") Then
        If QLSRes(98484, "LPN") Or QLSRes(98484, "PCT") Or QLSRes(98484, "RN") Then
            mhinds(24).checked = True
        End If
    End If
    End If
    If QLSCountValue(94528, ">15") >= 1 Then mhinds(24).checked = True
    
    If Not mhinds(24).checked Then
        If QLSRes(94863, "1:1 inter") Then
            If QLSRes(94864, "1:1 cont") Then mhinds(24).checked = True
        End If
    
        If QLSRes(94863, "yellow") Then
            If QLSRes(94864, "1:1 cont") Or QLSRes(94864, ">2 hr") Then mhinds(24).checked = True
        End If
    
    End If
    
    dprint "hCheck21to49 C"
        
If Not mhinds(24).checked Then
    If QLSCountValue(94528, ">=8 and result<=15") >= 1 Then mhinds(23).checked = True
    If Not (b92420 Or b90117) Then
        If QLSRes(94825, "yes curr") Then mhinds(23).checked = True
    End If
    If QLSRes(94864, "every hr") Then
        If QLSList("94862,94863") Then mhinds(23).checked = True
    End If
    If QLSRes(94822, "every hr") Then
        If QLSRes(94821, "limit setting") Then mhinds(23).checked = True
        If QLSRes(94821, "encourage expression") Then mhinds(23).checked = True
        If QLSRes(94821, "active listen") Then mhinds(23).checked = True
    End If
    
    If Not mhinds(23).checked Then
    If QLSCountValue(94528, ">=0 and result<=8") >= 1 Then mhinds(22).checked = True
    
    If QLSRes(94822, "once a shift") Then
        If QLSRes(94821, "limit setting") Then mhinds(22).checked = True
        If QLSRes(94821, "encourage expression") Then mhinds(22).checked = True
        If QLSRes(94821, "active listen") Then mhinds(22).checked = True
    End If
    
    If QLSRes(94864, "2 hr") Or QLSRes(94864, "4 hr") Then
        If QLSList("94862,94863") Then mhinds(22).checked = True
    End If
    End If 'not23
End If 'not 24

    dprint "hCheck21to49 D"

    If QLS(94528) Then mhinds(25).checked = True
    If QLSRes(94862, "limit visitors") Or QLSRes(94862, "Reduce stim") Then mhinds(25).checked = True
    If QLSRes(94863, "limit") Or QLSRes(94863, "ciwa") Then mhinds(25).checked = True
    
    If QLSRes(94863, "medicated") Then
        If QLSRes(94816, "agitated") Or QLSRes(94816, "angry") Then
            If QLSRes(94854, "abusive") Or QLSRes(94854, "aggres") Or QLSRes(94854, "combat") Or QLSRes(94854, "mania") Then
                mhinds(26).checked = True
            End If
        End If
    End If
    
    If QLSRes(94863, "restraint") Then
        mhinds(27).checked = True
        mhinds(40).checked = True
    End If
    
    If QLSRes(91123, "RN") Or QLSRes(91123, "LPN") Or QLSRes(91123, "PC") Then mhinds(29).checked = True
    If QLSRes(92241, "hostile") Or QLSRes(92241, "depress") Or QLSRes(92241, "flat") Then mhinds(29).checked = True
    
    If QLSRes(93034, "left") Then mhinds(40).checked = True
    If QLSRes(93034, "right") Then mhinds(40).checked = True
    If QLSRes(93034, "wrist") Then mhinds(40).checked = True
    If QLSRes(93034, "ankle") Then mhinds(40).checked = True
    If QLSRes(93034, "mitten") Then mhinds(40).checked = True
    If QLSRes(93034, "leather") Then mhinds(40).checked = True
    If QLSRes(93034, "belt") Then mhinds(40).checked = True
    If QLSRes(93034, "vest") Then mhinds(40).checked = True
    If QLSRes(93034, "enclosure") Then mhinds(40).checked = True
    
    dprint "hCheck21to49 E"
    
    If QLSListRes("90893,92044", "multi lumen cath") Then mhinds(35).checked = True
    
    If QLSList("90838,90841,90844,90851,90853,90856") Then mhinds(37).checked = True

    If QLSRes(89075, "see iv") Then mhinds(39).checked = True
    If QLSRes(90882, "tunel") Or QLSRes(90882, "tunnel") Then mhinds(39).checked = True
    
    If QLSList("6933") Then mhinds(41).checked = True
    If Not mhinds(41).checked Then
        If Not QLSList("89033,89034,89035") Then
            If QLSList("89039,89040,89041") Then mhinds(41).checked = True
        End If
    End If
    
    If QLSListRes("90893,92044", "hickman") Then mhinds(46).checked = True
    
    If QLSList("90863,90865,90872") Then mhinds(47).checked = True
    
    If QLSListRes("90893,92044", "groshong") Then mhinds(49).checked = True
    
    
End Sub


