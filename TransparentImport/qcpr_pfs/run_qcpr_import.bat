@ECHO OFF
ECHO Run the QCPR to AcuityPlus Transparent Classification...
QCPR_PFS -debug -range=10080

ECHO Import the classifications...
TransparentImport.exe -user=sysadmin -passwd=sysadmin _TransparentClassification.if

ECHO Done!
PAUSE
