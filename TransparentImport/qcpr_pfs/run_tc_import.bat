@ECHO OFF
ECHO Run the Transparent Classification chart translator
QCPR_PFS -debug -range=10080

ECHO Import the classifications...
TransparentImport -user=sysadmin -passwd=sysadmin _TransparentClassification.if

ECHO Done!
@rem PAUSE
