Attribute VB_Name = "MainModule"
Option Explicit

Public g_util       As New PFSUtility
Public g_tcutil     As New TCUtility
Public g_display    As New PFSDisplayProperties

Public Const AUTO_LOG_FILE_LIFE = 30   'keep log files for 30 days

Public gPollEvery   As Integer          'poll every x seconds

Public import_path  As String
Private logpath     As String

Const INI_FNAME = "TC_IMPORT.INI"
Private logfile     As Integer
Private logday      As Integer
    

Sub Main()
    On Error GoTo errHandler
    
    Dim frmAuto As New frmAutoChartImport
    Dim n As Integer, equal_position As Integer

    If (App.PrevInstance) Then Exit Sub

    ChDrive App.Path
    ChDir App.Path

    logpath = g_util.GetProfileString(INI_FNAME, "DIRS", "LogDir", "..\log")
    import_path = g_util.GetProfileString(INI_FNAME, "DIRS", "ImportDir", "..\load_me")

    logpath = g_util.ExpandPath(logpath)
    import_path = g_util.ExpandPath(import_path)

    'Parse the command-line arguments
    gPollEvery = 5               'default every 5 seconds
    
    n = InStr(1, Command$, "-poll")
    If n > 0 Then
        equal_position = InStr(n, Command$, "=")
        If equal_position > 0 Then
           gPollEvery = CLng(Trim(Right(Command$, Len(Command$) - equal_position)))
        End If
    End If

    frmAuto.Show
    Exit Sub
    
errHandler:
    g_util.MsgBoxError
End Sub


Private Function MakeLogName(dt As Date)
    'Make a log filename using a date
    MakeLogName = logpath & "\AutoChartImport_" & Format$(dt, "mmdd") & ".LOG"
End Function


Public Sub AddLogEntry(with_timestmp As Boolean, desc As String)
    On Error GoTo errHandler
    Static log_error As Boolean
    Dim logfname As String, msg As String, temp As String
    Dim need_to_open As Boolean, new_file As Boolean
    Dim i As Integer
    Dim dt As Date

    If (with_timestmp) Then
        msg = Format$(Now, "mm/dd hh:mm:ss") & vbTab & desc
    Else
        msg = desc
    End If

    '
    ' Open the log file if needed
    '
    need_to_open = False

    If logfile = 0 Then
        logfile = FreeFile
        need_to_open = True
    ElseIf logday <> day(Now) Then
        ' Don't call AddLogEntry here -- infinite recursion would result
        Print #logfile, "** End of log file"
        Close #logfile
        need_to_open = True
    End If

    If need_to_open Then
        logday = day(Now)
        logfname = MakeLogName(Now)
        new_file = Not g_util.FileExists(logfname)
        Open logfname For Append As #logfile

        'It is OK to call AddLogEntry now (recursive)

        If new_file Then
            Call LogInfo("Started new log file " & logfname)
        End If

        ' Delete old log files
        ' (allow the interface to be down for up to 5 days)
        dt = DateAdd("d", -AUTO_LOG_FILE_LIFE, Now)
        For i = 1 To 5
            temp = MakeLogName(dt)
            If g_util.FileExists(temp) Then
                Kill temp
                Call LogInfo("Deleted log file " & temp)
            End If
            dt = DateAdd("d", -1, dt)
        Next i
    End If
    
    '
    ' Add to the log file
    '
    Print #logfile, msg
    Exit Sub
    
errHandler:
    If log_error Then Exit Sub      'avoid recursive error messages
    log_error = True
    g_util.ThrowError , "Adding log entry to " & logfname
End Sub

Public Sub FlushLogFile()
    Close #logfile
    logfile = 0
End Sub


Public Sub LogEvent(s As String)
    Call AddLogEntry(True, s)
End Sub

Public Sub LogInfo(s As String)
    Call AddLogEntry(True, s)
End Sub

Public Sub LogWarning(s As String)
    Call AddLogEntry(True, "Warning: " & s)
End Sub

Public Sub LogError(s As String)
    Call AddLogEntry(True, "ERROR: " & s)
    FlushLogFile
End Sub

