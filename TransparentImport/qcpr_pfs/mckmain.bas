Attribute VB_Name = "MckMain"
Option Explicit
'The location of the input file is determined by the first line of the MSDICT.DAT dictionary.
'
' This is the main module for SiePFS
' File is formatted as:
'    <Header>
'       <Event>
'       <Event>
'         ...
'
Const TRANSP_FILENAME = "Transparent.txt"   'output file
Const TRANSP_INLOG_FILE_LIFE = 10           'days
Const TRANSP_OUTLOG_FILE_LIFE = 10          'days
Const TRANSP_DEBUG_FILE_LIFE = 10           'days
Const MSDIC_FNAME = "MSDICT.TXT"            'MS rules file
Const MHDIC_FNAME = "MHDICT.TXT"
Const DNLD_FNAME = "TCQUERY"                'prefix of import filename
Const VALIDMS_FNAME = "VALIDMS.TXT"         'list of valid MS units
Const VALIDMH_FNAME = "VALIDMH.TXT"
Const PERDATA_FNAME = "PERSIST.TXT"         'name of persistent data file (data saved for next time)

'==============================================================================
'HEADER RECORD - HIMSS DEMO
Const MAX_INDICATORS = 50       'only 50?

Const START_ACCT_NUM = 1
Const LEN_ACCT_NUM = 20

Const START_PTNAME = 100
Const LEN_PTNAME = 30

Const START_UNIT = 31
Const LEN_UNIT = 10

Const START_RM = 41
Const LEN_RM = 8            '10

Const START_BED = 51
Const LEN_BED = 4           '5

Const START_FACILITY = 200   'N/A
Const LEN_FAC = 5

Const START_BIRTHDATE = 86      'Birthdate?? This is in the HIMSS demo only
'Const LEN_BIRTH = 14

Const START_CLASS_DT = 200   'N/A
Const LEN_DT = 14


'==============================================================================
'EVENT RECORD - HIMSS DEMO
Const START_EVENT_DT = 5
'Const LEN_DT = 14

Const START_EVENT = 20
Const LEN_EVENT = 30

Const START_EVENT_DSC = 50
Const LEN_EVENT_DSC = 25

Const START_FREQ = 65             '75
Const LEN_FREQ = 10

Const START_RESULT = 100            '116
Const LEN_RESULT = 75


Const MAX_RANGE = 1440                  '24 hrs in minutes; this is overridden by the -range arg.

Const RANGE_FACTOR = 24 * 60            'For proportioning the count of Med/Pulm/Cardio

Const MAX_UNIQUE_CHART_TIMES = 20

Dim g_util As New RegistryUtils

Dim infn As String
Dim fnames() As String
Dim outfn As String
Dim inlogfile As Integer
Dim inlogname As String
Dim outlogfile As Integer
Dim outlogname As String
Dim dbugfile As Integer
Dim dbugname As String

Private Type indicator
  indwinpfs As Integer  'winpfs indic number associated with this cerner event
  eventid As String     'Event_CD of this indicator
  chartkey As String    'look for this key in charted result
  gavefreq As Boolean   'file supplied the frequency; ignore subsequent freqs
  check_freq As Boolean 'flag to check frequency
  freq_basis As Integer 'frequency required for this indicator (in minutes)
  act_freq As Integer   'calculated frequency from data (in minutes)
  last_datetime As String 'last date time of event found in this patient
  num_found As Integer  'number of times this was found in this patient
  also_mark As String   'if this indicator is marked, then also mark these.
  'charttime(MAX_UNIQUE_CHART_TIMES) As String       commented out and in code to save mem space 9/26/07
  one_row_find As Single    'for multiple charting results on the same event line (Cerner)
  persist As Boolean    'flag if this charting element persists
  perlen As Single      'for how long does it persist? (in minutes)
End Type

Private Type indicator_data
    checked As Boolean
    also_mark As String
End Type

Private Type event_wildcharting
    eventid As String 'event id
    count As Single
End Type

Private Type ProcessedInfo
    a As String     'acctnum
    already_did_persist As Boolean
End Type

Dim dicary() As indicator
Dim mhdicary() As indicator
Dim dicnum As Integer
Dim mhdicnum As Integer
Dim inDirPath As String  'path of input file
Dim winpfspath As String 'path of winpfs
Dim winlogpath As String 'path of winpfs\log
Dim winloadpath As String ' path of winpfs\load_me

Dim datafile As Integer
Dim outfile As Integer

Dim inds(MAX_INDICATORS) As indicator_data
Dim grps(MAX_INDICATORS) As Integer
Dim mhinds(MAX_INDICATORS) As indicator_data
Dim mhgrps(MAX_INDICATORS) As Integer

Dim lastname As String
Dim firstname As String
Dim unitname As String
Dim roomname As String
Dim bedname As String
Dim acctnum As String
Dim classdt As String
Dim classdate As String
Dim classtime As String
Dim nowdt As Date
Dim intime As String
Dim saveintime As String

Dim range As Single                 'number of minutes in the scope of time for freq.
Dim dbugon As Boolean
Dim suppressDbugLog As Boolean
Dim effdateon As Boolean
Dim effdate As String
Dim efftimeon As Boolean
Dim efftime As String
Dim pulltimeon As Boolean
Dim pulltime As String
Dim pulldateon As Boolean
Dim pulldate As String

Dim event6931 As Boolean
Dim event33474 As Boolean
Dim event16966 As Boolean
Dim event21430 As Boolean
Dim count18098 As Single
Dim event33765 As Boolean
Dim val33775 As Single
Dim event33775 As Boolean
Dim val35286 As Single
Dim event35286 As Boolean
Dim count5805 As Single
Dim count5806 As Single
Dim countwild(20) As event_wildcharting

Dim MSunitary() As String
Dim MHunitary() As String
Dim unitnum As Integer
Dim mhunitnum As Integer

Dim acctnumdirectory() As ProcessedInfo
Dim numacct As Single
Dim telemetry As Boolean


Sub Main()
    Const RANGE_PARAM = "-range="
    Const EFF_TIME = "-efftime="
    Const DBUG_ON = "-debug"
    Const ALTER_DATE = "-effdate="
    Const PULL_TIME = "-pulltime="
    Const PULL_DATE = "-pulldate="
    Dim n As Integer
    Dim i As Integer
    Dim cmdLine As String
    Dim rpos As Integer
    'Dim effective As String
    Dim h As String
    Dim t As Date
    Dim adpos As Integer
    
    '-efftime=hhmm  This is the time at which the classification is effective.
    '                 The date associated with this time is taken from header classdate.
    '                 If not specified, then -efftime is assumed to be the classtime
    '                 from the header.
    '-effdate=yyyymmdd   This is the date which, in combination with the
    '                 -effective time is used to specify the effective datetime.
    '                 If not specified, then -effdate is assumed to be the classdate
    '                 from the header.
    'Special value:  -effdate=yesterday means Now's yesterday.
        
    '-pulltime=hhmm  This is the time starting from which the pull is to
    '                look backwards from.
    '                If not specified, then this time is assumed to be the -effective time.
    '-pulldate=yyyymmdd This is the date of the pulltime.
    '                If not specified, then this is assumed to be Now's date.
    
    '-range=nnnn  This is the number of minutes backwards from the pull time
    '             that defines valid range of charting events.
    
    nowdt = Now
    cmdLine = LCase(Command$)
    
    
    dbugon = (InStr(cmdLine, DBUG_ON) > 0)
    suppressDbugLog = False
    
    effdateon = (InStr(cmdLine, ALTER_DATE) > 0)
    If effdateon Then
        adpos = InStr(cmdLine, ALTER_DATE)
        If UCase$(Mid$(cmdLine, adpos + Len(ALTER_DATE), 8)) = "YESTERDA" Then
            effdate = Format(DateAdd("d", -1, g_util.DateOnly(nowdt)), "yyyymmdd")
        Else
            effdate = Mid$(cmdLine, adpos + Len(ALTER_DATE), 8) 'yyyymmdd
        End If
    End If
    
    
    pulltimeon = (InStr(cmdLine, PULL_TIME) > 0)
    If pulltimeon Then
        pulltime = Mid$(cmdLine, InStr(cmdLine, PULL_TIME) + Len(PULL_TIME), 4)
    End If
    
    pulldateon = (InStr(cmdLine, PULL_DATE) > 0)
    If pulldateon Then
        pulldate = Mid$(cmdLine, InStr(cmdLine, PULL_DATE) + Len(PULL_DATE), 8)
    End If

    efftimeon = (InStr(cmdLine, EFF_TIME) > 0)
    If efftimeon Then
        efftime = Mid$(cmdLine, InStr(cmdLine, EFF_TIME) + Len(EFF_TIME), 4)
    End If
    
    rpos = InStr(cmdLine, RANGE_PARAM)
    If rpos > 0 Then
        'range = val(Mid$(cmdLine, InStr(cmdLine, RANGE_PARAM) + Len(RANGE_PARAM), 4))
        range = val(Mid$(cmdLine, rpos + Len(RANGE_PARAM)))       'what if range > 9999?  (take away ,4)
    End If
    
    If range <= 0 Then
        range = MAX_RANGE
    End If
    
    'No effective time given?  Use current time.    ***
    If efftime = "" Then
        efftime = Format(nowdt, "hhnn")
    End If
    
    intime = ""
    
    If (LoadDictionaries) Then
        InitGroups
        MakeLogFiles
        dprint -1, 0, "range=" & range
        dprint -1, 0, "effdate=" & effdate
        dprint -1, 0, "efftime=" & efftime
        dprint -1, 0, "pulltime=" & pulltime
        dprint -1, 0, "pulldate=" & pulldate
        
        If efftime <> "" Then
            ' command parameter needs to be in form hhmm ONLY
            If IsNumeric(efftime) Then
                efftime = Format$(val(efftime), "0000")
                i = year(g_util.DateOnly(FileDateTime(inlogname)))
                intime = CStr(i) & Mid$(inlogname, Len(inlogname) - Len("mmdd.log") + 1, Len("mmdd")) & efftime
                If effdateon Then
                    intime = effdate & Mid$(intime, 9, 4)
                End If
                saveintime = intime
            End If
        End If
        
        n = GetAllInputFilenames
'        i = 0      ONLY PROCESS THE LATEST FILE 11/4/05
        i = n - 1  'ONLY PROCESS THE LATEST FILE 11/4/05
'Open the Persist file, removing old items, and read acctnums into the acctnumdirectory array
        DeleteOldPersistData
        While i < n
            i = i + 1
            infn = inDirPath + "\" + fnames(i)
            Process
        Wend
        CloseLogFiles
        DeleteOldLogs
    End If

End Sub

Private Function LoadDictionaries() As Boolean
    Dim dicfn As String
    Dim dicfile As Integer
    Dim buf As String
    Dim unitfn As String
    Dim unitfile As Integer
    
    'Special dictionary characters:
    '  * = check freq or other things before triggering indicator
    '  # = persist only
    '  @ = persist and check freq

    dicnum = 0
    dicfile = FreeFile
    dicfn = App.Path & "\" & MSDIC_FNAME
    Open dicfn For Input As #dicfile
    While Not EOF(dicfile)
        Line Input #dicfile, buf
        
        If (Len(buf) = 0) Or (Left$(buf, 2) = "//") Then
            'ignore comment lines and empty lines
        ElseIf Len(inDirPath) = 0 Then
            inDirPath = buf
        Else
            dicnum = dicnum + 1
            ReDim Preserve dicary(0 To dicnum)
            With dicary(dicnum)
                .indwinpfs = val(Mid$(buf, 1, 2))
                .check_freq = (Mid$(buf, 3, 1) = "*") Or (Mid$(buf, 3, 1) = "@")
                .eventid = Trim$(Mid$(buf, 4, 29))          '30
                .freq_basis = val(Trim$(Mid$(buf, 34, 4)))
                .also_mark = Trim$(Mid$(buf, 39, 10))
                .chartkey = Trim$(Mid$(buf, 50, 75))
                .persist = (Mid$(buf, 3, 1) = "#") Or (Mid$(buf, 3, 1) = "@")
                If .persist Then .perlen = 1440
                'Debug.Print "ind#"; .indwinpfs; " event="; .eventid; " result="; .chartkey
            End With
        End If
    Wend
    Close #dicfile
'    dicnum = dicnum - 1
    
'Valid Med Surg units
    unitnum = 0
    unitfile = FreeFile
    unitfn = App.Path & "\" & VALIDMS_FNAME
    Open unitfn For Input As #unitfile
    While Not EOF(unitfile)
        Line Input #unitfile, buf
        If Trim$(buf) <> "" Then
            unitnum = unitnum + 1
            ReDim Preserve MSunitary(0 To unitnum)
            MSunitary(unitnum) = UCase$(Trim$(Mid$(buf, 1, 16)))
        End If
    Wend
    Close #unitfile
    
    mhdicnum = 0
    dicfile = FreeFile
    dicfn = App.Path & "\" & MHDIC_FNAME
'    Open dicfn For Input As #dicfile
'    While Not EOF(dicfile)
'        mhdicnum = mhdicnum + 1
'        Line Input #dicfile, buf
'        ReDim Preserve mhdicary(0 To mhdicnum - 1)
'        mhdicary(mhdicnum - 1).indwinpfs = val(Mid$(buf, 1, 2))
'        mhdicary(mhdicnum - 1).check_freq = (Mid$(buf, 3, 1) = "*") Or (Mid$(buf, 3, 1) = "@")
'        mhdicary(mhdicnum - 1).eventid = Trim$(Mid$(buf, 4, 30))
'        mhdicary(mhdicnum - 1).freq_basis = val(Trim$(Mid$(buf, 34, 4)))
'        mhdicary(mhdicnum - 1).also_mark = Trim$(Mid$(buf, 39, 10))
'        mhdicary(mhdicnum - 1).chartkey = Trim$(Mid$(buf, 50, 75))
'        mhdicary(mhdicnum - 1).persist = (Mid$(buf, 3, 1) = "#") Or (Mid$(buf, 3, 1) = "@")
'        If mhdicary(mhdicnum - 1).persist Then mhdicary(mhdicnum - 1).perlen = 1440
'    Wend
'    Close #dicfile
    mhdicnum = mhdicnum - 1
        
'Valid MH units
    mhunitnum = 0
    unitfile = FreeFile
    unitfn = App.Path & "\" & VALIDMH_FNAME
'    Open unitfn For Input As #unitfile
'    While Not EOF(unitfile)
'        Line Input #unitfile, buf
'        If Trim$(buf) <> "" Then
'            mhunitnum = mhunitnum + 1
'            ReDim Preserve MHunitary(0 To mhunitnum)
'            MHunitary(mhunitnum) = UCase$(Trim$(Mid$(buf, 1, 16)))
'        End If
'    Wend
'
''don't need the unit file anymore
'    Close #unitfile
    
'init wild charting array
    countwild(1).eventid = "25894"
    countwild(2).eventid = "27400"
    countwild(3).eventid = "27416"
    countwild(4).eventid = "27420"
    countwild(5).eventid = "27424"
    countwild(6).eventid = "27438"
    countwild(7).eventid = "27457"
    countwild(8).eventid = "27459"
    countwild(9).eventid = "27461"
    countwild(10).eventid = "27463"
    countwild(11).eventid = "27465"
    countwild(12).eventid = "27467"
    
    LoadDictionaries = True
    
End Function

Private Function GetAllInputFilenames() As Integer
    'The location is determined by the first line of the dictionary.
    Const INITIAL_BLOCK_SIZE = 24
    Dim i As Integer
    Dim infname As String
    
    outfn = winloadpath & "\" & TRANSP_FILENAME
    
    ReDim fnames(0 To INITIAL_BLOCK_SIZE) As String
    i = 0
    infname = Dir$(inDirPath + "\" & DNLD_FNAME & "*.TXT") 'returns ONLY the filename.  For wildcards.
    While infname <> ""
        i = i + 1
        If (i > UBound(fnames)) Then
            ReDim Preserve fnames(0 To i + INITIAL_BLOCK_SIZE) As String
        End If
        fnames(i) = infname
        infname = Dir$
    Wend
    
    BubbleSort fnames, i
    
    GetAllInputFilenames = i
    
End Function

Private Sub BubbleSort(arr As Variant, numEls As Integer)
    Dim lLoop1 As Integer
    Dim lLoop2 As Integer
    Dim lTemp As Variant
    
    For lLoop1 = numEls To LBound(arr) Step -1
       For lLoop2 = LBound(arr) + 1 To lLoop1
           If arr(lLoop2 - 1) > arr(lLoop2) Then
              lTemp = arr(lLoop2 - 1)
              arr(lLoop2 - 1) = arr(lLoop2)
              arr(lLoop2) = lTemp
           End If
       Next lLoop2
   Next lLoop1
End Sub

Private Function FoundInList(s As String, Optional freq As Variant) As Integer
    Dim i As Integer
    Dim useoption As Boolean
    Dim dontcare As Boolean

    FoundInList = 0
    useoption = Not IsMissing(freq)
    For i = 1 To dicnum             'numind
        If useoption Then
            dontcare = (Not dicary(i).check_freq)           'freq can be anything?
            If dicary(i).check_freq Then Debug.Print dicary(i).eventid; " freq_basis="; dicary(i).freq_basis
            If (s = dicary(i).eventid) Then     'And (dontcare Or (InStr(freq, dicary(i).freq) > 0)) Then
                FoundInList = i
                Exit For
            End If
        ElseIf (s = dicary(i).eventid) Then
            FoundInList = i
            Exit For
        End If
    Next i
End Function

Private Sub DoPatientSummary()
    Dim highest_is_on As Boolean
    Dim g As Integer
    Dim i As Integer
    Dim p As Integer
    
    'Here is where you now have to go through the frequencies to
    'determine which indicators to mark for this pt.
    
'    'Special ADL override:  3 is higher than 4.
'    If inds(3).checked Then inds(4).checked = False
'    If mhinds(3).checked Then mhinds(4).checked = False
    
    'set the act_freq to 1440 if num_found is >= 1
'    For i = 1 To dicnum
'        If dicary(i).num_found >= 1 Then
'            dicary(i).act_freq = 1440
'        End If
'    Next i
    
    If ValidMSUnit(unitname) Then
        'Special rules here
'        Check1
'        Check2_3_4
'        Check6
'        Check7
'        Check8
'        Check9
''        Check11
'        Check12_13
'        Check14_15_16
'        Check17_18_19
'        Check20_21_22
'        Check23
'        Check24_25_26
'        Check27
'        Check28
'        Check29
    

    'Next, loop the dictionary to find the indicator according to frequency.
    For i = 1 To dicnum
        If dicary(i).num_found > 0 Then
            'p = FindClosestFreq(i)
            p = FoundInList(dicary(p).eventid, True)
            If (p > 0) Then
                inds(dicary(p).indwinpfs).checked = True
                dprint dicary(p).indwinpfs, 1, "via fall through"

                If (dicary(p).num_found > 0 And Not inds(dicary(p).indwinpfs).checked) Then
                    dprint dicary(p).indwinpfs, -1, "***FAILED TO TURN ON***"
                End If

                If inds(dicary(p).indwinpfs).checked Then
                    If inds(dicary(p).indwinpfs).also_mark <> "" Then
                        'Only do the also mark if it originally arose from an also-mark ind.
                        ParseAlsoMark (inds(dicary(p).indwinpfs).also_mark)
                    End If
                End If
            End If
        End If
    Next i
    
'    If Not (inds(1).checked Or inds(2).checked Or inds(3).checked Or inds(4).checked) Then
'        dprint 2, 1, "via AtLeastOneADL"
'    End If
    'inds(2).checked = True
    
    'Look for any mutually exclusive indicators marked and remove the lesser ones.
    g = 0
    highest_is_on = False
    For i = MAX_INDICATORS To 1 Step -1
        If (grps(i) > 0) Then
            If (grps(i) <> g) Then
                g = grps(i)
                highest_is_on = inds(i).checked
            Else
                If highest_is_on Then
                    inds(i).checked = False
                Else
                    highest_is_on = inds(i).checked
                End If
            End If
        End If
    Next i
    
    'Make sure there is at least 1 ADL marked
    AtLeastOneADL
    
    End If
    
        
End Sub

Private Sub Process()
    Dim on_orders As Boolean
    Dim buf As String
    Dim p As Integer
    Dim i As Integer
    Dim d As String
    Dim formloading As Boolean
    Dim s As String
    Dim rows() As String
    Dim maxrow As Long
    Dim irow As Long
    Dim isodt As String
    Dim sNowdt As String
    Dim charting As String
    Dim prevcharting As String
    Dim res As String
    Dim rangedate As Date
    Dim rangedt As String
    Dim intimedate As Date
    Dim n As Integer
    Dim eventid As String
    Dim freq_given As Integer
    Dim skip_this_patient As Boolean
    Dim pf As Integer
    Dim w As Integer
    Dim just_read_a_line As Boolean
    Dim processing_persist_item As Boolean
    Dim percount As Single
    Dim valCIWA As Single
    
    On Error GoTo errProcess
        
    formloading = True
    skip_this_patient = False
'    cmdProcess.Enabled = False
'    cmdOutput.Enabled = False
    
    Debug.Print "Process "; infn
    datafile = FreeFile
    Open infn For Input As #datafile
    s = Input(LOF(datafile), datafile)
    rows() = Split(s, vbCrLf)
    maxrow = UBound(rows)
    
    outfile = FreeFile
    Open outfn For Append As #outfile
    
    val33775 = 99
    
    For irow = 0 To maxrow
        buf = rows(irow)
        Print #inlogfile, buf
        
        just_read_a_line = True
        processing_persist_item = False
        percount = 0
        While just_read_a_line Or processing_persist_item
            just_read_a_line = False  'the only way to get back into while loop is if processing
        
        'For PERSISTENT CHARTING:
        '  Data is by datetime + acct num + entire event line.  In text file (?).
        '  Pre-process: First, read through data list.  If any items past 24 hours, then delete them.
        '  Process Loop:
        '    Header: If acct num is found in data file, then add persistent charting items to current data
        '    while processing_persistent_items or just_read_a_line  we have the first item in persist.dat at this point
                'just_read_a_line = false
                
                'if event record then...
                '
                'else if header record...
                '  read from persist.dat
                '  if found a persist item then
                '     processing_per_item = true
                '     translate into buf
                '  else
                '     processing_persistent_item=false   to fall out of while loop and back into for loop
        
        '    wend
        '    Event: If eventid found is persistent (determined by dictionary), then add data to file with event line.
        
        '           QUESTION:  Are there special circumstances where the entire event line is not desired to
'                               be persistent?  Could only a part of the charting results on one line
'                               be persistent?  Because it's the dictionary that has the persistent
'                               attribute--not the data.   ASSUME NO for now.
        
        
        If Trim$(Mid$(buf, 1, 4)) = "" Then 'Event record
' There may be more than 1 dictionary entry with the same
' event code but with a different frequency.  So use the first occurrence
' of this event to keep the running frequency.  Then at then end,
' locate the appropriate indicator for the calculated frequency.
' OR, create a proc for the particular event code.
            If skip_this_patient Then
                'do nothing
            Else
            freq_given = 0
            isodt = Trim$(Mid$(buf, START_EVENT_DT, LEN_DT))        'event time

            'calc class effective IN time from either pull datetime or effective datetime
            If pulltimeon Then
                If pulldateon Then
                    intimedate = DateSerial(Mid$(pulldate, 1, 4), Mid$(pulldate, 5, 2), Mid$(pulldate, 7, 2)) _
                               + TimeSerial(Mid$(pulltime, 1, 2), Mid$(pulltime, 3, 2), 0)
                Else
                    intimedate = g_util.DateOnly(nowdt) + TimeSerial(Mid$(pulltime, 1, 2), Mid$(pulltime, 3, 2), 0)
                End If
            Else
                If intime = "" Then
                    intimedate = Now 'just to make it valid
                Else
                    intimedate = DateSerial(Mid$(intime, 1, 4), Mid$(intime, 5, 2), Mid$(intime, 7, 2)) _
                            + TimeSerial(Mid$(intime, 9, 2), Mid$(intime, 11, 2), 0)
                End If
            End If
            
            'dprint -1, 0, "intimedate=" & intimedate
            rangedate = DateAdd("n", -range, intimedate) ' range minutes before intime
            rangedt = Format$(rangedate, "yyyymmddhhnn")
            'dprint -1, 0, "rangedt=" & rangedt
'        Remove blinders for Newport -- rely entirely on data.
'            If rangedt > isodt Then
'                dprint -2, 0, buf
'            Else
                dprint -1, 0, buf
            eventid = Trim$(Mid(buf, START_EVENT, LEN_EVENT))
            charting = Trim$(Mid(buf, START_RESULT, LEN_RESULT))
            If Len(charting) > 1 Then
                If Asc(Right$(charting, 1)) < 32 Then
                    charting = Left$(charting, Len(charting) - 1)       'strip trailing LF from UNIX
                End If
            End If
            
'            'Both MS and MH flags
'            If eventid = "16996" Then 'telemetry tx
'                telemetry = True
'            End If
'
'            If eventid = "6931" And Not IsNull(charting) Then 'Fluid measurement
'                event6931 = True
'            End If
'            If eventid = "6933" And Not IsNull(charting) Then 'Fluid measurement; roll into event6931 for ease
'                event6931 = True
'                inds(44).checked = True
'            End If
'            If eventid = "33474" And Not IsNull(charting) Then 'Fluid measurement
'                event33474 = True
'            End If
'
'            If ValidMSUnit(unitname) Then 'Med/Surg only flags
'            If eventid = "16966" And Not IsNull(charting) Then 'Fluid measurement
'                event16966 = True
'            End If
'            If eventid = "21430" And Not IsNull(charting) Then 'Fluid measurement
'                event21430 = True
'            End If
'            If eventid = "18098" And Not IsNull(charting) Then
'                count18098 = count18098 + 1
'            End If
'            If eventid = "33765" Then
'                event33765 = True
'            End If
            
'            If eventid = "33775" Then ' <=16
'                If IsNumeric(Mid$(charting, 1, 2)) Then
'                    If val(Mid$(charting, 1, 2)) < val33775 Then
'                        val33775 = val(Mid$(charting, 1, 2))
'                        event33775 = True
'                    End If
'                End If
'            End If
'            If eventid = "35286" Then ' >=6
'                If IsNumeric(charting) Then
'                    If val(charting) > val35286 Then
'                        val35286 = val(charting)
'                        event35286 = True
'                    End If
'                End If
'            End If
            
                'check how many commas to find out how many diff chartings
                'the number of commas is determined by finding the length of the string
                'resulting from replacing the commas with a ",*" and then
                'subtracting the original length.
'            For w = 1 To 12
'                If eventid = countwild(w).eventid Then
'                    i = Len(Replace(charting, ",", ",*")) - Len(charting)
'                    If (i > countwild(w).count) Then
'                        countwild(w).count = i
'                    End If
'                End If
'            Next w
'            End If 'valid ms
            
'            If ValidMHUnit(unitname) Then
'                If eventid = "43647" And Not IsNull(charting) Then
'                    valCIWA = val(charting)
'                    If valCIWA >= 0 Then
'                        mhinds(17).checked = True
'                        mhinds(18).checked = True
'                        mhinds(25).checked = True
'                    End If
'                    If valCIWA >= 0 And valCIWA <= 8 Then
'                        mhinds(22).checked = True
'                    End If
'                    If valCIWA >= 0 And valCIWA <= 12 Then
'                        mhinds(22).checked = True
'                        mhinds(11).checked = True
'                        mhinds(14).checked = True
'                    End If
'                    If valCIWA > 8 Then
'                        mhinds(9).checked = True
'                    End If
'                    If valCIWA >= 9 And valCIWA <= 12 Then
'                        mhinds(23).checked = True
'                        mhinds(11).checked = True
'                        mhinds(14).checked = True
'                    End If
'                    If valCIWA > 12 Then
'                        mhinds(15).checked = True
'                    End If
'                    If valCIWA > 13 Then
'                        mhinds(24).checked = True
'                    End If
'                    If valCIWA >= 13 And valCIWA <= 24 Then
'                        mhinds(12).checked = True
'                    End If
'                    If valCIWA > 24 Then
'                        mhinds(13).checked = True
'                    End If
'                End If
'            End If 'valid mh
            
            If ValidMSUnit(unitname) Then
            
'                If eventid = "43647" And Not IsNull(charting) Then
'                    valCIWA = val(charting)
'                    If valCIWA >= 0 Then
'                        'inds(17).checked = True  Deleted 12/04/07
'                        inds(20).checked = True
'                        inds(29).checked = True
'                    End If
'                    If valCIWA >= 0 And valCIWA <= 12 Then inds(14).checked = True
'                    If valCIWA > 8 Then
'                        inds(7).checked = True
'                        inds(8).checked = True
'                    End If
'                    If valCIWA > 12 Then inds(15).checked = True
'                    If valCIWA > 15 Then
'                        'inds(18).checked = True Deleted 12/04/07
'                        inds(21).checked = True
'                    End If
'                    If valCIWA > 24 Then inds(9).checked = True
'                End If
            
                prevcharting = charting
                p = DicIndex(eventid, charting)
                pf = 0 'the number of one-row-finds
                
                If p > 0 Then
                    If dicary(p).persist And Not processing_persist_item Then 'only add to persist data from current data
                        AddPersistLine buf
                    End If
                End If
            
                While p > 0 'for multiple charting results on the same event line (Cerner)
                    pf = pf + 1
                    If dicary(p).check_freq Then ' asterisk (*) is associated with dic item
                        If dicary(p).num_found = 0 Then
                            dicary(p).num_found = 1
                            dicary(p).last_datetime = Trim$(Mid(buf, START_EVENT_DT, LEN_DT))
    '                        dicary(p).charttime(dicary(p).num_found) = Trim$(Mid(buf, START_EVENT_DT, LEN_DT))
                            dicary(p).act_freq = 0
                            'If this is the only occurrence, then it will be changed
                            'to 1440 at summary time.
                            dprint dicary(p).indwinpfs, -1, "num found = 1, freq=" & dicary(p).act_freq & " Item=" & p
                            If freq_given > 0 Then
                                dicary(p).gavefreq = True
                                dicary(p).act_freq = freq_given
                            End If
                        ElseIf freq_given > 0 Then
                            dicary(p).num_found = dicary(p).num_found + 1
                            dicary(p).act_freq = freq_given
                        Else
                            'CalcFrequency sets act_freq
                            dicary(p).num_found = dicary(p).num_found + 1
    '                        If dicary(p).num_found <= MAX_UNIQUE_CHART_TIMES Then
    '                            dicary(p).charttime(dicary(p).num_found) = Trim$(Mid(buf, START_EVENT_DT, LEN_DT))
    '                        End If
                            If Not dicary(p).gavefreq Then
                                CalcFrequency p
                            End If
                            dprint dicary(p).indwinpfs, -1, "num found =" & dicary(p).num_found & ", freq=" & dicary(p).act_freq & " Item=" & p
                        End If
                    Else
                        inds(dicary(p).indwinpfs).checked = True
                        dprint dicary(p).indwinpfs, 1, ""
                        If dicary(p).also_mark <> "" Then
                            ParseAlsoMark (dicary(p).also_mark)
                            dprint dicary(p).indwinpfs, 1, " Also-mark=" & dicary(p).also_mark
                        End If
                    End If
                    'Get the also-mark from the dictionary since this is its source.
                    If dicary(p).also_mark <> "" Then
                        inds(dicary(p).indwinpfs).also_mark = dicary(p).also_mark
                    End If
                    
                    If dicary(p).one_row_find < pf Then
                        dicary(p).one_row_find = pf
                    End If
                    
                    If prevcharting = charting Then
                        p = 0
                    Else
                        prevcharting = charting
                        p = DicIndex(eventid, charting)
                    End If
                Wend
            End If 'valid ms
            
'
            End If 'skip patient
        
        Else 'Header record
            If (Not formloading) Then 'end of the previous pt.  Output now.
                If Not skip_this_patient Then
                    DoPatientSummary
                    classdate = Mid$(classdt, 1, 8)
                    classtime = Mid$(classdt, 9, 4)
                    AssembleOutput
                End If
            End If
            skip_this_patient = False
            If dbugon Then suppressDbugLog = False
            dprint -1, 0, buf
            formloading = False
            InitIndicators
            ResetDictionaryFields
            ParsePatientInfo (buf) 'unitname, acctnum, etc. are set here
            If Not ValidMSUnit(unitname) And Not ValidMHUnit(unitname) Then
                skip_this_patient = True
            End If
            classdt = ""
            classdate = ""  'Mid$(buf, 21, 4) & Mid$(buf, 15, 2) & Mid$(buf, 18, 2)
            classtime = ""  'Mid$(buf, 26, 4)
            If efftimeon Then intime = saveintime
            event6931 = False
            event33474 = False
            event16966 = False
            event21430 = False
            count18098 = 0
            event33765 = False
            val33775 = 99
            event33775 = False
            val35286 = 0
            event35286 = False
            count5805 = 0
            count5806 = 0
            telemetry = False
            For i = 1 To 20
                countwild(i).count = 0
            Next i
        End If
        
        If AcctNumInPersistFile(acctnum) And Not DidProcessPersist(acctnum) Then  'reads persist data into buf?
            processing_persist_item = True
            percount = percount + 1
            GetNextPersistItemForThisAcctNum percount, buf
            If buf = "" Then
                processing_persist_item = False 'fall out of while
            End If
        Else
            processing_persist_item = False
        End If 'find acct num
            
        
        Wend 'while just_read_a_line or processing

        DoEvents
    Next

    If Not skip_this_patient Then
        DoPatientSummary
        classdate = Mid$(classdt, 1, 8)
        classtime = Mid$(classdt, 9, 4)
        AssembleOutput
    End If

    Close #datafile
    Close #outfile
    Debug.Print "delete "; infn
    'Kill infn
    
    Exit Sub
errProcess:
    dprint 0, 0, Err.Description
    Exit Sub
    Resume  'debug
End Sub

Private Function DicIndex(id As String, ByRef s As String) As Integer
    Dim i As Integer
    Dim p As Integer
    
    ' inds(i).num_found is set only for dictionary items with asterisk (*)
    ' in column 3 of the dictionary file.

    DicIndex = 0
    For i = 1 To dicnum
        If (id = dicary(i).eventid) Then
            If (Trim$(dicary(i).chartkey) <> "") Then
                p = InStr(1, s, dicary(i).chartkey, vbTextCompare)
                If (p > 0) Then
                    DicIndex = i
                    'Remove the charting text in prep for another eval.
                    '**** IS THIS FEATURE USED ANYWHERE?  (NOT IN THIS VERSION)  Don't modify s
                    's = Replace(s, dicary(i).chartkey, "", , , vbTextCompare)
                    dprint -3, 0, "Dictionary item found=" & i & " event=" & id & " result=" & s & " ind=" & dicary(i).indwinpfs
                    Exit For
                End If
            Else 'don't care about matching chartkey
                DicIndex = i
                dprint -3, 0, "Dictionary item found=" & i & " event=" & id & " result=%don't care%" & " ind=" & dicary(i).indwinpfs
                Exit For
            End If
        End If
    Next i
    
    'Debug.Print "("; id; "/"; s; ") not found"
End Function

'Private Function mhDicIndex(id As String, ByRef s As String) As Integer
'    Dim i As Integer
'    Dim p As Integer
'
'    mhDicIndex = 0
'    For i = 1 To mhdicnum
'        If (id = mhdicary(i).eventid) Then
'            If (Trim$(mhdicary(i).chartkey) <> "") Then
'                p = InStr(1, s, mhdicary(i).chartkey, vbTextCompare)
'                If (p > 0) Then
'                    mhDicIndex = i
'                    'Remove the charting text in prep for another eval.
'                    s = Replace(s, mhdicary(i).chartkey, "", , , vbTextCompare)
'                    dprint 0, 0, "Dictionary item found=" & i & " id=" & id & " charting=" & s & " mapping to=" & mhdicary(i).indwinpfs
'                    Exit For
'                End If
'            Else 'don't care about matching chartkey
'                mhDicIndex = i
'                dprint 0, 0, "Dictionary item found=" & i & " id=" & id & " charting=%don't care%" & " mapping to=" & mhdicary(i).indwinpfs
'                Exit For
'            End If
'        End If
'    Next i
'End Function

Private Sub InitIndicators()
    Dim i As Integer
    
    For i = 1 To MAX_INDICATORS
        inds(i).checked = False
        inds(i).also_mark = ""
        mhinds(i).checked = False
        mhinds(i).also_mark = ""
    Next i
    
End Sub
Private Sub InitGroups()
    Dim i As Integer
    
    For i = 1 To MAX_INDICATORS
        grps(i) = 0
    Next i
    
    For i = 1 To 4
        grps(i) = 1
    Next i
    For i = 8 To 9
        grps(i) = 2
    Next i
    For i = 11 To 13
        grps(i) = 3
    Next i
    For i = 14 To 16
        grps(i) = 4
    Next i
    For i = 17 To 19
        grps(i) = 5
    Next i
    For i = 20 To 22
        grps(i) = 6
    Next i
    For i = 24 To 26
        grps(i) = 7
    Next i
    
    
    For i = 1 To MAX_INDICATORS
        mhgrps(i) = 0
    Next i
    
    For i = 1 To 4
        mhgrps(i) = 1
    Next i
    For i = 9 To 10
        mhgrps(i) = 2
    Next i
    For i = 11 To 13
        mhgrps(i) = 3
    Next i
    For i = 14 To 15
        mhgrps(i) = 4
    Next i
    For i = 22 To 24
        mhgrps(i) = 5
    Next i
    
End Sub

Private Function ItemExists(col As Collection, key As String) As Boolean
    Dim dummy As Variant
    On Error Resume Next
    dummy = col.Item(key)
    ItemExists = (Err <> 5)
End Function

Private Sub AtLeastOneADL()
    Dim ind As Integer
    '03/23/06: ADL 2 is the minimum as decided in phone mtg on 3/22/06
    If Not (inds(1).checked Or inds(2).checked Or inds(3).checked) Then
        ind = 2
        inds(ind).checked = True
        dprint ind, 1, "via AtLeastOneADL"
    End If
End Sub

'Private Sub mhAtLeastOneADL()
'
'    If Not (mhinds(1).checked Or mhinds(2).checked Or mhinds(3).checked Or mhinds(4).checked) Then
'        mhinds(2).checked = True
'        dprint 1, 1, "via AtLeastOneADL"
'    End If
'End Sub

Private Sub ParseAlsoMark(s As String)
    Dim comma_pos As Integer
    Dim ind As Integer
    
    'for example:   29,31  is the also_mark string for RESTRAINTS
    
    Do
        comma_pos = InStr(s, ",")
        If (comma_pos = 0) Then
            ind = val(s)
        Else
            ind = val(Mid$(s, 1, comma_pos - 1))
            s = Mid$(s, comma_pos + 1, Len(s) - comma_pos)
        End If
        
        inds(ind).checked = True
        dprint ind, 1, "via Also-mark"
        
    Loop Until comma_pos = 0
End Sub
Private Sub mhParseAlsoMark(s As String)
    Dim comma_pos As Integer
    Dim ind As Integer
    
    'for example:   29,31  is the also_mark string for RESTRAINTS
    
    Do
        comma_pos = InStr(s, ",")
        If (comma_pos = 0) Then
            ind = val(s)
        Else
            ind = val(Mid$(s, 1, comma_pos - 1))
            s = Mid$(s, comma_pos + 1, Len(s) - comma_pos)
        End If
        
        mhinds(ind).checked = True
        dprint ind, 1, "via Also-mark"
        
    Loop Until comma_pos = 0
End Sub


Private Sub ParsePatientInfo(s As String)
    Dim fullname As String
    Dim commapos As Integer
    
    acctnum = Trim$(Mid$(s, START_ACCT_NUM, LEN_ACCT_NUM))
    
    fullname = Trim$(Mid$(s, START_PTNAME, LEN_PTNAME))
    commapos = InStr(fullname, ",")
    If commapos = 0 Then
        lastname = fullname
        firstname = ""
    Else
        lastname = Mid$(fullname, 1, commapos - 1)
        firstname = Mid$(fullname, commapos + 1, Len(fullname) - commapos)
    End If
    
    unitname = Trim$(Mid$(s, START_UNIT, LEN_UNIT))
    roomname = Trim$(Mid$(s, START_RM, LEN_RM))
    bedname = Trim$(Mid$(s, START_BED, LEN_BED))
    If Not efftimeon Then
        'no -efftime parameter; use date/time from patient header
        If IsDate(Mid$(s, START_CLASS_DT, LEN_DT)) Then
            intime = Trim$(Mid$(s, START_CLASS_DT, LEN_DT))
        End If
    End If

    Debug.Print "Acct="; acctnum
    Debug.Print "Name="; fullname
    Debug.Print "Unit="; unitname
    Debug.Print "Room="; roomname
    Debug.Print "Bed ="; bedname
End Sub


Private Sub AssembleOutput()
    On Error GoTo errHandler
    Dim outstr As String
    Dim i As Integer
    Dim din As Date
    Dim tin As Date
    Dim ok As Boolean
    
    ok = (acctnum <> "") And (unitname <> "")
'    ok = False
'    Select Case UCase$(unitname)
'        Case "A4W", "A6M", "A9M"
'            ok = True
'    End Select
    
    If Not ok Then
        Exit Sub
    End If
'    outstr = Space$(9) & unitname
'    outstr = outstr & Space$(26 - Len(outstr))
'    outstr = outstr & unitname '27
'    If telemetry And UCase$(unitname) = "A9M" Then
'        outstr = outstr & Space$(43 - Len(outstr))
'        outstr = outstr & "Telemetry"
'    ElseIf emus And UCase$(unitname) = "A4E" Then
'        outstr = outstr & Space$(43 - Len(outstr))
'        outstr = outstr & "EMUS"
'    ElseIf nimcu Then
'        Select Case UCase$(roomname)
'            Case "A405", "A406", "A407", "A408", "A409", "A410"
'                outstr = outstr & Space$(43 - Len(outstr))
'                outstr = outstr & "NIMCU"
'        End Select
'    End If
'    outstr = outstr & Space$(60 - Len(outstr))
'    outstr = outstr & Mid$(intime, 1, 8) & Space$(1) '61
''    outstr = outstr & classdate & Space$(1) '61
'    outstr = outstr & acctnum & Space$(20 - Len(acctnum) + 1) '70
'    outstr = outstr & lastname & Space$(32 - Len(lastname) + 1) '91
'    outstr = outstr & firstname & Space$(32 - Len(firstname) + 1) '124
    
'    If unitname = "A7H" Then
'        unitname = "A7T"
'    ElseIf unitname = "CL2" Or unitname = "CL3" Or unitname = "CL4" Then
'        unitname = "CL1"
'    ElseIf unitname = "CV2" Then
'        unitname = "CV1"
'    ElseIf unitname = "CP2" Then
'        unitname = "CP1"
'    ElseIf unitname = "CPD" Then
'        unitname = "CLD"
'    End If
    
'    'Add postfix letter onto acct num.
'    acctnum = acctnum & Mid$(unitname, 1, 1)

    intime = Left$(intime, 12)                  'yyyymmddhhnn

    outstr = Space$(8 + 1)  'facility code
    outstr = outstr & unitname      'unit name
    outstr = outstr & Space$(26 - Len(outstr)) '26
    outstr = outstr & unitname      'unit code
    If telemetry Then
        outstr = outstr & Space$(43 - Len(outstr))  '43
        outstr = outstr & "telemetry"
    End If
    outstr = outstr & Space$(60 - Len(outstr)) '60
    '(do not output the class date)
    outstr = outstr & Space$(69 - Len(outstr))      '69
    outstr = outstr & acctnum & Space$(20 - Len(acctnum) + 1) '90
    outstr = outstr & lastname & Space$(32 - Len(lastname) + 1) '123
    outstr = outstr & firstname & Space$(32 - Len(firstname) + 1) '156
    outstr = outstr & Space$(189 - Len(outstr)) '189
    outstr = outstr & roomname & Space$(8 - Len(roomname) + 1) '198
    outstr = outstr & bedname

    outstr = outstr & Space$(203 - Len(outstr))      '203
    outstr = outstr & intime                    'class datetime
    
    outstr = outstr & Space$(295 - Len(outstr)) '295
    outstr = outstr & intime                    'class IN time
    
    'Get ready for indicators
    outstr = outstr & Space$(378 - Len(outstr)) '378
    
    If ValidMSUnit(unitname) Then
        For i = 1 To MAX_INDICATORS
            If (inds(i).checked) Then
                outstr = outstr & "Y"
            Else
                outstr = outstr & "N"
            End If
        Next i
    ElseIf ValidMHUnit(unitname) Then
        For i = 1 To MAX_INDICATORS
            If (mhinds(i).checked) Then
                outstr = outstr & "Y"
            Else
                outstr = outstr & "N"
            End If
        Next i
    End If
    
    Print #outfile, outstr
    Print #outlogfile, outstr
    Exit Sub
    
errHandler:
    Debug.Print Err.Description
    Exit Sub
    Resume
End Sub


Private Sub CalcFrequency(p As Integer)
    Dim n As Integer
    Dim r As Single
    
'    r = range / RANGE_DENOMINATOR
    If ValidMSUnit(unitname) Then
        dicary(p).act_freq = range / dicary(p).num_found 'denom will never be zero
    ElseIf ValidMHUnit(unitname) Then
        mhdicary(p).act_freq = range / mhdicary(p).num_found 'denom will never be zero
    End If
' Use 8 hours as basis
'    Select Case dicary(p).indwinpfs
'        Case 14 To 22 'indicators 14-22 are to be counted instead of freq.
'            If dicary(p).num_found >= 4 * r And dicary(p).num_found <= 12 * r Then
'                dicary(p).act_freq = 240
'            ElseIf dicary(p).num_found > 12 * r And dicary(p).num_found <= 24 * r Then
'                dicary(p).act_freq = 60
'            ElseIf dicary(p).num_found > 24 * r Then
'                dicary(p).act_freq = 30
'            Else
'                dicary(p).act_freq = 480
'            End If
'        Case Else   ' Other values.
'            dicary(p).act_freq = range / dicary(p).num_found 'denom will never be zero
'    End Select

End Sub

Private Function CerDateToISO(d As String) As String
    Dim century As String
    ' Yields a string of the format yyyymmddhhnn from mm-dd-yy hh:nn
    
    If val(Mid$(d, 7, 2)) <= 50 Then
        century = "20"
    Else
        century = "19"
    End If
    CerDateToISO = century & Mid$(d, 7, 2) & Mid$(d, 1, 2) & Mid$(d, 4, 2) _
     & Mid$(d, 10, 2) & Mid$(d, 13, 2)

End Function

Private Sub ResetDictionaryFields()
    Dim i As Integer
    
    For i = 1 To dicnum
        dicary(i).num_found = 0
        dicary(i).act_freq = 0
        dicary(i).last_datetime = ""
        dicary(i).gavefreq = False
        dicary(i).one_row_find = 0
    Next i

    For i = 1 To mhdicnum
        mhdicary(i).num_found = 0
        mhdicary(i).act_freq = 0
        mhdicary(i).last_datetime = ""
        mhdicary(i).gavefreq = False
        mhdicary(i).one_row_find = 0
    Next i
End Sub

Private Sub MakeLogFiles()
    Const HKEY_LOCAL_MACHINE = &H80000002
    Dim dt As Variant
    
    If Not g_util.GetKeyValue(HKEY_LOCAL_MACHINE, "SOFTWARE\Quadramed\WinPFS", "Directory", winpfspath) Then
        winpfspath = App.Path
    End If

    If g_util.DirExists(winpfspath) Then
        winlogpath = winpfspath & "\log"
        winloadpath = winpfspath & "\load_me"
        If Not g_util.DirExists(winpfspath) Then
            MkDir$ (winlogpath)
        End If
    Else
        winlogpath = App.Path
        winloadpath = winlogpath
    End If

' For Temporary Testing
'    winlogpath = "c:\tc\log"
'    winloadpath = "c:\tc\load_me"
    
    
    dt = nowdt
    inlogname = winlogpath & "\TranspIn_" & Format$(dt, "mmdd") & ".log"
    outlogname = winlogpath & "\TranspOut_" & Format$(dt, "mmdd") & ".log"
    dbugname = winlogpath & "\DbugTransp_" & Format$(dt, "mmdd") & ".log"
    
    inlogfile = FreeFile
    Open inlogname For Append As #inlogfile
    Print #inlogfile, "**** WinPFS Transparent Classification Input    Time=" & nowdt & " ****"
    
    outlogfile = FreeFile
    Open outlogname For Append As #outlogfile
    Print #outlogfile, "**** WinPFS Transparent Classification Output    Time=" & nowdt & " ****"
    
    If dbugon Then
        dbugfile = FreeFile
        Open dbugname For Append As #dbugfile
        Print #dbugfile, "****TRANSPARENT TRANSLATION DEBUGGING MODE    Time=" & nowdt & " ****"
    End If
    
End Sub

Private Sub CloseLogFiles()

    Close #inlogfile
    Close #outlogfile
    If dbugon Then
        Close #dbugfile
    End If

    'inlogfile's dt = mid$(inlogname,len(inlogname)-len("mmdd.txt")+1,len("mmddhh.txt"))
'    FileCopy outfn, winlogpath & "\TranspOut_" & Mid$(inlogname, Len(inlogname) - Len("mmddhh.txt") + 1, Len("mmddhh.txt"))

End Sub

Private Sub DeleteOldLogs()
    Dim temp As String
    Dim i As Integer
    Dim dt As Variant

    ' Delete old log files
    ' (allow the interface to be down for up to 5 days)
    dt = DateAdd("d", -TRANSP_INLOG_FILE_LIFE, nowdt)
    For i = 1 To 5
        temp = winlogpath & "\TranspIn_" & Format$(dt, "mmdd") & ".log"
        If g_util.FileExists(temp) Then
            Kill temp
        End If
        dt = DateAdd("d", -1, dt)
    Next i

    dt = DateAdd("d", -TRANSP_OUTLOG_FILE_LIFE, nowdt)
    For i = 1 To 5
        temp = winlogpath & "\TranspOut_" & Format$(dt, "mmdd") & ".log"
        If g_util.FileExists(temp) Then
            Kill temp
        End If
        dt = DateAdd("d", -1, dt)
    Next i
    
    dt = DateAdd("d", -TRANSP_DEBUG_FILE_LIFE, nowdt)
    For i = 1 To 5
        temp = winlogpath & "\DbugTransp_" & Format$(dt, "mmdd") & ".log"
        If g_util.FileExists(temp) Then
            Kill temp
        End If
        dt = DateAdd("d", -1, dt)
    Next i

End Sub


Private Sub dprint(ind As Integer, status As Integer, s As String)
    Dim sstat As String

    If Not dbugon Or suppressDbugLog Then
        Exit Sub
    End If
    
    If ind = -1 Then
        Print #dbugfile, s
    ElseIf ind = -2 Then
        Print #dbugfile, "The following event was Out of Range and was rejected."
        Print #dbugfile, s
    ElseIf ind = -3 Then
        Print #dbugfile, "  * " & s
    ElseIf ind = 0 Then
        Print #dbugfile, "    " & s
    Else
        If (status = 1) Or (status = -1) Then
            sstat = "-TURNED ON-"
        Else
            sstat = "   "
        End If
        Print #dbugfile, "  * Indicator " & ind & sstat & s
    End If
            
End Sub
Private Function ValidMSUnit(u As String) As Boolean
    Dim i As Integer
    
    For i = 1 To unitnum
        If UCase$(u) = MSunitary(i) Then
            ValidMSUnit = True
            Exit For
        End If
    Next i

End Function
Private Function ValidMHUnit(u As String) As Boolean
    Dim i As Integer
    
    For i = 1 To mhunitnum
        If UCase$(u) = MHunitary(i) Then
            ValidMHUnit = True
            Exit For
        End If
    Next i

End Function

'Private Sub Check1()
'Private Sub Check2_3_4()
'Private Sub Check6()
'Private Su ()
'Private Sub Check8()
'Private Sub Check9()
'Private Sub Check11()
'Private Sub Check12_13()
'Private Sub Check14_15_16()
'Private Sub Check17_18_19()
'Private Sub Check20_21_22()
'Private Sub Check23()
'Private Sub Check24_25_26()
'Private Sub Check27()
'Private Sub Check28()

Private Sub Check1()
    Dim charting As String
    Dim i1, i2, i3 As Single
    
    suppressDbugLog = True
    
'    charting = "self"
'    i1 = DicIndex("16095", charting)
'
'    charting = "activity ad lib"
'    i2 = DicIndex("16992", charting)
    
    charting = "self"
    i3 = DicIndex("33405", charting)
    suppressDbugLog = False
    
'    If (dicary(i1).num_found > 0 Or dicary(i2).num_found > 0) And _
'       dicary(i3).num_found > 0 Then
'       suppressDbugLog = False
'       inds(1).checked = True
'       dprint 1, 1, "Triggered by (16095 or 16992) and 33405"
'    End If
    If dicary(i3).num_found > 0 Then
       inds(1).checked = True
       dprint 1, 1, "Triggered by 33405 self"
    End If

End Sub

Private Sub Check2_3_4()
    Dim charting As String
    Dim i9, i9a, i9b As Single
    
    suppressDbugLog = True
    
    If inds(3).checked Then
        Exit Sub
    End If
    
    charting = "unresponsive"
    i9 = DicIndex("5929", charting)
'    charting = "respond to pain"
'    i9a = DicIndex("5929", charting)
'    charting = "respond to voice"
'    i9b = DicIndex("5929", charting)
   
    suppressDbugLog = False
    
'    If (dicary(i9).num_found > 0) Or (dicary(i9a).num_found > 0) Or (dicary(i9b).num_found > 0) Then
    If (dicary(i9).num_found > 0) Then
       inds(3).checked = True
       dprint 3, 1, "Triggered by 5929 unresponsive"  '/respond to pain/voice"
    End If
    

End Sub

Private Sub Check6()
    Dim charting As String
    Dim idic, ibegin, iend, i9, i9a, i9b, i9c, i9d, i9e, i1, i2, i3 As Single
    
    suppressDbugLog = True
    
    If inds(6).checked Then
        Exit Sub
    End If
    
    'Find Beginning of indicator 6 in Dictionary with 5929 factor
    '6*20425                             amplification
    charting = "amplification"
    ibegin = DicIndex("20425", charting)
    'Find Ending of ind 6:
    '6*35542                             non-English
    charting = "non-English"
    iend = DicIndex("35542", charting)
    
    charting = "unresponsive"
    i9 = DicIndex("5929", charting)
    charting = "respond to pain"
    i9a = DicIndex("5929", charting)
    charting = "respond to voice"
    i9b = DicIndex("5929", charting)
    charting = "sedated"
    i9c = DicIndex("5929", charting)
    charting = "anesthesized"
    i9d = DicIndex("5929", charting)
    charting = "paralytic"
    i9e = DicIndex("5929", charting)
    
    suppressDbugLog = False
    
    If dicary(i9).num_found = 0 And dicary(i9a).num_found = 0 Then
        For idic = ibegin To iend
            If dicary(idic).num_found > 0 Then
                If dicary(idic).eventid = "24359" Or dicary(idic).eventid = "24363" Or dicary(idic).eventid = "24364" Or dicary(idic).eventid = "33423" Then
                    If dicary(i9b).num_found > 0 Or dicary(i9c).num_found > 0 Or dicary(i9d).num_found > 0 Or dicary(i9e).num_found > 0 Then
                        dprint 6, 1, "NOT Triggered due to respond to voice,sedated,anesth,paraly"
                    Else
                        inds(6).checked = True
                        dprint 6, 1, "Triggered by not(unresposive,respond to pain) & something between amplification and non-English"
                    End If
                Else
                    inds(6).checked = True
                    dprint 6, 1, "Triggered by not(unresposive,respond to pain) & something between amplification and non-English"
                End If
            End If
        Next idic
    End If
    
    If inds(6).checked Then
        Exit Sub
    End If
    
    suppressDbugLog = True
    charting = "capped"
    i1 = DicIndex("9506", charting)
    charting = "trach"
    i2 = DicIndex("35253", charting)
    charting = "et tube"
    i3 = DicIndex("35253", charting)
    charting = "awake"
    i9 = DicIndex("5929", charting)
    suppressDbugLog = False

    If (dicary(i1).num_found > 0 Or dicary(i2).num_found > 0 Or dicary(i3).num_found > 0) And _
       dicary(i9).num_found > 0 Then
        inds(6).checked = True
        dprint 6, 1, "Triggered by (capped or trach or et tube) & Awake"
    End If
    

End Sub

Private Sub Check7()
    Dim charting As String
    Dim i1, i1a, i1b, i1c, i1d, i1e, i1f, i1g As Single
    suppressDbugLog = True
'
'    If inds(7).checked Then
'        Exit Sub
'    End If
'
'    '7*34073                             alt mental stat
'    charting = "alt ment"
'    i1 = DicIndex("34073", charting)
'    suppressDbugLog = False
'
'    If dicary(i1).num_found > 0 Then
'        inds(7).checked = True
'        dprint 7, 1, "Triggered by alt ment"
'    End If
        charting = "alterd ment"
        i1a = DicIndex("24359", charting)
        charting = "alterd ment"
        i1b = DicIndex("24363", charting)
        charting = "alterd ment"
        i1c = DicIndex("24364", charting)
        charting = "alt ment"
        i1d = DicIndex("34073", charting)
        charting = "confused"
        i1e = DicIndex("5929", charting)
        charting = "combative"
        i1f = DicIndex("5929", charting)
        charting = "agitated"
        i1g = DicIndex("5929", charting)
      suppressDbugLog = False
        If dicary(i1a).num_found + _
           dicary(i1b).num_found + _
           dicary(i1c).num_found + _
           dicary(i1d).num_found + _
           dicary(i1e).num_found + _
           dicary(i1f).num_found + _
           dicary(i1g).num_found > 0 Then
            inds(7).checked = True
            dprint 7, 1, "Triggered by altered mental status or confused/combative/agitated"
        End If
        
    
End Sub

Private Sub Check8()
    Dim charting As String
    Dim i1, i2, i3, i1a, i1b, i1c, i1d, i1e, i1f, i1g As Single
    Dim maxcount1 As Integer
'123456789012345678901234567890
' 8*33682             30              patient checked
' 8*34064             120
' 8*34070             120
' 8*34072             120
' 8*34074                             bed exit alarm on
' 8*34074                             near nur station
' 8*34074                             toileting assist
' assume data range is 24 hours=1440 mins

    suppressDbugLog = True
    
    If inds(8).checked Then
        Exit Sub
    End If
    
'    If range >= 1440 Then
'        maxcount1 = 12
'    ElseIf range >= 480 Then
'        maxcount1 = 8
'    End If
'
'    charting = "patient checked"
'    i1 = DicIndex("33682", charting)
'    suppressDbugLog = False
'    If dicary(i1).num_found >= maxcount1 Then
'        inds(8).checked = True
'        dprint 8, 1, "Triggered by patient checked f=" & str(dicary(i1).num_found)
'    End If
'
'    suppressDbugLog = True
'    charting = "pt at nsg"
'    i1 = DicIndex("33682", charting)
'    suppressDbugLog = False
'    If dicary(i1).num_found >= maxcount1 Then
'        inds(8).checked = True
'        dprint 8, 1, "Triggered by pt at nsg desk f=" & str(dicary(i1).num_found)
'    End If

' 7#24359                                         alterd ment
' 7#24363                                         alterd ment
' 7#24364                                         alterd ment
' 7#34073                                         alt ment
' 7@5929                                          confused
' 7@5929                                          combative
' 7@5929                                          agitated
    
    charting = "patient checked"
    i1 = DicIndex("33682", charting)
    suppressDbugLog = False
    If dicary(i1).num_found > 0 Then
        suppressDbugLog = True
    
        charting = "alterd ment"
        i1a = DicIndex("24359", charting)
        charting = "alterd ment"
        i1b = DicIndex("24363", charting)
        charting = "alterd ment"
        i1c = DicIndex("24364", charting)
        charting = "alt ment"
        i1d = DicIndex("34073", charting)
        charting = "confused"
        i1e = DicIndex("5929", charting)
        charting = "combative"
        i1f = DicIndex("5929", charting)
        charting = "agitated"
        i1g = DicIndex("5929", charting)
        suppressDbugLog = False
        
        If dicary(i1a).num_found + _
           dicary(i1b).num_found + _
           dicary(i1c).num_found + _
           dicary(i1d).num_found + _
           dicary(i1e).num_found + _
           dicary(i1f).num_found + _
           dicary(i1g).num_found > 0 Then
            inds(8).checked = True
            dprint 8, 1, "Triggered by patient checked + other"
        End If
    End If
    
    suppressDbugLog = True
    charting = "pt at nsg"
    i1 = DicIndex("33682", charting)
    suppressDbugLog = False
    If dicary(i1).num_found > 0 Then
        inds(8).checked = True
        dprint 8, 1, "Triggered by pt at nsg desk"
    End If

    
    If range >= 1440 Then
        maxcount1 = 6
    ElseIf range >= 480 Then
        maxcount1 = 3
    End If
    suppressDbugLog = True
    charting = ""
    i1 = DicIndex("34064", charting)
    suppressDbugLog = False
    If dicary(i1).num_found >= maxcount1 Then
        inds(8).checked = True
        dprint 8, 1, "Triggered by 34064 f=" & str(dicary(i1).num_found)
    End If
    
    suppressDbugLog = True
    charting = ""
    i1 = DicIndex("34070", charting)
    suppressDbugLog = False
    If dicary(i1).num_found > 0 Then
        inds(33).checked = True
    End If
    If dicary(i1).num_found >= maxcount1 Then
        inds(8).checked = True
        dprint 8, 1, "Triggered by 34070 f=" & str(dicary(i1).num_found)
    End If
    
    suppressDbugLog = True
    charting = ""
    i1 = DicIndex("34072", charting)
    suppressDbugLog = False
    If dicary(i1).num_found >= maxcount1 Then
        inds(8).checked = True
        dprint 8, 1, "Triggered by 34072 f=" & str(dicary(i1).num_found)
    End If
    
' 8*34074                             bed exit alarm on
' 8*34074                             near nur station
' 8*34074                             toileting assist
    suppressDbugLog = True
    charting = "bed exit alrm on"
    i1 = DicIndex("34074", charting)
    charting = "near nur station"
    i2 = DicIndex("34074", charting)
    charting = "toil"
    i3 = DicIndex("34074", charting)
    
    suppressDbugLog = False
    If dicary(i1).num_found > 0 And dicary(i2).num_found > 0 And _
       dicary(i3).num_found > 0 Then
        inds(8).checked = True
        dprint 8, 1, "Triggered by bed exit alarm,near nur sta, toileting asst"
    End If
    

End Sub


Private Sub Check9()
    Dim charting As String
    Dim i1 As Single
    Dim maxcount1 As Integer
' 9 23265
' 9*6355              60              suicidal ideatns

    If inds(9).checked Then
        Exit Sub
    End If
    suppressDbugLog = True
    
    If range >= 1440 Then
        maxcount1 = 12
    ElseIf range >= 720 Then
        maxcount1 = 6
    ElseIf range >= 480 Then
        maxcount1 = 4
    End If
    
    charting = "suicidal ideatns"
    i1 = DicIndex("6355", charting)
    suppressDbugLog = False
    If dicary(i1).num_found >= maxcount1 Then
        inds(9).checked = True
        dprint 9, 1, "Triggered by 6355 f=" & str(dicary(i1).num_found)
    End If

End Sub

'Private Sub Check11()
'    Dim charting As String
'    Dim i1, i2, i3, i4, i5, i5a, i6, i7, i8 As Single
'    Dim countIntake, countOutput As Single
'    Dim ibegin, iend, idic As Single
'    Dim maxcount1, maxcount2, maxcount3 As Single
'
'End Sub

Private Sub Check12_13()
    Dim charting As String
    Dim idic, ibegin, iend As Single
    Dim i1, i1a, i1b, i2, i3, i4 As Single
    Dim maxcount12, maxcount13 As Integer
'12*6240              120             NPO
'12*6240              120             NPO except meds
'12*6240              120             NPO with chips
'12*6913              120
'12*16966             120
'12*21429             120
'12*35333             120             CBI Flowsheet

    suppressDbugLog = True

    charting = "NPO"
    ibegin = DicIndex("6240", charting)
    charting = "CBI Flowsheet"
    iend = DicIndex("35333", charting)

    charting = ""
    i2 = DicIndex("6913", charting)

    charting = ""
    i3 = DicIndex("16966", charting)

    charting = ""
    i4 = DicIndex("34072", charting)
    suppressDbugLog = False
    
    If dicary(i2).num_found >= 0 Or dicary(i3).num_found >= 0 Or dicary(i4).num_found >= 0 Then
        inds(11).checked = True
        dprint 11, 1, "Triggered by 6913 or 16966 or 34072"
    End If
    
    If range >= 1440 Then
        maxcount12 = 6
        maxcount13 = 12
    ElseIf range >= 720 Then
        maxcount12 = 3
        maxcount13 = 6
    ElseIf range >= 480 Then
        maxcount12 = 3
        maxcount13 = 4
    End If
    
    For idic = ibegin To iend
        If dicary(idic).num_found >= maxcount12 Then
            inds(12).checked = True
            dprint 12, 1, "Triggered by 6240,16966,21429,35333"
        End If
        If dicary(idic).num_found >= maxcount13 Then
            inds(13).checked = True
            dprint 13, 1, "Triggered by 6240,16966,21429,35333"
        End If
    Next idic

End Sub


Private Sub Check14_15_16()
    Dim charting As String
    Dim ibegin, iend, idic, count6119, maxcount1, maxcount2, maxcount3 As Single
    Dim ipapermar As Single
    Dim i6879, i6880, i6881 As Single
'14*6119              240             epidural
'...
'14*40717             240

    suppressDbugLog = True
    
    count6119 = 0
    
    charting = "epidural"
    ibegin = DicIndex("6119", charting)
    charting = "pca pump"
    iend = DicIndex("6119", charting)
    
    charting = ""
    ipapermar = DicIndex("43817", charting)
    
    suppressDbugLog = False
    
    For idic = ibegin To iend
        count6119 = count6119 + dicary(idic).num_found
    Next idic
    
    maxcount1 = 5
    maxcount2 = 12
    maxcount3 = 24
    If range >= 1440 Then
        maxcount1 = 5
        maxcount2 = 12
        maxcount3 = 24
    ElseIf range >= 720 Then
        maxcount1 = 3
        maxcount2 = 6
        maxcount3 = 12
    ElseIf range >= 480 Then
        maxcount1 = 2
        maxcount2 = 4
        maxcount3 = 8
    End If
    
'Check counts of 18098,6119
    If count18098 >= maxcount3 Or count6119 >= maxcount3 Then
        inds(16).checked = True
        dprint 16, 1, "Triggered by f(6119 or 18098) >= 24 hr level"
    ElseIf count18098 >= maxcount2 Or count6119 >= maxcount2 Then
        inds(15).checked = True
        dprint 15, 1, "Triggered by f(6119 or 18098) >= 12 hr level"
    ElseIf count18098 > maxcount1 Or count6119 >= maxcount1 Then
        inds(14).checked = True
        dprint 14, 1, "Triggered by f(6119 or 18098) >= 8 hr level"
    End If
    
'14*17002                         480             medicated
'14*40717                         480
    suppressDbugLog = True
    charting = "medicated"
    ibegin = DicIndex("17002", charting)
    charting = ""
    iend = DicIndex("41117", charting)
    
    charting = ""
    i6879 = DicIndex("6879", charting)
    charting = ""
    i6880 = DicIndex("6880", charting)
    charting = ""
    i6881 = DicIndex("6881", charting)
    
    suppressDbugLog = False
    
'Pull                    7am   5pm     8pm      5am
'Time frame      Prev 24 hr   7a-5p   7a-5p   7p-5a
'Classification Time     7a    11a     7p      11p
'A   q4 hrs          3 times  1 time  2 times  1 time
'B   q2 hrs          6 times  2 time  3 times  2 time
'C   q1 hr           6 times  4 time  6 times  4 time
'D   q 30 min       24 times  8 times 12 times 8 times
    maxcount1 = 3
    maxcount2 = 6
    maxcount3 = 24
    If range >= 1440 Then
        maxcount1 = 3
        maxcount2 = 6
        maxcount3 = 24
    ElseIf range >= 720 Then
        maxcount1 = 3
        maxcount2 = 6
        maxcount3 = 12
    ElseIf range >= 480 Then
        maxcount1 = 1
        maxcount2 = 4
        maxcount3 = 8
    End If
    
If dicary(i6879).num_found > 0 And dicary(i6880).num_found > 0 And dicary(i6881).num_found > 0 Then
    For idic = ibegin To iend
        If dicary(idic).num_found >= maxcount3 Then
            inds(16).checked = True
            dprint 16, 1, "Triggered by freq of " & dicary(idic).eventid & " >= 24 hr level"
        ElseIf dicary(idic).num_found >= maxcount2 Then
            inds(15).checked = True
            dprint 15, 1, "Triggered by freq of " & dicary(idic).eventid & " >= 12 hr level"
        ElseIf dicary(idic).num_found >= maxcount1 Then
            inds(14).checked = True
            dprint 14, 1, "Triggered by freq of " & dicary(idic).eventid & " >= 8 hr level"
        End If
    Next idic
Else
    dprint 0, 1, " 6879+6880+6881 NOT found"
End If
    
    If dicary(ipapermar).num_found >= maxcount3 Then
        inds(16).checked = True
        dprint 16, 1, "Triggered by f(paper MAR) >= 24 hr level"
    ElseIf dicary(ipapermar).num_found >= maxcount2 Then
        inds(15).checked = True
        dprint 15, 1, "Triggered by f(paper MAR) >= 12 hr level"
    ElseIf dicary(ipapermar).num_found >= maxcount1 Then
        inds(14).checked = True
        dprint 14, 1, "Triggered by f(paper MAR) >= 8 hr level"
    End If
    
End Sub


Private Sub Check17_18_19()
    Dim charting As String
    Dim ibegin, iend, idic As Single
    Dim maxcount1, maxcount2, maxcount3 As Single

    suppressDbugLog = True
    
'17*5793              240
'...
'17*41019             240
    charting = ""
    ibegin = DicIndex("5793", charting)
    charting = ""
    iend = DicIndex("41019", charting)
    
    suppressDbugLog = False


    maxcount1 = 6
    maxcount2 = 12
    maxcount3 = 24
    If range >= 1440 Then
        maxcount1 = 6
        maxcount2 = 12
        maxcount3 = 24
    ElseIf range >= 720 Then
        maxcount1 = 3
        maxcount2 = 6
        maxcount3 = 12
    ElseIf range >= 480 Then
        maxcount1 = 2
        maxcount2 = 4
        maxcount3 = 8
    End If

    For idic = ibegin To iend
        If dicary(idic).num_found >= maxcount3 Then
            inds(19).checked = True
            dprint 19, 1, "Triggered by f(" & dicary(idic).eventid & " at levels for 24 hours"
        ElseIf dicary(idic).num_found >= maxcount2 Then
            inds(18).checked = True
            dprint 18, 1, "Triggered by f(" & dicary(idic).eventid & " at levels for >=12 hours"
        ElseIf dicary(idic).num_found >= maxcount1 Then
            inds(17).checked = True
            dprint 17, 1, "Triggered by f(" & dicary(idic).eventid & " at levels for >=8 hours"
        End If
    Next idic
    
    'note that 16187 triggers 19 and 22
    

End Sub

Private Sub Check20_21_22()
    Dim charting As String
    Dim ibegin, iend, idic, i1, i1a, i1b, i1c, i1d As Single
    Dim maxcount1, maxcount2, maxcount3 As Single
    suppressDbugLog = True
    

    maxcount1 = 6
    maxcount2 = 12
    maxcount3 = 24
    If range >= 1440 Then
        maxcount1 = 6
        maxcount2 = 12
        maxcount3 = 24
    ElseIf range >= 720 Then
        maxcount1 = 3
        maxcount2 = 6
        maxcount3 = 12
    ElseIf range >= 480 Then
        maxcount1 = 2
        maxcount2 = 4
        maxcount3 = 8
    End If

'20*5749              240
'...
'20*35258             240
'20*40927             240
    charting = ""
    ibegin = DicIndex("5749", charting)
    charting = ""
    iend = DicIndex("40927", charting)
    
    suppressDbugLog = False
    For idic = ibegin To iend
        If dicary(idic).num_found >= maxcount3 Then
            inds(22).checked = True
            dprint 22, 1, "Triggered by f(" & dicary(idic).eventid & " at levels for 24 hours"
        ElseIf dicary(idic).num_found >= maxcount2 Then
            inds(21).checked = True
            dprint 21, 1, "Triggered by f(" & dicary(idic).eventid & " at levels for >=12 hours"
        ElseIf dicary(idic).num_found >= maxcount1 Then
            inds(20).checked = True
            dprint 20, 1, "Triggered by f(" & dicary(idic).eventid & " at levels for >=8 hours"
        End If
    Next idic
    
'20*5835              240             brisk
'20*5835              240             normal
'20*5835              240             sluggish
'21*5835              60
'22*5835              30
    suppressDbugLog = True
    charting = "brisk"
    i1 = DicIndex("5835", charting)
    charting = "normal"
    i1a = DicIndex("5835", charting)
    charting = "sluggish"
    i1b = DicIndex("5835", charting)
    charting = ""
    i1c = DicIndex("5835", charting)
    
    suppressDbugLog = False
    If dicary(i1).num_found >= 2 Or _
       dicary(i1a).num_found >= 2 Or _
       dicary(i1b).num_found >= 2 Then
        inds(20).checked = True
        dprint 20, 1, "Triggered by f(brisk, normal or sluggish)=2"
    End If
    
    If dicary(i1c).num_found >= maxcount3 Then
        inds(22).checked = True
        dprint 22, 1, "Triggered by f(5835)= at 24 hours"
    ElseIf dicary(i1c).num_found >= maxcount2 Then
        inds(21).checked = True
        dprint 21, 1, "Triggered by f(5835)= at >=12 hours"
    End If
    
'21*8                 60              flush bag chg
'21*8                 60              press bag flush
'21*8                 60              press tube chg
'21*8                 60              wave form damp
'21*8                 60              zeroed
    suppressDbugLog = True
    charting = "flush bag chg"
    i1 = DicIndex("8", charting)
    charting = "press bag flush"
    i1a = DicIndex("8", charting)
    charting = "press tube chg"
    i1b = DicIndex("8", charting)
    charting = "wave form damp"
    i1c = DicIndex("8", charting)
    charting = "zeroed"
    i1d = DicIndex("8", charting)
    
    suppressDbugLog = False
    If dicary(i1).num_found >= maxcount2 Or _
       dicary(i1a).num_found >= maxcount2 Or _
       dicary(i1b).num_found >= maxcount2 Or _
       dicary(i1c).num_found >= maxcount2 Or _
       dicary(i1d).num_found >= maxcount2 Then
        inds(21).checked = True
        dprint 21, 1, "Triggered by flush bag, tube chg, wave form, zeroed at f=12 hours"
    End If

'22*35086             30
'22*35087             30
'22*35088             30
'22*35089             30
    suppressDbugLog = True
    charting = ""
    i1 = DicIndex("35086", charting)
    charting = ""
    i1a = DicIndex("35087", charting)
    charting = ""
    i1b = DicIndex("35088", charting)
    charting = ""
    i1c = DicIndex("35089", charting)
    
    suppressDbugLog = False
    If dicary(i1).num_found >= maxcount3 Or _
       dicary(i1a).num_found >= maxcount3 Or _
       dicary(i1b).num_found >= maxcount3 Or _
       dicary(i1c).num_found >= maxcount3 Then
        inds(22).checked = True
        dprint 22, 1, "Triggered by 35806-35809 at f=24 hours"
    ElseIf dicary(i1).num_found >= maxcount2 Or _
       dicary(i1a).num_found >= maxcount2 Or _
       dicary(i1b).num_found >= maxcount2 Or _
       dicary(i1c).num_found >= maxcount2 Then
        inds(21).checked = True
        dprint 21, 1, "Triggered by 35806-35809 at f=12 hours"
    End If
    
End Sub

Private Sub Check23()
    Dim charting As String
    
    suppressDbugLog = True
    
    If event33765 Then
       inds(23).checked = True
    End If
    
    If event33775 And val33775 <= 16 Then
        inds(23).checked = True
    End If
    
    If event35286 And val35286 >= 6 Then
        inds(23).checked = True
    End If
    
    suppressDbugLog = False

End Sub


Private Sub Check24_25_26()
    Dim charting As String
    Dim ibegin, ibegin2, iend, idic, idic2, count, i As Single
    Dim eid As String

    suppressDbugLog = True
    
'24*25547
'24*25549
'24*25551
'24*25553
'24*25555
'24*25557
'24*25560
'24*25562
'24*25564
'24*25566
'24*25568
'24*25570
'24*25620
'24*25622
'24*25624
'24*25626
'24*25628
'24*25630
'24*27386
'24*27392
'...27479
    'should these be pro-rated according to the range?  Is it just these counts
    'as specified in the mapping doc.
    charting = ""
    ibegin = DicIndex("25547", charting)
    charting = ""
    iend = DicIndex("27479", charting)
    
    For idic = ibegin To iend
        If dicary(idic).one_row_find >= 3 And dicary(idic).indwinpfs = 26 Then
            inds(26).checked = True
            dprint 26, 1, "Triggered by f>=3 of " & dicary(idic).eventid
        ElseIf dicary(idic).one_row_find = 2 And dicary(idic).indwinpfs = 25 Then
            inds(25).checked = True
            dprint 25, 1, "Triggered by f=2 of " & dicary(idic).eventid
        ElseIf dicary(idic).one_row_find = 1 And dicary(idic).indwinpfs = 24 Then
            inds(24).checked = True
            dprint 24, 1, "Triggered by f=1 of " & dicary(idic).eventid
        End If
'        If dicary(idic).num_found >= 3 Then
'            inds(26).checked = True
'            dprint 26, 1, "Triggered by f>=3 of " & dicary(idic).eventid
'        ElseIf dicary(idic).num_found = 2 Then
'            inds(25).checked = True
'            dprint 25, 1, "Triggered by f=2 of " & dicary(idic).eventid
'        ElseIf dicary(idic).num_found = 1 Then
'            inds(24).checked = True
'            dprint 24, 1, "Triggered by f=1 of " & dicary(idic).eventid
'        End If
    Next idic
    
'Wound data only for 24
'24*33631                                         clean
'24*33631                                         drsg drng/intact
'24*33631                                         drsg dry/intact
'24*33631                                         reddened
'24*33631                                         staples
'24*33631                                         steri-strips
'24*33638                                         sutures
    For idic2 = 1 To 8
        count = 0
        eid = CStr(33630 + idic2)
        charting = "clean"
        ibegin = DicIndex(eid, charting)
        charting = "sutures"
        iend = DicIndex(eid, charting)
        
        For idic = ibegin To iend
            If dicary(idic).num_found > 0 Then
                count = count + 1
            End If
        Next idic
        
        If count >= 1 Then
            inds(24).checked = True
            dprint 24, 1, "Triggered by f>=1 of " & str(33630 + idic2)
        End If
    Next idic2

'Common wound data for 24-26
'24*33631                             bone visible
'...
'24*33638                             unapproximated
    For idic2 = 1 To 8
        count = 0
        eid = CStr(33630 + idic2)
        charting = "compress dev usd"
        ibegin = DicIndex(eid, charting)
        charting = "unapproximated"
        iend = DicIndex(eid, charting)
        
        For idic = ibegin To iend
            If dicary(idic).one_row_find >= 4 And dicary(idic).indwinpfs = 26 Then
                inds(26).checked = True
                dprint 26, 1, "Triggered by f>=4 of " & str(33630 + idic2)
            ElseIf dicary(idic).one_row_find >= 3 And dicary(idic).indwinpfs = 25 Then
                inds(25).checked = True
                dprint 25, 1, "Triggered by f>=3 of " & str(33630 + idic2)
            ElseIf dicary(idic).one_row_find >= 1 And dicary(idic).indwinpfs = 24 Then
                inds(24).checked = True
                dprint 24, 1, "Triggered by f>=1 of " & str(33630 + idic2)
            End If
'            If dicary(idic).num_found > 0 Then
'                count = count + 1
'            End If
        Next idic
        
'        If count >= 4 Then
'            inds(26).checked = True
'            dprint 26, 1, "Triggered by f>=4 of " & str(33630 + idic2)
'        ElseIf count = 3 Then
'            inds(25).checked = True
'            dprint 25, 1, "Triggered by f=3 of " & str(33630 + idic2)
'        ElseIf count >= 1 Then
'            inds(24).checked = True
'            dprint 24, 1, "Triggered by f>=1 of " & str(33630 + idic2)
'        End If
    Next idic2
    
'Wound data only for 25-26
'25*33638                                         bone visible
'25*33638                                         dehisced
'25*33638                                         sandbag intact
'25*33638                                         tendon visible
    For idic2 = 1 To 8
        count = 0
        eid = CStr(33630 + idic2)
        charting = "bone visible"
        ibegin = DicIndex(eid, charting)
        charting = "tendon visible"
        iend = DicIndex(eid, charting)
        
        For idic = ibegin To iend
            If dicary(idic).one_row_find >= 4 And dicary(idic).indwinpfs = 26 Then
                inds(26).checked = True
                dprint 26, 1, "Triggered by f>=4 of " & str(33630 + idic2)
            ElseIf dicary(idic).one_row_find >= 3 And dicary(idic).indwinpfs = 25 Then
                inds(25).checked = True
                dprint 25, 1, "Triggered by f>=3 of " & str(33630 + idic2)
            End If
'            If dicary(idic).num_found > 0 Then
'                count = count + 1
'            End If
        Next idic
        
'        If count >= 4 Then
'            inds(26).checked = True
'            dprint 26, 1, "Triggered by f>=4 of " & str(33630 + idic2)
'        ElseIf count >= 3 Then
'            inds(25).checked = True
'            dprint 25, 1, "Triggered by f>=3 of " & str(33630 + idic2)
'        End If
    Next idic2

    
'24*33724
'...
'24*33785
    charting = ""
    ibegin = DicIndex("33724", charting)
    charting = ""
    iend = DicIndex("33785", charting)
    
    For idic = ibegin To iend
        If dicary(idic).one_row_find >= 4 And dicary(idic).indwinpfs = 26 Then
            inds(26).checked = True
            dprint 26, 1, "Triggered by f>=4 of " & dicary(idic).eventid
        ElseIf dicary(idic).one_row_find = 3 And dicary(idic).indwinpfs = 25 Then
            inds(25).checked = True
            dprint 25, 1, "Triggered by f=3 of " & dicary(idic).eventid
        ElseIf dicary(idic).one_row_find >= 1 And dicary(idic).indwinpfs = 24 Then
            inds(24).checked = True
            dprint 24, 1, "Triggered by f>=1 of " & dicary(idic).eventid
        End If
'        If dicary(idic).num_found >= 4 Then
'            inds(26).checked = True
'            dprint 26, 1, "Triggered by f>=4 of " & dicary(idic).eventid
'        ElseIf dicary(idic).num_found = 3 Then
'            inds(25).checked = True
'            dprint 25, 1, "Triggered by f=3 of " & dicary(idic).eventid
'        ElseIf dicary(idic).num_found >= 1 Then
'            inds(24).checked = True
'            dprint 24, 1, "Triggered by f>=1 of " & dicary(idic).eventid
'        End If
    Next idic

'24*33639                             Stage II
'24*33640                             Stage II
'24*33641                             Stage II
'24*33642                             Stage II
'24*33643                             Stage II
'24*33644                             Stage II
'24*33645                             Stage II
'24*33646                             Stage II
'24*25535                             Stage II
'24*25537                             Stage II
'24*25539                             Stage II
'24*25541                             Stage II
'24*25543                             Stage II
'24*25545                             Stage II
    charting = "Stage II"
    ibegin = DicIndex("33639", charting)
    charting = "Stage II"
    iend = DicIndex("25545", charting)
    For idic = ibegin To iend
        If dicary(idic).num_found > 0 Then
            inds(24).checked = True
            dprint 24, 1, "Triggered by f>=1 of " & dicary(idic).eventid
        End If
    Next idic
    
    charting = "Stage III"
    ibegin = DicIndex("33639", charting)
    charting = "Stage III"
    iend = DicIndex("25545", charting)
    For idic = ibegin To iend
        If dicary(idic).num_found > 0 Then
            inds(25).checked = True
            dprint 25, 1, "Triggered by f>=1 of " & dicary(idic).eventid
        End If
    Next idic
    
    charting = "Stage IV"
    ibegin = DicIndex("33639", charting)
    charting = "Stage IV"
    iend = DicIndex("25545", charting)
    For idic = ibegin To iend
        If dicary(idic).num_found > 0 Then
            inds(26).checked = True
            dprint 26, 1, "Triggered by f>=1 of " & dicary(idic).eventid
        End If
    Next idic

'25*25572                             severe
'25*25572                             pitting
'25*25572                             weeping
    For idic2 = 1 To 6
        count = 0
        eid = CStr(25570 + idic2 * 2)
        charting = "moderate"
        ibegin = DicIndex(eid, charting)
        charting = "weeping"
        iend = DicIndex(eid, charting)
        
        For idic = ibegin To iend
            If dicary(idic).one_row_find >= 3 And dicary(idic).indwinpfs = 26 Then
                inds(26).checked = True
                dprint 26, 1, "Triggered by f>=3 of " & str(eid)
            ElseIf dicary(idic).one_row_find >= 2 And dicary(idic).indwinpfs = 25 Then
                inds(25).checked = True
                dprint 25, 1, "Triggered by f>=2 of " & str(eid)
            ElseIf dicary(idic).one_row_find >= 1 And dicary(idic).indwinpfs = 24 Then
                inds(24).checked = True
                dprint 24, 1, "Triggered by f>=1 of " & str(eid)
            End If
        Next idic
        
    Next idic2
    
'25*33710                             sheath art
'25*33710                             sheath venous
'25*33711                             sheath art
'25*33711                             sheath venous
'25*33712                             sheath art
'25*33712                             sheath venous
'25*33713                             sheath art
'25*33713                             sheath venous
'25*33714                             sheath art
'25*33714                             sheath venous
    
' 0*33677                             discontinued
' 0*33678                             discontinued
' 0*33679                             discontinued
' 0*33680                             discontinued
' 0*33681                             discontinued
    
    charting = "sheath art"
    ibegin = DicIndex("33710", charting)
    charting = "sheath venous"
    iend = DicIndex("33714", charting)
    
    charting = "discontinued"
    ibegin2 = DicIndex("33677", charting)
    
    For idic = ibegin To iend
        ' operator \ means div
        If dicary(idic).num_found > 0 And dicary(ibegin2 + (idic - ibegin) \ 2).num_found > 0 Then
            inds(25).checked = True
            dprint 25, 1, "Triggered by f>=1 of " & dicary(idic).eventid & " and (33677,discontinued)"
        End If
    Next idic

'26*27380                             pouch chg complex
'26*27380                             trough chg complex
'26*27380                             suction
'26*27380                             dressing changed
'26*27380                             periwnd sk trtmt
'26*27380                             system maintenan
'26*27380                             other
'24*27380
    charting = "pouch chg complex"
    ibegin = DicIndex("27380", charting)
    charting = "other"
    iend = DicIndex("27380", charting)
    count = 0
    For idic = ibegin To iend
        If dicary(idic).one_row_find >= 3 And dicary(idic).indwinpfs = 26 Then
            inds(26).checked = True
            dprint 26, 1, "Triggered by f>=3 of " & dicary(idic).eventid
        End If
    Next idic
    
'24*27380
    charting = ""
    idic = DicIndex("27380", charting)
    If dicary(idic).num_found > 0 Then
        inds(24).checked = True
        dprint 24, 1, "Triggered by f>=1 of " & dicary(idic).eventid
    End If
    
'26*27503                             pouch-complex
'26*27503                             trough-complex
'26*27503                             wound vac
'26*27503                             dsg dry
'26*27503                             cream/ointment
'26*27503                             suction
'26*27503                             other
    charting = "pouch-complex"
    ibegin = DicIndex("27503", charting)
    charting = "other"
    iend = DicIndex("27503", charting)
    count = 0
    For idic = ibegin To iend
        If dicary(idic).one_row_find >= 3 And dicary(idic).indwinpfs = 26 Then
            inds(26).checked = True
            dprint 26, 1, "Triggered by f>=3 of " & dicary(idic).eventid
        ElseIf dicary(idic).num_found >= 1 And dicary(idic).indwinpfs = 24 Then
            inds(24).checked = True
            dprint 24, 1, "Triggered by f>=1 of " & dicary(idic).eventid
        End If
    Next idic
    
    For idic = ibegin To ibegin + 2
        If dicary(idic).one_row_find >= 1 And dicary(idic).indwinpfs = 25 Then
            inds(25).checked = True
            dprint 25, 1, "Triggered by f>=1 of complex pouch/trough,woundvac " & dicary(idic).eventid
        End If
    Next idic
    
    For idic = ibegin + 3 To ibegin + 6
        If dicary(idic).one_row_find >= 2 And dicary(idic).indwinpfs = 25 Then
            inds(25).checked = True
            dprint 25, 1, "Triggered by f>=2 of dsg dry/cream/suct/other " & dicary(idic).eventid
        End If
    Next idic
    
'25*27505                             pouch chg complex
'25*27505                             trough chg complex
'25*27505                             suction
'25*27505                             dressing changed
'25*27505                             periwnd sk trtmt
'25*27505                             system maintenan
'25*27505                             other
    charting = "pouch chg complex"
    ibegin = DicIndex("27505", charting)
    charting = "other"
    iend = DicIndex("27505", charting)
    
    For idic = ibegin To ibegin + 1
        If dicary(idic).one_row_find = 2 And dicary(idic).indwinpfs = 25 Then
            inds(25).checked = True
            dprint 25, 1, "Triggered by f>=2 of pouch/trough complex " & dicary(idic).eventid
        End If
    Next idic
    
    For idic = ibegin + 2 To iend
        If dicary(idic).one_row_find = 2 And dicary(idic).indwinpfs = 25 Then
            inds(25).checked = True
            dprint 25, 1, "Triggered by f>=2 of " & dicary(idic).eventid
        End If
    Next idic

'26*27440                             pouch-complex
'26*27440                             barrier cream
'26*27440                             dry dsg
'26*27440                             foam
'26*27440                             tube stabilizer
'26*27440                             antifungals
'26*27440                             other
    charting = "pouch-complex"
    ibegin = DicIndex("27440", charting)
    charting = "other"
    iend = DicIndex("27440", charting)
    count = 0
    For idic = ibegin To iend
        If dicary(idic).one_row_find >= 3 And dicary(idic).indwinpfs = 26 Then
            inds(26).checked = True
            dprint 26, 1, "Triggered by f>=3 of " & dicary(idic).eventid
        ElseIf dicary(idic).num_found > 0 Then
            inds(24).checked = True
            dprint 24, 1, "Triggered by f>=1 of " & dicary(idic).eventid
        End If
    Next idic
    
    If dicary(ibegin).num_found > 0 Then
        inds(25).checked = True
        dprint 25, 1, "Triggered by pouch complex " & dicary(idic).eventid
    End If
    
    For idic = ibegin + 1 To iend
        If dicary(idic).one_row_find >= 2 And dicary(idic).indwinpfs = 25 Then
            inds(25).checked = True
            dprint 25, 1, "Triggered by f>=2 of " & dicary(idic).eventid
        End If
    Next idic
    
'26*27442                             pouch chg-complex
'26*27442                             skin care
'26*27442                             dressing changed
'26*27442                             AgNo3 hypergrnlt
'26*27442                             tube d/c
'26*27442                             other
    charting = "pouch chg-complex"
    ibegin = DicIndex("27442", charting)
    charting = "other"
    iend = DicIndex("27442", charting)
    count = 0
    For idic = ibegin To iend
        If dicary(idic).one_row_find >= 3 And dicary(idic).indwinpfs = 26 Then
            inds(26).checked = True
            dprint 26, 1, "Triggered by f>=3 of " & dicary(idic).eventid
        ElseIf dicary(idic).num_found > 0 Then
            inds(24).checked = True
            dprint 24, 1, "Triggered by f>=1 of " & dicary(idic).eventid
        End If
    Next idic
    
    If dicary(ibegin).num_found > 0 Then
        inds(25).checked = True
    End If
    
    For idic = ibegin + 1 To iend
        If dicary(idic).one_row_find >= 2 And dicary(idic).indwinpfs = 25 Then
            inds(25).checked = True
            dprint 25, 1, "Triggered by f>=2 of " & dicary(idic).eventid
        End If
    Next idic
    
'27501
    charting = ""
    idic = DicIndex("27501", charting)
    If dicary(idic).one_row_find >= 2 And dicary(idic).indwinpfs = 25 Then
        inds(25).checked = True
        dprint 25, 1, "Triggered by f>=2 of " & dicary(idic).eventid
    ElseIf dicary(idic).num_found > 0 Then
        inds(24).checked = True
        dprint 24, 1, "Triggered by f>=1 of " & dicary(idic).eventid
    End If
    
'24*25894
'24*27400
'24*27416
'24*27420
'24*27424
'24*27438
'24*27457
'24*27459
'24*27461
'24*27463
'24*27465
'24*27467
    If Not inds(24).checked Then
        For i = 1 To 12
            If countwild(i).count = 1 Then
                inds(24).checked = True
                dprint 24, 1, "Triggered by f=1 of " & countwild(i).eventid
            End If
        Next i
    End If
    If Not inds(25).checked Then
        For i = 1 To 12
            If countwild(i).count >= 2 Then
                inds(25).checked = True
                dprint 25, 1, "Triggered by f>=2 of " & countwild(i).eventid
            End If
        Next i
    End If

    suppressDbugLog = False
End Sub


Private Sub Check27()
    Dim charting As String
    Dim ibegin, iend, idic, ipt, icog, iresp1, iresp2 As Single
    suppressDbugLog = True
'27*33939
'27*34889                             regular
'27*34915
'27*34916
'27*34917
'27*34918
'27*34919
'27*34920                             bld transfusion
'27*34920                             PCA
'27*34920                             post op care
'27*34920                             vent booklet
'27*35345
    
' 0*5929                              unresponsive
' 0*5929                              respond to pain
' 0*34906                             pt
    
    charting = ""
    ibegin = DicIndex("33939", charting)
    charting = ""
    iend = DicIndex("35345", charting)
    
    charting = "patient"
    ipt = DicIndex("34906", charting)
    charting = "unresponsive"
    iresp1 = DicIndex("5929", charting)
    charting = "respond to pain"
    iresp2 = DicIndex("5929", charting)
    suppressDbugLog = False
    
    For idic = ibegin To iend
        If dicary(idic).num_found > 0 Then
            inds(27).checked = True
        End If
    Next idic
        
    If dicary(ipt).num_found > 0 And _
       (dicary(iresp1).num_found > 0 Or dicary(iresp2).num_found > 0) Then
        inds(27).checked = False
    End If
    
End Sub

Private Sub Check28()
    Dim charting As String
    Dim ibegin, iend, idic, ipt, ifam, icog, iresp1, iresp2, ialt As Single
    suppressDbugLog = True
    
' 0*5929                              unresponsive
' 0*5929                              respond to pain
' 0*34906                             pt
' 7*34073                             alt mental stat

'28*33279                             yes (matrls givn)
'...
'28*37334                             education
    charting = "yes"
    ibegin = DicIndex("33279", charting)
    charting = "education"
    iend = DicIndex("37334", charting)
    
    charting = "patient"
    ipt = DicIndex("34906", charting)
    charting = "family"
    ifam = DicIndex("34906", charting)
    charting = "cognitive"
    icog = DicIndex("34908", charting)
    charting = "unresponsive"
    iresp1 = DicIndex("5929", charting)
    charting = "respond to pain"
    iresp2 = DicIndex("5929", charting)
    charting = "alt ment"
    ialt = DicIndex("34073", charting)
    
    suppressDbugLog = False
    
    For idic = ibegin To iend
        If dicary(idic).num_found > 0 Then
            inds(28).checked = True
        End If
    Next idic
  
    If (dicary(ipt).num_found > 0 And dicary(ifam).num_found = 0) And _
      (dicary(icog).num_found > 0 Or dicary(iresp1).num_found > 0 Or _
       dicary(iresp2).num_found > 0 Or dicary(ialt).num_found > 0) Then
        inds(28).checked = False
    End If

    
End Sub
Private Sub Check29()
    Dim charting As String
    Dim ibegin As Single
    suppressDbugLog = True
    
' 0*9647                              WNL

    If inds(29).checked Then
        Exit Sub
    End If
    
    charting = "WNL except"
    ibegin = DicIndex("9647", charting)
    
    If dicary(ibegin).num_found > 0 Then
        inds(29).checked = True
    End If
    
    suppressDbugLog = False
   
End Sub

'Private Sub mhCheck1()
'    Dim charting As String
'    Dim i1, i2, i3 As Single
'
'    suppressDbugLog = True
'
'    If mhinds(1).checked Then
'        Exit Sub
'    End If
'
'    suppressDbugLog = False
'
'End Sub
'
'
'Private Sub mhCheck2_3_4()
'    Dim charting As String
'    Dim i9, i9a, i9b As Single
'
'    suppressDbugLog = True
'
'    If mhinds(3).checked Then
'        Exit Sub
'    End If
'
'    charting = "unresponsive"
'    i9 = mhDicIndex("5929", charting)
''    charting = "respond to pain"
''    i9a = mhDicIndex("5929", charting)
''    charting = "respond to voice"
''    i9b = mhDicIndex("5929", charting)
'
'    suppressDbugLog = False
'
''    If mhdicary(i9).num_found > 0 Or mhdicary(i9a).num_found > 0 Or mhdicary(i9b).num_found > 0 Then
'    If mhdicary(i9).num_found > 0 Then
'        mhinds(3).checked = True
'    End If
'
'End Sub
'
'Private Sub mhCheck6()
'    suppressDbugLog = True
'
'    If event33775 And val33775 <= 16 Then
'        mhinds(6).checked = True
'    End If
'
'    If event35286 And val35286 >= 6 Then
'        mhinds(6).checked = True
'    End If
'
'    suppressDbugLog = False
'
'End Sub
'
'Private Sub mhCheck7()
'    Dim charting As String
'    Dim i1, i2, i3 As Single
'
'    suppressDbugLog = True
'
'    charting = "abuse referral"
'    i1 = mhDicIndex("17002", charting)
'
'    If mhdicary(i1).num_found > 0 Then
'        mhinds(7).checked = True
'    End If
'
'    suppressDbugLog = False
'
'End Sub
'
'
'Private Sub mhCheck8()
'    Dim charting As String
'    Dim ibegin, iend, idic, irespa, iresp1, iresp2 As Single
'    suppressDbugLog = True
'
'    If mhinds(8).checked Then
'        Exit Sub
'    End If
'
'' 8*9506                                          capped
'' 8*35253                                         ET tube
'    charting = "capped"
'    ibegin = mhDicIndex("9506", charting)
'    charting = "vision impair"
'    iend = mhDicIndex("18500", charting)
'
'    charting = "awake"
'    irespa = mhDicIndex("5929", charting)
'
'    charting = "unresponsive"
'    iresp1 = mhDicIndex("5929", charting)
'    charting = "respond to pain"
'    iresp2 = mhDicIndex("5929", charting)
'
'    If mhdicary(irespa).num_found > 0 Then
'        For idic = ibegin To iend
'            If mhdicary(idic).num_found > 0 Then
'                mhinds(8).checked = True
'            End If
'        Next idic
'    End If
'
'    If mhinds(8).checked Then
'        Exit Sub
'    End If
'' 8*16585                                         amplification
'' 8*35542                                         non-English
'    If mhdicary(iresp1).num_found = 0 And mhdicary(iresp2).num_found = 0 Then
'        charting = "amplification"
'        ibegin = mhDicIndex("16585", charting)
'        charting = "non-English"
'        iend = mhDicIndex("35542", charting)
'        For idic = ibegin To iend
'            If mhdicary(idic).num_found > 0 Then
'                mhinds(8).checked = True
'            End If
'        Next idic
'    End If
'
'    suppressDbugLog = False
'End Sub
'
'Private Sub mhCheck9()
'    Dim charting As String
'    Dim ibegin, iend, idic, irespa, iresp1, iresp2, ihal, isui As Single
'    suppressDbugLog = True
'
'    If mhinds(9).checked Then
'        Exit Sub
'    End If
'
'    charting = "unresponsive"
'    iresp1 = mhDicIndex("5929", charting)
'    charting = "respond to pain"
'    iresp2 = mhDicIndex("5929", charting)
'
'    charting = "hallucinate"
'    ihal = mhDicIndex("6355", charting)
'    charting = "suicidal"
'    isui = mhDicIndex("6355", charting)
'    If mhdicary(ihal).num_found > 0 Or mhdicary(isui).num_found > 0 Then
'        mhinds(9).checked = True
'    End If
'
'    If mhinds(9).checked Then
'        Exit Sub
'    End If
'' 9*24359                                         alterd mental st
'' 9*34073                                         alt mental stat
'    charting = "alterd ment"
'    ibegin = mhDicIndex("24359", charting)
'    charting = "alt ment"
'    iend = mhDicIndex("34073", charting)
'    For idic = ibegin To iend
'        If mhdicary(idic).num_found > 0 Then
'            If mhdicary(iresp1).num_found = 0 And mhdicary(iresp2).num_found = 0 Then mhinds(9).checked = True
'            mhinds(11).checked = True
'        End If
'    Next idic
'
'    If mhinds(9).checked Then
'        Exit Sub
'    End If
'' 9*35346                                         yes
'    charting = "yes"
'    iresp1 = mhDicIndex("35346", charting)
'    charting = "alt ment"
'    iend = mhDicIndex("34073", charting)
'    If mhdicary(iresp1).num_found > 0 And mhdicary(iend).num_found > 0 Then
'        mhinds(9).checked = True
'    End If
'
'    suppressDbugLog = False
'End Sub
'
'
'Private Sub mhCheck10()
'    Dim charting As String
'    Dim i1 As Single
''10*6355                                          hallucinates
''10*6355                                          suicidal ideatns
'    suppressDbugLog = True
'
'    charting = "hallucinates"
'    i1 = mhDicIndex("6355", charting)
'    If mhdicary(i1).num_found > 0 Then
'        mhinds(9).checked = True
'        mhinds(10).checked = True
'    End If
'
'    charting = "suicidal ideatns"
'    i1 = mhDicIndex("6355", charting)
'    If mhdicary(i1).num_found > 0 Then
'        mhinds(9).checked = True
'        mhinds(10).checked = True
'    End If
'
'    suppressDbugLog = False
'
'End Sub
'Private Sub mhCheck11()
'    Dim charting As String
'    Dim i1 As Single
'    Dim m1 As Single
'
'    suppressDbugLog = True
'
'    m1 = 48
'    If range >= 1440 Then
'        m1 = 48
'    ElseIf range >= 720 Then
'        m1 = 24
'    ElseIf range >= 480 Then
'        m1 = 16
'    End If
'
'    charting = ""
'    i1 = mhDicIndex("44818", charting)
'    If mhdicary(i1).num_found >= m1 Then
'        mhinds(11).checked = True
'    End If
'
'
'    m1 = 12
'    If range >= 1440 Then
'        m1 = 12
'    ElseIf range >= 720 Then
'        m1 = 6
'    ElseIf range >= 480 Then
'        m1 = 4
'    End If
'
'    charting = "patient checked"
'    i1 = mhDicIndex("33682", charting)
'    If mhdicary(i1).num_found >= m1 Then
'        mhinds(11).checked = True
'    End If
'
'    suppressDbugLog = False
'
'End Sub
'
'Private Sub mhCheck12()
'    Dim charting As String
'    Dim i1, i2, i3 As Single
''12*33682                                         patient checked
'    suppressDbugLog = True
'
''22*17002                                         close observtn
''    charting = "patient checked"
''    i1 = mhDicIndex("33682", charting)
'    charting = "close observtn"
'    i2 = mhDicIndex("17002", charting)
'    If mhdicary(i2).num_found > 0 Then
'        mhinds(12).checked = True
'    End If
''
''    charting = "pt at nsg"
''    i1 = mhDicIndex("33682", charting)
''    If mhdicary(i1).num_found >= 6 * range \ MAX_RANGE Then
''        mhinds(12).checked = True
''    End If
'    suppressDbugLog = False
'
'End Sub
'
'Private Sub mhCheck14_15()
''    Dim charting As String
''    Dim count, ibegin, iend, idic As Single
''    Dim ipapermar As Single
''    Dim maxcount1, maxcount2 As Single
''    suppressDbugLog = True
'''14*17312                              17         HHN
'''14*17312                              17         IPPB
'''14*17312                              17         MDI
'''14*17312                              17         EZPAP
''
'''14*6119                                          medicated
'''14*17002                                         medicated
'''14*18286                                         medicated
'''14*21754
''
'''14*18095                                         present
'''14*18095                                         none
'''14*18095                                         pre-proc/surgery
'''14*18095                                         post-proc/surg
'''14*35341                                         anxs
'''14*35341                                         agtd
'''14*35341                                         rstles
'''14*35341                                         awake alert
'''14*35341                                         arouse/voice
'''14*35341                                         arouse/gntl shak
'''14*35341                                         respond to pain
'''14*18098
''    count = 0
''    charting = "HHN"
''    ibegin = mhDicIndex("17312", charting)
''    charting = "EZPAP"
''    iend = mhDicIndex("17312", charting)
''    For idic = ibegin To iend
''        If mhdicary(idic).num_found > 0 Then
''            count = count + 1
''        End If
''    Next idic
''    If count >= RANGE_FACTOR / 240 * range \ MAX_RANGE Then
''        mhinds(15).checked = True
''    ElseIf count >= RANGE_FACTOR / 480 * range \ MAX_RANGE Then
''        mhinds(14).checked = True
''        mhinds(17).checked = True
''    End If
'''14*6119                                          medicated
'''14*17002                                         medicated
'''14*18286                                         medicated
'''14*21754
''    count = 0
''    charting = "medicated"
''    ibegin = mhDicIndex("6119", charting)
''    charting = ""
''    iend = mhDicIndex("21754", charting)
''    For idic = ibegin To iend
''        If mhdicary(idic).num_found >= RANGE_FACTOR / 240 * range \ MAX_RANGE Then
''            mhinds(15).checked = True
''        ElseIf mhdicary(idic).num_found >= RANGE_FACTOR / 480 * range \ MAX_RANGE Then
''            mhinds(14).checked = True
''        End If
''    Next idic
''
'''14*18095                                         present
'''14*18095                                         none
'''14*18095                                         pre-proc/surgery
'''14*18095                                         post-proc/surg
''    count = 0
''    charting = "present"
''    ibegin = mhDicIndex("18095", charting)
''    charting = "pos-proc/surg"
''    iend = mhDicIndex("18095", charting)
''    For idic = ibegin To iend
''        If mhdicary(idic).num_found > 0 Then
''            count = count + 1
''        End If
''    Next idic
''    If count >= RANGE_FACTOR / 240 * range \ MAX_RANGE Then
''        mhinds(15).checked = True
''    ElseIf count >= RANGE_FACTOR / 480 * range \ MAX_RANGE Then
''        mhinds(14).checked = True
''    End If
''
''
''    count = 0
''    charting = "anxs"
''    ibegin = mhDicIndex("35341", charting)
''    charting = "respond to pain"
''    iend = mhDicIndex("35341", charting)
''    For idic = ibegin To iend
''        If mhdicary(idic).num_found > 0 Then
''            count = count + 1
''        End If
''    Next idic
''    If count >= RANGE_FACTOR / 240 * range \ MAX_RANGE Then
''        mhinds(15).checked = True
''    ElseIf count >= RANGE_FACTOR / 480 * range \ MAX_RANGE Then
''        mhinds(14).checked = True
''    End If
''
''    charting = "respond to pain"
''    iend = mhDicIndex("18098", charting)
''    If mhdicary(iend).num_found >= 6 Then
''        mhinds(15).checked = True
''    ElseIf mhdicary(iend).num_found = 5 Then
''        mhinds(14).checked = True
''    End If
''
''    maxcount1 = 6
''    maxcount2 = 12
''    If range >= 1440 Then
''        maxcount1 = 6
''        maxcount2 = 12
''    ElseIf range >= 720 Then
''        maxcount1 = 3
''        maxcount2 = 6
''    End If
''
''    charting = ""
''    ipapermar = mhDicIndex("43817", charting)
''    suppressDbugLog = False
''
''    If mhdicary(ipapermar).num_found >= maxcount2 Then
''        mhinds(15).checked = True
''        dprint 15, 1, "Triggered by f(paper MAR) >= 24 hr level"
''    ElseIf mhdicary(ipapermar).num_found >= maxcount1 Then
''        mhinds(14).checked = True
''        dprint 14, 1, "Triggered by f(paper MAR) >= 12 hr level"
''    End If
''
'End Sub
'
'Private Sub mhCheck16()
'    Dim charting As String
'    Dim i1, i2, i3, i4, i5, i6, i7 As Single
''6931 or 33474
''16*6240                                          calorie ct sheet
''16*6913
''16*6915
''16*16966
''16*37334                                         calorie count
''16*9995                                          daily weight
''16*34072
'
'    suppressDbugLog = True
'
'    charting = "calorie ct sheet"
'    i1 = mhDicIndex("6240", charting)
'    charting = ""
'    i2 = mhDicIndex("6913", charting)
'    charting = "daily weight"
'    i4 = mhDicIndex("9995", charting)
'    charting = ""
'    i5 = mhDicIndex("16966", charting)
'    charting = ""
'    i6 = mhDicIndex("34072", charting)
'    charting = "calorie count"
'    i7 = mhDicIndex("37334", charting)
'
'    If (mhdicary(i1).num_found > 0 Or _
'       mhdicary(i2).num_found > 0 Or _
'       mhdicary(i5).num_found > 0 Or _
'       mhdicary(i7).num_found > 0) And _
'       (event6931 Or event33474) Then
'        mhinds(16).checked = True
'    End If
'
'    If mhdicary(i4).num_found > 0 And event6931 Then
'        mhinds(16).checked = True
'    End If
'
'    If mhdicary(i6).num_found > 0 Then
'        mhinds(16).checked = True
'    End If
'
'    suppressDbugLog = False
'
'End Sub
'
'Private Sub mhCheck17()
'    Dim charting As String
'    Dim ibegin, iend, idic As Single
'    Dim l1 As Single
'
'    suppressDbugLog = True
'
'    l1 = 3
'    If range >= 1440 Then
'        l1 = 3
'    ElseIf range >= 720 Then
'        l1 = 2
'    ElseIf range >= 480 Then
'        l1 = 1
'    End If
'
'
''17*5793
''17*35343                                         >1liter
'    charting = ""
'    ibegin = mhDicIndex("5793", charting)
'    charting = ">1liter"
'    iend = mhDicIndex("35343", charting)
'    For idic = ibegin To iend
'        If mhdicary(idic).num_found >= l1 Then
'            mhinds(17).checked = True
'        End If
'    Next idic
'
'    suppressDbugLog = False
'
'End Sub
'
'Private Sub mhCheck18()
'    Dim charting As String
'    Dim ibegin, iend, idic, l1 As Single
'
'    suppressDbugLog = True
'    l1 = 3
'    If range >= 1440 Then
'        l1 = 3
'    ElseIf range >= 720 Then
'        l1 = 2
'    ElseIf range >= 480 Then
'        l1 = 1
'    End If
'
''18*5749
''18*35258
'    charting = ""
'    ibegin = mhDicIndex("5749", charting)
'    charting = ""
'    iend = mhDicIndex("35258", charting)
'    For idic = ibegin To iend
'        If mhdicary(idic).num_found >= l1 Then
'            mhinds(18).checked = True
'        End If
'    Next idic
'
'    suppressDbugLog = False
'End Sub
'
'Private Sub mhCheck19()
'    Dim charting As String
'    Dim ibegin, iend, idic As Single
'
''    suppressDbugLog = True
''
'''19 16999                                         pin care
'''19 33646                                         Stage IV
''    charting = "pin care"
''    ibegin = mhDicIndex("16999", charting)
''    charting = "Stage IV"
''    iend = mhDicIndex("33646", charting)
''    For idic = ibegin To iend
''        If mhdicary(idic).num_found > 0 Then
''            mhinds(19).checked = True
''        End If
''    Next idic
''
''    suppressDbugLog = False
'End Sub
'
'Private Sub mhCheck20()
'    Dim charting As String
'    Dim ibegin, iend, idic, ipt, icog, iresp1, iresp2 As Single
'    suppressDbugLog = True
'
'' 0*5929                                          unresponsive
'' 0*5929                                          respond to pain
'' 0*34906                                         pt
'    charting = "patient"
'    ipt = mhDicIndex("34906", charting)
'    charting = "unresponsive"
'    iresp1 = mhDicIndex("5929", charting)
'    charting = "respond to pain"
'    iresp2 = mhDicIndex("5929", charting)
'
''20*33939
''20*35345
'    If mhdicary(ipt).num_found > 0 And (mhdicary(iresp1).num_found > 0 Or mhdicary(iresp2).num_found > 0) Then
'    Else
'        charting = ""
'        ibegin = mhDicIndex("33939", charting)
'        charting = ""
'        iend = mhDicIndex("35345", charting)
'        For idic = ibegin To iend
'            If mhdicary(idic).num_found > 0 Then
'                mhinds(20).checked = True
'            End If
'        Next idic
'    End If
'
'    suppressDbugLog = False
'End Sub
'
'
'Private Sub mhCheck21()
'    Dim charting As String
'    Dim ibegin, iend, idic, ipt, ifam, icog, iresp1, iresp2, ialt As Single
'    suppressDbugLog = True
'
'' 9*34073                                         alt mental stat
'' 0*34908                                         cognitive
'' 0*5929                                          unresponsive
'' 0*5929                                          respond to pain
'' 0*34906                                         pt
'    charting = "patient"
'    ipt = mhDicIndex("34906", charting)
'    charting = "family"
'    ifam = mhDicIndex("34906", charting)
'    charting = "unresponsive"
'    iresp1 = mhDicIndex("5929", charting)
'    charting = "respond to pain"
'    iresp2 = mhDicIndex("5929", charting)
'    charting = "alt ment"
'    ialt = mhDicIndex("34073", charting)
'    charting = "cognitive"
'    icog = mhDicIndex("34908", charting)
'
''21*33279                                         yes (matrls givn)
''21*34920                                         smoking cess
'    If (mhdicary(ipt).num_found > 0 And mhdicary(ifam).num_found = 0) And _
'       (mhdicary(iresp1).num_found > 0 Or mhdicary(iresp2).num_found > 0 Or _
'       mhdicary(ialt).num_found > 0 Or mhdicary(icog).num_found > 0) Then
'    Else
'        charting = "yes (matrls givn)"
'        ibegin = mhDicIndex("33279", charting)
'        charting = "smoking cess"
'        iend = mhDicIndex("34920", charting)
'        For idic = ibegin To iend
'            If mhdicary(idic).num_found > 0 Then
'                mhinds(21).checked = True
'            End If
'        Next idic
'    End If
'
'    suppressDbugLog = False
'End Sub
'
'Private Sub mhCheck22_23()
'    Dim charting As String
'    Dim count, ibegin, iend, idic As Single
'    Dim i1 As Single
'
'    suppressDbugLog = True
'
'    charting = "abuse referral"  'also used with ind 7
'    i1 = mhDicIndex("17002", charting)
''22*17002                                         encourge
''22*17002                                         intervntn
'    count = 0
'    charting = "encourge express"
'    ibegin = mhDicIndex("17002", charting)
'    charting = "intervntn"
'    iend = mhDicIndex("17002", charting)
'    For idic = ibegin To iend
'        If mhdicary(idic).num_found > 0 Then
'            count = count + 1
'        End If
'    Next idic
'
'    If mhdicary(i1).num_found > 0 Then
'        count = count + 1
'    End If
'
'    If count > 0 Then
'        mhinds(22).checked = True
'    End If
'
'    charting = "suicide"
'    ibegin = mhDicIndex("17002", charting)
'    charting = "behavior mod"
'    iend = mhDicIndex("17002", charting)
'    For idic = ibegin To iend
'        If mhdicary(idic).num_found > 0 Then
'            count = count + 1
'        End If
'    Next idic
'
'    If count >= 4 Then
'        mhinds(23).checked = True
'    End If
'
'    suppressDbugLog = False
'
'End Sub
'
'Private Sub mhCheck26()
'    Dim charting As String
'    Dim i, i1, i1a, i2, i2a, i2b As Single
'
'    suppressDbugLog = True
'
'    charting = "medicated"
'    i = mhDicIndex("17002", charting)
'
'    'other requirements
'    charting = "hostile"
'    i1 = mhDicIndex("6354", charting)
'    charting = "angry"
'    i1a = mhDicIndex("6354", charting)
'    charting = "abusive"
'    i2 = mhDicIndex("6355", charting)
'    charting = "aggressive"
'    i2a = mhDicIndex("6355", charting)
'    charting = "combative"
'    i2b = mhDicIndex("6355", charting)
'
'    If mhdicary(i).num_found > 0 Then
'        If mhdicary(i1).num_found > 0 Or mhdicary(i1a).num_found > 0 Then
'            If mhdicary(i2).num_found > 0 Or mhdicary(i2a).num_found > 0 Or mhdicary(i2b).num_found > 0 Then
'                mhinds(26).checked = True
'            End If
'        End If
'    End If
'
'    suppressDbugLog = False
'
'End Sub
'
'Private Sub mhCheck29()
'    Dim charting As String
'    Dim count, ibegin, iend, idic, i11 As Single
'
'    suppressDbugLog = True
'
'''11*27142                                         lvl 2 sheet init
'''29*35344                                         MRI
'''29*35344                                         nuclear med
'''29*35344                                         operating room
'''29*35344                                         other
'''29*35344                                         radiation
'''29*35344                                         specials
''    charting = "lvl 2 sheet init"
''    i11 = mhDicIndex("27142", charting)
''    If mhdicary(i11).num_found > 0 Then
''        mhinds(11).checked = True
''    End If
''
''    count = 0
''    charting = "MRI"
''    ibegin = mhDicIndex("35344", charting)
''    charting = "specials"
''    iend = mhDicIndex("35344", charting)
''    For idic = ibegin To iend
''        If mhdicary(idic).num_found > 0 Then
''            count = count + 1
''        End If
''    Next idic
''
''    If count >= RANGE_FACTOR / 960 * range \ MAX_RANGE And mhdicary(i11).num_found > 0 Then
''        mhinds(29).checked = True
''    End If
'
'    suppressDbugLog = False
'End Sub


Private Sub DeleteOldPersistData()
    Dim perfn As String
    Dim perfile As Integer
    Dim newperfn As String
    Dim newperfile As Integer
    Dim buf As String
    Dim mins As Single
    Dim pdt As String
    Dim acct As String

    
    perfn = App.Path & "\" & PERDATA_FNAME          'Persist.dat
    If g_util.FileExists(perfn) Then
    
    perfile = FreeFile
    Open perfn For Input As #perfile 'this file needs to exist before it can be opened
    
    newperfn = App.Path & "\new" & PERDATA_FNAME
    newperfile = FreeFile
    Open newperfn For Append As #newperfile
    
    If intime = "" Then intime = Format(Now, "yyyymmddhhnn")
    While Not EOF(perfile)
        Line Input #perfile, buf
        pdt = Trim$(Mid$(buf, 1, LEN_DT)) 'this is the persist data line time
        mins = DateDiff("n", DateSerial(Mid$(pdt, 1, 4), Mid$(pdt, 5, 2), Mid$(pdt, 7, 2)) + _
                        TimeSerial(Mid$(pdt, 9, 2), Mid$(pdt, 11, 2), "00"), _
                        DateSerial(Mid$(intime, 1, 4), Mid$(intime, 5, 2), Mid$(intime, 7, 2)) + _
                        TimeSerial(Mid$(intime, 9, 2), Mid$(intime, 11, 2), "00"))
        'compare to intime to determin 24 hour diff
        'if buf's time is 24hrs+ old, then skip it
        'else
        If mins <= 1440 Then
            Print #newperfile, buf
            acct = Trim$(Mid$(buf, LEN_DT + 1, LEN_ACCT_NUM))
            If Not AcctNumInPersistFile(acct) Then
                numacct = numacct + 1
                PublishAcctNum acct
            End If
        End If
    Wend
    Close #perfile
    Close #newperfile
    Kill perfn
    Name newperfn As perfn
    
    Else
        dprint 0, -1, PERDATA_FNAME & " not found."
    End If
    
End Sub
Private Function AcctNumInPersistFile(ByRef a As String) As Boolean
    ' only the acctnum array; file is not necessarily open
    Dim i As Single
    Dim r As String
    
    AcctNumInPersistFile = False
    For i = 1 To numacct
        If a = acctnumdirectory(i).a Then
            AcctNumInPersistFile = True
            Exit For
        End If
    Next i
End Function

Private Function DidProcessPersist(ByRef a As String) As Boolean
    ' only the acctnum array; file is not necessarily open
    Dim i As Single
    Dim r As String
    
    DidProcessPersist = False
    For i = 1 To numacct
        If a = acctnumdirectory(i).a Then
            DidProcessPersist = acctnumdirectory(i).already_did_persist
            Exit For
        End If
    Next i
End Function

Private Sub SetAlreadyDidPersist(ByRef a As String)
    ' only the acctnum array; file is not necessarily open
    Dim i As Single
    Dim r As String
    
    For i = 1 To numacct
        If a = acctnumdirectory(i).a Then
            acctnumdirectory(i).already_did_persist = True
            Exit For
        End If
    Next i
End Sub



Private Sub PublishAcctNum(ByRef a As String)
    ReDim Preserve acctnumdirectory(0 To numacct)
    acctnumdirectory(numacct).a = a
End Sub

Private Sub AddPersistLine(ByRef b As String)
    Dim pdt As String
    Dim perfn As String
    Dim perfile As Integer
    
    'parse out event time
    pdt = Trim$(Mid$(b, START_EVENT_DT, LEN_DT))
    pdt = pdt & Space(LEN_DT - Len(pdt)) & acctnum & Space(LEN_ACCT_NUM - Len(acctnum)) & b
   
    perfn = App.Path & "\" & PERDATA_FNAME          'Persist.dat
    perfile = FreeFile
    Open perfn For Append As #perfile
    
    Print #perfile, pdt
    
    Close #perfile
    
End Sub

Private Sub GetNextPersistItemForThisAcctNum(ByRef pidx As Single, ByRef b As String) 'return the event string b
    Dim perfn As String
    Dim perfile As Integer
    Dim ppos As Single
    Dim a As String
    
    perfn = App.Path & "\" & PERDATA_FNAME          'Persist.dat
    perfile = FreeFile
    Open perfn For Input As #perfile
    
    ppos = 0
    While Not EOF(perfile) And ppos < pidx
        Line Input #perfile, b
        ' parse off the acct num from b and assign a to it.
        a = Trim$(Mid$(b, LEN_DT + 1, LEN_ACCT_NUM))
        If a = acctnum Then
            ppos = ppos + 1
            If ppos = pidx Then
                b = Mid$(b, LEN_DT + LEN_ACCT_NUM + 1, START_RESULT + LEN_RESULT) 'start from after acctnum for entire length of event line
            End If
        End If
        If EOF(perfile) Then
            b = ""
            SetAlreadyDidPersist (acctnum)
        End If
    Wend
    
    Close #perfile

End Sub
