Attribute VB_Name = "QCPR_Main"
Option Explicit
'
'Transparent classification - QCPR demo
'
'The location of the input file is determined by the first line of the MSDICT.DAT dictionary.
'
'The query file is formatted as:
'    <Header>
'       <Event>
'       <Event>
'         ...
'
Const TRANSP_FILENAME = "Transparent.txt"   'output file
Const TRANSP_INLOG_FILE_LIFE = 10           'days
Const TRANSP_OUTLOG_FILE_LIFE = 10          'days
Const TRANSP_DEBUG_FILE_LIFE = 10           'days
Const INI_FNAME = "TC_IMPORT.INI"
Const MSDIC_FNAME = "MSDICT.TXT"            'MS rules file
Const MHDIC_FNAME = "MHDICT.TXT"
Const VALIDMS_FNAME = "VALIDMS.TXT"         'list of valid MS units
Const VALIDMH_FNAME = "VALIDMH.TXT"
Const PERDATA_FNAME = "PERSIST.TXT"         'name of persistent data file (data saved for next time)

'==============================================================================
'HEADER RECORD - User Group 2008
Const MAX_INDICATORS = 50       'only 50?

Const START_ACCT_NUM = 1
Const LEN_ACCT_NUM = 20

Const START_PTNAME = 100
Const LEN_PTNAME = 30

Const START_UNIT = 31
Const LEN_UNIT = 10

Const START_RM = 41
Const LEN_RM = 8            '10

Const START_BED = 51
Const LEN_BED = 4           '5

Const START_FACILITY = 200   'N/A
Const LEN_FAC = 5

Const START_BIRTHDATE = 86      'Birthdate?? This is in the HIMSS demo only
'Const LEN_BIRTH = 14

Const START_CLASS_DT = 200   'N/A
Const LEN_DT = 14


'==============================================================================
'EVENT RECORD - User Group 2008
Const START_EVENT_DT = 5
'Const LEN_DT = 14

Const START_EVENT = 20
Const LEN_EVENT = 30

Const START_EVENT_DSC = 50
Const LEN_EVENT_DSC = 40

Const START_FREQ = 90               '65     '75
Const LEN_FREQ = 10

Const START_RESULT = 100            '116
Const LEN_RESULT = 75


Const MAX_RANGE = 1440                  '24 hrs in minutes; this is overridden by the -range arg.

Const RANGE_FACTOR = 24 * 60            'For proportioning the count of Med/Pulm/Cardio

Const MAX_UNIQUE_CHART_TIMES = 20
Const DT_FORMAT = "yyyymmddhhnn"

Public g_util   As New PFSUtility
Public g_tcutil As New TCUtility

Dim infn As String
Dim fnames() As String
Dim outfn As String
Dim inlogfile As Integer
Dim inlogname As String
Dim outlogfile As Integer
Dim outlogname As String
Dim dbugfile As Integer
Dim dbugname As String

Private Type dict_entry
  ind           As Integer  'winpfs indic number associated with this cerner event
  eventid       As String     'Event_CD of this indicator
  chart_result  As String    'look for this key in charted result
  gavefreq      As Boolean   'file supplied the frequency; ignore subsequent freqs
  check_freq    As Boolean  'flag to check frequency
  freq_basis    As Integer  'frequency required for this indicator (in minutes)
  act_freq      As Integer  'calculated frequency from data (in minutes)
  last_datetime As String   'last date time of event found in this patient
  num_found     As Integer  'number of times this was found in this patient
  also_mark     As String   'if this indicator is marked, then also mark these.
  'charttime(MAX_UNIQUE_CHART_TIMES) As String       commented out and in code to save mem space 9/26/07
  one_row_find  As Single    'for multiple charting results on the same event line (Cerner)
  persist       As Boolean    'flag if this charting element persists
  perlen        As Single      'for how long does it persist? (in minutes)
  'procedure info
  duration      As Integer      'in minutes
  start_dt      As Date
  finish_dt     As Date
End Type

Private Type indicator_data
    checked As Boolean
    also_mark As String
End Type

Private Type proc_info
    dic_index As Integer
End Type

Private Type event_wildcharting
    eventid As String 'event id
    count As Single
End Type

Private Type ProcessedInfo
    a As String     'acctnum
    already_did_persist As Boolean
End Type

Dim dicary() As dict_entry
Dim mhdicary() As dict_entry
Dim dicnum As Integer
Dim mhdicnum As Integer
Dim chartImportPath As String         'path of chart file
Dim winpfspath As String        'path of winpfs
Dim winlogpath As String        'path of winpfs\log
Dim winloadpath As String       'path of winpfs\load_me

Dim datafile As Integer
Dim outfile As Integer

Dim inds(MAX_INDICATORS) As indicator_data
Dim grps(MAX_INDICATORS) As Integer
Dim mhinds(MAX_INDICATORS) As indicator_data
Dim mhgrps(MAX_INDICATORS) As Integer

Dim lastname As String
Dim firstname As String
Dim unitname As String
Dim roomname As String
Dim bedname As String
Dim acctnum As String
Dim classdt As String
Dim classdate As String
Dim classtime As String
Dim nowdt As Date
Dim intime As String
Dim saveintime As String

Dim range As Single                 'number of minutes in the scope of time for freq.
Dim dbugon As Boolean
Dim suppressDbugLog As Boolean
Dim effdateon As Boolean
Dim effdate As String
Dim efftimeon As Boolean
Dim efftime As String
Dim pulltimeon As Boolean
Dim pulltime As String
Dim pulldateon As Boolean
Dim pulldate As String

Dim MSunitary() As String
Dim MHunitary() As String
Dim unitnum As Integer
Dim mhunitnum As Integer

Dim acctnumdirectory() As ProcessedInfo
Dim numacct As Single
Dim telemetry As Boolean
Dim procs() As proc_info
Dim num_procs As Integer


Sub Main()
    On Error GoTo errHandler
    
    Const RANGE_PARAM = "-range="
    Const EFF_TIME = "-efftime="
    Const DBUG_ON = "-debug"
    Const ALTER_DATE = "-effdate="
    Const PULL_TIME = "-pulltime="
    Const PULL_DATE = "-pulldate="
    Dim n As Integer
    Dim i As Integer
    Dim cmdLine As String
    Dim rpos As Integer
    'Dim effective As String
    Dim h As String
    Dim t As Date
    Dim adpos As Integer
    
    '-efftime=hhmm  This is the time at which the classification is effective.
    '                 The date associated with this time is taken from header classdate.
    '                 If not specified, then -efftime is assumed to be the classtime
    '                 from the header.
    '-effdate=yyyymmdd   This is the date which, in combination with the
    '                 -effective time is used to specify the effective datetime.
    '                 If not specified, then -effdate is assumed to be the classdate
    '                 from the header.
    'Special value:  -effdate=yesterday means Now's yesterday.
        
    '-pulltime=hhmm  This is the time starting from which the pull is to
    '                look backwards from.
    '                If not specified, then this time is assumed to be the -effective time.
    '-pulldate=yyyymmdd This is the date of the pulltime.
    '                If not specified, then this is assumed to be Now's date.
    
    '-range=nnnn  This is the number of minutes backwards from the pull time
    '             that defines valid range of charting events.
    
    ChDrive App.Path
    ChDir App.Path
    
    nowdt = Now
    cmdLine = LCase(Command$)

    dbugon = (InStr(cmdLine, DBUG_ON) > 0)
    suppressDbugLog = False
    
    effdateon = (InStr(cmdLine, ALTER_DATE) > 0)
    If effdateon Then
        adpos = InStr(cmdLine, ALTER_DATE)
        If UCase$(Mid$(cmdLine, adpos + Len(ALTER_DATE), 8)) = "YESTERDA" Then
            effdate = Format(DateAdd("d", -1, g_util.DateOnly(nowdt)), "yyyymmdd")
        Else
            effdate = Mid$(cmdLine, adpos + Len(ALTER_DATE), 8) 'yyyymmdd
        End If
    End If
    
    
    pulltimeon = (InStr(cmdLine, PULL_TIME) > 0)
    If pulltimeon Then
        pulltime = Mid$(cmdLine, InStr(cmdLine, PULL_TIME) + Len(PULL_TIME), 4)
    End If
    
    pulldateon = (InStr(cmdLine, PULL_DATE) > 0)
    If pulldateon Then
        pulldate = Mid$(cmdLine, InStr(cmdLine, PULL_DATE) + Len(PULL_DATE), 8)
    End If

    efftimeon = (InStr(cmdLine, EFF_TIME) > 0)
    If efftimeon Then
        efftime = Mid$(cmdLine, InStr(cmdLine, EFF_TIME) + Len(EFF_TIME), 4)
    End If
    
    rpos = InStr(cmdLine, RANGE_PARAM)
    If rpos > 0 Then
        'range = val(Mid$(cmdLine, InStr(cmdLine, RANGE_PARAM) + Len(RANGE_PARAM), 4))
        range = val(Mid$(cmdLine, rpos + Len(RANGE_PARAM)))       'what if range > 9999?  (take away ,4)
    End If
    
    If range <= 0 Then
        range = MAX_RANGE
    End If
    
    'No effective time given?  Use current time.    ***
    If efftime = "" Then
        efftime = Format(nowdt, "hhnn")
    End If
    
    intime = ""
    
    If (LoadDictionaries) Then
        InitGroups
        OpenLogFiles
        dprint -1, 0, "range=" & range
        dprint -1, 0, "effdate=" & effdate
        dprint -1, 0, "efftime=" & efftime
        dprint -1, 0, "pulltime=" & pulltime
        dprint -1, 0, "pulldate=" & pulldate
        
        If efftime <> "" Then
            ' command parameter needs to be in form hhmm ONLY
            If IsNumeric(efftime) Then
                efftime = Format$(val(efftime), "0000")
                i = year(g_util.DateOnly(FileDateTime(inlogname)))
                intime = CStr(i) & Mid$(inlogname, Len(inlogname) - Len("mmdd.log") + 1, Len("mmdd")) & efftime
                If effdateon Then
                    intime = effdate & Mid$(intime, 9, 4)
                End If
                saveintime = intime
            End If
        End If
        
        outfn = winloadpath & "\" & TRANSP_FILENAME     'output filename transparent.txt
    
        n = g_tcutil.GetAllImportFilenames(chartImportPath, fnames())
'        i = 0      'PROCESS ALL FILES
        i = n - 1  'ONLY PROCESS THE LATEST FILE
'Open the Persist file, removing old items, and read acctnums into the acctnumdirectory array
        DeleteOldPersistData
        While i < n
            i = i + 1
            infn = chartImportPath + "\" + fnames(i)
            Process
        Wend
        CloseLogFiles
        DeleteOldLogs
    End If

    Exit Sub
    
errHandler:
    g_util.MsgBoxError
End Sub

Private Function LoadDictionaries() As Boolean
    Dim dicfn As String
    Dim dicfile As Integer
    Dim buf As String
    Dim unitfn As String
    Dim unitfile As Integer
    Dim n As Integer

    'Special dictionary characters:
    '  * = check freq or other things before triggering indicator
    '  # = persist only
    '  @ = persist and check freq

    dicnum = 0
    dicfile = FreeFile
    dicfn = App.Path & "\" & MSDIC_FNAME
    Open dicfn For Input As #dicfile
    
    While Not EOF(dicfile)
        Line Input #dicfile, buf
        
        If Left$(buf, 1) = ";" Then
            buf = ""
        Else
            n = InStr(1, buf, "//")
            If (n > 0) Then
                buf = Trim$(Left$(buf, n - 1))              'strip comment to end of line
            End If
        End If
        
        If (Len(buf) = 0) Then
            'ignore comment lines and empty lines
        Else
            dicnum = dicnum + 1
            ReDim Preserve dicary(0 To dicnum)
            With dicary(dicnum)
                .ind = val(Mid$(buf, 1, 2))
                .check_freq = (Mid$(buf, 3, 1) = "*") Or (Mid$(buf, 3, 1) = "@")
                .eventid = Trim$(Mid$(buf, 4, 29))          '30
                .freq_basis = val(Trim$(Mid$(buf, 34, 4)))
                .also_mark = Trim$(Mid$(buf, 39, 10))
                .chart_result = Trim$(Mid$(buf, 50, 75))
                .persist = (Mid$(buf, 3, 1) = "#") Or (Mid$(buf, 3, 1) = "@")
                If .persist Then .perlen = 1440
                
                Debug.Print "ind#"; .ind, " event="; .eventid, " freq="; .freq_basis, " result="; .chart_result
            End With
        End If
    Wend
    Close #dicfile
'    dicnum = dicnum - 1
    
'Valid Med Surg units
    unitnum = 0
    unitfile = FreeFile
    unitfn = App.Path & "\" & VALIDMS_FNAME
    Open unitfn For Input As #unitfile
    While Not EOF(unitfile)
        Line Input #unitfile, buf
        If Trim$(buf) <> "" Then
            unitnum = unitnum + 1
            ReDim Preserve MSunitary(0 To unitnum)
            MSunitary(unitnum) = UCase$(Trim$(Mid$(buf, 1, 16)))
        End If
    Wend
    Close #unitfile
    
    mhdicnum = 0
    dicfile = FreeFile
    dicfn = App.Path & "\" & MHDIC_FNAME
'    Open dicfn For Input As #dicfile
'    While Not EOF(dicfile)
'        mhdicnum = mhdicnum + 1
'        Line Input #dicfile, buf
'        ReDim Preserve mhdicary(0 To mhdicnum - 1)
'        mhdicary(mhdicnum - 1).ind = val(Mid$(buf, 1, 2))
'        mhdicary(mhdicnum - 1).check_freq = (Mid$(buf, 3, 1) = "*") Or (Mid$(buf, 3, 1) = "@")
'        mhdicary(mhdicnum - 1).eventid = Trim$(Mid$(buf, 4, 30))
'        mhdicary(mhdicnum - 1).freq_basis = val(Trim$(Mid$(buf, 34, 4)))
'        mhdicary(mhdicnum - 1).also_mark = Trim$(Mid$(buf, 39, 10))
'        mhdicary(mhdicnum - 1).chart_result = Trim$(Mid$(buf, 50, 75))
'        mhdicary(mhdicnum - 1).persist = (Mid$(buf, 3, 1) = "#") Or (Mid$(buf, 3, 1) = "@")
'        If mhdicary(mhdicnum - 1).persist Then mhdicary(mhdicnum - 1).perlen = 1440
'    Wend
'    Close #dicfile
    mhdicnum = mhdicnum - 1
        
'Valid MH units
    mhunitnum = 0
    unitfile = FreeFile
    unitfn = App.Path & "\" & VALIDMH_FNAME
'    Open unitfn For Input As #unitfile
'    While Not EOF(unitfile)
'        Line Input #unitfile, buf
'        If Trim$(buf) <> "" Then
'            mhunitnum = mhunitnum + 1
'            ReDim Preserve MHunitary(0 To mhunitnum)
'            MHunitary(mhunitnum) = UCase$(Trim$(Mid$(buf, 1, 16)))
'        End If
'    Wend
'
''don't need the unit file anymore
'    Close #unitfile
    
'init wild charting array
    
    LoadDictionaries = True
    
End Function

Private Function FoundInList(eventid As String, Optional freq As Variant) As Integer
    Dim i As Integer
    Dim useoption As Boolean
    Dim dontcare As Boolean

    FoundInList = 0
    useoption = Not IsMissing(freq)
    For i = 1 To dicnum             'numind
        If useoption Then
            dontcare = (Not dicary(i).check_freq)           'freq can be anything?
            'If dicary(i).check_freq Then Debug.Print dicary(i).eventid; " freq="; dicary(i).freq_basis
            If (eventid = dicary(i).eventid) Then     'And (dontcare Or (InStr(freq, dicary(i).freq) > 0)) Then
                FoundInList = i
                Exit For
            End If
        ElseIf (eventid = dicary(i).eventid) Then
            FoundInList = i
            Exit For
        End If
    Next i
End Function

Private Sub FinalizeIndicators()
    On Error GoTo errHandler
    Dim highest_is_on As Boolean
    Dim g As Integer
    Dim i As Integer
    Dim p As Integer
    
    'Here is where you now have to go through the frequencies to
    'determine which indicators to mark for this pt.
    
    'set the act_freq to 1440 if num_found is >= 1
'    For i = 1 To dicnum
'        If dicary(i).num_found >= 1 Then
'            dicary(i).act_freq = 1440
'        End If
'    Next i
    
    If ValidMSUnit(unitname) Then
        'Special rules here
        check_14_15_16_17       'assessment
        Check_19_20             'wound care

    'Next, loop the dictionary to find the indicator according to frequency.
    For i = 1 To dicnum
        If dicary(i).num_found > 0 Then
            'p = FindClosestFreq(i)
            p = FoundInList(dicary(p).eventid, True)
            If (p > 0) Then
                inds(dicary(p).ind).checked = True
                dprint dicary(p).ind, 1, "via fall through"

                If (dicary(p).num_found > 0 And Not inds(dicary(p).ind).checked) Then
                    dprint dicary(p).ind, -1, "***FAILED TO TURN ON***"
                End If

                If inds(dicary(p).ind).checked Then
                    If inds(dicary(p).ind).also_mark <> "" Then
                        'Only do the also mark if it originally arose from an also-mark ind.
                        ParseAlsoMark (inds(dicary(p).ind).also_mark)
                    End If
                End If
            End If
        End If
    Next i
    
'    If Not (inds(1).checked Or inds(2).checked Or inds(3).checked Or inds(4).checked) Then
'        dprint 2, 1, "via AtLeastOneADL"
'    End If
    'inds(2).checked = True
    
    'Look for any mutually exclusive indicators marked and remove the lesser ones.
    '
    g = 0
    highest_is_on = False
    For i = MAX_INDICATORS To 1 Step -1
        If (grps(i) > 0) Then
            If (grps(i) <> g) Then
                g = grps(i)
                highest_is_on = inds(i).checked
            Else
                If highest_is_on Then
                    inds(i).checked = False
                Else
                    highest_is_on = inds(i).checked
                End If
            End If
        End If
    Next i
    
    'Make sure there is at least 1 ADL marked
    AtLeastOneADL

    End If
    Exit Sub

errHandler:
    g_util.ThrowError "FinalizeIndicators"
    Resume  'debug
End Sub

Private Function DatetimeFromProcTime(dt As Date, iTime As Integer)
    If (iTime Mod 100) = 60 Then            'time ends in 60?       ex: 660
        iTime = iTime + 40                  'bump to next hour      ex: 700
    End If
    
    DatetimeFromProcTime = DateValue(dt) & Format$(iTime, " 00:00")
End Function

Private Sub Process()
    Dim buf As String
    Dim p As Integer
    Dim i As Integer
    Dim d As String
    Dim formloading As Boolean
    Dim s As String
    Dim rows() As String
    Dim maxrow As Long
    Dim iRow As Long
    Dim isodt As String
    Dim sNowdt As String
    Dim chart_result As String
    Dim prevchart_result As String
'    Dim res As String
    Dim rangedate As Date
    Dim rangedt As String
    Dim intimedate As Date
    Dim n As Integer
    Dim eventid As String, eventdesc As String
    Dim freq_given As Integer
    Dim skip_this_patient As Boolean
    Dim pf As Integer
    Dim w As Integer
    Dim just_read_a_line As Boolean
    Dim processing_persist_item As Boolean
    Dim percount As Single
    Dim valCIWA As Single
    Dim arr() As String
    
    On Error GoTo errProcess
        
    formloading = True
    skip_this_patient = False
'    cmdProcess.Enabled = False
'    cmdOutput.Enabled = False
    
    Debug.Print ""
    Debug.Print "Process "; infn
    datafile = FreeFile
    Open infn For Input As #datafile
    s = Input(LOF(datafile), datafile)
    rows() = Split(s, vbCrLf)
    maxrow = UBound(rows)
    
    outfile = FreeFile
    Open outfn For Append As #outfile
    
    For iRow = 0 To maxrow
        buf = rows(iRow)
        Print #inlogfile, buf
        
        just_read_a_line = True
        processing_persist_item = False
        percount = 0
        While just_read_a_line Or processing_persist_item
            just_read_a_line = False  'the only way to get back into while loop is if processing
        
        
        If Trim$(Mid$(buf, 1, 4)) = "" Then 'Event record
            ' There may be more than 1 dictionary entry with the same
            ' event code but with a different frequency.  So use the first occurrence
            ' of this event to keep the running frequency.  Then at then end,
            ' locate the appropriate indicator for the calculated frequency.
            ' OR, create a proc for the particular event code.
            If skip_this_patient Then
                'do nothing
            Else
            
            isodt = Trim$(Mid$(buf, START_EVENT_DT, LEN_DT))        'event time
            
            freq_given = 0
            If IsNumeric(Mid$(buf, START_FREQ, LEN_FREQ)) Then
                freq_given = Mid$(buf, START_FREQ, LEN_FREQ)
                'the demo is sending freq in hours, not minutes
                If (freq_given <= 4) Then freq_given = freq_given * 60
            End If

            'calc class effective IN time from either pull datetime or effective datetime
            If pulltimeon Then
                If pulldateon Then
                    intimedate = DateSerial(Mid$(pulldate, 1, 4), Mid$(pulldate, 5, 2), Mid$(pulldate, 7, 2)) _
                               + TimeSerial(Mid$(pulltime, 1, 2), Mid$(pulltime, 3, 2), 0)
                Else
                    intimedate = g_util.DateOnly(nowdt) + TimeSerial(Mid$(pulltime, 1, 2), Mid$(pulltime, 3, 2), 0)
                End If
            Else
                If intime = "" Then
                    intimedate = Now 'just to make it valid
                Else
                    intimedate = DateSerial(Mid$(intime, 1, 4), Mid$(intime, 5, 2), Mid$(intime, 7, 2)) _
                            + TimeSerial(Mid$(intime, 9, 2), Mid$(intime, 11, 2), 0)
                End If
            End If
            
            'dprint -1, 0, "intimedate=" & intimedate
            rangedate = DateAdd("n", -range, intimedate) ' range minutes before intime
            rangedt = Format$(rangedate, "yyyymmddhhnn")
            'dprint -1, 0, "rangedt=" & rangedt

'        Remove blinders for Newport -- rely entirely on data.
'            If rangedt > isodt Then
'                dprint -2, 0, buf
'            Else
'                dprint -1, 0, buf
            eventid = Trim$(Mid(buf, START_EVENT, LEN_EVENT))
            eventdesc = Trim$(Mid$(buf, START_EVENT_DSC, LEN_EVENT_DSC))
            chart_result = Trim$(Mid(buf, START_RESULT, LEN_RESULT))
            If Len(chart_result) > 1 Then
                If Asc(Right$(chart_result, 1)) < 32 Then
                    chart_result = Left$(chart_result, Len(chart_result) - 1)       'strip trailing LF from UNIX
                End If
            End If
            
            If ValidMSUnit(unitname) Then
            
                prevchart_result = chart_result
                p = DicIndex(eventid, chart_result)             'look for event id
                If (p = 0) Then
                    p = DicIndex(eventdesc, chart_result)       'look for event desc
                End If
                
                pf = 0 'the number of one-row-finds
                
                If p > 0 Then
                    If dicary(p).persist And Not processing_persist_item Then 'only add to persist data from current data
                        AddPersistLine buf
                    End If
                End If
            
                While p > 0 'for multiple charting results on the same event line (Cerner)
                    pf = pf + 1
                    If dicary(p).check_freq Then        ' asterisk (*) is associated with dic item
                        If dicary(p).num_found = 0 Then
                            dicary(p).num_found = 1
                            dicary(p).last_datetime = Trim$(Mid(buf, START_EVENT_DT, LEN_DT))
    '                        dicary(p).charttime(dicary(p).num_found) = Trim$(Mid(buf, START_EVENT_DT, LEN_DT))
                            dicary(p).act_freq = 0
                            'If this is the only occurrence, then it will be changed
                            'to 1440 at summary time.
                            dprint dicary(p).ind, -1, "num found = 1, freq=" & dicary(p).act_freq & " Item=" & p
                            If freq_given > 0 Then
                                dicary(p).gavefreq = True
                                dicary(p).act_freq = freq_given
                            End If
                        ElseIf freq_given > 0 Then
                            dicary(p).num_found = dicary(p).num_found + 1
                            dicary(p).act_freq = freq_given
                        Else
                            'CalcFrequency sets act_freq
                            dicary(p).num_found = dicary(p).num_found + 1
    '                        If dicary(p).num_found <= MAX_UNIQUE_CHART_TIMES Then
    '                            dicary(p).charttime(dicary(p).num_found) = Trim$(Mid(buf, START_EVENT_DT, LEN_DT))
    '                        End If
                            If Not dicary(p).gavefreq Then
                                CalcFrequency p
                            End If
                            dprint dicary(p).ind, -1, "num found =" & dicary(p).num_found & ", freq=" & dicary(p).act_freq & " Item=" & p
                        End If
                    Else
                        inds(dicary(p).ind).checked = True
                        dprint dicary(p).ind, 1, ""
                        If dicary(p).also_mark <> "" Then
                            ParseAlsoMark (dicary(p).also_mark)
                            dprint dicary(p).ind, 1, " Also-mark=" & dicary(p).also_mark
                        End If
                    End If
                    
                    'Look for extended procedure info
                    'Make defaults if start/stop not found
                    If (dicary(p).num_found > 0) And (dicary(p).ind = 19) Then
                        With dicary(p)
                            n = g_util.SplitTextOnChar(chart_result, "|", arr(), 1, 0)
                            
                            If (InStr(1, eventdesc, "Duration") > 0) And (n >= 3) Then
                                .duration = CInt(arr(1))
                                .start_dt = DatetimeFromProcTime(intimedate, CInt(arr(2)))
                                .finish_dt = DatetimeFromProcTime(intimedate, CInt(arr(3)))
                                If (.finish_dt < .start_dt) Then            'crossed midnight?
                                    .finish_dt = DateAdd("h", 24, .finish_dt)
                                End If
                                Debug.Print "extracted procedure start="; .start_dt; " finish="; .finish_dt; " duration="; .duration; " for "; eventdesc; " p="; p
                            Else
                                'keep the values from the "duration" line in the input file
'                                .duration = 75
'                                .start_dt = intimedate
'                                .finish_dt = DateAdd("n", .duration, .start_dt)
                            End If
                        End With
                    End If
                    
                    'Get the also-mark from the dictionary since this is its source.
                    If dicary(p).also_mark <> "" Then
                        inds(dicary(p).ind).also_mark = dicary(p).also_mark
                    End If
                    
                    If dicary(p).one_row_find < pf Then
                        dicary(p).one_row_find = pf
                    End If
                    
                    If prevchart_result = chart_result Then
                        p = 0
                    Else
                        prevchart_result = chart_result
                        p = DicIndex(eventid, chart_result)
                    End If
                Wend
            End If 'valid ms
            
'
            End If 'skip patient
        
        Else 'Header record
            If (Not formloading) Then 'end of the previous pt.  Output now.
                If Not skip_this_patient Then
                    FinalizeIndicators
                    classdate = Mid$(classdt, 1, 8)
                    classtime = Mid$(classdt, 9, 4)
                    AssembleOutput
                End If
            End If
            skip_this_patient = False
            If dbugon Then suppressDbugLog = False
            dprint -1, 0, buf
            formloading = False
            InitIndicators
            ResetDictionaryFields
            ParsePatientInfo buf            'unitname, acctnum, etc. are set here
            If Not ValidMSUnit(unitname) And Not ValidMHUnit(unitname) Then
                skip_this_patient = True
            End If
            classdt = ""
            classdate = ""  'Mid$(buf, 21, 4) & Mid$(buf, 15, 2) & Mid$(buf, 18, 2)
            classtime = ""  'Mid$(buf, 26, 4)
            If efftimeon Then intime = saveintime
'            count5805 = 0
'            telemetry = False
'            For i = 1 To 20
'                countwild(i).count = 0
'            Next i
        End If
        
        If AcctNumInPersistFile(acctnum) And Not DidProcessPersist(acctnum) Then  'reads persist data into buf?
            processing_persist_item = True
            percount = percount + 1
            GetNextPersistItemForThisAcctNum percount, buf
            If buf = "" Then
                processing_persist_item = False 'fall out of while
            End If
        Else
            processing_persist_item = False
        End If 'find acct num
            
        
        Wend 'while just_read_a_line or processing

        DoEvents
    Next

    If Not skip_this_patient Then
        FinalizeIndicators
        classdate = Mid$(classdt, 1, 8)
        classtime = Mid$(classdt, 9, 4)
        AssembleOutput
    End If

    Close #datafile
    Close #outfile
    
    If 0 Then
        Debug.Print "delete "; infn
        Kill infn
    Else
        Name infn As Replace$(infn, ".txt", ".bak")
    End If
    
    Exit Sub
errProcess:
    dprint 0, 0, "*** " & Err.Description
    'Resume Next     'keep going?  (or die?)
    Exit Sub
    Resume  'debug
End Sub

Private Function DicIndex(code As String, ByRef result As String) As Integer
    Dim i As Integer
    Dim p As Integer
    
    ' inds(i).num_found is set only for dictionary items with asterisk (*)
    ' in column 3 of the dictionary file.

    DicIndex = 0
    For i = 1 To dicnum
        If (dicary(i).eventid = code) Then
            If Len(dicary(i).chart_result) > 0 Then
                p = InStr(1, result, dicary(i).chart_result, vbTextCompare)
                If (p > 0) Then
                    DicIndex = i
                    'Remove the charting text in prep for another eval.
                    '**** IS THIS FEATURE USED ANYWHERE?  (NOT IN THIS VERSION)  Don't modify result
                    'result = Replace(result, dicary(i).chart_result, "", , , vbTextCompare)
                    dprint -3, 0, "Dictionary item found=" & i & " event=" & code & " result=" & result & " ind=" & dicary(i).ind
                    Exit For
                End If
            Else    'don't care about matching chart result
                DicIndex = i
                dprint -3, 0, "Dictionary item found=" & i & " event=" & code & " result=<don't care>" & " ind=" & dicary(i).ind
                Exit For
            End If
        End If
    Next i
    
    'Debug.Print "("; code; "/"; result; ") not found"
End Function

'Private Function mhDicIndex(id As String, ByRef s As String) As Integer
'    Dim i As Integer
'    Dim p As Integer
'
'    mhDicIndex = 0
'    For i = 1 To mhdicnum
'        If (id = mhdicary(i).eventid) Then
'            If (Trim$(mhdicary(i).chart_result) <> "") Then
'                p = InStr(1, s, mhdicary(i).chart_result, vbTextCompare)
'                If (p > 0) Then
'                    mhDicIndex = i
'                    'Remove the charting text in prep for another eval.
'                    s = Replace(s, mhdicary(i).chart_result, "", , , vbTextCompare)
'                    dprint 0, 0, "Dictionary item found=" & i & " id=" & id & " charting=" & s & " mapping to=" & mhdicary(i).ind
'                    Exit For
'                End If
'            Else 'don't care about matching chartkey
'                mhDicIndex = i
'                dprint 0, 0, "Dictionary item found=" & i & " id=" & id & " charting=%don't care%" & " mapping to=" & mhdicary(i).ind
'                Exit For
'            End If
'        End If
'    Next i
'End Function

Private Sub InitIndicators()
    Dim i As Integer
    
    For i = 1 To MAX_INDICATORS
        inds(i).checked = False
        inds(i).also_mark = ""
        mhinds(i).checked = False
        mhinds(i).also_mark = ""
    Next i
    
    ReDim procs(0 To 0)
    num_procs = 0
    
End Sub
Private Sub InitGroups()
    Dim i As Integer
    
    For i = 1 To MAX_INDICATORS
        grps(i) = 0
    Next i
    
    For i = 1 To 4
        grps(i) = 1
    Next i
    For i = 8 To 9
        grps(i) = 2
    Next i
    For i = 11 To 13
        grps(i) = 3
    Next i
    For i = 14 To 16
        grps(i) = 4
    Next i
    For i = 17 To 19
        grps(i) = 5
    Next i
    For i = 20 To 22
        grps(i) = 6
    Next i
    For i = 24 To 26
        grps(i) = 7
    Next i
    
    
    For i = 1 To MAX_INDICATORS
        mhgrps(i) = 0
    Next i
    
    For i = 1 To 4
        mhgrps(i) = 1
    Next i
    For i = 9 To 10
        mhgrps(i) = 2
    Next i
    For i = 11 To 13
        mhgrps(i) = 3
    Next i
    For i = 14 To 15
        mhgrps(i) = 4
    Next i
    For i = 22 To 24
        mhgrps(i) = 5
    Next i
    
End Sub

Private Function ItemExists(col As Collection, key As String) As Boolean
    Dim dummy As Variant
    On Error Resume Next
    dummy = col.Item(key)
    ItemExists = (Err <> 5)
End Function

Private Sub AtLeastOneADL()
    Dim ind As Integer
    '03/23/06: ADL 2 is the minimum as decided in phone mtg on 3/22/06
    If Not (inds(1).checked Or inds(2).checked Or inds(3).checked) Then
        ind = 2
        inds(ind).checked = True
        dprint ind, 1, "via AtLeastOneADL"
    End If
End Sub

'Private Sub mhAtLeastOneADL()
'
'    If Not (mhinds(1).checked Or mhinds(2).checked Or mhinds(3).checked Or mhinds(4).checked) Then
'        mhinds(2).checked = True
'        dprint 1, 1, "via AtLeastOneADL"
'    End If
'End Sub

Private Sub ParseAlsoMark(s As String)
    Dim comma_pos As Integer
    Dim ind As Integer
    
    'for example:   29,31  is the also_mark string for RESTRAINTS
    
    Do
        comma_pos = InStr(s, ",")
        If (comma_pos = 0) Then
            ind = val(s)
        Else
            ind = val(Mid$(s, 1, comma_pos - 1))
            s = Mid$(s, comma_pos + 1, Len(s) - comma_pos)
        End If
        
        inds(ind).checked = True
        dprint ind, 1, "via Also-mark"
        
    Loop Until comma_pos = 0
End Sub
Private Sub mhParseAlsoMark(s As String)
    Dim comma_pos As Integer
    Dim ind As Integer
    
    'for example:   29,31  is the also_mark string for RESTRAINTS
    
    Do
        comma_pos = InStr(s, ",")
        If (comma_pos = 0) Then
            ind = val(s)
        Else
            ind = val(Mid$(s, 1, comma_pos - 1))
            s = Mid$(s, comma_pos + 1, Len(s) - comma_pos)
        End If
        
        mhinds(ind).checked = True
        dprint ind, 1, "via Also-mark"
        
    Loop Until comma_pos = 0
End Sub


Private Sub ParsePatientInfo(s As String)
    Dim fullname As String
    Dim commapos As Integer
    
    acctnum = Trim$(Mid$(s, START_ACCT_NUM, LEN_ACCT_NUM))
    
    fullname = Trim$(Mid$(s, START_PTNAME, LEN_PTNAME))
    commapos = InStr(fullname, ",")
    If commapos = 0 Then
        lastname = fullname
        firstname = ""
    Else
        lastname = Mid$(fullname, 1, commapos - 1)
        firstname = Mid$(fullname, commapos + 1, Len(fullname) - commapos)
    End If
    
    unitname = Trim$(Mid$(s, START_UNIT, LEN_UNIT))
    roomname = Trim$(Mid$(s, START_RM, LEN_RM))
    bedname = Trim$(Mid$(s, START_BED, LEN_BED))
    If Not efftimeon Then
        'no -efftime parameter; use date/time from patient header
        If IsDate(Mid$(s, START_CLASS_DT, LEN_DT)) Then
            intime = Trim$(Mid$(s, START_CLASS_DT, LEN_DT))
        End If
    End If

    Debug.Print "Acct="; acctnum
    Debug.Print "Name="; fullname
    Debug.Print "Unit="; unitname
    Debug.Print "Room="; roomname
    Debug.Print "Bed ="; bedname
End Sub


Private Sub AssembleOutput()
    On Error GoTo errHandler
    Dim basestr As String, outstr As String
    Dim i As Integer
    Dim din As Date
    Dim tin As Date
    Dim ok As Boolean
    
    ok = (acctnum <> "") And (unitname <> "")
    If Not ok Then Exit Sub

    intime = Left$(intime, 12)                  'yyyymmddhhnn

    basestr = Space$(379 + MAX_INDICATORS)
    Mid$(basestr, 1) = ""                       'facility code
    Mid$(basestr, 10) = unitname                'unit name
    Mid$(basestr, 27) = unitname                'unit code
    If telemetry Then
        Mid$(basestr, 44) = "telemetry"         'area code
    End If
    '(do not output the class date)
    Mid$(basestr, 70) = acctnum
    Mid$(basestr, 91) = lastname
    Mid$(basestr, 124) = firstname
    Mid$(basestr, 157) = ""                     'middle name
    Mid$(basestr, 190) = roomname
    Mid$(basestr, 199) = bedname

    'Generate the classification
    outstr = basestr
    Mid$(outstr, 204) = intime                   'class datetime
    Mid$(outstr, 296) = intime                   'class IN time
    
    For i = 1 To MAX_INDICATORS
        Mid$(outstr, 379 + i - 1) = IIf(inds(i).checked, "Y", "N")
    Next i
    
    Print #outfile, outstr
    Print #outlogfile, outstr
    
    'Now generate procedure records (if any)
    '
    For i = 1 To num_procs
        outstr = basestr
        With dicary(procs(i).dic_index)
            Debug.Print "proc p="; procs(i).dic_index
            Mid$(outstr, 256, 1) = "P"          'procedure record
            Mid$(outstr, 204, 14) = Format$(.start_dt, DT_FORMAT)
            Mid$(outstr, 348, 14) = Format$(.finish_dt, DT_FORMAT)
            Mid$(outstr, 379) = "nnnny"         '#5 = extensive wound care by RN
            Print #outfile, outstr
            Print #outlogfile, outstr
        End With
    Next i
    
    Exit Sub
    
errHandler:
    Debug.Print Err.Description
    Exit Sub
    Resume
End Sub


Private Sub CalcFrequency(p As Integer)
    Dim n As Integer
    Dim r As Single
    
'    r = range / RANGE_DENOMINATOR
    If ValidMSUnit(unitname) Then
        dicary(p).act_freq = range / dicary(p).num_found 'denom will never be zero
    ElseIf ValidMHUnit(unitname) Then
        mhdicary(p).act_freq = range / mhdicary(p).num_found 'denom will never be zero
    End If
' Use 8 hours as basis
'    Select Case dicary(p).ind
'        Case 14 To 22 'indicators 14-22 are to be counted instead of freq.
'            If dicary(p).num_found >= 4 * r And dicary(p).num_found <= 12 * r Then
'                dicary(p).act_freq = 240
'            ElseIf dicary(p).num_found > 12 * r And dicary(p).num_found <= 24 * r Then
'                dicary(p).act_freq = 60
'            ElseIf dicary(p).num_found > 24 * r Then
'                dicary(p).act_freq = 30
'            Else
'                dicary(p).act_freq = 480
'            End If
'        Case Else   ' Other values.
'            dicary(p).act_freq = range / dicary(p).num_found 'denom will never be zero
'    End Select

End Sub

Private Function CerDateToISO(d As String) As String
    Dim century As String
    ' Yields a string of the format yyyymmddhhnn from mm-dd-yy hh:nn
    
    If val(Mid$(d, 7, 2)) <= 50 Then
        century = "20"
    Else
        century = "19"
    End If
    CerDateToISO = century & Mid$(d, 7, 2) & Mid$(d, 1, 2) & Mid$(d, 4, 2) _
     & Mid$(d, 10, 2) & Mid$(d, 13, 2)

End Function

Private Sub ResetDictionaryFields()
    Dim i As Integer
    
    For i = 1 To dicnum
        dicary(i).num_found = 0
        dicary(i).act_freq = 0
        dicary(i).last_datetime = ""
        dicary(i).gavefreq = False
        dicary(i).one_row_find = 0
    Next i

    For i = 1 To mhdicnum
        mhdicary(i).num_found = 0
        mhdicary(i).act_freq = 0
        mhdicary(i).last_datetime = ""
        mhdicary(i).gavefreq = False
        mhdicary(i).one_row_find = 0
    Next i
End Sub

Private Sub OpenLogFiles()
    Const HKEY_LOCAL_MACHINE = &H80000002
    Dim dt As Variant

    chartImportPath = g_util.GetProfileString(INI_FNAME, "DIRS", "ImportDir", "..\load_me")
    winloadpath = g_util.GetProfileString(INI_FNAME, "DIRS", "LoadDir", "..\load_me")
    winlogpath = g_util.GetProfileString(INI_FNAME, "DIRS", "LogDir", "..\log")

    chartImportPath = g_util.ExpandPath(chartImportPath)
    winloadpath = g_util.ExpandPath(winloadpath)
    winlogpath = g_util.ExpandPath(winlogpath)

    dt = nowdt
    inlogname = winlogpath & "\TC_Input_" & Format$(dt, "mmdd") & ".log"
    outlogname = winlogpath & "\TC_Output_" & Format$(dt, "mmdd") & ".log"
    dbugname = winlogpath & "\TC_Debug_" & Format$(dt, "mmdd") & ".log"
    
    inlogfile = FreeFile
    Open inlogname For Append As #inlogfile
    Print #inlogfile, ""
    Print #inlogfile, String$(80, "=")
    Print #inlogfile, "AcuityPlus Transparent Classification - Input    Time=" & nowdt
    Print #inlogfile, ""
    
    outlogfile = FreeFile
    Open outlogname For Append As #outlogfile
    Print #outlogfile, ""
    Print #outlogfile, String$(80, "=")
    Print #outlogfile, "AcuityPlus Transparent Classification - Output    Time=" & nowdt
    Print #outlogfile, ""
    
    If dbugon Then
        dbugfile = FreeFile
        Open dbugname For Append As #dbugfile
        Print #dbugfile, ""
        Print #dbugfile, String$(80, "=")
        Print #dbugfile, "TRANSPARENT TRANSLATION DEBUGGING MODE    Time=" & nowdt
        Print #dbugfile, ""
    End If
    
End Sub

Private Sub CloseLogFiles()
    Close #inlogfile
    Close #outlogfile
    If dbugon Then
        Close #dbugfile
    End If

    'inlogfile's dt = mid$(inlogname,len(inlogname)-len("mmdd.txt")+1,len("mmddhh.txt"))
'    FileCopy outfn, winlogpath & "\TranspOut_" & Mid$(inlogname, Len(inlogname) - Len("mmddhh.txt") + 1, Len("mmddhh.txt"))
End Sub

Private Sub DeleteOldLogs()
    Dim temp As String
    Dim i As Integer
    Dim dt As Variant

    ' Delete old log files
    ' (allow the interface to be down for up to 5 days)
    dt = DateAdd("d", -TRANSP_INLOG_FILE_LIFE, nowdt)
    For i = 1 To 5
        temp = winlogpath & "\TranspIn_" & Format$(dt, "mmdd") & ".log"
        If g_util.FileExists(temp) Then
            Kill temp
        End If
        dt = DateAdd("d", -1, dt)
    Next i

    dt = DateAdd("d", -TRANSP_OUTLOG_FILE_LIFE, nowdt)
    For i = 1 To 5
        temp = winlogpath & "\TranspOut_" & Format$(dt, "mmdd") & ".log"
        If g_util.FileExists(temp) Then
            Kill temp
        End If
        dt = DateAdd("d", -1, dt)
    Next i
    
    dt = DateAdd("d", -TRANSP_DEBUG_FILE_LIFE, nowdt)
    For i = 1 To 5
        temp = winlogpath & "\DbugTransp_" & Format$(dt, "mmdd") & ".log"
        If g_util.FileExists(temp) Then
            Kill temp
        End If
        dt = DateAdd("d", -1, dt)
    Next i

End Sub


Private Sub dprint(ind As Integer, status As Integer, s As String)
    Dim sstat As String

    If Not dbugon Or suppressDbugLog Then
        Exit Sub
    End If
    
    If ind = -1 Then
        Print #dbugfile, s
    ElseIf ind = -2 Then
        Print #dbugfile, "The following event was Out of Range and was rejected."
        Print #dbugfile, s
    ElseIf ind = -3 Then
        Print #dbugfile, "  * " & s
    ElseIf ind = 0 Then
        Print #dbugfile, "    " & s
    Else
        If (status = 1) Or (status = -1) Then
            sstat = " -TURNED ON- "
        Else
            sstat = "   "
        End If
        Print #dbugfile, "  * Indicator " & ind & sstat & s
    End If
            
End Sub
Private Function ValidMSUnit(u As String) As Boolean
    Dim i As Integer
    
    For i = 1 To unitnum
        If UCase$(u) = MSunitary(i) Then
            ValidMSUnit = True
            Exit For
        End If
    Next i

End Function
Private Function ValidMHUnit(u As String) As Boolean
    Dim i As Integer
    
    For i = 1 To mhunitnum
        If UCase$(u) = MHunitary(i) Then
            ValidMHUnit = True
            Exit For
        End If
    Next i

End Function


'Private Sub Check1()
'    Dim charting As String
'    Dim i1, i2, i3 As Single
'
'    suppressDbugLog = True
'
''    charting = "self"
''    i1 = DicIndex("16095", charting)
''
''    charting = "activity ad lib"
''    i2 = DicIndex("16992", charting)
'
'    charting = "self"
'    i3 = DicIndex("33405", charting)
'    suppressDbugLog = False
'
''    If (dicary(i1).num_found > 0 Or dicary(i2).num_found > 0) And _
''       dicary(i3).num_found > 0 Then
''       suppressDbugLog = False
''       inds(1).checked = True
''       dprint 1, 1, "Triggered by (16095 or 16992) and 33405"
''    End If
'    If dicary(i3).num_found > 0 Then
'       inds(1).checked = True
'       dprint 1, 1, "Triggered by 33405 self"
'    End If
'
'End Sub
'
'Private Sub Check2_3_4()
'    Dim charting As String
'    Dim i9, i9a, i9b As Single
'
'    suppressDbugLog = True
'
'    If inds(3).checked Then
'        Exit Sub
'    End If
'
'    charting = "unresponsive"
'    i9 = DicIndex("5929", charting)
''    charting = "respond to pain"
''    i9a = DicIndex("5929", charting)
''    charting = "respond to voice"
''    i9b = DicIndex("5929", charting)
'
'    suppressDbugLog = False
'
''    If (dicary(i9).num_found > 0) Or (dicary(i9a).num_found > 0) Or (dicary(i9b).num_found > 0) Then
'    If (dicary(i9).num_found > 0) Then
'       inds(3).checked = True
'       dprint 3, 1, "Triggered by 5929 unresponsive"  '/respond to pain/voice"
'    End If
'
'
'End Sub
'
'Private Sub Check6()
'    Dim charting As String
'    Dim idic, ibegin, iend, i9, i9a, i9b, i9c, i9d, i9e, i1, i2, i3 As Single
'
'    suppressDbugLog = True
'
'    If inds(6).checked Then
'        Exit Sub
'    End If
'
'    'Find Beginning of indicator 6 in Dictionary with 5929 factor
'    '6*20425                             amplification
'    charting = "amplification"
'    ibegin = DicIndex("20425", charting)
'    'Find Ending of ind 6:
'    '6*35542                             non-English
'    charting = "non-English"
'    iend = DicIndex("35542", charting)
'
'    charting = "unresponsive"
'    i9 = DicIndex("5929", charting)
'    charting = "respond to pain"
'    i9a = DicIndex("5929", charting)
'    charting = "respond to voice"
'    i9b = DicIndex("5929", charting)
'    charting = "sedated"
'    i9c = DicIndex("5929", charting)
'    charting = "anesthesized"
'    i9d = DicIndex("5929", charting)
'    charting = "paralytic"
'    i9e = DicIndex("5929", charting)
'
'    suppressDbugLog = False
'
'    If dicary(i9).num_found = 0 And dicary(i9a).num_found = 0 Then
'        For idic = ibegin To iend
'            If dicary(idic).num_found > 0 Then
'                If dicary(idic).eventid = "24359" Or dicary(idic).eventid = "24363" Or dicary(idic).eventid = "24364" Or dicary(idic).eventid = "33423" Then
'                    If dicary(i9b).num_found > 0 Or dicary(i9c).num_found > 0 Or dicary(i9d).num_found > 0 Or dicary(i9e).num_found > 0 Then
'                        dprint 6, 1, "NOT Triggered due to respond to voice,sedated,anesth,paraly"
'                    Else
'                        inds(6).checked = True
'                        dprint 6, 1, "Triggered by not(unresposive,respond to pain) & something between amplification and non-English"
'                    End If
'                Else
'                    inds(6).checked = True
'                    dprint 6, 1, "Triggered by not(unresposive,respond to pain) & something between amplification and non-English"
'                End If
'            End If
'        Next idic
'    End If
'
'    If inds(6).checked Then
'        Exit Sub
'    End If
'
'    suppressDbugLog = True
'    charting = "capped"
'    i1 = DicIndex("9506", charting)
'    charting = "trach"
'    i2 = DicIndex("35253", charting)
'    charting = "et tube"
'    i3 = DicIndex("35253", charting)
'    charting = "awake"
'    i9 = DicIndex("5929", charting)
'    suppressDbugLog = False
'
'    If (dicary(i1).num_found > 0 Or dicary(i2).num_found > 0 Or dicary(i3).num_found > 0) And _
'       dicary(i9).num_found > 0 Then
'        inds(6).checked = True
'        dprint 6, 1, "Triggered by (capped or trach or et tube) & Awake"
'    End If
'
'
'End Sub
'
'Private Sub Check7()
'    Dim charting As String
'    Dim i1, i1a, i1b, i1c, i1d, i1e, i1f, i1g As Single
'    suppressDbugLog = True
''
''    If inds(7).checked Then
''        Exit Sub
''    End If
''
''    '7*34073                             alt mental stat
''    charting = "alt ment"
''    i1 = DicIndex("34073", charting)
''    suppressDbugLog = False
''
''    If dicary(i1).num_found > 0 Then
''        inds(7).checked = True
''        dprint 7, 1, "Triggered by alt ment"
''    End If
'        charting = "alterd ment"
'        i1a = DicIndex("24359", charting)
'        charting = "alterd ment"
'        i1b = DicIndex("24363", charting)
'        charting = "alterd ment"
'        i1c = DicIndex("24364", charting)
'        charting = "alt ment"
'        i1d = DicIndex("34073", charting)
'        charting = "confused"
'        i1e = DicIndex("5929", charting)
'        charting = "combative"
'        i1f = DicIndex("5929", charting)
'        charting = "agitated"
'        i1g = DicIndex("5929", charting)
'      suppressDbugLog = False
'        If dicary(i1a).num_found + _
'           dicary(i1b).num_found + _
'           dicary(i1c).num_found + _
'           dicary(i1d).num_found + _
'           dicary(i1e).num_found + _
'           dicary(i1f).num_found + _
'           dicary(i1g).num_found > 0 Then
'            inds(7).checked = True
'            dprint 7, 1, "Triggered by altered mental status or confused/combative/agitated"
'        End If
'
'
'End Sub
'
'Private Sub Check8()
'    Dim charting As String
'    Dim i1, i2, i3, i1a, i1b, i1c, i1d, i1e, i1f, i1g As Single
'    Dim maxcount1 As Integer
''123456789012345678901234567890
'' 8*33682             30              patient checked
'' 8*34064             120
'' 8*34070             120
'' 8*34072             120
'' 8*34074                             bed exit alarm on
'' 8*34074                             near nur station
'' 8*34074                             toileting assist
'' assume data range is 24 hours=1440 mins
'
'    suppressDbugLog = True
'
'    If inds(8).checked Then
'        Exit Sub
'    End If
'
''    If range >= 1440 Then
''        maxcount1 = 12
''    ElseIf range >= 480 Then
''        maxcount1 = 8
''    End If
''
''    charting = "patient checked"
''    i1 = DicIndex("33682", charting)
''    suppressDbugLog = False
''    If dicary(i1).num_found >= maxcount1 Then
''        inds(8).checked = True
''        dprint 8, 1, "Triggered by patient checked f=" & str(dicary(i1).num_found)
''    End If
''
''    suppressDbugLog = True
''    charting = "pt at nsg"
''    i1 = DicIndex("33682", charting)
''    suppressDbugLog = False
''    If dicary(i1).num_found >= maxcount1 Then
''        inds(8).checked = True
''        dprint 8, 1, "Triggered by pt at nsg desk f=" & str(dicary(i1).num_found)
''    End If
'
'' 7#24359                                         alterd ment
'' 7#24363                                         alterd ment
'' 7#24364                                         alterd ment
'' 7#34073                                         alt ment
'' 7@5929                                          confused
'' 7@5929                                          combative
'' 7@5929                                          agitated
'
'    charting = "patient checked"
'    i1 = DicIndex("33682", charting)
'    suppressDbugLog = False
'    If dicary(i1).num_found > 0 Then
'        suppressDbugLog = True
'
'        charting = "alterd ment"
'        i1a = DicIndex("24359", charting)
'        charting = "alterd ment"
'        i1b = DicIndex("24363", charting)
'        charting = "alterd ment"
'        i1c = DicIndex("24364", charting)
'        charting = "alt ment"
'        i1d = DicIndex("34073", charting)
'        charting = "confused"
'        i1e = DicIndex("5929", charting)
'        charting = "combative"
'        i1f = DicIndex("5929", charting)
'        charting = "agitated"
'        i1g = DicIndex("5929", charting)
'        suppressDbugLog = False
'
'        If dicary(i1a).num_found + _
'           dicary(i1b).num_found + _
'           dicary(i1c).num_found + _
'           dicary(i1d).num_found + _
'           dicary(i1e).num_found + _
'           dicary(i1f).num_found + _
'           dicary(i1g).num_found > 0 Then
'            inds(8).checked = True
'            dprint 8, 1, "Triggered by patient checked + other"
'        End If
'    End If
'
'    suppressDbugLog = True
'    charting = "pt at nsg"
'    i1 = DicIndex("33682", charting)
'    suppressDbugLog = False
'    If dicary(i1).num_found > 0 Then
'        inds(8).checked = True
'        dprint 8, 1, "Triggered by pt at nsg desk"
'    End If
'
'
'    If range >= 1440 Then
'        maxcount1 = 6
'    ElseIf range >= 480 Then
'        maxcount1 = 3
'    End If
'    suppressDbugLog = True
'    charting = ""
'    i1 = DicIndex("34064", charting)
'    suppressDbugLog = False
'    If dicary(i1).num_found >= maxcount1 Then
'        inds(8).checked = True
'        dprint 8, 1, "Triggered by 34064 f=" & str(dicary(i1).num_found)
'    End If
'
'    suppressDbugLog = True
'    charting = ""
'    i1 = DicIndex("34070", charting)
'    suppressDbugLog = False
'    If dicary(i1).num_found > 0 Then
'        inds(33).checked = True
'    End If
'    If dicary(i1).num_found >= maxcount1 Then
'        inds(8).checked = True
'        dprint 8, 1, "Triggered by 34070 f=" & str(dicary(i1).num_found)
'    End If
'
'    suppressDbugLog = True
'    charting = ""
'    i1 = DicIndex("34072", charting)
'    suppressDbugLog = False
'    If dicary(i1).num_found >= maxcount1 Then
'        inds(8).checked = True
'        dprint 8, 1, "Triggered by 34072 f=" & str(dicary(i1).num_found)
'    End If
'
'' 8*34074                             bed exit alarm on
'' 8*34074                             near nur station
'' 8*34074                             toileting assist
'    suppressDbugLog = True
'    charting = "bed exit alrm on"
'    i1 = DicIndex("34074", charting)
'    charting = "near nur station"
'    i2 = DicIndex("34074", charting)
'    charting = "toil"
'    i3 = DicIndex("34074", charting)
'
'    suppressDbugLog = False
'    If dicary(i1).num_found > 0 And dicary(i2).num_found > 0 And _
'       dicary(i3).num_found > 0 Then
'        inds(8).checked = True
'        dprint 8, 1, "Triggered by bed exit alarm,near nur sta, toileting asst"
'    End If
'
'
'End Sub
'
'
'Private Sub Check9()
'    Dim charting As String
'    Dim i1 As Single
'    Dim maxcount1 As Integer
'' 9 23265
'' 9*6355              60              suicidal ideatns
'
'    If inds(9).checked Then
'        Exit Sub
'    End If
'    suppressDbugLog = True
'
'    If range >= 1440 Then
'        maxcount1 = 12
'    ElseIf range >= 720 Then
'        maxcount1 = 6
'    ElseIf range >= 480 Then
'        maxcount1 = 4
'    End If
'
'    charting = "suicidal ideatns"
'    i1 = DicIndex("6355", charting)
'    suppressDbugLog = False
'    If dicary(i1).num_found >= maxcount1 Then
'        inds(9).checked = True
'        dprint 9, 1, "Triggered by 6355 f=" & str(dicary(i1).num_found)
'    End If
'
'End Sub
'
''Private Sub Check11()
''    Dim charting As String
''    Dim i1, i2, i3, i4, i5, i5a, i6, i7, i8 As Single
''    Dim countIntake, countOutput As Single
''    Dim ibegin, iend, idic As Single
''    Dim maxcount1, maxcount2, maxcount3 As Single
''
''End Sub
'
'Private Sub Check12_13()
'    Dim charting As String
'    Dim idic, ibegin, iend As Single
'    Dim i1, i1a, i1b, i2, i3, i4 As Single
'    Dim maxcount12, maxcount13 As Integer
''12*6240              120             NPO
''12*6240              120             NPO except meds
''12*6240              120             NPO with chips
''12*6913              120
''12*16966             120
''12*21429             120
''12*35333             120             CBI Flowsheet
'
'    suppressDbugLog = True
'
'    charting = "NPO"
'    ibegin = DicIndex("6240", charting)
'    charting = "CBI Flowsheet"
'    iend = DicIndex("35333", charting)
'
'    charting = ""
'    i2 = DicIndex("6913", charting)
'
'    charting = ""
'    i3 = DicIndex("16966", charting)
'
'    charting = ""
'    i4 = DicIndex("34072", charting)
'    suppressDbugLog = False
'
'    If dicary(i2).num_found >= 0 Or dicary(i3).num_found >= 0 Or dicary(i4).num_found >= 0 Then
'        inds(11).checked = True
'        dprint 11, 1, "Triggered by 6913 or 16966 or 34072"
'    End If
'
'    If range >= 1440 Then
'        maxcount12 = 6
'        maxcount13 = 12
'    ElseIf range >= 720 Then
'        maxcount12 = 3
'        maxcount13 = 6
'    ElseIf range >= 480 Then
'        maxcount12 = 3
'        maxcount13 = 4
'    End If
'
'    For idic = ibegin To iend
'        If dicary(idic).num_found >= maxcount12 Then
'            inds(12).checked = True
'            dprint 12, 1, "Triggered by 6240,16966,21429,35333"
'        End If
'        If dicary(idic).num_found >= maxcount13 Then
'            inds(13).checked = True
'            dprint 13, 1, "Triggered by 6240,16966,21429,35333"
'        End If
'    Next idic
'
'End Sub
'
'
''Medication
'Private Sub Check14_15_16()
'    Dim charting As String
'    Dim ibegin, iend, idic, count6119, maxcount1, maxcount2, maxcount3 As Single
'    Dim ipapermar As Single
'    Dim i6879, i6880, i6881 As Single
''14*6119              240             epidural
''...
''14*40717             240
'
'    suppressDbugLog = True
'
'    count6119 = 0
'
'    charting = "epidural"
'    ibegin = DicIndex("6119", charting)
'    charting = "pca pump"
'    iend = DicIndex("6119", charting)
'
'    charting = ""
'    ipapermar = DicIndex("43817", charting)
'
'    suppressDbugLog = False
'
'    For idic = ibegin To iend
'        count6119 = count6119 + dicary(idic).num_found
'    Next idic
'
'    maxcount1 = 5
'    maxcount2 = 12
'    maxcount3 = 24
'    If range >= 1440 Then
'        maxcount1 = 5
'        maxcount2 = 12
'        maxcount3 = 24
'    ElseIf range >= 720 Then
'        maxcount1 = 3
'        maxcount2 = 6
'        maxcount3 = 12
'    ElseIf range >= 480 Then
'        maxcount1 = 2
'        maxcount2 = 4
'        maxcount3 = 8
'    End If
'
''Check counts of 18098,6119
'    If count18098 >= maxcount3 Or count6119 >= maxcount3 Then
'        inds(16).checked = True
'        dprint 16, 1, "Triggered by f(6119 or 18098) >= 24 hr level"
'    ElseIf count18098 >= maxcount2 Or count6119 >= maxcount2 Then
'        inds(15).checked = True
'        dprint 15, 1, "Triggered by f(6119 or 18098) >= 12 hr level"
'    ElseIf count18098 > maxcount1 Or count6119 >= maxcount1 Then
'        inds(14).checked = True
'        dprint 14, 1, "Triggered by f(6119 or 18098) >= 8 hr level"
'    End If
'
''14*17002                         480             medicated
''14*40717                         480
'    suppressDbugLog = True
'    charting = "medicated"
'    ibegin = DicIndex("17002", charting)
'    charting = ""
'    iend = DicIndex("41117", charting)
'
'    charting = ""
'    i6879 = DicIndex("6879", charting)
'    charting = ""
'    i6880 = DicIndex("6880", charting)
'    charting = ""
'    i6881 = DicIndex("6881", charting)
'
'    suppressDbugLog = False
'
''Pull                    7am   5pm     8pm      5am
''Time frame      Prev 24 hr   7a-5p   7a-5p   7p-5a
''Classification Time     7a    11a     7p      11p
''A   q4 hrs          3 times  1 time  2 times  1 time
''B   q2 hrs          6 times  2 time  3 times  2 time
''C   q1 hr           6 times  4 time  6 times  4 time
''D   q 30 min       24 times  8 times 12 times 8 times
'    maxcount1 = 3
'    maxcount2 = 6
'    maxcount3 = 24
'    If range >= 1440 Then
'        maxcount1 = 3
'        maxcount2 = 6
'        maxcount3 = 24
'    ElseIf range >= 720 Then
'        maxcount1 = 3
'        maxcount2 = 6
'        maxcount3 = 12
'    ElseIf range >= 480 Then
'        maxcount1 = 1
'        maxcount2 = 4
'        maxcount3 = 8
'    End If
'
'If dicary(i6879).num_found > 0 And dicary(i6880).num_found > 0 And dicary(i6881).num_found > 0 Then
'    For idic = ibegin To iend
'        If dicary(idic).num_found >= maxcount3 Then
'            inds(16).checked = True
'            dprint 16, 1, "Triggered by freq of " & dicary(idic).eventid & " >= 24 hr level"
'        ElseIf dicary(idic).num_found >= maxcount2 Then
'            inds(15).checked = True
'            dprint 15, 1, "Triggered by freq of " & dicary(idic).eventid & " >= 12 hr level"
'        ElseIf dicary(idic).num_found >= maxcount1 Then
'            inds(14).checked = True
'            dprint 14, 1, "Triggered by freq of " & dicary(idic).eventid & " >= 8 hr level"
'        End If
'    Next idic
'Else
'    dprint 0, 1, " 6879+6880+6881 NOT found"
'End If
'
'    If dicary(ipapermar).num_found >= maxcount3 Then
'        inds(16).checked = True
'        dprint 16, 1, "Triggered by f(paper MAR) >= 24 hr level"
'    ElseIf dicary(ipapermar).num_found >= maxcount2 Then
'        inds(15).checked = True
'        dprint 15, 1, "Triggered by f(paper MAR) >= 12 hr level"
'    ElseIf dicary(ipapermar).num_found >= maxcount1 Then
'        inds(14).checked = True
'        dprint 14, 1, "Triggered by f(paper MAR) >= 8 hr level"
'    End If
'
'End Sub
'

'Assessment
Private Sub check_14_15_16_17()
    On Error GoTo errHandler
    Dim idic As Integer, save_idic As Integer

    For idic = 1 To dicnum
        With dicary(idic)
            If (.ind >= 14) And (.ind <= 17) Then
                Debug.Print "ind="; .ind; " act-f="; .act_freq; " f="; .freq_basis; " found="; .num_found
                
                If .num_found > 0 Then
                    inds(.ind).checked = True
                End If
            End If
        End With
    Next idic

    Exit Sub

errHandler:
    g_util.ThrowError "check_14_15_16_17"
    Resume  'debug

End Sub

'
'Private Sub Check17_18_19()
'    Dim charting As String
'    Dim ibegin, iend, idic As Single
'    Dim maxcount1, maxcount2, maxcount3 As Single
'
'    suppressDbugLog = True
'
''17*5793              240
''...
''17*41019             240
'    charting = ""
'    ibegin = DicIndex("5793", charting)
'    charting = ""
'    iend = DicIndex("41019", charting)
'
'    suppressDbugLog = False
'
'
'    maxcount1 = 6
'    maxcount2 = 12
'    maxcount3 = 24
'    If range >= 1440 Then
'        maxcount1 = 6
'        maxcount2 = 12
'        maxcount3 = 24
'    ElseIf range >= 720 Then
'        maxcount1 = 3
'        maxcount2 = 6
'        maxcount3 = 12
'    ElseIf range >= 480 Then
'        maxcount1 = 2
'        maxcount2 = 4
'        maxcount3 = 8
'    End If
'
'    For idic = ibegin To iend
'        If dicary(idic).num_found >= maxcount3 Then
'            inds(19).checked = True
'            dprint 19, 1, "Triggered by f(" & dicary(idic).eventid & " at levels for 24 hours"
'        ElseIf dicary(idic).num_found >= maxcount2 Then
'            inds(18).checked = True
'            dprint 18, 1, "Triggered by f(" & dicary(idic).eventid & " at levels for >=12 hours"
'        ElseIf dicary(idic).num_found >= maxcount1 Then
'            inds(17).checked = True
'            dprint 17, 1, "Triggered by f(" & dicary(idic).eventid & " at levels for >=8 hours"
'        End If
'    Next idic
'
'    'note that 16187 triggers 19 and 22
'
'
'End Sub
'
'Private Sub Check20_21_22()
'    Dim charting As String
'    Dim ibegin, iend, idic, i1, i1a, i1b, i1c, i1d As Single
'    Dim maxcount1, maxcount2, maxcount3 As Single
'    suppressDbugLog = True
'
'
'    maxcount1 = 6
'    maxcount2 = 12
'    maxcount3 = 24
'    If range >= 1440 Then
'        maxcount1 = 6
'        maxcount2 = 12
'        maxcount3 = 24
'    ElseIf range >= 720 Then
'        maxcount1 = 3
'        maxcount2 = 6
'        maxcount3 = 12
'    ElseIf range >= 480 Then
'        maxcount1 = 2
'        maxcount2 = 4
'        maxcount3 = 8
'    End If
'
''20*5749              240
''...
''20*35258             240
''20*40927             240
'    charting = ""
'    ibegin = DicIndex("5749", charting)
'    charting = ""
'    iend = DicIndex("40927", charting)
'
'    suppressDbugLog = False
'    For idic = ibegin To iend
'        If dicary(idic).num_found >= maxcount3 Then
'            inds(22).checked = True
'            dprint 22, 1, "Triggered by f(" & dicary(idic).eventid & " at levels for 24 hours"
'        ElseIf dicary(idic).num_found >= maxcount2 Then
'            inds(21).checked = True
'            dprint 21, 1, "Triggered by f(" & dicary(idic).eventid & " at levels for >=12 hours"
'        ElseIf dicary(idic).num_found >= maxcount1 Then
'            inds(20).checked = True
'            dprint 20, 1, "Triggered by f(" & dicary(idic).eventid & " at levels for >=8 hours"
'        End If
'    Next idic
'
''20*5835              240             brisk
''20*5835              240             normal
''20*5835              240             sluggish
''21*5835              60
''22*5835              30
'    suppressDbugLog = True
'    charting = "brisk"
'    i1 = DicIndex("5835", charting)
'    charting = "normal"
'    i1a = DicIndex("5835", charting)
'    charting = "sluggish"
'    i1b = DicIndex("5835", charting)
'    charting = ""
'    i1c = DicIndex("5835", charting)
'
'    suppressDbugLog = False
'    If dicary(i1).num_found >= 2 Or _
'       dicary(i1a).num_found >= 2 Or _
'       dicary(i1b).num_found >= 2 Then
'        inds(20).checked = True
'        dprint 20, 1, "Triggered by f(brisk, normal or sluggish)=2"
'    End If
'
'    If dicary(i1c).num_found >= maxcount3 Then
'        inds(22).checked = True
'        dprint 22, 1, "Triggered by f(5835)= at 24 hours"
'    ElseIf dicary(i1c).num_found >= maxcount2 Then
'        inds(21).checked = True
'        dprint 21, 1, "Triggered by f(5835)= at >=12 hours"
'    End If
'
''21*8                 60              flush bag chg
''21*8                 60              press bag flush
''21*8                 60              press tube chg
''21*8                 60              wave form damp
''21*8                 60              zeroed
'    suppressDbugLog = True
'    charting = "flush bag chg"
'    i1 = DicIndex("8", charting)
'    charting = "press bag flush"
'    i1a = DicIndex("8", charting)
'    charting = "press tube chg"
'    i1b = DicIndex("8", charting)
'    charting = "wave form damp"
'    i1c = DicIndex("8", charting)
'    charting = "zeroed"
'    i1d = DicIndex("8", charting)
'
'    suppressDbugLog = False
'    If dicary(i1).num_found >= maxcount2 Or _
'       dicary(i1a).num_found >= maxcount2 Or _
'       dicary(i1b).num_found >= maxcount2 Or _
'       dicary(i1c).num_found >= maxcount2 Or _
'       dicary(i1d).num_found >= maxcount2 Then
'        inds(21).checked = True
'        dprint 21, 1, "Triggered by flush bag, tube chg, wave form, zeroed at f=12 hours"
'    End If
'
''22*35086             30
''22*35087             30
''22*35088             30
''22*35089             30
'    suppressDbugLog = True
'    charting = ""
'    i1 = DicIndex("35086", charting)
'    charting = ""
'    i1a = DicIndex("35087", charting)
'    charting = ""
'    i1b = DicIndex("35088", charting)
'    charting = ""
'    i1c = DicIndex("35089", charting)
'
'    suppressDbugLog = False
'    If dicary(i1).num_found >= maxcount3 Or _
'       dicary(i1a).num_found >= maxcount3 Or _
'       dicary(i1b).num_found >= maxcount3 Or _
'       dicary(i1c).num_found >= maxcount3 Then
'        inds(22).checked = True
'        dprint 22, 1, "Triggered by 35806-35809 at f=24 hours"
'    ElseIf dicary(i1).num_found >= maxcount2 Or _
'       dicary(i1a).num_found >= maxcount2 Or _
'       dicary(i1b).num_found >= maxcount2 Or _
'       dicary(i1c).num_found >= maxcount2 Then
'        inds(21).checked = True
'        dprint 21, 1, "Triggered by 35806-35809 at f=12 hours"
'    End If
'
'End Sub
'
'Private Sub Check23()
'    Dim charting As String
'
'    suppressDbugLog = True
'
'    If event33765 Then
'       inds(23).checked = True
'    End If
'
'    If event33775 And val33775 <= 16 Then
'        inds(23).checked = True
'    End If
'
'    If event35286 And val35286 >= 6 Then
'        inds(23).checked = True
'    End If
'
'    suppressDbugLog = False
'
'End Sub


'PFS/QM Inpatient Wound Care - 24-26 - rated extensive and complex.
'
'Private Sub Check24_25_26()
'    Dim charting As String
'    Dim ibegin, ibegin2, iend, idic, idic2, count, i As Single
'    Dim eid As String
'
'    suppressDbugLog = True
'
''24*25547
''24*25549
''24*25551
''24*25553
''24*25555
''24*25557
''24*25560
''24*25562
''24*25564
''24*25566
''24*25568
''24*25570
''24*25620
''24*25622
''24*25624
''24*25626
''24*25628
''24*25630
''24*27386
''24*27392
''...27479
'    'should these be pro-rated according to the range?  Is it just these counts
'    'as specified in the mapping doc.
'    charting = ""
'    ibegin = DicIndex("25547", charting)
'    charting = ""
'    iend = DicIndex("27479", charting)
'
'    For idic = ibegin To iend
'        If dicary(idic).one_row_find >= 3 And dicary(idic).ind = 26 Then
'            inds(26).checked = True
'            dprint 26, 1, "Triggered by f>=3 of " & dicary(idic).eventid
'        ElseIf dicary(idic).one_row_find = 2 And dicary(idic).ind = 25 Then
'            inds(25).checked = True
'            dprint 25, 1, "Triggered by f=2 of " & dicary(idic).eventid
'        ElseIf dicary(idic).one_row_find = 1 And dicary(idic).ind = 24 Then
'            inds(24).checked = True
'            dprint 24, 1, "Triggered by f=1 of " & dicary(idic).eventid
'        End If
''        If dicary(idic).num_found >= 3 Then
''            inds(26).checked = True
''            dprint 26, 1, "Triggered by f>=3 of " & dicary(idic).eventid
''        ElseIf dicary(idic).num_found = 2 Then
''            inds(25).checked = True
''            dprint 25, 1, "Triggered by f=2 of " & dicary(idic).eventid
''        ElseIf dicary(idic).num_found = 1 Then
''            inds(24).checked = True
''            dprint 24, 1, "Triggered by f=1 of " & dicary(idic).eventid
''        End If
'    Next idic
'
''Wound data only for 24
''24*33631                                         clean
''24*33631                                         drsg drng/intact
''24*33631                                         drsg dry/intact
''24*33631                                         reddened
''24*33631                                         staples
''24*33631                                         steri-strips
''24*33638                                         sutures
'    For idic2 = 1 To 8
'        count = 0
'        eid = CStr(33630 + idic2)
'        charting = "clean"
'        ibegin = DicIndex(eid, charting)
'        charting = "sutures"
'        iend = DicIndex(eid, charting)
'
'        For idic = ibegin To iend
'            If dicary(idic).num_found > 0 Then
'                count = count + 1
'            End If
'        Next idic
'
'        If count >= 1 Then
'            inds(24).checked = True
'            dprint 24, 1, "Triggered by f>=1 of " & str(33630 + idic2)
'        End If
'    Next idic2
'
''Common wound data for 24-26
''24*33631                             bone visible
''...
''24*33638                             unapproximated
'    For idic2 = 1 To 8
'        count = 0
'        eid = CStr(33630 + idic2)
'        charting = "compress dev usd"
'        ibegin = DicIndex(eid, charting)
'        charting = "unapproximated"
'        iend = DicIndex(eid, charting)
'
'        For idic = ibegin To iend
'            If dicary(idic).one_row_find >= 4 And dicary(idic).ind = 26 Then
'                inds(26).checked = True
'                dprint 26, 1, "Triggered by f>=4 of " & str(33630 + idic2)
'            ElseIf dicary(idic).one_row_find >= 3 And dicary(idic).ind = 25 Then
'                inds(25).checked = True
'                dprint 25, 1, "Triggered by f>=3 of " & str(33630 + idic2)
'            ElseIf dicary(idic).one_row_find >= 1 And dicary(idic).ind = 24 Then
'                inds(24).checked = True
'                dprint 24, 1, "Triggered by f>=1 of " & str(33630 + idic2)
'            End If
''            If dicary(idic).num_found > 0 Then
''                count = count + 1
''            End If
'        Next idic
'
''        If count >= 4 Then
''            inds(26).checked = True
''            dprint 26, 1, "Triggered by f>=4 of " & str(33630 + idic2)
''        ElseIf count = 3 Then
''            inds(25).checked = True
''            dprint 25, 1, "Triggered by f=3 of " & str(33630 + idic2)
''        ElseIf count >= 1 Then
''            inds(24).checked = True
''            dprint 24, 1, "Triggered by f>=1 of " & str(33630 + idic2)
''        End If
'    Next idic2
'
''Wound data only for 25-26
''25*33638                                         bone visible
''25*33638                                         dehisced
''25*33638                                         sandbag intact
''25*33638                                         tendon visible
'    For idic2 = 1 To 8
'        count = 0
'        eid = CStr(33630 + idic2)
'        charting = "bone visible"
'        ibegin = DicIndex(eid, charting)
'        charting = "tendon visible"
'        iend = DicIndex(eid, charting)
'
'        For idic = ibegin To iend
'            If dicary(idic).one_row_find >= 4 And dicary(idic).ind = 26 Then
'                inds(26).checked = True
'                dprint 26, 1, "Triggered by f>=4 of " & str(33630 + idic2)
'            ElseIf dicary(idic).one_row_find >= 3 And dicary(idic).ind = 25 Then
'                inds(25).checked = True
'                dprint 25, 1, "Triggered by f>=3 of " & str(33630 + idic2)
'            End If
''            If dicary(idic).num_found > 0 Then
''                count = count + 1
''            End If
'        Next idic
'
''        If count >= 4 Then
''            inds(26).checked = True
''            dprint 26, 1, "Triggered by f>=4 of " & str(33630 + idic2)
''        ElseIf count >= 3 Then
''            inds(25).checked = True
''            dprint 25, 1, "Triggered by f>=3 of " & str(33630 + idic2)
''        End If
'    Next idic2
'
'
''24*33724
''...
''24*33785
'    charting = ""
'    ibegin = DicIndex("33724", charting)
'    charting = ""
'    iend = DicIndex("33785", charting)
'
'    For idic = ibegin To iend
'        If dicary(idic).one_row_find >= 4 And dicary(idic).ind = 26 Then
'            inds(26).checked = True
'            dprint 26, 1, "Triggered by f>=4 of " & dicary(idic).eventid
'        ElseIf dicary(idic).one_row_find = 3 And dicary(idic).ind = 25 Then
'            inds(25).checked = True
'            dprint 25, 1, "Triggered by f=3 of " & dicary(idic).eventid
'        ElseIf dicary(idic).one_row_find >= 1 And dicary(idic).ind = 24 Then
'            inds(24).checked = True
'            dprint 24, 1, "Triggered by f>=1 of " & dicary(idic).eventid
'        End If
''        If dicary(idic).num_found >= 4 Then
''            inds(26).checked = True
''            dprint 26, 1, "Triggered by f>=4 of " & dicary(idic).eventid
''        ElseIf dicary(idic).num_found = 3 Then
''            inds(25).checked = True
''            dprint 25, 1, "Triggered by f=3 of " & dicary(idic).eventid
''        ElseIf dicary(idic).num_found >= 1 Then
''            inds(24).checked = True
''            dprint 24, 1, "Triggered by f>=1 of " & dicary(idic).eventid
''        End If
'    Next idic
'
''24*33639                             Stage II
''24*33640                             Stage II
''24*33641                             Stage II
''24*33642                             Stage II
''24*33643                             Stage II
''24*33644                             Stage II
''24*33645                             Stage II
''24*33646                             Stage II
''24*25535                             Stage II
''24*25537                             Stage II
''24*25539                             Stage II
''24*25541                             Stage II
''24*25543                             Stage II
''24*25545                             Stage II
'    charting = "Stage II"
'    ibegin = DicIndex("33639", charting)
'    charting = "Stage II"
'    iend = DicIndex("25545", charting)
'    For idic = ibegin To iend
'        If dicary(idic).num_found > 0 Then
'            inds(24).checked = True
'            dprint 24, 1, "Triggered by f>=1 of " & dicary(idic).eventid
'        End If
'    Next idic
'
'    charting = "Stage III"
'    ibegin = DicIndex("33639", charting)
'    charting = "Stage III"
'    iend = DicIndex("25545", charting)
'    For idic = ibegin To iend
'        If dicary(idic).num_found > 0 Then
'            inds(25).checked = True
'            dprint 25, 1, "Triggered by f>=1 of " & dicary(idic).eventid
'        End If
'    Next idic
'
'    charting = "Stage IV"
'    ibegin = DicIndex("33639", charting)
'    charting = "Stage IV"
'    iend = DicIndex("25545", charting)
'    For idic = ibegin To iend
'        If dicary(idic).num_found > 0 Then
'            inds(26).checked = True
'            dprint 26, 1, "Triggered by f>=1 of " & dicary(idic).eventid
'        End If
'    Next idic
'
''25*25572                             severe
''25*25572                             pitting
''25*25572                             weeping
'    For idic2 = 1 To 6
'        count = 0
'        eid = CStr(25570 + idic2 * 2)
'        charting = "moderate"
'        ibegin = DicIndex(eid, charting)
'        charting = "weeping"
'        iend = DicIndex(eid, charting)
'
'        For idic = ibegin To iend
'            If dicary(idic).one_row_find >= 3 And dicary(idic).ind = 26 Then
'                inds(26).checked = True
'                dprint 26, 1, "Triggered by f>=3 of " & str(eid)
'            ElseIf dicary(idic).one_row_find >= 2 And dicary(idic).ind = 25 Then
'                inds(25).checked = True
'                dprint 25, 1, "Triggered by f>=2 of " & str(eid)
'            ElseIf dicary(idic).one_row_find >= 1 And dicary(idic).ind = 24 Then
'                inds(24).checked = True
'                dprint 24, 1, "Triggered by f>=1 of " & str(eid)
'            End If
'        Next idic
'
'    Next idic2
'
''25*33710                             sheath art
''25*33710                             sheath venous
''25*33711                             sheath art
''25*33711                             sheath venous
''25*33712                             sheath art
''25*33712                             sheath venous
''25*33713                             sheath art
''25*33713                             sheath venous
''25*33714                             sheath art
''25*33714                             sheath venous
'
'' 0*33677                             discontinued
'' 0*33678                             discontinued
'' 0*33679                             discontinued
'' 0*33680                             discontinued
'' 0*33681                             discontinued
'
'    charting = "sheath art"
'    ibegin = DicIndex("33710", charting)
'    charting = "sheath venous"
'    iend = DicIndex("33714", charting)
'
'    charting = "discontinued"
'    ibegin2 = DicIndex("33677", charting)
'
'    For idic = ibegin To iend
'        ' operator \ means div
'        If dicary(idic).num_found > 0 And dicary(ibegin2 + (idic - ibegin) \ 2).num_found > 0 Then
'            inds(25).checked = True
'            dprint 25, 1, "Triggered by f>=1 of " & dicary(idic).eventid & " and (33677,discontinued)"
'        End If
'    Next idic
'
''26*27380                             pouch chg complex
''26*27380                             trough chg complex
''26*27380                             suction
''26*27380                             dressing changed
''26*27380                             periwnd sk trtmt
''26*27380                             system maintenan
''26*27380                             other
''24*27380
'    charting = "pouch chg complex"
'    ibegin = DicIndex("27380", charting)
'    charting = "other"
'    iend = DicIndex("27380", charting)
'    count = 0
'    For idic = ibegin To iend
'        If dicary(idic).one_row_find >= 3 And dicary(idic).ind = 26 Then
'            inds(26).checked = True
'            dprint 26, 1, "Triggered by f>=3 of " & dicary(idic).eventid
'        End If
'    Next idic
'
''24*27380
'    charting = ""
'    idic = DicIndex("27380", charting)
'    If dicary(idic).num_found > 0 Then
'        inds(24).checked = True
'        dprint 24, 1, "Triggered by f>=1 of " & dicary(idic).eventid
'    End If
'
''26*27503                             pouch-complex
''26*27503                             trough-complex
''26*27503                             wound vac
''26*27503                             dsg dry
''26*27503                             cream/ointment
''26*27503                             suction
''26*27503                             other
'    charting = "pouch-complex"
'    ibegin = DicIndex("27503", charting)
'    charting = "other"
'    iend = DicIndex("27503", charting)
'    count = 0
'    For idic = ibegin To iend
'        If dicary(idic).one_row_find >= 3 And dicary(idic).ind = 26 Then
'            inds(26).checked = True
'            dprint 26, 1, "Triggered by f>=3 of " & dicary(idic).eventid
'        ElseIf dicary(idic).num_found >= 1 And dicary(idic).ind = 24 Then
'            inds(24).checked = True
'            dprint 24, 1, "Triggered by f>=1 of " & dicary(idic).eventid
'        End If
'    Next idic
'
'    For idic = ibegin To ibegin + 2
'        If dicary(idic).one_row_find >= 1 And dicary(idic).ind = 25 Then
'            inds(25).checked = True
'            dprint 25, 1, "Triggered by f>=1 of complex pouch/trough,woundvac " & dicary(idic).eventid
'        End If
'    Next idic
'
'    For idic = ibegin + 3 To ibegin + 6
'        If dicary(idic).one_row_find >= 2 And dicary(idic).ind = 25 Then
'            inds(25).checked = True
'            dprint 25, 1, "Triggered by f>=2 of dsg dry/cream/suct/other " & dicary(idic).eventid
'        End If
'    Next idic
'
''25*27505                             pouch chg complex
''25*27505                             trough chg complex
''25*27505                             suction
''25*27505                             dressing changed
''25*27505                             periwnd sk trtmt
''25*27505                             system maintenan
''25*27505                             other
'    charting = "pouch chg complex"
'    ibegin = DicIndex("27505", charting)
'    charting = "other"
'    iend = DicIndex("27505", charting)
'
'    For idic = ibegin To ibegin + 1
'        If dicary(idic).one_row_find = 2 And dicary(idic).ind = 25 Then
'            inds(25).checked = True
'            dprint 25, 1, "Triggered by f>=2 of pouch/trough complex " & dicary(idic).eventid
'        End If
'    Next idic
'
'    For idic = ibegin + 2 To iend
'        If dicary(idic).one_row_find = 2 And dicary(idic).ind = 25 Then
'            inds(25).checked = True
'            dprint 25, 1, "Triggered by f>=2 of " & dicary(idic).eventid
'        End If
'    Next idic
'
''26*27440                             pouch-complex
''26*27440                             barrier cream
''26*27440                             dry dsg
''26*27440                             foam
''26*27440                             tube stabilizer
''26*27440                             antifungals
''26*27440                             other
'    charting = "pouch-complex"
'    ibegin = DicIndex("27440", charting)
'    charting = "other"
'    iend = DicIndex("27440", charting)
'    count = 0
'    For idic = ibegin To iend
'        If dicary(idic).one_row_find >= 3 And dicary(idic).ind = 26 Then
'            inds(26).checked = True
'            dprint 26, 1, "Triggered by f>=3 of " & dicary(idic).eventid
'        ElseIf dicary(idic).num_found > 0 Then
'            inds(24).checked = True
'            dprint 24, 1, "Triggered by f>=1 of " & dicary(idic).eventid
'        End If
'    Next idic
'
'    If dicary(ibegin).num_found > 0 Then
'        inds(25).checked = True
'        dprint 25, 1, "Triggered by pouch complex " & dicary(idic).eventid
'    End If
'
'    For idic = ibegin + 1 To iend
'        If dicary(idic).one_row_find >= 2 And dicary(idic).ind = 25 Then
'            inds(25).checked = True
'            dprint 25, 1, "Triggered by f>=2 of " & dicary(idic).eventid
'        End If
'    Next idic
'
''26*27442                             pouch chg-complex
''26*27442                             skin care
''26*27442                             dressing changed
''26*27442                             AgNo3 hypergrnlt
''26*27442                             tube d/c
''26*27442                             other
'    charting = "pouch chg-complex"
'    ibegin = DicIndex("27442", charting)
'    charting = "other"
'    iend = DicIndex("27442", charting)
'    count = 0
'    For idic = ibegin To iend
'        If dicary(idic).one_row_find >= 3 And dicary(idic).ind = 26 Then
'            inds(26).checked = True
'            dprint 26, 1, "Triggered by f>=3 of " & dicary(idic).eventid
'        ElseIf dicary(idic).num_found > 0 Then
'            inds(24).checked = True
'            dprint 24, 1, "Triggered by f>=1 of " & dicary(idic).eventid
'        End If
'    Next idic
'
'    If dicary(ibegin).num_found > 0 Then
'        inds(25).checked = True
'    End If
'
'    For idic = ibegin + 1 To iend
'        If dicary(idic).one_row_find >= 2 And dicary(idic).ind = 25 Then
'            inds(25).checked = True
'            dprint 25, 1, "Triggered by f>=2 of " & dicary(idic).eventid
'        End If
'    Next idic
'
''27501
'    charting = ""
'    idic = DicIndex("27501", charting)
'    If dicary(idic).one_row_find >= 2 And dicary(idic).ind = 25 Then
'        inds(25).checked = True
'        dprint 25, 1, "Triggered by f>=2 of " & dicary(idic).eventid
'    ElseIf dicary(idic).num_found > 0 Then
'        inds(24).checked = True
'        dprint 24, 1, "Triggered by f>=1 of " & dicary(idic).eventid
'    End If
'
''24*25894
''24*27400
''24*27416
''24*27420
''24*27424
''24*27438
''24*27457
''24*27459
''24*27461
''24*27463
''24*27465
''24*27467
'    If Not inds(24).checked Then
'        For i = 1 To 12
'            If countwild(i).count = 1 Then
'                inds(24).checked = True
'                dprint 24, 1, "Triggered by f=1 of " & countwild(i).eventid
'            End If
'        Next i
'    End If
'    If Not inds(25).checked Then
'        For i = 1 To 12
'            If countwild(i).count >= 2 Then
'                inds(25).checked = True
'                dprint 25, 1, "Triggered by f>=2 of " & countwild(i).eventid
'            End If
'        Next i
'    End If
'
'    suppressDbugLog = False
'End Sub


Private Sub Check_19_20()
    On Error GoTo errHandler
    Dim idic As Integer, save_idic As Integer
    Dim total_duration As Integer

    'Add up the total duration for wound care
    'If any one procedure is > 1 hour, generate a procedure record
    For idic = 1 To dicnum
        If (dicary(idic).ind = 19) And (dicary(idic).num_found > 0) Then
            total_duration = total_duration + dicary(idic).duration
            save_idic = idic
            
            If dicary(idic).duration > 60 Then
                num_procs = num_procs + 1
                ReDim Preserve procs(num_procs)
                procs(num_procs).dic_index = idic
                dprint -3, 0, "Procedure #5 (wound care) triggered by duration > 60 min"
            End If
        End If
    Next idic

    If total_duration <= 30 Then
        inds(19).checked = True
        dprint 19, 1, "Triggered by min<=30 of " & dicary(save_idic).eventid
    Else
        inds(20).checked = True
        dprint 20, 1, "Triggered by min>30 of " & dicary(save_idic).eventid
    End If

    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_19_20"
    Resume  'debug
End Sub


'Private Sub Check27()
'    Dim charting As String
'    Dim ibegin, iend, idic, ipt, icog, iresp1, iresp2 As Single
'    suppressDbugLog = True
''27*33939
''27*34889                             regular
''27*34915
''27*34916
''27*34917
''27*34918
''27*34919
''27*34920                             bld transfusion
''27*34920                             PCA
''27*34920                             post op care
''27*34920                             vent booklet
''27*35345
'
'' 0*5929                              unresponsive
'' 0*5929                              respond to pain
'' 0*34906                             pt
'
'    charting = ""
'    ibegin = DicIndex("33939", charting)
'    charting = ""
'    iend = DicIndex("35345", charting)
'
'    charting = "patient"
'    ipt = DicIndex("34906", charting)
'    charting = "unresponsive"
'    iresp1 = DicIndex("5929", charting)
'    charting = "respond to pain"
'    iresp2 = DicIndex("5929", charting)
'    suppressDbugLog = False
'
'    For idic = ibegin To iend
'        If dicary(idic).num_found > 0 Then
'            inds(27).checked = True
'        End If
'    Next idic
'
'    If dicary(ipt).num_found > 0 And _
'       (dicary(iresp1).num_found > 0 Or dicary(iresp2).num_found > 0) Then
'        inds(27).checked = False
'    End If
'
'End Sub
'
'Private Sub Check28()
'    Dim charting As String
'    Dim ibegin, iend, idic, ipt, ifam, icog, iresp1, iresp2, ialt As Single
'    suppressDbugLog = True
'
'' 0*5929                              unresponsive
'' 0*5929                              respond to pain
'' 0*34906                             pt
'' 7*34073                             alt mental stat
'
''28*33279                             yes (matrls givn)
''...
''28*37334                             education
'    charting = "yes"
'    ibegin = DicIndex("33279", charting)
'    charting = "education"
'    iend = DicIndex("37334", charting)
'
'    charting = "patient"
'    ipt = DicIndex("34906", charting)
'    charting = "family"
'    ifam = DicIndex("34906", charting)
'    charting = "cognitive"
'    icog = DicIndex("34908", charting)
'    charting = "unresponsive"
'    iresp1 = DicIndex("5929", charting)
'    charting = "respond to pain"
'    iresp2 = DicIndex("5929", charting)
'    charting = "alt ment"
'    ialt = DicIndex("34073", charting)
'
'    suppressDbugLog = False
'
'    For idic = ibegin To iend
'        If dicary(idic).num_found > 0 Then
'            inds(28).checked = True
'        End If
'    Next idic
'
'    If (dicary(ipt).num_found > 0 And dicary(ifam).num_found = 0) And _
'      (dicary(icog).num_found > 0 Or dicary(iresp1).num_found > 0 Or _
'       dicary(iresp2).num_found > 0 Or dicary(ialt).num_found > 0) Then
'        inds(28).checked = False
'    End If
'
'
'End Sub
'Private Sub Check29()
'    Dim charting As String
'    Dim ibegin As Single
'    suppressDbugLog = True
'
'' 0*9647                              WNL
'
'    If inds(29).checked Then
'        Exit Sub
'    End If
'
'    charting = "WNL except"
'    ibegin = DicIndex("9647", charting)
'
'    If dicary(ibegin).num_found > 0 Then
'        inds(29).checked = True
'    End If
'
'    suppressDbugLog = False
'
'End Sub

Private Sub DeleteOldPersistData()
    Dim perfn As String
    Dim perfile As Integer
    Dim newperfn As String
    Dim newperfile As Integer
    Dim buf As String
    Dim mins As Single
    Dim pdt As String
    Dim acct As String

    
    perfn = App.Path & "\" & PERDATA_FNAME          'Persist.dat
    If g_util.FileExists(perfn) Then
    
    perfile = FreeFile
    Open perfn For Input As #perfile 'this file needs to exist before it can be opened
    
    newperfn = App.Path & "\new" & PERDATA_FNAME
    newperfile = FreeFile
    Open newperfn For Append As #newperfile
    
    If intime = "" Then intime = Format(Now, "yyyymmddhhnn")
    While Not EOF(perfile)
        Line Input #perfile, buf
        pdt = Trim$(Mid$(buf, 1, LEN_DT)) 'this is the persist data line time
        mins = DateDiff("n", DateSerial(Mid$(pdt, 1, 4), Mid$(pdt, 5, 2), Mid$(pdt, 7, 2)) + _
                        TimeSerial(Mid$(pdt, 9, 2), Mid$(pdt, 11, 2), "00"), _
                        DateSerial(Mid$(intime, 1, 4), Mid$(intime, 5, 2), Mid$(intime, 7, 2)) + _
                        TimeSerial(Mid$(intime, 9, 2), Mid$(intime, 11, 2), "00"))
        'compare to intime to determin 24 hour diff
        'if buf's time is 24hrs+ old, then skip it
        'else
        If mins <= 1440 Then
            Print #newperfile, buf
            acct = Trim$(Mid$(buf, LEN_DT + 1, LEN_ACCT_NUM))
            If Not AcctNumInPersistFile(acct) Then
                numacct = numacct + 1
                PublishAcctNum acct
            End If
        End If
    Wend
    Close #perfile
    Close #newperfile
    Kill perfn
    Name newperfn As perfn
    
    Else
        dprint 0, -1, PERDATA_FNAME & " not found."
    End If
    
End Sub
Private Function AcctNumInPersistFile(ByRef a As String) As Boolean
    ' only the acctnum array; file is not necessarily open
    Dim i As Single
    Dim r As String
    
    AcctNumInPersistFile = False
    For i = 1 To numacct
        If a = acctnumdirectory(i).a Then
            AcctNumInPersistFile = True
            Exit For
        End If
    Next i
End Function

Private Function DidProcessPersist(ByRef a As String) As Boolean
    ' only the acctnum array; file is not necessarily open
    Dim i As Single
    Dim r As String
    
    DidProcessPersist = False
    For i = 1 To numacct
        If a = acctnumdirectory(i).a Then
            DidProcessPersist = acctnumdirectory(i).already_did_persist
            Exit For
        End If
    Next i
End Function

Private Sub SetAlreadyDidPersist(ByRef a As String)
    ' only the acctnum array; file is not necessarily open
    Dim i As Single
    Dim r As String
    
    For i = 1 To numacct
        If a = acctnumdirectory(i).a Then
            acctnumdirectory(i).already_did_persist = True
            Exit For
        End If
    Next i
End Sub



Private Sub PublishAcctNum(ByRef a As String)
    ReDim Preserve acctnumdirectory(0 To numacct)
    acctnumdirectory(numacct).a = a
End Sub

Private Sub AddPersistLine(ByRef b As String)
    Dim pdt As String
    Dim perfn As String
    Dim perfile As Integer
    
    'parse out event time
    pdt = Trim$(Mid$(b, START_EVENT_DT, LEN_DT))
    pdt = pdt & Space(LEN_DT - Len(pdt)) & acctnum & Space(LEN_ACCT_NUM - Len(acctnum)) & b
   
    perfn = App.Path & "\" & PERDATA_FNAME          'Persist.dat
    perfile = FreeFile
    Open perfn For Append As #perfile
    
    Print #perfile, pdt
    
    Close #perfile
    
End Sub

Private Sub GetNextPersistItemForThisAcctNum(ByRef pidx As Single, ByRef b As String) 'return the event string b
    Dim perfn As String
    Dim perfile As Integer
    Dim ppos As Single
    Dim a As String
    
    perfn = App.Path & "\" & PERDATA_FNAME          'Persist.dat
    perfile = FreeFile
    Open perfn For Input As #perfile
    
    ppos = 0
    While Not EOF(perfile) And ppos < pidx
        Line Input #perfile, b
        ' parse off the acct num from b and assign a to it.
        a = Trim$(Mid$(b, LEN_DT + 1, LEN_ACCT_NUM))
        If a = acctnum Then
            ppos = ppos + 1
            If ppos = pidx Then
                b = Mid$(b, LEN_DT + LEN_ACCT_NUM + 1, START_RESULT + LEN_RESULT) 'start from after acctnum for entire length of event line
            End If
        End If
        If EOF(perfile) Then
            b = ""
            SetAlreadyDidPersist (acctnum)
        End If
    Wend
    
    Close #perfile

End Sub
