VERSION 5.00
Begin VB.Form frmAutoChartImport 
   ClientHeight    =   1440
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6705
   Icon            =   "AutoChartImport.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   1440
   ScaleWidth      =   6705
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer Timer1 
      Interval        =   300
      Left            =   6240
      Top             =   120
   End
   Begin VB.Label lblStatus 
      Caption         =   "Status"
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   1080
      Width           =   6495
   End
   Begin VB.Label lblInformation 
      Caption         =   "Information"
      Height          =   255
      Left            =   120
      TabIndex        =   1
      Top             =   720
      Width           =   6495
   End
   Begin VB.Label lblLastTime 
      Caption         =   "Time of last calculation or reason calculation failed"
      Height          =   555
      Left            =   120
      TabIndex        =   0
      Top             =   60
      Width           =   6495
   End
End
Attribute VB_Name = "frmAutoChartImport"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'
' Run the report calculations -- display progress.
'
Private TimeToRun               As Date
Private doing_import            As Boolean
Private cancel_calculations     As Boolean
Private save_width              As Long
Private save_height             As Long


Private Sub Form_Load()
    save_width = Me.Width
    save_height = Me.Height
    
    g_display.SetFontInAllControls Me, g_display.MessageFont
    Me.Caption = g_util.AppCaption()
    
    lblLastTime.Caption = ""
    lblInformation.Caption = ""
    
    LogEvent "Waiting for files in " & import_path
    
    'Wait a second to get on the screen then start first poll
    Timer1.Interval = 1000
    Timer1.Enabled = True
End Sub

Private Sub Form_Resize()
    'This form should have a fixed size, but we want to allow minimize
    Select Case Me.WindowState
    Case vbNormal
        Me.Width = save_width               'lock the size
        Me.Height = save_height
    Case vbMaximized
        Me.WindowState = vbNormal           'don't allow maximize
    Case vbMinimized
        'OK
    End Select
End Sub

Private Sub Form_Unload(Cancel As Integer)
    'breakpoint here for unexpected error
    LogInfo "Shutting down"
End Sub


Private Sub Timer1_Timer()
    Timer1.Enabled = False
    PollForImportFile
End Sub


Private Sub LaunchImport()
    On Error GoTo errHandler
    Dim cmd As String
    Dim exit_code As Long
    
    'prevent recursive calls
    If (doing_import) Then Exit Sub
    
    doing_import = True
    lblStatus.Caption = "Running import..."
    LogEvent "Staring import..."

    'Wait until this import is done so we don't keep trying to spawn new imports
    'on the same file.
    'Be sure to quote the .bat path in case it has blanks
    cmd = """" & App.Path & "\Run_TC_Import.bat"" > ..\log\Run_TC_Import.log 2> ..\log\Run_TC_Import.err"
    g_util.ExecuteAndWaitWithEvents cmd, exit_code
    
    doing_import = False
    lblLastTime.Caption = "Import finished " & Now
    LogEvent "... Import finished"
    Exit Sub
    
errHandler:
    'The batch script may be missing
    LogError Err.Description
    Unload Me
End Sub

Private Function PollForImportFile() As Boolean
    On Error GoTo errHandler
    
    Dim n As Integer
    Dim fnames() As String

    
    n = g_tcutil.GetAllImportFilenames(import_path, fnames())

    If n > 0 Then
        LaunchImport
    End If
    
    TimeToRun = DateAdd("s", gPollEvery, Now())
    lblInformation.Caption = "The next poll is scheduled for " & TimeToRun
    lblStatus.Caption = "Waiting for files in " & import_path

    FlushLogFile

    Timer1.Interval = gPollEvery * 1000
    Timer1.Enabled = True
    Exit Function
    
errHandler:
    'The folder name to poll may be in error
    LogError Err.Description
    Unload Me
End Function

