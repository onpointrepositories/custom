﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;                     // for LINQ; add a reference
using System.Data.SqlClient;                // for SqlConnection
using System.IO;                            // for Directo24ry, Path, Stream
using PfsShared;                            // add a reference to Shared2 project

// ============================================================================
// Inpatient 2.0 transparent mapping -- GOES HERE -- University of Wisconsin
// (The code below is a sample from Sharp)
// ============================================================================
// This processes one patient.  Only one database query is used.  :)
//
// All search functions use exact match for category, description and field name.
// Codes and results can be a single word or a list of words.
// Codes default to exact match; prefix with CODE_CODE_LIKE_PREFIX if desired (single word only)
// Results default to like matching; prefix with RESULT_EXACT_MATCH_PREFIX if desired (single word only)
//
// All searches are case insensitive.
//
//
namespace TransparentMapping
{
    class Inpatient2
    {
        private const int MAX_INDS = 150;
        private const int MAX_PROCS = 20;

        private const string CODE_LIKE_PREFIX = "%!";       // use to make codes %like%
        private const string EXACT_MATCH_PREFIX = "&!";     // use to make desc/result exact match
        private const string CHAR_COMMA = "||";             // use to insert literal comma in word list
        private const string ALL_ITEMS_PREFIX = "&@";       // use to indicate all items should exist in description

        private struct indicator_data {
            public bool     is_checked;     
            public int      radio_group;       
        }

        public struct proc_data {                      
            public int      procedure_number;
            public DateTime start;   
            public DateTime finish;
        }


        // These are database CHART_ITEMs for this patient
        private CHART_ITEM[] _chart_items_since_admission;
        private CHART_ITEM[] _chart_items_since_unit_arrival;
        private CHART_ITEM[] _chart_items_past_24Hrs;
        private CHART_ITEM[] _chart_items_during_pull_period;
        private CHART_ITEM[] _chart_items_past_12Hrs; 
        // These are database PROCEDURE_EVENTs for this patient
        private PROCEDURE_EVENT[] _procedure_events;

        private indicator_data[] _inds;
        public List<proc_data> _procs;

        private const int DEFAULT_BUCKET_SIZE = 20;       //min

        private PatientInfo _pat;
        private int  _bucket_size = DEFAULT_BUCKET_SIZE;
        //private bool is_default;
        private string _txarea;
        private bool ind24_via_vs;
        private bool ind24_via_rr;

        private enum SearchDepth {
            SearchDefault,
            SearchPullRange,            //search within the current pull     -- default
            Search24Hrs,
            Search12Hrs,
            SearchSinceArrival,         //search since arrival to the unit
            SearchSinceAdmission        //search everything since admission to the hospital
        }

        private enum CountMode {
            CountAll,
            CountFirst                  //stop after one is found
        }

        private enum GetValueMode {
            GetTotal,
            GetMax,
            GetLast
        }

        enum Frequencies {
            QNONE,
            Q4H,
            Q2H,
            Q1H,
            Q30M
        }

        private struct fmapRow {
            public double   los_high;                       //the LOS being testing
            public int[]    freq;                           //the count required for each Q value
        }
        private List<fmapRow> _freq_map;                    //1,2,4,8,12,24 hours

        private struct txRecord
        {
            public string txcode;
            public string txname;
        }
        private struct txareaRow
        {
            public string uname;
            public txRecord[] txRec;
        }
        private List<txareaRow> _txarr;
        private List<PatientLocation> procloclist;
     
        //
        // This is the main entry point
        //
        public void ProcessPatient(PatientInfo pat)
        {
            bool elig_for_default = false;
            bool no7am_exists = false;
            _pat = pat;

            InitIndicators(); // sets is_default
            InitProcs();
            ResolveProcLocations();
            LoadTxAreaTable();
            //if (!is_default)
            //    {
                LoadFreqTable();
                LoadPatientChart();
                SetLOSFromLocs();
                Check_1_2_3_4();
                Check_5();
                Check_6_7();
                Check_8();
                Check_9();
                Check_10_11();
                Check_12_13();
                Check_14();
                Check_15_16_17_18();
                Check_19();
                Check_20();
                Check_21_22();
                Check_23();
                Check_24();
            //}
            AtLeastOneADL();
            HighestIndicatorInEachGroupWins();

            //if (!is_defaul)
            //{
                CheckProcs();
            //}

            if (Program.g_no_output) return;
            if (_pat.default_ptype > DeterminePtypeOfIndicators())
            { // if the default pt type is higher than the pt type of this class
                // then if there is a default classification in the past 16 hrs
                // then make these indicators the default indicators.
                elig_for_default = true;
                Program.VerboseAudit("Eligible for default indicators");
            }

            //foreach (var ploc in Program.patloclist)
            //{
            //    Audit("unitid=" + ploc.unit_id + " start= " + ploc.in_time + " end=" + ploc.out_time);
            //}
            const string DATE_FORMAT = "yyyyMMdd";
            const string TIME_FORMAT = "HHmm";
            string str_date =Program.g_pull_finish.ToString(DATE_FORMAT); //tod7am or tod7pm
            string str_time =Program.g_pull_finish.ToString(TIME_FORMAT);
            string str_yest_date = Program.g_pull_finish.AddDays(-1).ToString(DATE_FORMAT);
            DateTime start_dt;
            int save_unit_id = -1;
            int save_methid = -1;
            bool is_7am = false;
            start_dt = PFSUtility.ISOToDateTime(str_yest_date + "1715");
            if (str_time == "0515")
            {
                //only output locations > yesterday 1700pm   and make a 5am today.
                is_7am = true;
                start_dt = PFSUtility.ISOToDateTime(str_yest_date + "1715");
            }
            else if (str_time == "0700")
            {
                //only output locations > today 5am   and make a 7am today.
                is_7am = true;
                start_dt = PFSUtility.ISOToDateTime(str_date + "0515");
            }
            else if (str_time == "1715")
            {
                //only output locations > today 7am   and make a 5pm today.
                start_dt = PFSUtility.ISOToDateTime(str_date + "0700");
                no7am_exists = No7amClassExists(start_dt);
            }


            foreach (var ploc in Program.patloclist)
            {
                if ((ploc.unit_id != -1) && (ploc.methid == 18))
                {
                    Check_TxArea(ploc.unit_id,ploc.service,ploc.room);
                    // for 7am: only output class for locs after yest 7pm
                    // for 7pm: only output class for locs after today 7am
                    if (ploc.in_time > start_dt)
                    {
                        Program.VerboseAudit("Aploc.in_time=" + ploc.in_time.ToString());
                        Program.VerboseAudit("Aploc.in_time=" + ploc.out_time.ToString());
                        OutputClass(Convert.ToInt32(elig_for_default), ploc.unit_id, ploc.in_time, ploc.out_time);
                    }
                    else
                        if (!is_7am && ploc.in_time == start_dt && no7am_exists)
                        {
                            Program.VerboseAudit("Bploc.in_time=" + ploc.in_time.ToString());
                            Program.VerboseAudit("Bploc.in_time=" + ploc.out_time.ToString());
                            OutputClass(2, ploc.unit_id, ploc.in_time, ploc.out_time);
                        }
                }
                if (ploc.out_time == Program.g_pull_finish)
                {
                    save_unit_id = ploc.unit_id;
                    save_methid = ploc.methid;
                }

            }
            //now create the 7am or 7pm   change: now create the 515, 700 or 1715
            if ((save_unit_id != -1) && (save_methid == 18))
            {
                Program.VerboseAudit("Cg_pull_finish=" + Program.g_pull_finish.ToString());
                OutputClass(Convert.ToInt32(elig_for_default), save_unit_id, Program.g_pull_finish, DateTime.MinValue);
                //if (is_7am)
                //    OutputClass(Convert.ToInt32(elig_for_default), save_unit_id, Program.g_pull_finish, Program.g_pull_finish.AddDays(1));
                //else
                //    OutputClass(Convert.ToInt32(elig_for_default), save_unit_id, Program.g_pull_finish, Program.g_pull_finish.AddHours(12));
            }

            OutputProcs();
        }

        private string GetUnitName(int unitid)
        {
            string result="";
            //Program.VerboseAudit("GetUnitName");
            var db = PFSUtility.NewPfsDataContext();
            var query = from u in db.UNITs
                        where (u.UNIT_ID == unitid)
                        select u.NAME;
            foreach (var urec in query)
            {
                result = urec.ToString();
            }
            Program.VerboseAudit("Unit=" + result);
            return result;
        }

       
        private void InitIndicators()
        {
            // Make a new (empty) indicator array
            _inds = new indicator_data[MAX_INDS + 1];           // This 1 based so add one
            //is_default = false;
            //if (_pat.los_hours <= 4.0) {
            //    is_default = true;
            //    Program.VerboseAudit("Patient was here 4 hrs or less. Will receive default indicators " + _pat.default_inds_str);
            //    foreach (var ind in _pat.default_inds) {
            //        if (ind <= _inds.GetUpperBound(0)) {
            //            _inds[ind].is_checked = true;
            //        }
            //    }
            //}

            // get indicator radio groups from the database
            // ** (This database access can be replaced once we have a C# methodolgy cache)
            ind24_via_vs = false;
            ind24_via_rr = false;
            var db = PFSUtility.NewPfsDataContext();
            var query = from idef in db.INDICATOR_DEFINITIONs
                        where (idef.METHODOLOGY_ID == _pat.meth_id)
                        select idef;
            foreach (var idef in query) {
                if (idef.INDICATOR_NUMBER <= _inds.GetUpperBound(0)) {
                    // (convert nulls to zero)
                    _inds[idef.INDICATOR_NUMBER].radio_group = PFSUtility.DBToInt(idef.RADIO_GROUP);
                }
            }
        }

        private void InitProcs()
        {
            _procs = new List<proc_data>();
        }

        private fmapRow LoadFreqTableRow(double los_high, string values)
        {
            fmapRow fmrow;
            
            fmrow.los_high = los_high;
            fmrow.freq = new int[5];

            var arr = values.Split(',');
            for (int i = 0; i <= fmrow.freq.GetUpperBound(0) ; i++ )
            {
                fmrow.freq[i] = arr[i].ToInteger();
            }
            return fmrow;
        }

        // These are the number of times you need to do something during a certain time period (LOS)
        // in order for it to be considered one of the frequencies listed.
        private void LoadFreqTable()
        {
            _freq_map = new List<fmapRow>();
            //                              LOS,  None Q4h Q2h Q1h Q30m
            _freq_map.Add(LoadFreqTableRow(1, "    0,  0,  0,  1,  2"));
            _freq_map.Add(LoadFreqTableRow(2, "    0,  0,  1,  2,  3"));
            _freq_map.Add(LoadFreqTableRow(4, "    0,  1,  2,  3,  5"));
            _freq_map.Add(LoadFreqTableRow(6, "    0,  1,  3,  4,  8"));
            _freq_map.Add(LoadFreqTableRow(8, "  0,  3,  5,  9, 12"));
            _freq_map.Add(LoadFreqTableRow(12, " 0,  3,  6,  12, 16"));
            _freq_map.Add(LoadFreqTableRow(16, "   0,  5,  9,  18, 24"));
            _freq_map.Add(LoadFreqTableRow(24, "   0,  6,  12, 24, 32"));
            _freq_map.Add(LoadFreqTableRow(9999, " 0,  4,  8, 15, 29"));
//New freq table 2/5/14
//q4	q2	q1	q30     q30
//            Non-ICU	ICU & SD
// 4	8	15	29	    36
// 3	5	9	17	    24
// 2	4	7	13	    19
// 2	3	5	10	    13

            //to read this table: if you have at least 6 asessmenets in a 12-hour period, you get Q1h
            //(LOS=12, column Q1h has a count of 6)
        }

        private Frequencies FreqForCount(double los_hours, int count) 
        {
            foreach(var fmrow in _freq_map) {
                if (los_hours <= fmrow.los_high) {
                    // Option: pro-rate the count upward if the LOS is less than the row we are interested in.
                    //         This will bump the count to what it might have been at the full LOS.
                    // Note: truncate the result; rounding inflates the value too much.
                    int prorated_count = (int)((fmrow.los_high / los_hours) * count);

                    // foreach goes low to high; go from high to low instead
                    for (int j = (int)Frequencies.Q30M; (j >= (int)Frequencies.QNONE); j--) { //search right to left
                        if (prorated_count >= fmrow.freq[j]) {
                            return (Frequencies)j;
                        }
                    } // next j
                }
            }
            
            return Frequencies.QNONE;
        }

        private txareaRow LoadTxAreaRow(string uname, string txcodes, string txnames)
        {
            txareaRow txrow;

            var arr1 = txcodes.Split(',');
            var arr2 = txnames.Split(',');

            txrow.uname = uname;
            Program.VerboseAudit(uname + " / " + txcodes + " / " + txnames + ": " + arr1.GetUpperBound(0).ToString());
            txrow.txRec = new txRecord[arr1.GetUpperBound(0)+1];

            for (int i = 0; i <= arr1.GetUpperBound(0); i++) {
                arr1[i] = arr1[i].Trim();                         // get rid of leading blanks
                arr1[i] = arr1[i].ToUpper();                      // convert for case-insensitive comparisons
                arr2[i] = arr2[i].Trim();                         // get rid of leading blanks
                //arr2[i] = arr2[i].ToUpper();                      // convert for case-insensitive comparisons
                txrow.txRec[i].txcode = arr1[i];
                txrow.txRec[i].txname = arr2[i];
            }

            return txrow;
        }

//B4/5 	S	General Care
//    I	IMC
//    T	ICU 
//    H	ICU 
//    R	General Care
//    V	General Care
//    J	General Care
//    W	General Care
//B4/6	S	General Care
//    I	IMC
//    R	General Care
//    V	General Care
//    W	General Care
//D4/5	S	General Care
//    I	IMC
//    R	General Care
//    V	General Care
//    J	General Care
//    W	General Care
//D6/5	S	General Care
//    I	IMC
//    R	General Care
//    V	General Care
//    W	General Care
//F4/4	S	General Care
//    I	IMC
//    R	General Care
//    V	General Care
//    J	General Care
//    W	General Care
//F4/5 	S	General Care
//    I	IMC
//    R	General Care
//    V	General Care
//    J	General Care
//    W	General Care
//F4M5	S	Ask NM - map to IMC or map to "none" QMed Treatment Area
//    I	IMC
//    T	ICU 
//    H	ICU 
//    R	Ask NM - map to IMC or map to "none" QMed Treatment Area
//    V	Ask NM - map to IMC or map to "none" QMed Treatment Area
//    J	Ask NM - map to IMC or map to "none" QMed Treatment Area
//    W	Ask NM - map to IMC or map to "none" QMed Treatment Area
//F6/4	S	CIU Patient
//    R	OSS/OBS Patient
//    V	CIU Patient
//    W	OSS/OBS Patient
//PICU	S	General Care
//    I	IMC (need to add IMC as a Treatment Area in QuadraMed)
//    T	ICU 
//    H	ICU 
//    R	General Care
//    V	General Care
//    J	General Care
//    W	General Care

        private void LoadTxAreaTable()
        {
            _txarr = new List<txareaRow>();
            _txarr.Add(LoadTxAreaRow("B4/5", "S,D,I,T,H,R,V,J,W", "General Care,General Care,IMC,ICU,ICU,General Care,General Care,General Care,General Care"));
            _txarr.Add(LoadTxAreaRow("B4/6", "S,D,I,R,V,J,W", "General Care,General Care,IMC,General Care,General Care,General Care,General Care"));
            _txarr.Add(LoadTxAreaRow("D4/5", "S,D,I,R,V,J,W", "General Care,General Care,IMC,General Care,General Care,General Care,General Care"));
            _txarr.Add(LoadTxAreaRow("D6/5", "S,D,I,R,V,J,W", "General Care,General Care,IMC,General Care,General Care,General Care,General Care"));
            _txarr.Add(LoadTxAreaRow("F4/4", "S,D,I,R,V,J,W", "General Care,General Care,IMC,General Care,General Care,General Care,General Care"));
            _txarr.Add(LoadTxAreaRow("F4/5", "S,D,I,R,V,J,W", "General Care,General Care,IMC,General Care,General Care,General Care,General Care"));
            _txarr.Add(LoadTxAreaRow("F4M5", "S,D,I,T,H,R,V,J,W", "General Care,General Care,IMC,ICU,ICU,General Care,General Care,General Care,General Care"));
            _txarr.Add(LoadTxAreaRow("F6/4", "S,D,R,V,J,W", "Medical/Surgical,Medical/Surgical,OSS/OBS,Medical/Surgical,Medical/Surgical,OSS/OBS"));
            _txarr.Add(LoadTxAreaRow("F6/4 CIU", "S,D,R,V,J,W", "Medical/Surgical,Medical/Surgical,OSS/OBS,Medical/Surgical,Medical/Surgical,OSS/OBS"));
            _txarr.Add(LoadTxAreaRow("PICU", "S,D,I,T,H,R,V,J,W", "General Care,General Care,IMC,ICU,ICU,General Care,General Care,General Care,General Care"));
            //_txarr.Add(LoadTxAreaRow("TLC", "S,I,T,H,R,V,J,W", "Keith,Keith,Keith,Keith,Keith,Keith,Keith,Keith"));
        }


        //
        // Load patient chart from the database (one DB access)
        // Load everything since admission
        //
        private void LoadPatientChart()
        {
            // Get the entire patient chart (all units and dates for this patient)
            var db = PFSUtility.NewPfsDataContext();
            var query = from item in db.CHART_ITEMs
                        where (item.ENCOUNTER_ID == _pat.encounter_id)
                        select item;
            // Save the result
            _chart_items_since_admission = query.ToArray();

            // Convert all results to lower case for case insensitive comparisons
            foreach (var item in _chart_items_since_admission) {
                if (item.CATEGORY != null) item.CATEGORY = item.CATEGORY.ToLower();
                if (item.CODE != null) item.CODE = item.CODE.ToLower();
                if (item.DESCRIPTION != null) item.DESCRIPTION = item.DESCRIPTION.ToLower();
                if (item.FIELD_NAME != null) item.FIELD_NAME = item.FIELD_NAME.ToLower();
                if (item.RESULT != null) item.RESULT = item.RESULT.ToLower();
            }

            // Prepare more versions of the chart
            var query2 = from item in _chart_items_since_admission
                    where(item.EVENT_DATETIME >= _pat.unit_arrival) && (item.EVENT_DATETIME <= _pat.pull_finish)
                    select item;
            _chart_items_since_unit_arrival = query2.ToArray();
            query2 = from item in _chart_items_since_admission
                     where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-24)) && (item.EVENT_DATETIME <= _pat.pull_finish)
                     select item;
            _chart_items_past_24Hrs = query2.ToArray();

            query2 = from item in _chart_items_past_24Hrs
                     where (item.EVENT_DATETIME >= _pat.pull_start) && (item.EVENT_DATETIME <= _pat.pull_finish)
                     select item;
            _chart_items_during_pull_period = query2.ToArray();

            query2 = from item in _chart_items_past_24Hrs
                     where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-12)) && (item.EVENT_DATETIME <= _pat.pull_finish)
                     select item;
            _chart_items_past_12Hrs = query2.ToArray();
        }

        // Delay looking for patient procedures until they are wanted
        private void LoadPatientProceduresIfNeeded()
        {
            if (_procedure_events != null) return;

            var db = PFSUtility.NewPfsDataContext();
            var query = from proc in db.PROCEDURE_EVENTs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                        select proc;
            _procedure_events = query.ToArray();
        }


        private void SetLOSFromLocs()
        {
            int los_mins = 0;

            foreach (var ploc in Program.patloclist)
            {
                Program.VerboseAudit("los_mins=" + ploc.los_mins + "  unit_id=" + ploc.unit_id);
                if ((ploc.unit_id != -1) && (!ploc.remove))
                    los_mins += ploc.los_mins;
            }
            _pat.los_hours = los_mins / 60.0;
        }

        // Started a new chart item query (of a certain depth)
        // Default is the chart during the pull range.  Options for unit arrival and admission to hospital.
        private IEnumerable<CHART_ITEM> StartNewQuery()
        {
            return StartNewQuery(SearchDepth.SearchDefault);
        }
        private IEnumerable<CHART_ITEM> StartNewQuery(SearchDepth search_depth)
        {
            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    return (from item in _chart_items_during_pull_period select item);
                case SearchDepth.Search24Hrs:
                    return (from item in _chart_items_past_24Hrs select item);
                case SearchDepth.Search12Hrs:
                    return (from item in _chart_items_past_12Hrs select item);
                case SearchDepth.SearchSinceArrival:
                    return (from item in _chart_items_since_unit_arrival select item);
                case SearchDepth.SearchSinceAdmission:
                    return (from item in _chart_items_since_admission select item);
            }
            return null;
        }

        // Is this search term a word or a list of words?
        private bool ValueIsAList(string s) 
        {
            return (s != null) && s.Contains(',');                //is this a comma-separated list?
        }

        // Break apart a wordlist and prepare its members
        private string[] SplitOnCommaAndPrepareElements(string s)
        {
            // If a word list has a space after each comma, we will get leading blanks in each term
            var arr = s.Split(',');
            for (int i = 0; i <= arr.GetUpperBound(0); i++) {
                arr[i] = arr[i].Trim();                         // get rid of leading blanks
                arr[i] = arr[i].Replace(CHAR_COMMA, ",");       // insert a literal comma into search word
                arr[i] = arr[i].ToLower();                      // convert for case-insensitive comparisons
            }
            return arr;
        }

        // Add a basic chart item filter to a LINQ query
        //
        // cat = exact match
        // code_list = exact match; optional "like" match
        // desc_list = "like" match; optional exact match
        // field = exact match
        // result_list = "like" match; optional exact match
        //
        // These are comma-separated lists.  
        // Search for a literal comma by using CHAR_COMMA in the string.
        private IEnumerable<CHART_ITEM> AndItemFilter(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list) 
        {
            // Converted all values to lower case for case-insensitive comparisons.
            if (!String.IsNullOrEmpty(cat)) {
                //cat = cat.ToLower();
                query = query.Where(e => e.CATEGORY == cat.ToLower());
            }

            if (!String.IsNullOrEmpty(code_list)) {
                code_list = code_list.ToLower();
            }
            if (ValueIsAList(code_list)) {
                if (code_list.Left(2) == EXACT_MATCH_PREFIX)
                {
                    query = AndCodeInListExact(query, code_list.Substring(2));            // find one of the words
                }
                else // use Like by default
                {
                    query = AndCodeInList(query, code_list);            // find one of the words
                }
            } else if (code_list.Left(2) == EXACT_MATCH_PREFIX) { // override for "like" match?
                query = query.Where(e => e.CODE == code_list.Substring(2));      // find this word
            }
            else if (code_list.Left(2) == CODE_LIKE_PREFIX)
            {
                query = query.Where(e => e.CODE.Contains(code_list.Substring(2)));
            }
            else   
            {
                query = query.Where(e => e.CODE.Contains(code_list));
            }
            
            
            if (!String.IsNullOrEmpty(desc_list)) {
                desc_list = desc_list.ToLower();
                if (desc_list.Left(2) == EXACT_MATCH_PREFIX) {          // exact match override?
                    query = query.Where(e => e.DESCRIPTION == desc_list.Substring(2));
                } else if (ValueIsAList(desc_list)) {
                    if (desc_list.Left(2) == ALL_ITEMS_PREFIX)
                    {          // for list of items all of which must exist
                        query = AndDescriptionALLInList(query, desc_list.Substring(2));
                    }
                    else
                    {
                        query = AndDescriptionInList(query, desc_list);
                    }
                } else {
                    desc_list = desc_list.Replace(CHAR_COMMA, ",");     // look for literal comma requests
                    query = query.Where(e => e.DESCRIPTION.Contains(desc_list));
                }
            }
            
            if (!String.IsNullOrEmpty(field)) {
                field = field.ToLower();
                query = query.Where(e => e.FIELD_NAME == field);
            }
            
            if (!String.IsNullOrEmpty(result_list)) {
                result_list = result_list.ToLower();                        // make all results case insensitve
                if (result_list.Left(2) == EXACT_MATCH_PREFIX) {     // exact match override?
                    query = query.Where(e => e.RESULT == result_list.Substring(2));
                } else if (ValueIsAList(result_list)) {
                    query = AndResultInList(query, result_list);
                } else {
                    result_list = result_list.Replace(CHAR_COMMA, ",");     // look for literal comma requests
                    query = query.Where(e => e.RESULT.Contains(result_list));
                }
            }
            return query;
        }

        // Look for a code that is one of the words in the list.
        private IEnumerable<CHART_ITEM> AndCodeInListExact(IEnumerable<CHART_ITEM> query, string code_list) 
        {
            if (String.IsNullOrEmpty(code_list)) return query;
            
            var arr = SplitOnCommaAndPrepareElements(code_list);
            return query.Where(e => arr.Contains(e.CODE));              // exact match
        }
        private IEnumerable<CHART_ITEM> AndCodeInList(IEnumerable<CHART_ITEM> query, string code_list)
        {
            if (String.IsNullOrEmpty(code_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(code_list);
            return query.Where(e => e.CODE.ContainsAny(arr)); // like match
        }

        // Look for a description that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndDescriptionInList(IEnumerable<CHART_ITEM> query, string desc_list) 
        {
            if (String.IsNullOrEmpty(desc_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(desc_list);
            return query.Where(e => e.DESCRIPTION.ContainsAny(arr));    // "like" match
        }

        private IEnumerable<CHART_ITEM> AndDescriptionALLInList(IEnumerable<CHART_ITEM> query, string desc_list)
        {
            if (String.IsNullOrEmpty(desc_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(desc_list);
            foreach (string s in arr)
            {
                query = query.Where(e => e.DESCRIPTION.Contains(s));
            }
            return query;
        }

        // Look for a result that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultInList(IEnumerable<CHART_ITEM> query, string result_list) 
        {
            if (String.IsNullOrEmpty(result_list)) return query;
            
            var arr = SplitOnCommaAndPrepareElements(result_list);
            return query.Where(e => e.RESULT.ContainsAny(arr));         // "like" match
        }

        // Look for a result that does not contain one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultNotInList(IEnumerable<CHART_ITEM> query, string result_list)
        {
            if (String.IsNullOrEmpty(result_list)) return query;
            
            var arr = SplitOnCommaAndPrepareElements(result_list);
            return query.Where(e => !e.RESULT.ContainsAny(arr));        // "like" match
        }

        private string DescribeSearchDepth(SearchDepth search_depth) 
        {
            string result = "";
            
            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    //result = "in pull range"
                    result = "";                         //be quiet since this is default
                    break;
                case SearchDepth.Search24Hrs:
                    result = "in the past 24 hours";
                    break;
                case SearchDepth.Search12Hrs:
                    result = "in the past 12 hours";
                    break;
                case SearchDepth.SearchSinceArrival:
                    result = "since arrival to unit";
                    break;
                case SearchDepth.SearchSinceAdmission:
                    result = "since admission";
                    break;
            }
            
            return result;
        }

        // Describe a query that has already been run
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Make a simple query and describe the results
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            var arr = query.ToArray();
            return Describe(arr, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private string LookingFor(string so_far, string what, string op, string value)
        {
            string result;
            if (String.IsNullOrEmpty(value)) return so_far;

            if (so_far == "")
                result = "looking for " + what;
            else
                result = so_far + " and " + what;

            if (value.Left(2) == EXACT_MATCH_PREFIX)
                result += "='" + value.Substring(2) + "'";
            else if (value.Left(2) == CODE_LIKE_PREFIX)
                result += " contains " + "'" + value.Substring(2) + "'";
            else if (ValueIsAList(value))
                result += " in '" + value + "'";
            else
                result += op + "'" + value + "'";
            
            return result;
        }
        private string Describe(CHART_ITEM[] arr, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string result = "";
            result = LookingFor(result, "cat", "=", cat);
            result = LookingFor(result, "code", "=", code_list);
            result = LookingFor(result, "desc", " contains ", desc_list);
            result = LookingFor(result, "field", "=", field);
            result = LookingFor(result, "result", " contains ", result_list);

            if (arr.Count() == 0) {
                result += "; not found " + DescribeSearchDepth(search_depth);
            } else {
                var e = arr[0];
                result = "FOUND: " + result + "; found";
                // We might have searched for a pattern or word list in several fields - show what was found
                if (e.CATEGORY != null)     result += " cat='" + e.CATEGORY + "'";
                if (e.CODE != null)         result += " code='" + e.CODE + "'";
                if (e.DESCRIPTION != null)  result += " desc='" + e.DESCRIPTION + "'";
                if (e.FIELD_NAME != null)   result += " field='" + e.FIELD_NAME + "'";
                if (e.RESULT != null)       result += " result='" + e.RESULT + "'";
                // Are there more results?  Just say how many; we aren't going to list them.
                if (arr.Count() == 2) {
                    result += " (1 more result)";
                } else if (arr.Count() > 2) {
                    result += " (" + (arr.Count() - 1) + " more results)";
                }
            }
            
            return result;
        }


        //Set an indicator for this reason (low level)
        private void SetInd(int inum, string reason)
        {
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Set Ind #" + inum + ": " + reason);          //already set - repeat for verbose only
            } else {
                _inds[inum].is_checked = true;
                Program.Audit("Set Ind #" + inum + ": " + reason);
            }
        }

        //Clear an indicator for this reason (low level)
        private void ClrInd(int inum, string reason)
        {
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Clr Ind #" + inum + ": " + reason);          //already clear - repeat verbose only
            } else {
                _inds[inum].is_checked = false;
                Program.Audit("Clr Ind #" + inum + ": " + reason);
            }
        }

        // Count how many items meet the conditions
        // All items are listed
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountItems(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            int count = query.Count();

            // always return what was found
            found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
            // echo the result?
            if (trace) Program.VerboseAudit(found_what);
            
            return count;
        }

        // Count how many items meet the conditions
        // This has the option to list every entry or give a total
        private int CountResultInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what="";
 
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);

            var arr = SplitOnCommaAndPrepareElements(result_list);

            foreach (var item in query) {
                // Figure out which of the search words were found
                var s = item.RESULT.ContainsWhich(arr);
                found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' code='" + item.CODE + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "'";
                if (trace) Program.VerboseAudit(found_what);
                count++;
                if (count_mode != CountMode.CountAll) break;        //stop counting
            }
            
            if (count > 0) {
                //We already printed what was found; maybe add how many?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }
            
            return count;
        }

        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (ValueIsAList(result_list)) {
                return CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what);
            } else {
                return CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what);
            }
        }

        // Is there an item with this result?
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list) 
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth) 
        {
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContains(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what) 
        {
            if (ValueIsAList(result_list)) {
                return (CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            } else {
                return (CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
            }
        }

        // Count how many items meet the conditions, with none of the results in result_list.
        // This has the option to say how many were found rather than list all.
        private int CountResultNotInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what) 
        {
            int count = 0, rec_count = 0;
            bool found_one;

            found_what = "";
            var arr = SplitOnCommaAndPrepareElements(result_list);         // also deals with CHAR_COMMA
            
            //Do not filter by result here; we need all results
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");
            
            foreach(var item in query) {
                rec_count++;
                //Look for each search word in the result
                found_one = false;
                
                foreach (string s in arr) {
                    if (String.Equals(item.RESULT, s)) {
                        found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "' -- ignore this";
                        Program.VerboseAudit(found_what);
                        found_one = true;
                        break;
                    }
                }

                if (! found_one) {
                    //none of the words were found - good!
                    found_what = Describe(cat, code_list, desc_list, field, "", search_depth) + " -- does not contain '" + result_list + "'";
                    Program.VerboseAudit(found_what);
                    count++;
                    if (count_mode != CountMode.CountAll) break;
                }
            }
            
            if (count > 0) {
                //We already printed what was found; maybe show the total?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else if (rec_count > 0) {
                //We already printed what we ignored
            } else {
                //Print what did not exist
                found_what = Describe(cat, code_list, desc_list, field, "", search_depth);      //not found
                Program.VerboseAudit(found_what);
            }
            
            return count;
        }

        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, string found_what) 
        {
            // Search in the pull range with trace on
            return CountResultDoesNotContain(cat, code_list, desc_list, field, result_list, out found_what, SearchDepth.SearchDefault, true);
        }
        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, out string found_what, SearchDepth search_depth, bool trace)
        {
            return CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0;
        }

        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, string found_what)
        {
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, out found_what, SearchDepth.SearchDefault, true);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, out string found_what, SearchDepth search_depth, bool trace)
        {
            return (CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0);
        }

        // Set the indicator if the conditions are met
        //
        private void SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already set
            if (_inds[inum].is_checked) return;
            
            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what)) {
                SetInd(inum, found_what);                          //echo here - found
            } else {
                Program.VerboseAudit(found_what);                  //and here - not found
            }
        }

        // Set the indicator if the result does not contain any of the words in result_list
        //
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultDoesNotContain(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {   
            string found_what;
            //avoid more queries if the indicator is already set
            if (_inds[inum].is_checked) return;
            
            //Turn trace off for ResultDoesNotContain() and echo what was set below with SetInd
            if (ResultDoesNotContain(cat, code_list, desc_list, field, result_list, out found_what, search_depth, false)) {
                SetInd(inum, found_what);                           //echo here - not found
            } else {
                Program.VerboseAudit(found_what);                   //and here - found
            }
        }

        // Clear the indicator if the result contains one of the words in the result_list
        //
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already clear
            if (! _inds[inum].is_checked) return;
            
            //Turn trace off for ResultContains() and echo what was set below with SetInd
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what)) {
                ClrInd(inum, found_what);                           //echo here - found
            } else {
                Program.VerboseAudit(found_what);                   //and here - not found
            }
        }


        //These slightly smaller functions are meant for places where you aren't really looking for a chart result
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            return (CountResultContains(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
        }

        // Variations with optional parameters (can switch to optional parameters in VS 2010)
        //
        private void SetIndIfFound(int inum, string cat)
        {
            SetIndIfResultContains(inum, cat, "", "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list)
        {
            SetIndIfResultContains(inum, cat, code_list, "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private void ClrIndIfFound(int inum, string cat)
        {
            ClrIndIfResultContains(inum, cat, "","","","", SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }


        // Get the max/total value from a result (usually in the middle of the text)
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetIntValue(get_mode, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            int value;
            int result = 0;
            bool found_one = false;

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            
            //Look for a number in the result
            
            foreach(var item in query) {
                // RESULT might be null so convert to empty string so Split won't blow up
                var arr = PFSUtility.DBToString(item.RESULT).Split(' ');
                foreach (string s in arr) {
                    //Look for 1st character numeric; IsNumeric("60min") will fail so look at 1st char only
                    if (s.Left(1).IsNumeric()) {
                        value = (int)s.Val();                           //Use Val; ToInteger will error on "60min"
                        switch (get_mode) {
                            case GetValueMode.GetMax:
                                result = Math.Max(value, result);       //max
                                break;
                            case GetValueMode.GetTotal:
                                result += value;                        //total
                                break;
                            case GetValueMode.GetLast:
                                result = value;                         //last
                                break;
                        }
                        
                        // print what we are searching for (the first time)
                        if (! found_one) {
                            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
                        }
                        found_one = true;
                        // print each value found
                        Program.VerboseAudit("  found numeric value " + result);
                        //Keep going in case there are more
                    }
                } 
            }
            
            if (! found_one) {
                //show what was not found
                Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
            }
            
            return result;
        }

        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetMaxValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return GetIntValue(GetValueMode.GetMax, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetTotalValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return GetIntValue(GetValueMode.GetTotal, cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Get a result; returns true if found with return_result
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result)
        {
            return GetResult(cat, code_list, desc_list, field, out return_result, SearchDepth.SearchDefault);
        }
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0) {
                return_result = PFSUtility.DBToString(query.First().RESULT);
            } else {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }
        private void GetResultforOther(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "Other (comment)");
            return_result="";
            foreach (var item in query)
            {
                if (!String.Equals(item.RESULT, "Other (comment)"))
                {
                    return_result = item.RESULT;
                    //Program.VerboseAudit(found_what);
                    break;
                }

            }
            return;
        }
        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, out string return_result, out DateTime return_evdt)
        {
            return GetResultAndEVDT(cat, code_list, desc_list, field, out return_result, out return_evdt, SearchDepth.SearchDefault);
        }
        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, out string return_result, out DateTime return_evdt, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0)
            {
                return_result = PFSUtility.DBToString(query.First().RESULT);
                return_evdt = PFSUtility.DBToDateTime(query.First().EVENT_DATETIME);
            }
            else
            {
                return_result = "";
                return_evdt = DateTime.MinValue;
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }


        //======================================================================================
        // Mapping starts here
        //======================================================================================

        private void Check_1_2_3_4()
        {

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("1. ADL Self");
            Program.VerboseAudit("2. ADL Assist");
            Program.VerboseAudit("3. ADL Extended");
            Program.VerboseAudit("4. ADL Complete");
            Program.VerboseAudit("---------------");
            
     // 4. COMPLETE CARE
            if (_pat.age < 4.0)
            {
                SetInd(4, "Age <=3 years");
            }
            if (_inds[4].is_checked) return;
            CheckSpinal();
            //SetIndIfResultContains(4, "", "PRECAUT_ACTIVITY", "", "", "Spinal");
string reslist;
bool ASST_HYG=false;
bool ASST_EAT=false;
bool ASST_DIET = false;
bool STAFF_BED = false;
bool STAFF_XFER=false;
bool STAFF_AMB = false;
bool ASST_APP = false;
            reslist = "Total";
            ASST_HYG = ResultContains("", "ASST_HYG", "", "", reslist);
            if (ASST_HYG) SetInd(140, "ASST_HYG Total");
            reslist="Total,1:1 Feeding/Supervision";
            ASST_EAT = ResultContains("", "ASST_EAT", "", "", reslist);
            if (ASST_EAT) SetInd(141, "ASST_EAT Total,1:1 Feeding/Supervision");
            reslist = "NPO";
            ASST_DIET = ResultContains("", "PERCENT_DIET", "", "", reslist);
            if (ASST_DIET) SetInd(142, "PERCENT_DIET NPO");
            reslist = "NPO";
            ASST_APP = ResultContains("", "ASST_APPETITE", "", "", reslist);
            if (ASST_APP) SetInd(143, "ASST_APP NPO");
            //reslist="1 person,2 people,3 people,4 people";
            //STAFF_BED = ResultContains("", "STAFF_BED", "", "", reslist);
            //STAFF_XFER = ResultContains("", "STAFF_XFER", "", "", reslist);
            //STAFF_AMB = ResultContains("", "STAFF_AMB", "", "", reslist);

            if (ASST_HYG && (ASST_EAT || ASST_DIET || ASST_APP)) SetInd(4, "Total Hyg + (Total Eat or NPO)");
            if (_inds[4].is_checked) return;

bool EATING=false;
            reslist="Total assistance,Helper administers,Helper scoops";
            EATING = ResultContains("", "ASST_BFAST", "", "", reslist) || 
                     ResultContains("", "ASST_LUNCH", "", "", reslist) ||
                     ResultContains("", "ASST_DINNER", "", "", reslist) ||
                     ResultContains("", "ASST_TUBEFEED", "", "", "Helper administers tube feedings");
            if (EATING) SetInd(144, "FIM4 Eat ASST_with meal Total,Helper");

bool HYG=false;
            HYG = ResultContains("", "ASST_GROOM", "", "", "Total assist,Helper provides") || 
                  ResultContains("", "ASST_BATH", "", "", "Total assist,Two caregivers");
            if (HYG) SetInd(145, "FIM4 Hyg ASST_groom/bath Total,Helper,2 careg");

bool DRESS=false;
            reslist="Total assist,Two caregivers,Patient requires,Helper sets up";
            DRESS = ResultContains("", "ASST_DRESSUPR", "", "", reslist) || 
                    ResultContains("", "ASST_DRESSLWR", "", "", reslist);
            if (DRESS) SetInd(146, "FIM4 Dress ASST_dress Total,Helper,2 careg,patient requires");

bool TOILET=false;
string reslist2;
string reslist3;
            reslist="Total assist,Two caregivers,Patient requires";
            reslist2="Helper manages Attends,Colostomy managed,Two caregivers,Helper empties,Patent performs 0-24,Patient performs 25-49,Patient performs 50-74";
            reslist3="Helper manages Attends,Two caregivers,Patient performs 0-24,Patient performs 25-49,Patient performs 50-74";
            TOILET = ResultContains("", "ASST_TOILET", "", "", reslist) || 
                     ResultContains("", "ASST_BOWEL", "", "", reslist2) ||
                     ResultContains("", "ASST_BLADDER", "", "", reslist3);
            if (TOILET) SetInd(147, "FIM4 Toilet ASST_toil/bowel/bladd Total,Helper,2 careg,patient requires,performs");

bool MOBILITY=false;
string reslist4;
string reslist5;
            reslist="Two caregivers,patient performs 25-49,patient performs 50-74";
            reslist2="Two caregivers required,Patient performs 0-24,patient performs 25-49,patient performs 50-74,Mechanical lift,Helper performs all tasks";
            reslist3="patient performs less than 25,Assistance of 2 helpers,Helper provides";
            reslist4="patient performs less than 25,Assistance of two helpers,Helper provides";
            reslist5="Assistance of two helpers,Helper provides";
            MOBILITY = ResultContains("", "ASST_XFERBATH", "", "", reslist) || 
                       ResultContains("", "ASST_XFERBED", "", "", reslist2) ||
                       ResultContains("", "ASST_WALK", "", "", reslist3) ||
                       ResultContains("", "ASST_WHLCHAIR", "", "", reslist4) ||
                       ResultContains("", "ASST_STAIRS", "", "", reslist5);
            if (MOBILITY) SetInd(148, "FIM4 Mobility ASST_xfer,walk,whlc,strs Helper,2 careg,patient requires,performs");

            if (EATING && HYG && DRESS && TOILET && MOBILITY) SetInd(4, "All 5 FIM Daily Care");
            if (_inds[4].is_checked) return;

     // 3 EXTENDED CARE
            reslist = "Partial,Total";
            ASST_HYG = ResultContains("", "ASST_HYG", "", "", reslist);
            if (ASST_HYG) SetInd(130, "ASST_HYG Partial/Total");
            reslist = "Partial,Total,1:1 Feeding/Supervision,Supervision of patient";
            ASST_EAT = ResultContains("", "ASST_EAT", "", "", reslist);
            if (ASST_EAT) SetInd(131, "ASST_EAT Partial/Total,1:1,supervi");
            reslist = "1 person,2 people,3 people,4 people";
            STAFF_BED = ResultContains("", "STAFF_BED", "", "", reslist);
            if (STAFF_BED) SetInd(132, "STAFF_BED 1+persons");
            STAFF_XFER = ResultContains("", "STAFF_XFER", "", "", reslist);
            if (STAFF_XFER) SetInd(133, "STAFF_XFER any");
            STAFF_AMB = ResultContains("", "STAFF_AMB", "", "", reslist);
            if (STAFF_AMB) SetInd(134, "STAFF_AMB any");
            // Daily care any 3 of 4
            if (((ASST_HYG || ASST_EAT) ? 1 : 0) + ((STAFF_BED || STAFF_XFER || STAFF_AMB) ? 1 : 0) == 2)
            {
                SetInd(3, "2 Daily Care: [Hyg or Eat] + [Mobility]");
            }
            Program.Audit("ASST_HYG=  " + (ASST_HYG ? 1 : 0));
            Program.Audit("ASST_EAT=  " + (ASST_EAT ? 1 : 0));
            Program.Audit("STAFF_BED= " + (STAFF_BED ? 1 : 0));
            Program.Audit("STAFF_XFER=" + (STAFF_XFER ? 1 : 0));
            Program.Audit("STAFF_AMB= " + (STAFF_AMB ? 1 : 0));

            if (_inds[3].is_checked) return;

            // FIM Daily care any 4 of 5
            reslist = "Total assistance,Helper administers,Helper scoops,Helper provides";
            EATING = ResultContains("", "ASST_BFAST", "", "", reslist) ||
                     ResultContains("", "ASST_LUNCH", "", "", reslist) ||
                     ResultContains("", "ASST_DINNER", "", "", reslist) ||
                     ResultContains("", "ASST_TUBEFEED", "", "", "Helper administers tube feedings");
            if (EATING) SetInd(135, "FIM3 Eat ASST_with meal Total,Helper");

            reslist = "Total assist,Helper provides,Helper applies";
            reslist2= "Total assist,Two caregivers,Patient requires steadying,Helper provides supervision";
            HYG = ResultContains("", "ASST_GROOM", "", "", reslist) ||
                  ResultContains("", "ASST_BATH", "", "", reslist2);
            if (HYG) SetInd(136, "FIM3 Hyg ASST_groom/bath Total,Helper,2 careg");

            reslist = "Total assist,Two caregivers,Patient requires,Helper sets up,Helper provides supervision";
            DRESS = ResultContains("", "ASST_DRESSUPR", "", "", reslist) ||
                    ResultContains("", "ASST_DRESSLWR", "", "", reslist);
            if (DRESS) SetInd(137, "FIM3 Dress ASST_dress Total,Helper,2 careg,patient requires");


            reslist = "Two caregivers,Patient requires,Helper provides supervision";
            reslist2 = "Helper manages Attends,Colostomy managed,Two caregivers,Helper empties,Patent performs 0-24,Patient performs 25-49,Patient performs 50-74,Patient performs 75-99,Helper provides supervision";
            TOILET = ResultContains("", "ASST_TOILET", "", "", reslist) ||
                     ResultContains("", "ASST_BOWEL", "", "", reslist2) ||
                     ResultContains("", "ASST_BLADDER", "", "", reslist2);
            if (TOILET) SetInd(138, "FIM3 Toilet ASST_toil/bowel/bladd Total,Helper,2 careg,patient requires,performs");

            reslist = "Two caregivers,patient performs 25-49,patient performs 50-74,patient performs 75-99,Helper sets-up,Helper provides supervision";
            reslist2 = "Two caregivers required,Patient performs 0-24,patient performs 25-49,patient performs 50-74,patient performs 75-99,Mechanical lift,Helper performs all tasks,Helper sets-up,Helper provides supervision";
            reslist3 = "patient performs less than 25,Assistance of 2 helpers,Helper provides";
            reslist4 = "patient performs less than 25,Assistance of two helpers,Helper provides";
            reslist5 = "Assistance of two helpers,Helper provides";
            MOBILITY = ResultContains("", "ASST_XFERBATH", "", "", reslist) ||
                       ResultContains("", "ASST_XFERBED", "", "", reslist2) ||
                       ResultContains("", "ASST_WALK", "", "", reslist3) ||
                       ResultContains("", "ASST_WHLCHAIR", "", "", reslist4) ||
                       ResultContains("", "ASST_STAIRS", "", "", reslist5);
            if (MOBILITY) SetInd(139, "FIM3 Mobility ASST_xfer,walk,whlc,strs Helper,2 careg,patient requires,performs");

            // FIM Daily care any 4/5
            if ((EATING ? 1 : 0) + (HYG ? 1 : 0) + (DRESS ? 1 : 0) + (TOILET ? 1 : 0) + (MOBILITY ? 1 : 0) >= 4)
            {
                SetInd(3, "4 FIM Daily Care");
            }
            if (_inds[3].is_checked) return;


      // 2 PARTIAL
            // Daily care any of 5
            if (ASST_HYG || ASST_EAT || STAFF_BED || STAFF_XFER || STAFF_AMB)
            {
                SetInd(2, "Assistance with Daily Care");
            }
            if (_inds[2].is_checked) return;

            if (EATING || HYG || DRESS || TOILET || MOBILITY)
            {
                SetInd(2, "Assistance with FIM Daily Care");
            }
            if (_inds[2].is_checked) return;

            reslist = "independent";
            ASST_HYG = ResultContains("", "ASST_HYG", "", "", reslist);
            ASST_EAT = ResultContains("", "ASST_EAT", "", "", reslist);
            STAFF_BED = ResultContains("", "STAFF_BED", "", "", reslist);
            STAFF_XFER = ResultContains("", "STAFF_XFER", "", "", reslist);
            STAFF_AMB = ResultContains("", "STAFF_AMB", "", "", reslist);

            if ((ASST_HYG ? 1 : 0) + (ASST_EAT ? 1 : 0) + ((STAFF_BED || STAFF_XFER || STAFF_AMB) ? 1 : 0) == 3)
            {
                SetInd(1, "All 3 of Hyg,Eat,Mobility are Independent");
            }

            if (_inds[1].is_checked) return;
    
            // Default to partial if nothing documented
            SetInd(1, "Default to ADL Self - No documentation found");

        }
        private void CheckSpinal()
        {
            string codelist = "PRECAUT_ACTIVITY";
            string reslist = "Spinal";
            // Note: check since admission
            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = AndItemFilter(query, "", codelist, "", "", reslist);
            query = AndResultNotInList(query, "Spinal - cleared");
            if (query.Count() > 0)
            {
                string found_what = Describe(query, "", codelist, "", "", reslist);
                SetInd(4, "Found: " + found_what);
            }
        }

        private void Check_5()
        {
            string found_what;
            
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("5. ADL Rehab");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(5, "", "RANGE_MOTION", "", "", "Yes");
            SetIndIfResultContains(5, "", "THERAPY_PLAN", "", "", "Physical Therapy,Occupational Therapy,Speech/Swallow Therapy");

            if (GetResult("", "MINS_PT", "", "", out found_what)) {
                var arr = found_what.Split(' ');
                if (arr[0].IsNumeric())
                {
                    if (arr[0].ToInteger() > 0) {
                        SetInd(5, "MINS_PT =" + found_what);
                    }
                }
            }
            if (_inds[5].is_checked) return;
            if (GetResult("", "MINS_OT", "", "", out found_what))
            {
                var arr = found_what.Split(' ');
                if (arr[0].IsNumeric())
                {
                    if (arr[0].ToInteger() > 0) {
                        SetInd(5, "MINS_OT =" + found_what);
                    }
                }
            }
            if (_inds[5].is_checked) return;
            if (GetResult("", "MINS_ST", "", "", out found_what))
            {
                var arr = found_what.Split(' ');
                if (arr[0].IsNumeric())
                {
                    if (arr[0].ToInteger() > 0) {
                        SetInd(5, "MINS_ST =" + found_what);
                    }
                }
            }
            if (_inds[5].is_checked) return;

            SetIndIfResultContains(5, "", "PRECAUT_OTHER", "", "", "Aspiration");
            SetIndIfResultContains(5, "", "BOWEL_METHOD", "", "", "");
            SetIndIfResultContains(5, "", "ASST_BLADDER", "", "", "2 hour toileting program cued by staff");

        }


        private void Check_6_7()
        {    
            string reslist;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("6. ADL 2-3 Caregivers");
            Program.VerboseAudit("7. ADL 4 or more Caregivers");
            Program.VerboseAudit("---------------");

            reslist = "Ceiling lift - Repositioning sheet,Ceiling lift - seated sling,Ceiling lift - turning sling";
            reslist += ",Friction reducing transfer sheet,Turn and position system";
            SetIndIfResultContains(7, "", "EQUIP_BED", "", "", reslist);

            reslist = "Air assistive transfer device";
            int ct = CountResultContains("", "EQUIP_BED", "", "", reslist);
            if (ct > 1) SetInd(7, "Air assistive transfer device count = " + ct);

            reslist = "Ceiling lift - Repositioning sheet,Ceiling lift - seated sling,Ceiling lift - turning sling";
            reslist += ",Friction reducing transfer sheet,Turn and position system,Portable electric lift";
            reslist += ",Portable non-electric lift,Sit to stand device,Slide board seated,Slide board supine";
            SetIndIfResultContains(7, "", "EQUIP_XFER", "", "", reslist);

            reslist = "Air assistive transfer device";
            ct = CountResultContains("", "EQUIP_XFER", "", "", reslist);
            if (ct > 1) SetInd(7, "Air assistive transfer device count = " + ct);

            reslist = "Ceiling lift with ambulation sling,Sit to stand device with ambulation sling";            
            SetIndIfResultContains(7, "", "EQUIP_GAIT", "", "", reslist);

            SetIndIfResultContains(7, "", "STAFF_BED", "", "", "4 people");
            SetIndIfResultContains(7, "", "STAFF_XFER", "", "", "4 people");
            SetIndIfResultContains(7, "", "STAFF_AMB", "", "", "4 people");
            if (_inds[7].is_checked) return;


            if (_inds[7].is_checked) return;
            if (_inds[6].is_checked) return;
            reslist = "2 people,3 people";
            SetIndIfResultContains(6, "", "STAFF_BED", "", "", reslist);
            SetIndIfResultContains(6, "", "STAFF_XFER", "", "", reslist);
            SetIndIfResultContains(6, "", "STAFF_AMB", "", "", reslist);
            if (_inds[6].is_checked) return;

            reslist = "Two caregivers";
            SetIndIfResultContains(6, "", "ASST_BATH", "", "", reslist);
            SetIndIfResultContains(6, "", "ASST_XFERBATH", "", "", reslist);
            SetIndIfResultContains(6, "", "ASST_DRESSUPR", "", "", reslist);
            SetIndIfResultContains(6, "", "ASST_DRESSLWR", "", "", reslist);
            SetIndIfResultContains(6, "", "ASST_TOILET", "", "", reslist);
            SetIndIfResultContains(6, "", "ASST_BOWEL", "", "", reslist);
            SetIndIfResultContains(6, "", "ASST_BLADDER", "", "", reslist);
            SetIndIfResultContains(6, "", "ASST_XFERBED", "", "", reslist);
            if (_inds[6].is_checked) return;

            reslist = "Assistance of 2 helpers,Assistance of two helpers";
            SetIndIfResultContains(6, "", "ASST_WALK", "", "", reslist);
            SetIndIfResultContains(6, "", "ASST_WHLCHAIR", "", "", reslist);
            SetIndIfResultContains(6, "", "ASST_STAIRS", "", "", reslist);
            if (_inds[6].is_checked) return;

            reslist = "Started,Continued";
            SetIndIfResultContains(6, "", "INCUBATOR_CHANGED", "", "", "Done");
            if (_pat.unit_name == "NICU")
            {
                SetIndIfResultContains(6, "", "STATUS_CPAP", "", "", reslist);
                SetIndIfResultContains(6, "", "STATUS_BPAP", "", "", reslist);
            }
            if (_inds[6].is_checked) return;

            if (ResultContains("", "O2DEVICE", "", "", "Mechanical Ventilation"))
            {
                reslist = "Kangaroo care,infant seat,infant swing";
                SetIndIfResultContains(6, "", "NEONATE_POSITION", "", "", reslist);
                SetIndIfResultContains(6, "", "NEONATE_COMFORT", "", "", reslist);
            }
            SetIndIfResultContains(6, "", "EQUIP_XFER", "", "", "tilt table");
        }

        private void Check_8()
        {
            string reslist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("8. Communication");
            Program.VerboseAudit("---------------");

            //Note: ADT-dependent trigger not included here. Comm support
            if ((_pat.language.Trim() != "") && (_pat.language.ToUpper() != "ENGLISH"))
            {
                SetInd(8, "Primary Language is: " + _pat.language.ToUpper());
            }

            reslist = "English is not the patient,An interpreter was present,An alternative communication system was used";
            SetIndIfResultContains(8, "", "OPTX_COMMUNICATION", "", "", reslist);

            reslist = "Developmentally delayed,Expressively aphasic,Garbled,Globally aphasic";
            reslist += ",Incomprehensible words,Language barrier - Non-English Speaking,Mouths words,Perseverates";
            reslist += ",Receptively aphasic,Sign Language,Slurred,Uses paraphasia,uses communication device/board";
            SetIndIfResultContains(8, "", "SPEECH", "", "", reslist);

            SetIndIfResultContains(8, "", "GCSVERBAL", "", "", "2,3");

            reslist = "Blind,Blurred,Double vision";
            if (ResultContains("", "VISIONRIGHT", "", "", reslist, SearchDepth.SearchSinceAdmission))
            {
                SetIndIfResultContains(8, "", "VISIONLEFT", "", "", reslist, SearchDepth.SearchSinceAdmission);
            }

            reslist = "Deaf" + CHAR_COMMA + " uncorrected,Hard of hearing" + CHAR_COMMA + " uncorrected";
            if (ResultContains("", "HEARING_RIGHT", "", "", reslist, SearchDepth.SearchSinceAdmission))
            {
                SetIndIfResultContains(8, "", "HEARING_LEFT", "", "", reslist, SearchDepth.SearchSinceAdmission);
            }
            SetIndIfResultContains(8, "", "VISION_BLIND", "", "", "Eye Bilateral", SearchDepth.SearchSinceAdmission);
            SetIndIfResultContains(8, "", "HEARING_PROBLEM", "", "", "Deaf"+CHAR_COMMA+" Uncorrected, Hard of Hearing"+CHAR_COMMA+" Uncorrected", SearchDepth.SearchSinceAdmission);


            reslist = "Patient expresses BASIC needs less than 25%";
            reslist += ",Patient expresses BASIC needs 25%-49%";
            reslist += ",Patient expresses BASIC needs 50%-74%";
            reslist += ",Patient expresses BASIC needs and ideas 75%-90%";
            SetIndIfResultContains(8, "", "FIM_EXPRESSION", "", "", reslist);

            PairSpeechWithTrach();

        }

        private void PairSpeechWithTrach()
        {
            DateTime evdt = DateTime.MinValue;
            bool done = false;

            var query = StartNewQuery();
            query = AndItemFilter(query, "", "SPEECH", "", "", "intubated/trached");
            query = query.OrderBy(e => e.EVENT_DATETIME);
            if (query.Count() > 0)
            {
                foreach (var ci in query)
                {
                    if (ci.EVENT_DATETIME != evdt && !done)
                    {
                        evdt = ci.EVENT_DATETIME;
                        var rquery = StartNewQuery();
                        rquery = AndItemFilter(rquery, "", "CONSC_LVL", "", "", "alert");
                        rquery = rquery.Where(e => e.EVENT_DATETIME == ci.EVENT_DATETIME);
                        if (rquery.Count() > 0)
                        {
                            done = true;
                        }
                    }
                }
            }

            if (done)
                SetInd(8, "Alert while intubated/trached at: " + evdt.ToString());

        } // pair


        private void Check_9()
        {
            string reslist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("9. Cognitive Support");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(9, "", "PAT_ORIENT", "", "", "Disoriented");

            reslist = "Provide reality orientation";
            SetIndIfResultContains(9, "", "LESSRESTRICT_ALT", "", "", reslist);

            reslist = "Reality orientation";
            SetIndIfResultContains(9, "", "FALLRISK_INTERVNTION", "", "", reslist);

            reslist = "Confused,Delusional";
            SetIndIfResultContains(9, "", "RESTRAINT_NVVISCHCK", "", "", reslist);
            SetIndIfResultContains(9, "", "CAM_FEATURE3", "", "", "Yes");

            reslist = "Patient does not respond appropriately or consistently despite prompting";
            reslist += ",Patient understands directions and conversation about BASIC daily needs less than 25%";
            reslist += ",Patient understands directions and conversation about BASIC daily needs 25%-49%";
            reslist += ",Patient understands directions and conversation about BASIC daily needs 50%-74%";
            SetIndIfResultContains(9, "", "FIM_COMPREHEN", "", "", reslist);

            reslist = "Patient recognizes and remembers less than 25%";
            reslist += ",Patient recognizes and remembers 25%-49%";
            reslist += ",Patient recognizes and remembers 50%-74%";
            SetIndIfResultContains(9, "", "FIM_MEMORY", "", "", reslist);

            reslist = "Confused and pulling,confused and unsteady,confused and elopement risk";
            SetIndIfResultContains(9, "", "ATTND_SAFETYREASON", "", "", reslist);

        }

        private void Check_10_11()
        {
            string reslist;
            int ct;
            string found_what;
            List<int> buckets;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("10. Behavior/Emotional Management");
            Program.VerboseAudit("11. Behavior/Emotional Mgmt - q 1 Hour");
            Program.VerboseAudit("---------------");

            if (_pat.los_hours >= 2)
            {
                reslist = "Violent and/or Self Destructive";
                ct = CountItems("", "RESTRAINT_TYPE", "", "", reslist, SearchDepth.SearchDefault, true, out found_what);
                if (ct >= 2)
                {
                    SetInd(11, found_what);
                }

                reslist = "Aggressive physically,Aggressive verbally";
                ct = CountItems("", "VISITOR_INTERACTION", "", "", reslist, SearchDepth.SearchDefault, true, out found_what);
                if (ct >= 2)
                {
                    SetInd(11, found_what);
                }

                reslist = "Confused,Physically threatening,Verbally abusive";
                ct = CountItems("", "RESTRAINT_NVVISCHCK", "", "", reslist, SearchDepth.SearchDefault, true, out found_what);
                if (ct >= 2)
                {
                    SetInd(11, found_what);
                }
                SetIndIfResultContains(11, "", "AGITATED_IMPULSIVE,AGITATED_UNCOOP,AGITATED_VIOLENT,AGITATED_ANGER,AGITATED_SELFABUSIVE", "", "", "4");

                if (_inds[11].is_checked) return;
                SetBucketSize(60);

                buckets = new List<int>();
                reslist = "Apathetic,Defensive,Guarded,Hostile,Uncooperative,Suspicious";
                AddBuckets(buckets, "", "ATTITUDE", "", "", reslist);
                reslist = "Agitated,Hostile,Hyperactive,Uncooperative";
                AddBuckets(buckets, "", "ASSMNT_BEHAVIOR", "", "", reslist);
                reslist = "Poor concentration or follow through";
                AddBuckets(buckets, "", "COPING", "", "", reslist);
                reslist = "Violent and/or Self Destructive";
                AddBuckets(buckets, "", "RESTRAINT_TYPE", "", "", reslist);
                reslist = "Apprehensive to situation,Refuses to acknowledge";
                AddBuckets(buckets, "", "INSIGHT", "", "", reslist);
                reslist = "Depressed,Despairing,Fearful,Labile,Hopeless";
                AddBuckets(buckets, "", "MOOD", "", "", reslist);
                reslist = "Locked restraints,Suicide precautions,Violent and/or self destructive restraints";
                AddBuckets(buckets, "", "ATTND_SAFETYREASON", "", "", reslist);
                reslist = "Anxious,Aggressive physically,Aggressive verbally,Flat affect,Tearful,Withdrawn,Inconsolable,Uncooperative";
                AddBuckets(buckets, "", "VISITOR_INTERACTION", "", "", reslist);
                reslist = "Agitated/restless,Confused,Delusional,Physically threatening,Verbally abusive";
                AddBuckets(buckets, "", "RESTRAINT_PATSTATUS", "", "", reslist);
                reslist = "3,4";
                AddBuckets(buckets, "", "AGITATED_IMPULSIVE,AGITATED_UNCOOP,AGITATED_VIOLENT,AGITATED_ANGER,AGITATED_SELFABUSIVE", "", "", reslist);
                AddBuckets(buckets, "", "AGITATED_SCORE", "", "", ">28");
                reslist = "Agitated/restless,Confused,Delusional,Physically threatening,Verbally abusive";
                AddBuckets(buckets, "", "RESTRAINT_NVVISCHCK", "", "", reslist);

                ct = CountBuckets(buckets);
                if (FreqForCount(_pat.los_hours, ct) >= Frequencies.Q2H) //use Q2H for q1 since these are 60-min buckets
                {
                    SetInd(11, "Behavior items at q1 hour frequency; count="+ct);
                }

                if (GetResult("", "AGITATED_SCORE", "", "", out found_what))
                {
                    if (found_what.IsNumeric())
                    {
                        if (found_what.ToInteger() >= 29 && found_what.ToInteger() <= 35)
                            SetInd(10, "AGITATED_SCORE =" + found_what);
                        else
                            if (found_what.ToInteger() > 35)
                                SetInd(11, "AGITATED_SCORE =" + found_what);
                    }
                }
                if (GetResult("", "NEONATE_NASSCORE", "", "", out found_what))
                {
                    if (found_what.IsNumeric())
                    {
                        if (found_what.ToInteger() >= 8)
                        {
                            SetInd(11, "NEONATE_NASSCORE =" + found_what);
                        }
                    }
                }

                if (_inds[11].is_checked) return;
            } // los hours >= 2

            LookFor2Violence();

            reslist = "Apathetic,Defensive,Guarded,Hostile,Uncooperative,Suspicious";
            SetIndIfResultContains(10, "", "ATTITUDE", "", "", reslist);
            reslist = "Agitated,Hostile,Hyperactive,Uncooperative";
            SetIndIfResultContains(10, "", "ASSMNT_BEHAVIOR", "", "", reslist);
            reslist = "Poor concentration or follow through";
            SetIndIfResultContains(10, "", "COPING", "", "", reslist);
            reslist = "Violent and/or Self Destructive";
            SetIndIfResultContains(10, "", "RESTRAINT_TYPE", "", "", reslist);
            reslist = "Apprehensive to situation,Refuses to acknowledge";
            SetIndIfResultContains(10, "", "INSIGHT", "", "", reslist);
            reslist = "Depressed,Despairing,Fearful,Labile,Hopeless";
            SetIndIfResultContains(10, "", "MOOD", "", "", reslist);
            reslist = "Locked restraints,Suicide precautions,Violent and/or self destructive restraints";
            SetIndIfResultContains(10, "", "ATTND_SAFETYREASON", "", "", reslist);
            reslist = "Anxious,Aggressive physically,Aggressive verbally,Flat affect,Tearful,Withdrawn,Inconsolable,Uncooperative";
            SetIndIfResultContains(10, "", "VISITOR_INTERACTION", "", "", reslist);
            reslist = "Agitated/restless,Confused,Delusional,Physically threatening,Verbally abusive";
            SetIndIfResultContains(10, "", "RESTRAINT_PATSTATUS", "", "", reslist);
            reslist = "3,4";
            SetIndIfResultContains(10, "", "AGITATED_IMPULSIVE,AGITATED_UNCOOP,AGITATED_VIOLENT,AGITATED_ANGER,AGITATED_SELFABUSIVE", "", "", reslist);
            SetIndIfResultContains(10, "", "AGITATED_SCORE", "", "", ">28");
            reslist = "Agitated/restless,Confused,Delusional,Physically threatening,Verbally abusive";
            SetIndIfResultContains(10, "", "RESTRAINT_NVVISCHCK", "", "", reslist);

            SetIndIfResultContains(10, "", "MOTOR_ACTIVITY", "", "", "Agitation");
            SetIndIfResultContains(10, "", "CONSC_LVL", "", "", "Agitated");

            reslist = "Impulsive,Poor safety awareness,Poor judgement";
            SetIndIfResultContains(10, "", "COGNITION", "", "", reslist);

            SetIndIfResultContains(10, "", "NEONATE_ACTIVITY", "", "", "Irritable");

            SetIndIfResultContains(10, "", "DEATH_NOTIFYREL", "", "", "Relative Present");
            SetIndIfResultContains(10, "", "PRECAUT_SUICIDE", "", "", "Yes");
            SetIndIfResultContains(10, "", "PRECAUT_AGGRESS", "", "", "Yes");
            ct = ReturnQ1HrCount("PRECAUT_SUICIDE", "Yes");
            if (ct >= _pat.los_hours / 2.0)  // check that q1 for half the los
            {
                SetInd(11, "PRECAUT_SUICIDE count = " + ct);
            }
            ct = CountResultContains("", "PRECAUT_AGGRESS", "", "", "Yes");
            if (ct >= 2)  
            {
                SetInd(11, "PRECAUT_AGGRESS count = " + ct);
            }

            reslist = "Patient interacts appropriately less than 25%";
            reslist += ",Patient interacts appropriately 25%-49%";
            reslist += ",Patient interacts appropriately 50%-74%";
            SetIndIfResultContains(10, "", "FIM_SOCINTERACT", "", "", reslist);

            SetIndIfResultContains(10, "", "FIM_VIOLENCE", "", "", "Yes");

            SetIndIfResultContains(10, "", "AGITATED_IMPULSIVE,AGITATED_UNCOOP,AGITATED_VIOLENT,AGITATED_ANGER,AGITATED_SELFABUSIVE", "", "", "3");

            if (GetResult("", "FIM_HRSINAPPR", "", "", out found_what))
            {
                if (found_what.IsNumeric())
                {
                    if (found_what.ToInteger() > 0)
                    {
                        SetInd(10, "FIM_HRSINAPPR =" + found_what);
                    }
                }
            }

            reslist = "Calming Environment,Distraction,Redirection";
            SetIndIfResultContains(10, "", "INTERVENTIONS_RESPONSE", "", "", reslist);
            SetBucketSize(60);
            buckets = new List<int>();
            AddBuckets(buckets, "", "INTERVENTIONS_RESPONSE", "", "", reslist);
            ct = CountBuckets(buckets);
            if (ct >= _pat.los_hours / 2) SetInd(11, "Interventions Response count=" + ct);

        }

        private void LookFor2Violence()
        {
            int ct = 0;
            DateTime evdt = DateTime.MaxValue;
            string s = "";

            var query = StartNewQuery();
            query = AndItemFilter(query, "", "FIM_VIOLENCE", "", "", "Yes");
            query = query.OrderBy(e => e.EVENT_DATETIME);
            if (query.Count() > 1)
            {
                foreach (var ci in query)
                {
                    if (ci.EVENT_DATETIME != evdt && ct < 2)
                    {
                        ct++;
                        evdt = ci.EVENT_DATETIME;
                        s += ci.EVENT_DATETIME.ToString() + " ";
                    }
                }
            }

            if (ct >= 2)
                SetInd(11, "At least 2 FIM_VIOLENCE found: " + s);
        }

        // (this isn't really a Q1h count -- it is just a count) Make this a q1hr count
        private int ReturnQ1HrCount(string code_list, string result_list)
        {
            var buckets = new List<int>();
            SetBucketSize(60);
            AddBuckets(buckets, "", code_list, "", "", result_list);
            return CountBuckets(buckets);
        }

        private bool IsQ1Hour(int count)
        {
            return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q1H);
        }

        // Use this if counting safety buckets
        //private void CheckSafety(int count, string desc)
        //{
        //    if (_inds[13].is_checked) return;             //skip if highest already checked
        //    if (count == 0) return;
        //    switch (FreqForCount(_pat.los_hours, count))
        //    {
        //    case Frequencies.Q30M:
        //        SetInd(13, desc + " q30min");
        //        break;
        //    case Frequencies.Q1H:
        //    case Frequencies.Q2H:
        //        SetInd(12, desc + " q2h");
        //        break;
        //    default:
        //        Program.VerboseAudit(desc + ": " + count + " in " + Math.Round(_pat.los_hours) + " hours is not enough");
        //        break;
        //    }
        //}

        private void Check_12_13()
        {
            string found_what;
            string reslist;
            string desclist;
            bool sedated=false;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("12. Safety Management - q 2 Hours");
            Program.VerboseAudit("13. Safety Management - q 30 Minutes");
            Program.VerboseAudit("---------------");

            //if (IsAttndSafetyContinued())
            //{
            //    SetInd(13, "Attnd Safety started or continued");
            //}

            SetIndIfResultContains(13, "", "MONITORING_TYPE", "", "", "Patient safety attendant");
            SetIndIfResultContains(13, "", "MONITORING_TYPE", "", "", "Video monitoring trial with patient safety attendant");
            SetIndIfResultContains(13, "", "MONITORING_TYPE", "", "", "Patient safety observer");
            SetIndIfResultContains(13, "", "MONITORING_TYPE", "", "", "Video monitoring trial with patient safety observer");
            reslist = "Restraint Monitoring Every 10 minutes (Violent or Self-Destructive) by RN, LPN, NA, or Technician";
            SetIndIfResultContains(13, "", "RESTRAINT_VISCHECK", "", "", reslist);
            SetIndIfResultContains(13, "", "FIM_PROBSOLV", "", "", "Patient requires 1:1 supervision");
            if (_inds[13].is_checked) return;

//2014-06-30 12:08:23.000	PLACEMENT_DT	20140630
//2014-06-30 12:08:23.000	PLACEMENT_DT&61	20140630
//2014-06-30 12:08:23.000	PLACEMENT_TM	12080000
//2014-06-30 12:08:23.000	PLACEMENT_TM&61	12080000
//2014-06-30 12:17:38.000	PLACEMENT_DT&62	20140630
//2014-06-30 12:17:38.000	PLACEMENT_TM&62	12170000

            if (_pat.unit_name == "NICU")
            {
//Catheter: Umbilical, Arterial, Single Lumen
//Catheter: Umbilical, Venous, Double Lumen
//Catheter: Umbilical, Venous, Single Lumen
                desclist = "Catheter: Umbilical";
                LookForCatheterPlacement(13, desclist, "Line Placement in NICU: ");

                if (_inds[13].is_checked) return;

                sedated = ResultContains("", "NEONATE_ACTIVITY", "", "", "Non-active,Paralyzed (medication),Sedated");
                if (!sedated)
                {
                    //reslist = "Catheter:,Hemodialysis:,Port:,Pump:,Single Lumen Infusion";
                    //SetIndIfResultContains(12, "", "PLACEMENT_DT,PLACEDBY", reslist, "", "");
                    //SetIndIfResultContains(12, "", "PLACEMENT_TM", reslist, "", "");
                    //SetIndIfResultContains(12, "", "REMOVAL_DT", reslist, "", "");
                    //SetIndIfResultContains(12, "", "REMOVAL_TM", reslist, "", "");
                    desclist = "Catheter: Arterial,Catheter: Left Atrial,Catheter: Right Atrial,Catheter: Central Venous";
                    desclist += ",Catheter: Epidural,Catheter: Impella,Catheter: Intra-aortic Balloon Pump";
                    desclist += ",Catheter: Intraosseous,Catheter: Intrathecal,Catheter: Intravascular Cooling";
                    desclist += ",Catheter: Introducer,Catheter: Midline Venous,Catheter: Nerve Block";
                    desclist += ",Catheter: Peripheral Venous,Catheter: Pulmonary Artery,Catheter: Sheath";
                    desclist += ",Catheter: Vascular Dialysis,Hemodialysis: Arteriovenous Access";
                    desclist += ",Port: Central,Port: Hepatic Artery Infusion,Port: Peritoneal";
                    desclist += ",Pump: Intravenous,Single Lumen Infusion Catheter";
                    LookForCatheterPlacement(12, desclist, "Line Placement in NICU: ");
                }
            }
            if (_inds[13].is_checked) return;

            SetIndIfResultContains(12, "", "MONITORING_TYPE", "", "", "Video monitoring");
            if (_inds[12].is_checked) return;

            SetIndIfResultContains(12, "", "PRECAUT_OTHER", "", "", "Seizure");
            SetIndIfResultContains(12, "", "PRECAUT_SUICIDE", "", "", "Yes");
            SetIndIfResultContains(12, "", "FALLRISK_STATUS", "", "", "High Fall Risk");
            SetIndIfResultContains(12, "", "FALLRISK_INTERVNTION", "", "", "Fall alert wristband,Patient situated close to nursing station");
            SetIndIfResultContains(12, "", "SAFETY_BEHAVIOR", "", "", "Attempts to get out of bed/chair without assistance,Pulling at lines,Wandering/Elopement");

            if (GetResult("", "FALLRISK_SCOREADULT", "", "", out found_what))
            {
                if (found_what.IsNumeric()) {
                    if (found_what.ToInteger() >= 5) {
                        SetInd(12, "FALLRISK_SCOREADULT =" + found_what);
                    }
                }
            }
            if (_inds[12].is_checked) return;
            
            if (GetResult("", "FALLRISK_SCOREPEDS", "", "", out found_what))
            {
                if (found_what.IsNumeric()) {
                    if (found_what.ToInteger() >= 2) {
                        SetInd(12, "FALLRISK_SCOREPEDS =" + found_what);
                    }
                }
            }
            if (_inds[12].is_checked) return;

            SetIndIfResultContains(12, "", "RESTRAINT_PATSTATUS", "", "", "");
            SetIndIfResultContains(12, "", "RESTRAINT_VIOLENT", "", "", "Start,Continued");
            SetIndIfResultContains(12, "", "RESTRAINT_NONVIOLENT", "", "", "Start,Continued");
            if (_inds[12].is_checked) return;
            reslist = "No - Cognitive impairment,No - Interference with therapy,No - Restless/Agitated behavior";
            reslist += ",No - Risk of serious harm from falling,No - Violent or self-destructive behavior";
            SetIndIfResultContains(12, "", "RESTRAINT_EVAL", "", "", reslist);
            SetIndIfResultContains(12, "", "AGITATED_PULLTUBES", "", "", "2,3,4");
            if (_inds[12].is_checked) return;
            if (GetResult("", "AGITATED_SCORE", "", "", out found_what))
            {
                if (found_what.IsNumeric()) {
                    if (found_what.ToInteger() > 28) {
                        SetInd(12, "AGITATED_SCORE =" + found_what);
                    }
                }
            }

            if (GetResult("", "FIM_HRSALARMPSA", "", "", out found_what))
            {
                if (found_what.IsNumeric()) {
                    if (found_what.ToInteger() > 0) {
                        SetInd(12, "FIM_HRSALARMPSA =" + found_what);
                    }
                }
            }
            if (GetResult("", "FIM_HRSROAMALRT", "", "", out found_what))
            {
                if (found_what.IsNumeric()) {
                    if (found_what.ToInteger() > 0) {
                        SetInd(12, "FIM_HRSROAMALRT =" + found_what);
                    }
                }
            }

            SetIndIfResultContains(12, "", "PRECAUT_AGGRESS", "", "", "Yes");
            SetIndIfResultContains(12, "", "FALLPRVNT_BEDALARM", "", "", "On");
            SetIndIfResultContains(12, "", "FALLPRVNT_CHAIRALARM", "", "", "On");
            SetIndIfResultContains(12, "", "SPEECH", "", "", "Developmentally delayed", SearchDepth.SearchSinceAdmission);
            SetIndIfResultContains(12, "", "COGNITION", "", "", "Developmentally delayed", SearchDepth.SearchSinceAdmission);

            if (_pat.age <= 5.0)
            {
                SetIndIfResultContains(12, "", "PARENT_INVOLVE", "", "", "Available by phone,No involvement,Sporadic");
            }

            //SetIndIfResultContains(12, "", "FAMILY_INTERACT", "", "", "Phone Unit,Virtual Visit");
            SetIndIfResultContains(12, "", "FIM_PROBSOLV", "", "", "Alarms,lap restraints,special bed for safety");
        }

        private bool IsAttndSafetyContinued()
        {
            DateTime evdt1 = DateTime.MinValue;
            DateTime evdt2 = DateTime.MinValue;
            int count = 0;

            //find latest time of attnd_safety since admission that is not discontinued
            // order_control=X means discontinue already processed
            string reslist = "Start,Continue,Weaning,Limited hours";
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = AndItemFilter(query, "", "ATTND_SAFETY", "", "", reslist);
            query = query.Where(e => !e.RESULT.Contains("Discontinue"));
            query = query.Where(e => !e.ORDER_CONTROL.Contains("X"));
            count = query.Count();
            if (count == 0) return false;

            evdt1 = PFSUtility.DBToDateTime(query.Max(x => x.EVENT_DATETIME));
            if (evdt1 > DateTime.MinValue)
            {
                Program.VerboseAudit("ATTND_SAFETY most recent start/continue on:"+evdt1.ToString());
            }
            else
            {
                Program.VerboseAudit("ATTND_SAFETY not found since admission");
            }


            if (evdt1 > DateTime.MinValue)
            {
                reslist = "Discontinue";
                query = StartNewQuery(SearchDepth.SearchSinceAdmission);
                query = AndItemFilter(query, "", "ATTND_SAFETY", "", "", reslist);
                query = query.Where(e => !e.ORDER_CONTROL.Contains("X"));
                count = query.Count();
                if (count > 0)
                {
                    evdt2 = PFSUtility.DBToDateTime(query.Max(x => x.EVENT_DATETIME));
                }
                if (evdt2 > DateTime.MinValue)
                {
                    Program.VerboseAudit("ATTND_SAFETY most recent Discontinue on:" + evdt2.ToString());
                }
                else
                {
                    Program.VerboseAudit("ATTND_SAFETY discontinue not found since admission");
                }
                return (evdt2 < evdt1);
            }
            else
            {
                return false;
            }

        }


        private void Check_14()
        {    
            string reslist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("14. Isolation");
            Program.VerboseAudit("---------------");
            
            reslist = "Airborne,Contact,Droplet,Enhanced Contact";
            SetIndIfResultContains(14, "", "ISOLATION_STATUS", "", "", reslist, SearchDepth.SearchPullRange);
            reslist = "Protective,Standard Burn Precautions";
            SetIndIfResultContains(14, "", "PRECAUT_OTHER", "", "", reslist, SearchDepth.SearchPullRange);
        }

        private void CheckAssessment(int count, string desc)
        {
            if (_inds[18].is_checked) return;          //skip if highest already checked
            if (count == 0) return;                    //skip if none
            
            // This should work the same as the original code:
            switch (FreqForCount(_pat.los_hours, count)) {
                case Frequencies.Q30M:
                    SetInd(18, desc);
                    break;
                case Frequencies.Q1H:
                    SetInd(17, desc);
                    break;
                case Frequencies.Q2H:
                    SetInd(16, desc);
                    break;
                case Frequencies.Q4H:
                    SetInd(15, desc);
                    break;
                default:
                    break;
            }

        }

        private void Check_15_16_17_18()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("15. Assessment q4h");
            Program.VerboseAudit("16. Assessment q2h");
            Program.VerboseAudit("17. Assessment q1h");
            Program.VerboseAudit("18. Assessment q30min");
            Program.VerboseAudit("---------------");
            
            CountAssessments(30);    // always do q30 buckets--but use different freq tables for icu vs non-icu.

        }


        // Count number of buckets; similar assessments in the same bucket count as one
        //
        private void CountAssessments(int bucket_size)
        {
            int ct;
            string codelist, reslist;
            List<int> buckets;

            SetBucketSize(bucket_size);

            buckets = new List<int>();
            codelist = "PULSE,HR,BPSNON,BPSINV,TEMP,RESP,MEWS_SCORE,PEWS_SCORE";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "VS=" + ct);
            if (_inds[18].is_checked) return;

            buckets = new List<int>();
            codelist = "SPO2,ETCO2,FIO2,O2_LITER_FLOW";
            AddBuckets(buckets, "", codelist, "", "");
            codelist = "O2DEVICE";
            reslist = "Aerosol mask,CPAP,Face tent,Flow Inflation bag,HIgh Flow Nasal Cannula,Mechanical Ventilation";
            reslist += ",Nasal Cannula,Non-invasive Positive Pressure Ventilation,Non-Invasive Ventilation";
            reslist += ",Non-Rebreather,Oxyhood,OxyMask,OxyTrach,RAM Cannula,Simple Mask,Trach Mask,Venti Mask,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);
            codelist = "BS_BILAT,RESP_PATTERN,RESP_SUCTION,PAIN_BREATHING,ASSMNT_CREPITUS";
            AddBuckets(buckets, "", codelist, "", "");
            codelist = "SPUTUM_OBTAINED";
            AddBuckets(buckets, "", codelist, "", "suction");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Respiratory=" + ct);
            if (_inds[18].is_checked) return;

            buckets = new List<int>();
            codelist = "CARDIAC_RHYTHM,CARDIAC_REGULARITY,CVP1,CO,PAP,SVO2,IABPTRIGGER,IABPFREQ,VADFLOW_IMPELLA,VAD_DEVICE";
            codelist += ",ATRIALPRESS_LEFT,ATRIALPRESS_RIGHT,ATRIALPRESS_RIGHT2,PACEMAKER_MODE,PACEMAKER_PERCENT,ABI_RIGHT,ABI_LEFT,FETAL_HR,HEARTOUNDS_PEDS";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Cardiac=" + ct);
            if (_inds[18].is_checked) return;

            buckets = new List<int>();
            codelist = "IN_PAIN,NPASS_PAINSCORE";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Pain=" + ct);
            if (_inds[18].is_checked) return;

            buckets = new List<int>();
            codelist = "ORIENTATION,COGNITION,SPEECH,CAM_SCORE,GCSSCORE,PUPILSIZE_LEFT,PUPILSIZE_RIGHT";
            codelist += ",ICP-1,ICP-2,ICPMON_1,ICPMON_2,PBTO2,BT,CIWA_SCORE,ASSMNT_POSTSEIZURE,CIWA_SCORE";
            codelist += ",ASSMNT_POSTSEIZURE,RSO2_CEREBRAL,RSO2_SOMATIC,TRAINOFFOUR_LOCATION,LUMBARDRAIN_MMHG,LUMBARDRAIN_CMH2O";
            codelist += ",AEEG_PATTERNLEFT,AEEG_PATTERNRIGHT,SCORE_NIHSS";

            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Neuro=" + ct);
            if (_inds[18].is_checked) return;

            buckets = new List<int>();
            codelist = "BOWEL_SOUNDS,ABD_INSPECTION,ABD_TENDERNESS,ABD_GIRTH";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "GI Assess=" + ct);
            if (_inds[18].is_checked) return;

            buckets = new List<int>();
            codelist = "VOIDING_PATTERN,BLADDER_VOLUME,BLADDER_PRESSURE,CRRT_REMOVAL,GENITALIA_MALE,GENITALIA_FEMALE,CATHURINEOUT";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "GU Assess=" + ct);
            if (_inds[18].is_checked) return;

            buckets = new List<int>();
            codelist = "SITECOND606409,SITECOND601706,SITECOND601706,SITECOND605019,SITECOND609310";
            codelist += ",SITEASSMNT604407,SITECOND608710,OSTOMY_STOMAASSMNT,SITECOND604707,SITECOND603608";
            codelist += ",SITECOND600203,SITECOND609411,SITECOND603809,SITECOND600147,SITECOND600147,SITECOND605909";
            codelist += ",SITECOND600147,SITECOND602006,SITEASSMNT604407,SITEASSMNT604407,SITECOND606207";
            codelist += ",SITECOND608006,UROSTOMY_STOMAASSMNT,SITECOND600258,SITECOND600258,SITECOND600258";
            codelist += ",SITECOND600258,SITECOND600258,SITECOND600258,BURNASSMNT_APPEAR,BURNASSMNT_GRAFT";
            codelist += ",BURNASSMNT_DONORSITE,WOUNDASSMNT_FISTULA,WOUNDASSMNT_PERIFIST,WOUNDASSMNT_COLOR";
            codelist += ",WOUNDASSMNT606911,WOUNDASSMNT606506,WOUNDASSMNT606509,WOUNDASSMNT606512,WOUNDASSMNT606515";
            codelist += ",WOUNDASSMNT606542,WOUNDASSMNT606551,WOUNDASSMNT607307,WOUNDASSMNT607218,WOUNDASSMNT607111";
            codelist += ",WOUNDASSMNT606310,WOUNDASSMNT606810,WOUNDASSMNT607012,SITEASSMNT_CIRCUM,SITEASSMNT_UMBCORD";
            codelist += ",PRESSURE_ULCERS,MUCOUS_MEMBRANES,BRUISES,RASH_LOCATION,PETECHIAE_LOCATION,REDNESS_STATUS,SITECOND604812";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Wound =" + ct);
            if (_inds[18].is_checked) return;


            buckets = new List<int>();
            codelist = "SEDSCOREADULT,SEDSCORERASS,SEDSCOREPEDS,NPASS_SEDATIONSCORE,EDEMA,FALLRISK_SCOREADULT";
            codelist += ",FALLRISK_SCOREPEDS,HYPOGLYCEMIC_EVENT,APNEA_BRDYCRDIA,APNEA_BRDYCRDIA_DUR,FONTANELS";
            codelist += ",NEONATE_ACTIVITY,NEONATE_MUSCLETONE,NEONATE_SUTURE,ABD_PROTRUSION,NEONATE_SKINSCORE,COOLING_BLANKET";
            codelist += ",BOTTLE_BREATHING,BOTTLE_SWALLOW,NEONATE_NASSCORE,BLOOD_GLUCOSE";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Misc Assess=" + ct);

        }

        //=====================================================================
        // Group like assessments into buckets of time
        // Each bucket counts as one assessment in the frequency count

        private void SetBucketSize(int minutes)
        {
            _bucket_size = minutes;
        }

        //Add to the list of bucket numbers (redundant buckets are fine)
        private void AddBuckets(List<int> bucket_list, string cat, string code_list, string desc, string field)
        {
            AddBuckets(bucket_list, cat, code_list, desc, field, "");
        }
        private void AddBuckets(List<int> bucket_list, string cat, string code_list, string desc, string field, string result_list)
        {
            // Assign (virtual) bucket numbers to each assessment.  
            // Every x minutes after that is a new bucket.  Bucket zero = pull_start time.

            // get the chart items for the assessments
            var query = StartNewQuery();
            query = AndItemFilter(query, cat, code_list, desc, field, result_list);

            // This step is needed for those who want to count the # within a bucket, like Shands #24.
            // This will have no effect on those that count buckets.
            var query2 = (from item in query select new { item.EVENT_DATETIME }).Distinct();

            // figure out what buckets the events belong to
            var query3 = from item in query2
                         select new {
                            bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size)
                         };
            // Add to the list
            foreach (var item in query3) {
                bucket_list.Add(item.bucket);
            }

            // This is just for auditing: Say what we are looking for and how many were found
            if (!String.IsNullOrEmpty(result_list)) {
                // print each word and if it was found or not
                int i = CountResultContains(cat, code_list, desc, field, result_list);
            } else {
                // print how many were found
                Program.VerboseAudit(Describe(cat, code_list, desc, field, ""));
            }
        }

        private int CountBuckets(List<int> bucket_list)       // bucket list: (ha ha)
        {
            int result = bucket_list.Distinct().Count();
            if (result > 0) Program.VerboseAudit(result + " unique");
            return result;
        }


        private void Check_19()
        {
            string desclist = "";
            string codelist = "";
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("19. Vascular Access Site Mgt q1 Hour");
            Program.VerboseAudit("---------------");

            if (_inds[19].is_checked) return;

            if (_pat.age < 5)
            {
                SetIndIfResultContains(19, "", "MEDFILE", "intravenous;", "", "");
                if (_inds[19].is_checked) return;
                desclist = "Catheter: Arterial,Catheter: Left Atrial,Catheter: Right Atrial,Catheter: Central Venous";
                desclist += ",Catheter: Epidural,Catheter: Impella,Catheter: Intra-aortic Balloon Pump";
                desclist += ",Catheter: Intraosseous,Catheter: Intrathecal,Catheter: Intravascular Cooling";
                desclist += ",Catheter: Introducer,Catheter: Midline Venous,Catheter: Nerve Block";
                desclist += ",Catheter: Peripheral Venous,Catheter: Pulmonary Artery,Catheter: Sheath";
                desclist += ",Catheter: Umbilical,Catheter: Vascular Dialysis,Hemodialysis: Arteriovenous Access";
                desclist += ",Port: Central,Port: Hepatic Artery Infusion,Port: Peritoneal";
                desclist += ",Pump: Intravenous,Single Lumen Infusion Catheter";
                LookForCatheterPlacement(19, desclist, "Line Placement for child<=4 yrs: ");
            }
            //get CODEs where desclist and code like PLACEMENT_DT&nnnn since admission
            // look for code='REMOVE_DT&nnnn' .if found then cancel

//Trigger #19 if these values found:
//•	DAILY TPN   OR
//•	DAILY PEDS TPN  OR
//•	Extravasation AND CONTINUOUS OR
//•	Extravasation AND HighHazardous 

//Also trigger #19 if:
//•	The value “Extravasation” is found plus the administration frequency of “Extravasation” drug(s) equals majority of classification period


            SetIndIfResultContains(19, "", "MEDFILE", "intravenous;", "Daily TPN", "");
            SetIndIfResultContains(19, "", "MEDFILE", "intravenous;", "Daily PEDS TPN", "");
            SetIndIfResultContains(19, "", "MEDFILE", ALL_ITEMS_PREFIX + "intravenous;,extravasation", "Continuous", "");
            SetIndIfResultContains(19, "", "MEDFILE", ALL_ITEMS_PREFIX + "intravenous;,extravasation,Hazardous Drug High", "", "");
            SetIndIfResultContains(19, "", "MEDFILE", ALL_ITEMS_PREFIX + "intravenous;,extravasation,Hazardous - High Risk", "", "");

            codelist = "SITECOND600457,SITECOND600477,SITECOND600311,SITECOND600171,SITECOND602608,SITECOND600720";
            codelist += ",SITECOND600311,SITECOND608215,SITECOND602307,DRESSCOND610313,SITECOND600407,SITECOND600507,SITECOND605707";
            int ct = ReturnQ1HrCount(codelist,"");

            if (ct >= _pat.los_hours / 2.0)  // check that q1 for half the los
            {
                SetInd(19,"Site Condition count = " + ct);
            }

        }


        private void LookForCatheterPlacement(int ind, string desclist, string outcomment)
        { //look for placement without removal
            string icode="";
            string pcode="";
            int found = 0;

            Program.VerboseAudit("Checking for LDA placement for child...");
            var pquery = StartNewQuery(SearchDepth.SearchSinceAdmission);
            pquery = AndItemFilter(pquery, "", "PLACEMENT_DT,PLACEDBY", desclist, "", "");
            int pcount = pquery.Count();
            foreach (var item in pquery)
            {// Code will be PLACEMENT_DT&nnnn; look for corresponding REMOVE_DT&nnnn
                icode = item.CODE; 
                found = icode.IndexOf("&");
                pcode = icode.Substring(found+1);
                Program.VerboseAudit("...Found "+icode+": "+item.DESCRIPTION);
                Program.DebugTrace("icode=" + icode, icode);
                Program.DebugTrace("found="+found.ToString(), found);
                Program.DebugTrace("pcode="+pcode, pcode);
                var rquery = StartNewQuery(SearchDepth.SearchSinceAdmission);
                rquery = AndItemFilter(rquery, "", "REMOVE_DT&"+pcode, "", "", "");
                int count = rquery.Count();
                if (count == 0)
                {
                    SetInd(ind, outcomment + item.DESCRIPTION);
                }
                else
                {
                    Program.VerboseAudit("...Removal Found for " + icode + ": " + item.DESCRIPTION);
                }
            }
            if (pcount == 0)
            {
                Program.VerboseAudit("...No LDA placements found.");
            }
        }


        private void Check_20()
        {
            int num_meds = 0;
            DateTime medtime = DateTime.MinValue;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("20. Medication Activity >= 20 minutes");
            Program.VerboseAudit("---------------");

            //Program.DebugTrace("check precaut other", "");
            SetIndIfResultContains(20, "", "PRECAUT_OTHER", "", "", "Hazardous Drug: High,Radiation");
            //Program.DebugTrace("check plctra", "");
            //SetIndIfResultContains(20, "", "PLCTRANSFSTS", "", "", "Started");
            SetIndIfResultContains(20, "", "BLOOD_ADMIN_VOLUME", "", "", "");
            SetIndIfResultContains(20, "", "BLOOD_ADMIN_REACTION", "", "", "");
            SetIndIfResultContains(20, "", "BLOOD_TRANSFUSE_STATUS", "", "", "Started");

            //Program.DebugTrace("check med file extrav", "");
            //SetIndIfResultContains(20, "", "MEDFILE", "extravasation", "", "");
            //Program.DebugTrace("check med file cat 1","");
            SetIndIfResultContains(20, "1", "MEDFILE", "intravenous;", "", "");
            SetIndIfResultContains(20, "", "MEDFILE", "Hazardous Drug High", "", "", SearchDepth.Search12Hrs);
            SetIndIfResultContains(20, "", "MEDFILE", "Hazardous - High Risk", "", "", SearchDepth.Search12Hrs);
            if (_inds[20].is_checked) return;
            num_meds = GetMaxConcurrentMeds(out medtime);
            if (num_meds >= 8)
                SetInd(20, "Found " + num_meds + " starting at " + medtime.ToString());

            //if (GetResult("", "ENTERAL_INTAKE", "", "", out found_what))
            //{
            //    if (found_what.IsNumeric())
            //    {
            //        if (found_what.ToInteger() > 0)
            //        {
            //            SetInd(20, "ENTERAL_INTAKE =" + found_what);
            //        }
            //    }
            //}

        }

        int GetMaxConcurrentMeds(out DateTime medtime)
        {
            int ct = 0;
            int maxct = 0;
            DateTime currevdt = DateTime.MinValue;
            medtime = DateTime.MinValue;

            //Trigger INP 20 if count of meds* administered at same time (defined as within 10 minutes 
            //of each other) is 8 or more. *Meds are not to be included in this count if route is CRRT, 
            //flush, intravenous, intraarterial, intraperitoneal, intravitreal, epidural, peripheral nerve, 
            //does not apply, or other.

            var query = StartNewQuery();
            query = AndItemFilter(query, "", "MEDFILE", "", "", "");
            query = query.Where(e => !e.DESCRIPTION.Contains("CRRT"));
            query = query.Where(e => !e.DESCRIPTION.Contains("flush"));
            query = query.Where(e => !e.DESCRIPTION.Contains("intravenous"));
            query = query.Where(e => !e.DESCRIPTION.Contains("intraarterial"));
            query = query.Where(e => !e.DESCRIPTION.Contains("intraperitoneal"));
            query = query.Where(e => !e.DESCRIPTION.Contains("intravitreal"));
            query = query.Where(e => !e.DESCRIPTION.Contains("epidural"));
            query = query.Where(e => !e.DESCRIPTION.Contains("peripheral nerve"));
            query = query.Where(e => !e.DESCRIPTION.Contains("does not apply"));
            query = query.Where(e => !e.DESCRIPTION.Contains("other"));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            if (query.Count() == 0) return maxct;

            foreach (var ev in query)
            {
                if (currevdt < ev.EVENT_DATETIME)
                {
                    currevdt = ev.EVENT_DATETIME;

                    var query2 = StartNewQuery();
                    query2 = AndItemFilter(query2, "", "MEDFILE", "", "", "");
                    query2 = query2.Where(e => !e.DESCRIPTION.Contains("CRRT"));
                    query2 = query2.Where(e => !e.DESCRIPTION.Contains("flush"));
                    query2 = query2.Where(e => !e.DESCRIPTION.Contains("intravenous"));
                    query2 = query2.Where(e => !e.DESCRIPTION.Contains("intraarterial"));
                    query2 = query2.Where(e => !e.DESCRIPTION.Contains("intraperitoneal"));
                    query2 = query2.Where(e => !e.DESCRIPTION.Contains("intravitreal"));
                    query2 = query2.Where(e => !e.DESCRIPTION.Contains("epidural"));
                    query2 = query2.Where(e => !e.DESCRIPTION.Contains("peripheral nerve"));
                    query2 = query2.Where(e => !e.DESCRIPTION.Contains("does not apply"));
                    query2 = query2.Where(e => !e.DESCRIPTION.Contains("other"));
                    query2 = query2.Where(e => e.EVENT_DATETIME >= currevdt);
                    query2 = query2.Where(e => e.EVENT_DATETIME <= currevdt.AddMinutes(10));
                    ct = query2.Count();
                    if (ct > maxct)
                    {
                        maxct = ct;
                        medtime = currevdt;
                    }
                }
            }
            return maxct;
        }

        private void Check_21_22()
        {
            string codelist;
            string reslist;
            
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("21. Wound/Injury Mgmt");
            Program.VerboseAudit("22. Wound/Injury Mgmt >= 30 Minutes");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(22, "", "LDAW_TYPE", "", "", "Integra");
            SetIndIfResultContains(22, "", "ACUITY_WOUNDMINS", "", "", "Yes");
            if (_inds[22].is_checked) return;
            codelist="MUCOSITISEXAM";
            SetIndIfResultContains(21, "", codelist, "", "", "3,4");
            codelist="MUCOUS_MEMBRANES";
            SetIndIfResultContains(21, "", codelist, "", "", "Bleeding,Confluent ulcerations,Necrosis,Patchy ulcerations");
            codelist="CBI";
            SetIndIfResultContains(21, "", codelist, "", "", "Yes");

            SetIndIfResultContains(21, "", "GI_COMPLAINT", "", "", "Rectal bleeding");
            reslist = "Black,Coffee ground,Red - bright,Red - dark";
            SetIndIfResultContains(21, "", "EMESIS_APPEAR", "", "", reslist);

codelist = "ASSMNT_SKIN,SITEASSMNT_HALOPIN";
codelist += ",SITEASSMNT_FIXWRBDY,SITEASSMNT_FIXUPRBD,ASSMNT_CASTSPICA,ASSMNT_CAST";
codelist += ",SITEASSMNT_CIRCUM,SITEASSMNT_UMBCORD";
codelist += ",DRESSCHNG600460,DRESSCHNG600613,DRESSCHNG600480,DRESSCHNG600314";
codelist += ",DRESSCHNG600174,DRESSCHNG602610,DRESSCHNG312924,DRESSCHNG601231,DRESSCHNG602711";
codelist += ",DRESSCHNG601112,DRESSCHNG605514,DRESSCHNG606608,DRESSCHNG601315,DRESSCHNG605816";
codelist += ",DRESSCHNG608210,DRESSCHNG602310,SITEASSMNT606707,DRESSCHNG600409,SITECOND600507";
codelist += ",DRESSCHNG605709,DISPOSABLESCHNG,SITECOND606409,DRESSCHNG606411,SITECOND601706";
codelist += ",DRESSCHNG601708,SITECOND602107,DRESSTYPE602108,SITECOND605019,DRESSTYPE605020";
codelist += ",SITECOND609310,DRESSCHNG609312,SITEASSMNT604407,FECALPOUCHCHNG,SITECOND608710";
codelist += ",DRESSCHNG608712,OSTOMY_STOMAASSMNT,OSTOMY_SKINASSMNT,OSTOMY_STOMACARE,OSTOMY_POUCHCHNG";
codelist += ",SITECOND604707,DRESSCHNG604709,SITECOND603608,DRESSCHNG603610,SITECOND600203,DRESSCHNG600243";
codelist += ",SITECOND609411,DRESSCHNG609413,SITECOND603809,DRESSCHNG603811,SITECOND600147,TUBECARE600244";
codelist += ",DRESSCHNG600150,TUBECARE600249,SITECOND605909,DRESSCHNG605912,TUBECARE600248,SITECOND602006";
codelist += ",DRESSCHNG602008,SITECOND606207,DRESSCHNG606211,SITECOND608006,DRESSCHNG608010,UROSTOMY_STOMAASSMNT";
codelist += ",UROSTOMY_SKINASSMNT,UROSTOMY_STOMACARE,UROSTOMY_POUCHCHNG,SITECOND600258,BURNASSMNT_APPEAR";
codelist += ",BURNASSMNT_GRAFT,BURNASSMNT_DONORSITE,DRESSCHNG604021,WOUNDDRESS604020,WOUNDASSMNT_FISTULA";
codelist += ",WOUNDASSMNT_PERIFIST,COLLECTPOUCHCHNG,WOUNDASSMNT_COLOR,WOUNDASSMNT_TEMP,WOUNDASSMNT_CAPILL";
codelist += ",WOUNDASSMNT_FEEDING,WOUNDDRESS606023,DRESSCHNG606024,WOUNDASSMNT606910,WOUNDASSMNT606911";
codelist += ",WOUNDASSMNT606912,WOUNDDRESS606919,DRESSCHNG606920,WOUNDASSMNT606506,WOUNDDRESS606533";
codelist += ",DRESSCHNG606534,WOUNDASSMNT606509,WOUNDDRESS606535,DRESSCHNG606536,WOUNDDRESS606548";
codelist += ",DRESSCHNG606549,WOUNDASSMNT606512,WOUNDDRESS606557,DRESSCHNG606558,WOUNDASSMNT607307";
codelist += ",WOUNDDRESS607313,DRESSCHNG607314,WOUNDASSMNT607217,WOUNDASSMNT607218,WOUNDASSMNT607219";
codelist += ",WOUNDDRESS607228,DRESSCHNG607229,WOUNDASSMNT607110,WOUNDASSMNT607111,WOUNDASSMNT607112";
codelist += ",WOUNDDRESS607120,DRESSCHNG607121,WOUNDASSMNT606310,WOUNDASSMNT606311,WOUNDDRESS606316";
codelist += ",DRESSCHNG606317,WOUNDASSMNT606809,WOUNDASSMNT606810,WOUNDASSMNT606811,DRESSCHNG606820";
codelist += ",WOUNDASSMNT607011,WOUNDASSMNT607012,WOUNDASSMNT607013,WOUNDDRESS607022,DRESSCHNG607023";
codelist += ",SITECOND604812,AIRWAYSITECARE";
            SetIndIfResultContains(21, "", codelist, "", "", "");
        }

        private void CheckNICUSkinIntegrity()
        {
            string codelist = "NEWBORNSKININTEGRITY";
            string reslist = "Abrasion, elastic turgor, melanocytic nevus, stork bite, Blister," +
                "Electrode mark, Excoriation, Forceps mark, Hemorrhoids, Infiltration, Laceration," +
                "Newborn Rash, Petechia, Rash, Redness, Swelling, Tear, Other (comment), Weeping";
            // Note: check since admission
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = AndItemFilter(query,"",codelist,"","",reslist);
            query = AndResultNotInList(query, "intact");
            if (query.Count() > 0) {
                string found_what = Describe(query, "", codelist, "", "", reslist);
                SetInd(21, "Found (without intact): " + found_what);
            }
        }

        //private void CheckSkinIntegrity()
        //{
        //    string codelist = "SKININTEGRITY";
        //    string reslist = "Abrasion, Burn, Denuded, Electrode mark, Erythema, Excoriation, Fragile," +
        //        "Hematoma, Indurated, Laceration, Lesion, Macerated, Petechia, Rash, Redness, Tear, Weeping";
        //    // Note: check since admission
        //    var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
        //    query = AndItemFilter(query,"",codelist,"","",reslist);
        //    query = AndResultNotInList(query, "intact");
        //    if (query.Count() > 0) {
        //        string found_what = Describe(query, "", codelist, "", "", reslist);
        //        SetInd(21, "Found (without intact): " + found_what);
        //    }
        //}

        //private void CheckDressing()
        //{
        //    string codelist = "DRSGINTERVENTION";
        //    string desclist =
        //        "Single Lumen, Double Lumen, Triple Lumen, Quadruple Lumen" +      // LDA%lumen
        //        "chamber port, LDA art line, LDA hemodialysis cath," +
        //        "LDA intraosseous line, LDA introducer, LDA neuraxial catheter, LDA pa catheter," +
        //        "LDA paravertebral catheter, LDA perineural catheter, LDA neuraxial catheter," +
        //        "LDA SH IP, LDA sub q catheter, LDA transthoracic line, LDA umbilical artery cath," +
        //        "OB LDA epidural catheter";
        //    // Note: check since admission
        //    var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
        //    query = AndItemFilter(query, "", codelist, desclist, "", "");
        //    if (query.Count() > 0) {
        //        string found_what = Describe(query, "", codelist, desclist, "", "");
        //        SetInd(21, found_what);
        //    }
        //}

        // Use this if you can total the education time
        //private void CheckEducation(int total)
        //{
        //    if (_inds[23].is_checked) return;             //skip if already checked

        //    if (total >= 60) {
        //        SetInd(23, "education >= 60 min");
        //    }
        //}

        private void Check_23()
            {
            int mins = 0;
            int m;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("23. Healthcare Mgmt Education >= 1 Hour");
            Program.VerboseAudit("---------------");

            m = GetTotalValue("", "LCDIRECTCAREMINS", "", "", "");
            {
                Program.VerboseAudit("LCDIRECTCAREMINS minutes = " + m);
            }
            mins += m;

            m = GetTotalValue("", "MINS_PT", "", "", "");
            {
                Program.VerboseAudit("MINS_PT minutes = " + m);
            }
            mins += m;
            m = GetTotalValue("", "MINS_OT", "", "", "");
            {
                Program.VerboseAudit("MINS_OT minutes = " + m);
            }
            mins += m;
            m = GetTotalValue("", "MINS_ST", "", "", "");
            {
                Program.VerboseAudit("MINS_ST minutes = " + m);
            }
            mins += m;
            m = GetTotalValue("", "ACUITY_DSCHGEDUMINS", "", "", "");
            {
                Program.VerboseAudit("Discharge Educ mins = " + m);
            }
            mins += m;

            if (mins >= 60)
                SetInd(23, "Total mins = " + mins);

            //SetIndIfResultContains(23, "", "LCDIRECTCAREMINS", "", "", "");
            //if (_inds[23].is_checked) return;
            //if (GetResult("", "MINS_PT", "", "", out found_what))
            //{
            //    var arr = found_what.Split(' ');
            //    if (arr[0].IsNumeric()) {
            //        if (arr[0].ToInteger() >= 10) {
            //            SetInd(23, "MINS_PT =" + found_what);
            //        }
            //    }
            //}
            //if (_inds[23].is_checked) return;
            //if (GetResult("", "MINS_OT", "", "", out found_what))
            //{
            //    var arr = found_what.Split(' ');
            //    if (arr[0].IsNumeric())
            //    {
            //        if (arr[0].ToInteger() >= 10)
            //        {
            //            SetInd(23, "MINS_OT =" + found_what);
            //        }
            //    }
            //}
            //if (_inds[23].is_checked) return;
            //if (GetResult("", "MINS_ST", "", "", out found_what))
            //{
            //    var arr = found_what.Split(' ');
            //    if (arr[0].IsNumeric())
            //    {
            //        if (arr[0].ToInteger() >= 10)
            //        {
            //            SetInd(23, "MINS_ST =" + found_what);
            //        }
            //    }
            //}

        }

        private void Check_24()
        {
            string s;
            bool rr24 = false;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("24. 1 to 1 Physiological Interv. >= 2 Hours");
            Program.VerboseAudit("---------------");

            ind24_via_vs = false;
            ind24_via_rr = false;

//"*Indicator frequency is based on frequency of documentation (q 30min, q 1hr, q 2hr, q 4hr) for 
            //majority of classification period.

//Requires a complete ""set"" of vital signs (VS) at the frequency intervals. 
            // A ""set"" of VS = Heart Rate + Blood Pressure + Respirations. 
//VS Set = [Pulse (176) or HR (177)] + [BP (178) or BP Invasive (179)] + [Resp (181)]


//"
//Trigger INP 24 if more than 9 "sets of vital signs" in a 2 hour period. 
            //See row ID 175 for rule on what  constitutes a "set of vital signs".  

//PULSE    A
//HR       A
//BPSNON   B
//BPSINV   B
//RESP     C
            rr24 = false;
            SetIndIfResultContains(24, "", "RRDISPOSTION", "", "", "Code called,Transferred to ICU,Transferred to IMC",SearchDepth.Search12Hrs);
            SetIndIfResultContains(24, "", "RESUSCITATEVENT", "", "", "Yes", SearchDepth.Search12Hrs);
            SetIndIfResultContains(24, "", "TPA_ADMIN", "", "", "Yes - at UW", SearchDepth.Search12Hrs);
            rr24 = (_inds[24].is_checked);
            ind24_via_rr = rr24;

            if (_inds[24].is_checked) return;

// Change for 5/5:  Need to add location=specific sensitivity to isICU
// for RapidResponse, non-ICU, ICU as these code paths indicate below.
            if (!IsICU(_pat.unit_name)) DoRapidResponse();

            if (!IsICU(_pat.unit_name)) return;
        //make a flag that ind 24 was triggered by VS, and inhibit indicator in non-icu
        // because this must be happening due to prev location in ICU.

            SetBucketSize(60);                                  // set one hour buckets
            //===A===
            string codelist = "PULSE,HR";
            var bucketsA = new List<int>();                      // list of bucket numbers (for each match)
            AddBuckets(bucketsA, "", codelist, "", "", "");
            var queryA = bucketsA.GroupBy(x => x)                 // group by the hour (x is an int)
                               .Select(g => new { Hour = g.Key, Count = g.Count() })
                               .OrderBy(e => e.Hour);
            var arrA = queryA.ToArray();

            s = "";
            foreach (var e in arrA)
            {
                s += ",(" + e.Hour + "," + e.Count + ")";
            }
            if (s.Left(1) == ",") s = s.Substring(1);
            Program.VerboseAudit("VS hourly counts PULSE or HR: " + s);
            //===A===
            //===B===
            codelist = "BPSNON,BPSINV";
            var bucketsB = new List<int>();                      // list of bucket numbers (for each match)
            AddBuckets(bucketsB, "", codelist, "", "", "");
            var queryB = bucketsB.GroupBy(x => x)                 // group by the hour (x is an int)
                               .Select(g => new { Hour = g.Key, Count = g.Count() })
                               .OrderBy(e => e.Hour);
            var arrB = queryB.ToArray();

            s = "";
            foreach (var e in arrB)
            {
                s += ",(" + e.Hour + "," + e.Count + ")";
            }
            if (s.Left(1) == ",") s = s.Substring(1);
            Program.VerboseAudit("VS hourly counts BP: " + s);
            //===B===
            //===C===
            codelist = "RESP";
            var bucketsC = new List<int>();                      // list of bucket numbers (for each match)
            AddBuckets(bucketsC, "", codelist, "", "", "");
            var queryC = bucketsC.GroupBy(x => x)                 // group by the hour (x is an int)
                               .Select(g => new { Hour = g.Key, Count = g.Count() })
                               .OrderBy(e => e.Hour);
            var arrC = queryC.ToArray();

            s = "";
            foreach (var e in arrC)
            {
                s += ",(" + e.Hour + "," + e.Count + ")";
            }
            if (s.Left(1) == ",") s = s.Substring(1);
            Program.VerboseAudit("VS hourly counts RESP: " + s);
            //===C===

            for (int i = 0; (i < arrA.GetUpperBound(0)); i++)
            {
                // Are these two hours in a row? (beware missing hours)
                if (arrA[i].Hour == arrA[i + 1].Hour - 1)
                {
                    // Add these two consecutive hours
                    if (arrA[i].Count + arrA[i + 1].Count >= 9)
                    {
                        for (int j = 0; (j < arrB.GetUpperBound(0)); j++)
                        {
                            if ((arrB[j].Hour == arrA[i].Hour) && (arrB[j].Hour == arrB[j + 1].Hour - 1))
                            {
                                if (arrB[j].Count + arrB[j + 1].Count >= 9)
                                {
                                    for (int k = 0; (k < arrC.GetUpperBound(0)); k++)
                                    {
                                        if ((arrC[k].Hour == arrA[i].Hour) && (arrC[k].Hour == arrC[k + 1].Hour - 1))
                                        {
                                            if (arrC[k].Count + arrC[k + 1].Count >= 9)
                                            {
                                                SetInd(24, "At least 9 sets of VS found within 2 hours");
                                                ind24_via_vs = true;
                                                return;
                                            }
                                        }
                                    }

                                }
                            }
                        }

                    }
                }
            }
        }

        private bool IsICU(string uname)
        {
            switch (uname)
            {
                case "Burn ICU":
                case "TLC":
                case "F4M5":
                case "F8/4":
                case "PICU":
                case "NICU":
                case "B4/5":
                    return true;
                default:
                    return false;
            }
        }

        private void DoRapidResponse()
        {
            const string DATETIME_FORMAT = "yyyyMMddHHmm";
            string arr_result = "";
            string dep_result = "";
            DateTime arr_evdt = DateTime.MinValue;
            DateTime dep_evdt = DateTime.MinValue;
            //look for latest rrarrivaltime, look for latest rrdeparttime
            //make the time = DateONLY(event_datetime) + result
            //   result is a string of 8 chars:  17300000
            //and then if the difference between these 2 times is > 4 hours, then ignore this.   (arrival should be less than depart)
            //else if the difference is >= 2 hours, then mark ind #24.
            var rquery = StartNewQuery(SearchDepth.Search12Hrs);
            rquery = AndItemFilter(rquery, "", "RRARRIVALTIME", "", "", "");
            rquery = rquery.OrderByDescending(e => e.EVENT_DATETIME);
            if (rquery.Count() > 0)
                {
                arr_result = PFSUtility.DBToString(rquery.First().RESULT);
                arr_evdt = PFSUtility.DBToDateTime(rquery.First().EVENT_DATETIME);
                Program.VerboseAudit("arr_result--" + arr_result);
                Program.VerboseAudit("arr_evdt----" + arr_evdt.ToString());
                }
            else
                return;

            rquery = StartNewQuery(SearchDepth.SearchSinceAdmission);
            rquery = AndItemFilter(rquery, "", "RRDEPARTTIME", "", "", "");
            rquery = rquery.Where(e => e.EVENT_DATETIME >= arr_evdt);
            rquery = rquery.OrderByDescending(e => e.EVENT_DATETIME);
            if (rquery.Count() > 0)
            {
                dep_result = PFSUtility.DBToString(rquery.First().RESULT);
                dep_evdt = PFSUtility.DBToDateTime(rquery.First().EVENT_DATETIME);
                Program.VerboseAudit("dep_result--" + dep_result);
                Program.VerboseAudit("dep_evdt----" + dep_evdt.ToString());
            }
            else
                return;

            //now we have both arrival and depart

            //datetime of arrival
            string dstr_arr = arr_evdt.ToString("yyyyMMdd");
            string tstr_arr = arr_result.Substring(0, 4);
            Program.VerboseAudit("dstr arr  --" + dstr_arr);
            Program.VerboseAudit("tstr_arr----" + tstr_arr);
            DateTime dt_arr = PFSUtility.ISOToDateTime(dstr_arr + tstr_arr);

            //datetime of depart
            string dstr_dep = dep_evdt.ToString("yyyyMMdd");
            string tstr_dep = dep_result.Substring(0, 4);
            Program.VerboseAudit("dstr dep  --" + dstr_dep);
            Program.VerboseAudit("tstr_dep----" + tstr_dep);
            DateTime dt_dep = PFSUtility.ISOToDateTime(dstr_dep + tstr_dep);

            System.TimeSpan diffResult = dt_dep - dt_arr;
            if (diffResult.Hours >= 2)
            {
                SetInd(24, "Rapid Response Length: " + diffResult.Hours +  " hours. Arrival=" + dt_arr.ToString(DATETIME_FORMAT) + " Depart=" + dt_dep.ToString(DATETIME_FORMAT));
                ind24_via_rr = true;
            }

        }



        private void Check_TxArea(int unitid, string serv, string room)
        {
            string return_tx = "";
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("Tx Area");
            Program.VerboseAudit("---------------");

//B6/6	BMT [1000] 	BMT
//        PCH [1080]	Palliative Care
//        NOT BMT or PCH	Oncology 
//D6/4 	EPI [1096]	Video Monitoring
//        NOT EPI	General Care

            _txarea = "";
            _pat.unit_name = GetUnitName(unitid);
            _pat.service = serv;
            Program.VerboseAudit("unit name=" + _pat.unit_name);
            Program.VerboseAudit("service=" + _pat.service);
            Program.VerboseAudit("room=" + room);
            if (_pat.unit_name.Equals("TAC")) 
            {
                if (room.Contains("4"))
                    _txarea = "TAC4";
                else if (room.Contains("5"))
                    _txarea = "TAC5";
                return;
            }
            if (_pat.unit_name.Equals("B6/6") || _pat.unit_name.Equals("D6/4"))
            {
                if (_pat.unit_name.Equals("B6/6"))
                {
                    if (_pat.service.Equals("BMT"))
                    {
                        _txarea = "BMT";
                    }
                    else
                    {
                        if (_pat.service.Equals("PCH"))
                        {
                            _txarea = "Palliative Care";
                        }
                        else
                        {
                            _txarea = "Oncology";
                        }
                    }
                }
                else
                {
                    if (_pat.unit_name.Equals("D6/4"))
                    {
                        if (_pat.service.Equals("EPI"))
                        {
                            _txarea = "Video Monitoring";
                        }
                        else
                        {
                            _txarea = "General Care";
                        }
                    }
                }

                if (_txarea.Equals(""))
                {
                    Program.VerboseAudit("Unit+Service does not map to a TxArea");
                    return;
                }
            }
            string codelist = "TXAREA";
            // Note: check since admission
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = AndItemFilter(query, "", codelist, "", "", "");
            //Program.VerboseAudit("TxArea records without orderby=" + query.Count().ToString());
            query = query.Where(e => e.RESULT.Trim() != "");
//            query = query.Where(e => (e.EVENT_DATETIME >= _pat.pull_start) && (e.EVENT_DATETIME < _pat.pull_finish));
            query = query.OrderByDescending(e => e.EVENT_DATETIME);
            //Program.VerboseAudit("TxArea records with orderby=" + query.Count().ToString());
            if (query.Count() > 0)
            {
                return_tx = PFSUtility.DBToString(query.First().RESULT.ToUpper());
                Program.VerboseAudit("Accommodation code=" + return_tx.ToString());
                string found_what = Describe(query, "", codelist, "", "", "");
            }
            //Program.VerboseAudit("_pat.unit_name=]" + _pat.unit_name.ToString() + "[");

            foreach (var t in _txarr)
            {
                //Program.VerboseAudit("t.uname=]" + t.uname.ToString() + "[");
                if (t.uname.Equals(_pat.unit_name.ToString()))
                {
                    foreach (var a in t.txRec)
                    {
                        //Program.VerboseAudit("a.txcode=]" + a.txcode.ToString() + "[");
                        if (return_tx.Equals(a.txcode.ToString()))
                        {
                            Program.VerboseAudit("TxArea=" + a.txname);
                            _txarea = a.txname;
                        }
                    }
                }
            }
            if (_txarea.Equals(""))
            {
                Program.VerboseAudit("Accommodation Code does not map to a TxArea");
                return;
            }
        }

        private void AtLeastOneADL()
        {
            Program.Audit("---------------");
            if (!(_inds[1].is_checked || _inds[2].is_checked || _inds[3].is_checked || _inds[4].is_checked)) {
                // Make "#2 ADL - Assist" the default.  (90% of patients)
                SetInd(1, "Defaulting to Self care due to lack of documentation.");
            }
        }

        private void HighestIndicatorInEachGroupWins()
        {
            int i, g;
            bool highest_is_on;
            string ind_list;

            Program.Audit("Select highest indicator in each group");
            
            g = 0;
            highest_is_on = false;
            // Go from bottom up - highest (lowest) indicator in each group wins
            for (i = MAX_INDS; (i >= 1); i--) {
                if (_inds[i].radio_group > 0) {
                    if (_inds[i].radio_group != g) {
                        //this is a new group
                        g = _inds[i].radio_group;
                        highest_is_on = _inds[i].is_checked;
                    } else {
                        //same group
                        if (highest_is_on) {
                            _inds[i].is_checked = false;             //uncheck a lower number
                        } else {
                            highest_is_on = _inds[i].is_checked;     //save this one
                        }
                    }
                }
            } // next i
            
            //Echo the indicators for an Audit (no classification will be saved)
            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++) {
                if (_inds[i].is_checked) ind_list += "," + i;
            } // next i

            Program.Audit("Final list = " + ind_list.Substring(1));
            Program.Audit("---------------");
        }


        private void CheckProcs()
        {
            //CheckProc_1();
            CheckProc_2NEW();
            //CheckProc_2(EXACT_MATCH_PREFIX + "ATTND_SAFETYPRESENT", "Yes", "No");
            //CheckProc_2(EXACT_MATCH_PREFIX + "ATTND_SAFETY", "Start,Continue,Weaning", "Discontinue");
            CheckProc_3to11();
            //CheckProc_3();
            //CheckProc_4();
            //CheckProc_5();
            //CheckProc_6();
            //CheckProc_7();
            //CheckProc_9();
            //CheckProc_10();
            //CheckProc_11();
        }

        private void CheckProc_1()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P1. 1-1 safety observation by RN");
            //Program.VerboseAudit("---------------");
        }

//        private void CheckProc_2(string codelist, string resstartlist, string resstop)
//        {
//            DateTime first_start_dt = DateTime.MinValue;
//            DateTime start_dt = DateTime.MinValue;
//            DateTime prev_start_dt = DateTime.MinValue;
//            DateTime prev_stop_dt = DateTime.MinValue;
//            List<proc_data> ci_list;
            
//            Program.VerboseAudit("---------------");
//            Program.VerboseAudit("A2. 1-1 safety observation by non-RN");
//            Program.VerboseAudit("---------------");
//            // No/Discontinue at the same time of a Yes/Continue is always equal to No (No trumps Yes)

//            // functions needed:  
//            //   1.  when is the first Start/Continue <= a given time
//            //   2.  when is the first No/DC >= or <= a given time

//            // No 1000, No 1200, yes 1400, yes 1500, yes 1800, No 2200 => in this case the start is at 1400 
////1/6/15 0:00	ATTND_SAFETYPRESENT	No
////1/6/15 8:00	ATTND_SAFETY	Continue
////1/6/15 8:00	ATTND_SAFETYPRESENT	No
////1/6/15 16:00	ATTND_SAFETY	Continue
////1/6/15 16:00	ATTND_SAFETYPRESENT	No
////1/6/15 20:00	ATTND_SAFETYPRESENT	Yes
////1/7/15 2:00	ATTND_SAFETYPRESENT	Yes
////1/7/15 9:00	ATTND_SAFETY	Continue
////1/7/15 9:00	ATTND_SAFETYPRESENT	No
////12/13/14 16:00	ATTND_SAFETY	Start

//            // look in neighborhood of g_pull_start for a start
//            string reslist = "Start,Continue,Weaning,Limited hours"; 
//            var query = StartNewQuery(SearchDepth.SearchPullRange);
//            //query = query.Where(e => (e.CODE == "ATTND_SAFETYPRESENT" && e.RESULT == "Yes") ||
//            //                         (e.CODE == "ATTND_SAFETY" && reslist.Contains(e.RESULT)));
//            query = query.Where(e => (e.CODE == "ATTND_SAFETYPRESENT".ToLower() && e.RESULT == "Yes".ToLower()) ||
//                                     (e.CODE == "ATTND_SAFETY".ToLower() && reslist.ToLower().Contains(e.RESULT)));
//            query = query.OrderBy(e => e.EVENT_DATETIME);
//            int count = query.Count();
//            //Program.DebugTrace("g_pull_start=" + Program.g_pull_start);
//            //Program.DebugTrace("g_pull_finish=" + Program.g_pull_finish);
//            //Program.DebugTrace("pat_start=" + _pat.pull_start);
//            //Program.DebugTrace("pat_finish=" + _pat.pull_finish);
//            //Program.DebugTrace("count=" + count);
//            if (count > 0)
//            {
//                //if patient is discharged, then need to add a Stop at discharge time.
//                // add stop to 2 arrays: 
//                //        _chart_items_during_pull_period
//                //        _chart_items_since_admission
//                AddStopIfDischarged();

//                bool done = false;
//                bool is_first = true; // flag for searching for earlier start before first record
//                DateTime stop_dt = DateTime.MinValue;
//                foreach (var item in query)
//                {
//                    Program.Audit("itemA: " + item.EVENT_DATETIME+ " - "+item.CODE + "/" + item.RESULT);
//                    if (!done && item.EVENT_DATETIME > stop_dt)
//                    {
//                        // is there a No at this item time?
//                        var squery = StartNewQuery(SearchDepth.SearchPullRange);
//                        squery = squery.Where(e => (e.CODE == "ATTND_SAFETYPRESENT".ToLower() && e.RESULT == "No".ToLower()) ||
//                                                   (e.CODE == "ATTND_SAFETY".ToLower() && (e.RESULT == "Discontinue".ToLower())));
//                        squery = squery.Where(e => e.EVENT_DATETIME == item.EVENT_DATETIME);
//                        int scount = squery.Count();
//                        if (scount == 0) // no stop at this time.  item.evdt is true start time.
//                        {
//                            done = true;
//                            start_dt = item.EVENT_DATETIME;
//                            // data:  Yes 0800, No 0800, Yes 0900, Yes 1000, No 1300, No 1400, Yes 1800, Yes 2000, No 0400, Yes 0600
//                            if (is_first) //ONLY if there was no No found before this item!
//                            { //then check if there is an earlier time to continue from.
//                                is_first = false;
//                                first_start_dt = item.EVENT_DATETIME;
//                                prev_start_dt = GetTimeOfPrevStart(start_dt);
//                                if (prev_start_dt > DateTime.MinValue && prev_start_dt < start_dt)
//                                {
//                                    start_dt = prev_start_dt;
//                                    if (start_dt < Program.g_pull_start)
//                                        start_dt = Program.g_pull_start;
//                                }
//                            }
//                            //when is the stop time after this start_dt?
//                            stop_dt = GetTimeOfNextStop(start_dt); // 1300 --should return pull_finish if no stop time in range

//                            string str_time = Program.g_pull_finish.ToString("HHmm");
//                            string str_date = Program.g_pull_finish.ToString("yyyyMMdd");
//                            DateTime sod_dt;
//                            if (str_time == "1900")
//                            {
//                                sod_dt = PFSUtility.ISOToDateTime(str_date + "0700");
//                                if (start_dt < sod_dt) start_dt = sod_dt;
//                            }
//                            MaybeAddSitter(start_dt, stop_dt);
//                            Program.Audit("PSA a: " + start_dt + " to " + stop_dt);
//                            if (stop_dt < Program.g_pull_finish) done = false;
//                        }
//                        else //a No was found with this item time. so this item is not a valid start.
//                        {
//                            is_first = false;  // the next start will be a valid start -- wont have to look for an earlier start
//                        }
//                    }
//                }
//            }

//            // Now check for the stop before first_start_dt: Its Start will be <= g_pull_start if it exists. 
//            // There could only be at most 1 since it's before the first start. 
//            // (otherwise there would have been a start)
//            var fquery = StartNewQuery(SearchDepth.SearchPullRange);
//            fquery = fquery.Where(e => (e.CODE == "ATTND_SAFETYPRESENT".ToLower() && e.RESULT == "No".ToLower()) ||
//                                       (e.CODE == "ATTND_SAFETY".ToLower() && (e.RESULT == "Discontinue".ToLower())));
//            fquery = fquery.Where(e => e.EVENT_DATETIME < first_start_dt);
//            int fcount = fquery.Count();
//            if (fcount > 0)
//            { // then there is a stop in the pull range before the first start.  
//                // find out when the start is for this stop.
//                DateTime first_stop_dt = PFSUtility.DBToDateTime(fquery.Min(x => x.EVENT_DATETIME));
//                if (first_stop_dt < first_start_dt)
//                {
//                    prev_start_dt = GetTimeOfPrevStart(first_stop_dt);
//                    prev_stop_dt = GetTimeOfPrevStop(first_stop_dt);
//                    if (prev_start_dt >= prev_stop_dt)
//                    {
//                        if (prev_start_dt < Program.g_pull_start) prev_start_dt = Program.g_pull_start;
//                        MaybeAddSitter(prev_start_dt, first_stop_dt);
//                        Program.VerboseAudit("PSA b: " + prev_start_dt + " to " + first_stop_dt);
//                    }
//                }

//            }


//            if (_procs.Count() > 0)
//            {
//                foreach (proc_data p in _procs)
//                {
//                    Program.Audit("check proc list: " + p.procedure_number + "::" + p.start + " - " + p.finish);
//                }
//            }
//        }

        private void CheckProc_2NEW()
        {
            DateTime first_start_dt = DateTime.MinValue;
            DateTime start_dt = DateTime.MinValue;
            DateTime prev_start_dt = DateTime.MinValue;
            DateTime prev_stop_dt = DateTime.MinValue;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A2. 1-1 safety observation by non-RN");
            Program.VerboseAudit("---------------");
            // No/Discontinue at the same time of a Yes/Continue is always equal to No (No trumps Yes)

            // functions needed:  
            //   1.  when is the first Start/Continue <= a given time
            //   2.  when is the first No/DC >= or <= a given time

            // No 1000, No 1200, yes 1400, yes 1500, yes 1800, No 2200 => in this case the start is at 1400 
            //1/6/15 0:00	ATTND_SAFETYPRESENT	No
            //1/6/15 8:00	ATTND_SAFETY	Continue
            //1/6/15 8:00	ATTND_SAFETYPRESENT	No
            //1/6/15 16:00	ATTND_SAFETY	Continue
            //1/6/15 16:00	ATTND_SAFETYPRESENT	No
            //1/6/15 20:00	ATTND_SAFETYPRESENT	Yes
            //1/7/15 2:00	ATTND_SAFETYPRESENT	Yes
            //1/7/15 9:00	ATTND_SAFETY	Continue
            //1/7/15 9:00	ATTND_SAFETYPRESENT	No
            //12/13/14 16:00	ATTND_SAFETY	Start

            // look in neighborhood of g_pull_start for a start
            var query = StartNewQuery(SearchDepth.Search24Hrs);
            query = query.Where(e => (e.CODE == "MONITORING_TYPE".ToLower() && (e.RESULT.Contains("patient safety attendant") || e.RESULT.Contains("patient safety observer"))));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            int count = query.Count();
            if (count > 0)
            {
                //if patient is discharged, then need to add a Stop at discharge time.
                // add stop to 2 arrays: 
                //        _chart_items_during_pull_period
                //        _chart_items_since_admission
                AddStopIfDischarged();

                bool is_first = true; // flag for searching for earlier start before first record
                DateTime stop_dt = DateTime.MinValue;
                foreach (var item in query)
                {
                    if (AttendPresent(item.EVENT_DATETIME))
                    {
                        Program.Audit("item: " + item.EVENT_DATETIME + " - " + item.CODE + "/" + item.RESULT);
                        //if (!done && item.EVENT_DATETIME > stop_dt)
                        Program.Audit("  stop: " + stop_dt);
                        if (item.EVENT_DATETIME >= stop_dt)
                            {
                            // is there a No at this item time?
                            //var squery = StartNewQuery(SearchDepth.SearchPullRange);
                            //squery = squery.Where(e => (e.CODE == "ATTND_SAFETYPRESENT".ToLower() && e.RESULT == "No".ToLower()) || (e.CODE == "MONITORING_TYPE".ToLower() && e.RESULT.Contains("video monitoring")));
                            //squery = squery.Where(e => e.EVENT_DATETIME == item.EVENT_DATETIME);
                            //int scount = squery.Count();
                            //if (scount == 0) // no stop at this time.  item.evdt is true start time.
                            if (!StopFoundAtThisTime(item.EVENT_DATETIME)) // no stop at this time.  item.evdt is true start time.
                                {
                                start_dt = item.EVENT_DATETIME;
                                Program.Audit("start: " + start_dt);

                                // data:  Yes 0800, No 0800, Yes 0900, Yes 1000, No 1300, No 1400, Yes 1800, Yes 2000, No 0400, Yes 0600
                                if (is_first) //ONLY if there was no No found before this item!
                                { //then check if there is an earlier time to continue from.
                                    is_first = false;
                                    first_start_dt = item.EVENT_DATETIME;
                                    prev_start_dt = GetTimeOfPrevStartNEW(start_dt);
                                    if (prev_start_dt > DateTime.MinValue && prev_start_dt < start_dt)
                                    {
                                        start_dt = prev_start_dt;
                                        if (start_dt < Program.g_pull_start)
                                            start_dt = Program.g_pull_start;
                                    }
                                }
                                //when is the stop time after this start_dt?
                                stop_dt = GetTimeOfNextStopNEW(start_dt); // 1300 --should return pull_finish if no stop time in range
                                Program.Audit("stop: " + stop_dt);

                                string str_time = Program.g_pull_finish.ToString("HHmm");
                                string str_date = Program.g_pull_finish.ToString("yyyyMMdd");
                                string str_dateyesterday = Program.g_pull_finish.AddDays(-1).ToString("yyyyMMdd");
                                DateTime st_dt;
                                st_dt = PFSUtility.ISOToDateTime(str_dateyesterday + "1715");
                                if (str_time == "0515")
                                    st_dt = PFSUtility.ISOToDateTime(str_dateyesterday + "1715");
                                else if (str_time == "0700")
                                    st_dt = PFSUtility.ISOToDateTime(str_date + "0515");
                                else if (str_time == "1715")
                                    st_dt = PFSUtility.ISOToDateTime(str_date + "0700");
                                if ((stop_dt > st_dt) && (start_dt < st_dt))
                                {
                                    start_dt = st_dt;
                                    MaybeAddSitter(start_dt, stop_dt);
                                    Program.Audit("PSA a: " + start_dt + " to " + stop_dt);
                                }
                                else if ((start_dt >= st_dt) && (stop_dt > start_dt))
                                {
                                    MaybeAddSitter(start_dt, stop_dt);
                                    Program.Audit("PSA b: " + start_dt + " to " + stop_dt);
                                }
                                //if (stop_dt < Program.g_pull_finish) done = false;
                            }
                            else //a No was found with this item time. so this item is not a valid start.
                            {
                                Program.Audit("There is a stop condition at the same time as this item.  No activity will be created.");
                                is_first = false;  // the next start will be a valid start -- wont have to look for an earlier start
                            }
                        }
                    } //if attend present
                }
            }

            // Now check for the stop before first_start_dt: Its Start will be <= g_pull_start if it exists. 
            // There could only be at most 1 since it's before the first start. 
            // (otherwise there would have been a start)
            var fquery = StartNewQuery(SearchDepth.Search24Hrs);
            fquery = fquery.Where(e => (e.CODE == "ATTND_SAFETYPRESENT".ToLower() && e.RESULT == "No".ToLower()));
            fquery = fquery.Where(e => e.EVENT_DATETIME < first_start_dt);
            int fcount = fquery.Count();
            if (fcount > 0)
            { // then there is a stop in the pull range before the first start.  
                // find out when the start is for this stop.
                DateTime first_stop_dt = PFSUtility.DBToDateTime(fquery.Min(x => x.EVENT_DATETIME));
                if (first_stop_dt < first_start_dt)
                {
                    prev_start_dt = GetTimeOfPrevStartNEW(first_stop_dt);
                    prev_stop_dt = GetTimeOfPrevStopNEW(first_stop_dt);
                    if (prev_start_dt >= prev_stop_dt)
                    {
                        if (prev_start_dt < Program.g_pull_start) prev_start_dt = Program.g_pull_start;
                        MaybeAddSitter(prev_start_dt, first_stop_dt);
                        Program.VerboseAudit("PSA b: " + prev_start_dt + " to " + first_stop_dt);
                    }
                }

            }


            if (_procs.Count() > 0)
            {
                foreach (proc_data p in _procs)
                {
                    Program.Audit("check proc list: " + p.procedure_number + "::" + p.start + " - " + p.finish);
                }
            }
        }

        private bool AttendPresent(DateTime evdt)
        {
            string reslist = "Start,Continue,Weaning,Limited hours";
            var query = StartNewQuery(SearchDepth.Search24Hrs);
            query = query.Where(e => e.EVENT_DATETIME == evdt);
            query = query.Where(e => (e.CODE == "ATTND_SAFETY".ToLower() && reslist.ToLower().Contains(e.RESULT.ToLower())) 
                || (e.CODE == "ATTND_SAFETYPRESENT".ToLower() && e.RESULT == "Yes".ToLower()));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            int count = query.Count();
            Program.Audit("AttendPresent count at: " + evdt.ToString() + " = " + count);
            return (count > 0);
        }

        private bool StopFoundAtThisTime(DateTime evdt)
        {
            var squery = StartNewQuery(SearchDepth.Search24Hrs);
            squery = squery.Where(e => (e.CODE == "ATTND_SAFETYPRESENT".ToLower() || e.CODE == "MONITORING_TYPE".ToLower()) );
            squery = squery.Where(e => e.EVENT_DATETIME == evdt);
            squery = squery.OrderByDescending(e => e.TIMESTAMP);
            if (squery.Count() > 0)
            {
                Program.Audit("sfatt: count= " + squery.Count());
                var r = squery.First(); //this record has the latest timestamp
                DateTime ts = r.TIMESTAMP;
                //Now tighten the query to be only for this timestamp
                squery = squery.Where(e => e.TIMESTAMP >= ts.AddMinutes(-5) && e.TIMESTAMP <= ts.AddMinutes(5));
                if (squery.Count() == 1)
                {
                    Program.Audit("sfatt A: count= " + squery.Count());
                    r = squery.First();
                    if (r.CODE == "ATTND_SAFETYPRESENT".ToLower())
                        return (r.RESULT == "no");
                    if (r.CODE == "MONITORING_TYPE".ToLower())
                        return (r.RESULT.ToLower() == "video monitoring");
                    return false;
                }
                else // count==2 and it must be one of each code: specific evdt,specific timestamp were given
                {
                    Program.Audit("sfatt B: count= " + squery.Count());
                    bool att_no = false;
                    bool vid = false;
                    foreach (var item in squery)
                    {
                        if (item.CODE == "ATTND_SAFETYPRESENT".ToLower())
                            att_no = (r.RESULT == "no");
                        if (item.CODE == "MONITORING_TYPE".ToLower())
                            vid = (r.RESULT.ToLower() == "video monitoring");
                    }
                    return (att_no || vid); // if only 1 of these is true then consider it a stop.
                }
            }
            return false;
        }

        private void AddStopIfDischarged()
        {
            DateTime dctime;
            CHART_ITEM ci_item;
            List<CHART_ITEM> ci_list;
            if (IsPatientDischarged(_pat.encounter_id, out dctime))
            {
                ci_item = new CHART_ITEM();
                ci_item.ENCOUNTER_ID = _pat.encounter_id;
                ci_item.EVENT_DATETIME = dctime;
                ci_item.CODE = "ATTND_SAFETYPRESENT";
                ci_item.RESULT = "No";

                ci_list = new List<CHART_ITEM>();
                ci_list = _chart_items_during_pull_period.ToList();
                ci_list.Add(ci_item);
                _chart_items_during_pull_period = ci_list.ToArray();

                ci_list = new List<CHART_ITEM>();
                ci_list = _chart_items_since_admission.ToList();
                ci_list.Add(ci_item);
                _chart_items_since_admission = ci_list.ToArray();
            }
        }

        private bool IsPatientDischarged(int encid, out DateTime dctime)
        {
            bool is_dc = true;
            dctime = DateTime.MinValue;
            // detect discharge by looking at enc_locs with no datetime_out=null
            var db = PFSUtility.NewPfsDataContext();
            var query = from encloc in db.ENCOUNTER_LOCATIONs
                        where (encloc.ENCOUNTER_ID == encid)
                        select encloc;
            foreach (var el in query)
            {
                if (el.DATETIME_OUT == null) is_dc = false;
            }
            if (is_dc)
            {
                dctime = PFSUtility.DBToDateTime(query.Max(x => x.DATETIME_OUT));
            }
            return is_dc;
        }

        private DateTime GetTimeOfNextStop(DateTime given_time)
        {
            DateTime stop_dt = DateTime.MinValue;

            var xquery = StartNewQuery(SearchDepth.SearchSinceAdmission);
            xquery = xquery.Where(e => (e.CODE == "ATTND_SAFETYPRESENT".ToLower() && e.RESULT == "No".ToLower()) ||
                                       (e.CODE == "ATTND_SAFETY".ToLower() && (e.RESULT == "Discontinue".ToLower())));
            xquery = xquery.Where(e => e.EVENT_DATETIME > given_time);
            int xcount = xquery.Count();
            if (xcount > 0)
                stop_dt = PFSUtility.DBToDateTime(xquery.Min(x => x.EVENT_DATETIME));
            else
                stop_dt = Program.g_pull_finish;

            // Now we have the next stop time
            return stop_dt;
        }
        private DateTime GetTimeOfNextStopNEW(DateTime given_time)
        {
            DateTime stop_dt = DateTime.MinValue;

            var xquery = StartNewQuery(SearchDepth.SearchSinceAdmission);
            xquery = xquery.Where(e => (e.CODE == "ATTND_SAFETYPRESENT".ToLower() && e.RESULT == "No".ToLower()) || (e.CODE == "MONITORING_TYPE".ToLower() && e.RESULT.ToLower() == "video monitoring"));
            xquery = xquery.Where(e => e.EVENT_DATETIME > given_time);
            int xcount = xquery.Count();
            if (xcount > 0)
                stop_dt = PFSUtility.DBToDateTime(xquery.Min(x => x.EVENT_DATETIME));
            else
                stop_dt = Program.g_pull_finish;

            // Now we have the next stop time
            return stop_dt;
        }

        private DateTime GetTimeOfPrevStop(DateTime given_time)
        {
            DateTime stop_dt = DateTime.MinValue;

            var xquery = StartNewQuery(SearchDepth.SearchSinceAdmission);
            xquery = xquery.Where(e => (e.CODE == "ATTND_SAFETYPRESENT".ToLower() && e.RESULT == "No".ToLower()) ||
                                       (e.CODE == "ATTND_SAFETY".ToLower() && (e.RESULT == "Discontinue".ToLower())));
            xquery = xquery.Where(e => e.EVENT_DATETIME < given_time);
            int xcount = xquery.Count();
            if (xcount > 0)
                stop_dt = PFSUtility.DBToDateTime(xquery.Max(x => x.EVENT_DATETIME));
            else
                stop_dt = Program.g_pull_start;

            // Now we have the next stop time
            return stop_dt;
        }
        private DateTime GetTimeOfPrevStopNEW(DateTime given_time)
        {
            DateTime stop_dt = DateTime.MinValue;

            var xquery = StartNewQuery(SearchDepth.SearchSinceAdmission);
            xquery = xquery.Where(e => (e.CODE == "ATTND_SAFETYPRESENT".ToLower() && e.RESULT == "No".ToLower()) || (e.CODE == "MONITORING_TYPE".ToLower() && e.RESULT.ToLower() == "video monitoring"));
            xquery = xquery.Where(e => e.EVENT_DATETIME < given_time);
            int xcount = xquery.Count();
            if (xcount > 0)
                stop_dt = PFSUtility.DBToDateTime(xquery.Max(x => x.EVENT_DATETIME));
            else
                stop_dt = Program.g_pull_start;

            // Now we have the next stop time
            return stop_dt;
        }
        private DateTime GetTimeOfPrevStart(DateTime given_time)
        { // get time of first Continue or Discontinue less than given time
            DateTime last_stop_dt = DateTime.MinValue;
            DateTime earliest_start_dt = DateTime.MinValue;

            //first, determine the last Stop in order to establ a lower bound for Starts
            // No 1000, No 1200, yes 1400, yes 1500, yes 1800 => in this case the start is at 1400 not 1800
            var xquery = StartNewQuery(SearchDepth.SearchSinceAdmission);
            xquery = xquery.Where(e => (e.CODE == "ATTND_SAFETYPRESENT".ToLower() && e.RESULT == "No".ToLower()) ||
                                       (e.CODE == "ATTND_SAFETY".ToLower() && (e.RESULT == "Discontinue".ToLower())));
            xquery = xquery.Where(e => e.EVENT_DATETIME < given_time);
            int xcount = xquery.Count();
            if (xcount > 0) last_stop_dt = PFSUtility.DBToDateTime(xquery.Max(x => x.EVENT_DATETIME));  //stop found at 1200

            var zquery = StartNewQuery(SearchDepth.SearchSinceAdmission);
            zquery = zquery.Where(e => (e.CODE == "ATTND_SAFETYPRESENT".ToLower() && e.RESULT == "Yes".ToLower()) ||
                                     (e.CODE == "ATTND_SAFETY".ToLower() && (e.RESULT == "Start".ToLower() || e.RESULT == "Continue".ToLower() || e.RESULT == "Weaning".ToLower())));
            zquery = zquery.Where(e => e.EVENT_DATETIME > last_stop_dt); // if xcount==0 then last_stop=min time, so ok.
            zquery = zquery.Where(e => e.EVENT_DATETIME < given_time);
            //query = query.OrderBy(e => e.EVENT_DATETIME);
            int count = zquery.Count();
            if (count > 0) // this earliest start dt will not have a Stop that is trumping it because it is later than the latest Stop from xquery.
                earliest_start_dt = PFSUtility.DBToDateTime(zquery.Min(x => x.EVENT_DATETIME)); // start found at 1400

            //Now we have the earliest start before the given time.
            return earliest_start_dt;
        }
        private DateTime GetTimeOfPrevStartNEW(DateTime given_time)
        { // get time of first Continue or Discontinue less than given time
            DateTime last_stop_dt = DateTime.MinValue;
            DateTime earliest_start_dt = DateTime.MinValue;

            //first, determine the last Stop in order to establ a lower bound for Starts
            // No 1000, No 1200, yes 1400, yes 1500, yes 1800 => in this case the start is at 1400 not 1800
            var xquery = StartNewQuery(SearchDepth.SearchSinceAdmission);
            xquery = xquery.Where(e => (e.CODE == "ATTND_SAFETYPRESENT".ToLower() && e.RESULT == "No".ToLower()) || (e.CODE == "MONITORING_TYPE".ToLower() && e.RESULT.ToLower() == "video monitoring"));
            xquery = xquery.Where(e => e.EVENT_DATETIME < given_time);
            int xcount = xquery.Count();
            if (xcount > 0) last_stop_dt = PFSUtility.DBToDateTime(xquery.Max(x => x.EVENT_DATETIME));  //stop found at 1200

            var zquery = StartNewQuery(SearchDepth.SearchSinceAdmission);
            zquery = zquery.Where(e => (e.CODE == "MONITORING_TYPE".ToLower() && (e.RESULT.Contains("patient safety attendant") || e.RESULT.Contains("patient safety observer"))));
            zquery = zquery.Where(e => e.EVENT_DATETIME > last_stop_dt); // if xcount==0 then last_stop=min time, so ok.
            zquery = zquery.Where(e => e.EVENT_DATETIME < given_time);
            //query = query.OrderBy(e => e.EVENT_DATETIME);
            int count = zquery.Count();
            if (count > 0) // this earliest start dt will not have a Stop that is trumping it because it is later than the latest Stop from xquery.
            {
                earliest_start_dt = PFSUtility.DBToDateTime(zquery.Min(x => x.EVENT_DATETIME)); // start found at 1400
                if (!AttendPresent(earliest_start_dt)) earliest_start_dt = DateTime.MinValue;
            }

            //Now we have the earliest start before the given time.
            return earliest_start_dt;
        }

        private void MaybeAddSitter(DateTime startdt, DateTime enddt)
        {
            if (ProcExistsInDB(2, startdt, enddt)) {
                Program.Audit("Activity 2: already exists");
            } else 
            {
                //if (!QueuedProcOverlaps(startdt, enddt))
                //{
                    var proc = new proc_data();
                    proc.procedure_number = 2;
                    proc.start = startdt;
                    proc.finish = enddt;
                    _procs.Add(proc);
                    Program.Audit("Activity 2: Found Sitter between " + startdt + " and " + enddt);
                //}
            }
        }

        private bool QueuedProcOverlaps(DateTime startdt, DateTime enddt)
        {
            bool overlap = false;
            Program.Audit("overlap: " + startdt + " - " + enddt);

            proc_data[] pary = _procs.ToArray();
            for (int i = 0; i <= pary.GetUpperBound(0); i++) 
            {
                Program.Audit("var p: " + pary[i].start + " - " + pary[i].finish);
                if (pary[i].procedure_number == 2) 
                {
                    //overlap if   p.start between stardt and enddt
                    //             p.finish between startdt and enddt
                    //             startdt >= p.start and enddt <= p.finish
                    if ((pary[i].start >= startdt) && (pary[i].start < enddt) ||
                        (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
                        (startdt >= pary[i].start) && (enddt <= pary[i].finish))
                    {
                        overlap = true;
                        Program.Audit("overlap = true");
                    }

                    if ((pary[i].start >= startdt) && (pary[i].start < enddt) && (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
                        (pary[i].start < startdt) && (pary[i].finish > startdt) && (pary[i].finish < enddt)
                        )
                    {
                        // the proc in the list is completely contained in startdt/endt pair
                        // change this proc to be the startdt/enddt
                        // overlap is still true, but the list item will be updated with the encompassing times
                        Program.Audit("Activity 2: Sitter " + startdt + " - " + enddt + " will trump " + pary[i].start + " - " + pary[i].finish);
                        pary[i].start = startdt;
                        pary[i].finish = enddt;
                    }
                }
            }
            _procs = pary.ToList();
            return overlap;
        }


        private void UpdateSitterInDB(int src, DateTime evdt, string itemcode)
        {
            string sql;

            if (itemcode.Left(2) == EXACT_MATCH_PREFIX)
            {
                itemcode = itemcode.Substring(2);
            }
            
            // update all ATTND_SAFETY leq evdt to have order_control='X'
            //Program.Audit("src = "+src);
            sql = "UPDATE CHART_ITEM SET ORDER_CONTROL='X' WHERE ENCOUNTER_ID=" + _pat.encounter_id; 
            sql+= " AND CODE=" + "'" + itemcode + "'";
            sql+= " AND EVENT_DATETIME<=" + PFSUtility.SQLDateTime(evdt);
            var db = PFSUtility.NewSqlConnection();
            var cmd = new SqlCommand(sql, db);
            cmd.ExecuteNonQuery();

            foreach (var item in _chart_items_since_admission)
            {
                if ((item.CODE.ToUpper() == itemcode) && (item.EVENT_DATETIME <= evdt))
                {
                    item.ORDER_CONTROL = "X";
                }
            }
            foreach (var item in _chart_items_since_unit_arrival)
            {
                if ((item.CODE.ToUpper() == itemcode) && (item.EVENT_DATETIME <= evdt))
                {
                    item.ORDER_CONTROL = "X";
                }
            }
            foreach (var item in _chart_items_during_pull_period)
            {
                if ((item.CODE.ToUpper() == itemcode) && (item.EVENT_DATETIME <= evdt))
                {
                    item.ORDER_CONTROL = "X";
                }
            }
                     

        }

        private bool ProcExistsInDB(int pnum, DateTime startdt, DateTime enddt)
        {
            // Look for already existing procs that are longer (that contain) this proc.
            //if there are any, then don't add this smaller proc.
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && ((startdt >= proc.PROCEDURE_DATETIME) && (startdt < proc.DEPARTURE_DATETIME)
                                ||
                                (startdt < proc.PROCEDURE_DATETIME) && (enddt > proc.PROCEDURE_DATETIME))
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new {proc.PROCEDURE_EVENT_ID};
            return (query.Count() > 0);
        }

//        procs 3-11:
//PLUS1_OFFUNITRN
//PLUS1_OFFUNITNONRN
//PLUS1_PATEDURN
//PLUS1_WOUNDRN
//PLUS1_WOUNDNONRN
//PLUS1_CCRN
//PLUS1_BEDSIDERN1
//PLUS1_BEDSIDENONRN1
//PLUS1_BEDSIDERN2



        private void CheckProc_3to11()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A3. Off unit accompanied by RN");
            Program.VerboseAudit("---------------");

            DoProc(3, "PLUS1_OFFUNITRN");

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A4. Off unit accompanied by non-RN");
            Program.VerboseAudit("---------------");

            DoProc(4, "PLUS1_OFFUNITNONRN");
            
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A5. Patient/family education by RN");
            Program.VerboseAudit("---------------");

            DoProc(5, "PLUS1_PATEDURN");
            
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A6. Extensive wound management by RN");
            Program.VerboseAudit("---------------");

            DoProc(6, "PLUS1_WOUNDRN");

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A7. Extensive wound management by non-RN");
            Program.VerboseAudit("---------------");

            DoProc(7, "PLUS1_WOUNDNONRN");

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A8. Coordination of care by RN");
            Program.VerboseAudit("---------------");

            DoProc(8, "PLUS1_CCRN");

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A9 1:1 RN at bedside");
            Program.VerboseAudit("---------------");

            DoProc(9, "PLUS1_BEDSIDERN1");

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A10. 1:1 non-RN at bedside");
            Program.VerboseAudit("---------------");

            DoProc(10, "PLUS1_BEDSIDENONRN1");

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A11. 2:1 by RN at bedside");
            Program.VerboseAudit("---------------");

            DoProc(11, "PLUS1_BEDSIDERN2");

        }

        private void DoProc(int pnum, string codestr)
        {
            DateTime evdt;
            DateTime startdt;
            double mins = 0;

            var query = StartNewQuery(SearchDepth.SearchPullRange);
            query = AndItemFilter(query, "", codestr, "", "", "");
            foreach (var item in query)
            {
                mins = 60.0 * item.RESULT.ToDouble();
                evdt = item.EVENT_DATETIME;
                startdt = evdt.AddMinutes(-mins);

                if (ProcExistsInDB(pnum, startdt, evdt))
                {
                    Program.Audit("Activity " + pnum + ": already exists");
                }
                else
                {
                    if (!QueuedProcOverlaps(startdt, evdt))
                    {
                        var proc = new proc_data();
                        proc.procedure_number = pnum;
                        proc.start = startdt;
                        proc.finish = evdt;
                        _procs.Add(proc);
                        Program.Audit("Activity " + pnum + ": Found " + codestr + " between " + startdt + " and " + evdt);
                    }
                }
            
            }

        }

        private void CheckProc_4()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P4. Off unit accompanied by non-RN");
            Program.VerboseAudit("---------------");
            string found_what;
            DateTime evdt;
            DateTime startdt;
            double mins = 0;

            if (GetResultAndEVDT("", "PLUS1_OFFUNITNONRN", "", "", out found_what, out evdt))
            {
                mins = 60.0 * found_what.ToDouble();
                startdt = evdt.AddMinutes(-mins);

                if (ProcExistsInDB(4, startdt, evdt))
                {
                    Program.Audit("Procedure 4: already exists");
                }
                else
                {
                    if (!QueuedProcOverlaps(startdt, evdt))
                    {
                        var proc = new proc_data();
                        proc.procedure_number = 4;
                        proc.start = startdt;
                        proc.finish = evdt;
                        _procs.Add(proc);
                        Program.Audit("Procedure 4: Found PLUS1_OFFUNITNONRN between " + startdt + " and " + evdt);
                    }
                }

            }

            //OBX|1|NM|PLUS1_OFFUNITNONRN||3.75||||||F|||201411041200<0xd>
        }


        private void CheckProc_5()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P5. Patient/family education by RN");
            Program.VerboseAudit("---------------");
            string found_what;
            DateTime evdt;
            DateTime startdt;
            double mins = 0;

            if (GetResultAndEVDT("", "PLUS1_PATEDURN", "", "", out found_what, out evdt))
            {
                mins = 60.0 * found_what.ToDouble();
                startdt = evdt.AddMinutes(-mins);

                if (ProcExistsInDB(4, startdt, evdt))
                {
                    Program.Audit("Procedure 5: already exists");
                }
                else
                {
                    if (!QueuedProcOverlaps(startdt, evdt))
                    {
                        var proc = new proc_data();
                        proc.procedure_number = 5;
                        proc.start = startdt;
                        proc.finish = evdt;
                        _procs.Add(proc);
                        Program.Audit("Procedure 5: Found PLUS1_PATEDURN between " + startdt + " and " + evdt);
                    }
                }

            }

        }

        private void CheckProc_6()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P6. Extensive wound management by RN");
            Program.VerboseAudit("---------------");

            string found_what;
            DateTime evdt;
            DateTime startdt;
            double mins = 0;

            if (GetResultAndEVDT("", "PLUS1_WOUNDRN", "", "", out found_what, out evdt))
            {
                mins = 60.0 * found_what.ToDouble();
                startdt = evdt.AddMinutes(-mins);

                if (ProcExistsInDB(6, startdt, evdt))
                {
                    Program.Audit("Procedure 6: already exists");
                }
                else
                {
                    if (!QueuedProcOverlaps(startdt, evdt))
                    {
                        var proc = new proc_data();
                        proc.procedure_number = 6;
                        proc.start = startdt;
                        proc.finish = evdt;
                        _procs.Add(proc);
                        Program.Audit("Procedure 6: Found PLUS1_WOUNDRN between " + startdt + " and " + evdt);
                    }
                }

            }

            //OBX|1|NM|PLUS1_WOUNDRN||2.25||||||F|||201411041358<0xd>
        }

        private void CheckProc_7()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P7. Extensive wound management by non-RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_8()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P8. Coordination of care by RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_9()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P9 1:1 RN at bedside");
            Program.VerboseAudit("---------------");

            string found_what;
            DateTime evdt;
            DateTime startdt;
            double mins = 0;

            if (GetResultAndEVDT("", "PLUS1_BEDSIDERN1", "", "", out found_what, out evdt))
            {
                mins = 60.0 * found_what.ToDouble();
                startdt = evdt.AddMinutes(-mins);

                if (ProcExistsInDB(9, startdt, evdt))
                {
                    Program.Audit("Procedure 9: already exists");
                }
                else
                {
                    if (!QueuedProcOverlaps(startdt, evdt))
                    {
                        var proc = new proc_data();
                        proc.procedure_number = 9;
                        proc.start = startdt;
                        proc.finish = evdt;
                        _procs.Add(proc);
                        Program.Audit("Procedure 9: Found PLUS1_BEDSIDERN1 between " + startdt + " and " + evdt);
                    }
                }

            }

            //OBX|1|NM|PLUS1_BEDSIDERN1||1.5||||||F|||201411041400<0xd>
        }

        private void CheckProc_10()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P10. 1:1 non-RN at bedside");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_11()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P11. 2:1 by RN at bedside");
            //Program.VerboseAudit("---------------");
        }

        
        private const string DATETIME_FORMAT = "yyyyMMddHHmm";              // ISO Date/Time w/o seconds

        private void OutputClass(int elig_for_default, int unitid, DateTime intime, DateTime outtime)
        {
            string outstr, ind_list, desc, str_pull_dt;
            int i, tc_event_id;
            string unitname;

            unitname = GetUnitName(unitid);

            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         212 242 304
            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1
            //1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901 3456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
            //1       |10 5            |                |                |        |10023121779         |ALEXANDER                       |GLORIA                          |                                |0508    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289530  |           |201403030400                                                                      |NNYNNNNYYNNYNNNYNNNYYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//1       |10 5            |                |                |        |10024544908         |ARMSTRONG                       |AARON                           |                                |0535    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289531  |           |201403030400                                                                      |NYNNYNNNNNNYNNNYNNNNYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//|0547    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289532  |           |201403030400                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//|0547    |A   |201403030700|                |201403030700    |18  |C|    |3040|1440|18289532  |           |201403030700                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN

            if (Program.g_is_test)
                tc_event_id = 9999;
            else
                tc_event_id = PFSUtility.NextGID();                         //get a unique id for this class
            str_pull_dt = _pat.pull_finish.ToString(DATETIME_FORMAT);

            outstr = _pat.facilty_code.FixedWidth(8);                       //(facility code)
            outstr += "|" + unitname.FixedWidth(16);
            outstr += "|" + "".FixedWidth(16);                               //(unit code)
            outstr += "|" + _txarea.FixedWidth(16);                               //(area code)
            outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
            outstr += "|" + _pat.acct.FixedWidth(20);
            outstr += "|" + _pat.last_name.FixedWidth(32);
            outstr += "|" + _pat.first_name.FixedWidth(32);
            outstr += "|" + _pat.middle_name.FixedWidth(32);
            outstr += "|" + _pat.room.FixedWidth(8);
            outstr += "|" + _pat.bed.FixedWidth(4);
          //outstr += "|" + str_pull_dt.FixedWidth(12);                      //class datetime (could change)
          outstr += "|" + intime.ToString(DATETIME_FORMAT).FixedWidth(12);                      //class datetime (could change)
          outstr += "|" + "".FixedWidth(16);                               //(login)
          //outstr += "|" + str_pull_dt.FixedWidth(16);                      //(employee)/(pull datetime)
          outstr += "|" + intime.ToString(DATETIME_FORMAT).FixedWidth(16);                      //(employee)/(pull datetime)
          outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
            outstr += "|" + "C".FixedWidth(1);                               //record type = class
            outstr += "|" + "".FixedWidth(4);                                //(stage)
            outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
            outstr += "|" + _pat.range.ToString().FixedWidth(4);             //TC pull range
            outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
            outstr += "|";
            outstr = outstr.FixedWidth(294);
          //outstr += "|" + _pat.effective.ToString(DATETIME_FORMAT);        //IN
          outstr += "|" + intime.ToString(DATETIME_FORMAT);        //IN
          outstr = outstr.FixedWidth(346);
          outstr += "|";
          if (outtime != DateTime.MinValue)
            outstr += outtime.ToString(DATETIME_FORMAT);        //OUT
          outstr = outstr.FixedWidth(377);
            outstr += "|";

            if (elig_for_default == 1 || elig_for_default == 2)
                if (elig_for_default == 2 || EarliestLocationInPast4hrs(unitid) || ExistDefault(intime))
                { //make all is_checked = false and then mark defaults
                    Program.VerboseAudit("Patient will receive default indicators " + _pat.default_inds_str);
                    for (i = 1; (i <= MAX_INDS); i++)
                    {
                        _inds[i].is_checked = false;
                    }
                    foreach (var ind in _pat.default_inds)
                    {
                        if (ind <= _inds.GetUpperBound(0))
                        {
                            _inds[ind].is_checked = true;
                        }
                    }
                    _inds[99].is_checked = true; //Flag to mark default continuation
                }
            
            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++) 
            {
                if (_inds[i].is_checked && i < 100) 
                {
                    if (i == 24)
                    {
                        //if (IsICU(unitname) || !ind24_via_vs)
                        //    outstr += "Y";
                        //else // not icu AND ind24_via_vs
                        //    outstr += "N";
                        if (ind24_via_vs)
                        {
                            if (IsICU(unitname))
                                outstr += "Y";
                            else
                                outstr += "N";
                        }
                        else if (ind24_via_rr)
                        {
                            if (!IsICU(unitname))
                                outstr += "Y";
                            else
                                outstr += "N";
                        }
                        else
                            outstr += "Y";

                    }
                    else
                        outstr += "Y";
                    ind_list += "," + i;                
                } 
                else
                    outstr += "N";
            } // next i
            ind_list = ind_list.Substring(1);                           //strip leading comma
  
            Program.outfile.WriteLine(outstr);                          //output to transparent.txt

            Program.Audit("");
            desc = "Classified: " + ind_list;
            if (Program.g_is_test) {
                Program.Audit(desc);
            } else {
                //Save the selected indicators plus both Program.Audits in the event log; link with tc_event_id
                PFSEventLog.AddTransparentMappingEventLogEntry(
                    desc, Program.gLogUnitID, Program.gLogEncounterID,
                    tc_event_id, Program.gLogMapperVersion,
                    Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
            }
        }

        private void OutputProcs()
        {
            int i;
            string outstr, proc_list, desc;
            int tc_event_id;
            DateTime procin = DateTime.MinValue;
            DateTime procout = DateTime.MinValue;
            string unitname;
            bool proc_ok;
            bool last_proc_was_unk = false;
            bool is_first_unit = true;
            bool this_unit_has_no_previous = false;
            var plen = 0.0;
            var plen2 = 0.0;

            foreach(var proc in _procs) {

                Program.VerboseAudit("outprocloop: "+proc.start.ToString()+" - "+proc.finish.ToString());
                //SetTimesAndUnitsForProcs(); and then make a loop here to output the procs
                foreach (var ploc in procloclist)
                {
                    this_unit_has_no_previous = false;

                    if (ploc.unit_id == -1)
                        last_proc_was_unk = true;
                    else if (last_proc_was_unk)
                        this_unit_has_no_previous = true;

                    if (ploc.unit_id != -1)
                        last_proc_was_unk = false;

                    Program.VerboseAudit("outprocloop: loc=" + ploc.unit_id + ": " + ploc.in_time.ToString() + " - " + ploc.out_time.ToString());
                    if ((ploc.unit_id != -1) && (ploc.methid == 18))
                    {
                        unitname = GetUnitName(ploc.unit_id);
                        procin = DateTime.MinValue;
                        if (proc.procedure_number != 2)
                        {
                        Program.VerboseAudit("ploc.in_time=" + ploc.in_time.ToString() + "   ploc.out_time=" + ploc.out_time.ToString() + "   ploc.arr_time=" + ploc.arr_time.ToString());
                        if (proc.finish > ploc.in_time && proc.finish <= ploc.out_time) 
                        {
                            Program.VerboseAudit("activity finish time falls into this unit: " + ploc.unit_id + ": " + proc.start.ToString() + "-" + proc.finish.ToString());
                            plen = proc.finish.Subtract(proc.start).TotalMinutes;
                            //procout = ploc.in_time.AddMinutes(plen);
                            if (proc.start >= ploc.arr_time)
                            {
                                procin = proc.start;
                                procout = proc.finish;
                            }
                            else
                            {
                                Program.VerboseAudit("retrofitting activity: " + ploc.unit_id + ": " + proc.start.ToString() + "-" + proc.finish.ToString());
                                Program.VerboseAudit("             into location " + ploc.in_time.ToString() + " - " + ploc.out_time.ToString());
                                procin = ploc.arr_time;
                                procout = procin.AddMinutes(plen);
                                //if (procout > ploc.out_time) procout = ploc.out_time;
                                //plen2 = procout.Subtract(procin).TotalMinutes;
                                //if (plen2 < plen)
                                //{
                                //    Program.VerboseAudit("Activity Length Shortened to fit location: " + ploc.unit_id + ": " + procin.ToString() + "-" + procout.ToString());
                                //}
                            }
                        }
                    }
                    else //procnum==2
                    {

                        if ((proc.start >= ploc.in_time) && (proc.finish <= ploc.out_time))  // wholly within location
                        {
                            //use this ploc for the unit with proc.start, proc.finish
                            procin = proc.start;
                            procout = proc.finish;
                        }
                        else if ((proc.start < ploc.in_time) && (proc.finish >= ploc.out_time)) //straddles across location
                        {
                            // add ploc.unit, ploc.in_time, proc.finish
                            procin = ploc.in_time;
                            procout = ploc.out_time;
                        }
                        else if ((proc.start >= ploc.in_time) && (proc.start < ploc.out_time)) // starts within location
                        {
                            // add ploc.unit, proc.start, ploc.out_time
                            procin = proc.start;
                            procout = ploc.out_time;
                        }
                        else if ((proc.finish > ploc.in_time) && (proc.finish <= ploc.out_time)) // ends within location
                        {
                            // add ploc.unit, ploc.in_time, proc.finish
                            //if (proc.start < ploc.arr_time) //then retrofit the proc by starting it at ploc.in_time
                            //{
                            //    procin = ploc.arr_time;
                            //    procout = ploc.arr_time.AddMinutes((int)PFSUtility.DateDiffInMinutes(proc.start, proc.finish));
                            //    Program.VerboseAudit("Retrofitting activity to new end time of: " + procout.ToString() + " from documented end time of: " + proc.finish.ToString());
                            //}
                            //else
                            //{
                            //    procin = proc.start;
                            //    procout = proc.finish;
                            //}
                            if (is_first_unit || this_unit_has_no_previous)
                            {
                                if (ploc.loc_start <= proc.start)
                                {
                                    Program.VerboseAudit("activity starts after locationintime: " + ploc.unit_id + ": " + proc.start.ToString() + "-" + proc.finish.ToString());
                                    Program.VerboseAudit("        location start =" + ploc.loc_start.ToString());
                                    procin = proc.start;
                                    procout = proc.finish;
                                }
                                else
                                {
                                    Program.VerboseAudit("retrofitting activityA: " + ploc.unit_id + ": " + proc.start.ToString() + "-" + proc.finish.ToString());
                                    Program.VerboseAudit("             into location " + ploc.in_time.ToString() + " - " + ploc.out_time.ToString());
                                    procin = ploc.in_time;
                                    // procout = proc.finish;
                                    plen = proc.finish.Subtract(proc.start).TotalMinutes;
                                    procout = ploc.in_time.AddMinutes(plen);
                                }
                            }
                            else
                            {
                                if ((ploc.in_time <= proc.start) && (ploc.out_time >= proc.finish))
                                {
                                    Program.VerboseAudit("resuming activityB: " + ploc.unit_id + ": " + proc.start.ToString() + "-" + proc.finish.ToString());
                                    Program.VerboseAudit("      into location " + ploc.in_time.ToString() + " - " + ploc.out_time.ToString());
                                    procin = ploc.in_time;
                                    procout = proc.finish;
                                }
                            }
                        }
                    } //else procnum==2
                        // validate this procedure against existing proce
                        if (procin == DateTime.MinValue || procout == DateTime.MinValue)
                            proc_ok = false;
                        else
                            proc_ok = ValidateProc(ploc.unit_id, procin, procout);

                        if (proc_ok && procin > DateTime.MinValue)
                        {
                            if (Program.g_is_test)
                                tc_event_id = 9999;
                            else
                                tc_event_id = PFSUtility.NextGID();                         //get a unique id for this proc

                            outstr = _pat.facilty_code.FixedWidth(8);
                            outstr += "|" + unitname;                                 //10
                            outstr = outstr.FixedWidth(68);
                            outstr += "|" + _pat.acct.FixedWidth(20);                       //90
                            outstr += "|" + _pat.last_name.FixedWidth(32);
                            outstr += "|" + _pat.first_name.FixedWidth(32);
                            outstr += "|" + _pat.middle_name.FixedWidth(32);
                            outstr = outstr.FixedWidth(202);
                            outstr += "|" + procin.ToString(DATETIME_FORMAT);           //204 proc dt
                            outstr = outstr.FixedWidth(254);
                            outstr += "|P";                                                 //256 procedure type record
                            outstr += "|" + "".FixedWidth(4);                               //(stage)
                            outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);     //TC source ID
                            outstr += "|" + _pat.range.ToString().FixedWidth(4);            //TC pull range
                            outstr += "|" + tc_event_id.ToString().FixedWidth(10);          //TC event ID
                            outstr += "|";
                            outstr = outstr.FixedWidth(294);
                            outstr += "|" + procin.ToString(DATETIME_FORMAT);           //296 procdt in
                            outstr = outstr.FixedWidth(346);
                            outstr += "|" + procout.ToString(DATETIME_FORMAT);          //348 procdt out
                            outstr = outstr.FixedWidth(377);
                            outstr += "|";

                            proc_list = "";
                            for (i = 1; (i < MAX_PROCS); i++)
                            {
                                if (proc.procedure_number == i)
                                {
                                    outstr += "Y";
                                    proc_list += "," + i;
                                }
                                else
                                {
                                    outstr += "N";
                                }
                            } // next i
                            proc_list = proc_list.Substring(1);                             //strip leading comma

                            Program.outfile.WriteLine(outstr);                              //output to transparent.txt

                            desc = "Procedures: " + proc_list;
                            if (Program.g_is_test)
                            {
                                Program.Audit(desc);
                            }
                            else
                            {
                                //Save the selected procedures plus both Program.Audits in the event log; link with tc_event_id
                                //(this assumes the procedure Program.Audit is mixed in with the class Program.Audit)
                                PFSEventLog.AddTransparentMappingEventLogEntry(desc, Program.gLogUnitID, Program.gLogEncounterID,
                                    tc_event_id, Program.gLogMapperVersion,
                                    Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
                            }
                        } // proc time within loc
                    } // unit != -1
                    is_first_unit = false;

                } // next location
            } // next proc
        }

        private bool ValidateProc(int unitid, DateTime procin, DateTime procout)
        {
            // see if these is an existing overlapping proc in the db, and if there is then compare the 2 lengths
            // if the db length is smaller then delete it and return true
            // else return false;
            Program.VerboseAudit("VPprocin=" + procin.ToString());
            Program.VerboseAudit("VPprocout=" + procout.ToString());
            string sql;
            bool db_has_longer_los = false;
            int this_proc_los; //in minutes
            this_proc_los = (int)PFSUtility.DateDiffInMinutes(procin, procout);

            var dbx = PFSUtility.NewPfsDataContext();
            var query = from p in dbx.PROCEDURE_EVENTs
                        where (p.ENCOUNTER_ID == _pat.encounter_id) && (p.UNIT_ID == unitid) &&
                          ((procin <= p.PROCEDURE_DATETIME) && (p.PROCEDURE_DATETIME < procout) ||
                           (procin < p.DEPARTURE_DATETIME) && (p.DEPARTURE_DATETIME < procout))
                        select new
                        {
                            p.PROCEDURE_EVENT_ID,
                            p.PROCEDURE_DATETIME,
                            p.DEPARTURE_DATETIME,
                            p.LOS_HOURS
                        };
            foreach (var prec in query)
            {
                Program.VerboseAudit("proc event id=" + prec.PROCEDURE_EVENT_ID +
                                     " in=" + prec.PROCEDURE_DATETIME.ToString() +
                                     " out=" + prec.DEPARTURE_DATETIME.ToString() +
                                     " los=" + prec.LOS_HOURS * 60);
                if (prec.LOS_HOURS * 60 < this_proc_los)
                {
                    Program.VerboseAudit("Deleting proc event id=" + prec.PROCEDURE_EVENT_ID);
                    //DeleteProc(prec.PROCEDURE_EVENT_ID);
                    sql = "DELETE PROCEDURE_ANSWER WHERE PROCEDURE_EVENT_ID=" + prec.PROCEDURE_EVENT_ID;
                    var db = PFSUtility.NewSqlConnection();
                    var cmd = new SqlCommand(sql, db);
                    cmd.ExecuteNonQuery();
                    sql = "DELETE RPT_PROC_BY_DAY WHERE PROCEDURE_EVENT_ID=" + prec.PROCEDURE_EVENT_ID;
                    cmd = new SqlCommand(sql, db);
                    cmd.ExecuteNonQuery();
                    sql = "DELETE PROCEDURE_EVENT WHERE PROCEDURE_EVENT_ID=" + prec.PROCEDURE_EVENT_ID;
                    cmd = new SqlCommand(sql, db);
                    cmd.ExecuteNonQuery();

                }
                else
                    db_has_longer_los = true;  //at least 1 proc in db is longer; dont add given proc to db

            }
            return !db_has_longer_los;  // valid is true only if db has all shorter los
        }

        private int DeterminePtypeOfIndicators()
        {
            int i;
            var pscore = 0.0;
            int pt_type = 6;
            var indlist = new List<int>();
            string indstr = "";

            Program.VerboseAudit("Determine patient type of triggered indicators");

            for (i = 1; (i <= MAX_INDS); i++)
            {
                if (_inds[i].is_checked)
                {
                    indlist.Add(i);
                    indstr += i.ToString() + ",";
                }
            }

            var db = PFSUtility.NewPfsDataContext();
            var query_ind_def = from ind_def in db.INDICATOR_DEFINITIONs
                                where (ind_def.METHODOLOGY_ID == PFSGlobal.METH_ID_APLUS_INPATIENT2) &&
                                  indlist.Contains(ind_def.INDICATOR_NUMBER)
                                select new
                                {
                                    ind_def.WEIGHT
                                };
            foreach (var wgts in query_ind_def)
            {
                pscore += wgts.WEIGHT;
            }
            Program.VerboseAudit("indicators=" + indstr);
            Program.VerboseAudit("score=" + pscore.ToString());

            var query_ptype = from ptype in db.PATIENT_TYPEs
                              where (ptype.METHODOLOGY_ID == PFSGlobal.METH_ID_APLUS_INPATIENT2)
                              orderby ptype.PATIENT_TYPE1
                              select new
                              {
                                  ptype.PATIENT_TYPE1,
                                  ptype.POINTS_HIVAL
                              };

            foreach (var ptypes in query_ptype)
            {
                //                Program.DebugTrace("type,hival=" + ptypes.PATIENT_TYPE1.ToString() + "," + ptypes.POINTS_HIVAL.ToString(), "");
                if (pscore <= ptypes.POINTS_HIVAL)
                {
                    if (pt_type > ptypes.PATIENT_TYPE1)
                    {
                        pt_type = ptypes.PATIENT_TYPE1;
                    }
                }
            }
            Program.VerboseAudit("patient type=" + pt_type.ToString());

            return pt_type;

        }

        private bool No7amClassExists(DateTime start_dt)
        {
            var db = PFSUtility.NewPfsDataContext();
            var query = from ce in db.CLASSIFICATION_EVENTs
                        where (ce.ENCOUNTER_ID == _pat.encounter_id)
                        && (ce.CLASSIFICATION_DATETIME == start_dt)
                        select ce;
            int cnt = query.Count();

            if (cnt == 0)
            {
                Program.VerboseAudit("No 7am classification.  Will make 7am default classification");
                return true;
            }
            return false;
        }

        private bool ExistDefault(DateTime intime)
        {
            var db = PFSUtility.NewPfsDataContext();

            var query = from ce in db.CLASSIFICATION_EVENTs
                        where (ce.ENCOUNTER_ID == _pat.encounter_id)
                        && (ce.CLASSIFIED_BY_ID == -2)
                        && (ce.EFFECTIVE_DATETIME_IN == intime)
                        select ce;
            int cnt_def = query.Count();

            if (cnt_def > 0)
            {
                Program.VerboseAudit("Default classification exists at: " + intime.ToString());
            }

            return (cnt_def > 0);

        }

        //private bool ExistDefaultInPast4hrs(int unitid)
        //{
        //    DateTime cdt1 = DateTime.MinValue;
        //    DateTime cdt2 = DateTime.MinValue;
        //    int cnt_def = 0;
        //    //get max class date of last non-default class = 1
        //    //get max class date of last default = 2
        //    // if 1 >= 2 then false
        //    // else
        //    //   if 2 > 1 and date is <= 16 hrs ago then true
        //    //   else false
        //    var db = PFSUtility.NewPfsDataContext();
        //    //var query = from ce in db.CLASSIFICATION_EVENTs
        //    //            where (ce.ENCOUNTER_ID == _pat.encounter_id)
        //    //            && (ce.CLASSIFIED_BY_ID != -2)
        //    //            && (System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(ce.CLASSIFICATION_DATETIME, Program.g_pull_start) <= 960)
        //    //            select ce;
        //    //cnt_all = query.Count();
        //    //if (cnt_all > 0)
        //    //{
        //    //    cdt1 = PFSUtility.DBToDateTime(query.Max(x => x.CLASSIFICATION_DATETIME));
        //    //    Program.VerboseAudit("Last regular classification was at:"+cdt1.ToString());
        //    //}
        //    //else {
        //    //    Program.VerboseAudit("No regular classifications within the past 16 hours");
        //    //}

        //    var query = from ce in db.CLASSIFICATION_EVENTs
        //                where (ce.ENCOUNTER_ID == _pat.encounter_id)
        //                && (ce.CLASSIFIED_BY_ID == -2)
        //                && (ce.UNIT_ID == unitid)
        //                && (ce.CLASSIFICATION_DATETIME <= Program.g_pull_finish)
        //                && (System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(ce.CLASSIFICATION_DATETIME, Program.g_pull_finish) <= 240)
        //                select ce;
        //    cnt_def = query.Count();

        //    if (cnt_def > 0)
        //    {
        //        cdt2 = PFSUtility.DBToDateTime(query.Max(x => x.CLASSIFICATION_DATETIME));
        //        Program.VerboseAudit("Last default classification was at:" + cdt2.ToString());
        //    }
        //    else
        //    {
        //        Program.VerboseAudit("No default classifications within the past 4 hours");
        //    }
        //    return (cnt_def > 0);

        //}

        private bool EarliestLocationInPast4hrs(int unitid)
        {
            DateTime min_dt = DateTime.MinValue;
            //get min enc loc effdt
            var db = PFSUtility.NewPfsDataContext();

            var query = from el in db.ENCOUNTER_LOCATIONs
                        where (el.ENCOUNTER_ID == _pat.encounter_id)
                        select new
                        {
                            el.EFFECTIVE_DATETIME_IN
                        };
            min_dt = PFSUtility.DBToDateTime(query.Min(x => x.EFFECTIVE_DATETIME_IN));
            if (System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(min_dt, Program.g_pull_finish) <= 240)
            {
                Program.VerboseAudit("Earliest location time is within the past 4 hours");
                return true;
            }

            return false;

        }

        private void ResolveProcLocations()
        {
            int num_locs = 0;
            procloclist = new List<PatientLocation>();
            var locary = Program.patloclist.ToArray();
            //  where there are adjacent same-locations, mark the duplicates as remove.
            for (int i = 0; i <= locary.GetUpperBound(0); i++)
            {
                Program.VerboseAudit("ProcloclistA:" + i + ": " + locary[i].unit_name + " " + locary[i].in_time.ToString() + " - " + locary[i].out_time.ToString());
            }
            for (int i = 0; i <= locary.GetUpperBound(0); i++)
            {
                num_locs++;
                locary[i].remove = false;
                if (i >= 1)
                {
                    if ((locary[i - 1].unit_id == locary[i].unit_id))
                    {
                        locary[i].remove = true;
                    }
                }
            }
            //  need to combine them into 1 loc record.
            //  make the outdt of that record be the latest outdt
            int lastgoodidx = 0;
            int lastadj = 0;
            for (int i = 0; i <= locary.GetUpperBound(0); i++)
            {
                if (!locary[i].remove)
                {
                    lastgoodidx = i;
                    lastadj = i;
                }
                else
                {
                    if (lastadj == i - 1)
                    {
                        locary[lastgoodidx].out_time = locary[i].out_time;
                        lastadj = i;
                    }
                }
            }
            // get rid of the removes
            int numkeep = 0;
            for (int i = 0; i <= locary.GetUpperBound(0); i++)
            {
                if (!locary[i].remove)
                {
                    numkeep++;
                    locary[numkeep - 1] = locary[i]; //-1 for 0-based ary                
                }
            }
            if (numkeep < num_locs)
            {
                Array.Resize(ref locary, numkeep);
            }
            //now determine los_mins
            for (int i = 0; i <= locary.GetUpperBound(0); i++)
            {
                locary[i].los_mins = (int)PFSUtility.DateDiffInMinutes(locary[i].in_time, locary[i].out_time);
            }

            for (int i = 0; i <= locary.GetUpperBound(0); i++)
            {
                Program.VerboseAudit("ProcloclistB:" + i + ": " + locary[i].unit_name + " " + locary[i].in_time.ToString() + " - " + locary[i].out_time.ToString());
            }
            procloclist = locary.ToList();
        }


    }
}
