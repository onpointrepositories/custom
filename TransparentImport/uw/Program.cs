﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;                // for SqlConnection
using System.IO;                            // for Directory, Path, Stream
using System.Windows.Forms;                 // for Application (also add reference)
using PfsShared;                            // add a reference to Shared2 project
//
// Transparent Mapping main program.
// NOTE: Each unit must set the "use transparent" flag.
//
// sample: -efftime=0700 -effdate=yesterday -pulltime=0700 -pulldate=today -range=1440
//
// This is designed to be run as a background job, so never stop for user input -- that means no error MsgBox.
// All communication is done through log files and the event log; it is OK to print to the console.
//
// In order to work with the AcuityPlus Patient Selection and Transparent import:
//
//   * The program name must be TransparentMapping.exe, placed in AcuityPlus\bin
//   * The audit file is called TransparentAudit.log, placed in AcuityPlus\log
//   * The output file is Transparent.txt, placed in AcuityPlus\load_me
//   * Events are saved in the database event log
//
namespace TransparentMapping
{
    public class PatientLocation
    {
        public int      unit_id;
        public string unit_name;
        public int methid;
        public string room;
        public string bed;
        public string service;
        public DateTime in_time;
        public DateTime out_time;
        public int los_mins;
        public bool remove;  //flag for compacting locs
        public DateTime arr_time;
        public DateTime loc_start;
    }

    public class PatientInfo
    {
        public int      encounter_id;
        public string   last_name;
        public string   first_name;
        public string   middle_name;
        public string   acct;
        public double   age;                    // age (in years) at admission
        public string   room;
        public string   bed;
        public DateTime unit_arrival;
        public DateTime unit_departure;
        public DateTime effective;              // patient specific (may be > g_effdt)
        public DateTime pull_start;             // patient specific (may be > g_pull_start)
        public DateTime pull_finish;            // patient specific (may be < g_pull_finish)
        public int      range;                  // patient specific (may be < g_range) - minutes
        public double   los_hours;              // hours during pull
        public int      TC_source_id;           // TCP port (chart source)
        // unit info
        public string   facilty_code;           // class import facility code
        public string   unit_name;
        public int      unit_id;
        public bool     is_ED;
        public bool     is_ICU;
        public int      meth_id;
        public List<int> default_inds;          // unit admission profile indicators for recent arrivals
        public string default_inds_str;         // "1,6,8"
        public int default_ptype;
        public string service;
        public string language;
    }

    public static class Program
    {

        const string    MAPPER_VERSION = "1.00";
        const string    TRANSP_FILENAME = "Transparent.txt";    // THE output file (for class import)
        const string    TRANSP_AUDIT_LOG = "TransparentAudit.log";  // Legacy debug/audit log
        const int       CHART_ITEM_LIFE = 30;                   // days in the chart_item table
        const int       DEFAULT_RANGE = 1440;                   // 24 hrs in minutes; override with -range

        public static bool g_abort;                         // stop!
        public static bool g_debug;                         // output to console?
        public static bool g_log;                           // output to log file? (legacy feature)
        public static bool g_no_output;                     // audit file only?
        public static bool g_is_test;                       // output to file w/dummy ids and no event log
        public static bool g_no_delete;                     // keep old chart items?
        public static bool g_stop_on_error;
        public static bool g_import_when_done;

        public static DateTime      g_effdt;                // effective datetime

        public static StreamWriter  outfile;                // Transparent.txt
        public static StreamWriter  logfile;                // TransparentLog.txt

        public static int           gLogUnitID;
        public static int           gLogEncounterID;
        public static string        gLogSourceText;
        public static string        gLogMapperVersion;

        public static StringBuilder gBriefAudit;            // the complete brief audit
        public static StringBuilder gVerboseAudit;          // the complete verbose audit

        public static DateTime      g_pull_start;           // global pull start  (not limited by patient values)
        public static DateTime      g_pull_finish;          // global pull finish
        public static int           g_range;                // global range

        private static string   _this_acct;                 // acct filter
        private static string   _this_unit_name;            // unit name filter
        private static int      _this_unit_id;              // unit id filter

        static string           _import_path;
        static string           _log_path;
        public static List<PatientLocation> patloclist;

        static void Main(string[] args)
        {
            try
            {
                CheckForLocHistLoss();
                InitGlobals();

                ParseCommandLine(args);
                SetMapperVersion();

                gLogSourceText = String.Join(" ", args);           // add command line to log
                LogInfo("Begin mapping and translation", PFSEventLog.EventLogCategory.EVENT_CATEGORY_STARTUP_SHUTDOWN);
                gLogSourceText = "";

                OpenOutputFiles();
                ProcessPatients();
                outfile.Close();

                DeleteOldChartItems();

                LogInfo("Mapping complete", PFSEventLog.EventLogCategory.EVENT_CATEGORY_STARTUP_SHUTDOWN);

                //MaybeRunImport();
                CloseOutputFiles();
            }
            catch (Exception e)
            {
                Console.WriteLine("Unexpected Error: {0}", e.Message);
                Console.WriteLine("{0}", e.StackTrace);
                LogUnexpectedError(e.Message, e.StackTrace);
            }

            if (g_debug)
            {
                Console.WriteLine(Environment.NewLine);
                Console.Write("Press any key...");
                Console.ReadKey();
            }
        }

        static void InitGlobals()
        {
            gLogSourceText = "";
            gBriefAudit = new StringBuilder();
            gVerboseAudit = new StringBuilder();
        }

        static void ParseCommandLine(string[] args)
        {
            string nowdate, nowtime;
            string value;
            string effdate, efftime;
            string pulldate, pulltime;

            var nowdt = DateTime.Now;
            nowdate = nowdt.ToString("yyyyMMdd");
            nowtime = nowdt.ToString("HHmm");
            effdate = "";
            efftime = "";
            pulldate = "";
            pulltime = "";

            _import_path = PFSUtility.DefaultImportPath();          // ...\load_me
            _log_path = PFSUtility.DefaultLogPath();                // ...\log

            foreach (var arg in args)
            {
                var arr = arg.Split('=');
                value = (arr.GetUpperBound(0) > 0) ? arr[1] : "";

                switch (arr[0])
                {
                    case "-acct":                               // process this patient only
                        _this_acct = value;
                        break;
                        
                    case "-debug":                              // output to console and pause at end
                        g_debug = true;
                        break;

                    case "-effdate":                            // effective date: yyyymmdd
                        if (value == "yesterday")
                            effdate = DateTime.Now.AddDays(-1).ToString("yyyyMMdd");
                        else
                            effdate = value.Left(8);
                        break;
                    
                    case "-efftime":                            // effective time: HHMM or HH
                        efftime = value.Left(4);
                        efftime = efftime.PadRight(4,'0');
                        break;
                    
                    case "-import":                             // run transparent import when done
                        g_import_when_done = true;
                        break;

                    case "-log":                                // create TransparentAudit.log
                        g_log = true;                           // (verbose audit)
                        break;

                    case "-path":                               // override default import path
                        _import_path = value;
                        break;

                    case "-n":                                  // no output to transparent.txt 
                        g_no_output = true;                     // (audit only)
                        break;

                    case "-nodelete":                           // do not delete old chart items
                        g_no_delete = true;
                        break;

                    case "-pulldate":                           // pull date: yyyymmdd
                        if (value == "today")
                            pulldate = DateTime.Now.ToString("yyyyMMdd");
                        else
                            pulldate = value.Left(8);
                        break;
                    
                    case "-pulltime":                           // pull time: HHMM or HH
                        pulltime = value.Left(4);
                        pulltime = pulltime.PadRight(4,'0');
                        break;

                    case "-range":                              // range in minutes; default = 1440 = 24hr
                        g_range = value.ToInteger();
                        break;
                        
                    case "-stoponerror":
                        g_stop_on_error = true;
                        break;

                    case "-test":
                        g_is_test = true;
                        break;

                    case "-unit":                               // process this unit only
                        _this_unit_name = value;
                        break;
                    case "-unit_id":                            // process this unit only
                        _this_unit_id = value.ToInteger();
                        break;

                    default:
                        Console.WriteLine("unexpected argument: {0}", arg);
                        break;
                }
            }

            if (String.IsNullOrEmpty(effdate))
                effdate = nowdate;
            if (String.IsNullOrEmpty(efftime))
                efftime = nowtime;

            // Note: pulldate defaults to effdate, not nowdate
            if (String.IsNullOrEmpty(pulldate))
                pulldate = effdate;
            if (String.IsNullOrEmpty(pulltime))
                pulltime = efftime;

            if (g_range == 0)
                g_range = DEFAULT_RANGE;

            DebugTrace("Import path=", _import_path);
            DebugTrace("Log path=", _log_path);
            DebugTrace("effdate={0}", effdate);
            DebugTrace("efftime={0}", efftime);
            DebugTrace("pulldate={0}", pulldate);
            DebugTrace("pulltime={0}", pulltime);
            DebugTrace("range={0}", g_range);

            // class IN time
            g_effdt = PFSUtility.ISOToDateTime(effdate + efftime);
            // range for chart item queries
            g_pull_finish = PFSUtility.ISOToDateTime(pulldate + pulltime);
            g_pull_start = g_pull_finish.AddMinutes(-g_range);

        }

        static void OpenOutputFiles()
        {
            // Append to existing file
            outfile = new StreamWriter(Path.Combine(_import_path, TRANSP_FILENAME), true);

            if (g_log) {
                // re-write new file
                DateTime nowdt = DateTime.Now;
                string nowdate;
                string nowtime;
                nowdate = nowdt.ToString("yyyyMMdd");
                nowtime = nowdt.ToString("HHmm");

                logfile = new StreamWriter(Path.Combine(_log_path, nowdate + nowtime + TRANSP_AUDIT_LOG));
            }
        }

        static void CloseOutputFiles()
        {
            
            if (logfile != null) {
                logfile.Close();
                logfile = null;
            }
        }

        static void SetMapperVersion()
        {
            gLogMapperVersion = MAPPER_VERSION;
        }

        static void CheckForLocHistLoss()
        {
            string sql;
            int ct=0;

            //sql = "select count(*) from encounter as e left join encounter_location as el on (e.encounter_id=el.encounter_id) where el.encounter_id is null and e.timestamp>=dateadd(d,-30,getdate())";
            sql = "select count(*) from encounter_location as el where (el.room='' or el.room is null) and el.effective_datetime_in>=dateadd(d,-60,getdate())";
            var db = PFSUtility.NewSqlConnection();
            var cmd = new SqlCommand(sql, db);
            SqlDataReader dr = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr.Read())
            {
                ct = PFSUtility.DBToInt(dr[0]);
            }
            dr.Close();

            //Add announcement or staffing note here
            using (var cn = PFSUtility.NewSqlConnection())
            {
                if (ct > 0)
                    sql = "update security_param set BEFORE_LOGIN_ANNOUNCEMENT='ATTENTION System Administrators: POSSIBLE LOCATION HISTORY LOSS'";
                else
                    sql = "update security_param set BEFORE_LOGIN_ANNOUNCEMENT=''";
                //Program.DebugTrace(sql);
                cmd = new SqlCommand(sql, cn);
                cmd.ExecuteNonQuery();
            }
            return;
        }

        static void ProcessPatients()
        {
            string sql, audit_header;
            int count;
            int prev_encid = 0;

            // Make a list of all patients with chart items during the pull period.
            //
            // Include only units that have the "use transparent" flag set.
            // Limit to one patient if -acct is given.  Limit to one unit with -unit.
            // Left join encounter_location - the patient may be discharged at pull time.
            // Look for the latest patient arrival to the unit before the pull time; ignore transfers within the unit.
            // Look for a patient departure (if any) before the pull time; ignore transfers within the unit.
            //sql = 
            //    " SELECT LIST.UNIT_ID, LIST.ENCOUNTER_ID, PARAM.METHODOLOGY_ID,\n" +
            //    "     UNIT.NAME AS UNIT_NAME, EL.ROOM, EL.BED, EL.DATETIME_IN AS BED_ARRIVAL,\n" +
            //    "     EL.HOSPITAL_SERVICE_LVC,F.CLASSIFICATION_FACILITY_CODE,\n" +
            //    "     ARRIVE.DATETIME_IN AS UNIT_ARRIVAL, DEPART.DATETIME_OUT AS UNIT_DEPARTURE,\n" +
            //    "     E.ACCT_NUMBER, P.LAST_NAME, P.FIRST_NAME, P.MIDDLE_NAME,\n" +
            //    "     P.LANGUAGE_LVC,E.AGE_AT_ADMISSION,\n" +
            //    "     UNIT.IS_ED, UNIT.IS_ICU\n" +
            //    " FROM (\n" +
            //    "     SELECT DISTINCT CI.UNIT_ID, CI.ENCOUNTER_ID\n" +
            //    "     FROM CHART_ITEM AS CI\n" +
            //    "     INNER JOIN UNIT ON (UNIT.UNIT_ID = CI.UNIT_ID)\n" +
            //    "     WHERE UNIT.USE_TRANSPARENT_CLASSIFICATION='Y'\n" +
            //    "     AND EVENT_DATETIME BETWEEN " + PFSUtility.SQLDateTime(g_pull_start) + " AND " + PFSUtility.SQLDateTime(g_pull_finish) +
            //    " ) AS LIST" +
            //    " INNER JOIN UNIT ON (UNIT.UNIT_ID = LIST.UNIT_ID)\n" +
            //    " INNER JOIN UNIT_PARAM AS PARAM ON (PARAM.UNIT_ID = UNIT.UNIT_ID) AND (" + PFSUtility.SQLDateTime(g_pull_finish) + " BETWEEN PARAM.EFFECTIVE_REPORT_DATE AND PARAM.EXPIRATION_REPORT_DATE)\n" +
            //    " INNER JOIN FACILITY AS F ON (F.FACILITY_ID = UNIT.FACILITY_ID)\n" +
            //    " INNER JOIN ENCOUNTER AS E ON (E.ENCOUNTER_ID = LIST.ENCOUNTER_ID)\n" +
            //    " INNER JOIN PERSON AS P ON (P.PERSON_ID = E.PERSON_ID)\n" +
            //    " LEFT  JOIN ENCOUNTER_LOCATION AS EL ON (EL.ENCOUNTER_ID = E.ENCOUNTER_ID) AND (" + PFSUtility.SQLDateTime(g_pull_finish) + " BETWEEN EL.EFFECTIVE_DATETIME_IN AND EL.EFFECTIVE_DATETIME_OUT)\n" +
            //    " INNER JOIN (\n" +
            //    "    SELECT WORKING_UNIT_ID AS UNIT_ID, ENCOUNTER_ID, MAX(DATETIME_IN) AS DATETIME_IN\n" +
            //    "    FROM ENCOUNTER_LOCATION AS EL\n" +
            //    "    WHERE WORKING_UNIT_ID IN (SELECT UNIT_ID FROM UNIT WHERE USE_TRANSPARENT_CLASSIFICATION='Y')\n" +
            //    "    AND DATETIME_IN < " + PFSUtility.SQLDateTime(g_pull_finish) +
            //    "    AND IS_TRANSFER_IN='Y'\n" +
            //    "    GROUP BY WORKING_UNIT_ID, ENCOUNTER_ID\n" +
            //    " ) AS ARRIVE ON (ARRIVE.UNIT_ID = UNIT.UNIT_ID) AND (ARRIVE.ENCOUNTER_ID = E.ENCOUNTER_ID)\n" +
            //    " LEFT JOIN (\n" +
            //    "    SELECT WORKING_UNIT_ID AS UNIT_ID, ENCOUNTER_ID, MAX(DATETIME_OUT) AS DATETIME_OUT\n" +
            //    "    FROM ENCOUNTER_LOCATION AS EL\n" +
            //    "    WHERE WORKING_UNIT_ID IN (SELECT UNIT_ID FROM UNIT WHERE USE_TRANSPARENT_CLASSIFICATION='Y')\n" +
            //    "    AND DATETIME_OUT < " + PFSUtility.SQLDateTime(g_pull_finish) +
            //    "    AND ((NEXT_WORKING_UNIT_ID<>WORKING_UNIT_ID) OR (NEXT_WORKING_UNIT_ID IS NULL))\n" +
            //    "    GROUP BY WORKING_UNIT_ID, ENCOUNTER_ID\n" +
            //    " ) AS DEPART ON (DEPART.UNIT_ID = UNIT.UNIT_ID) AND (DEPART.ENCOUNTER_ID = E.ENCOUNTER_ID)\n" +
            //    " WHERE (1=1)\n";
            
 sql =  " SELECT LIST.UNIT_ID, LIST.ENCOUNTER_ID, PARAM.METHODOLOGY_ID,\n" +
        "     UNIT.NAME AS UNIT_NAME, EL.ROOM, EL.BED, EL.DATETIME_IN AS BED_ARRIVAL,\n" +
        "     EL.HOSPITAL_SERVICE_LVC,F.CLASSIFICATION_FACILITY_CODE,\n" +
        "     ARRIVE.DATETIME_IN AS UNIT_ARRIVAL, DEPART.DATETIME_OUT AS UNIT_DEPARTURE,\n" +
        "     E.ACCT_NUMBER, P.LAST_NAME, P.FIRST_NAME,P.MIDDLE_NAME,\n" +
        "     P.LANGUAGE_LVC,E.AGE_AT_ADMISSION,\n" +
        "     UNIT.IS_ED, UNIT.IS_ICU\n" +
        //"     P.DOB, E.ADMISSION_DATETIME, E.REGISTRATION_DATETIME\n" +
        " FROM (\n" +
        "     SELECT DISTINCT ELLIST.UNIT_ID as UNIT_ID, ELLIST.ENCOUNTER_ID\n" +
        "     FROM ENCOUNTER_LOCATION AS ELLIST\n" +
        "     INNER JOIN UNIT ON (UNIT.UNIT_ID = ELLIST.UNIT_ID)\n" +
        "     WHERE UNIT.USE_TRANSPARENT_CLASSIFICATION='Y'\n" +
        "     AND (ELLIST.EFFECTIVE_DATETIME_IN BETWEEN " + PFSUtility.SQLDateTime(g_pull_start) + " AND " + PFSUtility.SQLDateTime(g_pull_finish) + " OR \n" +
        "          ELLIST.EFFECTIVE_DATETIME_OUT BETWEEN " + PFSUtility.SQLDateTime(g_pull_start) + " AND " + PFSUtility.SQLDateTime(g_pull_finish) + " OR \n" +
        "          (ELLIST.EFFECTIVE_DATETIME_IN <" + PFSUtility.SQLDateTime(g_pull_start) + " AND \n" +
        "           ELLIST.EFFECTIVE_DATETIME_OUT >" + PFSUtility.SQLDateTime(g_pull_finish) + "))\n" +
        " ) AS LIST\n";
    sql += 
        " INNER JOIN UNIT ON (UNIT.UNIT_ID = LIST.UNIT_ID)\n" +
        " INNER JOIN UNIT_PARAM AS PARAM ON (PARAM.UNIT_ID = UNIT.UNIT_ID) AND (" + PFSUtility.SQLDateTime(g_pull_finish) + " BETWEEN PARAM.EFFECTIVE_DATETIME AND EXPIRATION_DATETIME)\n" +
        " INNER JOIN FACILITY AS F ON (F.FACILITY_ID = UNIT.FACILITY_ID)\n" +
        " INNER JOIN ENCOUNTER AS E ON (E.ENCOUNTER_ID = LIST.ENCOUNTER_ID)\n" +
        " INNER JOIN PERSON AS P ON (P.PERSON_ID = E.PERSON_ID)\n" +
        " LEFT JOIN ENCOUNTER_LOCATION AS EL ON (EL.ENCOUNTER_ID = E.ENCOUNTER_ID) AND (" + PFSUtility.SQLDateTime(g_pull_finish) + " BETWEEN EL.EFFECTIVE_DATETIME_IN AND EL.EFFECTIVE_DATETIME_OUT)\n" +
        " INNER JOIN (\n" +
        "    SELECT UNIT_ID AS UNIT_ID, ENCOUNTER_ID, MAX(DATETIME_IN) AS DATETIME_IN\n" +
        "    FROM ENCOUNTER_LOCATION AS EL\n" +
        "    WHERE UNIT_ID IN (SELECT UNIT_ID FROM UNIT WHERE USE_TRANSPARENT_CLASSIFICATION='Y')\n" +
        "    AND DATETIME_IN < " + PFSUtility.SQLDateTime(g_pull_finish) +
        "    AND IS_TRANSFER_IN='Y'\n" +
        "    GROUP BY UNIT_ID, ENCOUNTER_ID\n" +
        " ) AS ARRIVE ON (ARRIVE.UNIT_ID = UNIT.UNIT_ID) AND (ARRIVE.ENCOUNTER_ID = E.ENCOUNTER_ID)\n" +
        " LEFT JOIN (\n" +
        "    SELECT UNIT_ID AS UNIT_ID, ENCOUNTER_ID, MAX(DATETIME_OUT) AS DATETIME_OUT\n" +
        "    FROM ENCOUNTER_LOCATION AS EL\n" +
        "    WHERE UNIT_ID IN (SELECT UNIT_ID FROM UNIT WHERE USE_TRANSPARENT_CLASSIFICATION='Y')\n" +
        "    AND DATETIME_OUT < " + PFSUtility.SQLDateTime(g_pull_finish) +
        "    AND ((NEXT_WORKING_UNIT_ID<>UNIT_ID) OR (NEXT_WORKING_UNIT_ID IS NULL))\n" +
        "    GROUP BY UNIT_ID, ENCOUNTER_ID\n" +
        " ) AS DEPART ON (DEPART.UNIT_ID = UNIT.UNIT_ID) AND (DEPART.ENCOUNTER_ID = E.ENCOUNTER_ID)\n" +
        " WHERE 1=1\n";

            if (!String.IsNullOrEmpty(_this_acct)) {
                sql += " AND E.ACCT_NUMBER=" + PFSUtility.SQLString(_this_acct) + "\n";
            }
            if (!String.IsNullOrEmpty(_this_unit_name)) {
                sql += " AND UNIT.NAME=" + PFSUtility.SQLString(_this_unit_name) + "\n";
            }
            if (_this_unit_id > 0) {
                sql += " AND UNIT.UNIT_ID=" + _this_unit_id + "\n";
            }
//            sql += " ORDER BY UNIT.NAME, P.LAST_NAME, P.FIRST_NAME, E.ACCT_NUMBER\n";
            sql += "  ORDER BY LIST.ENCOUNTER_ID\n";

            DebugTrace(sql);
            var db = PFSUtility.NewSqlConnection();
            var cmd = new SqlCommand(sql, db);
            SqlDataReader dr = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);

            //
            // Process all patients
            //
            count = 0;
            var default_inds = new List<int>();
            var default_inds_str = "";
            var default_ptype = 0;
            patloclist = new List<PatientLocation>();

            while (dr.Read())
            {
                count++;
                Console.Write("\rProcessing patient {0}", count);

                // Package the patient info
                // NOTE: ''dr["FIRST_NAME"] as string'' looks great but it will save nulls.
                //       use ''PFSUtility.DBToString(dr["FIRST_NAME"])'' to convert to empty strings.
                var pat = new PatientInfo();
                pat.unit_id = PFSUtility.DBToInt(dr["UNIT_ID"]);
                pat.encounter_id = PFSUtility.DBToInt(dr["ENCOUNTER_ID"]);
                pat.meth_id = PFSUtility.DBToInt(dr["METHODOLOGY_ID"]);
                pat.acct = PFSUtility.DBToString(dr["ACCT_NUMBER"]);
                pat.last_name = PFSUtility.DBToString(dr["LAST_NAME"]);
                pat.first_name = PFSUtility.DBToString(dr["FIRST_NAME"]);
                pat.middle_name = PFSUtility.DBToString(dr["MIDDLE_NAME"]);
                pat.facilty_code = PFSUtility.DBToString(dr["CLASSIFICATION_FACILITY_CODE"]);
                pat.unit_name = PFSUtility.DBToString(dr["UNIT_NAME"]);
                pat.room = PFSUtility.DBToString(dr["ROOM"]);
                pat.bed = PFSUtility.DBToString(dr["BED"]);
                pat.age = PFSUtility.DBToDouble(dr["AGE_AT_ADMISSION"]);                
                pat.unit_arrival = PFSUtility.DBToDateTime(dr["UNIT_ARRIVAL"]);
                pat.unit_departure = PFSUtility.DBToDateTime(dr["UNIT_DEPARTURE"]);
                pat.effective = PFSUtility.MaxDateTime(g_effdt, pat.unit_arrival);
                pat.pull_start = PFSUtility.MaxDateTime(g_pull_start, pat.unit_arrival);
                pat.pull_finish = PFSUtility.MinDateTime(g_pull_finish, pat.unit_departure);
                if (pat.pull_finish < pat.pull_start) {
                    pat.pull_finish = g_pull_finish;
                }
                pat.range = (int)PFSUtility.DateDiffInMinutes(pat.pull_start, pat.pull_finish);
                pat.los_hours = pat.range / 60.0;
                pat.TC_source_id = 0; //--PFSUtility.DBToInt(dr["TC_SOURCE_ID"]);        //TCP port
                pat.is_ED = PFSUtility.DBToBool(dr["IS_ED"]);
                pat.is_ICU = PFSUtility.DBToBool(dr["IS_ICU"]);
                pat.service = PFSUtility.DBToString(dr["HOSPITAL_SERVICE_LVC"]);
                pat.language = PFSUtility.DBToString(dr["LANGUAGE_LVC"]);

                // Get the list of default indicators on admission (if any)
                default_inds = GetUnitDefaultIndicators(pat.unit_id, g_pull_start, out default_inds_str, out default_ptype);
                pat.default_inds = default_inds;
                pat.default_inds_str = default_inds_str;
                pat.default_ptype = default_ptype;

                // Get ready for event log entries for this patient
                gLogUnitID = pat.unit_id;
                gLogEncounterID = pat.encounter_id;
                gLogSourceText = "";

                // Reset both audit strings and make headers for both
                gBriefAudit = new StringBuilder();
                gVerboseAudit = new StringBuilder();
                audit_header =
                    new String('=', 80) + Environment.NewLine +
                    "TRANSPARENT MAPPING AUDIT FILE                    Run Time=" + DateTime.Now + Environment.NewLine +
                    "Version:   " + gLogMapperVersion + Environment.NewLine +
                    "Pull:      " + g_pull_finish + Environment.NewLine +
                    "Effective: " + g_effdt + Environment.NewLine +
                    "Range:     " + g_range + " (" + Math.Round(g_range / 60.0, 1) + " hrs) starting " + g_pull_start;

                // This will add to both brief and verbose audits
                Audit(audit_header);
                Audit(new String('=', 80));
                Audit("Patient " + pat.last_name + ", " + pat.first_name + " " + pat.middle_name +
                    " in " + pat.unit_name + " " + pat.room + " " + pat.bed +
                    " acct=" + pat.acct + " age=" + Math.Round(pat.age, 2));
                Audit("prelocHere from " + pat.pull_start + " to " + pat.pull_finish +
                    "  (LOS=" + Math.Round(pat.los_hours, 2) + ")");

                // Find all locations in the pull range. Look between g_pull_start and g_pull_finish.
                patloclist = GetPatientLocations(pat.encounter_id,g_pull_start,g_pull_finish,out pat.pull_start,out pat.pull_finish);
                pat.los_hours = PFSUtility.DateDiffInMinutes(pat.pull_start, pat.pull_finish) / 60.0;
                Audit("postlocHere from " + pat.pull_start + " to " + pat.pull_finish +
                    "  (LOS=" + Math.Round(pat.los_hours, 2) + ")");
                Audit("rangestart=" + g_pull_start + " rangeend=" + g_pull_finish);
                foreach (var ploc in patloclist)
                {
                    Audit("unitid=" + ploc.unit_id + " start= " + ploc.in_time + " end=" + ploc.out_time);
                }

                if (prev_encid != pat.encounter_id)
                {
                    prev_encid = pat.encounter_id;
                    // Process this patient
                    // Catch any unexpected errors and continue with next patient.
                    try
                    {
                        // Run the appropriate mapping for this unit and date
                        //
                        switch (pat.meth_id)
                        {
                            case PFSGlobal.METH_ID_APLUS_INPATIENT2:
                                var ip = new Inpatient2();
                                ip.ProcessPatient(pat);
                                break;
                            default:
                                LogWarning("Methodology " + pat.meth_id + " in unit " + pat.unit_name + " is not supported");
                                prev_encid = -1; //allows the next location for same encid to be processed
                                break;
                        }

                        gLogUnitID = 0;
                        gLogEncounterID = 0;
                    }
                    catch (Exception e)
                    {
                        LogUnexpectedError(e.Message, e.StackTrace);
                        // Stop or keep going?
                        if (Program.g_stop_on_error) g_abort = true;
                    }
                }
                if (g_abort) break;
            }

            Console.WriteLine();
            dr.Close();
            
            if (count < 1) {
                Audit("");
                if(!String.IsNullOrEmpty(_this_acct)) {
                    LogWarning("The selected patient has no chart items in the given time range");
                } else {
                    LogWarning("No chart items found to process - have the unit(s) been enabled for transparent classification?");
                }
            }   
        }

        static List<int> GetUnitDefaultIndicators(int unit_id, DateTime pull_dt, out string default_inds_str, out int default_ptype)
        {
            var result = new List<int>();                   // make an empty list
            default_inds_str = "<none>";
            default_ptype = 6;
            var db = PFSUtility.NewPfsDataContext();        // look for the unit default admission profile
            var query = from param in db.UNIT_PARAMs
                        from profile in param.PATIENT_PROFILEs
                        where (param.UNIT_ID == unit_id)
                        && (pull_dt >= param.EFFECTIVE_DATETIME) && (pull_dt < param.EXPIRATION_DATETIME)
                        && (profile.PROFILE_NUMBER == param.DEFAULT_ADMISSION_PROFILE)
                        select new
                        {
                            profile.INDICATORS              // comma-separated indicator list
                        };
            foreach (var inds in query)
            {
                default_inds_str = inds.INDICATORS;
                string s = inds.INDICATORS;
                if (!String.IsNullOrEmpty(s))
                {
                    var arr = s.Split(',');
                    foreach (var t in arr)
                    {
                        if (t.IsNumeric())
                        {                // add an indicator number to the list
                            result.Add(t.ToInteger());
                        }
                    }
                }
            }

            var query_ind_def = from ind_def in db.INDICATOR_DEFINITIONs
                                where (ind_def.METHODOLOGY_ID == PFSGlobal.METH_ID_APLUS_INPATIENT2) &&
                                  result.Contains(ind_def.INDICATOR_NUMBER)
                                select new
                                {
                                    ind_def.WEIGHT
                                };
            var score = 0.0;
            foreach (var wgts in query_ind_def)
            {
                score += wgts.WEIGHT;
            }
            DebugTrace("indicators=" + default_inds_str, "");
            DebugTrace("score=" + score.ToString(), "");

            var query_ptype = from ptype in db.PATIENT_TYPEs
                              where (ptype.METHODOLOGY_ID == PFSGlobal.METH_ID_APLUS_INPATIENT2)
                              orderby ptype.PATIENT_TYPE1
                              select new
                              {
                                  ptype.PATIENT_TYPE1,
                                  ptype.POINTS_HIVAL
                              };

            foreach (var ptypes in query_ptype)
            {
                //DebugTrace("type,hival=" + ptypes.PATIENT_TYPE1.ToString()+","+ptypes.POINTS_HIVAL.ToString(), "");
                if (score <= ptypes.POINTS_HIVAL)
                {
                    if (default_ptype > ptypes.PATIENT_TYPE1)
                    {
                        default_ptype = ptypes.PATIENT_TYPE1;
                    }
                }
            }
            DebugTrace("def patient type=" + default_ptype.ToString(), "");

            return result;
        }


        static List<PatientLocation> GetPatientLocations(int encid, DateTime start_dt, DateTime end_dt, out DateTime patstart, out DateTime patfinish)
        {
            var result = new List<PatientLocation>();                   // make an empty list
            var db = PFSUtility.NewPfsDataContext();        // look for the unit default admission profile
            var query = from loc in db.ENCOUNTER_LOCATIONs
                        join u in db.UNITs on loc.UNIT_ID equals u.UNIT_ID
                        join up in db.UNIT_PARAMs on u.UNIT_ID equals up.UNIT_ID into xup
                        from subup in xup.DefaultIfEmpty()
                        where (loc.ENCOUNTER_ID == encid) && (u.UNIT_ID>=-1)
                        && (g_pull_start >= subup.EFFECTIVE_DATETIME) && (g_pull_start < subup.EXPIRATION_DATETIME)
                        && (
                            ((start_dt >= loc.EFFECTIVE_DATETIME_IN) && (start_dt <= loc.EFFECTIVE_DATETIME_OUT)) 
                            || ((end_dt >= loc.EFFECTIVE_DATETIME_IN) && (end_dt <= loc.EFFECTIVE_DATETIME_OUT))
                            || ((start_dt < loc.EFFECTIVE_DATETIME_IN) && (end_dt >= loc.EFFECTIVE_DATETIME_OUT)) 
                            )
                        orderby loc.EFFECTIVE_DATETIME_IN
                        select new
                        {
                            loc.UNIT_ID,
                            loc.ROOM,
                            loc.BED,
                            loc.HOSPITAL_SERVICE_LVC,
                            loc.EFFECTIVE_DATETIME_IN,
                            loc.EFFECTIVE_DATETIME_OUT,
                            u.NAME,
                            subup.METHODOLOGY_ID
                        };
            foreach (var locitem in query)
            {
                var ptloc = new PatientLocation();

                ptloc.unit_id = (int)locitem.UNIT_ID;
                ptloc.unit_name= (string)locitem.NAME;
                ptloc.room = (string)locitem.ROOM;
                ptloc.bed = (string)locitem.BED;
                ptloc.service = (string)locitem.HOSPITAL_SERVICE_LVC;
                if (locitem.METHODOLOGY_ID != null)
                    ptloc.methid = (int)locitem.METHODOLOGY_ID;
                else
                    ptloc.methid = 0;
                if ((start_dt >= (DateTime)locitem.EFFECTIVE_DATETIME_IN) && (start_dt < (DateTime)locitem.EFFECTIVE_DATETIME_OUT))
                {
                    ptloc.in_time = start_dt;
                    ptloc.loc_start = (DateTime)locitem.EFFECTIVE_DATETIME_IN;
                }
                else 
                {
                    ptloc.in_time = (DateTime)locitem.EFFECTIVE_DATETIME_IN;
                    ptloc.loc_start = (DateTime)locitem.EFFECTIVE_DATETIME_IN;
                }
                if ((end_dt >= (DateTime)locitem.EFFECTIVE_DATETIME_IN) && (end_dt <= (DateTime)locitem.EFFECTIVE_DATETIME_OUT))
                {
                    ptloc.out_time = end_dt;
                }
                else
                {
                    ptloc.out_time = (DateTime)locitem.EFFECTIVE_DATETIME_OUT;
                    if (ptloc.out_time > end_dt) ptloc.out_time = end_dt;
                }
Audit("loc " + ptloc.unit_name + "  in= " + ptloc.in_time + "  out= " + ptloc.out_time + "  meth=" + ptloc.methid);

                result.Add(ptloc);
            }
            PatientLocation[] locary = result.ToArray();
            int num_locs = 0;
            //  where there are adjacent same-locations, mark the duplicates as remove.
            for (int i = 0; i <= locary.GetUpperBound(0); i++)
            {
                num_locs++;
                locary[i].remove = false;
                if (i >= 1)
                {
                    if ((locary[i - 1].unit_name == "TAC") && 
                        (locary[i - 1].unit_name == locary[i].unit_name))
                    {
                        if ((locary[i - 1].room.StartsWith("4") && locary[i].room.StartsWith("4")) ||
                            (locary[i - 1].room.StartsWith("5") && locary[i].room.StartsWith("5")))
                        {
                            locary[i].remove = true;
                        }
                    }
                    else
                    {
                        if ((locary[i - 1].unit_id == locary[i].unit_id) 
                            && (locary[i - 1].out_time == locary[i].in_time)
                            && (locary[i - 1].service == locary[i].service))
                        {
                            locary[i].remove = true;
                        }
                    }
                }
            }
            //  where there are several adjacent same-unit locations, need to combine them into 1 loc record.
            //  make the outdt of that record be the latest outdt
            int lastgoodidx = 0;
            int lastadj = 0;
            for (int i = 0; i <= locary.GetUpperBound(0); i++) 
            {
                if (!locary[i].remove)
                {
                    lastgoodidx = i;
                    lastadj = i;
                }
                else
                {
                    if (lastadj == i - 1)
                    {
                        locary[lastgoodidx].out_time = locary[i].out_time;
                        lastadj = i;
                    }
                }
            }
            // get rid of the removes
            int numkeep = 0;
            for (int i = 0; i <= locary.GetUpperBound(0); i++)
            {
                if (!locary[i].remove)
                {
                    numkeep++;
                    locary[numkeep-1] = locary[i]; //-1 for 0-based ary                
                }
            }
            if (numkeep < num_locs)
            {
                Array.Resize(ref locary, numkeep);
            }
            //now determine los_mins
            for (int i = 0; i <= locary.GetUpperBound(0); i++)
            {
                locary[i].los_mins = (int)PFSUtility.DateDiffInMinutes(locary[i].in_time, locary[i].out_time);
            }
//    num_removed = 0
//    'where there are adjacent same-locations, mark the duplicates as remove.
//    For i = 1 To pat.num_loc
//        pat.loc(i).remove = False
//        If i >= 2 Then
//        If pat.loc(i - 1).UnitID = pat.loc(i).UnitID Then
//            pat.loc(i).remove = True
//            num_removed = num_removed + 1
//'            dvprint "Remove: " & i
//        End If
//        End If
//    Next i
    
//    'where there are several adjacent same-unit locations, need to combine them into 1 loc record.
//    '  make the outdt of that record be the latest outdt
//'1011  --    0731  1:1/1
//'-1    0731  1301  2:2/'911   1301  1345  3: 3/3
//'911   1345  ++    R  4: 3/3  3=4

//    If pat.num_loc > 1 Then
//        For i = 1 To pat.num_loc
//            If Not pat.loc(i).remove Then
//                lastgoodidx = i
//                lastadj = i
//            End If
//            If i > 1 Then
//                If pat.loc(i).remove Then
//                    If lastadj = i - 1 Then
//                        pat.loc(lastgoodidx).outdt = pat.loc(i).outdt
//                        lastadj = i
//'                        dvprint "change outdt of: " & lastgoodidx & " to " & i & ": " & g_dbutil.SQL_DateTime(pat.loc(i).outdt)
//                    End If
//                End If
//            End If
//        Next i
//    End If
    
//    num_done = 0
//    'Finally remove all the duplicates (remove=true)
//    '1=1,2=2,3=3
//    For i = 1 To pat.num_loc
//        If Not pat.loc(i).remove Then
//            num_done = num_done + 1
//            pat.loc(num_done) = pat.loc(i)
//' dvprint pat.loc(num_done).UnitID & ": " & g_dbutil.SQL_DateTime(pat.loc(num_done).indt) & g_dbutil.SQL_DateTime(pat.loc(num_done).outdt)
//        End If
//    Next i
//    pat.num_loc = pat.num_loc - num_removed

            // Now Get Unit Arrival Times
            patstart = g_pull_start;
            patfinish = g_pull_finish;
            for (int i = 0; i <= locary.GetUpperBound(0); i++)
            {
                locary[i].arr_time = GetArrivalTime(encid,locary[i].unit_id,locary[i].in_time);
                if (i == 0) patstart = locary[i].in_time;
                if (i == locary.GetUpperBound(0)) patfinish = locary[i].out_time;
            }
            result = locary.ToList();
            return result;
        }

        static DateTime GetArrivalTime(int encid, int unitid, DateTime intime)
        {
            DateTime arrtime = DateTime.MinValue;
            var db = PFSUtility.NewPfsDataContext();        // look for the unit default admission profile
            var query = from loc in db.ENCOUNTER_LOCATIONs
                        where (loc.UNIT_ID == unitid)
                        && (loc.ENCOUNTER_ID == encid)
                        && (loc.EFFECTIVE_DATETIME_IN <= intime)
                        orderby loc.EFFECTIVE_DATETIME_IN
                        select new
                        {
                            loc.UNIT_ID,loc.EFFECTIVE_DATETIME_IN,loc.EFFECTIVE_DATETIME_OUT
                        };

            // example: unit a is the target, but the query returned b as the first unit
            // loc b
            //     a
            //     a
            //     c
            bool first =  true;
            DateTime prev_in = DateTime.MinValue;
            DateTime prev_out = DateTime.MinValue;
            foreach (var locitem in query)
            {
                if (first)
                {
                    first = false;
                    prev_in = (DateTime)locitem.EFFECTIVE_DATETIME_IN;
                    prev_out = (DateTime)locitem.EFFECTIVE_DATETIME_OUT;
                    arrtime = (DateTime)locitem.EFFECTIVE_DATETIME_IN;
                }
                else
                {
                    if (prev_out != (DateTime)locitem.EFFECTIVE_DATETIME_IN)
                    {
                        arrtime = (DateTime)locitem.EFFECTIVE_DATETIME_IN;
                    }
                    prev_in = (DateTime)locitem.EFFECTIVE_DATETIME_IN;
                    prev_out = (DateTime)locitem.EFFECTIVE_DATETIME_OUT;
                }
            }
            return arrtime;
        }   

        static void DeleteOldChartItems()
        {
            string sql;
            DateTime dt;

            if (g_no_output || g_no_delete) return;

            DebugTrace("About to delete old char items...");
            //DebugPause();                                       // give a chance to ^C in debug mode

            LogInfo("Delete old chart items...");
            dt = g_effdt.AddDays(-CHART_ITEM_LIFE);
            sql = "DELETE FROM CHART_ITEM WHERE EVENT_DATETIME < " + PFSUtility.SQLDateTime(dt);
            PFSUtility.ExecuteSQL(sql);
            LogInfo("Done");
        }

        static void MaybeRunImport()
        {
            if (! g_import_when_done) return;
            
            string path = Path.GetDirectoryName(Application.ExecutablePath);
            if (path.Right(3) != "bin") {                       // running in visual studio?
                path = @"C:\qmdev\AcuityPlus\main\bin";
            }
            if (Directory.Exists(path))
            {
                LogInfo("Running transparent import...");
                // Import will add its own log entries; no need to add any here
                int rc = PFSUtility.ExecuteAndWait(Path.Combine(path, PFSGlobal.TRANSPARENT_IMPORT_EXE_NAME));
                LogInfo("Done; result=" + rc);
            }
            else
            {
                LogWarning("Can't find " + path);
            }
        }



        //=====================================================================
        // Audits and log files
        //=====================================================================
        // Print to console if debug is set
        public static void DebugTrace(string format, params object[] values)
        {
            if (g_debug)
            {
                Console.Write(DateTime.Now.ToString() + " ");
                Console.WriteLine(format, values);
            }
        }

        public static void DebugPause()
        {
            if (g_debug)
            {
                Console.Write("Press any key...");
                Console.ReadKey();
                Console.WriteLine("");
            }
        }

        // Save in both audit files
        static public void Audit(string s)
        {
            DebugTrace(s);
            if (logfile != null) logfile.WriteLine(s);
            gBriefAudit.AppendLine(s);                  // Add to both audit reports
            gVerboseAudit.AppendLine(s);
        }

        // Save in verbose audit only
        static public void VerboseAudit(string s)
        {
            DebugTrace(s);
            if (logfile != null) logfile.WriteLine(s);
            gVerboseAudit.AppendLine(s);                // Add to verbose audit only
        }

        public static void AddLogEntry(PFSEventLog.EventLogType type, string msg, PFSEventLog.EventLogCategory category)
        {
            Audit(msg);

            PFSEventLog.AddEventLogEntry(
                PFSEventLog.EventLogSource.EVENT_SOURCE_TRANSPARENT_MAPPING,
                type, category, msg, gLogSourceText, 0, gLogUnitID, gLogEncounterID);
        }

        public static void LogInfo(string msg, PFSEventLog.EventLogCategory category)
        {
            AddLogEntry(PFSEventLog.EventLogType.EVENT_TYPE_INFO, msg, category);
        }
        public static void LogInfo(string msg)
        {
            LogInfo(msg, PFSEventLog.EventLogCategory.EVENT_CATEGORY_PROCESSED);
        }

        public static void LogWarning(string msg, PFSEventLog.EventLogCategory category)
        {
            AddLogEntry(PFSEventLog.EventLogType.EVENT_TYPE_WARNING, msg, category);
        }
        public static void LogWarning(string msg)
        {
            LogWarning(msg, PFSEventLog.EventLogCategory.EVENT_CATEGORY_VALIDATION);
        }

        public static void LogError(string msg, PFSEventLog.EventLogCategory category)
        {
            AddLogEntry(PFSEventLog.EventLogType.EVENT_TYPE_ERROR, msg, category);
        }
        public static void LogError(string msg)
        {
            LogError(msg, PFSEventLog.EventLogCategory.EVENT_CATEGORY_VALIDATION);
        }
        public static void LogUnexpectedError(string msg, string stack_trace)
        {
            // Add the message and stack trace to event log; message only goes to screen
            gLogSourceText = stack_trace;
            AddLogEntry(PFSEventLog.EventLogType.EVENT_TYPE_ERROR, msg, PFSEventLog.EventLogCategory.EVENT_CATEGORY_UNEXPECTED);
            gLogSourceText = "";
            
            // Add the stack trace to screen and log files
            Audit(msg);
            Audit(stack_trace);
        }

    }
}
