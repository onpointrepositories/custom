﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;                     // for LINQ; add a reference
using System.Data.SqlClient;                // for SqlConnection
using System.IO;                            // for Directory, Path, Stream
using PfsShared;                            // add a reference to Shared2 project
//
// Arnot Ogden Inpatient 1.0 transparent mapping
// This v1.1 is converted from VB6
// Note: VS 2008 does not support optional parameters; VS 2010 does but we aren't using it yet.
//
// This processes one patient using the Inpatient methodology.
//
// Most Arnot cateogories start with one of these prefixes:
// * PHx = Patient History - saved across visits
// * VHx = Visit History - valid for entire visit
// * NHx = Nothing history - not saved (shift only)
//
// All search functions use exact match for category, description and field name.
// result_like looks for one word somewhere in the result.
// result_list looks for any one of a list of words somewhere in the result.
//
namespace ArnotOgden2
{
    class ArnotInpatient
    {
        private const int MAX_INDS = 50;
        private const int MAX_PROCS = 20;

        private struct indicator_data {
            public bool     is_checked;     
            public int      radio_group;       
        }

        private struct proc_data {                      //not used at Arnot
            public int      procedure_number;
            public DateTime start;   
            public DateTime finish;
        }

        private CHART_ITEM[] _chart_items_since_admission;
        private CHART_ITEM[] _chart_items_since_unit_arrival;
        private CHART_ITEM[] _chart_items_during_pull_period;

        private indicator_data[] _inds;
        private int             _numprocs;
        private List<proc_data> _procs;

        private PatientInfo _pat;
        private int         _assist_count;

        private enum SearchDepth {
            SearchDefault,
            SearchPullRange,            //search within the current pull     -- default
            SearchSinceArrival,         //search since arrival to the unit
            SearchSinceAdmission        //search everything since admission to the hospital
        }

        private enum MatchMode {
            MatchExact,                 //field='abc'
            MatchContains               //field like '%abc%'
        }

        private enum CompareMode {
            BinaryCompare,              //case dependent
            TextCompare,                //case independent
        }

        private enum CountMode {
            CountAll,
            CountFirst                  //stop after one is found
        }

        private enum GetValueMode {
            GetTotal,
            GetMax,
            GetLast
        }

        private const int FREQUENCY_BUCKET_SIZE = 20;       //min

        enum Frequencies {
            QNONE,
            Q4H,
            Q2H,
            Q1H,
            Q30M
        }

        private struct fmapRow {
            public double   los_high;                       //the LOS being testing
            public int[]    freq;                           //the count required for each Q value
        }

        private fmapRow[]   _freq_map;                     //1,2,4,8,12,24 hours

        private MatchMode   _result_match;                 //equal/like
        private CompareMode _result_compare;               //case/no case
               
        //
        // This is the main entry point
        //
        public void ProcessPatient(PatientInfo pat)
        {
            _pat = pat;

            _result_match = MatchMode.MatchContains;           // results contain search word(s)
            _result_compare = CompareMode.TextCompare;         // case independent (all lower case)

            InitIndicators();
            InitProcs();
            LoadFreqTable();

            LoadPatientChart();
            Check_1_2_3();
            Check_4();
            Check_5_6();
            Check_7();
            Check_8();
            Check_9_10();
            Check_11_12();
            Check_13();
            Check_14_15_16_17();
            Check_18();
            Check_19_20();
            Check_21();
            Check_22();
            AtLeastOneADL();
            HighestIndicatorInEachGroupWins();

            CheckProcs();

            if (Program.g_no_output) return;
            OutputClass();
            OutputProcs();
        }

       
        private void InitIndicators()
        {
            string sql;
            int inum;

            _inds = new indicator_data[MAX_INDS + 1];           // This 1 based so add one

            // ** (This section can be replaced once we have a C# methodolgy cache)
            
            // get indicator radio groups from the database
            sql = "SELECT INDICATOR_NUMBER, RADIO_GROUP FROM INDICATOR_DEFINITION WHERE METHODOLOGY_ID=" + _pat.meth_id;
            using (var db = PFSUtility.NewSqlConnection())      // auto dispose & close
            {
                var cmd = new SqlCommand(sql, db);
                SqlDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    inum = PFSUtility.DBToInt(dr["INDICATOR_NUMBER"]);
                    if (inum <= MAX_INDS)
                    {
                        _inds[inum].radio_group = PFSUtility.DBToInt(dr["RADIO_GROUP"]);  // null to zero
                    }
                }

                dr.Close();
            }
        }

        private void InitProcs()
        {
            _procs = new List<proc_data>();
            _numprocs = 0;
        }

        private fmapRow LoadFreqTableRow(double los_high, string values)
        {
            fmapRow fmap;
            
            fmap.los_high = los_high;
            fmap.freq = new int[5];

            var arr = values.Split(',');
            for (int i = 0; i <= fmap.freq.GetUpperBound(0) ; i++ )
            {
                fmap.freq[i] = arr[i].ToInteger();
            }
            return fmap;
        }

        private void LoadFreqTable()
        {
            _freq_map = new fmapRow[6];
            //                                LOS,    None Q4h Q2h Q1h Q30m
            _freq_map[0] = LoadFreqTableRow(   1, "    0,  0,  0,  0,  1");
            _freq_map[1] = LoadFreqTableRow(   2, "    0,  0,  0,  1,  2");
            _freq_map[2] = LoadFreqTableRow(   4, "    0,  1,  2,  3,  4");
            _freq_map[3] = LoadFreqTableRow(   8, "    0,  2,  3,  4,  8");
            _freq_map[4] = LoadFreqTableRow(  12, "    0,  2,  4,  6, 12");
            _freq_map[5] = LoadFreqTableRow(9999, "    0,  3,  6, 12, 24");       //24+

            //to read this table: if you have at least 6 asessmenets in a 12-hour period, you get Q1h
            //(row 4: LOS=12, column Q1h has a count of 6)
        }

        private Frequencies FreqForCount(double los_hours, int count) 
        {
            for (int i = 0; (i <= _freq_map.GetUpperBound(0)); i++) {                         //go from LOS 1 to 24+
                if (los_hours <= _freq_map[i].los_high) {
                    // foreach goes low to high; use for and go high to low
                    for (int j = (int)Frequencies.Q30M; (j >= (int)Frequencies.QNONE); j--) { //search right to left
                        if (count >= _freq_map[i].freq[j]) {
                            return (Frequencies)j;
                        }
                    } // next j
                }
            } // next i
            
            return Frequencies.QNONE;
        }

        private void LoadPatientChart()
        {
            // Get the entire patient chart (all units and dates for the patient)
            var pfs = PFSUtility.NewPfsDataContext();
            var query = from item in pfs.CHART_ITEMs
                        where (item.ENCOUNTER_ID == _pat.encounter_id)
                        select item;
            // This will query the database right now
            _chart_items_since_admission = query.ToArray();

            // Convert to results to lower case if case insensitive compares are desired
            if (_result_compare == CompareMode.TextCompare)
            {
                foreach (var item in _chart_items_since_admission)
                {
                    item.RESULT = item.RESULT.ToLower();
                }
            }

            // Pre-filter two more versions of the chart
            var query2 = from item in _chart_items_since_admission
                    where(item.EVENT_DATETIME >= _pat.unit_arrival) && (item.EVENT_DATETIME <= _pat.pull_finish)
                    select item;
            _chart_items_since_unit_arrival = query2.ToArray();

            query2 = from item in _chart_items_since_admission
                         where (item.EVENT_DATETIME >= _pat.pull_start) && (item.EVENT_DATETIME <= _pat.pull_finish)
                         select item;
            _chart_items_during_pull_period = query2.ToArray();
        }

        // Get started with a new chart item query (of a certain depth)
        private IEnumerable<CHART_ITEM> StartNewQuery()
        {
            return StartNewQuery(SearchDepth.SearchDefault);
        }
        private IEnumerable<CHART_ITEM> StartNewQuery(SearchDepth search_depth)
        {
            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    return (from item in _chart_items_during_pull_period select item);
                case SearchDepth.SearchSinceArrival:
                    return (from item in _chart_items_since_unit_arrival select item);
                case SearchDepth.SearchSinceAdmission:
                    return (from item in _chart_items_since_admission select item);
            }
            return null;
        }

        private bool ValueIsAList(string s) 
        {
            return (s != null) && s.Contains(',');                //is this a comma-separated list?
        }

        private string[] SplitOnCommaAndTrimElements(string s)
        {
            // If a word list has a space after each comma, we will get leading blanks in each term
            var arr = s.Split(',');
            for (int i = 0; i <= arr.GetUpperBound(0); i++)
                arr[i] = arr[i].Trim();                         // get rid of leading blanks
            return arr;
        }

        //Do the correct string comparison based on the module flags
        private bool EqualOrContains(string s, string t) 
        {
            if (_result_match == MatchMode.MatchExact) {
                return (String.Compare(s, t) == 0);
            } else {
                return (s != null) && s.Contains(t);
            }
        }

        //Look for any of these fields.  Cat/code/desc/field = exact match.  Result = exact/like
        private IEnumerable<CHART_ITEM> AndSimpleItemFilter(IEnumerable<CHART_ITEM> query, string cat, string code, string desc, string field, string result_like) 
        {
            if (!String.IsNullOrEmpty(cat))         query = query.Where(e => e.CATEGORY == cat);
            if (!String.IsNullOrEmpty(code))        query = query.Where(e => e.CODE  == code);
            if (!String.IsNullOrEmpty(desc))        query = query.Where(e => e.DESCRIPTION == desc);
            if (!String.IsNullOrEmpty(field))       query = query.Where(e => e.FIELD_NAME == field);
            if (!String.IsNullOrEmpty(result_like)) query = query.Where(e => EqualOrContains(e.RESULT, result_like));
            return query;
        }

        //Look for a result that Contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultInList(IEnumerable<CHART_ITEM> query, string result_list) 
        {
            if (String.IsNullOrEmpty(result_list)) return query;
            
            var arr = SplitOnCommaAndTrimElements(result_list);
            return query.Where(e => arr.Contains(e.RESULT));
        }

        private string DescribeSearchDepth(SearchDepth search_depth) 
        {
            string result = "";
            
            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    //result = "in pull range"
                    result = "";                         //be quiet since this is default
                    break;
                case SearchDepth.SearchSinceArrival:
                    result = "since arrival to unit";
                    break;
                case SearchDepth.SearchSinceAdmission:
                    result = "since admission";
                    break;
            }
            
            return result;
        }

        private string Describe(string cat, string code, string desc, string field, string result_list)
        {
            return Describe(cat, code, desc, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(string cat, string code, string desc, string field, string result_list, SearchDepth search_depth)
        {
            string result;
           
            result = "looking for";
            if (!String.IsNullOrEmpty(cat))   result += " cat='" + cat + "'";
            if (!String.IsNullOrEmpty(code))  result += " code='" + code + "'";
            if (!String.IsNullOrEmpty(desc))  result += " desc='" + desc + "'";
            if (!String.IsNullOrEmpty(field)) result += " field='" + field + "'";
            if (!String.IsNullOrEmpty(result_list)) {
                result += " result" + ((_result_match == MatchMode.MatchContains) ? " contains " : "=") + "'" + result_list + "'";
            }

            var query = StartNewQuery(search_depth);
            if (ValueIsAList(result_list)) {
                query = AndSimpleItemFilter(query, cat, code, desc, field, "");
                query = AndResultInList(query, result_list);
            } else {
                query = AndSimpleItemFilter(query, cat, code, desc, field, result_list);
            }
            
            // We don't want all results, just the first result (but we do want to know how many there are).
            // Convert to an array so we can get a value and a count.
            var arr = query.ToArray();

            if (arr.Count() == 0) {
                result += "; not found " + DescribeSearchDepth(search_depth);
            } else {
                var e = arr[0];
                result += "; found";
                //Add info for columns that were not specified and a value was found
                if (String.IsNullOrEmpty(cat)   && (e.CATEGORY != null))    result += " cat='" + e.CATEGORY + "'";
                if (String.IsNullOrEmpty(code)  && (e.CODE != null))        result += " code='" + e.CODE + "'";
                if (String.IsNullOrEmpty(desc)  && (e.DESCRIPTION != null)) result += " desc='" + e.DESCRIPTION + "'";
                if (String.IsNullOrEmpty(field) && (e.FIELD_NAME != null))  result += " field='" + e.FIELD_NAME + "'";
                //Add the complete result found (we searched for a word or words)
                result += " result='" + e.RESULT + "'";
                //Are there more results?  Just say how many; we aren't going to list them.
                if (arr.Count() == 2) {
                    result += " (1 more result)";
                } else if (arr.Count() > 2) {
                    result += " (" + (arr.Count() - 1) + " more results)";
                }
            }
            
            return result;
        }

        //Set an indicator for this reason (low level)
        private void SetInd(int inum, string reason)
        {
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Set Ind #" + inum + ": " + reason);          //already set - repeat for verbose only
            } else {
                _inds[inum].is_checked = true;
                Program.Audit("Set Ind #" + inum + ": " + reason);
            }
        }

        //Clear an indicator for this reason (low level)
        private void ClrInd(int inum, string reason)
        {
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Clr Ind #" + inum + ": " + reason);          //already clear - repeat verbose only
            } else {
                _inds[inum].is_checked = false;
                Program.Audit("Clr Ind #" + inum + ": " + reason);
            }
        }

        //Count how many items have the search word in result_like
        private int CountSimpleResult(string cat, string code, string desc, string field, string result_like)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountSimpleResult(cat, code, desc, field, result_like, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountSimpleResult(string cat, string code, string desc, string field, string result_like, SearchDepth search_depth, bool trace, out string found_what)
        {
            var query = StartNewQuery(search_depth);
            query = AndSimpleItemFilter(query, cat, code, desc, field, result_like);
            int count = query.Count();

            //Normally CountSimpleResult will report what it finds; not eveyone wants this so the trace can be turned off
            found_what = Describe(cat, code, desc, field, result_like, search_depth);
            if (trace) Program.VerboseAudit(found_what);
            
            return count;
        }

        // Count how many items have a result with one of the words given in the result_list
        // Returns a description of what word was found
        private int CountResultInList(string cat, string code, string desc, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what="";
 
            var query = StartNewQuery(search_depth);
            query = AndSimpleItemFilter(query, cat, code, desc, field, "");
            query = AndResultInList(query, result_list);

            var arr = SplitOnCommaAndTrimElements(result_list);

            foreach (var item in query) {
                // Now figure out which of the search words was found
                var s = arr.First(e => e == item.RESULT);
                found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' code='" + item.CODE + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "'";
                Program.VerboseAudit(found_what);
                count++;
                if (count_mode != CountMode.CountAll) break;  //stop counting
            }
            
            if (count > 0) {
                //We already printed what was found; maybe add how many?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else {
                // Describe what was not found
                found_what = Describe(cat, code, desc, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }
            
            return count;
        }

        private int CountResultContains(string cat, string code, string desc, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountResultContains(cat, code, desc, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountResultContains(string cat, string code, string desc, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (ValueIsAList(result_list)) {
                return CountResultInList(cat, code, desc, field, result_list, search_depth, CountMode.CountAll, trace, out found_what);
            } else {
                return CountSimpleResult(cat, code, desc, field, result_list, search_depth, trace, out found_what);
            }
        }

        private bool ResultContains(string cat, string code, string desc, string field, string result_list) 
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContains(cat, code, desc, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContains(string cat, string code, string desc, string field, string result_list, SearchDepth search_depth) 
        {
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContains(cat, code, desc, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContains(string cat, string code, string desc, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what) 
        {
            if (ValueIsAList(result_list)) {
                return (CountResultInList(cat, code, desc, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            } else {
                return (CountSimpleResult(cat, code, desc, field, result_list, search_depth, trace, out found_what) > 0);
            }
        }


        private int CountResultNotInList(string cat, string code, string desc, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what) 
        {
            int count = 0, rec_count = 0;
            bool found_one;

            found_what = "";
            var arr = SplitOnCommaAndTrimElements(result_list);
            
            //Do not filter by result here; we need all results
            var query = StartNewQuery(search_depth);
            query = AndSimpleItemFilter(query, cat, code, desc, field, "");
            
            foreach(var item in query) {
                rec_count++;
                //Look for each search word in the result
                found_one = false;
                
                foreach (string s in arr) {
                    if (EqualOrContains(item.RESULT, s)) {
                        found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "' -- ignore this";
                        Program.VerboseAudit(found_what);
                        found_one = true;
                        break;
                    }
                }

                if (! found_one) {
                    //none of the words were found - good!
                    found_what = Describe(cat, code, desc, field, "", search_depth) + " -- does not contain '" + result_list + "'";
                    Program.VerboseAudit(found_what);
                    count++;
                    if (count_mode != CountMode.CountAll) break;
                }
            }
            
            if (count > 0) {
                //We already printed what was found; maybe show the total?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else if (rec_count > 0) {
                //We already printed what we ignored
            } else {
                //Print what did not exist
                found_what = Describe(cat, code, desc, field, "", search_depth);      //not found
                Program.VerboseAudit(found_what);
            }
            
            return count;
        }

        private bool CountResultDoesNotContain(string cat, string code, string desc, string field, string result_list, string found_what) 
        {
            // Search in the pull range with trace on
            return CountResultDoesNotContain(cat, code, desc, field, result_list, out found_what, SearchDepth.SearchDefault, true);
        }
        private bool CountResultDoesNotContain(string cat, string code, string desc, string field, string result_list, out string found_what, SearchDepth search_depth, bool trace)
        {
            return CountResultNotInList(cat, code, desc, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0;
        }

        private bool ResultDoesNotContain(string cat, string code, string desc, string field, string result_list, string found_what)
        {
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code, desc, field, result_list, out found_what, SearchDepth.SearchDefault, true);
        }
        private bool ResultDoesNotContain(string cat, string code, string desc, string field, string result_list, out string found_what, SearchDepth search_depth, bool trace)
        {
            return (CountResultNotInList(cat, code, desc, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0);
        }

        //Set the indicator if the result contains one of the words in the result_list
        private void SetIndIfResultContains(int inum, string cat, string code, string desc, string field, string result_list)
        {
            SetIndIfResultContains(inum, cat, code, desc, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultContains(int inum, string cat, string code, string desc, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already set
            if (_inds[inum].is_checked) return;
            
            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContains(cat, code, desc, field, result_list, search_depth, false, out found_what)) {
                SetInd(inum, found_what);                          //echo here
            } else {
                Program.VerboseAudit(found_what);                  //and here
            }
        }

        //Set the indicator if the result does not contain any of the words in result_list
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code, string desc, string field, string result_list)
        {
            SetIndIfResultDoesNotContain(inum, cat, code, desc, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code, string desc, string field, string result_list, SearchDepth search_depth)
        {   
            string found_what;
            //avoid more queries if the indicator is already set
            if (_inds[inum].is_checked) return;
            
            //Turn trace off for ResultDoesNotContain() and echo what was set below with SetInd
            if (ResultDoesNotContain(cat, code, desc, field, result_list, out found_what, search_depth, false)) {
                SetInd(inum, found_what);                           //echo here
            } else {
                Program.VerboseAudit(found_what);                   //and here
            }
        }

        //Clear the indicator if the result contains one of the words in the result_list
        private void ClrIndIfResultContains(int inum, string cat, string code, string desc, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code, desc, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfResultContains(int inum, string cat, string code, string desc, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already clear
            if (! _inds[inum].is_checked) return;
            
            //Turn trace off for ResultContains() and echo what was set below with SetInd
            if (ResultContains(cat, code, desc, field, result_list, search_depth, false, out found_what)) {
                ClrInd(inum, found_what);                           //echo here
            } else {
                Program.VerboseAudit(found_what);                   //and here
            }
        }


        //These slightly smaller functions are meant for places where you aren't really looking for a chart result
        private bool Exists(string cat, string code, string desc, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists(cat, code, desc, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool Exists(string cat, string code, string desc, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            return (CountResultContains(cat, code, desc, field, result_list, search_depth, trace, out found_what) > 0);
        }

        private void SetIndIfFound(int inum, string cat)
        {
            SetIndIfResultContains(inum, cat, "", "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code, string desc, string field, string result_list)
        {
            SetIndIfResultContains(inum, cat, code, desc, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code, string desc, string field, string result_list, SearchDepth search_depth)
        {
            SetIndIfResultContains(inum, cat, code, desc, field, result_list, search_depth);
        }

        private void ClrIndIfFound(int inum, string cat)
        {
            ClrIndIfResultContains(inum, cat, "","","","", SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code, string desc, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code, desc, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code, string desc, string field, string result_list, SearchDepth search_depth)
        {
            ClrIndIfResultContains(inum, cat, code, desc, field, result_list, search_depth);
        }


        //Get the max/total value from a result (usually in the middle of the text)
        private int GetIntValue(GetValueMode get_mode, string cat, string code, string desc, string field, string result_like)
        {
            return GetIntValue(get_mode, cat, code, desc, field, result_like, SearchDepth.SearchDefault);
        }
        private int GetIntValue(GetValueMode get_mode, string cat, string code, string desc, string field, string result_like, SearchDepth search_depth)
        {
            int value;
            int result = 0;
            bool found_one = false;

            var query = StartNewQuery(search_depth);
            query = AndSimpleItemFilter(query, cat, code, desc, field, result_like);
            
            //Look for a number in the result
            
            foreach(var item in query) {
                // RESULT might be null so convert to empty string so Split won't blow up
                var arr = PFSUtility.DBToString(item.RESULT).Split(' ');
                foreach (string s in arr) {
                    //Look for 1st character numeric; <type>.IsNumeric("60min") will fail so look at 1st char only
                    if (s.Left(1).IsNumeric()) {
                        value = (int)s.Val();                           //Use Val; ToInteger will error on "60min"
                        switch (get_mode) {
                            case GetValueMode.GetMax:
                                result = Math.Max(value, result);       //max
                                break;
                            case GetValueMode.GetTotal:
                                result += value;                        //total
                                break;
                            case GetValueMode.GetLast:
                                result = value;                         //last
                                break;
                        }
                        
                        // print what we are searching for (the first time)
                        if (! found_one) {
                            Program.VerboseAudit(Describe(cat, code, desc, field, result_like, search_depth));
                        }
                        found_one = true;
                        // print each value found
                        Program.VerboseAudit("  found numeric value " + result);
                        //Keep going in case there are more
                    }
                } 
            }
            
            if (! found_one) {
                //show what was not found
                Program.VerboseAudit(Describe(cat, code, desc, field, result_like, search_depth));
            }
            
            return result;
        }

        private int GetMaxValue(string cat, string code, string desc, string field, string result_like)
        {
            return GetMaxValue(cat, code, desc, field, result_like, SearchDepth.SearchDefault);
        }
        private int GetMaxValue(string cat, string code, string desc, string field, string result_like, SearchDepth search_depth)
        {
            return GetIntValue(GetValueMode.GetMax, cat, code, desc, field, result_like, search_depth);
        }

        private int GetTotalValue(string cat, string code, string desc, string field, string result_like)
        {
            return GetTotalValue(cat, code, desc, field, result_like, SearchDepth.SearchDefault);
        }
        private int GetTotalValue(string cat, string code, string desc, string field, string result_like, SearchDepth search_depth)
        {
            return GetIntValue(GetValueMode.GetTotal, cat, code, desc, field, result_like, search_depth);
        }

        // Get a result; returns true if found with return_result set
        private bool GetResult(string cat, string code, string desc, string field, out string return_result)
        {
            return GetResult(cat, code, desc, field, out return_result, SearchDepth.SearchDefault);
        }
        private bool GetResult(string cat, string code, string desc, string field, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndSimpleItemFilter(query, cat, code, desc, field, "");

            if (query.Count() > 0) {
                return_result = PFSUtility.DBToString(query.First().RESULT);
            } else {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code, desc, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }

        //======================================================================================
        // Mapping starts here
        //======================================================================================

        private void Check_1_2_3()
        {
            bool bath_complete=false, meal_complete=false, meal_NPO=false, activity_assist=false;
            int n;
            string tmp;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("1. ADL Self");
            Program.VerboseAudit("2. ADL Assist");
            Program.VerboseAudit("3. ADL Complete");
            Program.VerboseAudit("---------------");
            
            //Note: call these separately so the second one is not evaluated if the first one is true, etc.
            if (ResultContains("Hygiene", "", "", "Performance", "complete")) {                //most here
                bath_complete = true;
            } else if (ResultContains("NHx ADL/Sleep/Rest", "", "", "Bathing", "complete")) {     //some here
                bath_complete = true;
            }
            
            if (Exists("NHx Intake IV/Bld/Fdg", "", "Tube Fdg Amt/Type*", "", "")) {           //about 60%
                meal_complete = true;
            } else if (ResultContains("Diet", "", "", "Mealtime Assist", "manually fed")) {       //about 40%
                meal_complete = true;
            }
            
            meal_NPO = ResultContains("Diet", "", "", "Diet Type", "NPO");

            //Assist counts are not always given for these; must look for keywords
            tmp = "dog, hoyer, infant, needs assist, stretcher, toddler";
        //    if (ResultContains("Hygiene", "", "", "Level of Care", "assist")) {
        //        activity_assist = true;
            if (ResultContains("", "", "", "Activity Needs", tmp)) {
                activity_assist = true;
            } else if (ResultContains("", "", "", "Activity Tolerance", tmp)) {
                activity_assist = true;
            } else if (ResultContains("VHx Activity Protocol", "", "", "Activity Assistance Needs", tmp, SearchDepth.SearchSinceAdmission)) {
                activity_assist = true;
            }
                
            //These can tell us how many people are needed; stop if we reach 4.
            n = 0;
            if (n < 4) {                                 //Almost all here in activity protocol
                n = Math.Max(GetMaxValue("VHx Activity Protocol", "", "", "Activity Assistance Needs", "people", SearchDepth.SearchSinceAdmission), n);
            }
            if (n < 4) {
                n = Math.Max(GetMaxValue("", "", "", "Activity Needs", "people"), n);
            }
            if (n < 4) {
                n = Math.Max(GetMaxValue("", "", "", "Activity Tolerance", "assist"), n);
            }
            if (n < 4) {                                     //2011-04-07
                n = Math.Max(GetMaxValue("", "", "", "Ability/Tol", "assist"), n);
            }
            if (n < 4) {
                n = Math.Max(GetMaxValue("VHx Activity Protocol", "", "", "Activity Assistance Needs", "total assist", SearchDepth.SearchSinceAdmission), n);
            }
            _assist_count = n;                          //save for IND #5-6
            
            //
            // 3. ADL complete
            //
            if (_pat.age == 0) {
                //unknown -- ignore it
            } else if (_pat.age < 4) {
                SetInd(3, "age < 4yr");                           //70% of ind #3
            }

            if (bath_complete && meal_complete) {
                SetInd(3, "complete bath and meal");              //21%
            }
            if (bath_complete && meal_NPO) {
                SetInd(3, "complete bath and meal NPO");          //9%
            }

            if (_inds[3].is_checked) return;
            
            //
            // 2. ADL assist
            //
            if (_assist_count >= 1) SetInd(2, "assist >= 1");       //approx 30% ind #2
            if (bath_complete) SetInd(2, "bath complete");             //approx 30%
            if (meal_complete) SetInd(2, "meal complete");             //approx 2.5%
            if (activity_assist) SetInd(2, "activity assist");         //approx 0.5%
            
            SetIndIfResultContains(2, "", "", "", "Performance", "assist, partial");
            SetIndIfResultContains(2, "", "", "", "Performed by", "assist");
            SetIndIfResultContains(2, "", "", "", "Mealtime Assist", "cut food, verbal directions");
            SetIndIfResultContains(2, "NHx ADL/Sleep/Rest", "", "", "", "assist");
            
            if (_inds[2].is_checked) return;
            
            //
            // 1. ADL self
            //
            SetIndIfResultContains(1, "", "", "", "Mealtime Assist", "self");             //most here
            SetIndIfResultContains(1, "", "", "", "Performance", "self");
            SetIndIfResultContains(1, "NHx ADL/Sleep/Rest", "", "", "", "independent");
        }


        private void Check_4()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("4. ADL Rehab");
            Program.VerboseAudit("---------------");
            
            SetIndIfFound(4, "Eval Inpatient Physical Therapy", "", "", "Home Recommendations", "", SearchDepth.SearchSinceAdmission);
            SetIndIfFound(4, "Cardiac Rehab Evaluation","","","","",SearchDepth.SearchSinceAdmission);
            //**These not used?
            SetIndIfFound(4, "Activity Rehab");
            SetIndIfFound(4, "NHx RHB Inital Evaluation","","","","",SearchDepth.SearchSinceAdmission);
            SetIndIfFound(4, "PHx Speech Tx Goals","","","","",SearchDepth.SearchSinceAdmission);
            SetIndIfFound(4, "Restorative Rehab Referral","","","","",SearchDepth.SearchSinceAdmission);
            SetIndIfFound(4, "NHx RHB OT Initial Eval","","","","",SearchDepth.SearchSinceAdmission);
            SetIndIfFound(4, "NHx RHB ROM TREAT");
            SetIndIfFound(4, "NHx RHB All But ROM");
            SetIndIfFound(4, "NHx RHB All TREAT");
            SetIndIfFound(4, "NHx RHB TREAT All");
            SetIndIfFound(4, "NHx RHB Rounds");
            SetIndIfFound(4, "NHx RHB OT Treat");
        }


        private void Check_5_6()
        {    
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("5. ADL 2-3 Caregivers");
            Program.VerboseAudit("6. ADL 4 or more Caregivers");
            Program.VerboseAudit("---------------");
            
            if (_assist_count >= 4) {
                SetInd(6, "assistance 4 or more persons");
            } else if (_assist_count >= 2) {
                SetInd(5, "assistance 2-3 persons");
            } else {
                Program.VerboseAudit("not set because assistance < 2 persons");            //add to Program.Audit
            }
        }

        private void Check_7()
        {
        //    bool hard_of_hearing, hearing_aid
        //    bool blind, ignore
        //    string tmp;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("7. Communication");
            Program.VerboseAudit("---------------");

            //This takes the guesswork out of levels of blind/deaf with glasses/hearing aid
            SetIndIfResultContains(7, "VHx Admitting Information", "", "", "Communication Impaired?", "yes", SearchDepth.SearchSinceAdmission);
            
            SetIndIfResultContains(7, "PHx Language Spoken", "", "", "Language Spoken", "sign language, spanish, other, w/gestures, w/board, nonverbal, unable to speak english", SearchDepth.SearchSinceAdmission);
            //These are great because they have been charted as a barrier:
            SetIndIfResultContains(7, "VHx Educ/Learning", "", "", "Barriers to Educ/Plan #2", "deaf, hard of hearing, hard to understand, language", SearchDepth.SearchSinceAdmission);
            
            SetIndIfResultDoesNotContain(7, "NHx Cognitive/Learning", "", "Speech*", "Ability", "has no difficulty speaking", SearchDepth.SearchSinceAdmission);
            
            // "unable to determine" = communication problem
            SetIndIfResultContains(7, "NHx Neuro/Muscle", "", "", "Orientation", "unable to determine");

            SetIndIfResultDoesNotContain(7, "NHx Cognitive/Learning", "", "", "Speech_Speech*", "no abnormalities", SearchDepth.SearchSinceAdmission);
            SetIndIfResultContains(7, "VHx Educ/Learning", "", "", "Education Level", "illiterate", SearchDepth.SearchSinceAdmission);
            SetIndIfResultContains(7, "NHx Assessment", "", "Neurological Assessment", "", "phasia", SearchDepth.SearchSinceAdmission);
            

        //    if (inds[7].is_checked) return;
        //
        //    //
        //    //Blind(ness) may be charted, but it can be cancelled by glasses or blindness in only one eye.
        //    //
        //    blind = ResultContains("VHx Admitting Information", "", "", "Vision Hx", "blind", SearchMode.SearchSinceAdmission)
        //    if (! blind) {
        //        blind = ResultContains("NHx Sensory/Perceptual", "", "", "Visual Disturbance", "blind, blurred, tunnel", SearchMode.SearchSinceAdmission)
        //    }
        //
        //    if (blind) {
        //        //look for glasses for left/right
        //        tmp = "glasses,left,right"
        //        ignore = ResultContains("VHx Admitting Information", "", "", "Vision Hx", tmp, SearchMode.SearchSinceAdmission)
        //        if (! ignore) {
        //            ignore = ResultContains("NHx Sensory/Perceptual", "", "", "Visual Disturbance", tmp, SearchMode.SearchSinceAdmission)
        //        }
        //
        //        if (! ignore) {
        //            SetInd(7, "blindness"
        //        } else {
        //            Program.VerboseAudit("partial blindness + glasses -- ignore"
        //        }
        //    }
        //
        //    //
        //    //Deaf(ness) may be charted, but it can be cancelled by hearing aids
        //    //
        //    hard_of_hearing = ResultContains("VHx Admitting Information", "", "", "Hearing Deficits", "deaf, hard of hearing", SearchMode.SearchSinceAdmission)
        //    if (! hard_of_hearing) {
        //        hard_of_hearing = ResultContains("NHx Sensory/Perceptual", "", "", "", "deaf, hard of hearing", SearchMode.SearchSinceAdmission)
        //    }
        //
        //    if (hard_of_hearing) {
        //        //Look for "hearing aid"
        //        hearing_aid = ResultContains("VHx Admitting Information", "", "", "Hearing Deficits", "hearing aid", SearchMode.SearchSinceAdmission)
        //        if (! hearing_aid) {
        //            hearing_aid = ResultContains("NHx Sensory/Perceptual", "", "", "", "hearing aid", SearchMode.SearchSinceAdmission)
        //        }
        //
        //        if (! hearing_aid) {
        //            SetInd(7, "deaf / hard of hearing"
        //        } else {
        //            Program.VerboseAudit("hard of hearing with hearing aid -- ignore"
        //        }
        //    }
        }

        private void Check_8()
        {
            string tmp;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("8. Cognitive Support");
            Program.VerboseAudit("---------------");

            //charted once at admission - very common
            SetIndIfResultContains(8, "VHx Educ/Learning", "", "", "Barriers to Educ/Plan #2", "cognitive, confused", SearchDepth.SearchSinceAdmission);
            
            tmp = "confused, disoriented, reoriented, forgetful, does not recognize parent";
            SetIndIfResultContains(8, "NHx Neuro/Muscle", "", "", "Orientation", tmp);                    //common
            SetIndIfResultContains(8, "NHx Neuro/Muscle", "", "", "Loc", tmp);                            //some
            SetIndIfResultContains(8, "NHx Education/Instruction", "", "", "Education Response", tmp);    //some
            SetIndIfResultContains(8, "", "", "", "Shift Comment", tmp);                                  //some
            SetIndIfResultContains(8, "NHx Cognitive/Learning", "", "", "Orientation", tmp);
            
            //**not needed or redundant?
            //SetIndIfResultContains(8, "", "", "", "Activity Tolerance", tmp);
            //SetIndIfResultContains(8, "", "", "", "Patient Status", tmp);
            //SetIndIfResultContains(8, "NHx Education/Instruction", "", "", "Learning Readiness", "cognitive barrier");
            
            //Restraint based on MD order for impairment, confusion, etc.
            SetIndIfResultContains(8, "Restraint Application (Med/Surg Healing)", "", "", "Reason", "Neurological impairment due to anesthesia, Confusion, Altered level of consciousness");
        }

        private void CheckBehavior(int count, string desc)
        {
            double los_hours;
            
            if (_inds[10].is_checked) return;             //skip if highest already checked
            if (count == 0) return;

            los_hours = _pat.range / 60.0;
            switch (FreqForCount(los_hours, count))
            {
                case Frequencies.Q1H:
                case Frequencies.Q30M:
                    SetInd(10, desc + " q1h");
                    break;
                default:
                    SetInd(9, desc);
                    break;
            }
        }

        private void Check_9_10()
        {
            string tmp;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("9.  Behavior/Emotional Management");
            Program.VerboseAudit("10. Behavior/Emotional Mgmt - q 1 Hour");
            Program.VerboseAudit("---------------");

            //
            // 10.       -- (none found)
            //
            //These are automatic Q1h
            SetIndIfResultContains(10, "NHx Psychological", "", "", "Anxiety Scale", "severe, panic");
            SetIndIfResultContains(10, "NHx Psychological", "", "", "Anxiety Relief Measure Results", "no relief");
            //Note: these restraints are specifically for behavior; restraints policy is q30m
            SetIndIfFound(10, "Behavioral Restraint Application (Behavioral Health Reasons)");
            SetIndIfFound(10, "Behavioral Restraint Maintenance (Behavioral Health Reasons)");
            if (_inds[10].is_checked) return;
            
            //These are based on frequency
            CheckBehavior(CountSimpleResult("Anxiety Support", "", "", "Method", ""), "Anxiety Support");
            CheckBehavior(CountSimpleResult("Emotional Support", "", "", "Method", ""), "Emotional Support");
            if (_inds[10].is_checked) return;
            
            
            //
            // 9.
            //
            tmp = "agitated,anxious,confused,depressed,forgetful,deficiency,limited";
            SetIndIfResultContains(9, "", "", "", "Mental Status", tmp);                              //most
            tmp = "anxious,combative,crying,depressed,fearful,inappropriate,uncooperative";
            SetIndIfResultContains(9, "", "", "", "Patient Status", tmp);                             //some
            SetIndIfResultContains(9, "NHx Neuro/Muscle", "", "", "Activity/Behavior", tmp);          //little
            SetIndIfResultContains(9, "NHx Cognitive/Learning", "", "", "General Behavior", tmp);     //**not needed?
            tmp = "agitated,angry,anxious,bored,depressed,dissatisfied,emptiness,fearful,guilt,helpless,impatient,impending doom,inadequacy,irritable,lethargic,nervous,no control,powerless,rejected,sad,tearful,tense,can't relax,worried,worthless";
            SetIndIfResultContains(9, "NHx Feelings", "", "", "Feelings", tmp);                       //**not needed?
            if (_inds[9].is_checked) return;

            //some
            CheckBehavior(CountSimpleResult("NHx Psychological", "", "", "Anxiety Relief Measures", ""), "Anxiety Relief Measures");
        }

        private void CheckSafety(int count, string desc)
        {
            double los_hours;
            
            if (_inds[12].is_checked) return;             //skip if highest already checked
            if (count == 0) return;

            los_hours = _pat.range / 60.0;
            
            switch (FreqForCount(los_hours, count))
            {
            case Frequencies.Q30M:
                SetInd(12, desc + " q30min");
                break;
            case Frequencies.Q1H:
            case Frequencies.Q2H:
                SetInd(11, desc + " q2h");
                break;
            default:
                Program.VerboseAudit(desc + ": " + count + " in " + Math.Round(los_hours, 0) + " hours is not enough");
                break;
            }
        }

        private void Check_11_12()
        {    
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("11. Safety Management - q 2 Hours");
            Program.VerboseAudit("12. Safety Management - q 30 Minutes");
            Program.VerboseAudit("---------------");

            //
            // 12.
            //
            SetIndIfResultContains(12, "", "", "", "Patient Status", "sitter, adult supervision");    //very common
            if (_inds[12].is_checked) return;
            
            //**not needed?  (none used)
            SetIndIfResultContains(12, "NHx Safety", "", "", "Safety Measures", "sitter");
            //Note: Arnot restraints policy is q30m
            SetIndIfFound(12, "Restraint Application (Med/Surg Healing)");
            SetIndIfFound(12, "Restraint Maintenance (Med/Surg Healing)");
            SetIndIfResultContains(12, "NHx Safety", "", "", "Safety Measures", "restraint");
            //SetIndIfResultContains(12, "", "", "Shift Comment", "restraint"     //could mean on or off
            if (_inds[12].is_checked) return;
            SetIndIfResultContains(12, "Continuum of Care First Contact", "", "", "Referral Reason", "suicide attempt", SearchDepth.SearchSinceAdmission);
            if (_inds[12].is_checked) return;

            //frequency based
            CheckSafety(CountSimpleResult("NHx Safety", "", "", "Safety Measures", ""), "NHx Safety");
            if (_inds[12].is_checked) return;

            //
            // 11.
            //
            //search for persistent items
            SetIndIfResultContains(11, "NHx Safety", "", "", "Fall Prevention Program", "FPP", SearchDepth.SearchSinceArrival);   //some
            SetIndIfResultContains(11, "NHx Injury/Fall/Risk?", "", "Morse Fall Risk Assessment", "Intervention Level", "medium, high", SearchDepth.SearchSinceArrival);
        }

        private void Check_13()
        {    
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("13. Isolation");
            Program.VerboseAudit("---------------");
            
            SetIndIfFound(13, "Precautions/Isolation Maintenance");           //majority
            SetIndIfFound(13, "Precautions/Isolation Setup");                 //some
            ClrIndIfFound(13, "Isolation Discontinued");
        }

        private void CheckAssessment(int count, string desc)
        {
            double los_hours;

            if (_inds[17].is_checked) return;             //skip if highest already checked
            if (count == 0) return;                    //skip if none

            //Here's the mathematical way:
            //    freq_min = g_util.DivideWithoutError(m_pat.range, count)
            //The book, however, says that you did this frequency for the majority of the time period.
            //This could mean that you did it for only 51% of the period.  Use a lookup table instead.
            
            los_hours = _pat.range / 60.0;
            
            switch (FreqForCount(los_hours, count))
            {
            case Frequencies.Q30M:
                SetInd(17, desc + " q30min");
                break;
            case Frequencies.Q1H:
                SetInd(16, desc + " q1h");
                break;
            case Frequencies.Q2H:
                SetInd(15, desc + " q2h");
                break;
            case Frequencies.Q4H:
                SetInd(14, desc + " q4h");
                break;
            case Frequencies.QNONE:
                Program.VerboseAudit(desc + ": " + count + " in " + Math.Round(los_hours, 0) + " hours is not enough");
                break;
            }
        }

        private void Check_14_15_16_17()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("14. Assessment q4h");
            Program.VerboseAudit("15. Assessment q2h");
            Program.VerboseAudit("16. Assessment q1h");
            Program.VerboseAudit("17. Assessment q30min");
            Program.VerboseAudit("---------------");
            
            // "like" assessments are grouped here

            CheckAssessment(CardioManagementCount(), "Cardio Management");          //most
            if (_inds[17].is_checked) return;
            CheckAssessment(MedicationManagementCount(), "Medication Management");  //tie
            if (_inds[17].is_checked) return;
            CheckAssessment(FluidManagementCount(), "Fluid Management");            //tie
            if (_inds[17].is_checked) return;
            CheckAssessment(PulmonaryManagementCount(), "Pulmonary Management");    //some
            if (_inds[17].is_checked) return;
            CheckAssessment(NeuroManagementCount(), "Neuro Management");            //less
            if (_inds[17].is_checked) return;

            //Give ICU have a minimum of q1h
            if (_pat.unit_name.Right(3) == "ICU") SetInd(16, "ICU default");       //most #16

            if (_inds[16].is_checked || _inds[17].is_checked) return;
            // Look for q1h or lower
            
            //**not used?
            SetIndIfFound(16, "Medication Reconciliation Transfer");      //10/22/10; needs verification
            SetIndIfFound(16, "Sedation Check");                          //03/30/11; needs verification
            
            if (_inds[15].is_checked || _inds[16].is_checked || _inds[17].is_checked) return;
            //Look for q4h

            //Make this a minimum of q4h no matter what the charted frequency is
            SetIndIfFound(14, "NHx CV - Cardiac Rhythm", "", "", "Rhythm","");       //some
        }


        //=====================================================================
        // Group like assessments into buckets of time
        // Each bucket counts as one assessment in the frequency count

        private int CountAssessmentBuckets(List<int> bucket_list)       // bucket list: (ha ha)
        {
            int result = bucket_list.Distinct().Count();
            Program.VerboseAudit(result + " unique time buckets");
            return result;
        }

        //Add to the list of assessment buckets (redundant buckets are fine)
        private void AddBuckets(List<int> bucket_list, string cat, string code, string desc, string field)
        {
            AddBuckets(bucket_list, cat, code, desc, field, "");
        }
        private void AddBuckets(List<int> bucket_list, string cat, string code, string desc, string field, string result_list)
        {
            // Assign (virtual) bucket numbers to each assessment.  
            // Every x minutes after that is a new bucket.  Bucket zero = pull_start time.

            // get the chart items for the assessments
            var query = StartNewQuery();
            query = AndSimpleItemFilter(query, cat, code, desc, field, result_list);
            // figure out what buckets they belong to
            var query2 = from item in query
                         select new {
                            bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / FREQUENCY_BUCKET_SIZE)
                         };
            // Add to the list
            foreach (var item in query2)
            {
                bucket_list.Add(item.bucket);
            }

            // This is just for auditing: Say what we are looking for and how many were found
            if (!String.IsNullOrEmpty(result_list)) {
                // print each word and if it was found or not
                int i = CountResultContains(cat, code, desc, field, result_list);
            } else {
                // print how many were found
                Program.VerboseAudit(Describe(cat, code, desc, field, ""));
            }
        }

        private int CardioManagementCount()
        {
            //Try to restict all searches to one field (the last argument below) or we will be flooded with results.
            //The extra results won't hurt the calc, but they will slow things down and clog the Program.Audit.

            Program.VerboseAudit("");
            Program.VerboseAudit("** Cardio Management **");

            var buckets = new List<int>();
            AddBuckets(buckets, "", "", "", "Temp");
            AddBuckets(buckets, "", "", "", "Pulse");
            AddBuckets(buckets, "", "", "", "BP");
            AddBuckets(buckets, "Pulse Oximetry", "", "", "O2 Sat");
            AddBuckets(buckets, "NHx Assessment", "", "", "Cardiovascular Assessment");
            AddBuckets(buckets, "NHx Assessment", "", "", "Cardio/Respiratory Assessment");
            AddBuckets(buckets, "NHx CV - Cardiac Rhythm", "", "", "Rhythm");
            AddBuckets(buckets, "Post Heart Catheterization Assessment", "", "", "Location");
            AddBuckets(buckets, "Post PCI Assessment", "", "", "Location");       //percutaneous corinary intervention

            return CountAssessmentBuckets(buckets);
        }

        private int NeuroManagementCount()
        {
            Program.VerboseAudit("");
            Program.VerboseAudit("** Neuro Management **");

            var buckets = new List<int>();
            AddBuckets(buckets, "NHx Assessment", "", "", "Neurological Assessment");
            AddBuckets(buckets, "NHx Assessment", "", "", "Neurovascular Assessment");
            AddBuckets(buckets, "NHx Neuro/Muscle", "", "", "Loc");

            return CountAssessmentBuckets(buckets);
        }

        private int PulmonaryManagementCount() 
        {
            Program.VerboseAudit("");
            Program.VerboseAudit("** Pulmonary Management **");

            var buckets = new List<int>();
            AddBuckets(buckets, "", "", "", "Resp");
            //Note: just because we find Incentive Spirometer doesn't mean they did it; we have to search the result also
            AddBuckets(buckets, "Routine Check Nursing", "", "", "Incentive Spirometer", "independent, encourage, observe, supervision, used, volume");
            //There are lot of fields in the assessment; limit to Cough to speed things up.
            AddBuckets(buckets, "Respiratory Therapy Assessment", "", "", "Cough");
            AddBuckets(buckets, "NHx Ped - Resp Assessment", "", "", "Cough");
            AddBuckets(buckets, "NHx Assessment", "", "", "Respiratory Pain Assessment");
            AddBuckets(buckets, "Airway Suction", "", "", "Performance");

            return CountAssessmentBuckets(buckets);
        }

        private int FluidManagementCount()
        {
            //The problem with I/O is that we will get a large number of measurements (q1h)
            //but no assessments that compare I/O?  (we are looking for assessments)
            
            Program.VerboseAudit("");
            Program.VerboseAudit("** Fluid Management **");

            var buckets = new List<int>();
            //Almost all I&O have Amt or Amount.  (don't have to list all the categories)
            AddBuckets(buckets, "", "", "", "Amt");
            AddBuckets(buckets, "", "", "", "Amount");
            AddBuckets(buckets, "", "", "", "Drainage Amt");
            //be specific here; "performance" is used in other areas
            AddBuckets(buckets, "Foley Output CA", "", "", "Performance");

            return CountAssessmentBuckets(buckets);
        }

        private int MedicationManagementCount()
        {
            Program.VerboseAudit("");
            Program.VerboseAudit("** Medication Management **");

            var buckets = new List<int>();
            AddBuckets(buckets, "PHx Hx - Medication", "", "", "Drug");
            AddBuckets(buckets, "NHx Sensory/Perceptual", "", "Pain Assessment*", "Pain Level");
            AddBuckets(buckets, "Morphine PCA", "", "", "Comment");
            AddBuckets(buckets, "Morphine PCA", "", "", "Sedation Level");
            AddBuckets(buckets, "Dilaudid (Hydromorphone) PCA", "", "", "Comment");
            AddBuckets(buckets, "Dilaudid (Hydromorphone) PCA", "", "", "Sedation Level");
            AddBuckets(buckets, "Insulin, Humalog - Sliding Scale", "", "", "Site");
            AddBuckets(buckets, "Bedside Glucose POC (Insulin Drip) ", "", "", "Glucose");
            AddBuckets(buckets, "Bedside Glucose POC", "", "", "Glucose");

            return CountAssessmentBuckets(buckets);
        }


        private void Check_18()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("18. Medication Preparation >= 20 Minutes");
            Program.VerboseAudit("---------------");
            
            SetIndIfResultContains(18, "NHx Activity Protocol", "", "", "Meds > 20 min", "yes");  //most here
            SetIndIfFound(18, "Transfusion for Inpatient");         //"Blood Transfusion for Inpatient" 04/01/11
        }

        private void CheckWound(int total)
        {
            if (_inds[20].is_checked) return;             //skip if highest already checked

            if (total == 0) {
                //skip it
            } else if (total >= 30) {
                SetInd(20, "wound >= 30 min");
            } else {
                SetInd(19, "wound < 30 min");
            }
        }

        private void Check_19_20()
        {
            int n;
            
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("19. Wound/Injury Mgmt");
            Program.VerboseAudit("20. Wound/Injury Mgmt >= 30 Minutes");
            Program.VerboseAudit("---------------");

            if (_pat.age * 365.24219 < 14) {                               //10/22/2010
                SetInd(19, "age < 2 weeks");
                return;
            }

            //Total the wound care with duration
            n = GetTotalValue("Incision/Wound Care", "", "", "Duration", "");    //almost 100% found
            CheckWound(n);
            if (_inds[20].is_checked) return;                                   //quick exit
            //Look for variations
            CheckWound(n +
                GetTotalValue("NHx Skin", "", "", "Duration", "") +
                GetTotalValue("Dressing Change", "", "", "Duration", ""));
            if (_inds[20].is_checked || _inds[19].is_checked) return;
            
            //Look for wound care without a duration
            SetIndIfFound(19, "NHx Skin", "", "Incision/Wound*","","");
            SetIndIfFound(19, "NHx Skin", "", "Incision*","","");
            SetIndIfFound(19, "NHx Skin", "", "", "", "skin integrity impaired");
            SetIndIfFound(19, "Wound Care RX");
            SetIndIfFound(19, "Incision/Wound Care");
            //Look for maternal wound care, or something that imples it
            SetIndIfFound(19, "Postpartum Void Check", "", "", "Pericare","");    //04/26/11
            SetIndIfFound(19, "NHx OB Delivery Incision");                        //05/10/11
            //SetIndIfResultDoesNotContain(19, "VHx OB Delivery + Incision Types", "", "", "Delivery Incision Type", "no incision"
            SetIndIfFound(19, "VHx OB Delivery + Incision Types");                //05/10/11
            SetIndIfFound(19, "NHx OB Pregnancy/Birth");                          //05/10/11
        }

        private int EstimateEducationMins(string chart_result)
        {
            int mins=0, count=0;

            //Most results have commas between subjects but some just run on without punctuation.
            //Split the results on commas but look for all keywords in each substring.
            
            // Convert the search string to lower case and seach for lower case
            var arr = chart_result.ToLower().Split(',');

            foreach(string s in arr) {
                if (!String.IsNullOrEmpty(s)) {
                    if (s.Contains("chf:")) mins += 40;
                    if (s.Contains("guaiac")) mins += 5;
                    
                    if (s.Contains("incentive spirometer")) mins += 10;
                    if (s.Contains("use of mdi")) mins += 15;
                    if (s.Contains("peak flow")) mins += 10;
                    if (s.Contains("svn")) mins += 5;
                    if (s.Contains("volume expansion")) mins += 10;
                    if (s.Contains("mucous clearing")) mins += 15;
                    
                    if (s.Contains("first time mother")) mins += 60;
                    if (s.Contains("first time breastfeeding")) mins += 60;
                    //ignore all others
                    count++;                //count non-null substrings (beware empty commas)
                }
            } 
            
            if (mins == 0) mins = 5;         //count * 5

            Program.VerboseAudit("Est " + mins + " mins for " + chart_result);

            return mins;
        }

        private void CheckEducation(int total)
        {
            if (_inds[21].is_checked) return;             //skip if already checked

            if (total >= 60) {
                SetInd(21, "education >= 60 min");
            }
        }

        private void Check_21()
            {
            int mins, n;
            string chart_result;
            
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("21. Healthcare Mgmt Education >= 1 Hour");
            Program.VerboseAudit("---------------");

            n = GetTotalValue("NHx Education/Instruction", "", "", "Duration of Teaching Nursing", "");  //almost 100%
            CheckEducation(n);
            if (_inds[21].is_checked) return;

            mins = n +
                GetTotalValue("NHx Education/Instruction", "", "", "Duration", "") +
                GetTotalValue("NHx Education/Instruction(ET)", "", "", "Duration", "") +
                GetTotalValue("NHx Education/Instruction", "", "", "Dietary Duration of teaching", "") +
                GetTotalValue("NHx Activity Protocol", "", "", "Education Time", "");
            CheckEducation(mins);
            if (_inds[21].is_checked) return;

            //No duration given?  Make an estimate
            if (mins == 0) {
                if (GetResult("NHx Education/Instruction", "", "", "Area of Instruction", out chart_result)) {
                    mins = mins + EstimateEducationMins(chart_result);
                }
                if (GetResult("NHx Education/Instruction(ET)", "", "", "Area of Instruction", out chart_result)) {
                    mins = mins + EstimateEducationMins(chart_result);
                }
                if (GetResult("NHx Education/Instruction(ET)", "", "", "Area of Instruction - Guaiac", out chart_result)) {
                    mins = mins + EstimateEducationMins(chart_result);
                }
            }
            
            CheckEducation(mins);
            if (! _inds[21].is_checked) {
                Program.VerboseAudit("Education < 60 min; (" + mins + " min total)");
            }
        }

        private void Check_22()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("22. 1 to 1 Physiological Interv. >= 2 Hours");
            Program.VerboseAudit("---------------");
           
            SetIndIfResultContains(22, "NHx - Acuity Plus indicators", "", "", "Physio Intervention", "2 hrs or more");
        }


        private void CheckProcs()
        {
            // No procedures are mapped at this time
            // If they were, we could add to the list of procedures like this:
            var p = new proc_data();
            p.start = _pat.pull_start;
            p.finish = p.start;
            p.procedure_number = 4;
#if EXAMPLE
            _procs.Add(p);
#endif
        }

        private void AtLeastOneADL()
        {
            if (! (_inds[1].is_checked || _inds[2].is_checked || _inds[3].is_checked)) {
                // Make "#2 ADL - Assist" the default.  (90% of patients)
                SetInd(2, "at least one ADL");
            }
        }

        private void HighestIndicatorInEachGroupWins()
        {
            int i, g;
            bool highest_is_on;
            string ind_list;

            Program.VerboseAudit("---------------");
            Program.Audit("Select highest indicator in each group");
            
            g = 0;
            highest_is_on = false;
            // Go from bottom up - highest (lowest) indicator in each group wins
            for (i = MAX_INDS; (i >= 1); i--) {
                if (_inds[i].radio_group > 0) {
                    if (_inds[i].radio_group != g) {
                        //this is a new group
                        g = _inds[i].radio_group;
                        highest_is_on = _inds[i].is_checked;
                    } else {
                        //same group
                        if (highest_is_on) {
                            _inds[i].is_checked = false;             //uncheck a lower number
                        } else {
                            highest_is_on = _inds[i].is_checked;     //save this one
                        }
                    }
                }
            } // next i
            
            //Echo the indicators for an Program.Audit (no classification will be saved)
            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++) {
                if (_inds[i].is_checked) ind_list += "," + i;
            } // next i
            
            Program.Audit("Final list = " + ind_list.Substring(1));
        }

        private const string DATETIME_FORMAT = "yyyyMMddHHmm";              // ISO Date/Time w/o seconds

        private void OutputClass()
        {
            string outstr, ind_list, desc, str_pull_dt;
            int i, tc_event_id;

            tc_event_id = PFSUtility.NextGID();                             //get a unique id for this class
            str_pull_dt = _pat.pull_finish.ToString(DATETIME_FORMAT);

            outstr = "".FixedWidth(8);                                      //(facility code)
            outstr +="|" + _pat.unit_name.FixedWidth(16);
            outstr +="|" + "".FixedWidth(16);                               //(unit code)
            outstr +="|" + "".FixedWidth(16);                               //(area code)
            outstr +="|" + "".FixedWidth(8);                                //(class date - give datetime instead)
            outstr +="|" + _pat.acct.FixedWidth(20);
            outstr +="|" + _pat.last_name.FixedWidth(32);
            outstr +="|" + _pat.first_name.FixedWidth(32);
            outstr +="|" + _pat.middle_name.FixedWidth(32);
            outstr +="|" + _pat.room.FixedWidth(8);
            outstr +="|" + _pat.bed.FixedWidth(4);
            outstr +="|" + str_pull_dt.FixedWidth(12);                      //class datetime (could change)
            outstr +="|" + "".FixedWidth(16);                               //(login)
            outstr +="|" + str_pull_dt.FixedWidth(16);                      //(employee)/(pull datetime)
            outstr +="|" + _pat.meth_id.ToString().FixedWidth(4);
            outstr +="|" + "C".FixedWidth(1);                               //record type = class
            outstr +="|" + "".FixedWidth(4);                                //(stage)
            outstr +="|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
            outstr +="|" + _pat.range.ToString().FixedWidth(4);             //TC pull range
            outstr +="|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
            outstr +="|";
            outstr = outstr.FixedWidth(294);
            outstr +="|" + _pat.effective.ToString(DATETIME_FORMAT);        //IN
            outstr = outstr.FixedWidth(377);
            outstr +="|";
            
            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++) {
                if (_inds[i].is_checked) {
                    outstr += "Y";
                    ind_list += "," + i;
                } else {
                    outstr += "N";
                }
            } // next i
            ind_list = ind_list.Substring(1);                           //strip leading comma
  
            Program.outfile.WriteLine(outstr);                          //output to transparent.txt
            
            //Save the selected indicators plus both Program.Audits in the event log; link with tc_event_id
            desc = "Classified: " + ind_list;

            PFSEventLog.AddTransparentMappingEventLogEntry(
                desc, Program.gLogUnitID, Program.gLogEncounterID,
                tc_event_id, Program.gLogMapperVersion, 
                Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
        }

        private void OutputProcs()
        {
            int i, j;
            string outstr, proc_list, desc;
            int tc_event_id;

            for (i = 0; (i < _numprocs); i++) {
                tc_event_id = PFSUtility.NextGID();                             //get a unique id for this proc

                outstr = "".FixedWidth(8);
                outstr += "|" + _pat.unit_name;                                 //10
                outstr = outstr.FixedWidth(69);
                outstr += _pat.acct;
                outstr = outstr.FixedWidth(89);
                outstr += "|" + _pat.acct.FixedWidth(20);                       //90
                outstr += "|" + _pat.last_name.FixedWidth(32);
                outstr += "|" + _pat.first_name.FixedWidth(32);
                outstr += "|" + _pat.middle_name.FixedWidth(32);
                outstr = outstr.FixedWidth(203);
                outstr += _procs[i].start.ToString(DATETIME_FORMAT);            //204 proc dt
                outstr = outstr.FixedWidth(255);
                outstr += "P";                                                  //256 procedure type record
                outstr += "|" + "".FixedWidth(4);                               //(stage)
                outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);     //TC source ID
                outstr += "|" + _pat.range.ToString().FixedWidth(4);            //TC pull range
                outstr += "|" + tc_event_id.ToString().FixedWidth(10);          //TC event ID
                outstr += "|";
                outstr = outstr.FixedWidth(295);
                outstr += _procs[i].start.ToString(DATETIME_FORMAT);            //296 procdt in
                outstr = outstr.FixedWidth(347);
                outstr += _procs[i].finish.ToString(DATETIME_FORMAT);           //348 procdt out
                outstr = outstr.FixedWidth(378);
                
                proc_list = "";
                for (j = 0; (j < MAX_PROCS); j++) {
                    if (_procs[i].procedure_number == j) {
                        outstr +="Y";
                        proc_list += "," + j;
                    } else {
                        outstr +="N";
                    }
                } // next j
                proc_list = proc_list.Substring(1);                             //strip leading comma

                Program.outfile.WriteLine(outstr);                              //output to transparent.txt
                
                //Save the selected procedures plus both Program.Audits in the event log; link with tc_event_id
                //(this assumes the procedure Program.Audit is mixed in with the class Program.Audit)
                desc = "Procedure: " + proc_list;
                PFSEventLog.AddTransparentMappingEventLogEntry(desc, Program.gLogUnitID, Program.gLogEncounterID,
                    tc_event_id, Program.gLogMapperVersion,
                    Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
            } // next i
        }


    }
}
