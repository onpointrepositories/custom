VERSION 5.00
Begin VB.Form frmMain 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Transparent Mapping"
   ClientHeight    =   825
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   5835
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   825
   ScaleWidth      =   5835
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Label lblProgress 
      Alignment       =   2  'Center
      Caption         =   "progress"
      Height          =   435
      Left            =   120
      TabIndex        =   0
      Top             =   240
      Width           =   5655
      WordWrap        =   -1  'True
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'
' This little window shows the mapping progress if the program is run interactively
'
Private Sub Form_Load()
    g_display.SetFontInAllControls Me, g_display.MessageFont
    Set Me.Icon = g_util.QuadraMedIcon()
    Me.Caption = g_util.AppCaption()
    lblProgress.Caption = "loading..."
End Sub

Public Sub SetProgress(msg As String)
    lblProgress.Caption = msg
    DoEvents
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If (UnloadMode = vbFormControlMenu) Then
        If MsgBox("Do you want to cancel the mapping in progress?", vbQuestion + vbYesNo) = vbYes Then
            g_abort = True
        End If
        Cancel = 1
    End If
End Sub
