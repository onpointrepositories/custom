﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;                     // for LINQ; add a reference
using System.Data.SqlClient;                // for SqlConnection
using System.IO;                            // for Directory, Path, Stream
using PfsShared;                            // add a reference to Shared2 project
//
// MCCG Inpatient 2.0 transparent mapping
//
// This processes one patient.  Only one database query is used.  :)
//
// All search functions use exact match for category, description and field name.
// Codes and results can be a single word or a list of words.
// Codes default to exact match; prefix with CODE_CODE_LIKE_PREFIX if desired (single word only)
// Results default to like matching; prefix with RESULT_EXACT_MATCH_PREFIX if desired (single word only)
//
// All searches are case insensitive.
//
namespace MCCG
{
    class MCCGInpt
    {
        private const int MAX_INDS = 50;
        private const int MAX_PROCS = 20;

        private const string CODE_LIKE_PREFIX = "%!";       // use to make codes %like%
        private const string EXACT_MATCH_PREFIX = "&!";     // use to make desc/result exact match
        private const string CHAR_COMMA = "||";             // use to insert literal comma in word list

        private struct indicator_data {
            public bool     is_checked;     
            public int      radio_group;       
        }

        private struct proc_data {                      
            public int      procedure_number;
            public DateTime start;   
            public DateTime finish;
        }

        // These are database CHART_ITEMs for this patient
        private CHART_ITEM[] _chart_items_since_admission;
        private CHART_ITEM[] _chart_items_since_unit_arrival;
        private CHART_ITEM[] _chart_items_during_pull_period;
        // These are database PROCEDURE_EVENTs for this patient
        private PROCEDURE_EVENT[] _procedure_events;

        private indicator_data[] _inds;
        private List<proc_data> _procs;

        private const int DEFAULT_BUCKET_SIZE = 20;       //min

        private PatientInfo _pat;
        //private bool adl23;
        //private bool adl4;
        //private bool tubefeed;
        //private int  morse;
        private int  _bucket_size = DEFAULT_BUCKET_SIZE;
        private bool is_default;


        private enum SearchDepth {
            SearchDefault,
            SearchPullRange,            //search within the current pull     -- default
            SearchSinceArrival,         //search since arrival to the unit
            SearchSinceAdmission        //search everything since admission to the hospital
        }

        private enum CountMode {
            CountAll,
            CountFirst                  //stop after one is found
        }

        private enum GetValueMode {
            GetTotal,
            GetMax,
            GetLast
        }

        enum Frequencies {
            QNONE,
            Q4H,
            Q2H,
            Q1H,
            Q30M
        }

        private struct fmapRow {
            public double   los_high;                       //the LOS being testing
            public int[]    freq;                           //the count required for each Q value
        }
        private List<fmapRow> _freq_map;                    //1,2,4,8,12,24 hours

     
        //
        // This is the main entry point
        //
        public void ProcessPatient(PatientInfo pat)
        {
            _pat = pat;

            InitIndicators();
            InitProcs();
            if (! is_default)
                {
                LoadFreqTable(_pat.is_ICU);
                LoadPatientChart();
                Check_1_2_3_4();
                Check_5();
                Check_6_7();
                Check_8();
                Check_9();
                Check_10_11();
                Check_12_13();
                Check_14();
                Check_15_16_17_18();
                Check_19();
                Check_20();
                Check_21_22();
                Check_23();
                Check_24();
            }
            AtLeastOneADL();
            HighestIndicatorInEachGroupWins();

            if (!is_default)
            {
                CheckProcs();
            }

            if (Program.g_no_output) return;
            OutputClass();
            OutputProcs();
        }

       
        private void InitIndicators()
        {
            // Make a new (empty) indicator array
            _inds = new indicator_data[MAX_INDS + 1];           // This 1 based so add one
            is_default = false;
            //if (_pat.los_hours <= 4.0) {
            //    is_default = true;
            //    Program.VerboseAudit("Patient was here 4 hrs or less. Will receive default indicators " + _pat.default_inds_str);
            //    foreach (var ind in _pat.default_inds) {
            //        if (ind <= _inds.GetUpperBound(0)) {
            //            _inds[ind].is_checked = true;
            //        }
            //    }
            //}

            //adl23 = false;
            //adl4 = false;
            //tubefeed = false;
            //morse = 0;

            // get indicator radio groups from the database
            // ** (This database access can be replaced once we have a C# methodolgy cache)
            var db = PFSUtility.NewPfsDataContext();
            var query = from idef in db.INDICATOR_DEFINITIONs
                        where (idef.METHODOLOGY_ID == _pat.meth_id)
                        select idef;
            foreach (var idef in query) {
                if (idef.INDICATOR_NUMBER <= _inds.GetUpperBound(0)) {
                    // (convert nulls to zero)
                    _inds[idef.INDICATOR_NUMBER].radio_group = PFSUtility.DBToInt(idef.RADIO_GROUP);
                }
            }
        }

        private void InitProcs()
        {
            _procs = new List<proc_data>();
        }

        private fmapRow LoadFreqTableRow(double los_high, string values)
        {
            fmapRow fmrow;
            
            fmrow.los_high = los_high;
            fmrow.freq = new int[5];

            var arr = values.Split(',');
            for (int i = 0; i <= fmrow.freq.GetUpperBound(0) ; i++ )
            {
                fmrow.freq[i] = arr[i].ToInteger();
            }
            return fmrow;
        }

        // These are the number of times you need to do something during a certain time period (LOS)
        // in order for it to be considered one of the frequencies listed.
        private void LoadFreqTable(bool is_icu)
        {
            _freq_map = new List<fmapRow>();
                //                              LOS,  None Q4h Q2h Q1h Q30m
                _freq_map.Add(LoadFreqTableRow(1, "    0,  0,  0,  0,  1"));
                _freq_map.Add(LoadFreqTableRow(2, "    0,  0,  0,  1,  2"));
                _freq_map.Add(LoadFreqTableRow(4, "    0,  1,  2,  3,  4"));
                _freq_map.Add(LoadFreqTableRow(6, "    0,  1,  2,  4,  7"));
                _freq_map.Add(LoadFreqTableRow(8.5, "  0,  2,  3,  5, 10"));
                _freq_map.Add(LoadFreqTableRow(12.5, " 0,  2,  4,  7, 13"));
                _freq_map.Add(LoadFreqTableRow(16, "   0,  3,  5,  9, 17"));
                _freq_map.Add(LoadFreqTableRow(24, "   0,  4,  8, 15, 29"));
                _freq_map.Add(LoadFreqTableRow(9999, " 0,  4,  8, 15, 29"));

            //to read this table: if you have at least 6 asessmenets in a 12-hour period, you get Q1h
            //(LOS=12, column Q1h has a count of 6)
        }

        private Frequencies FreqForCount(double los_hours, int count) 
        {
            foreach(var fmrow in _freq_map) {
                if (los_hours <= fmrow.los_high) {
                    // Option: pro-rate the count upward if the LOS is less than the row we are interested in.
                    //         This will bump the count to what it might have been at the full LOS.
                    // Note: truncate the result; rounding inflates the value too much.
                    int prorated_count = (int)((fmrow.los_high / los_hours) * count);

                    // foreach goes low to high; go from high to low instead
                    for (int j = (int)Frequencies.Q30M; (j >= (int)Frequencies.QNONE); j--) { //search right to left
                        if (prorated_count >= fmrow.freq[j]) {
                            return (Frequencies)j;
                        }
                    } // next j
                }
            }
            
            return Frequencies.QNONE;
        }

        //
        // Load patient chart from the database (one DB access)
        // Load everything since admission
        //
        private void LoadPatientChart()
        {
            // Get the entire patient chart (all units and dates for this patient)
            var db = PFSUtility.NewPfsDataContext();
            var query = from item in db.CHART_ITEMs
                        where (item.ENCOUNTER_ID == _pat.encounter_id)
                        select item;
            // Save the result
            _chart_items_since_admission = query.ToArray();

            // Convert all results to lower case for case insensitive comparisons
            foreach (var item in _chart_items_since_admission) {
                if (item.CATEGORY != null) item.CATEGORY = item.CATEGORY.ToLower();
                if (item.CODE != null) item.CODE = item.CODE.ToLower();
                if (item.DESCRIPTION != null) item.DESCRIPTION = item.DESCRIPTION.ToLower();
                if (item.FIELD_NAME != null) item.FIELD_NAME = item.FIELD_NAME.ToLower();
                if (item.RESULT != null) item.RESULT = item.RESULT.ToLower();
            }

            // Prepare two more versions of the chart
            var query2 = from item in _chart_items_since_admission
                    where(item.EVENT_DATETIME >= _pat.unit_arrival) && (item.EVENT_DATETIME <= _pat.pull_finish)
                    select item;
            _chart_items_since_unit_arrival = query2.ToArray();

            query2 = from item in _chart_items_since_admission
                     where (item.EVENT_DATETIME >= _pat.pull_start) && (item.EVENT_DATETIME <= _pat.pull_finish)
                     select item;
            _chart_items_during_pull_period = query2.ToArray();
        }

        // Delay looking for patient procedures until they are wanted
        private void LoadPatientProceduresIfNeeded()
        {
            if (_procedure_events != null) return;

            var db = PFSUtility.NewPfsDataContext();
            var query = from proc in db.PROCEDURE_EVENTs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                        select proc;
            _procedure_events = query.ToArray();
        }



        // Started a new chart item query (of a certain depth)
        // Default is the chart during the pull range.  Options for unit arrival and admission to hospital.
        private IEnumerable<CHART_ITEM> StartNewQuery()
        {
            return StartNewQuery(SearchDepth.SearchDefault);
        }
        private IEnumerable<CHART_ITEM> StartNewQuery(SearchDepth search_depth)
        {
            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    return (from item in _chart_items_during_pull_period select item);
                case SearchDepth.SearchSinceArrival:
                    return (from item in _chart_items_since_unit_arrival select item);
                case SearchDepth.SearchSinceAdmission:
                    return (from item in _chart_items_since_admission select item);
            }
            return null;
        }

        // Is this search term a word or a list of words?
        private bool ValueIsAList(string s) 
        {
            return (s != null) && s.Contains(',');                //is this a comma-separated list?
        }

        // Break apart a wordlist and prepare its members
        private string[] SplitOnCommaAndPrepareElements(string s)
        {
            // If a word list has a space after each comma, we will get leading blanks in each term
            var arr = s.Split(',');
            for (int i = 0; i <= arr.GetUpperBound(0); i++) {
                arr[i] = arr[i].Trim();                         // get rid of leading blanks
                arr[i] = arr[i].Replace(CHAR_COMMA, ",");       // insert a literal comma into search word
                arr[i] = arr[i].ToLower();                      // convert for case-insensitive comparisons
            }
            return arr;
        }

        // Add a basic chart item filter to a LINQ query
        //
        // cat = exact match
        // code_list = exact match; optional "like" match
        // desc_list = "like" match; optional exact match
        // field = exact match
        // result_list = "like" match; optional exact match
        //
        // These are comma-separated lists.  
        // Search for a literal comma by using CHAR_COMMA in the string.
        private IEnumerable<CHART_ITEM> AndItemFilter(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list) 
        {
            // Converted all values to lower case for case-insensitive comparisons.
            if (!String.IsNullOrEmpty(cat)) {
                cat = cat.ToLower();
                query = query.Where(e => e.CATEGORY == cat);
            }

            if (!String.IsNullOrEmpty(code_list)) {
                code_list = code_list.ToLower();
                if (code_list.Left(2) == CODE_LIKE_PREFIX) {            // override for "like" match?
                    query = query.Where(e => e.CODE.Contains(code_list.Substring(2)));
                } else if (ValueIsAList(code_list)) {
                    query = AndCodeInList(query, code_list);            // find one of the words
                } else {
                    query = query.Where(e => e.CODE == code_list);      // find this word
                }
            }
            
            if (!String.IsNullOrEmpty(desc_list)) {
                desc_list = desc_list.ToLower();
                if (desc_list.Left(2) == EXACT_MATCH_PREFIX) {          // exact match override?
                    query = query.Where(e => e.DESCRIPTION == desc_list.Substring(2));
                } else if (ValueIsAList(desc_list)) {
                    query = AndDescriptionInList(query, desc_list);
                } else {
                    desc_list = desc_list.Replace(CHAR_COMMA, ",");     // look for literal comma requests
                    query = query.Where(e => e.DESCRIPTION.Contains(desc_list));
                }
            }
            
            if (!String.IsNullOrEmpty(field)) {
                field = field.ToLower();
                query = query.Where(e => e.FIELD_NAME == field);
            }
            
            if (!String.IsNullOrEmpty(result_list)) {
                result_list = result_list.ToLower();                        // make all results case insensitve
                if (result_list.Left(2) == EXACT_MATCH_PREFIX) {     // exact match override?
                    query = query.Where(e => e.RESULT == result_list.Substring(2));
                } else if (ValueIsAList(result_list)) {
                    query = AndResultInList(query, result_list);
                } else {
                    result_list = result_list.Replace(CHAR_COMMA, ",");     // look for literal comma requests
                    query = query.Where(e => e.RESULT.Contains(result_list));
                }
            }
            return query;
        }

        // Look for a code that is one of the words in the list.
        private IEnumerable<CHART_ITEM> AndCodeInList(IEnumerable<CHART_ITEM> query, string code_list) 
        {
            if (String.IsNullOrEmpty(code_list)) return query;
            
            var arr = SplitOnCommaAndPrepareElements(code_list);
            return query.Where(e => arr.Contains(e.CODE));              // exact match
        }

        // Look for a description that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndDescriptionInList(IEnumerable<CHART_ITEM> query, string desc_list) 
        {
            if (String.IsNullOrEmpty(desc_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(desc_list);
            return query.Where(e => e.DESCRIPTION.ContainsAny(arr));    // "like" match
        }

        // Look for a result that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultInList(IEnumerable<CHART_ITEM> query, string result_list) 
        {
            if (String.IsNullOrEmpty(result_list)) return query;
            
            var arr = SplitOnCommaAndPrepareElements(result_list);
            return query.Where(e => e.RESULT.ContainsAny(arr));         // "like" match
        }

        // Look for a result that does not contain one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultNotInList(IEnumerable<CHART_ITEM> query, string result_list)
        {
            if (String.IsNullOrEmpty(result_list)) return query;
            
            var arr = SplitOnCommaAndPrepareElements(result_list);
            return query.Where(e => !e.RESULT.ContainsAny(arr));        // "like" match
        }

        private string DescribeSearchDepth(SearchDepth search_depth) 
        {
            string result = "";
            
            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    //result = "in pull range"
                    result = "";                         //be quiet since this is default
                    break;
                case SearchDepth.SearchSinceArrival:
                    result = "since arrival to unit";
                    break;
                case SearchDepth.SearchSinceAdmission:
                    result = "since admission";
                    break;
            }
            
            return result;
        }

        // Describe a query that has already been run
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Make a simple query and describe the results
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            var arr = query.ToArray();
            return Describe(arr, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private string LookingFor(string so_far, string what, string op, string value)
        {
            string result;
            if (String.IsNullOrEmpty(value)) return so_far;

            if (so_far == "")
                result = "looking for " + what;
            else
                result = so_far + " and " + what;

            if (value.Left(2) == EXACT_MATCH_PREFIX)
                result += "='" + value.Substring(2) + "'";
            else if (value.Left(2) == CODE_LIKE_PREFIX)
                result += " contains " + "'" + value.Substring(2) + "'";
            else if (ValueIsAList(value))
                result += " in '" + value + "'";
            else
                result += op + "'" + value + "'";
            
            return result;
        }
        private string Describe(CHART_ITEM[] arr, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string result = "";
            result = LookingFor(result, "cat", "=", cat);
            result = LookingFor(result, "code", "=", code_list);
            result = LookingFor(result, "desc", " contains ", desc_list);
            result = LookingFor(result, "field", "=", field);
            result = LookingFor(result, "result", " contains ", result_list);

            if (arr.Count() == 0) {
                result += "; not found " + DescribeSearchDepth(search_depth);
            } else {
                var e = arr[0];
                result = "FOUND: " + result + "; found";
                // We might have searched for a pattern or word list in several fields - show what was found
                if (e.CATEGORY != null)     result += " cat='" + e.CATEGORY + "'";
                if (e.CODE != null)         result += " code='" + e.CODE + "'";
                if (e.DESCRIPTION != null)  result += " desc='" + e.DESCRIPTION + "'";
                if (e.FIELD_NAME != null)   result += " field='" + e.FIELD_NAME + "'";
                if (e.RESULT != null)       result += " result='" + e.RESULT + "'";
                // Are there more results?  Just say how many; we aren't going to list them.
                if (arr.Count() == 2) {
                    result += " (1 more result)";
                } else if (arr.Count() > 2) {
                    result += " (" + (arr.Count() - 1) + " more results)";
                }
            }
            
            return result;
        }


        //Set an indicator for this reason (low level)
        private void SetInd(int inum, string reason)
        {
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Set Ind #" + inum + ": " + reason);          //already set - repeat for verbose only
            } else {
                _inds[inum].is_checked = true;
                Program.Audit("Set Ind #" + inum + ": " + reason);
            }
        }

        //Clear an indicator for this reason (low level)
        private void ClrInd(int inum, string reason)
        {
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Clr Ind #" + inum + ": " + reason);          //already clear - repeat verbose only
            } else {
                _inds[inum].is_checked = false;
                Program.Audit("Clr Ind #" + inum + ": " + reason);
            }
        }

        // Count how many items meet the conditions
        // All items are listed
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountItems(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            int count = query.Count();

            // always return what was found
            found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
            // echo the result?
            if (trace) Program.VerboseAudit(found_what);
            
            return count;
        }

        // Count how many items meet the conditions
        // This has the option to list every entry or give a total
        private int CountResultInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what="";
 
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);

            var arr = SplitOnCommaAndPrepareElements(result_list);

            foreach (var item in query) {
                // Figure out which of the search words were found
                var s = item.RESULT.ContainsWhich(arr);
                found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' code='" + item.CODE + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "'";
                if (trace) Program.VerboseAudit(found_what);
                count++;
                if (count_mode != CountMode.CountAll) break;        //stop counting
            }
            
            if (count > 0) {
                //We already printed what was found; maybe add how many?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }
            
            return count;
        }

        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (ValueIsAList(result_list)) {
                return CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what);
            } else {
                return CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what);
            }
        }

        // Is there an item with this result?
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list) 
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth) 
        {
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContains(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what) 
        {
            if (ValueIsAList(result_list)) {
                return (CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            } else {
                return (CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
            }
        }

        // Count how many items meet the conditions, with none of the results in result_list.
        // This has the option to say how many were found rather than list all.
        private int CountResultNotInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what) 
        {
            int count = 0, rec_count = 0;
            bool found_one;

            found_what = "";
            var arr = SplitOnCommaAndPrepareElements(result_list);         // also deals with CHAR_COMMA
            
            //Do not filter by result here; we need all results
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");
            
            foreach(var item in query) {
                rec_count++;
                //Look for each search word in the result
                found_one = false;
                
                foreach (string s in arr) {
                    if (String.Equals(item.RESULT, s)) {
                        found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "' -- ignore this";
                        Program.VerboseAudit(found_what);
                        found_one = true;
                        break;
                    }
                }

                if (! found_one) {
                    //none of the words were found - good!
                    found_what = Describe(cat, code_list, desc_list, field, "", search_depth) + " -- does not contain '" + result_list + "'";
                    Program.VerboseAudit(found_what);
                    count++;
                    if (count_mode != CountMode.CountAll) break;
                }
            }
            
            if (count > 0) {
                //We already printed what was found; maybe show the total?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else if (rec_count > 0) {
                //We already printed what we ignored
            } else {
                //Print what did not exist
                found_what = Describe(cat, code_list, desc_list, field, "", search_depth);      //not found
                Program.VerboseAudit(found_what);
            }
            
            return count;
        }

        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, string found_what) 
        {
            // Search in the pull range with trace on
            return CountResultDoesNotContain(cat, code_list, desc_list, field, result_list, out found_what, SearchDepth.SearchDefault, true);
        }
        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, out string found_what, SearchDepth search_depth, bool trace)
        {
            return CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0;
        }

        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, string found_what)
        {
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, out found_what, SearchDepth.SearchDefault, true);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, out string found_what, SearchDepth search_depth, bool trace)
        {
            return (CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0);
        }

        // Set the indicator if the conditions are met
        //
        private void SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already set
            if (_inds[inum].is_checked) return;
            
            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what)) {
                SetInd(inum, found_what);                          //echo here - found
            } else {
                Program.VerboseAudit(found_what);                  //and here - not found
            }
        }

        // Set the indicator if the result does not contain any of the words in result_list
        //
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultDoesNotContain(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {   
            string found_what;
            //avoid more queries if the indicator is already set
            if (_inds[inum].is_checked) return;
            
            //Turn trace off for ResultDoesNotContain() and echo what was set below with SetInd
            if (ResultDoesNotContain(cat, code_list, desc_list, field, result_list, out found_what, search_depth, false)) {
                SetInd(inum, found_what);                           //echo here - not found
            } else {
                Program.VerboseAudit(found_what);                   //and here - found
            }
        }

        // Clear the indicator if the result contains one of the words in the result_list
        //
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already clear
            if (! _inds[inum].is_checked) return;
            
            //Turn trace off for ResultContains() and echo what was set below with SetInd
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what)) {
                ClrInd(inum, found_what);                           //echo here - found
            } else {
                Program.VerboseAudit(found_what);                   //and here - not found
            }
        }


        //These slightly smaller functions are meant for places where you aren't really looking for a chart result
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            return (CountResultContains(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
        }

        // Variations with optional parameters (can switch to optional parameters in VS 2010)
        //
        private void SetIndIfFound(int inum, string cat)
        {
            SetIndIfResultContains(inum, cat, "", "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list)
        {
            SetIndIfResultContains(inum, cat, code_list, "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private void ClrIndIfFound(int inum, string cat)
        {
            ClrIndIfResultContains(inum, cat, "","","","", SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }


        // Get the max/total value from a result (usually in the middle of the text)
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetIntValue(get_mode, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            int value;
            int result = 0;
            bool found_one = false;

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            
            //Look for a number in the result
            
            foreach(var item in query) {
                // RESULT might be null so convert to empty string so Split won't blow up
                var arr = PFSUtility.DBToString(item.RESULT).Split(' ');
                foreach (string s in arr) {
                    //Look for 1st character numeric; IsNumeric("60min") will fail so look at 1st char only
                    if (s.Left(1).IsNumeric()) {
                        value = (int)s.Val();                           //Use Val; ToInteger will error on "60min"
                        switch (get_mode) {
                            case GetValueMode.GetMax:
                                result = Math.Max(value, result);       //max
                                break;
                            case GetValueMode.GetTotal:
                                result += value;                        //total
                                break;
                            case GetValueMode.GetLast:
                                result = value;                         //last
                                break;
                        }
                        
                        // print what we are searching for (the first time)
                        if (! found_one) {
                            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
                        }
                        found_one = true;
                        // print each value found
                        Program.VerboseAudit("  found numeric value " + result);
                        //Keep going in case there are more
                    }
                } 
            }
            
            if (! found_one) {
                //show what was not found
                Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
            }
            
            return result;
        }

        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetMaxValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return GetIntValue(GetValueMode.GetMax, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetTotalValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return GetIntValue(GetValueMode.GetTotal, cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Get a result; returns true if found with return_result
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result)
        {
            return GetResult(cat, code_list, desc_list, field, out return_result, SearchDepth.SearchDefault);
        }
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0) {
                return_result = PFSUtility.DBToString(query.First().RESULT);
            } else {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }
        private void GetResultforOther(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "Other (comment)");
            return_result="";
            foreach (var item in query)
            {
                if (!String.Equals(item.RESULT, "Other (comment)"))
                {
                    return_result = item.RESULT;
                    //Program.VerboseAudit(found_what);
                    break;
                }

            }
            return;
        }

        //======================================================================================
        // Mapping starts here
        //======================================================================================

        private void Check_1_2_3_4()
        {
            bool bathtot;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("1. ADL Self");
            Program.VerboseAudit("2. ADL Assist");
            Program.VerboseAudit("3. ADL Extended");
            Program.VerboseAudit("4. ADL Complete");
            Program.VerboseAudit("---------------");
            
            // 4. COMPLETE CARE

            if (ResultContains("", "", "LOC", "", "comatose", SearchDepth.SearchSinceAdmission)) SetInd(4,"Comatose");
            if (_inds[4].is_checked) return;

            if (_pat.age <= 4.0)
            {
                SetInd(4,"Age <= 4 years");
            }

            bathtot = ResultContains("", "", "BATHING", "", "total");

            if (bathtot)
            {
                if (ResultContains("", "", "EATING", "", "total"))
                {
                    SetInd(4,"Bathing Total + Eating Total");
                    //SetInd(5,"Bathing Total + Eating Total");
                }
                if (ResultContains("", "", "TUBEFEEDNGFLOWTYPE", "", "Intermittent")) SetInd(4, "Bathing Total + Tube Feed Intermittent");
                if (ResultContains("", "", "NBFEEDING", "", "total")) SetInd(4, "Bathing Total + NBFeeding");
                if (ResultContains("", "", "NBFEEDING", "", "breast,bottle,NPO after 12")) SetInd(3, "Bathing Total + NBFeeding");
            }
            SetIndIfResultContains(4,"", "", "TUBEFEEDNGFLOWTYPE", "", "Continuous,Bolus");
            if (_inds[4].is_checked) return;

            SetIndIfResultContains(3, "", "", "FEEDING", "", "");
            SetIndIfResultContains(3, "", "", "ADLREQUIRES4ORMORECAREGIVERS", "", "");

            if (ResultContains("", "", "EATING", "", "self,assisted,total") && ResultContains("", "", "BATHING", "", "assisted,total") && ResultContains("", "TOILETING", "", "", "assisted,total"))
            {
                SetIndIfResultContains(3, "", "", "NEUTROPENICISOLATIONREASON", "", "Chemotherapy");
                SetIndIfResultContains(3, "", "", "NEUTROPENICISOLATIONREASON", "", "Radiation Therapy");
            }
            if (_inds[3].is_checked) return;

            SetIndIfResultContains(2, "", "", "AMBULATING", "", "assisted");
            SetIndIfResultContains(2, "", "", "BATHING", "", "assisted,total");
            SetIndIfResultContains(2, "", "", "EATING", "", "assisted");
            SetIndIfResultContains(2, "", "", "GROOMING", "", "assisted");
            SetIndIfResultContains(2, "", "", "TOILETING", "", "assisted");

            if (!ResultContains("", "", "GASTRICTUBEACTIVITY", "", "discontinued", SearchDepth.SearchSinceArrival))
            {
                SetIndIfResultContains(2, "", "", "GASTRICTUBETYPE", "", "Orogastric,Nasogastric,Gastrostomy Button,tube,Magnetic,Super PEG,Nasojejunal,Nasoduodenal,Jejunostomy,Orojejunal,pH Probe,Salem");
            }
            if (_inds[2].is_checked) return;

            SetIndIfResultContains(1, "", "", "AMBULATING", "", "self");
            SetIndIfResultContains(1, "", "", "BATHING", "", "self");
            SetIndIfResultContains(1, "", "", "EATING", "", "self,partial");
            SetIndIfResultContains(1, "", "", "GROOMING", "", "self");
            SetIndIfResultContains(1, "", "", "TOILETING", "", "self");
        }


        private void Check_5()
        {
            
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("5. ADL Rehab");
            Program.VerboseAudit("---------------");

            if (ResultContains("", "", "LOC", "", "comatose", SearchDepth.SearchSinceAdmission)) return;

            //if (_inds[5].is_checked)
            //{
            //    SetInd(5, "Bathing Total + Eating Total");
            //    return;
            //}

            SetIndIfResultContains(5, "", "", "EATING", "", "Rehab");
            SetIndIfResultContains(5, "", "", "GROOMING", "", "Rehab");
            SetIndIfResultContains(5, "", "", "TOILETING", "", "Rehab");
            SetIndIfResultContains(5, "", "", "BATHING", "", "Rehab");
            SetIndIfResultContains(5, "", "", "AMBULATING", "", "Rehab");
            
        }


        private void Check_6_7()
        {    
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("6. ADL 2-3 Caregivers");
            Program.VerboseAudit("7. ADL 4 or more Caregivers");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(7, "", "", "ADLREQUIRES4ORMORECAREGIVERS", "", "yes");

            SetIndIfResultContains(6, "", "", "ADLREQUIRES23CAREGIVERS", "", "yes");
            SetIndIfResultContains(6, "", "", "ADLREQUIRES2ORMORECAREGIVERS", "", "yes");
            //SetIndIfResultContains(6, "", "", "PATIENTSHIFTPROTOCOLS", "", "bariatric");
        }

        private void Check_8()
        {

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("8. Communication");
            Program.VerboseAudit("---------------");
            
            if (ResultContains("", "", "LOC", "", "comatose", SearchDepth.SearchSinceAdmission)) return;

            if (_pat.age >= 3.0)
            {
                SetIndIfResultContains(8, "", "", "CHARACTERISTICSOFSPEECH", "", "Aphasia,Nonverbal,Garbled,Slurred", SearchDepth.SearchSinceAdmission);
            }

            //if (!ResultContains("", "", "EARABNORMALITY", "", "hearing aid", SearchDepth.SearchSinceAdmission))
            //{
            //    if (ResultContains("", "", "EARABNORMALITY", "", "bilateral", SearchDepth.SearchSinceAdmission))
            //    {
            //        SetIndIfResultContains(8, "", "", "EARABNORMALITY", "", "Deaf,diminished", SearchDepth.SearchSinceAdmission);
            //    }
            //}

            SetIndIfResultContains(8, "", "", "EDUCATIONRESOURCEUSED", "", "interpreter", SearchDepth.SearchSinceAdmission);

            //if (!ResultContains("", "", "EYESADMISSIONASSESSMENT", "", "contacts,glasses", SearchDepth.SearchSinceAdmission))
            //{
            //    if (ResultContains("", "", "EYESADMISSIONASSESSMENT", "", "bilateral", SearchDepth.SearchSinceAdmission))
            //    {
            //        SetIndIfResultContains(8, "", "", "EYESADMISSIONASSESSMENT", "", "loss of vision,blind,prosthetic", SearchDepth.SearchSinceAdmission);
            //    }
            //}


            //SetIndIfResultContains(8, "", "", "EYESADMISSIONASSESSMENT", "", "visual acuity problem", SearchDepth.SearchSinceAdmission);
            SetIndIfResultContains(8, "", "", "EYEABNORMALITY", "", "visual acuity", SearchDepth.SearchSinceAdmission);
            SetIndIfResultContains(8, "", "", "EYEBLIND", "", "bilateral,left eye,right eye", SearchDepth.SearchSinceAdmission);
            SetIndIfResultContains(8, "", "", "EARABNORMALITY", "", "hearing", SearchDepth.SearchSinceAdmission);
            SetIndIfResultContains(8, "", "", "HEARINGABNORMALITY", "", "deaf,diminished,hearing aid", SearchDepth.SearchSinceAdmission);

            //if (ResultContains("", "", "HEARINGABNORMALITY", "", "hearing abnormality", SearchDepth.SearchSinceAdmission))
            //{
            //    SetIndIfResultContains(8, "", "", "HEARINGABNORMALITY", "", "deaf,diminished", SearchDepth.SearchSinceAdmission);
            //}

            SetIndIfResultContains(8, "", "", "HEARINGIMPAIREDREFERRAL", "", "sign language,video", SearchDepth.SearchSinceAdmission);
            SetIndIfResultContains(8, "", "", "LEARNINGBARRIERS", "", "low literacy", SearchDepth.SearchSinceAdmission);

            if (_pat.age >= 3.0)
            {
                if (!ResultContains("", "", "LOC", "", "comatose", SearchDepth.SearchSinceAdmission))
                {
                    SetIndIfResultContains(8, "", "", "NECKHEENT", "", "tracheostomy", SearchDepth.SearchSinceAdmission);
                    SetIndIfResultContains(8, "", "", "OXYGENMODE", "", "Ventilator", SearchDepth.SearchSinceAdmission);
                }
                SetIndIfResultContains(8, "", "", "PEDSNECK", "", "tracheostomy", SearchDepth.SearchSinceAdmission);
            }

            string reslist="Spanish,Arabic,Cantonese,Filipino,French,German,Greek";
            reslist += ",Hindi,Italian,Japanese,Korean,Mandarin,Portuguese,Russian";
            reslist += ",Sign Language,Taiwanese,Vietnamese,Unknown,Other";
            SetIndIfResultContains(8, "", "", "PRIMARYLANGUAGE", "", reslist, SearchDepth.SearchSinceAdmission);
            SetIndIfResultContains(8, "", "", "NEUROSYMPTOMS", "", "receptive aphasia", SearchDepth.SearchSinceAdmission);

        }

        private void Check_9()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("9. Cognitive Support");
            Program.VerboseAudit("---------------");

            if (!ResultContains("", "", "LOC", "", "comatose", SearchDepth.SearchSinceAdmission))
            {
                if (_pat.age > 4.0)
                {
                    SetIndIfResultContains(9, "", "", "PEDSPTPASTMEDICALHX", "", "autism");
                }
                SetIndIfResultContains(9, "", "", "PATIENTSHIFTPROTOCOLS", "", "alcohol protocol", SearchDepth.SearchSinceAdmission);
                SetIndIfResultContains(9, "", "", "MENTALSTATUS", "", "confused,disoriented,combative");
            }
            
            SetIndIfResultContains(9, "", "", "SHIFTSAFETYCRITICALCARE", "", "close observation");
            SetIndIfResultContains(9, "", "", "NEUROSYMPTOMS", "", "confusion");

        }

        private void Check_10_11()
        {
            string reslist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("10. Behavior/Emotional Management");
            Program.VerboseAudit("11. Behavior/Emotional Mgmt - q 1 Hour");
            Program.VerboseAudit("---------------");

            if (ResultContains("", "", "LOC", "", "comatose", SearchDepth.SearchSinceAdmission)) return;

            reslist = "Seeks RN assistance q 30-60";
            if (ResultContains("", "", "EMOTIONALMANAGEMENT", "", reslist))
            {
                SetIndIfResultContains(11, "", "", "AFFECTBEHAVIOR", "", "anxious,crying");
                SetIndIfResultContains(11, "", "", "NEUROSYMPTOMS", "", "confusion");
            }

            if (_inds[11].is_checked) return;

            reslist = "Disruptive to unit environment, Requires repetitive Nurse encouragement, Requires comforting and/or limit";
            if (ResultContains("", "", "EMOTIONALMANAGEMENT", "", reslist))
            {
                SetIndIfResultContains(10, "", "", "AFFECTBEHAVIOR", "", "Homicidal,Suicidal");
                SetIndIfResultContains(10, "", "", "AFFECTBEHAVIOR", "", "Agitated, Anxious, Combative, Crying, Fearful, Hostile, Inability to cope");
            }

            reslist = "Requires repetitive Nurse encouragement";
            if (ResultContains("", "", "EMOTIONALMANAGEMENT", "", reslist))
            {
                SetIndIfResultContains(10, "", "", "AFFECTBEHAVIOR", "", "depressed, withdrawn");
            }

            SetIndIfResultContains(10, "", "", "SHIFTSAFETYCRITICALCARE", "", "Close Observation 1:1");
            SetIndIfResultContains(10, "", "", "PATIENTSHIFTPROTOCOLS", "", "See Restraint sheet");

        }

        // (this isn't really a Q1h count -- it is just a count)
        private int ReturnQ1HrCount(string code_list, string result_list)
        {
            var buckets = new List<int>();
            SetBucketSize(10);
            AddBuckets(buckets, "", code_list, "", "", result_list);
            return CountBuckets(buckets);
        }

        private bool IsQ1Hour(int count)
        {
            return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q1H);
        }

        // Use this if counting safety buckets
        //private void CheckSafety(int count, string desc)
        //{
        //    if (_inds[13].is_checked) return;             //skip if highest already checked
        //    if (count == 0) return;
        //    switch (FreqForCount(_pat.los_hours, count))
        //    {
        //    case Frequencies.Q30M:
        //        SetInd(13, desc + " q30min");
        //        break;
        //    case Frequencies.Q1H:
        //    case Frequencies.Q2H:
        //        SetInd(12, desc + " q2h");
        //        break;
        //    default:
        //        Program.VerboseAudit(desc + ": " + count + " in " + Math.Round(_pat.los_hours) + " hours is not enough");
        //        break;
        //    }
        //}

        private void Check_12_13()
        {
            string res;
            int value = 0;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("12. Safety Management - q 2 Hours");
            Program.VerboseAudit("13. Safety Management - q 30 Minutes");
            Program.VerboseAudit("---------------");

            if (ResultContains("", "", "LOC", "", "comatose", SearchDepth.SearchSinceAdmission)) return;

            if (ResultContains("", "", "PATIENTSHIFTPROTOCOLS", "", "See Restraint sheet") ||
                ResultContains("", "", "PATIENTSAFETYSTANDARD", "", "See Restraint sheet"))
            {
                SetIndIfResultContains(13, "", "", "CURRENTSAFETYENVIRONMENT", "", "Safety Mgmt q 30");
                SetIndIfResultContains(13, "", "", "SHIFTSAFETYCRITICALCARE", "", "Safety Mgmt q 30");
                SetIndIfResultContains(12, "", "", "CURRENTSAFETYENVIRONMENT", "", "Safety Mgmt q 2");
                SetIndIfResultContains(12, "", "", "SHIFTSAFETYCRITICALCARE", "", "Safety Mgmt q 2");
            }
            SetIndIfResultContains(12, "", "", "CURRENTSAFETYENVIRONMENT", "", "Unit based sitter");
            SetIndIfResultContains(12, "", "", "AFFECTBEHAVIOR", "", "suicidal");
            SetIndIfResultContains(12, "", "", "BEDSAFETY", "", "safety net bed");
            SetIndIfResultContains(12, "", "", "NURSINGJUDGMENTHIGHFALLRISKPROTOCOL", "", "50");
            if (GetResult("", "", "MORSEFALLRISKSCORE", "", out res))
            {
                if (res.IsNumeric())
                {
                    value = (int)res.Val();  //Use Val; ToInteger will error on "60min"
                    if (value >= 50) SetInd(12, "Morse Score=" + res);
                }

            }


        }

        private void Check_14()
        {    

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("14. Isolation");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(14, "", "", "SHIFTSAFETYCRITICALCARE", "", "isolation", SearchDepth.SearchSinceArrival);
            SetIndIfResultContains(14, "", "", "SHIFTSAFETYCRITICALCARE", "", "Neutropenic Precautions", SearchDepth.SearchSinceArrival);
            SetIndIfResultContains(14, "", "", "NEUTROPENICISOLATIONREASON", "", "", SearchDepth.SearchSinceArrival);
            SetIndIfResultContains(14, "", "", "ISOLATIONREASONAIRBORNE", "", "", SearchDepth.SearchSinceArrival);
            SetIndIfResultContains(14, "", "", "ISOLATIONREASONDROPLET", "", "", SearchDepth.SearchSinceArrival);
            SetIndIfResultContains(14, "", "", "ISOLATIONREASONCONTACT", "", "", SearchDepth.SearchSinceArrival);

        }

        private void CheckAssessment(int count, string desc)
        {
            if (_inds[18].is_checked) return;          //skip if highest already checked
            if (count == 0) return;                    //skip if none
            
            // This should work the same as the original code:
            switch (FreqForCount(_pat.los_hours, count)) {
                case Frequencies.Q30M:
                    SetInd(18, desc);
                    break;
                case Frequencies.Q1H:
                    SetInd(17, desc);
                    break;
                case Frequencies.Q2H:
                    SetInd(16, desc);
                    break;
                case Frequencies.Q4H:
                    SetInd(15, desc);
                    break;
                default:
                    break;
            }

        }

        private void Check_15_16_17_18()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("15. Assessment q4h");
            Program.VerboseAudit("16. Assessment q2h");
            Program.VerboseAudit("17. Assessment q1h");
            Program.VerboseAudit("18. Assessment q30min");
            Program.VerboseAudit("---------------");
            
            ////direct triggers for NICU
            //SetIndIfResultContains(18, "", "ADDITIONALINFANTMONITORING", "", "", "Cooling");
            //if (_inds[18].is_checked) return;
            //SetIndIfResultContains(17, "", "APNEA", "", "", "Yes,No");
            //SetIndIfResultContains(17, "", "BRADYCARDIA", "", "", "");
            //SetIndIfResultContains(17, "", "PEDSFEEDINGROUTE", "", "", "Continuous");
            
            ////direct trigger for minimum Q2
            //SetIndIfResultContains(16, "", "LVAD", "", "", "");

            // ct events q 12 hours - count distinct event times!
            // select count(distinct(times)) where eventcode in ()
            //select min(time) where code in () and time>=pull-24
            //add 12 hours
            //select count(distinct(times)) where code in () and time between min and min+12
            //returnmaxcount(codeslist,n) in the past n hours
            //3=15, 6=16, 12=17, 24=18

            CountAssessments(30);               // always do q30 buckets--but use different freq tables for icu vs non-icu.

        }


        // Count number of buckets; similar assessments in the same bucket count as one
        //
        private void CountAssessments(int bucket_size)
        {
            int ct;
            string desclist;
            List<int> buckets;

            SetBucketSize(bucket_size);

            //desclist = "CARDIACDEVICES,CHESTTUBE,COMMANDS,DIALYSISTYPE,DIASTOLICBLOODPRESSUREMMHG,EQUIPMENTPATIENTCARE,";
            //desclist += "FEEDINGFREQUENCY,HEARTSOUNDS,ICP,INCENTIVESPIROMETRY,IV6VENOUSLINE,LOC,LUNGSOUNDS,MEDICATIONMANAGEMENT,";
            //desclist += "NAILBEDCOLOR,NEURODEVICES,OFFUNITNONRN,OFFUNITNONRNDATETIMESTART,ORALCAREDEFICIENCYPOTENTIAL,OXYGENMODE,";
            //desclist += "PATIENTSHIFTCARE,PATIENTSHIFTPROTOCOLS,PEDSEQUIPMENT,PULSERATE,RESPIRATORYRATE,SCALETYPE,SHIFTCARE,";
            //desclist += "SYSTOLICBLOODPRESSUREMMHG,TUBEFEEDNGFLOWTYPE";

            desclist = "EQUIPMENTPATIENTCARE,MEDICATIONMANAGEMENT,PATIENTSHIFTPROTOCOLS";
            buckets = new List<int>();
            AddBuckets(buckets, "", "", desclist, "");
            ct = CountBuckets(buckets);
            ShowBuckets(buckets);
            CheckAssessment(ct, "Med Mgt items=" + ct);
            if (_inds[18].is_checked) return;

            desclist = "CHESTTUBE,INCENTIVESPIROMETRY,LUNGSOUNDS,OXYGENMODE,PATIENTSHIFTCARE,RESPIRATORYRATE,SHIFTCARE";
            buckets = new List<int>();
            AddBuckets(buckets, "", "", desclist, "");
            ct = CountBuckets(buckets);
            ShowBuckets(buckets);
            CheckAssessment(ct, "Pulm items=" + ct);
            if (_inds[18].is_checked) return;

            desclist = "CARDIACDEVICES,EQUIPMENTPATIENTCARE,HEARTSOUNDS,NAILBEDCOLOR,PULSERATE,COMMANDS,LOC,NEURODEVICES,ICP";
            buckets = new List<int>();
            AddBuckets(buckets, "", "", desclist, "");
            ct = CountBuckets(buckets);
            ShowBuckets(buckets);
            CheckAssessment(ct, "Neuro items=" + ct);
            if (_inds[18].is_checked) return;

            desclist = "DIALYSISTYPE,DIASTOLICBLOODPRESSUREMMHG,";
            desclist += "FEEDINGFREQUENCY,IV6VENOUSLINE,";
            desclist += "OFFUNITNONRN,OFFUNITNONRNDATETIMESTART,ORALCAREDEFICIENCYPOTENTIAL,";
            desclist += "PEDSEQUIPMENT,SCALETYPE,";
            desclist += "SYSTOLICBLOODPRESSUREMMHG,TUBEFEEDNGFLOWTYPE";
            buckets = new List<int>();
            AddBuckets(buckets, "", "", desclist, "");
            ct = CountBuckets(buckets);
            ShowBuckets(buckets);
            CheckAssessment(ct, "Other Assess items=" + ct);
            if (_inds[18].is_checked) return;
            //buckets = new List<int>();
            //codelist = "ORALNASALETCO2,RESPIRATION,SPO22PREDUCTAL";
            //AddBuckets(buckets, "", codelist, "", "");
            //ct = CountBuckets(buckets);
            //CheckAssessment(ct, "Pulmonary=" + ct);
            //if (_inds[18].is_checked) return;

            //buckets = new List<int>();
            //codelist = "HEARTRATE,BLOODPRESSURE2,HEARTRATEMONITOR,BP,MAP2,BLOODPRESSURE3,BLOODPRESSURE4,OBFHRDOPPLER,LVAD,";
            //codelist = codelist + "ARTLINEBP,ARTLINEBP2,ARTLINEBP3,IABFREQUENCY,SWANGANZ,OBFHRDOPPLER,DIASTOLIC_BP,SYSTOLIC_BP";
            ////removed CARDRHYTHM for
            //AddBuckets(buckets, "", codelist, "", "");
            //ct = CountBuckets(buckets);
            //CheckAssessment(ct, "Cardiac=" + ct);
            //if (_inds[18].is_checked) return;

            //buckets = new List<int>();
            //codelist = "CARDRHYTHM";
            //AddBuckets(buckets, "", codelist, "", "");
            //ct = CountBuckets(buckets);
            //CheckAssessment(ct, "CardRhythm=" + ct);
            //if (_inds[18].is_checked) return;

            //buckets = new List<int>();
            //codelist = "BLADDERPRESSURE,ABDOMGIRTH,BLADDERSCAN";
            //AddBuckets(buckets, "", codelist, "", "");
            //ct = CountBuckets(buckets);
            //CheckAssessment(ct, "Intra-Abdominal=" + ct);
            //if (_inds[18].is_checked) return;

            //buckets = new List<int>();
            //codelist = "CATHSITECHECK,EPIDURALSITEASSESS,DOPPFLAPCHECK";
            //AddBuckets(buckets, "", codelist, "", "");
            //ct = CountBuckets(buckets);
            //CheckAssessment(ct, "Wound=" + ct);
            //if (_inds[18].is_checked) return;

            //buckets = new List<int>();
            //codelist = "BISRANGE,TRAINOFFOUR,CHARTINGTYPE";
            //AddBuckets(buckets, "", codelist, "", "");
            //ct = CountBuckets(buckets);
            //CheckAssessment(ct, "Neuro=" + ct);
            //if (_inds[18].is_checked) return;

            //buckets = new List<int>();
            //codelist = "DVPRSPAINSCALE,ANVPSPAINSCORE,PAINSCALE0TO10,NIPSSCORE,NPASSSCORE,PEDSPAINASSESSFLACC,PAINWONGBAKERFACESSCORE";
            //AddBuckets(buckets, "", codelist, "", "");
            //ct = CountBuckets(buckets);
            //CheckAssessment(ct, "Pain=" + ct);
            //if (_inds[18].is_checked) return;
        }

        //=====================================================================
        // Group like assessments into buckets of time
        // Each bucket counts as one assessment in the frequency count

        private void SetBucketSize(int minutes)
        {
            _bucket_size = minutes;
        }

        //Add to the list of bucket numbers (redundant buckets are fine)
        private void AddBuckets(List<int> bucket_list, string cat, string code_list, string desc, string field)
        {
            AddBuckets(bucket_list, cat, code_list, desc, field, "");
        }
        private void AddBuckets(List<int> bucket_list, string cat, string code_list, string desc, string field, string result_list)
        {
            // Assign (virtual) bucket numbers to each assessment.  
            // Every x minutes after that is a new bucket.  Bucket zero = pull_start time.

            // get the chart items for the assessments
            var query = StartNewQuery();
            query = AndItemFilter(query, cat, code_list, desc, field, result_list);

            // This step is needed for those who want to count the # within a bucket, like Shands #24.
            // This will have no effect on those that count buckets.
            var query2 = (from item in query select new { item.EVENT_DATETIME }).Distinct();

            // figure out what buckets the events belong to
            var query3 = from item in query2
                         select new {
                            bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size)
                         };
            // Add to the list
            foreach (var item in query3) {
                bucket_list.Add(item.bucket);
            }

            // This is just for auditing: Say what we are looking for and how many were found
            if (!String.IsNullOrEmpty(result_list)) {
                // print each word and if it was found or not
                int i = CountResultContains(cat, code_list, desc, field, result_list);
            } else {
                // print how many were found
                Program.VerboseAudit(Describe(cat, code_list, desc, field, ""));
            }
        }

        private int CountBuckets(List<int> bucket_list)       // bucket list: (ha ha)
        {
            int result = bucket_list.Distinct().Count();
            if (result > 0) Program.VerboseAudit(result + " unique");
            return result;
        }

        private void ShowBuckets(List<int> bucket_list)
        {
            StringBuilder builder = new StringBuilder();
            foreach (int ct in bucket_list)
            {
                // Append each int to the StringBuilder overload.
                builder.Append(ct).Append("/");
            }
            string result = builder.ToString();
            Program.VerboseAudit(result);
        }

        private void Check_19()
        {
            string reslist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("19. Vascular Access Site Mgt q1 Hour");
            Program.VerboseAudit("---------------");

            if (ResultContains("", "", "PERIPHERALIVACTIVITY", "", "Start,Assess"))
            {
                reslist = "Midline Catheter,Over the Needle Catheter,Butterfly Catheter,A-V Graft,AV Fistula";
                SetIndIfResultContains(19, "", "", "PERIPHERALIVCATHETERTYPE", "", reslist);
            }

            reslist="Administration of sclerosing agents,Alternative to repetitive venous Cannulations, Central venous pressure measurement or plasmapherisis, Placement of pulmonary wedge catheters";
            if (ResultContains("", "", "CENTRALIVINDICATION", "", reslist))
            {
                reslist = "Broviac,Groshong,Hickman,Portacath,Introducer/Sheath,Introducer/PA catheter,Multi-lumen catheter,Permacath,Peripherally inserted,Quinton,Mahurkar,Tessio,Implanted port";
                SetIndIfResultContains(19, "", "", "CENTRALIVACCESSTYPE", "", reslist);
            }
        }

        private void Check_20()
        {
            string medlist;
            
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("20. Medication Activity >= 20 minutes");
            Program.VerboseAudit("---------------");

            medlist = "WHOLEBLOOD,PRBC,PRBCUNITCOUNT,CRYOPRECIP,FCTVLLL,FCTLX,FFP,FFPUNITCOUNT,";
            medlist += "PLATELETS,PLATELETSUNITCOUNT,AUTOLOGOUSBLOOD";
            SetIndIfResultContains(20, "", "", medlist, "", "");

            SetIndIfResultContains(20, "", "", "MEDICATIONPREPORTO20MINS", "", "yes");
        }

        private void Check_21_22()
        {

            string start_tm;
            string end_tm;
            string reslist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("21. Wound/Injury Mgmt");
            Program.VerboseAudit("22. Wound/Injury Mgmt >= 30 Minutes");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(22, "", "", "WOUNDMANAGEMENT", "", "30 minutes");
            
            // these next 2 require start end times
            if (ResultContains("", "", "EXTENSIVEWOUNDMGMTRN", "", "yes"))
            {
                //EXWOUNDMGMTRNTIMESTART
                //EXWOUNDMGMTRNTIMEEND
                if (GetResult("", "", "EXWOUNDMGMTRNTIMESTART", "", out start_tm))
                {
                    if (GetResult("", "", "EXWOUNDMGMTRNTIMEEND", "", out end_tm))
                    {
                        SetInd(22, "Extensive Wound by RN: " + start_tm + "-" + end_tm);
                    }
                }
            };
            if (ResultContains("", "", "EXTENSIVEWOUNDMGMTNONRN", "", "yes"))
            {
                //EXWOUNDMGMTNONRNTIMESTART
                //EXWOUNDMGMTNONRNTIMEEND
                if (GetResult("", "", "EXWOUNDMGMTNONRNTIMESTART", "", out start_tm))
                {
                    if (GetResult("", "", "EXWOUNDMGMTNONRNTIMEEND", "", out end_tm))
                    {
                        SetInd(22, "Extensive Wound by Non-RN: " + start_tm + "-" + end_tm);
                    }
                }
            }

            SetIndIfResultContains(21, "", "", "ARTIFICIALAIRWAYS", "", "Tracheostomy");
            SetIndIfResultContains(21, "", "", "CHARACTERISTICSOFSPEECH", "", "et/trach");
            SetIndIfResultContains(21, "", "", "FUNDUS", "", "firm,boggy");
            SetIndIfResultContains(21, "", "", "GENITALDESCRIPTION", "", "episiotomy");
            SetIndIfResultContains(21, "", "", "CENTRALIVACTIVITY", "", "dressing change");
            SetIndIfResultContains(21, "", "", "SHIFTCARE", "", "Central Line Care,PEG Care");
            SetIndIfResultContains(21, "", "", "SKINCARE", "", "Circumcision Care,Cord Care");
            SetIndIfResultContains(21, "", "", "GASTRICTUBETYPE", "", "Gastrostomy Tube,PEG Tube,Jejunostomy");
            SetIndIfResultContains(21, "", "", "WOUNDMANAGEMENT", "", "Wound/Injury Management");
            SetIndIfResultContains(21, "", "", "WOUNDCONDITION", "", "Open to Air,Reinforced only,Dressing assessed,Dressing changed,Dressing change not due at this time,JFTK Only");
            SetIndIfResultContains(21, "", "", "WOUNDINJURYMANAGEMENT", "", "yes");
            SetIndIfResultContains(21, "", "", "PROCEDURETYPE", "", "PEG Placement,Thoracentesis,ICP monitoring,Emergent open chest");
            reslist ="Abrasion,Blister,Burn,Excoriation,Gun Shot Wound,IV Infiltration with Tissue Damage,Laceration,";
            reslist += "Open Skin Abscess/Boil,Puncture: Non-IV,Skin Graft Donor Site,Skin Graft Recipient  Site,";
            reslist += "Skin Tear,Stab Wound,Surgical incision,Traumatic Amputation,Traumatic Wound-Other,Ulcer,Other";
            SetIndIfResultContains(21, "", "", "TYPEOFWOUND", "", reslist);
        
        }

        private void Check_23()
            {
                string educ_items;
                bool coma_ok=false;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("23. Healthcare Mgmt Education >= 1 Hour");
            Program.VerboseAudit("---------------");

            //SetIndIfResultContains(23, "", "", "INDIVIDUALSTAUGHT", "", "");
            SetIndIfResultContains(23, "", "", "EDUCATIONCONTINUOUS1HRORGREATER", "", "");
            //SetIndIfResultContains(23, "", "", "EDUCATIONCONTINUOUSSTARTDATETIME", "", "");
            //SetIndIfResultContains(23, "", "", "EDUCATIONCONTINUOUSENDDATETIME", "", "");
            if (ResultContains("", "", "LOC", "", "comatose", SearchDepth.SearchSinceAdmission))
            {
                if (ResultContains("", "", "INDIVIDUALSTAUGHT", "", "Family Member, Friend, Parent, Significant Other, Other"))
                {
                    coma_ok=true;
                }
            }
            if ((coma_ok) || (ResultContains("", "", "INDIVIDUALSTAUGHT", "", "Patient,Family Member, Friend, Parent, Significant Other, Other")))
            {
                educ_items = "Infant Bonding,Safety,Invasive equipment/homecare teaching,Hygiene,Invasive Line Care";
                educ_items += ",Lab tests,Mechanical Ventilation,Medications,Pain Management,Physical limitations";
                educ_items += ",Skin Care,Surgical procedure,Smoking Cessation,Treatment Procedures,Urinary Catheter Care";
                educ_items += ",Cast Care,Trach care,Wound/Incision Care,Chronic disease,Developmental Care,Restraints,Diabetic";
                SetIndIfResultContains(23, "", "", "EDUCATIONTOPICS", "", educ_items);
            }
        }

        private void Check_24()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("24. 1 to 1 Physiological Interv. >= 2 Hours");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(24, "", "", "PHYSIOLOGINTERVENTGREATERTHAN2HRS", "", "yes");
        }


        private void AtLeastOneADL()
        {
            Program.Audit("---------------");
            if (!(_inds[1].is_checked || _inds[2].is_checked || _inds[3].is_checked || _inds[4].is_checked)) {
                // Make "#2 ADL - Assist" the default.  (90% of patients)
                SetInd(2, "Defaulting to partial care due to lack of documentation.");
            }
        }

        private void HighestIndicatorInEachGroupWins()
        {
            int i, g;
            bool highest_is_on;
            string ind_list;

            Program.Audit("Select highest indicator in each group");
            
            g = 0;
            highest_is_on = false;
            // Go from bottom up - highest (lowest) indicator in each group wins
            for (i = MAX_INDS; (i >= 1); i--) {
                if (_inds[i].radio_group > 0) {
                    if (_inds[i].radio_group != g) {
                        //this is a new group
                        g = _inds[i].radio_group;
                        highest_is_on = _inds[i].is_checked;
                    } else {
                        //same group
                        if (highest_is_on) {
                            _inds[i].is_checked = false;             //uncheck a lower number
                        } else {
                            highest_is_on = _inds[i].is_checked;     //save this one
                        }
                    }
                }
            } // next i
            
            //Echo the indicators for an Audit (no classification will be saved)
            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++) {
                if (_inds[i].is_checked) ind_list += "," + i;
            } // next i

            Program.Audit("Final list = " + ind_list.Substring(1));
            Program.Audit("---------------");
        }


        private void CheckProcs()
        {
            //CheckProc_1();
            //CheckProc_2();
            //CheckProc_3();
            //CheckProc_4();
            //CheckProc_5();
            //CheckProc_6();
            //CheckProc_7();
            //CheckProc_9();
            //CheckProc_10();
            //CheckProc_11();
        }

        private void CheckProc_1()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P1. 1-1 safety observation by RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_2()
        {
            string nowstr;
            string toddtstr;
            string yesdtstr;
            string timea ="";
            DateTime timea_startdt, timea_enddt;
            DateTime timeb_startdt, timeb_enddt;
            DateTime nowdt = _pat.pull_finish;              // "now" is pull time
            DateTime yesdt;
            
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P2. 1-1 safety observation by non-RN");
            Program.VerboseAudit("---------------");

            nowstr = nowdt.ToString("yyyyMMddHHmm");
            yesdt = nowdt.AddDays(-1);
            toddtstr = nowdt.ToString("yyyyMMdd");
            yesdtstr = yesdt.ToString("yyyyMMdd");
            
            //when is now? (yesterday/today)
            //yes 7am -- yes 7p  -- tod 7a -- tod 7p
            //                                   A                   B
            //if nowdt >= tod7pm then check  tod 7am-tod 7pm and tod 7pm-tom 7a
            //if nowdt >= tod7am then check  yes 7p - tod 7a     tod 7a-tod 7p
            //if nowdt >= yes7pm then check  yes 7a-yes 7p       yes 7p-tod 7a
            if (nowstr.CompareTo(toddtstr + "1900") >= 0) {
                timea = toddtstr + "0700";
            } else if (nowstr.CompareTo(toddtstr + "0700") >= 0) {
                timea = yesdtstr + "1900";
            } else if (nowstr.CompareTo(yesdtstr + "1900") >= 0) {
                timea = yesdtstr + "0700";
            }
            
            timea_startdt = PFSUtility.ISOToDateTime(timea);
            timea_enddt = timea_startdt.AddHours(12);
            MaybeAddSitter(timea_startdt, timea_enddt);

            timeb_startdt = timea_enddt;
            timeb_enddt = timeb_startdt.AddHours(12);
            MaybeAddSitter(timeb_startdt, timeb_enddt);
        }

        private void MaybeAddSitter(DateTime startdt, DateTime enddt)
        {
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);    // add custom time range below
            query = query.Where(e => (e.EVENT_DATETIME >= startdt) && (e.EVENT_DATETIME < enddt));
            query = AndCodeInList(query, "Sitter");
            query = AndResultInList(query,"continued, initiated");
            query = AndResultNotInList(query, "discontinued");

            if (query.Count() > 0) {
                if (ProcExists(2, startdt, enddt)) {
                    Program.Audit("Procedure 2: already exists");
                } else {
                    var proc = new proc_data();
                    proc.procedure_number = 2;
                    proc.start = startdt;
                    proc.finish = enddt;
                    _procs.Add(proc);
                    Program.Audit("Procedure 2: Found Sitter between " + startdt + " and " + enddt);
                }
            }
            
        }

        private bool ProcExists(int pnum, DateTime startdt, DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && (proc.PROCEDURE_DATETIME == startdt)
                            && (proc.DEPARTURE_DATETIME == enddt)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new {proc.PROCEDURE_EVENT_ID};
            return (query.Count() > 0);
        }


        private void CheckProc_3()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P3. Off unit accompanied by RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_4()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P4. Off unit accompanied by non-RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_5()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P5. Patient/family education by RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_6()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P6. Extensive wound management by RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_7()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P7. Extensive wound management by non-RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_8()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P8. Coordination of care by RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_9()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P9 1:1 RN at bedside");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_10()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P10. 1:1 non-RN at bedside");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_11()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P11. 2:1 by RN at bedside");
            //Program.VerboseAudit("---------------");
        }

        
        private const string DATETIME_FORMAT = "yyyyMMddHHmm";              // ISO Date/Time w/o seconds

        private void OutputClass()
        {
            string outstr, ind_list, desc, str_pull_dt, outstr0700;
            int i, tc_event_id;

            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         212 242 304
            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1
            //1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901 3456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
            //1       |10 5            |                |                |        |10023121779         |ALEXANDER                       |GLORIA                          |                                |0508    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289530  |           |201403030400                                                                      |NNYNNNNYYNNYNNNYNNNYYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//1       |10 5            |                |                |        |10024544908         |ARMSTRONG                       |AARON                           |                                |0535    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289531  |           |201403030400                                                                      |NYNNYNNNNNNYNNNYNNNNYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//|0547    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289532  |           |201403030400                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//|0547    |A   |201403030700|                |201403030700    |18  |C|    |3040|1440|18289532  |           |201403030700                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN

            if (Program.g_is_test)
                tc_event_id = 9999;
            else
                tc_event_id = PFSUtility.NextGID();                         //get a unique id for this class
            str_pull_dt = _pat.pull_finish.ToString(DATETIME_FORMAT);

            outstr = _pat.facilty_code.FixedWidth(8);                       //(facility code)
            outstr += "|" + _pat.unit_name.FixedWidth(16);
            outstr += "|" + "".FixedWidth(16);                               //(unit code)
            outstr += "|" + "".FixedWidth(16);                               //(area code)
            outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
            outstr += "|" + _pat.acct.FixedWidth(20);
            outstr += "|" + _pat.last_name.FixedWidth(32);
            outstr += "|" + _pat.first_name.FixedWidth(32);
            outstr += "|" + _pat.middle_name.FixedWidth(32);
            outstr += "|" + _pat.room.FixedWidth(8);
            outstr += "|" + _pat.bed.FixedWidth(4);
          outstr += "|" + str_pull_dt.FixedWidth(12);                      //class datetime (could change)
            outstr += "|" + "".FixedWidth(16);                               //(login)
          outstr += "|" + str_pull_dt.FixedWidth(16);                      //(employee)/(pull datetime)
            outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
            outstr += "|" + "C".FixedWidth(1);                               //record type = class
            outstr += "|" + "".FixedWidth(4);                                //(stage)
            outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
            outstr += "|" + _pat.range.ToString().FixedWidth(4);             //TC pull range
            outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
            outstr += "|";
            outstr = outstr.FixedWidth(294);
          outstr += "|" + _pat.effective.ToString(DATETIME_FORMAT);        //IN
            outstr = outstr.FixedWidth(377);
            outstr += "|";
            
            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++) {
                if (_inds[i].is_checked) {
                    outstr += "Y";
                    ind_list += "," + i;
                } else {
                    outstr += "N";
                }
            } // next i
            ind_list = ind_list.Substring(1);                           //strip leading comma
  
            Program.outfile.WriteLine(outstr);                          //output to transparent.txt
            if (Program.g_make0700)
            {  //212 242 304
                char[] buffer = outstr.ToCharArray();
                buffer[212 - 1 + 0] = '0';
                buffer[212 - 1 + 1] = '7';
                buffer[212 - 1 + 2] = '0';
                buffer[212 - 1 + 3] = '0';

                buffer[242 - 1 + 0] = '0';
                buffer[242 - 1 + 1] = '7';
                buffer[242 - 1 + 2] = '0';
                buffer[242 - 1 + 3] = '0';

                buffer[304 - 1 + 0] = '0';
                buffer[304 - 1 + 1] = '7';
                buffer[304 - 1 + 2] = '0';
                buffer[304 - 1 + 3] = '0';
                outstr0700 = new string(buffer);
                Program.outfile.WriteLine(outstr0700);                          //output to transparent.txt
            }

            Program.Audit("");
            desc = "Classified: " + ind_list;
            if (Program.g_is_test) {
                Program.Audit(desc);
            } else {
                //Save the selected indicators plus both Program.Audits in the event log; link with tc_event_id
                PFSEventLog.AddTransparentMappingEventLogEntry(
                    desc, Program.gLogUnitID, Program.gLogEncounterID,
                    tc_event_id, Program.gLogMapperVersion,
                    Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
            }
        }

        private void OutputProcs()
        {
            int i;
            string outstr, proc_list, desc;
            int tc_event_id;

            foreach(var proc in _procs) {
                if (Program.g_is_test)
                    tc_event_id = 9999;
                else
                    tc_event_id = PFSUtility.NextGID();                         //get a unique id for this proc

                outstr = _pat.facilty_code.FixedWidth(8);
                outstr += "|" + _pat.unit_name;                                 //10
                outstr = outstr.FixedWidth(68);
                outstr += "|" + _pat.acct.FixedWidth(20);                       //90
                outstr += "|" + _pat.last_name.FixedWidth(32);
                outstr += "|" + _pat.first_name.FixedWidth(32);
                outstr += "|" + _pat.middle_name.FixedWidth(32);
                outstr = outstr.FixedWidth(202);
                outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //204 proc dt
                outstr = outstr.FixedWidth(254);
                outstr += "|P";                                                 //256 procedure type record
                outstr += "|" + "".FixedWidth(4);                               //(stage)
                outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);     //TC source ID
                outstr += "|" + _pat.range.ToString().FixedWidth(4);            //TC pull range
                outstr += "|" + tc_event_id.ToString().FixedWidth(10);          //TC event ID
                outstr += "|";
                outstr = outstr.FixedWidth(294);
                outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //296 procdt in
                outstr = outstr.FixedWidth(346);
                outstr += "|" + proc.finish.ToString(DATETIME_FORMAT);          //348 procdt out
                outstr = outstr.FixedWidth(377);
                outstr += "|";
                
                proc_list = "";
                for (i = 1; (i < MAX_PROCS); i++) {
                    if (proc.procedure_number == i) {
                        outstr += "Y";
                        proc_list += "," + i;
                    } else {
                        outstr += "N";
                    }
                } // next i
                proc_list = proc_list.Substring(1);                             //strip leading comma

                Program.outfile.WriteLine(outstr);                              //output to transparent.txt

                desc = "Procedures: " + proc_list;
                if (Program.g_is_test) {
                    Program.Audit(desc);
                } else {
                    //Save the selected procedures plus both Program.Audits in the event log; link with tc_event_id
                    //(this assumes the procedure Program.Audit is mixed in with the class Program.Audit)
                    PFSEventLog.AddTransparentMappingEventLogEntry(desc, Program.gLogUnitID, Program.gLogEncounterID,
                        tc_event_id, Program.gLogMapperVersion,
                        Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
                }
            } // next proc
        }


    }
}
