﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;                     // for LINQ; add a reference
using System.Data.SqlClient;                // for SqlConnection
using System.IO;                            // for Directory, Path, Stream
using PfsShared;                            // add a reference to Shared2 project

// ============================================================================
// Inpatient 2.0 transparent mapping -- GOES HERE --  University of Nebraska
// (The code below is a sample from Sharp)
// ============================================================================
// This processes one patient.  Only one database query is used.  :)
//
// All search functions use exact match for category, description and field name.
// Codes and results can be a single word or a list of words.
// Codes default to exact match; prefix with CODE_CODE_LIKE_PREFIX if desired (single word only)
// Results default to like matching; prefix with RESULT_EXACT_MATCH_PREFIX if desired (single word only)
//
// All searches are case insensitive.
//
//
namespace TransparentMapping
{
    class Inpatient2
    {
        private const int MAX_INDS = 104;
        private const int MAX_PROCS = 20;

        private const string CODE_LIKE_PREFIX = "%!";       // use to make codes %like%
        private const string EXACT_MATCH_PREFIX = "&!";     // use to make desc/result exact match
        private const string CHAR_COMMA = "||";             // use to insert literal comma in word list
        private const string NOT_PREFIX = "!!";             // use to insert literal comma in word list

        private struct indicator_data {
            public bool     is_checked;     
            public int      radio_group;       
        }

        private struct proc_data {                      
            public int      procedure_number;
            public DateTime start;   
            public DateTime finish;
        }

        // These are database CHART_ITEMs for this patient
        private CHART_ITEM[] _chart_items_since_admission;
        private CHART_ITEM[] _chart_items_since_unit_arrival;
        private CHART_ITEM[] _chart_items_during_pull_period;
        private CHART_ITEM[] _chart_items_24hrs;
        // These are database PROCEDURE_EVENTs for this patient
        private PROCEDURE_EVENT[] _procedure_events;

        private indicator_data[] _inds;
        private List<proc_data> _procs;
        private List<proc_data> _outcomes;

        private const int DEFAULT_BUCKET_SIZE = 20;       //min

        private PatientInfo _pat;
        private bool adl23;
        private bool adl4;
        private bool tubefeed;
        private bool coma;
        private int  _bucket_size = DEFAULT_BUCKET_SIZE;
        private bool is_default;
        private bool give_default_inds = false;


        private enum SearchDepth {
            SearchDefault,
            SearchPullRange,            //search within the current pull     -- default
            SearchSinceArrival,         //search since arrival to the unit
            SearchSinceAdmission,        //search everything since admission to the hospital
            Search24Hrs
        }

        private enum CountMode {
            CountAll,
            CountFirst                  //stop after one is found
        }

        private enum GetValueMode {
            GetTotal,
            GetMax,
            GetLast
        }

        enum Frequencies {
            QNONE,
            Q4H,
            Q2H,
            Q1H,
            Q30M
        }

        private struct fmapRow {
            public double   los_high;                       //the LOS being testing
            public int[]    freq;                           //the count required for each Q value
        }
        private List<fmapRow> _freq_map;                    //1,2,4,8,12,24 hours

     
        //
        // This is the main entry point
        //
        public void ProcessPatient(PatientInfo pat)
        {
            _pat = pat;

            InitIndicators(); // sets is_default
            InitProcs();
            if (!Program.g_no_class)
            {
                if (!is_default)
                {
                    LoadFreqTable();
                    LoadPatientChart();
                    Check_1_2_3_4();
                    Check_5();
                    Check_6_7();
                    Check_8();
                    Check_9();
                    Check_10_11();
                    Check_12_13();
                    Check_14();
                    Check_15_16_17_18();
                    Check_19();
                    Check_20();
                    Check_21_22();
                    Check_23();
                    Check_24();
                }
                AtLeastOneADL();
                HighestIndicatorInEachGroupWins();
            }

            if (!is_default)
            {
                if (!Program.g_no_activities)
                {
                    if (Program.g_no_class) LoadPatientChart();
                    CheckProcs();
                }
                //CheckOutcomes();
            }

            if (Program.g_no_output) return;
            bool def_in_past4hrs;
            if (def_in_past4hrs = DefaultInPast4hrs())
            {
                Program.VerboseAudit("default found def ptype=" + _pat.default_ptype);
                give_default_inds = (_pat.default_ptype > DeterminePtypeOfIndicators());
            }
            if (!Program.g_no_class) OutputClass();
            if (!Program.g_no_activities) OutputProcs();
            //OutputOutcomes();
        }

       
        private void InitIndicators()
        {
            // Make a new (empty) indicator array
            _inds = new indicator_data[MAX_INDS + 1];           // This 1 based so add one
            is_default = false;
            //if (_pat.los_hours <= 4.0) {
            //    is_default = true;
            //    Program.VerboseAudit("Patient was here 4 hrs or less. Will receive default indicators " + _pat.default_inds_str);
            //    foreach (var ind in _pat.default_inds) {
            //        if (ind <= _inds.GetUpperBound(0)) {
            //            _inds[ind].is_checked = true;
            //        }
            //    }
            //}

            adl23 = false;
            adl4 = false;
            tubefeed = false;
            coma = false;

            // get indicator radio groups from the database
            // ** (This database access can be replaced once we have a C# methodolgy cache)
            var db = PFSUtility.NewPfsDataContext();
            var query = from idef in db.INDICATOR_DEFINITIONs
                        where (idef.METHODOLOGY_ID == _pat.meth_id)
                        select idef;
            foreach (var idef in query) {
                if (idef.INDICATOR_NUMBER <= _inds.GetUpperBound(0)) {
                    // (convert nulls to zero)
                    _inds[idef.INDICATOR_NUMBER].radio_group = PFSUtility.DBToInt(idef.RADIO_GROUP);
                }
            }
        }

        private void InitProcs()
        {
            _procs = new List<proc_data>();
            _outcomes = new List<proc_data>();
        }

        private fmapRow LoadFreqTableRow(double los_high, string values)
        {
            fmapRow fmrow;
            
            fmrow.los_high = los_high;
            fmrow.freq = new int[5];

            var arr = values.Split(',');
            for (int i = 0; i <= fmrow.freq.GetUpperBound(0) ; i++ )
            {
                fmrow.freq[i] = arr[i].ToInteger();
            }
            return fmrow;
        }

        // These are the number of times you need to do something during a certain time period (LOS)
        // in order for it to be considered one of the frequencies listed.
        private void LoadFreqTable()
        {
            _freq_map = new List<fmapRow>();
            //                              LOS,  None Q4h Q2h Q1h Q30m
            _freq_map.Add(LoadFreqTableRow(1, "    0,  0,  0,  1,  2"));
            _freq_map.Add(LoadFreqTableRow(2, "    0,  0,  1,  2,  3"));
            _freq_map.Add(LoadFreqTableRow(4, "    0,  1,  2,  3,  5"));
            _freq_map.Add(LoadFreqTableRow(6, "    0,  1,  3,  4,  8"));
            _freq_map.Add(LoadFreqTableRow(8, "  0,  1,  3,  6, 10"));
            _freq_map.Add(LoadFreqTableRow(12, " 0,  2,  5,  8, 15"));
            _freq_map.Add(LoadFreqTableRow(16, "   0,  3,  6,  9, 20"));
            _freq_map.Add(LoadFreqTableRow(24, "   0,  4,  8, 15, 29"));
            _freq_map.Add(LoadFreqTableRow(9999, " 0,  4,  8, 15, 29"));
//New freq table 2/5/14
//q4	q2	q1	q30     q30
//            Non-ICU	ICU & SD
// 4	8	15	29	    36
// 3	5	9	17	    24
// 2	4	7	13	    19
// 2	3	5	10	    13

            //to read this table: if you have at least 6 asessmenets in a 12-hour period, you get Q1h
            //(LOS=12, column Q1h has a count of 6)
        }

        private Frequencies FreqForCount(double los_hours, int count) 
        {
            foreach(var fmrow in _freq_map) {
                if (los_hours <= fmrow.los_high) {
                    // Option: pro-rate the count upward if the LOS is less than the row we are interested in.
                    //         This will bump the count to what it might have been at the full LOS.
                    // Note: truncate the result; rounding inflates the value too much.
                    int prorated_count = (int)((fmrow.los_high / los_hours) * count);

                    // foreach goes low to high; go from high to low instead
                    for (int j = (int)Frequencies.Q30M; (j >= (int)Frequencies.QNONE); j--) { //search right to left
                        if (prorated_count >= fmrow.freq[j]) {
                            return (Frequencies)j;
                        }
                    } // next j
                }
            }
            
            return Frequencies.QNONE;
        }

        //
        // Load patient chart from the database (one DB access)
        // Load everything since admission
        //
        private void LoadPatientChart()
        {
            // Get the entire patient chart (all units and dates for this patient)
            var db = PFSUtility.NewPfsDataContext();
            var query = from item in db.CHART_ITEMs
                        where (item.ENCOUNTER_ID == _pat.encounter_id) && 
                        (item.UNIT_ID != -1 || 
                         (item.CODE.Trim().ToUpper() == "SAFETY RN-Q" ||
                          item.CODE.Trim().ToUpper() == "SAFETY NON-RN-Q" ||
                          item.CODE.Trim().ToUpper() == "OFF UNIT RN-Q" ||
                          item.CODE.Trim().ToUpper() == "OFF UNIT NON-RN-Q" ||
                          item.CODE.Trim().ToUpper() == "EDUC RN-Q" ||
                          item.CODE.Trim().ToUpper() == "EX WOUND RN-Q" ||
                          item.CODE.Trim().ToUpper() == "EX WOUND NON-RN-Q" ||
                          item.CODE.Trim().ToUpper() == "COOR CARE RN-Q" ||
                          item.CODE.Trim().ToUpper() == "PROC RN-Q" ||
                          item.CODE.Trim().ToUpper() == "PROC NON-RN-Q" ||
                          item.CODE.Trim().ToUpper() == "2:1 PROC RN-Q" ||
                          item.CODE.Trim().ToUpper() == "MEDFILE"))
                        select item;
            // Save the result
            _chart_items_since_admission = query.ToArray();

            // Convert all results to lower case for case insensitive comparisons
            foreach (var item in _chart_items_since_admission) {
                if (item.CATEGORY != null) item.CATEGORY = item.CATEGORY.ToLower();
                if (item.CODE != null) item.CODE = item.CODE.ToLower();
                if (item.DESCRIPTION != null) item.DESCRIPTION = item.DESCRIPTION.ToLower();
                if (item.FIELD_NAME != null) item.FIELD_NAME = item.FIELD_NAME.ToLower();
                if (item.RESULT != null) item.RESULT = item.RESULT.ToLower();
            }

            // Prepare more versions of the chart
            var query2 = from item in _chart_items_since_admission
                    where(item.EVENT_DATETIME >= _pat.unit_arrival) && (item.EVENT_DATETIME <= _pat.pull_finish)
                    select item;
            _chart_items_since_unit_arrival = query2.ToArray();

            query2 = from item in _chart_items_since_admission
                     where (item.EVENT_DATETIME >= _pat.pull_start) && (item.EVENT_DATETIME <= _pat.pull_finish)
                     select item;
            _chart_items_during_pull_period = query2.ToArray();

            query2 = from item in _chart_items_since_admission
                     where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-24))
                     select item;
            _chart_items_24hrs = query2.ToArray();

        }

        // Delay looking for patient procedures until they are wanted
        private void LoadPatientProceduresIfNeeded()
        {
            if (_procedure_events != null) return;

            var db = PFSUtility.NewPfsDataContext();
            var query = from proc in db.PROCEDURE_EVENTs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                        select proc;
            _procedure_events = query.ToArray();
        }



        // Started a new chart item query (of a certain depth)
        // Default is the chart during the pull range.  Options for unit arrival and admission to hospital.
        private IEnumerable<CHART_ITEM> StartNewQuery()
        {
            return StartNewQuery(SearchDepth.SearchDefault);
        }
        private IEnumerable<CHART_ITEM> StartNewQuery(SearchDepth search_depth)
        {
            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    return (from item in _chart_items_during_pull_period select item);
                case SearchDepth.SearchSinceArrival:
                    return (from item in _chart_items_since_unit_arrival select item);
                case SearchDepth.SearchSinceAdmission:
                    return (from item in _chart_items_since_admission select item);
                case SearchDepth.Search24Hrs:
                    return (from item in _chart_items_24hrs select item);
            }
            return null;
        }

        // Is this search term a word or a list of words?
        private bool ValueIsAList(string s) 
        {
            return (s != null) && s.Contains(',');                //is this a comma-separated list?
        }

        // Break apart a wordlist and prepare its members
        private string[] SplitOnCommaAndPrepareElements(string s)
        {
            // If a word list has a space after each comma, we will get leading blanks in each term
            var arr = s.Split(',');
            for (int i = 0; i <= arr.GetUpperBound(0); i++) {
                arr[i] = arr[i].Trim();                         // get rid of leading blanks
                arr[i] = arr[i].Replace(CHAR_COMMA, ",");       // insert a literal comma into search word
                arr[i] = arr[i].ToLower();                      // convert for case-insensitive comparisons
            }
            return arr;
        }

        // Add a basic chart item filter to a LINQ query
        //
        // cat = exact match
        // code_list = exact match; optional "like" match
        // desc_list = "like" match; optional exact match
        // field = exact match
        // result_list = "like" match; optional exact match
        //
        // These are comma-separated lists.  
        // Search for a literal comma by using CHAR_COMMA in the string.
        private IEnumerable<CHART_ITEM> AndItemFilter(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list) 
        {
            // Converted all values to lower case for case-insensitive comparisons.
            if (!String.IsNullOrEmpty(cat)) {
                cat = cat.ToLower();
                query = query.Where(e => e.CATEGORY == cat);
            }

            if (!String.IsNullOrEmpty(code_list)) {
                code_list = code_list.ToLower();
                if (code_list.Left(2) == EXACT_MATCH_PREFIX) {          // exact match override?
                    query = query.Where(e => e.CODE == code_list.Substring(2));
                } else if (code_list.Left(2) == CODE_LIKE_PREFIX)
                {            // override for "like" match?
                    query = query.Where(e => e.CODE.Contains(code_list.Substring(2)));
                } else if (ValueIsAList(code_list)) {
                    query = AndCodeInList(query, code_list);            // find one of the words
                } else {
                    query = query.Where(e => e.CODE.Contains(code_list));      // find this word
                }
            }
            
            if (!String.IsNullOrEmpty(desc_list)) {
                desc_list = desc_list.ToLower();
                if (desc_list.Left(2) == EXACT_MATCH_PREFIX) {          // exact match override?
                    query = query.Where(e => e.DESCRIPTION == desc_list.Substring(2));
                } else if (desc_list.Left(2) == NOT_PREFIX) { 
                    query = query.Where(e => !e.DESCRIPTION.Contains(desc_list.Substring(2)));
                } else if (ValueIsAList(desc_list)) {
                    query = AndDescriptionInList(query, desc_list);
                } else {
                    desc_list = desc_list.Replace(CHAR_COMMA, ",");     // look for literal comma requests
                    query = query.Where(e => e.DESCRIPTION.Contains(desc_list));
                }
            }
            
            if (!String.IsNullOrEmpty(field)) {
                field = field.ToLower();
                query = query.Where(e => e.FIELD_NAME == field);
            }
            
            if (!String.IsNullOrEmpty(result_list)) {
                result_list = result_list.ToLower();                        // make all results case insensitve
                if (result_list.Left(2) == EXACT_MATCH_PREFIX) {     // exact match override?
                    query = query.Where(e => e.RESULT == result_list.Substring(2));
                } else if (ValueIsAList(result_list)) {
                    query = AndResultInList(query, result_list);
                } else {
                    result_list = result_list.Replace(CHAR_COMMA, ",");     // look for literal comma requests
                    query = query.Where(e => e.RESULT.Contains(result_list));
                }
            }
            return query;
        }

        // Look for a code that is one of the words in the list.
        private IEnumerable<CHART_ITEM> AndCodeInList(IEnumerable<CHART_ITEM> query, string code_list) 
        {
            if (String.IsNullOrEmpty(code_list)) return query;
            
            var arr = SplitOnCommaAndPrepareElements(code_list);
            return query.Where(e => e.CODE.ContainsAny(arr));  // use like match. Exact match = arr.Contains(e.CODE));   
        }

        // Look for a description that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndDescriptionInList(IEnumerable<CHART_ITEM> query, string desc_list) 
        {
            if (String.IsNullOrEmpty(desc_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(desc_list);
            return query.Where(e => e.DESCRIPTION.ContainsAny(arr));    // "like" match
        }

        // Look for a result that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultInList(IEnumerable<CHART_ITEM> query, string result_list) 
        {
            if (String.IsNullOrEmpty(result_list)) return query;
            
            var arr = SplitOnCommaAndPrepareElements(result_list);
            return query.Where(e => e.RESULT.ContainsAny(arr));         // "like" match
        }

        private IEnumerable<CHART_ITEM> AndFIELDNAMEInList(IEnumerable<CHART_ITEM> query, string result_list)
        {
            if (String.IsNullOrEmpty(result_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(result_list);
            return query.Where(e => e.FIELD_NAME.ContainsAny(arr));         // "like" match
        }


        // Look for a result that does not contain one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultNotInList(IEnumerable<CHART_ITEM> query, string result_list)
        {
            if (String.IsNullOrEmpty(result_list)) return query;
            
            var arr = SplitOnCommaAndPrepareElements(result_list);
            return query.Where(e => !e.RESULT.ContainsAny(arr));        // "like" match
        }

        private string DescribeSearchDepth(SearchDepth search_depth) 
        {
            string result = "";
            
            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    //result = "in pull range"
                    result = "";                         //be quiet since this is default
                    break;
                case SearchDepth.SearchSinceArrival:
                    result = "since arrival to unit";
                    break;
                case SearchDepth.SearchSinceAdmission:
                    result = "since admission";
                    break;
                case SearchDepth.Search24Hrs:
                    result = "since 24 hours ago";
                    break;
            }
            
            return result;
        }

        // Describe a query that has already been run
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Make a simple query and describe the results
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            var arr = query.ToArray();
            return Describe(arr, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private string LookingFor(string so_far, string what, string op, string value)
        {
            string result;
            if (String.IsNullOrEmpty(value)) return so_far;

            if (so_far == "")
                result = "looking for " + what;
            else
                result = so_far + " and " + what;

            if (value.Left(2) == EXACT_MATCH_PREFIX)
                result += "='" + value.Substring(2) + "'";
            else if (value.Left(2) == CODE_LIKE_PREFIX)
                result += " contains " + "'" + value.Substring(2) + "'";
            else if (ValueIsAList(value))
                result += " in '" + value + "'";
            else
                result += op + "'" + value + "'";
            
            return result;
        }
        private string Describe(CHART_ITEM[] arr, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string result = "";
            result = LookingFor(result, "cat", "=", cat);
            result = LookingFor(result, "code", "=", code_list);
            result = LookingFor(result, "desc", " contains ", desc_list);
            result = LookingFor(result, "field", "=", field);
            result = LookingFor(result, "result", " contains ", result_list);

            if (arr.Count() == 0) {
                result += "; not found " + DescribeSearchDepth(search_depth);
            } else {
                var e = arr[0];
                result = "FOUND: " + result + "; found";
                // We might have searched for a pattern or word list in several fields - show what was found
                if (e.CATEGORY != null)     result += " cat='" + e.CATEGORY + "'";
                if (e.CODE != null)         result += " code='" + e.CODE + "'";
                if (e.DESCRIPTION != null)  result += " desc='" + e.DESCRIPTION + "'";
                if (e.FIELD_NAME != null)   result += " field='" + e.FIELD_NAME + "'";
                if (e.RESULT != null)       result += " result='" + e.RESULT + "'";
                // Are there more results?  Just say how many; we aren't going to list them.
                if (arr.Count() == 2) {
                    result += " (1 more result)";
                } else if (arr.Count() > 2) {
                    result += " (" + (arr.Count() - 1) + " more results)";
                }
            }
            
            return result;
        }


        //Set an indicator for this reason (low level)
        private void SetInd(int inum, string reason)
        {
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Set Ind #" + inum + ": " + reason);          //already set - repeat for verbose only
            } else {
                _inds[inum].is_checked = true;
                Program.Audit("Set Ind #" + inum + ": " + reason);
            }
        }

        //Clear an indicator for this reason (low level)
        private void ClrInd(int inum, string reason)
        {
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Clr Ind #" + inum + ": " + reason);          //already clear - repeat verbose only
            } else {
                _inds[inum].is_checked = false;
                Program.Audit("Clr Ind #" + inum + ": " + reason);
            }
        }

        // Count how many items meet the conditions
        // All items are listed
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountItems(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            int count = query.Count();

            // always return what was found
            found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
            // echo the result?
            if (trace) Program.VerboseAudit(found_what);
            
            return count;
        }

        // Count how many items meet the conditions
        // This has the option to list every entry or give a total
        private int CountResultInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what="";
 
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);

            var arr = SplitOnCommaAndPrepareElements(result_list);

            foreach (var item in query) {
                // Figure out which of the search words were found
                var s = item.RESULT.ContainsWhich(arr);
                found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' code='" + item.CODE + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "'";
                if (trace) Program.VerboseAudit(found_what);
                count++;
                if (count_mode != CountMode.CountAll) break;        //stop counting
            }
            
            if (count > 0) {
                //We already printed what was found; maybe add how many?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }
            
            return count;
        }

        // Count how many items contain ALL results in list
        private int CountResultInListAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what = "";

            var arr = SplitOnCommaAndPrepareElements(result_list);

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            for (int i = 0; i <= arr.GetUpperBound(0); i++)
            {
                if (arr[i].StartsWith(NOT_PREFIX))
                {
                    query = query.Where(e => !e.RESULT.Contains(arr[i].Substring(2)));
                }
                else
                {
                    query = query.Where(e => e.RESULT.Contains(arr[i]));
                }
            }

            count = query.Count();

            if (count > 0)
            {
                found_what = "found item with all results in '" + result_list + "' without those prefixed by !!";
                if (trace) Program.VerboseAudit(found_what);
            }
            else
            {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (ValueIsAList(result_list)) {
                return CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what);
            } else {
                return CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what);
            }
        }

        // Is there an item with this result?
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list) 
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth) 
        {
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContains(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what) 
        {
            if (ValueIsAList(result_list)) {
                return (CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            } else {
                return (CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
            }
        }

        // Result contains all in list
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (ValueIsAList(result_list))
            {
                return (CountResultInListAll(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            }
            else
            {
                found_what = "Not valid for non-list results.";
                return false;
            }
        }
        // Count how many items meet the conditions, with none of the results in result_list.
        // This has the option to say how many were found rather than list all.
        private int CountResultNotInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what) 
        {
            int count = 0, rec_count = 0;
            bool found_one;

            found_what = "";
            var arr = SplitOnCommaAndPrepareElements(result_list);         // also deals with CHAR_COMMA
            
            //Do not filter by result here; we need all results
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");
            
            foreach(var item in query) {
                rec_count++;
                //Look for each search word in the result
                found_one = false;
                
                foreach (string s in arr) {
                    if (item.RESULT.Contains(s)) {
                        found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "' -- ignore this";
                        Program.VerboseAudit(found_what);
                        found_one = true;
                        break;
                    }
                }

                if (! found_one) {
                    //none of the words were found - good!
                    found_what = Describe(cat, code_list, desc_list, field, "", search_depth) + " -- does not contain '" + result_list + "'";
                    Program.VerboseAudit(found_what);
                    count++;
                    if (count_mode != CountMode.CountAll) break;
                }
            }
            
            if (count > 0) {
                //We already printed what was found; maybe show the total?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else if (rec_count > 0) {
                //We already printed what we ignored
            } else {
                //Print what did not exist
                found_what = Describe(cat, code_list, desc_list, field, "", search_depth);      //not found
                Program.VerboseAudit(found_what);
            }
            
            return count;
        }

        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, string found_what) 
        {
            // Search in the pull range with trace on
            return CountResultDoesNotContain(cat, code_list, desc_list, field, result_list, out found_what, SearchDepth.SearchDefault, true);
        }
        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, out string found_what, SearchDepth search_depth, bool trace)
        {
            return CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0;
        }

        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            return (CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0);
        }

        // Set the indicator if the conditions are met
        //
        private bool SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);

        }
        private bool SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            bool b = false;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return true;
            
            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what)) {
                SetInd(inum, found_what);                          //echo here - found
                b = true;
            } else {
                Program.VerboseAudit(found_what);                  //and here - not found
            }
            return b;
        }
        // Special contains all
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContainsAll(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already set
            if (_inds[inum].is_checked) return;

            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                          //echo here - found
            }
            else
            {
                Program.VerboseAudit(found_what);                  //and here - not found
            }
        }
        // Set the indicator if the result does not contain any of the words in result_list
        //
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultDoesNotContain(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {   
            string found_what;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return;
            
            //Turn trace off for ResultDoesNotContain() and echo what was set below with SetInd
            if (ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                           //echo here - not found
            } else {
                Program.VerboseAudit(found_what);                   //and here - found
            }
        }

        // Clear the indicator if the result contains one of the words in the result_list
        //
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already clear
            if (! _inds[inum].is_checked) return;
            
            //Turn trace off for ResultContains() and echo what was set below with SetInd
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what)) {
                ClrInd(inum, found_what);                           //echo here - found
            } else {
                Program.VerboseAudit(found_what);                   //and here - not found
            }
        }


        //These slightly smaller functions are meant for places where you aren't really looking for a chart result
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            return (CountResultContains(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
        }

        // Variations with optional parameters (can switch to optional parameters in VS 2010)
        //
        private void SetIndIfFound(int inum, string cat)
        {
            SetIndIfResultContains(inum, cat, "", "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list)
        {
            SetIndIfResultContains(inum, cat, code_list, "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private void ClrIndIfFound(int inum, string cat)
        {
            ClrIndIfResultContains(inum, cat, "","","","", SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }


        // Get the max/total value from a result (usually in the middle of the text)
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetIntValue(get_mode, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            int value;
            int result = 0;
            bool found_one = false;

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            
            //Look for a number in the result
            
            foreach(var item in query) {
                // RESULT might be null so convert to empty string so Split won't blow up
                var arr = PFSUtility.DBToString(item.RESULT).Split(' ');
                foreach (string s in arr) {
                    //Look for 1st character numeric; IsNumeric("60min") will fail so look at 1st char only
                    if (s.Left(1).IsNumeric()) {
                        value = (int)s.Val();                           //Use Val; ToInteger will error on "60min"
                        switch (get_mode) {
                            case GetValueMode.GetMax:
                                result = Math.Max(value, result);       //max
                                break;
                            case GetValueMode.GetTotal:
                                result += value;                        //total
                                break;
                            case GetValueMode.GetLast:
                                result = value;                         //last
                                break;
                        }
                        
                        // print what we are searching for (the first time)
                        if (! found_one) {
                            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
                        }
                        found_one = true;
                        // print each value found
                        Program.VerboseAudit("  found numeric value " + result);
                        //Keep going in case there are more
                    }
                } 
            }
            
            if (! found_one) {
                //show what was not found
                Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
            }
            
            return result;
        }

        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetMaxValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return GetIntValue(GetValueMode.GetMax, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetTotalValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return GetIntValue(GetValueMode.GetTotal, cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Get a result; returns true if found with return_result
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result)
        {
            return GetResult(cat, code_list, desc_list, field, out return_result, SearchDepth.SearchDefault);
        }
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0) {
                return_result = PFSUtility.DBToString(query.First().RESULT);
            } else {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }
        private void GetResultforOther(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "Other (comment)");
            return_result="";
            foreach (var item in query)
            {
                if (!String.Equals(item.RESULT, "Other (comment)"))
                {
                    return_result = item.RESULT;
                    //Program.VerboseAudit(found_what);
                    break;
                }

            }
            return;
        }

        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, out string return_result, out DateTime return_evdt)
        {
            return GetResultAndEVDT(cat, code_list, desc_list, field, out return_result, out return_evdt, SearchDepth.SearchDefault);
        }
        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, out string return_result, out DateTime return_evdt, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0)
            {
                return_result = PFSUtility.DBToString(query.First().RESULT);
                return_evdt = PFSUtility.DBToDateTime(query.First().EVENT_DATETIME);
            }
            else
            {
                return_result = "";
                return_evdt = DateTime.MinValue;
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }


        //======================================================================================
        // Mapping starts here
        //======================================================================================

        private void Check_1_2_3_4()
        {
            string found_what;
            SearchDepth scope = SearchDepth.SearchDefault;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("1. ADL Self");
            Program.VerboseAudit("2. ADL Assist");
            Program.VerboseAudit("3. ADL Extended");
            Program.VerboseAudit("4. ADL Complete");
            Program.VerboseAudit("---------------");
            
            // 4. COMPLETE CARE

            if (_pat.age <= 3.0) {
                SetInd(4,"Age <=3 years");
                //SetInd(104, "Age <=3 years");
            }
            if (_inds[4].is_checked) return;

            bool nut1 = ResultContains("", "NUT ASSIST-Q", "", "", "Full assist",scope);
            bool nut2 = ResultContains("", "OTHER NUTRITION-Q", "", "", "TPN,Lipids,Omega 3,Tube feed", scope);
            bool nutnpo = ResultContains("", "DIET TYPE-Q", "", "", "NPO", scope);
            bool hyg1 = ResultContains("", "HYG ASSIST-Q", "", "", "Maximum assist", scope);
            bool hyg1a = ResultContains("", "HYG ASSIST-Q", "", "", EXACT_MATCH_PREFIX + "dependent", scope);
            if ((nut1 || nut2 || nutnpo) && (hyg1 || hyg1a)) 
                SetInd(4, "Complete Eating & Hygiene");

            //bool nut3 = ResultDoesNotContain("", "NUT ASSIST-Q", "", "", "Self,Other,setup only,no straw", scope);
            bool nut3 = ResultContains("", "NUT ASSIST-Q", "", "", "Partial assist,full assist,Alternated solids and liquids,Swallowed 2 times per bite/sip,Supervision,Chin tucked during swallow", scope);
            bool nut4 = ResultContains("", "OTHER NUTRITION-Q", "", "", "", scope);
            bool hyg2 = ResultContains("", "HYG ASSIST-Q", "", "", "Moderate assist,Maximum assist", scope);
            bool hyg2a = ResultContains("", "HYG ASSIST-Q", "", "", EXACT_MATCH_PREFIX + "dependent", scope);
            bool act1 = ResultDoesNotContain("", "ACT ASSIST-Q", "", "", "None,Other", scope);
            bool act2 = ResultContains("", "REPOS ASSIST-Q", "", "", "assist", scope);
            //int incont = Incont4xIn12Hrs();
            //if (incont >= 4) SetInd(3, "FOUND Incontinence items 4x in 12 hours.");
            bool u1 = ResultContains("", "URINE INCONT-Q", "", "", "Yes", scope);
            bool u2 = ResultContains("", "URINE INCONT PEDS-Q", "", "", "Yes", scope);
            int unum = CountResultContains("", "URINE OCCUR-Q", "", "", "Yes", scope, false, out found_what);
            bool ux = (u1 || u2) && (unum > 0);
            if (ux && (unum >= 4))
                SetInd(3, "FOUND Urine incontinence 4x");

            bool b1 = ResultContains("", "BOWEL INCONT-Q", "", "", "Yes", scope);
            bool b2 = ResultContains("", "CONT STOOL PEDS-Q", "", "", "Yes", scope);
            int bnum = CountResultContains("", "STOOL OCCUR-Q", "", "", "Yes", scope, false, out found_what);
            bool bx = (b1 || b2) && (bnum > 0);
            if (bx && (bnum >= 4))
                SetInd(3, "FOUND Stool incontinence 4x");

            //bool b1 = ResultContains("", "BOWEL INCONT-Q", "", "", "Yes");
            //bool b2 = ResultContains("", "CONT STOOL PEDS-Q", "", "", "No");
            if (3 <= ((nut1 || nut2 || nutnpo || nut3 || nut4) ? 1 : 0) + 
                     ((hyg1 || hyg1a || hyg2 || hyg2a) ? 1:0) + 
                     ((act1 || act2) ? 1:0) + 
                     ((ux || bx) ? 1 :0)) SetInd(3, "Assist on 3 of Eating,Hygiene,Activity,Toileting");
            if (nut1 || nut2 || nutnpo || nut3 || nut4 || hyg1 || hyg1a || hyg2 || hyg2a || act1 || act2 || ux || bx) SetInd(2, "Assist on some of Eating,Hygiene,Activity,Toileting");

            bool nut5 = ResultContains("", "NUT ASSIST-Q", "", "", "Self", scope);
            bool hyg3 = ResultContains("", "HYG ASSIST-Q", "", "", "Independent", scope);
            bool act3 = ResultDoesNotContain("", "ACT ASSIST-Q", "", "", "None", scope);
            if (nut5 || hyg3 || act3) SetInd(1, "Self/Minimal Care on Eating,Hygiene,Activity");

            //default ADL complete for ICUs when no documentation

            //scope = SearchDepth.Search24Hrs;
            //nut1 = ResultContains("", "NUT ASSIST-Q", "", "", "Full assist", scope);
            //nut2 = ResultContains("", "OTHER NUTRITION-Q", "", "", "TPN,Lipids,Omega 3,Tube feed", scope);
            //nutnpo = ResultContains("", "DIET TYPE-Q", "", "", "NPO", scope);
            //hyg1 = ResultContains("", "HYG ASSIST-Q", "", "", "Maximum assist", scope);
            //hyg1a = ResultContains("", "HYG ASSIST-Q", "", "", EXACT_MATCH_PREFIX + "dependent", scope);
            //if ((nut1 || nut2 || nutnpo) && (hyg1 || hyg1a)) SetInd(104, "Complete Eating & Hygiene");

            //nut3 = ResultDoesNotContain("", "NUT ASSIST-Q", "", "", "Self,Other", scope);
            //nut4 = ResultContains("", "OTHER NUTRITION-Q", "", "", "", scope);
            //hyg2 = ResultContains("", "HYG ASSIST-Q", "", "", "Minimal assist,Moderate assist,Maximum assist", scope);
            //hyg2a = ResultContains("", "HYG ASSIST-Q", "", "", EXACT_MATCH_PREFIX + "dependent", scope);
            //act1 = ResultDoesNotContain("", "ACT ASSIST-Q", "", "", "None,Other", scope);
            //act2 = ResultContains("", "REPOS ASSIST-Q", "", "", "assist", scope);
            ////int incont = Incont4xIn12Hrs();
            ////if (incont >= 4) SetInd(3, "FOUND Incontinence items 4x in 12 hours.");
            //u1 = ResultContains("", "URINE INCONT-Q", "", "", "Yes", scope);
            //u2 = ResultContains("", "URINE INCONT PEDS-Q", "", "", "Yes", scope);
            //unum = CountResultContains("", "URINE OCCUR-Q", "", "", "Yes", scope, false, out found_what);
            //ux = (u1 || u2) && (unum > 0);
            //if (ux && (unum >= 4))
            //    SetInd(103, "FOUND Urine incontinence 4x");

            //b1 = ResultContains("", "BOWEL INCONT-Q", "", "", "Yes", scope);
            //b2 = ResultContains("", "CONT STOOL PEDS-Q", "", "", "Yes", scope);
            //bnum = CountResultContains("", "STOOL OCCUR-Q", "", "", "Yes", scope, false, out found_what);
            //bx = (b1 || b2) && (bnum > 0);
            //if (bx && (bnum >= 4))
            //    SetInd(103, "FOUND Stool incontinence 4x");

            ////bool b1 = ResultContains("", "BOWEL INCONT-Q", "", "", "Yes");
            ////bool b2 = ResultContains("", "CONT STOOL PEDS-Q", "", "", "No");
            //if (3 <= ((nut1 || nut2 || nutnpo || nut3 || nut4) ? 1 : 0) +
            //         ((hyg1 || hyg1a || hyg2 || hyg2a) ? 1 : 0) +
            //         ((act1 || act2) ? 1 : 0) +
            //         ((ux || bx) ? 1 : 0)) SetInd(103, "Assist on 3 of Eating,Hygiene,Activity,Toileting");
            //if (nut1 || nut2 || nutnpo || nut3 || nut4 || hyg1 || hyg1a || hyg2 || hyg2a || act1 || act2 || ux || bx)
            //{
            //    SetInd(102, "Assist on some of Eating,Hygiene,Activity,Toileting");
            //}

            //nut5 = ResultContains("", "NUT ASSIST-Q", "", "", "Self", scope);
            //hyg3 = ResultContains("", "HYG ASSIST-Q", "", "", "Independent", scope);
            //act3 = ResultDoesNotContain("", "ACT ASSIST-Q", "", "", "None", scope);
            //if (nut5 || hyg3 || act3) SetInd(101, "Self/Minimal Care on Eating,Hygiene,Activity");

        }

        private int Incont4xIn12Hrs()
        {
            //SetBucketSize(60);   // set 60-min buckets
            //var buckets = new List<int>();    // list of bucket numbers (for each match)
            //string codelist = "Incontinence Items";
            //AddBuckets(buckets, "", "URINE INCONT-Q", "", "", "Yes");
            //AddBuckets(buckets, "", "BOWEL INCONT-Q", "", "", "Yes");
            //AddBuckets(buckets, "", "CONT STOOL PEDS-Q", "", "", "No");
            //// Instead of counting the number of buckets to get the frequency,
            //// we want to see if any 12 hours in a row had 4 or more assessments in the buckets.
            //// Make a query that returns each bucket number and its item count.
            //var query = buckets.GroupBy(x => x)   // group by the hour (x is an int)
            //                   .Select(g => new { Hour = g.Key, Count = g.Count() })
            //                   .OrderBy(e => e.Hour);
            //var arr = query.ToArray();

            //string s = "";
            //foreach (var e in arr)
            //{
            //    s += ",(" + e.Hour + "," + e.Count + ")";
            //}
            //if (s.Left(1) == ",") s = s.Substring(1);
            //Program.VerboseAudit("Hourly buckets for " + codelist + " = " + s);

            //int ct = 0;
            //int maxhour;
            //bool has_something = false;
            //for (int i = 0; (i < arr.GetUpperBound(0)); i++)
            //{
            //    ct = 0;
            //    maxhour = arr[i].Hour + 12; // only add up to index with hour <= Hour+12
            //    for (int j = i; (j < arr.GetUpperBound(0)); j++)
            //    {
            //        if (arr[j].Hour <= maxhour)
            //        {
            //            ct++;
            //            has_something = true;
            //        }
            //        if (ct >= 4) return ct;
            //    }
            //}
            //if (has_something)
            //    return 1;
            //else
            return 0;
        }


        private void Check_5()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("5. ADL Rehab");
            Program.VerboseAudit("---------------");

            
            if (!Program.g_rehab) return;

            bool pt = ResultContains("", "PT FREQ-Q", "", "", "/wk,daily,day", SearchDepth.SearchSinceAdmission);
            if (pt) SetIndIfResultContains(5, "", "PT FUP-Q", "", "", "yes", SearchDepth.SearchSinceAdmission);
            bool ot = ResultContains("", "OT FREQ-Q", "", "", "/wk,bid,daily", SearchDepth.SearchSinceAdmission);
            if (ot) SetIndIfResultContains(5, "", "OT FUP-Q", "", "", "yes", SearchDepth.SearchSinceAdmission);
            
            SetIndIfResultContains(5, "", "CARDIAC REHAB-Q", "", "", "", SearchDepth.SearchSinceAdmission);
            SetIndIfResultContains(5, "", "SLP FUP-Q", "", "", "Yes", SearchDepth.SearchSinceAdmission);

        }


        private void Check_6_7()
        {    
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("6. ADL 2-3 Caregivers");
            Program.VerboseAudit("7. ADL 4 or more Caregivers");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(7, "", "REPOS ASSIST-Q", "", "", "4");
            if (_inds[7].is_checked) return;
            SetIndIfResultContains(6, "", "REPOS ASSIST-Q", "", "", "2,3");
            if (_inds[6].is_checked) return;
            SetIndIfResultContains(6, "", "ACT ASSIST-Q", "", "", "2,3");
            SetIndIfResultContains(6, "", "CHEST BOUNCE NICU-Q", "", "", "");
            bool v = ResultContains("", "O2 DEVICE-Q", "", "", "Ventilator");
            bool k = ResultContains("", "KANGAROO NICU-Q", "", "", "");
            if (v && k) SetInd(6, "O2 Device is Ventilator PLUS Kangaroo found.");
        }

        private void Check_8()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("8. Communication");
            Program.VerboseAudit("---------------");
            //175214 trach pt mar 31 3a-3p
            //161953 did not capture mar 31 3a-3p
            SetIndIfResultDoesNotContain(8, "", "SPEECH-Q", "", "", "Clear,Appropriate for,Untestable,UTA");
            SetIndIfResultDoesNotContain(8, "", "COMP SPEECH-Q", "", "", "Clear,Appropriate for,Untestable,UTA");

            string rlist = "impaired vision,blind,double vision";
            bool rt = ResultContains("", "RT EYE-Q", "", "", rlist);
            if (rt)
            {
                SetIndIfResultContains(8, "", "LT EYE-Q", "", "", rlist);
            }

            rlist = "impaired,deaf";
            rt = ResultContains("", "RT EAR-Q", "", "", rlist);
            if (rt)
            {
                SetIndIfResultContains(8, "", "LT EAR-Q", "", "", rlist);
            }
            SetIndIfResultContains(8, "", "MEDFILE", "", "", "yes",SearchDepth.SearchSinceAdmission);

        }

        private void Check_9()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("9. Cognitive Support");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(9, "", "ORIENT LEVEL-Q", "", "", "Disoriented to person,Disoriented to place,Disoriented to time,Disoriented to situation,Disoriented x4");
            SetIndIfResultContains(9, "", "COMP ORIENT LEVEL-Q", "", "", "Disoriented to person,Disoriented to place,Disoriented to time,Disoriented to situation,Disoriented x4");
            SetIndIfResultContains(9, "", "ORIENT PEDS-Q", "", "", "Disoriented to person,Disoriented to place,Disoriented to time,Confused");

        }

        private void Check_10_11()
        {
            string reslist, found_what;
            int ct;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("10. Behavior/Emotional Management");
            Program.VerboseAudit("11. Behavior/Emotional Mgmt - q 1 Hour");
            Program.VerboseAudit("---------------");

            string rlist = "Anxious,Affect inconsistent with mood,aggressive,combative,non-compliant,";
            rlist += "not interactive,restless,tearful,uncooperative,verbal,withdrawn";
            bool beh = ResultContains("", "PT BEHAV-Q", "", "", rlist);
            if (beh)
            {
                reslist = "Medication,Emotional support,Relaxation technique,Therapeutic communication,Distraction";
                SetIndIfResultContains(10, "", "PT SUPP-Q", "", "", reslist);
            }

            rlist = "Anxious,Affect inconsistent with mood,aggressive,non-compliant,";
            rlist += "non-supportive,tearful,uncooperative,verbal";
            SetIndIfResultContains(10, "", "FAM BEHAV-Q", "", "", rlist);

            if (Exists("", "INF COMFORT NICU-Q", "", "", ""))
            {
                rlist = "agitated,irritable,restless";
                SetIndIfResultContains(10, "", "INF BEHAV PEDS-Q", "", "", rlist);

                bool rt = ResultContains("", "INF BEHAV PEDS-Q", "", "", "crying");
                if (rt) SetIndIfResultContains(10, "", "INF BEHAV PEDS-Q", "", "", "difficult to console");
            }


            if (_inds[10].is_checked) return;

            CheckVisBehav();
            
        }

        private void CheckVisBehav()
        {
            //rlist = "appropriate for situation,calm,verbal,flat affect,cooperative,supportive";
            //SetIndIfResultDoesNotContain(10, "", "VIS BEHAV-Q", "", "", rlist);
            //but cant use this because of negative terms uncooperative and non-supportive
            var query = StartNewQuery(SearchDepth.SearchDefault);
            string reslist = "Anxious,Affect inconsistent with mood,Aggressive physically,Aggressive verbally,Non-compliant,Non-supportive,Tearful,Uncooperative";
            query = AndItemFilter(query, "", "VIS BEHAV-Q", "", "", reslist);
            query = query.Where(e => !e.RESULT.Contains("appropriate for situation"));
            query = query.Where(e => !e.RESULT.Contains("calm"));
            query = query.Where(e => !e.RESULT.Contains("verbal"));
            query = query.Where(e => !e.RESULT.Contains("flat affect"));
            query = query.Where(e => !e.RESULT.Contains("cooperative") || e.RESULT.Contains("uncooperative"));
            query = query.Where(e => !e.RESULT.Contains("supportive") || e.RESULT.Contains("non-supportive"));
            int ct = query.Count();
            if (ct >= 1)
            {
                string foundlist;
                foreach (var item in query)
                {
                    foundlist = "FOUND: VIS BEHAV-Q with result=" + item.RESULT + " at " + item.EVENT_DATETIME.ToString();
                    SetInd(10, foundlist);
                }

            }
            
        }

        // (this isn't really a Q1h count -- it is just a count) Make this a q1hr count
        private int ReturnQ1HrCount(string code_list, string result_list)
        {
            var buckets = new List<int>();
            SetBucketSize(60);
            AddBuckets(buckets, "", code_list, "", "", result_list);
            return CountBuckets(buckets);
        }

        private bool IsQ1Hour(int count)
        {
            return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q1H);
        }

        // Use this if counting safety buckets
        //private void CheckSafety(int count, string desc)
        //{
        //    if (_inds[13].is_checked) return;             //skip if highest already checked
        //    if (count == 0) return;
        //    switch (FreqForCount(_pat.los_hours, count))
        //    {
        //    case Frequencies.Q30M:
        //        SetInd(13, desc + " q30min");
        //        break;
        //    case Frequencies.Q1H:
        //    case Frequencies.Q2H:
        //        SetInd(12, desc + " q2h");
        //        break;
        //    default:
        //        Program.VerboseAudit(desc + ": " + count + " in " + Math.Round(_pat.los_hours) + " hours is not enough");
        //        break;
        //    }
        //}

        private void Check_12_13()
        {
            bool suppress13=false;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("12. Safety Management - q 2 Hours");
            Program.VerboseAudit("13. Safety Management - q 30 Minutes");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(12, "", "NV REST-Q", "", "", "L&T,L\\T\\T,ETT,O");

            SetIndIfResultContains(35, "", "NV REST-Q", "", "", "ETT");
            SetIndIfResultContains(16, "", "NV REST-Q", "", "", "ETT");

            SetIndIfResultContains(37, "", "NV REST-Q", "", "", "L&T,L\\T\\T,O");
            SetIndIfResultContains(16, "", "NV REST-Q", "", "", "L&T,L\\T\\T,O");

            SetIndIfResultContains(13, "", EXACT_MATCH_PREFIX + "V REST-Q", "", "", "A,O,S");
            SetIndIfResultContains(36, "", EXACT_MATCH_PREFIX + "V REST-Q", "", "", "A,O,S");
            SetIndIfResultContains(18, "", EXACT_MATCH_PREFIX + "V REST-Q", "", "", "A,O,S");

            //bool highfall = ResultContains("", "FALL RISK-Q", "", "", EXACT_MATCH_PREFIX + "H");
            //bool vhighfall = ResultContains("", "FALL RISK-Q", "", "", EXACT_MATCH_PREFIX+"VH");
            //if (highfall || vhighfall)
            //{
            //    SetIndIfResultContains(12, "", "BED ALARM-Q", "", "", "On");
            //    SetIndIfResultContains(12, "", "CHAIR ALARM-Q", "", "", "On");
            //}
            //
            SetIndIfResultContains(12, "", "MOBILITY MOD SCH-Q", "", "", "1");
            SetIndIfResultContains(12, "", "MENTATION MOD SCH-Q", "", "", "1");
            SetIndIfResultContains(12, "", "ELIM MOD SCH-Q", "", "", "1");
            SetIndIfResultContains(12, "", "PRIOR FALL MOD SCH-Q", "", "", "2");

            SetIndIfResultContains(12, "", "VIDEO MON-Q", "", "", "On");
            SetIndIfResultContains(12, "", "NON VIOL RESTRAINT NEED-Q", "", "", "C");

            //string res;
            //int value = 0;
            //if (GetResult("", "FALL SCORE PEDS-Q", "", "", out res))
            //{
            //    if (res.Left(1).IsNumeric())
            //    {
            //        value = (int)res.Val(); //Use Val; ToInteger will error on "60min"
            //        if (value >= 3) SetInd(12, "FALL SCORE PEDS-Q value=" + res);
            //    }
            //}

            SetIndIfResultContains(12, "", "LINES TUBES NICU PEDS-Q", "", "", "Yes");

            SetIndIfResultContains(13, "", EXACT_MATCH_PREFIX + "VIOL RESTRAINT NEED-Q", "", "", "C");
            SetIndIfResultContains(13, "", "PT COMPANION-Q", "", "", "Initiated,continued,accompanied patient off unit");
            //SetIndIfResultContains(13, "", "HOURS ON CC-Q", "", "", "");
            SetIndIfResultContains(13, "", "HOURS ON WB-Q", "", "", "");
            SetIndIfResultContains(13, "", "HOURS ON ECMO-Q", "", "", "");

            SetIndIfResultContains(13, "", "SAFETY RN-Q,SAFETY NON-RN-Q", "", "", "start");
    
        }

        private void Check_14()
        {    
            string reslist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("14. Isolation");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(14, "", "ISOL PREC-Q", "", "", "Contact,droplet,airborne,enhanced,burn");
            SetIndIfResultContains(14, "", "AIR PREC-Q", "", "", "Maintained");
            SetIndIfResultContains(14, "", "DROP PREC-Q", "", "", "Maintained");
            SetIndIfResultContains(14, "", "CON PREC-Q", "", "", "Maintained");
            SetIndIfResultContains(14, "", "BURN PREC-Q", "", "", "Maintained");
            SetIndIfResultContains(14, "", "ENH CON PREC-Q", "", "", "Maintained");
            SetIndIfResultContains(14, "", "HAZARDMED/CHEMO PREC-Q", "", "", "Maintained");

        }

        private void CheckAssessment(int count, string desc)
        {
            if (_inds[18].is_checked) return;          //skip if highest already checked
            if (count == 0) return;                    //skip if none
            
            // This should work the same as the original code:
            switch (FreqForCount(_pat.los_hours, count)) {
                case Frequencies.Q30M:
                    SetInd(18, desc);
                    break;
                case Frequencies.Q1H:
                    SetInd(17, desc);
                    break;
                case Frequencies.Q2H:
                    SetInd(16, desc);
                    break;
                case Frequencies.Q4H:
                    SetInd(15, desc);
                    break;
                default:
                    break;
            }

        }


        private void Check_15_16_17_18()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("15. Assessment q4h");
            Program.VerboseAudit("16. Assessment q2h");
            Program.VerboseAudit("17. Assessment q1h");
            Program.VerboseAudit("18. Assessment q30min");
            Program.VerboseAudit("---------------");

            var query = StartNewQuery(SearchDepth.Search24Hrs);
            query = AndItemFilter(query, "", "MEDFILE", "insulin regular human", "Intravenous", "");
            query = query.Where(e => e.ORDER_TIMING.ToLower() == "continuous");
            if (query.Count() > 0)
                SetInd(17, "Found insulin+intravenous+continuous in the past 24 hours");

            query = StartNewQuery(SearchDepth.Search24Hrs);
            query = AndItemFilter(query, "", "MEDFILE", "insulin,glargine,lispro,nph,regular human", "Subcutaneous", "");
            var query2 = (from item in query select new { item.EVENT_DATETIME }).Distinct();
            int ct = query2.Count();
            if (ct >= 4)
                SetInd(17, "Found [ " + ct + " ] subcutaneous insulin in the past 12 hours (minimum = 4)");

            if (_pat.unit_name == "NICU")
                SetInd(15, "NICU triggered q4 by default");

            CountAssessments(30);               // always do q30 buckets--but use different freq tables for icu vs non-icu.

        }

        private bool IsICU()
        {
            switch (_pat.unit_name)
            {
                case "MICU":
                case "CVICU":
                case "SICU":
                case "BURN":
                case "NSICU":
                case "BMC ICU":
                    return true;
                default:
                    return false;
            }
        }

        //private bool IsTele()
        //{
        //    switch (_pat.unit_name)
        //    {
        //        case "TELE":
        //            return true;
        //        default:
        //            return false;
        //    }
        //}


        private void CountAssessments(int bucket_size)
        {
            int ct;
            string codelist;
            List<int> buckets;

            SetBucketSize(bucket_size);

            buckets = new List<int>();
            AddBucketsNotContain(buckets, "", "TUBE FEED METH NICU-Q", "", "", "NA,Other");
            codelist = "OTHER NUTRITION-Q,DRAIN OUTPUT-Q,CT OUTPUT-Q,CBI TOTAL OUTPUT-Q,IUC OUT-Q,IUC OUT 2-Q";
            AddBuckets(buckets, "", codelist, "", "");
            AddBuckets(buckets, "", "NXSTAGE CONNECTION-Q", "", "");
            AddBuckets(buckets, "", "CRRT CONNECTION-Q", "", "Yes");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "FluidMgt count=" + ct);
            AlternateBucketCounts(buckets,"FluidMgt");

            //if (_inds[18].is_checked) return;

            buckets = new List<int>();
            AddBuckets(buckets, "", "URINE OUT-Q", "", "");
            ct = CountBuckets(buckets);
            if (ct >= 8) 
                SetInd(16, "Urine out count=" + ct);
            else if (ct >= 5) 
                SetInd(15, "Urine out count=" + ct);

            buckets = new List<int>();
            codelist = "NUM PAIN INTERV-Q,BEHAV PAIN INTERV-Q,FACES PAIN INTERV-Q";
            codelist += ",VERB PAIN INTERV-Q,NPASS PAIN INTERV-Q,FLACC PAIN INTERV-Q,CPOT PAIN INTERV-Q";
            AddBucketsNotContain(buckets, "", codelist, "", "", "Declined,MD Notified");

            codelist = "NUM PAIN NONPHARM INT,BEHAV PAIN NONPHARM INT,FLACC PAIN NONPHARM INT,CPOT PAIN NONPHARM INT";
            AddBuckets(buckets, "", codelist, "", "", "Repositioned,Cold,Diversion/Relaxation,Heat,Splinting","Emotional Support,Other");

            AddDependentBuckets(buckets, "PCA BOLUS-Q", "RESP-Q");
            //AddBuckets(buckets, "", "WAS TOTAL-Q", "", "Yes");
            AddBuckets(buckets, "", "CIWA-AR", "", "");
            AddBuckets(buckets, "", "ORAL TRX-Q", "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "MedMgt count=" + ct);
            AlternateBucketCounts(buckets, "MedMgt");
            //if (_inds[18].is_checked) return;

            CheckInsulin();

            buckets = new List<int>();
            AddDependentBuckets(buckets, "RESP-Q", "SPO2-Q");
            AddDependentBuckets(buckets, "CHEST BOUNCE NICU-Q", "SPO2-Q");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "PulmMgt count=" + ct);
            AlternateBucketCounts(buckets, "PulmMgt");
            //if (_inds[18].is_checked) return;

            buckets = new List<int>();

            codelist = "CMS FLAP-Q";
            codelist += ",RUE CAP REF-Q,LUE CAP REF-Q,RH CAP REF-Q,LH CAP REF-Q,RLE CAP REF-Q";
            codelist += ",LLE CAP REF-Q,CMS GRAFT-Q,RF CAP REF-Q,LF CAP REF-Q";
            AddBuckets(buckets, "", codelist, "", "");
            AddDependentBuckets(buckets, "PULSE-Q", "BP-Q,BP2-Q,BP3-Q,ARTBP3-Q,ARTBP4-Q");
            AddDependentBuckets(buckets, "CHEST BOUNCE NICU-Q", "PULSE-Q");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "CardioMgt count=" + ct);
            AlternateBucketCounts(buckets, "CardioMgt");
            //if (_inds[18].is_checked) return;

            buckets = new List<int>();
            AddBuckets(buckets, "", "NEURO WDL-Q", "", "WDL,X");
            AddBuckets(buckets, "", "COMP LEVEL OF CONS-Q", "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "NeuroMgt count=" + ct);
            AlternateBucketCounts(buckets, "NeuruMgt");
            if (_inds[18].is_checked) return;

        }

        private void AlternateBucketCounts(List<int> bucket_list, string category)
        {
            List<int> buckets120;
            List<int> buckets180;
            List<int> buckets240;
            int ct;

            buckets120 = new List<int>();  //factor = 120/30 = 4
            buckets180 = new List<int>();  //factor = 180/30 = 6
            buckets240 = new List<int>();  //factor = 240/30 = 8
            foreach (var item in bucket_list)
            {
                buckets120.Add(item / 4);
                buckets180.Add(item / 6);
                buckets240.Add(item / 8);
            }
            ct = CountBuckets(buckets120);
            Program.VerboseAudit(category + " 4 hour count=" + ct);
            ct = CountBuckets(buckets180);
            Program.VerboseAudit(category + " 6 hour count=" + ct);
            ct = CountBuckets(buckets240);
            Program.VerboseAudit(category + " 8 hour count=" + ct);
        }

        private void CheckInsulin()
        {

            //[Order Display Name (column 3) = insulin regular human (HumuLIN R,NovoLIN R]  
            //   AND [Route=Intravenous]  
            //   AND  [Frequency=Continuous] **trigger for a 24 hour period
            // description, field, ord_tmg
            //[Order Display Name (column 3)  =  insulin]  
            //    AND   [Route = Subcutaneous]  
            //    AND   [4 or more administrations in a 12 hour period]   
            //** Any insulin - glargine (Lantus), lispro (HumaLOG), NPH (HumuLIN N,NovoLIN N), regular human (HumuLIN R,NovoLIN R)
            // description, field
            //"Times","CSN","Display Name","Dual-Sign Medication Order","Frequency","Ordered Route",
            // 0       1     2               3                             4                 5
            //"Irritant/Vesicant","Interpreter Needed?"
            //   6                     7

            //    //Program.DebugTrace("desc = {0}", desc);
            //    Program.DebugTrace("times = {0}", rec_items[0]);
            //    string res = rec_items[7].Trim();//RESULT=Interpreter
            //    string code = "MEDFILE";
            //    string cat = rec_items[3].Trim(); //CATEGORY
            //    string ord_tmg = rec_items[4].Trim(); // ORDER_TIMING
            //    string field = rec_items[5].Trim(); // FIELD_NAME
            //    string desc = rec_items[2].Trim(); // DESCRIPTION
            //    string ord_stat = "";
            //    if (rec_items[6].Trim().ToLower() == "true") ord_stat = "Y";

        }


        //=====================================================================
        // Group like assessments into buckets of time
        // Each bucket counts as one assessment in the frequency count

        private void SetBucketSize(int minutes)
        {
            _bucket_size = minutes;
        }

        //Add to the list of bucket numbers (redundant buckets are fine)
        private void AddBuckets(List<int> bucket_list, string cat, string code_list, string desc, string field)
        {
            AddBuckets(bucket_list, cat, code_list, desc, field, "", "");
        }
        private void AddBuckets(List<int> bucket_list, string cat, string code_list, string desc, string field, string result_list)
        {
            AddBuckets(bucket_list, cat, code_list, desc, field, result_list, "");
        }
        private void AddBuckets(List<int> bucket_list, string cat, string code_list, string desc, string field, string result_list, string andnotresult_list)
        {
            // Assign (virtual) bucket numbers to each assessment.  
            // Every x minutes after that is a new bucket.  Bucket zero = pull_start time.

            // get the chart items for the assessments
            var query = StartNewQuery();
            query = AndItemFilter(query, cat, code_list, desc, field, result_list);
            if (andnotresult_list.Trim() != "")
                query = query.Where(e => !andnotresult_list.Contains(e.RESULT));

            // This step is needed for those who want to count the # within a bucket, like Shands #24.
            // This will have no effect on those that count buckets.
            var query2 = (from item in query select new { item.EVENT_DATETIME }).Distinct();

            // figure out what buckets the events belong to
            var query3 = from item in query2
                         select new {
                            bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size)
                         };
            // Add to the list
            foreach (var item in query3) {
                bucket_list.Add(item.bucket);
            }

            // This is just for auditing: Say what we are looking for and how many were found
            if (!String.IsNullOrEmpty(result_list)) {
                // print each word and if it was found or not
                int i = CountResultContains(cat, code_list, desc, field, result_list);
            } else {
                // print how many were found
                Program.VerboseAudit(Describe(cat, code_list, desc, field, ""));
            }
        }
        private void AddBucketsNotContain(List<int> bucket_list, string cat, string code_list, string desc, string field, string result_list)
        {
            // Assign (virtual) bucket numbers to each assessment.  
            // Every x minutes after that is a new bucket.  Bucket zero = pull_start time.

            // get the chart items for the assessments
            var query = StartNewQuery();
            query = AndCodeInList(query, code_list);
            query = AndResultNotInList(query, result_list);

            // This step is needed for those who want to count the # within a bucket, like Shands #24.
            // This will have no effect on those that count buckets.
            var query2 = (from item in query select new { item.EVENT_DATETIME }).Distinct();

            // figure out what buckets the events belong to
            var query3 = from item in query2
                         select new
                         {
                             bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size)
                         };
            // Add to the list
            foreach (var item in query3)
            {
                bucket_list.Add(item.bucket);
            }

            // This is just for auditing: Say what we are looking for and how many were found
            if (!String.IsNullOrEmpty(result_list))
            {
                string found_what = "";
                // print each word and if it was found or not
                bool b = CountResultDoesNotContain(cat, code_list, desc, field, result_list, found_what);
            }
            else
            {
                // print how many were found
                Program.VerboseAudit(Describe(cat, code_list, desc, field, ""));
            }
        }
        private void AddDependentBuckets(List<int> bucket_list, string codelist1, string codelist2)
        {
            // get the chart items for the assessments
            var query1 = StartNewQuery();
            query1 = AndItemFilter(query1, "", codelist1, "", "", "");
            var query2 = StartNewQuery();
            query2 = AndItemFilter(query2, "", codelist2, "", "", "");

            // figure out what buckets the events belong to
            var query1a = from item in query1
                         select new
                         {
                             bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size)
                         };
            var query2a = from item in query2
                          select new
                          {
                              bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size)
                          };
            string s = "List for " + codelist1;
            foreach (var item in query1a)
            {
                s += item.bucket + ",";
            }
            Program.VerboseAudit(s);

            s = "List for " + codelist2;
            foreach (var item in query2a)
            {
                s += item.bucket + ",";
            }
            Program.VerboseAudit(s);
            // Add to the list IFF items in both lists occur in same bucket
            foreach (var item1 in query1a)
            {
                foreach (var item2 in query2a)
                {
                    if (item1.bucket == item2.bucket)
                        bucket_list.Add(item1.bucket);
                }
            }

        }

        private int CountBuckets(List<int> bucket_list)       // bucket list: (ha ha)
        {
            int result = bucket_list.Distinct().Count();
            if (result > 0) Program.VerboseAudit(result + " unique");
            return result;
        }


        private void Check_19()
        {
            int ct;
            string codelist;
            List<int> buckets;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("19. Vascular Access Site Mgt q1 Hour");
            Program.VerboseAudit("---------------");

            //codelist = "LDA SITE ASSESS-Q,SHEATH SITE ASSESS-Q,UVC/UAC SITE ASSESS-Q,EPIDURAL SITE ASSESS-Q";
            //SetIndIfResultContains(19, "", codelist, "", "", "", SearchDepth.SearchDefault);
            if (_pat.age < 5.0)
            {
                //make this require continuous
                var query = StartNewQuery(SearchDepth.Search24Hrs);
                query = AndItemFilter(query, "", "MEDFILE", "", "Intravenous", "");
                query = query.Where(e => e.ORDER_TIMING.ToLower() == "continuous");
                if (query.Count() > 0)
                    SetInd(19, "Found intravenous+continuous for child < 5yr");
            }

            SetBucketSize(30);

            buckets = new List<int>();
            codelist = "LDA SITE ASSESS-Q,SHEATH SITE ASSESS-Q,UVC/UAC SITE ASSESS-Q,EPIDURAL SITE ASSESS-Q,ECLS ART TEMP ASSESS,ECLS VEN TEMP ASSESS";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            if (IsQ1Hour(ct))
                SetInd(19,"Site assessment count=" + ct);

        }

        private void Check_20()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("20. Medication Activity >= 20 minutes");
            Program.VerboseAudit("---------------");
            
            SetIndIfResultDoesNotContain(20, "", "MBM BOTTLE NICU-Q", "", "", "mbm 20,DHM20");
            SetIndIfResultDoesNotContain(20, "", "MBM TUBE NICU-Q", "", "", "mbm 20,DHM20");
            SetIndIfResultContains(20, "", "DEXT GRAMS NICU-Q", "", "", "");
            SetIndIfResultContains(20, "", "BLOOD CONSENT-Q", "", "", "");
            SetIndIfResultContains(20, "", "CAR-T PRODUCT NAME", "", "", "");
            SetIndIfResultContains(20, "", "HSCT PROD UNIQUE ID", "", "", "");

            //change this so that dual signoff always looks 24 hours ago.
            // only dual signoff should trigger
            //
            SetIndIfResultContains(20, "1", "MEDFILE", "", "", "",SearchDepth.Search24Hrs);
            //need 8 of these within 90 mins
            //SetIndIfResultContains(20, "", "MEDFILE", "", "Per G Tube", "");
            //SetIndIfResultContains(20, "", "MEDFILE", "", "Per NG Tube", "");
            //SetIndIfResultContains(20, "", "MEDFILE", "", "Per J Tube", "");
            //SetIndIfResultContains(20, "", "MEDFILE", "", "Per Feeding Tube", "");
            //SetIndIfResultContains(20, "", "MEDFILE", "", "Per DH Tube", "");
            //SetIndIfResultContains(20, "", "MEDFILE", "", "Oral", "");
            if (!_inds[20].is_checked)
            {
                CheckMedsVolume();  // checks for 8 administrations of above within 90 mins
            }

        }

        private void CheckMedsVolume()
        {
            int ct = 0;

            var query = StartNewQuery(SearchDepth.Search24Hrs);
            query = AndCodeInList(query, "MEDFILE");
            query = AndFIELDNAMEInList(query, "Per G Tube,Per NG Tube,Per J Tube,Per Feeding Tube,Per DH Tube,Oral");
            var query2 = (from item in query select new { item.EVENT_DATETIME }).Distinct();
            query2 = query2.OrderBy(e => e.EVENT_DATETIME);
            foreach (var item in query2)
            {
                Program.VerboseAudit("Med Tube item time: " + item.EVENT_DATETIME.ToString());
                var q2 = StartNewQuery(SearchDepth.SearchDefault);
                q2 = AndCodeInList(q2, "MEDFILE");
                q2 = AndFIELDNAMEInList(q2, "Per G Tube,Per NG Tube,Per J Tube,Per Feeding Tube,Per DH Tube,Oral");
                q2 = q2.Where(e => e.EVENT_DATETIME >= item.EVENT_DATETIME && e.EVENT_DATETIME <= item.EVENT_DATETIME.AddMinutes(90));
                ct = Math.Max(ct, q2.Count());
            }
            if (ct >= 8) SetInd(20, "Found " + ct + " Med Administrations via Tube or Oral within 90 minutes.");
            
        }

        private void Check_21_22()
        {
            string codelist;

            Program.VerboseAudit("---------------"); //   check mrn for wounds: 96104875
            Program.VerboseAudit("21. Wound/Injury Mgmt");
            Program.VerboseAudit("22. Wound/Injury Mgmt >= 30 Minutes");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(22, "", "ARTERIAL DRSG INTERV-Q,DIALYSIS CATH-Q,DRSG INTER-Q,BURN DRESSING-Q,WOUND DRESSING-Q,WOUND VAC DRESSING-Q", "", "", "> 30");

            //SetIndIfResultContains(21, "", "DRSG INTER-Q", "", "", "dressing changed");
            SetIndIfResultContains(21, "", "ARTERIAL DRSG INTERV-Q,DIALYSIS CATH-Q,DRSG INTER-Q,BURN DRESSING-Q,WOUND DRESSING-Q,WOUND VAC DRESSING-Q", "", "", "< 30");

            SetIndIfResultContains(21, "", "STOOL APPEAR-Q", "", "", "Bloody");
            if (ResultContains("", "URINE APPEAR-Q", "", "", ""))
                SetIndIfResultContains(21, "", "CBI TOTAL OUTPUT-Q", "", "", "");

            codelist = "NPWT DRESSING TYPE-Q,PERI WOUND ASSESS-Q";
            codelist += ",PERI STOMA SITE ASSESS-Q,IABP DRESSING STATUS-Q";
            SetIndIfResultContains(21, "", codelist, "", "", "");
            SetIndIfResultDoesNotContain(21, "", "TUBE DRAIN DRESSING-Q", NOT_PREFIX + "nasogastric", "", "cast padding");
            //string res;
            //int value = 0;
            //if (GetResult("", "INFILTRATION SCALE-Q", "", "", out res))
            //{
            //    if (res.Left(1).IsNumeric())
            //    {
            //        value = (int)res.Val(); //Use Val; ToInteger will error on "60min"
            //        if (value > 0) SetInd(21, "INFILTRATION SCALE-Q value=" + res);
            //    }
            //}

            SetIndIfResultContains(21, "", "INFILTRATION SCALE-Q", "", "", "4");
            SetIndIfResultContains(21, "", "WOUND LDA-Q", "", "", "Burn,Wound");

            if (Exists("", "SKIN CARE-Q,PERI CARE-Q", "", "", ""))
            {
                //string reslist = "Abrasion,Blister,Bruising,Burn,Cracking,Electode mark,Excoriation,Fissure,Laceration";
                string reslist = "Blister,Cracking,Excoriation,Fissure";
                SetIndIfResultContains(21, "", "SKIN INTEGRITY-Q", "", "", reslist);
                SetIndIfResultContains(21, "", "R NEO/INFANT SKIN INTEGRITY", "", "", reslist);
            }

        }

        // Use this if you can total the education time
        //private void CheckEducation(int total)
        //{
        //    if (_inds[23].is_checked) return;             //skip if already checked

        //    if (total >= 60) {
        //        SetInd(23, "education >= 60 min");
        //    }
        //}

        private void Check_23()
            {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("23. Healthcare Mgmt Education >= 1 Hour");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(23, "", "DC EDUC-Q", "", "", "");
            }

        private void Check_24()
        {
            const int MINCOUNT = 9; //decr from 10 on Jan27 2020. Look for previous unit:if O.R. then mark this in ICU.
//Below are the ICU units that we would use for the pts that have the location 2 CPERIOP for the 2 hr recover time.
//CVICU
//NSICU
//SICU
//MICU
//WICU

            //room=2CPERIOP
            string s;
            int a2index, a3index;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("24. 1 to 1 Physiological Interv. >= 2 Hours");
            Program.VerboseAudit("---------------");

            if (_pat.came_from_or) SetInd(24, "Patient was in 2CPERIOP recently.");

            SetBucketSize(60);   // set 60-min buckets
            var buckets = new List<int>();    // list of bucket numbers (for each match)
            var buckets2 = new List<int>();    // list of bucket numbers (for each match)
            var buckets3 = new List<int>();    // list of bucket numbers (for each match)
            string codelist = "BP-Q,BP2-Q,BP3-Q,ARTBP3-Q,ARTBP4-Q";
            AddBuckets(buckets, "", codelist, "", "", "");
            // Instead of counting the number of buckets to get the frequency,
            // we want to see if any two hours in a row had 8 or more assessments in the buckets.
            // Make a query that returns each bucket number and its item count.
            var query = buckets.GroupBy(x => x)   // group by the hour (x is an int)
                               .Select(g => new { Hour = g.Key, Count = g.Count() })
                               .OrderBy(e => e.Hour);
            var arr = query.ToArray();

            string codelist2 = "PULSE-Q";
            AddBuckets(buckets2, "", codelist2, "", "", "");
            var query2 = buckets2.GroupBy(x => x)   // group by the hour (x is an int)
                               .Select(g => new { Hour = g.Key, Count = g.Count() })
                               .OrderBy(e => e.Hour);
            var arr2 = query2.ToArray();

            string codelist3 = "RESP-Q";
            AddBuckets(buckets3, "", codelist3, "", "", "");
            var query3 = buckets3.GroupBy(x => x)   // group by the hour (x is an int)
                               .Select(g => new { Hour = g.Key, Count = g.Count() })
                               .OrderBy(e => e.Hour);
            var arr3 = query3.ToArray();

            s = "";
            foreach (var e in arr)
            {
                s += ",(" + e.Hour + "," + e.Count + ")";
            }
            if (s.Left(1) == ",") s = s.Substring(1);
            Program.VerboseAudit("Hourly counts for "+codelist+" = "+ s);

            s = "";
            foreach (var e in arr2)
            {
                s += ",(" + e.Hour + "," + e.Count + ")";
            }
            if (s.Left(1) == ",") s = s.Substring(1);
            Program.VerboseAudit("Hourly counts for " + codelist2 + " = " + s);

            s = "";
            foreach (var e in arr3)
            {
                s += ",(" + e.Hour + "," + e.Count + ")";
            }
            if (s.Left(1) == ",") s = s.Substring(1);
            Program.VerboseAudit("Hourly counts for " + codelist3 + " = " + s);

            for (int i = 0; (i < arr.GetUpperBound(0)); i++)
            {
                // Are these two hours in a row? (beware missing hours)
                Program.VerboseAudit("i=" +i);
            if (i+1 <=arr.GetUpperBound(0))
                if (arr[i].Hour == arr[i + 1].Hour - 1)
                {
                    Program.VerboseAudit("Hour=" + arr[i].Hour + " arr[" + i + "] has an adj hour.");
                    // Add these two consecutive hours
                    Program.VerboseAudit("arr[" + i + "].count=" + arr[i].Count + " arr[" + (i+1).ToString() + "].count="+arr[i+1].Count);
                    if (arr[i].Count + arr[i + 1].Count >= MINCOUNT)
                    {
                        a2index = Array.FindIndex(arr2, e => e.Hour == arr[i].Hour);
                        Program.VerboseAudit("a2index=" + a2index + " arr2.GetUpperBound(0)=" + arr2.GetUpperBound(0));
                        if (a2index >= 0)
                        if (a2index < arr2.GetUpperBound(0))
                        {
                            Program.VerboseAudit("arr2[" + a2index + "].count=" + arr2[a2index].Count + " arr2[" + (a2index + 1).ToString() + "].count=" + arr2[a2index + 1].Count);
                            int j = a2index;
                            if (j+1<=arr2.GetUpperBound(0))
                            if ((arr[i].Hour == arr2[j].Hour) && (arr2[j].Hour == arr2[j + 1].Hour - 1) && (arr2[j].Count + arr2[j + 1].Count >= MINCOUNT))
                            {
                                a3index = Array.FindIndex(arr3, e => e.Hour == arr[i].Hour);
                                Program.VerboseAudit("a3index=" + a3index + " arr3.GetUpperBound(0)=" + arr3.GetUpperBound(0));
                                if (a3index >=0)
                                if (a3index < arr3.GetUpperBound(0))
                                {
                                    Program.VerboseAudit("arr3[" + a3index + "].count=" + arr3[a3index].Count + " arr3[" + (a3index + 1).ToString() + "].count=" + arr3[a3index + 1].Count);
                                    int k = a3index;
                                    if (k + 1 <= arr3.GetUpperBound(0))
                                    if ((arr[i].Hour == arr3[k].Hour) && (arr3[k].Hour == arr3[k + 1].Hour - 1) && (arr3[k].Count + arr3[k + 1].Count >= MINCOUNT))
                                    {
                                        SetInd(24, "Found " + MINCOUNT.ToString() + " or more in the same 2 hour period for each group: (" + codelist + ") (" + codelist2 + ") (" + codelist3 + ")");
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }


        private void AtLeastOneADL()
        {
            Program.Audit("---------------");
            if (!(_inds[1].is_checked || _inds[2].is_checked || _inds[3].is_checked || _inds[4].is_checked))
            {
                if (IsICU()) 
                    SetInd(4, "ICU unit: Defaulting to ADL Complete");
                else
                    // Make "#2 ADL - Assist" the default.  (90% of patients)
                    SetInd(1, "Defaulting to self care due to lack of documentation.");
            }
            //if (!(_inds[101].is_checked || _inds[102].is_checked || _inds[103].is_checked || _inds[104].is_checked)) 
            //{
            //    SetInd(101, "Defaulting to self care due to lack of documentation.");
            //}
        }

        private void HighestIndicatorInEachGroupWins()
        {
            int i, g;
            bool highest_is_on;
            string ind_list;

            Program.Audit("Select highest indicator in each group");
            
            g = 0;
            highest_is_on = false;
            // Go from bottom up - highest (lowest) indicator in each group wins
            for (i = MAX_INDS; (i >= 1); i--) {
                if (_inds[i].radio_group > 0) {
                    if (_inds[i].radio_group != g) {
                        //this is a new group
                        g = _inds[i].radio_group;
                        highest_is_on = _inds[i].is_checked;
                    } else {
                        //same group
                        if (highest_is_on) {
                            _inds[i].is_checked = false;             //uncheck a lower number
                        } else {
                            highest_is_on = _inds[i].is_checked;     //save this one
                        }
                    }
                }
            } // next i
            
            //Echo the indicators for an Audit (no classification will be saved)
            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++) {
                if (_inds[i].is_checked) ind_list += "," + i;
            } // next i

            Program.Audit("Final list = " + ind_list.Substring(1));
            Program.Audit("---------------");
        }


        private void CheckProcs()
        {
            CheckProc_1();
            CheckProc_2();
            CheckProc_3();
            CheckProc_4();
            CheckProc_5();
            CheckProc_6();
            CheckProc_7();
            CheckProc_8();
            CheckProc_9();
            CheckProc_10();
            CheckProc_11();

        }


        private void DoProc(int pnum, string code)
        {
            // look for all the STARTs in the 24-hr range g_start-12hrs until g_finish
            // and pair them with their STOPs.
            // if no STOP is found for a START, then dont save the proc--will catch it next pull
            var query = StartNewQuery(SearchDepth.Search24Hrs);
            query = AndItemFilter(query, "", code, "", "", "START");
            query = query.Where(e => e.EVENT_DATETIME >= _pat.pull_finish.AddHours(-24));
            query = query.Where(e => e.ORDER_STATUS.ToUpper() != "X");
            query = query.OrderBy(e => e.EVENT_DATETIME);
            foreach (var item in query)
            {
                var q = StartNewQuery(SearchDepth.Search24Hrs);
                q = AndItemFilter(q, "", code, "", "", "STOP");
                q = q.Where(s => s.EVENT_DATETIME >= item.EVENT_DATETIME);
                q = q.OrderBy(s => s.EVENT_DATETIME);
                bool first = true;
                foreach (var sitem in q)
                {
                    if (first)
                    {
                        first = false;
                        if (!ProcExistsInDB(pnum, item.EVENT_DATETIME, sitem.EVENT_DATETIME))
                        {
                            var proc = new proc_data();
                            proc.procedure_number = pnum;
                            proc.start = item.EVENT_DATETIME;
                            proc.finish = sitem.EVENT_DATETIME;
                            _procs.Add(proc);
                            Program.Audit("Procedure " + pnum + ": Found " + code + " between " + item.EVENT_DATETIME.ToString() + " and " + sitem.EVENT_DATETIME.ToString());
                            ChangeOrderStatusOfActivity(code, item.EVENT_DATETIME);

                        }
                    }
                }
            }

        }

        private void ChangeOrderStatusOfActivity(string code, DateTime evdt)
        {
            if (code.Left(2) == EXACT_MATCH_PREFIX) code = code.Substring(2);
            string sql = "update chart_item set order_status='X' where encounter_id=" + _pat.encounter_id.ToString();
            sql += " and code='" + code + "' and result='start' and event_datetime<=" + PFSUtility.SQLDateTime(evdt);
            using (var cn = PFSUtility.NewSqlConnection())
            {
                var cmd = new SqlCommand(sql, cn);
                cmd.ExecuteNonQuery();
            }
            
        }

        private bool ProcExistsInDB(int pnum, DateTime startdt, DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && (proc.PROCEDURE_DATETIME == startdt)
                            && (proc.DEPARTURE_DATETIME == enddt)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new { proc.PROCEDURE_EVENT_ID };
            return (query.Count() > 0);
        }

        private bool QueuedProcOverlaps(int pnum, DateTime startdt, DateTime enddt)
        {
            bool overlap = false;

            proc_data[] pary = _procs.ToArray();
            for (int i = 0; i <= pary.GetUpperBound(0); i++)
            {
                if (pary[i].procedure_number == pnum)
                {
                    //overlap if   p.start between stardt and enddt
                    //             p.finish between startdt and enddt
                    //             startdt >= p.start and enddt <= p.finish
                    if ((pary[i].start >= startdt) && (pary[i].start < enddt) ||
                        (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
                        (startdt >= pary[i].start) && (enddt <= pary[i].finish))
                    {
                        overlap = true;
                    }

                    if ((pary[i].start >= startdt) && (pary[i].start < enddt) && (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
                        (pary[i].start < startdt) && (pary[i].finish > startdt) && (pary[i].finish < enddt)
                        )
                    {
                        // the proc in the list is completely contained in startdt/endt pair
                        // change this proc to be the startdt/enddt
                        // overlap is still true, but the list item will be updated with the encompassing times
                        //Program.Audit("Procedure " + pnum + ": " + startdt + " - " + enddt + " will trump " + pary[i].start + " - " + pary[i].finish);
                        pary[i].start = startdt;
                        pary[i].finish = enddt;
                    }
                }
            }
            _procs = pary.ToList();
            return overlap;
        }
        
        //SAFETY RN-Q,SAFETY NON-RN-Q,OFF UNIT RN-Q,
        //    OFF UNIT NON-RN-Q,EDUC RN-Q,EX WOUND RN-Q,
        //    EX WOUND NON-RN-Q,COOR CARE RN-Q,PROC RN-Q,
        //    PROC NON-RN-Q,2:1 PROC RN-Q

        private void CheckProc_1()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P1. 1-1 safety observation by RN");
            Program.VerboseAudit("---------------");
            DoProc(1, "SAFETY RN-Q");
        }

        private void CheckProc_2()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P2. 1-1 safety observation by non-RN");
            Program.VerboseAudit("---------------");
            DoProc(2, "SAFETY NON-RN-Q");
        }
        

        //private void CheckProc_2()
        //{
        //    string nowstr;
        //    string toddtstr;
        //    string yesdtstr;
        //    string timea ="";
        //    DateTime timea_startdt, timea_enddt;
        //    DateTime timeb_startdt, timeb_enddt;
        //    DateTime nowdt = _pat.pull_finish;              // "now" is pull time
        //    DateTime yesdt;
            
        //    Program.VerboseAudit("---------------");
        //    Program.VerboseAudit("P2. 1-1 safety observation by non-RN");
        //    Program.VerboseAudit("---------------");

        //    nowstr = nowdt.ToString("yyyyMMddHHmm");
        //    yesdt = nowdt.AddDays(-1);
        //    toddtstr = nowdt.ToString("yyyyMMdd");
        //    yesdtstr = yesdt.ToString("yyyyMMdd");
            
        //    //when is now? (yesterday/today)
        //    //yes 7am -- yes 7p  -- tod 7a -- tod 7p
        //    //                                   A                   B
        //    //if nowdt >= tod7pm then check  tod 7am-tod 7pm and tod 7pm-tom 7a
        //    //if nowdt >= tod7am then check  yes 7p - tod 7a     tod 7a-tod 7p
        //    //if nowdt >= yes7pm then check  yes 7a-yes 7p       yes 7p-tod 7a
        //    if (nowstr.CompareTo(toddtstr + "1900") >= 0) {
        //        timea = toddtstr + "0700";
        //    } else if (nowstr.CompareTo(toddtstr + "0700") >= 0) {
        //        timea = yesdtstr + "1900";
        //    } else if (nowstr.CompareTo(yesdtstr + "1900") >= 0) {
        //        timea = yesdtstr + "0700";
        //    }
            
        //    timea_startdt = PFSUtility.ISOToDateTime(timea);
        //    timea_enddt = timea_startdt.AddHours(12);
        //    MaybeAddSitter(timea_startdt, timea_enddt);

        //    timeb_startdt = timea_enddt;
        //    timeb_enddt = timeb_startdt.AddHours(12);
        //    MaybeAddSitter(timeb_startdt, timeb_enddt);
        //}

        //private void MaybeAddSitter(DateTime startdt, DateTime enddt)
        //{
        //    var query = StartNewQuery(SearchDepth.SearchSinceAdmission);    // add custom time range below
        //    query = query.Where(e => (e.EVENT_DATETIME >= startdt) && (e.EVENT_DATETIME < enddt));
        //    query = AndCodeInList(query, "Sitter");
        //    query = AndResultInList(query,"continued, initiated");
        //    query = AndResultNotInList(query, "discontinued");

        //    if (query.Count() > 0) {
        //        if (ProcExists(2, startdt, enddt)) {
        //            Program.Audit("Procedure 2: already exists");
        //        } else {
        //            var proc = new proc_data();
        //            proc.procedure_number = 2;
        //            proc.start = startdt;
        //            proc.finish = enddt;
        //            _procs.Add(proc);
        //            Program.Audit("Procedure 2: Found Sitter between " + startdt + " and " + enddt);
        //        }
        //    }
            
        //}

        private bool ProcExists(int pnum, DateTime startdt, DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && (proc.PROCEDURE_DATETIME == startdt)
                            && (proc.DEPARTURE_DATETIME == enddt)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new {proc.PROCEDURE_EVENT_ID};
            return (query.Count() > 0);
        }


        private void CheckProc_3()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P3. Off unit accompanied by RN");
            Program.VerboseAudit("---------------");
            DoProc(3, "OFF UNIT RN-Q");
        }

        private void CheckProc_4()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P4. Off unit accompanied by non-RN");
            Program.VerboseAudit("---------------");
            DoProc(4, "OFF UNIT NON-RN-Q");

        }

        private void CheckProc_5()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P5. Patient/family education by RN");
            Program.VerboseAudit("---------------");
            DoProc(5, "EDUC RN-Q");

        }

        private void CheckProc_6()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P6. Extensive wound management by RN");
            Program.VerboseAudit("---------------");
            DoProc(6, "EX WOUND RN-Q");

        }

        private void CheckProc_7()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P7. Extensive wound management by non-RN");
            //Program.VerboseAudit("---------------");
            DoProc(7, "EX WOUND NON-RN-Q");

        }

        private void CheckProc_8()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P8. Coordination of care by RN");
            Program.VerboseAudit("---------------");
            DoProc(8, "COOR CARE RN-Q");

        }

        private void CheckProc_9()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P9 1:1 RN at bedside");
            Program.VerboseAudit("---------------");
            DoProc(9, EXACT_MATCH_PREFIX+"PROC RN-Q");  //need to be exact due to 2:1 

        }

        private void CheckProc_10()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P10. 1:1 non-RN at bedside");
            Program.VerboseAudit("---------------");
            DoProc(10, "PROC NON-RN-Q");

        }

        private void CheckProc_11()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P11. 2:1 by RN at bedside");
            Program.VerboseAudit("---------------");
            DoProc(11, "2:1 PROC RN-Q");
        }

        private void CheckOutcomes()
        {
            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = AndItemFilter(query, "", "A_MHPostFalltime", "", "", "");
            foreach (var ch in query)
            {
                var outc = new proc_data();
                outc.procedure_number = 1;
                outc.start = ch.EVENT_DATETIME;
                _outcomes.Add(outc);
                Program.Audit("Outcomes 1: Found A_MHPostFalltime at " + outc.start);
            }
        }
        

        private const string DATETIME_FORMAT = "yyyyMMddHHmm";              // ISO Date/Time w/o seconds

        private void OutputClass()
        {
            string outstr, ind_list, desc, str_pull_dt;
            int i, tc_event_id;

            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         212 242 304
            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1
            //1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901 3456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
            //1       |10 5            |                |                |        |10023121779         |ALEXANDER                       |GLORIA                          |                                |0508    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289530  |           |201403030400                                                                      |NNYNNNNYYNNYNNNYNNNYYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//1       |10 5            |                |                |        |10024544908         |ARMSTRONG                       |AARON                           |                                |0535    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289531  |           |201403030400                                                                      |NYNNYNNNNNNYNNNYNNNNYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//|0547    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289532  |           |201403030400                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//|0547    |A   |201403030700|                |201403030700    |18  |C|    |3040|1440|18289532  |           |201403030700                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN

            if (Program.g_is_test)
                tc_event_id = 9999;
            else
                tc_event_id = PFSUtility.NextGID();                         //get a unique id for this class
            str_pull_dt = _pat.pull_finish.ToString(DATETIME_FORMAT);

            outstr = _pat.facilty_code.FixedWidth(8);                       //(facility code)
            outstr += "|" + _pat.unit_name.FixedWidth(16);
            outstr += "|" + "".FixedWidth(16);                               //(unit code)
            outstr += "|" + "".FixedWidth(16);                               //(area code)
            outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
            outstr += "|" + _pat.acct.FixedWidth(20);
            outstr += "|" + _pat.last_name.FixedWidth(32);
            outstr += "|" + _pat.first_name.FixedWidth(32);
            outstr += "|" + _pat.middle_name.FixedWidth(32);
            outstr += "|" + _pat.room.FixedWidth(8);
            outstr += "|" + _pat.bed.FixedWidth(4);
            outstr += "|" + _pat.effective.ToString(DATETIME_FORMAT);// str_pull_dt.FixedWidth(12);                      //class datetime (could change)
            outstr += "|" + "".FixedWidth(16);                               //(login)
          outstr += "|" + str_pull_dt.FixedWidth(16);                      //(employee)/(pull datetime)
            outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
            outstr += "|" + "C".FixedWidth(1);                               //record type = class
            outstr += "|" + "".FixedWidth(4);                                //(stage)
            outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
            outstr += "|" + _pat.range.ToString().FixedWidth(4);             //TC pull range
            outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
            outstr += "|";
            outstr = outstr.FixedWidth(294);
          outstr += "|" + _pat.effective.ToString(DATETIME_FORMAT);        //IN
            outstr = outstr.FixedWidth(377);
            outstr += "|";
            
            ind_list = "";

            if (give_default_inds)
            {
                Program.VerboseAudit("Patient will receive default indicators " + _pat.default_inds_str);
                for (i = 1; (i <= MAX_INDS); i++)
                {
                    _inds[i].is_checked = false;
                }
                foreach (var ind in _pat.default_inds)
                {
                    if (ind <= _inds.GetUpperBound(0))
                    {
                        _inds[ind].is_checked = true;
                    }
                }
            }

            for (i = 1; (i <= MAX_INDS); i++) {
                if (_inds[i].is_checked) {
                    outstr += "Y";
                    ind_list += "," + i;
                } else {
                    outstr += "N";
                }
            } // next i
            ind_list = ind_list.Substring(1);                           //strip leading comma

 
            Program.outfile.WriteLine(outstr);                          //output to transparent.txt

            if (Program.g_make0700)
            {  //212 242 304
                char[] buffer = outstr.ToCharArray();
                buffer[212 - 1 + 0] = '0';
                buffer[212 - 1 + 1] = '7';
                buffer[212 - 1 + 2] = '0';
                buffer[212 - 1 + 3] = '0';

                buffer[242 - 1 + 0] = '0';
                buffer[242 - 1 + 1] = '7';
                buffer[242 - 1 + 2] = '0';
                buffer[242 - 1 + 3] = '0';

                buffer[304 - 1 + 0] = '0';
                buffer[304 - 1 + 1] = '7';
                buffer[304 - 1 + 2] = '0';
                buffer[304 - 1 + 3] = '0';
                string outstr0700 = new string(buffer);
                Program.outfile.WriteLine(outstr0700);                          //output to transparent0700.txt
            }


            Program.Audit("");
            desc = "Classified: " + ind_list;
            if (Program.g_is_test) {
                Program.Audit(desc);
            } else {
                //Save the selected indicators plus both Program.Audits in the event log; link with tc_event_id
                PFSEventLog.AddTransparentMappingEventLogEntry(
                    desc, Program.gLogUnitID, Program.gLogEncounterID,
                    tc_event_id, Program.gLogMapperVersion,
                    Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
            }
        }

        private void OutputProcs()
        {
            int i;
            string outstr, proc_list, desc;
            int tc_event_id;
            proc_data p;

            foreach(var proc in _procs) {
                p = proc;
                AdjustProcTimesIfNeeded(ref p);
                if (Program.g_is_test)
                    tc_event_id = 9999;
                else
                    tc_event_id = PFSUtility.NextGID();                         //get a unique id for this proc
                
                outstr = _pat.facilty_code.FixedWidth(8);
                outstr += "|" + _pat.unit_name;                                 //10
                outstr = outstr.FixedWidth(68);
                outstr += "|" + _pat.acct.FixedWidth(20);                       //90
                outstr += "|" + _pat.last_name.FixedWidth(32);
                outstr += "|" + _pat.first_name.FixedWidth(32);
                outstr += "|" + _pat.middle_name.FixedWidth(32);
                outstr = outstr.FixedWidth(202);
                outstr += "|" + p.start.ToString(DATETIME_FORMAT);           //204 proc dt
                outstr = outstr.FixedWidth(254);
                outstr += "|P";                                                 //256 procedure type record
                outstr += "|" + "".FixedWidth(4);                               //(stage)
                outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);     //TC source ID
                outstr += "|" + _pat.range.ToString().FixedWidth(4);            //TC pull range
                outstr += "|" + tc_event_id.ToString().FixedWidth(10);          //TC event ID
                outstr += "|";
                outstr = outstr.FixedWidth(294);
                outstr += "|" + p.start.ToString(DATETIME_FORMAT);           //296 procdt in
                outstr = outstr.FixedWidth(346);
                outstr += "|" + p.finish.ToString(DATETIME_FORMAT);          //348 procdt out
                outstr = outstr.FixedWidth(377);
                outstr += "|";
                
                proc_list = "";
                for (i = 1; (i < MAX_PROCS); i++) {
                    if (proc.procedure_number == i) {
                        outstr += "Y";
                        proc_list += "," + i;
                    } else {
                        outstr += "N";
                    }
                } // next i
                proc_list = proc_list.Substring(1);                             //strip leading comma

                Program.outfile.WriteLine(outstr);                              //output to transparent.txt

                desc = "Procedures: " + proc_list;
                if (Program.g_is_test) {
                    Program.Audit(desc);
                } else {
                    //Save the selected procedures plus both Program.Audits in the event log; link with tc_event_id
                    //(this assumes the procedure Program.Audit is mixed in with the class Program.Audit)
                    PFSEventLog.AddTransparentMappingEventLogEntry(desc, Program.gLogUnitID, Program.gLogEncounterID,
                        tc_event_id, Program.gLogMapperVersion,
                        Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
                }
            } // next proc
        }

        private void AdjustProcTimesIfNeeded(ref proc_data proc)
        {
            DateTime s,f;
            int diffmins = 0;

            s = proc.start;
            f = proc.finish;

            var db = PFSUtility.NewPfsDataContext();
            var query = from el in db.ENCOUNTER_LOCATIONs
                        where (el.ENCOUNTER_ID == _pat.encounter_id)
                        select el;
            query = query.OrderByDescending(e => e.EFFECTIVE_DATETIME_IN);
            bool found_min_loc = false;
            foreach (var encloc in query)
            {
                if (!found_min_loc && (encloc.UNIT_ID > 0))
                {
                    if (encloc.EFFECTIVE_DATETIME_IN <= s.AddHours(1))
                    {
                        found_min_loc = true;
                        if (s < encloc.EFFECTIVE_DATETIME_IN)
                        {
                            diffmins = (int)(PFSUtility.DateDiffInMinutes(s, encloc.EFFECTIVE_DATETIME_IN));
                            proc.start = proc.start.AddMinutes(diffmins);
                            proc.finish = proc.finish.AddMinutes(diffmins);
                            Program.VerboseAudit("[A1] Activity was adjusted to fit within nearest location. Minutes shifted=" + diffmins);
                        }
                    }
                }
            }
            if (!found_min_loc) // means that proc start is before any location. choose the earliest.
            {
                foreach (var encloc in query)
                {
                    //if (encloc.UNIT_ID == _pat.unit_id)
                    //{
                        diffmins = (int)(PFSUtility.DateDiffInMinutes(proc.start, encloc.EFFECTIVE_DATETIME_IN));
                        s = proc.start.AddMinutes(diffmins);
                        f = proc.finish.AddMinutes(diffmins);
                    //}
                }
                proc.start = s;
                proc.finish = f;
                Program.VerboseAudit("[A2] Activity was adjusted to fit within location. Minutes shifted=" + diffmins);
            }

        }

        private void OutputOutcomes()
        {
            string outstr;

            foreach (var oc in _outcomes)
            {
                outstr = "".FixedWidth(9) + _pat.unit_name.FixedWidth(16);       // unitname
                outstr = outstr.FixedWidth(60);
                outstr += oc.start.ToString(DATETIME_FORMAT);                 //event_datetime
                outstr = outstr.FixedWidth(73);
                outstr += _pat.acct.FixedWidth(20);                            //acct
                outstr = outstr.FixedWidth(94);
                outstr += oc.procedure_number;                                //outcome indicator
                Program.out2file.WriteLine(outstr);                          //output to outcomesindicator.txt
            }

        }

        private bool DefaultInPast4hrs()
        {
            bool def = false;

            var db = PFSUtility.NewPfsDataContext();

            var query = from ce in db.CLASSIFICATION_EVENTs
                        where (ce.ENCOUNTER_ID == _pat.encounter_id)
                        where (ce.EFFECTIVE_DATETIME_IN >= _pat.pull_finish.AddMinutes(-(4 * 60)))
                        orderby (ce.EFFECTIVE_DATETIME_IN)
                        select ce;

            int ct = query.Count();
            if (ct == 0) return def;
            bool first = true;
            foreach (var byid in query)
            {
                if (first)
                {
                    first = false;
                    def = (byid.CLASSIFIED_BY_ID == -2); // this is a default
                    Program.VerboseAudit("ceid=" + byid.CLASSIFICATION_EVENT_ID + "  byid=" + byid.CLASSIFIED_BY_ID + "  exists=" + def.ToString());
                }
            }
            return def;
        }
        private int DeterminePtypeOfIndicators()
        {
            int i;
            var pscore = 0.0;
            int pt_type = 6;
            var indlist = new List<int>();
            string indstr = "";

            Program.VerboseAudit("Determine patient type of triggered indicators");

            for (i = 1; (i <= MAX_INDS); i++)
            {
                if (_inds[i].is_checked)
                {
                    indlist.Add(i);
                    indstr += i.ToString() + ",";
                }
            }

            var db = PFSUtility.NewPfsDataContext();
            var query_ind_def = from ind_def in db.INDICATOR_DEFINITIONs
                                where (ind_def.METHODOLOGY_ID == 18) &&
                                  indlist.Contains(ind_def.INDICATOR_NUMBER)
                                select new
                                {
                                    ind_def.WEIGHT
                                };
            foreach (var wgts in query_ind_def)
            {
                pscore += wgts.WEIGHT;
            }
            Program.VerboseAudit("indicators=" + indstr);
            Program.VerboseAudit("score=" + pscore.ToString());

            var query_ptype = from ptype in db.PATIENT_TYPEs
                              where (ptype.METHODOLOGY_ID == 18)
                              orderby ptype.PATIENT_TYPE1
                              select new
                              {
                                  ptype.PATIENT_TYPE1,
                                  ptype.POINTS_HIVAL
                              };

            foreach (var ptypes in query_ptype)
            {
                //                Program.DebugTrace("type,hival=" + ptypes.PATIENT_TYPE1.ToString() + "," + ptypes.POINTS_HIVAL.ToString(), "");
                if (pscore <= ptypes.POINTS_HIVAL)
                {
                    if (pt_type > ptypes.PATIENT_TYPE1)
                    {
                        pt_type = ptypes.PATIENT_TYPE1;
                    }
                }
            }
            Program.VerboseAudit("patient type=" + pt_type.ToString());

            return pt_type;
        }

    }
}




//#include <stdio.h>
//#include <math.h>

//float calculateSD(float data[]);

//int main()
//{
//    int i;
//    float data[10];

//    printf("Enter 10 elements: ");
//    for(i=0; i < 10; ++i)
//        scanf("%f", &data[i]);

//    printf("\nStandard Deviation = %.6f", calculateSD(data));

//    return 0;
//}

//float calculateSD(float data[])
//{
//    float sum = 0.0, mean, standardDeviation = 0.0;

//    int i;

//    for(i=0; i<10; ++i)
//    {
//        sum += data[i];
//    }

//    mean = sum/10;

//    for(i=0; i<10; ++i)
//        standardDeviation += pow(data[i] - mean, 2);

//    return sqrt(standardDeviation/10);
//}