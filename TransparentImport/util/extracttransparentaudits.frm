VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmExtractTransparentAudits 
   Caption         =   "Transparent Audit Extractor"
   ClientHeight    =   4590
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   9555
   LinkTopic       =   "Form1"
   ScaleHeight     =   4590
   ScaleWidth      =   9555
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdStop 
      Caption         =   "STOP"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   2040
      TabIndex        =   16
      Top             =   3960
      Width           =   1815
   End
   Begin VB.Frame Frame1 
      Caption         =   "Select Audit"
      Height          =   795
      Left            =   120
      TabIndex        =   12
      Top             =   2100
      Width           =   9255
      Begin VB.PictureBox Picture1 
         BorderStyle     =   0  'None
         Height          =   555
         Left            =   120
         ScaleHeight     =   555
         ScaleWidth      =   9015
         TabIndex        =   13
         Top             =   180
         Width           =   9015
         Begin VB.OptionButton optVerbose 
            Caption         =   "Verbose"
            Height          =   315
            Left            =   1440
            TabIndex        =   15
            Top             =   120
            Width           =   1215
         End
         Begin VB.OptionButton optBrief 
            Caption         =   "Brief"
            Height          =   315
            Left            =   120
            TabIndex        =   14
            Top             =   120
            Width           =   1215
         End
      End
   End
   Begin VB.CommandButton cmdSQL1 
      Caption         =   "SQL Query1"
      Height          =   495
      Left            =   6000
      TabIndex        =   11
      Top             =   3960
      Width           =   1575
   End
   Begin VB.CommandButton cmdSQL2 
      Caption         =   "SQL Query2"
      Height          =   495
      Left            =   7680
      TabIndex        =   10
      Top             =   3960
      Width           =   1575
   End
   Begin VB.CommandButton cmdGo 
      Caption         =   "GO"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   120
      TabIndex        =   8
      Top             =   3960
      Width           =   1815
   End
   Begin MSComctlLib.ProgressBar ProgressBar1 
      Height          =   315
      Left            =   120
      TabIndex        =   7
      Top             =   3540
      Width           =   9255
      _ExtentX        =   16325
      _ExtentY        =   556
      _Version        =   393216
      Appearance      =   1
   End
   Begin VB.TextBox txtTmpTablename 
      Height          =   375
      Left            =   2880
      TabIndex        =   6
      Top             =   1680
      Width           =   1695
   End
   Begin MSComCtl2.DTPicker dtpTo 
      Height          =   375
      Left            =   5040
      TabIndex        =   2
      Top             =   1260
      Width           =   1575
      _ExtentX        =   2778
      _ExtentY        =   661
      _Version        =   393216
      Format          =   23134209
      CurrentDate     =   41442
   End
   Begin MSComCtl2.DTPicker dtpFrom 
      Height          =   375
      Left            =   2880
      TabIndex        =   1
      Top             =   1260
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   661
      _Version        =   393216
      Format          =   23134209
      CurrentDate     =   41442
   End
   Begin VB.Label lblSummary 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Caption         =   "summary"
      ForeColor       =   &H80000008&
      Height          =   375
      Left            =   120
      TabIndex        =   9
      Top             =   3060
      Width           =   9255
   End
   Begin VB.Label Label3 
      Caption         =   "Temporary table name"
      Height          =   315
      Left            =   120
      TabIndex        =   5
      Top             =   1740
      Width           =   2655
   End
   Begin VB.Label Label2 
      Caption         =   "to"
      Height          =   375
      Left            =   4680
      TabIndex        =   4
      Top             =   1320
      Width           =   255
   End
   Begin VB.Label Label1 
      Caption         =   "Transparent Mapping Events from"
      Height          =   315
      Left            =   120
      TabIndex        =   3
      Top             =   1320
      Width           =   2655
   End
   Begin VB.Label lblDesc 
      Appearance      =   0  'Flat
      BorderStyle     =   1  'Fixed Single
      Caption         =   "desc"
      ForeColor       =   &H80000008&
      Height          =   1035
      Left            =   120
      TabIndex        =   0
      Top             =   60
      Width           =   9255
      WordWrap        =   -1  'True
   End
End
Attribute VB_Name = "frmExtractTransparentAudits"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_stop_request As Boolean

Private Sub cmdSQL1_Click()
    Dim sql As String, tmp As String
    tmp = "/* Analyze one indicator */"
    sql = "select count(*),txt from " & txtTmpTablename & " where txt like 'Set Ind #1:%' group by txt  having count(*)>1 order by COUNT(*) desc"
    tmp = InputBox("Copy this text to " & tmp, , sql)
End Sub

Private Sub cmdSQL2_Click()
    Dim sql As String, tmp As String
    tmp = "/* Browse all indicators */"
    sql = "select count(*),txt from " & txtTmpTablename & " where txt like 'Set Ind%' group by txt  having count(*)>1 order by txt, COUNT(*) desc"
    tmp = InputBox("Copy this text to " & tmp, , sql)
End Sub

Private Sub Form_Load()
    Dim sql As String
    Dim rs As New Recordset
    
    lblDesc.Caption = _
        "This utility will look for transparent classification audits in the event log" & _
        " and explode them line by line into a temporary table.  This table can then be searched and" & _
        " grouped to look for keywords by volume and maybe help tune the mapping process by looking" & _
        " for more common keywords first." & vbCrLf & _
        vbCrLf & _
        "Note: Existing entries in the same date range will be overwritten."

    lblSummary.Caption = ""
    
    txtTmpTablename.Text = "zAudit"
    
    optBrief.value = True

    sql = "select min(timestamp) from event_log where brief_audit is not null"
    rs.Open sql, g_cnADO
    dtpFrom.value = rs(0)
    rs.Close
    
    sql = "select max(timestamp) from event_log where brief_audit is not null"
    rs.Open sql, g_cnADO
    dtpTo.value = rs(0)
    rs.Close

    If Not IsDate(dtpFrom.value) Then
        MsgBox "There are no event log items with a brief audit", vbCritical
    End If

End Sub

Private Sub cmdGo_Click()
    Dim sql As String, where As String, audit_column As String
    Dim rs As New Recordset
    Dim count As Long, i As Long
    
    cmdGo.Enabled = False
    cmdStop.Enabled = True
    
    CreateTempTable
    
    where = _
        " where timestamp between " & g_dbutil.SQL_Date(dtpFrom.value) & _
        "   and " & g_dbutil.SQL_Date(dtpTo.value + 1)

    sql = "delete from " & txtTmpTablename & where
    Debug.Print sql
    g_cnADO.Execute sql
    
    where = where & " and brief_audit is not null"
    
    sql = "select count(*) from event_log" & where
    Debug.Print sql
    rs.Open sql, g_cnADO
    count = rs(0)
    rs.Close
    
    lblSummary.Caption = "There are " & count & " audits in the date range"
    lblSummary.Refresh

    ProgressBar1.value = 0
    ProgressBar1.Max = count + 1

    audit_column = IIf(optBrief.value, "brief_audit", "verbose_audit")
    sql = "select timestamp, " & audit_column & " as txt from event_log" & where
    rs.Open sql, g_cnADO

    For i = 1 To count
        ProgressBar1.value = i
        DoEvents
        If m_stop_request Then Exit For
        ExplodeAudit rs
        rs.MoveNext
    Next i

    rs.Close
    
    cmdGo.Enabled = True
    cmdStop.Enabled = False
End Sub

Private Sub cmdStop_Click()
    m_stop_request = True
End Sub

Private Sub CreateTempTable()
    Dim sql As String
    sql = "create table " & txtTmpTablename & " (" & _
        "  timestamp datetime" & _
        "  ,txt varchar(1024)" & _
        ")"
    On Error Resume Next
    g_cnADO.Execute sql
End Sub

Private Sub ExplodeAudit(rs As Recordset)
    Dim sql As String, arr() As String
    Dim n As Long, i As Long
    
    sql = "insert into " & txtTmpTablename & " (timestamp, txt) "
    
    n = g_util.SplitTextOnChar(rs("txt"), vbLf, arr(), 1, 0)
    For i = 1 To n
        If Len(arr(i)) Then
            If (i > 1) Then sql = sql & " union all "
            sql = sql & " select " & _
                g_dbutil.SQL_DateTime(rs("timestamp")) & "," & g_dbutil.SQL_String(Trim$(arr(i)))
            End If
    Next i
    
    g_cnADO.Execute sql
End Sub

