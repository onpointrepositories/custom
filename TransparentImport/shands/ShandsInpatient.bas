Attribute VB_Name = "ShandsInpatient"
Option Explicit
'
' Shands Inpatient (IP 2.0)
'
' This processes one patient using the Inpatient methodology.
'
' All search functions use exact match for code.
'
' result_like looks for LIKE matches in the result, except where EXACT_MATCH_PREFIX is pre-appended to target.
' result_list looks for any one of a list of words exactly as the result.
'
Private Const MAX_PROCS = 20

Private Const EXACT_MATCH_PREFIX = "&!"
Private Const LIKE_PREFIX = "%!"
Private Const CHAR_COMMA = "||"


Private Type indicator_data
    checked     As Boolean
    group       As Integer
    also_mark   As String                   'not used at Arnot (handy with user-defined indicators?)
End Type

Private Type procdata                       'not used at Arnot
    pnum    As Integer
    start   As Date
    finish  As Date
    isvalid As Boolean 'used in combining procs
    pindex As Integer 'used in combining procs = the index of procs whose start/finish times are equal to this one
End Type
Private Type ocdata                       'not used at Arnot
    checked As Boolean
    pnum    As Integer
    start   As String
End Type
'Private Type buckettype
'    start As Date
'    finish As Date
'    count As Integer
'End Type
Private Type bucket_type
    startdt As String
    enddt As String
    count As Integer
End Type
    

Private inds(MAX_INDS)          As indicator_data

Private numprocs                As Integer
Private numoutcomes             As Integer
Private procs(MAX_PROCS)        As procdata
Private oc(MAX_PROCS)           As ocdata

Private WHERE_ENCOUNTER         As String
Private AND_UNIT                As String
Private AND_PULL_RANGE          As String
Private AND_OUTCOMES_RANGE      As String
Private AND_ARRIVAL             As String
Private BUCKET_CALC             As String

Private m_pat                   As PatientInfo
Private m_assist_count          As Integer

Private adl23                   As Boolean
Private adl4                    As Boolean
Private tubefeed                As Boolean
Private morse                   As Integer

Private num_buckets             As Integer
Private bucket(48)             As bucket_type


Private Enum SearchMode
    SearchDefault
    Searchpullrange             'search within the current pull     -- default
    SearchSinceArrival          'search since arrival to the unit
    SearchSinceAdmission        'search everything since admission to the hospital
    SearchOutcomesRange         'search within the current pull + 24 hours before
    SearchAssessments
End Enum

Private Enum CountMode
    CountAll
    CountFirst                  'stop after one is found
End Enum

Private Enum GetValueMode
    GetTotal
    GetMax
    GetLast
End Enum

Private Const FREQUENCY_BUCKET_SIZE = 20    'min

Private Enum Frequencies
    QNONE
    Q4H
    Q2H
    Q1H
    Q30M
End Enum

Private Type fmapRow
    los_high                As Single       'the LOS being testing
    freq(QNONE To Q30M)     As Integer      'the count required for each Q value
End Type

Private m_freq_map(6)       As fmapRow      '1,2,4,8,12,24 hours
Private los_mins            As Long
Private tc_event_id         As Long



'This is the main entry point
'
Public Sub ProcessInpatient(pat As PatientInfo)
    Dim t1 As Single
    Dim t2 As Single
    On Error GoTo errHandler
    
    m_pat = pat
'    frmMain.SetProgress "Processing acct: " & m_pat.acct
    InitGroupsIfNeeded
    SetSQLConstants
    LoadFreqTable
    ResetIndicators
    
    los_mins = DateDiff("n", m_pat.pull_start, g_pull_finish)
    If los_mins <= 240 Then
        dvprint "Patient was here 4 hrs or less. Will receive default indicators."
        CheckDefaultIndicators (m_pat.unit_id)
    Else
        ResetProcs
    '    ResetOutcomes
    t1 = Timer
        Check_1234
    t2 = Timer
    dvprint "t01=" & t2 - t1
        Check_5
        Check_67
        Check_8
        Check_9
    t1 = Timer
        Check_1011
    t2 = Timer
    dvprint "t10=" & t2 - t1
    t1 = Timer
        Check_1213
    t2 = Timer
    dvprint "t12=" & t2 - t1
        Check_14
    t1 = Timer
        Check_15161718
    t2 = Timer
    dvprint "t15=" & t2 - t1
    t1 = Timer
        Check_19
    t2 = Timer
    dvprint "t19=" & t2 - t1
    t1 = Timer
        Check_20
    t2 = Timer
    dvprint "t20=" & t2 - t1
    t1 = Timer
        Check_2122
    t2 = Timer
    dvprint "t21=" & t2 - t1
        Check_23
    t1 = Timer
        Check_24
    t2 = Timer
    dvprint "t24=" & t2 - t1
        'CheckCustom
    End If
    HighestIndicatorInEachGroupWins

    CheckProcs
'    CheckOutcomes

    If g_no_output Then Exit Sub
    OutputClass
    OutputProcs
'    OutputOutcomes
    Exit Sub
    
errHandler:
    LogError Err.Description & " in " & Err.source, EVENT_CATEGORY_UNEXPECTED
    Exit Sub
    Resume  'debug
End Sub
Private Sub CheckDefaultIndicators(uid As Long)
    Dim i As Integer
    Dim j As Integer

    For i = 1 To numunit
        If definds(i).uid = uid Then
            For j = 1 To MAX_INDS
                inds(j).checked = (Mid$(definds(i).indstr, j, 1) = "Y")
            Next j
        End If
    Next i

End Sub

Private Sub ResetIndicators()
    Dim i As Integer
    
    For i = 1 To MAX_INDS
        inds(i).checked = False
        inds(i).also_mark = ""
    Next i
    
    adl23 = False
    adl4 = False
    tubefeed = False
    morse = 0
    
End Sub

Private Sub ResetProcs()
    numprocs = 0
End Sub
Private Sub ResetOutcomes()
    numoutcomes = 0
End Sub

Private Sub InitGroupsIfNeeded()
    On Error GoTo errHandler
    
    Static been_here As Boolean
    Dim rs As New Recordset
    Dim sql As String
    Dim inum As Integer
    
    If been_here Then Exit Sub
    
    'get indicator radio groups from the database
    sql = "SELECT INDICATOR_NUMBER, RADIO_GROUP FROM INDICATOR_DEFINITION WHERE METHODOLOGY_ID=" & m_pat.meth_id
    rs.Open sql, g_cnADO
    
    Do While Not rs.EOF
        inum = rs("INDICATOR_NUMBER")
        If (inum < MAX_INDS) Then
            inds(inum).group = g_dbutil.DBToInteger(rs("RADIO_GROUP"))  'could be null
        End If
        rs.MoveNext
    Loop
    
    rs.Close
    Exit Sub
    
errHandler:
    g_util.ThrowError "InitGroupsIfNeeded"
End Sub

Private Sub LoadFreqTableRow(m_freq As fmapRow, los_high As Single, values As String)
    Dim arr() As String, n As Integer, i As Integer
    
    m_freq.los_high = los_high

    n = g_util.SplitTextOnChar(values, ",", arr(), 0, 0)
    For i = 0 To n - 1
        m_freq.freq(i) = CInt(arr(i))
    Next i
End Sub

Private Sub LoadFreqTable()
                                 ' LOS,   None Q4h Q2h Q1h Q30m
    LoadFreqTableRow m_freq_map(1), 1, "    0,  0,  0,  0,  1"
    LoadFreqTableRow m_freq_map(2), 2, "    0,  0,  0,  1,  2"
    LoadFreqTableRow m_freq_map(3), 4, "    0,  1,  2,  3,  4"
    LoadFreqTableRow m_freq_map(4), 8, "    0,  2,  3,  4,  8"
    LoadFreqTableRow m_freq_map(5), 12, "   0,  2,  4,  6, 12"
    LoadFreqTableRow m_freq_map(6), 9999, " 0,  3,  6, 12, 24"         '24+
    
    'To read this table: if you have at least 6 asessmenets in a 12-hour period, you get Q1h
    '(row 5: LOS=12, column Q1h has a count of 6)
End Sub

Private Function FreqForCount(los_hours As Single, count As Integer) As Frequencies
    Dim i As Integer, j As Integer
    
    For i = 1 To UBound(m_freq_map)                         'go from LOS 1 to 24+
        If (los_hours <= m_freq_map(i).los_high) Then
            For j = Q30M To QNONE Step -1                   'search right to left
                If (count >= m_freq_map(i).freq(j)) Then
                    FreqForCount = j
                    Exit Function
                End If
            Next j
        End If
    Next i
    
    FreqForCount = QNONE
End Function

Private Sub SetSQLConstants()
    'These SQL constants are combined in different ways for searching
    WHERE_ENCOUNTER = " where (encounter_id = " & m_pat.encounter_id & ") and timestamp<=" & g_dbutil.SQL_DateTime(g_pull_finish)
    AND_UNIT = " and (unit_id = " & m_pat.unit_id & ")"
    AND_PULL_RANGE = " and (event_datetime between " & g_dbutil.SQL_DateTime(m_pat.pull_start) & " and " & g_dbutil.SQL_DateTime(m_pat.pull_finish) & ")"
    AND_ARRIVAL = " and (event_datetime between " & g_dbutil.SQL_DateTime(m_pat.unit_arrival) & " and " & g_dbutil.SQL_DateTime(m_pat.pull_finish) & ")"
    AND_OUTCOMES_RANGE = " and (event_datetime between " & g_dbutil.SQL_DateTime(DateAdd("d", -1, m_pat.pull_start)) & " and " & g_dbutil.SQL_DateTime(m_pat.pull_finish) & ")"
    
    'What bucket number does the event belong to?
    BUCKET_CALC = "DateDiff(minute," & g_dbutil.SQL_DateTime(m_pat.pull_start) & ",event_datetime) / " & FREQUENCY_BUCKET_SIZE
End Sub

'Add this to all chart item queries to start the WHERE clause
'This is where the search depth is set
Private Function WhereBase(Optional search_mode As SearchMode = SearchDefault)
    Dim result As String
    
    Select Case search_mode
    Case Searchpullrange, SearchDefault
        result = WHERE_ENCOUNTER & AND_UNIT & AND_PULL_RANGE    'search within pull range
    Case SearchSinceArrival
        result = WHERE_ENCOUNTER & AND_UNIT & AND_ARRIVAL       'search since arrival to the unit
    Case SearchSinceAdmission
        result = WHERE_ENCOUNTER                                'search since admission to the hospital (any unit)
    Case SearchOutcomesRange
        result = WHERE_ENCOUNTER & AND_UNIT & AND_OUTCOMES_RANGE    'search within pull range+24hrs before
    Case SearchAssessments
        result = WHERE_ENCOUNTER & AND_UNIT     'search within 12 hour range
    End Select
    
    'Since we are looking for one patient in one facility, we don't have to filter by TC_source_ID.
    
    WhereBase = result
End Function

Private Function ValueIsAList(result As String) As Boolean
    ValueIsAList = (InStr(1, result, ",") > 0)                'is there a comma-separated list?
End Function

'Look for any of these fields.  Cat/desc/field = exact match.  Result = like match.
Private Function AndSimpleItemFilter(cat As String, code As String, desc As String, field As String, result_like As String) As String
    Dim result As String
    Dim pos As Integer
    
    If Len(cat) Then result = result & " and category=" & g_dbutil.SQL_String(cat)
    If Len(code) Then
        If Mid$(code, 1, 2) = LIKE_PREFIX Then
            result = result & " and code like " & g_dbutil.SQL_String(Mid$(code, 3, Len(code) - 2) & "%")
        ElseIf ValueIsAList(code) Then
            result = result & " and " & FormatCodeList(code)
        Else
            result = result & " and code =" & g_dbutil.SQL_String(code)
        End If
    End If
    If Len(desc) Then result = result & " and description=" & g_dbutil.SQL_String(desc)
    If Len(field) Then result = result & " and field_name=" & g_dbutil.SQL_String(field)
    If Len(result_like) Then
        pos = InStr(1, result_like, CHAR_COMMA)
        If pos > 0 Then
            result_like = Mid$(result_like, 1, pos - 1) & "," & Mid$(result_like, pos + 2, Len(result_like) - pos - 1)
        End If

        If InStr(result_like, EXACT_MATCH_PREFIX) = 1 Then 'exact match
            result_like = Mid$(result_like, 3, Len(result_like) - Len(EXACT_MATCH_PREFIX))
            result = result & " and result= " & g_dbutil.SQL_String(result_like)
        Else
            result = result & " and result like '%" & Trim$(result_like) & "%'"
        End If
    End If

    AndSimpleItemFilter = result
End Function
Private Function FormatCodeList(code_list As String) As String
    Dim arr() As String
    Dim i As Integer, n As Integer
    Dim result As String

    If Len(code_list) = 0 Then Exit Function
    
    n = g_util.SplitTextOnChar(code_list, ",", arr(), 1, 0)
    
    result = "("
    
    For i = 1 To n
'        result = result & "code like '" & Trim$(arr(i)) & "%' or "
        result = result & "code ='" & Trim$(arr(i)) & "' or "
    Next i
    
    result = Mid$(result, 1, Len(result) - 3) & ")"
    
    FormatCodeList = result
End Function

'Look for a result that contains one of the words in the list
'The only drawback with this function is that we don't know what it found (for audits)
'Because of this, it is only used in as few places.
Private Function AndResultContains(result_list As String) As String
    Dim arr() As String
    Dim i As Integer, n As Integer
    Dim result As String
    Dim pos As Integer

    If Len(result_list) = 0 Then Exit Function
    
    n = g_util.SplitTextOnChar(result_list, ",", arr(), 1, 0)
    
    result = "and ((1=0)"   ' or (result=this) or (result=that)
    
    For i = 1 To n
        pos = InStr(1, arr(i), CHAR_COMMA)
        If pos > 0 Then
            arr(i) = Mid$(arr(i), 1, pos - 1) & "," & Mid(arr(i), pos + 2, Len(arr(i)) - pos - 1)
        End If
        result = result & " or (result like '" & "%" & Trim$(arr(i)) & "%')"
    Next i
    
    result = result & ")"
    
    AndResultContains = result
End Function

Private Function DescribeSearchDepth(search_mode As SearchMode) As String
    Dim result As String
    
    Select Case search_mode
    Case Searchpullrange, SearchDefault
        'result = "in pull range"
        result = ""                         'be quiet since this is default
    Case SearchSinceArrival
        result = "since arrival to unit"
    Case SearchSinceAdmission
        result = "since admission"
    Case SearchOutcomesRange
        result = "since 24hrs before pull"
    End Select
    
    DescribeSearchDepth = result
End Function

Private Function Describe(cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = Searchpullrange) As String
    Dim sql As String, and_filter As String
    Dim rs As New Recordset
    Dim result As String
   
    If Not g_debug Then Exit Function           'avoid extra overhead if not making a log

    result = "looking for"
    If Len(cat) Then result = result & " cat='" & cat & "'"
    If Len(code) Then result = result & " code='" & code & "'"
    If Len(desc) Then result = result & " desc='" & desc & "'"
    If Len(field) Then result = result & " field='" & field & "'"
    If Len(result_list) Then result = result & " result contains '" & result_list & "'"

    If ValueIsAList(result_list) Then
        and_filter = AndSimpleItemFilter(cat, code, desc, field, "") & AndResultContains(result_list)
    Else
        and_filter = AndSimpleItemFilter(cat, code, desc, field, result_list)
    End If
    
    sql = "select code, description, result from chart_item" & WhereBase(search_mode) & and_filter
    rs.Open sql, g_cnADO
    If rs.EOF Then
        result = result & "; not found " & DescribeSearchDepth(search_mode)
    Else
        result = result & "; found"
        'Add info for columns that were not specified and a value was found
        If (Len(code) = 0 Or ValueIsAList(code)) And Len(rs("code")) Then result = result & " code='" & rs("code") & "'"
        If (Len(desc) = 0) And Len(rs("description")) Then result = result & " desc='" & rs("description") & "'"
        'Add the complete result found (we searched for a word or words)
        result = result & " result='" & rs("result") & "'"
        'Are there more results?  Just say how many; we aren't going to list them.
        If (rs.RecordCount = 2) Then
            result = result & " (1 more result)"
        ElseIf (rs.RecordCount > 2) Then
            result = result & " (" & rs.RecordCount - 1 & " more results)"
        End If
    End If
    rs.Close
    
    Describe = result
End Function

'Set an indicator for this reason (low level)
Private Sub SetInd(inum As Integer, reason As String)
    If inds(inum).checked And Not g_debug Then Exit Sub       'already set and no log?

    inds(inum).checked = True
    dprint "Set Ind #" & inum & ": " & reason, 1
End Sub

'Clear an indicator for this reason (low level)
Private Sub ClrInd(inum As Integer, reason As String)
    If Not inds(inum).checked And Not g_debug Then Exit Sub   'already clear and no log?
    
    inds(inum).checked = False
    dprint "Clr Ind #" & inum & ": " & reason, 1
End Sub

'Count how many items have the search word in result_like
'This is simple and fast
Private Function CountSimpleResult(cat As String, code As String, desc As String, field As String, result_like As String, Optional search_mode As SearchMode = Searchpullrange, Optional trace As Boolean = True, Optional found_what As String) As Long
    Dim sql As String, and_filter As String
    Dim rs As New Recordset
    Dim count As Long
    Dim pos As Integer

    and_filter = AndSimpleItemFilter(cat, code, desc, field, result_like)
    sql = "select count(*) from chart_item" & WhereBase(search_mode) & and_filter
'    dvprint sql
    'Debug.Print sql
    rs.Open sql, g_cnADO
    count = rs(0)
    rs.Close
    
    'Normally CountSimpleResult will report what it finds; not eveyone wants this so the trace can be turned off
    found_what = ""
    If count > 0 Then found_what = "FOUND: "
    found_what = found_what & Describe(cat, code, desc, field, result_like, search_mode) & IIf(count > 0, "; count=" & count, "")
    If trace Then dvprint found_what
    
    CountSimpleResult = count
End Function
'Private Function CountUniqueCodesInList(codelist As String, retlist As String) As Long
'    Dim sql As String, and_filter As String
'    Dim rs As New Recordset
'    Dim count As Long
'
'    retlist = ""
'    sql = "select distinct(code) from chart_item" & WhereBase & " and code in (" & codelist & ")"
'    rs.Open sql, g_cnADO
'    Do While Not rs.EOF
'        If Not IsNull(rs(0)) Then
'            count = count + 1
'            retlist = retlist & rs(0) & " "
'        End If
'        rs.MoveNext
'    Loop
'    rs.Close
'
'    'Normally CountSimpleResult will report what it finds; not eveyone wants this so the trace can be turned off
'
'    CountUniqueCodesInList = count
'End Function
'Private Function CountCodeHitsInList(codelist As String, retlist As String) As Long
'    Dim sql As String, and_filter As String
'    Dim rs As New Recordset
'    Dim count As Long
'
'    retlist = ""
'    sql = "select count(code),code from chart_item" & WhereBase & " and code in (" & codelist & ")"
'    sql = sql & " group by code having count(code)>=2"
'    rs.Open sql, g_cnADO
'    Do While Not rs.EOF
'        If Not IsNull(rs(0)) And Not IsNull(rs(1)) Then
'            count = count + 1
'            retlist = retlist & rs(1) & " = " & rs(0) & " times; "
'        End If
'        rs.MoveNext
'    Loop
'    rs.Close
'    If Len(retlist) Then retlist = Mid$(retlist, 1, Len(retlist) - 2) & "."
'    'Normally CountSimpleResult will report what it finds; not eveyone wants this so the trace can be turned off
'
'    CountCodeHitsInList = count
'End Function

'Count how many items have a result with one of the words given in the result_list
'Returns a description of what word was found
'This is more powerful than CountSimpleResult but it is slower
Private Function CountResultInList(cat As String, code As String, desc As String, field As String, result_list As String, search_mode As SearchMode, count_mode As CountMode, trace As Boolean, found_what As String) As Integer
    On Error GoTo errHandler
    Dim sql As String, and_filter As String, arr() As String
    Dim rs As New Recordset
    Dim i As Integer, n As Integer, count As Integer
    Dim pos As Long
    
    n = g_util.SplitTextOnChar(result_list, ",", arr(), 1, 0)
    
    'Do not filter by result here; we need all results
    and_filter = AndSimpleItemFilter(cat, code, desc, field, "")
    sql = "select code,result from chart_item" & WhereBase(search_mode) & and_filter
    'Debug.Print sql
    rs.Open sql, g_cnADO
    Do While Not rs.EOF
        'Look for each search word in the result (case insensitive)
        For i = 1 To n
            arr(i) = Trim$(arr(i))          'we split on comma so there usually are leading blanks
            
            pos = InStr(1, arr(i), CHAR_COMMA)
            If pos > 0 Then
                arr(i) = Mid$(arr(i), 1, pos - 1) & "," & Mid(arr(i), pos + 2, Len(arr(i)) - pos - 1)
            End If
            
            If InStr(arr(i), EXACT_MATCH_PREFIX) = 1 Then
                arr(i) = Mid$(arr(i), 3, Len(arr(i)) - Len(EXACT_MATCH_PREFIX))
                pos = (rs("result") = arr(i))
            Else
                pos = InStr(1, rs("result"), arr(i), vbTextCompare)  'bad when looking for "RN" in "Non RN"
            End If
            If pos > 0 Then
                found_what = "found '" & arr(i) & "' in code='" & rs("code") & "' result='" & rs("result") & "'"
                If trace Then dvprint found_what
                count = count + 1
                If count_mode <> CountAll Then Exit Do  'stop counting
                Exit For                                'try next record
            End If
        Next i

        rs.MoveNext
    Loop
    
    If (count > 0) Then
        'We already printed what was found; maybe add how many?
        If trace And (count_mode = CountAll) Then dvprint "found " & count & " total"
    Else
        found_what = Describe(cat, code, desc, field, result_list, search_mode)       'not found
        If trace Then dvprint found_what
    End If
    
    rs.Close
    
    CountResultInList = count
    Exit Function
    
errHandler:
    g_util.ThrowError "CountResultInList"
    Resume   'debug
End Function

Private Function CountResultContains(cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = Searchpullrange, Optional trace As Boolean = True, Optional found_what As String) As Integer
    If ValueIsAList(result_list) Then
        CountResultContains = CountResultInList(cat, code, desc, field, result_list, search_mode, CountAll, trace, found_what)
    Else
        CountResultContains = CountSimpleResult(cat, code, desc, field, result_list, search_mode, trace, found_what)
    End If
End Function

Private Function ResultContains(cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = Searchpullrange, Optional trace As Boolean = True, Optional found_what As String) As Boolean
    'tracing can be turned off and you can ask for what was found instead of having it printed
    If ValueIsAList(result_list) Then
        ResultContains = (CountResultInList(cat, code, desc, field, result_list, search_mode, CountFirst, trace, found_what) > 0)
    Else
        ResultContains = (CountSimpleResult(cat, code, desc, field, result_list, search_mode, trace, found_what) > 0)
    End If
End Function


Private Function CountResultNotInList(cat As String, code As String, desc As String, field As String, result_list As String, search_mode As SearchMode, count_mode As CountMode, trace As Boolean, found_what As String) As Integer
    On Error GoTo errHandler
    Dim sql As String, and_filter As String, arr() As String
    Dim rs As New Recordset
    Dim i As Integer, n As Integer, count As Integer
    Dim found_one As Boolean

    n = g_util.SplitTextOnChar(result_list, ",", arr(), 1, 0)
    
    'Do not filter by result here; we need all results
    and_filter = AndSimpleItemFilter(cat, code, desc, field, "")
    sql = "select code, result from chart_item" & WhereBase(search_mode) & and_filter
    sql = sql & " and result<>'' and result is not null"
    'Debug.Print sql
    rs.Open sql, g_cnADO
    
    Do While Not rs.EOF
        'Look for each search word in the result (case insensitive)
        found_one = False
        
        For i = 1 To n
            arr(i) = Trim$(arr(i))          'we split on comma so there usually are leading blanks
            If InStr(1, rs("result"), arr(i), vbTextCompare) > 0 Then
                found_what = "found '" & arr(i) & "' in code='" & rs("code") & "' result='" & rs("result") & "' -- ignore this"
                If trace Then dvprint found_what
                found_one = True
                Exit For
            End If
        Next i

        If (Not found_one) Then
            'none of the words were found - good!
            found_what = Describe(cat, code, desc, field, "", search_mode) & " -- does not contain '" & result_list & "'"
            If trace Then dvprint found_what
            count = count + 1
            If count_mode <> CountAll Then Exit Do
        End If

        rs.MoveNext
    Loop
    
    If (count > 0) Then
        'We already printed what was found; maybe show the total?
        If trace And (count_mode = CountAll) Then dvprint "found " & count & " total"
    ElseIf (rs.RecordCount > 0) Then
        'We already printed what we ignored
    Else
        'Print what did not exist
        found_what = Describe(cat, code, desc, field, "", search_mode)      'not found
        If trace Then dvprint found_what
    End If
    
    rs.Close
    
    CountResultNotInList = count
    Exit Function
    
errHandler:
    g_util.ThrowError "CountResultNotInList"
    Resume   'debug
End Function

Private Function CountResultDoesNotContain(cat As String, code As String, desc As String, field As String, result_list As String, found_what As String, Optional search_mode As SearchMode = Searchpullrange, Optional trace As Boolean = True) As Integer
    CountResultDoesNotContain = CountResultNotInList(cat, code, desc, field, result_list, search_mode, CountAll, trace, found_what)
End Function

Private Function ResultDoesNotContain(cat As String, code As String, desc As String, field As String, result_list As String, found_what As String, Optional search_mode As SearchMode = Searchpullrange, Optional trace As Boolean = True) As Boolean
    ResultDoesNotContain = (CountResultNotInList(cat, code, desc, field, result_list, search_mode, False, trace, found_what) > 0)
End Function

'Set the indicator if the result contains one of the words in the result_list
Private Sub SetIndIfResultContains(inum As Integer, cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = Searchpullrange)
    Dim found_what As String
    'avoid more queries if the indicator is already set
    If inds(inum).checked Then Exit Sub
    
    'Turn trace off for ResultContains() so the result can be placed on the "SetInd#" line below
    If ResultContains(cat, code, desc, field, result_list, search_mode, False, found_what) Then
        SetInd inum, found_what             'echo here
    Else
        dvprint found_what                  'and here
    End If
End Sub

'Set the indicator if the result does not contain any of the words in result_list
Private Sub SetIndIfResultDoesNotContain(inum As Integer, cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = Searchpullrange)
    Dim found_what As String
    'avoid more queries if the indicator is already set
    If inds(inum).checked Then Exit Sub
    
    'Turn trace off for ResultDoesNotContain() and echo what was set below with SetInd
    If ResultDoesNotContain(cat, code, desc, field, result_list, found_what, search_mode, False) Then
        SetInd inum, found_what             'echo here
    Else
        dvprint found_what                  'and here
    End If
End Sub

'Clear the indicator if the result contains on of the words in the result_list
Private Sub ClrIndIfResultContains(inum As Integer, cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = Searchpullrange)
    Dim found_what As String
    'avoid more queries if the indicator is already clear
    If Not inds(inum).checked Then Exit Sub
    
    'Turn trace off for ResultContains() and echo what was set below with SetInd
    If ResultContains(cat, code, desc, field, result_list, search_mode, False, found_what) Then
        ClrInd inum, found_what             'echo here
    Else
        dvprint found_what                  'and here
    End If
End Sub


'These slightly smaller functions are meant for places where you aren't really looking for a chart result
Private Function Exists(cat As String, Optional code As String, Optional desc As String, Optional field As String, Optional result_list As String, Optional search_mode As SearchMode = Searchpullrange, Optional trace As Boolean = True, Optional found_what As String) As Boolean
    Exists = (CountResultContains(cat, code, desc, field, result_list, search_mode, trace, found_what) > 0)
End Function

Private Sub SetIndIfFound(inum As Integer, cat As String, Optional code As String, Optional desc As String, Optional field As String, Optional result_list As String, Optional search_mode As SearchMode = Searchpullrange)
    SetIndIfResultContains inum, cat, code, desc, field, result_list, search_mode
End Sub

Private Sub ClrIndIfFound(inum As Integer, cat As String, Optional code As String, Optional desc As String, Optional field As String, Optional result_list As String, Optional search_mode As SearchMode = Searchpullrange)
    ClrIndIfResultContains inum, cat, code, desc, field, result_list, search_mode
End Sub


'Get the max/total value from a result (usually in the middle of the text)
Private Function GetIntValue(get_mode As GetValueMode, cat As String, code As String, desc As String, field As String, result_like As String, Optional search_mode As SearchMode = Searchpullrange) As Integer
    Dim sql As String, and_filter As String, arr() As String, msg As String
    Dim rs As New Recordset
    Dim result As Integer, i As Integer, n As Integer, value As Integer
    Dim found_one As Boolean

    and_filter = AndSimpleItemFilter(cat, code, desc, field, result_like)
    sql = "select result from chart_item" & WhereBase(search_mode) & and_filter
    'Debug.Print sql
    rs.Open sql, g_cnADO
    
    'Look for a number in the result
    
    Do While Not rs.EOF
        n = g_util.SplitTextOnChar(rs(0), " ", arr(), 1, 0)
        For i = 1 To n
            'Look for 1st character numeric; IsNumeric("60min") will fail so look at 1st char only
            If IsNumeric(Left$(arr(i), 1)) Then
                value = val(arr(i))                         'Use Val; CInt will error on "60min"
                Select Case get_mode
                Case GetMax
                    result = g_util.Max(value, result)      'max
                Case GetTotal
                    result = result + value                 'total
                Case GetLast
                    result = value                          'last
                End Select
                
                'print what we are searching for (the first time)
                If Not found_one Then
                    dvprint Describe(cat, code, desc, field, result_like, search_mode)
                End If
                found_one = True
                'print each value found
                dvprint "  found numeric value " & result
                'Keep going in case there are more
            End If
        Next i
        rs.MoveNext
    Loop
    
    rs.Close
    
    If Not found_one Then
        'show what was not found
        If g_verbose Then dprint Describe(cat, code, desc, field, result_like, search_mode), 1
    End If
    
    GetIntValue = result
End Function

Private Function GetMaxValue(cat As String, code As String, desc As String, field As String, result_like As String, Optional search_mode As SearchMode = Searchpullrange) As Integer
    GetMaxValue = GetIntValue(GetMax, cat, code, desc, field, result_like, search_mode)
End Function

Private Function GetTotalValue(cat As String, code As String, desc As String, field As String, result_like As String, Optional search_mode As SearchMode = Searchpullrange) As Integer
    GetTotalValue = GetIntValue(GetTotal, cat, code, desc, field, result_like, search_mode)
End Function

'Get a result; returns True if found with return_result set
Private Function GetResult(cat As String, code As String, desc As String, field As String, return_result As String, Optional search_mode As SearchMode = Searchpullrange) As Boolean
    Dim rs As New Recordset
    Dim sql As String

    sql = "select result from chart_item" & WhereBase(search_mode) & AndSimpleItemFilter(cat, code, desc, field, "")
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        return_result = rs(0) & ""
    Else
        return_result = ""
    End If
    rs.Close

    dvprint Describe(cat, code, desc, field, "", search_mode)
    GetResult = (Len(return_result) > 0)
End Function
Private Function GetResultOfLatest(cat As String, code As String, desc As String, field As String, return_result As String, Optional search_mode As SearchMode = Searchpullrange) As Boolean
    Dim rs As New Recordset
    Dim sql As String
    Dim evdt As Date
    Dim done As Boolean

    sql = "select event_datetime,result from chart_item" & WhereBase(search_mode) & AndSimpleItemFilter(cat, code, desc, field, "")
    sql = sql & " order by event_datetime desc"
    rs.Open sql, g_cnADO
    
    return_result = ""
    done = False
    
    If Not rs.EOF Then evdt = rs(0)
    Do While Not rs.EOF And Not done
        If evdt = rs(0) Then
            return_result = return_result & rs(1) & ","
        Else
            done = True
        End If
        rs.MoveNext
    Loop
    rs.Close

    dvprint Describe(cat, code, desc, field, "", search_mode)
    GetResultOfLatest = (Len(return_result) > 0)

End Function

Private Sub Check_1234()
    On Error GoTo errHandler
    Dim mob3 As Boolean
    Dim mob4 As Boolean
    Dim feed3 As Boolean
    Dim feed4 As Boolean
    Dim hyg3 As Boolean
    Dim hyg4 As Boolean
    Dim found_what As String


    dvprint "---------------"
    dvprint "1. ADL Self"
    dvprint "2. ADL Assist"
    dvprint "3. ADL Extended"
    dvprint "4. ADL Complete"
    dvprint "---------------"
'COMPLETE CARE
    adl23 = ResultContains("", "LOAMOBILITY", "", "", "ADL assistance by 2-3 caregivers", , True, found_what) Or _
        ResultContains("", "HYGIENEASSISTANCE", "", "", "ADL assistance by 2-3 caregivers", , True, found_what)
    adl4 = ResultContains("", "LOAMOBILITY", "", "", "ADL assistance by 4 or more caregivers", , True, found_what) Or _
        ResultContains("", "HYGIENEASSISTANCE", "", "", "ADL assistance by 4 or more caregivers", , True, found_what)
    tubefeed = ResultContains("", "DIETTYPE", "", "", "Tube Feeding", , True, found_what)
    If GetResult("", "MORSESCORE", "", "", found_what) Then
        If IsNumeric(found_what) Then
            If found_what >= 45 Then
                morse = found_what
            End If
        End If
    End If
    
    SetADLCompleteWhenAge "<=3"
    If inds(4).checked Then Exit Sub
    
    feed4 = ResultContains("", "FEEDING", "", "", "Total Assist", , True, found_what) Or tubefeed Or _
            ResultContains("", "DIETTYPE", "", "", "NPO", , True, found_what)
    hyg4 = ResultContains("", "HYGIENEASSISTANCE", "", "", EXACT_MATCH_PREFIX & "Dependent", , True, found_what)
    If Not hyg4 Then hyg4 = ResultContains("", "HYGIENEASSISTANCE", "", "", "Maximum assist", , True, found_what)
    
    mob4 = ResultContains("", "LOAMOBILITY", "", "", "Dependent" & CHAR_COMMA & " patient does less than 25%", , True, found_what)
    If feed4 And hyg4 Then
        SetInd 4, "Total Feed+Dependent Hyg"
    End If
    
'    If ResultContains("", "FEEDING", "", "", "Total Assist") And _
'         (ResultContains("", "HYGIENEASSISTANCE", "", "", "Maximum assist") Or _
'          ResultContains("", "HYGIENEASSISTANCE", "", "", EXACT_MATCH_PREFIX & "Dependent")) Then
'            SetInd 4, "Total Feeding + Max/Dep Hygiene"
'    End If
                
                
    If inds(4).checked Then Exit Sub
    
'    SetIndIfResultContains 4, "", "FEEDING", "", "", "Total Assist"
'    SetIndIfResultContains 4, "", "LOAMOBILITY", "", "", "Maximum assist" & CHAR_COMMA & " patient does 25-49%"
'    SetIndIfResultContains 4, "", "LOAMOBILITY", "", "", "Dependent" & CHAR_COMMA & " patient does less than 25%"
    'SetIndIfResultContains 4, "", "HYGIENEASSISTANCE", "", "", "Maximum assist,Dependent"
'    SetIndIfResultContains 4, "", "HYGCONFACTORS", "", "", "Impaired Mobility,Obesity,Restraints,Risk of Dislodging LDAs,Unable to Follow Commands,Other (Comment)"

    SetIndIfResultContains 3, "", "GISYMPTOMS", "", "", "Incontinence"
    SetIndIfResultContains 3, "", "GUSYMPTOMS", "", "", "Incontinence"
    
    If feed4 Then feed3 = True
    If Not feed3 Then feed3 = ResultContains("", "FEEDING", "", "", "Needs Assist", , True, found_what)
    
    If hyg4 Then hyg3 = True
    If Not hyg3 Then
        hyg3 = ResultContains("", "HYGIENEASSISTANCE", "", "", "Minimal assist,Moderate assist,Maximum assist", , True, found_what)
    End If
    If Not hyg3 Then hyg3 = ResultContains("", "HYGIENEASSISTANCE", "", "", "ADL assistance by 2-3 caregivers,ADL assistance by 4 or more caregivers", , True, found_what)
   
    If mob4 Then mob3 = True
    If Not mob3 Then mob3 = ResultContains("", "LOAMOBILITY", "", "", "Minimal assist,Moderate assist,Maximum assist", , True, found_what) Or adl23 Or adl4
    If Not mob3 Then mob3 = ResultContains("", "LOAMOBILITY", "", "", EXACT_MATCH_PREFIX & "Dependent", , True, found_what)
    If Not mob3 Then
        If morse >= 45 Then
            dvprint "Morse score=" & morse
            mob3 = True
        End If
    End If
    
    If feed3 And hyg3 And mob3 Then
        SetInd 3, "Feed Assist + Hygiene Assist + Mobility Assist"
    End If
'    ElseIf feed3 And mob3 Then
'        SetInd 3, "Feed Assist + Mobility Assist"
'    ElseIf hyg3 And mob3 Then
'        SetInd 3, "Hygiene Assist + Mobility Assist"
    
    If inds(3).checked Or inds(4).checked Then Exit Sub

    
    If feed3 Or feed4 Then SetInd 2, "Feed Assist"
    If adl23 Or adl4 Then SetInd 2, "ADL assist by 2+"
    If mob3 Or mob4 Then SetInd 2, "Mobility assist"
    If hyg3 Or hyg4 Then SetInd 2, "Hygiene assist"
    SetIndIfResultContains 2, "", "ASSISTIVEDEVICE", "", "", "Cane,Crutches,walker,Gait belt,Hand held,lift,Sling,Wheelchair"
    
'    SetIndIfResultContains 2, "", "LOAMOBILITY", "", "", "Moderate assist,maximum assist"
    SetIndIfResultContains 2, "", "ACTCONFACTORS", "", "", "Impaired Mobility,Obesity,Restraints"
    SetIndIfResultContains 2, "", "ACTCONFACTORS", "", "", "Risk of Dislodging LDAs,Unable to Follow Commands,Other (Comment)"
    SetIndIfResultContains 2, "", "FEEDING", "", "", "Needs set up"
    'SetIndIfResultContains 2, "", "DIETTYPE", "", "", "Tube feeding"
    
    If inds(2).checked Then Exit Sub
    
    SetIndIfResultContains 1, "", "LOAMOBILITY", "", "", "Independent"
    SetIndIfResultContains 1, "", "LOAMOBILITY", "", "", "Minimal assist" & CHAR_COMMA & " patient does 75"

    If Not (inds(1).checked Or inds(2).checked Or inds(3).checked Or inds(4).checked) Then
        SetInd 2, "Defaulting to partial due to lack of documentation."
    End If
    
    Exit Sub
errHandler:
    g_util.ThrowError "Check_1234"
    Resume  'debug
End Sub
Private Sub SetADLCompleteWhenAge(agecond As String)  ' agecond example: "<=3"
    Dim sql As String
    Dim rs As New Recordset

'
'select case when round(age_at_admission,0,1) <=55 then 1 else 0 end from encounter where encounter_id=6990

    sql = "select case when round(age_at_admission,0,1) " & agecond & " then 1 else 0 end from encounter " & WHERE_ENCOUNTER
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        If rs(0) = 1 Then
            SetInd 4, "Age <=3 years"
        End If
    End If
    rs.Close

End Sub


Private Sub Check_5()
    Dim reslist As String
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "5. ADL Rehab"
    dvprint "---------------"
    
    reslist = "Decreased knowledge of therapeutic intervention,Impaired bed mobility,Impaired transfer ability,Impaired gait,Impaired wheelchair mobility,Decreased ability to ascend/descend stairs,Decreased ability to negotiate curb,Decreased aerobic capacity,Decreased UE ROM,Decreased LE ROM,Decreased UE strength,Decreased LE strength,Decreased sensation,Decreased skin integrity,Decreased ADL status,Impaired neurodevelopment (comment),Abnormal tone,Impaired balance,Impaired judgement,Impaired cognition,Impaired vision,Impaired coordination,Increased pain,Increased risk for falls,Other (comment)"
    SetIndIfResultContains 5, "", "PTASSESSMENT", "", "", reslist
    
    reslist = "Decreased ability to perform ADLs,Decreased knowledge self care,Decreased knowledge of therapeutic intervention,Decreased UE ROM,Decreased LE ROM,Decreased UE strength,Decreased LE strength,Decreased sensation,Decreased skin integrity,Impaired judgement,Impaired cognition,Impaired vision,Impaired coordination,Abnormal tone,Impaired neurodevelopment,Impaired feeding,Impaired balance,Impaired bed mobility,Impaired transfer mobility,Decreased aerobic capacity,Increased pain,Increased risk for falls"
    SetIndIfResultContains 5, "", "OTASSESSMENT", "", "", reslist
    SetIndIfResultContains 5, "", "ACTIVITY", "", "", "Assistance with relearning ADL activities"
    
    SetIndIfResultContains 5, "", "ACTIVITY", "", "", "Assistance with relearning ADL activities"
    SetIndIfResultContains 5, "", "ACTIVITY", "", "", "Therapy consulted via MD order"
    SetIndIfResultContains 5, "", "LOAMOBILITY", "", "", "Assistance with re-learning ADL activities"
    SetIndIfResultContains 5, "", "FEEDING", "", "", "Able to feed self with adaptive devices"
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_5"
    Resume  'debug
End Sub


Private Sub Check_67()
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "6. ADL 2-3 Caregivers"
    dvprint "7. ADL 4 or more Caregivers"
    dvprint "---------------"
    
    If adl23 Then SetInd 6, "ADL Assist by 2-3"
    If adl4 Then SetInd 7, "ADL Assist by 4+"
    SetIndIfResultContains 6, "", "THERMOREGULATION", "", "", "ECMO"
    SetIndIfResultContains 6, "", "OXYGENDEVICE", "", "", "Oscillator"
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_67"
    Resume  'debug
End Sub

Private Sub Check_8()
    Dim reslist As String
    Dim found_what As String
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "8. Communication"
    dvprint "---------------"
    
    SetIndIfResultContains 8, "", "RIGHTEYE", "", "", "Blind,(uncorrected)", SearchSinceAdmission
    SetIndIfResultContains 8, "", "LEFTEYE", "", "", "Blind,(uncorrected)", SearchSinceAdmission
    SetIndIfResultContains 8, "", "RIGHTEAR", "", "", "Deaf,(uncorrected)", SearchSinceAdmission
    SetIndIfResultContains 8, "", "LEFTEAR", "", "", "Deaf,(uncorrected)", SearchSinceAdmission
    
    SetIndIfResultContains 8, "", "BARRIERSCOMMUNICATION", "", "", "Vision,Hearing,Language,Literacy,Other", SearchSinceAdmission
'    reslist = "Slurred,Delayed responses,aphasia,Incomprehensible,Uses written communication"
'    SetIndIfResultContains 8, "", "SPEECH", "", "", reslist
'    reslist = "Sign Language,Communication board,Interpreter,Intubated"
'    SetIndIfResultContains 8, "", "SPEECH", "", "", reslist
'    reslist = "Trached,Nods/gestures appropriately,Augmentative Device"
'    SetIndIfResultContains 8, "", "SPEECH", "", "", reslist
'    reslist = "Difficulty talking,trach"
    
    reslist = "Slurred,Delayed responses,aphasia,Incomprehensible,Uses written communication"
    If ResultContains("", "SPEECH", "", "", reslist, , False, found_what) Then
        If InStr(1, found_what, "clear", vbTextCompare) = 0 And InStr(1, found_what, "appropriate for", vbTextCompare) = 0 Then
            SetInd 8, found_what
        End If
    End If
    reslist = "Sign Language,Communication board,Interpreter,Intubated"
    If ResultContains("", "SPEECH", "", "", reslist, , False, found_what) Then
        If InStr(1, found_what, "clear", vbTextCompare) = 0 And InStr(1, found_what, "appropriate for", vbTextCompare) = 0 Then
            SetInd 8, found_what
        End If
    End If
    reslist = "Trached,Nods/gestures appropriately,Augmentative Device"
    If ResultContains("", "SPEECH", "", "", reslist, , False, found_what) Then
        If InStr(1, found_what, "clear", vbTextCompare) = 0 And InStr(1, found_what, "appropriate for", vbTextCompare) = 0 Then
            SetInd 8, found_what
        End If
    End If
    reslist = "Difficulty talking,trach"
    SetIndIfResultContains 8, "", "VOICE", "", "", reslist

'    'If ResultDoesNotContain(cat, code, desc, field, result_list, found_what, search_mode, False) Then
'    SetIndIfResultDoesNotContain 8, "", "LANGUAGE", "", "", "English,Primary Language"
'    If inds(8).checked Then Exit Sub

    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_8"
    Resume  'debug
End Sub

Private Sub Check_9()
    On Error GoTo errHandler
    Dim found_what As String
    Dim reslist As String

    dvprint "---------------"
    dvprint "9. Cognitive Support"
    dvprint "---------------"
    
    SetIndIfResultContains 9, "", "ORIENTATION", "", "", "Disoriented"
    
    reslist = "Poor judgement,Poor safety awareness,Poor attention / concentration,Unable to follow commands"
    SetIndIfResultContains 9, "", "COGNITION", "", "", reslist
    
    If inds(9).checked Then Exit Sub
    If Not ResultContains("", "COGNITION", "", "", "No short term memory loss", , False, found_what) Then
        If ResultContains("", "COGNITION", "", "", "short term memory loss", , False, found_what) Then
            SetInd 9, "Found Short term memory loss."
        End If
    End If
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_9"
    Resume  'debug
End Sub

Private Sub Check_1011()
    On Error GoTo errHandler
    Dim reslist As String
    Dim ct As Integer
    Dim found_what As String
    Dim return_result As String
    Dim numbuckets As Integer

    dvprint "---------------"
    dvprint "10. Behavior/Emotional Management"
    dvprint "11. Behavior/Emotional Mgmt - q 1 Hour"
    dvprint "---------------"
    
    SetIndIfResultDoesNotContain 10, "", "INFANTBEHAVIOR", "", "", "Sleeping"
    
    If GetResult("", "NASTOTALSCORE", "", "", found_what) Then
        If IsNumeric(found_what) Then
            If found_what >= 8 Then
                SetInd 11, "NAS Total Score=" & found_what
            Else
                SetInd 10, "NAS Total Score=" & found_what
            End If
        End If
    End If
    
    reslist = "Anxious,Aggressive physically,Aggressive verbally,Depressed affect,Hallucinations,"
    reslist = reslist & "Homicidal,Hyperactivity,Non -consolable,Not interactive,"
    reslist = reslist & "Suicidal,Tearful,Uncooperative,Withdrawn"
    
    ct = ReturnQ1HrCount("PATIENTBEHAVIORS", reslist)
    If IsQ1Hour(ct) Then
        SetInd 11, "Patient Behavior count of " & ct & " in a range of " & g_range \ 60 & " hours."
    End If
'    If numbuckets >= 4 Then
'        If 1# * ct / numbuckets > 0.5 Then 'need doc at least half and for at least 4 hours
'            SetInd 11, "Patient Behavior found in a range of at least 4 hours and at least half of those hours."
'        End If
'    End If
    If inds(11).checked Then Exit Sub
    
    reslist = "anger,agitated,apathetic,combative,confused,denial,fearful,frustrated,hopeless,irritable,pessimistic,stressed,tearful,unrealistic"
    SetIndIfResultContains 10, "", "OBSERVEDFEELINGS", "", "", reslist
    If inds(10).checked Then Exit Sub
    
    reslist = "Anxious,Aggressive physically,Aggressive verbally,Depressed affect,Hallucinations,Homicidal,Hyperactivity,Non -consolable,Not interactive,Suicidal,Tearful,Uncooperative,Withdrawn"
    SetIndIfResultContains 10, "", "PATIENTBEHAVIORS", "", "", reslist
    If inds(10).checked Then Exit Sub
    SetIndIfResultContains 10, "", "FAMILYBEHAVIORS", "", "", reslist
    If inds(10).checked Then Exit Sub
    SetIndIfResultContains 10, "", "VISITORBEHAVIORS", "", "", reslist
    If inds(10).checked Then Exit Sub
'    reslist = "Hallucinations,Homicidal,Hyperactivity"
'    SetIndIfResultContains 10, "", "PATIENTBEHAVIORS", "", "", reslist
'    SetIndIfResultContains 10, "", "FAMILYBEHAVIORS", "", "", reslist
'    SetIndIfResultContains 10, "", "VISITORBEHAVIORS", "", "", reslist
'    If inds(10).checked Then Exit Sub
'    reslist = "Non -consolable,Not interactive,Suicidal"
'    SetIndIfResultContains 10, "", "PATIENTBEHAVIORS", "", "", reslist
'    SetIndIfResultContains 10, "", "FAMILYBEHAVIORS", "", "", reslist
'    SetIndIfResultContains 10, "", "VISITORBEHAVIORS", "", "", reslist
'    reslist = "Tearful,Uncooperative,Withdrawn"
'    SetIndIfResultContains 10, "", "PATIENTBEHAVIORS", "", "", reslist
'    SetIndIfResultContains 10, "", "FAMILYBEHAVIORS", "", "", reslist
'    SetIndIfResultContains 10, "", "VISITORBEHAVIORS", "", "", reslist
    
    SetIndIfResultContains 10, "", "PRECAUTIONS", "", "", "Suicide"
   
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_1011"
    Resume  'debug
End Sub
Private Function IsQ1Hour(count As Integer) As Boolean
    IsQ1Hour = False
    If g_range = 1440 Then
        IsQ1Hour = (count >= 14)
    ElseIf g_range >= 960 Then
        IsQ1Hour = (count >= 9)
    ElseIf g_range >= 720 Then
        IsQ1Hour = (count >= 6)
    ElseIf g_range >= 480 Then
        IsQ1Hour = (count >= 5)
    ElseIf g_range >= 240 Then
        IsQ1Hour = (count >= 3)
    End If

End Function

Private Sub Check_1213()
    On Error GoTo errHandler
    Dim ct As Integer
    Dim found_what As String
    Dim reslist As String
    
    dvprint "---------------"
    dvprint "12. Safety Management - q 2 Hours"
    dvprint "13. Safety Management - q 30 Minutes"
    dvprint "---------------"
    
    SetIndIfResultContains 13, "", "RESTRAINTBEHAVIORV", "", "", "No"
    reslist = "Start,Continued"
    If inds(13).checked Then Exit Sub
    SetIndIfResultContains 13, "", "VIOLENTMITTRT", "", "", reslist
    SetIndIfResultContains 13, "", "VIOLENTMITTLFT", "", "", reslist
    SetIndIfResultContains 13, "", "VIOLENTWRISTRT", "", "", reslist
    If inds(13).checked Then Exit Sub
    SetIndIfResultContains 13, "", "VIOLENTWRISTLFT", "", "", reslist
    SetIndIfResultContains 13, "", "VIOLENTANKLERT", "", "", reslist
    SetIndIfResultContains 13, "", "VIOLENTANKLELFT", "", "", reslist
    SetIndIfResultContains 13, "", "VIOLENTROLLBELT", "", "", reslist
    SetIndIfResultContains 13, "", "VIOLENTVEST", "", "", reslist
    If inds(13).checked Then Exit Sub
    SetIndIfResultContains 13, "", "VIOLENTSECLUSION", "", "", reslist
    SetIndIfResultContains 13, "", "VIOLENTLAPWAIST", "", "", reslist
    SetIndIfResultContains 13, "", "VIOLENTCHEST", "", "", reslist
    SetIndIfResultContains 13, "", "VIOLENTFOURWAY", "", "", reslist
    If inds(13).checked Then Exit Sub
    SetIndIfResultContains 13, "", "VIOLENTOTHER", "", "", reslist
    
    reslist = "Initiated,Continued"
    SetIndIfResultContains 13, "", "SITTER", "", "", reslist
    If inds(13).checked Then Exit Sub
    
    reslist = "Adjusting CPAP prongs,Adjusting bilimask,adjusting lines"
    If ResultContains("", "FREQUENTINTERVENTIONS", "", "", reslist, , True, found_what) Then
        SetIndIfResultContains 13, "", "FREQUENTINTERVENTIONS", "", "", "at least every 30"
        If Not inds(13).checked Then
            SetInd 12, "Frequent interventions: " & found_what
        End If
    End If
    
    If inds(13).checked Then Exit Sub
    If inds(12).checked Then Exit Sub
    If morse >= 45 Then SetInd 12, "Morse Score=" & morse
    If inds(12).checked Then Exit Sub
    If GetResult("", "GRAFPIFSCORE", "", "", found_what) Then
        If IsNumeric(found_what) Then
            If found_what >= 2 Then
                SetInd 12, "GRAF-PIF Score=" & found_what
            End If
        End If
    End If
    

    If inds(12).checked Then Exit Sub
    reslist = "Aspiration,Bleeding,Critical airway,Fracture,Fragile Bones,Latex,Neutropenic,Precaution Sign Posted,Reflux,Shunt,Silo,Sternal,Other (Comment)"
    SetIndIfResultContains 12, "", "NICUPRECAUTIONS", "", "", reslist
    If inds(12).checked Then Exit Sub
    
    
    
    SetIndIfResultContains 12, "", "PRECAUTIONS", "", "", "Aneurysm,Aspiration,Assault,A-V Fistula/Shunt,Bleeding,Blowout,Cardiac,Cytotoxic"
    SetIndIfResultContains 12, "", "PRECAUTIONS", "", "", "Critical Airway,Elopement,Epidural,Extremity,Latex,Pica"
    SetIndIfResultContains 12, "", "PRECAUTIONS", "", "", "Seizure,Spinal,sternal,Open sternal,Suicide,Victim of violence,Other (Comment)"
    
    If inds(12).checked Then Exit Sub
    
    
    SetIndIfResultContains 12, "", "RESTRAINTBEHAVIORNV", "", "", "No"
    reslist = "Start,Continued"
    SetIndIfResultContains 12, "", "NONVIOLENTMITTRT", "", "", reslist
    SetIndIfResultContains 12, "", "NONVIOLENTMITTLFT", "", "", reslist
    SetIndIfResultContains 12, "", "NONVIOLENTWRISTRT", "", "", reslist
    If inds(12).checked Then Exit Sub
    SetIndIfResultContains 12, "", "NONVIOLENTWRISTLFT", "", "", reslist
    SetIndIfResultContains 12, "", "NONVIOLENTANKLERT", "", "", reslist
    SetIndIfResultContains 12, "", "NONVIOLENTANKLELFT", "", "", reslist
    SetIndIfResultContains 12, "", "NONVIOLENTVEST", "", "", reslist
    If inds(12).checked Then Exit Sub
    SetIndIfResultContains 12, "", "NONVIOLENTLAPBELT", "", "", reslist
    SetIndIfResultContains 12, "", "NONVIOLENTROLLBELT", "", "", reslist
    SetIndIfResultContains 12, "", "GERICHAIR", "", "", reslist
    SetIndIfResultContains 12, "", "LOCKEDLIMB", "", "", reslist
    If inds(12).checked Then Exit Sub
    SetIndIfResultContains 12, "", "UNIVERSALCHAIR", "", "", reslist
    SetIndIfResultContains 12, "", "UNIVERSALBED", "", "", reslist
    SetIndIfResultContains 12, "", "NONVIOLENTOTHER", "", "", reslist
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_1213"
    Resume  'debug
End Sub

Private Sub Check_14()
    On Error GoTo errHandler
    Dim found_what As String
    Dim reslist As String
    
    dvprint "---------------"
    dvprint "14. Isolation"
    dvprint "---------------"

    'changed to since arrival 3/29
    reslist = "Contact,Droplet,Enteric,Airborne,Strict,Special,Neutropenic,Radiation"
    SetIndIfResultContains 14, "", "ISOLATIONPRECAUTIONS", "", "", reslist, SearchSinceArrival
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_14"
    Resume  'debug
End Sub

Private Sub CheckAssessment(count As Integer, desc As String)
    Dim los As Single
    Dim p As Single

    If (inds(18).checked) Then Exit Sub             'skip if highest already checked
    If (count = 0) Then Exit Sub                    'skip if none
    
    los = DateDiff("n", m_pat.pull_start, m_pat.pull_finish)
    
'    If los_mins > g_range Then
'        los = g_range
'    Else
'        los = los_mins
'    End If
'
    p = 1# * los / g_range  ' pro-rate factor 0 < p <= 1
    dvprint "Pro-rate factor=" & p

    If g_range = 1440 Then
        desc = "24hour counts: " & desc
        If count >= g_util.Max(8, 29 * p) Then
            SetInd 18, desc
        ElseIf count >= g_util.Max(4, 15 * p) Then
            SetInd 17, desc
        ElseIf count >= g_util.Max(2, 8 * p) Then
            SetInd 16, desc
        ElseIf count >= g_util.Max(1, 4 * p) Then
            SetInd 15, desc
        End If
    ElseIf g_range >= 960 Then
        desc = "16hour counts: " & desc
        If count >= g_util.Max(8, 16 * p) Then
            SetInd 18, desc
        ElseIf count >= g_util.Max(4, 9 * p) Then
            SetInd 17, desc
        ElseIf count >= g_util.Max(2, 5 * p) Then
            SetInd 16, desc
        ElseIf count >= g_util.Max(1, 3 * p) Then
            SetInd 15, desc
        End If
    ElseIf g_range >= 720 Then
        desc = "12.5hour counts: " & desc
        If count >= g_util.Max(8, 11 * p) Then
            SetInd 18, desc
        ElseIf count >= g_util.Max(4, 6 * p) Then
            SetInd 17, desc
        ElseIf count >= g_util.Max(2, 3 * p) Then
            SetInd 16, desc
        ElseIf count >= g_util.Max(1, 2 * p) Then
            SetInd 15, desc
        End If
    ElseIf g_range >= 480 Then
        desc = "8.5hour counts: " & desc
        If count >= g_util.Max(8, 10 * p) Then
            SetInd 18, desc
        ElseIf count >= g_util.Max(4, 5 * p) Then
            SetInd 17, desc
        ElseIf count >= g_util.Max(2, 3 * p) Then
            SetInd 16, desc
        ElseIf count >= g_util.Max(1, 2 * p) Then
            SetInd 15, desc
        End If
    ElseIf g_range >= 240 Then
        desc = "4hour counts: " & desc
        If count >= g_util.Max(4, 5 * p) Then
            SetInd 18, desc
        ElseIf count >= g_util.Max(3, 3 * p) Then
            SetInd 17, desc
        ElseIf count >= g_util.Max(2, 2 * p) Then
            SetInd 16, desc
        ElseIf count >= g_util.Max(1, 1 * p) Then
            SetInd 15, desc
        End If
    End If

End Sub

Private Sub Check_15161718()
    Dim found_what As String
    Dim qmins As Integer
    Dim ct As Integer
    Dim ct1 As Integer
    Dim ct2 As Integer
    Dim ct3 As Integer
    Dim ct4 As Integer
    Dim ct5 As Integer
    Dim codelist As String
    Dim return_result As String
    
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "15. Assessment q4h"
    dvprint "16. Assessment q2h"
    dvprint "17. Assessment q1h"
    dvprint "18. Assessment q30min"
    dvprint "---------------"
    
    'direct triggers for NICU
    SetIndIfResultContains 18, "", "ADDITIONALINFANTMONITORING", "", "", "Cooling"
    If inds(18).checked Then Exit Sub
    SetIndIfResultContains 17, "", "APNEA", "", "", "Yes,No"
    SetIndIfResultContains 17, "", "BRADYCARDIA", "", "", ""
    SetIndIfResultContains 17, "", "PEDSFEEDINGROUTE", "", "", "Continuous"
    
    'direct trigger for minimum Q2
    SetIndIfResultContains 16, "", "LVAD", "", "", ""
    
' ct events q 12 hours - count distinct event times!
' select count(distinct(times)) where eventcode in ()
'select min(time) where code in () and time>=pull-24
'add 12 hours
'select count(distinct(times)) where code in () and time between min and min+12
'returnmaxcount(codeslist,n) in the past n hours
'3=14, 6=15, 12=16, 24=17

    If IsAssessUnit Then 'special q30 buckets
        SpecialAssess
    Else
        RegularAssess
    End If
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_15161718"
    Resume  'debug
End Sub
Private Sub SpecialAssess()
    Dim found_what As String
    Dim qmins As Integer
    Dim ct As Integer
    Dim codelist As String
    Dim return_result As String
    
    SetupBuckets (30)

    codelist = "CORETEMP,TEMPERATURE,TEMPSOURCE"
    PutIntoBuckets (codelist)
PrintBucketFreq "Temp buckets="
ct = CountBuckets
ClearBuckets
    CheckAssessment ct, "Temp=" & ct
    If inds(18).checked Then Exit Sub
    
    codelist = "ORALNASALETCO2,RESPIRATION,SPO22PREDUCTAL"
    PutIntoBuckets (codelist)
PrintBucketFreq "Pulmonary buckets="
ct = CountBuckets
ClearBuckets
    CheckAssessment ct, "Pulmonary=" & ct
    If inds(18).checked Then Exit Sub
    
    
    codelist = "HEARTRATE,BLOODPRESSURE2,HEARTRATEMONITOR,BP,MAP2,BLOODPRESSURE3,BLOODPRESSURE4,OBFHRDOPPLER,LVAD,"
    codelist = codelist & "ARTLINEBP,ARTLINEBP2,ARTLINEBP3,IABFREQUENCY,SWANGANZ,OBFHRDOPPLER,DIASTOLIC_BP,SYSTOLIC_BP"
    'removed CARDRHYTHM for
    PutIntoBuckets (codelist)
PrintBucketFreq "Cardiac buckets="
ct = CountBuckets
ClearBuckets
    CheckAssessment ct, "Cardiac=" & ct
    If inds(18).checked Then Exit Sub
    
    codelist = "CARDRHYTHM"
    PutIntoBuckets (codelist)
PrintBucketFreq "CardRhythm buckets="
ct = CountBuckets
ClearBuckets
    CheckAssessment ct, "CardRhythm=" & ct
    If inds(18).checked Then Exit Sub


    codelist = "BLADDERPRESSURE,ABDOMGIRTH,BLADDERSCAN"
    PutIntoBuckets (codelist)
PrintBucketFreq "Intra-Abdominal buckets="
ct = CountBuckets
ClearBuckets
    CheckAssessment ct, "Intra-Abdominal=" & ct
    If inds(18).checked Then Exit Sub

'    codelist = LIKE_PREFIX & "EPIDURALSITEASSESS"
'    ct = ReturnAssessCount(codelist)
'    codelist = LIKE_PREFIX & "DOPPFLAPCHECK"
'    ct = ct + ReturnAssessCount(codelist)
    codelist = "CATHSITECHECK,EPIDURALSITEASSESS,DOPPFLAPCHECK"
    PutIntoBuckets (codelist)
PrintBucketFreq "Wound buckets="
ct = CountBuckets
ClearBuckets
    CheckAssessment ct, "Wound=" & ct
    If inds(18).checked Then Exit Sub
    
    codelist = "BISRANGE,TRAINOFFOUR,CHARTINGTYPE"
    PutIntoBuckets (codelist)
PrintBucketFreq "Neuro buckets="
ct = CountBuckets
ClearBuckets
    CheckAssessment ct, "Neuro=" & ct
    If inds(18).checked Then Exit Sub


    codelist = "DVPRSPAINSCALE,ANVPSPAINSCORE,PAINSCALE0TO10,NIPSSCORE,NPASSSCORE,PEDSPAINASSESSFLACC,PAINWONGBAKERFACESSCORE"
    PutIntoBuckets (codelist)
PrintBucketFreq "Pain buckets="
ct = CountBuckets
ClearBuckets
    CheckAssessment ct, "Pain=" & ct
    If inds(18).checked Then Exit Sub
    

End Sub
Private Sub RegularAssess()
    Dim found_what As String
    Dim qmins As Integer
    Dim ct As Integer
    Dim codelist As String
    Dim return_result As String
    
    codelist = "CORETEMP,TEMPERATURE,TEMPSOURCE"
    ct = ReturnAssessCount(codelist)
    CheckAssessment ct, "Temperature=" & ct
    
    codelist = "ORALNASALETCO2,RESPIRATION,SPO22PREDUCTAL"
    ct = ReturnAssessCount(codelist)
    CheckAssessment ct, "Pulmonary=" & ct

    codelist = "HEARTRATE,BLOODPRESSURE2,HEARTRATEMONITOR,BP,MAP2,BLOODPRESSURE3,BLOODPRESSURE4,OBFHRDOPPLER,LVAD,"
    codelist = codelist & "ARTLINEBP,ARTLINEBP2,ARTLINEBP3,IABFREQUENCY,SWANGANZ,OBFHRDOPPLER,DIASTOLIC_BP,SYSTOLIC_BP"
    'removed CARDRHYTHM for
    ct = ReturnAssessCount(codelist)
    CheckAssessment ct, "Cardiac=" & ct
    
    codelist = "CARDRHYTHM"
    ct = ReturnAssessCount(codelist)
    CheckAssessment ct, "CardRhythm=" & ct


    codelist = "BLADDERPRESSURE,ABDOMGIRTH,BLADDERSCAN"
    ct = ReturnAssessCount(codelist)
    CheckAssessment ct, "Intra-Abdominal=" & ct

'    codelist = LIKE_PREFIX & "EPIDURALSITEASSESS"
'    ct = ReturnAssessCount(codelist)
'    codelist = LIKE_PREFIX & "DOPPFLAPCHECK"
'    ct = ct + ReturnAssessCount(codelist)
    codelist = "CATHSITECHECK,EPIDURALSITEASSESS,DOPPFLAPCHECK"
    ct = ReturnAssessCount(codelist)
    CheckAssessment ct, "Wound=" & ct
    
    codelist = "BISRANGE,TRAINOFFOUR,CHARTINGTYPE"
    ct = ReturnAssessCount(codelist)
    CheckAssessment ct, "Neuro=" & ct


    codelist = "DVPRSPAINSCALE,ANVPSPAINSCORE,PAINSCALE0TO10,NIPSSCORE,NPASSSCORE,PEDSPAINASSESSFLACC,PAINWONGBAKERFACESSCORE"
    ct = ReturnAssessCount(codelist)
    CheckAssessment ct, "Pain=" & ct
    
'1 - Temp
'CORETEMP,TEMPERATURE,TEMPSOURCE

'2-pulm
'ORALNASALETCO2,RESPIRATION,SPO22PREDUCTAL

'3-cardiac
'HEARTRATE,BLOODPRESSURE2,HEARTRATEMONITOR,BP,MAP2,BLOODPRESSURE3,BLOODPRESSURE4,OBFHRDOPPLER,CARDRHYTHM,LVAD,
'ARTLINEBP,ARTLINEBP2,ARTLINEBP3,IABFREQUENCY,SWANGANZ,OBFHRDOPPLER,DIASTOLIC_BP,SYSTOLIC_BP

'4 abdm
'BLADDERPRESSURE,ABDOMGIRTH,BLADDERSCAN

'5 wound
'OBFUNDUS,EPIDURALSITEASSESS,DOPPFLAPCHECK

'6 neuro
'BISRANGE,TRAINOFFOUR,CHARTINGTYPE

'7 pain
'PAINSCALE0TO10,NIPSSCORE,NPASSSCORE,PEDSPAINASSESSFLACC,PAINWONGBAKERFACESSCORE

End Sub

Private Sub Check_19()
    Dim codelist As String
    Dim ct As Integer
    Dim found_what As String
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "19. Vascular Access Site Mgt q1 Hour"
    dvprint "---------------"
        
    codelist = LIKE_PREFIX & "LINESITEASSESS"
    ct = ReturnQ1HrCount(codelist, "")
    If IsQ1Hour(ct) Then
        SetInd 19, "Line Site count of " & ct & " in a range of " & g_range \ 60 & " hours."
    End If

'added 4/5 Vascular triggered when:
'age < 17 and under with periph iv
    If m_pat.age >= 17 Then Exit Sub
    
    If m_pat.unit_name = "NICU 2" Or m_pat.unit_name = "NICU 3" Then
        If ResultContains("", "LINESTATUS", "", "", "infusing", , False, found_what) Then
            SetInd 17, "NICU: Line Status Infusing" & found_what
            SetInd 19, "NICU: Line Status Infusing" & found_what
        End If
    End If
        

    codelist = LIKE_PREFIX & "DRSGINTERVENTION"
    If ResultContains("", codelist, "", "", "", , False, found_what) Then
        dvprint found_what
        If InStr(1, found_what, "peripheral iv", vbTextCompare) > 0 Then
            SetInd 19, "Age<17: " & found_what
        End If
    End If
    
    codelist = LIKE_PREFIX & "DRSGSTATUS"
    If ResultContains("", codelist, "", "", "", , False, found_what) Then
        dvprint found_what
        If InStr(1, found_what, "peripheral iv", vbTextCompare) > 0 Then
            SetInd 19, "Age<17: " & found_what
        End If
    End If

'    ct = ReturnAssessCount(codelist)
'    If g_range = 1440 Then
'        If ct >= 12 Then SetInd 19, "LINESITEASSESS Count=" & ct & " for 24 hours."
'    ElseIf g_range = 600 Then
'        If ct >= 5 Then SetInd 19, "LINESITEASSESS Count=" & ct & " for 10 hours."
'    ElseIf g_range = 240 Then
'        If ct >= 2 Then SetInd 19, "LINESITEASSESS Count=" & ct & " for 4 hours."
'    End If
    
'    SetIndIfFound 19, "", LIKE_PREFIX & "LINESITEASSESS"
'For 24 hours, count is 12+; for 4 hour pull 2+; for 9 hour pull 5+
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_19"
    Resume  'debug
End Sub
Private Sub Check_20()
    Dim found_what As String
    Dim medlist As String
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "20. Medication Activity >= 20 minutes"
    dvprint "---------------"
    
    SetIndIfResultContains 20, "", "PATIENTBEHAVIORS", "", "", "Difficulty taking medications"
    SetIndIfResultContains 20, "", "THROAT", "", "", "swallow"
    If tubefeed Then SetInd 20, "Tube Feeding"
    SetIndIfResultContains 20, "", "PRECAUTIONS", "", "", "Cytotoxic"
    If inds(20).checked Then Exit Sub
    medlist = "ABCIXIMABVOLUME,AMIODARONEVOLUME,ARGATROBANVOLUME,ATRACURIUMVOLUME,BIVALIRUDINVOLUME,DEXMEDETOMIDINEVOLUME,"
medlist = medlist & "DILTIAZEMVOLUME,DOBUTAMINEVOLUME,DOPAMINEVOLUME,EPIDURALINTAKE,EPINEPHRINEVOLUME,EPOPROSTENOLVOLUME,"
medlist = medlist & "EPTIFIBATIDEVOLUME,ESMOLOLVOLUME,FENTANYLVOLUME,HEPARINVOLUME,HYDROMORPHONEVOLUME,INSULINVOLUME,"
medlist = medlist & "ISOPROTERENOLVOLUME,KETAMINEVOLUME,LABETALOLVOLUME,LIDOCAINEVOLUME,LORAZEPAMVOLUME,MAGNESIUMVOLUME,"
medlist = medlist & "MIDAZOLAMVOLUME,MILRINONEVOLUME,NESIRITIDEVOLUME,NITROGLYCERINVOLUME,NITROPRUSSIDEVOLUME,NOREPINEPHRINEVOLUME,"
medlist = medlist & "OXYTOCINVOLUME,PANCURONIUMVOLUME,PHENYLEPHRINEVOLUME,PROCAINAMIDEVOLUME,PROPOFOLVOLUME,TIROFIBANVOLUME,"
medlist = medlist & "TREPROSTINILVOLUME,VASOPRESSINVOLUME,VECURONIUMVOLUME,TPNVOLUME,BLOODPRODUCTVOLUME,ALTEPLASEVOLUME"
    SetIndIfResultContains 20, "", medlist, "", "", ""
    
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_20"
    Resume  'debug
End Sub

'Private Sub CheckWound(total As Integer)
'    If (inds(20).checked) Then Exit Sub             'skip if highest already checked
'
'    If (total = 0) Then
'        'skip it
'    ElseIf (total >= 30) Then
'        SetInd 20, "wound >= 30 min"
'    Else
'        SetInd 19, "wound < 30 min"
'    End If
'End Sub

Private Sub Check_2122()
    Dim return_result As String
    Dim codelist As String
    Dim found_what As String
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "21. Wound/Injury Mgmt"
    dvprint "22. Wound/Injury Mgmt >= 30 Minutes"
    dvprint "---------------"
    
    CheckNICUSkinIntegrity

    SetIndIfFound 22, "", "CONFACWNDCARETIME"
    
    If inds(22).checked Then Exit Sub
    CheckDressing
    If inds(21).checked Then Exit Sub
    
    codelist = "OBFUNDUS,CATHSITECHECK"
    SetIndIfFound 21, "", codelist
    If inds(21).checked Then Exit Sub
    
    codelist = LIKE_PREFIX & "WOUNDSITECLOSURE"
    SetIndIfFound 21, "", codelist
    
    
'    codelist = LIKE_PREFIX & "DRSGINTERVENTION"
'    SetIndIfFound 21, "", codelist
'    codelist = LIKE_PREFIX & "DRSGSTATUS"
'    SetIndIfFound 21, "", codelist
    codelist = LIKE_PREFIX & "OSTOMYSITEASSESS"
    SetIndIfFound 21, "", codelist
    If inds(21).checked Then Exit Sub
    
    CheckSkinIntegrity

'    If ResultContains("", "SKININTEGRITY", "", "", "intact", , False, found_what) Then
'        SetIndIfResultContains 21, "", "SKININTEGRITY", "", "", "Abrasion,Blister,Burn,Denuded,Electrode mark,Erythema,Excoriation"
'        SetIndIfResultContains 21, "", "SKININTEGRITY", "", "", "Fragile,Hematoma,Indurated,Laceration,Lesion,Macerated,Petechia,Rash,Redness,Tear,Weeping"
'    End If
    If inds(21).checked Then Exit Sub

    codelist = "OBNEWBORNUMBCARE,OBNEWBORNCIRCCARE,OBPERIAPPEAR,CBIRRIGANT"
    SetIndIfFound 21, "", codelist

'DRSGINTERVENTION& 938
'DRSGSTATUS& 1004
'  DOPPFLAPCHECK& 3091
'  EPIDURALSITEASSESS& 1001
'  LINESITEASSESS& 100
'OSTOMYSITEASSESS& 1006
'WOUNDSITECLOSURE& 1023


    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_2122"
    Resume  'debug
End Sub


Private Sub Check_23()
    Dim fnd284 As Boolean
    Dim fnd295 As Boolean
    
    On Error GoTo errHandler
    
    Dim mins As Integer
    Dim chart_result As String
    
    dvprint "---------------"
    dvprint "23. Healthcare Mgmt Education >= 1 Hour"
    dvprint "---------------"

    SetIndIfResultContains 23, "", "EDPERTAINTO", "", "", "1,2,3"
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_23"
    Resume  'debug
End Sub

Private Sub Check_24()
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "24. 1 to 1 Physiological Interv. >= 2 Hours"
    dvprint "---------------"

    If Not IsICU Then Exit Sub
    SetupBuckets 60
    If ReturnVSCount(9) >= 9 Then SetInd 24, "9 or more VS in 2 hours."

    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_24"
    Resume  'debug
End Sub
'Private Sub CheckCustom()
'    On Error GoTo errHandler
'
'    dvprint "---------------"
'    dvprint "33. Central Line"
'    dvprint "34. Ventilator"
'    dvprint "35. Foley Catheter"
'    dvprint "---------------"
'
'    SetIndIfFound 33, "", "CENTRAL_LINE,GROSHONG,HICKMAN,IMPLANTED_PORT,PICC,POWER_PORT,ADAPTER"
'    If Not inds(33).checked Then
'        'until discontinued
'        SetIndIfFound 33, "", "CENTRL,CRLDSG,PICCARE"
'    End If
'
'    If Not inds(33).checked Then
'        SetIndIfFound 33, "", "FND103633,FND103481"
'    End If
'
'    If Not inds(33).checked Then
'        SetIndIfFound 33, "", "FND103311,FND103809,FND103338,FND103339,FND103340,FND103341,FND103342,FND103343,FND103344"
'    End If
'
'    If Not inds(33).checked Then
'    If Exists("", "FND102293") Then
'        If Exists("", "FND10338,FND10339,FND10340,FND10341,FND10342,FND10343,FND10344") Then
'            SetInd 33, "FND102293"
'        End If
'    End If
'    End If
'
'    SetIndIfFound 34, "", "FND10386,FND10387,FND10388,FND10390,FND10391,VENTCHG,VENT1ST"
'    If Not inds(34).checked Then
'        SetIndIfFound 34, "", "FND10360,FND10361,FND10456,FND103633,FND101679,FND103618,FND103621,FND103622"
'    End If
'
'
'    If Not Exists("", "FND107250") Then   'FND107250 negates 35
'    SetIndIfFound 35, "", "FOLEY,Ucatheter,FOLEYIR"
'    If Not inds(35).checked Then
'        SetIndIfFound 35, "", "FND188,CATH1"
'    End If
'    If Not inds(35).checked Then
'        SetIndIfFound 35, "", "FND100323,FND100321,FND100322,FND104310,FND104309"
'    End If
'    If Not inds(35).checked Then
'        SetIndIfFound 35, "", "FND104315,FND102318,FND100594,FND100325,FND100326"
'    End If
'    If Not inds(35).checked Then
'        SetIndIfFound 35, "", "FND107399,FND107400,FND107401,FND107402,FND107403"
'    End If
'    If Not inds(35).checked Then
'        SetIndIfFound 35, "", "FND107404,FND107405,FND038"
'    End If
'    End If 'negate
'
'
'    Exit Sub
'
'errHandler:
'    g_util.ThrowError "CheckCustom"
'    Resume  'debug
'End Sub

Private Sub ProcessProc1()
    Dim sql As String
    Dim rs As New Recordset
    Dim nowstr As String
    Dim yesdt As Date
    Dim toddtstr As String
    Dim yesdtstr As String
    Dim timea As String
    Dim timea_startdt As Date, timea_enddt As Date
    Dim timeb_startdt As Date, timeb_enddt As Date
    
    nowstr = Format$(nowdt, "yyyymmddhhnn")
    yesdt = DateAdd("d", -1, nowdt)
    toddtstr = Format$(nowdt, "yyyymmdd")
    yesdtstr = Format$(yesdt, "yyyymmdd")
    
    'when is now?
    'yes 7am -- yes 7p  -- tod 7a -- tod 7p
    '                                   A                   B
    'if nowdt >= tod7pm then check  tod 7am-tod 7pm and tod 7pm-tom 7a
    'if nowdt >= tod7am then check  yes 7p - tod 7a   tod 7a-tod 7p
    'if nowdt >= yes7p then check  yes 7a-yes 7p       yes 7p-tod 7a
    If nowstr >= toddtstr & "1900" Then
        timea = toddtstr & "0700"
'        timeb = toddtstr & "1900"
    ElseIf nowstr >= toddtstr & "0700" Then
        timea = yesdtstr & "1900"
'        timeb = toddtstr & "0700"
    ElseIf nowstr >= yesdtstr & "1900" Then
        timea = yesdtstr & "0700"
'        timeb = yesdtstr & "1900"
    End If
    
    timea_startdt = g_util.CDateEx(timea)
    timea_enddt = DateAdd("h", 12, timea_startdt)
    sql = "select count(*) from chart_item " & "where encounter_id=" & m_pat.encounter_id
    sql = sql & AND_UNIT & " and code = 'Sitter' and"
    sql = sql & " (result like '%continued%' or result like '%initiated%') and (result not like '%discontinued%')"
    sql = sql & " and event_datetime between " & g_dbutil.SQL_DateTime(timea_startdt)
    sql = sql & " and " & g_dbutil.SQL_DateTime(timea_enddt)
    dvprint sql
    rs.Open sql, g_cnADO
'    dvprint sql

    If Not rs.EOF Then
        If rs(0) > 0 Then
            'if proc1exists between timea and timea+12
            If Not ProcExists(2, timea_startdt, timea_enddt) Then
                numprocs = numprocs + 1
                procs(numprocs).pnum = 2
                procs(numprocs).start = timea_startdt
                procs(numprocs).finish = timea_enddt
                dvprint "Procedure 2 triggered: Found Sitter between " & timea_startdt & " and " & timea_enddt
            End If
        End If
    End If
    rs.Close

    timeb_startdt = timea_enddt
    timeb_enddt = DateAdd("h", 12, timeb_startdt)
    sql = "select count(*) from chart_item " & "where encounter_id=" & m_pat.encounter_id
    sql = sql & AND_UNIT & " and code = 'Sitter' and"
    sql = sql & " (result like '%continued%' or result like '%initiated%') and (result not like '%discontinued%')"
    sql = sql & " and event_datetime between " & g_dbutil.SQL_DateTime(timeb_startdt)
    sql = sql & " and " & g_dbutil.SQL_DateTime(timeb_enddt)
    'dvprint sql
    rs.Open sql, g_cnADO
'    dvprint sql

    If Not rs.EOF Then
        If rs(0) > 0 Then
            'if proc1exists between timea and timea+12
            If Not ProcExists(2, timeb_startdt, timeb_enddt) Then
                numprocs = numprocs + 1
                procs(numprocs).pnum = 2
                procs(numprocs).start = timeb_startdt
                procs(numprocs).finish = timeb_enddt
                dvprint "Procedure 2 triggered: Found Sitter between " & timeb_startdt & " and " & timeb_enddt
            End If
        End If
    End If
    rs.Close
    
End Sub
Private Function ProcExists(pnum As Integer, startdt As Date, enddt As Date) As Boolean
    Dim sql As String
    Dim rs As New Recordset
    
    ProcExists = False
    
    sql = "select pe.procedure_event_id,pa.procedure_number from procedure_event as pe"
    sql = sql & " inner join procedure_answer as pa on (pe.procedure_event_id=pa.procedure_event_id)"
    sql = sql & " where encounter_id=" & m_pat.encounter_id & AND_UNIT
    sql = sql & " and pe.procedure_datetime=" & g_dbutil.SQL_DateTime(startdt)
    sql = sql & " and pe.departure_datetime=" & g_dbutil.SQL_DateTime(enddt)
'    dvprint sql
    rs.Open sql, g_cnADO

    If Not rs.EOF Then
        If IsNull(rs(0)) Or IsNull(rs(1)) Then
            ProcExists = False
        Else
            If rs(1) = pnum Then
                ProcExists = True
            End If
        End If
    End If
    rs.Close

End Function
Private Sub CheckProcs()
    Dim return_result As String
    Dim proc8, proc9 As Boolean

    On Error GoTo errHandler
    dvprint "---------------"
    dvprint "P2. 1-1 safety observation by non-RN"
    dvprint "---------------"

    ProcessProc1
'    reslist = "Initiated,Continued"
'    SetIndIfResultContains 13, "", "SITTER", "", "", reslist

'    ProcessProc 1, "OLAMBPROG8", "OLAMBPRO02", "OLAMBPR03"
'
'    dvprint "---------------"
'    dvprint "P2. Off unit accompanied by RN"
'    dvprint "---------------"
'
'    ProcessProc 2, "OLACUITY25", "OLACUITY30", "OLACUITY31"
'
'    dvprint "---------------"
'    dvprint "P3. Off unit accompanied by non-RN"
'    dvprint "---------------"
'
'    ProcessProc 3, "OLACUITY20", "OLACUITY32", "OLACUITY33"
'
'    dvprint "---------------"
'    dvprint "P4. Patient/family education by RN"
'    dvprint "---------------"
'
'    ProcessProc 4, "OLACUITY21", "OLACUITY45", "OLACUITY46"
'
'    dvprint "---------------"
'    dvprint "P5. Extensive wound management by RN"
'    dvprint "---------------"
'
'    ProcessProc 5, "OLACUITY22", "OLACUITY34", "OLACUITY35"
'
'    dvprint "---------------"
'    dvprint "P6. Extensive wound management by non-RN"
'    dvprint "---------------"
'
'    ProcessProc 6, "OLACUITY23", "OLACUITY36", "OLACUITY37"
'
'    dvprint "---------------"
'    dvprint "P7. Coordination of care by RN"
'    dvprint "---------------"
'
'    ProcessProc 7, "OLACUITY24", "OLACUITY43", "OLACUITY44"
'
'    dvprint "---------------"
'    dvprint "P8&P9. 1-1 or 2-1 by RN at bedside"
'    dvprint "---------------"
'
'    'olacuity59 and olacuity60 and ptunitmatchescode on both
'    proc8 = False
'    proc9 = False
'    If GetResult("", "OLACUITY59", "", "", return_result) Then
'        If PtUnitMatchesCode(return_result) Then
'            ProcessProc 8, "OLACUITY26", "OLACUITY38", "OLACUITY39"
'            If procs(numprocs).pnum = 8 Then proc8 = True
'            If GetResult("", "OLACUITY60", "", "", return_result) Then
'                If PtUnitMatchesCode(return_result) Then
'                    ProcessProc 9, "OLACUITY27", "OLACUITY41", "OLACUITY42"
'                    If procs(numprocs).pnum = 9 Then proc9 = True
'                End If
'            End If
'        End If
'    End If
'
'    If proc9 Then
'        If proc8 Then procs(numprocs - 1) = procs(numprocs)
'        With procs(numprocs)
'            .pindex = 0
'            .start = 0
'            .finish = 0
'            .isvalid = False
'            .pnum = 0
'        End With
'        numprocs = numprocs - 1
'    End If
'
''    dvprint "---------------"
''    dvprint "P8. 1-1 by RN at bedside"
''    dvprint "---------------"
''
''    'olacuity59 and ptunitmatchescode
''    If GetResult("", "OLACUITY59", "", "", return_result) Then
''        If PtUnitMatchesCode(return_result) Then
''            ProcessProc 8, "OLACUITY26", "OLACUITY38", "OLACUITY39"
''        End If
''    End If
'
'
    Exit Sub

errHandler:
    g_util.ThrowError "CheckProcs"
    Resume  'debug
End Sub
'Private Sub CheckOutcomes()
'    Dim return_result As String
'
'    'How to handle multiple outcomes per pt, each with different times?
'    dvprint "---------------"
'    dvprint "Outcomes: FALL"
'    dvprint "---------------"
'    If GetResult("", "FALL", "", "", return_result, SearchOutcomesRange) Then
'        numoutcomes = numoutcomes + 1
'        With oc(numoutcomes)
'            .checked = True
'            .pnum = 1
'            .start = return_result
'        End With
'    End If
'
'    dvprint "---------------"
'    dvprint "Outcomes: MED"
'    dvprint "---------------"
'    If GetResult("", "MED", "", "", return_result, SearchOutcomesRange) Then
'        numoutcomes = numoutcomes + 1
'        With oc(numoutcomes)
'            .checked = True
'            .pnum = 2
'            .start = return_result
'        End With
'    End If
'
'    dvprint "---------------"
'    dvprint "Outcomes: PROC"
'    dvprint "---------------"
'    If GetResult("", "PROC", "", "", return_result, SearchOutcomesRange) Then
'        numoutcomes = numoutcomes + 1
'        With oc(numoutcomes)
'            .checked = True
'            .pnum = 21
'            .start = return_result
'        End With
'    End If
'
'    Exit Sub
'
'errHandler:
'    g_util.ThrowError "CheckOutcomes"
'    Resume  'debug
'End Sub


'Private Sub AtLeastOneADL()
'    If Not (inds(1).checked Or inds(2).checked Or inds(3).checked) Then
'        'Note: Heather wants "#2 ADL - Assist" to be the default.  (90% of patients)
'        SetInd 2, "at least one ADL"
'    End If
'End Sub

Private Sub HighestIndicatorInEachGroupWins()
    Dim i As Integer
    Dim g As Integer
    Dim highest_is_on As Boolean
    Dim ind_list As String

    dvprint "---------------"
    dprint "Select highest indicator in each group", 1
    
    g = 0
    highest_is_on = False
    For i = MAX_INDS To 1 Step -1
        If (inds(i).group > 0) Then
            If (inds(i).group <> g) Then
                'this is a new group
                g = inds(i).group
                highest_is_on = inds(i).checked
            Else
                'same group
                If highest_is_on Then
                    inds(i).checked = False             'uncheck a lower number
                Else
                    highest_is_on = inds(i).checked     'save this one
                End If
            End If
        End If
    Next i
    
        For i = 1 To MAX_INDS
            If inds(i).checked Then ind_list = ind_list & "," & i
        Next i
        dprint "Final list = " & Mid$(ind_list, 2), 1

End Sub

Private Sub OutputClass()
    Dim outstr As String, ind_list As String
    Dim i As Integer
    Dim txarea As String
    Dim desc As String
'-debug -pulltime=1100 -efftime=1100 -range=960
'-debug -pulltime=1600 -efftime=1600 -range=540
'-debug -pulltime=2000 -efftime=2000 -range=660
    If is_test Then
        tc_event_id = 999
    Else
        tc_event_id = g_dbutil.NextGID()                                        'get a unique id for this class
    End If
    txarea = ""
'    If m_pat.unit_id = 108 Then 'surg/ortho/peds
'        If m_pat.age <= 16 Then txarea = "Pediatrics"
'    End If
        
    outstr = g_util.FixedWidth("1", 8)                                       '(facility code)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.unit_name, 16)
    outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(unit code)
    outstr = outstr & "|" & g_util.FixedWidth(txarea, 16)                       '(area code)
    outstr = outstr & "|" & g_util.FixedWidth("", 8)                        '(class date - give datetime instead)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.acct, 20)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.last_name, 32)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.first_name, 32)
    outstr = outstr & "|" & g_util.FixedWidth("", 32)                       '(middle)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.room, 8)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.bed, 4)
    If Format$(g_pull_finish, "hhnn") = "1100" Then
        outstr = outstr & "|" & Format$(m_pat.effective, "yyyymmdd") & "1100"      'class datetime
    Else
        outstr = outstr & "|" & Format$(m_pat.effective, "yyyymmddhhnn")      'class datetime
    End If
    outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(login)
    outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(employee)
    outstr = outstr & "|" & g_util.FixedWidth(CStr(m_pat.meth_id), 4)
    outstr = outstr & "|" & g_util.FixedWidth("C", 1)                       'record type = class
    outstr = outstr & "|" & g_util.FixedWidth("", 4)                        '(stage)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.TC_source_id, 4)        'TC source ID
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.range, 4)               'TC pull range
    outstr = outstr & "|" & g_util.FixedWidth(tc_event_id, 10)               'TC EVENT ID
    outstr = outstr & "|"
    outstr = g_util.FixedWidth(outstr, 294)
    outstr = outstr & "|" & Format$(m_pat.effective, "yyyymmddhhnn")        'IN
    outstr = g_util.FixedWidth(outstr, 377)
    outstr = outstr & "|"
    
    For i = 1 To MAX_INDS
        If (inds(i).checked) Then
            outstr = outstr & "Y"
            ind_list = ind_list & "," & i
        Else
            outstr = outstr & "N"
        End If
    Next i
    ind_list = Mid$(ind_list, InStr(1, ind_list, ",") + 1)                'strip leading comma
    
    Print #outfile, outstr

    If Not is_test Then
        desc = "Audit"
        g_event.AddTransparentMappingEventLogEntry desc, gLogUnitID, gLogEncounterID, _
            tc_event_id, MAPPER_VERSION, brief_audit, verbose_audit
    ' 6/6: new - make 0630 its own run.
    '    If Format$(g_pull_finish, "hhnn") = "0330" Then  'make a class for 7am also 6/5.
    '        Mid$(outstr, 304, 4) = "0700"
    '        Print #outfile, outstr
    '    End If
        
        AddLogEntry EVENT_TYPE_INFO, "Classified: " & ind_list, EVENT_CATEGORY_PROCESSED
    End If
End Sub

Private Sub OutputProcs()
    Dim i, j As Integer
    Dim s, f As Date
    Dim outstr As String, proc_list As String

    For i = 1 To numprocs
        procs(i).isvalid = True
    Next i
    For i = 1 To numprocs - 1
        If procs(i).isvalid Then
        For j = i + 1 To numprocs
            If procs(j).isvalid And procs(j).start = procs(i).start And procs(j).finish = procs(i).finish Then   'this can be combined.
                procs(j).isvalid = False
                procs(j).pindex = i
            End If
        Next j
        End If
    Next i

    For i = 1 To numprocs
        If procs(i).isvalid Then
        outstr = g_util.FixedWidth("", 8)                                       '(facility code)
        outstr = outstr & "|" & g_util.FixedWidth(m_pat.unit_name, 16)
        outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(unit code)
        outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(area code)
        outstr = outstr & "|" & g_util.FixedWidth("", 8)                        '(class date - give datetime instead)
        outstr = outstr & "|" & g_util.FixedWidth(m_pat.acct, 20)
        outstr = outstr & "|" & g_util.FixedWidth(m_pat.last_name, 32)
        outstr = outstr & "|" & g_util.FixedWidth(m_pat.first_name, 32)
        outstr = outstr & "|" & g_util.FixedWidth("", 32)                       '(middle)
        outstr = outstr & "|" & g_util.FixedWidth(m_pat.room, 8)
        outstr = outstr & "|" & g_util.FixedWidth(m_pat.bed, 4)
        outstr = outstr & "|" & Format$(procs(i).start, "yyyymmddhhnn")       '204 procdt
        outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(login)
        outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(employee)
        outstr = outstr & "|" & g_util.FixedWidth(CStr(m_pat.meth_id), 4)
        outstr = outstr & "|" & g_util.FixedWidth("P", 1)                       'record type = class
        outstr = outstr & "|"
        outstr = outstr & Space$(294 - Len(outstr))
        outstr = outstr & "|" & Format$(procs(i).start, "yyyymmddhhnn")       '296 procdt in
        outstr = outstr & Space$(346 - Len(outstr))
        If procs(i).finish = 0 Then
            outstr = outstr & "|" & Space$(12)
        Else
            outstr = outstr & "|" & Format$(procs(i).finish, "yyyymmddhhnn")      '348 procdt out
        End If
        outstr = g_util.FixedWidth(outstr, 377)
        outstr = outstr & "|NNNNNNNNN"
        Mid$(outstr, 378 + procs(i).pnum, 1) = "Y"
        proc_list = proc_list & "," & procs(i).pnum
        For j = i + 1 To numprocs
            If Not procs(j).isvalid And procs(j).pindex = i Then
                Mid$(outstr, 378 + procs(j).pnum, 1) = "Y"
                proc_list = proc_list & "," & procs(j).pnum
            End If
        Next j
        proc_list = Mid$(proc_list, InStr(1, proc_list, ",") + 1)          'strip leading comma

        Print #outfile, outstr
        If Not is_test Then
            AddLogEntry EVENT_TYPE_INFO, "Procedure: " & proc_list, EVENT_CATEGORY_PROCESSED
        End If
        End If 'isvalid
    Next i

End Sub
Private Sub OutputOutcomes()
'    Dim i, j As Integer
'    Dim s, f As Date
'    Dim outstr As String
'    Dim octime As String
'
'    If numoutcomes = 0 Then Exit Sub
'
'    For i = 1 To numoutcomes
'        If (oc(i).checked) Then
'            outstr = g_util.FixedWidth("", 8)
'            outstr = outstr & "|" & g_util.FixedWidth(m_pat.unit_name, 16)
'            outstr = outstr & "|" & g_util.FixedWidth("", 16)
'            outstr = outstr & "|" & g_util.FixedWidth("", 16)
'            outstr = outstr & "|" & g_util.FixedWidth(oc(i).start, 12)
'            outstr = outstr & "|" & g_util.FixedWidth(m_pat.acct, 20)
'            outstr = outstr & "|" & g_util.FixedWidth(oc(i).pnum, 3)
'            Print #outfile2, outstr 'Print line to outcomesindicator.TXT
'        End If
'    Next i
End Sub


Private Function ReturnQ1HrCount(codelist As String, reslist As String) As Integer
    Dim sql As String
    Dim rs As New Recordset
    Dim ct As Integer
    Dim done_ct As Integer
    Dim mintime As Date
    Dim maxtime As Date
    Dim done As Boolean
    
    ReturnQ1HrCount = 0

    sql = "select count(distinct(event_datetime)) from chart_item " & WhereBase & AndSimpleItemFilter("", codelist, "", "", "")
    sql = sql & AndResultContains(reslist) & " and event_datetime is not null"
    'dvprint sql
    rs.Open sql, g_cnADO
'    dvprint sql

    If Not rs.EOF Then
        If IsNull(rs(0)) Then
            ct = 0
            rs.Close
            Exit Function
        Else
            ct = rs(0)
            rs.Close
        End If
    Else
        ct = rs(0)
        rs.Close
        Exit Function
    End If
    
    ReturnQ1HrCount = ct
    
End Function
Private Function ReturnAssessCount(codelist As String) As Integer
    Dim sql As String
    Dim rs As New Recordset
    Dim ct As Integer
    Dim done_ct As Integer
    Dim mintime As Date
    Dim maxtime As Date
    Dim done As Boolean
    
    ReturnAssessCount = 0

    sql = "select count(distinct(event_datetime)) from chart_item " & WhereBase & AndSimpleItemFilter("", codelist, "", "", "")
    sql = sql & " and event_datetime is not null"
    'dvprint sql
    rs.Open sql, g_cnADO
'    dvprint sql

    If Not rs.EOF Then
        If IsNull(rs(0)) Then
            ct = 0
            rs.Close
            Exit Function
        Else
            ct = rs(0)
            rs.Close
        End If
    Else
        ct = rs(0)
        rs.Close
        Exit Function
    End If
    
    ReturnAssessCount = ct
    
End Function


'Private Function SumEventDateTime(code As String, res As String) As Date
'    Dim sql As String
'    Dim sql2 As String
'    Dim total As Integer
'    Dim rs As New Recordset
'    Dim rs2 As New Recordset
'
'    sql = "select event_datetime from chart_item " & WhereBase
'    sql = sql & " and code=" & g_dbutil.SQL_String(code) & " and result=" & g_dbutil.SQL_String(res) & " and event_datetime is not null"
'    rs.Open sql, g_cnADO
'
'    Do While Not rs.EOF
'        If Not IsNull(rs(0)) Then
'            sql2 = "select sum(result) from chart_item " & WhereBase
'            sql2 = sql & " and code=" & g_dbutil.SQL_String(code) & " and event_datetime=" & g_dbutil.SQL_Date(rs(0))
'            rs2.Open sql2, g_cnADO
'            If Not rs2.EOF Then
'                If Not IsNull(rs2(0)) Then
'                    If IsNumeric(rs2(0)) Then
'                        total = total + rs2(0)
'                    End If
'                End If
'            End If
'        End If
'        rs.MoveNext
'    Loop
'    rs2.Close
'    rs.Close
'    SumEventDateTime = total
'End Function


'Private Function ReturnMaxCount(codelist As String, reslist As String, ByRef numbuckets As Integer) As Integer 'needs quotes in lists!
'    Dim sql As String
'    Dim rs As New Recordset
'    Dim ct As Integer
'    Dim done_ct As Integer
'    Dim mintime As Date
'    Dim maxtime As Date
'    Dim done As Boolean
'    Dim i As Integer
'    Dim j As Integer
'    Dim totals(12) As Boolean
'    Dim bucket(24) As buckettype
'
'    ReturnMaxCount = 0
'    numbuckets = 0
'
'    'do an initial count to see if warrants continuing
'    sql = "select count(*) from chart_item " & WhereBase
'    sql = sql & " and code in (" & codelist & ")"
'    sql = sql & " and result in (" & reslist & ")"
'    sql = sql & " and event_datetime is not null"
'    rs.Open sql, g_cnADO
'    If Not rs.EOF Then
'        If rs(0) = 0 Then
'            rs.Close
'            Exit Function
'        End If
'    End If
'    rs.Close
'
'    'g_pull_start for g_range mins
'    'need to return number of hours between start and finish to get  hits/hour because start may be pt arrival time
'    '1. determine number of hours btwn start and finish.  sets the number of buckets
'    '2.
'    numbuckets = 24
'    For i = 1 To 24
'        bucket(i).start = DateAdd("h", i - 1, g_pull_start)
'        bucket(i).finish = DateAdd("n", 59, bucket(i).start)
'        If bucket(i).finish >= g_pull_finish Then
'            numbuckets = i 'may be less than 24 buckets
'            Exit For
'        End If
'    Next i
'
'    sql = "select "
'    For i = 1 To numbuckets
'        sql = sql & "sum(case when event_datetime between " & bucket(i).start & " and " & bucket(i).finish & " then 1 else 0 end) as range" & i & ","
'    Next i
'    sql = Mid$(sql, 1, Len(sql) - 1) 'remove comma
'    sql = sql & " from (select distinct(event_datetime) from chart_item " & WhereBase
'    sql = sql & " and code in (" & codelist & ")"
'    sql = sql & " and result in (" & reslist & ")"
'    sql = sql & " and event_datetime is not null) as d1"
'    rs.Open sql, g_cnADO
'
'    ct = 0
'    For i = 1 To numbuckets
'        bucket(i).count = rs(i - 1)
'        If bucket(i).count > 0 Then ct = ct + 1
'    Next i
'    rs.Close
'
'    ReturnMaxCount = ct
'
'    'do an initial count to see if warrants continuing; set the mintime of all codes
''    sql = "select sum(case when event_datetime between CONVERT(VARCHAR(10),GETDATE(),111) + ' 06:00' and CONVERT(VARCHAR(10),GETDATE(),111) + ' 06:59' then 1 else 0 end) as range24,"
''    sql = sql & "sum(case when event_datetime between CONVERT(VARCHAR(10),GETDATE(),111) + ' 05:00' and CONVERT(VARCHAR(10),GETDATE(),111) + ' 05:59' then 1 else 0 end) as range23,"
'
'End Function

Private Sub CheckDressing()
    Dim sql As String
    Dim rs As New Recordset

'    codelist = LIKE_PREFIX & "DRSGINTERVENTION"
'    If ResultContains("", codelist, "", "", "", , False, found_what) Then
'        dvprint found_what
'        If InStr(1, found_what, "peripheral iv", vbTextCompare) = 0 Then
'            SetInd 21, found_what
'        End If
'    End If
'
'    codelist = LIKE_PREFIX & "DRSGSTATUS"
'    If ResultContains("", codelist, "", "", "", , False, found_what) Then
'        dvprint found_what
'        If InStr(1, found_what, "peripheral iv", vbTextCompare) = 0 And _
'            InStr(1, found_what, "hemodialysis", vbTextCompare) = 0 Then     'added 4/5 exclude hemodialysis
'            SetInd 21, found_what
'        End If
'    End If
    
    sql = "select code,description from chart_item " & WhereBase
    sql = sql & " and (code = 'DRSGINTERVENTION')"
    sql = sql & " and (description like '%LDA%lumen%'"
    sql = sql & " or description like '%chamber port%'"
    sql = sql & " or description like '%LDA art line%'"
    sql = sql & " or description like '%LDA hemodialysis cath'"
    sql = sql & " or description like '%LDA intraosseous line%'"
    sql = sql & " or description like '%LDA introducer%'"
    sql = sql & " or description like '%LDA neuraxial catheter%'"
    sql = sql & " or description like '%LDA pa catheter%'"
    sql = sql & " or description like '%LDA paravertebral catheter%'"
    sql = sql & " or description like '%LDA perineural catheter%'"
    sql = sql & " or description like '%LDA neuraxial catheter%'"
    sql = sql & " or description like '%LDA SH IP%'"
    sql = sql & " or description like '%LDA sub q catheter%'"
    sql = sql & " or description like '%LDA transthoracic line%'"
    sql = sql & " or description like '%LDA umbilical artery cath%'"
    sql = sql & " or description like '%OB LDA epidural catheter%')"
    rs.Open sql, g_cnADO
    If rs.EOF Then
        rs.Close
        Exit Sub
    Else
        SetInd 21, "Found " & rs(0) & " with " & rs(1)
        rs.Close
    End If

End Sub

Private Sub CheckSkinIntegrity()
    Dim sql As String
    Dim rs As New Recordset

    sql = "select code,description from chart_item " & WhereBase
    sql = sql & " and code = 'SKININTEGRITY'"
    sql = sql & " and result not like '%intact%'"
    sql = sql & " and (result like '%Abrasion%'"
    sql = sql & " or result like '%Burn%'"
    sql = sql & " or result like '%Denuded%'"
    sql = sql & " or result like '%Electrode mark%'"
    sql = sql & " or result like '%Erythema%'"
    sql = sql & " or result like '%Excoriation%'"
    sql = sql & " or result like '%Fragile%'"
    sql = sql & " or result like '%Hematoma%'"
    sql = sql & " or result like '%Indurated%'"
    sql = sql & " or result like '%Laceration%'"
    sql = sql & " or result like '%Lesion%'"
    sql = sql & " or result like '%Macerated%'"
    sql = sql & " or result like '%Petechia%'"
    sql = sql & " or result like '%Rash%'"
    sql = sql & " or result like '%Redness%'"
    sql = sql & " or result like '%Tear%'"
    sql = sql & " or result like '%Weeping%')"
    rs.Open sql, g_cnADO
    If rs.EOF Then
        rs.Close
        Exit Sub
    Else
        SetInd 21, "Found (without intact) " & rs(0) & " with " & rs(1)
        rs.Close
    End If

End Sub

Private Sub CheckNICUSkinIntegrity()
    Dim sql As String
    Dim rs As New Recordset

    sql = "select code,description from chart_item " & WhereBase
    sql = sql & " and code = 'NEWBORNSKININTEGRITY'"
    sql = sql & " and result not like '%intact%'"
    sql = sql & " and (result like '%Abrasion%'"
    sql = sql & " or result like '%elastic turgor%'"
    sql = sql & " or result like '%melanocytic nevus%'"
    sql = sql & " or result like '%stork bite%'"
    sql = sql & " or result like '%Blister%'"
    sql = sql & " or result like '%Electrode mark%'"
    sql = sql & " or result like '%Excoriation%'"
    sql = sql & " or result like '%Forceps mark%'"
    sql = sql & " or result like '%Hemorrhoids%'"
    sql = sql & " or result like '%Infiltration%'"
    sql = sql & " or result like '%Laceration%'"
    sql = sql & " or result like '%Newborn Rash%'"
    sql = sql & " or result like '%Petechia%'"
    sql = sql & " or result like '%Rash%'"
    sql = sql & " or result like '%Redness%'"
    sql = sql & " or result like '%Swelling%'"
    sql = sql & " or result like '%Tear%'"
    sql = sql & " or result like '%Other (comment)%'"
    sql = sql & " or result like '%Weeping%')"
    rs.Open sql, g_cnADO
    If rs.EOF Then
        rs.Close
        Exit Sub
    Else
        SetInd 21, "Found (without intact) " & rs(0) & " with " & rs(1)
        rs.Close
    End If

End Sub

Private Function AssessSQL() As String
    Dim i As Integer
    Dim s As String
    
    s = ""
    For i = 1 To num_buckets
        s = s & "sum(case when event_datetime between " & g_dbutil.SQL_DateTime(bucket(i).startdt) & " and " & g_dbutil.SQL_DateTime(bucket(i).enddt) & " then 1 else 0 end) as range" & num_buckets - i + 1 & ","
    Next i
    
    s = Mid$(s, 1, Len(s) - 1)  'remove last comma
    AssessSQL = s

End Function
Private Function ReturnVSCount(min_to_trigger As Integer) As Integer
    Dim sql As String
    Dim rs As New Recordset
    Dim ct As Integer
    Dim done_ct As Integer
    Dim bucketout(24) As Integer
    Dim i As Integer, j As Integer
    Dim s As String
    Dim codelist As String
        
        
    ReturnVSCount = 0
    
    codelist = "CORETEMP,TEMPERATURE"
    
    codelist = codelist & "RESPIRATION,SPO22PREDUCTAL"
    
    codelist = codelist & "HEARTRATE,BLOODPRESSURE2,HEARTRATEMONITOR,BP,MAP2,BLOODPRESSURE3,BLOODPRESSURE4,OBFHRDOPPLER,"
    codelist = codelist & "ARTLINEBP,ARTLINEBP2,ARTLINEBP3,IABFREQUENCY,SWANGANZ,CARDRHYTHM"
        
    sql = "select count(distinct(event_datetime)) from chart_item " & WhereBase & AndSimpleItemFilter("", codelist, "", "", "")
    sql = sql & " and event_datetime is not null"
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        If rs(0) = 0 Then
            rs.Close
            dvprint "No documentation found for VS."
            Exit Function
        End If
    End If
    rs.Close

    s = "VS hourly counts for 1:1 Phys Interv: "
    sql = "select " & AssessSQL
    sql = sql & " from (select distinct(event_datetime) from chart_item " & WhereBase & AndSimpleItemFilter("", codelist, "", "", "") & ") as ci1"
dvprint sql
    rs.Open sql, g_cnADO

    If Not rs.EOF Then
        For i = 0 To num_buckets - 1  'zero based for rs
            If IsNull(rs(i)) Then
                bucket(i + 1).count = 0
            Else
                bucket(i + 1).count = rs(i)
            End If
            s = s & bucket(i + 1).count & ","
        Next
    Else
        dvprint "No documentation found for VS."
        rs.Close
        Exit Function
    End If
dvprint s
    
    rs.Close
    
'    For i = 0 To num_buckets - 1
'        listout = listout & bucketout(i)
'    Next i
'    dvprint "VS=" & listout
    done_ct = 0
    For i = 1 To num_buckets - 1 ' less 1 for 2-hr span
        ct = 0
        For j = 0 To 1
            ct = ct + bucket(i + j).count
        Next j
        If ct > done_ct Then done_ct = ct
        If ct >= min_to_trigger Then Exit For
    Next i

    ReturnVSCount = done_ct
    
End Function
Private Function IsICU() As Boolean
    Dim sql As String
    Dim rs As New Recordset

    IsICU = False
    sql = "select is_icu from unit where unit_id=" & m_pat.unit_id
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        If IsNull(rs(0)) Then
        ElseIf rs(0) = "" Or rs(0) = "N" Then
        ElseIf rs(0) = "Y" Then IsICU = True
        End If
    End If
    rs.Close
End Function

Private Function IsAssessUnit() As Boolean
'These are the units at Shands to put the assessments into a bucket where there is only a count of 1 q 30 minutes,
'regardless of the number of assessments completed - so all pain assessments in a 30 minute time frame = 1,
'all pulmonary assessments in a 30 minute time frame, all cardiac assessments in a 30 minute time frame - and so on!
'
'55, 54, 65, 64, 75, 74, 95nt,95,35,44,45,42,6w,6e,5w,8e, 11ms, 10-5
'155,115,100,170,190,130,7197814, 95? , 35? ,7197814,7112825,7112819, 7112816 , 220 ,7112826,7112756,2511461,7112818
    Select Case m_pat.unit_id
        Case 155, 115, 100, 170, 190, 130, 380, 350, 14047404, 7197814, 7197814, 7112825, 7112819, 7112816, 220, 7112826, 7112756, 2511461, 7112818
            IsAssessUnit = True
        Case Else
            IsAssessUnit = False
    End Select
    
        
'name             unit_id
'---------------- -----------
'SICU 1
'65 MS 100
'54 MS 115
'74 MS 130
'55 MS 155
'64 MS 170
'75 MS 190
'UA6E 220
'MICU 235
'25 IC 250
'NICU 2           265
'NICU 3           280
'BICU 305
'BMTU 350
'94 IMC 365
'MBU 380
'4 PEDS 395
'PICU 410
'52 PY 435
'Vista South      450
'old Vista West   480
'old Vista East   495
'Other 496
'MSS AGH          497
'Rehab Nursing    514682
'oldVista North   515047
'MSO AGH          531947
'MS AGH           531962
'CARD AGH         531982
'SACC AGH         532002
'IMC AGH          532017
'SICU AGH         532032
'AICU AGH         532047
'MBU AGH          532062
'NICU AGH         532077
'Vista West       532642
'Vista East       532657
'Vista North      532672
'Trauma 1504622
'TEST AGH         1698032
'Other 1752602
'11 MS 2511461
'10 SI 3232907
'45 MS 3232908
'PEDS 3355356
'PEDS 3355376
'PEDs 7           3355391
'PICU 3355406
'LD AGH           4501805
'Test MBU @Shands 4504840
'Test LD Shands A 4504842
'82 NS 5216839
'CARD 5 AGH       5278326
'L&D              6576074
'8E               7112756
'44 PEDS 7112757
'4E               7112758
'4 W 7112759
'5E               7112815
'6 W 7112816
'11 5             7112817
'10 5             7112818
'42 PEDS 7112819
'45 PEDS 7112825
'5 W 7112826
'24 IC 7112827
'NT95 7197814
'Transparent 12546789
'32 MS 14047404


End Function

Public Sub SetupBuckets(bucketsize As Integer)
    Dim i As Integer
'b1 = pull_start to +70
'b2=  b1 end
' 700 - 1022  range = 3h 22min = 202mins   202/70=2 -> 3 buckets
' 700:700+70=810, 810:810+70=920,  920:finish
    num_buckets = g_range \ bucketsize
    If (g_range Mod bucketsize) <> 0 Then num_buckets = num_buckets + 1
    For i = 1 To num_buckets
        bucket(i).startdt = DateAdd("n", (i - 1) * bucketsize, m_pat.pull_start)
        bucket(i).count = 0
    Next i
    For i = 1 To num_buckets - 1
        bucket(i).enddt = bucket(i + 1).startdt
    Next i
    bucket(num_buckets).enddt = m_pat.pull_finish


End Sub

Private Sub PutIntoBuckets(codelist As String)
    Dim sql As String, and_filter As String
    Dim rs As New Recordset
    Dim count As Long, i As Integer
    Dim pos As Integer
    Dim s As String
    
    s = ""
    For i = 1 To num_buckets
        s = s & "sum(case when event_datetime>=" & g_dbutil.SQL_DateTime(bucket(i).startdt) & " and "
        If i < num_buckets Then
            s = s & "event_datetime<" & g_dbutil.SQL_DateTime(bucket(i).enddt) & " then 1 else 0 end) as range" & i & ","
        Else
            s = s & "event_datetime<=" & g_dbutil.SQL_DateTime(bucket(i).enddt) & " then 1 else 0 end) as range" & i & ","
        End If
    Next i
    s = Mid$(s, 1, Len(s) - 1)  'remove last comma

    sql = "select " & s
    sql = sql & " from (select distinct(event_datetime) from chart_item " & WhereBase() & AndSimpleItemFilter("", codelist, "", "", "") & ") as Q"
    If is_test Then dvprint sql
    rs.Open sql, g_cnADO
    If Not IsNull(rs(0)) Then
        For i = 1 To num_buckets
            bucket(i).count = bucket(i).count + rs(i - 1)
        Next i
    End If
        
    rs.Close

End Sub
Public Sub PrintBucketFreq(desc As String)
    Dim i As Integer
    Dim s As String
    
    s = desc
    For i = 1 To num_buckets
        s = s & bucket(i).count & ", "
    Next i
    dvprint s

End Sub

Public Function CountBuckets() As Integer
    Dim i As Integer, ct As Integer
    
    ct = 0
    For i = 1 To num_buckets
        If bucket(i).count > 0 Then ct = ct + 1
    Next i
    CountBuckets = ct

End Function
Public Sub ClearBuckets()
    Dim i As Integer
    
    For i = 1 To num_buckets
        bucket(i).count = 0
    Next i

End Sub

