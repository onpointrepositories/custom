﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;                // for SqlConnection
using System.IO;                            // for Path
using PfsShared;
//
// Data Reader for Transparent Classification
// This loads a text file into the chart_item table.
// Patients must already exist.
//
namespace DataReader
{
    class DataReader
    {
        // Info about the patient being processed
        const string DATETIME_FORMAT = "yyyyMMddHHmm";              // ISO Date/Time w/o seconds
        const string DATE_FORMAT = "yyyyMMdd";              // ISO Date/Time w/o seconds
        string _acct_number;
        //string      _unit_name;
        int _encounter_id = 0;
        int _unit_id = 0;
        DateTime _start = DateTime.Now;
        DateTime _finish = DateTime.Now;

        // Process one text file.
        // Returns true if the file has been processed and it is OK to rename.
        //
        public bool process(string pathname)
        {
            StreamReader infile;
            string line;
            string prev_acct_num = "Starting account num";
            //int line_num = 0;
            string filename = Path.GetFileName(pathname);
            bool skip_pt = false;
            DateTime perf_time;

            short sequence = 0;

            var pfs = PFSUtility.NewPfsDataContext();

            try
            {
                infile = new StreamReader(pathname);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error opening file {0}", pathname);
                Console.WriteLine(e.Message);
                Program.LogValidationError(e.Message, pathname);
                return false;
            }

            DeleteExistingBorderItems();

            //00122834969|5/6/13  8:50 AM|iohexol (OMNIPAQUE) 300 MG/ML injection 150 mL||Lexi-Comp 0A
            //00122987140|5/6/13  8:27 AM0D,5/6/13  8:30 AM0D,5/6/13  8:37 AM|FENTanyl (SUBLIMAZE) injection|0|Lexi-Comp 0A

            while ((line = infile.ReadLine()) != null)
            {
                Program.DebugTrace(line);                   // only in debug mode
//                var arr = line.Split((char)10); //split into lines with delim=0A hex
                // need to sort array into acct number order.
                //Array.Sort (arr);
                var rec_items = line.Split('^'); // split each line on carat
                _acct_number = rec_items[1].Trim();
                if (_acct_number != prev_acct_num)
                { //acct
                    if (prev_acct_num != "Starting account num")
                    {
                        SubmitPatientChartItems(pfs);
                    }
                    sequence = 0;
                    skip_pt = false;
                    prev_acct_num = _acct_number;
                    var encounter_lookup =
                        from enc in pfs.ENCOUNTERs
                        where enc.ACCT_NUMBER == _acct_number
                        select new
                        {
                            enc.ENCOUNTER_ID
                        };
                    if (encounter_lookup.Count() == 0)
                    {
                        Program.LogValidationError("Account number not found: " + _acct_number, filename);
                        skip_pt = true;
                        _encounter_id = 0;
                    }

                    if (!skip_pt)
                    { //Unit
                        var enc_found = encounter_lookup.First();
                        _encounter_id = enc_found.ENCOUNTER_ID;
                        Program.DebugTrace("Encounter ID = {0}", _encounter_id);
                        // Search for unit id
                        // Then look for the unit name
                        var encloc_lookup =
                            from encloc in pfs.ENCOUNTER_LOCATIONs
                            where encloc.ENCOUNTER_ID == _encounter_id
                            orderby encloc.EFFECTIVE_DATETIME_OUT descending
                            select new
                            {
                                encloc.WORKING_UNIT_ID
                            };
                        if (encloc_lookup.Count() == 0)
                        {
                            //Program.LogValidationError("Unit not found: " + _unit_name, filename);
                            skip_pt = true;
                            _unit_id = 0;
                            //return false;
                        }
                        if (!skip_pt)
                        {
                            var encloc_found = encloc_lookup.First();
                            _unit_id = (int)encloc_found.WORKING_UNIT_ID;
                        }
                    } //Unit
                } //acct
//"Times","CSN","Display Name","Dual-Sign Medication Order","Frequency","Ordered Route",
// 0       1     2               3                             4                 5
//"Irritant/Vesicant","Interpreter Needed?"
//   6                     7

                if (!skip_pt)
                {
//                    Program.DebugTrace("ub = {0}", rec_items.GetUpperBound(0));
                    //Program.DebugTrace("desc = {0}", desc);
//                    Program.DebugTrace("times = {0}", rec_items[0]);
                    string res = rec_items[7].Trim();//RESULT=Interpreter
                    string code = "MEDFILE";
                    string cat = rec_items[3].Trim(); //CATEGORY
                    string ord_tmg = rec_items[4].Trim(); // ORDER_TIMING
                    string field = rec_items[5].Trim(); // FIELD_NAME
                    string desc = rec_items[2].Trim(); // DESCRIPTION
                    string ord_stat = "";
                    if (rec_items[6].Trim().ToLower() == "true") ord_stat = "Y";
                    var arr_ev_times = rec_items[0].Split('|'); // split each time on pipe
                    foreach (string ev_time in arr_ev_times)
                    {
                        var med_time = ev_time;
                        med_time = med_time.Trim(); //trim of control char 0Dhex
                        if (med_time != "")
                        {
                            int mlen = med_time.Length;
                            med_time = med_time.Substring(0, mlen - 2) + ":" + med_time.Substring(mlen - 2, 2);
                            Console.WriteLine("Parsed {0}", med_time);
                            if (DateTime.TryParse(med_time, out perf_time))
                            {

                                // get latest location for unit id
                                var ci = NewChartItem(code, sequence, cat, desc, res, field, ord_tmg, ord_stat, perf_time);
                                pfs.CHART_ITEMs.InsertOnSubmit(ci);

                                if (sequence == 0)
                                {
                                    _start = perf_time;
                                    _finish = perf_time;
                                }
                                else
                                {
                                    // note: items may be in random order
                                    if (perf_time < _start) _start = perf_time;
                                    if (perf_time > _finish) _finish = perf_time;
                                }
                                sequence++;
                            }
                            else
                            {
                                Console.WriteLine("Unable to parse {0}", med_time);
                            }
                        }
                    }
                }
                sequence++;
            } //while

            infile.Close();
            SubmitPatientChartItems(pfs);

            return true;
        }

        private void DeleteExistingBorderItems()
        {
            //
            // Delete existing MEDFILE chart items for this encounter bewteen the timestamps
            //
            string str_dt = _start.ToString(DATETIME_FORMAT); // yyyymmddhhnn
            string str_0700dt = _start.ToString(DATE_FORMAT) + "0200"; // yyyymmdd
            string str_1900dt = _start.ToString(DATE_FORMAT) + "1400"; // yyyymmdd
            string str_ev_dt;
            DateTime ev_dt;
            Program.DebugTrace(str_dt);
            Program.DebugTrace(str_1900dt);
            int result = str_dt.CompareTo(str_1900dt);
            if (result > 0)
            {
                Program.DebugTrace("compare is greater");
                //then current time is greater than 1900 = delete todays 0700
                str_ev_dt = str_0700dt.Substring(4, 2) + "/" + str_0700dt.Substring(6, 2) + "/" + str_0700dt.Substring(0, 4) + " 02:00";
            }
            else
            {
                Program.DebugTrace("compare is Less");
                //then current time is less than 1900 = delete yesterday 1900
                ev_dt = _start.AddDays(-1);
                str_dt = ev_dt.ToString(DATE_FORMAT); // yyyymmdd
                str_1900dt = str_dt + "1400"; // yyyymmddhhnn
                str_ev_dt = str_1900dt.Substring(4, 2) + "/" + str_1900dt.Substring(6, 2) + "/" + str_1900dt.Substring(0, 4) + " 14:00";
            }


            Program.DebugTrace("Delete existing...");
            using (var cn = PFSUtility.NewSqlConnection())
            {
                string sql = "delete from chart_item where " +
                    " code='MEDFILE'" +
                    " and event_datetime = '" + str_ev_dt + "'";
                Program.DebugTrace(sql);
                var cmd = new SqlCommand(sql, cn);
                cmd.ExecuteNonQuery();
            }

        }

        private void SubmitPatientChartItems(PfsDataContext pfs)
        {
            if (_encounter_id == 0) return;
            if (_unit_id == 0) return;


            // Now save the new chart items
            pfs.SubmitChanges();
            Console.WriteLine("Processed acct {0}", _acct_number);
        }

        private CHART_ITEM NewChartItem(string code, short sequence, string cat, string desc, string res, string field, string ord_tmg, string ord_stat, DateTime perf_time)
        {
            var result = new CHART_ITEM();

            result.EVENT_DATETIME = perf_time;
            result.ENCOUNTER_ID = _encounter_id;
            result.UNIT_ID = _unit_id;
            result.SEQUENCE = sequence;                     // needed in case of duplicate codes
            result.CODE = code;
            result.DESCRIPTION = desc;
            result.CATEGORY = cat;
            result.FIELD_NAME = field;  //freq
            result.RESULT = res;
            result.ORDER_TIMING = ord_tmg;
            result.ORDER_STATUS = ord_stat;
            // We should reach out and get the server time, but this should be running on the server anyway
            result.TIMESTAMP = DateTime.Now;                        // ** should use server time

            return result;
        }

    }
}
