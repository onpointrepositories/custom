﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Data.SqlClient;                // for SqlConnection
using System.IO;                            // for Path
using PfsShared;

//
// Data Reader for Transparent Classification
// This is a console program, so no message boxes!
//

namespace DataReader
{
    static class Program
    {
        const int MAX_FILE_LIFE_IN_DAYS = 30;

        public static bool _debug;

        static string _path;
        static string _file = "MEDTCDATA.TXT";
        static bool _norename;
        static bool _nodelete;
        static bool _pause;

        static void Main(string[] args)
        {
            try
            {
                ParseCommandLine(args);
                ProcessFiles();
                //DeleteOldFiles();
            }
            catch(Exception e)
            {
                Console.WriteLine("Unexpected Error: {0}", e.Message);
                Console.WriteLine("{0}", e.StackTrace);
                LogUnexpectedError(e.Message, e.StackTrace);
            }

            if (_debug || _pause)
            {
                Console.WriteLine("\n");
                if (_pause) Console.Write("Press any key...");
                if (_pause) Console.ReadKey();   // only in "pause" mode
            }
        }

        static void ParseCommandLine(string[] args)
        {
            _path = @"..\load_me";
            //_path = @".";

            foreach (var arg in args)
            {
                var arr = arg.Split('=');
                
                switch(arr[0])
                {
                    case "-debug":
                        _debug = true;
                        break;
                    case "-file":
                        _file = arr[1];
                        break;
                    case "-nodelete":
                        _nodelete = true;
                        break;
                    case "-norename":
                        _norename = true;
                        break;
                    case "-path":
                        _path = arr[1];
                        break;
                    case "-pause":
                        _pause = true;
                        break;
                    default:
                        if (arr[0].Left(1) != "-")
                        {
                            _path = arr[0];                 // assume it is a path
                        }
                        else
                        {
                            Console.WriteLine("unexpected argument: {0}", arg);
                        }
                        break;
                }
            }
        }

        static void ProcessFiles()
        {
            if (!string.IsNullOrEmpty(_file))
            {
                Console.WriteLine("ProcessA");
                ReplaceWhiteSpace(Path.Combine(_path, _file));
                ProcessFile(Path.Combine(_path, _file));
                LogInfo("Processed ", _file);
            }
            else
            {
                Console.WriteLine("ProcessB");
                DebugTrace("Look for files...");
                string[] files = Directory.GetFiles(_path, _file);       // QuadramedAm.20160114
                foreach (var file in files)
                {
                    ReplaceWhiteSpace(file);
                    ProcessFile(file);
                    LogInfo("Processed ", file);
                }
            }
        }

        static void ReplaceWhiteSpace(string pathname)
        {
            Console.WriteLine("Replacing white spaces {0}", pathname);

            //System.IO.File.WriteAllLines(
            //        Path.Combine(_path, "MEDS.TXT"),
            //        System.IO.File.ReadAllLines(pathname).Select(line =>
            //            System.Text.RegularExpression.Regex.Replace(line, @"\s+", " ")
            //        )
            //        ).ToArray();
            File.WriteAllText(pathname,
                File.ReadAllText(pathname).Replace("\"\x0A", "\r\n"));
            File.WriteAllText(pathname,
                File.ReadAllText(pathname).Replace("\x4D\x0A", "M|"));
            File.WriteAllText(pathname,
                File.ReadAllText(pathname).Replace("\x0A", "|"));
            File.WriteAllText(pathname,
                File.ReadAllText(pathname).Replace("\",\"", "\"^\""));
            File.WriteAllText(pathname,
                File.ReadAllText(pathname).Replace(",,,,,", "^^^^^"));
            File.WriteAllText(pathname,
                File.ReadAllText(pathname).Replace(",,,,", "^^^^"));
            File.WriteAllText(pathname,
                File.ReadAllText(pathname).Replace(",,,", "^^^"));
            File.WriteAllText(pathname,
                File.ReadAllText(pathname).Replace(",,", "^^"));
            File.WriteAllText(pathname,
                File.ReadAllText(pathname).Replace("\"", ""));
            File.WriteAllText(pathname,
                File.ReadAllText(pathname).Replace("'", "."));
            File.WriteAllText(pathname,
                File.ReadAllText(pathname).Replace("\x2C\x0D\x0A", "^\r\n"));
            //File.WriteAllText(pathname,
            //    File.ReadAllText(pathname).Replace("\"", ""));
            //File.WriteAllText(pathname,
            //     File.ReadAllText(pathname).Replace("\"\x0D\x0A", "\"\r\n"));

            // This bit of code sorts the file by account number
            // where the acct num is the second element in the carat-delimited file.
            // Achieved by using LINQ query!!
            string[] lines = File.ReadAllLines(pathname);
            IEnumerable<string> query =
                from line in lines
                let x = line.Split('^')
                orderby x[1]
                select line;
            File.WriteAllLines(pathname, query.ToArray());

            File.WriteAllText(pathname,
                File.ReadAllText(pathname).Replace("\x2C\x0D\x0A", "^\r\n"));
            //            Array.Sort(lines);
//            File.WriteAllLines(pathname, lines);

        }


        static void ProcessFile(string pathname)
        {
            var reader = new DataReader();

            Console.WriteLine("Processing {0}", pathname);
            if (reader.process(pathname)) {

                string newname = string.Format(@"{0}\{1}_{2:yyyyMMddHHmmss}.TXT",
                    Path.GetDirectoryName(pathname),
                    Path.GetFileNameWithoutExtension(pathname),
                    DateTime.Now);

                if (_norename) {
                    DebugTrace("Would rename to {0}", newname);
                } else {
                    // rename the processed file
                    DebugTrace("Rename to {0}", newname);
                    File.Delete(newname);               // in case it already exists (no error if not)
                    File.Move(pathname, newname);       // rename
                }
            }
        }

        static void DeleteTheseOldFiles(string pattern)
        {
            string[] files = Directory.GetFiles(_path, pattern);

            foreach (string file in files)
            {
                FileInfo fi = new FileInfo(file);
                if (fi.LastWriteTime < DateTime.Now.AddDays(-MAX_FILE_LIFE_IN_DAYS))
                {
                    if (_nodelete)
                        DebugTrace("Would delete {0}", file);
                    else
                    {
                        DebugTrace("Delete {0}", file);
                        fi.Delete();
                    }
			    }
            }
        }
        
        static void DeleteOldFiles()
        {
            DebugTrace("delete old files...");
            string path = "c:\\program files (x86)\\quadramed\\acuityplus\\load_me\\";
            DeleteTheseOldFiles(path + "quadramedam*.*");
            DeleteTheseOldFiles(path + "quadramedpm*.*");
        }
        
        public static void DebugTrace(string format, params object[] values)
        {
            if (_debug)
            {
                Console.WriteLine(format, values);
            }
        }

        public static void LogInfo(string msg, string source)
        {
            PFSEventLog.AddEventLogEntry(
                PFSEventLog.EventLogSource.EVENT_SOURCE_TRANSPARENT_MAPPING,
                PFSEventLog.EventLogType.EVENT_TYPE_INFO,
                PFSEventLog.EventLogCategory.EVENT_CATEGORY_PROCESSED,
                msg, source, 0, 0, 0);
        }

        public static void LogValidationError(string msg, string source)
        {
            Console.WriteLine(msg);
            Console.WriteLine("The next call is to AddEventLog");
            PFSEventLog.AddEventLogEntry(
                PFSEventLog.EventLogSource.EVENT_SOURCE_TRANSPARENT_MAPPING,
                PFSEventLog.EventLogType.EVENT_TYPE_ERROR,
                PFSEventLog.EventLogCategory.EVENT_CATEGORY_VALIDATION,
                msg, source, 0, 0, 0);
        }

        public static void LogUnexpectedError(string msg, string source)
        {
            Console.WriteLine(msg);
            PFSEventLog.AddEventLogEntry(
                PFSEventLog.EventLogSource.EVENT_SOURCE_TRANSPARENT_MAPPING,
                PFSEventLog.EventLogType.EVENT_TYPE_ERROR,
                PFSEventLog.EventLogCategory.EVENT_CATEGORY_UNEXPECTED,
                msg, source, 0, 0, 0);
        }
    }
}
