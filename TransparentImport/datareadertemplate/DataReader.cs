﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;                // for SqlConnection
using System.IO;                            // for Path
using PfsShared;
//
// Data Reader for Transparent Classification
// This loads a text file into the chart_item table.
// Patients must already exist.
//
namespace DataReader
{
    class DataReader
    {      
        // Info about the patient being processed
        string      _acct_number;
        string      _unit_name;
        int         _encounter_id = 0; 
        int         _unit_id = 0;
        DateTime    _start = DateTime.Now;
        DateTime    _finish = DateTime.Now;

        // Process one text file.
        // Returns true if the file has been processed and it is OK to rename.
        //
        public bool process(string pathname)
        {
            StreamReader infile;
            string line;
            int line_num = 0;
            string filename = Path.GetFileName(pathname);
            bool skip_pt = false;

            short sequence = 0;

            var pfs = PFSUtility.NewPfsDataContext();

            try
            {
                infile = new StreamReader(pathname);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error opening file {0}", pathname);
                Console.WriteLine(e.Message);
                Program.LogValidationError(e.Message, pathname);
                return false;
            }
            
            //         1         2         3         4         5         6         7         8
            //12345678901234567890123456789012345678901234567890123456789012345678901234567890
            //0980013384105       EC2       EC08 03   SMITH, JOHN
            //    DCPGENERICCODE                         92663           04/15/14 12:12:00 Neurological
            //    DCPGENERICCODE                         92663           04/15/14 12:12:00 EC Note
            //    DCPGENERICCODE                         92663           04/15/14 12:12:00 Psycho/Social
            //    DCPGENERICCODE                         92663           04/15/14 12:12:00 Physical Assessment

            while ((line = infile.ReadLine()) != null) {
                line_num++;
                if (line.Length > 0  &&  !Char.IsControl(line[0])) {
                    
                    Program.DebugTrace(line);                   // only in debug mode
                    // Pad the line so we don't have to check the length before every Substring
                    line = line.PadRight(78);
                    
                    if (line.Left(4) != "    ") {
                        // Header line
                        skip_pt = false;
                        // Finish previous patient
                        SubmitPatientChartItems(pfs);

                        // reset for this patient
                        _acct_number = line.Left(20).Trim();
                        _unit_name = line.Substring(21-1, 10).Trim();    // note: Substring is zero based
                        sequence = 0;
                        
                        // Search for acct# in database
                        //
                        var encounter_lookup =
                            from enc in pfs.ENCOUNTERs
                            where enc.ACCT_NUMBER == _acct_number
                            select new {
                                enc.ENCOUNTER_ID
                            };
                        if (encounter_lookup.Count() == 0) {
                            Program.LogValidationError("Account number not found: " + _acct_number, filename);
                            skip_pt = true;
                            _encounter_id = 0;
                            //return false;
                        }
                        if (!skip_pt)
                        {
                            var enc_found = encounter_lookup.First();
                            _encounter_id = enc_found.ENCOUNTER_ID;
                            Program.DebugTrace("Encounter ID = {0}", _encounter_id);

                            // Search for unit id
                            //
                            // First look for a unit alias name
                            var alias_lookup =
                                from alias in pfs.UNIT_ALIAS
                                where alias.UNIT_ALIAS_NAME == _unit_name
                                select new
                                {
                                    alias.UNIT_ID
                                };
                            if (alias_lookup.Count() > 0)
                            {
                                var alias_found = alias_lookup.First();
                                _unit_id = alias_found.UNIT_ID;
                            }
                            else
                            {
                                // Then look for the unit name
                                var unit_lookup =
                                    from unit in pfs.UNITs
                                    where unit.NAME == _unit_name
                                    select new
                                    {
                                        unit.UNIT_ID
                                    };
                                if (unit_lookup.Count() == 0)
                                {
                                    Program.LogValidationError("Unit name not found: " + _unit_name, filename);
                                    skip_pt = true;
                                    _unit_id = 0;
                                    //return false;
                                }
                                if (!skip_pt)
                                {
                                    var unit_found = unit_lookup.First();
                                    _unit_id = unit_found.UNIT_ID;
                                }
                            }
                        }

                    } else {
                        if (!skip_pt)
                        {
                            // Detail line
                            string desc = line.Substring(5 - 1, 38).Trim();
                            string code = line.Substring(44 - 1, 16).Trim();
                            var perf_time = DateTime.Parse(line.Substring(60 - 1, 17));
                            string res = line.Substring(78 - 1).Trim();

                            // Some items, like Patient Age, do not have the event time; instead they have data
                            // 
                            if (PFSUtility.DateDiffInDays(perf_time, _start) > 30)
                            {
                                // Ignore this line
                                Program.DebugTrace("Ignore {0}", desc);
                            }
                            else
                            {

                                var ci = NewChartItem(code, sequence, desc, perf_time, res);
                                pfs.CHART_ITEMs.InsertOnSubmit(ci);

                                if (sequence == 0)
                                {
                                    _start = perf_time;
                                    _finish = perf_time;
                                }
                                else
                                {
                                    // note: items may be in random order
                                    if (perf_time < _start) _start = perf_time;
                                    if (perf_time > _finish) _finish = perf_time;
                                }
                                sequence++;
                            }
                        }
                    }
                }
            }

            infile.Close();

            // Finish last patient
            SubmitPatientChartItems(pfs);

            return true;
        }

        private void SubmitPatientChartItems(PfsDataContext pfs)
        {
            if (_encounter_id == 0) return;
            if (_unit_id == 0) return;

            //
            // Delete existing chart items for this encounter bewteen the timestamps
            //
            Program.DebugTrace("Delete existing...");
            using (var cn = PFSUtility.NewSqlConnection()) {
                string sql = "delete from chart_item where encounter_id=" + _encounter_id +
                    " and event_datetime between " + PFSUtility.SQLDateTime(_start) +
                                       " and " + PFSUtility.SQLDateTime(_finish);
                Program.DebugTrace(sql);
                var cmd = new SqlCommand(sql, cn);
                cmd.ExecuteNonQuery();
            }

            // Now save the new chart items
            pfs.SubmitChanges();
            Console.WriteLine("Processed acct {0}", _acct_number);
        }
        
        private CHART_ITEM NewChartItem(string code, short sequence, string desc, DateTime perf_time, string res)
        {
            var result = new CHART_ITEM();

            result.EVENT_DATETIME = perf_time;
            result.ENCOUNTER_ID = _encounter_id;
            result.UNIT_ID = _unit_id;
            result.SEQUENCE = sequence;                     // needed in case of duplicate codes
            result.CODE = code;
            result.DESCRIPTION = desc;
            result.CATEGORY = "";
            result.RESULT = res;
            // We should reach out and get the server time, but this should be running on the server anyway
            result.TIMESTAMP = DateTime.Now;                        // ** should use server time
            
            return result;
        }

    }
}
