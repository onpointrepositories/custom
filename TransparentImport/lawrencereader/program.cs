﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Data.SqlClient;                // for SqlConnection
using System.IO;                            // for Path
using PfsShared;

namespace DataReader
{
    static class Program
    {
        const int MAX_FILE_LIFE_IN_DAYS = 45;

        public static bool _debug;

        static string _path = "";
        static string _file = "";
        static bool _norename;
        static bool _nodelete;
        static bool _pause;

        static void Main(string[] args)
        {
            try
            {
                ParseCommandLine(args);
                ProcessFiles();
                DeleteOldFiles();
            }
            catch(Exception e)
            {
                Console.WriteLine("Unexpected Error: {0}", e.Message);
                Console.WriteLine("{0}", e.StackTrace);
                LogUnexpectedError(e.Message, e.StackTrace);
            }

            if (_debug || _pause)
            {
                Console.WriteLine("\n");
                Console.Write("Press any key...");
                Console.ReadKey();
            }
        }

        static void ParseCommandLine(string[] args)
        {
            //_path = @".";
            _path = @"c:\users\public\acuityplus\load_me";

            foreach (var arg in args)
            {
                var arr = arg.Split('=');
                
                switch(arr[0])
                {
                    case "-debug":
                        _debug = args.Contains("-debug");
                        break;
                    case "-file":
                        _file = arr[1];
                        break;
                    case "-nodelete":
                        _nodelete = true;
                        break;
                    case "-norename":
                        _norename = true;
                        break;
                    case "-path":
                        _path = arr[1];
                        break;
                    case "-pause":
                        _pause = true;
                        break;
                    default:
                        if (arr[0].Left(1) != "-")
                        {
                            _path = arr[0];                 // assume it is a path
                        }
                        else
                        {
                            Console.WriteLine("unexpected argument: {0}", arg);
                        }
                        break;
                }
            }
        }

        static void ProcessFiles()
        {
            if (!string.IsNullOrEmpty(_file))
            {
                ProcessFile(Path.Combine(_path, _file),0);
                LogInfo("Processed one file", _file);
            }
            else
            {
                DebugTrace("Look for files...");
                int count = 0;
                var files = Directory.GetFiles(_path,"*.xml").OrderBy(f=>f);
                string prevfn = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
                string currfn;
                int ofs = 0;
                foreach (var file in files)
                {
                    currfn = Path.GetFileName(file);
                    if (currfn.Substring(1, 8) == prevfn.Substring(1, 8))
                        ofs++;
                    else
                        ofs = 0;
                    prevfn = currfn;
                    ProcessFile(file,ofs);
                    count++;
                }
                LogInfo("Processed " + count + " files", "");
            }
        }

        static void ProcessFile(string pathname, int offset)
        {
            var reader = new DataReader();

            Console.WriteLine("Processing {0} Multi={1}", pathname,offset);
            if (reader.process(pathname,offset) && !_norename)
            {
                // rename the processed file
                DebugTrace("rename file...");
                string newname = Path.ChangeExtension(pathname, ".bak");
                File.Delete(newname);               // in case it already exists (no error if not)
                File.Move(pathname, newname);       // rename
            }
        }

        static void DeleteTheseOldFiles(string pattern)
        {
            string[] files = Directory.GetFiles(_path, pattern);

            foreach (string file in files)
            {
                FileInfo fi = new FileInfo(file);
                if (fi.LastWriteTime < DateTime.Now.AddDays(-MAX_FILE_LIFE_IN_DAYS))
                {
                    if (_nodelete)
                        DebugTrace("Would delete {0}", file);
                    else
                    {
                        DebugTrace("Delete {0}", file);
                        fi.Delete();
                    }
			    }
            }
        }
        
        static void DeleteOldFiles()
        {
            DebugTrace("delete old files...");
            // Don't delete *.*; keep old unprocessed xml files
            DeleteTheseOldFiles("*.bak");
            DeleteTheseOldFiles("*.txt");
        }
        
        public static void DebugTrace(string format, params object[] values)
        {
            if (_debug)
            {
                Console.WriteLine(format, values);
            }
        }

        public static void LogInfo(string msg, string source)
        {
            PFSEventLog.AddEventLogEntry(
                PFSEventLog.EventLogSource.EVENT_SOURCE_TRANSPARENT_MAPPING,
                PFSEventLog.EventLogType.EVENT_TYPE_INFO,
                PFSEventLog.EventLogCategory.EVENT_CATEGORY_PROCESSED,
                msg, source, 0, 0, 0);
        }

        public static void LogValidationError(string msg, string source)
        {
            Console.WriteLine(msg);
            PFSEventLog.AddEventLogEntry(
                PFSEventLog.EventLogSource.EVENT_SOURCE_TRANSPARENT_MAPPING,
                PFSEventLog.EventLogType.EVENT_TYPE_ERROR,
                PFSEventLog.EventLogCategory.EVENT_CATEGORY_VALIDATION,
                msg, source, 0, 0, 0);
        }

        public static void LogUnexpectedError(string msg, string source)
        {
            Console.WriteLine(msg);
            PFSEventLog.AddEventLogEntry(
                PFSEventLog.EventLogSource.EVENT_SOURCE_TRANSPARENT_MAPPING,
                PFSEventLog.EventLogType.EVENT_TYPE_ERROR,
                PFSEventLog.EventLogCategory.EVENT_CATEGORY_UNEXPECTED,
                msg, source, 0, 0, 0);
        }
    }
}
