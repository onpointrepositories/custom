﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Data.SqlClient;                //for SqlConnection
using System.IO;                            // for Path
using PfsShared;
//
// Custom XML reader for Mayo Rochester ED.
// This loads xml files into the chart_item table.
// Patients must already exist.
//
namespace DataReader
{
    class DataReader
    {
        const int MAX_RESULT_WIDTH = 625;
        
        // Info about the chart being processed
        int         _encounter_id;          // (int is 32 bit so this is OK)
        int         _sequence;              // sequence# for dummy codes
        DateTime    _start_dt = DateTime.Now.AddHours(-24);
        DateTime    _finish_dt = DateTime.Now;
        string      _treatment_area;
        ENCOUNTER_LOCATION[] _locations;    // for unit_id lookup
        string _category;
        string _fndcode;
        string _descript;
        string _group;
        string _result;
        string _perfdate;
        int seq = DateTime.Now.Hour * 10;
        //start with a diff seq for each pull. 
        // the 10x multiplier is just to make it more remotely possible to have
        // duplicates.  Say FND01 with 5am is the first item in the file.
        // then likely it will be the first item in the next file.
        // but if the sequence starts at 10xhour, then that should take it well out of
        // the sequencing range from the initial entry.
        string _sourcefn;

        // Process one xml file.
        // Returns true if the file has been processed and it is OK to rename.
        //
        public bool process(string xml_pathname, int offset)
        {
            XDocument doc;
//            seq = offset * 100;
            _sourcefn = Path.GetFileName(xml_pathname);
            var pfs = PFSUtility.NewPfsDataContext();

            try
            {
                doc = XDocument.Load(xml_pathname);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error opening file {0}", xml_pathname);
                Console.WriteLine(e.Message);                   // xml validation error
                Program.LogValidationError(e.Message, xml_pathname);
                return false;
            }

            // There might be a .txt file with the same name
            //_treatment_area = LoadTreatmentAreaIfAny(xml_pathname);
            
            var ibex_charts =
                from ibex_chart in doc.Descendants("Encounter")
                select ibex_chart;

            string mrn = "";
            string acct_number = "";

            foreach (var ibex_chart in ibex_charts)
            {
                var idents =
                    from ident in ibex_chart.Descendants("Identifiers")
                    select ident;

                // Combine multiple data items into one long result
                foreach (var ident in idents)
                {
                    mrn = ElementValueOrNullString(ident, "MRN");
                    acct_number = ElementValueOrNullString(ident, "EncounterId");
                }

                Console.WriteLine("mrn=" + mrn);
                Console.WriteLine("acct_num=" + acct_number);

                // Search for acct# in database
                var encounter_lookup =
                    from enc in pfs.ENCOUNTERs
                    where enc.ACCT_NUMBER == acct_number
                    select new
                    {
                        enc.ENCOUNTER_ID
                    };
                Console.WriteLine("lookup count=" + encounter_lookup.Count());
                if (encounter_lookup.Count() == 0)
                {
                    Program.LogValidationError("Account number not found: " + acct_number, xml_pathname);
                    //Program.LogValidationError("  Chart items range from " + _start_dt + " to " + _finish_dt, "");
                    return false;
                }
                var enc_found = encounter_lookup.First();
                _encounter_id = enc_found.ENCOUNTER_ID;
                Console.WriteLine("_encounter_id=" + _encounter_id);
                Program.DebugTrace("Encounter ID = {0}", _encounter_id);

                //
                // Look up the encounter locations
                //
                var locations_lookup =
                    from loc in pfs.ENCOUNTER_LOCATIONs
                    where loc.ENCOUNTER_ID == _encounter_id
                    orderby loc.EFFECTIVE_DATETIME_IN
                    select loc;
                _locations = locations_lookup.ToArray();        // save for quick lookup
                if (_locations.Count() == 0)
                {
                    Program.LogValidationError("Patient has no locations - Acct: " + acct_number, Path.GetFileName(xml_pathname));
                    return false;
                }
                Program.DebugTrace("This patient has {0} locations", _locations.Count());

                //DeleteDateRange(doc);
                SaveFindings(pfs, ibex_chart);
                SaveMeds(pfs, ibex_chart);
                SaveIOs(pfs, ibex_chart);
                SaveMeasurements(pfs, ibex_chart);
                SaveVitals(pfs, ibex_chart);
                SaveIVSites(pfs, ibex_chart);
                SavePainEntries(pfs, ibex_chart);

                if (acct_number == null)
                {
                    Console.WriteLine("missing acct_number");
                    ShowElementInfo(ibex_chart);
                    Program.LogValidationError("Missing acct_number", xml_pathname);
                    return false;
                }
            } //for each ibex_chart

            // Save all of the new chart items
            Program.DebugTrace("submitting...");
         //   SubmitOverwrite(pfs);
            //pfs.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
            Program.DebugTrace("done");

            // We can list each file, but it is probably good enough just to say how many files
            // were loaded at the end.
            //Program.LogInfo(String.Format("Loaded chart for account number {0}", acct_number), xml_pathname);
            return true;
        }

        public void DeleteDateRange(XDocument doc)
        {

            // Assume document is an XDocument
            List<string> perfdates = doc.Descendants()
                   .Attributes("EnteredForDate")
                   .Select(x => x.Value)
                   .OrderBy(x => x.ToString())
                   .ToList();

            string minstr = perfdates.First();
            string maxstr = perfdates.Last();

            //Console.WriteLine("min entered for date=" + minstr);
            //Console.WriteLine("max entered for date=" + maxstr);
            Program.DebugTrace("Delete existing...");
            using (var cn = PFSUtility.NewSqlConnection())
            {
                string sql = "delete from chart_item where encounter_id=" + _encounter_id +
                    " and event_datetime between '" + minstr +
                                       "' and '" + maxstr + "'";
                Program.DebugTrace(sql);
                var cmd = new SqlCommand(sql, cn);
                cmd.ExecuteNonQuery();
            }

        }

        public void SubmitOverwrite(PfsDataContext pfs)
        {
            bool moreToSubmit = true;
            do 
            {
                try
                {
                    pfs.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
                    moreToSubmit = false;
                }
                catch (System.Data.Linq.ChangeConflictException)
                {
                    foreach (System.Data.Linq.ObjectChangeConflict occ in pfs.ChangeConflicts)
                    {
                        // All database values overwrite current values with 
                        //values from database
                        occ.Resolve(System.Data.Linq.RefreshMode.OverwriteCurrentValues);
                    }
                }
            }
            while (moreToSubmit);

        }


        void SaveFindings(PfsDataContext pfs, XElement ibex_chart)
        {
            Console.WriteLine("SaveFindings...");
            var findings =
                from finding in ibex_chart.Descendants("Findings")
                select finding;
            var finding_entries =
                from finding_entry in findings.Descendants("Finding")
                select finding_entry;

            foreach (var finding_entry in finding_entries)
            {
                _category = finding_entry.Attribute("Category").Value;
                _group = finding_entry.Attribute("Group").Value;
                _fndcode = finding_entry.Attribute("Finding").Value;
                if (_fndcode.Trim() == "FND0000")
                    _fndcode += _group;
                _descript = finding_entry.Attribute("Description").Value;
                _result = finding_entry.Attribute("Value").Value;
                _perfdate = finding_entry.Attribute("EnteredForDate").Value;
                Console.WriteLine("category=" + _category);
                Console.WriteLine("group=" + _group);
                Console.WriteLine("fndcode=" + _fndcode);
                Console.WriteLine("descript=" + _descript);
                Console.WriteLine("result=" + _result);
                Console.WriteLine("performeddate=" + _perfdate);
                Console.WriteLine("seq=" + seq);
                if (_fndcode.Trim() != "0000000000") SaveChartItem(pfs);

                var secfinding_entries =
                    from secfinding_entry in finding_entry.Descendants("SecondaryFinding")
                    select secfinding_entry;
                foreach (var secfinding_entry in secfinding_entries)
                {
                    _category = secfinding_entry.Attribute("Category").Value;
                    _group = secfinding_entry.Attribute("Group").Value;
                    _fndcode = secfinding_entry.Attribute("Finding").Value;
                    if (_fndcode.Trim() == "FND0000")
                        _fndcode += _group;
                    _descript = secfinding_entry.Attribute("Description").Value;
                    _perfdate = secfinding_entry.Attribute("EnteredForDate").Value;
                    Console.WriteLine("sec category=" + _category);
                    Console.WriteLine("sec group=" + _group);
                    Console.WriteLine("sec fndcode=" + _fndcode);
                    Console.WriteLine("sec descript=" + _descript);
                    Console.WriteLine("sec performeddate=" + _perfdate);
                    Console.WriteLine("sec seq=" + seq);
                    if (_fndcode.Trim() != "0000000000") SaveChartItem(pfs);
                }
            }
        }

        void SaveChartItem(PfsDataContext pfs)
        {
            //DateTime pdate = PFSUtility.ISOToDateTime(_perfdate);
            var ci = new CHART_ITEM();

            string format = "yyyyMMddHHmm";
            if (_perfdate.Length == 12)
                ci.EVENT_DATETIME = DateTime.ParseExact(_perfdate, format, System.Globalization.CultureInfo.InvariantCulture);
            else
                ci.EVENT_DATETIME = Convert.ToDateTime(_perfdate);
            ci.ENCOUNTER_ID = _encounter_id;
            ci.UNIT_ID = LookupUnitID(_start_dt);
            ci.CODE = _fndcode;
            ci.CATEGORY = _category;
            ci.DESCRIPTION = _descript;
            ci.FIELD_NAME = _group;
            ci.RESULT = _result;
            ci.TIMESTAMP = DateTime.Now;
            ci.SEQUENCE = (short)seq++;
            ci.SOURCE_TEXT = _sourcefn;

            pfs.CHART_ITEMs.InsertOnSubmit(ci);
            MaybeSubmitChanges(pfs);

        }
        void SaveMeasurements(PfsDataContext pfs, XElement ibex_chart)
        {
            Console.WriteLine("SaveMeasurements...");
            var measures =
                from measurements in ibex_chart.Descendants("Measurements")
                select measurements;
            var measure =
                from meas in measures.Descendants("Measurement")
                select meas;
            foreach (var meas in measure)
            {
                _category = "Measurement";
                _fndcode = meas.Attribute("Code").Value;
                _descript = meas.Attribute("Description").Value;
                _result = meas.Attribute("Value").Value;
                _perfdate = meas.Attribute("EnteredForDate").Value;
                //Console.WriteLine("category=" + category);
                //Console.WriteLine("fndcode=" + fndcode);
                //Console.WriteLine("descript=" + descript);
                //Console.WriteLine("result=" + result);
                //Console.WriteLine("performeddate=" + perfdate);
                SaveChartItem(pfs);
            }
        }

        void SaveIOs(PfsDataContext pfs, XElement ibex_chart)
        {
            Console.WriteLine("SaveIOs...");
            var inptoutp =
                from io in ibex_chart.Descendants("InputOutput")
                select io;
            var inps =
                from inp in inptoutp.Descendants("Input")
                select inp;
            var outps =
                from outp in inptoutp.Descendants("Output")
                select outp;

            foreach (var inp in inps)
            {
                _category = inp.Attribute("Type").Value;
                _fndcode = inp.Attribute("SourceCode").Value;
                _descript = inp.Attribute("SourceName").Value;
                _perfdate = inp.Attribute("EnteredForDate").Value;
                _result = inp.Attribute("Amount").Value;
                //Console.WriteLine("category=" + category);
                //Console.WriteLine("fndcode=" + fndcode);
                //Console.WriteLine("descript=" + descript);
                //Console.WriteLine("performeddate=" + perfdate);
                SaveChartItem(pfs);
            }
            foreach (var outp in outps)
            {
                _category = outp.Attribute("Type").Value;
                _fndcode = outp.Attribute("SourceCode").Value;
                _descript = outp.Attribute("SourceName").Value;
                _perfdate = outp.Attribute("EnteredForDate").Value;
                _result = outp.Attribute("Amount").Value;
                //Console.WriteLine("category=" + category);
                //Console.WriteLine("fndcode=" + fndcode);
                //Console.WriteLine("descript=" + descript);
                //Console.WriteLine("performeddate=" + perfdate);
                SaveChartItem(pfs);
            }

        }

        void SaveVitals(PfsDataContext pfs, XElement ibex_chart)
        {
            Console.WriteLine("SaveVitals...");
            var vitals =
                from vital in ibex_chart.Descendants("Vitals")
                select vital;
            var vital_entries =
                from vital_entry in vitals.Descendants("Vital")
                select vital_entry;
            foreach (var vit in vital_entries)
            {
                _category = "Vitals";
                _fndcode = vit.Attribute("Code").Value;
                _descript = vit.Attribute("Description").Value;
                _result = vit.Attribute("Value1").Value;
                _perfdate = vit.Attribute("EnteredForDate").Value;
                //Console.WriteLine("category=" + category);
                //Console.WriteLine("fndcode=" + fndcode);
                //Console.WriteLine("descript=" + descript);
                //Console.WriteLine("result=" + result);
                //Console.WriteLine("performeddate=" + perfdate);
                SaveChartItem(pfs);
            }
        }

        void SaveIVSites(PfsDataContext pfs, XElement ibex_chart)
        {
            Console.WriteLine("SaveIVSites...");
            var items =
                from item in ibex_chart.Descendants("IntravenousSites")
                select item;
            var item_entries =
                from item_entry in items.Descendants("IntravenousSite")
                select item_entry;

            foreach (var item_entry in item_entries)
            {
                _category = item_entry.Attribute("Site").Value;
                _group = item_entry.Attribute("Type").Value;
                _fndcode = "IVSite";
                _descript = "";
                _result = item_entry.Attribute("Lumens").Value;
                _perfdate = item_entry.Attribute("EnteredForDate").Value;
                Console.WriteLine("category=" + _category);
                Console.WriteLine("group=" + _group);
                Console.WriteLine("fndcode=" + _fndcode);
                Console.WriteLine("descript=" + _descript);
                Console.WriteLine("result=" + _result);
                Console.WriteLine("performeddate=" + _perfdate);
                SaveChartItem(pfs);
            }
        }

        void SavePainEntries(PfsDataContext pfs, XElement ibex_chart)
        {
            Console.WriteLine("SavePain...");
            var items =
                from item in ibex_chart.Descendants("PainSites")
                select item;
            var item_entries =
                from item_entry in items.Descendants("PainSite")
                select item_entry;
            foreach (var item_entry in item_entries)
            {
                _category = "";
                _group = "";
                _descript = item_entry.Attribute("Location").Value;
                var pain_entries =
                    from pain_entry in item_entry.Descendants("Pain")
                    select pain_entry;
                foreach (var pain_entry in pain_entries)
                {
                    _fndcode = pain_entry.Attribute("Group").Value;
                    _result = pain_entry.Attribute("Finding").Value;
                    _perfdate = pain_entry.Attribute("EnteredForDate").Value;
                    //Console.WriteLine("category=" + _category);
                    Console.WriteLine("group=" + _group);
                    Console.WriteLine("fndcode=" + _fndcode);
                    Console.WriteLine("descript=" + _descript);
                    Console.WriteLine("result=" + _result);
                    Console.WriteLine("performeddate=" + _perfdate);
                    SaveChartItem(pfs);
                }
            }
        }

        void SaveMeds(PfsDataContext pfs, XElement ibex_chart)
        {
            string comments;

            Console.WriteLine("SaveMeds...");
            var items =
                from item in ibex_chart.Descendants("RxOrders")
                select item;
            var item_entries =
                from item_entryA in items.Descendants("RxOrder")
                select item_entryA;
            foreach (var item_entry in item_entries)
            {
                string order_number = item_entry.Attribute("OrderNumber").Value;
                var med_entries =
                    from med_entryA in item_entry.Descendants("RxMedication")
                    select med_entryA;
                foreach (var med_entry in med_entries)
                {
                    _category = "";
                    _group = "";
                    _fndcode = "RXORDER" + order_number;
                    if (item_entry.Attribute("Comments") != null)
                        comments = item_entry.Attribute("Comments").Value;
                    else
                        comments = "";
                    _descript = med_entry.Attribute("Description").Value + "; " + comments;
                    if (_descript.Length > 256)
                        _descript = _descript.Substring(0, 255);
                    _result = "Dose=" + med_entry.Attribute("Dose").Value +
                              ";Route=" + med_entry.Attribute("Route").Value +
                              ";Freq=" + med_entry.Attribute("Frequency").Value;
                    var admin_entries =
                        from admin_entryA in med_entry.Descendants("RxAdministration")
                        select admin_entryA;
                    foreach (var admin_entry in admin_entries)
                    {
                        _perfdate = admin_entry.Attribute("AdministrationTime").Value;
                        //Console.WriteLine("category=" + _category);
                        Console.WriteLine("group=" + _group);
                        Console.WriteLine("fndcode=" + _fndcode);
                        Console.WriteLine("descript=" + _descript);
                        Console.WriteLine("result=" + _result);
                        Console.WriteLine("performeddate=" + _perfdate);
                        if (_perfdate.Trim() != "")
                            SaveChartItem(pfs);
                    }
                }
            }
        }


        bool DateIsValid(string dt)
        {
            return true;
        }

        //string LoadTreatmentAreaIfAny(string xml_pathname)
        //{
        //    string txt_pathname = Path.ChangeExtension(xml_pathname, ".txt");
        //    string result = "";
        //    try
        //    {
        //        using (StreamReader sr = new StreamReader(txt_pathname))
        //        {
        //            result = sr.ReadLine();
        //        }
        //    }
        //    catch
        //    {
        //        // Don't care if it doesn't exist
        //    }
        //    Program.DebugTrace("Treatment area = {0}", result);
        //    return result;
        //}

        //void AddTreatmentAreaChartItem(PfsDataContext pfs)
        //{
        //    if (_treatment_area.Length == 0) return;

        //    const string ta = "TREATMENT_AREA";
        //    var ci = new CHART_ITEM();

        //    // The treatment area is for the Visit (first) unit only - use _start_dt
        //    ci.EVENT_DATETIME = _start_dt;
        //    ci.ENCOUNTER_ID = _encounter_id;
        //    ci.UNIT_ID = LookupUnitID(_start_dt);
        //    ci.CODE = NextDummyCode();
        //    ci.CATEGORY = ta;
        //    ci.DESCRIPTION = ta;
        //    ci.FIELD_NAME = ta;
        //    ci.RESULT = _treatment_area;
        //    ci.TIMESTAMP = DateTime.Now;

        //    pfs.CHART_ITEMs.InsertOnSubmit(ci);
        //    MaybeSubmitChanges(pfs);
        //}

        int LookupUnitID(DateTime dt)
        {
            if (_locations.Count() == 0) return -1;
            if (dt < _locations.First().EFFECTIVE_DATETIME_IN) return _locations.First().UNIT_ID;  // invalid? return first

            foreach (var loc in _locations)
            {
                if ((dt >= loc.EFFECTIVE_DATETIME_IN) && (dt < loc.EFFECTIVE_DATETIME_OUT))
                    return loc.UNIT_ID;
            }
            return _locations.Last().UNIT_ID;                       // invalid? return last
        }

        //string NextDummyCode()
        //{
        //    return "dummy" + string.Format("{0:0000}", _sequence++);
        //}

        //CHART_ITEM NewChartItem(XElement entry, string category, string description)
        //{
        //    var result = new CHART_ITEM();

        //    string ts = entry.Element("entry_timestamp").Value;     // try for entry_timestamp
        //    if (string.IsNullOrEmpty(ts))
        //        ts = entry.Element("timestamp").Value;              // fall back on timestamp
        //    result.EVENT_DATETIME = PFSUtility.ISOToDateTime(ts);

        //    result.ENCOUNTER_ID = _encounter_id;
        //    result.UNIT_ID = LookupUnitID(result.EVENT_DATETIME);
        //    // There is no CODE and multiple entries may have the same section & timestamp.
        //    // Generate unique codes for entries with the same timestamp.
        //    result.CODE = NextDummyCode();
        //    result.CATEGORY = category;
        //    result.DESCRIPTION = description;
        //    // We should reach out and get the server time, but this should be running on the server anyway
        //    result.TIMESTAMP = DateTime.Now;                        // ** should use server time
            
        //    return result;
        //}

        //void ParseChartEntry(PfsDataContext pfs, XElement entry)
        //{
        //    var combinedText = new StringBuilder();
        //    string category = entry.Element("section").Value;
        //    string description = entry.Element("sub_section").Value;

        //    Program.DebugTrace("entry {0}", entry.Attribute("num"));

        //    Program.DebugTrace("  timestamp={0}  entry timestamp={1}", 
        //        entry.Element("timestamp").Value,
        //        entry.Element("entry_timestamp").Value);
        //    Program.DebugTrace("  user={0}", entry.Element("user").Value);
        //    Program.DebugTrace("  CATEGORY={0}", category);
        //    Program.DebugTrace("  DESCRIPTION={0}", description);

        //    // Beware empty timestamps!
        //    if (entry.Element("timestamp").Value.Length == 0)
        //        return;

        //    //
        //    // Look for array of <data> for the result
        //    //
        //    var data =
        //        from datum in entry.Descendants("data")
        //        select datum;

        //    // Combine multiple data items into one long result
        //    foreach (var datum in data)
        //    {
        //        string txt = ElementValueOrNullString(datum, "text");
        //        Program.DebugTrace("  data={0}", txt);

        //        // Look for simple "field: value" strings
        //        //
        //        string[] sep = { ": " };
        //        var arr = txt.Split(sep, StringSplitOptions.None);
        //        if (arr.Length == 2)
        //        {
        //            Program.DebugTrace("  FIELD={0}", arr[0]);
        //            Program.DebugTrace("  RESULT={0}", arr[1]);
        //            var ci = NewChartItem(entry, category, description);
        //            ci.FIELD_NAME = arr[0].Left(64);
        //            ci.RESULT = arr[1].TruncateWithEllipsis(MAX_RESULT_WIDTH);

        //            pfs.CHART_ITEMs.InsertOnSubmit(ci);
        //            MaybeSubmitChanges(pfs);
        //        }
        //        else
        //        {
        //            if (combinedText.Length > 0)
        //                combinedText.Append("; ");
        //            combinedText.Append(txt);
        //        }
        //    }

        //    if (combinedText.Length > 0)
        //    {
        //        Program.DebugTrace("  RESULT={0}", combinedText.ToString());
        //        var ci = NewChartItem(entry, category, description);
        //        ci.FIELD_NAME = null;
        //        ci.RESULT = combinedText.ToString().TruncateWithEllipsis(MAX_RESULT_WIDTH);

        //        pfs.CHART_ITEMs.InsertOnSubmit(ci);
        //        MaybeSubmitChanges(pfs);
        //    }

        //}

        //void ParseFlowsheetEntry(PfsDataContext pfs, XElement entry)
        //{
        //    XElement field_group = entry.Element("field_group");
        //    if (field_group == null)
        //        return;
        //    string category = field_group.Attribute("name").Value;
        //    string description = "";

        //    Program.DebugTrace("entry {0}", entry.Attribute("num"));

        //    Program.DebugTrace("  timestamp={0}  entry timestamp={1}",
        //        entry.Element("timestamp").Value,
        //        entry.Element("entry_timestamp").Value);
        //    Program.DebugTrace("  user={0}", entry.Element("user").Value);
        //    Program.DebugTrace("  CATEGORY={0}", category);
        //    //Program.DebugTrace("  DESCRIPTION={0}", description);

        //    //
        //    // Look for array of <field>
        //    //
        //    //var field_groups =
        //    //    from field_group in entry.Descendants("field_group")
        //    //    select field_group;
        //    var fields =
        //        from field in field_group.Descendants("field")
        //        select field;

        //    foreach (var field in fields)
        //    {
        //        string name = ElementValueOrNullString(field, "name");
        //        string value = ElementValueOrNullString(field, "value");
        //        Program.DebugTrace("  FIELD={0}", name);
        //        Program.DebugTrace("  RESULT={0}", value);
        //        // The first character of a vital sign value needs to be numeric or '-'
        //        // Legal non-numeric values are 120/80 and -80-
        //        if ((category == "VITAL SIGNS") && (!value.Left(1).IsNumeric() && value.Left(1) != "-"))
        //            Program.DebugTrace("  ** Skipping vital sign without a numeric value");
        //        else
        //        {
        //            var ci = NewChartItem(entry, category, description);
        //            ci.FIELD_NAME = name;
        //            ci.RESULT = value;

        //            pfs.CHART_ITEMs.InsertOnSubmit(ci);
        //            MaybeSubmitChanges(pfs);
        //        }
        //    }

        //}

        //void ParseOrderEntry(PfsDataContext pfs, XElement entry)
        //{
        //    string category = "ORDER";
        //    string description = ElementValueOrNullString(entry, "order_description");
        //    var ci = NewChartItem(entry, category, description);
        //    ci.FIELD_NAME = null;
        //    ci.RESULT = null;

        //    pfs.CHART_ITEMs.InsertOnSubmit(ci);
        //    MaybeSubmitChanges(pfs);
        //}


        //string AddLeadingZeros(string s, int desired_length)
        //{
        //    string result = s;
        //    while (result.Length < desired_length)
        //    {
        //        result = "0" + result;
        //    }
        //    return result;
        //}

        string ElementValueOrNullString(XElement node, string name)
        {
            XElement elem = node.Element(name);
            if (elem != null)
                return elem.Value;
            return "";
        }

        void ShowElementInfo(XElement xelem)
        {
            Program.DebugTrace("{2} -- Has Attr? {0}, Has Elem? {1}",
                xelem.HasAttributes,
                xelem.HasElements,
                xelem.Name);
        }

        void MaybeSubmitChanges(PfsDataContext pfs)
        {
            SubmitOverwrite(pfs);
            // Turn on debug mode to insert a row at a time
            //if (Program._debug)
            //{
            //    // Multiple inserts are not as effecient but this could really help track down
            //    // an unexpected error: the trace should show what entry was being parsed
            //    // if the insert fails.
            //    pfs.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
            //}
        }

    }
}
