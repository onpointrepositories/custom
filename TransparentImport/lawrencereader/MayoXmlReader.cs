﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Data.SqlClient;                //for SqlConnection
using PfsShared;

namespace MayoXmlReader
{
    class MayoEdXmlReader
    {
        // Multiple inserts are not as efficient, but they give better error messages because you
        // can see where the error was.
        const bool WITH_MULTIPLE_INSERTS = true;

        int _encounter_id;               // int is 32 bit
        int _unit_id;

        public void process(string filename)
        {
            // Q: How do we decide on the unit?  **
            // A: look at patient locations
            //
            _unit_id = 361;     // **

            XDocument doc;
            string acct_number;

            var pfs = PFSUtility.NewPfsDataContext();

            try
            {
                doc = XDocument.Load(filename);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error opening file {0}", filename);
                Console.WriteLine(e.Message);                   // xml validation error
                LogValidationError(e.Message, filename);
                return;
            }
            
            var ibex_charts =
                from ibex_chart in doc.Descendants("ibex_medical_chart")
                select ibex_chart;

            foreach (var ibex_chart in ibex_charts)
            {

                if (ibex_chart.Attribute("acctnum") == null)
                {
                    Console.WriteLine("missing acctnum");
                    ShowElementInfo(ibex_chart);
                    LogValidationError("Missing acctnum", filename);
                    return;
                }

                Program.DebugTrace("");
                Program.DebugTrace(new String('=', 60));
                Program.DebugTrace("ibex chart for {0}, {1} {2} -- acct {3}",
                              ibex_chart.Attribute("lname").Value,
                            ibex_chart.Attribute("fname").Value,
                            ibex_chart.Attribute("mname").Value,
                            ibex_chart.Attribute("acctnum").Value);

                // Scan the chart for date/time range
                var charts =
                    from chart in ibex_chart.Descendants("chart")
                    select chart;
                var entries =
                    from entry in charts.Descendants("entry")
                    select entry;

                // Get the time range of transactions
                // beware of null string timestamps
                var transactionDates = 
                    from t in entries
                    where t.Element("entry_timestamp") != null
                    select t.Element("entry_timestamp").Value;
                var dates = new { 
                    FromDate = transactionDates.Min(), 
                    ToDate = transactionDates.Max() 
                };
                Program.DebugTrace("chart item dates from {0} to {1}", dates.FromDate, dates.ToDate);

                var q = 
                    from e in entries
                    where e.Element("entry_timestamp") != null
                    group e by 1 into g             //fake grouping with a constant value
                    select new {
                        FromDate = g.Min(t => t.Element("entry_timestamp").Value),
                        ToDate = g.Max(t => t.Element("entry_timestamp").Value)          
                    };
                var qq = q.First();
                Program.DebugTrace("Method 2: dates from {0} to {1}", qq.FromDate, qq.ToDate);

                //
                // Look up the account number
                //
                acct_number = ibex_chart.Attribute("acctnum").Value;
                acct_number = AddLeadingZeros(acct_number, 13);         // zero pad
                
                // Search for acct# in database
                var encounter_lookup =
                    from enc in pfs.ENCOUNTERs
                    where enc.ACCT_NUMBER == acct_number
                    select new
                    {
                        enc.ENCOUNTER_ID
                    };
                if (encounter_lookup.Count() == 0)
                {
                    Program.DebugTrace("Account number not found: " + acct_number);
                    LogValidationError("Account number not found: " + acct_number, filename);
                    return;
                }
                var enc_found = encounter_lookup.First();
                _encounter_id = enc_found.ENCOUNTER_ID;
                Program.DebugTrace("Encounter ID = {0}", _encounter_id);

                
                // Delete existing chart items for this encounter bewteen the timestamps
                //
                Program.DebugTrace("Delete existing...");
                using (var cn = PFSUtility.NewSqlConnection())
                {
                    string sql = "delete from chart_item where encounter_id=" + _encounter_id +
                        " and event_datetime between " + PFSUtility.SQLDateTime(PFSUtility.ISOToDateTime(qq.FromDate)) +
                                          " and " + PFSUtility.SQLDateTime(PFSUtility.ISOToDateTime(qq.ToDate));
                    Program.DebugTrace(sql);
                    var cmd = new SqlCommand(sql, cn);
                    cmd.ExecuteNonQuery();
                }

                foreach (var entry in entries)
                {
                    string subSection = entry.Element("sub_section").Value;

                    switch (subSection)
                    {
                        case "CHART VIEW":
                        case "CHART PRINT":
                            // skip these
                            break;
                        default:
                            // add chart item(s) for this entry
                            ParseEntry(pfs, entry);
                            break;
                    }

                }

                // Save the new chart items
                Program.DebugTrace("submitting...");
                pfs.SubmitChanges();
                Program.DebugTrace("done");

                // The mapper will add a "processed" event
                //LogInfo(String.Format("Loaded chart for account number {0}", acct_number), filename);
            }
        }

        int _sequence;                  // sequence# for dummy codes

        CHART_ITEM NewChartItem(XElement entry)
        {
            var result = new CHART_ITEM();

            string ts = entry.Element("entry_timestamp").Value;     // try for entry_timestamp
            if (string.IsNullOrEmpty(ts))
                ts = entry.Element("timestamp").Value;              // fall back on timestamp

            result.EVENT_DATETIME= PFSUtility.ISOToDateTime(ts);

            // There is no CODE and multiple entries have the same section & timestamp.
            // Need to generate unique codes for entries with the same timestamp.
            result.ENCOUNTER_ID = _encounter_id;
            result.UNIT_ID = _unit_id;
            result.CODE = "dummy" + string.Format("{0:0000}", _sequence++);
            result.CATEGORY = entry.Element("section").Value;
            result.DESCRIPTION = entry.Element("sub_section").Value;
            result.TIMESTAMP = DateTime.Now;                        // ** should use server time
            
            return result;
        }

        void MaybeSubmitChages(PfsDataContext pfs)
        {
            if (WITH_MULTIPLE_INSERTS)
            {
                pfs.SubmitChanges();
            }
        }

        void ParseEntry(PfsDataContext pfs, XElement entry)
        {
            var combinedText = new StringBuilder();

            Program.DebugTrace("entry {0}", entry.Attribute("num"));

            Program.DebugTrace("  timestamp={0}  entry timestamp={1}", 
                entry.Element("timestamp").Value,
                entry.Element("entry_timestamp").Value);
            Program.DebugTrace("  user={0}", entry.Element("user").Value);
            //Program.DebugTrace("  CODE=dummy{0}", _sequence++);      // make unique codes for dup timestamps
            Program.DebugTrace("  CATEGORY={0}", entry.Element("section").Value);
            Program.DebugTrace("  DESCRIPTION={0}", entry.Element("sub_section").Value);

            // Beware empty timestamps!
            if (entry.Element("timestamp").Value.Length == 0)
                return;

            var data =
                from datum in entry.Descendants("data")
                select datum;

            // Combine multiple data items into one long result
            foreach (var datum in data)
            {
                string txt = ElementValueOrNullString(datum, "text");
                Program.DebugTrace("  data={0}", txt);

                // Look for simple "field: value" strings
                //
                string[] sep = { ": " };
                var arr = txt.Split(sep, StringSplitOptions.None);
                if (arr.Length == 2)
                {
                    Program.DebugTrace("  FIELD={0}", arr[0]);
                    Program.DebugTrace("  RESULT={0}", arr[1]);
                    var ci = NewChartItem(entry);
                    ci.FIELD_NAME = arr[0].Left(64);
                    ci.RESULT = "foo";
                    ci.RESULT = arr[1].TruncateWithEllipsis(625);

                    pfs.CHART_ITEMs.InsertOnSubmit(ci);
                    MaybeSubmitChages(pfs);
                }
                else
                {
                    if (combinedText.Length > 0)
                        combinedText.Append("; ");
                    combinedText.Append(txt);
                }
            }

            if (combinedText.Length > 0)
            {
                Program.DebugTrace("  RESULT={0}", combinedText.ToString());
                var ci = NewChartItem(entry);
                ci.FIELD_NAME = null;
                ci.RESULT = combinedText.ToString().TruncateWithEllipsis(800);

                pfs.CHART_ITEMs.InsertOnSubmit(ci);
                MaybeSubmitChages(pfs);
            }
        }

        string AddLeadingZeros(string s, int desired_length)
        {
            string result = s;
            while (result.Length < desired_length)
            {
                result = "0" + result;
            }
            return result;
        }

        string ElementValueOrNullString(XElement node, string name)
        {
            XElement elem = node.Element(name);
            if (elem != null)
                return elem.Value;
            return "";
        }

        void ShowElementInfo(XElement xelem)
        {
            Program.DebugTrace("{2} -- Has Attr? {0}, Has Elem? {1}",
                xelem.HasAttributes,
                xelem.HasElements,
                xelem.Name);
        }

        void LogInfo(string msg, string filename)
        {
            PFSEventLog.AddEventLogEntry(
                PFSEventLog.EventLogSource.EVENT_SOURCE_TRANSPARENT_MAPPING,
                PFSEventLog.EventLogType.EVENT_TYPE_INFO,
                PFSEventLog.EventLogCategory.EVENT_CATEGORY_PROCESSED,
                msg, filename, 0, 0, 0);
        }

        void LogValidationError(string msg, string filename)
        {
            PFSEventLog.AddEventLogEntry(
                PFSEventLog.EventLogSource.EVENT_SOURCE_TRANSPARENT_MAPPING,
                PFSEventLog.EventLogType.EVENT_TYPE_ERROR,
                PFSEventLog.EventLogCategory.EVENT_CATEGORY_VALIDATION,
                msg, filename, 0, 0, 0);
        }
    }
}
