Attribute VB_Name = "AffMain"
Option Explicit
'
' This is the main module for Aff-WinPFS
'

Const MAX_INDICATORS = 32

Const START_GROUP_TITLE = 1
Const LEN_GROUP_TITLE = 20

Const START_ORDERS = 8
Const LEN_ORDERS = 20      'max len of Orders in afwinpfs*.dat

Const START_CLIN = 33
Const LEN_CLIN = 30

Const START_OTHER = 3
Const LEN_INDICATOR = 16  'max len of List, Categories, Responses, etc in afwinpfs*.dat

Const START_FREQ = 31  'start column of frequency in afwinpfs*.dat
Const LEN_FREQ = 15

Dim g_util As New RegistryUtils

Dim infn As String
Dim infname As String
Dim fnames() As String
Dim outfn As String
Dim dicfn As String
Dim dicfile As Integer
Private Type affindicator
  indwinpfs As Integer  'winpfs indic number associated with this indicator
  name As String        'name of this indicator
  freq As String        'frequency for this indic
  also_mark As String   'comma delimited list of other indics to mark
End Type
Dim indary() As affindicator
Dim numind As Integer
Dim inDirPath As String

Dim datafile As Integer
Dim outfile As Integer

Dim inds(32) As Boolean
Dim grps(32) As Integer

Dim lastname As String
Dim firstname As String
Dim unitname As String
Dim roomname As String
Dim bedname As String
Dim acctnum As String
Dim classdate As String
Dim classtime As String

Dim NPO As Boolean
Dim A000224 As Boolean
Dim A100085 As Boolean
Dim A100088 As Boolean
Dim I090054 As Boolean
Dim FEEDING As Boolean
Dim A050346 As Boolean
Dim A140340 As Boolean
Dim A140341 As Boolean
Dim A140342 As Boolean
Dim A000829 As Boolean
Dim A000830 As Boolean
Dim A100001 As Boolean

Sub Main()
    Dim n As Integer
    Dim i As Integer

    If (LoadDictionary) Then
        InitGroups
        n = GetAllInputFilenames
        i = 0
        While i < n
            i = i + 1
            infn = inDirPath + "\" + fnames(i)
            Process
        Wend
    End If

End Sub
Private Function LoadDictionary() As Boolean
    ' Note: IO with freq=<blank> (meaning "anything other than the frequencies listed")
     '  is listed last in the data file because the InStr function returns non-zero
     '  when comparing "" against "something".

    Dim buf As String

    numind = 0
    dicfile = FreeFile
    dicfn = App.Path & "\AFFWINPFS.DAT"
    Open dicfn For Input As #dicfile
    While Not EOF(dicfile)
        numind = numind + 1
        If (numind = 1) Then
            Line Input #dicfile, inDirPath
        Else
            Line Input #dicfile, buf
            ReDim Preserve indary(0 To numind - 1)
            indary(numind - 1).indwinpfs = val(Mid$(buf, 1, 2))
            indary(numind - 1).name = Trim$(Mid$(buf, 3, 27))
            indary(numind - 1).freq = Trim$(Mid$(buf, 30, 15))
            indary(numind - 1).also_mark = Trim$(Mid$(buf, 45, 15))
        End If
    Wend
    Close #dicfile
    numind = numind - 1
    LoadDictionary = True
End Function

Private Function SetFilenames() As Boolean
    Const HKEY_LOCAL_MACHINE = &H80000002
    Dim dirpath As String

    If g_util.GetKeyValue(HKEY_LOCAL_MACHINE, "SOFTWARE\Quadramed\WinPFS", "Directory", dirpath) Then
        outfn = dirpath & "\Load_Me\Classify.txt"
        'infn = App.Path & "\afwinpfs.txt"
    Else
        outfn = App.Path & "\Classify.txt"
    End If
    
    infname = Dir$(inDirPath + "\afwinpfs.*") 'returns ONLY the filename.  For wildcards.
    
    infn = inDirPath + "\" + infname
    
    SetFilenames = (infname <> "")
    
End Function

Private Function GetAllInputFilenames() As Integer
    Const HKEY_LOCAL_MACHINE = &H80000002
    Const INITIAL_BLOCK_SIZE = 24
    Dim dirpath As String
    Dim i As Integer
    
    If g_util.GetKeyValue(HKEY_LOCAL_MACHINE, "SOFTWARE\Quadramed\WinPFS", "Directory", dirpath) Then
        outfn = dirpath & "\Load_Me\Classify.txt"
        'infn = App.Path & "\afwinpfs.txt"
    Else
        outfn = App.Path & "\Classify.txt"
    End If
    
    ReDim fnames(0 To INITIAL_BLOCK_SIZE) As String
    i = 0
    infname = Dir$(inDirPath + "\afwinpfs.*") 'returns ONLY the filename.  For wildcards.
    While infname <> ""
        i = i + 1
        If (i > UBound(fnames)) Then
            ReDim Preserve fnames(0 To i + INITIAL_BLOCK_SIZE) As String
        End If
        fnames(i) = infname
        infname = Dir$
    Wend
    
    BubbleSort fnames, i
    
    GetAllInputFilenames = i
    
End Function

Private Sub BubbleSort(arr As Variant, numEls As Integer)
    Dim lLoop1 As Integer
    Dim lLoop2 As Integer
    Dim lTemp As Variant
    
    For lLoop1 = numEls To LBound(arr) Step -1
       For lLoop2 = LBound(arr) + 1 To lLoop1
           If arr(lLoop2 - 1) > arr(lLoop2) Then
              lTemp = arr(lLoop2 - 1)
              arr(lLoop2 - 1) = arr(lLoop2)
              arr(lLoop2) = lTemp
           End If
       Next lLoop2
   Next lLoop1
End Sub

'Private Sub ValidateFilenames()
'    Dim curFile As String
'
'    infn = Dir(App.Path + "afwinpfs.*")
'        While curFile <> ""
'            .ZipFileName = curFile
'            [...]
'            curFile = Dir
'        Wend
'    Wend
'
'End Sub

Private Sub DoPatientSummary()
    Dim highest_is_on As Boolean
    Dim g As Integer
    Dim i As Integer
    
    If (A050346 Or A140340 Or A140341 Or A140342 Or A000829 Or A000830) Then
        'authoritative indicator overrides all others marked
        For i = 1 To MAX_INDICATORS
            inds(i) = False
        Next i
        If (A050346) Then
            inds(2) = True
            inds(11) = True
            inds(19) = True
            inds(22) = True
            inds(27) = True
        ElseIf (A140340) Then
            inds(2) = True
            inds(11) = True
            inds(14) = True
            inds(17) = True
            inds(22) = True
        ElseIf (A140341) Then
            inds(3) = True
            inds(12) = True
            inds(15) = True
            inds(18) = True
            inds(22) = True
        ElseIf (A140342) Then
            inds(3) = True
            inds(5) = True
            inds(13) = True
            inds(15) = True
            inds(19) = True
            inds(22) = True
            inds(27) = True
            inds(28) = True
            inds(29) = True
        ElseIf (A000829) Then
            inds(1) = True
            inds(11) = True
            inds(17) = True
            inds(20) = True
            inds(27) = True
        ElseIf (A000830) Then
            inds(1) = True
            inds(20) = True
            inds(27) = True
            inds(28) = True
        End If
    Else
        If ((FEEDING Or NPO Or A100085 Or A100088 Or A100001) And (A000224 Or I090054)) Or _
            (A100085 And A100088) Or _
            (I090054 And A000224) Then
            inds(3) = True
        End If
        
        g = 0
        For i = MAX_INDICATORS To 1 Step -1
            If (grps(i) > 0) Then
                If (grps(i) <> g) Then
                    g = grps(i)
                    highest_is_on = inds(i)
                Else
                    If highest_is_on Then
                        inds(i) = False
                    Else
                        highest_is_on = inds(i)
                    End If
                End If
            End If
        Next i
        
        AtLeastOneADL
        
    End If

End Sub


Private Sub Process()
    Dim on_orders As Boolean
    
    Dim togo As Long, toread As Long
    Dim blocksize As Long
    Dim buf As String
    Dim p As Integer
    Dim i As Integer
    Dim start As Integer
    Dim d As String
        
    Dim formloading As Boolean
    
    On Error GoTo errProcess
        
    formloading = True
'    cmdProcess.Enabled = False
'    cmdOutput.Enabled = False
    
    datafile = FreeFile
    Open infn For Input As #datafile
    
    outfile = FreeFile
    Open outfn For Append As #outfile
    
    While Not EOF(datafile)
        Line Input #datafile, buf
        
        Select Case Trim$(Mid$(buf, START_GROUP_TITLE, LEN_GROUP_TITLE))
            Case "Orders"
                start = START_ORDERS
                Line Input #datafile, buf
            Case "Clin Circs"
                start = START_CLIN
                Line Input #datafile, buf
            Case "Lists", "Categories", "Responses"
                start = START_OTHER
                Line Input #datafile, buf
            Case Else
        End Select
        
        If Mid$(buf, 1, 11) = "Class Time:" Then
            If (Not formloading) Then
                DoPatientSummary
                AssembleOutput
            End If
            formloading = False
            InitIndicators
            classdate = Mid$(buf, 21, 4) & Mid$(buf, 15, 2) & Mid$(buf, 18, 2)
            classtime = Mid$(buf, 26, 4)
        ElseIf Mid$(buf, 1, 8) = "Patient:" Then
            ParsePatientInfo (buf)
'            Print #outfile, buf
        ElseIf Mid$(buf, 1, 10) = "Isolation:" Then
            If Trim$(Mid$(buf, 1, Len(buf) - 1)) > "Isolation:" Then
                inds(10) = True
            End If
        ElseIf Trim$(Mid$(buf, 1, 4)) = "Age:" Then
            If Trim$(Mid$(buf, 8, 3)) = "0" Then
                inds(3) = True
            End If
        ElseIf (start = START_OTHER) Then
            p = FoundInList(Trim$(Mid$(buf, start, LEN_INDICATOR)))
            If (p > 0) Then
                If (Not A000224) Then
                    A000224 = (indary(p).name = "A000224")
                End If
                If (Not A100085) Then
                    A100085 = (indary(p).name = "A100085")
                End If
                If (Not A100088) Then
                    A100088 = (indary(p).name = "A100088")
                End If
                If (Not I090054) Then
                    I090054 = (indary(p).name = "I090054")
                End If
                If (Not A100001) Then
                    A100001 = (indary(p).name = "A100001")
                End If
                
                'Authoritative indicators: overrides all other indicators
                '  and replaces them with their own custom set of indicators.
                If (Not A050346) Then
                    A050346 = (Trim$(Mid$(buf, START_OTHER, LEN_INDICATOR)) = "A050346")
                End If
                If (Not A140340) Then
                    A140340 = (Trim$(Mid$(buf, START_OTHER, LEN_INDICATOR)) = "A140340")
                End If
                If (Not A140341) Then
                    A140341 = (Trim$(Mid$(buf, START_OTHER, LEN_INDICATOR)) = "A140341")
                End If
                If (Not A140342) Then
                    A140342 = (Trim$(Mid$(buf, START_OTHER, LEN_INDICATOR)) = "A140342")
                End If
                If (Not A000829) Then
                    A000829 = (Trim$(Mid$(buf, START_OTHER, LEN_INDICATOR)) = "A000829")
                End If
                If (Not A000830) Then
                    A000830 = (Trim$(Mid$(buf, START_OTHER, LEN_INDICATOR)) = "A000830")
                End If
                
                inds(indary(p).indwinpfs) = True
                ParseAlsoMark (indary(p).also_mark)
            End If
        ElseIf (start = START_ORDERS) Then
            If Trim$(Mid$(buf, start, LEN_ORDERS)) = "IO" Then
                p = FoundInList("IO", Trim$(Mid(buf, START_FREQ, LEN_FREQ)))
                If (p > 0) Then
                    inds(indary(p).indwinpfs) = True
                Else
                    inds(11) = True
                End If
            Else 'takes care of VS, TEMP/PULSE/RESP, FEEDING TUBE
                If Trim$(Mid(buf, START_FREQ, LEN_FREQ)) = "" Then
                    p = FoundInList(Trim$(Mid$(buf, start, LEN_ORDERS)))
                Else
                    p = FoundInList(Trim$(Mid$(buf, start, LEN_ORDERS)), Trim$(Mid(buf, START_FREQ, LEN_FREQ)))
                End If
                If (Not FEEDING) Then
                    FEEDING = Trim$(Mid$(buf, start, LEN_ORDERS)) = "FEEDING TUBE"
                End If
                If (Not NPO) Then
                    NPO = Trim$(Mid$(buf, start, LEN_ORDERS)) = "NPO"
                End If
                If (p > 0) Then
                    inds(indary(p).indwinpfs) = True
                    ParseAlsoMark (indary(p).also_mark)
                    If (Not NPO) Then
                        NPO = (indary(p).name = "NPO")
                    End If
                End If
            End If
        ElseIf (start = START_CLIN) Then
            p = FoundInList(Trim$(Mid$(buf, start, LEN_CLIN)))
            If (p > 0) Then
                inds(indary(p).indwinpfs) = True
'                ParseAlsoMark (indary(p).also_mark)
            End If
        End If
        

        DoEvents
    Wend

    DoPatientSummary
'    For i = 1 To MAX_INDICATORS
'        If (inds(i)) Then
'            Print #outfile, i
'        End If
'    Next i
    AssembleOutput

    Close #datafile
    Close #outfile
    
    Kill infn
'    Set VScol = Nothing
'    MsgBox "Done!  Exit or select another input file."

    Exit Sub
errProcess:
        
End Sub



Private Function FoundInList(s As String, Optional f As Variant) As Integer
    Dim i As Integer
    Dim useoption As Boolean
    Dim dontcare As Boolean

    FoundInList = 0
    useoption = Not IsMissing(f)
    For i = 1 To numind
        If useoption Then
            dontcare = (indary(i).freq = "") 'freq can be anything
            If (s = indary(i).name) And (dontcare Or (InStr(f, indary(i).freq) > 0)) Then
                FoundInList = i
                Exit For
            End If
        ElseIf (s = indary(i).name) Then
            FoundInList = i
            Exit For
        End If
    Next i
End Function

Private Sub InitIndicators()
    Dim i As Integer
    
    For i = 1 To MAX_INDICATORS
        inds(i) = False
    Next i
NPO = False
A000224 = False
A100085 = False
A100088 = False
I090054 = False
FEEDING = False
A050346 = False
A140340 = False
A140341 = False
A140342 = False
A000829 = False
A000830 = False
A100001 = False
    
End Sub
Private Sub InitGroups()
    Dim i As Integer
    
    For i = 1 To MAX_INDICATORS
        grps(i) = 0
    Next i
    
    For i = 1 To 4
        grps(i) = 1
    Next i
    For i = 8 To 9
        grps(i) = 2
    Next i
    For i = 11 To 13
        grps(i) = 3
    Next i
    For i = 14 To 16
        grps(i) = 4
    Next i
    For i = 17 To 19
        grps(i) = 5
    Next i
    For i = 20 To 22
        grps(i) = 6
    Next i
    For i = 24 To 26
        grps(i) = 7
    Next i
End Sub

Private Function ItemExists(col As Collection, key As String) As Boolean
    Dim dummy As Variant
    On Error Resume Next
    dummy = col.Item(key)
    ItemExists = (Err <> 5)
End Function

'Private Sub RemoveAllItems()
'    Do While VScol.Count
'        VScol.Remove 1
'    Loop
'End Sub

Private Sub AtLeastOneADL()

    If Not (inds(1) Or inds(2) Or inds(3) Or inds(4)) Then
        inds(1) = True
    End If
End Sub

Private Sub ParseAlsoMark(s As String)
    Dim comma_pos As Integer
    Dim ind As Integer
    
    'for example:   29,31  is the also_mark string for RESTRAINTS
    
    Do
        comma_pos = InStr(s, ",")
        If (comma_pos = 0) Then
            ind = val(s)
        Else
            ind = val(Mid$(s, 1, comma_pos - 1))
            s = Mid$(s, comma_pos + 1, Len(s) - comma_pos)
        End If
        
        inds(ind) = True
        
    Loop Until comma_pos = 0
End Sub

Private Sub ParsePatientInfo(s As String)
    Dim fullname As String
    Dim commapos As Integer
    
    fullname = Trim$(Mid$(s, 11, 30))
    commapos = InStr(fullname, ",")
    If commapos = 0 Then
        lastname = fullname
        firstname = ""
    Else
        lastname = Mid$(fullname, 1, commapos - 1)
        firstname = Mid$(fullname, commapos + 1, Len(fullname) - commapos)
    End If
    unitname = Trim$(Mid$(s, 41, 11))
    roomname = Trim$(Mid$(s, 52, 7))
    bedname = Trim$(Mid$(s, 59, 4))
    acctnum = Trim$(Mid$(s, 63, Len(s) - 63 + 1))
End Sub


Private Sub AssembleOutput()
    Dim outstr As String
    Dim i As Integer
    
    outstr = Space$(9) & unitname
    outstr = outstr & Space$(26 - Len(outstr))
    outstr = outstr & unitname '27
    outstr = outstr & Space$(60 - Len(outstr))
    outstr = outstr & classdate & Space$(1) '61
    outstr = outstr & acctnum & Space$(20 - Len(acctnum) + 1)
    outstr = outstr & lastname & Space$(32 - Len(lastname) + 1)
    outstr = outstr & firstname & Space$(32 - Len(firstname) + 1)
    outstr = outstr & Space$(32 + 1)
    outstr = outstr & roomname & Space$(8 - Len(roomname) + 1)
    outstr = outstr & bedname & Space$(4 - Len(bedname) + 1)
    outstr = outstr & classdate & classtime & Space$(1)
    outstr = outstr & Space$(162)
    For i = 1 To MAX_INDICATORS
        If (inds(i)) Then
            outstr = outstr & "Y"
        Else
            outstr = outstr & "N"
        End If
    Next i
    
    Print #outfile, outstr
    
End Sub


