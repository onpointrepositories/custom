VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form Affinity 
   Caption         =   "Affinity-WinPFS Indicator Translator"
   ClientHeight    =   3030
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7035
   Icon            =   "Affinity.frx":0000
   ScaleHeight     =   3030
   ScaleWidth      =   7035
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdDictionary 
      Caption         =   "Load Dictionary"
      Height          =   375
      Left            =   4440
      TabIndex        =   6
      Top             =   2580
      Width           =   1455
   End
   Begin VB.CommandButton cmdExit 
      Caption         =   "Exit"
      Height          =   375
      Left            =   6120
      TabIndex        =   5
      Top             =   2580
      Width           =   855
   End
   Begin VB.TextBox Text2 
      Height          =   375
      Left            =   1800
      TabIndex        =   4
      Top             =   1140
      Width           =   3495
   End
   Begin VB.CommandButton cmdProcess 
      Caption         =   "Process"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   2520
      TabIndex        =   3
      Top             =   1920
      Width           =   1455
   End
   Begin VB.CommandButton cmdOutput 
      Caption         =   "Select Output File"
      Height          =   435
      Left            =   120
      TabIndex        =   2
      Top             =   1140
      Width           =   1455
   End
   Begin VB.CommandButton cmdSource 
      Caption         =   "Select Source File"
      Height          =   435
      Left            =   120
      TabIndex        =   1
      Top             =   420
      Width           =   1455
   End
   Begin VB.TextBox Text1 
      Height          =   375
      Left            =   1800
      TabIndex        =   0
      Top             =   420
      Width           =   3495
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   6480
      Top             =   120
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
End
Attribute VB_Name = "Affinity"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const MAX_INDICATORS = 32

Dim infn As String
Dim outfn As String
Dim dicfn As String
Dim dicfile As Integer
Private Type affindicator
  indwinpfs As Integer
  name As String
  freq As String
  also_mark As String
End Type
Dim indary() As affindicator
Dim numind As Integer

Dim datafile As Integer
Dim outfile As Integer

Dim inds(32) As Boolean
Dim grps(32) As Integer

Dim lastname As String
Dim firstname As String
Dim unitname As String
Dim roomname As String
Dim bedname As String
Dim acctnum As String
Dim classdate As String
Dim classtime As String

Dim NPO As Boolean
Dim A000224 As Boolean
Dim A100085 As Boolean
Dim A100088 As Boolean
Dim I090054 As Boolean
Dim FEEDING As Boolean
Dim A050346 As Boolean
Dim A140340 As Boolean
Dim A140341 As Boolean
Dim A140342 As Boolean

Private Sub cmdExit_Click()
    Unload Me
End Sub

Private Sub cmdSource_Click()
    CommonDialog1.CancelError = True                'raise error on cancel
    
    CommonDialog1.Flags = cdlOFNHideReadOnly Or cdlOFNFileMustExist Or cdlOFNNoReadOnlyReturn
    CommonDialog1.Filter = "All Files (*.*)|*.*|" & _
                           "Data Files (*.txt,*.dat)|*.txt;*.dat|"
    CommonDialog1.FilterIndex = 2
    CommonDialog1.ShowOpen

    Text1 = CommonDialog1.filename
    infn = Text1
    
    cmdOutput.Enabled = (Text1 <> "")
    
End Sub
Private Sub cmdOutput_Click()
    CommonDialog1.CancelError = True                'raise error on cancel
    
    CommonDialog1.Flags = cdlOFNHideReadOnly
    CommonDialog1.Filter = "All Files (*.*)|*.*|" & _
                           "Data Files (*.txt,*.dat)|*.txt;*.dat|"
    CommonDialog1.FilterIndex = 2
    CommonDialog1.ShowOpen

    Text2 = CommonDialog1.filename
    outfn = Text2
    
    cmdProcess.Enabled = (Text2 <> "")
    
End Sub



Private Sub Form_Load()
    If (LoadDictionary) Then
        cmdSource.Enabled = True
        cmdOutput.Enabled = False
        cmdProcess.Enabled = False
        cmdDictionary.Enabled = False
    Else
        cmdSource.Enabled = False
        cmdOutput.Enabled = False
        cmdProcess.Enabled = False
        cmdDictionary.Enabled = False
    End If
    
    InitGroups

End Sub

Private Sub DoPatientSummary()
    Dim highest_is_on As Boolean
    Dim g As Integer
    Dim i As Integer
    
    If (A050346 Or A140340 Or A140341 Or A140342) Then
        'authoritative indicator overrides all others marked
        For i = 1 To MAX_INDICATORS
            inds(i) = False
        Next i
        If (A050346) Then
            inds(2) = True
            inds(11) = True
            inds(19) = True
            inds(22) = True
            inds(27) = True
        ElseIf (A140340) Then
            inds(2) = True
            inds(11) = True
            inds(14) = True
            inds(19) = True
            inds(22) = True
        ElseIf (A140341) Then
            inds(3) = True
            inds(12) = True
            inds(15) = True
            inds(18) = True
            inds(22) = True
        ElseIf (A140342) Then
            inds(3) = True
            inds(5) = True
            inds(13) = True
            inds(15) = True
            inds(19) = True
            inds(22) = True
            inds(27) = True
            inds(28) = True
            inds(29) = True
        End If
    Else
        If (NPO And A000224) Or (A100085 And A000224) Or (A100085 And A100088) Or _
           (I090054 And A000224) Or (FEEDING And A000224) Or (A100088 And A000224) Then
            inds(3) = True
        End If
        
        g = 0
        For i = MAX_INDICATORS To 1 Step -1
            If (grps(i) > 0) Then
                If (grps(i) <> g) Then
                    g = grps(i)
                    highest_is_on = inds(i)
                Else
                    If highest_is_on Then
                        inds(i) = False
                    Else
                        highest_is_on = inds(i)
                    End If
                End If
            End If
        Next i
        
        AtLeastOneADL
        
    End If

End Sub


Private Sub cmdProcess_Click()
    Dim on_orders As Boolean
    
    Dim togo As Long, toread As Long
    Dim blocksize As Long
    Dim buf As String
    Dim p As Integer
    Dim i As Integer
    Dim start As Integer
    Dim d As String
        
    Dim formloading As Boolean
        
    formloading = True
    cmdProcess.Enabled = False
    cmdOutput.Enabled = False
    
    datafile = FreeFile
    Open infn For Input As #datafile
    
    outfile = FreeFile
    Open outfn For Output As #outfile
    
    While Not EOF(datafile)
        Line Input #datafile, buf
        
        Select Case Trim$(Mid$(buf, 1, 20))
            Case "Orders"
                start = 8
                Line Input #datafile, buf
            Case "Clin Circs"
                start = 33
                Line Input #datafile, buf
            Case "Lists", "Categories", "Responses"
                start = 3
                Line Input #datafile, buf
            Case Else
        End Select
        
        If Mid$(buf, 1, 15) = "Class Date/Time" Then
            If (Not formloading) Then
                DoPatientSummary
                AssembleOutput
            End If
            formloading = False
            InitIndicators
            classdate = Mid$(buf, 26, 4) & Mid$(buf, 23, 2) & Mid$(buf, 20, 2)
            classtime = Mid$(buf, 31, 4)
        ElseIf Mid$(buf, 1, 8) = "Patient:" Then
            ParsePatientInfo (buf)
'            Print #outfile, buf
        ElseIf Mid$(buf, 1, 10) = "Isolation:" Then
            If Trim$(Mid$(buf, 1, Len(buf) - 1)) > "Isolation:" Then
                inds(10) = True
            End If
        ElseIf Trim$(Mid$(buf, 1, 4)) = "Age:" Then
            If Trim$(Mid$(buf, 8, 3)) = "0" Then
                inds(3) = True
            End If
        ElseIf (start = 3) Then
            p = FoundInList(Trim$(Mid$(buf, start, 16)))
            If (p > 0) Then
                If (Not A000224) Then
                    A000224 = (indary(p).name = "A000224")
                End If
                If (Not A100085) Then
                    A100085 = (indary(p).name = "A100085")
                End If
                If (Not A100088) Then
                    A100088 = (indary(p).name = "A100088")
                End If
                If (Not I090054) Then
                    I090054 = (indary(p).name = "I090054")
                End If
                If (Not FEEDING) Then
                    FEEDING = (indary(p).name = "FEEDING TUBE")
                End If
                
                'Authoritative indicators: overrides all other indicators
                '  and replaces them with their own custom set of indicators.
                If (Not A050346) Then
                    A050346 = (Trim$(Mid$(buf, 3, 16)) = "A050346")
                End If
                If (Not A140340) Then
                    A140340 = (Trim$(Mid$(buf, 3, 16)) = "A140340")
                End If
                If (Not A140341) Then
                    A140341 = (Trim$(Mid$(buf, 3, 16)) = "A140341")
                End If
                If (Not A140342) Then
                    A140342 = (Trim$(Mid$(buf, 3, 16)) = "A140342")
                End If
                
                inds(indary(p).indwinpfs) = True
                ParseAlsoMark (indary(p).also_mark)
            End If
        ElseIf (start = 8) Then
            If Trim$(Mid$(buf, start, 18)) = "VS" Then
                p = FoundInList("VS", Trim$(Mid(buf, 31, 15)))
                inds(indary(p).indwinpfs) = True
                ParseAlsoMark (indary(p).also_mark)
            ElseIf Trim$(Mid$(buf, start, 18)) = "IO" Then
                p = FoundInList("IO", Trim$(Mid(buf, 31, 15)))
                If (p > 0) Then
                    inds(indary(p).indwinpfs) = True
                Else
                    inds(11) = True
                End If
            Else
                p = FoundInList(Trim$(Mid$(buf, start, 18)))
                If (p > 0) Then
                    inds(indary(p).indwinpfs) = True
                    ParseAlsoMark (indary(p).also_mark)
                    If (Not NPO) Then
                        NPO = (indary(p).name = "NPO")
                    End If
                End If
            End If
        ElseIf (start = 33) Then
            p = FoundInList(Trim$(Mid$(buf, start, 30)))
            If (p > 0) Then
                inds(indary(p).indwinpfs) = True
'                ParseAlsoMark (indary(p).also_mark)
            End If
        End If
        

        DoEvents
    Wend

    DoPatientSummary
'    For i = 1 To MAX_INDICATORS
'        If (inds(i)) Then
'            Print #outfile, i
'        End If
'    Next i
    AssembleOutput

    Close #datafile
    Close #outfile
'    Set VScol = Nothing
    MsgBox "Done!  Exit or select another input file."
    
End Sub

Private Function LoadDictionary() As Boolean
    Dim buf As String

    numind = 0
    dicfile = FreeFile
    dicfn = "C:\AFFWINPFS.DAT"
    Open dicfn For Input As #dicfile
    While Not EOF(dicfile)
        numind = numind + 1
        Line Input #dicfile, buf
        ReDim Preserve indary(0 To numind)
        indary(numind).indwinpfs = val(Mid$(buf, 1, 2))
        indary(numind).name = Trim$(Mid$(buf, 3, 27))
        indary(numind).freq = Trim$(Mid$(buf, 30, 15))
        indary(numind).also_mark = Trim$(Mid$(buf, 45, 15))
    Wend
    Close #dicfile
    LoadDictionary = True
End Function


Private Function FoundInList(s As String, Optional f As Variant) As Integer
    Dim i As Integer
    Dim useoption As Boolean

    FoundInList = 0
    useoption = Not IsMissing(f)
    For i = 1 To numind
        If useoption Then
            If (s = indary(i).name) And (InStr(f, indary(i).freq) > 0) Then
                FoundInList = i
                Exit For
            End If
        ElseIf (s = indary(i).name) Then
            FoundInList = i
            Exit For
        End If
    Next i
End Function

Private Sub InitIndicators()
    Dim i As Integer
    
    For i = 1 To MAX_INDICATORS
        inds(i) = False
    Next i
NPO = False
A000224 = False
A100085 = False
A100088 = False
I090054 = False
FEEDING = False
A050346 = False
A140340 = False
A140341 = False
A140342 = False
    
End Sub
Private Sub InitGroups()
    Dim i As Integer
    
    For i = 1 To MAX_INDICATORS
        grps(i) = 0
    Next i
    
    For i = 1 To 4
        grps(i) = 1
    Next i
    For i = 8 To 9
        grps(i) = 2
    Next i
    For i = 11 To 13
        grps(i) = 3
    Next i
    For i = 14 To 16
        grps(i) = 4
    Next i
    For i = 17 To 19
        grps(i) = 5
    Next i
    For i = 20 To 22
        grps(i) = 6
    Next i
    For i = 24 To 26
        grps(i) = 7
    Next i
End Sub

Private Function ItemExists(col As Collection, key As String) As Boolean
    Dim dummy As Variant
    On Error Resume Next
    dummy = col.Item(key)
    ItemExists = (Err <> 5)
End Function

'Private Sub RemoveAllItems()
'    Do While VScol.Count
'        VScol.Remove 1
'    Loop
'End Sub

Private Sub AtLeastOneADL()

    If Not (inds(1) Or inds(2) Or inds(3) Or inds(4)) Then
        inds(1) = True
    End If
End Sub

Private Sub ParseAlsoMark(s As String)
    Dim comma_pos As Integer
    Dim ind As Integer
    
    'for example:   29,31  is the also_mark string for RESTRAINTS
    
    Do
        comma_pos = InStr(s, ",")
        If (comma_pos = 0) Then
            ind = val(s)
        Else
            ind = val(Mid$(s, 1, comma_pos - 1))
            s = Mid$(s, comma_pos + 1, Len(s) - comma_pos)
        End If
        
        inds(ind) = True
        
    Loop Until comma_pos = 0
End Sub

Private Sub ParsePatientInfo(s As String)
    Dim fullname As String
    Dim commapos As Integer
    
    fullname = Trim$(Mid$(s, 12, 30))
    commapos = InStr(fullname, ",")
    If commapos = 0 Then
        lastname = fullname
        firstname = ""
    Else
        lastname = Mid$(fullname, 1, commapos - 1)
        firstname = Mid$(fullname, commapos + 1, Len(fullname) - commapos)
    End If
    unitname = Trim$(Mid$(s, 42, 4))
    roomname = Trim$(Mid$(s, 46, 4))
    bedname = Trim$(Mid$(s, 50, 4))
    acctnum = Trim$(Mid$(s, 60, Len(s) - 60 + 1))
End Sub


Private Sub AssembleOutput()
    Dim outstr As String
    Dim i As Integer
    
    outstr = Space$(9) & unitname
    outstr = outstr & Space$(60 - Len(outstr))
    outstr = outstr & classdate & Space$(1) '61
    outstr = outstr & acctnum & Space$(20 - Len(acctnum) + 1)
    outstr = outstr & lastname & Space$(32 - Len(lastname) + 1)
    outstr = outstr & firstname & Space$(32 - Len(firstname) + 1)
    outstr = outstr & Space$(32 + 1)
    outstr = outstr & roomname & Space$(8 - Len(roomname) + 1)
    outstr = outstr & bedname & Space$(4 - Len(bedname) + 1)
    outstr = outstr & classdate & classtime & Space$(1)
    outstr = outstr & Space$(73 + 3 + 3 + 13 + 13 + 13 + 13 + 13 + 9 + 9)
    For i = 1 To MAX_INDICATORS
        If (inds(i)) Then
            outstr = outstr & "Y"
        Else
            outstr = outstr & "N"
        End If
    Next i
    
    Print #outfile, outstr
    
End Sub

Private Sub Text1_Change()
    infn = Text1
    cmdOutput.Enabled = (Text1 <> "")
End Sub

Private Sub Text2_Change()
    outfn = Text2
    cmdProcess.Enabled = (Text2 <> "")
End Sub


