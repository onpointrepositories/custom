Attribute VB_Name = "CerMain"
Option Explicit
'The location of the input is determined by the first line of the dictionary.
'
' This is the main module for CerPFS
' File is formatted as:
'    <Header>
'       <Event>
'       <Event>
'         ...
'
Const TRANSP_FILENAME = "Transparent.txt"
Const TRANSP_INLOG_FILE_LIFE = 5 'days
Const TRANSP_OUTLOG_FILE_LIFE = 30 'days
Const DNLD_FNAME = "CEWINPFS"

'HEADER RECORD
Const MAX_INDICATORS = 50

Const START_ACCT_NUM = 1
Const LEN_ACCT_NUM = 20

Const START_PTNAME = 41 '21
Const LEN_PTNAME = 30

Const START_UNIT = 21 '51
Const LEN_UNIT = 10

Const START_RM = 31 '61
Const LEN_RM = 5

Const START_BED = 36 '66
Const LEN_BED = 5

'EVENT RECORD
Const START_EVENT = 44
Const LEN_EVENT = 16

Const START_RESULT = 78
Const LEN_RESULT = 240

Const START_DATETIME = 60
Const LEN_DATETIME = 17

Const RANGE_DENOMINATOR = 24 * 60   'For proportioning the count of Med/Pulm/Cardio

Const MAX_UNIQUE_CHART_TIMES = 20

Dim g_util As New RegistryUtils

Dim infn As String
Dim fnames() As String
Dim outfn As String
Dim inlogfile As Integer
Dim inlogname As String
Dim outlogfile As Integer
Dim outlogname As String
Dim dbugfile As Integer
Dim dbugname As String

Private Type cerindicator
  indwinpfs As Integer  'winpfs indic number associated with this cerner event
  eventid As String     'Event_CD of this indicator
  chartkey As String    'look for this key in charted result
  check_freq As Boolean 'flag to check frequency
  freq_basis As Integer 'frequency required for this indicator (in minutes)
  act_freq As Integer   'calculated frequency from data (in minutes)
  last_datetime As String 'last date time of event found in this patient
  num_found As Integer  'number of times this was found in this patient
  also_mark As String   'if this indicator is marked, then also mark these.
  charttime(MAX_UNIQUE_CHART_TIMES) As String
End Type

Private Type indicator_data
    checked As Boolean
    also_mark As String
End Type

Dim dicary() As cerindicator
Dim dicnum As Integer
Dim inDirPath As String  'path of input file
Dim winpfspath As String 'path of winpfs
Dim winlogpath As String 'path of winpfs\log
Dim winloadpath As String ' path of winpfs\load_me

Dim datafile As Integer
Dim outfile As Integer

Dim inds(MAX_INDICATORS) As indicator_data
Dim grps(MAX_INDICATORS) As Integer

Dim lastname As String
Dim firstname As String
Dim unitname As String
Dim roomname As String
Dim bedname As String
Dim acctnum As String
Dim classdt As String
Dim classdate As String
Dim classtime As String
Dim nowdt As Date
Dim intime As String
Dim saveintime As String
'Dim event21793372 As Boolean

Dim range As Single  'number of minutes in the scope of time for freq.
Dim dbugon As Boolean
Dim suppressDbugLog As Boolean
Dim alterdateon As Boolean
Dim alterdate As String
Dim pulltimeon As Boolean
Dim pulltime As String
Dim pulldateon As Boolean
Dim pulldate As String
Dim telemetry As Boolean
Dim emus As Boolean
Dim unitnum As Integer
Dim unitary() As String
Dim classinon As Boolean
Dim classindt As String
Dim classouton As Boolean
Dim classoutdt As String


Sub Main()
    Const RANGE_PARAM = "-range="
    Const EFFECTIVE_PARAM = "-effective="
    Const DBUG_ON = "debug"
    Const ALTER_DATE = "-date="
    Const PULL_TIME = "-pulltime="
    Const PULL_DATE = "-pulldate="
    Const CLASS_INDT = "-classindt="   'pos 296 in the import file
    Const CLASS_OUTDT = "-classoutdt=" 'pos 348 in the import file
    Dim n As Integer
    Dim i As Integer
    Dim cmdLine As String
    Dim epos As Integer
    Dim rpos As Integer
    Dim effective As String
    Dim h As String
    Dim t As Date
    Dim adpos As Integer
    
    '-effective=hhmm  This is the time at which the classification is effective.
    '                 The date associated with this time is taken from Now.
    '                 If not specified, then this -effective time is assumed to be Now.
    
    '-pulltime=hhmm  This is a time, on Now's date, to specify the starting point at which
    '                the pull is to look backwards from.  If not specified,
    '                Then this time is assumed to be the effective time.
    '-pulldate=yyyymmdd This is the date of the pulltime.
    
    '-date=yyyymmdd   This is the date which, in combination with the
    '                 -effective time is used to specify the effective datetime.
    '                 In the absence of this parameter, Now's date is used.
    'Special value:  -date=yesterday means Now's yesterday.
    
    '-range=nnnn  This is the number of minutes backwards from the pull time
    '             that defines valid charting events.
    
    nowdt = Now
    cmdLine = LCase(Command$)
    
    rpos = InStr(cmdLine, RANGE_PARAM)
    epos = InStr(cmdLine, EFFECTIVE_PARAM)
    
    dbugon = (InStr(cmdLine, DBUG_ON) > 0)
    suppressDbugLog = False
    
    alterdateon = (InStr(cmdLine, ALTER_DATE) > 0)
    If alterdateon Then
        adpos = InStr(cmdLine, ALTER_DATE)
        If UCase$(Mid$(cmdLine, adpos + Len(ALTER_DATE), 8)) = "YESTERDA" Then
            alterdate = Format(DateAdd("d", -1, g_util.DateOnly(nowdt)), "yyyymmdd")
        Else
            alterdate = Mid$(cmdLine, adpos + 6, 8) 'yyyymmdd
        End If
    End If
    
    pulltimeon = (InStr(cmdLine, PULL_TIME) > 0)
    If pulltimeon Then
        pulltime = Mid$(cmdLine, InStr(cmdLine, PULL_TIME) + Len(PULL_TIME), 4)
    End If
    
    pulldateon = (InStr(cmdLine, PULL_DATE) > 0)
    If pulldateon Then
        pulldate = Mid$(cmdLine, InStr(cmdLine, PULL_DATE) + Len(PULL_DATE), 8)
    End If
    
    classinon = (InStr(cmdLine, CLASS_INDT) > 0)
    If classinon Then
        classindt = Mid$(cmdLine, InStr(cmdLine, CLASS_INDT) + Len(CLASS_INDT), 12)
    End If
    
    classouton = (InStr(cmdLine, CLASS_OUTDT) > 0)
    If classouton Then
        classoutdt = Mid$(cmdLine, InStr(cmdLine, CLASS_OUTDT) + Len(CLASS_OUTDT), 12)
    End If
    

    If epos > 0 Then
        effective = Mid$(cmdLine, InStr(cmdLine, EFFECTIVE_PARAM) + Len(EFFECTIVE_PARAM), 4)
    End If
    
    If rpos > 0 Then
        range = val(Mid$(cmdLine, InStr(cmdLine, RANGE_PARAM) + Len(RANGE_PARAM), 4))
    End If
    
    If range <= 0 Then
        range = 480
    End If
    
    intime = ""
    
    If (LoadDictionary) Then
        InitGroups
        MakeLogFiles
        dprint -1, 0, "alterdate=" & alterdate
        dprint -1, 0, "pulltime=" & pulltime
        dprint -1, 0, "pulldate=" & pulldate
        dprint -1, 0, "effective=" & effective
        dprint -1, 0, "range=" & range
        If IsNull(effective) Then
            effective = ""
        Else
            ' command parameter needs to be in form hhmm ONLY
            If IsNumeric(effective) Then
                If Len(effective) < 4 Then
                    If Len(effective) = 1 Then
                        effective = "000" & effective
                    ElseIf Len(effective) = 2 Then
                        effective = "00" & effective
                    Else
                        effective = "0" & effective
                    End If
                Else
                    effective = Mid$(effective, 1, 4)
                End If
                effective = Mid$(effective, 1, 2) & ":" & Mid$(effective, 3, 2)
                t = CDate(effective)
                If IsDate(t) Then
                    effective = Mid$(effective, 1, 2) & Mid$(effective, 4, 2)
                    i = year(g_util.DateOnly(FileDateTime(inlogname)))
                    intime = CStr(i) & Mid$(inlogname, Len(inlogname) - Len("mmdd.log") + 1, Len("mmdd")) & effective
                    If alterdateon Then
                        intime = alterdate & Mid$(intime, 9, 4)
                    End If
                    saveintime = intime
                End If
            End If
        End If
        n = GetAllInputFilenames
'        i = 0      ONLY PROCESS THE LATEST FILE 11/4/05
        i = n - 1  'ONLY PROCESS THE LATEST FILE 11/4/05
        While i < n
            i = i + 1
            Process
        Wend
        CloseLogFiles
        DeleteOldLogs
    End If

End Sub
Private Function LoadDictionary() As Boolean
    Dim dicfn As String
    Dim dicfile As Integer
    Dim buf As String
    Dim unitfn As String
    Dim unitfile As Integer
    
    unitnum = 0
    unitfile = FreeFile
    unitfn = App.Path & "\CEWINPFS.UNT"
    Open unitfn For Input As #unitfile
    While Not EOF(unitfile)
        Line Input #unitfile, buf
        If Trim$(buf) <> "" Then
            unitnum = unitnum + 1
            ReDim Preserve unitary(0 To unitnum)
            unitary(unitnum) = UCase$(Trim$(Mid$(buf, 1, 16)))
        End If
    Wend
    Close #unitfile
    
    LoadDictionary = True
End Function

Private Function GetAllInputFilenames() As Integer
    'The location is determined by the first line of the dictionary.
    Const INITIAL_BLOCK_SIZE = 24
    Dim i As Integer
    Dim infname As String
   
    outfn = winloadpath & "\" & TRANSP_FILENAME
    
    ReDim fnames(0 To INITIAL_BLOCK_SIZE) As String
    i = 0
    infname = Dir$(inDirPath + "\" & DNLD_FNAME & ".*") 'returns ONLY the filename.  For wildcards.
    While infname <> ""
        If InStr(infname, ".txt") = 0 Then
            i = i + 1
            If (i > UBound(fnames)) Then
                ReDim Preserve fnames(0 To i + INITIAL_BLOCK_SIZE) As String
            End If
            fnames(i) = infname
        End If
        infname = Dir$
    Wend
    
    BubbleSort fnames, i
    
    GetAllInputFilenames = i
 
End Function

Private Sub BubbleSort(arr As Variant, numEls As Integer)
    Dim lLoop1 As Integer
    Dim lLoop2 As Integer
    Dim lTemp As Variant
    
    For lLoop1 = numEls To LBound(arr) Step -1
       For lLoop2 = LBound(arr) + 1 To lLoop1
           If arr(lLoop2 - 1) > arr(lLoop2) Then
              lTemp = arr(lLoop2 - 1)
              arr(lLoop2 - 1) = arr(lLoop2)
              arr(lLoop2) = lTemp
           End If
       Next lLoop2
   Next lLoop1
End Sub

Private Sub DoPatientSummary()
    Dim highest_is_on As Boolean
    Dim g As Integer
    Dim i As Integer
    Dim p As Integer
    
    'Here is where you now have to go through the frequencies to
    'determine which indicators to mark for this pt.
    
    'First, set the act_freq to 1440 if num_found is 1
    'If .num_found > 0 AND .act_freq=0 then change .act_freq to 1440.
    'Leave oral care score alone.  Oral care score is in act_freq.
    For i = 1 To dicnum
        If dicary(i).num_found = 1 And dicary(i).eventid <> "21793956" And dicary(i).eventid <> "530719" Then
            dicary(i).act_freq = 1440
        End If
    Next i
    
    telemetry = CheckTelemetry
    emus = CheckEMUS
    
    'Next, loop the dictionary to find the indicator.
    For i = 1 To dicnum
        If dicary(i).num_found > 0 Then
            p = FindClosestFreq(i)
            If (p > 0) Then
            If (dicary(i).eventid = "200748") Then 'respiratory rate
              'Future dictionary changes: if 200748 is used for a different
              'winpfs indicator other than 17-19, then this check must change.
                inds(dicary(p).indwinpfs).checked = True 'inds(dicary(p).indwinpfs).checked Or CheckVitals(True)
                dprint dicary(p).indwinpfs, CInt(inds(dicary(p).indwinpfs).checked), "from CheckVitals"
            ElseIf (dicary(i).eventid = "92663") Then
                g = CheckBlood
                If g > 0 Then
                    inds(11).checked = True
                    dprint 11, 1, " by I&O alone, but still can be overridden by mutuality."
                End If
            ElseIf Check3Filter(i) Then
                dprint 3, 0, "passed Check3Filter"
                If Check3 Then
                    inds(3).checked = True
                    dprint 3, 1, "passed Check3"
                End If
            ElseIf Check6Filter(i) Then
                dprint 6, 1, "passed Check6Filter"
                inds(6).checked = True
                'If event21793372 And InComa Then
                If InComa Then
                    inds(6).checked = False
                    dprint 6, 0, "event 21793372 and In Coma"
                End If
            ElseIf Check7Filter(i) Then
                dprint 7, 1, "passed Check7Filter"
                inds(7).checked = True
                'If event21793372 And InComa Then
                If InComa Then
                    inds(7).checked = False
                    dprint 7, 0, "event 21793372 and In Coma"
                End If
            ElseIf Check11Filter(i) Then
'                If Check11 Then            commented out 11/30/05
'                    inds(dicary(p).indwinpfs).checked = True
'                End If
                If dicary(i).eventid = "22124174" And UCase$(dicary(i).chartkey) = "FEEDING TUBE RESIDUAL CHE" Then
                    g = CheckBlood
                    If g = 11 Then
                        inds(11).checked = True
                    ElseIf g >= 12 Then
                        inds(12).checked = True
                    End If
                Else
                    inds(dicary(p).indwinpfs).checked = True
                End If
                dprint 11, 1, "passed Check11Filter"
            ElseIf (dicary(p).indwinpfs = 12) Then
                If dicary(i).eventid = "21793665" And _
                   (UCase$(dicary(i).chartkey) = "INTERMITTENT" Or _
                    UCase$(dicary(i).chartkey) = "CONTINUOUS" Or _
                    UCase$(dicary(i).chartkey) = "BOLUS") Then
                    g = CheckBlood
                    If g >= 12 Then
                        inds(12).checked = True
                        dprint 12, 1, "passed I&O Intermitt,Cont,or Bolus with indicator=" & g
                    End If
                Else
                    g = CheckBlood
                    If g > 0 Then
                        inds(g).checked = True
                        dprint 12, 1, "passed I&O with indicator=" & g
                    End If
                End If
            ElseIf (dicary(p).indwinpfs = 27) Then
                inds(27).checked = True
                If InComa And Not FamilyPresent Then
                    inds(27).checked = False
                End If
            ElseIf (dicary(p).indwinpfs = 28) Then
                inds(28).checked = True
                If (InComa Or Confused) And Not FamilyPresent Then
                    inds(28).checked = False
                End If
            ElseIf dicary(p).eventid = "21793085" Then 'pulse rate
                If CheckVitals Then
                    inds(dicary(p).indwinpfs).checked = True
                    dprint dicary(p).indwinpfs, 1, "passed CheckVitals"
                End If
            ElseIf dicary(p).eventid = "28153581" Then
                If CheckOralCare Then
                    inds(dicary(p).indwinpfs).checked = True
                    dprint dicary(p).indwinpfs, 1, "passed CheckOralCare"
                End If
            ElseIf dicary(p).eventid = "530719" Then
                If CheckBraden Then
                    inds(dicary(p).indwinpfs).checked = True
                    dprint dicary(p).indwinpfs, 1, "passed CheckBraden"
                End If
            ElseIf (dicary(i).chartkey = "Dressing Changed") Then
'                If CheckCentral(i) > 0 Then
'                    inds(25).checked = True
'                Else
'                    inds(24).checked = True
'                End If
                If CheckCentral(i) > 0 Then
                    inds(24).checked = True
                    dprint 24, 1, "passed Dressing Changed with CheckCentral"
                End If
            Else
                inds(dicary(p).indwinpfs).checked = True
                dprint dicary(p).indwinpfs, 1, "via fall through"
            End If
            
            If (dicary(p).num_found > 0 And Not inds(dicary(p).indwinpfs).checked) Then
                dprint dicary(p).indwinpfs, -1, "***FAILED TO TURN ON***"
            End If
            
            If inds(dicary(p).indwinpfs).checked Then
                If inds(dicary(p).indwinpfs).also_mark <> "" Then
                    'Only do the also mark if it originally arose from an also-mark ind.
                    ParseAlsoMark (inds(dicary(p).indwinpfs).also_mark)
                End If
            End If
            End If
        End If
    Next i
    
    If InComa Then
        inds(6).checked = False
        inds(7).checked = False
    End If
            
    '03/23/06: ADL 2 is the minimum as decided in phone mtg on 3/22/06
    If Not (inds(1).checked Or inds(2).checked Or inds(3).checked Or inds(4).checked) Then
        dprint 2, 1, "via AtLeastOneADL"
    End If
    inds(2).checked = True
    
    'Now, if there are any mutually exclusive indicators marked
    'then remove the lesser ones.
    g = 0
    For i = MAX_INDICATORS To 1 Step -1
        If (grps(i) > 0) Then
            If (grps(i) <> g) Then
                g = grps(i)
                highest_is_on = inds(i).checked
            Else
                If highest_is_on Then
                    inds(i).checked = False
                Else
                    highest_is_on = inds(i).checked
                End If
            End If
        End If
    Next i
    
    'Make sure there is at least 1 ADL marked
    '3/23/06: No longer need this
    'AtLeastOneADL
        
End Sub

Private Sub Process()
    Dim on_orders As Boolean
    
    Dim togo As Long, toread As Long
    Dim blocksize As Long
    Dim buf As String
    Dim p As Integer
    Dim i As Integer
    Dim start As Integer
    Dim d As String
    Dim formloading As Boolean
    Dim s As String
    Dim rows() As String
    Dim maxrow As Long
    Dim irow As Long
    Dim isodt As String
    Dim sNowdt As String
    Dim charting As String
    Dim prevcharting As String
    Dim res As String
    Dim cond1 As Boolean
    Dim cond2 As Boolean
    Dim rangedate As Date
    Dim rangedt As String
    Dim intimedate As Date
    Dim n As Integer
    Dim dupfound As Boolean
    Dim eventid As String
    
    On Error GoTo errProcess
        
    formloading = True
'    cmdProcess.Enabled = False
'    cmdOutput.Enabled = False
    
    datafile = FreeFile
    Open "c:\client folders\mccglog\transpin_0315_4am.log" For Input As #datafile
    s = Input(LOF(datafile), datafile)
    rows() = Split(s, vbCrLf)
    maxrow = UBound(rows)
    
   
    For irow = 0 To maxrow
        buf = rows(irow)
        
        If Trim$(Mid$(buf, 1, 1)) = Chr$(12) Then 'do nothing -- pg break
        ElseIf Trim$(Mid$(buf, 1, 4)) = "" Then 'Event record
' There may be more than 1 dictionary entry with the same
' event code but with a different frequency.  So use the first occurrence
' of this event to keep the running frequency.  Then at then end,
' locate the appropriate indicator for the calculated frequency.
            isodt = Trim$(Mid$(buf, 69, 2)) & Trim$(Mid$(buf, 72, 2))
            'dprint -1, 0, "intimedate=" & intimedate
            rangedate = DateAdd("n", -range, intimedate) ' range minutes before intime
            rangedt = Format(rangedate, "yyyymmddhhnn")
            'dprint -1, 0, "rangedt=" & rangedt
            If isodt <= "0130" Or isodt >= "0400" Then
                Print #inlogfile, buf
            End If
        Else 'Header record
            Print #inlogfile, buf
        End If
        

        DoEvents
    Next

    CloseLogFiles
    Close #datafile
    Close #outfile
    Close #infn
    
    Exit Sub
errProcess:
        
End Sub




Private Function FoundInList(id As String, ByRef s As String) As Integer
    Dim i As Integer
    Dim p As Integer

    FoundInList = 0
    For i = 1 To dicnum
        If (id = dicary(i).eventid) Then
            If (Trim$(dicary(i).chartkey) <> "") Then
                p = InStr(1, s, dicary(i).chartkey, vbTextCompare)
                If (p > 0) Then
                    FoundInList = i
                    s = Replace(s, dicary(i).chartkey, "", , , vbTextCompare)
                    dprint 0, 0, "Dictionary item found=" & i & " id=" & id & " charting=" & s & " mapping to=" & dicary(i).indwinpfs
                    Exit For
                End If
            Else 'don't care about matching chartkey
                FoundInList = i
                dprint 0, 0, "Dictionary item found=" & i & " id=" & id & " charting=%don't care%" & " mapping to=" & dicary(i).indwinpfs
                Exit For
            End If
        End If
    Next i
End Function

Private Sub InitIndicators()
    Dim i As Integer
    
    For i = 1 To MAX_INDICATORS
        inds(i).checked = False
        inds(i).also_mark = ""
    Next i
    
End Sub
Private Sub InitGroups()
    Dim i As Integer
    
    For i = 1 To MAX_INDICATORS
        grps(i) = 0
    Next i
    
    For i = 1 To 4
        grps(i) = 1
    Next i
    For i = 8 To 9
        grps(i) = 2
    Next i
    For i = 11 To 13
        grps(i) = 3
    Next i
    For i = 14 To 16
        grps(i) = 4
    Next i
    For i = 17 To 19
        grps(i) = 5
    Next i
    For i = 20 To 22
        grps(i) = 6
    Next i
    For i = 24 To 26
        grps(i) = 7
    Next i
End Sub

Private Function ItemExists(col As Collection, key As String) As Boolean
    Dim dummy As Variant
    On Error Resume Next
    dummy = col.Item(key)
    ItemExists = (Err <> 5)
End Function

Private Sub AtLeastOneADL()

    If Not (inds(1).checked Or inds(2).checked Or inds(3).checked Or inds(4).checked) Then
        inds(1).checked = True
        dprint 1, 1, "via AtLeastOneADL"
    End If
End Sub

Private Sub ParseAlsoMark(s As String)
    Dim comma_pos As Integer
    Dim ind As Integer
    
    'for example:   29,31  is the also_mark string for RESTRAINTS
    
    Do
        comma_pos = InStr(s, ",")
        If (comma_pos = 0) Then
            ind = val(s)
        Else
            ind = val(Mid$(s, 1, comma_pos - 1))
            s = Mid$(s, comma_pos + 1, Len(s) - comma_pos)
        End If
        
        inds(ind).checked = True
        dprint ind, 1, "via Also-mark"
        
    Loop Until comma_pos = 0
End Sub

Private Sub ParsePatientInfo(s As String)
    Dim fullname As String
    Dim commapos As Integer
    
    acctnum = Trim$(Mid$(s, START_ACCT_NUM, LEN_ACCT_NUM))
    
    fullname = Trim$(Mid$(s, START_PTNAME, LEN_PTNAME))
    commapos = InStr(fullname, ",")
    If commapos = 0 Then
        lastname = fullname
        firstname = ""
    Else
        lastname = Mid$(fullname, 1, commapos - 1)
        firstname = Mid$(fullname, commapos + 1, Len(fullname) - commapos)
    End If
    
    unitname = Trim$(Mid$(s, START_UNIT, LEN_UNIT))
    roomname = Trim$(Mid$(s, START_RM, LEN_RM))
    If UCase$(unitname) = "A6M" Then
    Select Case UCase$(roomname)
        Case "A655", "A656", "A657", "A658", "A659", "A660", "A661", "A662", "A663", "A664", "A665"
            unitname = "OBS"
    End Select
    ElseIf UCase$(unitname) = "A3E" Then
    Select Case UCase$(roomname)
        Case "A301", "A302", "A303", "A304", "A305", "A306", "A307", "A308", "A309", "A310"
            unitname = "FBC"
    End Select
    End If
    bedname = Trim$(Mid$(s, START_BED, LEN_BED))
End Sub


Private Sub AssembleOutput()
    Dim outstr As String
    Dim i As Integer
    Dim din As Date
    Dim tin As Date
    Dim ok As Boolean
    
'    ok = False
'    Select Case UCase$(unitname)
'        Case "A4W", "A6M", "A9M"
'            ok = True
'    End Select
    ok = ValidUnit(unitname)
    
    If Not ok Then
        Exit Sub
    End If
    
    outstr = Space$(9) & unitname
    outstr = outstr & Space$(26 - Len(outstr))
    outstr = outstr & unitname '27
    If telemetry And UCase$(unitname) = "A9M" Then
        outstr = outstr & Space$(43 - Len(outstr))
        outstr = outstr & "Telemetry"
    ElseIf emus And UCase$(unitname) = "A4E" Then
        outstr = outstr & Space$(43 - Len(outstr))
        outstr = outstr & "EMUS"
    End If
    outstr = outstr & Space$(60 - Len(outstr))
    outstr = outstr & Mid$(intime, 1, 8) & Space$(1) '61
'    outstr = outstr & classdate & Space$(1) '61
    outstr = outstr & acctnum & Space$(20 - Len(acctnum) + 1) '70
    outstr = outstr & lastname & Space$(32 - Len(lastname) + 1) '91
    outstr = outstr & firstname & Space$(32 - Len(firstname) + 1) '124
    outstr = outstr & Space$(32 + 1) '157
    outstr = outstr & roomname & Space$(8 - Len(roomname) + 1) '190
    outstr = outstr & bedname & Space$(4 - Len(bedname) + 1) '199
    outstr = outstr & intime & Space$(1) '204
'    outstr = outstr & classdate & classtime & Space$(1)
    outstr = outstr & Space$(78 + 1) '217 + 79 = 296
'    If (Not IsNull(intime) And Not IsNull(classdate) And Not IsNull(classtime)) Then
'        If (Mid$(intime, 1, 8) > classdate) Then
'            tin = TimeSerial(val(Mid$(intime, 9, 2)), val(Mid$(intime, 11, 2)), 0)
'            intime = classdate
'            din = DateSerial(val(Mid$(intime, 1, 4)), val(Mid$(intime, 5, 2)), val(Mid$(intime, 7, 2)))
'            intime = Format(din + tin, "yyyymmddhhnn")
'        End If
'        If intime > (classdate & classtime) Then
'            din = DateSerial(val(Mid$(intime, 1, 4)), val(Mid$(intime, 5, 2)), val(Mid$(intime, 7, 2)))
'            tin = TimeSerial(val(Mid$(intime, 9, 2)), val(Mid$(intime, 11, 2)), 0)
'            din = DateAdd("d", -1, din)
'            intime = Format(din + tin, "yyyymmddhhnn")
'        End If
'    End If
    If Not classinon Then '296 + 13 = 309
        outstr = outstr & intime & Space$(12 - Len(intime) + 1)
    Else
        outstr = outstr & classindt & Space$(12 - Len(classindt) + 1)
    End If
    If Not classouton Then
        outstr = outstr & Space$(70) 'go directly to 379 indicators
    Else
        outstr = outstr & Space$(39) '309 + 39 = 348
        outstr = outstr & classoutdt & Space$(12 - Len(classoutdt)) '360
        outstr = outstr & Space$(19)
    End If
    For i = 1 To MAX_INDICATORS
        If (inds(i).checked) Then
            outstr = outstr & "Y"
        Else
            outstr = outstr & "N"
        End If
    Next i
    
    Print #outfile, outstr
    Print #outlogfile, outstr
End Sub


Private Sub CalcFrequency(p As Integer)
    Dim n As Integer
    Dim r As Single
    
    r = range / RANGE_DENOMINATOR
' Use 8 hours as basis
    Select Case dicary(p).indwinpfs
        Case 14 To 22 'indicators 14-22 are to be counted instead of freq.
            If dicary(p).num_found >= 4 * r And dicary(p).num_found <= 12 * r Then
                dicary(p).act_freq = 240
            ElseIf dicary(p).num_found > 12 * r And dicary(p).num_found <= 24 * r Then
                dicary(p).act_freq = 60
            ElseIf dicary(p).num_found > 24 * r Then
                dicary(p).act_freq = 30
            Else
                dicary(p).act_freq = 480
            End If
        Case Else   ' Other values.
            dicary(p).act_freq = range / dicary(p).num_found 'denom will never be zero
    End Select

End Sub

Private Function CerDateToISO(d As String) As String
    Dim century As String
    ' Yields a string of the format yyyymmddhhnn
    
    If val(Mid$(d, 7, 2)) <= 50 Then
        century = "20"
    Else
        century = "19"
    End If
    CerDateToISO = century & Mid$(d, 7, 2) & Mid$(d, 1, 2) & Mid$(d, 4, 2) _
     & Mid$(d, 10, 2) & Mid$(d, 13, 2)

End Function

Private Sub ResetDictionaryFields()
    Dim i As Integer
    
    For i = 1 To dicnum
        dicary(i).num_found = 0
        dicary(i).act_freq = 0
        dicary(i).last_datetime = ""
    Next i

End Sub

Private Function FindClosestFreq(p As Integer) As Integer
    Dim i As Integer
    Dim keep As Integer
    
    keep = 0
    For i = 1 To dicnum
        If dicary(i).eventid = dicary(p).eventid And dicary(i).chartkey = dicary(p).chartkey Then
            If dicary(p).act_freq <= dicary(i).freq_basis Then
                keep = i
            End If
        End If
    Next i
    
    FindClosestFreq = keep
    
End Function

Private Function CheckVitals() As Boolean
    Dim e193099 As Integer  'index of 193099 in dictionary
    Dim e193101 As Integer  '  "      193100    "
    Dim charting As String
    
    suppressDbugLog = True
    
    CheckVitals = False
    charting = " "
    e193099 = FoundInList("193099", charting)
    charting = " "
    e193101 = FoundInList("193101", charting)
    charting = " "
'    e200748 = FoundInList("200748", charting)
'    If isRespiratory Then
'        If dicary(e193099).num_found > 0 And dicary(e193101).num_found > 0 And _
'            dicary(e200748).num_found > 0 Then
'            CheckVitals = True
'        End If
'    ElseIf dicary(e193099).act_freq = dicary(e193101).act_freq Then
'            CheckVitals = True
'    End If
    If dicary(e193099).num_found > 0 And dicary(e193101).num_found > 0 Then
        CheckVitals = True
    End If
    suppressDbugLog = False
    
End Function

Private Function Check3Filter(i As Integer) As Boolean

    Check3Filter = _
       dicary(i).eventid = "21793395" And UCase$(dicary(i).chartkey) = "TOTAL" Or _
       dicary(i).eventid = "21793401" And UCase$(dicary(i).chartkey) = "TOTAL" Or _
       dicary(i).eventid = "22124195" And UCase$(dicary(i).chartkey) = "NPO" Or _
       dicary(i).eventid = "21793665" And UCase$(dicary(i).chartkey) = "INTERMITTENT" Or _
       dicary(i).eventid = "21793665" And UCase$(dicary(i).chartkey) = "BOLUS"

End Function

Private Function Check3() As Boolean
    Dim bathtotal As Integer
    Dim eatingtotal As Integer
    Dim npo As Integer
    Dim interm As Integer
    Dim bolus As Integer
    Dim continuous As Integer
    Dim charting As String
    suppressDbugLog = True
    
    Check3 = False
    charting = "total"
    bathtotal = FoundInList("21793395", charting)
    charting = "total"
    eatingtotal = FoundInList("21793401", charting)
    charting = "npo"
    npo = FoundInList("22124195", charting)
    charting = "intermittent"
    interm = FoundInList("21793665", charting)
    charting = "bolus"
    bolus = FoundInList("21793665", charting)
    charting = "continuous"
    continuous = FoundInList("21793665", charting)
    If dicary(bathtotal).num_found > 0 And _
       (dicary(eatingtotal).num_found > 0 Or dicary(npo).num_found > 0 Or _
        dicary(interm).num_found > 0 Or dicary(bolus).num_found > 0 Or dicary(continuous).num_found > 0) Then
        Check3 = True
    End If
    suppressDbugLog = False

End Function

Private Function Check6Filter(i As Integer) As Boolean

    Check6Filter = _
        dicary(i).eventid = "21793446" And UCase$(dicary(i).chartkey) = "TRACHEOSTOMY" Or _
        dicary(i).eventid = "193845" And UCase$(dicary(i).chartkey) = "VENTILATOR"
        
End Function

Private Function InComa() As Boolean
    Dim coma As Integer
    Dim semicoma As Integer
    Dim charting As String
    
    suppressDbugLog = True
    charting = "comatose"
    coma = FoundInList("21793372", charting)
    charting = "semicomatose"
    semicoma = FoundInList("21793372", charting)
    InComa = dicary(coma).num_found > 0 Or dicary(semicoma).num_found > 0
    suppressDbugLog = False
    
End Function

Private Function Check7Filter(i As Integer) As Boolean
    Dim charting As String
    Dim unresp As Integer
    
    suppressDbugLog = True
    Check7Filter = False
    If dicary(i).eventid = "21793689" And _
        (UCase$(dicary(i).chartkey) = "CONFUSED" Or _
         UCase$(dicary(i).chartkey) = "DISORIENTED" Or _
         UCase$(dicary(i).chartkey) = "COMBATIVE") Then
        charting = "unresponsive"
        unresp = FoundInList("21793689", charting)
        Check7Filter = (unresp = 0)
    End If
    suppressDbugLog = False

End Function

Private Function Check11Filter(i As Integer) As Boolean

    Check11Filter = _
        dicary(i).eventid = "22124174" And UCase$(dicary(i).chartkey) = "FEEDING TUBE RESIDUAL CHE" And dicary(i).num_found > 0 Or _
        dicary(i).eventid = "282664" And dicary(i).num_found > 0 And _
            (UCase$(dicary(i).chartkey) = "BED" Or _
             UCase$(dicary(i).chartkey) = "SLING" Or _
             UCase$(dicary(i).chartkey) = "UPRIGHT")

End Function

' Commented out per 11/30/05 review - not a valid criterion
'
'Private Function Check11() As Boolean
'    Dim ivaccess As Integer
'    Dim charting As String
'
'    charting = "yes"
'    ivaccess = FoundInList("28179262", charting)
'    Check11 = dicary(ivaccess).num_found > 0
'
'End Function

Private Function CheckBlood() As Integer
    Dim bloodoutput As Integer
    Dim charting As String

    suppressDbugLog = True
    CheckBlood = 0
    charting = "INTAKE / OUTPUT / SHIFT CARE"
    bloodoutput = FoundInList("92663", charting)
    If dicary(bloodoutput).num_found > 0 Then
        If dicary(bloodoutput).act_freq <= 60 Then
            CheckBlood = 13
        ElseIf dicary(bloodoutput).act_freq <= 120 Then
            CheckBlood = 12
        ElseIf dicary(bloodoutput).act_freq <= 480 Then
            CheckBlood = 11
        End If
    End If
    suppressDbugLog = False
    
End Function

Private Function CheckOralCare() As Boolean
    Dim oralscore As Integer
    Dim charting As String
    
    suppressDbugLog = True
    charting = " "
    oralscore = FoundInList("21793956", charting)
    CheckOralCare = dicary(oralscore).act_freq > 6
    suppressDbugLog = False
End Function

Private Function CheckCentral(i As Integer) As Integer
    Dim central As Integer
    Dim charting As String
    
    suppressDbugLog = True
    charting = "central"
    If (dicary(i).eventid = "28161419") Then
        central = FoundInList("28161401", charting)
    ElseIf (dicary(i).eventid = "28185338") Then
        central = FoundInList("28185323", charting)
    ElseIf (dicary(i).eventid = "28185368") Then
        central = FoundInList("28185356", charting)
    ElseIf (dicary(i).eventid = "28185398") Then
        central = FoundInList("28185389", charting)
    ElseIf (dicary(i).eventid = "28308753") Then
        central = FoundInList("28308759", charting)
    ElseIf (dicary(i).eventid = "28179318") Then
        central = FoundInList("28179293", charting)
    End If
    
    CheckCentral = dicary(central).num_found
    suppressDbugLog = False
            
End Function

Private Function CheckBraden() As Boolean
    Dim braden As Integer
    Dim charting As String
    
    suppressDbugLog = True
    charting = " "
    braden = FoundInList("530719", charting)
    CheckBraden = dicary(braden).act_freq <= 18
    suppressDbugLog = False
End Function

'Public Function Rename_File(ByVal Location As String, ByVal name As String, ByVal changeto As String) As Boolean
'
'    On Error GoTo ErrorHandler
'
'    If FileExists(Location & name) Then
'        If FileExists(Location & changeto) Then
'            Kill Location & changeto
'            Name Location & name As Location & changeto
'            Rename_File = True
'            Exit Function
'        Else
'            Rename_File = FileExists(Location & changeto)
'            Exit Function
'        End If
'    End If
'ErrorHandler:
' Err.Clear
'End Function

Private Sub MakeLogFiles()
    Const HKEY_LOCAL_MACHINE = &H80000002
    Dim dt As Variant
    
    If Not g_util.GetKeyValue(HKEY_LOCAL_MACHINE, "SOFTWARE\Quadramed\WinPFS", "Directory", winpfspath) Then
        winpfspath = App.Path
    End If

    If g_util.DirExists(winpfspath) Then
        winlogpath = winpfspath & "\log"
        winloadpath = winpfspath & "\load_me"
        If Not g_util.DirExists(winpfspath) Then
            MkDir$ (winlogpath)
        End If
    Else
        winlogpath = App.Path
        winloadpath = winlogpath
    End If
    
    dt = nowdt
    inlogname = winlogpath & "\TranspIn_" & Format$(dt, "mmdd") & ".log"
    outlogname = winlogpath & "\TranspOut_" & Format$(dt, "mmdd") & ".log"
    dbugname = winlogpath & "\DbugTransp_" & Format$(dt, "mmdd") & ".log"
    
    inlogfile = FreeFile
    Open inlogname For Append As #inlogfile
    Print #inlogfile, "**** WinPFS Transparent Classification Input    Time=" & nowdt & " ****"
    
    outlogfile = FreeFile
    Open outlogname For Append As #outlogfile
    Print #outlogfile, "**** WinPFS Transparent Classification Output    Time=" & nowdt & " ****"
    
    If dbugon Then
        dbugfile = FreeFile
        Open dbugname For Append As #dbugfile
        Print #dbugfile, "**TRANSPARENT TRANSLATION DEBUGGING MODE**"
    End If
    
End Sub

'Private Sub SetIntimeFromCommand()
'    Dim i As Integer
'    Dim h As String
'    Dim t As Date
'
'    ' command parameter needs to be in form hh:mm ONLY
'    intime = ""
'    h = Command$
'    If IsDate(h) Then
'        t = g_util.DateTimeOrNull(h)
'        If Not IsNull(t) Then
'            h = Format$(t, "hhNN")
'            i = year(g_util.DateOnly(FileDateTime(inlogname)))
'            intime = CStr(i) & Mid$(inlogname, Len(inlogname) - Len("mmdd.log") + 1, Len("mmdd")) & h
'        End If
'    End If
'
'End Sub

Private Sub CloseLogFiles()

    Close inlogfile
    Close outlogfile
    If dbugon Then
        Close dbugfile
    End If

    'inlogfile's dt = mid$(inlogname,len(inlogname)-len("mmdd.txt")+1,len("mmddhh.txt"))
'    FileCopy outfn, winlogpath & "\TranspOut_" & Mid$(inlogname, Len(inlogname) - Len("mmddhh.txt") + 1, Len("mmddhh.txt"))

End Sub

Private Sub DeleteOldLogs()
    Dim temp As String
    Dim i As Integer
    Dim dt As Variant

    ' Delete old log files
    ' (allow the interface to be down for up to 5 days)
    dt = DateAdd("d", -TRANSP_INLOG_FILE_LIFE, nowdt)
    For i = 1 To 5
        temp = winlogpath & "\TranspIn_" & Format$(dt, "mmdd") & ".log"
        If g_util.FileExists(temp) Then
            Kill temp
        End If
        dt = DateAdd("d", -1, dt)
    Next i

    dt = DateAdd("d", -TRANSP_OUTLOG_FILE_LIFE, nowdt)
    For i = 1 To 5
        temp = winlogpath & "\TranspOut_" & Format$(dt, "mmdd") & ".log"
        If g_util.FileExists(temp) Then
            Kill temp
        End If
        dt = DateAdd("d", -1, dt)
    Next i

End Sub


Private Sub dprint(ind As Integer, status As Integer, s As String)
    Dim sstat As String

    If Not dbugon Or suppressDbugLog Then
        Exit Sub
    End If
    
    If ind = -1 Then
        Print #dbugfile, s
    ElseIf ind = -2 Then
        Print #dbugfile, "The following event was Out of Range and was rejected."
        Print #dbugfile, s
    ElseIf ind = 0 Then
        Print #dbugfile, "    " & s
    Else
        If (status = 1) Or (status = -1) Then
            sstat = "-TURNED ON-"
        Else
            sstat = "   "
        End If
        Print #dbugfile, "    Indicator " & ind & sstat & s
    End If
            
End Sub

Private Function CheckTelemetry() As Boolean
    Dim tel1 As Integer
    Dim tel2 As Integer
    Dim charting As String
'20*272342            1440            Telemetry
'20*282736            1440            Telemetry

    suppressDbugLog = True
    CheckTelemetry = False
    charting = "Telemetry"
    tel1 = FoundInList("272342", charting)
    tel2 = FoundInList("282736", charting)
    If dicary(tel1).num_found > 0 Or dicary(tel2).num_found > 0 Then
        CheckTelemetry = True
    End If
    suppressDbugLog = False
End Function

Private Function CheckEMUS() As Boolean
    Dim e As Integer
    Dim charting As String
    
    suppressDbugLog = True
    CheckEMUS = False
    charting = "EMUS Protocol"
    e = FoundInList("22124177", charting)
    If dicary(e).num_found > 0 Then
        CheckEMUS = True
    End If
    suppressDbugLog = False
End Function

Private Function ValidUnit(u As String) As Boolean
    Dim i As Integer
    
    For i = 1 To unitnum
        If UCase$(u) = unitary(i) Then
            ValidUnit = True
            Exit For
        End If
    Next i

End Function

Private Function FamilyPresent() As Boolean
    Dim c As Integer
    Dim fm As Integer
    Dim f As Integer
    Dim p As Integer
    Dim so As Integer
    Dim s As Integer
    Dim o As Integer
    Dim charting As String
    
    suppressDbugLog = True
    FamilyPresent = False
    charting = "Child"
    c = FoundInList("21793267", charting)
    charting = "Family Member"
    fm = FoundInList("21793267", charting)
    charting = "Friend"
    f = FoundInList("21793267", charting)
    charting = "Parent"
    p = FoundInList("21793267", charting)
    charting = "Significant other"
    so = FoundInList("21793267", charting)
    charting = "Spouse"
    s = FoundInList("21793267", charting)
    charting = "Other"
    o = FoundInList("21793267", charting)
    
    If dicary(c).num_found > 0 Or dicary(fm).num_found > 0 Or dicary(f).num_found > 0 Or _
        dicary(p).num_found > 0 Or dicary(so).num_found > 0 Or dicary(s).num_found > 0 Or _
        dicary(o).num_found > 0 Then
        FamilyPresent = True
    End If
    suppressDbugLog = False

End Function

Private Function Confused() As Boolean
    Dim c As Integer
    Dim d As Integer
    Dim charting As String
    
    suppressDbugLog = True
    Confused = False
    c = FoundInList("21793689", "Confused")
    d = FoundInList("21793689", "Disoriented")
    If dicary(c).num_found > 0 Or dicary(d).num_found > 0 Then
        Confused = True
    End If
    suppressDbugLog = False

End Function
