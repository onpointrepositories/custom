Attribute VB_Name = "NorthCountryInpatient"
Option Explicit
'
' Mission Inpatient
'
' This processes one patient using the Inpatient methodology.
' CHANGE 7/2013:  For ICU only:  set m_pat.pull_start = beginning of last Service (ICU, MED=non-ICU).
'                  If service is non-ICU, then give class txarea = MED.
'
' All search functions use exact match for code.
'
' result_like looks for LIKE matches in the result, except where EXACT_MATCH_PREFIX is pre-appended to target.
' result_list looks for any one of a list of words exactly as the result.
'
Private Const MAX_INDS = 50
Private Const MAX_PROCS = 20

Private Const EXACT_MATCH_PREFIX = "&!"
Private Const LIKE_PREFIX = "%!"

Private Type indicator_data
    checked     As Boolean
    group       As Integer
    also_mark   As String                   'not used at Arnot (handy with user-defined indicators?)
End Type

Private Type procdata                       'not used at Arnot
    pnum    As Integer
    start   As Date
    finish  As Date
    isvalid As Boolean 'used in combining procs
    pindex As Integer 'used in combining procs = the index of procs whose start/finish times are equal to this one
End Type
Private Type ocdata                       'not used at Arnot
    checked As Boolean
    pnum    As Integer
    start   As String
End Type

Private inds(MAX_INDS)          As indicator_data

Private numprocs                As Integer
Private numoutcomes             As Integer
Private procs(MAX_PROCS)        As procdata
Private oc(MAX_PROCS)           As ocdata

Private WHERE_ENCOUNTER         As String
Private AND_UNIT                As String
Private AND_PULL_RANGE          As String
Private AND_OUTCOMES_RANGE      As String
Private AND_ARRIVAL             As String
Private BUCKET_CALC             As String

Private m_pat                   As PatientInfo
Private m_assist_count          As Integer

Private Enum SearchMode
    SearchDefault
    Searchpullrange             'search within the current pull     -- default
    SearchSinceArrival          'search since arrival to the unit
    SearchSinceAdmission        'search everything since admission to the hospital
    SearchOutcomesRange         'search within the current pull + 24 hours before
    SearchAssessments
End Enum

Private Enum CountMode
    CountAll
    CountFirst                  'stop after one is found
End Enum

Private Enum GetValueMode
    GetTotal
    GetMax
    GetLast
End Enum

Private Const FREQUENCY_BUCKET_SIZE = 20    'min

Private Enum Frequencies
    QNONE
    Q4H
    Q2H
    Q1H
    Q30M
End Enum

Private Type fmapRow
    los_high                As Single       'the LOS being testing
    freq(QNONE To Q30M)     As Integer      'the count required for each Q value
End Type

Private m_freq_map(6)       As fmapRow      '1,2,4,8,12,24 hours
Private txarea              As String
Private tc_event_id As Long



'This is the main entry point
'
Public Sub ProcessInpatient(pat As PatientInfo)
    On Error GoTo errHandler
    
    m_pat = pat
'    frmMain.SetProgress "Processing acct: " & m_pat.acct
    InitGroupsIfNeeded
    SetSQLConstants
    LoadFreqTable
    ResetIndicators
'    ResetProcs
'    ResetOutcomes
    txarea = ""
    If m_pat.unit_name = "ICU" Then
        dvprint "pull start pretx=" & g_dbutil.SQL_DateTime(m_pat.pull_start)
        'set m_pat.pull_start to beginning of last SERVICE (or leave it as is if beginning < m_pat.pull_start).
        'set tx area to the service.
        SetTxForICUPt
        
        dvprint "pull start posttx=" & g_dbutil.SQL_DateTime(m_pat.pull_start)
    End If
    
    CheckExploding
    Check_1234
    Check_5
    Check_67
    Check_8
    Check_9
    Check_1011
    Check_1213
    Check_14
    Check_15161718
    Check_19
    Check_20
    Check_2122
    Check_23
    Check_24
    CheckCustom
    HighestIndicatorInEachGroupWins

'    CheckProcs
'    CheckOutcomes

    If g_no_output Then Exit Sub
    OutputClass
'    OutputProcs
'    OutputOutcomes
    Exit Sub
    
errHandler:
    LogError Err.Description & " in " & Err.source, EVENT_CATEGORY_UNEXPECTED
    Exit Sub
    Resume  'debug
End Sub

Private Sub ResetIndicators()
    Dim i As Integer
    
    For i = 1 To MAX_INDS
        inds(i).checked = False
        inds(i).also_mark = ""
    Next i
    
End Sub

Private Sub ResetProcs()
    numprocs = 0
End Sub
Private Sub ResetOutcomes()
    numoutcomes = 0
End Sub

Private Sub InitGroupsIfNeeded()
    On Error GoTo errHandler
    
    Static been_here As Boolean
    Dim rs As New Recordset
    Dim sql As String
    Dim inum As Integer
    
    If been_here Then Exit Sub
    
    'get indicator radio groups from the database
    sql = "SELECT INDICATOR_NUMBER, RADIO_GROUP FROM INDICATOR_DEFINITION WHERE METHODOLOGY_ID=" & m_pat.meth_id
    rs.Open sql, g_cnADO
    
    Do While Not rs.EOF
        inum = rs("INDICATOR_NUMBER")
        If (inum < MAX_INDS) Then
            inds(inum).group = g_dbutil.DBToInteger(rs("RADIO_GROUP"))  'could be null
        End If
        rs.MoveNext
    Loop
    
    rs.Close
    Exit Sub
    
errHandler:
    g_util.ThrowError "InitGroupsIfNeeded"
End Sub

Private Sub LoadFreqTableRow(m_freq As fmapRow, los_high As Single, values As String)
    Dim arr() As String, n As Integer, i As Integer
    
    m_freq.los_high = los_high

    n = g_util.SplitTextOnChar(values, ",", arr(), 0, 0)
    For i = 0 To n - 1
        m_freq.freq(i) = CInt(arr(i))
    Next i
End Sub

Private Sub LoadFreqTable()
                                 ' LOS,   None Q4h Q2h Q1h Q30m
    LoadFreqTableRow m_freq_map(1), 1, "    0,  0,  0,  0,  1"
    LoadFreqTableRow m_freq_map(2), 2, "    0,  0,  0,  1,  2"
    LoadFreqTableRow m_freq_map(3), 4, "    0,  1,  2,  3,  4"
    LoadFreqTableRow m_freq_map(4), 8, "    0,  2,  3,  4,  8"
    LoadFreqTableRow m_freq_map(5), 12, "   0,  2,  4,  6, 12"
    LoadFreqTableRow m_freq_map(6), 9999, " 0,  3,  6, 12, 24"         '24+
    
    'To read this table: if you have at least 6 asessmenets in a 12-hour period, you get Q1h
    '(row 5: LOS=12, column Q1h has a count of 6)
End Sub

Private Function FreqForCount(los_hours As Single, count As Integer) As Frequencies
    Dim i As Integer, j As Integer
    
    For i = 1 To UBound(m_freq_map)                         'go from LOS 1 to 24+
        If (los_hours <= m_freq_map(i).los_high) Then
            For j = Q30M To QNONE Step -1                   'search right to left
                If (count >= m_freq_map(i).freq(j)) Then
                    FreqForCount = j
                    Exit Function
                End If
            Next j
        End If
    Next i
    
    FreqForCount = QNONE
End Function

Private Sub SetSQLConstants()
    'These SQL constants are combined in different ways for searching
    WHERE_ENCOUNTER = " where (encounter_id = " & m_pat.encounter_id & ")"
    AND_UNIT = " and (unit_id = " & m_pat.unit_id & ")"
    AND_PULL_RANGE = " and (event_datetime between " & g_dbutil.SQL_DateTime(m_pat.pull_start) & " and " & g_dbutil.SQL_DateTime(m_pat.pull_finish) & ")"
    AND_ARRIVAL = " and (event_datetime between " & g_dbutil.SQL_DateTime(m_pat.unit_arrival) & " and " & g_dbutil.SQL_DateTime(m_pat.pull_finish) & ")"
    AND_OUTCOMES_RANGE = " and (event_datetime between " & g_dbutil.SQL_DateTime(DateAdd("d", -1, m_pat.pull_start)) & " and " & g_dbutil.SQL_DateTime(m_pat.pull_finish) & ")"
    
    'What bucket number does the event belong to?
    BUCKET_CALC = "DateDiff(minute," & g_dbutil.SQL_DateTime(m_pat.pull_start) & ",event_datetime) / " & FREQUENCY_BUCKET_SIZE
End Sub

'Add this to all chart item queries to start the WHERE clause
'This is where the search depth is set
Private Function WhereBase(Optional search_mode As SearchMode = SearchDefault)
    Dim result As String
    
    Select Case search_mode
    Case Searchpullrange, SearchDefault
        result = WHERE_ENCOUNTER & AND_UNIT & AND_PULL_RANGE    'search within pull range
    Case SearchSinceArrival
        result = WHERE_ENCOUNTER & AND_UNIT & AND_ARRIVAL       'search since arrival to the unit
    Case SearchSinceAdmission
        result = WHERE_ENCOUNTER                                'search since admission to the hospital (any unit)
    Case SearchOutcomesRange
        result = WHERE_ENCOUNTER & AND_UNIT & AND_OUTCOMES_RANGE    'search within pull range+24hrs before
    Case SearchAssessments
        result = WHERE_ENCOUNTER & AND_UNIT     'search within 12 hour range
    End Select
    
    'Since we are looking for one patient in one facility, we don't have to filter by TC_source_ID.
    
    WhereBase = result
End Function

Private Function ValueIsAList(result As String) As Boolean
    ValueIsAList = (InStr(1, result, ",") > 0)                'is there a comma-separated list?
End Function

'Look for any of these fields.  Cat/desc/field = exact match.  Result = like match.
Private Function AndSimpleItemFilter(cat As String, code As String, desc As String, field As String, result_like As String) As String
    Dim result As String
    
    If Len(cat) Then result = result & " and category=" & g_dbutil.SQL_String(cat)
    If Len(code) Then
        If Mid$(code, 1, 2) = LIKE_PREFIX Then
            result = result & " and code like " & g_dbutil.SQL_String(Mid$(code, 3, Len(code) - 2))
        ElseIf ValueIsAList(code) Then
            result = result & " and code in " & FormatCodeList(code)
        Else
            result = result & " and code=" & g_dbutil.SQL_String(code)
        End If
    End If
    If Len(desc) Then result = result & " and description=" & g_dbutil.SQL_String(desc)
    If Len(field) Then result = result & " and field_name=" & g_dbutil.SQL_String(field)
    If Len(result_like) Then
        If InStr(result_like, EXACT_MATCH_PREFIX) = 1 Then 'exact match
            result_like = Mid$(result_like, 3, Len(result_like) - Len(EXACT_MATCH_PREFIX))
            result = result & " and result= " & g_dbutil.SQL_String(result_like)
        Else
            result = result & " and result like '%" & Trim$(result_like) & "%'"
        End If
    End If

    AndSimpleItemFilter = result
End Function
Private Function FormatCodeList(code_list As String) As String
    Dim arr() As String
    Dim i As Integer, n As Integer
    Dim result As String

    If Len(code_list) = 0 Then Exit Function
    
    n = g_util.SplitTextOnChar(code_list, ",", arr(), 1, 0)
    
    result = "("
    
    For i = 1 To n
        result = result & g_dbutil.SQL_String(Trim$(arr(i)))
        If i < n Then result = result & ","
    Next i
    
    result = result & ")"
    
    FormatCodeList = result
End Function

'Look for a result that contains one of the words in the list
'The only drawback with this function is that we don't know what it found (for audits)
'Because of this, it is only used in as few places.
Private Function AndResultContains(result_list As String) As String
    Dim arr() As String
    Dim i As Integer, n As Integer
    Dim result As String

    If Len(result_list) = 0 Then Exit Function
    
    n = g_util.SplitTextOnChar(result_list, ",", arr(), 1, 0)
    
    result = "and ((1=0)"   ' or (result=this) or (result=that)
    
    For i = 1 To n
        result = result & " or (result like '" & "%" & Trim$(arr(i)) & "%')"
    Next i
    
    result = result & ")"
    
    AndResultContains = result
End Function

Private Function DescribeSearchDepth(search_mode As SearchMode) As String
    Dim result As String
    
    Select Case search_mode
    Case Searchpullrange, SearchDefault
        'result = "in pull range"
        result = ""                         'be quiet since this is default
    Case SearchSinceArrival
        result = "since arrival to unit"
    Case SearchSinceAdmission
        result = "since admission"
    Case SearchOutcomesRange
        result = "since 24hrs before pull"
    End Select
    
    DescribeSearchDepth = result
End Function

Private Function Describe(cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = Searchpullrange) As String
    Dim sql As String, and_filter As String
    Dim rs As New Recordset
    Dim result As String
   
    If Not g_debug Then Exit Function           'avoid extra overhead if not making a log

    result = "looking for"
    If Len(cat) Then result = result & " cat='" & cat & "'"
    If Len(code) Then result = result & " code='" & code & "'"
    If Len(desc) Then result = result & " desc='" & desc & "'"
    If Len(field) Then result = result & " field='" & field & "'"
    If Len(result_list) Then result = result & " result contains '" & result_list & "'"

    If ValueIsAList(result_list) Then
        and_filter = AndSimpleItemFilter(cat, code, desc, field, "") & AndResultContains(result_list)
    Else
        and_filter = AndSimpleItemFilter(cat, code, desc, field, result_list)
    End If
    
    sql = "select code, description, result from chart_item" & WhereBase(search_mode) & and_filter
    rs.Open sql, g_cnADO
    If rs.EOF Then
        result = result & "; not found " & DescribeSearchDepth(search_mode)
    Else
        result = result & "; found"
        'Add info for columns that were not specified and a value was found
        If (Len(code) = 0 Or ValueIsAList(code)) And Len(rs("code")) Then result = result & " code='" & rs("code") & "'"
        If (Len(desc) = 0) And Len(rs("description")) Then result = result & " desc='" & rs("description") & "'"
        'Add the complete result found (we searched for a word or words)
        result = result & " result='" & rs("result") & "'"
        'Are there more results?  Just say how many; we aren't going to list them.
        If (rs.RecordCount = 2) Then
            result = result & " (1 more result)"
        ElseIf (rs.RecordCount > 2) Then
            result = result & " (" & rs.RecordCount - 1 & " more results)"
        End If
    End If
    rs.Close
    
    Describe = result
End Function

'Set an indicator for this reason (low level)
Private Sub SetInd(inum As Integer, reason As String)
    If inds(inum).checked And Not g_debug Then Exit Sub       'already set and no log?

    inds(inum).checked = True
    dprint "Set Ind #" & inum & ": " & reason
End Sub

'Clear an indicator for this reason (low level)
Private Sub ClrInd(inum As Integer, reason As String)
    If Not inds(inum).checked And Not g_debug Then Exit Sub   'already clear and no log?
    
    inds(inum).checked = False
    dprint "Clr Ind #" & inum & ": " & reason
End Sub

'Count how many items have the search word in result_like
'This is simple and fast
Private Function CountSimpleResult(cat As String, code As String, desc As String, field As String, result_like As String, Optional search_mode As SearchMode = Searchpullrange, Optional trace As Boolean = True, Optional found_what As String) As Long
    Dim sql As String, and_filter As String
    Dim rs As New Recordset
    Dim count As Long

    and_filter = AndSimpleItemFilter(cat, code, desc, field, result_like)
    sql = "select count(*) from chart_item" & WhereBase(search_mode) & and_filter
'    dvprint sql
    'Debug.Print sql
    rs.Open sql, g_cnADO
    count = rs(0)
    rs.Close
    
    'Normally CountSimpleResult will report what it finds; not eveyone wants this so the trace can be turned off
    found_what = Describe(cat, code, desc, field, result_like, search_mode) & IIf(count > 0, "; count=" & count, "")
    If trace Then dvprint found_what
    
    CountSimpleResult = count
End Function
Private Function CountUniqueCodesInList(codelist As String, retlist As String) As Long
    Dim sql As String, and_filter As String
    Dim rs As New Recordset
    Dim count As Long

    retlist = ""
    sql = "select distinct(code) from chart_item" & WhereBase & " and code in (" & codelist & ")"
    rs.Open sql, g_cnADO
    Do While Not rs.EOF
        If Not IsNull(rs(0)) Then
            count = count + 1
            retlist = retlist & rs(0) & " "
        End If
        rs.MoveNext
    Loop
    rs.Close
    
    'Normally CountSimpleResult will report what it finds; not eveyone wants this so the trace can be turned off
    
    CountUniqueCodesInList = count
End Function
Private Function CountCodeHitsInList(codelist As String, retlist As String) As Long
    Dim sql As String, and_filter As String
    Dim rs As New Recordset
    Dim count As Long

    retlist = ""
    sql = "select count(code),code from chart_item" & WhereBase & " and code in (" & codelist & ")"
    sql = sql & " group by code having count(code)>=2"
    rs.Open sql, g_cnADO
    Do While Not rs.EOF
        If Not IsNull(rs(0)) And Not IsNull(rs(1)) Then
            count = count + 1
            retlist = retlist & rs(1) & " = " & rs(0) & " times; "
        End If
        rs.MoveNext
    Loop
    rs.Close
    If Len(retlist) Then retlist = Mid$(retlist, 1, Len(retlist) - 2) & "."
    'Normally CountSimpleResult will report what it finds; not eveyone wants this so the trace can be turned off
    
    CountCodeHitsInList = count
End Function

'Count how many items have a result with one of the words given in the result_list
'Returns a description of what word was found
'This is more powerful than CountSimpleResult but it is slower
Private Function CountResultInList(cat As String, code As String, desc As String, field As String, result_list As String, search_mode As SearchMode, count_mode As CountMode, trace As Boolean, found_what As String) As Integer
    On Error GoTo errHandler
    Dim sql As String, and_filter As String, arr() As String
    Dim rs As New Recordset
    Dim i As Integer, n As Integer, count As Integer
    Dim pos As Long
    
    n = g_util.SplitTextOnChar(result_list, ",", arr(), 1, 0)
    
    'Do not filter by result here; we need all results
    and_filter = AndSimpleItemFilter(cat, code, desc, field, "")
    sql = "select code,result from chart_item" & WhereBase(search_mode) & and_filter
    'Debug.Print sql
    rs.Open sql, g_cnADO
    Do While Not rs.EOF
        'Look for each search word in the result (case insensitive)
        For i = 1 To n
            arr(i) = Trim$(arr(i))          'we split on comma so there usually are leading blanks
            If InStr(arr(i), EXACT_MATCH_PREFIX) = 1 Then
                arr(i) = Mid$(arr(i), 3, Len(arr(i)) - Len(EXACT_MATCH_PREFIX))
                pos = (rs("result") = arr(i))
            Else
                pos = InStr(1, rs("result"), arr(i), vbTextCompare)  'bad when looking for "RN" in "Non RN"
            End If
            If pos > 0 Then
                found_what = "found '" & arr(i) & "' in code='" & rs("code") & "' result='" & rs("result") & "'"
                If trace Then dvprint found_what
                count = count + 1
                If count_mode <> CountAll Then Exit Do  'stop counting
                Exit For                                'try next record
            End If
        Next i

        rs.MoveNext
    Loop
    
    If (count > 0) Then
        'We already printed what was found; maybe add how many?
        If trace And (count_mode = CountAll) Then dvprint "found " & count & " total"
    Else
        found_what = Describe(cat, code, desc, field, result_list, search_mode)       'not found
        If trace Then dvprint found_what
    End If
    
    rs.Close
    
    CountResultInList = count
    Exit Function
    
errHandler:
    g_util.ThrowError "CountResultInList"
    Resume   'debug
End Function

Private Function CountResultContains(cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = Searchpullrange, Optional trace As Boolean = True, Optional found_what As String) As Integer
    If ValueIsAList(result_list) Then
        CountResultContains = CountResultInList(cat, code, desc, field, result_list, search_mode, CountAll, trace, found_what)
    Else
        CountResultContains = CountSimpleResult(cat, code, desc, field, result_list, search_mode, trace, found_what)
    End If
End Function

Private Function ResultContains(cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = Searchpullrange, Optional trace As Boolean = True, Optional found_what As String) As Boolean
    'tracing can be turned off and you can ask for what was found instead of having it printed
    If ValueIsAList(result_list) Then
        ResultContains = (CountResultInList(cat, code, desc, field, result_list, search_mode, CountFirst, trace, found_what) > 0)
    Else
        ResultContains = (CountSimpleResult(cat, code, desc, field, result_list, search_mode, trace, found_what) > 0)
    End If
End Function


Private Function CountResultNotInList(cat As String, code As String, desc As String, field As String, result_list As String, search_mode As SearchMode, count_mode As CountMode, trace As Boolean, found_what As String) As Integer
    On Error GoTo errHandler
    Dim sql As String, and_filter As String, arr() As String
    Dim rs As New Recordset
    Dim i As Integer, n As Integer, count As Integer
    Dim found_one As Boolean

    n = g_util.SplitTextOnChar(result_list, ",", arr(), 1, 0)
    
    'Do not filter by result here; we need all results
    and_filter = AndSimpleItemFilter(cat, code, desc, field, "")
    sql = "select code, result from chart_item" & WhereBase(search_mode) & and_filter
    sql = sql & " and result<>'' and result is not null"
    'Debug.Print sql
    rs.Open sql, g_cnADO
    
    Do While Not rs.EOF
        'Look for each search word in the result (case insensitive)
        found_one = False
        
        For i = 1 To n
            arr(i) = Trim$(arr(i))          'we split on comma so there usually are leading blanks
            If InStr(1, rs("result"), arr(i), vbTextCompare) > 0 Then
                found_what = "found '" & arr(i) & "' in code='" & rs("code") & "' result='" & rs("result") & "' -- ignore this"
                If trace Then dvprint found_what
                found_one = True
                Exit For
            End If
        Next i

        If (Not found_one) Then
            'none of the words were found - good!
            found_what = Describe(cat, code, desc, field, "", search_mode) & " -- does not contain '" & result_list & "'"
            If trace Then dvprint found_what
            count = count + 1
            If count_mode <> CountAll Then Exit Do
        End If

        rs.MoveNext
    Loop
    
    If (count > 0) Then
        'We already printed what was found; maybe show the total?
        If trace And (count_mode = CountAll) Then dvprint "found " & count & " total"
    ElseIf (rs.RecordCount > 0) Then
        'We already printed what we ignored
    Else
        'Print what did not exist
        found_what = Describe(cat, code, desc, field, "", search_mode)      'not found
        If trace Then dvprint found_what
    End If
    
    rs.Close
    
    CountResultNotInList = count
    Exit Function
    
errHandler:
    g_util.ThrowError "CountResultNotInList"
    Resume   'debug
End Function

Private Function CountResultDoesNotContain(cat As String, code As String, desc As String, field As String, result_list As String, found_what As String, Optional search_mode As SearchMode = Searchpullrange, Optional trace As Boolean = True) As Integer
    CountResultDoesNotContain = CountResultNotInList(cat, code, desc, field, result_list, search_mode, CountAll, trace, found_what)
End Function

Private Function ResultDoesNotContain(cat As String, code As String, desc As String, field As String, result_list As String, found_what As String, Optional search_mode As SearchMode = Searchpullrange, Optional trace As Boolean = True) As Boolean
    ResultDoesNotContain = (CountResultNotInList(cat, code, desc, field, result_list, search_mode, False, trace, found_what) > 0)
End Function

'Set the indicator if the result contains one of the words in the result_list
Private Sub SetIndIfResultContains(inum As Integer, cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = Searchpullrange)
    Dim found_what As String
    'avoid more queries if the indicator is already set
    If inds(inum).checked Then Exit Sub
    
    'Turn trace off for ResultContains() so the result can be placed on the "SetInd#" line below
    If ResultContains(cat, code, desc, field, result_list, search_mode, False, found_what) Then
        SetInd inum, found_what             'echo here
    Else
        dvprint found_what                  'and here
    End If
End Sub

'Set the indicator if the result does not contain any of the words in result_list
Private Sub SetIndIfResultDoesNotContain(inum As Integer, cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = Searchpullrange)
    Dim found_what As String
    'avoid more queries if the indicator is already set
    If inds(inum).checked Then Exit Sub
    
    'Turn trace off for ResultDoesNotContain() and echo what was set below with SetInd
    If ResultDoesNotContain(cat, code, desc, field, result_list, found_what, search_mode, False) Then
        SetInd inum, found_what             'echo here
    Else
        dvprint found_what                  'and here
    End If
End Sub

'Clear the indicator if the result contains on of the words in the result_list
Private Sub ClrIndIfResultContains(inum As Integer, cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = Searchpullrange)
    Dim found_what As String
    'avoid more queries if the indicator is already clear
    If Not inds(inum).checked Then Exit Sub
    
    'Turn trace off for ResultContains() and echo what was set below with SetInd
    If ResultContains(cat, code, desc, field, result_list, search_mode, False, found_what) Then
        ClrInd inum, found_what             'echo here
    Else
        dvprint found_what                  'and here
    End If
End Sub


'These slightly smaller functions are meant for places where you aren't really looking for a chart result
Private Function Exists(cat As String, Optional code As String, Optional desc As String, Optional field As String, Optional result_list As String, Optional search_mode As SearchMode = Searchpullrange, Optional trace As Boolean = True, Optional found_what As String) As Boolean
    Exists = (CountResultContains(cat, code, desc, field, result_list, search_mode, trace, found_what) > 0)
End Function

Private Sub SetIndIfFound(inum As Integer, cat As String, Optional code As String, Optional desc As String, Optional field As String, Optional result_list As String, Optional search_mode As SearchMode = Searchpullrange)
    SetIndIfResultContains inum, cat, code, desc, field, result_list, search_mode
End Sub

Private Sub ClrIndIfFound(inum As Integer, cat As String, Optional code As String, Optional desc As String, Optional field As String, Optional result_list As String, Optional search_mode As SearchMode = Searchpullrange)
    ClrIndIfResultContains inum, cat, code, desc, field, result_list, search_mode
End Sub


'Get the max/total value from a result (usually in the middle of the text)
Private Function GetIntValue(get_mode As GetValueMode, cat As String, code As String, desc As String, field As String, result_like As String, Optional search_mode As SearchMode = Searchpullrange) As Integer
    Dim sql As String, and_filter As String, arr() As String, msg As String
    Dim rs As New Recordset
    Dim result As Integer, i As Integer, n As Integer, value As Integer
    Dim found_one As Boolean

    and_filter = AndSimpleItemFilter(cat, code, desc, field, result_like)
    sql = "select result from chart_item" & WhereBase(search_mode) & and_filter
    'Debug.Print sql
    rs.Open sql, g_cnADO
    
    'Look for a number in the result
    
    Do While Not rs.EOF
        n = g_util.SplitTextOnChar(rs(0), " ", arr(), 1, 0)
        For i = 1 To n
            'Look for 1st character numeric; IsNumeric("60min") will fail so look at 1st char only
            If IsNumeric(Left$(arr(i), 1)) Then
                value = val(arr(i))                         'Use Val; CInt will error on "60min"
                Select Case get_mode
                Case GetMax
                    result = g_util.Max(value, result)      'max
                Case GetTotal
                    result = result + value                 'total
                Case GetLast
                    result = value                          'last
                End Select
                
                'print what we are searching for (the first time)
                If Not found_one Then
                    dvprint Describe(cat, code, desc, field, result_like, search_mode)
                End If
                found_one = True
                'print each value found
                dvprint "  found numeric value " & result
                'Keep going in case there are more
            End If
        Next i
        rs.MoveNext
    Loop
    
    rs.Close
    
    If Not found_one Then
        'show what was not found
        If g_verbose Then dprint Describe(cat, code, desc, field, result_like, search_mode)
    End If
    
    GetIntValue = result
End Function

Private Function GetMaxValue(cat As String, code As String, desc As String, field As String, result_like As String, Optional search_mode As SearchMode = Searchpullrange) As Integer
    GetMaxValue = GetIntValue(GetMax, cat, code, desc, field, result_like, search_mode)
End Function

Private Function GetTotalValue(cat As String, code As String, desc As String, field As String, result_like As String, Optional search_mode As SearchMode = Searchpullrange) As Integer
    GetTotalValue = GetIntValue(GetTotal, cat, code, desc, field, result_like, search_mode)
End Function

'Get a result; returns True if found with return_result set
Private Function GetResult(cat As String, code As String, desc As String, field As String, return_result As String, Optional search_mode As SearchMode = Searchpullrange) As Boolean
    Dim rs As New Recordset
    Dim sql As String

    sql = "select result from chart_item" & WhereBase(search_mode) & AndSimpleItemFilter(cat, code, desc, field, "")
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        return_result = rs(0) & ""
    Else
        return_result = ""
    End If
    rs.Close

    dvprint Describe(cat, code, desc, field, "", search_mode)
    GetResult = (Len(return_result) > 0)
End Function
Private Function GetResultOfLatest(cat As String, code As String, desc As String, field As String, return_result As String, Optional search_mode As SearchMode = Searchpullrange) As Boolean
    Dim rs As New Recordset
    Dim sql As String
    Dim evdt As Date
    Dim done As Boolean

    sql = "select event_datetime,result from chart_item" & WhereBase(search_mode) & AndSimpleItemFilter(cat, code, desc, field, "")
    sql = sql & " order by event_datetime desc"
    rs.Open sql, g_cnADO
    
    return_result = ""
    done = False
    
    If Not rs.EOF Then evdt = rs(0)
    Do While Not rs.EOF And Not done
        If evdt = rs(0) Then
            return_result = return_result & rs(1) & ","
        Else
            done = True
        End If
        rs.MoveNext
    Loop
    rs.Close

    dvprint Describe(cat, code, desc, field, "", search_mode)
    GetResultOfLatest = (Len(return_result) > 0)

End Function
Private Sub CheckExploding()
    'FND107755 2,18,24 unless FND107756
    dvprint "---------------"
    dvprint "0. Exploding Triggers"
    dvprint "---------------"
    
    If Not Exists("", "FND107756") Then
        If Exists("", "FND107755") Then
            dprint "Setting indicators 2,18,24 due to FND107755."
            SetInd 2, "FND107755"
            SetInd 18, "FND107755"
            SetInd 24, "FND107755"
        End If
    End If
    
End Sub

Private Sub Check_1234()
    On Error GoTo errHandler
    Dim b36625459dep As Boolean
Dim fnd762 As Boolean
Dim fnd763 As Boolean
Dim fnd001 As Boolean
Dim fnd002 As Boolean
Dim fnd248 As Boolean
Dim fnd027 As Boolean
Dim reason As String
    Dim codelist As String
    Dim retlist As String
    Dim ct As Integer


    dvprint "---------------"
    dvprint "1. ADL Self"
    dvprint "2. ADL Assist"
    dvprint "3. ADL Extended"
    dvprint "4. ADL Complete"
    dvprint "---------------"
'COMPLETE CARE
    SetADLCompleteWhenAge "<=3"
    If inds(4).checked Then Exit Sub
    
'4
    If Exists("", "FND763") Then fnd763 = True 'feeding
    If Exists("", "FND248") Then fnd248 = True '
    If fnd763 And fnd248 Then
        SetInd 4, "FND763+FND248"
    End If
    If inds(4).checked Then Exit Sub
    If Exists("", "FND027") Then fnd027 = True
    If fnd027 And fnd248 Then
        SetInd 4, "FND027+FND248"
    End If
    If inds(4).checked Then Exit Sub
    If Exists("", "FND104004") Then
        If fnd027 Or fnd763 Then
            SetInd 4, "FND104004+(FND027 or fnd763)"
        End If
    End If
    If Exists("", "FND108553") And Exists("", "FND108554") Then
        SetInd 4, "Total Bath+Total Feed"
    End If
    If inds(4).checked Then Exit Sub
    'SetIndIfFound 4, "", "FND101990,FND101996,FND101998,FND101379,FND248,FND102246,FND100928,FND763,FND027,FND007,FND102056"
    SetIndIfFound 4, "", "FND108629"
    
'3

    SetIndIfFound 3, "", "FND102313,FND014"
'If used with 3 other findings from this group once in 12 hours applies regardless of frequency
    codelist = "'FND007','FND027','FND100928','FND101379','FND101781','FND101987','FND101990','FND101996','FND101998','FND102056','FND175','FND763','FND208','FND248','FND251','FND101988','FND101989'"
    codelist = codelist & ",'fnd762','FND107367','FND102246','FND101987','FND101991','FND104043','FND101781','FND102301','FND208','FND100928','fnd763','fnd027','FND175','FND251','FND104004'"
    codelist = codelist & ",'FND108368','FND108369','FND108370','FND108371','FND108372','FND108373'"
    ct = CountUniqueCodesInList(codelist, retlist)
    If ct >= 4 Then SetInd 3, "Four unique findings found:" & retlist
    If inds(3).checked Then Exit Sub
    codelist = "'FND007','FND027','FND100928','FND101379','FND101781','FND101987','FND101990','FND101996','FND101998','FND102056','FND175','FND763','FND208','FND248','FND251','FND101988','FND101989'"
    codelist = codelist & ",'fnd762','FND107367','FND102246','FND101987','FND101991','FND104043','FND101781','FND102301','FND208','FND100928','fnd763','fnd027','FND175','FND251','FND104004'"
    ct = CountCodeHitsInList(codelist, retlist)
    If ct >= 1 Then SetInd 3, "Findings found at least twice:" & retlist
    
    If inds(3).checked Then Exit Sub
   
'2
'If used with FND762 (Assisted Feed), FND763 (Total Feed)
'If used with FND001 (Shower); FND002 (Tub)
    If Exists("", "FND762") Then fnd762 = True
    If Exists("", "FND001") Then fnd001 = True
    If Exists("", "FND002") Then fnd002 = True
    If fnd762 Or fnd763 Then
        If Exists("", "FND207") Then SetInd 2, "FND207 with FND762 or 763"
    End If
    If fnd001 Or fnd002 Then
        If ResultContains("", "FND100928,FND763,FND027,FND251", "", "", "") Then SetInd 2, "FND100928,FND763,FND027,FND251 with FND001 or 002"
    End If

    If inds(2).checked Then Exit Sub
    codelist = "'FND101987','FND101991','FND104043','FND101781','FND102301','FND208','FND175','FND101990','FND101996','FND102057','FND102056','FND175','FND251','FND101998','FND248','FND245','FND101379','FND102246'"
    ct = CountUniqueCodesInList(codelist, retlist)
    If ct >= 1 Then SetInd 2, "At least one finding found:" & retlist

    If inds(2).checked Then Exit Sub
    SetIndIfFound 2, "", "FND108368,FND108369,FND108370,FND108371,FND108372,FND108373"

'1
    If inds(2).checked Then Exit Sub
    SetIndIfFound 1, "", "FND102042,FND101362,FND101365,FND100044,FND101409,FND207,FND101654,FND100929,FND005,FND165,FND108366,FND108367"

    If Not (inds(1).checked Or inds(2).checked Or inds(3).checked Or inds(4).checked) Then
        SetInd 2, "Defaulting to partial due to lack of documentation."
    End If
    
    Exit Sub
errHandler:
    g_util.ThrowError "Check_1234"
    Resume  'debug
End Sub
Private Sub SetADLCompleteWhenAge(agecond As String)  ' agecond example: "<=3"
    Dim sql As String
    Dim rs As New Recordset

'
'select case when round(age_at_admission,0,1) <=55 then 1 else 0 end from encounter where encounter_id=6990

    sql = "select case when round(age_at_admission,0,1) " & agecond & " then 1 else 0 end from encounter " & WHERE_ENCOUNTER
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        If rs(0) = 1 Then
            SetInd 4, "Age <=3 years"
        End If
    End If
    rs.Close

End Sub
Private Sub Set21WhenAge() ' agecond example: "<=3"
    Dim sql As String
    Dim rs As New Recordset

    sql = "select datediff(d,p.dob,getdate()) from encounter as e inner join person as p on "
    sql = sql & "(e.person_id=p.person_id) " & WHERE_ENCOUNTER
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        If rs(0) < 15 Then
            SetInd 21, "Age <=14 days"
        End If
    End If
    rs.Close

End Sub



Private Sub Check_5()
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "5. ADL Rehab"
    dvprint "---------------"
'FND296,FND10041,FND10040,FND10042
    
    SetIndIfFound 5, "", "FND10041,FND10040,FND10042"
    If inds(5).checked Then Exit Sub
    SetIndIfFound 5, "", "MCK_QMR001,MCK_QMR038,MCK_QMR045,MCK_QMR031,MCK_QMR033,MCK_QMR034"

    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_5"
    Resume  'debug
End Sub


Private Sub Check_67()
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "6. ADL 2-3 Caregivers"
    dvprint "7. ADL 4 or more Caregivers"
    dvprint "---------------"

    SetIndIfFound 6, "", "FND101988,FND108369,FND108370"
    SetIndIfFound 7, "", "FND101989,FND108371"
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_67"
    Resume  'debug
End Sub

Private Sub Check_8()
    Dim supp As Boolean
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "8. Communication"
    dvprint "---------------"

    'If ResultDoesNotContain(cat, code, desc, field, result_list, found_what, search_mode, False) Then
    SetIndIfResultDoesNotContain 8, "", "LANGUAGE", "", "", "English,Primary Language"
    If inds(8).checked Then Exit Sub


    SetIndIfResultContains 8, "", "COMMUNICATION BARRIER", "", "", "BLIND,DEAF,HOH", SearchSinceAdmission
    If inds(8).checked Then Exit Sub

    SetIndIfResultContains 8, "", "COMMUNICATION BARRIER", "", "", "UNABLE_TO_SPEAK,UNABLE_TO_VERBAL,UNABLE_T0_WRITE", SearchSinceAdmission
    If inds(8).checked Then Exit Sub
    SetIndIfResultContains 8, "", "COMMUNICATION BARRIER", "", "", "VISION_IMPAIRMENT,SPEECH_IMPAIRMENT", SearchSinceAdmission
    If inds(8).checked Then Exit Sub
    SetIndIfResultContains 8, "", "COMMUNICATION BARRIER", "", "", "DIFF_HEARING,DIFF_SEEING,DIFF_SPEAKING,HEARING_IMPAIRMENT", SearchSinceAdmission
    If inds(8).checked Then Exit Sub
    SetIndIfResultContains 8, "", "COMMUNICATION BARRIER", "", "", "INTERPRETER_REQUIRED,MUTE,NEED_INTERPRETER", SearchSinceAdmission
    If inds(8).checked Then Exit Sub
    SetIndIfResultContains 8, "", "COMMUNICATION BARRIER", "", "", "UNABLE_TO_COMM,UNABLE_TO_INFER,UNABLE_TO_READ", SearchSinceAdmission
    If inds(8).checked Then Exit Sub
    
    
    SetIndIfFound 8, "", "ESGN,SEP,EWR,RSGN,RSP,RWR", "", "", ""
    If inds(8).checked Then Exit Sub
    
    SetIndIfFound 8, "", "FND100155,FND103714,FND100494,FND100495,FND377,FND10034,FND10032"
    If inds(8).checked Then Exit Sub
    SetIndIfFound 8, "", "FND10485,FND10035,FND589,FND105642,FND105643,FND105641"
    If inds(8).checked Then Exit Sub
    SetIndIfFound 8, "", "FND10189,FND106631,FND106632,FND106634,FND106636,FND106637"
    If inds(8).checked Then Exit Sub
    SetIndIfFound 8, "", "FND106633,FND106713,FND105639,FND1059", "", "", ""
    If inds(8).checked Then Exit Sub
    
    If ResultContains("", "FND748,RT VENT", "", "", "") Then
        If Not Exists("", "FND106970,FND106930") Then
            SetInd 8, "FND748 or RT VENT found without extubation."
        End If
    End If
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_8"
    Resume  'debug
End Sub

Private Sub Check_9()
    On Error GoTo errHandler
    Dim tmp As String
    Dim supp As Boolean
    Dim found_what As String

    dvprint "---------------"
    dvprint "9. Cognitive Support"
    dvprint "---------------"
    
    SetIndIfFound 9, "", "FND100132,FND100133,FND100372,FND103027,FND103062,FND236"
    If inds(9).checked Then Exit Sub
    SetIndIfFound 9, "", "FND711,FND877,FND104510,FND104511,FND104512,FND100204"
    If inds(9).checked Then Exit Sub
    SetIndIfFound 9, "", "FND103077,FND106983,FND106984,N42.3.2,FND103125"
    If inds(9).checked Then Exit Sub
    SetIndIfFound 9, "", "FND108445,FND108444,FND108441,FND108442,FND108454,FND108460"
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_9"
    Resume  'debug
End Sub

Private Sub Check_1011()
    On Error GoTo errHandler
    Dim tmp As String
    Dim ct As Integer
    Dim found_what As String
    Dim return_result As String

    dvprint "---------------"
    dvprint "10. Behavior/Emotional Management"
    dvprint "11. Behavior/Emotional Mgmt - q 1 Hour"
    dvprint "---------------"

    SetIndIfFound 11, "", "FND104062,FND103052"
    If inds(11).checked Then Exit Sub

    ct = CountResultContains("", "OPI", "", "", "")
    If ct >= 6 Then SetInd 11, "OPI count = " & ct
    
    If inds(11).checked Then Exit Sub

    ct = CountResultContains("", "SUB 28", "", "", "")
    If ct >= 6 Then SetInd 11, "SUB 28 count = " & ct

    If inds(11).checked Then Exit Sub
    ct = CountResultContains("", "FND103027", "", "", "")
    If ct >= 6 Then SetInd 11, "FND103027 count = " & ct
    
    If inds(11).checked Then Exit Sub

    ct = CountResultContains("", "FND103062", "", "", "")
    If ct >= 6 Then SetInd 11, "FND103062 count = " & ct
    
    If inds(11).checked Then Exit Sub

    ct = CountResultContains("", "FND103125", "", "", "")
    If ct >= 6 Then SetInd 11, "FND103125 count = " & ct
    
    If inds(11).checked Then Exit Sub

    ct = CountResultContains("", "FND103126", "", "", "")
    If ct >= 6 Then SetInd 11, "FND103126 count = " & ct
    
    If inds(11).checked Then Exit Sub

    ct = CountResultContains("", "FND103106", "", "", "")
    If ct >= 6 Then SetInd 11, "FND103106 count = " & ct
    
    If inds(11).checked Then Exit Sub

    ct = CountResultContains("", "FND103448", "", "", "")
    If ct >= 6 Then SetInd 11, "FND103448 count = " & ct
    
    If inds(11).checked Then Exit Sub

    ct = CountResultContains("", "FND103108", "", "", "")
    If ct >= 6 Then SetInd 11, "FND103108 count = " & ct
    
    If inds(11).checked Then Exit Sub

    ct = CountResultContains("", "FND103183", "", "", "")
    If ct >= 6 Then SetInd 11, "FND103183 count = " & ct
    
    If inds(11).checked Then Exit Sub

    ct = CountResultContains("", "FND103149", "", "", "")
    If ct >= 6 Then
        If Not Exists("", "FND104304,FND102717,FND103184") Then
            SetInd 11, "FND103149 count = " & ct
        End If
    End If
    
    If inds(11).checked Then Exit Sub
    
    ct = CountResultContains("", "FND162", "", "", "")
    If ct >= 6 Then
        SetInd 11, "FND162 count = " & ct
    ElseIf ct >= 1 Then
        ct = ct + CountResultContains("", "FND100389", "", "", "")
        ct = ct + CountResultContains("", "FND104071", "", "", "")
        If ct >= 3 Then SetInd 11, "FND162+FND100389+FND104071 count = " & ct
    End If
    
    If inds(11).checked Then Exit Sub

    ct = CountResultContains("", "FND1273", "", "", "")
    If ct >= 6 Then
        SetInd 11, "FND1273 count = " & ct
    ElseIf ct >= 1 Then
        ct = ct + CountResultContains("", "FND104073", "", "", "")
        ct = ct + CountResultContains("", "FND104071", "", "", "")
        If ct >= 3 Then SetInd 11, "FND1273+FND104073+FND104071 count = " & ct
    End If
    
    If inds(11).checked Then Exit Sub

    SetIndIfFound 10, "", "FND711,FND623" 'removed fnd864 11/16
    SetIndIfFound 10, "", "FND108445,FND108444"
    If inds(10).checked Then Exit Sub
    SetIndIfFound 10, "", "FND103126,FND103106,FND103448,FND103108,FND103183,FND103149"
    If inds(10).checked Then Exit Sub
    SetIndIfFound 10, "", "FND162,FND1273,FND867,FND103150,FND868,Restraints"
    If inds(10).checked Then Exit Sub
    SetIndIfFound 10, "", "NA42.4,N1.1.2,OPI,SUB 28"
    If inds(10).checked Then Exit Sub
    If GetResult("", "SUB 1", "", "", return_result) Then
        If IsNumeric(return_result) Then
            If return_result > 3 Then
                SetInd 10, "SADD result =" & return_result
            End If
        End If
    End If
    If inds(10).checked Then Exit Sub
    If GetResult("", "ETOH1", "", "", return_result) Then
        If IsNumeric(return_result) Then
            If return_result > 15 Then
                SetInd 10, "ETOH result =" & return_result
            End If
        End If
    End If
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_1011"
    Resume  'debug
End Sub


Private Sub Check_1213()
    On Error GoTo errHandler
    Dim ct As Integer
    Dim found_what As String
    Dim ct389 As Integer
    Dim ct71 As Integer
    
    dvprint "---------------"
    dvprint "12. Safety Management - q 2 Hours"
    dvprint "13. Safety Management - q 30 Minutes"
    dvprint "---------------"
    


    SetIndIfFound 13, "", "FND104062,FND103052,FND103027,FND100369,FND103056,FND100369"
    If inds(13).checked Then Exit Sub
    SetIndIfFound 13, "", "Restraints,RESTRN1,RESTRN4,RESTRN2,FND102645,FND102605,FND103052"
    If inds(13).checked Then Exit Sub


    ct = CountResultContains("", "FND103150", "", "", "")
    If ct >= 6 Then SetInd 12, "FND103150 count = " & ct
    If inds(12).checked Then Exit Sub
    ct = CountResultContains("", "FND104073", "", "", "")
    If ct >= 6 Then SetInd 12, "FND104073 count = " & ct
    If inds(12).checked Then Exit Sub
    ct = CountResultContains("", "FND868", "", "", "")
    If ct >= 6 Then SetInd 12, "FND868 count = " & ct
    If inds(12).checked Then Exit Sub
    ct = CountResultContains("", "FND100389", "", "", "")
    If ct >= 6 Then SetInd 12, "FND100389 count = " & ct
    If inds(12).checked Then Exit Sub
    ct = CountResultContains("", "FND104071", "", "", "")
    If ct >= 6 Then SetInd 12, "FND104071 count = " & ct
    If inds(12).checked Then Exit Sub
    
    SetIndIfFound 12, "", "FND107364"
    If inds(12).checked Then Exit Sub
    SetIndIfFound 12, "", "FND108537,FND108538,FND108539,FND108540,FND108541,FND108542,FND108543,FND108544"
    SetIndIfFound 12, "", "FND108545,FND108546,FND108547,FND108548,FND108549,FND108550,FND108551"
    If inds(12).checked Then Exit Sub
    
    ct389 = CountResultContains("", "FND100389", "", "", "")
    ct71 = CountResultContains("", "FND104071", "", "", "")
    
    ct = CountResultContains("", "FND103150", "", "", "")
    If ct >= 6 Then
        SetInd 12, "FND103150 count = " & ct
    ElseIf ct >= 1 Then
        ct = ct + ct389 + ct71
        If ct >= 3 Then SetInd 12, "FND103150+FND100389+FND104071 count = " & ct
    End If
    If inds(12).checked Then Exit Sub
    ct = CountResultContains("", "RESTMED", "", "", "")
    If ct >= 6 Then
        SetInd 12, "RESTMED count = " & ct
    ElseIf ct >= 1 Then
        ct = ct + ct389 + ct71
        If ct >= 3 Then SetInd 12, "RESTMED+FND100389+FND104071 count = " & ct
    End If
    If inds(12).checked Then Exit Sub
    ct = CountResultContains("", "FND104073", "", "", "")
    If ct >= 6 Then
        SetInd 12, "FND104073 count = " & ct
    ElseIf ct >= 1 Then
        ct = ct + ct389 + ct71
        If ct >= 3 Then SetInd 12, "FND104073+FND100389+FND104071 count = " & ct
    End If
    If inds(12).checked Then Exit Sub
    
    ct71 = CountResultContains("", "FND104073", "", "", "")
    
    ct = CountResultContains("", "FND867", "", "", "")
    If ct >= 6 Then
        SetInd 12, "FND867 count = " & ct
    ElseIf ct >= 1 Then
        ct = ct + ct389 + ct71
        If ct >= 3 Then SetInd 12, "FND867+FND100389+FND104073 count = " & ct
    End If
    If inds(12).checked Then Exit Sub
    
    ct = CountResultContains("", "FND868", "", "", "")
    If ct >= 6 Then
        SetInd 12, "FND868 count = " & ct
    ElseIf ct >= 1 Then
        ct = ct + ct389 + ct71
        If ct >= 3 Then SetInd 12, "FND868+FND100389+FND104073 count = " & ct
    End If
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_1213"
    Resume  'debug
End Sub

Private Sub Check_14()
    On Error GoTo errHandler
    Dim found_what As String
    
    dvprint "---------------"
    dvprint "14. Isolation"
    dvprint "---------------"

    SetIndIfFound 14, "", LIKE_PREFIX & "MRSA%"
    If inds(14).checked Then Exit Sub
    SetIndIfFound 14, "", "VRE POSITIVE,AIRBORN,CONAIR,BLEACH,CONTACT,DROPCON,DROPLET,NEUTRO"
    If inds(14).checked Then Exit Sub
    SetIndIfFound 14, "", "FND10484,FND10483,FND10482,FND100380,FND901,FND103031"
    
    
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_14"
    Resume  'debug
End Sub

Private Sub CheckAssessment(count As Integer, desc As String)
    Dim los_hours As Single

    If (inds(18).checked) Then Exit Sub             'skip if highest already checked
    If (count = 0) Then Exit Sub                    'skip if none

    If count >= 23 Then
        SetInd 18, desc
    ElseIf count >= 11 Then
        SetInd 17, desc
    ElseIf count >= 5 Then
        SetInd 16, desc
    ElseIf count >= 2 Then
        SetInd 15, desc
    End If

End Sub

Private Sub Check_15161718()
    Dim found_what As String
    Dim qmins As Integer
    Dim ct As Integer
    Dim ct1 As Integer
    Dim ct2 As Integer
    Dim ct3 As Integer
    Dim ct4 As Integer
    Dim ct5 As Integer
    Dim codelist As String
    Dim return_result As String
    
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "15. Assessment q4h"
    dvprint "16. Assessment q2h"
    dvprint "17. Assessment q1h"
    dvprint "18. Assessment q30min"
    dvprint "---------------"
    
' ct events q 12 hours - count distinct event times!
' select count(distinct(times)) where eventcode in ()
'select min(time) where code in () and time>=pull-24
'add 12 hours
'select count(distinct(times)) where code in () and time between min and min+12
'returnmaxcount(codeslist,n) in the past n hours
'3=14, 6=15, 12=16, 24=17

    ct = ReturnAssessCount("temp,HEART_RATE,Pulse,BP,Resp,O2Sat,CVP")
    CheckAssessment ct, "Vitals count=" & ct
    If inds(18).checked Then Exit Sub
    
    codelist = "FTL_HRT_RTE,BP_MEAN,PUL_CAP_WDG_P,PULM_ART_PRESS,GCS,"
    codelist = codelist & "BLOODGLUC,CIRCUM1,CIRCUM2,CIRCUM3,CIRC_UPARM_LT,CIRC_UPARM_RT,CIRC_LOWARM_LT,CIRC_LOWARM_RT,"
    codelist = codelist & "NUMERIC2,OTHER13,FND738,FND100897,CRD001,NUR001,NUR004,RSP001,RT001,RT2,"
    codelist = codelist & "RTO2,RTTX,NUR002,RTVENT,MEDRESP,PRN,"
codelist = codelist & "SURG,LAC,TUBE,WOU001,WOU002,IRF017,IRF019,WNDAS1,WNDAS2,WNDAS3,WOUND1,WOUND2,WOUND3,"
codelist = codelist & "RESPPE,SEDATION,RESPA,PCA005,EPIDURAL,EPIDR,SUB 1,ETOH1,OPI,SUB 28,TRACH1,FND104679"
codelist = codelist & "FND107710,FND107711,FND107712,FND107713"
    ct = ReturnAssessCount(codelist)
    CheckAssessment ct, "Assessment count=" & ct
    If inds(18).checked Then Exit Sub
    
' changed/removed 11/16
'    ct1 = CountResultContains("", LIKE_PREFIX & "MCK_IVPHL%", "", "", "", , , found_what)
'    ct2 = CountResultContains("", LIKE_PREFIX & "MCK_IVINF%", "", "", "", , , found_what)
    ct3 = CountResultContains("", LIKE_PREFIX & "MCK_GCS%", "", "", "", , , found_what)
    ct4 = CountResultContains("", LIKE_PREFIX & "MCK_CPS%", "", "", "", , , found_what)
    ct5 = CountResultContains("", LIKE_PREFIX & "MCK_FPS%", "", "", "", , , found_what)

    ct = ct3 + ct4 + ct5
    CheckAssessment ct, "Assessment count=" & ct
    If inds(18).checked Then Exit Sub

'
'codelist = "ALBUMIN25,ALBUMIN5,OTHER2,IRRIGATEBL,BLDVAG,BLOOD,BLDLOSS,BOTTLE,BM,FORMBM,INCONTBM,"
'codelist = codelist & "LIQUIDBM,LOOSEBM,OSTOMYBM,BREAKPRO,BREAKSUPP,BREASTBL,BREASTL,BREASTR,BREASTML,"
'codelist = codelist & "CTUBE3,CTUBE2,CTUBE1,COLOSBM,CONTRAST,CRYOPRE,IVFLUID6,IVFLUID4,IVFLUID10,IVFLUID11,"
'codelist = codelist & "IVFLUID12,IVFLUID5,IVFLUID14,IVFLUID3,IVFLUID8,IVFLUID7,IVFLUID13,IVFLUID15,"
'codelist = codelist & "DINNERSUPP,DRAIN2,DRAIN3,DRAIN4,OTHER3,EMESIS1,EMESIS2,EPIDURAL,EBL,"
'codelist = codelist & "GTUBE,JTUBE,NGTUBE,BFEED,FTFLUSH,GFLUSH,JFLUSH,NGFLUSH,FFP,"
'codelist = codelist & "GTUBE,GAVAGEN,HEMOVAC1,HEMOVAC2,CBI,DRAINING,IRRIGATE1,IRRIGFOLEY,IRRIGMURPH,"
'codelist = codelist & "TUBEIRRIG,JP1,JP2,JP3,IVFLUID1,LIPIDS,LUNCHPRO,LUNCHSUPP,MEALBREAK,MEALEVE,MEALNOON,"
'codelist = codelist & "NGTUBE,NEPHROSL,NEPHROSR,IVFLUID2,IVFLUID16,IVFLUID17,ORBLOOD,ORAL1,"
'codelist = codelist & "OTHER1,PRBC,PARACENTES,PENROSE,PERIPHPN,PPF,PLATELETS,REGURGLG,REGURGMOD,"
'codelist = codelist & "REGURGSMAL,SNACKAFT,SNACKEVE,SNACKMORN,DRAIN1,STOOL1,STOOL2,STOOLDIAP,STOOLMEC,SUPPERPRO,"
'codelist = codelist & "SYRINGE,TTUBE,THORACENTE,TPN,TUBE1,TUBE2,URINE,UCATHETER,ORCATH,PACUCATH,"
'codelist = codelist & "UDIAPER,FOLEY,ITEALCONDU,URINEINCON,MURPHYOUT,NEPHROSTOM,URESIDUAL,SELFCATH,"
'codelist = codelist & "STRAGHTCA,SUPRAPUBIC,UNMEASURED,UROSTOMY,VOIDED,URETERAL,VENTRICULO,WOUNDVAC"
'    ct = ReturnAssessCount(codelist)
'    CheckAssessment ct, "I&O count=" & ct
'    If inds(18).checked Then Exit Sub

    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_15161718"
    Resume  'debug
End Sub

Private Sub Check_19()
    Dim found_what As String
    Dim ct As Integer
    Dim ct1 As Integer
    Dim ct2 As Integer
    Dim ct3 As Integer
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "19. Vascular Access Site Mgt q1 Hour"
    dvprint "---------------"
    
    SetIndIfFound 19, "", "SL,IVTHER"
    If inds(19).checked Then Exit Sub
    
    ct1 = CountResultContains("", LIKE_PREFIX & "IV_INS_%", "", "", "", , , found_what)
    ct2 = CountResultContains("", LIKE_PREFIX & "MCK_IVPHL%", "", "", "", , , found_what)
    ct3 = CountResultContains("", LIKE_PREFIX & "MCK_IVINF%", "", "", "", , , found_what)
    If ct1 + ct2 + ct3 >= 12 Then SetInd 19, found_what  'changed 11/16
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_19"
    Resume  'debug
End Sub
Private Sub Check_20()
    Dim found_what As String
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "20. Medication Activity >= 20 minutes"
    dvprint "---------------"
    
    SetIndIfFound 20, "", "FND107318,FND107233,FND107208,FND104532"
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_20"
    Resume  'debug
End Sub

'Private Sub CheckWound(total As Integer)
'    If (inds(20).checked) Then Exit Sub             'skip if highest already checked
'
'    If (total = 0) Then
'        'skip it
'    ElseIf (total >= 30) Then
'        SetInd 20, "wound >= 30 min"
'    Else
'        SetInd 19, "wound < 30 min"
'    End If
'End Sub

Private Sub Check_2122()
    Dim return_result As String
    
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "21. Wound/Injury Mgmt"
    dvprint "22. Wound/Injury Mgmt >= 30 Minutes"
    dvprint "---------------"
    
    SetIndIfFound 22, "", "WoundVac,TRACH1,RWNEV1,RWNEV2,RWNTRN,FND107210"
    
    If inds(22).checked Then Exit Sub

    Set21WhenAge
    If inds(21).checked Then Exit Sub
    SetIndIfFound 21, "", "FND101766,FND906,FND102172,FND1118,FND149,FND976,FND812"
    SetIndIfFound 21, "", "FND1121,FND100807,FND104890,FND103469,FND103470,FND103487,FND104890"
    SetIndIfFound 21, "", "FND103506,FND103504,FND103505,FND1150,FND104672,FND104673"
    SetIndIfFound 21, "", "FND104675,FND104676,FND104677,FND954,FND107541,FND1150"
    SetIndIfFound 21, "", "FND100793,FND1113,FND100794,FND369,FND103468"
    SetIndIfFound 21, "", "FND108398,FND108399,FND108400,FND108401,FND108410,FND108199,FND108216"

    SetIndIfFound 21, "", "Drain3,MurphyOut,IrrigateBl,Drain2,JP2"

    If inds(21).checked Then Exit Sub
    SetIndIfFound 21, "", "FND10537,FND10542,FND10541,FND10547,FND103001,FND794,FND10538,FND10552"
    If inds(21).checked Then Exit Sub
    SetIndIfFound 21, "", "FND10553,FND10545,FND10544,FND10543,FND10551,FND10554,FND10540,FND899"
    If inds(21).checked Then Exit Sub
    SetIndIfFound 21, "", "FND10550,FND10555,FND10546,FND10539,FND103874,FND103875,FND10549,FND10548"
    If inds(21).checked Then Exit Sub
    SetIndIfFound 21, "", "FND1294,FND814,FND690,FND807,FND691,FND426,FND808"
    If inds(21).checked Then Exit Sub


    SetIndIfFound 21, "", "SURG,LAC,TUBE,WOU001,WOU002"
    If inds(21).checked Then Exit Sub
    SetIndIfFound 21, "", "IRF017,IRF019"
    If inds(21).checked Then Exit Sub
    SetIndIfFound 21, "", "WNDAS1,WNDAS2,WNDAS3,WOUND1,WOUND2,WOUND3"
    If inds(21).checked Then Exit Sub
    
    SetIndIfFound 21, "", "PP,Body Wound,IV_ACT_DRESSING_CHG"
    If inds(21).checked Then Exit Sub
    SetIndIfFound 21, "", "CORDCAR,CIRCCAR"

    If inds(21).checked Then Exit Sub
    SetIndIfFound 21, "", "Drain3,MurphyOut,IrrigateBl,Drain2,JP2"

    If inds(21).checked Then Exit Sub
    SetIndIfFound 21, "", "bldVag,EBL,BldLoss"

    If inds(21).checked Then Exit Sub
    SetIndIfFound 21, "", "Hemovac2,WoundVac"

    If inds(21).checked Then Exit Sub
    SetIndIfFound 21, "", "Hemovac1,JP3,Suprapubic"

    If inds(21).checked Then Exit Sub

    SetIndIfFound 21, "", "JP1,Ctube3,Drain4,Ctube1,Ctube2,Penrose,Thoracente"
    If inds(21).checked Then Exit Sub
    SetIndIfFound 21, "", "Ttube,Gtube,Paracentes"
    If inds(21).checked Then Exit Sub
    SetIndIfFound 21, "", "RWNEV1,RWNEV2,RWNTRN"

    If inds(21).checked Then Exit Sub
    SetIndIfFound 21, "", "FND103633,FND103481,FND102293,FND103311,FND103809"

    If inds(21).checked Then Exit Sub
    SetIndIfFound 21, "", "CHE001,CHEST2,CHEST1"

    If inds(21).checked Then Exit Sub
    SetIndIfFound 21, "", "CTACTOB,CTDRAIN,CHEST"

    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_2122"
    Resume  'debug
End Sub


Private Sub Check_23()
    Dim fnd284 As Boolean
    Dim fnd295 As Boolean
    
    On Error GoTo errHandler
    
    Dim mins As Integer
    Dim chart_result As String
    
    dvprint "---------------"
    dvprint "23. Healthcare Mgmt Education >= 1 Hour"
    dvprint "---------------"
    
    SetIndIfFound 23, "", "FND104685,FND1210,FND1192,FND1193,FND10488,FND10233"
    SetIndIfFound 23, "", "FND103493,FND103494,FND103495,FND103496,FND103497,FND103498,FND103499"
    SetIndIfFound 23, "", "FND1194,FND1196,FND1198,FND1209,FND1189,FND1199"
    SetIndIfFound 23, "", "FND1200,FND1201,FND1202,FND1203,FND1204,FND1205"
    SetIndIfFound 23, "", "FND1208,FND1211,FND1212,FND1213,FND1214,FND107682"

    If inds(23).checked Then Exit Sub
    
    If Exists("", "FND105284") Then fnd284 = True
    If Exists("", "FND105295") Then fnd295 = True
   
    If fnd284 Then
        SetIndIfFound 23, "", "AMI 1"
        If inds(23).checked Then Exit Sub
        If fnd295 Then SetInd 23, "FND105284+FND105295"
        If inds(23).checked Then Exit Sub
    End If
    If fnd284 Or fnd295 Then
        SetIndIfFound 23, "", "FND105296,FND105297,FND105298,FND105299,FND105300"
        If inds(23).checked Then Exit Sub
        SetIndIfFound 23, "", "FND105301,FND105302,FND105313,FND105304,FND105305"
        If inds(23).checked Then Exit Sub
    End If
    
    If Exists("", "FND105283") Then
        If Exists("", "FND105286") And Exists("", "FND105287") Then
            SetInd 23, "FND105283+FND105286+FND105287"
        End If
    End If
    If inds(23).checked Then Exit Sub

    If Exists("", "FND106597") Then
        If Exists("", "FND105296,FND105297,FND105298,FND105299,FND105300,FND105301,FND105302,FND105313,FND105304,FND105305") Then
            SetInd 23, "FND106597"
        End If
    End If
    If inds(23).checked Then Exit Sub

    If Exists("", "FND105306") Then
        If Exists("", "FND296") Then
            SetInd 23, "FND105306+FND296"
        End If
    End If
    If inds(23).checked Then Exit Sub

    SetIndIfFound 23, "", "CHFT,FND105281,FND105284,FND105347,FND105348,FND105349"
    If inds(23).checked Then Exit Sub
    SetIndIfFound 23, "", "FND106598,FND105390,FND105391,FND841,FND829"
    If inds(23).checked Then Exit Sub
    SetIndIfFound 23, "", "FND102296,FND102297,FND105444,FND104428"
    If inds(23).checked Then Exit Sub
    SetIndIfFound 23, "", "FND1036,FND1342,FND1341,FND1035"
    If inds(23).checked Then Exit Sub
    SetIndIfFound 23, "", "FND1217,FND391,FND1222,FND104429,FND1221"
    If inds(23).checked Then Exit Sub
    SetIndIfFound 23, "", "FND367,FND1037,FND1038,FND1040,FND1041"
    If inds(23).checked Then Exit Sub
    SetIndIfFound 23, "", "FND101861,FND101862,FND101863,FND101864,FND930,FND107206"
    If inds(23).checked Then Exit Sub

    If Exists("", "FND105419") Then
        If Exists("", "FND10535,FND105422,FND105433") Then
            SetInd 23, "FND105419"
        End If
    End If
    If inds(23).checked Then Exit Sub
    
    If Exists("", "FND106599") Then
        If Exists("", "FND105391") Then
            SetInd 23, "FND106599"
        End If
    End If
    If inds(23).checked Then Exit Sub
    
'FND106782     If used with FND106600 AND (Fnd105446 or FND105529) AND FND105533
    If Exists("", "FND106782") Then
        If Exists("", "FND106600") And Exists("", "FND105533") And (Exists("", "Fnd105446") Or Exists("", "FND105529")) Then
            SetInd 23, "FND106782"
        End If
    End If
    If inds(23).checked Then Exit Sub
    
    If Not Exists("", "FND105929,FND10155,FND105365,FND102300,FND105282") Then
        SetIndIfFound 23, "", "FND102301,FND103716,FND100811"
    End If
    If inds(23).checked Then Exit Sub
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_23"
    Resume  'debug
End Sub

Private Sub Check_24()
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "24. 1 to 1 Physiological Interv. >= 2 Hours"
    dvprint "---------------"

    SetIndIfFound 24, "", "code,CODEM,FND107209"

    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_24"
    Resume  'debug
End Sub
Private Sub CheckCustom()
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "33. Central Line"
    dvprint "34. Ventilator"
    dvprint "35. Foley Catheter"
    dvprint "---------------"

    SetIndIfFound 33, "", "CENTRAL_LINE,GROSHONG,HICKMAN,IMPLANTED_PORT,PICC,POWER_PORT,ADAPTER"
    If Not inds(33).checked Then
        'until discontinued
        SetIndIfFound 33, "", "CENTRL,CRLDSG,PICCARE"
    End If

    If Not inds(33).checked Then
        SetIndIfFound 33, "", "FND103633,FND103481"
    End If

    If Not inds(33).checked Then
        SetIndIfFound 33, "", "FND103311,FND103809,FND103338,FND103339,FND103340,FND103341,FND103342,FND103343,FND103344"
    End If
    
    If Not inds(33).checked Then
    If Exists("", "FND102293") Then
        If Exists("", "FND10338,FND10339,FND10340,FND10341,FND10342,FND10343,FND10344") Then
            SetInd 33, "FND102293"
        End If
    End If
    End If

    SetIndIfFound 34, "", "FND10386,FND10387,FND10388,FND10390,FND10391,VENTCHG,VENT1ST"
    If Not inds(34).checked Then
        SetIndIfFound 34, "", "FND10360,FND10361,FND10456,FND10363,FND101679,FND103618,FND103621,FND103622"
    End If


    If Not Exists("", "FND107398") Then   'FND107250 negates 35 chngd to -398 on 11/16
    SetIndIfFound 35, "", "FOLEY,Ucatheter,FOLEYIR"
    If Not inds(35).checked Then
        SetIndIfFound 35, "", "FND188,CATH1"
    End If
    If Not inds(35).checked Then
        SetIndIfFound 35, "", "FND100323,FND100321,FND100322,FND104310,FND104309"
    End If
    If Not inds(35).checked Then
        SetIndIfFound 35, "", "FND104315,FND102318,FND100594,FND100325,FND100326"
    End If
    If Not inds(35).checked Then
        SetIndIfFound 35, "", "FND107399,FND107400,FND107401,FND107402,FND107403"
    End If
    If Not inds(35).checked Then
        SetIndIfFound 35, "", "FND107404,FND107405"
    End If
    End If 'negate
    
    SetIndIfFound 36, "", "FND103125,FND103126,FND103106,FND103448,FND103108,FND103183,FND103149,FND162,FND1273,FND867,FND103150,FND868,Restraints"

    Exit Sub
    
errHandler:
    g_util.ThrowError "CheckCustom"
    Resume  'debug
End Sub

Private Sub ProcessProc(pnum As Integer, code1 As String, code2 As String, code3 As String)
    Dim d As Date
'
'499872183
'499867279
'    If Exists("", "499867279", , , EXACT_MATCH_PREFIX & "RN") Then
'        If SumEventDateTime() > 1 Then
'        End If
'    End If

End Sub
'
'Private Sub CheckProcs()
'    Dim return_result As String
'    Dim proc8, proc9 As Boolean
'
'    On Error GoTo errHandler
'    dvprint "---------------"
'    dvprint "P1. 1-1 safety observation by non-RN"
'    dvprint "---------------"
'
'    ProcessProc 1, "OLAMBPROG8", "OLAMBPRO02", "OLAMBPR03"
'
'    dvprint "---------------"
'    dvprint "P2. Off unit accompanied by RN"
'    dvprint "---------------"
'
'    ProcessProc 2, "OLACUITY25", "OLACUITY30", "OLACUITY31"
'
'    dvprint "---------------"
'    dvprint "P3. Off unit accompanied by non-RN"
'    dvprint "---------------"
'
'    ProcessProc 3, "OLACUITY20", "OLACUITY32", "OLACUITY33"
'
'    dvprint "---------------"
'    dvprint "P4. Patient/family education by RN"
'    dvprint "---------------"
'
'    ProcessProc 4, "OLACUITY21", "OLACUITY45", "OLACUITY46"
'
'    dvprint "---------------"
'    dvprint "P5. Extensive wound management by RN"
'    dvprint "---------------"
'
'    ProcessProc 5, "OLACUITY22", "OLACUITY34", "OLACUITY35"
'
'    dvprint "---------------"
'    dvprint "P6. Extensive wound management by non-RN"
'    dvprint "---------------"
'
'    ProcessProc 6, "OLACUITY23", "OLACUITY36", "OLACUITY37"
'
'    dvprint "---------------"
'    dvprint "P7. Coordination of care by RN"
'    dvprint "---------------"
'
'    ProcessProc 7, "OLACUITY24", "OLACUITY43", "OLACUITY44"
'
'    dvprint "---------------"
'    dvprint "P8&P9. 1-1 or 2-1 by RN at bedside"
'    dvprint "---------------"
'
'    'olacuity59 and olacuity60 and ptunitmatchescode on both
'    proc8 = False
'    proc9 = False
'    If GetResult("", "OLACUITY59", "", "", return_result) Then
'        If PtUnitMatchesCode(return_result) Then
'            ProcessProc 8, "OLACUITY26", "OLACUITY38", "OLACUITY39"
'            If procs(numprocs).pnum = 8 Then proc8 = True
'            If GetResult("", "OLACUITY60", "", "", return_result) Then
'                If PtUnitMatchesCode(return_result) Then
'                    ProcessProc 9, "OLACUITY27", "OLACUITY41", "OLACUITY42"
'                    If procs(numprocs).pnum = 9 Then proc9 = True
'                End If
'            End If
'        End If
'    End If
'
'    If proc9 Then
'        If proc8 Then procs(numprocs - 1) = procs(numprocs)
'        With procs(numprocs)
'            .pindex = 0
'            .start = 0
'            .finish = 0
'            .isvalid = False
'            .pnum = 0
'        End With
'        numprocs = numprocs - 1
'    End If
'
''    dvprint "---------------"
''    dvprint "P8. 1-1 by RN at bedside"
''    dvprint "---------------"
''
''    'olacuity59 and ptunitmatchescode
''    If GetResult("", "OLACUITY59", "", "", return_result) Then
''        If PtUnitMatchesCode(return_result) Then
''            ProcessProc 8, "OLACUITY26", "OLACUITY38", "OLACUITY39"
''        End If
''    End If
'
'
'    Exit Sub
'
'errHandler:
'    g_util.ThrowError "CheckProcs"
'    Resume  'debug
'End Sub
'Private Sub CheckOutcomes()
'    Dim return_result As String
'
'    'How to handle multiple outcomes per pt, each with different times?
'    dvprint "---------------"
'    dvprint "Outcomes: FALL"
'    dvprint "---------------"
'    If GetResult("", "FALL", "", "", return_result, SearchOutcomesRange) Then
'        numoutcomes = numoutcomes + 1
'        With oc(numoutcomes)
'            .checked = True
'            .pnum = 1
'            .start = return_result
'        End With
'    End If
'
'    dvprint "---------------"
'    dvprint "Outcomes: MED"
'    dvprint "---------------"
'    If GetResult("", "MED", "", "", return_result, SearchOutcomesRange) Then
'        numoutcomes = numoutcomes + 1
'        With oc(numoutcomes)
'            .checked = True
'            .pnum = 2
'            .start = return_result
'        End With
'    End If
'
'    dvprint "---------------"
'    dvprint "Outcomes: PROC"
'    dvprint "---------------"
'    If GetResult("", "PROC", "", "", return_result, SearchOutcomesRange) Then
'        numoutcomes = numoutcomes + 1
'        With oc(numoutcomes)
'            .checked = True
'            .pnum = 21
'            .start = return_result
'        End With
'    End If
'
'    Exit Sub
'
'errHandler:
'    g_util.ThrowError "CheckOutcomes"
'    Resume  'debug
'End Sub


'Private Sub AtLeastOneADL()
'    If Not (inds(1).checked Or inds(2).checked Or inds(3).checked) Then
'        'Note: Heather wants "#2 ADL - Assist" to be the default.  (90% of patients)
'        SetInd 2, "at least one ADL"
'    End If
'End Sub

Private Sub HighestIndicatorInEachGroupWins()
    Dim i As Integer
    Dim g As Integer
    Dim highest_is_on As Boolean
    Dim ind_list As String

    dvprint "---------------"
    dprint "Select highest indicator in each group"
    
    g = 0
    highest_is_on = False
    For i = MAX_INDS To 1 Step -1
        If (inds(i).group > 0) Then
            If (inds(i).group <> g) Then
                'this is a new group
                g = inds(i).group
                highest_is_on = inds(i).checked
            Else
                'same group
                If highest_is_on Then
                    inds(i).checked = False             'uncheck a lower number
                Else
                    highest_is_on = inds(i).checked     'save this one
                End If
            End If
        End If
    Next i
    
    'Echo the indicators for an audit (no classification will be saved)
    If g_debug And g_no_output Then
        For i = 1 To MAX_INDS
            If inds(i).checked Then ind_list = ind_list & "," & i
        Next i
        dprint "Final list = " & Mid$(ind_list, 2)
    End If

End Sub

Private Sub OutputClass()
    Dim outstr As String, ind_list As String
    Dim i As Integer

tc_event_id = g_dbutil.NextGID

    outstr = g_util.FixedWidth("", 8)                                       '(facility code)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.unit_name, 16)
    outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(unit code)
    outstr = outstr & "|" & g_util.FixedWidth(txarea, 16)                       '(area code)
    outstr = outstr & "|" & g_util.FixedWidth("", 8)                        '(class date - give datetime instead)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.acct, 20)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.last_name, 32)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.first_name, 32)
    outstr = outstr & "|" & g_util.FixedWidth("", 32)                       '(middle)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.room, 8)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.bed, 4)
    outstr = outstr & "|" & Format$(m_pat.effective, "yyyymmddhhnn")      'class datetime
    outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(login)
    outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(employee)
    outstr = outstr & "|" & g_util.FixedWidth(CStr(m_pat.meth_id), 4)
    outstr = outstr & "|" & g_util.FixedWidth("C", 1)                       'record type = class
    outstr = outstr & "|" & g_util.FixedWidth("", 4)                        '(stage)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.TC_source_id, 4)        'TC source ID
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.range, 4)               'TC pull range
    outstr = outstr & "|" + g_util.FixedWidth(tc_event_id, 10)            'TC event ID
    outstr = outstr & "|"
    outstr = g_util.FixedWidth(outstr, 294)
    outstr = outstr & "|" & Format$(m_pat.effective, "yyyymmddhhnn")        'IN
    outstr = g_util.FixedWidth(outstr, 377)
    outstr = outstr & "|"
    
    For i = 1 To MAX_INDS
        If (inds(i).checked) Then
            outstr = outstr & "Y"
            ind_list = ind_list & "," & i
        Else
            outstr = outstr & "N"
        End If
    Next i
    ind_list = Mid$(ind_list, InStr(1, ind_list, ",") + 1)                'strip leading comma
    
    Print #outfile, outstr
    g_event.DoAddEventLogEntry EVENT_SOURCE_TRANSPARENT_MAPPING, EVENT_TYPE_INFO, EVENT_CATEGORY_PROCESSED, _
        "Classified: " & ind_list, Null, 0, m_pat.unit_id, m_pat.encounter_id, _
        tc_event_id, "8.4", g_brief_audit, g_verbose_audit
    
    AddLogEntry EVENT_TYPE_INFO, "Classified: " & ind_list, EVENT_CATEGORY_PROCESSED
End Sub

Private Sub OutputProcs()
'    Dim i, j As Integer
'    Dim s, f As Date
'    Dim outstr As String, proc_list As String
'
'    For i = 1 To numprocs
'        procs(i).isvalid = True
'    Next i
'    For i = 1 To numprocs - 1
'        If procs(i).isvalid Then
'        For j = i + 1 To numprocs
'            If procs(j).isvalid And procs(j).start = procs(i).start And procs(j).finish = procs(i).finish Then   'this can be combined.
'                procs(j).isvalid = False
'                procs(j).pindex = i
'            End If
'        Next j
'        End If
'    Next i
'
'    For i = 1 To numprocs
'        If procs(i).isvalid Then
'        outstr = g_util.FixedWidth("", 8)                                       '(facility code)
'        outstr = outstr & "|" & g_util.FixedWidth(m_pat.unit_name, 16)
'        outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(unit code)
'        outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(area code)
'        outstr = outstr & "|" & g_util.FixedWidth("", 8)                        '(class date - give datetime instead)
'        outstr = outstr & "|" & g_util.FixedWidth(m_pat.acct, 20)
'        outstr = outstr & "|" & g_util.FixedWidth(m_pat.last_name, 32)
'        outstr = outstr & "|" & g_util.FixedWidth(m_pat.first_name, 32)
'        outstr = outstr & "|" & g_util.FixedWidth("", 32)                       '(middle)
'        outstr = outstr & "|" & g_util.FixedWidth(m_pat.room, 8)
'        outstr = outstr & "|" & g_util.FixedWidth(m_pat.bed, 4)
'        outstr = outstr & "|" & Format$(procs(i).start, "yyyymmddhhnn")       '204 procdt
'        outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(login)
'        outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(employee)
'        outstr = outstr & "|" & g_util.FixedWidth(CStr(m_pat.meth_id), 4)
'        outstr = outstr & "|" & g_util.FixedWidth("P", 1)                       'record type = class
'        outstr = outstr & "|"
'        outstr = outstr & Space$(294 - Len(outstr))
'        outstr = outstr & "|" & Format$(procs(i).start, "yyyymmddhhnn")       '296 procdt in
'        outstr = outstr & Space$(346 - Len(outstr))
'        If procs(i).finish = 0 Then
'            outstr = outstr & "|" & Space$(12)
'        Else
'            outstr = outstr & "|" & Format$(procs(i).finish, "yyyymmddhhnn")      '348 procdt out
'        End If
'        outstr = g_util.FixedWidth(outstr, 377)
'        outstr = outstr & "|NNNNNNNNN"
'        Mid$(outstr, 378 + procs(i).pnum, 1) = "Y"
'        proc_list = proc_list & "," & procs(i).pnum
'        For j = i + 1 To numprocs
'            If Not procs(j).isvalid And procs(j).pindex = i Then
'                Mid$(outstr, 378 + procs(j).pnum, 1) = "Y"
'                proc_list = proc_list & "," & procs(j).pnum
'            End If
'        Next j
'        proc_list = Mid$(proc_list, InStr(1, proc_list, ",") + 1)          'strip leading comma
'
'        Print #outfile, outstr
'
'        AddLogEntry EVENT_TYPE_INFO, "Procedure: " & proc_list, EVENT_CATEGORY_PROCESSED
'        End If 'isvalid
'    Next i
'
End Sub
Private Sub OutputOutcomes()
'    Dim i, j As Integer
'    Dim s, f As Date
'    Dim outstr As String
'    Dim octime As String
'
'    If numoutcomes = 0 Then Exit Sub
'
'    For i = 1 To numoutcomes
'        If (oc(i).checked) Then
'            outstr = g_util.FixedWidth("", 8)
'            outstr = outstr & "|" & g_util.FixedWidth(m_pat.unit_name, 16)
'            outstr = outstr & "|" & g_util.FixedWidth("", 16)
'            outstr = outstr & "|" & g_util.FixedWidth("", 16)
'            outstr = outstr & "|" & g_util.FixedWidth(oc(i).start, 12)
'            outstr = outstr & "|" & g_util.FixedWidth(m_pat.acct, 20)
'            outstr = outstr & "|" & g_util.FixedWidth(oc(i).pnum, 3)
'            Print #outfile2, outstr 'Print line to outcomesindicator.TXT
'        End If
'    Next i
End Sub


'Private Function ReturnMaxCount(codelist As String, nmins As Integer) As Integer
'    Dim sql As String
'    Dim rs As New Recordset
'    Dim ct As Integer
'    Dim done_ct As Integer
'    Dim mintime As Date
'    Dim maxtime As Date
'    Dim done As Boolean
'
'    ReturnMaxCount = 0
'
'    'do an initial count to see if warrants continuing; set the mintime of all codes
'    sql = "select count(distinct(event_datetime)),min(event_datetime) from chart_item " & WhereBase
'    sql = sql & " and code in (" & codelist & ") and event_datetime is not null"
'    rs.Open sql, g_cnADO
''    dvprint sql
'
'    If Not rs.EOF Then
'        If IsNull(rs(0)) Then
'            dvprint "No documentation found for this category of assessments."
'            rs.Close
'            Exit Function
'        ElseIf rs(0) = 0 Then
'            dvprint "No documentation found for this category of assessments."
'            rs.Close
'            Exit Function
'        End If
'    Else
'        dvprint "No documentation found for this category of assessments."
'        rs.Close
'        Exit Function
'    End If
'
'    If Not IsNull(rs(0)) Then
'        ct = rs(0)
'    Else
'        ct = 0
'    End If
'    If Not IsNull(rs(1)) Then
'        mintime = rs(1)
'    Else
'        mintime = #1/1/1900#
'    End If
'
'    If ct < 3 Then
'        dvprint "Insufficient documentation found for this category of assessments."
'        ReturnMaxCount = ct
'        rs.Close
'        Exit Function
'    End If
'
'    rs.Close
'    done = False
'    done_ct = 0
'    Do
'        maxtime = DateAdd("h", 12, mintime)
'        If maxtime > m_pat.pull_finish Then
'            maxtime = m_pat.pull_finish
'            done = True
'        End If
'        'count the event times within a 12 hour range btween mintime and maxtime
'        sql = "select count(distinct(event_datetime)) from chart_item " & WhereBase(SearchAssessments)
'        sql = sql & " and code in (" & codelist & ")"
'        sql = sql & " and event_datetime between " & g_dbutil.SQL_DateTime(mintime) & " and " & g_dbutil.SQL_DateTime(maxtime)
''    dvprint sql
'        rs.Open sql, g_cnADO
'        If rs.EOF Then
'            done = True
'        ElseIf Not IsNull(rs(0)) Then
'            ct = rs(0)
'            If ct >= 24 Then 'dont look any further
'                done_ct = ct
'                done = True
'            ElseIf ct > done_ct Then 'save the greatest count
'                done_ct = ct
'            End If
'        End If
'        rs.Close
'        If Not done Then
'            'Now get the next greatest mintime, looking AFTER the current mintime
'            sql = "select min(event_datetime) from chart_item " & WhereBase
'            sql = sql & " and code in (" & codelist & ") AND event_datetime>" & g_dbutil.SQL_DateTime(mintime)
''        dvprint sql
'            rs.Open sql, g_cnADO
'            If rs.EOF Then
'               done = True
'            ElseIf Not IsNull(rs(0)) Then
'                mintime = rs(0)
'            Else
'                done = True
'            End If
'            rs.Close
'        End If
'    Loop Until done
''    rs.Close
''Start by getting total count and min time.  if total count < 3 then don't even bother continuing.
''Loop
''    maxtime=mintime+12 hours
''    count where eventime>=mintime and eventime<=maxtime
''    if count>=24 then 17 and stop
''    elseif maxtime > pull_finish then end
''    else
''
''    sql = "select min(event_datetime) from chart_item " & WhereBase
''    sql = sql & " and code in (" & codelist & ") AND eventtime>mintime"
''    rs.Open sql, g_cnADO
''
''endloop
''THE PROBLEM with this loop is that it might advance by only 1 minute per iteration.
''IS THERE A more intelligent way of doing this?  Try advancing by 1 hour at a time from mintime upwards if it takes too long.
'
'    ReturnMaxCount = done_ct
'
'End Function
'' ct events q 12 hours - count distinct event times!
'' select count(distinct(times)) where eventcode in ()
''select min(time) where code in () and time>=pull-24
''add 12 hours
''select count(distinct(times)) where code in () and time between min and min+12
''returnmaxcount(codeslist,n) in the past n hours
''3=14, 6=15, 12=16, 24=17
Private Function ReturnAssessCount(codelist As String) As Integer
    Dim sql As String
    Dim rs As New Recordset
    Dim ct As Integer
    Dim done_ct As Integer
    Dim mintime As Date
    Dim maxtime As Date
    Dim done As Boolean
    
    ReturnAssessCount = 0

    sql = "select count(distinct(event_datetime)) from chart_item " & WhereBase & AndSimpleItemFilter("", codelist, "", "", "")
    sql = sql & " and event_datetime is not null"
    dvprint sql
    rs.Open sql, g_cnADO
'    dvprint sql

    If Not rs.EOF Then
        If IsNull(rs(0)) Then
            ct = 0
            rs.Close
            Exit Function
        Else
            ct = rs(0)
            rs.Close
        End If
    Else
        ct = rs(0)
        rs.Close
        Exit Function
    End If
    
    ReturnAssessCount = ct
    
End Function

'Private Function SumEventDateTime(code As String, res As String) As Date
'    Dim sql As String
'    Dim sql2 As String
'    Dim total As Integer
'    Dim rs As New Recordset
'    Dim rs2 As New Recordset
'
'    sql = "select event_datetime from chart_item " & WhereBase
'    sql = sql & " and code=" & g_dbutil.SQL_String(code) & " and result=" & g_dbutil.SQL_String(res) & " and event_datetime is not null"
'    rs.Open sql, g_cnADO
'
'    Do While Not rs.EOF
'        If Not IsNull(rs(0)) Then
'            sql2 = "select sum(result) from chart_item " & WhereBase
'            sql2 = sql & " and code=" & g_dbutil.SQL_String(code) & " and event_datetime=" & g_dbutil.SQL_Date(rs(0))
'            rs2.Open sql2, g_cnADO
'            If Not rs2.EOF Then
'                If Not IsNull(rs2(0)) Then
'                    If IsNumeric(rs2(0)) Then
'                        total = total + rs2(0)
'                    End If
'                End If
'            End If
'        End If
'        rs.MoveNext
'    Loop
'    rs2.Close
'    rs.Close
'    SumEventDateTime = total
'End Function

Private Sub SetTxForICUPt()
        'set m_pat.pull_start to beginning of last SERVICE (or leave it as is if beginning < m_pat.pull_start).
        'set tx area to the service.
    Dim rs As New Recordset
    Dim sql As String
    Dim serv As String
    Dim isicu As Boolean
    Dim indt As Date, mindtin As Date, earliestdt As Date
    Dim done As Boolean

    sql = "select max(datetime_in) from encounter_location where encounter_id=" & m_pat.encounter_id
    sql = sql & " and unit_id=" & m_pat.unit_id
    rs.Open sql, g_cnADO
    If Not rs.EOF Then indt = rs(0)
    rs.Close
    
    sql = "select hospital_service_lvc from encounter_location where encounter_id=" & m_pat.encounter_id
    sql = sql & " and datetime_in=" & g_dbutil.SQL_DateTime(indt)
    dvprint sql
    rs.Open sql, g_cnADO
    If Not rs.EOF Then serv = Trim$(rs(0))
    rs.Close
    isicu = (serv = "ICU")
'Now we know the last service
'But when did this service start?
'Get the min dt in where the service started -- we don't need to go through entire LOS of pt, so set a lowerbound=mindt
    sql = "select max(datetime_in) from encounter_location where encounter_id=" & m_pat.encounter_id
    sql = sql & " and datetime_in<=" & g_dbutil.SQL_DateTime(m_pat.pull_start)
    dvprint sql
    rs.Open sql, g_cnADO
    If Not rs.EOF Then mindtin = rs(0)
    rs.Close
    
    sql = "select datetime_in,hospital_service_lvc from encounter_location where encounter_id=" & m_pat.encounter_id
    sql = sql & " and datetime_in>=" & g_dbutil.SQL_DateTime(mindtin) & " order by datetime_in desc"
    dvprint sql
    rs.Open sql, g_cnADO
    done = False
    Do While Not rs.EOF And Not done
        If isicu Then
            If Trim$(rs(1)) = "ICU" Then
                earliestdt = rs(0)
            Else
                done = True
            End If
        Else
            If Trim$(rs(1)) <> "ICU" Then
                earliestdt = rs(0)
            Else
                done = True
            End If
        End If
        rs.MoveNext
    Loop
    rs.Close
    
    If earliestdt > m_pat.pull_start Then m_pat.pull_start = earliestdt
    If isicu Then
        txarea = "ICU"
    Else
        txarea = "MED"
    End If

End Sub

