using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Win32;                      // for ODBC
using System.Data.SqlClient;                // for SqlConnection
using System.Windows.Forms;                 // for MessageBox (also add reference)
using System.IO;                            // for Directory
using System.Diagnostics;                   // for Process
using System.Data;                          // for DataTable
// Add a reference to System.Data.Linq

namespace PfsShared
{
    // Generic utility functions
    // These static functions can be called any time without creating an object

    public static class PFSUtility
    {
        //=====================================================================
        // Database Connection
        //=====================================================================

        /// <summary>
        /// Returns an open connection for use with SQL commands.
        /// You must then create a SqlCommand and a DataReader.
        /// </summary>
        /// <returns></returns>
        public static SqlConnection NewSqlConnection()
        {
            SqlConnection db = new SqlConnection(MakeConnectionString());
            db.Open();
            return db;
        }

        /// <summary>
        /// Returns an open context for use with LINQ commands.
        /// You must then create a LINQ expression.
        /// </summary>
        /// <returns></returns>
        public static PfsDataContext NewPfsDataContext()
        {
            return new PfsDataContext(MakeConnectionString());
        }

        private struct WinPfsConnectionInfo
        {
            public string server, dbname;
        }

        private static WinPfsConnectionInfo GetWinPfsConnectionInfo()
        {
            WinPfsConnectionInfo result;
            // Read ODBC definition to get server name and default database.
            // The default database is usually not defined, but can be.
            string odbcRoot = @"HKEY_LOCAL_MACHINE\SOFTWARE\ODBC\ODBC.INI\WinPFS";
            result.server = (string)Registry.GetValue(odbcRoot, "Server", "");
            result.dbname = (string)Registry.GetValue(odbcRoot, "Database", "pfs");
            if (String.IsNullOrEmpty(result.server))
            {
                // The VB setup program only creates a 32-bit ODBC entry.
                // If we are a 64-bit app installed by the 32-bit VB setup we need to
                // look in the Wow6432Node for the WinPFS definition.
                odbcRoot = @"HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\ODBC\ODBC.INI\WinPFS";
                result.server = (string)Registry.GetValue(odbcRoot, "Server", "");
                result.dbname = (string)Registry.GetValue(odbcRoot, "Database", "pfs");
            }
            return result;
        }

        private static string MakeConnectionString()
        {
            var info = GetWinPfsConnectionInfo();

            if (String.IsNullOrEmpty(info.server))
                throw new MissingFieldException("Missing WinPFS ODBC entry");
            if (info.dbname == "master")                     // ODBC is wrong - cannot be master
                info.dbname = "pfs";

            return "Data Source=" + info.server +
                "; Initial Catalog=" + info.dbname +
                "; Connection Timeout=20" +                 //up from 15
                "; User Id=winpfs; Password=pfs4you";
        }

        // You can retry some database errors, we should just give up with these
        public static bool IsFatalDatabaseError(string errmsg)
        {
            string msg = errmsg.ToLower();
            return msg.Contains("communication link") ||    // failure
                msg.Contains("network") ||
                msg.Contains("connectionread") ||
                msg.Contains("connectionwrite") ||
                msg.Contains("terminated") ||
                msg.Contains("the transaction log");        // is full
        }

        /// <summary>
        /// Execute a (non-query) command and return the number of rows affected.
        /// This is good for INSERT, UPDATE and DELETE.
        /// </summary>
        /// <returns></returns>
        public static int ExecuteSQL(string sql)
        {
            using (var db = PFSUtility.NewSqlConnection())
            {
                var cmd = new SqlCommand(sql, db);
                return cmd.ExecuteNonQuery();
            }
        }

        //=====================================================================
        // Database Utilities
        //=====================================================================
        /// <summary>
        /// Return the server's current date and time - note this is a round-trip call
        /// </summary>
        public static DateTime ServerDateTime()
        {
            var result = DateTime.Now;                          // default to workstation time (just in case)
            using (var db = PFSUtility.NewSqlConnection())      // auto dispose & close
            {
                var cmd = new SqlCommand("SELECT GETDATE()", db);
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read()) result = DBToDateTime(dr[0]);
                dr.Close();
            }
            return result;
        }

        //
        // Unique IDs from SYS_SEQUENCE
        //
        private const string GID_SEQUENCE_NAME = "GID";
        private const int    GID_DEFAULT_BLOCKSIZE = 10;

        private static int gid_blocksize = 0;
        private static int gid_next_id = 0;
        private static int gid_last_id = 0;

        public static void ReserveGIDs(int num_ids)
        {
            gid_blocksize = num_ids;                  // save for next call to NextGID
        }

        /// <summary>
        /// Return the next ID from the database "GID" sequence (Global ID)
        /// </summary>
        public static int NextGID()
        {
            int result;
    
            // have some ids already been reserved?
            if ((gid_next_id <= gid_last_id) && (gid_next_id != 0)) {
                // yes, return the next one
                result = gid_next_id;
            } else {
                //Use the default block size unless ReserveGIDs was called
                if (gid_blocksize < 1) gid_blocksize = GID_DEFAULT_BLOCKSIZE;
                //Get a new block of numbers
                result = AllocateIDs(GID_SEQUENCE_NAME, gid_blocksize);
                //Save the last valid number
                gid_last_id = result + gid_blocksize - 1;
                //gid_blocksize = 0;                    //revert to default for next time
            }

            gid_next_id = result + 1;                   // ready for next time
            return result;
        }

        public static int AllocateIDs(string sequence_name, int num_ids)
        {
            //Allocate a block of IDs from the SYS_SEQUENCE table
            var db = PFSUtility.NewSqlConnection();
            string sql;
            int next_id = 0;
            
            do {
                var tran = db.BeginTransaction();

                try {
                    sql = "SELECT NEXT_VALUE FROM SYS_SEQUENCE WHERE SEQUENCE_NAME=" + PFSUtility.SQLString(sequence_name);
                    Debug.WriteLine(sql);
                    var cmd = new SqlCommand(sql, db, tran);
                    var dr = cmd.ExecuteReader();
                    if (dr.Read()) {
                        next_id = (dr["NEXT_VALUE"] as int? ?? 0);      //get the next id
                    }
                    dr.Close();

                    if (next_id == 0) {
                        // This is a new sequence
                        next_id = 1;                                    //always start at one, not zero.
                        var cmd2 = new SqlCommand(sql, db, tran);
                        cmd2.CommandText = "INSERT INTO SYS_SEQUENCE (SEQUENCE_NAME, NEXT_VALUE) VALUES (@name, @value)";
                        cmd2.Parameters.AddWithValue("@name", sequence_name);
                        cmd2.Parameters.AddWithValue("@value", next_id);
                        cmd2.Transaction = tran;
                        Debug.WriteLine(cmd.CommandText);
                        cmd2.ExecuteNonQuery();
                    } else {
                        var cmd2 = new SqlCommand(sql, db, tran);
                        cmd2.CommandText = "UPDATE SYS_SEQUENCE SET NEXT_VALUE=@value WHERE SEQUENCE_NAME=@name";
                        cmd2.Parameters.AddWithValue("@value", next_id + num_ids);
                        cmd2.Parameters.AddWithValue("@name", sequence_name);
                        cmd2.Transaction = tran;
                        Debug.WriteLine(cmd.CommandText);
                        cmd2.ExecuteNonQuery();
                    }
                    tran.Commit();
                }
                catch (DBConcurrencyException e) {
                    // Two of us tried to update the same sequence at the same time
                    Debug.WriteLine(e.Message);
                    tran.Rollback();
                    Sleep(500);
                }
                catch (SqlException e) {
                    // ** We need to die if this was a fatal database error
                    Debug.WriteLine(e.Message);
                    break;
                }

            } while (next_id == 0);

            db.Close();

            return next_id;
        }
        

        //=====================================================================
        // Help creating SQL commands (used extensively in report writer)
        //=====================================================================

        public static string SQLBoolean(bool v)
        {
            return SQLString(v ? "Y" : "N");
        }

        //Make a region-independent date for a SQL command.
        public static string SQLDate(DateTime dt)
        {
            return "'" + dt.ToString("yyyyMMdd") + "'";
        }

        public static string SQLTime(DateTime dt)
        {
            return "'" + dt.ToString("HH:mm:ss") + "'";
        }

        public static string SQLDateTime(DateTime dt)
        {
            return "'" + dt.ToString("yyyyMMdd HH:mm:ss") + "'";
        }

        /// <summary>
        // Surround with apostrophies after quoting imbedded apostrophies
        /// </summary>
        public static string SQLString(string s)
        {
            if (s == null)
                return "NULL";
            else
                return "'" + s.Replace("'", "''") + "'";
        }

        public static string SQLZeroToNull(double v)
        {
            return (v == 0 ? "NULL" : v.ToString());
        }

        //=====================================================================
        // File system
        //=====================================================================
        public static string  DefaultPathToWritableFolder(string subfolder) 
        {
            // Windows 7 and later has a write-enabled C:\Users\Public folder.
            // Program Files cannot be written into unless we are running as admin.
            if (Directory.Exists(@"C:\Users\Public"))
            {
                if (! Directory.Exists(@"C:\Users\Public\AcuityPlus"))
                    Directory.CreateDirectory(@"C:\Users\Public\AcuityPlus");

                if (! Directory.Exists(@"C:\Users\Public\AcuityPlus\" + subfolder))
                    Directory.CreateDirectory(@"C:\Users\Public\AcuityPlus\" + subfolder);

                return @"C:\Users\Public\AcuityPlus\" + subfolder;
            }
            else
            {
                return Directory.GetCurrentDirectory() + @"\..\" + subfolder;
            }
        }

        public static string DefaultImportPath() 
        {
            return DefaultPathToWritableFolder("load_me");
        }

        public static string DefaultExportPath() 
        {
            return DefaultPathToWritableFolder("export");
        }

        public static string DefaultLogPath() 
        {
            return DefaultPathToWritableFolder("log");
        }

        public static string DefaultArchivePath()
        {
            return DefaultPathToWritableFolder("archive");
        }

        //=====================================================================
        // Conversions
        //=====================================================================

        /// <summary>
        /// Convert YYYYMMDDHHMMSS (ISO format) to DateTime
        /// </summary>
        public static DateTime ISOToDateTime(string s)
        {
            int year = 1900, month = 1, day = 1, hour = 0, minute = 0, second = 0;

            if (s.Length >= 8)
            {
                //Assume we have YYYYMMDD
                year = Convert.ToInt32(s.Substring(0, 4));
                month = Convert.ToInt32(s.Substring(4, 2));
                day = Convert.ToInt32(s.Substring(6, 2));

                if (s.Length >= 10)
                {
                    hour = Convert.ToInt32(s.Substring(8, 2));
                }
                if (s.Length >= 12)
                {
                    minute = Convert.ToInt32(s.Substring(10, 2));
                }
                if (s.Length >= 14)
                {
                    second = Convert.ToInt32(s.Substring(12, 2));
                }
            }
            return new DateTime(year, month, day, hour, minute, second);
        }

        /// <summary>
        /// Convert YYYYMMDD (ISO format) to DateTime (at midnight)
        /// </summary>
        public static DateTime ISODateToDateTime(string s)
        {
            return ISOTimeToDateTime(s);
        }

        /// <summary>
        /// Convert HHMMSS to DateTime (on 1/1/1900)
        /// </summary>
        public static DateTime ISOTimeToDateTime(string s)
        {
            int year = 1900, month = 1, day = 1, hour = 0, minute = 0, second = 0;

            if (s.Length >= 2)
                hour = Convert.ToInt32(s.Substring(0, 2));
            if (s.Length >= 4)
                minute = Convert.ToInt32(s.Substring(2, 2));
            if (s.Length >= 6)
                second = Convert.ToInt32(s.Substring(4, 2));
            return new DateTime(year, month, day, hour, minute, second);
        }

        public static DateTime CombineDateAndTime(DateTime d, DateTime t)
        {
            return new DateTime(d.Year, d.Month, d.Day, t.Hour, t.Minute, t.Second);
        }

        /// <summary>
        /// Convert DateTime to ISO DateTime YYYYMMDDHHMMSS
        /// </summary>
        public static string DateTimeToISODateTime(DateTime d)
        {
            return d.ToString("yyyyMMddHHmmss");
        }
        /// <summary>
        /// Convert DateTime to ISO Date YYYYMMDD
        /// </summary>
        public static string DateTimeToISODate(DateTime d)
        {
            return d.ToString("yyyyMMdd");
        }
        /// <summary>
        /// Convert DateTime to ISO Time HHMMSS
        /// </summary>
        public static string DateTimeToISOTime(DateTime d)
        {
            return d.ToString("HHmmss");
        }


        public static double DateDiffInYears(DateTime? d1, DateTime? d2)
        {
            if ((d1 == null) || (d2 == null)) return -1;
            TimeSpan ts = ((DateTime)d2 - (DateTime)d1);
            return (ts.TotalDays / 365.24219);
        }
        public static double DateDiffInDays(DateTime? d1, DateTime? d2)
        {
            if ((d1 == null) || (d2 == null)) return -1;
            TimeSpan ts = ((DateTime)d2 - (DateTime)d1);
            return ts.TotalDays;
        }
        public static double DateDiffInHours(DateTime? d1, DateTime? d2)
        {
            if ((d1 == null) || (d2 == null)) return -1;
            TimeSpan ts = ((DateTime)d2 - (DateTime)d1);
            return ts.TotalHours;
        }
        public static double DateDiffInMinutes(DateTime? d1, DateTime? d2)
        {
            if ((d1 == null) || (d2 == null)) return -1;
            TimeSpan ts = ((DateTime)d2 - (DateTime)d1);
            return ts.TotalMinutes;
        }

        /// <summary>
        /// Select the smaller of the two datetimes.  Null counts as zero.
        /// </summary>
        public static DateTime MinDateTime(DateTime? d1, DateTime? d2)
        {
            if (d1 == null)
                return d2 ?? DateTime.MinValue;
            if (d2 == null)
                return d1 ?? DateTime.MinValue;
            // Neither is null
            if (((DateTime)d1).CompareTo((DateTime)d2) <= 0)
                return (DateTime)d1;
            else
                return (DateTime)d2;
        }

        /// <summary>
        /// Select the larger of the two datetimes.  Null counts as zero.
        /// </summary>
        public static DateTime MaxDateTime(DateTime? d1, DateTime? d2)
        {
            if (d1 == null)
                return d2 ?? DateTime.MinValue;
            if (d2 == null)
                return d1 ?? DateTime.MinValue;
            // Neither is null
            if (((DateTime)d1).CompareTo((DateTime)d2) >= 0)
                return (DateTime)d1;
            else
                return (DateTime)d2;
        }

        /// <summary>
        /// Is this a date (or is it null)?  Used to help converted VB code
        /// </summary>
        public static bool IsDate(DateTime? dt)
        {
            return (dt != null);
        }

        // ** DateOnly?
        // ** TimeOnly?

        //=====================================================================
        // Database reads
        //=====================================================================
        /// <summary>
        /// Convert database Y/N to bool (NULL == false)
        /// </summary>
        public static bool DBToBool(Object dbval)
        {
            return (DBToString(dbval) == "Y");
        }

        /// <summary>
        /// Returns a DateTime? (nullable DateTime)
        /// </summary>
        public static DateTime? DBToDateTimeOrNull(Object dbval)
        {
            return (dbval as DateTime?);
        }

        /// <summary>
        /// Converts database NULL to min datetime
        /// </summary>
        public static DateTime DBToDateTime(Object dbval)
        {
            return (dbval as DateTime?) ?? DateTime.MinValue;
        }

        /// <summary>
        /// Converts database NULL to zero
        /// </summary>
        public static double DBToDouble(Object dbval)
        {
            //return (dbval as double? ?? 0);
            double result;
            if ((dbval != null) && double.TryParse(dbval.ToString(), out result))
                return result;
            else
                return 0;
        }

        /// <summary>
        /// Converts database NULL to zero
        /// </summary>
        public static int DBToInt(Object dbval)
        {
            int result;
            //return (dbval as int? ?? 0);                  // this fails on short :(
            if ((dbval != null) && int.TryParse(dbval.ToString(), out result))
                return result;
            else
                return 0;
        }

        /// <summary>
        /// Converts database NULL to empty string
        /// </summary>
        public static string DBToString(Object dbval)
        {
            return (dbval as string ?? "");
        }

        //=====================================================================
        // Database write
        //=====================================================================
        public static string BooleanToDB(bool v)
        {
            return (v ? "Y" : "N");
        }

        /// <summary>
        /// Converts empty string to null
        /// </summary>
        public static string StringToDB(string s)
        {
            return (String.IsNullOrEmpty(s) ? null : s);
        }

        public static long? ZeroToNull(long v)
        {
            return (v == 0 ? null : (long?)v);
        }

        //=====================================================================
        // Math
        //=====================================================================
        public static double DivideWithoutError(double num, double dem)
        {
            if (dem != 0)
                return num / dem;
            else
                return 0;
        }

        //=====================================================================
        // Misc
        //=====================================================================
        public static void Sleep(int ms)
        {
            System.Threading.Thread.Sleep(ms);
        }

        private static string StripTrailingDotZero(string s)
        {
            string result = s;
            if (s.EndsWith(".0"))
                result = s.Substring(0, s.Length - 2);
            return result;
        }

        // Format the application version for use in window titles and report footers
        public static string AppVersion()
        {
            string result;
            result = Application.ProductVersion;            // 8.0.0.0  - full version & build
            result = result.Substring(0, 5);                // 8.0.0    - strip the build number
            result = StripTrailingDotZero(result);          // 8.0      - strip the patch number (if zero)
            return result;
        }

        //=====================================================================
        // Strings
        //=====================================================================
        // (see Extensions.cs)

        //=====================================================================
        // Exception handling
        //=====================================================================
        public static void MessageBoxError(System.Exception e)
        {
            MessageBoxError(e, "");
        }

        // Describe an exception
        public static void MessageBoxError(System.Exception e, string more)
        {
            string msg, short_stack_trace="";
            string[] tmp;

            // The full stack trace can get crazy with all the system calls.
            // Look for functions within our program.
            tmp = e.StackTrace.Split('\n');
            foreach (string s in tmp)
            {
                // look for our stuff
                // ** How do we keep up to date with all our namespaces?
                // ** One possibility is to always start the namespace with Pfs.
                if (s.Contains("Pfs") || s.Contains("Reports2"))
                {
                    short_stack_trace += s + "\n";      // add to short trace
                }
            }

            msg = e.Message;
            if (e.InnerException != null)
            {
                //If there is an inner message, the outer is usually worthless
                msg += "  " + e.InnerException.Message;     // detail message
            }
            msg += "\n\nError occurred" + short_stack_trace;

            if (more.Length > 0)
            {
                msg += "\nError while " + more;
            }

            MessageBox.Show(msg,"Unexpected Error");
        }

        //=====================================================================
        // Processes
        //=====================================================================

        /// <summary>
        /// Execute a program and wait for it to finish.
        /// Returns the result code.
        /// </summary>
        /// <returns>int result_code</returns>
        public static int ExecuteAndWait(string path)
        {
            var proc = new Process();

            proc.StartInfo.FileName = path;
            proc.EnableRaisingEvents = true;
            proc.Start();
            proc.WaitForExit();
            
            return proc.ExitCode;
        }
    }
}
