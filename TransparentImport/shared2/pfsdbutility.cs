﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ADODB;
using System.Data.SqlClient;
using Microsoft.Win32;

namespace PfsShared
{
    public static class PFSDBUtility
    {
        const string PFS_REMOTE_DSN = "WinPFS";
        const string PFS_REMOTE_UID = "WinPFS";
        const string PFS_REMOTE_PWD = "pfs4you";

        const string PFS_LOCAL_MDB = "";
        const string PFS_LOCAL_UID = "Admin";
        const string PFS_LOCAL_PWD = "";

        const string GID_SEQUENCE_NAME = "GID";
        const int GID_DEFAULT_BLOCKSIZE = 5;

        public enum DriverTypeEnum
        {
            DRV_ODBC,
            DRV_OLEDB
        }

        //=====================================================================
        // Database Connection
        //=====================================================================

        /// <summary>
        /// NewSqlConnection()
        /// You must then create a SqlCommand and a DataReader. 
        /// </summary>
        /// <returns>Returns an open connection for use with SQL commands.</returns>
        public static System.Data.SqlClient.SqlConnection NewSqlConnection()
        {
            SqlConnection db = new SqlConnection(MakeConnectionString());
            db.Open();
            return db;
        }

        /// <summary>
        /// NewOleDbConnection()
        /// </summary>
        /// <param name="db_full_name">is used for Access connection</param>
        /// <returns>Returns an open connection</returns>
        public static System.Data.OleDb.OleDbConnection NewOleDbConnection(string db_full_name)
        {
            String connectionStr = "";
            if (String.IsNullOrEmpty(db_full_name))
                // For Sql connection
                connectionStr = MakeConnectionString();
            else
                // For access connection
                connectionStr = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + db_full_name;
            
            System.Data.OleDb.OleDbConnection db = new System.Data.OleDb.OleDbConnection(connectionStr);
            db.Open();
            
            return db;
        }

        /// <summary>
        /// New_ADODB_Connection()
        /// </summary>
        /// <param name="db_full_name">is used for Access connection</param>
        /// <returns></returns>
        public static ADODB.Connection New_ADODB_Connection(string db_full_name)
        {
            

             String connectionStr = "";
            if (String.IsNullOrEmpty(db_full_name))
                // For Sql connection
                connectionStr = MakeConnectionString();
            else
                // For access connection
                connectionStr = "Provider=Microsoft.ACE.OLEDB.12.0;" +
                                "Data Source=" + db_full_name +
                                ";Persist Security Info=False;";
            
            ADODB.Connection cn = new Connection();

            cn.Open(connectionStr, null, null, 0);

            return cn;
        }

        private static WinPfsConnectionInfo GetWinPfsConnectionInfo()
        {
            WinPfsConnectionInfo result;
            // Read ODBC definition to get server name and default database.
            // The default database is usually not defined, but can be.
            string odbcRoot = @"HKEY_LOCAL_MACHINE\SOFTWARE\ODBC\ODBC.INI\WinPFS";
            result.server = (string)Registry.GetValue(odbcRoot, "Server", "");
            result.dbname = (string)Registry.GetValue(odbcRoot, "Database", "pfs");
            if (String.IsNullOrEmpty(result.server))
            {
                // The VB setup program only creates a 32-bit ODBC entry.
                // If we are a 64-bit app installed by the 32-bit VB setup we need to
                // look in the Wow6432Node for the WinPFS definition.
                odbcRoot = @"HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\ODBC\ODBC.INI\WinPFS";
                result.server = (string)Registry.GetValue(odbcRoot, "Server", "");
                result.dbname = (string)Registry.GetValue(odbcRoot, "Database", "pfs");
            }
            return result;
        }

        // You can retry some database errors, we should just give up with these
        public static bool IsFatalDatabaseError(string errmsg)
        {
            string msg = errmsg.ToLower();
            return msg.Contains("communication link") ||    // failure
                msg.Contains("network") ||
                msg.Contains("connectionread") ||
                msg.Contains("connectionwrite") ||
                msg.Contains("terminated") ||
                msg.Contains("the transaction log");        // is full
        }

        /// <summary>
        /// Execute a (non-query) command and return the number of rows affected.
        /// This is good for INSERT, UPDATE and DELETE.
        /// </summary>
        /// <returns></returns>
        public static int ExecuteSQL(string sql)
        {
            using (var db = PFSDBUtility.NewSqlConnection())
            {
                var cmd = new SqlCommand(sql, db);
                return cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// SqlExecuteReader : Execute SQLDataReader
        /// </summary>
        /// <param name="conn"> SQLConnection</param>
        /// <param name="sql"> query</param>
        /// <param name="field"> This is the column name would like to retreive</param>
        /// <returns>This is used for returning a single value</returns>
        public static object ExecuteSqlDataReader(SqlConnection conn, string sql, string field)
        {
            object value = null;
            SqlDataReader reader = null;
            try
            {
                using (SqlCommand command = new SqlCommand(sql, conn))
                {
                    reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        if (!reader.IsDBNull(0))
                        {
                            value = reader.GetValue(reader.GetOrdinal(field));                          
                        }
                    }
                }
            }
            catch (Exception e)
            {
                if (PfsShared.PFSDBUtility.IsFatalDatabaseError(e.ToString()))
                    PfsShared.PFSUtility.MessageBoxError(e);
            }
            finally
            {
                reader.Close();
            }

            return value;
        }

        /// <summary>
        /// Returns an open context for use with LINQ commands.
        /// You must then create a LINQ expression.
        /// </summary>
        /// <returns></returns>
        public static PfsDataContext NewPfsDataContext()
        {
            return new PfsDataContext(MakeConnectionString());
        }

        private struct WinPfsConnectionInfo
        {
            public string server, dbname;
        }

        //=====================================================================
        // Database Utilities
        //=====================================================================
        /// <summary>
        /// Return the server's current date and time - note this is a round-trip call
        /// </summary>
        public static DateTime ServerDateTime()
        {
            var result = DateTime.Now;                          // default to workstation time (just in case)
            using (var db = PFSDBUtility.NewSqlConnection())      // auto dispose & close
            {
                var cmd = new SqlCommand("SELECT GETDATE()", db);
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read()) result = DBToDateTime(dr[0]);
                dr.Close();
            }
            return result;
        }

        //=====================================================================
        // Help creating SQL commands (used extensively in report writer)
        //=====================================================================

        public static string SQLBoolean(bool v)
        {
            return SQLString(v ? "Y" : "N");
        }

        //Make a region-independent date for a SQL command.
        public static string SQLDate(DateTime dt)
        {
            return "'" + dt.ToString("yyyyMMdd") + "'";
        }

        public static string AccessDate(DateTime dt)
        {
            return "#" + dt.ToString("MM/dd/yyyy") + "#";
        }


        public static string SQLTime(DateTime dt)
        {
            return "'" + dt.ToString("HH:mm:ss") + "'";
        }

        public static string SQLDateTime(DateTime dt)
        {
            return "'" + dt.ToString("yyyy-MM-dd HH:mm:ss") + "'";
        }

        /// <summary>
        // Surround with apostrophies after quoting imbedded apostrophies
        /// </summary>
        public static string SQLString(string s)
        {
            if (s == null)
                return "NULL";
            else
                return "'" + s.Replace("'", "''") + "'";
        }

        public static string SQLZeroToNull(double v)
        {
            return (v == 0 ? "NULL" : v.ToString());
        }

        //=====================================================================
        // Database reads
        //=====================================================================
        /// <summary>
        /// Convert database Y/N to bool (NULL == false)
        /// </summary>
        public static bool DBToBool(Object dbval)
        {
            return (DBToString(dbval) == "Y");
        }

        /// <summary>
        /// Returns a DateTime? (nullable DateTime)
        /// </summary>
        public static DateTime? DBToDateTimeOrNull(Object dbval)
        {
            return (dbval as DateTime?);
        }

        /// <summary>
        /// Converts database NULL to min datetime
        /// </summary>
        public static DateTime DBToDateTime(Object dbval)
        {
            return (dbval as DateTime?) ?? DateTime.MinValue;
        }

        /// <summary>
        /// Converts database NULL to zero
        /// </summary>
        public static double DBToDouble(Object dbval)
        {
            //return (dbval as double? ?? 0);
            double result;
            if ((dbval != null) && double.TryParse(dbval.ToString(), out result))
                return result;
            else
                return 0;
        }

        /// <summary>
        /// Converts database NULL to zero
        /// </summary>
        public static int DBToInt(Object dbval)
        {
            int result;
            //return (dbval as int? ?? 0);                  // this fails on short :(
            if ((dbval != null) && int.TryParse(dbval.ToString(), out result))
                return result;
            else
                return 0;
        }

        /// <summary>
        /// Converts database NULL to empty string
        /// </summary>
        public static string DBToString(Object dbval)
        {
            return (dbval as string ?? "");
        }

        //=====================================================================
        // Database write
        //=====================================================================
        public static string BooleanToDB(bool v)
        {
            return (v ? "Y" : "N");
        }

        /// <summary>
        /// Converts empty string to null
        /// </summary>
        public static string StringToDB(string s)
        {
            return (String.IsNullOrEmpty(s) ? null : s);
        }

        public static long? ZeroToNull(long v)
        {
            return (v == 0 ? null : (long?)v);
        }

        public static Connection NewRemoteConnection()
        {
            string wDsn = PFS_REMOTE_DSN, wUid = PFS_REMOTE_UID, wPwd = PFS_REMOTE_PWD;
            DriverTypeEnum wDriver = DriverTypeEnum.DRV_ODBC;
            Connection wResult;

            wResult = new Connection();
            string wConnectionString = MakeConnectionString(wDsn, wUid, wPwd, wDriver);
            wResult.ConnectionTimeout = 25;
            wResult.Open(wConnectionString,wUid,wPwd,0);
            wResult.CommandTimeout = 180;
            return wResult;
        }

        private static string MakeConnectionString()
        {
            var info = GetWinPfsConnectionInfo();

            if (String.IsNullOrEmpty(info.server))
                throw new MissingFieldException("Missing WinPFS ODBC entry");
            if (info.dbname == "master")                     // ODBC is wrong - cannot be master
                info.dbname = "pfs";

            return  "Data Source=" + info.server +
                    "; Initial Catalog=" + info.dbname +
                    "; Connection Timeout=20" +                 //up from 15
                    "; User Id=winpfs; Password=pfs4you; Asynchronous Processing=true; MultipleActiveResultSets=True";
            }

        public static string MakeConnectionString(string iDsn, string iUid, string iPwd, DriverTypeEnum iDriver)
        {
            string wResult = "";
            switch (iDriver)
            {
                case (DriverTypeEnum.DRV_OLEDB):
                    wResult = MakeOLEDBConnectionString(iDsn, iUid, iPwd);
                    break;
                case (DriverTypeEnum.DRV_ODBC):
                    wResult = string.Format("DSN={0}; uid={1}; pwd={2}; Persist Security Info=true;", iDsn, iUid, iPwd);
                    break;
            }

            return wResult;
        }

        // TODO: MakeOLEDBConnectionString() Not yet implement
        public static string MakeOLEDBConnectionString(string iDsn, string iUid, string iPwd)
        {
            //string wRegKey = @"SOFTWARE\ODBC\odbc.ini\WinPFS";
            string wResult = ""; //wServer_name, wDefault_db, wSubkey = "Server", wKeyValue;

            return wResult;
        }

        public static string ServerName()
        {
            string serverName = "";
            string odbcRoot = @"HKEY_LOCAL_MACHINE\SOFTWARE\ODBC\ODBC.INI\WinPFS";
            serverName = (string)Registry.GetValue(odbcRoot, "Server", "");

            if (String.IsNullOrEmpty(serverName))
            {
                // The VB setup program only creates a 32-bit ODBC entry.
                // If we are a 64-bit app installed by the 32-bit VB setup we need to
                // look in the Wow6432Node for the WinPFS definition.
                odbcRoot = @"HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\ODBC\ODBC.INI\WinPFS";
                serverName = (string)Registry.GetValue(odbcRoot, "Server", "");
            }
            return serverName;
        }

        public static string DabaseName()
        {
            string dbName = "";
            string odbcRoot = @"HKEY_LOCAL_MACHINE\SOFTWARE\ODBC\ODBC.INI\WinPFS";
            dbName = (string)Registry.GetValue(odbcRoot, "Database", "pfs");
            if (String.IsNullOrEmpty(dbName))
            {
                // The VB setup program only creates a 32-bit ODBC entry.
                // If we are a 64-bit app installed by the 32-bit VB setup we need to
                // look in the Wow6432Node for the WinPFS definition.
                odbcRoot = @"HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\ODBC\ODBC.INI\WinPFS";

                dbName = (string)Registry.GetValue(odbcRoot, "Database", "pfs");
            }
            return dbName;
        }

        /// <summary>
        /// DatabaseVersion(): Get the current database version        /// 
        /// </summary>
        /// <param name="conn"></param>
        /// <returns> -1 if there is no version yet</returns>
        public static double DatabaseVersion(ADODB.Connection conn)
        {
            object recs;
            string sql = "SELECT MAX(DATABASE_REVISION) FROM SYS_REVISION";
            double dbVersion = -1;
             ADODB.Recordset rs = new Recordset();
            try
            {               
                rs = conn.Execute(sql, out recs, 0);
                while (!rs.EOF)
                {
                    if (rs.Fields[0].Value != null)
                        dbVersion = (double)rs.Fields[0].Value;
                }
            }
            catch (Exception e)
            {
                if (IsFatalDatabaseError(e.ToString()))
                    PFSUtility.MessageBoxError(e);
            }
            finally
            {
                rs.Close();
            }
            return dbVersion;

        }

        /// <summary>
        /// DatabaseVersion(): Get the current database version
        /// </summary>
        /// <param name="conn"></param>
        /// <returns></returns>
        public static double DatabaseVersion(SqlConnection conn)
        {
            double dbVersion = -1;
           SqlDataReader reader = null;
           try
           {
               using (SqlCommand command = new SqlCommand("SELECT MAX(DATABASE_REVISION) AS DBVERSION FROM SYS_REVISION", conn))
               {
                   reader = command.ExecuteReader();
                   while (reader.Read())
                   {
                       if (!reader.IsDBNull(0))
                       {
                           object value = reader.GetValue(reader.GetOrdinal("DBVERSION"));
                           dbVersion = Math.Round(Convert.ToDouble(value), 3);
                       }
                   }
               }
           }
           catch (Exception e)
           {
               if (IsFatalDatabaseError(e.ToString()))
                   PFSUtility.MessageBoxError(e);
           }
           finally
           {
               reader.Close();
           }
            return dbVersion;
        }

        public static string FormatDbVersion(double value)
        {
            string result = "";
            result = String.Format("{0:0.000}", value); //Format version as 0.000
            return result;
        }
    }
}
