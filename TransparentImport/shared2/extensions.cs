﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// These are extensions to built-in types.

namespace PfsShared
{
    public static class Extensions
    {
        /// <summary>
        /// Returns the leftmost count characters
        /// </summary>
        /// <param name="str"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static string Left(this string s, int count)
        {
            if (s.Length <= count)
                return s;
            return s.Substring(0, count);
        }

        /// <summary>
        /// Returns the rightmost count characters
        /// </summary>
        /// <param name="s"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static string Right(this string s, int count)
        {
            if (s.Length <= count)
                return s;
            return s.Substring(s.Length - count, count);
        }

        /// <summary>
        /// Look for any element of the search array anywhere in the string being searched
        /// </summary>
        public static bool ContainsAny(this string s, string[] arr)
        {
            foreach (string t in arr) {
                if (s.Contains(t)) return true;
            }
            return false;
        }
        /// <summary>
        /// Returns which word in the search array was found in the string being searched
        /// </summary>
        public static string ContainsWhich(this string s, string[] arr)
        {
            foreach (string t in arr) {
                if (s.Contains(t)) return t;
            }
            return "";
        }

        /// <summary>
        /// Repeat the first character (like VB Space$)
        /// </summary>
        /// <param name="s"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        public static string Repeat(this string s, int count)
        {
            return (!String.IsNullOrEmpty(s) ? new String(s[0], count) : "");
        }

        /// <summary>
        /// Returns a value of the width specified.  The string is truncated or padded as needed.
        /// </summary>
        /// <param name="str"></param>
        /// <param name="width"></param>
        /// <returns></returns>
        public static string FixedWidth(this string s, int width)
        {
            return s.Left(width).PadRight(width, ' ');
        }

        /// <summary>
        /// Truncate string and append ellipsis if length exceeds maxlen
        /// </summary>
        /// <param name="str"></param>
        /// <param name="maxlen"></param>
        /// <returns></returns>
        public static string TruncateWithEllipsis(this string s, int maxlen)
        {
            if (maxlen < 4) 
                return s.Left(maxlen);
            if (s.Length <= maxlen)
                return s;
            return s.Substring(0, maxlen - 3) + "...";
        }

        /// <summary>
        /// String trailing CrLf
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string StripCrLf(this string s)
        {
            return s.TrimEnd('\r', '\n');
        }
        
        
        /// <summary>
        /// Is this a valid number?  (int or float)
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static bool IsNumeric(this string s)
        {
            double val;
            return double.TryParse(s, out val);
        }

        /// <summary>
        /// Convert string to integer; returns zero if invalid; discard floating point part
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static int ToInteger(this string s)
        {
            int result = 0;
            int.TryParse(s, out result);                // this rejects floating point
            if (s.IsNumeric() && result == 0)
                result = Convert.ToInt32(s.ToDouble()); // get int portion
            return result;
        }

        /// <summary>
        /// Convert string to double; returns zero if invalid
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static double ToDouble(this string s)
        {
            double result = 0;
            double.TryParse(s, out result);
            return result;
        }

        /// <summary>
        /// Convert string to double; stops converting when a non-numeric character is encountered.  (from VB)
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static double Val(this string s)
        {
            double result = 0;
            int count = 0;

            foreach (char ch in s)
            {
                if (Char.IsDigit(ch) || (ch == '-') || (ch == '.'))
                    count++;
                else
                    break;
            }
            if (count > 0)
            {
                result = s.Left(count).ToDouble();
            }
            return result;
        }
    }
}
