﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PfsShared;                    // (add Shared2 project reference first)
using System.Data.SqlClient;        // for SQL access
using System.Data.Linq;             // For LINQ (add project reference first)

// Unit tests for PfsShared
//
namespace PfsShared2Tests
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                TestMath();
                TestSQL();
                TestLINQ();
                TestSQLBuilder();
                TestConversions();
                TestExtensions();
                TestException();        // last test
            }
            catch(Exception e)
            {
                PFSUtility.MessageBoxError(e);
            }

            Console.WriteLine("\nPress Any Key...");    // cw<tab><tab>
            Console.ReadKey();
            Console.WriteLine("\nGood bye!");
        }

        static void TestDivideWithoutError(double a, double b)
        {
            Console.WriteLine("{0,-3} / {1,-3} = {2}", a, b, PFSUtility.DivideWithoutError(a,b));
        }

        static void TestMath()
        {
            Console.WriteLine("Test divide without error");
            TestDivideWithoutError(355, 113);
            TestDivideWithoutError(2.5, .5);
            TestDivideWithoutError(0, 4);
            TestDivideWithoutError(4, 0);
        }

        private static void TestSQL()
        {
            using (var db = PFSDBUtility.NewSqlConnection())      // "using" will close the connection
            {
                string sql = "select unit_id, name, parent_unit_id from unit where unit_id < 0";

                Console.WriteLine("\nSpecial units (SQL)");
                var cmd = new SqlCommand(sql, db);
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Console.WriteLine("{0} = {1,-10} \tparent={2}", dr[0], dr["NAME"], dr[2]);
                }
                dr.Close();
            }
        }

        private static void TestLINQ()
        {
            var pfs = PFSDBUtility.NewPfsDataContext();
            var units = from unit in pfs.UNITs
                        where (unit.UNIT_ID < 0)
                        select new
                        {
                            unit.UNIT_ID,
                            unit.NAME,
                            unit.PARENT_UNIT_ID,
                            unit.PFS_CLINICAL_SPECIALTY_LVC
                        };
            Console.WriteLine("\nSpecial units (LINQ)");
            foreach (var unit in units)
            {
                Console.WriteLine("{0} = {1,-10} \tparent={2} specialty={3}", unit.UNIT_ID, unit.NAME, unit.PARENT_UNIT_ID,
                        unit.PFS_CLINICAL_SPECIALTY_LVC);
            }
        }

        private static void TestSQLBuilder()
        {
            DateTime dt = DateTime.Now;
            Console.WriteLine("\nSQL String Builder functions");
            Console.WriteLine("SQLDate     is {0}", PFSDBUtility.SQLDate(dt));
            Console.WriteLine("SQLTime     is {0}", PFSDBUtility.SQLTime(dt));
            Console.WriteLine("SQLDateTime is {0}", PFSDBUtility.SQLDateTime(dt));
            Console.WriteLine("SQLString   is {0}", PFSDBUtility.SQLString("This is a test"));
            Console.WriteLine("SQLString   is {0}", PFSDBUtility.SQLString("Don't do it!  Type='a'"));
        }

        private static void TestConversions()
        {
            Console.WriteLine("\nTest conversions");
            ISOTest("2010");                //fail
            ISOTest("20101230");            //minimum
            ISOTest("201012302245");
            ISOTest("20101230224530");
        }

        private static void ISOTest(string s)
        {
            Console.WriteLine("ISOToDateTime({0,-14}) is {1}", s, PFSUtility.ISOToDateTime(s));
        }

        private static void CompareValues<T>(T result, T expected, string test_name)
        {
            if (result.Equals(expected))
                Console.WriteLine("Test passed: {0}", test_name);
            else
                Console.WriteLine("** Test failed: {0}.  Got '{1}', expected '{2}'", test_name, result, expected);
        }

        private static void TestExtensions()
        {
            string a = "Hello Test!", b = "TrailingLF\r\n\r\n";
            string istr="123", fstr="123.456";

            CompareValues("no", "match", "this should fail");
            CompareValues(a.Left(5), "Hello", "Left(5)");
            CompareValues(a.Left(100), a, "Left(100)");            // 100 > len
            CompareValues(a.Right(5), "Test!", "Right(5)");
            CompareValues(a.Right(100), a, "Right(100)");          // 100 > len
            CompareValues(a.TruncateWithEllipsis(8), "Hello...", "Truncate(8)");
            CompareValues(a.TruncateWithEllipsis(2), "He", "Truncate(2)");
            CompareValues(b.StripCrLf(), "TrailingLF", "StripCrLf");

            CompareValues(a.IsNumeric(), false, "a is not numeric");
            CompareValues(istr.IsNumeric(), true, "istr is numeric");
            CompareValues(fstr.IsNumeric(), true, "fstr is numeric");
            CompareValues("3a".IsNumeric(), false, "3a is not numeric");
            
            CompareValues(istr.ToInteger(), 123, "ToInteger(123)");
            CompareValues(fstr.ToInteger(), 123, "ToInteger(123.456)");
            CompareValues(a.ToInteger(), 0, "ToInteger() error");

            CompareValues(istr.ToDouble(), 123, "ToDouble(123)");
            CompareValues(fstr.ToDouble(), 123.456, "ToDouble(123.456)");
            CompareValues(a.ToDouble(), 0.0, "ToDouble() error");
        }

        private static void TestException()
        {
            try
            {
                Console.WriteLine("\nCatch error and show small stack trace...");
                // make the stack trace deeper:
                DownAnotherLevel("abc", null);
            }
            catch (Exception e)
            {
                // This will print a simplified stack track trace
                PFSUtility.MessageBoxError(e);
            }
        }

        private static void DownAnotherLevel(string s, string t)
        {
            int a = 4;
            int b = 0;

            Console.WriteLine("s={0} t={1}", s,t);  // OK
            s = t;                                  // OK
            s = t.ToLower();                        // Error!  object not set

            Console.WriteLine("a/b={0}",a/b);       // divide by zero
        }

    }
}
