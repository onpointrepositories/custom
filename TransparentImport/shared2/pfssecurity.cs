﻿using System;
using System.Linq;                              // for array.Count()
using Utility = PfsShared.PFSUtility;
using DBUtility = PfsShared.PFSDBUtility;
using Microsoft.Win32;
//
// PFSSecurity: User Validation and password encryption
//
namespace PfsShared
{
    static public class PFSSecurity
    {
        const string QUOTE = "\"";                  // this can be hard to mix with @"" strings.  Do it here.

        private static string                       _lastServerInfo = "~";      // last info string
        private static PfsShared.PfsConnectionInfo  _lastConnectionInfo;        // last decrypted results

        /// <summary>
        /// This will always fetch from the 64-bit registry even if you are running as a 32-bit process
        /// </summary>
        /// <param name="server"></param>
        /// <returns></returns>
        public static PfsShared.PfsConnectionInfo GetServerLoginInfo(string server)
        {
            // Look to see if there is an entry for this server
            string key = @"HKEY_LOCAL_MACHINE\SOFTWARE\Quadramed\AcuityPlus";
            string sInfo = PFSUtility.GetRegKey64(key, server, "");             // get login info string for server

            if (sInfo == _lastServerInfo)                                       // same info string?
            {
                return new PfsShared.PfsConnectionInfo(_lastConnectionInfo);    // same as last time
            }

            var result = new PfsShared.PfsConnectionInfo();
            bool found_user = false, found_password = false, found_dbname = false;

            string[] args = sInfo.Split(';');       // user=uuuu; password=pppp; database=dddd; <reserved>=<value>

            foreach (var arg in args)
            {
                // WARNING: don't split on '=' because the password is padded with '='s
                int n = arg.IndexOf('=');
                if (n > 0  &&  arg.Length >= 3)
                {
                    string tag = arg.Left(n).ToLower().Trim();
                    string value = arg.Substring(n+1).Trim();

                    switch (tag)
                    {
                        case "user":
                            result.user = value;
                            found_user = true;
                            break;
                        case "password":
                            try
                            {
                                result.password = DecryptString(value);
                            }
                            catch (Exception)
                            {
                                result.password = "(invalid)";
                            }
                            found_password = true;
                            break;
                        case "database":                                    // (not used now but could be)
                            result.dbname = value;
                            found_dbname = true;
                            break;
                        default:
                            // Ignore extra things (future additions)
                            break;
                    }
                }

            }

            if (!found_user) result.user = "winpfs";
            if (!found_password) result.password = "pfs4you";
            if (!found_dbname) result.dbname = "pfs";

            _lastConnectionInfo = result.Clone();     // save for next time

            return result;
        }

        /// <summary>
        /// This is what we want to save in the registry for each server
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public static string CreateServerRegistryString(PfsConnectionInfo info)
        {
            return "user=" + info.user + "; password=" + EncryptString(info.password);
        }

        /// <summary>
        /// This command can be put in a .BAT file
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public static string RegistryUpdateBatchFileContent(PfsConnectionInfo info)
        {
            // The /d data needs to be quoted on the command line.
            return
                @"reg.exe add HKLM\Software\Quadramed\AcuityPlus /v " + info.server + @" /d " + QUOTE + CreateServerRegistryString(info) + QUOTE + @" /f" + "\n" +
                "@if %ERRORLEVEL% EQU 0 goto done\n" +
                "@echo Did you run this as administrator?\n" +
                ":done\n" +
                "pause\n";
        }

        /// <summary>
        /// This content can be put into a .REG file
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public static string RegistryUpdateRegFileContent(PfsConnectionInfo info)
        {
            return
                "Windows Registry Editor Version 5.00\n\n" +
                @"[HKEY_LOCAL_MACHINE\Software\Quadramed\AcuityPlus]" + "\n" +
                QUOTE + info.server + QUOTE + " = " + QUOTE + CreateServerRegistryString(info) + QUOTE + "\n";
        }

        /// <summary>
        /// Package this connection info and add to the local registry
        /// </summary>
        /// <param name="info"></param>
        public static void UpdateLocalRegistry(PfsConnectionInfo info)
        {
            // Having trouble with this...
            // Make sure this goes in the 64-bit section even if we are running as x86 on 64-bit hardware
            //string key = @"HKEY_LOCAL_MACHINE\SOFTWARE\Quadramed\AcuityPlus";
            //PFSUtility.SetRegKey64(key, info.server, CreateServerLoginInfoString(info));

            // This will work correctly as long as the process is targed for "any cpu"
            RegistryKey AcuityPlus = Registry.LocalMachine.OpenSubKey(@"Software\Quadramed\AcuityPlus", true);
            AcuityPlus.SetValue(info.server, CreateServerRegistryString(info));
        }

        //=========================================================================================
        // These need to stand alone without any external help.
        // These are secret keys.  We don't have to be too tricky about hiding them because anyone
        // with a debugger can breakpoint to the call to decrypt and see the actual key no matter
        // how it was hidden.
        //
        // Encrypted strings are converted to base-64 encoding.
        // The base-64 digits in ascending order from zero are:
        //  1) uppercase characters "A" to "Z"
        //  2) lowercase characters "a" to "z"
        //  3) numerals "0" to "9"
        //  4) symbols "+" and "/"
        // The valueless character, "=", is used for trailing padding.
        //=========================================================================================
        // These (strong) keys were generated at random; DO NOT CHANGE
        private static byte[] _cryptKey = { 48, 165, 247, 254, 65, 39, 65, 143, 55, 125, 159, 113, 210, 212, 117, 243, 161, 150, 157, 180, 31, 10, 184, 197, 89, 224, 172, 8, 130, 201, 242, 49 };
        private static byte[] _authKey = { 168, 238, 239, 66, 140, 47, 218, 97, 9, 39, 182, 129, 235, 20, 77, 214, 223, 29, 234, 112, 90, 196, 90, 117, 213, 197, 90, 52, 243, 160, 8, 194 };

        public static string EncryptString(string value)
        {
            return PFSCryptography.AESThenHMAC.EncryptStringWithKeys(value, _cryptKey, _authKey);
        }

        public static string DecryptString(string value)
        {
            return PFSCryptography.AESThenHMAC.DecryptStringWithKeys(value,_cryptKey,_authKey);
        }
    }
}
