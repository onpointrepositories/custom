Attribute VB_Name = "MainModule"
Option Explicit
'
' Copyright (c) 2009 Quadramed Corporation. All rights reserved.
'
' This software and documentation is the confidential and proprietary
' information of Quadramed Corporation and may be used only in accordance
' with the terms of QuadraMed�s license agreement.
' Change: updated to new location

'
' This is the main module for AcuityPlus.exe (formerly WinPFS.exe)
'
Public g_cnADO      As ADODB.Connection             'main DB connection
Public g_util       As New PFSUtility               'utility classes
Public g_dbutil     As New PFSDBUtility
Public g_command    As String                       'command line

Const MAX_INDS = 111  'including custom
Const MAX_INDICATORS = 22
Const MAX_TYPES = 6
Const MAX_PROC = 10
Const MAX_OCI = 32
Const TRANSP_INLOG_FILE_LIFE = 7 'days
Const TRANSP_OUTLOG_FILE_LIFE = 7 'days
Const DBTABLENAME = "TC_DATA"

Private Type procdata
    checked As Boolean
    stime As String
    etime As String
    void As Boolean
End Type

Private Type cerindicator
  ind As Integer  'winpfs indic number associated with this cerner event
  lab As Long    'label seq
  leg As String   'legend code
  count As Integer  'unique count by performed time
  persist As Boolean
  validhrs As Integer 'number of hours since charting that this is valid
End Type

Private Type indicator_data
    checked As Boolean
End Type

Private Type division
    stime As Date
    etime As Date
    exists As Boolean
End Type

Private Type ind_weight
    pweight As Integer
    cweight As Integer
End Type

Private Type unitinfo
    name As String
    ptype As Integer
    ctype As Integer
    indstr As String
    defindstr As String
End Type

'Dim dicary() As cerindicator
Dim diclab() As cerindicator  'dictionary ary for key=label seq
Dim dicleg() As cerindicator  'dictionary ary for key=legend codes
Dim dicll() As cerindicator 'dictionary ary for key=lab seq+leg code
Dim unitary() As unitinfo

'Dim dicnum As Integer
Dim labnum As Integer
Dim legnum As Integer
Dim llnum As Integer
Dim unitnum As Integer

Dim nowdt As Date

Dim winpfspath As String 'path of winpfs
Dim logpath As String 'path of winpfs\log
Dim loadpath As String 'path of winpfs\load_me

Dim datafile As Integer
Dim outfile As Integer

Dim tcfile As Integer
Dim tcfile1 As Integer
Dim tcfile2 As Integer
Dim inlogfile As Integer
Dim outlogfile As Integer
Dim ocfile As Integer
Dim dbugfile As Integer
Dim dbugon As Boolean

Dim acct As String
Dim unit As String
Dim lname As String
Dim fname As String
Dim room As String
Dim intime As String 'classdt and in d/t
Dim classtm As String

Dim inds(MAX_INDS) As indicator_data
Dim proc(MAX_PROC) As procdata
Dim oc(MAX_OCI) As procdata
Dim grps(MAX_INDICATORS) As Integer

Dim lookback_on As Boolean
Dim lookback_min As Integer
Dim lookback_dt As Date
Dim txarea As String
Dim N3Gbucket As Integer
Dim nonN3Gbucket As Integer

Dim ind_weights(0 To MAX_INDICATORS) As ind_weight
Dim pat_types(0 To MAX_TYPES) As Long
Dim cmp_types(0 To MAX_TYPES) As Long

Dim experimentison As Boolean
Dim experimenttm As String 'the time of the experimental classification
Dim resumetm As String 'the time of the resuming classification
Dim expi As Integer
Dim specacct As String
Dim gtime0, gtime1, gtime2, gtime3, gtime4, gtime5, gtime6, gtime7, gtime8, gtime9, gtime10 As Integer

Sub Main()
    Dim rsacct As New Recordset
    Dim i, count As Integer
    Dim sql As String
    
    Set g_cnADO = g_dbutil.NewRemoteConnection
    
    SetInitialVars
    gtime0 = gtime1 = gtime2 = gtime3 = gtime4 = gtime5 = gtime6 = gtime7 = gtime8 = gtime9 = gtime10 = 0
    
    AddPersistentDataToDB
    ExtendSheathTimes  ' change sheaths to performdate+12hrs
    
    sql = "SELECT DISTINCT(PATIENT_ID),DEPT_ID,LAST_NAME,FIRST_NAME,ROOM_ID FROM " & DBTABLENAME
    If lookback_on Then
        lookback_dt = DateAdd("n", -lookback_min, _
                DateSerial(CInt(Mid$(intime, 1, 4)), CInt(Mid$(intime, 5, 2)), CInt(Mid$(intime, 7, 2))) + _
                TimeSerial(CInt(Mid$(intime, 9, 2)), CInt(Mid$(intime, 11, 2)), 0))
        sql = sql & " where convert(datetime,performdate)>=" & g_dbutil.SQL_DateTime(lookback_dt)
        If specacct <> "" Then
            sql = sql & " and patient_id " & specacct
        End If
        dprint sql
    Else
        If specacct <> "" Then
            sql = sql & " where patient_id=" & specacct
        End If
    End If
    rsacct.Open sql, g_cnADO
    count = rsacct.RecordCount
    dprint "Total patient count=" & count
    rsacct.MoveFirst
    For i = 1 To count
        If ValidUnit(rsacct(1)) Then
            InitIndicators
            acct = rsacct(0)
            unit = rsacct(1)
            lname = rsacct(2)
            fname = rsacct(3)
            If IsNull(rsacct(4)) Then
                room = ""
            Else
                room = rsacct(4)
            End If
            dprint "Processing data for: " & acct & " - " & lname & ", " & fname & "  " & unit & "/" & room
            If Not (IsNull(acct) Or IsNull(unit)) Then
                Process
                SetTxArea
                OutputIndicators
                OutputProcs
            End If
        End If
        
        If Not rsacct.EOF Then rsacct.MoveNext
    Next i
    rsacct.Close
    Set rsacct = Nothing
    
    SaveLatestPersistentDataToFile
    
'    RestoreLegendCodes
    dprint "Process time gtime0=" & gtime0
    dprint "Process time gtime1=" & gtime1
    dprint "Process time gtime2=" & gtime2
    dprint "Process time gtime3=" & gtime3
    dprint "Process time gtime4=" & gtime4
    dprint "Process time gtime5=" & gtime5
    dprint "Process time gtime6=" & gtime6
    dprint "Process time gtime7=" & gtime7
    dprint "Process time gtime8=" & gtime8
    dprint "Process time gtime9=" & gtime9
    dprint "Process time gtime10=" & gtime10
    dprint "Total time =" & gtime0 + gtime1 + gtime2 + gtime3 + gtime4 + gtime5 + gtime6 + gtime7 + gtime8 + gtime9 + gtime10
    CloseLogFiles
    DeleteOldLogs

End Sub

Private Sub OutputIndicators()
    Dim i As Integer
    Dim outstr As String
    Dim ptype As Integer
    Dim ctype As Integer
    Dim dstr As String
    Dim experimentstring As String
    Dim vs As String
    Dim indsel As String
    Dim ceid As Long
    Dim cept As Integer
    Dim cect As Integer
    Dim cestr As String
    Dim octime As String
    
'10 unit
'44=tx
'70=acct
'91 fullname
'204=classdt
'296=intime
'379=inds

    dstr = ""
    
    outstr = Space$(9) & unit '10 unit
    outstr = outstr & Space$(43 - Len(outstr))
    outstr = outstr & txarea '44 tx area
    outstr = outstr & Space$(69 - Len(outstr))
    outstr = outstr & acct '70 acct
    outstr = outstr & Space$(90 - Len(outstr))
    outstr = outstr & lname & " " & fname '91 last first
    outstr = outstr & Space$(203 - Len(outstr))
    outstr = outstr & intime '204 classdt  yyyymmddhhmm
    outstr = outstr & Space$(295 - Len(outstr))
    outstr = outstr & intime '296 classdt
    outstr = outstr & Space$(378 - Len(outstr))
    If Not (inds(1).checked Or inds(2).checked Or inds(3).checked) Then
        inds(2).checked = True
    End If
    CheckRehab
    CheckMutuality
    indsel = "("
    For i = 1 To MAX_INDS   '379
        If (inds(i).checked) Then
            outstr = outstr & "Y"
            indsel = indsel & i & ","
        Else
            outstr = outstr & "N"
        End If
    Next i
    indsel = indsel & ")"
    
    experimentstring = ""     ' if null then dont output to experiment or resume files, either
    
    '8/19/10 call with Kelly:
    '1.  check typing of each unit ahead of time, in order to know the min typing for each unit.
    '  Start doing 1 now to see if this saves time
    '2.  check only 8 hours back (protection of type reduced to 8 hours) and give min q4(14)
    '  for possible Future
    
    CheckScore ptype, ctype
    i = GetUnitIndex(unit)
    If unitary(i).ptype <= ptype Then 'dont need to get default
        experimentstring = outstr
        Print #tcfile, outstr 'Print line to TRANSPARENT.TXT
    ElseIf LatestClassIsDefaultInPast8Hours(ceid) Then  'need to check for minimum
        dprint "CEID=" & Str(ceid)
        'CheckScore ptype, ctype
        'now use the ceid to get the default class
            'allow default to continue or use default for 7am
        If classtm = "0700" Then 'need to put in the default indicators as a TC. only do this for the 7am.
            experimentstring = Mid$(outstr, 1, 378) & unitary(i).defindstr
            Print #tcfile, Mid$(outstr, 1, 378) & unitary(i).defindstr 'Print line to TRANSPARENT.TXT = default class
            dprint "CREATING A 7AM DEFAULT CLASS"
        End If
        dprint "NOTE: THIS PATIENT HAD A DEFAULT WITHIN PAST 16 HOURS AND DID NOT MEET MINIMAL TYPING LEVELS; ALLOWING DEFAULT TO CONTINUE"
        dprint "WOULDVE BEEN: " & indsel
    Else
        'allow tc class to go through -- patient gets what he gets
        experimentstring = outstr
        Print #tcfile, outstr 'Print line to TRANSPARENT.TXT
    End If
    
'    If (ptype = 1 And ctype = 1) Or (ptype <= 4 And ctype <= 4 And IsICU) Or (ptype <= 3 And ctype <= 3 And IsN4G) Then
'        If LatestClassIsDefault(dstr) Then  ' we know that this TC does not meet min; default is dstr
'            dprint "NOTE: THIS PATIENT DID NOT MEET MINIMAL TYPING LEVELS; ALLOWING DEFAULT TO CONTINUE"
'            dprint "WOULDVE BEEN: " & indsel
'            If classtm = "0700" Then 'need to put in the default indicators as a TC. only do this for the 7am.
'                'set string of default admission indicators for this unit
'                'replace the outstr indicators with these default indicators
'                experimentstring = Mid$(outstr, 1, 378) & dstr
'                Print #tcfile, Mid$(outstr, 1, 378) & dstr 'Print line to TRANSPARENT.TXT = default class
'            End If
'        Else
'            experimentstring = outstr
'            Print #tcfile, outstr 'Print line to TRANSPARENT.TXT
'        End If
'    Else
'        experimentstring = outstr
'        Print #tcfile, outstr 'Print line to TRANSPARENT.TXT
'    End If
    
    If experimentison And experimentstring <> "" Then 'repeat output for the 0702,1502,2302,0302 files
        vs = experimentstring
        Mid$(vs, 212, 4) = experimenttm 'replace time with 01 time
        Mid$(vs, 304, 4) = experimenttm
        If expi >= 14 And expi <= 17 Then
            Mid$(vs, 392, 4) = "NNNN"
            Mid$(vs, 378 + expi, 1) = "Y"  'put in the experiment indicator
        End If
        Print #tcfile1, vs 'contains experimental indicators: to file 0701TRANSPARENT.TXT
        
        vs = experimentstring
        Mid$(vs, 212, 4) = resumetm 'replace time with resume time
        Mid$(vs, 304, 4) = resumetm
        Print #tcfile2, vs 'Print copy of regular TC to resumetm file 0702TRANSPARENT.TXT
    End If 'experimentison
    
    outstr = "Acct#=" & acct & " Unit=" & unit & "  " & lname & " " & fname & " ,"
    For i = 1 To MAX_INDS
        If inds(i).checked Then
            outstr = outstr & "Y"
        Else
            outstr = outstr & "N"
        End If
        outstr = outstr & ","
    Next i
    
    Print #outlogfile, outstr  'Print different line to TranspOut_mmdd.log file
    
    'Output Outcomes data -- there may be several outcomes per patient, each on its own record
    For i = 1 To MAX_OCI   ' column 95
        If (oc(i).checked) Then
        If ProcTimesOK(oc(i).stime) Then
            If IsDate(oc(i).stime) Then
                octime = StrTimeToOutStr(oc(i).stime)
            Else
                octime = intime
            End If
            outstr = Space$(9) & unit '10 unit
            outstr = outstr & Space$(60 - Len(outstr))
            outstr = outstr & octime '61
            outstr = outstr & Space$(73 - Len(outstr))
            outstr = outstr & acct '74 acct
            outstr = outstr & Space$(94 - Len(outstr))
            outstr = outstr & i
            Print #ocfile, outstr 'Print line to outcomesindicator.TXT
        End If
        End If
    Next i

End Sub
Private Sub CheckRehab()
    If inds(4).checked Then
        dprint "Rehab triggers ADL=partial minimally"
        inds(2).checked = True
    End If
End Sub

Private Sub OutputProcs()
    Dim i, j, p As Integer 'p is the number of unique start/end times
    Dim outstr As String
    Dim proctime As String
    Dim procouttime As String
    Dim bucketfound As Boolean
    Dim pbucket(MAX_PROC) As procdata 'unique procedure start and ends
    Dim pproc(MAX_PROC) As Integer 'pproc(procedure) is the pbucket index.
    
    dprint "enter OutputProcs"
    p = 0
    For i = 1 To MAX_PROC
        pproc(i) = 0
        If (proc(i).checked) Then
        If ProcTimesOK(proc(i).stime) Then
            proctime = ""
            procouttime = ""
            If IsDate(proc(i).stime) Then proctime = StrTimeToOutStr(proc(i).stime)
            If IsDate(proc(i).etime) Then procouttime = StrTimeToOutStr(proc(i).etime)
            If procouttime <= proctime Then procouttime = ""
            p = p + 1 'incr number of proc times
            If p = 1 Then
                pproc(i) = 1 'procedure i has the 1st set of start/end times
                pbucket(1).stime = proctime
                pbucket(1).etime = procouttime
                pbucket(1).void = False
            ElseIf p > 1 Then
                bucketfound = False
                For j = 1 To p - 1
                    If proctime = pbucket(j).stime And procouttime = pbucket(j).etime Then
                        bucketfound = True
                        pproc(i) = j 'procedure i has j start/end times
                        pbucket(p).void = True
                        Exit For
                    End If
                Next j
                If Not bucketfound Then
                    pproc(i) = p
                    pbucket(p).stime = proctime
                    pbucket(p).etime = procouttime
                    pbucket(p).void = False
                End If
            End If
        Else
            dprint "Procedure time out of range for patient: " & proc(i).stime
        End If 'proctimesok
        End If
    Next i
    
    For i = 1 To p  'loop the set of start/end times
        If Not pbucket(i).void Then
        outstr = ""
        outstr = Space$(9) & unit '10 unit
        outstr = outstr & Space$(43 - Len(outstr))
        outstr = outstr & "" '44 tx area
        outstr = outstr & Space$(69 - Len(outstr))
        outstr = outstr & acct '70 acct
        outstr = outstr & Space$(90 - Len(outstr))
        outstr = outstr & lname & " " & fname '91 last first
        outstr = outstr & Space$(203 - Len(outstr))
        outstr = outstr & pbucket(i).stime '204 procdt
        outstr = outstr & Space$(255 - Len(outstr))
        outstr = outstr & "p"    '256 procedure type record
        outstr = outstr & Space$(295 - Len(outstr))
        outstr = outstr & pbucket(i).stime '296 procdt in
        outstr = outstr & Space$(347 - Len(outstr))
        outstr = outstr & pbucket(i).etime '348 procdt out
        outstr = outstr & Space$(378 - Len(outstr))
        For j = 1 To MAX_PROC   '379  Loop the procs to see if they belong to this set of start/end times
            If i = pproc(j) Then
                outstr = outstr & "Y"
            Else
                outstr = outstr & "N"
            End If
        Next j

        Print #tcfile, outstr 'Print line to TRANSPARENT.TXT

        outstr = "Procedure: Acct#=" & acct & " Unit=" & unit & "  " & lname & " " & fname & " ,"
        For j = 1 To MAX_PROC
            If i = pproc(j) Then
                outstr = outstr & "Y"
            Else
                outstr = outstr & "N"
            End If
            outstr = outstr & ","
        Next j

        Print #outlogfile, outstr  'Print different line to TranspOut_mmdd.log file
        End If 'not void
    Next i
    
    Erase pproc
    Erase pbucket
'    For i = 1 To MAX_PROC
'        If (proc(i).checked) Then
'        If ProcTimesOK(proc(i).stime) Then
'            proctime = ""
'            procouttime = ""
'            If IsDate(proc(i).stime) Then proctime = StrTimeToOutStr(proc(i).stime)
'            If IsDate(proc(i).etime) Then procouttime = StrTimeToOutStr(proc(i).etime)
'            If procouttime <= proctime Then procouttime = ""
'
'            outstr = ""
'            outstr = Space$(9) & unit '10 unit
'            outstr = outstr & Space$(43 - Len(outstr))
'            outstr = outstr & "" '44 tx area
'            outstr = outstr & Space$(69 - Len(outstr))
'            outstr = outstr & acct '70 acct
'            outstr = outstr & Space$(90 - Len(outstr))
'            outstr = outstr & lname & " " & fname '91 last first
'            outstr = outstr & Space$(203 - Len(outstr))
'            outstr = outstr & proctime '204 procdt
'            outstr = outstr & Space$(255 - Len(outstr))
'            outstr = outstr & "p"    '256 procedure type record
'            outstr = outstr & Space$(295 - Len(outstr))
'            outstr = outstr & proctime '296 procdt in
'            outstr = outstr & Space$(347 - Len(outstr))
'            outstr = outstr & procouttime '348 procdt out
'            outstr = outstr & Space$(378 - Len(outstr))
'            For j = 1 To MAX_PROC   '379
'                If i = j Then
'                    outstr = outstr & "Y"
'                Else
'                    outstr = outstr & "N"
'                End If
'            Next j
'
'            Print #tcfile, outstr 'Print line to TRANSPARENT.TXT
'
'            outstr = "Procedure: Acct#=" & acct & " Unit=" & unit & "  " & lname & " " & fname & " ,"
'            For j = 1 To MAX_PROC
'                If i = j Then
'                    outstr = outstr & "Y"
'                Else
'                    outstr = outstr & "N"
'                End If
'                outstr = outstr & ","
'            Next j
'
'            Print #outlogfile, outstr  'Print different line to TranspOut_mmdd.log file
'        Else
'            dprint "Procedure time out of range for patient: " & proc(i).stime
'        End If 'proctimesok
'        End If 'checked
'    Next i
    dprint "exit OutputProcs"

End Sub
Private Sub SetInitialVars()
    Const DBUG_ON = "-debug"
    Const CLASS_TIME = "-classtime="   '4-digit time only, to be combined with (today's date-past_days)
                                       '  pos 204=classdt and 296=indt in the import file
    Const DATE_OFS = "-dateofs="  ' date offset, absent=today, -1=yesterday's date, 1=tomorrow's date, etc.
    Const LOOKBACK = "-lookback="
    Const N3GDIV = "-n3gbucket="
    Const NONN3GDIV = "-bucket="
    Const EXPERIMENT_ON = "-experimenton" 'this 1 minute after the classtm
    Const SPEC_ACCT = "-acct="
    Dim p As Integer
    Dim cmdLine As String
    Dim ofs As Integer
    Dim yyyy As String
    Dim mm As String
    Dim dd As String

    nowdt = Now
    
    If LoadDictionary Then
    End If
    
    cmdLine = LCase(Command$)
    p = InStr(cmdLine, DBUG_ON)
    dbugon = (p > 0)
    experimentison = False
    
    lookback_on = False
    p = InStr(cmdLine, LOOKBACK)
    If p > 0 Then
        lookback_on = True
        lookback_min = CInt(Mid$(cmdLine, InStr(cmdLine, LOOKBACK) + Len(LOOKBACK), 4))
    End If
    'negatc -debug -classtime=0700 -pastdays=0
    
    p = InStr(cmdLine, N3GDIV)
    If p > 0 Then
        N3Gbucket = CInt(Mid$(cmdLine, InStr(cmdLine, N3GDIV) + Len(N3GDIV), 2))
    Else
        N3Gbucket = 5
    End If
    
    p = InStr(cmdLine, NONN3GDIV)
    If p > 0 Then
        nonN3Gbucket = CInt(Mid$(cmdLine, InStr(cmdLine, NONN3GDIV) + Len(NONN3GDIV), 2))
    Else
        nonN3Gbucket = 5
    End If
    
    p = InStr(cmdLine, CLASS_TIME)
    If p = 0 Then
        classtm = "0700"
    Else
        classtm = Mid$(cmdLine, InStr(cmdLine, CLASS_TIME) + Len(CLASS_TIME), 4)
    End If
    
    p = InStr(cmdLine, EXPERIMENT_ON)
    If p > 0 Then
        experimentison = True
        experimenttm = Mid$(classtm, 1, 3) & "1" '0701, etc
        resumetm = Mid$(classtm, 1, 3) & "2"     '0702, 1502, etc
    End If
    
    p = InStr(cmdLine, SPEC_ACCT)
    If p > 0 Then
        specacct = Trim$(Mid$(cmdLine, p + Len(SPEC_ACCT), 80))  ' in ('a1','a2')  or not in ('a1','a2')
    Else
        specacct = ""
    End If
    
    p = InStr(cmdLine, DATE_OFS)
    If p = 0 Then
        ofs = 0 'today
    Else
        ofs = CInt(Mid$(cmdLine, InStr(cmdLine, DATE_OFS) + Len(DATE_OFS), 2)) 'number of days offset from now.
    End If

    yyyy = Trim$(Str(Year(g_util.DateOnly(nowdt + ofs))))
    mm = Trim$(Str(Month(g_util.DateOnly(nowdt + ofs))))
    If Month(g_util.DateOnly(nowdt + ofs)) < 10 Then
        mm = "0" & mm
    End If
    dd = Trim$(Str(Day(g_util.DateOnly(nowdt + ofs))))
    If Day(g_util.DateOnly(nowdt + ofs)) < 10 Then
        dd = "0" & dd
    End If
    
    intime = yyyy & mm & dd & classtm
    
    InitGroups
    
    MakeLogFiles
    
    InitTyping
    UnitDefaultTyping

End Sub

Private Sub Process()
    Dim ptime1, ptime2 As Date
    
    LogDataIntoTranspIn
    
    'IMPORTANT: The Legend code dictionary and the LabelLegend dictionary must not share common
    '           legend codes.  This is because the legend codes will be deleted from RESULT.
    '     wait, why do i have to delete them from RESULT??  Because there may be legend codes that
    '     have identical beginning characters.  For example:  RN, RNNON.  If we did not remove RNNONs first,
    '     then they would be found as RN.  It's too expensive to search for XnnnnRnnonYmmmm where the search
    '     is sensitive to the following capital letter.  A better solution would be to relocate the processed
    '     legendcode into another column on the same row.
    ptime1 = Now
    QLegLabs 'do LL first to find labelseq-specific legends codes like Rnnon+183614
    ptime2 = Now
    gtime0 = gtime0 + DateDiff("s", ptime1, ptime2)
    
    ptime1 = Now
    QLegendCodes
    ptime2 = Now
    gtime1 = gtime1 + DateDiff("s", ptime1, ptime2)
    
    ptime1 = Now
    QLabelSeqs
    ptime2 = Now
    gtime2 = gtime2 + DateDiff("s", ptime1, ptime2)
    
    Check123456
    Check78
    Check910
    Check111213
    Check14151617
    Check181920
    Check21
    Check22
    CheckCustom
    CheckProcs
    CheckOutcomes
    
    
End Sub

Private Function FindLeg(s As String) As Boolean
    Dim i As Integer
    
    For i = 1 To legnum
        If dicleg(i).leg = s Then
            If dicleg(i).count > 0 Then
                FindLeg = True
                Exit Function
            End If
        End If
    Next i
End Function

Private Function FindLab(ls As Long) As Boolean
    Dim i As Integer
    
    For i = 1 To labnum
        If diclab(i).lab = ls Then
            If diclab(i).count > 0 Then
                FindLab = True
                Exit Function
            End If
        End If
    Next i
End Function

Private Function FindLegCount(s As String) As Integer
    Dim i As Integer
    
    FindLegCount = 0
    For i = 1 To legnum
        If dicleg(i).leg = s Then
            If dicleg(i).count > 0 Then
                FindLegCount = dicleg(i).count
                Exit Function
            End If
        End If
    Next i
End Function

Private Function FindLabCount(ls As Long) As Integer
    Dim i As Integer
    
    FindLabCount = 0
    For i = 1 To labnum
        If diclab(i).lab = ls Then
            If diclab(i).count > 0 Then
                FindLabCount = diclab(i).count
                Exit Function
            End If
        End If
    Next i
End Function
Private Function FindLL(s As String, ls As Long) As Boolean
    Dim i As Integer
    
    For i = 1 To llnum
        If dicll(i).lab = ls And dicll(i).leg = s Then
            If dicll(i).count > 0 Then
                FindLL = True
                Exit Function
            End If
        End If
    Next i
End Function

Private Sub Check123456()
    Dim npo, bth, l1, l2, ndsasist, full, cmplbth, a23 As Boolean
    
    If FindLeg("Self") Then
        inds(1).checked = True
    End If
    If FindLeg("Slfbth") And FindLeg("Indpdt") Then
        inds(1).checked = True
    End If
    
    If FindLeg("Self") Or FindLeg("Slfbth") Or FindLeg("Indpdt") Then
        If FindLeg("Upadlb") Then
            inds(1).checked = True
        End If
    End If
    
    
    a23 = FindLeg("Asist2") Or FindLeg("Assis3")
    If FindLeg("Asist1") Or a23 Or FindLeg("Stbyst") Then
        inds(2).checked = True
    End If
    npo = FindLeg("Npoice") Or FindLeg("Npomed") Or FindLeg("Npo")
    ndsasist = FindLeg("Ndsast")
    bth = FindLeg("Slfbth") Or FindLeg("Astbth") Or FindLeg("Pbdbth")
    l1 = FindLab(131307)
    If (npo Or ndsasist Or l1) And bth Then
        inds(2).checked = True
    End If
    If FindLeg("Astbth") Or FindLeg("Pbdbth") Then
        If FindLeg("Dangle") Then
            inds(2).checked = True
        End If
    End If
    
    full = FindLeg("Fulast")
    l2 = FindLab(6928)
    cmplbth = FindLeg("Cmpltb")
    If IsICU And FindLab(128611) Then
        inds(3).checked = True
    End If
    If (full Or npo Or l1 Or l2) And cmplbth Then
        inds(3).checked = True
    End If
    If FindLeg("Unrspv") Or FindLeg("Stupor") Or FindLeg("Sedatd") Or FindLeg("Obtund") Or FindLab(137062) Then
        inds(3).checked = True
    End If
    If cmplbth Then
        If FindLeg("Indpdt") Or FindLeg("Ndseat") Or FindLeg("Setup") Then
            inds(2).checked = True
            If inds(3).checked Then inds(3).checked = False
        End If
    End If
    

'    c1 = 0
'    c2 = 0
'    c3 = 0
'    c1 = FindLegCount("Self") + FindLegCount("Slfbth") + FindLegCount("Indpdt") + FindLegCount("Upadlb")
'    c2 = FindLeg("Asist1") + FindLegCount("Asist2") + FindLegCount("Assis3") + FindLegCount("Dangle")
'
'    npo = FindLeg("Npoice") Or FindLeg("Npomed") Or FindLeg("Npo")
'    ndsasist = FindLeg("Ndsast")
'    bth = FindLeg("Slfbth") Or FindLeg("Astbth") Or FindLeg("Pbdbth")
'    l1 = FindLab(131307)
'    If (npo Or ndsasist Or l1) And bth Then
'        inds(2).checked = True
'    End If
'
'    full = FindLeg("Fulast")
'    l2 = FindLab(6928)
'    cmplbth = FindLeg("Cmpltb")
''    If FindLab(128611) Then
''        inds(3).checked = True
''    End If
'    If (full Or npo Or l1 Or l2) And cmplbth Then
'        inds(3).checked = True
'    End If
'    If FindLeg("Unrespv") Or FindLeg("Stupor") Or FindLeg("Sedatd") Or FindLeg("Obtund") Or FindLab(137062) Then
'        inds(3).checked = True
'    End If
'    If cmplbth Then
'        If FindLeg("Indpdt") Or FindLeg("Ndseat") Or FindLeg("Setup") Then
'            inds(2).checked = True
'            If inds(3).checked Then inds(3).checked = False
'        End If
'    End If
'
    
    If FindLL("Tretmt", 157434) Or FindLL("Tretmt", 163062) Then
        inds(4).checked = True
    End If

    
    ' the qualification of "not indicator 1" must be interpreted differently in order to
    ' take into account no documentation for ADLs and the default ADL of 2.
    If (inds(2).checked Or inds(3).checked) Or Not (inds(1).checked Or inds(2).checked Or inds(3).checked) Then
'        If FindLab(138609) Then
'            inds(4).checked = True
'        End If
        If a23 Then
            inds(5).checked = True
        End If
        If FindLeg("Assis4") Then
            If IsICU Then
                inds(5).checked = True 'exception for ICU 6/19/09
            Else
                If CountUniqueLegs("Assis4") >= 2 Then inds(6).checked = True
            End If
        End If
    End If
End Sub


Private Sub Check78()
    Dim disqual As Boolean
    
    If FindLeg("Bldnes") Or FindLeg("Deafn") Or FindLab(145932) Then
        inds(7).checked = True
    End If
    disqual = FindLeg("Unrspv") Or FindLeg("Stupor") Or FindLeg("Sedatd") Or FindLeg("Obtund") Or FindLeg("Anespt")
    If Not disqual Then
        If FindLeg("Sdysar") Or FindLeg("Slurrd") Or FindLeg("Trache") Or FindLeg("Mute") Or FindLeg("Langbr") Then
            inds(7).checked = True
        End If
        If Not inds(7).checked Then
        If FindLL("Garbld", 131573) Or FindLL("Recaph", 131573) Or FindLL("Exprap", 131573) Or FindLL("Intubd", 131573) Or FindLL("Garbld", 25640) Then
            inds(7).checked = True
        End If
        End If
'        If FindLeg("Commun") Or FindLeg("Lang") Or FindLeg("Heardf") Or FindLeg("Visbar") Or FindLeg("Unablr") Or FindLL("Exprap", 138267) Or FindLL("Recaph", 138267) Then
'            If unresporstupor Or FindLeg("Dsornt") Or FindLL("Confsd", 5929) Then
'                If FindLeg("Family") Or FindLeg("Other") Or FindLeg("Parent") Or FindLeg("Sigoth") Or FindLeg("Spouse") Then
'                    inds(7).checked = True
'                End If
'            Else
'                inds(7).checked = True
'            End If
'        End If
    End If
    
    If Not FindLab(137062) And Not disqual Then
        If FindLL("Confsd", 124256) Or FindLeg("Reorit") Or FindLeg("Dsornt") Then
            inds(8).checked = True
        End If
        If Not inds(8).checked Then
            If FindLL("Confsd", 25638) Or FindLL("Delus", 25638) Or FindLL("Mendel", 25638) Then
                inds(8).checked = True
            End If
        End If
        If Not inds(8).checked Then
            If FindLL("Audito", 25639) Or FindLL("Visual", 25639) Then
                inds(8).checked = True
            End If
        End If
    End If
End Sub
Private Sub Check910()
    Dim unresporstupor, sedated, i6355, i138610, i11240, i124256, i26328, i26323, i26327 As Boolean
    
    
    i6355 = FindLL("Abusiv", 6355) Or FindLL("Agresv", 6355) Or _
        FindLL("Agitat", 6355) Or FindLL("Agoth", 6355) Or FindLL("Angry", 6355) Or FindLL("Attsek", 6355) Or _
        FindLL("Combtv", 6355) Or FindLL("Crying", 6355) Or FindLL("Demndg", 6355) Or FindLL("Despro", 6355) Or _
        FindLL("Extsek", 6355) Or FindLL("Fearso", 6355) Or FindLL("Greivg", 6355) Or FindLL("Homici", 6355) Or _
        FindLL("Iratnl", 6355) Or FindLL("Nobond", 6355) Or FindLL("Scream", 6355) Or FindLL("Suicdl", 6355) Or _
        FindLL("Threat", 6355) Or FindLL("Wander", 6355)
        
    i26328 = FindLL("Behmod", 26328) Or FindLL("Holdng", 26328) Or FindLL("Limits", 26328) Or FindLL("Listnd", 26328) Or FindLL("Reassd", 26328) Or FindLL("Rockng", 26328) Or _
        FindLL("Extdsp", 26328) Or FindLL("Famcnf", 26328) Or FindLL("Medctd", 26328) Or FindLL("Refcas", 26328) Or FindLL("Refpas", 26328) Or FindLL("Sa2", 26328)
        
    i138610 = FindLab(138610)
    
    i11240 = FindLL("Angry", 11240) Or FindLL("Homici", 11240) Or FindLL("Suicdl", 11240)
    i124256 = FindLL("Agitat", 124256) Or FindLL("Combtv", 124256) Or FindLL("Confsd", 124256)
    If i6355 Or i124256 Or FindLL("Delus", 25638) Or FindLL("Prnoid", 25638) Or _
        FindLL("Mendel", 25638) Or FindLL("Audito", 25639) Or FindLL("Visual", 25639) Or i11240 Or FindLL("Yes", 25644) Then
        If i26328 Then
            If i138610 Then
                inds(10).checked = True
            Else
                inds(9).checked = True
            End If
        End If
    End If
    
    i26323 = FindLL("Abusiv", 26323) Or FindLL("Agresv", 26323) Or FindLL("Angry", 26323) Or FindLL("Greivg", 26323) Or FindLL("Threat", 26323)
    i26327 = FindLL("Extdsp", 26327) Or FindLL("Escort", 26327) Or FindLL("Famcnf", 26327) Or FindLL("Ntngr", 26327) Or FindLL("Notpol", 26327) Or FindLL("Notsec", 26327) Or FindLL("Refcas", 26327) Or FindLL("Refpas", 26327)
    If i26323 And i26327 Then
        If i138610 Then
            inds(10).checked = True
        Else
            inds(9).checked = True
        End If
    End If
    
End Sub

Private Sub Check111213()

    If FindLL("Wander", 6355) Then
        inds(11).checked = True
    End If
    If FindLab(138658) Then
        inds(11).checked = True
        inds(50).checked = True
    End If
    
    If FindLL("Suicde", 132651) Or FindLL("Homici", 132651) Then
        If FindLL("Sa2", 26328) Or FindLL("Sa2", 138651) Then
            inds(12).checked = True
        End If
    End If
    
' not valid for any units 7/22/09
'    If unit <> "N3G" Then  ' not valid for ICU
'    If FindLab(141180) Then
'        inds(12).checked = True
'    End If
'    End If
    
    If FindLab(131609) Then
        inds(13).checked = True
    End If
    If Not inds(13).checked Then
    If FindLL("Airbor", 132650) Or FindLL("Droplt", 132650) Or FindLL("Contac", 132650) Or FindLL("Cdif", 132650) Or FindLL("Mrsa", 132650) Or FindLL("Vre", 132650) Or FindLL("Nutrpn", 132650) Or FindLL("Raditn", 132650) Then
        inds(13).checked = True
    End If
    End If
End Sub
Private Sub Check14151617()
    
    If FindLeg("Implmd") Or FindLL("Mntain", 131528) Then
        inds(14).checked = True
    End If
    
'    QAssess False, False, False   turned off 8/28/09
    expi = 0
'    QAssess True, True, False   'default is (makeTC, include VS, only VS) = (T,T,F)
    QExperimentAssess
'    If experimentison Then 'we want this to be a regular assessment
'        QExperimentAssess
'    End If

'    QAssess False, False, True   'default is (makeTC, include VS, only VS) = (T,T,F)

End Sub
Private Sub Check181920()

    If FindLab(138617) Or FindLab(134454) Or FindLab(134455) Or FindLab(134456) Or FindLab(134457) Or FindLab(134458) Then
        inds(18).checked = True
    End If
    
    If FindLab(138613) Then
        inds(20).checked = True
    End If
    
    QSheath20
    
 'if result < 30 minutes then 19....
 'if  result >= 30 minutes then 20....
 '47138, 47137, 47134, 47133, 47136, 47135, 71863, 71844, 47132, 47131
 'How will these result values be formatted?  Use new function QFindLabVal
    If Not inds(20).checked Then
    If FindLab(47138) Or FindLab(47137) Or FindLab(47134) Or FindLab(47133) Or FindLab(47136) Or _
       FindLab(47135) Or FindLab(71863) Or FindLab(71844) Or FindLab(47132) Or FindLab(47131) Then
        inds(19).checked = True
        If QWound >= 30 Then
            inds(20).checked = True
        End If
    End If
    End If
    
    If Not (inds(20).checked Or inds(19).checked) Then
        Q19
    End If
    
    'Ostomy 19
    If Not (inds(20).checked Or inds(19).checked) Then
        If FindLab(141785) Or FindLab(134435) Or FindLab(134436) Or FindLab(134437) Or FindLab(134438) Or _
           FindLab(134439) Or FindLab(134440) Or FindLab(134441) Or FindLab(134442) Or FindLab(134443) Then
            inds(19).checked = True
        End If
    End If
    
    If Not (inds(19).checked Or inds(20).checked) Then
    If FindLeg("Cordcr") Then
        inds(19).checked = True
    End If
    End If
    
    If Not (inds(19).checked Or inds(20).checked) Then
    If FindLeg("Abrasn") Or FindLeg("Blster") Or FindLeg("Burn") Or FindLeg("Denude") Or FindLeg("Ecortd") Or FindLeg("Tear") Or FindLeg("Ulcrtn") Or FindLeg("Vaglek") Then
        inds(19).checked = True
    End If
    End If
    
    If Not (inds(20).checked Or inds(19).checked) Then
        QWound19
    End If
    
    If Not (inds(20).checked Or inds(19).checked) Then
        QDressing19
    End If
    
    If Not (inds(19).checked Or inds(20).checked) Then
    If FindLab(133262) Or FindLab(132275) Or FindLab(132327) Or FindLab(132288) Or FindLab(132301) Or _
       FindLab(132314) Or FindLab(132340) Or FindLab(132357) Or FindLab(132264) Or FindLab(132277) Or _
       FindLab(132329) Or FindLab(132290) Or FindLab(132303) Or FindLab(132316) Or FindLab(132342) Or _
       FindLab(132359) Or FindLab(132269) Or FindLab(132282) Or FindLab(132334) Or FindLab(132295) Or _
       FindLab(132308) Or FindLab(132321) Or FindLab(132347) Or FindLab(132364) Or FindLab(132263) Or _
       FindLab(132276) Or FindLab(132328) Or FindLab(132289) Or FindLab(132302) Or FindLab(132315) Or _
       FindLab(132341) Or FindLab(132358) Or FindLab(131722) Or FindLab(66043) Then
        inds(19).checked = True
    End If
    End If

    If Not (inds(19).checked Or inds(20).checked) Then
    If FindLab(135253) Or _
       FindLab(153634) Or FindLab(153635) Or FindLab(153636) Or FindLab(153367) Or FindLab(153368) Or _
       FindLab(153369) Or FindLab(154133) Then
        inds(19).checked = True
    End If
    End If
    
    If Not (inds(19).checked Or inds(20).checked) Then
    If FindLab(47061) Or FindLab(71856) Or FindLab(71837) Or FindLab(47058) Or FindLab(47057) Or _
       FindLab(47064) Or FindLab(47063) Or FindLab(47060) Or FindLab(47059) Or FindLab(47062) Then
        inds(19).checked = True
    End If
    End If
    
    If FindLeg("Ctline") Or FindLeg("Dblumn") Or FindLeg("Dialys") Or FindLeg("Groshg") Or FindLeg("Hickmn") Or FindLeg("Picc") Or FindLeg("Porcat") Or FindLeg("Ppicc") Or FindLeg("Sglumn") Or FindLeg("Trilum") Or FindLeg("Twncth") Then
        inds(56).checked = True
        If FindLeg("Drsgch") Then
            inds(19).checked = True
        End If
    End If
 
End Sub
Private Sub Check21()
    Dim rs As New Recordset
    Dim sql As String
    Dim educprovided As Boolean
    Dim lop As Integer
    Dim largestmins As Integer
    
    dprint "Check21"
    
    sql = "select count(*) from " & DBTABLENAME & " where patient_id='" & acct & "' and patresultslabelseq in "
    sql = sql & "(167958,137119,137738,137724,137726,137727,137731,137723,137736,137734,"
    sql = sql & "141869,137635,137955,137950,137948,137949,"
    sql = sql & "138559,138564,138576,138581,137788,137792,138554,"
    sql = sql & "138122,138605,137935,137937,137934,137946,137932,"
    sql = sql & "137996,138000,137999,138007,137995,138010,137997,138011,138012,138004,138009,"
    sql = sql & "138017,138018,138019,141446,138035,164515,138022,141451,141441,138039,138024,"
    sql = sql & "138058,138046,138041,138042,138043,138050,"
    sql = sql & "138065,138066,138061,138062,138071,138072,138085,138105,138059,138075,138097,138081,"
    sql = sql & "137855,137836,137860,141404,137842,"
    sql = sql & "138141,138136,138150,138149,138139,138135,138133,138138,138143,138130,138142)"
    If lookback_on Then
        sql = sql & " and convert(datetime,performdate)>=" & g_dbutil.SQL_DateTime(lookback_dt)
    End If
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        If rs.RecordCount > 0 Then educprovided = (rs(0) > 0)
    End If
    rs.Close
    Set rs = Nothing
    
    If Not educprovided Then Exit Sub
    dprint "Education was provided."
        
    largestmins = 0
    If FindLL("Rnstf", 138615) Or FindLL("Rnnstf", 138615) Or FindLL("Rnnon", 138615) Or _
       FindLL("Rnstf", 246011) Or FindLL("Rnnstf", 246011) Or FindLL("Rnnon", 246011) Then
       
       lop = EducTime("(178382,246012)", largestmins)
       If lop >= 60 Then 'sum minutes of 178382, 246012
            inds(21).checked = True
            'and procedure 4 IFF largestmins>=60
        End If
    End If
    
    If largestmins >= 60 Then 'add proc 4  ONLY IF greatest chunk is >=60min AND cap at 120mins
        proc(4).stime = QLatestPerfTime("(138615,246011)") 'looking at all Rnstf,Rnnstf,Rnnon
        If largestmins > 120 Then largestmins = 120
        dprint "Proc stime4=" & proc(4).stime & " minutes=" & largestmins
        proc(4).etime = DateAdd("n", largestmins, CDate(proc(4).stime)) 'add largestmins to start
        proc(4).checked = True
    End If

End Sub
Private Function EducTime(strlab As String, largestmins As Integer) As Integer
    Dim rs As New Recordset
    Dim sql, s As String
    Dim i, count, v As Integer
    
    v = 0
    largestmins = 0
    sql = "select result_value from " & DBTABLENAME & " where patient_id='" & acct & "' and patresultslabelseq in " & strlab
    If lookback_on Then
        sql = sql & " and convert(datetime,performdate)>=" & g_dbutil.SQL_DateTime(lookback_dt)
    End If
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        count = rs.RecordCount
        rs.MoveFirst
        For i = 1 To count
            s = rs(0)
            s = Replace(s, "min", "")
            If IsNumeric(s) Then
                v = v + CInt(s)
                If CInt(s) > largestmins Then largestmins = CInt(s)
            End If
            If Not rs.EOF Then rs.MoveNext
        Next i
    End If
    
    EducTime = v
    rs.Close
    Set rs = Nothing

End Function

Private Sub Check22()
'    If FindLab(138615) Then
'        If FindLab(141379) And FindLab(141380) Then
'            If FindLeg("Unrspv") Or FindLeg("Stupor") Or FindLeg("Dsornt") Or FindLL("Confsd", 124256) Then
'                If FindLeg("Family") Or FindLeg("Other") Or FindLeg("Parent") Or FindLeg("Sigoth") Or FindLeg("Spouse") Then
'                    inds(21).checked = True
'                End If
'            Else
'                inds(21).checked = True
'            End If
'        End If
'    End If
    
    If FindLab(138612) Then
        inds(22).checked = True
    End If
    If FindLab(135332) Then '3,5,17,19,22,78
        inds(3).checked = True
        inds(5).checked = True
        inds(17).checked = True
        inds(19).checked = True
        inds(22).checked = True
        inds(78).checked = True
    End If
    If FindLab(154140) Or FindLab(63768) Then '3,5,16,22,79
        inds(3).checked = True
        inds(5).checked = True
        inds(17).checked = True
        inds(19).checked = True
        inds(22).checked = True
        inds(79).checked = True
    End If
    If unit = "C3A" Or unit = "C3B" Then
        If FindLL("Cvor", 131815) Then    'there is a time limit of 12 hrs on this in the dicll
            inds(3).checked = True
            inds(5).checked = True
            inds(17).checked = True
            inds(19).checked = True
            inds(22).checked = True
            inds(102).checked = True
        End If
    End If
    
    If FindLL("Cathlb", 131815) Or FindLL("Epstud", 131815) Or _
       FindLL("Cathlb", 137692) Or FindLL("Epstud", 137692) Then
       'there is a time limit 12 hrs on these in the dicll
        inds(2).checked = True
        inds(5).checked = True
        inds(11).checked = True
        inds(16).checked = True
        inds(19).checked = True
        inds(109).checked = True
    End If
    
    
End Sub

Private Sub CheckCustom()
    Dim count As Integer
'    dprint "CheckCustom"
    If FindLab(138658) Then
        inds(11).checked = True
        inds(50).checked = True
    End If
    
    If FindLab(146832) Or FindLab(149187) Then
        inds(51).checked = True
    End If

    If FindLeg("Trachi") Then
        inds(52).checked = True
    End If
    
    Q53
    
    If FindLab(137063) Or FindLab(3372) Or FindLab(19529) Then
        inds(54).checked = True
    End If
    
    If FindLL("Vre", 132650) Then
        inds(13).checked = True
        inds(57).checked = True
    End If
    
    If FindLL("Yes", 246285) Then     'changed from 141385 for Mar01 2011
        inds(22).checked = True
        inds(58).checked = True
    End If
    
    If FindLab(137693) And (FindLeg("Icu") Or FindLeg("Rghc")) Then
        inds(59).checked = True
    End If
    
    If FindLab(138611) Then
        inds(12).checked = True
        If FindLL("Suicde", 132651) Or FindLL("Homici", 132651) Or FindLL("Homici", 11240) Or FindLL("Suicdl", 11240) Then
            inds(66).checked = True
        Else
            inds(60).checked = True
        End If
    End If
    
    If FindLL("Intubd", 134342) Then
        inds(62).checked = True
    End If
    
    If FindLeg("Extbtd") Or FindLeg("Extubs") Then
        inds(63).checked = True
    End If

    If FindLab(138502) Then
        inds(64).checked = True
    End If
    
    If FindLab(128593) Then
        inds(14).checked = True
        inds(67).checked = True
    End If
    
    If FindLab(6925) Then
        inds(68).checked = True
    End If
    
    
    If Not IsICU Then  ' not valid for ICU
        If FindLL("High Risk", 177674) Then
            inds(11).checked = True
            inds(69).checked = True
        End If
        
        If Not (inds(11).checked And inds(69).checked) Then
        If FindLL("Fallr5", 134895) Then ' we want to know that the label seq exists
            If QFallr5("Fallr5", 134895) >= 5 Then   ' now a greatest of zero value makes sense
                inds(11).checked = True
                inds(69).checked = True
            End If
        End If
        End If
    End If
    
    If FindLeg("Hypotp") Or FindLeg("Therpu") Then
        inds(3).checked = True
        inds(16).checked = True
        inds(70).checked = True
    End If
    
    If FindLab(141385) Then  'only for unit NNICU
        inds(71).checked = True
    End If
    
    If FindLab(6982) And FindLab(18816) Then
        count = CountUniqueLabs(18816)
        If count >= 13 Then
            inds(72).checked = True
            inds(16).checked = True
            inds(18).checked = True '8/17/10 call with Kelly
        ElseIf count >= 7 Then
            inds(73).checked = True
            inds(15).checked = True
            inds(18).checked = True
        End If
    End If
    
    If FindLab(7048) Or FindLab(129816) Or FindLab(129645) Then
        inds(16).checked = True
        inds(74).checked = True
    End If
    
    If FindLab(129637) Or FindLab(7050) Or FindLab(7049) Then
        inds(16).checked = True
        inds(75).checked = True
    End If
    
    If FindLab(129648) Then
        inds(16).checked = True
        inds(76).checked = True
    End If
    
    If FindLab(6975) Then
        inds(14).checked = True
        inds(77).checked = True
    End If
    
    If FindLab(135339) Or FindLab(77939) Or FindLab(131959) Or FindLab(131957) Or _
       FindLab(135332) Or FindLab(135333) Or FindLab(135334) Or FindLab(135335) Or FindLab(135336) Or FindLab(135337) Then
        inds(3).checked = True
        inds(5).checked = True
        inds(17).checked = True
        inds(19).checked = True
        inds(22).checked = True
        inds(78).checked = True
    End If

    If FindLab(131292) Then
        inds(3).checked = True
        inds(5).checked = True
        inds(16).checked = True
        inds(22).checked = True
        inds(79).checked = True
    End If
    
    If FindLeg("Folcth") Or FindLab(6933) Then
        inds(80).checked = True
    End If
    
    If FindLeg("Piccln") Then
        inds(81).checked = True
    End If
    
    If FindLab(10084) Or FindLab(131963) Then
        inds(16).checked = True
        inds(82).checked = True
    End If
    
    If FindLab(131314) Then
        inds(16).checked = True
        inds(83).checked = True
    End If

    If FindLL("Yes", 246283) Then  'changed from 135049
        inds(87).checked = True
    End If
    If FindLab(129617) Then
        inds(100).checked = True
    End If
    
    If FindLab(170176) Or FindLab(170177) Or FindLab(170178) Or FindLab(170179) Or FindLab(170180) Then
        inds(3).checked = True
        inds(6).checked = True
        inds(17).checked = True
        inds(101).checked = True
    End If
    
    QCustom103to108
    ' 3424,139202,4301,20708,3383,20803,19159,19250
'    dprint "end CheckCustom"
End Sub

Private Sub CheckProcs()
    dprint "CheckProcs"
    If FindLab(138611) Then
        If FindLab(141363) Then
            proc(1).stime = QLatestDateTimeOfLab(141363)
            proc(1).etime = QLatestDateTimeOfLab(141364)
            If proc(1).stime <> "" Then
            If proc(1).etime = "" Then
                proc(1).checked = True 'default to eod
            Else
                proc(1).checked = DateDiff("n", CDate(proc(1).stime), CDate(proc(1).etime)) >= 60
            End If
            End If
        End If
    End If
                
    If FindLL("Rn", 137691) Then
        If FindLab(141383) Then
            proc(2).stime = QLatestDateTimeOfLab(141383)
            dprint "Proc stime2=" & proc(2).stime
            proc(2).etime = QLatestDateTimeOfLab(141384)
            dprint "Proc etime2=" & proc(2).etime
            If proc(2).stime <> "" Then
            If proc(2).etime = "" Then
                proc(2).etime = DateAdd("n", 60, CDate(proc(2).stime)) 'no end time then 1 hr
                proc(2).checked = True
            Else
                proc(2).checked = DateDiff("n", CDate(proc(2).stime), CDate(proc(2).etime)) >= 60
            End If
            End If
        End If
    End If

    If FindLeg("Lpnurs") Or FindLeg("Cnapct") Then
        If FindLab(141383) Then
            proc(3).stime = QLatestDateTimeOfLab(141383)
            dprint "Proc stime3=" & proc(3).stime
            proc(3).etime = QLatestDateTimeOfLab(141384)
            dprint "Proc stime3=" & proc(3).etime
            If proc(3).stime <> "" Then
            If proc(3).etime = "" Then
                proc(3).etime = DateAdd("n", 60, CDate(proc(3).stime)) 'no end time then 1 hr
                proc(3).checked = True
            Else
                proc(3).checked = DateDiff("n", CDate(proc(3).stime), CDate(proc(3).etime)) >= 60
            End If
            End If
        End If
    End If
    
'    If FindLab(138615) Then
'        If FindLab(141379) Then
'            proc(4).stime = QLatestDateTimeOfLab(141379)
'            dprint "Proc stime4=" & proc(4).stime
'            proc(4).etime = QLatestDateTimeOfLab(141380)
'            dprint "Proc etime4=" & proc(4).etime
'            If proc(4).stime <> "" Then
'            If proc(4).etime = "" Then
'                proc(4).etime = DateAdd("n", 120, CDate(proc(4).stime)) 'no end time then 2 hr
'                proc(4).checked = True
'            Else
'                proc(4).checked = DateDiff("n", CDate(proc(4).stime), CDate(proc(4).etime)) >= 60
'                If DateDiff("n", CDate(proc(4).stime), CDate(proc(4).etime)) > 120 Then
'                    proc(4).etime = DateAdd("n", 120, CDate(proc(4).stime)) '2 hr max
'                End If
'            End If
'            End If
'        End If
'    End If
    
    If FindLL("Rn", 138614) Then
        If FindLab(141377) Then
            proc(5).stime = QLatestDateTimeOfLab(141377)
            dprint "Proc stime5=" & proc(5).stime
            proc(5).etime = QLatestDateTimeOfLab(141378)
            dprint "Proc stime5=" & proc(5).etime
            If proc(5).stime <> "" Then
            If proc(5).etime = "" Then
                proc(5).etime = DateAdd("n", 60, CDate(proc(5).stime)) 'no end time then 1 hr
                proc(5).checked = True
                inds(20).checked = True
            Else
                proc(5).checked = DateDiff("n", CDate(proc(5).stime), CDate(proc(5).etime)) >= 60
                If proc(5).checked Then inds(20).checked = True
            End If
            End If
        End If
    End If

    If FindLL("Rnnon", 138614) Then
        If FindLab(141377) Then
            proc(6).stime = QLatestDateTimeOfLab(141377)
            dprint "Proc stime6=" & proc(6).stime
            proc(6).etime = QLatestDateTimeOfLab(141378)
            dprint "Proc etime6=" & proc(6).etime
            If proc(6).stime <> "" Then
            If proc(6).etime = "" Then
                proc(6).etime = DateAdd("n", 60, CDate(proc(6).stime)) 'no end time then 1 hr
                proc(6).checked = True
                inds(20).checked = True
            Else
                proc(6).checked = DateDiff("n", CDate(proc(6).stime), CDate(proc(6).etime)) >= 60
                If proc(6).checked Then inds(20).checked = True
            End If
            End If
        End If
    End If

    If FindLab(138616) Then
        If FindLab(141373) Then
            proc(7).stime = QLatestDateTimeOfLab(141373)
            dprint "Proc stime7=" & proc(7).stime
            proc(7).etime = QLatestDateTimeOfLab(141374)
            dprint "Proc etime7=" & proc(7).stime
            If proc(7).stime <> "" Then
            If proc(7).etime = "" Then
                proc(7).etime = DateAdd("n", 60, CDate(proc(7).stime)) 'no end time then 1 hr
                proc(7).checked = True
            Else
                proc(7).checked = DateDiff("n", CDate(proc(7).stime), CDate(proc(7).etime)) >= 60
            End If
            End If
        End If
    End If

    If FindLL("Oneone", 138620) Then
        If FindLab(141375) Then
            proc(8).stime = QLatestDateTimeOfLab(141375)
            dprint "Proc stime8=" & proc(8).stime
            proc(8).etime = QLatestDateTimeOfLab(141376)
            dprint "Proc etime8=" & proc(8).stime
            If proc(8).stime <> "" Then
            If proc(8).etime = "" Then
                proc(8).etime = DateAdd("n", 60, CDate(proc(8).stime)) 'no end time then 1 hr
                proc(8).checked = True
            Else
                proc(8).checked = DateDiff("n", CDate(proc(8).stime), CDate(proc(8).etime)) >= 60
            End If
            End If
        End If
    End If

    If Not proc(8).checked And FindLab(132591) And FindLab(132653) Then
            proc(8).stime = QLatestDateTimeOfLab(132591)
            dprint "Proc stime8=" & proc(8).stime
            proc(8).etime = QLatestDateTimeOfLab(132653)
            dprint "Proc etime8=" & proc(8).stime
            If proc(8).stime <> "" Then
            If proc(8).etime = "" Then
                proc(8).etime = DateAdd("n", 60, CDate(proc(8).stime)) 'no end time then 1 hr
                proc(8).checked = True
            Else
                proc(8).checked = DateDiff("n", CDate(proc(8).stime), CDate(proc(8).etime)) >= 60
            End If
            End If
    End If
    
    If FindLL("Twoone", 138620) Then
        If FindLab(141375) Then
            proc(9).stime = QLatestDateTimeOfLab(141375)
            dprint "Proc stime9=" & proc(9).stime
            proc(9).etime = QLatestDateTimeOfLab(141376)
            dprint "Proc etime9=" & proc(9).stime
            If proc(9).stime <> "" Then
            If proc(9).etime = "" Then
                proc(9).etime = DateAdd("n", 60, CDate(proc(9).stime)) 'no end time then 1 hr
                proc(9).checked = True
            Else
                proc(9).checked = DateDiff("n", CDate(proc(9).stime), CDate(proc(9).etime)) >= 60
            End If
            End If
        End If
    End If
    
    If FindLL("Rn", 138615) Then
        If FindLab(141379) Then
            proc(4).stime = QLatestDateTimeOfLab(141379)
            dprint "Proc stime4h=" & proc(4).stime
            proc(4).etime = QLatestDateTimeOfLab(141380)
            dprint "Proc etime4h=" & proc(4).stime
            If proc(4).stime <> "" Then
            If proc(4).etime = "" Then
                proc(4).etime = DateAdd("n", 60, CDate(proc(4).stime)) 'no end time then 1 hr
                proc(4).checked = True
                inds(21).checked = True
            Else
                proc(4).checked = DateDiff("n", CDate(proc(4).stime), CDate(proc(4).etime)) >= 60
                If proc(4).checked Then inds(21).checked = True
            End If
            End If
        End If
    End If
    
    
    dprint "end CheckProcs"
        
End Sub

Private Sub CheckOutcomes()
    If FindLab(141184) Then
        oc(1).stime = QLatestDateTimeOfLab(141184)
        dprint "Outcome time=" & oc(1).stime
        If oc(1).stime <> "" Then
            oc(1).checked = True
        End If
    End If
    If FindLL("Yes", 246285) Then
        oc(30).stime = QLatestDateTimeOfLab(246285)
        dprint "Outcome30 time=" & oc(30).stime
        If oc(30).stime <> "" Then
            oc(30).checked = True
        End If
    End If
    If FindLL("Yes", 246283) Then
        oc(31).stime = QLatestDateTimeOfLab(246283)
        dprint "Outcome31 time=" & oc(31).stime
        If oc(31).stime <> "" Then
            oc(31).checked = True
        End If
    End If
End Sub

Private Function LoadDictionary() As Boolean
'There are 3 dictionaries:  1 for LabelSeqs,1 for legend codes and 1 for both -- none intersect!
    Dim dicfn As String
    Dim dicfile As Integer
    Dim buf As String
    Dim p As Integer
    Dim p2 As Integer

    labnum = 0
    dicfile = FreeFile
    dicfn = App.Path & "\DICTLAB.DAT"
    Open dicfn For Input As #dicfile
    While Not EOF(dicfile)
        Line Input #dicfile, buf
        If IsNumeric(Trim$(Mid$(buf, 1, 20))) Then
            labnum = labnum + 1
            ReDim Preserve diclab(0 To labnum)
            'diclab(labnum).ind = val(Mid$(buf, 1, 2))
            diclab(labnum).lab = CLng(Trim$(Mid$(buf, 1, 20)))
        End If
    Wend
    Close #dicfile
    
    legnum = 0
    dicfile = FreeFile
    dicfn = App.Path & "\DICTLEG.DAT"
    Open dicfn For Input As #dicfile
    While Not EOF(dicfile)
        Line Input #dicfile, buf
        If Trim$(buf) <> "" Then
            legnum = legnum + 1
            ReDim Preserve dicleg(0 To legnum)
            'dicleg(legnum).ind = val(Mid$(buf, 1, 2))
            dicleg(legnum).persist = (Mid$(buf, 1, 1) = "#")
            dicleg(legnum).leg = Trim$(Mid$(buf, 1 + Abs(dicleg(legnum).persist), 20))
        End If
    Wend
    Close #dicfile
    
    llnum = 0
    dicfile = FreeFile
    dicfn = App.Path & "\DICTLL.DAT"
    Open dicfn For Input As #dicfile
    While Not EOF(dicfile)
        Line Input #dicfile, buf
        If Trim$(buf) <> "" Then
            llnum = llnum + 1
            ReDim Preserve dicll(0 To llnum)
            'dicll(llnum).ind = val(Mid$(buf, 1, 2))
            p = InStr(buf, ",")
            dicll(llnum).leg = Trim$(Mid$(buf, 1, p - 1))
            p2 = InStr(Mid$(buf, p + 1, Len(buf)), ",")
            If p2 = 0 Then
                dicll(llnum).lab = CLng(Trim$(Mid$(buf, p + 1, 20)))
                dicll(llnum).validhrs = 0   'zero means always valid
            Else
                dicll(llnum).lab = CLng(Mid$(buf, p + 1, p2 - 1))
                dicll(llnum).validhrs = CInt(Trim$(Mid$(buf, p2 + p + 1, 4)))
            End If
        End If
    Wend
    Close #dicfile
    
    unitnum = 0
    dicfile = FreeFile
    dicfn = App.Path & "\NEGAUNIT.UNT"
    Open dicfn For Input As #dicfile
    While Not EOF(dicfile)
        Line Input #dicfile, buf
        If Trim$(buf) <> "" Then
            unitnum = unitnum + 1
            ReDim Preserve unitary(0 To unitnum)
            unitary(unitnum).name = UCase$(Trim$(Mid$(buf, 1, 16)))
        End If
    Wend
    Close #dicfile
    
    LoadDictionary = True
End Function


Private Sub InitIndicators()
    Dim i As Integer
    
    For i = 1 To MAX_INDS
        inds(i).checked = False
    Next i
    For i = 1 To MAX_PROC
        proc(i).checked = False
        proc(i).stime = ""
        proc(i).etime = ""
    Next i
    For i = 1 To MAX_OCI
        oc(i).checked = False
    Next i
    
End Sub
Private Sub InitGroups()
    Dim i As Integer
    
    'THIS IS FOR the INPATIENT METHODOLOGY ONLY
    
    For i = 1 To MAX_INDICATORS
        grps(i) = 0
    Next i
    
    For i = 1 To 3
        grps(i) = 1
    Next i
    For i = 5 To 6
        grps(i) = 2
    Next i
    For i = 9 To 10
        grps(i) = 3
    Next i
    For i = 11 To 12
        grps(i) = 4
    Next i
    For i = 14 To 17
        grps(i) = 5
    Next i
    For i = 19 To 20
        grps(i) = 6
    Next i

End Sub

Private Function ValidUnit(u As String) As Boolean
    Dim i As Integer
    
    ValidUnit = False
    
    For i = 1 To unitnum
        If UCase$(u) = unitary(i).name Then
            ValidUnit = True
            Exit For
        End If
    Next i
    

End Function

Private Function GetUnitIndex(u As String) As Integer
    Dim i As Integer
    
    GetUnitIndex = 0
    
    For i = 1 To unitnum
        If UCase$(u) = unitary(i).name Then
            GetUnitIndex = i
            Exit For
        End If
    Next i
    

End Function

Private Function StrTimeToOutStr(t As String) As String
    Dim yyyy As String
    Dim mm As String
    Dim dd As String
    Dim hh As String
    Dim nn As String
    
        yyyy = Trim$(Str(Year(g_util.DateOnly(t))))
        mm = Trim$(Str(Month(g_util.DateOnly(t))))
        If Month(g_util.DateOnly(t)) < 10 Then
            mm = "0" & mm
        End If
        dd = Trim$(Str(Day(g_util.DateOnly(t))))
        If Day(g_util.DateOnly(t)) < 10 Then
            dd = "0" & dd
        End If
        hh = Trim$(Str(Hour(g_util.TimeOnly(t))))
        If Hour(g_util.TimeOnly(t)) < 10 Then
            hh = "0" & hh
            If Hour(g_util.TimeOnly(t)) = 0 Then hh = "00"
        End If
        nn = Trim$(Str(Minute(g_util.TimeOnly(t))))
        If Minute(g_util.TimeOnly(t)) < 10 Then
            nn = "0" & nn
            If Minute(g_util.TimeOnly(t)) = 0 Then nn = "00"
        End If
        StrTimeToOutStr = yyyy & mm & dd & hh & nn

End Function

Private Sub CheckMutuality()
    Dim i, g As Integer
    Dim highest_is_on As Boolean
    'Now, if there are any mutually exclusive indicators marked
    'then remove the lesser ones.
    g = 0
    For i = MAX_INDICATORS To 1 Step -1
        If (grps(i) > 0) Then
            If (grps(i) <> g) Then
                g = grps(i)
                highest_is_on = inds(i).checked
            Else
                If highest_is_on Then
                    inds(i).checked = False
                Else
                    highest_is_on = inds(i).checked
                End If
            End If
        End If
    Next i
    
    '109 and 110 are mutually exclusive
    If inds(109).checked And inds(110).checked Then
        inds(109).checked = False
    End If

End Sub


Private Sub MakeLogFiles()
    Const HKEY_LOCAL_MACHINE = &H80000002
    Dim tcname As String
    Dim tcname1 As String
    Dim tcname2 As String
    Dim ocname As String
    Dim inlogname As String
    Dim outlogname As String
    Dim dbugname As String
    Dim dt As Variant
    
    If Not g_util.GetKeyValue(HKEY_LOCAL_MACHINE, "SOFTWARE\Quadramed\WinPFS", "Directory", winpfspath) Then
        winpfspath = App.Path
    End If

    If g_util.DirExists(winpfspath) Then
        loadpath = winpfspath & "\load_me"
        If Not g_util.DirExists(loadpath) Then
            MkDir$ (loadpath)
        End If
        logpath = winpfspath & "\log"
        If Not g_util.DirExists(logpath) Then
            MkDir$ (logpath)
        End If
    Else
        loadpath = App.Path
        logpath = App.Path
    End If
    
    dt = nowdt
    tcname = loadpath & "\Transparent.txt"
    If experimentison Then
        tcname1 = loadpath & "\" & experimenttm & "Transparent.txt" 'This file has the 0701,1501 etc times.
        tcname2 = loadpath & "\" & resumetm & "Transparent.txt" 'This file has the 0702,1502 etc times.
    End If
    inlogname = logpath & "\TranspIn_" & Format$(dt, "mmdd") & ".log"
    outlogname = logpath & "\TranspOut_" & Format$(dt, "mmdd") & ".log"
    dbugname = logpath & "\DbugTransp_" & Format$(dt, "mmdd") & ".log"
    ocname = loadpath & "\OutcomesIndicator.txt"
    
    tcfile = FreeFile
    Open tcname For Append As #tcfile
    
    If experimentison Then
        tcfile1 = FreeFile
        Open tcname1 For Append As #tcfile1
        tcfile2 = FreeFile
        Open tcname2 For Append As #tcfile2
    End If
    
    inlogfile = FreeFile
    Open inlogname For Append As #inlogfile
    Print #inlogfile, "****** WinPFS Transparent Classification Input    Time=" & nowdt & " ****"
    
    outlogfile = FreeFile
    Open outlogname For Append As #outlogfile
    Print #outlogfile, "****** WinPFS Transparent Classification Output    Time=" & nowdt & " ****"
    
    ocfile = FreeFile
    Open ocname For Append As #ocfile
    
    If dbugon Then
        dbugfile = FreeFile
        Open dbugname For Append As #dbugfile
        Print #dbugfile, "******TRANSPARENT TRANSLATION DEBUGGING MODE*****"
    End If
    
End Sub

Private Sub CloseLogFiles()

    Close tcfile
    Close inlogfile
    Close outlogfile
    Close ocfile
    If experimentison Then
        Close tcfile1
        Close tcfile2
    End If
    If dbugon Then
        Close dbugfile
    End If

End Sub

Private Sub DeleteOldLogs()
    Dim temp As String
    Dim i As Integer
    Dim dt As Variant

    ' Delete old log files > LIFE days old.
    dt = DateAdd("d", -TRANSP_INLOG_FILE_LIFE, nowdt)
    For i = 1 To 5
        temp = logpath & "\TranspIn_" & Format$(dt, "mmdd") & ".log"
        If g_util.FileExists(temp) Then
            Kill temp
        End If
        dt = DateAdd("d", -1, dt)
    Next i

    dt = DateAdd("d", -TRANSP_OUTLOG_FILE_LIFE, nowdt)
    For i = 1 To 5
        temp = logpath & "\TranspOut_" & Format$(dt, "mmdd") & ".log"
        If g_util.FileExists(temp) Then
            Kill temp
        End If
        dt = DateAdd("d", -1, dt)
    Next i

End Sub


Private Sub LogDataIntoTranspIn()
    Dim rs As New Recordset
    Dim sql As String
    Dim i, count As Integer
    Dim pid As String
    Dim labseq As String, result As String, pdate As String
    Dim deptid As String
    
    sql = "SELECT PATIENT_ID, PATRESULTSLABELSEQ as LABSEQ, RESULT_VALUE as RES, PERFORMDATE as PDATE, DEPT_ID FROM " & DBTABLENAME & " WHERE PATIENT_ID='" & acct & "'"
    If lookback_on Then
        sql = sql & " and convert(datetime,performdate)>=" & g_dbutil.SQL_DateTime(lookback_dt)
    End If
    rs.Open sql, g_cnADO
    count = rs.RecordCount
    rs.MoveFirst
    For i = 1 To count
        pid = rs(0)
        labseq = rs(1)
        result = g_dbutil.SQL_String(rs(2))
        pdate = rs(3)
        deptid = rs(4)
        
        Print #inlogfile, deptid, pid, lname, fname, labseq, result, pdate
        
        If Not rs.EOF Then rs.MoveNext
    Next i
    rs.Close
    Set rs = Nothing

End Sub

Private Sub QLegendCodes()
    Dim rs As New Recordset
    Dim sql As String
    Dim i As Integer
    
    'handle exception rn:  Rename these when found:  rnnon
    'handle exception npo: Rename these when found:  npoice, npomed
    'handle exception picc: Rename these when found:  piccln
    dprint "QLegendCodes"
'  select * from tc_data where patient_id=x and result_value like '%sa2%'
    For i = 1 To legnum
        sql = "select count(*) from " & DBTABLENAME & " where patient_id='" & acct & "' and result_value COLLATE SQL_Latin1_General_CP1_CS_AS like " & "'%" & dicleg(i).leg & "%'"
        If dicleg(i).leg = "Rn" Then
            sql = sql & " and result_value COLLATE SQL_Latin1_General_CP1_CS_AS not like '%Rnnon%'"
        ElseIf dicleg(i).leg = "Picc" Then
            sql = sql & " and result_value COLLATE SQL_Latin1_General_CP1_CS_AS not like '%Piccln%'"
        ElseIf dicleg(i).leg = "Npo" Then
            sql = sql & " and result_value COLLATE SQL_Latin1_General_CP1_CS_AS not like '%Npoice%'"
            sql = sql & " and result_value COLLATE SQL_Latin1_General_CP1_CS_AS not like '%Npomed%'"
        End If
        If lookback_on Then
            sql = sql & " and convert(datetime,performdate)>=" & g_dbutil.SQL_DateTime(lookback_dt)
        End If
        rs.Open sql, g_cnADO
        dicleg(i).count = rs(0)
        rs.Close
        
'        If dicleg(i).count > 0 Then
'        dprint "Legend code=" & dicleg(i).leg & " x " & dicleg(i).count
'        Select Case dicleg(i).leg
'            Case "Rnnon", "Npoice", "Npomed", "Piccln": RemoveLegFromLeg i
'        End Select
'        End If
    Next i
    Set rs = Nothing
'    dprint "end QLegendCodes"
End Sub
Private Sub QLabelSeqs()
    Dim rs As New Recordset
    Dim sql As String
    Dim i As Integer
    Dim count As Integer
    dprint "QLabelSeqs"
'  select * from tc_data where patient_id=x and patresultslabelseq='123456'
'    For i = 1 To labnum
'        sql = "select count(*) from " & DBTABLENAME & " where patient_id='" & acct & "' and patresultslabelseq=" & str(diclab(i).lab)
'        If lookback_on Then
'            sql = sql & " and convert(datetime,performdate)>=" & g_dbutil.SQL_DateTime(lookback_dt)
'        End If
'        rs.Open sql, g_cnADO
'        diclab(i).count = rs(0)
'        rs.Close
'    Next i
'    Set rs = Nothing
    For i = 1 To labnum
        diclab(i).count = 0
    Next i
    
    sql = "select patresultslabelseq,count(patresultslabelseq) from " & DBTABLENAME & " where patient_id='" & acct & "'"
    If lookback_on Then
        sql = sql & " and convert(datetime,performdate)>=" & g_dbutil.SQL_DateTime(lookback_dt)
    End If
    sql = sql & " group by patresultslabelseq"
    rs.Open sql, g_cnADO
    count = rs.RecordCount
    If count > 0 Then
        rs.MoveFirst
        For i = 1 To count
            MarkDicLab rs(0), rs(1)
            If Not rs.EOF Then rs.MoveNext
        Next i
    End If
    
'    dprint "end QLabelSeqs"
End Sub

Private Sub MarkDicLab(ls As Long, count As Integer)
    Dim i As Integer
    
    For i = 1 To labnum
        If ls = diclab(i).lab Then
            diclab(i).count = count
            Exit For
        End If
    Next i

End Sub
Private Sub QLegLabs()
    Dim rs As New Recordset
    Dim sql As String
    Dim i As Integer
    dprint "QLegLabs"
'  select * from tc_data where patient_id=x and result_value like '%sa2%' and patresultslabelseq='123456'
    For i = 1 To llnum
        sql = "select count(*) from " & DBTABLENAME & " where patient_id='" & acct & "' and result_value COLLATE SQL_Latin1_General_CP1_CS_AS like " & "'%" & dicll(i).leg & "%'"
        If dicll(i).leg = "Rn" Then
            sql = sql & " and result_value COLLATE SQL_Latin1_General_CP1_CS_AS not like '%Rnnon%'"
        End If
        sql = sql & " and patresultslabelseq=" & Str(dicll(i).lab)
        If lookback_on Then
            sql = sql & " and convert(datetime,performdate)>=" & g_dbutil.SQL_DateTime(lookback_dt)
        End If
        If dicll(i).validhrs > 0 Then
            sql = sql & " and convert(datetime,performdate)>= dateadd(hh," & -dicll(i).validhrs & "," & g_dbutil.SQL_DateTime(g_util.CDateEx(intime)) & ")"
            'dprint sql
            'dprint "  SQL appended for Lab=" & dicll(i).lab & "/Leg=" & dicll(i).leg & "/ValidHours=" & dicll(i).validhrs
        End If
        rs.Open sql, g_cnADO
        dicll(i).count = rs(0)
        rs.Close
        
'        If dicll(i).count > 0 Then
'        If dicll(i).leg = "Rnnon" And dicll(i).lab = 138614 Then
'            RemoveLegFromLL i
'        End If
'        End If

    Next i
    Set rs = Nothing
'    dprint "end QLegLabs"
End Sub

'Private Sub QAssess(makeTC As Boolean, inclVS As Boolean, onlyVS As Boolean)
'    Dim rs As New Recordset
'    Dim sql, sql2, lslist As String
'    Dim i, j As Integer
'    Dim count As Integer
'    Dim lotime As String
'    Dim hitime As String
'    Dim numdiv As Integer
'    Dim ranges() As division
'    Dim bucket_size As Integer
'    Dim sbucket As String
'    Dim b As Integer
'    Dim vs As String
'    Dim patresconditions As String
'    Dim pain1 As String
'
'    If inclVS Then
'        vs = " including VS"
'    Else
'        vs = " excluding VS"
'    End If
'    If onlyVS Then
'        vs = " ONLY VS"
'        inclVS = True
'    End If
'    dprint "QAssess" & vs
'    lslist = "(6913,6914,6915,6918,6922,6923,6925,6926,6928,6931,6933,6934,6937,6938,6940,6947,6953,"
'    lslist = lslist & "6956,6957,6958,6959,6971,6978,6979,6987,6989,12557,12558,14590,131317,131330,131579,131580,"
'    lslist = lslist & "5793,5794,5795,5796,5797,5798,5799,5800,5804,5805,5806,5836,5837,5845,5846,5847,"
'    lslist = lslist & "10041,11945,11946,12113,12114,12115,12116,12117,12118,12119,12120,"
'    lslist = lslist & "12121,12122,13219,13220,13221,13222,18816,22832,23829,23834,23835,23836,23837,23838,"
'    lslist = lslist & "23840,23841,23842,23843,23844,23845,23846,23847,23848,23849,26297,41406,41406,"
'    lslist = lslist & "41407,67119,67120,67121,67122,67127,67128,67129,67130,"
'    lslist = lslist & "67131,67132,67133,67134,67135,69970,70060,71622,71623,73525,73527,73528,73529,"
'    lslist = lslist & "77953,83766,83767,83768,83769,83770,83771,83772,83773,83774,83775,83776,83777,"
'    lslist = lslist & "83778,83779,83780,83781,83782,83783,83784,83785,83786,84000,84001,84002,84003,"
'    lslist = lslist & "84004,84005,84006,84007,84008,84009,84010,84011,84012,132161,"
'    lslist = lslist & "131526,131614,131622,131648,131649,131668,131669,131722,131809,"
'    lslist = lslist & "131810,131811,131812,131823,131829,131838,131839,131858,131869,131877,131878,"
'    lslist = lslist & "131914,131915,131917,131920,131922,131923,131924,131925,131926,131927,131970,"
'    lslist = lslist & "131971,132154,132155,132156,132157,132158,132159,132160,132166,132167,132168,"
'    lslist = lslist & "132169,132170,132171,132194,132255,132257,132409,132678,132679,134335,134336,134337,"
'    lslist = lslist & "134338,134339,134342,134355,134356,134357,134358,134359,134564,135052,"
'    lslist = lslist & "135301,135302,135303,135304,135305,135306,135307,135308,135780,135782,"
'    lslist = lslist & "140543,140545,140548,140550,140551,140552,140553,140554,140557,140560,140561,"
'    lslist = lslist & "140625,141361,141785,142638,142639,142640,142641,142642,142644,144036,144037)"
'
'    sql2 = ""
'    If inclVS Then
'        vs = "(131360,137759,141371,137762,137763,131342,9757,6880,131431,131033,47180,46936,46937,77934,137752,137707,138590)"
'        sql2 = sql2 & " or patresultslabelseq in " & vs
'    End If
'    sql2 = sql2 & " or (patresultslabelseq>=134435 and patresultslabelseq<=134443)"
'
'    'pain
'    pain1 = " or patresultslabelseq in (135072,77177,128187,128506,140471,140477,140483,140489,140495,140501,140507,140513,140519,140525,140531,141470)"
'    pain1 = pain1 & " or (patresultslabelseq>=66083 and patresultslabelseq<=66151)"
'    pain1 = pain1 & " or (patresultslabelseq>=134517 and patresultslabelseq<=134570)"
'    If Not IsICU Then
'        sql2 = sql2 & pain1
'    End If
'
'    If onlyVS Then
'        lslist = vs
'        sql2 = ""
'    End If
'
''    sql = "select distinct(performdate) from " & DBTABLENAME & " where patient_id='" & acct & "' and patresultslabelseq in " & lslist
''    rs.Open sql, g_cnADO
''    count = rs.RecordCount
''    rs.Close
'    patresconditions = "patresultslabelseq in " & lslist
'    patresconditions = patresconditions & sql2
'
'    sql = "select min(convert(datetime,performdate)) from " & DBTABLENAME & " where patient_id='" & acct & "' and (" & patresconditions & ")"
'    If lookback_on Then
'        sql = sql & " and convert(datetime,performdate)>=" & g_dbutil.SQL_DateTime(lookback_dt)
'    End If
'    rs.Open sql, g_cnADO
'    If IsNull(rs(0)) Then
'        lotime = ""
'    Else
'        lotime = rs(0)
'    End If
'    rs.Close
'
'    If lotime = "" Then
'        Set rs = Nothing
'        Exit Sub
'    End If
'
'    sql = "select max(convert(datetime,performdate)) from " & DBTABLENAME & " where patient_id='" & acct & "' and (" & patresconditions & ")"
'    If lookback_on Then
'        sql = sql & " and convert(datetime,performdate)>=" & g_dbutil.SQL_DateTime(lookback_dt)
'    End If
'    rs.Open sql, g_cnADO
'    If IsNull(rs(0)) Then
'        hitime = ""
'    Else
'        hitime = rs(0)
'    End If
'    rs.Close
'
'    If IsICU Then
'        bucket_size = N3Gbucket 'default is 5 mins; specify -n3gbucket=nn to change it.
'    Else
'        bucket_size = nonN3Gbucket 'default is 5 mins; specify -bucket=nn to change it.
'    End If
'    dprint "Bucket size=" & bucket_size & " mins"
'
'    numdiv = DateDiff("n", CDate(lotime), CDate(hitime)) \ bucket_size ' minute intervals
'    numdiv = numdiv + 1
'    ReDim ranges(0 To numdiv)
'    For i = 1 To numdiv
'        ranges(i).stime = DateAdd("n", (i - 1) * bucket_size, CDate(lotime))
'        ranges(i).etime = DateAdd("n", i * bucket_size, CDate(lotime))
'        ranges(i).exists = False
'        If i = 1 Or i = numdiv Then
'            If makeTC Then dprint i & ": " & ranges(i).stime
'        End If
'    Next i
'
'    If makeTC Then dprint "Assessment divisions=" & numdiv
'   'sql = "select distinct(convert(datetime,performdate)),patresultslabelseq from " & DBTABLENAME & " where patient_id='" & acct & "' and (" & patresconditions & ")"
'    sql = "select convert(datetime,performdate),patresultslabelseq from " & DBTABLENAME & " where patient_id='" & acct & "' and (" & patresconditions & ")"
'    If lookback_on Then
'        sql = sql & " and convert(datetime,performdate)>=" & g_dbutil.SQL_DateTime(lookback_dt)
'    End If
'    rs.Open sql, g_cnADO
'    count = rs.RecordCount
'    rs.MoveFirst
'    For i = 1 To count
'        For j = 1 To numdiv
'            'Existence can be determined more efficiently with some modulo math, but it will be harder to read
'            ranges(j).exists = (ranges(j).stime <= rs(0) And ranges(j).etime > rs(0)) Or ranges(j).exists
'        Next j
'
''            sbucket = ""
''            For j = 1 To numdiv
''                If ranges(j).exists Then
''                    sbucket = sbucket & "," & j
''                End If
''            Next j
''            If makeTC Then dprint rs(1) & ": " & rs(0) & " Buckets=" & sbucket
'
'        If Not rs.EOF Then rs.MoveNext
'    Next i
'
''    For j = 1 To numdiv
''        If ranges(j).exists Then
''        vs = "VS bucket " & str(j) & ":"
''        rs.MoveFirst
''        For i = 1 To count
''            If ranges(j).stime <= rs(0) And ranges(j).etime > rs(0) Then
''            Select Case rs(1)
''                Case 131360, 137759, 141371, 137762, 137763, 131342, 9757, 6880, 131431, 131033, 47180, 46936, 46937, 77934, 137752, 137707, 138590:
''                    vs = vs & str(rs(1)) & ","
''            End Select
''            End If
''            If Not rs.EOF Then rs.MoveNext
''        Next i
''        If inclVS Then dprint vs
''        End If
''    Next j
'    rs.Close
'
'    count = 0
'    For j = 1 To numdiv
'        If ranges(j).exists Then
'            count = count + 1
'        End If
'    Next j
'
'    If count >= 58 Then
'        i = 17
'    ElseIf count >= 24 Then
'        i = 16
'    ElseIf count >= 12 Then
'        i = 15
'    ElseIf count >= 5 Then
'        i = 14
'    Else
'        i = 0
'    End If
'
'    If inclVS Then
'        dprint acct & " Assessment count=" & count & "=>" & i
'    Else
'        dprint acct & " Assessment count without VS=" & count & "=>" & i
'    End If
'
'' too slow:
''    dprint "Assessment 5-minute divisions=" & numdiv
''    count = 0
''    For i = 1 To numdiv
''        sql = "select count(*) from " & DBTABLENAME & " where patient_id='" & acct & "' and patresultslabelseq in " & lslist
''        sql = sql & " and convert(datetime,performdate) >=" & g_dbutil.SQL_DateTime(ranges(i).stime)
''        sql = sql & " and convert(datetime,performdate) <" & g_dbutil.SQL_DateTime(ranges(i).etime)
''        rs.Open sql, g_cnADO
''        If CInt(rs(0)) > 0 Then count = count + 1
''        rs.Close
''    Next i
''    dprint "Assessment count=" & count
'
'    If makeTC Then
'        If count >= 58 Then
'            inds(17).checked = True
'        ElseIf count >= 24 Then
'            inds(16).checked = True
'        ElseIf count >= 12 Then
'            inds(15).checked = True
'        ElseIf count >= 5 Then
'            inds(14).checked = True
'        End If
'    End If
'    Set rs = Nothing
'    Erase ranges
'
'End Sub
Private Function CountUniqueLabs(ls As Long) As Integer
    Dim rs As New Recordset
    Dim sql, lslist As String
    Dim i As Integer
    Dim count As Integer

    sql = "select distinct(performdate) from " & DBTABLENAME & " where patient_id='" & acct & "' and patresultslabelseq=" & Str(ls)
    If lookback_on Then
        sql = sql & " and convert(datetime,performdate)>=" & g_dbutil.SQL_DateTime(lookback_dt)
    End If
    rs.Open sql, g_cnADO
    CountUniqueLabs = rs.RecordCount
    rs.Close
    Set rs = Nothing
End Function

Private Sub RemoveLegFromLeg(ileg As Integer)
    Dim rs As New Recordset
    Dim sql As String
    Dim i, count As Integer
    Dim s As String
    
'  select * from tc_data where patient_id=x and result_value like '%sa2%'
    sql = "select * from " & DBTABLENAME & " where patient_id='" & acct & "' and result_value COLLATE SQL_Latin1_General_CP1_CS_AS like " & "'%" & dicleg(ileg).leg & "%'"
    If lookback_on Then
        sql = sql & " and convert(datetime,performdate)>=" & g_dbutil.SQL_DateTime(lookback_dt)
    End If
    rs.Open sql, g_cnADO, adOpenDynamic, adLockOptimistic
    count = rs.RecordCount
    If count > 0 Then
    rs.MoveFirst
    For i = 1 To count
        dprint "RemoveLegFromLeg " & dicleg(ileg).leg & " record count=" & rs.RecordCount
        s = rs("result_value")
        dprint "Replacing result_value=" & s
        s = Replace(s, dicleg(ileg).leg, UCase$(dicleg(ileg).leg))
        dprint "Resulting result_value=" & s
        rs("result_value") = s
        rs.Update
        If Not rs.EOF Then rs.MoveNext
    Next i
    End If
    rs.Close
    Set rs = Nothing
End Sub
Private Sub RemoveLegFromLL(ill As Integer)
    Dim rs As New Recordset
    Dim sql As String
    Dim i, count As Integer
    Dim s As String
    
'  select * from tc_data where patient_id=x and result_value like '%sa2%'
    sql = "select * from " & DBTABLENAME & " where patient_id='" & acct & "' and result_value COLLATE SQL_Latin1_General_CP1_CS_AS like " & "'%" & dicll(ill).leg & "%'"
    sql = sql & " and patresultslabelseq=" & Str(dicll(ill).lab)
    If lookback_on Then
        sql = sql & " and convert(datetime,performdate)>=" & g_dbutil.SQL_DateTime(lookback_dt)
    End If
    rs.Open sql, g_cnADO, adOpenDynamic, adLockOptimistic
    count = rs.RecordCount
    If count > 0 Then
    dprint "count=" & count
    rs.MoveFirst
    For i = 1 To count
        dprint "RemoveLegFrom L.L. " & dicll(ill).leg & "+" & dicll(ill).lab & " record count=" & rs.RecordCount
        s = rs("result_value")
        dprint "Replacing result_value=" & s
        s = Replace(s, dicll(ill).leg, UCase$(dicll(ill).leg))
        dprint "Resulting result_value=" & s
        rs("result_value") = s
        rs.Update
        If Not rs.EOF Then rs.MoveNext
    Next i
    End If
    rs.Close
    Set rs = Nothing
End Sub

Private Sub dprint(s As String)
    If dbugon Then
        Print #dbugfile, s
    End If
End Sub

Private Sub AddPersistentDataToDB()
    Dim rs As New Recordset
    Dim rs2 As New Recordset
    Dim sql As String
    Dim fn As String
    Dim f As Integer
    Dim buf As String
    Dim hours As Integer
    Dim items() As String
    Dim a As String
    Dim add_to_this_acct As Boolean
    Dim is_new_acct As Boolean
    Dim count As Integer

    fn = App.Path & "\PERSIST.DAT"
    If Not g_util.FileExists(fn) Then
        dprint "existing add persist - no file"
        Set rs = Nothing
        Set rs2 = Nothing
        Exit Sub
    End If
    
    count = 0
    f = FreeFile
    
'    sql = "select top(1) patient_id,dept_id,last_name,first_name,result_value,patresultslabelseq,performdate from " & DBTABLENAME
    rs.Open DBTABLENAME, g_cnADO, adOpenDynamic, adLockOptimistic
    
    Open fn For Input As #f
    a = ""
    While Not EOF(f)
        Line Input #f, buf
        items() = Split(buf, ",") 'PERSIST.DAT is comma-delimited
        If a = items(0) Then 'same acct number
            is_new_acct = False
        Else
            a = items(0)
            is_new_acct = True
        End If
            
        If is_new_acct Then 'minimize io by only searching for new acct numbers
            sql = "select patient_id from " & DBTABLENAME & " where patient_id='" & items(0) & "' and dept_id='" & items(1) & "'"
            rs2.Open sql, g_cnADO
            add_to_this_acct = (rs2.RecordCount > 0)
            rs2.Close
        End If
            
        If add_to_this_acct Then 'acct num with unit exists in data
            If items(5) = "141184" Then 'check if this Outcomes indicator with this performdate is in the db.
                sql = "delete from " & DBTABLENAME & " where patient_id='" & items(0) & "' and patresultslabelseq=141184"
                If items(4) = "" Then
                    sql = sql & "  and result_value is null"
                Else
                    sql = sql & "  and result_value='" & items(4) & "'"
                End If
                g_cnADO.Execute sql
            Else
                count = count + 1
                rs.AddNew
                rs("PATIENT_ID") = items(0) 'acct number
                rs("DEPT_ID") = items(1) 'unit
                rs("LAST_NAME") = items(2) 'last
                rs("FIRST_NAME") = items(3) 'first
                rs("RESULT_VALUE") = items(4) 'result value (legend codes)
                rs("PATRESULTSLABELSEQ") = g_dbutil.SQL_Number(items(5)) 'label seq
                rs("PERFORMDATE") = items(6) 'perform date:string
                rs.Update
            End If
        End If
    Wend
    dprint "Added persist records into table from file=" & count
        
    Close #f
    rs.Close
    Kill fn 'we don't need this file anymore because the data is now in the db
    Set rs = Nothing
    Set rs2 = Nothing
    
'   Open PERSIST.DAT file
'   Open DB for write
'   For each line in file
'       Write to DB
'   Close DB
'   Kill PERSIST.DAT

End Sub

Private Sub SaveLatestPersistentDataToFile()
    Dim rs As New Recordset
    Dim sql As String
    Dim fn As String
    Dim f As Integer
    Dim i, count As Integer
    Dim s As String
    Dim res As String
    
    dprint "SaveLatestPersistentDataToFile"
    fn = App.Path & "\PERSIST.DAT"
    dprint fn
    If g_util.FileExists(fn) Then Kill fn 'should be a new file
    dprint "Post-kill fn"
    f = FreeFile
    dprint "Post freefile"
    Open fn For Append As #f
    dprint "Post open for append"
    sql = "select patient_id,dept_id,last_name,first_name,result_value,patresultslabelseq,performdate from " & DBTABLENAME
    sql = sql & " where patresultslabelseq in (141363,141364,141383,141384,141379,141380,141375,141376,141184)"
    sql = sql & " and datediff(hh,performdate," & g_dbutil.SQL_DateTime(nowdt) & ") <= 24"
    If lookback_on Then
        sql = sql & " and convert(datetime,performdate)>=" & g_dbutil.SQL_DateTime(lookback_dt)
    End If
    sql = sql & " order by patient_id"
    dprint sql
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        count = rs.RecordCount
        rs.MoveFirst
        For i = 1 To count
            If IsNull(rs(4)) Then
                res = ""
            Else
                res = rs(4)
            End If
            s = rs(0) & "," & rs(1) & "," & rs(2) & "," & rs(3) & "," & res & "," & rs(5) & "," & rs(6)
            Print #f, s
            If Not rs.EOF Then rs.MoveNext
        Next i
    End If
    dprint "Saved persist data to file=" & count
    rs.Close
    Set rs = Nothing
    Close #f
'   Open PERSIST.DAT file for append
'   query DB for all label seqs that are persistent with perform_date <= 24 hours ago.
'   write each of these records to PERSIST.DAT
'   Close PERSIST.DAT
End Sub

Private Function QGreatestLabVal(lab As Long) As Integer
    Dim rs As New Recordset
    Dim sql As String
    Dim i, count As Integer
    Dim hi As Integer
    Dim v As Integer
    dprint "QGreatestLabVal"
    If FindLab(lab) Then
        hi = 0
    '  select * from tc_data where patient_id=x and patresultslabelseq=123456
        sql = "select result_value from " & DBTABLENAME & " where patient_id='" & acct & "' and patresultslabelseq=" & Str(lab)
        If lookback_on Then
            sql = sql & " and convert(datetime,performdate)>=" & g_dbutil.SQL_DateTime(lookback_dt)
        End If
        rs.Open sql, g_cnADO
        count = rs.RecordCount
        rs.MoveFirst
        For i = 1 To count
            If IsNumeric(rs(0)) Then
                v = CInt(rs(0))
                If v > hi Then
                    hi = v
                End If
            End If
            If Not rs.EOF Then rs.MoveNext
        Next i
        rs.Close
    Else
        hi = 0
    End If
    
    Set rs = Nothing
    QGreatestLabVal = hi

End Function
Private Function QWound() As Integer
    Dim rs As New Recordset
    Dim sql As String
    Dim i, count As Integer
    Dim hi As Integer
    Dim v As Integer
    dprint "QWound"
    hi = 0
'  select * from tc_data where patient_id=x and patresultslabelseq=123456
    sql = "select result_value from " & DBTABLENAME & " where patient_id='" & acct & "' and patresultslabelseq in "
    sql = sql & "(47138, 47137, 47134, 47133, 47136, 47135, 71863, 71844, 47132, 47131)"
    If lookback_on Then
        sql = sql & " and convert(datetime,performdate)>=" & g_dbutil.SQL_DateTime(lookback_dt)
    End If
    rs.Open sql, g_cnADO
    count = rs.RecordCount
    rs.MoveFirst
    For i = 1 To count
        If IsNumeric(rs(0)) Then
            v = CInt(rs(0))
            If v > hi Then
                hi = v
            End If
        End If
        If Not rs.EOF Then rs.MoveNext
    Next i
    rs.Close
    Set rs = Nothing
    QWound = hi
    

End Function
Private Sub QSheath20()
    Dim rs As New Recordset
    Dim sql As String
    Dim i, count As Integer
    Dim sh, shdc As String
    Dim dcok, shok As Boolean
    dprint "QSheath20"
'  select * from tc_data where patient_id=x and patresultslabelseq=123456
    
    sh = ""
    shdc = ""
    dcok = False
    shok = False

'47162, 47161, 47158, 47157, 47160, 47159, 71862, 71843, 47156, 47155
'+Shthdc
    sql = "select max(convert(datetime,performdate)) from " & DBTABLENAME & " where patient_id='" & acct & "' and patresultslabelseq in "
    sql = sql & "(47162, 47161, 47158, 47157, 47160, 47159, 71862, 71843, 47156, 47155) and "
    sql = sql & " result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Shthdc%'"
    If lookback_on Then
        sql = sql & " and convert(datetime,performdate)>=" & g_dbutil.SQL_DateTime(lookback_dt)
    End If
    rs.Open sql, g_cnADO
    If Not IsNull(rs(0)) Then
        shdc = rs(0)
        dcok = True
    End If
    dprint "shdc=" & shdc
    rs.Close

g_dbutil.SQL_DateTime (g_util.CDateEx(intime))

    sql = "select max(convert(datetime,performdate)) from " & DBTABLENAME & " where patient_id='" & acct & "' and patresultslabelseq in "
    sql = sql & "(47072,47080,47088,47096,47071,47079,47087,47095,47066,47074,47082,47090,47065,47073,47081,47089) "
    sql = sql & "and convert(datetime,performdate)>" & g_dbutil.SQL_DateTime(g_util.CDateEx(intime))
    If lookback_on Then
        sql = sql & " and convert(datetime,performdate)>=" & g_dbutil.SQL_DateTime(lookback_dt)
    End If
    dprint sql
    rs.Open sql, g_cnADO
    sh = 0
    If Not IsNull(rs(0)) Then
        sh = rs(0)
        shok = True
    End If
    rs.Close
    Set rs = Nothing
    dprint "sh=" & sh
    
    If dcok Or shok Then
    If shdc <> "" And sh <> "" Then
        If shdc >= sh Then
            inds(19).checked = True
            dprint "19: Shthdc=" & shdc & " sh=" & sh
        ElseIf sh > shdc Then
            inds(3).checked = True
            inds(5).checked = True
            inds(11).checked = True
            inds(16).checked = True
            inds(20).checked = True
            inds(110).checked = True   'make 109 and 110 mutually exclusive
            dprint "20: Shthdc=" & shdc & " sh=" & sh
        End If
    ElseIf shdc <> "" And sh = "" Then
            inds(19).checked = True
            dprint "19: Shthdc=" & shdc & " sh=" & sh
    ElseIf shdc = "" And sh <> "" Then
            inds(3).checked = True
            inds(5).checked = True
            inds(11).checked = True
            inds(16).checked = True
            inds(20).checked = True
            inds(110).checked = True
            dprint "20: Shthdc=" & shdc & " sh=" & sh
    End If
    End If 'ok
End Sub

Private Function QLatestDateTimeOfLab(lab As Long) As String 'For Procedures
    Dim rs As New Recordset
    Dim sql As String
    Dim i, count As Integer
    Dim hires As String
    Dim d As Date
    Dim hid As Date
    Dim res As String
    dprint "QLatestDateTimeOfLab"
    hires = ""
    hid = CDate("1/1/70")
    If FindLab(lab) Then
        dprint "Labelseq found=" & Str(lab)
    '  select * from tc_data where patient_id=x and patresultslabelseq=123456
        sql = "select result_value from " & DBTABLENAME & " where patient_id='" & acct & "' and patresultslabelseq=" & Str(lab)
        If lookback_on Then
            sql = sql & " and convert(datetime,performdate)>=" & g_dbutil.SQL_DateTime(lookback_dt)
        End If
        rs.Open sql, g_cnADO
        count = rs.RecordCount
        For i = 1 To count
            dprint "i=" & Str(i)
            If IsNull(rs(0)) Then
                dprint "rs(0) IS NULL"
            Else
            dprint "rs(0) is not null"
            res = g_dbutil.DBToString(rs(0))
            dprint "after res=rs(0)"
            If IsDate(rs(0)) Then
                dprint "rs(0)=" & res
                d = CDate(res)
                If d <= CDate("1/1/70") Then d = CDate("1/1/90")
                dprint "d=" & d
                If d > hid Then
                    hid = d
                    hires = res
                End If
            End If
            End If
            If Not rs.EOF Then rs.MoveNext
        Next i
        rs.Close
    End If
    Set rs = Nothing
    
    QLatestDateTimeOfLab = hires
    dprint "end QLatestDateTimeOfLab= " & hires

End Function
Private Function QFallr5(leg As String, lab As Long) As Integer
    Dim rs As New Recordset
    Dim sql As String
    Dim i, count As Integer
    Dim hi As Integer
    Dim v As Integer
    Dim p As Integer
    Dim vfall As String
    dprint "QFallr5"
    hi = 0
'  select * from tc_data where patient_id=x and patresultslabelseq=123456
    sql = "select result_value from " & DBTABLENAME & " where patient_id='" & acct & "' and patresultslabelseq=" & Str(lab)
    sql = sql & " and result_value COLLATE SQL_Latin1_General_CP1_CS_AS like " & "'%" & leg & "%'"
    If lookback_on Then
        sql = sql & " and convert(datetime,performdate)>=" & g_dbutil.SQL_DateTime(lookback_dt)
    End If
    rs.Open sql, g_cnADO
    count = rs.RecordCount
    rs.MoveFirst
    For i = 1 To count
        p = InStr(rs(0), leg)
        vfall = Mid$(rs(0), 1, p - 1)
        If IsNumeric(vfall) Then
            v = CInt(vfall)
            If v > hi Then
                hi = v
            End If
        ElseIf 5 > hi Then  'just take fallr5 to represent a value of 5
            hi = 5
        End If
        If Not rs.EOF Then rs.MoveNext
    Next i
    rs.Close
    Set rs = Nothing
    
    QFallr5 = hi

End Function

Private Sub QWound19()
    Dim rs As New Recordset
    Dim sql As String
    dprint "QWound19"
'  select * from tc_data where patient_id=x and patresultslabelseq in (123456,...)
    sql = "select count(*) from " & DBTABLENAME & " where patient_id='" & acct & "' and patresultslabelseq in "
    sql = sql & "(137067,137068,137069,137070,137071,137549,137545,137609,137557,137553,137565,137561,137072,137073,"
    sql = sql & "137074,137513,137529,137541,137537,137075,140043,137577,137593,137589,137585,140218,137076,137077,"
    sql = sql & "137078,137079,137080,137081,137625,137621,137082,137083,137617,137613,137601,137597,139839,139814,"
    sql = sql & "139714,139689,139664,137084,137085,137086,137087,137517,137525,137521,140243,140293,137509,140268,"
    sql = sql & "140318,139939,139914,139889,139864,140039,140014,140093,140068,140143,140118,140193,140168,137633,"
    sql = sql & "137629,137088,137089,139764,139789,140368,140343,137533,137569,137573,139964,139989,137090,137091,"
    sql = sql & "137581,139739,137092,137093,137094,137095,137096,137097,137098,137099,137100,137101,138997,139022,"
    sql = sql & "139047,139072,139097,139122,140418,140393,140468,140443,137605)"
    If lookback_on Then
        sql = sql & " and convert(datetime,performdate)>=" & g_dbutil.SQL_DateTime(lookback_dt)
    End If
    rs.Open sql, g_cnADO
    If rs.RecordCount > 0 Then
        If rs(0) > 0 Then inds(19).checked = True
    End If
    rs.Close
    Set rs = Nothing

End Sub

Private Sub QDressing19()
    Dim rs As New Recordset
    Dim sql As String
    dprint "QDressing19"
    sql = "select count(*) from " & DBTABLENAME & " where patient_id='" & acct & "' and (patresultslabelseq in "
    sql = sql & "(136508,136529,136665,136688,136711,"
    sql = sql & "136734,136757,136780,136803,136826,"
    sql = sql & "136849,136872,136895,136918,136941,"
    sql = sql & "136964,136987,137010,138980,139005,"
    sql = sql & "139030,139055,139080,139105,139647,"
    sql = sql & "139672,139697,139722,139747,139772,"
    sql = sql & "139797,139822,139847,139872,139897,"
    sql = sql & "139922,139947,139972,139997,140022,"
    sql = sql & "140051,140076,140101,140126,140151,"
    sql = sql & "140176,140201,140226,140251,140276,"
    sql = sql & "140301,140326,140351,140376,140401,140426,140451) or "
    sql = sql & "(patresultslabelseq>=133530 and patresultslabelseq<=133584))"
    If lookback_on Then
        sql = sql & " and convert(datetime,performdate)>=" & g_dbutil.SQL_DateTime(lookback_dt)
    End If
    rs.Open sql, g_cnADO
    If rs.RecordCount > 0 Then
        If rs(0) > 0 Then inds(19).checked = True
    End If
    rs.Close
    Set rs = Nothing
'    dprint "end QDressing19"
End Sub
' 133530-133584
'136508,136529,136665,136688,136711,
'136734,136757,136780,136803,136826,
'136849,136872,136895,136918,136941,
'136964,136987,137010,138980,139005,
'139030,139055,139080,139105,139647,
'139672,139697,139722,139747,139772,
'139797,139822,139847,139872,139897,
'139922,139947,139972,139997,140022,
'140051,140076,140101,140126,140151,
'140176,140201,140226,140251,140276,
'140301,140326,140351,140376,140401,
'140426,140451

Private Sub RestoreLegendCodes()
    Dim rs As New Recordset
    Dim sql As String
    Dim i, count As Integer
    Dim s As String
    Dim w(0 To 4) As String
    Dim iw As Integer
    dprint "RestoreLegendCodes"

    w(1) = "RNNON"
    w(2) = "NPOICE"
    w(3) = "NPOMED"
    w(4) = "PICCLN"
    ' "RNNON", "NPOICE", "NPOMED", "PICCLN"

    For iw = 1 To 4
    '  select * from tc_data where patient_id=x and result_value like '%RNNON%'
        sql = "select * from " & DBTABLENAME & " where result_value COLLATE SQL_Latin1_General_CP1_CS_AS like " & "'%" & w(iw) & "%'"
        If lookback_on Then
            sql = sql & " and convert(datetime,performdate)>=" & g_dbutil.SQL_DateTime(lookback_dt)
        End If
        rs.Open sql, g_cnADO, adOpenDynamic, adLockOptimistic
        count = rs.RecordCount
        If count > 0 Then
            rs.MoveFirst
            For i = 1 To count
                dprint "Restoring legend code=" & w(iw)
                s = rs("RESULT_VALUE")
                dprint "Restoring from..." & s
                s = Replace(s, w(iw), Mid$(w(iw), 1, 1) & LCase$(Mid$(w(iw), 2, Len(w(iw)) - 1)))
                dprint "Resulting in..." & s
                rs("RESULT_VALUE") = s
                rs.Update
                If Not rs.EOF Then rs.MoveNext
            Next i
        End If
        rs.Close
    Next iw
    Set rs = Nothing

End Sub

Private Sub SetTxArea()
    dprint "SetTxArea"
    txarea = ""
    If unit = "N3G" Then  'Neuro pt if 10084, 131314
        If FindLab(10084) Or FindLab(131314) Then
            txarea = "Neuro Pt"
        Else
            txarea = "Other"
        End If
    ElseIf unit = "N4G" Then 'Joints in Motion 140562, 85163, 85162, 140563, 85166, 85165, 138021, 138025
        If FindLab(140562) Or FindLab(140563) Or FindLab(85162) Or FindLab(85163) Or FindLab(85165) Or _
           FindLab(85166) Or FindLab(138021) Or FindLab(138025) Then
            txarea = "Joints in Motion"
        Else
            txarea = "Other"
        End If
    ElseIf unit = "S5E" Then 'Renal Pt 6426, 132591, 132653, -137692 Dialis
        If FindLab(6426) Or FindLab(132591) Or FindLab(132653) Or FindLL("Dialis", 137692) Then
            txarea = "Renal Pt"
        Else
            txarea = "Other"
        End If
    ElseIf unit = "S1B" Then '70060, 131809, 131810, 131811, 131812
        'If FindLab(70060) Or FindLab(131809) Or FindLab(131810) Or FindLab(131811) Or FindLab(131812) Then
        If FindLab(169341) Then
            txarea = "Stroke Pts"
        Else
            txarea = "Other"
        End If
    ElseIf unit = "C3A" Or unit = "C3B" Then
        If FindLL("Cvor", 131815) Or FindLL("Surgry", 131815) Then
            txarea = "Surgical cardiac"
        Else
            txarea = "Other"
        End If
    ElseIf unit = "S2E" Or unit = "S3E" Then
        If FindLab(47065) Or FindLab(47066) Or FindLab(47071) Or FindLab(47072) Or FindLab(47073) Or FindLab(47074) Or _
           FindLab(47079) Or FindLab(47080) Or FindLab(47081) Or FindLab(47082) Or FindLab(47087) Or FindLab(47088) Or _
           FindLab(47089) Or FindLab(47090) Or FindLab(47095) Or FindLab(47096) Then
            txarea = "Cath with Sheath"
        Else
            txarea = "Other"
        End If
    ElseIf unit = "N6G" Then
        Select Case room
            Case "6710", "6711", "6712", "6713", "6717", "6718", "6719", "6720":
                txarea = "BOP"  'NOTE: THERE IS A SCRIPT TO SET BOP TO DEFAULTS.  MUST BE UPDATED IF ROOMS CHANGE
        End Select
    End If
    dprint "Tx area=" & txarea
End Sub
'Private Type ind_weight
'    ind_num As Integer
'    pweight As Integer
'    cweight As Integer
'End Type
'Private Type pat_type
'    ptype As Integer
'    hival As Integer
'End Type
'Private Type cmp_type
'    ctype As Integer
'    hival As Integer
'End Type

Private Sub CheckScore(ptype As Integer, ctype As Integer)
    Dim pscore As Integer
    Dim cscore As Integer
    Dim i As Integer
    
    pscore = 0
    cscore = 0

    For i = 1 To MAX_INDICATORS
        If inds(i).checked Then
            pscore = pscore + ind_weights(i).pweight
            cscore = cscore + ind_weights(i).cweight
        End If
    Next i
    dprint "Score=(" & pscore & "," & cscore & ")"
    For i = MAX_TYPES To 1 Step -1
        If pscore <= pat_types(i) Then
            ptype = i
        End If
        If cscore <= cmp_types(i) Then
            ctype = i
        End If
    Next i
    dprint "Type=(" & ptype & "," & ctype & ")"

End Sub
Private Sub InitTyping()
    Dim rs As New Recordset
    Dim sql As String
    Dim i As Integer

    sql = "select * from INDICATOR_DEFINITION where methodology_id=15 order by indicator_number"
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        For i = 1 To MAX_INDICATORS
            ind_weights(i).pweight = g_dbutil.DBToInteger(rs("WEIGHT"))
            ind_weights(i).cweight = g_dbutil.DBToInteger(rs("COMPLEXITY_WEIGHT"))
            If Not rs.EOF Then
                rs.MoveNext
            Else
                i = MAX_INDICATORS
            End If
        Next i
    End If
    rs.Close
    sql = "select * from PATIENT_TYPE where methodology_id=15 order by patient_type"
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        For i = 1 To MAX_TYPES
            pat_types(i) = g_dbutil.DBToLong(rs("POINTS_HIVAL"))
            If Not rs.EOF Then rs.MoveNext
        Next i
    End If
    rs.Close
    sql = "select * from COMPLEXITY_TYPE where methodology_id=15 order by complexity_type"
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        For i = 1 To MAX_TYPES
            cmp_types(i) = g_dbutil.DBToLong(rs("POINTS_HIVAL"))
            If Not rs.EOF Then rs.MoveNext
        Next i
    End If
    rs.Close
    
    Set rs = Nothing
End Sub


Private Function LatestClassIsDefault(definds As String) As Boolean
    Dim rs As New Recordset
    Dim sql As String
    
'select ce1.classification_event_id,
'       ce1.patient_type,
'       ce1.complexity_type,
'       ce1.classification_datetime,
'       ce1.datetime_in from classification_event ce1
'inner join encounter e1 on (e1.encounter_id=ce1.encounter_id)
'where e1.acct_number='0000184339150' and
'ce1.classification_datetime in
'(select max(ce2.classification_datetime) from classification_event ce2 inner join
'encounter e2 on (e2.encounter_id=ce2.encounter_id)
'where e2.acct_number='0000184339150')
    LatestClassIsDefault = False
    
    sql = "select ce1.classified_by_id,ce1.classification_event_id,ce1.datetime_in from classification_event ce1"
    sql = sql & " inner join encounter e1 on (e1.encounter_id=ce1.encounter_id)"
    sql = sql & " where e1.acct_number='" & acct & "' and"
    sql = sql & " ce1.classification_datetime in"
    sql = sql & " (select max(ce2.classification_datetime) from classification_event ce2 inner join"
    sql = sql & " encounter e2 on (e2.encounter_id=ce2.encounter_id)"
    sql = sql & " where e2.acct_number='" & acct & "')"
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
    If rs.RecordCount > 0 Then
        rs.MoveFirst
        If rs(0) = -2 Then ' default class
            LatestClassIsDefault = True
            If classtm = "0700" Then
                GetDefaultInds rs(1), definds
            End If
        End If
    End If
    End If
    rs.Close
    Set rs = Nothing

End Function

Private Sub GetDefaultInds(eventid As Long, definds As String)
    Dim rs As New Recordset
    Dim sql As String
    Dim i As Integer
    Dim count As Integer
    Dim di(0 To MAX_INDS) As Boolean
    
    dprint "GetDefaultInds"
    definds = ""
    sql = "select * from indicator_answer where classification_event_id=" & Str(eventid) & " order by indicator_number"
    dprint sql
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
    If rs.RecordCount > 0 Then
        count = rs.RecordCount
        rs.MoveFirst
        For i = 1 To count
            di(rs("INDICATOR_NUMBER")) = True
            If Not rs.EOF Then rs.MoveNext
        Next i
    End If
    End If
    rs.Close
    Set rs = Nothing
    
    For i = 1 To MAX_INDS
        If di(i) Then
            definds = definds & "Y"
        Else
            definds = definds & "N"
        End If
    Next i
    dprint "END GetDefaultInds"

End Sub

Private Function IsICU() As Boolean
    IsICU = (unit = "N3G" Or unit = "C3A" Or unit = "C3B" Or unit = "C2A" Or unit = "C2B")
End Function

Private Function IsN4G() As Boolean
    IsN4G = (unit = "N4G")
End Function
Private Function IsCHF() As Boolean
    IsCHF = (unit = "S3D")
End Function
Private Function IsTestTCUnit() As Boolean
    IsTestTCUnit = (unit = "N3G") Or (unit = "N4G") Or (unit = "S5E") Or (unit = "S5D") Or (unit = "S1B")
End Function


'Private Sub QLikeAssess()
'    Dim rs As New Recordset
'    Dim sql, sql2, lslist(0 To 10) As String
'    Dim i, j, k As Integer
'    Dim count(0 To 10) As Integer
'    Dim lotime(0 To 10) As String
'    Dim hitime(0 To 10) As String
'    Dim numdiv As Integer
'    Dim ranges() As division
'    Dim bucket_size As Integer
'    Dim sbucket As String
'    Dim b As Integer
'    Dim vs As String
'    Dim patresconditions As String
'    Dim pain1 As String
'    Dim countsource As Integer
'    Dim hicount As Integer
'    Dim rcount As Integer
'    Dim cat As String
'
'    dprint "QLikeAssess"
'
'    'Neuro
'    k = 1
'    lslist(k) = "(131668, 131669, 69970, 67119, 67120, 67121, 67122, 67127, 67130, 67133, 67129, 67132, 67135, 67128, 67131, 67134,"
'    lslist(k) = lslist(k) & "70060, 131809, 131810, 131811, 131812, 141361, 131823, 131858, 144036, 144037, 131829, 131869,"
'    lslist(k) = lslist(k) & "12113, 12114, 12115, 12116, 12117, 12118, 12119, 12120, 12121, 12122)"
'    'Cardiac
'    k = 2
'    lslist(k) = "(131914, 131915, 131923, 131922, 131927, 5837, 5836, 135780, 135782, 135301, 5847, 5846, 135303, 11946,11945, 135305, "
'    lslist(k) = lslist(k) & "5845, 41406, 135307, 41407, 41406, 131926, 131925, 131924, 140543, 131526, 131917, 131920, 26297,"
'    lslist(k) = lslist(k) & "134339, 134338, 134337, 134335, 134336, 140554, 140561,134359, 134357, 134358, 134355, 134356, 140553, 140560,"
'    lslist(k) = lslist(k) & "73527, 140550, 23835, 23834, 140557, 23837, 23836,73528, 140551, 23829, 23838, 23841, 23840,"
'    lslist(k) = lslist(k) & "132169, 132155, 132154, 135302, 132159, 132158, 135304, 132161, 132160, 135306, 132157, 132156, 135308,"
'    lslist(k) = lslist(k) & "71623, 71622, 132168, 132167, 132166, 140545, 131360, 137759, 141371, 137762, 137763, 131342, 9757,"
'    lslist(k) = lslist(k) & "131033, 47180, 46936, 46937, 77934, 137752, 137707, 131528)"
'    'Pulmonary
'    k = 3
'    lslist(k) = "(131838, 131839, 10041, 132194, 5804, 5805, 5806, 5793, 5794, 5795, 5796, 5797, 5798, 5799, 5800,"
'    lslist(k) = lslist(k) & "13219, 13220, 13221, 13222, 84000, 84001, 84002, 84003, 83767, 83766, 83769, 83768,"
'    lslist(k) = lslist(k) & "83770, 83771, 83772, 83773, 83774, 83775, 83776, 83777, 83778, 83779, 83780, 83781,"
'    lslist(k) = lslist(k) & "83782, 83783, 83784, 83785, 83786, 84004, 84005, 84006, 84007, 84008, 84009, 84010, 84011, 84012,"
'    lslist(k) = lslist(k) & "134342, 6880, 131431, 138590)"
''    'Fluids
''    k = 4
''    lslist(k) = "(132170, 132171,141785,131722, 131648, 131649,140625,22832,142642, 142644, 142639, 142638, 142641, 142640,"
''    lslist(k) = lslist(k) & "6913, 6928, 6925, 6914, 131317, 6915, 6918, 131330, 12558, 14590, 6987, 6922, 6923, 131580, 6926, 6978,"
''    lslist(k) = lslist(k) & "6931, 6933, 6934, 6953, 6956, 6957, 6958, 6979, 12557, 6937, 6938, 131579, 6940, 6971, 6959, 6989, 6947)"
'    'Pain
'    k = 4
'    lslist(k) = "(66082,135072,128506, 134517, 134518, 134519, 66114, 66120, 66126, 66132,66133, 66093, 66100, 66101, 134520, 134521, 134522, 134523,"
'    lslist(k) = lslist(k) & "66119, 140471, 134524, 134525, 134526, 134527, 134528, 134529,134530, 134531, 134532, 134533, 134534, 134535, 134536, 134537,"
'    lslist(k) = lslist(k) & "134538, 134539, 134540, 66086, 66092, 66108, 66109, 66112, 66113,66138, 66139, 66146, 66147, 134541, 134542, 134543, 134544, 128187,"
'    lslist(k) = lslist(k) & "134545, 134546, 134547, 134548, 134549, 134550, 134551, 134552,134553, 134554, 134555, 134556, 66121, 66083, 66084, 66085,"
'    lslist(k) = lslist(k) & "66087, 66088, 66089, 66090, 66091, 66094, 66095, 66096, 66097,66098, 66099, 66102, 66103, 66104, 66105, 66106, 66107, 66110,"
'    lslist(k) = lslist(k) & "66111, 66115, 66116, 66117, 66118, 66122, 66123, 66124, 66125,66127, 66128, 66129, 66130, 66131, 66134, 66135, 66136, 66137,"
'    lslist(k) = lslist(k) & "66140, 66141, 66142, 66143, 66144, 66145, 66148, 66149, 66150,66151, 140519, 140513, 134557, 134558, 134559, 134560, 77177,"
'    lslist(k) = lslist(k) & "141470, 140489, 140495, 140477, 140483, 140501, 140507, 134561,134562, 134563, 134564, 134565, 134566, 134567, 134568, 134569,"
'    lslist(k) = lslist(k) & "134570, 140525, 140531)"
'    'Skin/Muscle
'    k = 5
'    lslist(k) = "(132255, 132257,73529, 140552, 23843, 23842, 23845, 23844,73525, 140548, 23847, 23846, 23849, 23848,"
'    lslist(k) = lslist(k) & "135052,132409, 77953, 132678)"
'    'EENT
'    k = 6
'    lslist(k) = "(131877, 131878)"
'    'Reproductive
'    k = 7
'    lslist(k) = "(131614, 131622)"
'    'Psycho-Social
'    k = 8
'    lslist(k) = "(131970, 131971)"
'    'Diabetic
'    k = 9
'    lslist(k) = "(18816)"
'
'    For k = 1 To 9
'        dprint "Now evaluating LikeAssessment category=" & k
'        patresconditions = "patresultslabelseq in " & lslist(k)
'        sql = "select min(convert(datetime,performdate)) from " & DBTABLENAME & " where patient_id='" & acct & "' and (" & patresconditions & ")"
'        rs.Open sql, g_cnADO
'        If IsNull(rs(0)) Then
'            lotime(k) = ""
'        Else
'            lotime(k) = rs(0)
'        End If
'        rs.Close
'
'        sql = "select max(convert(datetime,performdate)) from " & DBTABLENAME & " where patient_id='" & acct & "' and (" & patresconditions & ")"
'        rs.Open sql, g_cnADO
'        If IsNull(rs(0)) Then
'            hitime(k) = ""
'        Else
'            hitime(k) = rs(0)
'        End If
'        rs.Close
'
'        If IsICU Then
'            bucket_size = N3Gbucket 'default is 5 mins; specify -n3gbucket=nn to change it.
'        Else
'            bucket_size = nonN3Gbucket 'default is 5 mins; specify -bucket=nn to change it.
'        End If
'        dprint "LikeBucket size=" & bucket_size & " mins"
'
'        count(k) = 0
'        If lotime(k) <> "" Then 'there were labelseqs found
'        numdiv = DateDiff("n", CDate(lotime(k)), CDate(hitime(k))) \ bucket_size ' minute intervals
'        numdiv = numdiv + 1
'        ReDim ranges(0 To numdiv)
'        For i = 1 To numdiv
'            ranges(i).stime = DateAdd("n", (i - 1) * bucket_size, CDate(lotime(k)))
'            ranges(i).etime = DateAdd("n", i * bucket_size, CDate(lotime(k)))
'            ranges(i).exists = False
'            If i = 1 Or i = numdiv Then
'                dprint i & ": " & ranges(i).stime 'print lotime and hitime
'            End If
'        Next i
'
'        dprint "LikeAssessment divisions=" & numdiv
'        sql = "select convert(datetime,performdate),patresultslabelseq from " & DBTABLENAME & " where patient_id='" & acct & "' and (" & patresconditions & ")"
'        rs.Open sql, g_cnADO
'        rcount = rs.RecordCount
'        rs.MoveFirst
'        For i = 1 To rcount 'place each item into its appropriate time bucket
'            For j = 1 To numdiv
'                ranges(j).exists = (ranges(j).stime <= rs(0) And ranges(j).etime > rs(0)) Or ranges(j).exists
'            Next j
'
'            sbucket = ""
'            For j = 1 To numdiv
'                If ranges(j).exists Then
'                    sbucket = sbucket & "," & j
'                End If
'            Next j
'            dprint rs(1) & ": " & rs(0) & " Buckets=" & sbucket
'
'            If Not rs.EOF Then rs.MoveNext
'        Next i
'
'        rs.Close
'
'        For j = 1 To numdiv
'            If ranges(j).exists Then
'                count(k) = count(k) + 1
'            End If
'        Next j
'        End If 'lotime(k)<>""
'
'        Erase ranges
'    Next k
'
'    hicount = 0
'    countsource = 0
'    For k = 1 To 9
'        If count(k) > hicount Then
'            hicount = count(k)
'            countsource = k
'        End If
'    Next k
'    If hicount >= 58 Then
'        i = 17
'    ElseIf hicount >= 24 Then
'        i = 16
'    ElseIf hicount >= 12 Then
'        i = 15
'    ElseIf hicount >= 5 Then
'        i = 14
'    Else
'        i = 0
'    End If
'
'
'    Select Case countsource
'        Case 1: cat = "Neuro"
'        Case 2: cat = "Cardio"
'        Case 3: cat = "Pulm"
'        Case 4: cat = "Pain"
'        Case 5: cat = "Skin/Muscle"
'        Case 6: cat = "EENT"
'        Case 7: cat = "Repro"
'        Case 8: cat = "Psych"
'        Case 9: cat = "Diab"
'        Case 0: cat = "none"
'    End Select
'    dprint acct & " FINAL LikeAssessment count=" & hicount & " triggers " & i & "  category=" & cat
'
''Keep this commented out until tested
'        If hicount >= 58 Then
'            inds(17).checked = True
'        ElseIf hicount >= 24 Then
'            inds(16).checked = True
'        ElseIf hicount >= 12 Then
'            inds(15).checked = True
'        ElseIf hicount >= 5 Then
'            inds(14).checked = True
'        End If
'    Set rs = Nothing
'
'End Sub

Private Sub QExperimentAssess()
    Dim rs As New Recordset
    Dim sql, sql2, lslist As String
    Dim i, j As Integer
    Dim count As Integer
    Dim lodt As Date
    Dim hidt As Date
    Dim numdiv As Integer
    Dim ranges() As division
    Dim bucket_size As Integer
    Dim sbucket As String
    Dim b As Integer
    Dim vs As String
    Dim patresconditions As String
    Dim pain1 As String
    
    vs = " including VS"
    dprint "QExperimentAssess" & vs
    lslist = "(6913,6914,6915,6918,6922,6923,6925,6926,6928,6931,6933,6934,6937,6938,6940,6947,6953,"
    lslist = lslist & "6956,6957,6958,6959,6971,6978,6979,6987,6989,12557,12558,14590,131317,131330,131579,131580,"
    lslist = lslist & "5793,5794,5795,5796,5797,5798,5799,5800,5835,5836,5837,5844,5845,5846,5847,"
    lslist = lslist & "10041,11945,11946,12113,12114,12115,12116,12117,12118,12119,12120,"
    lslist = lslist & "12121,12122,13219,13220,13221,13222,18816,22832,23829,23834,23835,23836,23837,23838,"
    lslist = lslist & "23840,23841,23842,23843,23844,23845,23846,23847,23848,23849,41406,41406,"
    lslist = lslist & "41407,62632,62633,67119,67120,67121,67122,67127,67128,67129,67130,"
    lslist = lslist & "67131,67132,67133,67134,67135,69970,70060,71622,71623,73525,73527,73528,73529,"
    lslist = lslist & "77953,83766,83767,83768,83769,83770,83771,83772,83773,83774,83775,83776,83777,"
    lslist = lslist & "83778,83779,83780,83781,83782,83783,83784,83785,83786,84000,84001,84002,84003,"
    lslist = lslist & "84004,84005,84006,84007,84008,84009,84010,84011,84012,132161,"
    lslist = lslist & "131526,131614,131622,131648,131649,131668,131669,131722,131809,"
    lslist = lslist & "131810,131811,131812,131823,131829,131838,131839,131858,131869,131877,131878,"
    lslist = lslist & "131914,131915,131917,131920,131922,131923,131924,131925,131926,131927,131970,"
    lslist = lslist & "131971,132154,132155,132156,132157,132158,132159,132160,132166,132167,132168,"
    lslist = lslist & "132169,132170,132171,132194,132255,132257,132409,132678,132679,134335,134336,134337,"
    lslist = lslist & "134338,134339,134355,134356,134357,134358,134359,134564,"
    lslist = lslist & "135301,135302,135303,135304,135305,135306,135307,135308,135384,135385,135386,135387,135780,135782,"
    lslist = lslist & "140545,140548,140550,140551,140552,140553,140554,140557,140560,140561,"
    lslist = lslist & "140625,141361,141785,142638,142639,142640,142641,142642,142644,144036,144037)"
    
    sql2 = ""
        vs = "(131360,137759,141371,137762,137763,131342,9757,6880,131431,131033,47180,46936,46937,77934,137752,137707,138590)"
        sql2 = sql2 & " or patresultslabelseq in " & vs
    sql2 = sql2 & " or (patresultslabelseq>=134435 and patresultslabelseq<=134443)"
    
    'leg code specific
'Blrvis , Chheav, Chstpr, Chtigh, Diapho, Diznes, Dysnea, Flushd, Headac, Hrchg, Lthead, Nausea, Palps, Sobb, Syncop, Vomit
    sql2 = sql2 & " or (patresultslabelseq=140543 and ("
    sql2 = sql2 & " result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Blrvis%'"
    sql2 = sql2 & " or result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Chheav%'"
    sql2 = sql2 & " or result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Chstpr%'"
    sql2 = sql2 & " or result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Chtigh%'"
    sql2 = sql2 & " or result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Diapho%'"
    sql2 = sql2 & " or result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Diznes%'"
    sql2 = sql2 & " or result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Dysnea%'"
    sql2 = sql2 & " or result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Flushd%'"
    sql2 = sql2 & " or result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Headac%'"
    sql2 = sql2 & " or result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Hrchg%'"
    sql2 = sql2 & " or result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Lthead%'"
    sql2 = sql2 & " or result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Nausea%'"
    sql2 = sql2 & " or result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Palps%'"
    sql2 = sql2 & " or result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Sobb%'"
    sql2 = sql2 & " or result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Syncop%'"
    sql2 = sql2 & " or result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Vomit%'))"
    
'Medctd, Mdnoty, Pacpdo, Pulgeo
    sql2 = sql2 & " or (patresultslabelseq=26297 and ("
    sql2 = sql2 & " result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Medctd%'"
    sql2 = sql2 & " or result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Mdnoty%'"
    sql2 = sql2 & " or result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Pacpdo%'"
    sql2 = sql2 & " or result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Pulgeo%'))"

'Small, Moderat, Large, Copius
'5804, 5805, 5806
    sql2 = sql2 & " or (patresultslabelseq in (5804,5805,5806) and ("
    sql2 = sql2 & " result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Small%'"
    sql2 = sql2 & " or result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Moderat%'"
    sql2 = sql2 & " or result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Large%'"
    sql2 = sql2 & " or result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Copius%'))"

'Cntneb, Dbcogh, Cpt, Emrgeq, Incent, Intubd, Medctd, Nebtmt, PD, Rtnoti, Sxnet, Sxnas, Sxora, Sxtra, Sxstaf, Trachi, Trachr, O2a
    sql2 = sql2 & " or (patresultslabelseq=134342 and ("
    sql2 = sql2 & " result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Cntneb%'"
    sql2 = sql2 & " or result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Dbcogh%'"
    sql2 = sql2 & " or result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Cpt%'"
    sql2 = sql2 & " or result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Emrgeq%'"
    sql2 = sql2 & " or result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Incent%'"
    sql2 = sql2 & " or result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Intubd%'"
    sql2 = sql2 & " or result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Medctd%'"
    sql2 = sql2 & " or result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Nebtmt%'"
    sql2 = sql2 & " or result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%PD%'"
    sql2 = sql2 & " or result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Rtnoti%'"
    sql2 = sql2 & " or result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Sx%'"
    sql2 = sql2 & " or result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Trach%'"
    sql2 = sql2 & " or result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%O2a%'))"
    
'Icepks, Kpad, Medctd, Moisht, Tens
    sql2 = sql2 & " or (patresultslabelseq=135052 and ("
    sql2 = sql2 & " result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Icepks%'"
    sql2 = sql2 & " or result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Kpad%'"
    sql2 = sql2 & " or result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Medctd%'"
    sql2 = sql2 & " or result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Moisht%'"
    sql2 = sql2 & " or result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%Tens%'))"
    
    'pain
    pain1 = " or patresultslabelseq in (135072,177675,77177,128187,128506,140471,140477,140483,140489,140495,140501,140507,140513,140519,140525,140531,141470,178131,178132,178133,178134,178135)"
    pain1 = pain1 & " or (patresultslabelseq>=66083 and patresultslabelseq<=66151)"
    pain1 = pain1 & " or (patresultslabelseq>=134517 and patresultslabelseq<=134570)"
    If Not IsICU Then
        sql2 = sql2 & pain1
    End If
    
    
'    sql = "select distinct(performdate) from " & DBTABLENAME & " where patient_id='" & acct & "' and patresultslabelseq in " & lslist
'    rs.Open sql, g_cnADO
'    count = rs.RecordCount
'    rs.Close
    patresconditions = "patresultslabelseq in " & lslist
    patresconditions = patresconditions & sql2
    
    hidt = g_util.CDateEx(intime)
    lodt = DateAdd("d", -1, hidt)
    dprint "Bucket range = " & lodt & " TO " & hidt
        
    If IsICU Then
        bucket_size = N3Gbucket 'default is 5 mins; specify -n3gbucket=nn to change it.
    Else
        bucket_size = nonN3Gbucket 'default is 5 mins; specify -bucket=nn to change it.
    End If
    dprint "Bucket size=" & bucket_size & " mins"
        
    numdiv = DateDiff("n", lodt, hidt) \ bucket_size ' minute intervals = 24*60/buckets = 24*60/20 = 72
    numdiv = numdiv + 1
    ReDim ranges(0 To numdiv)
    For i = 1 To numdiv
        ranges(i).stime = DateAdd("n", (i - 1) * bucket_size, lodt)
        ranges(i).etime = DateAdd("n", i * bucket_size, lodt)
        ranges(i).exists = False
        If i = 1 Or i = numdiv Then
            dprint i & ": " & ranges(i).stime
        End If
    Next i
    
    dprint "Assessment divisions=" & numdiv
   'sql = "select distinct(convert(datetime,performdate)),patresultslabelseq from " & DBTABLENAME & " where patient_id='" & acct & "' and (" & patresconditions & ")"
    sql = "select convert(datetime,performdate),patresultslabelseq from " & DBTABLENAME & " where patient_id='" & acct & "' and (" & patresconditions & ")"
    If lookback_on Then
        sql = sql & " and convert(datetime,performdate)>=" & g_dbutil.SQL_DateTime(lookback_dt)
    End If
    rs.Open sql, g_cnADO
    count = rs.RecordCount
    If count >= 5 Then
    rs.MoveFirst
    For i = 1 To count
        For j = 1 To numdiv
            'Existence can be determined more efficiently with some modulo math, but it will be harder to read
            ranges(j).exists = (ranges(j).stime <= rs(0) And ranges(j).etime > rs(0)) Or ranges(j).exists
        Next j
        
'            sbucket = ""
'            For j = 1 To numdiv
'                If ranges(j).exists Then
'                    sbucket = sbucket & "," & j
'                End If
'            Next j
'            dprint rs(1) & ": " & rs(0) & " Buckets=" & sbucket
            
        If Not rs.EOF Then rs.MoveNext
    Next i
    
'    For j = 1 To numdiv
'        If ranges(j).exists Then
'        vs = "VS bucket " & str(j) & ":"
'        rs.MoveFirst
'        For i = 1 To count
'            If ranges(j).stime <= rs(0) And ranges(j).etime > rs(0) Then
'            Select Case rs(1)
'                Case 131360, 137759, 141371, 137762, 137763, 131342, 9757, 6880, 131431, 131033, 47180, 46936, 46937, 77934, 137752, 137707, 138590:
'                    vs = vs & str(rs(1)) & ","
'            End Select
'            End If
'            If Not rs.EOF Then rs.MoveNext
'        Next i
'        dprint vs
'        End If
'    Next j
    End If 'count >=5
    rs.Close
    Set rs = Nothing
    
    count = 0
    For j = 1 To numdiv
        If ranges(j).exists Then
            count = count + 1
        End If
    Next j
    Erase ranges
    
    If count >= 58 Then
        expi = 17
    ElseIf count >= 24 Then
        expi = 16
    ElseIf count >= 12 Then
        expi = 15
    ElseIf count >= 5 Then
        expi = 14
    Else
        expi = 0
    End If
    
    dprint acct & "Assessment count=" & count & "=>" & expi
    vs = ""
    
    If IsCHF And expi > 15 Then
        count = 12 'downgrade 16 and 17 to 15; allow 14 to come across
        dprint acct & "Assessment on unit CHF downgraded to==>15"
    End If
    
    If count >= 58 Then
        inds(17).checked = True
    ElseIf count >= 24 Then
        inds(16).checked = True
    ElseIf count >= 12 Then
        inds(15).checked = True
    ElseIf count >= 5 Then
        inds(14).checked = True
    End If

End Sub

Private Function ProcTimesOK(stime As String) As Boolean
    Dim rs As New Recordset
    Dim sql As String
    Dim elmin As Date
        
    ProcTimesOK = False
    
    sql = "select min(el.datetime_in) from encounter_location el"
    sql = sql & " inner join encounter e on (e.encounter_id=el.encounter_id)"
    sql = sql & " where e.acct_number='" & acct & "'"
    
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
    If rs.RecordCount > 0 Then
        rs.MoveFirst
        If Not IsNull(rs(0)) Then
            elmin = rs(0)
            ProcTimesOK = (CDate(stime) >= elmin)
        End If
    End If
    End If
    rs.Close
    Set rs = Nothing
    

End Function

Private Function LatestClassIsDefaultInPast8Hours(ceid As Long) As Boolean
    Dim rs As New Recordset
    Dim sql As String
    Dim d As Long
    Dim cnt As Integer
    
    LatestClassIsDefaultInPast8Hours = False
    ceid = 0
    
    sql = "select ce1.classification_event_id from classification_event ce1"
    sql = sql & " inner join encounter e1 on (e1.encounter_id=ce1.encounter_id) where"
    sql = sql & " e1.acct_number='" & acct & "' and ce1.classified_by_id=-2 and"
    sql = sql & " datediff(n,ce1.classification_datetime," & g_dbutil.SQL_DateTime(nowdt) & ")<=960"
    rs.Open sql, g_cnADO
    cnt = rs.RecordCount
    rs.Close
    
    If cnt > 0 Then
    sql = "select ce1.classification_event_id,ce1.classification_datetime from classification_event ce1"
    sql = sql & " inner join encounter e1 on (e1.encounter_id=ce1.encounter_id) where"
    sql = sql & " e1.acct_number='" & acct & "' and ce1.classified_by_id=-2 and ce1.classification_datetime ="
    sql = sql & " (select max(ce2.classification_datetime) from classification_event ce2 inner join"
    sql = sql & " encounter e2 on (e2.encounter_id=ce2.encounter_id)"
    sql = sql & " where e2.acct_number='" & acct & "' and  ce2.classified_by_id=-2)"
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
    If rs.RecordCount > 0 Then
        rs.MoveFirst
        d = DateDiff("n", rs(1), g_util.CDateEx(intime))
        dprint "Default was " & d & " minutes ago"
        LatestClassIsDefaultInPast8Hours = (d <= 960)
        ceid = rs(0)
    End If
    End If
    rs.Close
    End If 'cnt>0
    Set rs = Nothing

End Function

Private Sub GetDefaultClass(ceid As Long, cept As Integer, cect As Integer, cestr As String)
    Dim rs As New Recordset
    Dim sql As String
    
    sql = "select ce.patient_type,ce.complexity_type from classification_event ce"
    sql = sql & " where ce.classification_event_id=" & Str(ceid)
    rs.Open sql, g_cnADO
    cept = rs(0)
    cect = rs(1)
    If classtm = "0700" Then 'cestr only needed for the 7am -- for all other pulls the default continues
        GetDefaultInds ceid, cestr
    End If
    rs.Close
    Set rs = Nothing

End Sub

Private Sub Q53()
    Dim rs As New Recordset
    Dim sql As String
    dprint "Q53"
    sql = "select count(*) from " & DBTABLENAME & " where patient_id='" & acct & "' and patresultslabelseq in "
    sql = sql & "(137064,76747,76750,76753,76748,76751,76754,76746,76749,76752,129617,3566,8340,19787,3273,130516,129992,130365,144235,150333)"
    If lookback_on Then
        sql = sql & " and convert(datetime,performdate)>=" & g_dbutil.SQL_DateTime(lookback_dt)
    End If
    rs.Open sql, g_cnADO
    If rs.RecordCount > 0 Then
        If rs(0) > 0 Then inds(53).checked = True
    End If
    rs.Close
    Set rs = Nothing
'    dprint "end Q53"
End Sub

Private Sub Q19()
    Dim rs As New Recordset
    Dim sql As String
    dprint "QCVSite19"
    sql = "select count(*) from " & DBTABLENAME & " where patient_id='" & acct & "' and patresultslabelseq in "
    sql = sql & "(47147,47148,47149,47150,47151,47152,47153,47154,71859,71840,135350,135354,63161,63162,63201,"
    sql = sql & "63202,63157,63158,63159,63160,63197,63198,63199,63200,131656,131657,131658,131619,131620,6428,6429,6430,6431,6432,6433)"
    If lookback_on Then
        sql = sql & " and convert(datetime,performdate)>=" & g_dbutil.SQL_DateTime(lookback_dt)
    End If
    rs.Open sql, g_cnADO
    If rs.RecordCount > 0 Then
        If rs(0) > 0 Then inds(19).checked = True
    End If
    rs.Close
    Set rs = Nothing
'    dprint "end Q53"
End Sub

Private Sub QCustom103to108()
    Dim rs As New Recordset
    Dim sql As String
    '103:  3424, 139202
    '104:  4301
    '105:  20708
    '106:  3383, 20803
    '107:  19159
    '108:  19250
    dprint "QCustom103to108"
    sql = "select count(*) from " & DBTABLENAME & " where patient_id='" & acct & "' and patresultslabelseq in "
    sql = sql & "(3424,139202)"
    If lookback_on Then
        sql = sql & " and convert(datetime,performdate)>=" & g_dbutil.SQL_DateTime(lookback_dt)
    End If
    rs.Open sql, g_cnADO
    If rs.RecordCount > 0 Then
        If rs(0) > 0 Then inds(103).checked = True
    End If
    rs.Close
    
    sql = "select count(*) from " & DBTABLENAME & " where patient_id='" & acct & "' and patresultslabelseq in "
    sql = sql & "(4301)"
    If lookback_on Then
        sql = sql & " and convert(datetime,performdate)>=" & g_dbutil.SQL_DateTime(lookback_dt)
    End If
    rs.Open sql, g_cnADO
    If rs.RecordCount > 0 Then
        If rs(0) > 0 Then inds(104).checked = True
    End If
    rs.Close
    
    sql = "select count(*) from " & DBTABLENAME & " where patient_id='" & acct & "' and patresultslabelseq in "
    sql = sql & "(20708)"
    If lookback_on Then
        sql = sql & " and convert(datetime,performdate)>=" & g_dbutil.SQL_DateTime(lookback_dt)
    End If
    rs.Open sql, g_cnADO
    If rs.RecordCount > 0 Then
        If rs(0) > 0 Then inds(105).checked = True
    End If
    rs.Close
    
    sql = "select count(*) from " & DBTABLENAME & " where patient_id='" & acct & "' and patresultslabelseq in "
    sql = sql & "(3383, 20803)"
    If lookback_on Then
        sql = sql & " and convert(datetime,performdate)>=" & g_dbutil.SQL_DateTime(lookback_dt)
    End If
    rs.Open sql, g_cnADO
    If rs.RecordCount > 0 Then
        If rs(0) > 0 Then inds(106).checked = True
    End If
    rs.Close
    
    sql = "select count(*) from " & DBTABLENAME & " where patient_id='" & acct & "' and patresultslabelseq in "
    sql = sql & "(19159)"
    If lookback_on Then
        sql = sql & " and convert(datetime,performdate)>=" & g_dbutil.SQL_DateTime(lookback_dt)
    End If
    rs.Open sql, g_cnADO
    If rs.RecordCount > 0 Then
        If rs(0) > 0 Then inds(107).checked = True
    End If
    rs.Close
    
    sql = "select count(*) from " & DBTABLENAME & " where patient_id='" & acct & "' and patresultslabelseq in "
    sql = sql & "(19250)"
    If lookback_on Then
        sql = sql & " and convert(datetime,performdate)>=" & g_dbutil.SQL_DateTime(lookback_dt)
    End If
    rs.Open sql, g_cnADO
    If rs.RecordCount > 0 Then
        If rs(0) > 0 Then inds(108).checked = True
    End If
    rs.Close
    
    sql = "select count(*) from " & DBTABLENAME & " where patient_id='" & acct & "' and patresultslabelseq in "
    sql = sql & "(246054,246123)"
    If lookback_on Then
        sql = sql & " and convert(datetime,performdate)>=" & g_dbutil.SQL_DateTime(lookback_dt)
    End If
    rs.Open sql, g_cnADO
    If rs.RecordCount > 0 Then
        If rs(0) > 0 Then inds(111).checked = True
    End If
    rs.Close
    
    Set rs = Nothing

End Sub

Private Function CountUniqueLegs(leg As String) As Integer
    Dim rs As New Recordset
    Dim sql As String

    sql = "select distinct(performdate) from " & DBTABLENAME & " where patient_id='" & acct & "' and result_value COLLATE SQL_Latin1_General_CP1_CS_AS like '%" & leg & "%'"
    If lookback_on Then
        sql = sql & " and convert(datetime,performdate)>=" & g_dbutil.SQL_DateTime(lookback_dt)
    End If
    rs.Open sql, g_cnADO
    CountUniqueLegs = rs.RecordCount
    rs.Close
    Set rs = Nothing
End Function

Private Sub ExtendSheathTimes()
    Dim sql As String
'performdate is a string 07/25/10 07:00
'47072,47080, 47088, 47096, 47071, 47079, 47087, 47095, 47066, 47074, 47082, 47090, 47065, 47073, 47081, 47089
    sql = "UPDATE " & DBTABLENAME & " set performdate=dateadd(hh,12,performdate)"
    sql = sql & " where patresultslabelseq in "
    sql = sql & "(47072,47080, 47088, 47096, 47071, 47079, 47087, 47095, 47066, 47074, 47082, 47090, 47065, 47073, 47081, 47089)"
    g_cnADO.Execute sql
    
End Sub
Private Sub UnitDefaultTyping()
    Dim rs As New Recordset
    Dim sql As String
    Dim i, j As Integer
    Dim upid As Long
    Dim admp As Integer
    Dim indstr As String
    Dim pscore, cscore As Integer
    Dim definds() As String
    
    For i = 1 To unitnum
        unitary(i).defindstr = ""
        For j = 1 To MAX_INDS
            unitary(i).defindstr = unitary(i).defindstr & "N"
        Next j
        
        sql = "select up.unit_param_id,up.default_admission_profile from unit_param up inner join unit u on (up.unit_id = u.unit_id)"
        sql = sql & " inner join unit_alias ua on (ua.unit_id=u.unit_id) where ua.unit_alias_name='" & unitary(i).name & "'"
        sql = sql & " and getdate() between up.effective_datetime and up.expiration_datetime"
        rs.Open sql, g_cnADO
        If Not rs.EOF Then
            upid = g_dbutil.DBToLong(rs(0))
            admp = g_dbutil.DBToLong(rs(1))
            rs.Close
            sql = "select indicators from patient_profile where unit_param_id=" & upid & " and profile_number=" & admp
            rs.Open sql, g_cnADO
            If Not rs.EOF Then
                indstr = rs(0)
                unitary(i).indstr = indstr
                rs.Close
                definds() = Split(indstr, ",") 'indicators are comma-delimited
                
                pscore = 0
                cscore = 0
                
                For j = 0 To UBound(definds)
                    If IsNumeric(definds(j)) Then
                        Mid$(unitary(i).defindstr, definds(j), 1) = "Y"
                        pscore = pscore + ind_weights(definds(j)).pweight
                        cscore = cscore + ind_weights(definds(j)).cweight
                    End If
                Next j
                For j = MAX_TYPES To 1 Step -1
                    If pscore <= pat_types(j) Then
                        unitary(i).ptype = j
                    End If
                    If cscore <= cmp_types(j) Then
                        unitary(i).ctype = j
                    End If
                Next j
                
            Else
                rs.Close
                unitary(i).ptype = 0
                unitary(i).ctype = 0
            End If
        Else
            rs.Close
            unitary(i).ptype = 0
            unitary(i).ctype = 0
        End If
        dprint unitary(i).name & " has typing (" & unitary(i).ptype & "," & unitary(i).ctype & ") from inds " & unitary(i).indstr
    Next i
    
    Set rs = Nothing

End Sub
Private Function QLatestPerfTime(lablist As String) As String 'For Procedure 4
    Dim rs As New Recordset
    Dim sql As String
    Dim i, count As Integer
    Dim hires As String
    
    dprint "QLatestPerfTime"
    hires = ""
    '  select * from tc_data where patient_id=x and patresultslabelseq=123456
    sql = "select max(convert(datetime,performdate)) from " & DBTABLENAME & " where patient_id='" & acct & "' and patresultslabelseq in " & lablist
    sql = sql & " and (result_value like '%Rnstf%' or result_value like '%Rnnstf%' or result_value like '%Rnnon%')"
    If lookback_on Then
        sql = sql & " and convert(datetime,performdate)>=" & g_dbutil.SQL_DateTime(lookback_dt)
    End If
    rs.Open sql, g_cnADO
    count = rs.RecordCount
    If Not IsNull(rs(0)) Then
        hires = rs(0)
    End If
    rs.Close
    Set rs = Nothing
    
    QLatestPerfTime = hires
    dprint "end QLatestPerfTime= " & hires

End Function

