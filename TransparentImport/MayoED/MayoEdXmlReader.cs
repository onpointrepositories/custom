﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Data.SqlClient;                //for SqlConnection
using System.IO;                            // for Path
using PfsShared;
//
// Custom XML reader for Mayo Rochester ED.
// This loads xml files into the chart_item table.
// Patients must already exist.
//
namespace MayoXmlReader
{
    class MayoEdXmlReader
    {
        const int MAX_RESULT_WIDTH = 625;
        
        // Info about the chart being processed
        int         _encounter_id;          // (int is 32 bit so this is OK)
        int         _sequence;              // sequence# for dummy codes
        DateTime    _start_dt;
        DateTime    _finish_dt;
        string      _treatment_area;
        ENCOUNTER_LOCATION[] _locations;    // for unit_id lookup


        // Process one xml file.
        // Returns true if the file has been processed and it is OK to rename.
        //
        public bool process(string xml_pathname)
        {
            XDocument doc;
            string acct_number;

            var pfs = PFSUtility.NewPfsDataContext();

            try
            {
                doc = XDocument.Load(xml_pathname);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error opening file {0}", xml_pathname);
                Console.WriteLine(e.Message);                   // xml validation error
                Program.LogValidationError(e.Message, xml_pathname);
                return false;
            }

            // There might be a .txt file with the same name
            _treatment_area = LoadTreatmentAreaIfAny(xml_pathname);
            
            var ibex_charts =
                from ibex_chart in doc.Descendants("ibex_medical_chart")
                select ibex_chart;

            foreach (var ibex_chart in ibex_charts)
            {

                if (ibex_chart.Attribute("acctnum") == null)
                {
                    Console.WriteLine("missing acctnum");
                    ShowElementInfo(ibex_chart);
                    Program.LogValidationError("Missing acctnum", xml_pathname);
                    return false;
                }

                Program.DebugTrace("");
                Program.DebugTrace(new String('=', 60));
                Program.DebugTrace("ibex chart for {0}, {1} {2} -- acct {3}",
                              ibex_chart.Attribute("lname").Value,
                            ibex_chart.Attribute("fname").Value,
                            ibex_chart.Attribute("mname").Value,
                            ibex_chart.Attribute("acctnum").Value);
                //
                // Look up the account number
                //
                acct_number = ibex_chart.Attribute("acctnum").Value;
                acct_number = AddLeadingZeros(acct_number, 13);         // leading zero pad for Mayo ADT

                //
                // Scan the chart for date/time range
                // (do this before looking up the acct number)
                //
                var charts =
                    from chart in ibex_chart.Descendants("chart")
                    select chart;
                var chart_entries =
                    from entry in charts.Descendants("entry")
                    select entry;

                if (chart_entries.Count() == 0) {
                    Program.LogValidationError("The chart is empty", "Acct=" + acct_number+ "XML=" + xml_pathname);
                    return true;                    // return TRUE since there is nothing to process
                }

                // Get the time range of transactions
                // Beware of entries with no timestamps (null) and timestamps with empty values
                var transactionDates = 
                    from e in chart_entries
                    where (e.Element("entry_timestamp") != null) && (e.Element("entry_timestamp").Value.Length > 0)
                    select e.Element("entry_timestamp").Value;
                var dates = new { 
                    FromDate = transactionDates.Min(), 
                    ToDate = transactionDates.Max() 
                };
                Program.DebugTrace("chart item dates from {0} to {1}", dates.FromDate, dates.ToDate);

                // Here's another way to do the same thing
                var q = 
                    from e in chart_entries
                    where (e.Element("entry_timestamp") != null) && (e.Element("entry_timestamp").Value.Length > 0)
                    group e by 1 into g             //fake grouping with a constant value
                    select new {
                        FromDate = g.Min(t => t.Element("entry_timestamp").Value),
                        ToDate = g.Max(t => t.Element("entry_timestamp").Value)          
                    };
                var qq = q.First();
                //Program.DebugTrace("Method 2: dates from {0} to {1}", qq.FromDate, qq.ToDate);

                // Convert ISO string to DateTime
                if (DateIsValid(qq.FromDate) && DateIsValid(qq.ToDate))
                {
                    _start_dt = PFSUtility.ISOToDateTime(qq.FromDate);
                    _finish_dt = PFSUtility.ISOToDateTime(qq.ToDate);
                }
                else
                {
                    Program.LogValidationError("Invalid date found: " + qq.FromDate + " or " + qq.ToDate,"Acct=" + acct_number+ "XML=" + xml_pathname);
                    return false;
                }

                // Search for acct# in database
                var encounter_lookup =
                    from enc in pfs.ENCOUNTERs
                    where enc.ACCT_NUMBER == acct_number
                    select new
                    {
                        enc.ENCOUNTER_ID
                    };
                if (encounter_lookup.Count() == 0)
                {
                    Program.LogValidationError("Account number not found: " + acct_number, xml_pathname);
                    Program.LogValidationError("  Chart items range from " + _start_dt + " to " + _finish_dt, "");
                    return false;
                }
                var enc_found = encounter_lookup.First();
                _encounter_id = enc_found.ENCOUNTER_ID;
                Program.DebugTrace("Encounter ID = {0}", _encounter_id);

                //
                // Look up the encounter ED locations
                // The patient may be in multiple ED units (visit and inpatient)
                // The data in this file is for ED only.
                // There may be mistakes or overlap in times with inpatient.
                // If we get ED units only then we will be sure to assign the chart items to an ED unit.
                //
                var locations_lookup =
                    from loc in pfs.ENCOUNTER_LOCATIONs
                    where loc.ENCOUNTER_ID == _encounter_id
                        && loc.UNIT.IS_ED.ToString() == "Y"
                    orderby loc.EFFECTIVE_DATETIME_IN
                    select loc;
                _locations = locations_lookup.ToArray();        // save for quick lookup
                if (_locations.Count() == 0)
                {
                    Program.LogValidationError("Patient has no locations - Acct: " + acct_number, Path.GetFileName(xml_pathname) );
                    return false;
                }
                Program.DebugTrace("This patient has {0} locations", _locations.Count());
                
                // if any ED locations don't have an out time then abandon ibex and wait
                foreach (var loc in _locations)
                {
                    if (loc.DATETIME_OUT.Equals(null))
                    {
                        Program.LogValidationError("On-Hold: Not discharged from all ED locations", acct_number);
                        return false;
                    }
                }
                
                // Start saving chart items

                //
                // Delete existing chart items for this encounter bewteen the timestamps
                //
                Program.DebugTrace("Delete existing...");
                using (var cn = PFSUtility.NewSqlConnection())
                {
                    string sql = "delete from chart_item where encounter_id=" + _encounter_id +
                        " and event_datetime between " + PFSUtility.SQLDateTime(_start_dt) +
                                           " and " + PFSUtility.SQLDateTime(_finish_dt);
                    Program.DebugTrace(sql);
                    var cmd = new SqlCommand(sql, cn);
                    cmd.ExecuteNonQuery();
                }

                // Add a treatment area (if any).  This info is not reliably in the chart
                // so it was given to us in a separate file.
                AddTreatmentAreaChartItem(pfs);

                //
                // Process the <chart> entries
                //
                foreach (var chart_entry in chart_entries)
                {
                    string subSection = chart_entry.Element("sub_section").Value;

                    switch (subSection)
                    {
                        case "CHART VIEW":
                        case "CHART PRINT":
                            // skip these
                            break;
                        default:
                            // add chart item(s) for this entry
                            try
                            {
                                ParseChartEntry(pfs, chart_entry);
                            }
                            catch (ArgumentException e)
                            {
                                // Log a validation error and skip this file but continue with others
                                Program.LogValidationError(e.Message, xml_pathname);
                                return false;
                            }
                            break;
                    }

                }

                //
                // Look for a flowsheet
                //
                var flowsheets =
                    from flowsheet in ibex_chart.Descendants("flowsheet")
                    select flowsheet;
                var flow_entries =
                    from entry in flowsheets.Descendants("entry")
                    select entry;
                //Find min,max times of flowsheet entries for Deletion
                q =
                    from e in flow_entries
                    where (e.Element("entry_timestamp") != null) && (e.Element("entry_timestamp").Value.Length > 0)
                    group e by 1 into g             //fake grouping with a constant value
                    select new
                    {
                        FromDate = g.Min(t => t.Element("entry_timestamp").Value),
                        ToDate = g.Max(t => t.Element("entry_timestamp").Value)
                    };
                qq = q.First();
                Program.DebugTrace("Flowsheet: Method 2: dates from {0} to {1}", qq.FromDate, qq.ToDate);

                // Convert ISO string to DateTime
                if (DateIsValid(qq.FromDate) && DateIsValid(qq.ToDate))
                {
                    _start_dt = PFSUtility.ISOToDateTime(qq.FromDate);
                    _finish_dt = PFSUtility.ISOToDateTime(qq.ToDate);
                }
                else
                {
                    Program.LogValidationError("Invalid date found: " + qq.FromDate + " or " + qq.ToDate, "Acct=" + acct_number + "XML=" + xml_pathname);
                    return false;
                }
                //
                // Delete existing chart items for this encounter bewteen the timestamps
                //
                Program.DebugTrace("Delete existing...");
                using (var cn = PFSUtility.NewSqlConnection())
                {
                    string sql = "delete from chart_item where encounter_id=" + _encounter_id +
                        " and event_datetime between " + PFSUtility.SQLDateTime(_start_dt) +
                                           " and " + PFSUtility.SQLDateTime(_finish_dt);
                    Program.DebugTrace(sql);
                    var cmd = new SqlCommand(sql, cn);
                    cmd.ExecuteNonQuery();
                }
                //
                // Process the <flowsheet> entries
                //
                foreach (var flowsheet_entry in flow_entries)
                {
                    try
                    {
                        ParseFlowsheetEntry(pfs, flowsheet_entry);
                    }
                    catch (ArgumentException e)
                    {
                        Program.LogValidationError(e.Message, xml_pathname);
                        return false;
                    }
                }

                //
                // Look for orders
                //
                var orders =
                    from order in ibex_chart.Descendants("orders")
                    select order;
                var order_entries =
                    from entry in orders.Descendants("order")
                    select entry;
                //Find min,max times of order entries for Deletion
                q =
                    from e in order_entries
                    where (e.Element("entry_timestamp") != null) && (e.Element("entry_timestamp").Value.Length > 0)
                    group e by 1 into g             //fake grouping with a constant value
                    select new
                    {
                        FromDate = g.Min(t => t.Element("entry_timestamp").Value),
                        ToDate = g.Max(t => t.Element("entry_timestamp").Value)
                    };
                qq = q.First();
                Program.DebugTrace("Orders: Method 2: dates from {0} to {1}", qq.FromDate, qq.ToDate);

                // Convert ISO string to DateTime
                if (DateIsValid(qq.FromDate) && DateIsValid(qq.ToDate))
                {
                    _start_dt = PFSUtility.ISOToDateTime(qq.FromDate);
                    _finish_dt = PFSUtility.ISOToDateTime(qq.ToDate);
                }
                else
                {
                    Program.LogValidationError("Invalid date found: " + qq.FromDate + " or " + qq.ToDate, "Acct=" + acct_number + "XML=" + xml_pathname);
                    return false;
                }
                //
                // Delete existing chart items for this encounter bewteen the timestamps
                //
                Program.DebugTrace("Delete existing...");
                using (var cn = PFSUtility.NewSqlConnection())
                {
                    string sql = "delete from chart_item where encounter_id=" + _encounter_id +
                        " and event_datetime between " + PFSUtility.SQLDateTime(_start_dt) +
                                           " and " + PFSUtility.SQLDateTime(_finish_dt);
                    Program.DebugTrace(sql);
                    var cmd = new SqlCommand(sql, cn);
                    cmd.ExecuteNonQuery();
                }
                //
                // Process the <order> entries
                //
                foreach (var order_entry in order_entries)
                {
                    try
                    {
                        ParseOrderEntry(pfs, order_entry);
                    }
                    catch (ArgumentException e)
                    {
                        Program.LogValidationError(e.Message, xml_pathname);
                        return false;
                    }
                }

            }

            // Save all of the new chart items
            Program.DebugTrace("submitting...");
            pfs.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
            Program.DebugTrace("done");

            // We can list each file, but it is probably good enough just to say how many files
            // were loaded at the end.
            //Program.LogInfo(String.Format("Loaded chart for account number {0}", acct_number), xml_pathname);
            return true;
        }

        bool DateIsValid(string dt)
        {
            return true;
        }

        string LoadTreatmentAreaIfAny(string xml_pathname)
        {
            string txt_pathname = Path.ChangeExtension(xml_pathname, ".txt");
            string result = "";
            try
            {
                using (StreamReader sr = new StreamReader(txt_pathname))
                {
                    result = sr.ReadLine();
                }
            }
            catch
            {
                // Don't care if it doesn't exist
            }
            Program.DebugTrace("Treatment area = {0}", result);
            return result;
        }

        void AddTreatmentAreaChartItem(PfsDataContext pfs)
        {
            if (_treatment_area.Length == 0) return;

            const string ta = "TREATMENT_AREA";
            var ci = new CHART_ITEM();

            // The treatment area is for the Visit (first) unit only - use _start_dt
            ci.EVENT_DATETIME = _start_dt;
            ci.ENCOUNTER_ID = _encounter_id;
            ci.UNIT_ID = LookupUnitID(_start_dt);
            ci.CODE = NextDummyCode();
            ci.CATEGORY = ta;
            ci.DESCRIPTION = ta;
            ci.FIELD_NAME = ta;
            ci.RESULT = _treatment_area;
            ci.TIMESTAMP = DateTime.Now;

            pfs.CHART_ITEMs.InsertOnSubmit(ci);
            MaybeSubmitChages(pfs);
        }

        int LookupUnitID(DateTime dt)
        {
            if (_locations.Count() == 0) return -1;
            if (dt < _locations.First().EFFECTIVE_DATETIME_IN) return _locations.First().UNIT_ID;  // invalid? return first

            foreach (var loc in _locations)
            {
                if ((dt >= loc.EFFECTIVE_DATETIME_IN) && (dt < loc.EFFECTIVE_DATETIME_OUT))
                    return loc.UNIT_ID;
            }
            return _locations.Last().UNIT_ID;                       // invalid? return last
        }

        string NextDummyCode()
        {
            return "dummy" + string.Format("{0:0000}", _sequence++);
        }

        CHART_ITEM NewChartItem(XElement entry, string category, string description)
        {
            var result = new CHART_ITEM();

            string ts = entry.Element("entry_timestamp").Value;     // try for entry_timestamp
            if (string.IsNullOrEmpty(ts))
                ts = entry.Element("timestamp").Value;              // fall back on timestamp
            result.EVENT_DATETIME = PFSUtility.ISOToDateTime(ts);

            result.ENCOUNTER_ID = _encounter_id;
            result.UNIT_ID = LookupUnitID(result.EVENT_DATETIME);
            // There is no CODE and multiple entries may have the same section & timestamp.
            // Generate unique codes for entries with the same timestamp.
            result.CODE = NextDummyCode();
            result.CATEGORY = category;
            result.DESCRIPTION = description;
            // We should reach out and get the server time, but this should be running on the server anyway
            result.TIMESTAMP = DateTime.Now;                        // ** should use server time
            
            return result;
        }

        void ParseChartEntry(PfsDataContext pfs, XElement entry)
        {
            var combinedText = new StringBuilder();
            string category = entry.Element("section").Value;
            string description = entry.Element("sub_section").Value;

            Program.DebugTrace("entry {0}", entry.Attribute("num"));

            Program.DebugTrace("  timestamp={0}  entry timestamp={1}", 
                entry.Element("timestamp").Value,
                entry.Element("entry_timestamp").Value);
            Program.DebugTrace("  user={0}", entry.Element("user").Value);
            Program.DebugTrace("  CATEGORY={0}", category);
            Program.DebugTrace("  DESCRIPTION={0}", description);

            // Beware empty timestamps!
            if (entry.Element("timestamp").Value.Length == 0)
                return;

            //
            // Look for array of <data> for the result
            //
            var data =
                from datum in entry.Descendants("data")
                select datum;

            // Combine multiple data items into one long result
            foreach (var datum in data)
            {
                string txt = ElementValueOrNullString(datum, "text");
                Program.DebugTrace("  data={0}", txt);

                // Look for simple "field: value" strings
                //
                string[] sep = { ": " };
                var arr = txt.Split(sep, StringSplitOptions.None);
                if (arr.Length == 2)
                {
                    Program.DebugTrace("  FIELD={0}", arr[0]);
                    Program.DebugTrace("  RESULT={0}", arr[1]);
                    var ci = NewChartItem(entry, category, description);
                    ci.FIELD_NAME = arr[0].Left(64);
                    ci.RESULT = arr[1].TruncateWithEllipsis(MAX_RESULT_WIDTH);

                    pfs.CHART_ITEMs.InsertOnSubmit(ci);
                    MaybeSubmitChages(pfs);
                }
                else
                {
                    if (combinedText.Length > 0)
                        combinedText.Append("; ");
                    combinedText.Append(txt);
                }
            }

            if (combinedText.Length > 0)
            {
                Program.DebugTrace("  RESULT={0}", combinedText.ToString());
                var ci = NewChartItem(entry, category, description);
                ci.FIELD_NAME = null;
                ci.RESULT = combinedText.ToString().TruncateWithEllipsis(MAX_RESULT_WIDTH);

                pfs.CHART_ITEMs.InsertOnSubmit(ci);
                MaybeSubmitChages(pfs);
            }

        }

        void ParseFlowsheetEntry(PfsDataContext pfs, XElement entry)
        {
            XElement field_group = entry.Element("field_group");
            if (field_group == null)
                return;
            string category = field_group.Attribute("name").Value;
            string description = "";

            Program.DebugTrace("entry {0}", entry.Attribute("num"));

            Program.DebugTrace("  timestamp={0}  entry timestamp={1}",
                entry.Element("timestamp").Value,
                entry.Element("entry_timestamp").Value);
            Program.DebugTrace("  user={0}", entry.Element("user").Value);
            Program.DebugTrace("  CATEGORY={0}", category);
            //Program.DebugTrace("  DESCRIPTION={0}", description);

            //
            // Look for array of <field>
            //
            //var field_groups =
            //    from field_group in entry.Descendants("field_group")
            //    select field_group;
            var fields =
                from field in field_group.Descendants("field")
                select field;

            foreach (var field in fields)
            {
                string name = ElementValueOrNullString(field, "name");
                string value = ElementValueOrNullString(field, "value");
                Program.DebugTrace("  FIELD={0}", name);
                Program.DebugTrace("  RESULT={0}", value);
                // The first character of a vital sign value needs to be numeric or '-'
                // Legal non-numeric values are 120/80 and -80-
                if ((category == "VITAL SIGNS") && (!value.Left(1).IsNumeric() && value.Left(1) != "-"))
                    Program.DebugTrace("  ** Skipping vital sign without a numeric value");
                else
                {
                    var ci = NewChartItem(entry, category, description);
                    ci.FIELD_NAME = name;
                    ci.RESULT = value;

                    pfs.CHART_ITEMs.InsertOnSubmit(ci);
                    MaybeSubmitChages(pfs);
                }
            }

        }

        void ParseOrderEntry(PfsDataContext pfs, XElement entry)
        {
            string category = "ORDER";
            string description = ElementValueOrNullString(entry, "order_description");
            var ci = NewChartItem(entry, category, description);
            ci.FIELD_NAME = null;
            ci.RESULT = null;

            pfs.CHART_ITEMs.InsertOnSubmit(ci);
            MaybeSubmitChages(pfs);
        }


        string AddLeadingZeros(string s, int desired_length)
        {
            string result = s;
            while (result.Length < desired_length)
            {
                result = "0" + result;
            }
            return result;
        }

        string ElementValueOrNullString(XElement node, string name)
        {
            XElement elem = node.Element(name);
            if (elem != null)
                return elem.Value;
            return "";
        }

        void ShowElementInfo(XElement xelem)
        {
            Program.DebugTrace("{2} -- Has Attr? {0}, Has Elem? {1}",
                xelem.HasAttributes,
                xelem.HasElements,
                xelem.Name);
        }

        void MaybeSubmitChages(PfsDataContext pfs)
        {
            // Turn on debug mode to insert a row at a time
            if (Program._debug)
            {
                // Multiple inserts are not as effecient but this could really help track down
                // an unexpected error: the trace should show what entry was being parsed
                // if the insert fails.
                pfs.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
            }
        }

    }
}
