Attribute VB_Name = "ShandsMain"
Option Explicit
'
' Shands (Epic data) transparent classification
'-verbose -efftime=0700 -effdate=yesterday -pulltime=0700 -pulldate=today -range=1440
' Each unit must set the "use transparent" flag.
'
' This is designed to be run as a background job, so never stop for user input -- that means no error MsgBox.
' All communication is done through log files and the event log; it is OK to print to the console.
'
' In order to be version independent, this program does not use any AcuityPlus dlls.
' Some AcuityPlus utility classes have been copied here.
'
' In order to work with the AcuityPlus Patient Selection and Transparent import:
'
'   * The program name must be TransparentMapping.exe, placed in AcuityPlus\bin
'   * The audit file is called TransparentAudit.log, placed in AcuityPlus\log
'   * The output file is Transparent.txt, placed in AcuityPlus\load_me
'   * Events are saved in the database event log
'
Public g_cnADO          As ADODB.Connection             'main DB connection
Public g_util           As New PFSUtility               'utility functions
Public g_dbutil         As New PFSDBUtility
Public g_event          As New PFSEventLog
Public g_display        As New PFSDisplayProperties
Public g_command        As String                       'command line (for db utility)
Public g_abort          As Boolean


Public Const MAPPER_VERSION = "6.31"
Public Const MAX_INDS = 50
Private Const TRANSP_FILENAME = "Transparent.txt"       'THE output file
Private Const TRANSP_AUDIT_LOG = "TransparentAudit.log" 'The debug/audit log
Private Const CHART_ITEM_LIFE = 30                      'days in the chart_item table
Private Const OUTCOMES_FILENAME = "OutcomesIndicator.txt"   'outcomes output file

Private Const INPUT_PRODPATH = "c:\program files\quadramed\acuityplus\load_me\"
Private Const INPUT_IFILE = "tcquery."
'C:\Documents and Settings\kmasumoto\Desktop\CLIENT FOLDERS\Mission
Private Const DEFAULT_RANGE = 1440                      '24 hrs in minutes; overridden by -range

Public Type PatientInfo
    unit_id             As Long
    encounter_id        As Long
    meth_id             As Long
    last_name           As String
    first_name          As String
    acct                As String
    age                 As Single                   'age (in years) at admission
    unit_name           As String
    room                As String
    bed                 As String
    unit_arrival        As Date
    effective           As Date                     'patient specific (may be > g_effdt)
    pull_start          As Date                     'patient specific (may be > g_pull_start)
    pull_finish         As Date                     'patient specific (may be < g_pull_finish)
    range               As Integer                  'patient specific (may be < g_range)
    TC_source_id        As Integer
End Type
Public Type user_defined_type
    uid As Long
    upid As Long
    admp As Integer
    indstr As String
    indicators As String
End Type

Public g_debug          As Boolean
Public g_verbose        As Boolean                  'verbose debug output?
Public g_no_output      As Boolean      'if no ouput, then no input files either

Public g_effdt          As Date                     'effective datetime

Public outfile          As Integer
Public outfile2          As Integer
Public dbugfile         As Integer

Public gLogUnitID       As Long
Public gLogEncounterID  As Long
Public gLogSourceText   As String

Public g_pull_start     As Date
Public g_pull_finish    As Date                     'pull datetime
Public g_range          As Long
Public definds()        As user_defined_type
Public numunit          As Integer

Private winpfspath      As String                   'path of winpfs
Private winlogpath      As String                   'path of winpfs\log
Private winloadpath     As String                   'path of winpfs\load_me
Private outfilename     As String
Private outfile2name    As String
Private dbugname        As String

Private nowdt           As Date
Private debug_acct      As String                   'acct filter
Private debug_unit      As String                   'unit filter
Private debug_unit_id   As Long

Private inputqfile      As Integer
Private inputpfile      As Integer
Private inputofile      As Integer
Private inputqname      As String
Private inputifile      As Integer
Private inputiname      As String
Private inputpname      As String
Private inputoname      As String
Public ADLUnitList As String
Public brief_audit      As String
Public verbose_audit      As String


Sub Main()
    Dim s As String
    On Error GoTo errHandler
    
    frmMain.Show vbModeless                         'progress window
    DoEvents
    
    Set g_cnADO = g_dbutil.NewRemoteConnection
    
    gLogSourceText = Command$
    LogInfo "Begin mapping and translation", EVENT_CATEGORY_STARTUP_SHUTDOWN 'deletes old event_log records
    gLogSourceText = ""
    
    ParseCommandLine
    OpenOutputFiles
    UnitDefaultTyping
    Process
    CloseOutputFiles
    DeleteOldLogs
    DeleteOldChartItems

    LogInfo "Mapping complete", EVENT_CATEGORY_STARTUP_SHUTDOWN
    g_cnADO.Close
normalExit:
    Unload frmMain
    Exit Sub

errHandler:
    LogError Err.Description & " in " & Err.source
    LogInfo "Unexpected error - shut down", EVENT_CATEGORY_STARTUP_SHUTDOWN
    GoTo normalExit
    Resume  'debug
End Sub

Private Sub ParseCommandLine()
    On Error GoTo errHandler

    Dim argc As Integer, argv() As String, subarg() As String, tag As String, value As String
    Dim i As Integer, n As Integer
    Dim effdate As String, efftime As String
    Dim pulldate As String, pulltime As String
    Dim nowdate As String, nowtime As String
    Dim called_from_server As Boolean
    
    '-effdate=yyyymmdd   This is the date which, in combination with the
    '                   -efftime time is used to specify the classification In time.
    '                   If not specified, -effdate is assumed to be the current date.
    'Special value:  -effdate=yesterday
    
    '-efftime=hhmm      Used to set the classification In time.
    '                   If not specified, then -efftime is assumed to be the current time.
        
    '-pulldate=yyyymmdd This is the date/time of the data pull time.
    '                   If not specified, then this is assumed to be the -effdate.
    '-pulltime=hhmm     This is the time starting from which the pull is to
    '                   look backwards from.
    '                   If not specified, then this time is assumed to be the -efftime.
    
    '-range=nnnn        This is the number of minutes backwards from the pull time
    '                   that defines valid g_range of charting events.
    '                   If not specified, this will default to 1440 (24 hours)

    '-debug             echo debug/audit info; save in log file
    '-brief             Minimal debug info -- sets -debug
    '-verbose           Lots of debugging info -- sets -debug
    '-n                 No output to transparent.txt - debug only
    '-audit             sets -debug and -n
    '
    '-acct=12345        Process this patient only
    '-unit=5N           Process this unit only
    '-unit_id=1234      process this unit only
    
'==================
'        pull_dt = rs("classification_datetime")
'        eff_dt = rs("effective_datetime_in")
'        range = g_dbutil.DBToInteger(rs("TC_pull_range"))
'        If (range = 0) Then range = 1440
'    cmd = "..\bin\" & TRANSPARENT_MAPPING_EXE_NAME
'    If verbose Then
'        cmd = cmd & " -verbose"
'    Else
'        cmd = cmd & " -brief"
'    End If
'    cmd = cmd & " -n"                               'no output to transparent.txt
'    cmd = cmd & " -unit_id=" & cdlUnit.code(cboUnit.Text)
'    cmd = cmd & " -acct=" & cboAcct.Text
'    cmd = cmd & " -pulldate=" & Format$(pull_dt, "yyyymmdd")
'    cmd = cmd & " -pulltime=" & Format$(pull_dt, "hhnn")
'    cmd = cmd & " -effdate=" & Format$(eff_dt, "yyyymmdd")
'    cmd = cmd & " -efftime=" & Format$(eff_dt, "hhnn")
'    cmd = cmd & " -range=" & range
'
'==================
    
    nowdt = Now
    nowdate = Format$(nowdt, "yyyymmdd")
    nowtime = Format$(nowdt, "hhnn")
    
    g_verbose = True

    argc = g_util.SplitTextOnChar(LCase$(Command$), " ", argv(), 1, 1)

    For i = 1 To argc
        n = g_util.SplitTextOnChar(argv(i), "=", subarg(), 1, 2)        '<tag>=<value>
        tag = subarg(1)
        value = subarg(2)
        
        Select Case tag
        Case "-server"    'must be first arg
            called_from_server = True
        
        Case "-acct"
            debug_acct = value
            
        Case "-audit"
            g_debug = True
            g_no_output = True
        
        Case "-brief"
            g_verbose = False
            g_debug = True
        
        Case "-debug"
            g_debug = True
        
        Case "-effdate"
            If (value = "yesterday") Then
                effdate = Format(DateAdd("d", -1, Date), "yyyymmdd")
            Else
                effdate = value
            End If
        
        Case "-efftime"
            efftime = Left$(value, 4)
            efftime = String$(4 - Len(efftime), "0") & efftime
        
        Case "-n"
            g_no_output = True

        Case "-pulldate"
            If (value = "today") Then
                pulldate = Format(Date, "yyyymmdd")
            Else
                pulldate = value
            End If
        
        Case "-pulltime"
            pulltime = Left$(value, 4)
            pulltime = String$(4 - Len(pulltime), "0") & pulltime

        Case "-range"
            g_range = CInt(value)
            
        Case "-unit"
            debug_unit = value
        Case "-unit_id"
            debug_unit_id = CLng(value)
            
        Case "-verbose"
            g_verbose = True
            g_debug = True

        Case Else
            LogWarning "Unexpected argument: " & tag
        End Select
    Next i
    
'    If Not called_from_server Then
'        'adjust times
'        effdate = Format(DateAdd("d", 1, g_util.CDateEx(effdate)), "yyyymmdd")
'        efftime = "0700"
'        pulldate = effdate
'        pulltime = efftime
'        g_range = 1440
'    End If

    If Len(effdate) = 0 Then effdate = nowdate
    If Len(efftime) = 0 Then efftime = nowtime
    
    'Note: pulldate defaults to effdate, not nowdate
    If Len(pulldate) = 0 Then pulldate = effdate
    If Len(pulltime) = 0 Then pulltime = efftime

    If g_range = 0 Then g_range = DEFAULT_RANGE
    
    'Note: the log file isn't open yet so these just go to the screen
    Debug.Print "Command line: " & Command$
    Debug.Print "effdate=" & effdate
    Debug.Print "efftime=" & efftime
    Debug.Print "pulldate=" & pulldate
    Debug.Print "pulltime=" & pulltime
    Debug.Print "range=" & g_range
    
    g_effdt = g_util.CDateEx(effdate & efftime)
    
    g_pull_finish = g_util.CDateEx(pulldate & pulltime)     'for chart item queries
    g_pull_start = DateAdd("n", -g_range, g_pull_finish)
    Exit Sub
    
errHandler:
    g_util.ThrowError "ParseCommandLine"
    Resume  'debug
End Sub


Private Sub Process()
    On Error GoTo errHandler
    
    Dim pat As PatientInfo
    Dim rs As New Recordset
    Dim sql As String
    Dim count As Integer

        
    '
    ' Make a list of all patients with chart items during the pull period.
    ' Include only units that have the "use transparent" flag set.
    ' Limit to one patient if -acct is given.  Limit to one unit with -unit.
    ' Left join encounter_location - the patient may be discharged at pull time.
    ' Look for the latest patient arrival to the unit before the pull time; ignore transfers within the unit.
    ' Look for a patient departure (if any) before the pull time; ignore transfers within the unit.
    sql = "" & _
        " SELECT LIST.UNIT_ID, LIST.ENCOUNTER_ID, PARAM.METHODOLOGY_ID," & _
        "     UNIT.NAME AS UNIT, EL.ROOM, EL.BED, EL.DATETIME_IN AS BED_ARRIVAL," & _
        "     ARRIVE.DATETIME_IN AS UNIT_ARRIVAL, DEPART.DATETIME_OUT AS UNIT_DEPARTURE," & _
        "     E.ACCT_NUMBER, P.LAST_NAME, P.FIRST_NAME," & _
        "     P.DOB, E.ADMISSION_DATETIME, E.REGISTRATION_DATETIME" & _
        " FROM (" & _
        "     SELECT DISTINCT CI.UNIT_ID, CI.ENCOUNTER_ID" & _
        "     FROM CHART_ITEM AS CI" & _
        "     INNER JOIN UNIT ON (UNIT.UNIT_ID = CI.UNIT_ID)" & _
        "     WHERE UNIT.USE_TRANSPARENT_CLASSIFICATION='Y'" & _
        "     AND EVENT_DATETIME BETWEEN " & g_dbutil.SQL_DateTime(g_pull_start) & " AND " & g_dbutil.SQL_DateTime(g_pull_finish) & _
        " ) AS LIST"
    sql = sql & _
        " INNER JOIN UNIT ON (UNIT.UNIT_ID = LIST.UNIT_ID)" & _
        " INNER JOIN UNIT_PARAM AS PARAM ON (PARAM.UNIT_ID = UNIT.UNIT_ID) AND (" & g_dbutil.SQL_DateTime(g_pull_finish) & " BETWEEN PARAM.EFFECTIVE_DATEtime AND param.EXPIRATION_DATEtime)" & _
        " INNER JOIN ENCOUNTER AS E ON (E.ENCOUNTER_ID = LIST.ENCOUNTER_ID)" & _
        " INNER JOIN PERSON AS P ON (P.PERSON_ID = E.PERSON_ID)" & _
        " LEFT  JOIN ENCOUNTER_LOCATION AS EL ON (EL.ENCOUNTER_ID = E.ENCOUNTER_ID) AND (" & g_dbutil.SQL_DateTime(g_pull_finish) & " BETWEEN EL.EFFECTIVE_DATETIME_IN AND EL.EFFECTIVE_DATETIME_OUT)" & _
        " INNER JOIN (" & _
        "    SELECT WORKING_UNIT_ID AS UNIT_ID, ENCOUNTER_ID, MAX(DATETIME_IN) AS DATETIME_IN" & _
        "    FROM ENCOUNTER_LOCATION AS EL" & _
        "    WHERE WORKING_UNIT_ID IN (SELECT UNIT_ID FROM UNIT WHERE USE_TRANSPARENT_CLASSIFICATION='Y')" & _
        "    AND DATETIME_IN < " & g_dbutil.SQL_DateTime(g_pull_finish) & _
        "    AND IS_TRANSFER_IN='Y'" & _
        "    GROUP BY WORKING_UNIT_ID, ENCOUNTER_ID" & _
        " ) AS ARRIVE ON (ARRIVE.UNIT_ID = UNIT.UNIT_ID) AND (ARRIVE.ENCOUNTER_ID = E.ENCOUNTER_ID)" & _
        " LEFT JOIN (" & _
        "    SELECT WORKING_UNIT_ID AS UNIT_ID, ENCOUNTER_ID, MAX(DATETIME_OUT) AS DATETIME_OUT" & _
        "    FROM ENCOUNTER_LOCATION AS EL" & _
        "    WHERE WORKING_UNIT_ID IN (SELECT UNIT_ID FROM UNIT WHERE USE_TRANSPARENT_CLASSIFICATION='Y')" & _
        "    AND DATETIME_OUT < " & g_dbutil.SQL_DateTime(g_pull_finish) & _
        "    AND ((NEXT_WORKING_UNIT_ID<>WORKING_UNIT_ID) OR (NEXT_WORKING_UNIT_ID IS NULL))" & _
        "    GROUP BY WORKING_UNIT_ID, ENCOUNTER_ID" & _
        " ) AS DEPART ON (DEPART.UNIT_ID = UNIT.UNIT_ID) AND (DEPART.ENCOUNTER_ID = E.ENCOUNTER_ID)" & _
        " WHERE 1=1"
    
    If Len(debug_acct) Then
        sql = sql & " AND E.ACCT_NUMBER=" & g_dbutil.SQL_String(debug_acct)
    End If
    If Len(debug_unit) Then
        sql = sql & " AND UNIT.NAME=" & g_dbutil.SQL_String(debug_unit)
    End If
    If (debug_unit_id > 0) Then
        sql = sql & " AND UNIT.UNIT_ID=" & debug_unit_id
    End If
    sql = sql & " ORDER BY UNIT.NAME, P.LAST_NAME, P.FIRST_NAME, E.ACCT_NUMBER"
    
    Debug.Print sql
    dprint sql
    rs.Open sql, g_cnADO
    count = 0

    Do While Not rs.EOF
        'Package the patient info (don't use unit/encounter objects)
        With pat
            .unit_id = rs("UNIT_ID")
            .encounter_id = rs("ENCOUNTER_ID")
            .meth_id = rs("METHODOLOGY_ID")
            .acct = rs("ACCT_NUMBER")
            .last_name = rs("LAST_NAME") & ""
            .first_name = rs("FIRST_NAME") & ""
            .unit_name = rs("UNIT") & ""
            .room = rs("ROOM") & ""
            .bed = rs("BED") & ""
            
            If Not IsNull(rs("DOB")) And Not IsNull(rs("ADMISSION_DATETIME")) Then
                .age = DateDiff("n", rs("DOB"), rs("ADMISSION_DATETIME")) / 60# / 24# / 365.24219
            ElseIf Not IsNull(rs("DOB")) And Not IsNull(rs("REGISTRATION_DATETIME")) Then
                .age = DateDiff("n", rs("DOB"), rs("REGISTRATION_DATETIME")) / 60# / 24# / 365.24219
            Else
                .age = 0
            End If
            
            .unit_arrival = rs("UNIT_ARRIVAL")
            .effective = g_util.Max(g_effdt, .unit_arrival)
            .pull_start = g_util.Max(g_pull_start, .unit_arrival)
            .pull_finish = g_util.Min(g_pull_finish, rs("UNIT_DEPARTURE"))
            If (.pull_finish < .pull_start) Then
                .pull_finish = g_pull_finish
            End If
            .range = DateDiff("n", .pull_start, .pull_finish)
            .TC_source_id = 1 'g_dbutil.DBToInteger(rs("TC_SOURCE_ID"))
        End With
        
        'Save ids for log entries
        gLogUnitID = pat.unit_id
        gLogEncounterID = pat.encounter_id
    
        'Process this patient
        count = count + 1
        frmMain.SetProgress "Processing patient " & count & " of " & rs.RecordCount
        
    brief_audit = ""
    verbose_audit = ""
        dprint "TRANSPARENT MAPPING AUDIT FILE Version " & MAPPER_VERSION, 1
        dprint String$(80, "="), 1
    dprint ""
    dprint "Pull at:   " & g_pull_finish & " (back to " & g_pull_start & "; range=" & g_range & ")", 1
    dprint "Effective: " & g_effdt, 1
        dvprint ""
        dprint "Patient " & count & " of " & rs.RecordCount & ": " & _
            pat.last_name & ", " & pat.first_name & _
            " in " & pat.unit_name & " " & pat.room & " " & pat.bed & _
            " acct=" & pat.acct & " age=" & Round(pat.age, 2), 1
        dprint "Here from " & pat.pull_start & " to " & pat.pull_finish & _
            "  (LOS=" & Round(pat.range / 60, 2) & ")", 1
        dprint String$(10, "-"), 1
        'Most sites will have one source of chart items
        'If there are multiple sources that need different mapping, this is the place to do it
        Select Case pat.TC_source_id
'        Case 123
'            Select Case pat.meth_id
'            Case METH_ID_INPATIENT
'                ProcessInpatient_123 pat
'            Case Else
'                LogWarning "Methodology " & pat.meth_id & " in unit " & pat.unit_name & " is not supported"
'            End Select
        
        Case Else

            Select Case pat.meth_id
            Case 18
                ProcessInpatient pat
            Case Else
                LogWarning "Methodology " & pat.meth_id & " in unit " & pat.unit_name & " is not supported"
            End Select

        End Select
        
        gLogUnitID = 0
        gLogEncounterID = 0

        If g_abort Then Exit Do
        
        rs.MoveNext
    Loop
    
    rs.Close
    
    If (count < 1) Then
        dprint ""
        If Len(debug_acct) Then
            LogWarning "The selected patient has no chart items in the given time range", EVENT_CATEGORY_UNEXPECTED
        Else
            LogWarning "No chart items found to process - have the unit(s) been enabled for transparent classification?", EVENT_CATEGORY_UNEXPECTED
        End If
    End If
    
    Exit Sub

errHandler:
    g_util.ThrowError "Process"
    Resume  'debug
End Sub

'debug print
Public Sub dprint(s As String, Optional out_to As Integer = 0)
    If Not g_debug Then Exit Sub
    
    Debug.Print s                           'add to debug window
    'It would be nice if we could print to the cmd window, but it takes some effort

    If dbugfile <> 0 Then
        Print #dbugfile, s                  'add to debug log file
    End If
    If out_to = 0 Then 'brief only
        brief_audit = brief_audit & s & vbCrLf
    ElseIf out_to = 1 Then 'both
        brief_audit = brief_audit & s & vbCrLf
        verbose_audit = verbose_audit & s & vbCrLf
    End If
End Sub

'debug verbose print
Public Sub dvprint(s As String)
    If g_verbose Then
        verbose_audit = verbose_audit & s & vbCrLf
        dprint s, 2
    End If
End Sub

Private Sub OpenOutputFiles()
    On Error GoTo errHandler
    
    If Not g_util.GetKeyValue(HKEY_LOCAL_MACHINE, "SOFTWARE\Quadramed\WinPFS", "Directory", winpfspath) Then
        winpfspath = App.Path & "\.."
    End If

    winlogpath = winpfspath & "\log"
    winloadpath = winpfspath & "\load_me"
    If Not g_util.DirExists(winlogpath) Then
        MkDir$ winlogpath
    End If

    'dbugname = winlogpath & "\" & TRANSP_DEBUG & Format$(nowdt, "mmdd") & ".log"
    dbugname = winlogpath & "\" & TRANSP_AUDIT_LOG
    
    If g_debug Then
        dbugfile = FreeFile
        Open dbugname For Output As #dbugfile                   'overwrite existing
        Print #dbugfile, String(80, "=")
        Print #dbugfile, "TRANSPARENT MAPPING AUDIT FILE Version " & MAPPER_VERSION & "                 Time=" & nowdt
    End If
    
    If g_no_output Then Exit Sub

    outfilename = winloadpath & "\" & TRANSP_FILENAME           'load_me\transparent.txt
    outfile = FreeFile
    Open outfilename For Append As #outfile                     'add to existing
    
    Exit Sub

errHandler:
    g_util.ThrowError "OpenOutputFiles"
    Resume  'debug
End Sub
Private Sub CloseOutputFiles()
    If (outfile <> 0) Then Close #outfile
    If (outfile2 <> 0) Then Close #outfile2
    
    If (dbugfile <> 0) Then
        Close #dbugfile
        dbugfile = 0                                            'stop writing to debug file
    End If
End Sub

Private Function ConvertDate(d As String) As String

'2012-07-10 13:53:00.000

    ConvertDate = Mid$(d, 1, 4) & Mid$(d, 6, 2) & Mid$(d, 9, 2) & Mid$(d, 12, 2) & Mid$(d, 15, 2) & Mid$(d, 18, 2)
End Function


Private Sub DeleteOldLogs()
    'There are no more log files; history has been added to the event log
End Sub

Private Sub DeleteOldChartItems()
    On Error GoTo errHandler
    
    Dim sql As String
    Dim dt As Date

    dt = DateAdd("d", -CHART_ITEM_LIFE, Date)
    sql = "DELETE FROM CHART_ITEM WHERE EVENT_DATETIME < " & g_dbutil.SQL_DateTime(dt)
    'Debug.Print sql
    g_cnADO.Execute sql
    Exit Sub
    
errHandler:
    g_util.ThrowError "DeleteOldChartItems"
    Resume  'debug
End Sub


Public Sub AddLogEntry(event_type As EventLogType, msg As String, _
    Optional event_category As EventLogCategory = EVENT_CATEGORY_NONE _
)
    On Error GoTo errHandler

    dprint msg                      'copy to debug log

    g_event.AddEventLogEntry EVENT_SOURCE_TRANSPARENT_MAPPING, event_type, event_category, _
        msg, gLogSourceText, 0, gLogUnitID, gLogEncounterID
    Exit Sub
    
errHandler:
    'no message boxes allowed (this is a command-line program)
    Debug.Print Err.Description
End Sub

Public Sub LogInfo(s As String, Optional event_category As EventLogCategory = EVENT_CATEGORY_NONE)
    AddLogEntry EVENT_TYPE_INFO, s, event_category
End Sub

Public Sub LogWarning(s As String, Optional event_category As EventLogCategory = EVENT_CATEGORY_VALIDATION)
    AddLogEntry EVENT_TYPE_WARNING, s, event_category
End Sub

Public Sub LogError(s As String, Optional event_category As EventLogCategory = EVENT_CATEGORY_VALIDATION)
    AddLogEntry EVENT_TYPE_ERROR, s, event_category
End Sub


Private Sub UnitDefaultTyping()
    Dim rs As New Recordset
    Dim sql As String
    Dim i, j As Integer
    Dim indary() As String
    
    sql = "select u.unit_id,up.unit_param_id,up.default_admission_profile from unit_param up inner join unit u on (up.unit_id = u.unit_id)"
    sql = sql & " where u.is_active='Y' and u.USE_TRANSPARENT_CLASSIFICATION='Y'"
    sql = sql & " and getdate() between up.effective_datetime and up.expiration_datetime"
    sql = sql & " and up.default_admission_profile>0"
    rs.Open sql, g_cnADO
    Do While Not rs.EOF
        numunit = numunit + 1
        ReDim Preserve definds(0 To numunit)
        definds(numunit).uid = rs(0)
        definds(numunit).upid = rs(1)
        definds(numunit).admp = rs(2)
        For j = 1 To MAX_INDS
            definds(numunit).indstr = definds(numunit).indstr & "N"
        Next j
        rs.MoveNext
    Loop
        
    rs.Close
    
    For i = 1 To numunit
        sql = "select indicators from patient_profile where unit_param_id=" & definds(i).upid & " and profile_number=" & definds(i).admp
        rs.Open sql, g_cnADO
        If Not rs.EOF Then
            definds(i).indicators = rs(0)
        End If
        rs.Close
    Next i
    
    For i = 1 To numunit
        indary = Split(definds(i).indicators, ",")
        For j = 0 To UBound(indary)
            If IsNumeric(indary(j)) Then
                Mid$(definds(i).indstr, indary(j), 1) = "Y"
            End If
        Next j
    Next i
    Set rs = Nothing

End Sub

