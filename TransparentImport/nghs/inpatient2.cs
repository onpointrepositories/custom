﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;                     // for LINQ; add a reference
using System.Data.SqlClient;                // for SqlConnection
using System.IO;                            // for Directory, Path, Stream
using PfsShared;                            // add a reference to Shared2 project

// ============================================================================
// NGHS Inpatient 2.0 transparent mapping -- GOES HERE --
// (The code below is a sample from Sharp)
// ============================================================================
// This processes one patient.  Only one database query is used.  :)
//
// All search functions use exact match for category, description and field name.
// Codes and results can be a single word or a list of words.
// Codes default to exact match; prefix with CODE_CODE_LIKE_PREFIX if desired (single word only)
// Results default to like matching; prefix with RESULT_EXACT_MATCH_PREFIX if desired (single word only)
//
// All searches are case insensitive.
//
//
namespace TransparentMapping
{
    class Inpatient2
    {
        private const int MAX_INDS = 120;
        private const int MAX_PROCS = 20;
        private const int MAX_OCI = 32;

        private const string CODE_LIKE_PREFIX = "%!";       // use to make codes %like%
        private const string EXACT_MATCH_PREFIX = "&!";     // use to make desc/result exact match
        private const string CHAR_COMMA = "||";             // use to insert literal comma in word list

        private struct indicator_data {
            public bool     is_checked;     
            public int      radio_group;       
        }

        private struct proc_data {                      
            public int      procedure_number;
            public DateTime start;   
            public DateTime finish;
        }

        // These are database CHART_ITEMs for this patient
        private CHART_ITEM[] _chart_items_since_admission;
        private CHART_ITEM[] _chart_items_since_unit_arrival;
        private CHART_ITEM[] _chart_items_during_pull_period;
        private CHART_ITEM[] _chart_items_past_24hrs;
        private CHART_ITEM[] _chart_items_past_12hrs;
        // These are database PROCEDURE_EVENTs for this patient
        private PROCEDURE_EVENT[] _procedure_events;

        private indicator_data[] _inds;
        private List<proc_data> _procs;
        private List<proc_data> _ocinds; // outcomes indicators

        private const int DEFAULT_BUCKET_SIZE = 20;       //min

        private PatientInfo _pat;
        private int  _bucket_size = DEFAULT_BUCKET_SIZE;
        private bool is_default;
        private string txarea;


        private enum SearchDepth {
            SearchDefault,
            SearchPullRange,            //search within the current pull     -- default
            SearchSinceArrival,         //search since arrival to the unit
            SearchSinceAdmission,        //search everything since admission to the hospital
            SearchPast24Hrs,
            SearchPast12Hrs
        }

        private enum CountMode {
            CountAll,
            CountFirst                  //stop after one is found
        }

        private enum GetValueMode {
            GetTotal,
            GetMax,
            GetLast
        }

        enum Frequencies {
            QNONE,
            Q4H,
            Q2H,
            Q1H,
            Q30M
        }

        private struct fmapRow {
            public double   los_high;                       //the LOS being testing
            public int[]    freq;                           //the count required for each Q value
        }
        private List<fmapRow> _freq_map;                    //1,2,4,8,12,24 hours


     
        //
        // This is the main entry point
        //
        public void ProcessPatient(PatientInfo pat)
        {
            bool use_default = false;
            _pat = pat;

            InitIndicators(); // sets is_default
            InitProcs();
            if (! is_default)
                {
                LoadFreqTable();
                LoadPatientChart();
                SetMinimumsByUnit();
                Check_1_2_3_4();
                Check_5();
                Check_6_7();
                Check_8();
                Check_9();
                Check_10_11();
                Check_12_13();
                Check_14();
                Check_15_16_17_18();
                Check_19();
                Check_20();
                Check_21_22();
                Check_23();
                Check_24();
                Check_Other();
                Check_TxArea();
            }
            AtLeastOneADL();
            HighestIndicatorInEachGroupWins();

            if (!is_default)
            {
                //CheckProcs();
                //CheckOutcomes();
            }

            if (Program.g_no_output) return;
            if (_pat.default_ptype > DeterminePtypeOfIndicators())
            { // if the default pt type is higher than the pt type of this class
                // then if there is a default classification in the past 16 hrs
                // then make these indicators the default indicators.
                use_default = ExistDefaultInPast16hrs();
                if (use_default)
                {
                    Program.VerboseAudit("Default indicators will be used" );
                }
            }
            OutputClass(use_default);
            //OutputProcs();
            //OutputOutcomes();
        }

       
        private void InitIndicators()
        {
            // Make a new (empty) indicator array
            _inds = new indicator_data[MAX_INDS + 1];           // This 1 based so add one
            is_default = false;
            txarea = "";
            if (_pat.los_hours <= 4.0) {
                is_default = true;
                Program.VerboseAudit("Patient was here 4 hrs or less. Will receive default indicators " + _pat.default_inds_str);
                foreach (var ind in _pat.default_inds) {
                    if (ind <= _inds.GetUpperBound(0)) {
                        _inds[ind].is_checked = true;
                    }
                }
            }


            // get indicator radio groups from the database
            // ** (This database access can be replaced once we have a C# methodolgy cache)
            var db = PFSUtility.NewPfsDataContext();
            var query = from idef in db.INDICATOR_DEFINITIONs
                        where (idef.METHODOLOGY_ID == _pat.meth_id)
                        select idef;
            foreach (var idef in query) {
                if (idef.INDICATOR_NUMBER <= _inds.GetUpperBound(0)) {
                    // (convert nulls to zero)
                    _inds[idef.INDICATOR_NUMBER].radio_group = PFSUtility.DBToInt(idef.RADIO_GROUP);
                }
            }
        }

        private void InitProcs()
        {
            _procs = new List<proc_data>();
            _ocinds = new List<proc_data>();
        }

        private fmapRow LoadFreqTableRow(double los_high, string values)
        {
            fmapRow fmrow;
            
            fmrow.los_high = los_high;
            fmrow.freq = new int[5];

            var arr = values.Split(',');
            for (int i = 0; i <= fmrow.freq.GetUpperBound(0) ; i++ )
            {
                fmrow.freq[i] = arr[i].ToInteger();
            }
            return fmrow;
        }

        // These are the number of times you need to do something during a certain time period (LOS)
        // in order for it to be considered one of the frequencies listed.
        private void LoadFreqTable()
        {
            _freq_map = new List<fmapRow>();
            //                              LOS,  None Q4h Q2h Q1h Q30m
            _freq_map.Add(LoadFreqTableRow(1, "    0,  0,  0,  1,  2"));
            _freq_map.Add(LoadFreqTableRow(2, "    0,  0,  1,  2,  3"));
            _freq_map.Add(LoadFreqTableRow(4, "    0,  1,  3,  5,  8"));
            _freq_map.Add(LoadFreqTableRow(6, "    0,  2,  3,  6,  10"));
            _freq_map.Add(LoadFreqTableRow(8, "    0,  2,  5,  9,  15"));
            _freq_map.Add(LoadFreqTableRow(12, "   0,  3,  6,  12, 20"));
            _freq_map.Add(LoadFreqTableRow(16, "   0,  4,  9,  18, 30"));
            _freq_map.Add(LoadFreqTableRow(24, "   0,  5,  12, 24, 40"));
            _freq_map.Add(LoadFreqTableRow(9999, " 0,  5,  12, 24, 40"));


            //to read this table: Using 30-min buckets. if you have at least 12 asessmenets in a 12-hour period, you get Q1h
            //(LOS=12, column Q1h has a count of 12
        }

        private Frequencies FreqForCount(double los_hours, int count) 
        {
            foreach(var fmrow in _freq_map) {
                if (los_hours <= fmrow.los_high) {
                    // Option: pro-rate the count upward if the LOS is less than the row we are interested in.
                    //         This will bump the count to what it might have been at the full LOS.
                    // Note: truncate the result; rounding inflates the value too much.
                    int prorated_count = (int)((fmrow.los_high / los_hours) * count);

                    // foreach goes low to high; go from high to low instead
                    for (int j = (int)Frequencies.Q30M; (j >= (int)Frequencies.QNONE); j--) { //search right to left
                        if (prorated_count >= fmrow.freq[j]) {
                            return (Frequencies)j;
                        }
                    } // next j
                }
            }
            
            return Frequencies.QNONE;
        }

        //
        // Load patient chart from the database (one DB access)
        // Load everything since admission
        //
        private void LoadPatientChart()
        {
            // Get the entire patient chart (all units and dates for this patient)
            var db = PFSUtility.NewPfsDataContext();
            var query = from item in db.CHART_ITEMs
                        where (item.ENCOUNTER_ID == _pat.encounter_id)
                        select item;
            // Save the result
            _chart_items_since_admission = query.ToArray();

            // Convert all results to lower case for case insensitive comparisons
            foreach (var item in _chart_items_since_admission) {
                if (item.CATEGORY != null) item.CATEGORY = item.CATEGORY.ToLower();
                if (item.CODE != null) item.CODE = item.CODE.ToLower();
                if (item.DESCRIPTION != null) item.DESCRIPTION = item.DESCRIPTION.ToLower();
                if (item.FIELD_NAME != null) item.FIELD_NAME = item.FIELD_NAME.ToLower();
                if (item.RESULT != null) item.RESULT = item.RESULT.ToLower();
            }

            // Prepare 3 more versions of the chart
            var query2 = from item in _chart_items_since_admission
                    where(item.EVENT_DATETIME >= _pat.unit_arrival) && (item.EVENT_DATETIME <= _pat.pull_finish)
                    select item;
            _chart_items_since_unit_arrival = query2.ToArray();

            query2 = from item in _chart_items_since_admission
                     where (item.EVENT_DATETIME >= _pat.pull_start) && (item.EVENT_DATETIME <= _pat.pull_finish)
                     select item;
            _chart_items_during_pull_period = query2.ToArray();

            query2 = from item in _chart_items_since_admission
                     where (item.EVENT_DATETIME >= _pat.pull_finish.AddDays(-1)) && (item.EVENT_DATETIME <= _pat.pull_finish)
                     select item;
            _chart_items_past_24hrs = query2.ToArray();

            query2 = from item in _chart_items_since_admission
                     where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-12)) && (item.EVENT_DATETIME <= _pat.pull_finish)
                     select item;
            _chart_items_past_12hrs = query2.ToArray();
        }

        // Delay looking for patient procedures until they are wanted
        private void LoadPatientProceduresIfNeeded()
        {
            if (_procedure_events != null) return;

            var db = PFSUtility.NewPfsDataContext();
            var query = from proc in db.PROCEDURE_EVENTs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                        select proc;
            _procedure_events = query.ToArray();
        }

        
        // Started a new chart item query (of a certain depth)
        // Default is the chart during the pull range.  Options for unit arrival and admission to hospital.
        private IEnumerable<CHART_ITEM> StartNewQuery()
        {
            return StartNewQuery(SearchDepth.SearchDefault);
        }
        private IEnumerable<CHART_ITEM> StartNewQuery(SearchDepth search_depth)
        {
            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    return (from item in _chart_items_during_pull_period select item);
                case SearchDepth.SearchSinceArrival:
                    return (from item in _chart_items_since_unit_arrival select item);
                case SearchDepth.SearchSinceAdmission:
                    return (from item in _chart_items_since_admission select item);
                case SearchDepth.SearchPast24Hrs:
                    return (from item in _chart_items_past_24hrs select item);
                case SearchDepth.SearchPast12Hrs:
                    return (from item in _chart_items_past_12hrs select item);
            }
            return null;
        }

        // Is this search term a word or a list of words?
        private bool ValueIsAList(string s) 
        {
            return (s != null) && s.Contains(',');                //is this a comma-separated list?
        }

        // Break apart a wordlist and prepare its members
        private string[] SplitOnCommaAndPrepareElements(string s)
        {
            // If a word list has a space after each comma, we will get leading blanks in each term
            var arr = s.Split(',');
            for (int i = 0; i <= arr.GetUpperBound(0); i++) {
                arr[i] = arr[i].Trim();                         // get rid of leading blanks
                arr[i] = arr[i].Replace(CHAR_COMMA, ",");       // insert a literal comma into search word
                arr[i] = arr[i].ToLower();                      // convert for case-insensitive comparisons
            }
            return arr;
        }

        // Add a basic chart item filter to a LINQ query
        //
        // cat = exact match
        // code_list = exact match; optional "like" match
        // desc_list = "like" match; optional exact match
        // field = exact match
        // result_list = "like" match; optional exact match
        //
        // These are comma-separated lists.  
        // Search for a literal comma by using CHAR_COMMA in the string.
        private IEnumerable<CHART_ITEM> AndItemFilter(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list) 
        {
            // Converted all values to lower case for case-insensitive comparisons.
            if (!String.IsNullOrEmpty(cat)) {
                cat = cat.ToLower();
                query = query.Where(e => e.CATEGORY == cat);
            }

            if (!String.IsNullOrEmpty(code_list)) {
                code_list = code_list.ToLower();
                if (code_list.Left(2) == CODE_LIKE_PREFIX) {            // override for "like" match?
                    query = query.Where(e => e.CODE.Contains(code_list.Substring(2)));
                } else if (ValueIsAList(code_list)) {
                    query = AndCodeInList(query, code_list);            // find one of the words
                } else {
                    query = query.Where(e => e.CODE == code_list);      // find this word
                }
            }
            
            if (!String.IsNullOrEmpty(desc_list)) {
                desc_list = desc_list.ToLower();
                if (desc_list.Left(2) == EXACT_MATCH_PREFIX) {          // exact match override?
                    query = query.Where(e => e.DESCRIPTION == desc_list.Substring(2));
                } else if (ValueIsAList(desc_list)) {
                    query = AndDescriptionInList(query, desc_list);
                } else {
                    desc_list = desc_list.Replace(CHAR_COMMA, ",");     // look for literal comma requests
                    query = query.Where(e => e.DESCRIPTION.Contains(desc_list));
                }
            }
            
            if (!String.IsNullOrEmpty(field)) {
                field = field.ToLower();
                query = query.Where(e => e.FIELD_NAME == field);
            }
            
            if (!String.IsNullOrEmpty(result_list)) {
                query = query.Where(e => e.RESULT != null);
                result_list = result_list.ToLower();                        // make all results case insensitve
                if (result_list.Left(2) == EXACT_MATCH_PREFIX) {     // exact match override?
                    query = query.Where(e => e.RESULT == result_list.Substring(2));
                } else if (ValueIsAList(result_list)) {
                    query = AndResultInList(query, result_list);
                } else {
                    result_list = result_list.Replace(CHAR_COMMA, ",");     // look for literal comma requests
                    query = query.Where(e => e.RESULT.Contains(result_list));
                }
            }
            return query;
        }

        // Look for a code that is one of the words in the list.
        private IEnumerable<CHART_ITEM> AndCodeInList(IEnumerable<CHART_ITEM> query, string code_list) 
        {
            if (String.IsNullOrEmpty(code_list)) return query;
            
            var arr = SplitOnCommaAndPrepareElements(code_list);
            return query.Where(e => arr.Contains(e.CODE));              // exact match
        }

//string[] myStrings = { "a", "b", "c" };
//string checkThis = "abc";

//if (myStrings.Any(checkThis.Contains))
//{
//    MessageBox.Show("checkThis contains a string from string array myStrings.");
//}

        // Look for a description that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndDescriptionInList(IEnumerable<CHART_ITEM> query, string desc_list) 
        {
            if (String.IsNullOrEmpty(desc_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(desc_list);
//            return query.Where(e => e.DESCRIPTION.ContainsAny(arr));    // "exact" match
            return query.Where(e => arr.Any(x => x.Contains(e.DESCRIPTION))); // "like" match
        }

        // Look for a result that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultInList(IEnumerable<CHART_ITEM> query, string result_list) 
        {
            if (String.IsNullOrEmpty(result_list)) return query;
            
            var arr = SplitOnCommaAndPrepareElements(result_list);
//            return query.Where(e => e.RESULT.ContainsAny(arr));         // "exact" match
            return query.Where(e => arr.Any(x => x.Contains(e.RESULT))); // "like" match
        }

        // Look for a result that does not contain one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultNotInList(IEnumerable<CHART_ITEM> query, string result_list)
        {
            if (String.IsNullOrEmpty(result_list)) return query;
            
            var arr = SplitOnCommaAndPrepareElements(result_list);
//            return query.Where(e => !arr.Any(e.RESULT.Contains));        // 
            return query.Where(e => !arr.Any(x => x.Contains(e.RESULT))); // "like" match
        }

        private string DescribeSearchDepth(SearchDepth search_depth) 
        {
            string result = "";
            
            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    //result = "in pull range"
                    result = "";                         //be quiet since this is default
                    break;
                case SearchDepth.SearchSinceArrival:
                    result = "since arrival to unit";
                    break;
                case SearchDepth.SearchSinceAdmission:
                    result = "since admission";
                    break;
                case SearchDepth.SearchPast24Hrs:
                    result = "past 24 hours";
                    break;
                case SearchDepth.SearchPast12Hrs:
                    result = "past 12 hours";
                    break;
            }
            
            return result;
        }

        // Describe a query that has already been run
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Make a simple query and describe the results
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            var arr = query.ToArray();
            return Describe(arr, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private string LookingFor(string so_far, string what, string op, string value)
        {
            string result;
            if (String.IsNullOrEmpty(value)) return so_far;

            if (so_far == "")
                result = "looking for " + what;
            else
                result = so_far + " and " + what;

            if (value.Left(2) == EXACT_MATCH_PREFIX)
                result += "='" + value.Substring(2) + "'";
            else if (value.Left(2) == CODE_LIKE_PREFIX)
                result += " contains " + "'" + value.Substring(2) + "'";
            else if (ValueIsAList(value))
                result += " in '" + value + "'";
            else
                result += op + "'" + value + "'";
            
            return result;
        }
        private string Describe(CHART_ITEM[] arr, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string result = "";
            result = LookingFor(result, "cat", "=", cat);
            result = LookingFor(result, "code", "=", code_list);
            result = LookingFor(result, "desc", " contains ", desc_list);
            result = LookingFor(result, "field", "=", field);
            result = LookingFor(result, "result", " contains ", result_list);

            if (arr.Count() == 0) {
                result += "; not found " + DescribeSearchDepth(search_depth);
            } else {
                var e = arr[0];
                result = "FOUND: " + result + "; found";
                // We might have searched for a pattern or word list in several fields - show what was found
                if (e.CATEGORY != null)     result += " cat='" + e.CATEGORY + "'";
                if (e.CODE != null)         result += " code='" + e.CODE + "'";
                if (e.DESCRIPTION != null)  result += " desc='" + e.DESCRIPTION + "'";
                if (e.FIELD_NAME != null)   result += " field='" + e.FIELD_NAME + "'";
                if (e.RESULT != null)       result += " result='" + e.RESULT + "'";
                // Are there more results?  Just say how many; we aren't going to list them.
                if (arr.Count() == 2) {
                    result += " (1 more result)";
                } else if (arr.Count() > 2) {
                    result += " (" + (arr.Count() - 1) + " more results)";
                }
            }
            
            return result;
        }


        //Set an indicator for this reason (low level)
        private void SetInd(int inum, string reason)
        {
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Set Ind #" + inum + ": " + reason);          //already set - repeat for verbose only
            } else {
                _inds[inum].is_checked = true;
                Program.Audit("Set Ind #" + inum + ": " + reason);
            }
        }

        //Set several indicators for this reason (low level)  inum_list is comma delimited string of ints
        private void SetMultiInd(string inum_list, string reason)
        {
            var arr = inum_list.Split(',');
            foreach (string ind in arr)
            {
                _inds[ind.ToInteger()].is_checked = true;
            }
            Program.Audit("Set Ind #s " + inum_list + ": " + reason);
        }
        //Clear an indicator for this reason (low level)
        private void ClrInd(int inum, string reason)
        {
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Clr Ind #" + inum + ": " + reason);          //already clear - repeat verbose only
            } else {
                _inds[inum].is_checked = false;
                Program.Audit("Clr Ind #" + inum + ": " + reason);
            }
        }

        // Count how many items meet the conditions
        // All items are listed
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountItems(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            int count = query.Count();

            // always return what was found
            found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
            // echo the result?
            if (trace) Program.VerboseAudit(found_what);
            
            return count;
        }

        // Count how many items meet the conditions
        // This has the option to list every entry or give a total
        private int CountResultInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what="";
 
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);

            var arr = SplitOnCommaAndPrepareElements(result_list);

            foreach (var item in query) {
                // Figure out which of the search words were found
                var s = item.RESULT.ContainsWhich(arr);
                found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' code='" + item.CODE + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "'";
                if (trace) Program.VerboseAudit(found_what);
                count++;
                if (count_mode != CountMode.CountAll) break;        //stop counting
            }
            
            if (count > 0) {
                //We already printed what was found; maybe add how many?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }
            
            return count;
        }

        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (ValueIsAList(result_list)) {
                return CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what);
            } else {
                return CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what);
            }
        }

        // Is there an item with this result?
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list) 
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth) 
        {
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContains(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what) 
        {
            if (ValueIsAList(result_list)) {
                return (CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            } else {
                return (CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
            }
        }

        // Count how many items meet the conditions, with none of the results in result_list.
        // This has the option to say how many were found rather than list all.
        private int CountResultNotInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what) 
        {
            int count = 0, rec_count = 0;
            bool found_one;

            found_what = "";
            var arr = SplitOnCommaAndPrepareElements(result_list);         // also deals with CHAR_COMMA
            
            //Do not filter by result here; we need all results
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");
            
            foreach(var item in query) {
                rec_count++;
                //Look for each search word in the result
                found_one = false;
                
                foreach (string s in arr) {
                    if (String.Equals(item.RESULT, s)) {
                        found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "' -- ignore this";
                        Program.VerboseAudit(found_what);
                        found_one = true;
                        break;
                    }
                }

                if (! found_one) {
                    //none of the words were found - good!
                    found_what = Describe(cat, code_list, desc_list, field, "", search_depth) + " -- does not contain '" + result_list + "'";
                    Program.VerboseAudit(found_what);
                    count++;
                    if (count_mode != CountMode.CountAll) break;
                }
            }
            
            if (count > 0) {
                //We already printed what was found; maybe show the total?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else if (rec_count > 0) {
                //We already printed what we ignored
            } else {
                //Print what did not exist
                found_what = Describe(cat, code_list, desc_list, field, "", search_depth);      //not found
                Program.VerboseAudit(found_what);
            }
            
            return count;
        }

        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, string found_what) 
        {
            // Search in the pull range with trace on
            return CountResultDoesNotContain(cat, code_list, desc_list, field, result_list, out found_what, SearchDepth.SearchDefault, true);
        }
        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, out string found_what, SearchDepth search_depth, bool trace)
        {
            return CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0;
        }

        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, string found_what)
        {
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, out found_what, SearchDepth.SearchDefault, true);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, out string found_what, SearchDepth search_depth, bool trace)
        {
            return (CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0);
        }

        // Set the indicator if the conditions are met
        //
        private void SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already set
            if (_inds[inum].is_checked) return;
            
            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what)) {
                SetInd(inum, found_what);                          //echo here - found
            } else {
                Program.VerboseAudit(found_what);                  //and here - not found
            }
        }

        // Set the indicator if the result does not contain any of the words in result_list
        //
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultDoesNotContain(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {   
            string found_what;
            //avoid more queries if the indicator is already set
            if (_inds[inum].is_checked) return;
            
            //Turn trace off for ResultDoesNotContain() and echo what was set below with SetInd
            if (ResultDoesNotContain(cat, code_list, desc_list, field, result_list, out found_what, search_depth, false)) {
                SetInd(inum, found_what);                           //echo here - not found
            } else {
                Program.VerboseAudit(found_what);                   //and here - found
            }
        }

        // Clear the indicator if the result contains one of the words in the result_list
        //
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already clear
            if (! _inds[inum].is_checked) return;
            
            //Turn trace off for ResultContains() and echo what was set below with SetInd
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what)) {
                ClrInd(inum, found_what);                           //echo here - found
            } else {
                Program.VerboseAudit(found_what);                   //and here - not found
            }
        }


        //These slightly smaller functions are meant for places where you aren't really looking for a chart result
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            return (CountResultContains(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
        }

        // Variations with optional parameters (can switch to optional parameters in VS 2010)
        //
        private void SetIndIfFound(int inum, string cat)
        {
            SetIndIfResultContains(inum, cat, "", "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list)
        {
            SetIndIfResultContains(inum, cat, code_list, "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private void ClrIndIfFound(int inum, string cat)
        {
            ClrIndIfResultContains(inum, cat, "","","","", SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }


        // Get the max/total value from a result (usually in the middle of the text)
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetIntValue(get_mode, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            int value;
            int result = 0;
            bool found_one = false;

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            
            //Look for a number in the result
            
            foreach(var item in query) {
                // RESULT might be null so convert to empty string so Split won't blow up
                var arr = PFSUtility.DBToString(item.RESULT).Split(' ');
                foreach (string s in arr) {
                    //Look for 1st character numeric; IsNumeric("60min") will fail so look at 1st char only
                    if (s.Left(1).IsNumeric()) {
                        value = (int)s.Val();                           //Use Val; ToInteger will error on "60min"
                        switch (get_mode) {
                            case GetValueMode.GetMax:
                                result = Math.Max(value, result);       //max
                                break;
                            case GetValueMode.GetTotal:
                                result += value;                        //total
                                break;
                            case GetValueMode.GetLast:
                                result = value;                         //last
                                break;
                        }
                        
                        // print what we are searching for (the first time)
                        if (! found_one) {
                            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
                        }
                        found_one = true;
                        // print each value found
                        Program.VerboseAudit("  found numeric value " + result);
                        //Keep going in case there are more
                    }
                } 
            }
            
            if (! found_one) {
                //show what was not found
                Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
            }
            
            return result;
        }

        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetMaxValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return GetIntValue(GetValueMode.GetMax, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetTotalValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return GetIntValue(GetValueMode.GetTotal, cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Get a result; returns true if found with return_result
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result)
        {
            return GetResult(cat, code_list, desc_list, field, out return_result, SearchDepth.SearchDefault);
        }
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0) {
                return_result = PFSUtility.DBToString(query.First().RESULT);
            } else {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }
        private void GetResultforOther(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "Other (comment)");
            return_result="";
            foreach (var item in query)
            {
                if (!String.Equals(item.RESULT, "Other (comment)"))
                {
                    return_result = item.RESULT;
                    //Program.VerboseAudit(found_what);
                    break;
                }

            }
            return;
        }
        private bool GetEVDT(string cat, string code_list, string desc_list, string field, out DateTime evdt)
        {
            return GetEVDT(cat, code_list, desc_list, field, out evdt, SearchDepth.SearchDefault);
        }
        private bool GetEVDT(string cat, string code_list, string desc_list, string field, out DateTime evdt, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0)
            {
                evdt = PFSUtility.DBToDateTime(query.Max(x => x.EVENT_DATETIME));
            }
            else
            {
                evdt = DateTime.MinValue;
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (evdt > DateTime.MinValue);
        }

        //======================================================================================
        // Mapping starts here
        //======================================================================================
        private void SetMinimumsByUnit()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("0. Set Minimums by Unit");
            Program.VerboseAudit("---------------");

            var list1 = new List<int> { 530265,33016409,415 }; // N$G C2A
            if (list1.Contains(_pat.unit_id))
            {
                SetMultiInd("2,16", "Cardiology: S2E, S3E, S3D");
            }

            var list2 = new List<int> { 290,330,380,440,27218005,27218006,33016408,111302742,111302743,111302745};
            if (list2.Contains(_pat.unit_id))
            {
                if ((_pat.unit_id != 440) || 
                    (_pat.unit_id == 440) && (_pat.room != "5470") && (_pat.room != "5471"))
                {
                    SetMultiInd("2,15", "Med-Surg: S1B, S4E, S4D, S5E, N4G, N5G, N6G, Brasel M/S");
                }
            }
            var list3 = new List<int> { 355,305,111302744 };
            if (list3.Contains(_pat.unit_id))
            {
                SetMultiInd("3,17", "Critical Care: C2A, C2B, C3A, C3B, N3G, Brasel ICU");
            }

            if (_pat.unit_id == 465)
            {
                SetMultiInd("3,5,15", "Rehab: S4B");
            }

            if (_pat.unit_id == 265)
            {
                SetMultiInd("2,15", "Pediatric: S3B");
            }

//Cardiology: S2E, S3E, S3D 
//•       Assessment - Q2H 
//•       ADL - Partial Assist
//Med-Surg: S1B, S4E, S4D, S5E (except as noted below), N4G, N5G, N6G 
//•       Assessment - Q4H 
//•       ADL - Partial Assit
//Critical Care: C2A, C2B, C3A, C3B, N3G
//•       Assessment - Q1H 
//•       ADL - Extended Assit
//Rehab S4B
//•       Assessment - Q4H 
//•       ADL - Extended Assist 
//•       ADL - Rehabilitative
//Pediatric S3B
//•       Assessment - Q4H 
//•       ADL - Partial Assist (except for patients under 4 yrs - they'll stay ADL - complete)
//S5E (5470 and 5471 rooms only) No Default
//            339,313,303,379 Exception rms, 308,363,373
//'137 - Rehab S4B
//'302 - HFU   S3D
//'303 - M/S OF    S4D
//'308 - Ortho/Neur    N4G
//'310 - M/B   S2B
//'310 - M/B   S2D
//'312 - PEDI  S3B
//'313 - Med/Surg  S4E
//'319 - L&D   S1C
//'326 - ED    ER
//'330 - CCU   C3A
//'330 - CCU   C3B
//'331 - CVU   C2A
//'331 - CVU   C2B
//'339 - Medical   S1B
//'352 - ICU   N3G
//'363 - Surgical  N5G
//'364 - PCCU  S3E
//'370 - PCVU  S2E
//'373 - Surg/Bari N6G
//'374 - EOU   C1A
//'379 - Oncology  S5D
//'379 - Oncology  S5E

        }

        private void Check_1_2_3_4()
        {
            string found_what;
            string fndlist;
            bool hygiene = false;
            bool mobility = false;
            bool nutrassist = false;
            bool toilet = false;
            int adlcat_count = 0;
            bool tot_bath = false;
            bool tot_feed = false;
            int ct = 0;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("1. ADL Self");
            Program.VerboseAudit("2. ADL Assist");
            Program.VerboseAudit("3. ADL Extended");
            Program.VerboseAudit("4. ADL Complete");
            Program.VerboseAudit("---------------");

            if (_pat.age <= 3.0)
            {
                SetInd(4, "Age less than 4 years");
            }
            if (_inds[4].is_checked) return;


            // 4. COMPLETE CARE
            fndlist = "FND100146,FND101245,FND335,FND237,FND235,FND102085,FND101252";
            SetIndIfResultContains(4, "LOC001", fndlist, "", "", "");
            SetIndIfResultContains(4, "LOCORNT", "FND101252", "", "", "");
            fndlist = "FND101254,FND101246,FND101253,FND101251,FND102085";
            SetIndIfResultContains(4, "NUR02", fndlist, "", "", "");
            SetIndIfResultContains(4, "ORL001", "FND007", "", "", "");
            fndlist = "FND106792,FND101253";
            SetIndIfResultContains(4, "PEENT8", fndlist, "", "", "");
            fndlist = "FND101017,FND101920";
            SetIndIfResultContains(4, "PRESP17", fndlist, "", "", "");
            if (_pat.is_ICU)
            {
                fndlist = "FND103125";
                SetIndIfResultContains(4, "PRESP17", fndlist, "", "", "");
            }
            if (_inds[4].is_checked) return;

            fndlist = "FND104513";
            SetIndIfResultContains(4, "PSYSOC", fndlist, "", "", "");
            fndlist = "FND107412,FND107413,FND107414,FND107415,FND107446,FND107417,FND107418,FND107419,FND107420,FND107421,";
            SetIndIfResultContains(4, "RASS1", fndlist, "", "", "");
            fndlist = "MCK_QMR111,MCK_QMR112,MCK_QMR113,MCK_QMR114,MCK_QMR115";
            SetIndIfResultContains(4, "REPORTSTAT", fndlist, "", "", "");
            fndlist = "FND237,FND335";
            SetIndIfResultContains(4, "RFR001", fndlist, "", "", "");
            fndlist = "FND107422,FND107423,FND107424,FND107425,FND107426,FND107427,FND107428";
            SetIndIfResultContains(4, "SEDVAC1", fndlist, "", "", "");
            fndlist = "FND102085";
            SetIndIfResultContains(4, "SPNEURO", fndlist, "", "", "");
            if (_inds[4].is_checked) return;

            fndlist = "FND102085";
            SetIndIfResultContains(4, "SPNEURO", fndlist, "", "", "");
            fndlist = "FND101017";
            SetIndIfResultContains(4, "SPRES677", fndlist, "", "", "");
            fndlist = "FND102085";
            SetIndIfResultContains(4, "SPNEURO", fndlist, "", "", "");
            fndlist = "FND101245,FND335,FND237";
            SetIndIfResultContains(4, "SURGLOC", fndlist, "", "", "");
            if (_inds[4].is_checked) return;

            fndlist = "FND100006,FND007,FND100928,FND102434,FND106399";
            if (ResultContains("BTH001", fndlist, "", "", ""))
            {
                tot_bath = true;
                hygiene = true;
            }
            fndlist = "FND106399";
            if (ResultContains("HYG002", fndlist, "", "", ""))
            {
                tot_bath = true;
                hygiene = true;
            }
            fndlist = "FND026";
            if (ResultContains("DAILY14", fndlist, "", "", ""))
            {
                tot_feed = true;
                nutrassist = true;
            }
            fndlist = "FND106486";
            if (ResultContains("DAILY19", fndlist, "", "", ""))
            {
                tot_feed = true;
                nutrassist = true;
            }
            fndlist = "FND102929,FND763,FND027,FND026";
            if (ResultContains("EAT001", fndlist, "", "", ""))
            {
                tot_feed = true;
                nutrassist = true;
            }
            fndlist = "FND102929";
            if (ResultContains("FED001", fndlist, "", "", "")) 
            {
                tot_feed = true;
                nutrassist = true; 
            }
            fndlist = "FND102880";
            if (ResultContains("PEENT8", fndlist, "", "", ""))
            {
                tot_feed = true;
                nutrassist = true;
            }
            fndlist = "FND110431";
            if (ResultContains("SWALSCRN3", fndlist, "", "", ""))
            {
                tot_feed = true;
                nutrassist = true;
            }
            fndlist = "FND106547,FND102893";
            if (ResultContains("TUBEFEED11", fndlist, "", "", ""))
            {
                tot_feed = true;
                nutrassist = true;
            }
            fndlist = "FND766,FND038";
            if (ResultContains("TUBEFEED7", fndlist, "", "", "")) 
            {
                tot_feed = true;
                nutrassist = true;
            }

            if (tot_bath && tot_feed)
            {
                SetInd(4, "Total Bath + Total Feed.");
            }

            if (_inds[4].is_checked) return;
            
            // 3 EXTEMDED
            fndlist = "FND195,FND103684,FND100269";
            SetIndIfResultContains(3, "CMP001", fndlist, "", "", "");
            fndlist = "FND101430,FND103684,FND195";
            SetIndIfResultContains(3, "PEENT1", fndlist, "", "", "");
            fndlist = "FND102870";
            SetIndIfResultContains(3, "PEENT8", fndlist, "", "", "");
            if (_inds[3].is_checked) return;

            fndlist = "FND014,FND102786,FND102786";
            ct = CountItems("BWL001", "", "", "", "", SearchDepth.SearchPast24Hrs, true, out found_what);
            fndlist = "FND799,FND102786";
            ct += CountItems("CMP001", "", "", "", "", SearchDepth.SearchPast24Hrs, true, out found_what);
            fndlist = "FND102786";
            ct += CountItems("IRF519", "", "", "", "", SearchDepth.SearchPast24Hrs, true, out found_what);
            fndlist = "FND102786";
            ct += CountItems("PEENT1", "", "", "", "", SearchDepth.SearchPast24Hrs, true, out found_what);
            fndlist = "FND102786";
            ct += CountItems("PGASTRO4", "", "", "", "", SearchDepth.SearchPast24Hrs, true, out found_what);
            fndlist = "FND102786";
            ct += CountItems("PURINARY1", "", "", "", "", SearchDepth.SearchPast24Hrs, true, out found_what);
            if (ct >= 4)
            {
                SetInd(3, "At least 4 of Toileting items found in past 24 hours.");
            }

            if (_inds[3].is_checked) return;
            
            // hygiene
            if (!hygiene)
            {
                fndlist = "FND008,FND251,FND102431,FND804,FND004,FND989,FND006,FND100924,FND100006,FND007,FND100928,FND102434";
                if (ResultContains("BTH001", fndlist, "", "", "")) hygiene = true;
                fndlist = "FND106396,FND106399,FND106420,FND106394,FND106395,FND106428,FND106423,FND103765";
                if (ResultContains("HYG002", fndlist, "", "", "")) hygiene = true;
            }
            if (!hygiene)
            {
                fndlist = "FND102431,FND103745,FND103765,FND102882";
                if (ResultContains("PEENT8", fndlist, "", "", "")) hygiene=true;

                fndlist = "FND103765";
                if (ResultContains("REPRODUC16", fndlist, "", "", "")) hygiene = true;
            }
            
            if (_inds[3].is_checked) return;

            // mobility
            fndlist = "FND102171,FND102172,FND102173,FND102174";
            if (ResultContains("ACTDEVTYPE", fndlist, "", "", "")) mobility = true;
            fndlist = "FND101478";
            if (ResultContains("AST001", fndlist, "", "", "")) mobility = true;
            fndlist = "FND103815,FND105846";
            if (ResultContains("CMP001", fndlist, "", "", "")) mobility = true;
            fndlist = "FND102431,FND164";
            if (ResultContains("DAILY4", fndlist, "", "", "")) mobility = true;
            fndlist = "FND100925,FND100926,FND106416";
            if (ResultContains("DAILY5", fndlist, "", "", "")) mobility = true;
            if (!mobility)
            {
                fndlist = "FND102257,FND102828,FND102831,FND104882,FND1132,FND164,FND208,FND211";
                if (ResultContains("MBL001", fndlist, "", "", "")) mobility = true;
                fndlist = "FND102174,FND103872,FND104158,FND106885,FND106886";
                if (ResultContains("MOBILITY11", fndlist, "", "", "")) mobility = true;
                fndlist = "FND164,FND106410,FND106414,FND106879";
                if (ResultContains("MOBILITY14", fndlist, "", "", "")) mobility = true;
                fndlist = "FND102174,FND103872,FND104158,FND106885,FND106886";
                if (ResultContains("MOBILITY15", fndlist, "", "", "")) mobility = true;
                fndlist = "FND102174,FND103872,FND104039,FND104879,FND106410";
                if (ResultContains("MOBILITY4", fndlist, "", "", "")) mobility = true;
            }
            if (!mobility)
            {
                fndlist = "FND106867";
                if (ResultContains("MOBILITY5", fndlist, "", "", "")) mobility = true;
                fndlist = "FND164,FND106410,FND106414,FND106879";
                if (ResultContains("MOBILITY8", fndlist, "", "", "")) mobility = true;
                fndlist = "FND102174,FND104039,FND101893,FND104158,FND105516,FND105519";
                if (ResultContains("MOBILITY9", fndlist, "", "", "")) mobility = true;
                fndlist = "FND100351,FND100353";
                if (ResultContains("ORTHO678", fndlist, "", "", "")) mobility = true;
                fndlist = "FND100351,FND100353";
                if (ResultContains("ORTHO789", fndlist, "", "", "")) mobility = true;
                fndlist = "FND103738,FND103815";
                if (ResultContains("PEENT1", fndlist, "", "", "")) mobility = true;
                fndlist = "FND104158";
                if (ResultContains("PEENT8", fndlist, "", "", "")) mobility = true;
                fndlist = "FND102125";
                if (ResultContains("PMUSC31", fndlist, "", "", "")) mobility = true;
                fndlist = "FND102125";
                if (ResultContains("PMUSC32", fndlist, "", "", "")) mobility = true;
                fndlist = "FND104505";
                if (ResultContains("PSYSOC", fndlist, "", "", "")) mobility = true;
            }
            // nutrassist
            if (!nutrassist)
            {
                fndlist = "FND008,FND102431,FND100247,FND102927,FND804,FND006,FND102929,FND763,FND027,FND026";
                if (ResultContains("EAT001", fndlist, "", "", "")) nutrassist = true;
                fndlist = "FND102431,FND102929";
                if (ResultContains("FED001", fndlist, "", "", "")) nutrassist = true;
                fndlist = "FND7000173,FND102930,FND107139";
                if (ResultContains("NUTR2233", fndlist, "", "", "")) nutrassist = true;
            }
            // toileting
            fndlist = "FND015,FND102322,FND008,FND010,FND016,FND102935,FND013,FND014,FND102786,FND102930,FND006,FND007,FND102931";
            if (ResultContains("BWL001", fndlist, "", "", "")) toilet = true;
            fndlist = "FND799,FND102786";
            if (ResultContains("CMP001", fndlist, "", "", "")) toilet = true;
            fndlist = "FND804,FND103177,FND006,FND007";
            if (ResultContains("CTH001", fndlist, "", "", "")) toilet = true;
            fndlist = "FND104344,FND103624";
            if (ResultContains("DAILY4", fndlist, "", "", "")) toilet = true;
            fndlist = "FND102935,FND102786,FND102930,FND102931";
            if (ResultContains("IRF519", fndlist, "", "", "")) toilet = true;
            fndlist = "FND103713";
            if (ResultContains("PACUCATH", fndlist, "", "", "")) toilet = true;
            fndlist = "FND102786";
            if (ResultContains("PEENT1", fndlist, "", "", "")) toilet = true;
            fndlist = "FND102786";
            if (ResultContains("PGASTRO4", fndlist, "", "", "")) toilet = true;
            fndlist = "FND102788";
            if (ResultContains("PGASTRO5", fndlist, "", "", "")) toilet = true;
            fndlist = "FND102786";
            if (ResultContains("PURINARY1", fndlist, "", "", "")) toilet = true;
            fndlist = "FND103692,FND698,FND103696,FND102229,FND103713,FND103695";
            if (ResultContains("PURINARY10", fndlist, "", "", "")) toilet = true;
            fndlist = "FND104314,FND103692,FND102983,FND104313,FND698,FND103696,FND103713,FND103695,FND108172";
            if (ResultContains("PURINARY2", fndlist, "", "", "")) toilet = true;

            adlcat_count = (hygiene ? 1 : 0) + (mobility ? 1 : 0) + (nutrassist ? 1 : 0) + (toilet ? 1 : 0);
            if (adlcat_count == 4)
            {
                SetInd (3, "All four ADL categories found.");
            }
            else {
                if (adlcat_count >= 2)
                {
                    SetInd(2, "Two or more ADL categories found.");
                }
            }
            if (_inds[3].is_checked) return;
            if (_inds[2].is_checked) return;

            // 1. SELF
            fndlist = "FND990,FND005,FND102926";
            SetIndIfResultContains(1, "BTH001", fndlist, "", "", "");
            fndlist = "FND990";
            SetIndIfResultContains(1, "BWL001", fndlist, "", "", "");
            fndlist = "FND1129";
            SetIndIfResultContains(1, "DAILY4", fndlist, "", "", "");
            fndlist = "FND104688";
            SetIndIfResultContains(1, "DAILY5", fndlist, "", "", "");
            fndlist = "FND990";
            SetIndIfResultContains(1, "EAT001", fndlist, "", "", "");
            fndlist = "FND102928";
            SetIndIfResultContains(1, "FED001", fndlist, "", "", "");
            fndlist = "FND102188";
            SetIndIfResultContains(1, "FUNCSCREEN", fndlist, "", "", "");
            fndlist = "FND102926";
            SetIndIfResultContains(1, "IRF519", fndlist, "", "", "");
            fndlist = "FND102430";
            SetIndIfResultContains(1, "MOBILITY4", fndlist, "", "", "");
            fndlist = "FND104688";
            SetIndIfResultContains(1, "NUTR2233", fndlist, "", "", "");
            fndlist = "FND102926";
            SetIndIfResultContains(1, "PEENT8", fndlist, "", "", "");
//Default N3G, C2A, C2B, C3A, C3B to minimum ADL #2
//If no documentation, default to ADL #2

        }


        private void Check_5()
        {

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("5. ADL Rehab");
            Program.VerboseAudit("---------------");

            //Default S4B, N4G to ADL #5
            if (_pat.unit_id == 465 || _pat.unit_id == 290)
            {
                SetInd(5, "Unit is S4B or N4G");
            }


        }


        private void Check_6_7()
        {
            int ct = 0;
            string found_what;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("6. ADL 2-3 Caregivers");
            Program.VerboseAudit("7. ADL 4 or more Caregivers");
            Program.VerboseAudit("---------------");

            ct = CountItems("DAILY5", "FND106413", "", "", "", SearchDepth.SearchPast24Hrs, true, out found_what);
            ct += CountItems("MOBILITY14", "FND106413", "", "", "", SearchDepth.SearchPast24Hrs, true, out found_what);
            ct += CountItems("MOBILITY8", "FND106413", "", "", "", SearchDepth.SearchPast24Hrs, true, out found_what);
            if (ct >= 3)
            {
                SetInd(7, "At least 3 of Assist 4+ found in past 24 hours.");
            }
            if (_inds[7].is_checked) return;

            SetIndIfResultContains(6, "DAILY5", "FND106411,FND106412", "", "", "");
            SetIndIfResultContains(6, "MOBILITY14", "FND106411,FND106412", "", "", "");
            SetIndIfResultContains(6, "MOBILITY8", "FND106411,FND106412", "", "", "");
        }

        private void Check_8()
        {
            string fndlist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("8. Communication");
            Program.VerboseAudit("---------------");

            fndlist = "FND038,FND102219,FND102919,FND102922,FND102923,FND1058,FND377,FND590,FND591";
            SetIndIfResultContains(8, "COM002", fndlist, "", "", "");
            fndlist = "FND038,FND102917,FND1349,FND1350,FND1351,FND1352";
            SetIndIfResultContains(8, "LAN001", fndlist, "", "", "");
            fndlist = "FND038,FND102917,FND1349,FND1350,FND1351";
            SetIndIfResultContains(8, "LAN002", fndlist, "", "", "");
            fndlist = "FND10032,FND10034,FND10485,FND377";
            SetIndIfResultContains(8, "EDU011", fndlist, "", "", "");
            fndlist = "FND101240,FND101293,FND102567";
            SetIndIfResultContains(8, "CMP001", fndlist, "", "", "");
            fndlist = "FND101269,FND101270,FND101271,FND101277,FND101278,FND101279,FND10190,FND104594,FND104595,FND254,FND785";
            SetIndIfResultContains(8, "SPH001", fndlist, "", "", "");
            fndlist = "FND101269,FND101270,FND101271,FND101277,FND10190,FND785";
            SetIndIfResultContains(8, "VRB001", fndlist, "", "", "");
            fndlist = "FND101293,FND102567";
            SetIndIfResultContains(8, "PEENT1", fndlist, "", "", "");
            fndlist = "FND110355,FND101478";
            SetIndIfResultContains(8, "TRANLUSED", fndlist, "", "", "");
            fndlist = "FND102167";
            SetIndIfResultContains(8, "SENSDVTYP", fndlist, "", "", "");
            fndlist = "FND102219,FND102568,FND102919,FND105657,FND107508,FND107509,FND107536";
            SetIndIfResultContains(8, "IPER", fndlist, "", "", "");

        }

        private void Check_9()
        {
            string fndlist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("9. Cognitive Support");
            Program.VerboseAudit("---------------");

            fndlist = "FND103638";
            SetIndIfResultContains(9, "ACTREQREST", fndlist, "", "", "");
            fndlist = "FND100139,FND101402,FND101432,FND102220,FND102224,FND102249,FND103155";
            SetIndIfResultContains(9, "CMP001", fndlist, "", "", "");
            fndlist = "FND107510";
            SetIndIfResultContains(9, "IPER", fndlist, "", "", "");
            fndlist = "FND236";
            SetIndIfResultContains(9, "LOC001", fndlist, "", "", "");
            fndlist = "FND101432";
            SetIndIfResultContains(9, "LOCORNT", fndlist, "", "", "");
            fndlist = "FND101608,FND102426";
            SetIndIfResultContains(9, "PEENT8", fndlist, "", "", "");
            fndlist = "FND102218,FND102220,FND102224";
            SetIndIfResultContains(9, "PNEURO3", fndlist, "", "", "");
            fndlist = "FND104583";
            SetIndIfResultContains(9, "PSYSOC6", fndlist, "", "", "");
            fndlist = "FND236";
            SetIndIfResultContains(9, "RFR001", fndlist, "", "", "");
            fndlist = "FND101432,FND102220,FND102224,FND102249,FND103155";
            SetIndIfResultContains(9, "SGN001", fndlist, "", "", "");
            fndlist = "FND236";
            SetIndIfResultContains(9, "SURGLOC", fndlist, "", "", "");
            fndlist = "FND101403,FND102249,FND104602,FND104604";
            SetIndIfResultContains(9, "THOUPROCES", fndlist, "", "", "");

            if (!Exists("", "FND102133", "", "", ""))
            {
                fndlist = "FND100104,FND101504,FND101505,FND101506,FND101507,FND239,FND240,FND241";
                SetIndIfResultContains(9, "ONT001", fndlist, "", "", "");
            }

        }

        private void Check_10_11()
        {
            string fndlist;
            bool problem=false;
            bool interv=false;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("10. Behavior/Emotional Management");
            Program.VerboseAudit("11. Behavior/Emotional Mgmt - q 1 Hour");
            Program.VerboseAudit("---------------");

            if (_inds[11].is_checked) return;

            fndlist = "FND105185";
            SetIndIfResultContains(10, "REPRO21", fndlist, "", "", "");

            // INTERVENTIONS
            fndlist = "FND104514,FND104515";
            if (ResultContains("EMT002", fndlist, "", "", "")) interv = true;
            fndlist = "FND102306,FND103602,FND104539,FND104552,FND104555,FND104556,FND104557,FND104560";
            if (ResultContains("PEENT8", fndlist, "", "", "")) interv = true;
            fndlist = "FND104549,FND104552,FND104555,FND104556,FND104557,FND104560";
            if (ResultContains("PSYFAMINTE", fndlist, "", "", "")) interv = true;
            fndlist = "FND104549,FND104552,FND104555,FND104556,FND104557,FND104560";
            if (ResultContains("PSYSOC14", fndlist, "", "", "")) interv = true;

            // PROBLEMS
            if (interv)
            {
                fndlist = "FND101258,FND101833,FND102842,FND103414,FND103416,FND104523,FND104562,FND104563,FND104565,FND104569,";
                fndlist += "FND104570,FND104572,FND104573,FND104574,FND104575,FND104576,FND104578,FND104583,FND104584,FND104585,";
                fndlist += "FND104586,FND104587,FND104588,FND104589";
                if (ResultContains("BEH001", fndlist, "", "", "")) problem = true;
                fndlist = "FND615";
                if (ResultContains("COP001", fndlist, "", "", "")) problem = true;
                fndlist = "FND101257,FND101833,FND101843,FND102844,FND104525,FND104528,FND620,FND623,FND624,FND711,FND879";
                if (ResultContains("EMT001", fndlist, "", "", "")) problem = true;
                fndlist = "FND107515";
                if (ResultContains("IPER", fndlist, "", "", "")) problem = true;
                fndlist = "FND605,FND606";
                if (ResultContains("MEM001", fndlist, "", "", "")) problem = true;
                fndlist = "FND102195";
                if (ResultContains("MENHELSCR", fndlist, "", "", "")) problem = true;
                fndlist = "FND101257,FND101258,FND101262";
                if (ResultContains("NUR 002", fndlist, "", "", "")) problem = true;
                fndlist = "FND102842,FND102843,FND103414,FND103416,FND104528,FND104544,FND104562,FND104563,FND104566,FND104567";
                if (ResultContains("PATEXHIBIT", fndlist, "", "", "")) problem = true;
                if (!problem)
                {
                    fndlist = "FND101258,FND101426,FND101833,FND102842,FND103414,FND103416,FND104522,FND104523,FND104528,FND104562,FND104563,";
                    fndlist += "FND104565,FND104569,FND104570,FND104572,FND104573,FND104574,FND104575,FND104576,FND104578,FND104583,FND104584,";
                    fndlist += "FND104585,FND104586,FND104587,FND104588,FND104589";
                    if (ResultContains("PNEURO2", fndlist, "", "", "")) problem = true;
                    fndlist = "FND104538,FND104541,FND104696";
                    if (ResultContains("PSYSOC10", fndlist, "", "", "")) problem = true;
                    fndlist = "FND102842,FND102843,FND103414,FND103416,FND104528,FND104544,FND104567,FND104694,FND104695";
                    if (ResultContains("PSYSOC12", fndlist, "", "", "")) problem = true;
                    fndlist = "FND101257,FND104525,FND104528,FND104583,FND104592,FND104593";
                    if (ResultContains("PSYSOC3", fndlist, "", "", "")) problem = true;
                    fndlist = "FND101257,FND101977,FND103069,FND104525,FND104583,FND104592,FND104593";
                    if (ResultContains("PSYSOC4", fndlist, "", "", "")) problem = true;
                    fndlist = "FND101477,FND101535,FND101538";
                    if (ResultContains("PSYSOC5", fndlist, "", "", "")) problem = true;
                    fndlist = "FND104583";
                    if (ResultContains("PSYSOC8", fndlist, "", "", "")) problem = true;
                    fndlist = "FND104532";
                    if (ResultContains("PSYSOC9", fndlist, "", "", "")) problem = true;
                    fndlist = "FND102842,FND102843,FND103414,FND103416,FND104528,FND104544,FND104567,FND104694,FND104695";
                    if (ResultContains("PSYSOFAMIL", fndlist, "", "", "")) problem = true;
                    fndlist = "FND100139,FND101402";
                    if (ResultContains("SGN001", fndlist, "", "", "")) problem = true;
                    fndlist = "FND103468";
                    if (ResultContains("THOUPROCES", fndlist, "", "", "")) problem = true;
                }
            } // interv

            if (problem && interv)
            {
                SetInd(10, "Problem and Intervention found");
            }

            if (_inds[10].is_checked)
            {
                fndlist = "FND104542";
                SetIndIfResultContains(11, "PSYSOC13", fndlist, "", "", "");
            }

        }

        // (this isn't really a Q1h count -- it is just a count) Make this a q1hr count
        private int ReturnQ1HrCount(string code_list, string result_list)
        {
            var buckets = new List<int>();
            SetBucketSize(60);
            AddBuckets(buckets, "", code_list, "", "", result_list);
            return CountBuckets(buckets);
        }

        private bool IsQ1Hour(int count)
        {
            return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q1H);
        }

        // Use this if counting safety buckets
        //private void CheckSafety(int count, string desc)
        //{
        //    if (_inds[13].is_checked) return;             //skip if highest already checked
        //    if (count == 0) return;
        //    switch (FreqForCount(_pat.los_hours, count))
        //    {
        //    case Frequencies.Q30M:
        //        SetInd(13, desc + " q30min");
        //        break;
        //    case Frequencies.Q1H:
        //    case Frequencies.Q2H:
        //        SetInd(12, desc + " q2h");
        //        break;
        //    default:
        //        Program.VerboseAudit(desc + ": " + count + " in " + Math.Round(_pat.los_hours) + " hours is not enough");
        //        break;
        //    }
        //}

        private void Check_12_13()
        {
            string fndlist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("12. Safety Management - q 2 Hours");
            Program.VerboseAudit("13. Safety Management - q 30 Minutes");
            Program.VerboseAudit("---------------");

            fndlist = "FND103622";
            if (ResultContains("PEENT8", fndlist, "", "", ""))
            {
                SetInd(13, "Sitter at bedside");
                if (Exists("", "FND103416,FND103414", "", "", ""))
                {
                    SetInd(66, "Suicidal/Homicidal");
                }
                else {
                    SetInd(60, "Sitter at bedside");
                }

            }
            fndlist = "FND104579,FND104580";
            SetIndIfResultContains(13, "BEH001", fndlist, "", "", "");
            fndlist = "FND101260,FND104579,FND104580";
            SetIndIfResultContains(13, "PNEURO2", fndlist, "", "", "");
            fndlist = "FND101478,FND104503,FND104504";
            SetIndIfResultContains(13, "PSYSOC1", fndlist, "", "", "");

            fndlist = "FND102229,FND103672";
            SetIndIfResultContains(12, "PCSRPTROM", fndlist, "", "", "");
            SetIndIfResultContains(4, "PCSRPTROM", fndlist, "", "", "");
            SetIndIfResultContains(50, "PCSRPTROM", fndlist, "", "", "");
            fndlist = "FND102229,FND103672";
            SetIndIfResultContains(12, "RESTRAIN11", fndlist, "", "", "");
            SetIndIfResultContains(4, "RESTRAIN11", fndlist, "", "", "");
            SetIndIfResultContains(50, "RESTRAIN11", fndlist, "", "", "");
            fndlist = "FND101478";
            SetIndIfResultContains(12, "RESTRAIN9", fndlist, "", "", "");
            SetIndIfResultContains(4, "RESTRAIN9", fndlist, "", "", "");
            SetIndIfResultContains(50, "RESTRAIN9", fndlist, "", "", "");
            fndlist = "FND105223";
            SetIndIfResultContains(12, "TRNSPRT8", fndlist, "", "", "");
            SetIndIfResultContains(4, "TRNSPRT8", fndlist, "", "", "");
            SetIndIfResultContains(50, "TRNSPRT8", fndlist, "", "", "");

            fndlist = "FND107085,FND107086";
            SetIndIfResultContains(12, "FALLRSK5", fndlist, "", "", "");
            SetIndIfResultContains(69, "FALLRSK5", fndlist, "", "", "");
        }

        private void Check_14()
        {
            string fndlist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("14. Isolation");
            Program.VerboseAudit("---------------");

            fndlist = "FND100380,FND101771,FND101769,FND111185,FND111187,FND101770";
            SetIndIfResultContains(14, "ISOLOBSV", fndlist, "", "", "");
            fndlist = "FND106587";
            SetIndIfResultContains(14, "LUMBRPNTR", fndlist, "", "", "");
            SetIndIfResultContains(21, "LUMBRPNTR", fndlist, "", "", "");
            fndlist = "FND106348";
            SetIndIfResultContains(14, "MISC10", fndlist, "", "", "");
        }

        private void CheckAssessment(int count, string desc)
        {
            if (_inds[18].is_checked) return;          //skip if highest already checked
            if (count == 0) return;                    //skip if none
            
            // This should work the same as the original code:
            switch (FreqForCount(_pat.los_hours, count)) {
                case Frequencies.Q30M:
                    SetInd(18, desc);
                    break;
                case Frequencies.Q1H:
                    SetInd(17, desc);
                    break;
                case Frequencies.Q2H:
                    SetInd(16, desc);
                    break;
                case Frequencies.Q4H:
                    SetInd(15, desc);
                    break;
                default:
                    break;
            }

        }

        private void Check_15_16_17_18()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("15. Assessment q4h");
            Program.VerboseAudit("16. Assessment q2h");
            Program.VerboseAudit("17. Assessment q1h");
            Program.VerboseAudit("18. Assessment q30min");
            Program.VerboseAudit("---------------");
            
            CountAssessments(30);               // always do q30 buckets--but use different freq tables for icu vs non-icu.

        }


        // Count number of buckets; similar assessments in the same bucket count as one
        //
        private void CountAssessments(int bucket_size)
        {
            int ct;
            string fndlist;
            List<int> buckets;

            SetBucketSize(bucket_size);

            buckets = new List<int>();
            fndlist = "FND101427,FND102127,FND102300";
            AddBuckets(buckets, "PCARDIO5", fndlist, "", "");
            fndlist = "FND038,FND067,FND100027,FND101092,FND101093,FND101094,FND123,FND481,FND482";
            AddBuckets(buckets, "HEARTSOU", fndlist, "", "");
            fndlist = "BP,BP_2,BP_ART,BP_ART_2,BP_NI,BP_NI_2,CVP,HEART_RATE,HRT_RTE_BSA,HRT_RTE_HEMO,Pulse,Pulse_2,FTL_HRT_RTE";
            AddBuckets(buckets, "", fndlist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Cardio=" + ct);
            if (_inds[18].is_checked) return;

            buckets = new List<int>();
            fndlist = "FND101263,FND102163,FND102218,FND102779,FND102780,FND102781,FND102783,FND104510,FND256,FND372,FND532,FND533,FND967";
            AddBuckets(buckets, "BWL 011", fndlist, "", "");
            fndlist = "FND102146,FND102218,FND102779,FND102780,FND102782,FND102783,FND102785,FND238,FND256,FND532,FND533";
            AddBuckets(buckets, "BWL006", fndlist, "", "");
            fndlist = "FND101263,FND102163,FND102218,FND102779,FND102780,FND102781,FND102783,FND104510";
            AddBuckets(buckets, "BWL010", fndlist, "", "");
            fndlist = "FND101263,FND102163,FND102218,FND102779,FND102780,FND102781,FND102783,FND104510,FND256,FND372,FND532,FND533,FND967";
            AddBuckets(buckets, "BWL012", fndlist, "", "");
            fndlist = "FND101263,FND102163,FND102218,FND102779,FND102780,FND102781,FND102783,FND104510";
            AddBuckets(buckets, "BWL014", fndlist, "", "");
            fndlist = "FND102163,FND102218,FND102779,FND102780,FND102781,FND102783,FND256,FND372,FND532,FND533,FND967";
            AddBuckets(buckets, "EDBWL010", fndlist, "", "");
            fndlist = "FND102163,FND102218,FND102779,FND102780,FND102781,FND102783,FND256,FND372,FND532,FND533,FND967";
            AddBuckets(buckets, "EDBWL014", fndlist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "GI=" + ct);
            if (_inds[18].is_checked) return;

            buckets = new List<int>();
            fndlist = "FND711,FND038,FND100144,FND100145,FND100146,FND101242,FND101243,FND101244,FND101245,FND233,FND234,FND235,FND236,FND237,FND335,FND621";
            AddBuckets(buckets, "LOC001", fndlist, "", "");
            fndlist = "FND0000";
            AddBuckets(buckets, "NIH", fndlist, "", "");
            fndlist = "FND711,FND101242,FND233,FND237,FND236,FND101243,FND100144,FND101244,FND234,FND101245,FND038,FND100145,FND621,FND235,FND100146,FND335";
            AddBuckets(buckets, "SURGLOC", fndlist, "", "");
            fndlist = "INTRACRAN_PRESS,Pulse_12,CPP";
            AddBuckets(buckets, "", fndlist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Neuro=" + ct);
            if (_inds[18].is_checked) return;

            buckets = new List<int>();
            fndlist = "FND102124";
            AddBuckets(buckets, "PCARDIO5", fndlist, "", "");
            fndlist = "FND110720,FND110715,FND110721,FND110719,FND102506,FND105458,FND110713,FND101479,FND110717,FND110712,FND110714,FND101709,FND110711,FND110718";
            AddBuckets(buckets, "PEWS01", fndlist, "", "");
            fndlist = "FND647,FND710,FND038,FND102124,FND101740,FND742,FND101628,FND265,FND102105,FND101291,FND102106,FND101738,";
            fndlist += "FND102108,FND102229,FND102109,FND101683,FND101739,FND101729,FND102117,FND102582,FND10490";
            AddBuckets(buckets, "RSP001", fndlist, "", "");
            fndlist = "FND110417,FND110419,FND110413,FND110414,FND110416,FND257,FND105012,FND101684,FND238,FND110415,FND110418";
            AddBuckets(buckets, "RTWARMSCO", fndlist, "", "");
            fndlist = "AO2SAT,END_TID_CO2,O2SAT,O2SAT_PUL_OX,O2SAT_SKN_PROBE,Resp,VEN_O2_SAT";
            AddBuckets(buckets, "", fndlist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Resp=" + ct);
            if (_inds[18].is_checked) return;

            buckets = new List<int>();
            fndlist = "BLD_TEMP,BLD_TEMP_CF,CORE_TEMP,ESOPH_TEMP,RECTAL_TEMP,SKIN_TEMP,Temp,TYMPANIC_TEMP";
            AddBuckets(buckets, "", fndlist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Temp=" + ct);

        }

        //=====================================================================
        // Group like assessments into buckets of time
        // Each bucket counts as one assessment in the frequency count

        private void SetBucketSize(int minutes)
        {
            _bucket_size = minutes;
        }

        //Add to the list of bucket numbers (redundant buckets are fine)
        private void AddBuckets(List<int> bucket_list, string cat, string code_list, string desc, string field)
        {
            AddBuckets(bucket_list, cat, code_list, desc, field, "");
        }
        private void AddBuckets(List<int> bucket_list, string cat, string code_list, string desc, string field, string result_list)
        {
            // Assign (virtual) bucket numbers to each assessment.  
            // Every x minutes after that is a new bucket.  Bucket zero = pull_start time.

            // get the chart items for the assessments
            var query = StartNewQuery();
            query = AndItemFilter(query, cat, code_list, desc, field, result_list);

            // This step is needed for those who want to count the # within a bucket, like Shands #24.
            // This will have no effect on those that count buckets.
            var query2 = (from item in query select new { item.EVENT_DATETIME }).Distinct();

            // figure out what buckets the events belong to
            var query3 = from item in query2
                         select new {
                            bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size)
                         };
            // Add to the list
            foreach (var item in query3) {
                bucket_list.Add(item.bucket);
            }

            // This is just for auditing: Say what we are looking for and how many were found
            if (!String.IsNullOrEmpty(result_list)) {
                // print each word and if it was found or not
                int i = CountResultContains(cat, code_list, desc, field, result_list);
            } else {
                // print how many were found
                Program.VerboseAudit(Describe(cat, code_list, desc, field, ""));
            }
        }

        private int CountBuckets(List<int> bucket_list)       // bucket list: (ha ha)
        {
            int result = bucket_list.Distinct().Count();
            if (result > 0) Program.VerboseAudit(result + " unique");
            return result;
        }


        private void Check_19()
        {
            string fndlist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("19. Vascular Access Site Mgt q1 Hour");
            Program.VerboseAudit("---------------");

            fndlist = "MCK_IV100,MCK_IV101,MCK_IV102,MCK_IV103,MCK_IV104,MCK_IV105,MCK_IV106,MCK_IV107,MCK_IV108,MCK_IV109,MCK_IV110";
            SetIndIfResultContains(19, "IVS001", fndlist, "", "", "");


        }

        private void Check_20()
        {
            string fndlist;
            
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("20. Medication Activity >= 20 minutes");
            Program.VerboseAudit("---------------");

            fndlist = "FND103177";
            SetIndIfResultContains(20, "MISC39", fndlist, "", "", "");
            SetIndIfResultContains(117, "MISC39", fndlist, "", "", "");
            SetIndIfResultContains(20, "MISC38", fndlist, "", "", "");
        }


        private void Check_21_22()
        {
            string fndlist;
            
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("21. Wound/Injury Mgmt");
            Program.VerboseAudit("22. Wound/Injury Mgmt >= 30 Minutes");
            Program.VerboseAudit("---------------");

            fndlist = "FND110691";
            SetIndIfResultContains(22, "WOUNDS2", fndlist, "", "", "");
            fndlist = "FND110693,FND110694";
            SetIndIfResultContains(22, "WNDTIME", fndlist, "", "", "60");

            fndlist = "FND106587,FND106589";
            SetIndIfResultContains(21, "LUMBRPNTR", fndlist, "", "", "");
            SetIndIfResultContains(14, "LUMBRPNTR", fndlist, "", "", "");

            fndlist = "FND100350,FND100351,FND100352,FND100353";
            SetIndIfResultContains(21, "PMUSC28", fndlist, "", "", "");
            SetIndIfResultContains(2, "PMUSC28", fndlist, "", "", "");

            if (_inds[22].is_checked) return;

            fndlist = "FND102229,FND102321,FND104079,FND107890,FND107894,FND426,FND690,FND691,FND807";
            SetIndIfResultContains(21, "WOUNDS110", fndlist, "", "", "");
            SetIndIfResultContains(21, "WOUND4592", fndlist, "", "", "");
            SetIndIfResultContains(21, "WOUNDS65", fndlist, "", "", "");
            SetIndIfResultContains(21, "WOUNDS68", fndlist, "", "", "");
            SetIndIfResultContains(21, "WOUNDS71", fndlist, "", "", "");
            SetIndIfResultContains(21, "WQOND5DR", fndlist, "", "", "");
            SetIndIfResultContains(21, "WONDDRAN", fndlist, "", "", "");
            SetIndIfResultContains(21, "WD34", fndlist, "", "", "");
            SetIndIfResultContains(21, "WD45", fndlist, "", "", "");
            SetIndIfResultContains(21, "WD90", fndlist, "", "", "");

            fndlist = "FND101478,FND101479";
            SetIndIfResultContains(21, "WNDREM2", fndlist, "", "", "");

            fndlist = "FND038,FND101478,FND101479";
            SetIndIfResultContains(21, "WNDREM1", fndlist, "", "", "");

            fndlist = "FND100007,FND101495,FND104495,FND106229,FND107853,FND107854,FND107855,FND107856,FND111176,FND111290";
            SetIndIfResultContains(21, "WOUNDS102", fndlist, "", "", "");
            SetIndIfResultContains(21, "WOUND2", fndlist, "", "", "");
            SetIndIfResultContains(21, "WOUND3", fndlist, "", "", "");
            SetIndIfResultContains(21, "WOUND4", fndlist, "", "", "");
            SetIndIfResultContains(21, "WOUND5", fndlist, "", "", "");
            SetIndIfResultContains(21, "WOUND6", fndlist, "", "", "");
            SetIndIfResultContains(21, "WOUND7", fndlist, "", "", "");
            SetIndIfResultContains(21, "WOUND8", fndlist, "", "", "");
            SetIndIfResultContains(21, "WOUND9", fndlist, "", "", "");
            SetIndIfResultContains(21, "WOUND10", fndlist, "", "", "");

            fndlist = "FND104496,FND104011,FND104010,FND104760";
            SetIndIfResultContains(21, "WOUNDS102", fndlist, "", "", "");

            fndlist = "FND100007,FND101495,FND104495,FND106229,FND107853,FND107854";
            fndlist += ",FND107855,FND107856,FND111176,FND111290,FND104496";
            fndlist += ",FND104011,FND104010,FND104760";
            SetIndIfResultContains(21, "WOUNDS90", fndlist, "", "", "");

            fndlist = "FND100007,FND101495,FND104495,FND106229,FND107853";
            fndlist += ",FND107854,FND107855,FND107856,FND111176,FND111290";
            fndlist += ",FND104496,FND104011,FND104010,FND104760";
            SetIndIfResultContains(21, "WOUND1", fndlist, "", "", "");

            fndlist = "FND100264,FND102647,FND102854,FND102855,FND102859,FND201";
            SetIndIfResultContains(21, "158089", fndlist, "", "", "");
            fndlist = "FND102647,FND103700,FND1114,FND201";
            SetIndIfResultContains(21, "APR001", fndlist, "", "", "");
            fndlist = "FND100062,FND100063,FND102889";
            SetIndIfResultContains(21, "BRST500", fndlist, "", "", "");
            fndlist = "FND015,FND102322";
            SetIndIfResultContains(21, "BWL001", fndlist, "", "", "");
            fndlist = "FND101345";
            SetIndIfResultContains(21, "CAPD14", fndlist, "", "", "");
            fndlist = "FND101512,FND101541,FND101542,FND108400,FND108401";
            SetIndIfResultContains(21, "CAPD3", fndlist, "", "", "");
            if (_inds[21].is_checked) return;
            fndlist = "FND102647";
            SetIndIfResultContains(21, "CCPD13", fndlist, "", "", "");
            fndlist = "FND904,FND905";
            SetIndIfResultContains(21, "CHE001", fndlist, "", "", "");
            fndlist = "FND102576,FND103679,FND103708,FND192,FND530";
            SetIndIfResultContains(21, "CMP001", fndlist, "", "", "");
            fndlist = "FND101597,FND102229,FND102652,FND102710,FND102713,FND102853,FND104011,FND104487,FND104492";
            SetIndIfResultContains(21, "CRD001", fndlist, "", "", "");
            fndlist = "FND101541,FND101542,FND103216,FND103217,FND103218,FND103220";
            SetIndIfResultContains(21, "CT2", fndlist, "", "", "");
            fndlist = "FND101541,FND101542,FND103216,FND103217,FND103218,FND103220";
            SetIndIfResultContains(21, "CT3LOC", fndlist, "", "", "");
            fndlist = "FND101541,FND101542,FND103216,FND103217,FND103218,FND103220";
            SetIndIfResultContains(21, "CT4LOCATIO", fndlist, "", "", "");
            fndlist = "FND101150,FND101151,FND102229,FND103250,FND103251,FND103252,FND103253,FND103274,FND103275,FND103276,FND103277";
            SetIndIfResultContains(21, "CV001", fndlist, "", "", "");
            if (_inds[21].is_checked) return;
            fndlist = "FND101150,FND101151,FND102229,FND103250,FND103251,FND103252,FND103253,FND103274,FND103275,FND103276,FND103277";
            SetIndIfResultContains(21, "CV177", fndlist, "", "", "");
            fndlist = "FND101150,FND101151,FND102229,FND103250,FND103251,FND103252,FND103253,FND103274,FND103275,FND103276,FND103277";
            SetIndIfResultContains(21, "CV188", fndlist, "", "", "");
            fndlist = "FND101367,FND101524,FND102361";
            SetIndIfResultContains(21, "EAR010", fndlist, "", "", "");
            fndlist = "FND101524,FND102361";
            SetIndIfResultContains(21, "EAR011", fndlist, "", "", "");
            fndlist = "FND101524";
            SetIndIfResultContains(21, "EAR013", fndlist, "", "", "");
            fndlist = "FND102540";
            SetIndIfResultContains(21, "EAR100", fndlist, "", "", "");
            fndlist = "FND102540";
            SetIndIfResultContains(21, "EAR101", fndlist, "", "", "");
            fndlist = "FND102540";
            SetIndIfResultContains(21, "EAR110", fndlist, "", "", "");
            fndlist = "FND102540";
            SetIndIfResultContains(21, "EAR111", fndlist, "", "", "");
            fndlist = "FND101524";
            SetIndIfResultContains(21, "EDLEYE", fndlist, "", "", "");
            fndlist = "FND100095,FND102647,FND102856";
            SetIndIfResultContains(21, "EME001", fndlist, "", "", "");
            if (_inds[21].is_checked) return;
            fndlist = "FND102342";
            SetIndIfResultContains(21, "FUNDUS", fndlist, "", "", "");
            fndlist = "FND102648,FND102855,FND102859";
            SetIndIfResultContains(21, "GIT5564", fndlist, "", "", "");
            fndlist = "FND102711";
            SetIndIfResultContains(21, "GITU000", fndlist, "", "", "");
            fndlist = "FND101709,FND102854,FND102855,FND103700";
            SetIndIfResultContains(21, "GU1122", fndlist, "", "", "");
            fndlist = "FND100065,FND102229,FND102664";
            SetIndIfResultContains(21, "GU567", fndlist, "", "", "");
            fndlist = "FND101622";
            SetIndIfResultContains(21, "HEM001", fndlist, "", "", "");
            fndlist = "FND102322";
            SetIndIfResultContains(21, "IRF519", fndlist, "", "", "");
            fndlist = "FND102099,FND102229,FND102328,FND102329,FND102330,FND102645,FND102855,FND105178,FND105179,FND105180,FND105181";
            SetIndIfResultContains(21, "LCH001", fndlist, "", "", "");
            fndlist = "FND102996,FND106340,FND106615";
            SetIndIfResultContains(21, "MISC10", fndlist, "", "", "");
            fndlist = "FND103265";
            SetIndIfResultContains(21, "MISC15", fndlist, "", "", "");
            fndlist = "FND101345,FND101585,FND102323";
            SetIndIfResultContains(21, "MISC16", fndlist, "", "", "");
            fndlist = "FND101622,FND149";
            SetIndIfResultContains(21, "MTH001", fndlist, "", "", "");
            if (_inds[21].is_checked) return;
            fndlist = "FND101355,FND101622";
            SetIndIfResultContains(21, "MUC001", fndlist, "", "", "");
            fndlist = "FND101355,FND149";
            SetIndIfResultContains(21, "MUC002", fndlist, "", "", "");
            fndlist = "FND102010";
            SetIndIfResultContains(21, "MUSC3956", fndlist, "", "", "");
            fndlist = "FND102010";
            SetIndIfResultContains(21, "MUSC4508", fndlist, "", "", "");
            fndlist = "FND106789,FND106790";
            SetIndIfResultContains(21, "NEURO1", fndlist, "", "", "");
            fndlist = "FND106789,FND106790";
            SetIndIfResultContains(21, "NEURO2", fndlist, "", "", "");
            fndlist = "FND100195,FND102854,FND102855,FND201";
            SetIndIfResultContains(21, "NGDRAIN", fndlist, "", "", "");
            fndlist = "FND101051";
            SetIndIfResultContains(21, "NGTUSIT", fndlist, "", "", "");
            fndlist = "FND101355,FND102539,FND102540,FND102544,FND153";
            SetIndIfResultContains(21, "NSE001", fndlist, "", "", "");
            if (_inds[21].is_checked) return;
            fndlist = "FND100193,FND100195,FND102647,FND102854,FND201";
            SetIndIfResultContains(21, "NSE002", fndlist, "", "", "");
            fndlist = "FND101524";
            SetIndIfResultContains(21, "NUR 25", fndlist, "", "", "");
            fndlist = "FND101355";
            SetIndIfResultContains(21, "PACER12", fndlist, "", "", "");
            fndlist = "FND101587,FND101731,FND102068,FND102069,FND102070,FND102071,FND102253,FND102559,FND103267,FND107020";
            SetIndIfResultContains(21, "PACER23", fndlist, "", "", "");
            fndlist = "FND107023,FND107024,FND107025,FND107026,FND107027,FND107028,FND107029";
            SetIndIfResultContains(21, "PACER5", fndlist, "", "", "");
            fndlist = "FND102073";
            SetIndIfResultContains(21, "PACUCATH", fndlist, "", "", "");
            fndlist = "FND101584";
            SetIndIfResultContains(21, "PACUDRN2", fndlist, "", "", "");
            fndlist = "FND102576,FND103679,FND103708,FND192,FND530";
            SetIndIfResultContains(21, "PEENT1", fndlist, "", "", "");
            fndlist = "FND101524";
            SetIndIfResultContains(21, "PEENT13", fndlist, "", "", "");
            fndlist = "FND101524";
            SetIndIfResultContains(21, "PEENT14", fndlist, "", "", "");
            if (_inds[21].is_checked) return;
            fndlist = "FND101699,FND102073,FND102074,FND102554,FND102558,FND103265,FND103373,FND103760,FND103762,FND104394";
            SetIndIfResultContains(21, "PEENT8", fndlist, "", "", "");
            fndlist = "FND102664,FND103703";
            SetIndIfResultContains(21, "PERICARD1", fndlist, "", "", "");
            fndlist = "FND101355";
            SetIndIfResultContains(21, "PERICARD3", fndlist, "", "", "");
            fndlist = "FND101584,FND101585,FND102647";
            SetIndIfResultContains(21, "PGASTRO15", fndlist, "", "", "");
            fndlist = "FND101587,FND102069,FND102082,FND102229,FND102896";
            SetIndIfResultContains(21, "PGASTRO17", fndlist, "", "", "");
            fndlist = "FND102072,FND102073,FND102684,FND103265";
            SetIndIfResultContains(21, "PGASTRO20", fndlist, "", "", "");
            fndlist = "FND101345,FND102854,FND102855,FND102856,FND102857";
            SetIndIfResultContains(21, "PGASTRO23", fndlist, "", "", "");
            fndlist = "FND101870,FND102026,FND102027,FND102028,FND102029,FND103581,FND103582,FND103583,FND103584";
            SetIndIfResultContains(21, "PGASTRO28", fndlist, "", "", "");
            fndlist = "FND101345";
            SetIndIfResultContains(21, "PGASTRO30", fndlist, "", "", "");
            fndlist = "FND102854,FND102855,FND201";
            SetIndIfResultContains(21, "PGASTRO7", fndlist, "", "", "");
            fndlist = "FND006,FND007,FND253,FND990";
            SetIndIfResultContains(21, "PIN001", fndlist, "", "", "");
            if (_inds[21].is_checked) return;
            fndlist = "FND101326";
            SetIndIfResultContains(21, "PMUSC26", fndlist, "", "", "");
            fndlist = "FND100350,FND100351,FND100352,FND100353";
            SetIndIfResultContains(21, "PMUSC28", fndlist, "", "", "");
            SetIndIfResultContains(2, "PMUSC28", fndlist, "", "", "");
            fndlist = "FND101329,FND102258,FND103265";
            SetIndIfResultContains(21, "PMUSC28", fndlist, "", "", "");
            fndlist = "FND101478,FND110341";
            SetIndIfResultContains(21, "PSKIN12", fndlist, "", "", "");
            fndlist = "FND103695,FND103696,FND698";
            SetIndIfResultContains(21, "PURINARY10", fndlist, "", "", "");
            fndlist = "FND103696,FND104315,FND698";
            SetIndIfResultContains(21, "PURINARY2", fndlist, "", "", "");
            fndlist = "FND102854,FND102855,FND103700";
            SetIndIfResultContains(21, "PURINARY4", fndlist, "", "", "");
            fndlist = "FND102548";
            SetIndIfResultContains(21, "REPRO08", fndlist, "", "", "");
            fndlist = "FND101291,FND101683,FND102229,FND102855,FND103270,FND105189,FND105192,FND105193,FND105194";
            SetIndIfResultContains(21, "REPRO09", fndlist, "", "", "");
            fndlist = "FND102827";
            SetIndIfResultContains(21, "REPRODUC10", fndlist, "", "", "");
            if (_inds[21].is_checked) return;
            fndlist = "FND101345,FND102855";
            SetIndIfResultContains(21, "REPRODUC12", fndlist, "", "", "");
            fndlist = "FND101345,FND102855";
            SetIndIfResultContains(21, "REPRODUC13", fndlist, "", "", "");
            fndlist = "FND102827";
            SetIndIfResultContains(21, "REPRODUC14", fndlist, "", "", "");
            fndlist = "FND103760";
            SetIndIfResultContains(21, "REPRODUC16", fndlist, "", "", "");
            fndlist = "FND102827";
            SetIndIfResultContains(21, "REPRODUC17", fndlist, "", "", "");
            fndlist = "FND103762";
            SetIndIfResultContains(21, "REPRODUC20", fndlist, "", "", "");
            fndlist = "FND101345,FND102647,FND102855";
            SetIndIfResultContains(21, "REPRODUC27", fndlist, "", "", "");
            fndlist = "FND102647";
            SetIndIfResultContains(21, "REPRODUC28", fndlist, "", "", "");
            fndlist = "FND101512,FND101541,FND101542,FND102342,FND105341,FND105342,FND105343,FND105344,FND105345,FND105346,FND105347,FND105348,FND105353";
            SetIndIfResultContains(21, "REPROF1", fndlist, "", "", "");
            fndlist = "FND102548";
            SetIndIfResultContains(21, "REPROF4", fndlist, "", "", "");
            fndlist = "FND101291,FND101683,FND102229,FND102855,FND103270,FND105192,FND105193,FND105194";
            SetIndIfResultContains(21, "REPROF6", fndlist, "", "", "");
            if (_inds[21].is_checked) return;
            fndlist = "FND102337,FND103561,FND106581";
            SetIndIfResultContains(21, "SCRPOA", fndlist, "", "", "");
            fndlist = "FND102576,FND181,FND530";
            SetIndIfResultContains(21, "SGN001", fndlist, "", "", "");
            fndlist = "FND101478,FND110341";
            SetIndIfResultContains(21, "SKIN122", fndlist, "", "", "");
            fndlist = "FND101587,FND101590,FND101594,FND101595,FND101980,FND102893,FND103553,FND103703";
            SetIndIfResultContains(21, "SPECNEUR1", fndlist, "", "", "");
            fndlist = "FND104906";
            SetIndIfResultContains(21, "SPN1855", fndlist, "", "", "");
            fndlist = "FND038,FND102717,FND102718,FND103176,FND103177,FND103183";
            SetIndIfResultContains(21, "SPRESP10", fndlist, "", "", "");
            fndlist = "FND101541,FND101542,FND103216,FND103217,FND103218,FND103220";
            SetIndIfResultContains(21, "SPRESP11", fndlist, "", "", "");
            fndlist = "FND101584,FND101585,FND102855";
            SetIndIfResultContains(21, "SPRESP13", fndlist, "", "", "");
            fndlist = "FND101355,FND101622";
            SetIndIfResultContains(21, "THR001", fndlist, "", "", "");
            fndlist = "FND104906";
            SetIndIfResultContains(21, "TRA001", fndlist, "", "", "");
            fndlist = "FND006,FND007,FND253,FND990";
            SetIndIfResultContains(21, "TRC001", fndlist, "", "", "");
            fndlist = "FND100167,FND100168,FND102540,FND102685";
            SetIndIfResultContains(21, "TRC004", fndlist, "", "", "");
            if (_inds[21].is_checked) return;
            fndlist = "FND100063,FND102889,FND104751";
            SetIndIfResultContains(21, "TUBEFEED7", fndlist, "", "", "");
            fndlist = "FND102229,FND102258,FND103427,FND103553,FND488";
            SetIndIfResultContains(21, "VASC232", fndlist, "", "", "");
            fndlist = "FND101852,FND101857,FND101858,FND101859,FND101867,FND101869,FND101871,FND101875,FND101876,FND101879,FND101880,FND101881,";
            fndlist += "FND101882,FND101886,FND101887,FND101890,FND102014,FND102016,FND102022,FND102038,FND103346,FND106765,FND107922,FND107923,FND107924";
            SetIndIfResultContains(21, "WOUND10L", fndlist, "", "", "");
            SetIndIfResultContains(21, "WOUND1L", fndlist, "", "", "");
            SetIndIfResultContains(21, "WOUND2L", fndlist, "", "", "");
            SetIndIfResultContains(21, "WOUND3L", fndlist, "", "", "");
            SetIndIfResultContains(21, "WOUND4L", fndlist, "", "", "");
            SetIndIfResultContains(21, "WOUND5L", fndlist, "", "", "");
            SetIndIfResultContains(21, "WOUND6L", fndlist, "", "", "");
            SetIndIfResultContains(21, "WOUND7L", fndlist, "", "", "");
            SetIndIfResultContains(21, "WOUND8L", fndlist, "", "", "");
            SetIndIfResultContains(21, "WOUND9L", fndlist, "", "", "");
            SetIndIfResultContains(21, "WOUNDS91", fndlist, "", "", "");
            fndlist = "FND107024,FND107029,FND107027,FND107023,FND107028,FND107026,FND107025";
            SetIndIfResultContains(21, "PMK001", fndlist, "", "", "");
            fndlist = "FND107024,FND107060,FND107029,FND107027,FND107023,FND107009,FND107010,FND10350,FND100331,FND107028,FND107026,FND107025,FND107059";
            SetIndIfResultContains(21, "PACER0002", fndlist, "", "", "");


        }

        private void Check_23()
            {
            string fndlist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("23. Healthcare Mgmt Education >= 1 Hour");
            Program.VerboseAudit("---------------");

            fndlist = "FND105072,FND105082,FND110423,FND110424";
            SetIndIfResultContains(23, "IPERCARUNI", fndlist, "", "", "");
            fndlist = "FND105072,FND105082,FND110421,FND110422";
            SetIndIfResultContains(23, "IPERHFEDUC", fndlist, "", "", "");
            fndlist = "FND101478";
            SetIndIfResultContains(23, "MCK_QMR248", fndlist, "", "", "");
            fndlist = "FND105072,FND105082";
            SetIndIfResultContains(23, "TEACHPROV", fndlist, "", "", "");
        }

        private void Check_24()
        {
            string res;
            DateTime evdt;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("24. 1 to 1 Physiological Interv. >= 2 Hours");
            Program.VerboseAudit("---------------");

            //Limit to 16 hours after evdt
            if (GetEVDT("MISCRNTIME", "FND0000", "", "", out evdt))
            {
                System.TimeSpan duration = new System.TimeSpan(16, 0, 0);  // 16 hrs
                if (evdt.Add(duration) >= _pat.pull_finish)
                {
                    if (GetResult("MISCRNTIME", "FND0000", "", "", out res))
                    {
                        if (res.IsNumeric())
                        {
                            if (res.ToInteger() >= 120) SetInd(24, "1:1 RN at least 120 mins");
                        }
                    }
                }
            }
        }

        private void Check_Other()
        {
            string fndlist;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("Other Indicators");
            Program.VerboseAudit("---------------");

            fndlist = "FND102229,FND103672";
            if (ResultContains("PCSRPTROM", fndlist, "", "", ""))
            {
                SetMultiInd("50,4", "Restraints");
            }
            fndlist = "FND102229,FND103672";
            if (ResultContains("RESTRAIN11", fndlist, "", "", ""))
            {
                SetMultiInd("50,4", "Restraints");
            }
            fndlist = "FND101478";
            if (ResultContains("RESTRAIN9", fndlist, "", "", ""))
            {
                SetMultiInd("50,4", "Restraints");
            }
            fndlist = "FND110599";
            if (ResultContains("RESTRTYPE", fndlist, "", "", ""))
            {
                SetMultiInd("50,4", "Restraints");
            }
            fndlist = "FND105223";
            if (ResultContains("TRNSPRT8", fndlist, "", "", ""))
            {
                SetMultiInd("50,4", "Restraints");
            }

            fndlist = "FND101478";
            if (ResultContains("CODE/MET21", fndlist, "", "", ""))
            {
                SetMultiInd("58,4,6,18,20,24", "Code Blue/Rainbow");
            }
            fndlist = "FND101919";
            if (ResultContains("MET334", fndlist, "", "", ""))
            {
                SetMultiInd("58,4,6,18,20,24", "Code Blue/Rainbow");
            }
            fndlist = "FND101280";
            if (ResultContains("MISC10", fndlist, "", "", ""))
            {
                SetMultiInd("58,4,6,18,20,24", "Code Blue/Rainbow");
            }
            fndlist = "FND101478";
            if (ResultContains("CODE/MET21", fndlist, "", "", ""))
            {
                SetMultiInd("58,4,6,18,20,24", "Code Blue/Rainbow");
            }

            fndlist = "FND103622";
            if (ResultContains("PEENT8", fndlist, "", "", ""))
            {
                fndlist = "FND103631";
                if (ResultContains("", fndlist, "", "", ""))
                {
                    SetInd(60, "Sitter w/patient");
                }
                else
                {
                    fndlist = "FND103670";
                    SetIndIfResultContains(66, "", fndlist, "", "", "");
                }
            }
            fndlist = "FND103177";
            SetIndIfResultContains(61, "MISC41", fndlist, "", "", "");
            fndlist = "FND107356";
            SetIndIfResultContains(64, "MET333", fndlist, "", "", "");
            if (_inds[60].is_checked)
            {
                fndlist = "FND103631,FND103670";
                SetIndIfResultContains(66, "REAFORREST", fndlist, "", "", "");
                SetIndIfResultContains(66, "RESTRAIN5", fndlist, "", "", "");
            }
            fndlist = "FND103177";
            SetIndIfResultContains(68, "MISC38", fndlist, "", "", "");
            fndlist = "FND107085,FND107086";
            SetIndIfResultContains(69, "FALLRSK5", fndlist, "", "", "");

            fndlist = "FND111952,FND111953,FND111954,FND106430,FND111990,FND111988";
            if (ResultContains("FALLSAFEQ", fndlist, "", "", ""))
                SetMultiInd("69,12", "Fall Safety");

            fndlist = "FND106794";
            if (ResultContains("HYPOTH1", fndlist, "", "", ""))
            {
                SetMultiInd("70,4,6,18,20,21,24", "Hypothermia Protocol");
            }
            fndlist = "FND103252,FND103253";
            if (ResultContains("IABP2", fndlist, "", "", ""))
            {
                SetMultiInd("78,4,6,18,20,21,24", "IABP");
            }
            fndlist = "FND106788";
            if (ResultContains("NEURO1", fndlist, "", "", "") || ResultContains("NEURO2", fndlist, "", "", ""))
            {
                SetMultiInd("82,17,21", "ICP");
            }
            fndlist = "FND104079";
            if (ResultContains("NEURO1", fndlist, "", "", "") || ResultContains("NEURO2", fndlist, "", "", ""))
            {
                SetMultiInd("83,17,21", "Lumbar Drain");
            }

            fndlist = "FND0000";
            SetIndIfResultContains(87, "CODE/MET26", fndlist, "", "", "");
            fndlist = "FND104056";
            if (ResultContains("SPECRESP1", fndlist, "", "", ""))
            {
                SetMultiInd("101,4,6,18,20,24", "HFOV");
            }

            fndlist = "FND110771,FND110770,FND104755";
            if (ResultContains("TRNSPRT1", fndlist, "", "", "", SearchDepth.SearchPast24Hrs))
            {
                SetMultiInd("102,4,6,18,21,24", "CVOR");
            }
            fndlist = "FND110771,FND104755";
            if (ResultContains("TRNSPRT16", fndlist, "", "", "", SearchDepth.SearchPast24Hrs))
            {
                SetMultiInd("102,4,6,18,21,24", "CVOR");
            }

            fndlist = "FND101150,FND101151,FND102229,FND103250,FND103251,FND103252,FND103253,FND103274,FND103275,FND103276,FND103277";
            if (ResultContains("CVSS1", fndlist, "", "", "", SearchDepth.SearchPast12Hrs))
            {
                SetMultiInd("109,2,6,12,17,21", "Post-Cath");
            }
            fndlist = "FND101150,FND101151,FND103250,FND103251,FND103252,FND103253,FND103274,FND103275,FND103276,FND103277";
            if (ResultContains("CVSS2", fndlist, "", "", "", SearchDepth.SearchPast12Hrs))
            {
                SetMultiInd("109,2,6,12,17,21", "Post-Cath");
            }
            fndlist = "FND106040,FND106046";
            if (ResultContains("TRNSPRT1", fndlist, "", "", "", SearchDepth.SearchPast12Hrs))
            {
                SetMultiInd("109,2,6,12,17,21", "Post-Cath");
            }
            fndlist = "FND105248,FND105249,FND105250";
            if (ResultContains("TRNSPRT16", fndlist, "", "", "", SearchDepth.SearchPast12Hrs))
            {
                SetMultiInd("109,2,6,12,17,21", "Post-Cath");
            }

            fndlist = "FND103361,FND103363,FND103364,FND110457,FND110458";
            if (ResultContains("CVSS1", fndlist, "", "", "", SearchDepth.SearchPast12Hrs))
            {
                SetMultiInd("110,3,6,12,17,21", "Post-Cath with Sheath");
            }
            if (ResultContains("CVSS2", fndlist, "", "", "", SearchDepth.SearchPast12Hrs))
            {
                SetMultiInd("110,3,6,12,17,21", "Post-Cath with Sheath");
            }
            fndlist = "FND103374";
            if (ResultContains("PEENT8", fndlist, "", "", "", SearchDepth.SearchPast12Hrs))
            {
                SetMultiInd("110,3,6,12,17,21", "Post-Cath with Sheath");
            }

            fndlist = "FND103177";
            SetIndIfResultContains(117, "MISC39", fndlist, "", "", "");

        }

        private void Check_TxArea()
        {
            string fndlist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("Treatment Area");
            Program.VerboseAudit("---------------");
//C2B	355
//N3G	305
//N6G	27218005
//S1B	27218006
//S2E	530265
//S3B	265
//S3E	415
//S4B	465
//S5E	440

            if (_pat.unit_id == 305 || _pat.unit_id == 465 || _pat.unit_id == 27218006)
            {
                // N3G,S4B,S1B
                fndlist = "MCK_TOTAL";
                if (ResultContains("NIH", fndlist, "", "", ""))
                {
                    txarea="Stroke";
                }
            }

            if (_pat.unit_id == 355)
            {
                fndlist = "FND110771,FND104755,FND110770";
                if (ResultContains("", fndlist, "", "", ""))
                {
                    txarea = "Surgical Cardiac";
                }
            }
            if (_pat.unit_id == 440)
            {
                fndlist = "FND101541,FND108401,FND101512,FND101542,FND108400";
                if (ResultContains("CAPD3", fndlist, "", "", ""))
                {
                    txarea = "Renal";
                }
                else
                {
                    fndlist = "FND103792,FND103795,FND103793,FND103794";
                    if (ResultContains("RENAL1122", fndlist, "", "", ""))
                    {
                        txarea = "Renal";
                    }
                }
            }
            if (_pat.unit_id == 530265 || _pat.unit_id == 415)
            {
                fndlist = "FND103374";
                if (ResultContains("PEENT8 ", fndlist, "", "", ""))
                {
                    txarea = "Cath with Sheath";
                }
            }

            if (_pat.unit_id == 265)
            {
                if (_pat.age >= 16.0)
                    txarea = "Adult Pt";
                else
                    txarea = "Pedi Pt";
            }
        }

        private void AtLeastOneADL()
        {
            Program.Audit("---------------");
            if (_inds[1].is_checked)
            {
                var list = new List<int> {305,355}; // N$G C2A
                if (list.Contains(_pat.unit_id))
                {
                    SetInd(2, "Promoting to ADL partial care for this unit.");
                }
            }
            if (!(_inds[1].is_checked || _inds[2].is_checked || _inds[3].is_checked || _inds[4].is_checked)) {
                // Make "#2 ADL - Assist" the default.  (90% of patients)
                SetInd(2, "Defaulting to ADL partial care due to lack of documentation.");
            }
        }

        private void HighestIndicatorInEachGroupWins()
        {
            int i, g;
            bool highest_is_on;
            string ind_list;

            Program.Audit("Select highest indicator in each group");
            
            g = 0;
            highest_is_on = false;
            // Go from bottom up - highest (lowest) indicator in each group wins
            for (i = MAX_INDS; (i >= 1); i--) {
                if (_inds[i].radio_group > 0) {
                    if (_inds[i].radio_group != g) {
                        //this is a new group
                        g = _inds[i].radio_group;
                        highest_is_on = _inds[i].is_checked;
                    } else {
                        //same group
                        if (highest_is_on) {
                            _inds[i].is_checked = false;             //uncheck a lower number
                        } else {
                            highest_is_on = _inds[i].is_checked;     //save this one
                        }
                    }
                }
            } // next i
            
            //Echo the indicators for an Audit (no classification will be saved)
            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++) {
                if (_inds[i].is_checked) ind_list += "," + i;
            } // next i

            Program.Audit("Final list = " + ind_list.Substring(1));
            Program.Audit("---------------");
        }

        private int DeterminePtypeOfIndicators()
        {
            int i;
            var pscore = 0.0;
            int pt_type = 6;
            var indlist = new List<int>();
            string indstr = "";

            Program.VerboseAudit("Determine patient type of triggered indicators");

            for (i = 1; (i <= MAX_INDS); i++)
            {
                if (_inds[i].is_checked)
                {
                    indlist.Add(i);
                    indstr += i.ToString()+",";
                }
            }

            var db = PFSUtility.NewPfsDataContext(); 
            var query_ind_def = from ind_def in db.INDICATOR_DEFINITIONs
                                where (ind_def.METHODOLOGY_ID == PFSGlobal.METH_ID_APLUS_INPATIENT2) &&
                                  indlist.Contains(ind_def.INDICATOR_NUMBER)
                                select new
                                {
                                    ind_def.WEIGHT
                                };
            foreach (var wgts in query_ind_def)
            {
                pscore += wgts.WEIGHT;
            }
            Program.VerboseAudit("indicators=" + indstr);
            Program.VerboseAudit("score=" + pscore.ToString());

            var query_ptype = from ptype in db.PATIENT_TYPEs
                              where (ptype.METHODOLOGY_ID == PFSGlobal.METH_ID_APLUS_INPATIENT2)
                              orderby ptype.PATIENT_TYPE1
                              select new
                              {
                                  ptype.PATIENT_TYPE1,
                                  ptype.POINTS_HIVAL
                              };

            foreach (var ptypes in query_ptype)
            {
//                Program.DebugTrace("type,hival=" + ptypes.PATIENT_TYPE1.ToString() + "," + ptypes.POINTS_HIVAL.ToString(), "");
                if (pscore <= ptypes.POINTS_HIVAL)
                {
                    if (pt_type > ptypes.PATIENT_TYPE1)
                    {
                        pt_type = ptypes.PATIENT_TYPE1;
                    }
                }
            }
            Program.VerboseAudit("patient type=" + pt_type.ToString());

            return pt_type;

        }


        private bool ExistDefaultInPast16hrs()
        {
            DateTime cdt1 = DateTime.MinValue;
            DateTime cdt2 = DateTime.MinValue;
            int cnt_all = 0;
            int cnt_def = 0;
            //get max class date of last non-default class = 1
            //get max class date of last default = 2
            // if 1 >= 2 then false
            // else
            //   if 2 > 1 and date is <= 16 hrs ago then true
            //   else false
            var db = PFSUtility.NewPfsDataContext();
            //var query = from ce in db.CLASSIFICATION_EVENTs
            //            where (ce.ENCOUNTER_ID == _pat.encounter_id)
            //            && (ce.CLASSIFIED_BY_ID != -2)
            //            && (System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(ce.CLASSIFICATION_DATETIME, Program.g_pull_start) <= 960)
            //            select ce;
            //cnt_all = query.Count();
            //if (cnt_all > 0)
            //{
            //    cdt1 = PFSUtility.DBToDateTime(query.Max(x => x.CLASSIFICATION_DATETIME));
            //    Program.VerboseAudit("Last regular classification was at:"+cdt1.ToString());
            //}
            //else {
            //    Program.VerboseAudit("No regular classifications within the past 16 hours");
            //}

            var query = from ce in db.CLASSIFICATION_EVENTs
                    where (ce.ENCOUNTER_ID == _pat.encounter_id)
                    && (ce.CLASSIFIED_BY_ID == -2)
                    && (System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(ce.CLASSIFICATION_DATETIME, Program.g_pull_finish) <= 960)
                    select ce;
            cnt_def = query.Count();

            if (cnt_def > 0)
            {
                cdt2 = PFSUtility.DBToDateTime(query.Max(x => x.CLASSIFICATION_DATETIME));
                Program.VerboseAudit("Last default classification was at:"+cdt2.ToString());
            }
            else {
                Program.VerboseAudit("No default classifications within the past 16 hours");
            }
             return (cnt_def > 0);

        }

        private void CheckOutcomes()
        {
            string fndlist;
            DateTime evdt;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("Outcomes Indicators");
            Program.VerboseAudit("---------------");
   
            fndlist = "FND0000";
            if (GetEVDT("FALLRSK", fndlist, "", "", out evdt))
            {
                var oc = new proc_data();
                oc.procedure_number = 1;
                oc.start = evdt;
                _ocinds.Add(oc);
                Program.Audit("Outcome 1: Found FALLRSK+FND0000 at " + evdt);
            }

            fndlist = "FND101495";
            if (GetEVDT("", fndlist, "", "", out evdt))
            {
                var oc = new proc_data();
                oc.procedure_number = 3;
                oc.start = evdt;
                _ocinds.Add(oc);
                Program.Audit("Outcome 3: Found FND101495 at " + evdt);
            }
            fndlist = "FND101919,FND101478,FND101280";
            if (GetEVDT("", fndlist, "", "", out evdt))
            {
                var oc = new proc_data();
                oc.procedure_number = 30;
                oc.start = evdt;
                _ocinds.Add(oc);
                Program.Audit("Outcome 30: Found Code Blue at " + evdt);
            }

            fndlist = "FND0000";
            if (GetEVDT("CODE/MET26", fndlist, "", "", out evdt))
            {
                var oc = new proc_data();
                oc.procedure_number = 31;
                oc.start = evdt;
                _ocinds.Add(oc);
                Program.Audit("Outcome 31: Found CODE/MET26+FND0000 at " + evdt);
            }


        }

        private void CheckProcs()
        {
            //CheckProc_1();
            //CheckProc_2();
            //CheckProc_3();
            //CheckProc_4();
            //CheckProc_5();
            //CheckProc_6();
            //CheckProc_7();
            //CheckProc_9();
            //CheckProc_10();
            //CheckProc_11();
        }

        private void CheckProc_1()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P1. 1-1 safety observation by RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_2()
        {
            string nowstr;
            string toddtstr;
            string yesdtstr;
            string timea ="";
            DateTime timea_startdt, timea_enddt;
            DateTime timeb_startdt, timeb_enddt;
            DateTime nowdt = _pat.pull_finish;              // "now" is pull time
            DateTime yesdt;
            
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P2. 1-1 safety observation by non-RN");
            Program.VerboseAudit("---------------");

            nowstr = nowdt.ToString("yyyyMMddHHmm");
            yesdt = nowdt.AddDays(-1);
            toddtstr = nowdt.ToString("yyyyMMdd");
            yesdtstr = yesdt.ToString("yyyyMMdd");
            
            //when is now? (yesterday/today)
            //yes 7am -- yes 7p  -- tod 7a -- tod 7p
            //                                   A                   B
            //if nowdt >= tod7pm then check  tod 7am-tod 7pm and tod 7pm-tom 7a
            //if nowdt >= tod7am then check  yes 7p - tod 7a     tod 7a-tod 7p
            //if nowdt >= yes7pm then check  yes 7a-yes 7p       yes 7p-tod 7a
            if (nowstr.CompareTo(toddtstr + "1900") >= 0) {
                timea = toddtstr + "0700";
            } else if (nowstr.CompareTo(toddtstr + "0700") >= 0) {
                timea = yesdtstr + "1900";
            } else if (nowstr.CompareTo(yesdtstr + "1900") >= 0) {
                timea = yesdtstr + "0700";
            }
            
            timea_startdt = PFSUtility.ISOToDateTime(timea);
            timea_enddt = timea_startdt.AddHours(12);
            MaybeAddSitter(timea_startdt, timea_enddt);

            timeb_startdt = timea_enddt;
            timeb_enddt = timeb_startdt.AddHours(12);
            MaybeAddSitter(timeb_startdt, timeb_enddt);
        }

        private void MaybeAddSitter(DateTime startdt, DateTime enddt)
        {
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);    // add custom time range below
            query = query.Where(e => (e.EVENT_DATETIME >= startdt) && (e.EVENT_DATETIME < enddt));
            query = AndCodeInList(query, "Sitter");
            query = AndResultInList(query,"continued, initiated");
            query = AndResultNotInList(query, "discontinued");

            if (query.Count() > 0) {
                if (ProcExists(2, startdt, enddt)) {
                    Program.Audit("Procedure 2: already exists");
                } else {
                    var proc = new proc_data();
                    proc.procedure_number = 2;
                    proc.start = startdt;
                    proc.finish = enddt;
                    _procs.Add(proc);
                    Program.Audit("Procedure 2: Found Sitter between " + startdt + " and " + enddt);
                }
            }
            
        }

        private bool ProcExists(int pnum, DateTime startdt, DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && (proc.PROCEDURE_DATETIME == startdt)
                            && (proc.DEPARTURE_DATETIME == enddt)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new {proc.PROCEDURE_EVENT_ID};
            return (query.Count() > 0);
        }


        private void CheckProc_3()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P3. Off unit accompanied by RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_4()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P4. Off unit accompanied by non-RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_5()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P5. Patient/family education by RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_6()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P6. Extensive wound management by RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_7()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P7. Extensive wound management by non-RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_8()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P8. Coordination of care by RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_9()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P9 1:1 RN at bedside");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_10()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P10. 1:1 non-RN at bedside");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_11()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P11. 2:1 by RN at bedside");
            //Program.VerboseAudit("---------------");
        }

        
        private const string DATETIME_FORMAT = "yyyyMMddHHmm";              // ISO Date/Time w/o seconds

        private void OutputClass(bool use_default)
        {
            string outstr, ind_list, desc, str_pull_dt;
            int i, tc_event_id;

            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         212 242 304
            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1
            //1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901 3456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
            //1       |10 5            |                |                |        |10023121779         |ALEXANDER                       |GLORIA                          |                                |0508    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289530  |           |201403030400                                                                      |NNYNNNNYYNNYNNNYNNNYYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//1       |10 5            |                |                |        |10024544908         |ARMSTRONG                       |AARON                           |                                |0535    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289531  |           |201403030400                                                                      |NYNNYNNNNNNYNNNYNNNNYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//|0547    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289532  |           |201403030400                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//|0547    |A   |201403030700|                |201403030700    |18  |C|    |3040|1440|18289532  |           |201403030700                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN

            if (Program.g_is_test)
                tc_event_id = 9999;
            else
                tc_event_id = PFSUtility.NextGID();                         //get a unique id for this class
            str_pull_dt = _pat.pull_finish.ToString(DATETIME_FORMAT);

            outstr = _pat.facilty_code.FixedWidth(8);                       //(facility code)
            outstr += "|" + _pat.unit_name.FixedWidth(16);
            outstr += "|" + "".FixedWidth(16);                               //(unit code)
            outstr += "|" + txarea.FixedWidth(16);                               //(area code)
            outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
            outstr += "|" + _pat.acct.FixedWidth(20);
            outstr += "|" + _pat.last_name.FixedWidth(32);
            outstr += "|" + _pat.first_name.FixedWidth(32);
            outstr += "|" + _pat.middle_name.FixedWidth(32);
            outstr += "|" + _pat.room.FixedWidth(8);
            outstr += "|" + _pat.bed.FixedWidth(4);
          outstr += "|" + str_pull_dt.FixedWidth(12);                      //class datetime (could change)
            outstr += "|" + "".FixedWidth(16);                               //(login)
          outstr += "|" + str_pull_dt.FixedWidth(16);                      //(employee)/(pull datetime)
            outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
            outstr += "|" + "C".FixedWidth(1);                               //record type = class
            outstr += "|" + "".FixedWidth(4);                                //(stage)
            outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
            outstr += "|" + _pat.range.ToString().FixedWidth(4);             //TC pull range
            outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
            outstr += "|";
            outstr = outstr.FixedWidth(294);
          outstr += "|" + _pat.effective.ToString(DATETIME_FORMAT);        //IN
            outstr = outstr.FixedWidth(377);
            outstr += "|";

            if (use_default)
            { //make all is_checked = false and then mark defaults
                Program.VerboseAudit("Patient Will receive default indicators " + _pat.default_inds_str);
                for (i = 1; (i <= MAX_INDS); i++)
                {
                    _inds[i].is_checked = false;
                }
                foreach (var ind in _pat.default_inds)
                {
                    if (ind <= _inds.GetUpperBound(0))
                    {
                        _inds[ind].is_checked = true;
                    }
                }
            }

            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++)
            {
                if (_inds[i].is_checked)
                {
                    outstr += "Y";
                    ind_list += "," + i;
                }
                else
                {
                    outstr += "N";
                }
            } // next i
            ind_list = ind_list.Substring(1);                           //strip leading comma
  
            Program.outfile.WriteLine(outstr);                          //output to transparent.txt

            Program.Audit("");
            desc = "Classified: " + ind_list;
            if (Program.g_is_test) {
                Program.Audit(desc);
            } else {
                //Save the selected indicators plus both Program.Audits in the event log; link with tc_event_id
                PFSEventLog.AddTransparentMappingEventLogEntry(
                    desc, Program.gLogUnitID, Program.gLogEncounterID,
                    tc_event_id, Program.gLogMapperVersion,
                    Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
            }
        }

        private void OutputProcs()
        {
            int i;
            string outstr, proc_list, desc;
            int tc_event_id;

            foreach(var proc in _procs) {
                if (Program.g_is_test)
                    tc_event_id = 9999;
                else
                    tc_event_id = PFSUtility.NextGID();                         //get a unique id for this proc

                outstr = _pat.facilty_code.FixedWidth(8);
                outstr += "|" + _pat.unit_name;                                 //10
                outstr = outstr.FixedWidth(68);
                outstr += "|" + _pat.acct.FixedWidth(20);                       //90
                outstr += "|" + _pat.last_name.FixedWidth(32);
                outstr += "|" + _pat.first_name.FixedWidth(32);
                outstr += "|" + _pat.middle_name.FixedWidth(32);
                outstr = outstr.FixedWidth(202);
                outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //204 proc dt
                outstr = outstr.FixedWidth(254);
                outstr += "|P";                                                 //256 procedure type record
                outstr += "|" + "".FixedWidth(4);                               //(stage)
                outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);     //TC source ID
                outstr += "|" + _pat.range.ToString().FixedWidth(4);            //TC pull range
                outstr += "|" + tc_event_id.ToString().FixedWidth(10);          //TC event ID
                outstr += "|";
                outstr = outstr.FixedWidth(294);
                outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //296 procdt in
                outstr = outstr.FixedWidth(346);
                outstr += "|" + proc.finish.ToString(DATETIME_FORMAT);          //348 procdt out
                outstr = outstr.FixedWidth(377);
                outstr += "|";
                
                proc_list = "";
                for (i = 1; (i < MAX_PROCS); i++) {
                    if (proc.procedure_number == i) {
                        outstr += "Y";
                        proc_list += "," + i;
                    } else {
                        outstr += "N";
                    }
                } // next i
                proc_list = proc_list.Substring(1);                             //strip leading comma

                Program.outfile.WriteLine(outstr);                              //output to transparent.txt

                desc = "Procedures: " + proc_list;
                if (Program.g_is_test) {
                    Program.Audit(desc);
                } else {
                    //Save the selected procedures plus both Program.Audits in the event log; link with tc_event_id
                    //(this assumes the procedure Program.Audit is mixed in with the class Program.Audit)
                    PFSEventLog.AddTransparentMappingEventLogEntry(desc, Program.gLogUnitID, Program.gLogEncounterID,
                        tc_event_id, Program.gLogMapperVersion,
                        Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
                }
            } // next proc
        }

        private void OutputOutcomes()
        {
            int i;
            string outstr, oc_list, desc;
            //int tc_event_id;

            foreach (var oc in _ocinds)
            {
                outstr = new String(' ', 9);
                outstr += _pat.unit_name;                                 //
                outstr = outstr.FixedWidth(60);
                outstr += oc.start.ToString(DATETIME_FORMAT);           //
                outstr = outstr.FixedWidth(73);
                outstr += _pat.acct.FixedWidth(20);                       //
                outstr = outstr.FixedWidth(94);

                oc_list = "";
                for (i = 1; (i < MAX_OCI); i++)
                {
                    if (oc.procedure_number == i)
                    {
                        outstr += i;
                        oc_list += "," + i;
                    }
                    else
                    {
                        outstr += "N";
                    }
                } // next i
                oc_list = oc_list.Substring(1);                             //strip leading comma

                Program.ocoutfile.WriteLine(outstr);                              //output to outcomesindicator.txt

                desc = "Outcomes: " + oc_list;
                if (Program.g_is_test)
                {
                    Program.Audit(desc);
                }
                else
                {
                    //Save the selected procedures plus both Program.Audits in the event log; link with tc_event_id
                    //(this assumes the procedure Program.Audit is mixed in with the class Program.Audit)
                    //PFSEventLog.AddTransparentMappingEventLogEntry(desc, Program.gLogUnitID, Program.gLogEncounterID,
                    //    tc_event_id, Program.gLogMapperVersion,
                    //    Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
                }
            } // next proc
        }

    }
}
