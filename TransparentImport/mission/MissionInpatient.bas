Attribute VB_Name = "MissionInpatient"
Option Explicit
'
' Mission Inpatient
'
' This processes one patient using the Inpatient methodology.
'
' All search functions use exact match for code.
'
' result_like looks for LIKE matches in the result, except where EXACT_MATCH_PREFIX is pre-appended to target.
' result_list looks for any one of a list of words exactly as the result.
'assessments are counted by buckets, looking at the max count of any 12-hours period:
'Counts: 2,0,0,0,0,2,0,0,1,3,0,0,0,7,0,2,2,2,0,4,3,0,2,2,
'Set Ind #15: Med Mgt max count=8

'
Private Const MAX_PROCS = 20

Private Const EXACT_MATCH_PREFIX = "&!"

Private Type indicator_data
    checked     As Boolean
    group       As Integer
    also_mark   As String                   'not used at Arnot (handy with user-defined indicators?)
End Type

Private Type procdata                       'not used at Arnot
    pnum    As Integer
    start   As Date
    finish  As Date
    isvalid As Boolean 'used in combining procs
    pindex As Integer 'used in combining procs = the index of procs whose start/finish times are equal to this one
End Type
Private Type ocdata                       'not used at Arnot
    checked As Boolean
    pnum    As Integer
    start   As String
End Type
Private Type bucket_type
    startdt As String
    enddt As String
End Type
Private Type proc_type
    evdt As Date
    mins As Integer
End Type

Private inds(MAX_INDS)          As indicator_data

Private numprocs                As Integer
Private numoutcomes             As Integer
Private procs(MAX_PROCS)        As procdata
Private oc(MAX_PROCS)           As ocdata

Private WHERE_ENCOUNTER         As String
Private AND_UNIT                As String
Private AND_PULL_RANGE          As String
Private AND_OUTCOMES_RANGE      As String
Private AND_ARRIVAL             As String
Private BUCKET_CALC             As String

Private m_pat                   As PatientInfo
Private m_assist_count          As Integer

Private bucket(24)             As bucket_type
Private num_buckets             As Integer
Private isq30                   As Boolean

Private Enum SearchMode
    SearchDefault
    Searchpullrange             'search within the current pull     -- default
    SearchSinceArrival          'search since arrival to the unit
    SearchSinceAdmission        'search everything since admission to the hospital
    SearchOutcomesRange         'search within the current pull + 24 hours before
    SearchAssessments
End Enum

Private Enum CountMode
    CountAll
    CountFirst                  'stop after one is found
End Enum

Private Enum GetValueMode
    GetTotal
    GetMax
    GetLast
End Enum

Private Const FREQUENCY_BUCKET_SIZE = 20    'min

Private Enum Frequencies
    QNONE
    Q4H
    Q2H
    Q1H
    Q30M
End Enum

Private Type fmapRow
    los_high                As Single       'the LOS being testing
    freq(QNONE To Q30M)     As Integer      'the count required for each Q value
End Type

Private m_freq_map(6)       As fmapRow      '1,2,4,8,12,24 hours
Private pary(MAX_PROCS)     As proc_type
Private nump                As Integer


'This is the main entry point
'
Public Sub ProcessInpatient(pat As PatientInfo)
    Dim los As Long
    
    On Error GoTo errHandler
    m_pat = pat
'    frmMain.SetProgress "Processing acct: " & m_pat.acct
    InitGroupsIfNeeded
    SetSQLConstants
    If m_pat.is_icu Then AND_UNIT = ""  'look at all data if ICU
    LoadFreqTable
    ResetIndicators
    ResetProcs
    ResetOutcomes
    
    los = DateDiff("n", m_pat.unit_arrival, g_pull_finish)
    If los <= 240 Then
        dvprint "Patient was here 4 hrs or less. Will receive default indicators."
        CheckDefaultIndicators (m_pat.unit_id)
    Else
        SetupBuckets

        Check_1_2_3
        Check_4
        Check_5_6
        Check_7
        Check_8
        Check_9_10
        Check_11_12
        Check_13
        Check_14_15_16_17
        Check_18
        Check_19_20
        Check_21
        Check_22
    End If

    CheckProcs
'    CheckOutcomes
    AtLeastOneADL
    HighestIndicatorInEachGroupWins

    If g_no_output Then Exit Sub
    OutputClass
    OutputProcs
    OutputOutcomes
    Exit Sub
    
errHandler:
    LogError Err.Description & " in " & Err.source, EVENT_CATEGORY_UNEXPECTED
    Exit Sub
    Resume  'debug
End Sub

Private Sub ResetIndicators()
    Dim i As Integer
    
    For i = 1 To MAX_INDS
        inds(i).checked = False
        inds(i).also_mark = ""
    Next i
    isq30 = False
End Sub

Private Sub ResetProcs()
    numprocs = 0
End Sub
Private Sub ResetOutcomes()
    numoutcomes = 0
End Sub

Private Sub InitGroupsIfNeeded()
    On Error GoTo errHandler
    
    Static been_here As Boolean
    Dim rs As New Recordset
    Dim sql As String
    Dim inum As Integer
    
    If been_here Then Exit Sub
    
    'get indicator radio groups from the database
    sql = "SELECT INDICATOR_NUMBER, RADIO_GROUP FROM INDICATOR_DEFINITION WHERE METHODOLOGY_ID=" & m_pat.meth_id
    rs.Open sql, g_cnADO
    
    Do While Not rs.EOF
        inum = rs("INDICATOR_NUMBER")
        If (inum < MAX_INDS) Then
            inds(inum).group = g_dbutil.DBToInteger(rs("RADIO_GROUP"))  'could be null
        End If
        rs.MoveNext
    Loop
    
    rs.Close
    Exit Sub
    
errHandler:
    g_util.ThrowError "InitGroupsIfNeeded"
End Sub

Private Sub LoadFreqTableRow(m_freq As fmapRow, los_high As Single, values As String)
    Dim arr() As String, n As Integer, i As Integer
    
    m_freq.los_high = los_high

    n = g_util.SplitTextOnChar(values, ",", arr(), 0, 0)
    For i = 0 To n - 1
        m_freq.freq(i) = CInt(arr(i))
    Next i
End Sub

Private Sub LoadFreqTable()
                                 ' LOS,   None Q4h Q2h Q1h Q30m
    LoadFreqTableRow m_freq_map(1), 1, "    0,  0,  0,  0,  1"
    LoadFreqTableRow m_freq_map(2), 2, "    0,  0,  0,  1,  2"
    LoadFreqTableRow m_freq_map(3), 4, "    0,  1,  2,  3,  4"
    LoadFreqTableRow m_freq_map(4), 8, "    0,  2,  3,  4,  8"
    LoadFreqTableRow m_freq_map(5), 12, "   0,  2,  4,  6, 12"
    LoadFreqTableRow m_freq_map(6), 9999, " 0,  3,  6, 12, 24"         '24+
    
    'To read this table: if you have at least 6 asessmenets in a 12-hour period, you get Q1h
    '(row 5: LOS=12, column Q1h has a count of 6)
End Sub

Private Function FreqForCount(los_hours As Single, count As Integer) As Frequencies
    Dim i As Integer, j As Integer
    
    For i = 1 To UBound(m_freq_map)                         'go from LOS 1 to 24+
        If (los_hours <= m_freq_map(i).los_high) Then
            For j = Q30M To QNONE Step -1                   'search right to left
                If (count >= m_freq_map(i).freq(j)) Then
                    FreqForCount = j
                    Exit Function
                End If
            Next j
        End If
    Next i
    
    FreqForCount = QNONE
End Function

Private Sub SetSQLConstants()
    'These SQL constants are combined in different ways for searching
    WHERE_ENCOUNTER = " where (encounter_id = " & m_pat.encounter_id & ")"
    AND_UNIT = " and (unit_id = " & m_pat.unit_id & ")"
    AND_PULL_RANGE = " and (event_datetime between " & g_dbutil.SQL_DateTime(m_pat.pull_start) & " and " & g_dbutil.SQL_DateTime(m_pat.pull_finish) & ")"
    AND_ARRIVAL = " and (event_datetime between " & g_dbutil.SQL_DateTime(m_pat.unit_arrival) & " and " & g_dbutil.SQL_DateTime(m_pat.pull_finish) & ")"
    AND_OUTCOMES_RANGE = " and (event_datetime between " & g_dbutil.SQL_DateTime(DateAdd("d", -1, m_pat.pull_start)) & " and " & g_dbutil.SQL_DateTime(m_pat.pull_finish) & ")"
    
    'What bucket number does the event belong to?
    BUCKET_CALC = "DateDiff(minute," & g_dbutil.SQL_DateTime(m_pat.pull_start) & ",event_datetime) / " & FREQUENCY_BUCKET_SIZE
End Sub

'Add this to all chart item queries to start the WHERE clause
'This is where the search depth is set
Private Function WhereBase(Optional search_mode As SearchMode = SearchDefault)
    Dim result As String
    
    Select Case search_mode
    Case Searchpullrange, SearchDefault
        result = WHERE_ENCOUNTER & AND_UNIT & AND_PULL_RANGE    'search within pull range
    Case SearchSinceArrival
        result = WHERE_ENCOUNTER & AND_UNIT & AND_ARRIVAL       'search since arrival to the unit
    Case SearchSinceAdmission
        result = WHERE_ENCOUNTER                                'search since admission to the hospital (any unit)
    Case SearchOutcomesRange
        result = WHERE_ENCOUNTER & AND_UNIT & AND_OUTCOMES_RANGE    'search within pull range+24hrs before
    Case SearchAssessments
        result = WHERE_ENCOUNTER & AND_UNIT     'search within 12 hour range
    End Select
    
    'Since we are looking for one patient in one facility, we don't have to filter by TC_source_ID.
    
    WhereBase = result
End Function

Private Function ValueIsAList(result As String) As Boolean
    ValueIsAList = (InStr(1, result, ",") > 0)                'is there a comma-separated list?
End Function

'Look for any of these fields.  Cat/desc/field = exact match.  Result = like match.
Private Function AndSimpleItemFilter(cat As String, code As String, desc As String, field As String, result_like As String) As String
    Dim result As String
    
    If Len(cat) Then result = result & " and category=" & g_dbutil.SQL_String(cat)
    If Len(code) Then result = result & " and (code=" & g_dbutil.SQL_String(code) & " or code like " & g_dbutil.SQL_String(code & "(%") & ")"
    If Len(desc) Then result = result & " and description like '" & Trim$(desc) & "%'"
    If Len(field) Then result = result & " and field_name=" & g_dbutil.SQL_String(field)
    If InStr(result_like, EXACT_MATCH_PREFIX) = 1 Then 'exact match
        result_like = Mid$(result_like, 3, Len(result_like) - Len(EXACT_MATCH_PREFIX))
        If Len(result_like) Then result = result & " and result= " & g_dbutil.SQL_String(result_like)
    Else
        If Len(result_like) Then result = result & " and result like '%" & Trim$(result_like) & "%'"
    End If

    AndSimpleItemFilter = result
End Function

'Look for a result that contains one of the words in the list
'The only drawback with this function is that we don't know what it found (for audits)
'Because of this, it is only used in as few places.
Private Function AndResultContains(result_list As String) As String
    Dim arr() As String
    Dim i As Integer, n As Integer
    Dim result As String

    If Len(result_list) = 0 Then Exit Function
    
    n = g_util.SplitTextOnChar(result_list, ",", arr(), 1, 0)
    
    result = "and ((1=0)"   ' or (result=this) or (result=that)
    
    For i = 1 To n
        result = result & " or (result like '" & "%" & Trim$(arr(i)) & "%')"
    Next i
    
    result = result & ")"
    
    AndResultContains = result
End Function

Private Function DescribeSearchDepth(search_mode As SearchMode) As String
    Dim result As String
    
    Select Case search_mode
    Case Searchpullrange, SearchDefault
        'result = "in pull range"
        result = ""                         'be quiet since this is default
    Case SearchSinceArrival
        result = "since arrival to unit"
    Case SearchSinceAdmission
        result = "since admission"
    Case SearchOutcomesRange
        result = "since 24hrs before pull"
    End Select
    
    DescribeSearchDepth = result
End Function

Private Function Describe(cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = Searchpullrange) As String
    Dim sql As String, and_filter As String
    Dim rs As New Recordset
    Dim result As String
   
    If Not g_debug Then Exit Function           'avoid extra overhead if not making a log

    result = "looking for"
    If Len(cat) Then result = result & " cat='" & cat & "'"
    If Len(code) Then result = result & " code='" & code & "'"
    If Len(desc) Then result = result & " desc='" & desc & "'"
    If Len(field) Then result = result & " field='" & field & "'"
    If Len(result_list) Then result = result & " result contains '" & result_list & "'"

    If ValueIsAList(result_list) Then
        and_filter = AndSimpleItemFilter(cat, code, desc, field, "") & AndResultContains(result_list)
    Else
        and_filter = AndSimpleItemFilter(cat, code, desc, field, result_list)
    End If
    
    sql = "select code, description, result from chart_item" & WhereBase(search_mode) & and_filter
    rs.Open sql, g_cnADO
    If rs.EOF Then
        result = result & "; not found " & DescribeSearchDepth(search_mode)
    Else
        result = result & "; found"
        'Add info for columns that were not specified and a value was found
        If (Len(code) = 0) And Len(rs("code")) Then result = result & " code='" & rs("code") & "'"
        If (Len(desc) = 0) And Len(rs("description")) Then result = result & " desc='" & rs("description") & "'"
        'Add the complete result found (we searched for a word or words)
        result = result & " result='" & rs("result") & "'"
        'Are there more results?  Just say how many; we aren't going to list them.
        If (rs.RecordCount = 2) Then
            result = result & " (1 more result)"
        ElseIf (rs.RecordCount > 2) Then
            result = result & " (" & rs.RecordCount - 1 & " more results)"
        End If
    End If
    rs.Close
    
    Describe = result
End Function

'Set an indicator for this reason (low level)
Private Sub SetInd(inum As Integer, reason As String)
    If inds(inum).checked And Not g_debug Then Exit Sub       'already set and no log?

    inds(inum).checked = True
    dprint "Set Ind #" & inum & ": " & reason
End Sub

'Clear an indicator for this reason (low level)
Private Sub ClrInd(inum As Integer, reason As String)
    If Not inds(inum).checked And Not g_debug Then Exit Sub   'already clear and no log?
    
    inds(inum).checked = False
    dprint "Clr Ind #" & inum & ": " & reason
End Sub

'Count how many items have the search word in result_like
'This is simple and fast
Private Function CountSimpleResult(cat As String, code As String, desc As String, field As String, result_like As String, Optional search_mode As SearchMode = Searchpullrange, Optional trace As Boolean = True, Optional found_what As String) As Long
    Dim sql As String, and_filter As String
    Dim rs As New Recordset
    Dim count As Long

    and_filter = AndSimpleItemFilter(cat, code, desc, field, result_like)
    sql = "select count(*) from chart_item" & WhereBase(search_mode) & and_filter
'    dvprint sql
    'Debug.Print sql
    rs.Open sql, g_cnADO
    count = rs(0)
    rs.Close
    
    'Normally CountSimpleResult will report what it finds; not eveyone wants this so the trace can be turned off
    found_what = Describe(cat, code, desc, field, result_like, search_mode) & IIf(count > 0, "; count=" & count, "")
    If trace Then dvprint found_what
    
    CountSimpleResult = count
End Function

'Count how many items have a result with one of the words given in the result_list
'Returns a description of what word was found
'This is more powerful than CountSimpleResult but it is slower
Private Function CountResultInList(cat As String, code As String, desc As String, field As String, result_list As String, search_mode As SearchMode, count_mode As CountMode, trace As Boolean, found_what As String) As Integer
    On Error GoTo errHandler
    Dim sql As String, and_filter As String, arr() As String
    Dim rs As New Recordset
    Dim i As Integer, n As Integer, count As Integer
    Dim pos As Long
    
    n = g_util.SplitTextOnChar(result_list, ",", arr(), 1, 0)
    
    'Do not filter by result here; we need all results
    and_filter = AndSimpleItemFilter(cat, code, desc, field, "")
    sql = "select code,result from chart_item" & WhereBase(search_mode) & and_filter
    'Debug.Print sql
    rs.Open sql, g_cnADO
    Do While Not rs.EOF
        'Look for each search word in the result (case insensitive)
        For i = 1 To n
            arr(i) = Trim$(arr(i))          'we split on comma so there usually are leading blanks
            If InStr(arr(i), EXACT_MATCH_PREFIX) = 1 Then
                arr(i) = Mid$(arr(i), 3, Len(arr(i)) - Len(EXACT_MATCH_PREFIX))
                pos = (rs("result") = arr(i))
            Else
                pos = InStr(1, rs("result"), arr(i), vbTextCompare)  'bad when looking for "RN" in "Non RN"
            End If
            If pos > 0 Then
                found_what = "found '" & arr(i) & "' in code='" & rs("code") & "' result='" & rs("result") & "'"
                If trace Then dvprint found_what
                count = count + 1
                If count_mode <> CountAll Then Exit Do  'stop counting
                Exit For                                'try next record
            End If
        Next i

        rs.MoveNext
    Loop
    
    If (count > 0) Then
        'We already printed what was found; maybe add how many?
        If trace And (count_mode = CountAll) Then dvprint "found " & count & " total"
    Else
        found_what = Describe(cat, code, desc, field, result_list, search_mode)       'not found
        If trace Then dvprint found_what
    End If
    
    rs.Close
    
    CountResultInList = count
    Exit Function
    
errHandler:
    g_util.ThrowError "CountResultInList"
    Resume   'debug
End Function

Private Function CountResultContains(cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = Searchpullrange, Optional trace As Boolean = True, Optional found_what As String) As Integer
    If ValueIsAList(result_list) Then
        CountResultContains = CountResultInList(cat, code, desc, field, result_list, search_mode, CountAll, trace, found_what)
    Else
        CountResultContains = CountSimpleResult(cat, code, desc, field, result_list, search_mode, trace, found_what)
    End If
End Function

Private Function ResultContains(cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = Searchpullrange, Optional trace As Boolean = True, Optional found_what As String) As Boolean
    'tracing can be turned off and you can ask for what was found instead of having it printed
    If ValueIsAList(result_list) Then
        ResultContains = (CountResultInList(cat, code, desc, field, result_list, search_mode, CountFirst, trace, found_what) > 0)
    Else
        ResultContains = (CountSimpleResult(cat, code, desc, field, result_list, search_mode, trace, found_what) > 0)
    End If
End Function


Private Function CountResultNotInList(cat As String, code As String, desc As String, field As String, result_list As String, search_mode As SearchMode, count_mode As CountMode, trace As Boolean, found_what As String) As Integer
    On Error GoTo errHandler
    Dim sql As String, and_filter As String, arr() As String
    Dim rs As New Recordset
    Dim i As Integer, n As Integer, count As Integer
    Dim found_one As Boolean

    n = g_util.SplitTextOnChar(result_list, ",", arr(), 1, 0)
    
    'Do not filter by result here; we need all results
    and_filter = AndSimpleItemFilter(cat, code, desc, field, "")
    sql = "select code, result from chart_item" & WhereBase(search_mode) & and_filter
    'Debug.Print sql
    rs.Open sql, g_cnADO
    
    Do While Not rs.EOF
        'Look for each search word in the result (case insensitive)
        found_one = False
        
        For i = 1 To n
            arr(i) = Trim$(arr(i))          'we split on comma so there usually are leading blanks
            If InStr(1, rs("result"), arr(i), vbTextCompare) > 0 Then
                found_what = "found '" & arr(i) & "' in code='" & rs("code") & "' result='" & rs("result") & "' -- ignore this"
                If trace Then dvprint found_what
                found_one = True
                Exit For
            End If
        Next i

        If (Not found_one) Then
            'none of the words were found - good!
            found_what = Describe(cat, code, desc, field, rs("result"), search_mode) & " -- does not contain '" & result_list & "'"
            If trace Then dvprint found_what
            count = count + 1
            If count_mode <> CountAll Then Exit Do
        End If

        rs.MoveNext
    Loop
    
    If (count > 0) Then
        'We already printed what was found; maybe show the total?
        If trace And (count_mode = CountAll) Then dvprint "found " & count & " total"
    ElseIf (rs.RecordCount > 0) Then
        'We already printed what we ignored
    Else
        'Print what did not exist
        found_what = Describe(cat, code, desc, field, "", search_mode)      'not found
        If trace Then dvprint found_what
    End If
    
    rs.Close
    
    CountResultNotInList = count
    Exit Function
    
errHandler:
    g_util.ThrowError "CountResultNotInList"
    Resume   'debug
End Function

Private Function CountResultDoesNotContain(cat As String, code As String, desc As String, field As String, result_list As String, found_what As String, Optional search_mode As SearchMode = Searchpullrange, Optional trace As Boolean = True) As Integer
    CountResultDoesNotContain = CountResultNotInList(cat, code, desc, field, result_list, search_mode, CountAll, trace, found_what)
End Function

Private Function ResultDoesNotContain(cat As String, code As String, desc As String, field As String, result_list As String, found_what As String, Optional search_mode As SearchMode = Searchpullrange, Optional trace As Boolean = True) As Boolean
    ResultDoesNotContain = (CountResultNotInList(cat, code, desc, field, result_list, search_mode, False, trace, found_what) > 0)
End Function

'Set the indicator if the result contains one of the words in the result_list
Private Sub SetIndIfResultContains(inum As Integer, cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = Searchpullrange)
    Dim found_what As String
    'avoid more queries if the indicator is already set
    If inds(inum).checked Then Exit Sub
    
    'Turn trace off for ResultContains() so the result can be placed on the "SetInd#" line below
    If ResultContains(cat, code, desc, field, result_list, search_mode, False, found_what) Then
        SetInd inum, found_what             'echo here
    Else
        dvprint found_what                  'and here
    End If
End Sub

'Set the indicator if the result does not contain any of the words in result_list
Private Sub SetIndIfResultDoesNotContain(inum As Integer, cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = Searchpullrange)
    Dim found_what As String
    'avoid more queries if the indicator is already set
    If inds(inum).checked Then Exit Sub
    
    'Turn trace off for ResultDoesNotContain() and echo what was set below with SetInd
    If ResultDoesNotContain(cat, code, desc, field, result_list, found_what, search_mode, False) Then
        SetInd inum, found_what             'echo here
    Else
        dvprint found_what                  'and here
    End If
End Sub

'Clear the indicator if the result contains on of the words in the result_list
Private Sub ClrIndIfResultContains(inum As Integer, cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = Searchpullrange)
    Dim found_what As String
    'avoid more queries if the indicator is already clear
    If Not inds(inum).checked Then Exit Sub
    
    'Turn trace off for ResultContains() and echo what was set below with SetInd
    If ResultContains(cat, code, desc, field, result_list, search_mode, False, found_what) Then
        ClrInd inum, found_what             'echo here
    Else
        dvprint found_what                  'and here
    End If
End Sub


'These slightly smaller functions are meant for places where you aren't really looking for a chart result
Private Function Exists(cat As String, Optional code As String, Optional desc As String, Optional field As String, Optional result_list As String, Optional search_mode As SearchMode = Searchpullrange, Optional trace As Boolean = True, Optional found_what As String) As Boolean
    Exists = (CountResultContains(cat, code, desc, field, result_list, search_mode, trace, found_what) > 0)
End Function

Private Sub SetIndIfFound(inum As Integer, cat As String, Optional code As String, Optional desc As String, Optional field As String, Optional result_list As String, Optional search_mode As SearchMode = Searchpullrange)
    SetIndIfResultContains inum, cat, code, desc, field, result_list, search_mode
End Sub

Private Sub ClrIndIfFound(inum As Integer, cat As String, Optional code As String, Optional desc As String, Optional field As String, Optional result_list As String, Optional search_mode As SearchMode = Searchpullrange)
    ClrIndIfResultContains inum, cat, code, desc, field, result_list, search_mode
End Sub


'Get the max/total value from a result (usually in the middle of the text)
Private Function GetIntValue(get_mode As GetValueMode, cat As String, code As String, desc As String, field As String, result_like As String, Optional search_mode As SearchMode = Searchpullrange) As Integer
    Dim sql As String, and_filter As String, arr() As String, msg As String
    Dim rs As New Recordset
    Dim result As Integer, i As Integer, n As Integer, value As Integer
    Dim found_one As Boolean

    and_filter = AndSimpleItemFilter(cat, code, desc, field, result_like)
    sql = "select result from chart_item" & WhereBase(search_mode) & and_filter
    'Debug.Print sql
    rs.Open sql, g_cnADO
    
    'Look for a number in the result
    
    Do While Not rs.EOF
        n = g_util.SplitTextOnChar(rs(0), " ", arr(), 1, 0)
        For i = 1 To n
            'Look for 1st character numeric; IsNumeric("60min") will fail so look at 1st char only
            If IsNumeric(Left$(arr(i), 1)) Then
                value = val(arr(i))                         'Use Val; CInt will error on "60min"
                Select Case get_mode
                Case GetMax
                    result = g_util.Max(value, result)      'max
                Case GetTotal
                    result = result + value                 'total
                Case GetLast
                    result = value                          'last
                End Select
                
                'print what we are searching for (the first time)
                If Not found_one Then
                    dvprint Describe(cat, code, desc, field, result_like, search_mode)
                End If
                found_one = True
                'print each value found
                dvprint "  found numeric value " & result
                'Keep going in case there are more
            End If
        Next i
        rs.MoveNext
    Loop
    
    rs.Close
    
    If Not found_one Then
        'show what was not found
        If g_verbose Then dprint Describe(cat, code, desc, field, result_like, search_mode)
    End If
    
    GetIntValue = result
End Function

Private Function GetMaxValue(cat As String, code As String, desc As String, field As String, result_like As String, Optional search_mode As SearchMode = Searchpullrange) As Integer
    GetMaxValue = GetIntValue(GetMax, cat, code, desc, field, result_like, search_mode)
End Function

Private Function GetTotalValue(cat As String, code As String, desc As String, field As String, result_like As String, Optional search_mode As SearchMode = Searchpullrange) As Integer
    GetTotalValue = GetIntValue(GetTotal, cat, code, desc, field, result_like, search_mode)
End Function

'Get a result; returns True if found with return_result set
Private Function GetResult(cat As String, code As String, desc As String, field As String, return_result As String, Optional search_mode As SearchMode = Searchpullrange) As Boolean
    Dim rs As New Recordset
    Dim sql As String

    sql = "select result from chart_item" & WhereBase(search_mode) & AndSimpleItemFilter(cat, code, desc, field, "")
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        return_result = rs(0) & ""
    Else
        return_result = ""
    End If
    rs.Close

    dvprint Describe(cat, code, desc, field, "", search_mode)
    GetResult = (Len(return_result) > 0)
End Function
Private Function GetResultOfLatest(cat As String, code As String, desc As String, field As String, return_result As String, evdt As Date, Optional search_mode As SearchMode = Searchpullrange) As Boolean
    Dim rs As New Recordset
    Dim sql As String
    Dim done As Boolean

    sql = "select event_datetime,result from chart_item" & WhereBase(search_mode) & AndSimpleItemFilter(cat, code, desc, field, "")
    sql = sql & " order by event_datetime desc"
    rs.Open sql, g_cnADO
    
    return_result = ""
    done = False
    
    If Not rs.EOF Then evdt = rs(0)
    Do While Not rs.EOF And Not done
        If evdt = rs(0) Then
            return_result = return_result & rs(1) & ","
        Else
            done = True
        End If
        rs.MoveNext
    Loop
    rs.Close

    dvprint Describe(cat, code, desc, field, "", search_mode)
    GetResultOfLatest = (Len(return_result) > 0)

End Function

Private Sub Check_1_2_3()
    On Error GoTo errHandler
    Dim b36625459dep As Boolean
Dim b499870115pcc As Boolean
Dim b246642309non As Boolean
Dim b246642349ass As Boolean
Dim b246642352ass As Boolean
Dim reason As String

    dvprint "range=" & g_range
    dvprint "---------------"
    dvprint "1. ADL Self"
    dvprint "2. ADL Assist"
    dvprint "3. ADL Complete"
    dvprint "---------------"
'COMPLETE CARE
    SetADLCompleteWhenAge "<=2"
    If inds(3).checked Then Exit Sub

    If ResultContains("", "499870115", "", "", "Complete") Then SetInd 3, "Complete care"
    If ResultContains("", "499870115", "", "", "Partial") Then SetInd 2, "Partial care"
    If ResultContains("", "499870115", "", "", "Self") Then SetInd 1, "Self/minimal care"
    If Not inds(1).checked And Not inds(2).checked And Not inds(3).checked Then
        SetInd 2, "Partial Care defaulted due to lack of documentation."
    End If
    
    Exit Sub
errHandler:
    g_util.ThrowError "Check_1_2_3"
    Resume  'debug
End Sub
Private Sub SetADLCompleteWhenAge(agecond As String)  ' agecond example: "<=3"
    Dim sql As String
    Dim rs As New Recordset

'
'select case when round(age_at_admission,0,1) <=55 then 1 else 0 end from encounter where encounter_id=6990

    sql = "select case when round(age_at_admission,0,1) " & agecond & " then 1 else 0 end from encounter " & WHERE_ENCOUNTER
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        If rs(0) = 1 Then
            SetInd 3, "Age <=2 years"
        End If
    End If
    rs.Close

End Sub


Private Sub Check_4()
    On Error GoTo errHandler
    Dim rehab As Boolean
    
    dvprint "---------------"
    dvprint "4. ADL Rehab"
    dvprint "---------------"
    
    'SetIndIfFound 4, "", "246642349"
   ' SetIndIfResultContains 4, "", "708981371", "", "", "can function,needs review"
    'If ResultContains("", "246642309", "", "", "crutch") Then SetInd 4, "Crutches"
    'If ResultContains("", "246642309", "", "", "walker") Then SetInd 4, "Walker"
    rehab = Exists("", "708980610", "", "", "Bed to Chair Transfers,Bowel and Bladder Management,Energy Conservation,Incontinence Management,Mobility")
    If Not rehab Then rehab = Exists("", "649068015", "", "", "Tube feeding,Walker,Wheelchair,Shower Chair,Bedside Commode,Cane")
    If Not rehab Then rehab = Exists("", "708980745")
    If Not rehab Then rehab = Exists("", "649073875")
    
    If rehab Then
        dvprint "Found Rehab basis; now looking for Taught To."
        SetIndIfResultContains 4, "", "607399893", "", "", "patient"
        'SetIndIfResultContains 4, "", "607399893", "", "", EXACT_MATCH_PREFIX & "patient"
    End If

Exit Sub
    
errHandler:
    g_util.ThrowError "Check_4"
    Resume  'debug
End Sub


Private Sub Check_5_6()
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "5. ADL 2-3 Caregivers"
    dvprint "6. ADL 4 or more Caregivers"
    dvprint "---------------"
    
    If ResultContains("", "499870115", "", "", "2-3") Then SetInd 5, "2-3 caregivers"
    If ResultContains("", "499870115", "", "", "4") Then SetInd 6, "4+ caregivers"
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_5_6"
    Resume  'debug
End Sub

Private Sub Check_7()
    Dim supp As Boolean
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "7. Communication"
    dvprint "---------------"

    If m_pat.age <= 3 Then Exit Sub
'    If ResultContains("", "37704792", "", "", "Sedated") Then
'        supp = True
'        dvprint "Patient Sedated - Suppressing #7"
'    End If
    If ResultContains("", "37704792", "", "", "Unresponsive") Then
        supp = True
        dvprint "Patient Unresponsive - Suppressing #7"
    End If
    
    If supp Then Exit Sub
    
    SetIndIfResultContains 7, "", "499868939", "", "", "Deaf", SearchSinceAdmission
    SetIndIfResultContains 7, "", "499868935", "", "", "Deaf", SearchSinceAdmission
    
    If Not ResultContains("", "499869623", "", "", "") Then 'no hearing device
        SetIndIfResultContains 7, "", "499868939", "", "", "Hearing Loss", SearchSinceAdmission
        SetIndIfResultContains 7, "", "499868935", "", "", "Hearing Loss", SearchSinceAdmission
    End If
    
    SetIndIfResultContains 7, "", "499869207", "", "", "Blindness,Eyeball absent,Vision loss", SearchSinceAdmission
    SetIndIfResultContains 7, "", "499869203", "", "", "Blindness,Eyeball absent,Vision loss", SearchSinceAdmission
    
    SetIndIfResultContains 7, "", "37704861", "", "", "Aphasic,Artificial Airway,Deaf,Hard of Hearing,Hoarse", SearchSinceAdmission
    SetIndIfResultContains 7, "", "37704861", "", "", "Hyperverbal,Inappropriate Speech,Incoherent", SearchSinceAdmission
    SetIndIfResultContains 7, "", "37704861", "", "", "Incomprehensible Sounds,Mute,Nonverbal,Pressured", SearchSinceAdmission
    SetIndIfResultContains 7, "", "37704861", "", "", "Slurred Stutters,Word finding Difficulty,Language Barrier", SearchSinceAdmission

    SetIndIfResultContains 7, "", "36620530", "", "", "Cuffed,Uncuffed,Intermediate Hi/Lo", SearchSinceAdmission
    SetIndIfResultContains 7, "", "36620580", "", "", "Cuffed,Uncuffed,Fenestrated Cuffed", SearchSinceAdmission
    SetIndIfResultContains 7, "", "36620580", "", "", "Fenestrated Uncuffed,Plugged,Unplugged,Passy Muir,Changed Trach Tube", SearchSinceAdmission

    SetIndIfResultContains 7, "", "499868683", "", "", "Interpreter", SearchSinceAdmission
    SetIndIfResultContains 7, "", "607401801", "", "", "Reading ability", SearchSinceAdmission
    SetIndIfResultContains 7, "", "499870527", "", "", "NPPV,CPAP,EPAP,IPAP"
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_7"
    Resume  'debug
End Sub

Private Sub Check_8()
    On Error GoTo errHandler
    Dim tmp As String
    Dim supp As Boolean
    Dim found_what As String

    dvprint "---------------"
    dvprint "8. Cognitive Support"
    dvprint "---------------"

    If m_pat.age <= 3 Then Exit Sub
'    If ResultContains("", "37704792", "", "", "Sedated") Then
'        supp = True
'        dvprint "Patient Sedated - Suppressing #8"
'    End If
    If ResultContains("", "37704792", "", "", "Unresponsive") Then
        supp = True
        dvprint "Patient Unresponsive - Suppressing #8"
    End If
    
    If supp Then Exit Sub
    
    SetIndIfResultContains 8, "", "37704795", "", "", "Disoriented to Person"
    SetIndIfResultContains 8, "", "37704795", "", "", "Disoriented to Place"
    SetIndIfResultContains 8, "", "37704795", "", "", "Disoriented to Time"
    SetIndIfResultContains 8, "", "37704795", "", "", "Disoriented to Situation"
    SetIndIfResultContains 8, "", "37704795", "", "", "Confused"

    If Not inds(8).checked Then
        If ResultDoesNotContain("", "37704795", "", "", "oriented x4", found_what) Then
            If ResultDoesNotContain("", "37704795", "", "", "appropriate", found_what) Then
                SetInd 8, found_what
            End If
        End If
    End If

    If Not inds(8).checked Then
        If ResultDoesNotContain("", "37704800", "", "", "denies", found_what) Then
            If ResultDoesNotContain("", "37704800", "", "", "appropriate", found_what) Then
                SetInd 8, found_what
            End If
        End If
    End If
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_8"
    Resume  'debug
End Sub

Private Sub Check_9_10()
    On Error GoTo errHandler
    Dim tmp As String
    Dim ct As Integer
    Dim found_what As String

    dvprint "---------------"
    dvprint "9.  Behavior/Emotional Management"
    dvprint "10. Behavior/Emotional Mgmt - q 1 Hour"
    dvprint "---------------"

    If ResultContains("", "517154997", "", "", "comforted,limits set") Then
        If ResultContains("", "37705032", "", "", "Agitated,Combative,Compulsive,Impulsive,Intrusive,Isolative,Jittery,Pacing,Passive,Resistant,Restless,Attention Seeking,Dependent,Destructive,Disruptive,Explosive,Guarded,Hypervigilant,Limit Testing,Manipulative,Paranoid,Reckless,Self Abusive,Self Endangering,Superficial,Suspicious,Threatening,Withdrawn") Or _
           ResultContains("", "37705029", "", "", "Blocked,Concrete,Delayed Associations,Delusional,Dissociative,Flight of Ideas,Helpless,Hopeless,Incoherent,Loose Associations,Obsessive,Paranoid,Perseveration,Racing Thoughts,Tangential") Then
            SetInd 9, "Comforted or Limits Set with intervention not hourly."
            SetIndIfResultContains 10, "", "721073931", "", "", "yes"
        End If
    End If
    If inds(10).checked Then Exit Sub
'    If ResultContains("", "37705032", "", "", "Agitated,Combative,Impulsive,Self Abusive,Self Endangering,Threatening") Then
'        SetIndIfResultContains 10, "", "721073931", "", "", "yes"
'    End If
'    If inds(10).checked Then Exit Sub
'
'    'SetIndIfResultDoesNotContain 9, "", "517154997", "", "", "appropriate,passive"
'    If ResultContains("", "37705032", "", "", "Agitated,Combative,Impulsive,Self Abusive,Self Endangering,Threatening") Then
'        SetIndIfResultContains 9, "", "517154997", "", "", "comforted,limits set"
'    End If
    'SetIndIfResultDoesNotContain 9, "", "37705032", "", "", "Appropriate,passive"
    SetIndIfResultDoesNotContain 9, "", "111930698", "", "", "Cooperative"
    
'    ct = CountSimpleResult("", "517154997", "", "", "", , , found_what)
'    If ct >= 12 Then
'        SetInd 10, "Count=" & ct & " of Behav mgt 517154997"
'    End If
'    ct = CountSimpleResult("", "578156332", "", "", "", , , found_what)
'    If ct >= 12 Then
'        SetInd 10, "Count=" & ct & " of restraints 578156332"
'        SetInd 12, "Count=" & ct & " of restraints 578156332"
'    End If
'578156332  if hourly then 10

    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_9_10"
    Resume  'debug
End Sub


Private Sub Check_11_12()
    On Error GoTo errHandler
    Dim ct As Integer
    Dim found_what As String
    
    dvprint "---------------"
    dvprint "11. Safety Management - q 2 Hours"
    dvprint "12. Safety Management - q 30 Minutes"
    dvprint "---------------"
    
'Trigger q30 in line 94 ONLY IF Sitter (line 90) is NOT Yes
'Only trigger if 87/95 when appropriate values in 66/70, except q30 must also have NO SITTER.
    If ResultContains("", "721058227", "", "", "yes") Then  'safety needed
        If ResultContains("", "37704795", "", "", "Disoriented,Reorientation Provided") Or _
           ResultContains("", "37705029", "", "", "Confused,Homicidal,Impaired Focus/Concentration,Suicidal") Then
            If Exists("", "499871743", "", "", "yes") Then   'sitter
                SetIndIfResultContains 11, "", "721073657", "", "", "q30"  'make if q2 even if q30 was marked
            Else  'NO SITTER
                SetIndIfResultContains 12, "", "721073657", "", "", "q30"  'only trigger q30 when no sitter
            End If
            If inds(12).checked Then Exit Sub
            SetIndIfResultContains 11, "", "721073657", "", "", "q2"
        End If
    End If
'SAVE-7/8/13
'    If ResultContains("", "721058227", "", "", "yes") Then  'safety needed
'        If ResultContains("", "37705032", "", "", "Agitated,Combative,Impulsive,Self Abusive,Self Endangering,Threatening") Or _
'           ResultContains("", "37705029", "", "", "Confused,Delusional,Homicidal,Impaired Focus/Concentration,Incoherent,Paranoid,Suicidal") Then
'            If Exists("", "499871743", "", "", "yes") Then   'sitter
'                SetIndIfResultContains 11, "", "721073657", "", "", "q30"  'make if q2 even if q30 was marked
'            Else  'NO SITTER
'                SetIndIfResultContains 12, "", "721073657", "", "", "q30"  'only trigger q30 when no sitter
'            End If
'            If inds(12).checked Then Exit Sub
'            SetIndIfResultContains 11, "", "721073657", "", "", "q2"
'        End If
'    End If
    
    
'violent restraint reason 578162259
'nonviolent restrain reason 578162576
    SetIndIfResultContains 12, "", "578162259", "", "", ""
    SetIndIfResultContains 11, "", "578162576", "", "", ""
    
'    If inds(12).checked Then Exit Sub
'    SetIndIfResultContains 12, "", "517115730", "", "", "yes"
'    SetIndIfResultContains 12, "", "578156332", "", "", ""
'
'    If inds(12).checked Then Exit Sub
'    SetIndIfResultContains 11, "", "58196179", "", "", ""
'    SetIndIfResultContains 11, "", "499871743", "", "", "yes"
'    If inds(11).checked Then Exit Sub
'    SetIndIfResultContains 11, "", "499922160", "", "", "confusion,impulsivity,poor judg"
'    SetIndIfResultContains 11, "", "499869239", "", "", "Fall Precaution Sticker,Attendant at Bedside,Bedrails Up x2/,Chair Alarm in Place,4 Siderails Up,Orthostatic BP"
    
'Pull q2 hours if checked and in addition, one or more of the following is also checked:
'37705032
'Assessment Med Surg->Mental->Behavior:Agitated/Combative/Impulsive/Self Abusive/Self Endangering/Threatening
'OR
'37704800
'Thought Process-> Confused/Delusional/Homicidal/Impaired Concentraion/Incoherent/Paranoid/Suicidal
    
'    ct = CountSimpleResult("", "517115730", "", "", "yes", , , found_what)
'    If ct >= 12 Then
'        SetInd 12, found_what
'    ElseIf ct >= 6 Then
'        SetInd 11, found_what
'    End If
'
'    If inds(12).checked Then Exit Sub
'
'    '
'    ' 11.
'    '
'    SetIndIfResultContains 11, "", "499871743", "", "", "yes"
'    SetIndIfResultContains 11, "", "499869239", "", "", "Fall Precaution Sticker on Chart and Door"
'    SetIndIfResultContains 11, "", "499922160", "", "", "Confusion,Impulsivity,Poor Judgement"
'
'    ct = CountSimpleResult("", "58196179", "", "", "", , , found_what)
'    If ct >= 6 Then
'        SetInd 11, found_what
'    End If

    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_11_12"
    Resume  'debug
End Sub

Private Sub Check_13()
    On Error GoTo errHandler
    Dim found_what As String
    
    dvprint "---------------"
    dvprint "13. Isolation"
    dvprint "---------------"

    'SetIndIfResultContains 13, "", "204473679", "", "", "Isolation"
    If ResultDoesNotContain("", "204473679", "", "", "None", found_what) Then
        SetInd 13, found_what
    End If
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_13"
    Resume  'debug
End Sub

Private Sub CheckAssessment(count As Integer, desc As String)
    Dim los_hours As Single
    Dim proratect8 As Integer, proratect4 As Integer, proratect2 As Integer
    Dim range As Integer
    
    proratect2 = num_buckets \ 2
    If proratect2 < 4 Then proratect2 = 4
    proratect4 = num_buckets \ 4
    If proratect4 < 2 Then proratect4 = 2
    proratect8 = num_buckets \ 8
    If proratect8 < 1 Then proratect8 = 1

    If (inds(17).checked) Then Exit Sub             'skip if highest already checked
    If (count = 0) Then Exit Sub                    'skip if none
    
    If g_range = 1440 Then
        dvprint "24-hr min count=3"
        If count >= 22 And isq30 Then
            dvprint "Meets q30 criteria of any 10 buckets with counts of 2+ among 12 consecutive buckets."
            SetInd 17, desc
        ElseIf count >= 22 Then
            dvprint "Does not meet q30 criteria, but has high count."
            SetInd 16, desc
        ElseIf count >= 11 Then 'change 5/22
            SetInd 16, desc
        ElseIf count >= 6 Then
            SetInd 15, desc
        ElseIf count >= 3 Then
            SetInd 14, desc
        End If
    Else
        dvprint "Pro-rate min counts=" & proratect8 & "/" & proratect4 & "/" & proratect2 & "/" & (num_buckets \ 2) * 2
        If count >= (num_buckets \ 2) * 2 And isq30 Then
            dvprint "Meets q30 criteria of " & num_buckets \ 2 & " consecutive buckets with counts of 2+."
            SetInd 17, desc
        ElseIf count >= proratect2 Then
            SetInd 16, desc
        ElseIf count >= proratect4 Then
            SetInd 15, desc
        ElseIf count >= proratect8 Then
            SetInd 14, desc
        End If
    End If
    isq30 = False
'on top of density, change q30 to require 10 consecutive hours of at least 2 items.
'also, if los<=4 hours, then give pt default profile indicators for that unit

End Sub
Private Sub CheckSpinalAssessment(count As Integer, desc As String)
    Dim los_hours As Single
    Dim proratect As Integer
    Dim range As Integer
    
    proratect = num_buckets \ 4
    If proratect < 1 Then proratect = 1

    If (inds(17).checked) Then Exit Sub             'skip if highest already checked
    If (count = 0) Then Exit Sub                    'skip if none
    
    If g_range = 1440 Then
        dvprint "24-hr min count=3"
        If count >= 12 Then
            SetInd 17, desc
        ElseIf count >= 6 Then
            SetInd 15, desc
        ElseIf count >= 3 Then
            SetInd 14, desc
        End If
    Else
        dvprint "Pro-rate min count=" & proratect
        If count >= (num_buckets \ 2) * 2 Then
            SetInd 17, desc
        ElseIf count >= num_buckets \ 2 Then
            SetInd 15, desc
        ElseIf count >= proratect Then
            SetInd 14, desc
        End If
    End If
'on top of density, change q30 to require 10 consecutive hours of at least 2 items.
'also, if los<=4 hours, then give pt default profile indicators for that unit

End Sub
Private Sub Check_14_15_16_17()
    Dim found_what As String
    Dim qmins As Integer
    Dim ct As Integer
    Dim codelist As String
    Dim outcodelist As String
    Dim return_result As String
    Dim iv_list As String
    
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "14. Assessment q4h"
    dvprint "15. Assessment q2h"
    dvprint "16. Assessment q1h"
    dvprint "17. Assessment q30min"
    dvprint "---------------"
    
    If m_pat.age <= 2 Then
        'if any IV then automatic 16
        If ReturnIVs(iv_list) Then SetInd 16, "Found IVs: " & iv_list
    End If
' ct events q 12 hours - count distinct event times!
' select count(distinct(times)) where eventcode in ()
'select min(time) where code in () and time>=pull-24
'add 12 hours
'select count(distinct(times)) where code in () and time between min and min+12
'returnmaxcount(codeslist,n) in the past n hours
'3=14, 6=15, 12=16, 24=17

'    codelist = "'38872550','38872547','38872541','38872520','38872544','474747459','474747492','474747385','474748618',"
'    codelist = codelist & "'474589503','474748233','474585419','474585639','474588406','38872493','38873069',"
'    codelist = codelist & "'38873072','499871623','499870035','517115579','38872897','38872556','38872487',"
'    codelist = codelist & "'474587698','474586930','4999872635','499983487','38872906','38872903','38872788',"
'    codelist = codelist & "'38872513','91337879','474747477','474747510','474747403','474748559','38872779',"
'    codelist = codelist & "'38872507','38872504','474748196','474586552','474747562','374290803','133398194',"
'    codelist = codelist & "'499872487','499868447','51715619','133398194'"

' Do Spinal drain '474587698', separately  and if q1 then make it q30, so will only have q4,q2,q30 from this
    codelist = "'474587698'"
    dvprint "Spinal Output Category..."
    ct = ReturnSpinalCount(codelist, 12)
    dvprint "Spinal output number of hour buckets with >0 =" & ct
    CheckSpinalAssessment ct, "Spinal output number of hour buckets with >0 =" & ct
    If inds(17).checked Then Exit Sub

    codelist = "'38872541','38872520','38872544','474747459','474747492','474747385','38872493','517115579'"
    
    outcodelist = "'38872897','38872556','474586930','4999872635','499983487','38872906','38872903',"
    outcodelist = outcodelist & "'38872788','91337879','474747477','474747510','474748559',"
    outcodelist = outcodelist & "'474748196','474586552','474747562','51715619','133398194'"

    dvprint "Fluids Category..."
    ct = ReturnFluidsCount(codelist, outcodelist, 12)
    dvprint "Fluid Mgt: number of hours where IO both exist=" & ct
    CheckAssessment ct, "Fluid Mgt: number of hours where IO both exist=" & ct
    If inds(17).checked Then Exit Sub
    
    codelist = "'30758281','36620173','36620157','36620187','36620160','36620120','36620123','36620272','36620275',"
    codelist = codelist & "'36620193','36620166','36620190','36620163','36620126','36620129','36620798','36620812',"
    codelist = codelist & "'517115408','517115426','517115461','517115485','30758555','30758521'"

    dvprint "Cardio Category..."
    ct = ReturnMaxCount(codelist, 12)
    dvprint "Cardiovascular Mgt max count=" & ct
    CheckAssessment ct, "Cardiovascular Mgt max count=" & ct
    If inds(17).checked Then Exit Sub
    
    dvprint "Pulm Category..."
    codelist = "'499944745','35529651.','35529654','499870227','35529657','35529660','44863237','904360','499869667','514770778','30758295','30758239'"
    ct = ReturnMaxCount(codelist, 12)
    dvprint "Pulmonary Mgt max count=" & ct
    CheckAssessment ct, "Pulmonary Mgt max count=" & ct
    If inds(17).checked Then Exit Sub
    
    codelist = "'750454','499871715','499871719','499868991','499868987','499868983',"
    codelist = codelist & "'499868979','499869359','499869355','499869683','499869679','499869987',"
    codelist = codelist & "'499869983','499867591','499867587','499872379','499872375','499872063',"
    codelist = codelist & "'499872059','499870223','499870219','499870143','499870139','499870419',"
    codelist = codelist & "'499870415','499872343','499872339','499869547','499869543','499867599',"
    codelist = codelist & "'499867595','499871791','499871787','499871183','499871179','91337079',"
    codelist = codelist & "'499921028','37704795','499914995'"
    
    dvprint "Neuro Category..."
    ct = ReturnMaxCount(codelist, 12)
    dvprint "Neurological Mgt max count=" & ct
    CheckAssessment ct, "Neurological Mgt max count=" & ct
    If inds(17).checked Then Exit Sub
    
    codelist = "'47590272','474586515','47586655','474587492','474590344','474588923','91337012',"
    codelist = codelist & "'474589841','38872487','517115654'"
    
    dvprint "Wound Mgt Category..."
    ct = ReturnMaxCount(codelist, 12)
    dvprint "Wound Mgt max count=" & ct
    CheckAssessment ct, "Wound Mgt max count=" & ct
    If inds(17).checked Then Exit Sub
    
    codelist = "'499920706','72474656'"
    
    dvprint "Tech Mgt..."
    ct = ReturnMaxCount(codelist, 12)
    dvprint "Tech Mgt max count=" & ct
    CheckAssessment ct, "Tech Mgt max count=" & ct
    If inds(17).checked Then Exit Sub
'327716582   N-PASS Outcome
'271824288   NPASS Pain Score
'628104349   NPASS Pain Score, Circ
'271824306   NPASS Sedated Score
'327698062   NPASS Target Score
'271824272   Pain Behavior State
'271824268   Pain Crying / Irritable
'271824275   Pain Extremities Tone
'271824285   Pain Facial Expression
'517974499   Pain Interventions NPASS
'271824281   Pain Prematurity Baseline
'271824278   Pain Vital Signs
    
    codelist = "'173265631','499869055','499871815','499870359','664466219','665033680',"
    codelist = codelist & "'173265631','664467678','444971399','318158','318184','17213195',"
    codelist = codelist & "'318159','144175149','59488040','318161','907614','318162','846284',"
    codelist = codelist & "'318163','318164','318166','318167','1813135','318170','120286736',"
    codelist = codelist & "'318173','318172','318171','1046186','206918875','680589','840362',"
    codelist = codelist & "'839738','318168','108410690','318165','318169','318174','318157',"
    codelist = codelist & "'318182','17213136','318175','680608','318179','318177','318178',"
    codelist = codelist & "'318181','318183','318185','680610','1714085','1046187','318186',"
    codelist = codelist & "'78491968','318156','318180','17213168','318187','318189','120286735',"
    codelist = codelist & "'497474676','209239403','209239434','318190','78491875','680612','120286730',"
    codelist = codelist & "'680614','1046188','1004876','1004877','318191','30758545','38872636',"
    codelist = codelist & "'38872639','38872642','38872648','38873091','38873091','38873091',"
    codelist = codelist & "'38872651','38872651','38872651','38872651','38873115','38873115',"
    codelist = codelist & "'38873115','38873115','38872671','38872682','38872685','38872685',"
    codelist = codelist & "'38872685','38872695','91337805','91337807','91337812','91337818',"
    codelist = codelist & "'91337821','91337822','91337825','91337826','91337829','91337830',"
    codelist = codelist & "'91337831','91337832','91337833','327716582','271824288','628104349','271824306',"
    codelist = codelist & "'327698062','271824272','271824268','271824275','271824285','517974499','271824281','271824278'"
    
    dvprint "Meds Category..."
    ct = ReturnMaxCount(codelist, 12)
    dvprint "Med Mgt max count=" & ct
    CheckAssessment ct, "Med Mgt max count=" & ct
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_14_15_16_17"
    Resume  'debug
End Sub

Private Sub Check_18()
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "18. Medication Preparation >= 20 Minutes"
    dvprint "---------------"

    SetIndIfResultContains 18, "", "38927667", "", "", "expressed"    'expressed breast milk added 6/5
'=CONCATENATE("SetIndIfResultContains 18,",  """"",",  """",  J1, """,",  """"","""",""", K1,"""")
    SetIndIfResultContains 18, "", "318170", "Diltiazem", "", "IV"
    SetIndIfResultContains 18, "", "318170", "Dobutamine", "", "IV"
    SetIndIfResultContains 18, "", "318170", "Dopamine", "", "IV"
    SetIndIfResultContains 18, "", "318170", "Eptifibatide", "", "IV"
    SetIndIfResultContains 18, "", "318170", "Fentanyl", "", "IV"
    SetIndIfResultContains 18, "", "318170", "Heparin", "", "IV"
    SetIndIfResultContains 18, "", "318170", "Insulin", "", "IV"
    SetIndIfResultContains 18, "", "318170", "Lidocaine", "", "IV"
    SetIndIfResultContains 18, "", "318170", "Milrinone", "", "IV"
    SetIndIfResultContains 18, "", "318170", "Morphine", "", "IV"
    SetIndIfResultContains 18, "", "318170", "Nitroglycerin", "", "IV"
    SetIndIfResultContains 18, "", "318170", "Amiodarone", "", "IV"
    SetIndIfResultContains 18, "", "318170", "Nimbex", "", "IV"
    SetIndIfResultContains 18, "", "318170", "Epinephrine", "", "IV"
    SetIndIfResultContains 18, "", "318170", "Isuprel", "", "IV"
    SetIndIfResultContains 18, "", "318170", "Lorazepam", "", "IV"
    SetIndIfResultContains 18, "", "318170", "Mannitol", "", "IV"
    SetIndIfResultContains 18, "", "318170", "Nesiritide", "", "IV"
    SetIndIfResultContains 18, "", "318170", "Nipride", "", "IV"
    SetIndIfResultContains 18, "", "318170", "Levophed", "", "IV"
    SetIndIfResultContains 18, "", "318170", "Neosynephrine", "", "IV"
    SetIndIfResultContains 18, "", "318170", "propofol", "", "IV"
    SetIndIfResultContains 18, "", "318170", "Octreotide", "", "IV"
    SetIndIfResultContains 18, "", "318170", "Aggrastat", "", "IV"

    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_18"
    Resume  'debug
End Sub

Private Sub CheckWound(total As Integer)
    If (inds(20).checked) Then Exit Sub             'skip if highest already checked

    If (total = 0) Then
        'skip it
    ElseIf (total >= 30) Then
        SetInd 20, "wound >= 30 min"
    Else
        SetInd 19, "wound < 30 min"
    End If
End Sub

Private Sub Check_19_20()
    Dim return_result As String
    Dim ol15, ol16 As Boolean
    
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "19. Wound/Injury Mgmt"
    dvprint "20. Wound/Injury Mgmt >= 30 Minutes"
    dvprint "---------------"

    
    SetIndIfFound 20, "", "474590048"   '6/4 added
    SetIndIfFound 20, "", "474589996"
    SetIndIfFound 20, "", "474590175"
    
    If inds(20).checked Then Exit Sub
    
    If GetResult("", "541545515", "", "", return_result) Then
        If IsNumeric(return_result) Then
            If return_result >= 30 Then
                SetInd 20, "Wound Care Time:=" & return_result
            ElseIf return_result > 0 Then
                SetInd 19, "Wound Care Time:=" & return_result
            End If
        End If
    End If
    If GetResult("", "541545547", "", "", return_result) Then
        If IsNumeric(return_result) Then
            If return_result >= 30 Then
                SetInd 20, "Nephrostomy Care Time:=" & return_result
            ElseIf return_result > 0 Then
                SetInd 19, "Wound Care Time:=" & return_result
            End If
        End If
    End If
    If GetResult("", "541545577", "", "", return_result) Then
        If IsNumeric(return_result) Then
            If return_result >= 30 Then
                SetInd 20, "Drain Care Time:=" & return_result
            ElseIf return_result > 0 Then
                SetInd 19, "Wound Care Time:=" & return_result
            End If
        End If
    End If
    If GetResult("", "541545607", "", "", return_result) Then
        If IsNumeric(return_result) Then
            If return_result >= 30 Then
                SetInd 20, "Urostomy Care Time:=" & return_result
            ElseIf return_result > 0 Then
                SetInd 19, "Wound Care Time:=" & return_result
            End If
        End If
    End If
    If GetResult("", "541545637", "", "", return_result) Then
        If IsNumeric(return_result) Then
            If return_result >= 30 Then
                SetInd 20, "GI Ostomy Care Time:=" & return_result
            ElseIf return_result > 0 Then
                SetInd 19, "Wound Care Time:=" & return_result
            End If
        End If
    End If
    If GetResult("", "541545667", "", "", return_result) Then
        If IsNumeric(return_result) Then
            If return_result >= 30 Then
                SetInd 20, "Burn Care Time:=" & return_result
            ElseIf return_result > 0 Then
                SetInd 19, "Wound Care Time:=" & return_result
            End If
        End If
    End If
    
    If inds(20).checked Then Exit Sub
    
    SetIndIfFound 19, "", "474589898" '6/4 added
    SetIndIfFound 19, "", "474589917"
    
    SetIndIfFound 19, "", "474586655"
    SetIndIfFound 19, "", "474586729"
    SetIndIfFound 19, "", "474587578"
    SetIndIfFound 19, "", "36620558"
    SetIndIfFound 19, "", "37704777"
    SetIndIfResultContains 19, "", "36624795", "", "", "Positive"
    SetIndIfResultDoesNotContain 19, "", "517115504", "", "", "n/a"

    SetIndIfFound 19, "", "474586552"  'nephrostomy output
    
    SetIndIfFound 19, "", "474589268" 'added these 9/25/13 for Renal
    SetIndIfFound 19, "", "474589286"
    SetIndIfFound 19, "", "474589306"
    SetIndIfFound 19, "", "474589323"
    SetIndIfFound 19, "", "474589345"
    SetIndIfFound 19, "", "474589365"
    SetIndIfFound 19, "", "474589379"
    SetIndIfFound 19, "", "474589399"
    SetIndIfFound 19, "", "474589448"
    SetIndIfFound 19, "", "474589465"
    
'474589268   Access Site Condition:
'474589286   Access Drainage:
'474589306   Access Drainage Amount:
'474589323   Access Site Care:
'474589345   Access Care:
'474589365   How Access Secured:
'474589379   Access Dressing:
'474589399   Access Patency:
'474589448   Bruit/Thrill Present:
'474589465   AV/Shunt Description:

        
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_19_20"
    Resume  'debug
End Sub


Private Sub Check_21()
    On Error GoTo errHandler
    
    Dim mins As Integer
    Dim chart_result As String
    
    dvprint "---------------"
    dvprint "21. Healthcare Mgmt Education >= 1 Hour"
    dvprint "---------------"
    

    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_21"
    Resume  'debug
End Sub

Private Sub Check_22()
    Dim codelist As String
    
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "22. 1 to 1 Physiological Interv. >= 2 Hours"
    dvprint "---------------"

    SetIndIfFound 22, "", "499922206"
    SetIndIfFound 22, "", "499868447"
    
    codelist = "30758281,36620120,36620123,30758555,30758521,750454"
    CheckCodesForQ30gt2Hrs codelist
'30758281
'36620120
'36620123
'30758555
'30758521
'750454

    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_22"
    Resume  'debug
End Sub

Private Sub ProcessProc23()
    Dim rs As New Recordset
    Dim sql As String
    Dim phrs As Integer
    Dim and_filter As String
    Dim parr(20) As procdata
    Dim pindex As Integer
    Dim pskill As String
    Dim i As Integer
    Dim j As Integer

    pindex = 0
    and_filter = AndSimpleItemFilter("", "499872183", "", "", "")
    sql = "select result,event_datetime from chart_item" & WhereBase() & and_filter
    'Debug.Print sql
    rs.Open sql, g_cnADO
    Do While Not rs.EOF
        If IsNumeric(rs(0)) Then
            phrs = rs(0)
            If phrs >= 60 Then
                pindex = pindex + 1
                parr(pindex).start = rs(1)
                parr(pindex).finish = DateAdd("n", phrs, parr(pindex).start) 'chg to min 7/8/13
                parr(pindex).pnum = 0
            End If
        End If
        
        rs.MoveNext
    Loop
    rs.Close
    
    'Now the procedure results are filled with times.
    'Next, clean up the duplicates, if any, and keep the greatest hours for a particular start
    For i = 1 To pindex
        parr(i).isvalid = True
    Next i
    If pindex > 1 Then
    For i = 1 To pindex - 1
        For j = i + 1 To pindex
            If parr(i).isvalid And parr(j).isvalid Then
                If parr(i).start = parr(j).start Then
                    If parr(i).finish >= parr(j).finish Then
                        parr(j).isvalid = False
                    Else
                        parr(i).isvalid = False
                    End If
                End If
            End If
        Next j
    Next i
    End If
    
    and_filter = AndSimpleItemFilter("", "499867279", "", "", "")
    
    For i = 1 To pindex 'for each time, find its pair for the skill type: RN=proc2 Non RN=proc3
        If parr(i).isvalid Then
            sql = "select result from chart_item" & WhereBase() & and_filter
            sql = sql & " and event_datetime=" & g_dbutil.SQL_DateTime(parr(i).start)
            'Debug.Print sql
            rs.Open sql, g_cnADO
            Do While Not rs.EOF
                'Look for each search word in the result (case insensitive)
                If Not IsNull(rs(0)) Then
                    pskill = rs(0)
                    If pskill = "RN" Then 'RN will win over non-rn; cannot have two procedures triggered.
                        parr(i).pnum = 2
                    ElseIf pskill = "Non RN" And parr(i).pnum <> 2 Then
                        parr(i).pnum = 3
                    End If
                End If
                rs.MoveNext
            Loop
            rs.Close
        End If
    Next i
    
    For i = 1 To pindex
        If parr(i).isvalid And parr(i).pnum <> 0 Then
            numprocs = numprocs + 1
            procs(numprocs).isvalid = True
            procs(numprocs).start = parr(i).start
            procs(numprocs).finish = parr(i).finish
            procs(numprocs).pnum = parr(i).pnum
        End If
    Next i
    

End Sub
'Private Sub ProcessProc56(code1 As String, code2 As String, desc As String)
'    Dim pmin As Integer
'    Dim evdt As Date
'    Dim return_result As String
'
'    'only allows for either RN or non-RN -- not both
'    If GetResult("", code1, "", "", return_result) Then
'        If IsNumeric(return_result) Then
'            If return_result >= 60 Then
'                pmin = return_result
'                If GetResultOfLatest("", code2, "", "", return_result, evdt) Then
'                    If return_result = "Unit RN" Then ' 5
'                        numprocs = numprocs + 1
'                        procs(numprocs).isvalid = True
'                        procs(numprocs).start = evdt
'                        procs(numprocs).finish = DateAdd("n", pmin, procs(numprocs).start)
'                        procs(numprocs).pnum = 5
'                        dvprint "P5: " & desc & " Care by RN mins=" & pmin
'                    ElseIf return_result = "Unit Non-RN" Then '6
'                        numprocs = numprocs + 1
'                        procs(numprocs).isvalid = True
'                        procs(numprocs).start = evdt
'                        procs(numprocs).finish = DateAdd("n", pmin, procs(numprocs).start)
'                        procs(numprocs).pnum = 6
'                        dvprint "P6: " & desc & " Care by Non-RN mins=" & pmin
'                    End If
'                End If
'            End If
'        End If
'    End If
'
'End Sub
'Private Sub ProcessProc89(code1 As String, code2 As String)
'    Dim pmin As Integer
'    Dim evdt As Date
'    Dim return_result As String
'
'    'only allows for either RN or non-RN -- not both
'    If GetResult("", code1, "", "", return_result) Then
'        If IsNumeric(return_result) Then
'            If return_result >= 60 Then
'                pmin = return_result
'                If GetResultOfLatest("", code2, "", "", return_result, evdt) Then
'                    If return_result = "1" Then ' 8
'                        numprocs = numprocs + 1
'                        procs(numprocs).isvalid = True
'                        procs(numprocs).start = evdt
'                        procs(numprocs).finish = DateAdd("n", pmin, procs(numprocs).start)
'                        procs(numprocs).pnum = 8
'                        dvprint "P8: Bedside Care by 1 RN mins=" & pmin
'                    ElseIf return_result >= "2" Then '9
'                        numprocs = numprocs + 1
'                        procs(numprocs).isvalid = True
'                        procs(numprocs).start = evdt
'                        procs(numprocs).finish = DateAdd("n", pmin, procs(numprocs).start)
'                        procs(numprocs).pnum = 9
'                        dvprint "P9: Bedside Care by 2 RNs mins=" & pmin
'                    End If
'                End If
'            End If
'        End If
'    End If
'
'End Sub
Private Sub ProcessProcInit(code1 As String, code2 As String, pnum1 As Integer, pnum2 As Integer, pres1 As String, pres2 As String, pmsg1 As String, pmsg2 As String)
    Dim rs As New Recordset
    Dim sql As String
    Dim i As Integer

'    sql = "select result,event_datetime from chart_item" & WhereBase() & AndSimpleItemFilter("", code1, "", "", "")
'    rs.Open sql, g_cnADO
'    Do While Not rs.EOF
'        If IsNumeric(rs(0)) Then
'            If rs(0) >= 30 Then
'                pmin = rs(0)
'                evdt = rs(1)
'                ProcessProcEnd code2, evdt, pnum1, pnum2, pres1, pres2, pmsg1, pmsg2, pmin
'            End If
'        End If
'        rs.MoveNext
'    Loop
    
    sql = "select distinct(event_datetime) from chart_item" & WhereBase() & AndSimpleItemFilter("", code1, "", "", "")
    sql = sql & " order by event_datetime"
    rs.Open sql, g_cnADO
    Do While Not rs.EOF
        nump = nump + 1
        pary(nump).evdt = rs(0)
        pary(nump).mins = 0
        rs.MoveNext
    Loop
    rs.Close
    
    For i = 1 To nump
        sql = "select result from chart_item" & WhereBase() & AndSimpleItemFilter("", code1, "", "", "")
        sql = sql & " and event_datetime=" & g_dbutil.SQL_DateTime(pary(i).evdt)
        rs.Open sql, g_cnADO
        Do While Not rs.EOF
            If IsNumeric(rs(0)) Then
                pary(i).mins = pary(i).mins + rs(0)
            End If
            rs.MoveNext
        Loop
        rs.Close
    Next i
    
    For i = 1 To nump
        If pary(i).mins >= 60 Then
            ProcessProcEnd code2, pary(i).evdt, pnum1, pnum2, pres1, pres2, pmsg1, pmsg2, pary(i).mins
        End If
    Next i
    
'     "541545515"
'     "541545547"
'     "541545577"
'     "541545607"
'     "541545637"
'     "541545667"
'     "499872171"
'    sql = "delete chart_item " & WhereBase() & " and code like '541545515%' or code like '541545547%' or code like '541545577%'"
'    sql = sql & " or code like '541545607%' or code like '541545637%' or code like '541545667%' or code like '499872171%'"
'    g_cnADO.Execute sql
End Sub
Private Sub ProcessProcEnd(code2 As String, evdt As Date, pnum1 As Integer, pnum2 As Integer, pres1 As String, pres2 As String, pmsg1 As String, pmsg2 As String, pmin As Integer)
    Dim rs As New Recordset
    Dim sql As String
    Dim return_result As String

    sql = "select result from chart_item" & WhereBase() & AndSimpleItemFilter("", code2, "", "", "")
    sql = sql & " and event_datetime=" & g_dbutil.SQL_DateTime(evdt)
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        return_result = rs(0)
        If return_result = pres1 Then ' p1
            numprocs = numprocs + 1
            procs(numprocs).isvalid = True
            procs(numprocs).start = evdt
            procs(numprocs).finish = DateAdd("n", pmin, procs(numprocs).start)
            procs(numprocs).pnum = pnum1
            If pmin >= 120 Then SetInd 22, "120 mins or greater for procedure " & pnum1
            dvprint "P" & pnum1 & ": " & pmsg1 & pmin
        ElseIf return_result = pres2 Or (pnum1 = 8 And return_result >= pres2) Then 'p2
            numprocs = numprocs + 1
            procs(numprocs).isvalid = True
            procs(numprocs).start = evdt
            procs(numprocs).finish = DateAdd("n", pmin, procs(numprocs).start)
            procs(numprocs).pnum = pnum2
            If pmin >= 120 Then SetInd 22, "120 mins or greater for procedure " & pnum2
            dvprint "P" & pnum2 & ": " & pmsg2 & pmin
        End If
    End If

    rs.Close
    
End Sub
Private Sub CheckProcs()
    Dim return_result As String
    Dim proc8, proc9 As Boolean

    On Error GoTo errHandler
'    dvprint "---------------"
'    dvprint "P1. 1-1 safety observation by non-RN"
'    dvprint "---------------"
'
'    ProcessProc 1, "OLAMBPROG8", "OLAMBPRO02", "OLAMBPR03"

    dvprint "---------------"
    dvprint "P2. Off unit accompanied by RN"
    dvprint "P3. Off unit accompanied by non-RN"
    dvprint "---------------"

    ProcessProc23

'    dvprint "---------------"
'    dvprint "P4. Patient/family education by RN"
'    dvprint "---------------"
'
'    ProcessProc 4, "OLACUITY21", "OLACUITY45", "OLACUITY46"
'
    dvprint "---------------"
    dvprint "P5. Extensive wound management by RN"
    dvprint "P6. Extensive wound management by non-RN"
    dvprint "---------------"

    ProcessProcInit "541545515", "541545500", 5, 6, "Unit RN", "Unit Non-RN", "Wound Care by RN mins=", "Wound Care by Non-RN mins="
    ProcessProcInit "541545547", "541545530", 5, 6, "Unit RN", "Unit Non-RN", "Nephrostomy Care by RN mins=", "Nephrostomy Care by Non-RN mins="
    ProcessProcInit "541545577", "541545562", 5, 6, "Unit RN", "Unit Non-RN", "Drain Care by RN mins=", "Drain Care by Non-RN mins="
    ProcessProcInit "541545607", "541545592", 5, 6, "Unit RN", "Unit Non-RN", "Urostomy Care by RN mins=", "Urostomy Care by Non-RN mins="
    ProcessProcInit "541545637", "541545622", 5, 6, "Unit RN", "Unit Non-RN", "GI Ostomy Care by RN mins=", "GI Ostomy Care by Non-RN mins="
    ProcessProcInit "541545667", "541545652", 5, 6, "Unit RN", "Unit Non-RN", "Burn Care by RN mins=", "Burn Care by Non-RN mins="

'    dvprint "---------------"
'    dvprint "P6. Extensive wound management by non-RN"
'    dvprint "---------------"
'
'    ProcessProc 6, "OLACUITY23", "OLACUITY36", "OLACUITY37"
'
'    dvprint "---------------"
'    dvprint "P7. Coordination of care by RN"
'    dvprint "---------------"
'
'    ProcessProc 7, "OLACUITY24", "OLACUITY43", "OLACUITY44"
'
    dvprint "---------------"
    dvprint "P8. 1-1 by RN at bedside"
    dvprint "P9. 2-1 by RN at bedside"
    dvprint "---------------"
    
    ProcessProcInit "499872171", "517116268", 8, 9, "1", "2", "Bedside Care by 1xRN mins=", "Bedside Care by 2xRN mins="
'NEW 7/31'should also trigger ind 22 when mins >= 120mins.
    nump = 0

    Exit Sub

errHandler:
    g_util.ThrowError "CheckProcs"
    Resume  'debug
End Sub
'Private Sub CheckOutcomes()
'    Dim return_result As String
'
'    'How to handle multiple outcomes per pt, each with different times?
'    dvprint "---------------"
'    dvprint "Outcomes: FALL"
'    dvprint "---------------"
'    If GetResult("", "FALL", "", "", return_result, SearchOutcomesRange) Then
'        numoutcomes = numoutcomes + 1
'        With oc(numoutcomes)
'            .checked = True
'            .pnum = 1
'            .start = return_result
'        End With
'    End If
'
'    dvprint "---------------"
'    dvprint "Outcomes: MED"
'    dvprint "---------------"
'    If GetResult("", "MED", "", "", return_result, SearchOutcomesRange) Then
'        numoutcomes = numoutcomes + 1
'        With oc(numoutcomes)
'            .checked = True
'            .pnum = 2
'            .start = return_result
'        End With
'    End If
'
'    dvprint "---------------"
'    dvprint "Outcomes: PROC"
'    dvprint "---------------"
'    If GetResult("", "PROC", "", "", return_result, SearchOutcomesRange) Then
'        numoutcomes = numoutcomes + 1
'        With oc(numoutcomes)
'            .checked = True
'            .pnum = 21
'            .start = return_result
'        End With
'    End If
'
'    Exit Sub
'
'errHandler:
'    g_util.ThrowError "CheckOutcomes"
'    Resume  'debug
'End Sub


Private Sub AtLeastOneADL()
    If Not (inds(1).checked Or inds(2).checked Or inds(3).checked) Then
        'Note: Heather wants "#2 ADL - Assist" to be the default.  (90% of patients)
        SetInd 2, "at least one ADL"
    End If
End Sub

Private Sub HighestIndicatorInEachGroupWins()
    Dim i As Integer
    Dim g As Integer
    Dim highest_is_on As Boolean
    Dim ind_list As String

    dvprint "---------------"
    dprint "Select highest indicator in each group"
    
    g = 0
    highest_is_on = False
    For i = MAX_INDS To 1 Step -1
        If (inds(i).group > 0) Then
            If (inds(i).group <> g) Then
                'this is a new group
                g = inds(i).group
                highest_is_on = inds(i).checked
            Else
                'same group
                If highest_is_on Then
                    inds(i).checked = False             'uncheck a lower number
                Else
                    highest_is_on = inds(i).checked     'save this one
                End If
            End If
        End If
    Next i
    
    'Echo the indicators for an audit (no classification will be saved)
    If g_debug And g_no_output Then
        For i = 1 To MAX_INDS
            If inds(i).checked Then ind_list = ind_list & "," & i
        Next i
        dprint "Final list = " & Mid$(ind_list, 2)
    End If

End Sub

Private Sub OutputClass()
    Dim outstr As String, ind_list As String
    Dim i As Integer
    Dim txarea As String

    txarea = ""
    If m_pat.unit_id = 108 Then 'surg/ortho/peds
        If m_pat.age <= 16 Then txarea = "Pediatrics"
    End If
        
    outstr = g_util.FixedWidth("", 8)                                       '(facility code)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.unit_name, 16)
    outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(unit code)
    outstr = outstr & "|" & g_util.FixedWidth(txarea, 16)                       '(area code)
    outstr = outstr & "|" & g_util.FixedWidth("", 8)                        '(class date - give datetime instead)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.acct, 20)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.last_name, 32)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.first_name, 32)
    outstr = outstr & "|" & g_util.FixedWidth("", 32)                       '(middle)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.room, 8)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.bed, 4)
    'for the class time, cant use next day because it might exceed discharge time.
    outstr = outstr & "|" & Format$(m_pat.Effective, "yyyymmddhhnn")      'class datetime  PULLDT
    outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(login)
    outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(employee)
    outstr = outstr & "|" & g_util.FixedWidth(CStr(m_pat.meth_id), 4)
    outstr = outstr & "|" & g_util.FixedWidth("C", 1)                       'record type = class
    outstr = outstr & "|" & g_util.FixedWidth("", 4)                        '(stage)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.TC_source_id, 4)        'TC source ID
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.range, 4)               'TC pull range
    outstr = outstr & "|"
    outstr = g_util.FixedWidth(outstr, 294)
    If Format$(m_pat.Effective, "hhnn") <= "0659" Then 'need to use yesterdays date
        outstr = outstr & "|" & Format$(DateAdd("d", -1, m_pat.Effective), "yyyymmdd") & "0700"
    Else
        outstr = outstr & "|" & Format$(m_pat.Effective, "yyyymmdd0700")        'IN always 0700
    End If
    outstr = g_util.FixedWidth(outstr, 346)
'    If Format$(m_pat.Effective, "hhnn") = "0700" Then 'need classtime to be today at 7am
'        outstr = outstr & "|" & Format$(DateAdd("d", 1, m_pat.Effective), "yyyymmdd") & "0700"
'    End If
    If Format$(g_pull_finish, "hhnn") = "0700" Then 'need classtime to be today at 7am
        outstr = outstr & "|" & Format$(g_pull_finish, "yyyymmddhhnn")
    End If
    outstr = g_util.FixedWidth(outstr, 377)
    outstr = outstr & "|"
    
    For i = 1 To MAX_INDS
        If (inds(i).checked) Then
            outstr = outstr & "Y"
            ind_list = ind_list & "," & i
        Else
            outstr = outstr & "N"
        End If
    Next i
    ind_list = Mid$(ind_list, InStr(1, ind_list, ",") + 1)                'strip leading comma
    
    Print #outfile, outstr
    
    AddLogEntry EVENT_TYPE_INFO, "Classified: " & ind_list, EVENT_CATEGORY_PROCESSED
End Sub

Private Sub OutputProcs()
    Dim i, j As Integer
    Dim s, f As Date
    Dim outstr As String, proc_list As String

    For i = 1 To numprocs
        If procs(i).isvalid Then
        outstr = g_util.FixedWidth("", 8)                                       '(facility code)
        outstr = outstr & "|" & g_util.FixedWidth(m_pat.unit_name, 16)
        outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(unit code)
        outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(area code)
        outstr = outstr & "|" & g_util.FixedWidth("", 8)                        '(class date - give datetime instead)
        outstr = outstr & "|" & g_util.FixedWidth(m_pat.acct, 20)
        outstr = outstr & "|" & g_util.FixedWidth(m_pat.last_name, 32)
        outstr = outstr & "|" & g_util.FixedWidth(m_pat.first_name, 32)
        outstr = outstr & "|" & g_util.FixedWidth("", 32)                       '(middle)
        outstr = outstr & "|" & g_util.FixedWidth(m_pat.room, 8)
        outstr = outstr & "|" & g_util.FixedWidth(m_pat.bed, 4)
        outstr = outstr & "|" & Format$(procs(i).start, "yyyymmddhhnn")       '204 procdt
        outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(login)
        outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(employee)
        outstr = outstr & "|" & g_util.FixedWidth(CStr(m_pat.meth_id), 4)
        outstr = outstr & "|" & g_util.FixedWidth("P", 1)                       'record type = class
        outstr = outstr & "|"
        outstr = outstr & Space$(294 - Len(outstr))
        outstr = outstr & "|" & Format$(procs(i).start, "yyyymmddhhnn")       '296 procdt in
        outstr = outstr & Space$(346 - Len(outstr))
        If procs(i).finish = 0 Then
            outstr = outstr & "|" & Space$(12)
        Else
            outstr = outstr & "|" & Format$(procs(i).finish, "yyyymmddhhnn")      '348 procdt out
        End If
        outstr = g_util.FixedWidth(outstr, 377)
        outstr = outstr & "|NNNNNNNNN"
        Mid$(outstr, 378 + procs(i).pnum, 1) = "Y"
'        proc_list = proc_list & "," & procs(i).pnum
'        For j = i + 1 To numprocs
'            If Not procs(j).isvalid And procs(j).pindex = i Then
'                Mid$(outstr, 378 + procs(j).pnum, 1) = "Y"
'                proc_list = proc_list & "," & procs(j).pnum
'            End If
'        Next j
'        proc_list = Mid$(proc_list, InStr(1, proc_list, ",") + 1)          'strip leading comma
        If Not DuplicateProc(procs(i).pnum, procs(i).start, procs(i).finish) Then
            Print #outfile, outstr
            AddLogEntry EVENT_TYPE_INFO, "Procedure: " & procs(i).pnum, EVENT_CATEGORY_PROCESSED
        End If

        End If 'isvalid
    Next i

End Sub
Private Sub OutputOutcomes()
'    Dim i, j As Integer
'    Dim s, f As Date
'    Dim outstr As String
'    Dim octime As String
'
'    If numoutcomes = 0 Then Exit Sub
'
'    For i = 1 To numoutcomes
'        If (oc(i).checked) Then
'            outstr = g_util.FixedWidth("", 8)
'            outstr = outstr & "|" & g_util.FixedWidth(m_pat.unit_name, 16)
'            outstr = outstr & "|" & g_util.FixedWidth("", 16)
'            outstr = outstr & "|" & g_util.FixedWidth("", 16)
'            outstr = outstr & "|" & g_util.FixedWidth(oc(i).start, 12)
'            outstr = outstr & "|" & g_util.FixedWidth(m_pat.acct, 20)
'            outstr = outstr & "|" & g_util.FixedWidth(oc(i).pnum, 3)
'            Print #outfile2, outstr 'Print line to outcomesindicator.TXT
'        End If
'    Next i
End Sub


Private Function ReturnMaxCount(codelist As String, nmins As Integer) As Integer
    Dim sql As String
    Dim rs As New Recordset
    Dim ct As Integer
    Dim done_ct As Integer
    Dim mintime As Date
    Dim maxtime As Date
    Dim done As Boolean
    Dim ctbucket(24) As Integer
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim totals(12) As Boolean
    Dim s As String
    Dim q30ct As Integer
    Dim adj As Integer
    
    ReturnMaxCount = 0

    'do an initial count to see if warrants continuing; set the mintime of all codes
'    sql = "select count(distinct(event_datetime)),min(event_datetime) from chart_item " & WhereBase
'    sql = sql & " and code in (" & codelist & ") and event_datetime is not null"
'    rs.Open sql, g_cnADO
'    dvprint sql

'start at g_pull_finish and go back g_range minutes
'how many buckets are there?  g_range \ 60, if g_range mod 60 <> 0, then g_range+1
'
'1bucket start = g_pull_finish - 60   6am
'1bucket end = g_pull_finish - 1       6:59am
'2bucket start = g_pull_finish - 2 x 60   5am
'2bucket end = g_pull_finish - 60 - 1    5:59am
'lastbucket start = g_pull_start   g_pull_finish - g_range
'lastbucket end = (last+1)bucket start -1

'SetupBuckets:
    s = "Counts: "
    sql = "select " & AssessSQL
'    sql = sql & " from chart_item " & WhereBase
'    sql = sql & " and code in (" & codelist & ") and event_datetime is not null"
    sql = sql & " from (select distinct(event_datetime) from chart_item " & WhereBase
    sql = sql & " and code in (" & codelist & ") and event_datetime between " & bucket(num_buckets).startdt & " and " & bucket(1).enddt & ") as ci1"
    
    
    rs.Open sql, g_cnADO

    If Not rs.EOF Then
        For i = 0 To num_buckets - 1
            If IsNull(rs(i)) Then
                ctbucket(i) = 0
            Else
                ctbucket(i) = rs(i)
            End If
            s = s & ctbucket(i) & ","
        Next
        rs.Close
    Else
        dvprint "No documentation found for this category of assessments."
        rs.Close
        Exit Function
    End If
    dvprint s & " Scanning across adjacent hours..."
    
    done_ct = 0
    If g_range = 1440 And num_buckets >= 12 Then 'there will be at least 1 sum of 12 buckets
        For i = 0 To num_buckets - 12 '-- (for 24 hours 13 sums)
            ct = 0
            For j = 0 To 11  '...of 12 buckets each
                If ctbucket(i + j) > 0 Then ct = ct + 1
            Next j
            If ct >= 12 Then totals(i) = True 'total all in this sum
            If ct > done_ct Then done_ct = ct
        Next i
        If done_ct >= 12 Then  ' at least one of the sums has all buckets
            done_ct = 0
            For i = 0 To 12
                If totals(i) Then ' this sum has all buckets with something
                    ct = 0
                    q30ct = 0
                    For j = 0 To 11
                        ct = ct + ctbucket(i + j)  ' actual total count in this sum
                        If ctbucket(i + j) >= 2 Then q30ct = q30ct + 1
                    Next j
                    If q30ct >= 10 Then isq30 = True
                    If ct > done_ct Then done_ct = ct  'keep the max count
                End If
            Next i
        End If
    ElseIf g_range < 1440 Then      'there will be at least 1 sum of 6 buckets
        adj = 0
        If (num_buckets Mod 2) = 0 Then adj = 1
        dvprint "num_buckets=" & num_buckets & " adj=" & adj
        For i = 0 To num_buckets \ 2 ' (for 12 hours 7 sums)
            ct = 0
            For j = 0 To num_buckets \ 2 - adj '...of 6 buckets each
                If ctbucket(i + j) > 0 Then ct = ct + 1
            Next j
            If ct >= num_buckets \ 2 Then totals(i) = True                 'total all in this sum
            If ct > done_ct Then done_ct = ct
        Next i
        dvprint "Among " & num_buckets \ 2 - adj + 1 & " consecutive buckets " & done_ct & " have something."
        If done_ct >= num_buckets \ 2 Then ' at least one of the sums has all buckets
            done_ct = 0
            For i = 0 To num_buckets \ 2
                If totals(i) Then ' this sum has all buckets with something
                    ct = 0
                    q30ct = 0
                    For j = 0 To num_buckets \ 2 - adj
                        ct = ct + ctbucket(i + j)  ' actual total count in this sum
                        'dvprint "bucket" & i + j + 1 & "=" & ctbucket(i + j) & " ct=" & ct
                        If ctbucket(i + j) >= 2 Then q30ct = q30ct + 1
                        'dvprint "bucket count with 2 or more=" & q30ct
                    Next j
                    If q30ct >= num_buckets \ 2 Then
                        isq30 = True
                        dvprint "Eligible for q30 because " & q30ct & " consecutive buckets with two or more.  Need>=" & num_buckets \ 2
                    End If
                    If ct > done_ct Then done_ct = ct  'keep the max count
                End If
            Next i
        End If
'    Else
'            i = 0
'            ct = 0
'            For j = 0 To num_buckets - 1 ' use all buckets b/c there are LT 12
'                If ctbucket(i + j) > 0 Then ct = ct + 1
'            Next j
'            If ct > done_ct Then done_ct = ct
'
    End If

    ReturnMaxCount = done_ct
    
End Function
' ct events q 12 hours - count distinct event times!
' select count(distinct(times)) where eventcode in ()
'select min(time) where code in () and time>=pull-24
'add 12 hours
'select count(distinct(times)) where code in () and time between min and min+12
'returnmaxcount(codeslist,n) in the past n hours
'3=14, 6=15, 12=16, 24=17

'Private Function SumEventDateTime(code As String, res As String) As Date
'    Dim sql As String
'    Dim sql2 As String
'    Dim total As Integer
'    Dim rs As New Recordset
'    Dim rs2 As New Recordset
'
'    sql = "select event_datetime from chart_item " & WhereBase
'    sql = sql & " and code=" & g_dbutil.SQL_String(code) & " and result=" & g_dbutil.SQL_String(res) & " and event_datetime is not null"
'    rs.Open sql, g_cnADO
'
'    Do While Not rs.EOF
'        If Not IsNull(rs(0)) Then
'            sql2 = "select sum(result) from chart_item " & WhereBase
'            sql2 = sql & " and code=" & g_dbutil.SQL_String(code) & " and event_datetime=" & g_dbutil.SQL_Date(rs(0))
'            rs2.Open sql2, g_cnADO
'            If Not rs2.EOF Then
'                If Not IsNull(rs2(0)) Then
'                    If IsNumeric(rs2(0)) Then
'                        total = total + rs2(0)
'                    End If
'                End If
'            End If
'        End If
'        rs.MoveNext
'    Loop
'    rs2.Close
'    rs.Close
'    SumEventDateTime = total
'End Function
Private Function ReturnFluidsCount(inlist As String, outlist As String, nmins As Integer) As Integer
    Dim sql As String
    Dim sql2 As String
    Dim asql As String
    Dim rs As New Recordset
    Dim ct As Integer
    Dim done_ct As Integer
    Dim bucketin(24) As Integer
    Dim bucketout(24) As Integer
    Dim i As Integer
    Dim listin As String
    Dim listout As String
    Dim s As String
        
    ReturnFluidsCount = 0
    
    sql2 = "select count(*) from chart_item " & WhereBase
    asql = sql2 & " and code in (" & inlist & ") and event_datetime is not null"
    rs.Open asql, g_cnADO
    If Not rs.EOF Then
        If rs(0) = 0 Then
            rs.Close
            dvprint "No documentation found for Intake."
            Exit Function
        End If
    End If
    rs.Close

    s = "FluidInCounts: "
    sql = "select " & AssessSQL
    sql = sql & " from chart_item " & WhereBase
    asql = sql & " and code in (" & inlist & ") and event_datetime is not null"
    rs.Open asql, g_cnADO

    If Not rs.EOF Then
        For i = 0 To num_buckets - 1
            If IsNull(rs(i)) Then
                bucketin(i) = 0
            Else
                bucketin(i) = rs(i)
            End If
            s = s & bucketin(i) & ","
        Next
    Else
        dvprint "No documentation found for Intake."
        rs.Close
        Exit Function
    End If
    dvprint s
    
    rs.Close
    
    sql2 = "select count(*) from chart_item " & WhereBase
    asql = sql2 & " and code in (" & outlist & ") and event_datetime is not null"
    rs.Open asql, g_cnADO
    If Not rs.EOF Then
        If rs(0) = 0 Then
            rs.Close
            dvprint "No documentation found for Output."
            Exit Function
        End If
    End If
    rs.Close
    
    s = "FluidOutCounts: "
    asql = sql & " and code in (" & outlist & ") and event_datetime is not null"
    rs.Open asql, g_cnADO

    If Not rs.EOF Then
        For i = 0 To num_buckets - 1
            If IsNull(rs(i)) Then
                bucketout(i) = 0
            Else
                bucketout(i) = rs(i)
            End If
            s = s & bucketout(i) & ","
        Next
    Else
        dvprint "No documentation found for Output."
        rs.Close
        Exit Function
    End If
    dvprint s
    
    rs.Close
    
    For i = 0 To num_buckets - 1
        listin = listin & bucketin(i)
        listout = listout & bucketout(i)
    Next i
    dvprint "In =" & listin
    dvprint "Out=" & listout
    
    done_ct = 0
    For i = 0 To num_buckets - 1
        If bucketin(i) > 0 And bucketout(i) > 0 Then done_ct = done_ct + 1
    Next i

    ReturnFluidsCount = done_ct
    
End Function
Private Function ReturnSpinalCount(outlist As String, nmins As Integer) As Integer
    Dim sql As String
    Dim sql2 As String
    Dim asql As String
    Dim rs As New Recordset
    Dim ct As Integer
    Dim done_ct As Integer
    Dim bucketout(24) As Integer
    Dim i As Integer
    Dim listin As String
    Dim listout As String
    Dim s As String
        
    ReturnSpinalCount = 0
    
    sql2 = "select count(*) from chart_item " & WhereBase
    asql = sql2 & " and code in (" & outlist & ") and event_datetime is not null"
    rs.Open asql, g_cnADO
    If Not rs.EOF Then
        If rs(0) = 0 Then
            rs.Close
            dvprint "No documentation found for SpinalOut."
            Exit Function
        End If
    End If
    rs.Close

    s = "SpinalOutCounts: "
    sql = "select " & AssessSQL
    sql = sql & " from chart_item " & WhereBase
    asql = sql & " and code in (" & outlist & ") and event_datetime is not null"
    rs.Open asql, g_cnADO

    If Not rs.EOF Then
        For i = 0 To num_buckets - 1
            If IsNull(rs(i)) Then
                bucketout(i) = 0
            Else
                bucketout(i) = rs(i)
            End If
            s = s & bucketout(i) & ","
        Next
    Else
        dvprint "No documentation found for SpinalOut."
        rs.Close
        Exit Function
    End If
    dvprint s
    
    rs.Close
    
    For i = 0 To num_buckets - 1
        listout = listout & bucketout(i)
    Next i
    dvprint "SpinalOut=" & listout
    
    done_ct = 0
    For i = 0 To num_buckets - 1
        If bucketout(i) > 0 Then done_ct = done_ct + 1
    Next i

    ReturnSpinalCount = done_ct
    
End Function

Private Sub SetupBuckets()
    Dim i As Integer
'start at g_pull_finish and go back g_range minutes
'how many buckets are there?  g_range \ 60, if g_range mod 60 <> 0, then g_range+1
'
'1bucket start = g_pull_finish - 60   6am
'1bucket end = g_pull_finish - 1       6:59am
'2bucket start = g_pull_finish - 2 x 60   5am
'2bucket end = g_pull_finish - 60 - 1    5:59am
'lastbucket start = (last+1)bucket start -1
'lastbucket end = g_pull_start   g_pull_finish - g_range


'    num_buckets = g_range \ 60
'    If (g_range Mod 60) <> 0 Then num_buckets = num_buckets + 1
'    For i = 1 To num_buckets - 1
'        bucket(i).startdt = g_dbutil.SQL_DateTime(DateAdd("n", -i * 60, g_pull_finish))
'        bucket(i).enddt = g_dbutil.SQL_DateTime(DateAdd("n", -((i - 1) * 60 + 1), g_pull_finish))
'    Next i
'    bucket(num_buckets).startdt = g_dbutil.SQL_DateTime(g_pull_start)
'    If num_buckets = 1 Then
'        bucket(num_buckets).enddt = g_dbutil.SQL_DateTime(DateAdd("n", -g_range, g_pull_start))
'    Else
'        bucket(num_buckets).enddt = g_dbutil.SQL_DateTime(DateAdd("n", -(num_buckets - 1) * 60 - 1, g_pull_finish))
'    End If
'
'    For i = 1 To num_buckets
'        dvprint "bucket" & i & "=" & bucket(i).startdt & " - " & bucket(i).enddt
'    Next i

    num_buckets = g_range \ 60
    If (g_range Mod 60) <> 0 Then num_buckets = num_buckets + 1
    For i = 1 To num_buckets - 1
        bucket(i).startdt = g_dbutil.SQL_DateTime(DateAdd("n", -i * 60, m_pat.pull_finish))
        bucket(i).enddt = g_dbutil.SQL_DateTime(DateAdd("n", -((i - 1) * 60 + 1), m_pat.pull_finish))
    Next i
    bucket(num_buckets).startdt = g_dbutil.SQL_DateTime(m_pat.pull_start)
    If num_buckets = 1 Then
        bucket(num_buckets).enddt = g_dbutil.SQL_DateTime(DateAdd("n", -g_range, m_pat.pull_start))
    Else
        bucket(num_buckets).enddt = g_dbutil.SQL_DateTime(DateAdd("n", -(num_buckets - 1) * 60 - 1, m_pat.pull_finish))
    End If

    For i = 1 To num_buckets
        dvprint "bucket" & i & "=" & bucket(i).startdt & " - " & bucket(i).enddt
    Next i

'================================================================================
'Patient 2 of 712: BEDSOLE, ERIC in 6NTH   acct=252080343084 age=49.7
'Here from 4/2/2013 7:00:00 AM to 4/2/2013 2:45:00 PM  (LOS=7.75)
'bucket1='20130403 06:00:00' - '20130403 06:59:00'
'bucket2='20130403 05:00:00' - '20130403 05:59:00'
'bucket3='20130403 04:00:00' - '20130403 04:59:00'
'bucket4='20130403 03:00:00' - '20130403 03:59:00'
'bucket5='20130403 02:00:00' - '20130403 02:59:00'
'bucket6='20130403 01:00:00' - '20130403 01:59:00'
'bucket7='20130403 00:00:00' - '20130403 00:59:00'
'bucket8='20130402 07:00:00' - '20130402 23:59:00'
'range = 465
End Sub

Private Function AssessSQL() As String
    Dim i As Integer
    Dim s As String
    
    s = ""
    For i = 1 To num_buckets
        s = s & "sum(case when event_datetime between " & bucket(i).startdt & " and " & bucket(i).enddt & " then 1 else 0 end) as range" & num_buckets - i + 1 & ","
    Next i
    
    s = Mid$(s, 1, Len(s) - 1)  'remove last comma
    AssessSQL = s

End Function


Private Sub CheckDefaultIndicators(uid As Long)
    Dim i As Integer
    Dim j As Integer

    For i = 1 To numunit
        If definds(i).uid = uid Then
            For j = 1 To MAX_INDS
                inds(j).checked = (Mid$(definds(i).indstr, j, 1) = "Y")
            Next j
        End If
    Next i

End Sub

Private Function DuplicateProc(procnum As Integer, stime As Date, etime As Date) As Boolean
    Dim rs As New Recordset
    Dim sql As String
    
    dvprint "Check for Duplicate Proc"
    
    sql = "select count(*) from"
    sql = sql & " procedure_event pe"
    sql = sql & " inner join procedure_answer pa on (pa.procedure_event_id=pe.procedure_event_id)"
    sql = sql & " where pe.encounter_id=" & m_pat.encounter_id & " And "
    sql = sql & " pe.procedure_datetime=" & g_dbutil.SQL_DateTime(stime) & " and"
    sql = sql & " pe.departure_datetime=" & g_dbutil.SQL_DateTime(etime) & " and"
    sql = sql & " pa.procedure_number=" & procnum
    
    rs.Open sql, g_cnADO
    If rs.RecordCount > 0 Then
        If rs(0) > 0 Then DuplicateProc = True
    End If
    rs.Close

End Function

Private Function ReturnIVs(iv_list As String) As Boolean
    Dim rs As New Recordset
    Dim sql As String

    iv_list = ""
    sql = "select distinct(description) from chart_item" & WhereBase & " and result='IV'"
    'Debug.Print sql
    rs.Open sql, g_cnADO
    Do While Not rs.EOF
        iv_list = iv_list & rs(0) & "; "
        rs.MoveNext
    Loop
    
    rs.Close
    ReturnIVs = (iv_list <> "")

End Function
Private Sub CheckCodesForQ30gt2Hrs(codelist As String)
    Dim sql As String
    Dim rs As New Recordset
    Dim ct As Integer
    Dim good As Boolean
    Dim startdt As Date, targetenddt As Date
    Dim targetidx As Integer
    Dim codeevent() As Date
    Dim i As Integer, j As Integer
        
    sql = "select distinct(event_datetime) from chart_item " & WhereBase
    sql = sql & " and code in (" & codelist & ") and event_datetime is not null order by event_datetime"
    rs.Open sql, g_cnADO
    ct = 0
    Do While Not rs.EOF
        ct = ct + 1
        ReDim Preserve codeevent(ct)
        codeevent(ct) = rs(0)
        rs.MoveNext
    Loop
    rs.Close
    good = False
    
    For i = 1 To ct - 1
        startdt = codeevent(i)
        targetenddt = DateAdd("n", 120, startdt)
        targetidx = 0
        For j = i + 1 To ct
            If codeevent(j) >= targetenddt Then
                targetidx = j
                j = ct + 1
            End If
        Next j
        If targetidx = 0 Then 'no such 2-hr range exists; end
            i = ct
        Else
            good = True
            For j = i + 1 To targetidx
                good = good And (DateDiff("n", codeevent(j - 1), codeevent(j)) <= 30)
            Next j
            If good Then
                i = ct
            End If
        End If
    Next i
    
    If good Then SetInd 22, "Frequency of q30min for at least 2 hours exists for codes=" & codelist
    

End Sub
