Attribute VB_Name = "MayoMain"
Option Explicit
'
' Shands (Epic data) transparent classification
'-zserver -zn -zbrief -verbose -ofsmins=600 -zefftime=0700 -zpulltime=0700 -zeffdate=20120608 -zunit=f6/6 -acct=0033039271111
' Each unit must set the "use transparent" flag.
'
' This is designed to be run as a background job, so never stop for user input -- that means no error MsgBox.
' All communication is done through log files and the event log; it is OK to print to the console.
'
' In order to be version independent, this program does not use any AcuityPlus dlls.
' Some AcuityPlus utility classes have been copied here.
'
' In order to work with the AcuityPlus Patient Selection and Transparent import:
'
'   * The program name must be TransparentMapping.exe, placed in AcuityPlus\bin
'   * The audit file is called TransparentAudit.log, placed in AcuityPlus\log
'   * The output file is Transparent.txt, placed in AcuityPlus\load_me
'   * Events are saved in the database event log
'
Public g_cnADO          As ADODB.Connection             'main DB connection
Public g_util           As New PFSUtility               'utility functions
Public g_dbutil         As New PFSDBUtility
Public g_event          As New PFSEventLog
Public g_display        As New PFSDisplayProperties
Public g_command        As String                       'command line (for db utility)
Public g_abort          As Boolean

Private Const TRANSP_FILENAME = "Transparent.txt"       'THE output file
Private Const TRANSP_AUDIT_LOG = "TransparentAudit.log" 'The debug/audit log
Private Const CHART_ITEM_LIFE = 30                      'days in the chart_item table
Private Const OUTCOMES_FILENAME = "OutcomesIndicator.txt"   'outcomes output file
Private Const MAXLOCS = 8

'C:\Documents and Settings\kmasumoto\Desktop\CLIENT FOLDERS\Mission
Private Const DEFAULT_RANGE = 1440                      '24 hrs in minutes; overridden by -range

Public Type LocationData
    time_in As Date
    time_out As Date
    unit_id As Long
    meth_id As Long
    range As Long
    toremove As Boolean
    txarea As String
End Type

Public Type PatientInfo
    unit_id             As Long
    encounter_id        As Long
    meth_id             As Long
    last_name           As String
    first_name          As String
    acct                As String
    age                 As Single                   'age (in years) at admission
    unit_name           As String
    room                As String
    bed                 As String
    unit_arrival        As Date
    effective           As Date                     'patient specific (may be > g_effdt)
    pull_start          As Date                     'patient specific (may be > g_pull_start)
    pull_finish         As Date                     'patient specific (may be < g_pull_finish)
    range               As Long                  'patient specific (may be < g_range)
    TC_source_id        As Integer
    fac_code            As Integer
    num_locs            As Integer
    locary(MAXLOCS)     As LocationData
End Type
Public Type UnitData
    unit_id As Long
    name As String
End Type

Public g_debug          As Boolean
Public g_verbose        As Boolean                  'verbose debug output?
Public g_no_output      As Boolean      'if no ouput, then no input files either

Public g_effdt          As Date                     'effective datetime

Public outfile          As Integer
Public outfile2          As Integer
Public dbugfile         As Integer

Public gLogUnitID       As Long
Public gLogEncounterID  As Long
Public gLogSourceText   As String

Public g_pull_start     As Date
Public g_pull_finish    As Date                     'pull datetime
Public g_range          As Long
Public g_datelog        As Boolean
Public g_units(MAXLOCS) As UnitData

Private winpfspath      As String                   'path of winpfs
Private winlogpath      As String                   'path of winpfs\log
Private winloadpath     As String                   'path of winpfs\load_me
Private outfilename     As String
Private outfile2name    As String
Private dbugname        As String

Private nowdt           As Date
Private debug_acct      As String                   'acct filter
Private debug_unit      As String                   'unit filter
Private debug_unit_id   As Long
Private g_chart_start   As Date
Private called_from_server As Boolean
Private parent_id       As Long
Private vunit_id       As Long
Private iunit_id       As Long

Sub Main()
    Dim s As String
    On Error GoTo errHandler
    
    parent_id = 2817
    'Prod env:
    vunit_id = 43729784
    iunit_id = 43730689
'    'Test env:
'    vunit_id = 42946928
'    iunit_id = 42947838
    'qmvm-qa-sqlrs
'    parent_id = 7120140
'    vunit_id = 7120379
'    iunit_id = 7120454
    
    frmMain.Show vbModeless                         'progress window
    DoEvents
    g_command = "-oledb"
    Set g_cnADO = g_dbutil.NewRemoteConnection
    
    gLogSourceText = Command$
    ParseCommandLine
    GetUnitNames
    
    If called_from_server Then
        LogInfo "Begin mapping and translation", EVENT_CATEGORY_STARTUP_SHUTDOWN 'deletes old event_log records
    End If
    gLogSourceText = ""
    
    OpenOutputFiles
    
    If called_from_server Then DeleteIbexFiles
    Process
    CloseOutputFiles
    DeleteOldLogs
    DeleteOldChartItems

    If called_from_server Then
        LogInfo "Mapping complete", EVENT_CATEGORY_STARTUP_SHUTDOWN
    End If
    g_cnADO.Close
normalExit:
    Unload frmMain
    Exit Sub

errHandler:
    LogError Err.Description & " in " & Err.source
    LogInfo "Unexpected error - shut down", EVENT_CATEGORY_STARTUP_SHUTDOWN
    GoTo normalExit
    Resume  'debug
End Sub

Private Sub ParseCommandLine()
    On Error GoTo errHandler

    Dim argc As Integer, argv() As String, subarg() As String, tag As String, value As String
    Dim i As Integer, n As Integer
    Dim effdate As String, efftime As String
    Dim pulldate As String, pulltime As String
    Dim nowdate As String, nowtime As String
    
    '-effdate=yyyymmdd   This is the date which, in combination with the
    '                   -efftime time is used to specify the classification In time.
    '                   If not specified, -effdate is assumed to be the current date.
    'Special value:  -effdate=yesterday
    
    '-efftime=hhmm      Used to set the classification In time.
    '                   If not specified, then -efftime is assumed to be the current time.
        
    '-pulldate=yyyymmdd This is the date/time of the data pull time.
    '                   If not specified, then this is assumed to be the -effdate.
    '-pulltime=hhmm     This is the time starting from which the pull is to
    '                   look backwards from.
    '                   If not specified, then this time is assumed to be the -efftime.
    
    '-range=nnnn        This is the number of minutes backwards from the pull time
    '                   that defines valid g_range of charting events.
    '                   If not specified, this will default to 1440 (24 hours)

    '-debug             echo debug/audit info; save in log file
    '-brief             Minimal debug info -- sets -debug
    '-verbose           Lots of debugging info -- sets -debug
    '-n                 No output to transparent.txt - debug only
    '-audit             sets -debug and -n
    '
    '-acct=12345        Process this patient only
    '-unit=5N           Process this unit only
    '-unit_id=1234      process this unit only
    '
    '-datelog           Puts date in log file name for file retention
    
'==================
'        pull_dt = rs("classification_datetime")
'        eff_dt = rs("effective_datetime_in")
'        range = g_dbutil.DBToInteger(rs("TC_pull_range"))
'        If (range = 0) Then range = 1440
'    cmd = "..\bin\" & TRANSPARENT_MAPPING_EXE_NAME
'    If verbose Then
'        cmd = cmd & " -verbose"
'    Else
'        cmd = cmd & " -brief"
'    End If
'    cmd = cmd & " -n"                               'no output to transparent.txt
'    cmd = cmd & " -unit_id=" & cdlUnit.code(cboUnit.Text)
'    cmd = cmd & " -acct=" & cboAcct.Text
'    cmd = cmd & " -pulldate=" & Format$(pull_dt, "yyyymmdd")
'    cmd = cmd & " -pulltime=" & Format$(pull_dt, "hhnn")
'    cmd = cmd & " -effdate=" & Format$(eff_dt, "yyyymmdd")
'    cmd = cmd & " -efftime=" & Format$(eff_dt, "hhnn")
'    cmd = cmd & " -range=" & range
'
'==================
    
    nowdt = Now
    nowdate = Format$(nowdt, "yyyymmdd")
    nowtime = Format$(nowdt, "hhnn")
    g_chart_start = DateAdd("n", -1, nowdt)
    g_verbose = True
    called_from_server = False

    argc = g_util.SplitTextOnChar(LCase$(Command$), " ", argv(), 1, 1)

    For i = 1 To argc
        n = g_util.SplitTextOnChar(argv(i), "=", subarg(), 1, 2)        '<tag>=<value>
        tag = subarg(1)
        value = subarg(2)
        
        Select Case tag
        Case "-server"    'must be first arg
            called_from_server = True
        
        Case "-acct"
            debug_acct = value
            
        Case "-audit"
            g_debug = True
            g_no_output = True
        
        Case "-brief"
            g_verbose = False
            g_debug = True
        
        Case "-debug"
            g_debug = True
        
        Case "-effdate"
            If (value = "yesterday") Then
                effdate = Format(DateAdd("d", -1, Date), "yyyymmdd")
            Else
                effdate = value
            End If
        
        Case "-efftime"
            efftime = Left$(value, 4)
            efftime = String$(4 - Len(efftime), "0") & efftime
        
        Case "-n"
            g_no_output = True

        Case "-pulldate"
            If (value = "today") Then
                pulldate = Format(Date, "yyyymmdd")
            Else
                pulldate = value
            End If
        
        Case "-pulltime"
            pulltime = Left$(value, 4)
            pulltime = String$(4 - Len(pulltime), "0") & pulltime

        Case "-range"
            g_range = CInt(value)
            
        Case "-unit"
            debug_unit = value
        Case "-unit_id"
            debug_unit_id = parent_id 'CLng(value)
            
        Case "-verbose"
            g_verbose = True
            g_debug = True

        Case "-ofsmins"  'number of minutes before Nowdt to look for chart_item timestamp; default to 1 min.
            If IsNumeric(value) Then
                g_chart_start = DateAdd("n", -value, nowdt)
            End If
            
        Case "-datelog"
            g_datelog = True
            
        Case "-parent"
            parent_id = CLng(value)  'ids because there could be spaces in the name
        Case "-vchild"
            vunit_id = CLng(value)
        Case "-ichild"
            iunit_id = CLng(value)

        Case Else
            LogWarning "Unexpected argument: " & tag
        End Select
    Next i
    
'    If Not called_from_server Then
'        'adjust times
'        effdate = Format(DateAdd("d", 1, g_util.CDateEx(effdate)), "yyyymmdd")
'        efftime = "0700"
'        pulldate = effdate
'        pulltime = efftime
'        g_range = 1440
'    End If

    If Len(effdate) = 0 Then effdate = nowdate
    If Len(efftime) = 0 Then efftime = nowtime
    
    'Note: pulldate defaults to effdate, not nowdate
    If Len(pulldate) = 0 Then pulldate = effdate
    If Len(pulltime) = 0 Then pulltime = efftime

    If g_range = 0 Then g_range = DEFAULT_RANGE
    
    'Note: the log file isn't open yet so these just go to the screen
    Debug.Print "Command line: " & Command$
    'Debug.Print "chart start=" & g_dbutil.SQL_DateTime(g_chart_start)
'    Debug.Print "effdate=" & effdate
'    Debug.Print "efftime=" & efftime
'    Debug.Print "pulldate=" & pulldate
'    Debug.Print "pulltime=" & pulltime
'    Debug.Print "range=" & g_range
    
    g_effdt = g_util.CDateEx(effdate & efftime)
    
    g_pull_start = g_util.CDateEx(pulldate & pulltime)     'for chart item queries
    g_pull_finish = DateAdd("n", g_range, g_pull_start)
    Exit Sub
    
errHandler:
    g_util.ThrowError "ParseCommandLine"
    Resume  'debug
End Sub


Private Sub Process()
    On Error GoTo errHandler
    
    Dim pat As PatientInfo
    Dim rs As New Recordset
    Dim sql As String
    Dim count As Integer
    Dim current_meth_id As Long
    Dim i As Integer
    
If called_from_server Then 'query by timestamp
    nowdt = Now
'MsgBox "Process"
    'dprint ""
    'dprint "Pull at:   " & g_pull_finish & " (back to " & g_pull_start & "; range=" & g_range & ")"
    'dprint "Effective: " & g_effdt
    
    dprint "Searching for charting added since: " & g_dbutil.SQL_DateTime(g_chart_start)
    sql = g_dbutil.DatabaseName(g_cnADO)
    sql = g_dbutil.ServerName(g_cnADO)
'MsgBox "sql string"
    
    '
    ' Make a list of all patients with chart items during the pull period.
    ' Include only units that have the "use transparent" flag set.
    ' Limit to one patient if -acct is given.  Limit to one unit with -unit.
    ' Left join encounter_location - the patient may be discharged at pull time.
    ' Look for the latest patient arrival to the unit before the pull time; ignore transfers within the unit.
    ' Look for a patient departure (if any) before the pull time; ignore transfers within the unit.
    sql = "" & _
        " SELECT LIST.UNIT_ID, LIST.ENCOUNTER_ID, PARAM.METHODOLOGY_ID," & _
        "     UNIT.NAME AS UNIT," & _
        "     ARRIVE.DATETIME_IN AS UNIT_ARRIVAL, DEPART.DATETIME_OUT AS UNIT_DEPARTURE," & _
        "     E.ACCT_NUMBER, P.LAST_NAME, P.FIRST_NAME," & _
        "     P.DOB, E.ADMISSION_DATETIME, E.REGISTRATION_DATETIME, F.CLASSIFICATION_FACILITY_CODE" & _
        " FROM (" & _
        "     SELECT DISTINCT CI.UNIT_ID, CI.ENCOUNTER_ID" & _
        "     FROM CHART_ITEM AS CI" & _
        "     INNER JOIN UNIT ON (UNIT.UNIT_ID = CI.UNIT_ID)" & _
        "     WHERE UNIT.USE_TRANSPARENT_CLASSIFICATION='Y'" & _
        "     AND TIMESTAMP>=" & g_dbutil.SQL_DateTime(g_chart_start) & _
        " ) AS LIST"
    sql = sql & _
        " INNER JOIN UNIT ON (UNIT.UNIT_ID = LIST.UNIT_ID)" & _
        " INNER JOIN FACILITY AS F ON (UNIT.FACILITY_ID=F.FACILITY_ID)" & _
        " INNER JOIN UNIT_PARAM AS PARAM ON (PARAM.UNIT_ID = UNIT.UNIT_ID) AND (" & g_dbutil.SQL_DateTime(g_chart_start) & " BETWEEN PARAM.EFFECTIVE_DATEtime AND param.EXPIRATION_DATEtime)" & _
        " INNER JOIN ENCOUNTER AS E ON (E.ENCOUNTER_ID = LIST.ENCOUNTER_ID)" & _
        " INNER JOIN PERSON AS P ON (P.PERSON_ID = E.PERSON_ID)" & _
        " INNER JOIN (" & _
        "    SELECT WORKING_UNIT_ID AS UNIT_ID, ENCOUNTER_ID, MAX(DATETIME_IN) AS DATETIME_IN" & _
        "    FROM ENCOUNTER_LOCATION AS EL" & _
        "    WHERE WORKING_UNIT_ID IN (SELECT UNIT_ID FROM UNIT WHERE USE_TRANSPARENT_CLASSIFICATION='Y')" & _
        "    AND DATETIME_IN < " & g_dbutil.SQL_DateTime(g_chart_start) & _
        "    AND IS_TRANSFER_IN='Y'" & _
        "    GROUP BY WORKING_UNIT_ID, ENCOUNTER_ID" & _
        " ) AS ARRIVE ON (ARRIVE.UNIT_ID = UNIT.UNIT_ID) AND (ARRIVE.ENCOUNTER_ID = E.ENCOUNTER_ID)" & _
        " left JOIN (" & _
        "    SELECT WORKING_UNIT_ID AS UNIT_ID, ENCOUNTER_ID, MAX(DATETIME_OUT) AS DATETIME_OUT" & _
        "    FROM ENCOUNTER_LOCATION AS EL" & _
        "    WHERE WORKING_UNIT_ID IN (SELECT UNIT_ID FROM UNIT WHERE USE_TRANSPARENT_CLASSIFICATION='Y')" & _
        "    AND DATETIME_OUT < " & g_dbutil.SQL_DateTime(g_chart_start) & _
        "    AND ((NEXT_WORKING_UNIT_ID<>WORKING_UNIT_ID) OR (NEXT_WORKING_UNIT_ID IS NULL))" & _
        "    GROUP BY WORKING_UNIT_ID, ENCOUNTER_ID" & _
        " ) AS DEPART ON (DEPART.UNIT_ID = UNIT.UNIT_ID) AND (DEPART.ENCOUNTER_ID = E.ENCOUNTER_ID)" & _
        " WHERE 1=1"
'Oh yeah.  Each patient will have their own time range.
'Since they are ED you might get away with not giving any in/out times and rely on the ADT information.
'Yes, you could use the "pull" time to get encounter ids from chart items - but use the timestamp -
'if someone reloads a chart, the chart items will get new timestamps.
'Once you have encounter ids you can classify them and let the import work out the in/out times.
    
'    If Len(debug_acct) Then
'        sql = sql & " AND E.ACCT_NUMBER=" & g_dbutil.SQL_String(debug_acct)
'    End If
'    If Len(debug_unit) Then
'        sql = sql & " AND UNIT.NAME=" & g_dbutil.SQL_String(debug_unit)
'    End If
'    If (debug_unit_id > 0) Then
'        sql = sql & " AND UNIT.UNIT_ID=" & debug_unit_id
'    End If
    
Else ' called from client - query by event_datetime
    sql = "" & _
        " SELECT LIST.UNIT_ID, LIST.ENCOUNTER_ID, PARAM.METHODOLOGY_ID," & _
        "     UNIT.NAME AS UNIT," & _
        "     ARRIVE.DATETIME_IN AS UNIT_ARRIVAL, DEPART.DATETIME_OUT AS UNIT_DEPARTURE," & _
        "     E.ACCT_NUMBER, P.LAST_NAME, P.FIRST_NAME," & _
        "     P.DOB, E.ADMISSION_DATETIME, E.REGISTRATION_DATETIME, F.CLASSIFICATION_FACILITY_CODE" & _
        " FROM (" & _
        "     SELECT DISTINCT CI.UNIT_ID, CI.ENCOUNTER_ID" & _
        "     FROM CHART_ITEM AS CI" & _
        "     INNER JOIN UNIT ON (UNIT.UNIT_ID = CI.UNIT_ID)" & _
        "     WHERE UNIT.USE_TRANSPARENT_CLASSIFICATION='Y'" & _
        " ) AS LIST"
'        "     AND EVENT_DATETIME BETWEEN " & g_dbutil.SQL_DateTime(g_pull_start) & " AND " & g_dbutil.SQL_DateTime(g_pull_finish) & _
'        " ) AS LIST"
    sql = sql & _
        " INNER JOIN UNIT ON (UNIT.UNIT_ID = LIST.UNIT_ID)" & _
        " INNER JOIN FACILITY AS F ON (UNIT.FACILITY_ID=F.FACILITY_ID)" & _
        " INNER JOIN UNIT_PARAM AS PARAM ON (PARAM.UNIT_ID = UNIT.UNIT_ID) AND (" & g_dbutil.SQL_DateTime(g_pull_finish) & " BETWEEN PARAM.EFFECTIVE_DATEtime AND param.EXPIRATION_DATEtime)" & _
        " INNER JOIN ENCOUNTER AS E ON (E.ENCOUNTER_ID = LIST.ENCOUNTER_ID)" & _
        " INNER JOIN PERSON AS P ON (P.PERSON_ID = E.PERSON_ID)" & _
        " INNER JOIN (" & _
        "    SELECT WORKING_UNIT_ID AS UNIT_ID, ENCOUNTER_ID, MAX(DATETIME_IN) AS DATETIME_IN" & _
        "    FROM ENCOUNTER_LOCATION AS EL" & _
        "    WHERE WORKING_UNIT_ID IN (SELECT UNIT_ID FROM UNIT WHERE USE_TRANSPARENT_CLASSIFICATION='Y')" & _
        "    AND DATETIME_IN < " & g_dbutil.SQL_DateTime(g_pull_finish) & _
        "    AND IS_TRANSFER_IN='Y'" & _
        "    GROUP BY WORKING_UNIT_ID, ENCOUNTER_ID" & _
        " ) AS ARRIVE ON (ARRIVE.UNIT_ID = UNIT.UNIT_ID) AND (ARRIVE.ENCOUNTER_ID = E.ENCOUNTER_ID)" & _
        " left JOIN (" & _
        "    SELECT WORKING_UNIT_ID AS UNIT_ID, ENCOUNTER_ID, MAX(DATETIME_OUT) AS DATETIME_OUT" & _
        "    FROM ENCOUNTER_LOCATION AS EL" & _
        "    WHERE WORKING_UNIT_ID IN (SELECT UNIT_ID FROM UNIT WHERE USE_TRANSPARENT_CLASSIFICATION='Y')" & _
        "    AND DATETIME_OUT <= " & g_dbutil.SQL_DateTime(g_pull_finish) & _
        "    AND ((NEXT_WORKING_UNIT_ID<>WORKING_UNIT_ID) OR (NEXT_WORKING_UNIT_ID IS NULL))" & _
        "    GROUP BY WORKING_UNIT_ID, ENCOUNTER_ID" & _
        " ) AS DEPART ON (DEPART.UNIT_ID = UNIT.UNIT_ID) AND (DEPART.ENCOUNTER_ID = E.ENCOUNTER_ID)" & _
        " WHERE 1=1"
End If
    If Len(debug_acct) Then
        sql = sql & " AND E.ACCT_NUMBER=" & g_dbutil.SQL_String(debug_acct)
    End If
'    If Len(debug_unit) Then
'        sql = sql & " AND UNIT.NAME=" & g_dbutil.SQL_String(debug_unit)
'    End If
'    If (debug_unit_id > 0) Then
'        sql = sql & " AND UNIT.UNIT_ID=" & debug_unit_id
'    End If
    
    Debug.Print sql
    dprint sql
    rs.Open sql, g_cnADO
    count = 0

    Do While Not rs.EOF
        'Package the patient info (don't use unit/encounter objects)
        With pat
            .unit_id = rs("UNIT_ID")
            .fac_code = rs("CLASSIFICATION_FACILITY_CODE")
            .encounter_id = rs("ENCOUNTER_ID")
            .meth_id = 0 'rs("METHODOLOGY_ID")
            .acct = rs("ACCT_NUMBER")
            .last_name = rs("LAST_NAME") & ""
            .first_name = rs("FIRST_NAME") & ""
            .unit_name = rs("UNIT") & ""
            .room = ""
            .bed = ""
            
            If Not IsNull(rs("DOB")) And Not IsNull(rs("ADMISSION_DATETIME")) Then
                .age = DateDiff("n", rs("DOB"), rs("ADMISSION_DATETIME")) / 60# / 24# / 365.24219
            ElseIf Not IsNull(rs("DOB")) And Not IsNull(rs("REGISTRATION_DATETIME")) Then
                .age = DateDiff("n", rs("DOB"), rs("REGISTRATION_DATETIME")) / 60# / 24# / 365.24219
            Else
                .age = 0
            End If
            
            .unit_arrival = rs("UNIT_ARRIVAL")
            .effective = .unit_arrival
            .pull_start = .unit_arrival
            If called_from_server Then 'if pt not departed/discharged then set a default out
                If IsNull(rs("UNIT_DEPARTURE")) Then
                    g_pull_finish = nowdt
                End If
            End If
            .pull_finish = g_util.Min(g_pull_finish, rs("UNIT_DEPARTURE"))
            If (.pull_finish < .pull_start) Then
                .pull_finish = g_pull_finish
            End If
            .range = DateDiff("n", .pull_start, .pull_finish)
            .TC_source_id = 1 'g_dbutil.DBToInteger(rs("TC_SOURCE_ID"))
        End With
        
        'Save ids for log entries
        gLogUnitID = pat.unit_id
        gLogEncounterID = pat.encounter_id
    
        'Process this patient
        count = count + 1
        frmMain.SetProgress "Processing patient " & count & " of " & rs.RecordCount
        
        dvprint ""
        dprint String$(80, "=")
        dprint "Patient " & count & " of " & rs.RecordCount & ": " & _
            pat.last_name & ", " & pat.first_name & _
            " in " & pat.unit_name & " " & pat.room & " " & pat.bed & _
            " acct=" & pat.acct & " age=" & Round(pat.age, 2)
        dprint "Here from " & pat.pull_start & " to " & pat.pull_finish & _
            "  (LOS=" & Round(pat.range / 60, 2) & ")"

        g_range = pat.range
        
        pat.num_locs = 0
'MsgBox "about to get lochist"
'        dprint "About to GetLocationHistory"
        GetLocationHistory pat
'        dprint "About to SetUnits"
        SetLocationUnits pat
        CondenseUnits pat
        
'Loc History could possibly be:
'ETUR EDVisit
'ETUW EDVisit
'EDOB EDInpt
        
'ETUR EDVisit
'EDOB EDInpt
'EDOC EDInpt

' So: MAY Need to condense adjacent EDVisit or adjacent EDInp locations
        
        'Most sites will have one source of chart items
        'If there are multiple sources that need different mapping, this is the place to do it
        For i = 1 To pat.num_locs
            current_meth_id = pat.locary(i).meth_id
            Select Case pat.TC_source_id
    '        Case 123
    '            Select Case pat.meth_id
    '            Case METH_ID_INPATIENT
    '                ProcessInpatient_123 pat
    '            Case Else
    '                LogWarning "Methodology " & pat.meth_id & " in unit " & pat.unit_name & " is not supported"
    '            End Select
            
            Case Else
    
                Select Case current_meth_id
                Case METH_ID_ED_VISIT
                    dprint "========== ED Visit data evaluation =========="
                    ProcessEDVisit pat, i, i = pat.num_locs
                Case METH_ID_ED_INPATIENT
                    dprint "========== ED Inpatient data evaluation =========="
                    ProcessEDInpatient pat, i, i = pat.num_locs
                Case Else
                    LogWarning "Methodology " & pat.meth_id & " in unit " & pat.unit_name & " is not supported"
                End Select
    
            End Select
        Next i
        
        gLogUnitID = 0
        gLogEncounterID = 0

        If g_abort Then Exit Do
        
        rs.MoveNext
    Loop
    
    rs.Close
    
    If (count < 1) Then
        dprint ""
        If Len(debug_acct) Then
            LogWarning "The selected patient has no chart items in the given time range", EVENT_CATEGORY_UNEXPECTED
        Else
            LogWarning "No chart items found to process - have the unit(s) been enabled for transparent classification?", EVENT_CATEGORY_UNEXPECTED
        End If
    End If
    
    Exit Sub

errHandler:
    g_util.ThrowError "Process"
    Resume  'debug
End Sub

'debug print
Public Sub dprint(s As String)
    If Not g_debug Then Exit Sub
    
    Debug.Print s                           'add to debug window
    'It would be nice if we could print to the cmd window, but it takes some effort

    If dbugfile <> 0 Then
        Print #dbugfile, s                  'add to debug log file
    End If
End Sub

'debug verbose print
Public Sub dvprint(s As String)
    If g_verbose Then dprint s
End Sub

Private Sub OpenOutputFiles()
    On Error GoTo errHandler
    Dim dstr As String
    
    If Not g_util.GetKeyValue(HKEY_LOCAL_MACHINE, "SOFTWARE\Wow6432Node\Quadramed\WinPFS", "Directory", winpfspath) Then
        winpfspath = App.Path & "\.."
    End If

    winlogpath = winpfspath & "\log"
    winloadpath = winpfspath & "\load_me"
'    MsgBox winlogpath
    If Not g_util.DirExists(winlogpath) Then
        MkDir$ winlogpath
    End If

    'dbugname = winlogpath & "\" & TRANSP_DEBUG & Format$(nowdt, "mmdd") & ".log"
    If g_datelog Then dstr = Format$(nowdt, "mmdd")
    dbugname = "c:\users\public\acuityplus\log\transparentaudit.log" 'winlogpath & "\" & dstr & TRANSP_AUDIT_LOG
'    MsgBox dbugname
    
    If g_debug Then
        dbugfile = FreeFile
        Open dbugname For Output As #dbugfile                   'overwrite existing
        Print #dbugfile, String(80, "=")
        Print #dbugfile, "TRANSPARENT MAPPING AUDIT FILE                       Time=" & nowdt
    End If
    
    If g_no_output Then Exit Sub

    outfilename = winloadpath & "\" & TRANSP_FILENAME           'load_me\transparent.txt
    outfile = FreeFile
    Open outfilename For Append As #outfile                     'add to existing
    
    Exit Sub

errHandler:
    g_util.ThrowError "OpenOutputFiles"
    Resume  'debug
End Sub
Private Sub CloseOutputFiles()
    If (outfile <> 0) Then Close #outfile
    If (outfile2 <> 0) Then Close #outfile2
    
    If (dbugfile <> 0) Then
        Close #dbugfile
        FileCopy "c:\users\public\acuityplus\log\transparentaudit.log", winlogpath & "\" & TRANSP_AUDIT_LOG
        dbugfile = 0                                            'stop writing to debug file
    End If
End Sub

Private Function ConvertDate(d As String) As String

'2012-07-10 13:53:00.000

    ConvertDate = Mid$(d, 1, 4) & Mid$(d, 6, 2) & Mid$(d, 9, 2) & Mid$(d, 12, 2) & Mid$(d, 15, 2) & Mid$(d, 18, 2)
End Function


Private Sub DeleteOldLogs()
    'There are no more log files; history has been added to the event log
End Sub

Private Sub DeleteOldChartItems()
    On Error GoTo errHandler
    
    Dim sql As String
    Dim dt As Date
    
    If Not called_from_server Then Exit Sub

    dt = DateAdd("d", -CHART_ITEM_LIFE, Date)
    sql = "DELETE FROM CHART_ITEM WHERE EVENT_DATETIME < " & g_dbutil.SQL_DateTime(dt)
    'Debug.Print sql
    g_cnADO.Execute sql
    Exit Sub
    
errHandler:
    g_util.ThrowError "DeleteOldChartItems"
    Resume  'debug
End Sub


Public Sub AddLogEntry(event_type As EventLogType, msg As String, _
    Optional event_category As EventLogCategory = EVENT_CATEGORY_NONE _
)
    On Error GoTo errHandler

    dprint msg                      'copy to debug log
'dvprint "logentry args: event_type=" & event_type & " msg=" & msg & " cat=" & event_category & " unitid=" & gLogUnitID & " encid=" & gLogEncounterID
    g_event.AddEventLogEntry EVENT_SOURCE_TRANSPARENT_MAPPING, event_type, event_category, _
        msg, gLogSourceText, 0, gLogUnitID, gLogEncounterID
    Exit Sub
    
errHandler:
    'no message boxes allowed (this is a command-line program)
    Debug.Print Err.Description
End Sub

Public Sub LogInfo(s As String, Optional event_category As EventLogCategory = EVENT_CATEGORY_NONE)
    AddLogEntry EVENT_TYPE_INFO, s, event_category
End Sub

Public Sub LogWarning(s As String, Optional event_category As EventLogCategory = EVENT_CATEGORY_VALIDATION)
    AddLogEntry EVENT_TYPE_WARNING, s, event_category
End Sub

Public Sub LogError(s As String, Optional event_category As EventLogCategory = EVENT_CATEGORY_VALIDATION)
    AddLogEntry EVENT_TYPE_ERROR, s, event_category
End Sub


Private Sub DeleteIbexFiles()
    On Error GoTo errHandler
    Dim rs As New Recordset
    Dim ibex As String, tibex As String, pathfn As String
    Dim sql As String, sql2 As String, reason As String, rsfn As String, pos As Integer

    sql = "select free_text,this_date,lookup_description,report_note_id from report_notes"
    sql = sql & " inner join lookup_value on (reason_lvc=lookup_code)"
    sql = sql & " where reason_lvc in (98,99) and lookup_definition_key=46"
    rs.Open sql, g_cnADO
    Do While Not rs.EOF
        rsfn = Trim$(rs(0))
        pos = InStr(1, rsfn, ".xml", vbTextCompare)
        If pos > 0 Then
            rsfn = Mid$(rsfn, 1, pos - 1)
        End If
        pathfn = winpfspath & "\loadibex\" & rsfn
'dvprint "delibex: pathfn=" & pathfn
        ibex = pathfn & ".xml"
        tibex = pathfn & ".txt"
        If g_util.FileExists(ibex) Then
            Kill ibex
            If g_util.FileExists(tibex) Then Kill tibex
            gLogSourceText = rs(0)
            AddLogEntry EVENT_TYPE_INFO, rs(2), EVENT_CATEGORY_PROCESSED
            gLogSourceText = ""
            sql2 = "delete REPORT_NOTES where REPORT_NOTE_ID=" & rs(3)
            g_cnADO.Execute sql2
        Else
            gLogSourceText = rs(0)
            reason = "Fail to delete: IBEX file not found; Staffing Note " & Format$(rs(1), "mm/dd/yy")
            AddLogEntry EVENT_TYPE_REJECT, reason, EVENT_CATEGORY_VALIDATION
            gLogSourceText = ""
        End If
        rs.MoveNext
    Loop
    'Debug.Print sql
    rs.Close
    Exit Sub
    
errHandler:
    g_util.ThrowError "DeleteIbexFiles"
    Resume  'debug
End Sub

Private Sub GetLocationHistory(ByRef pat As PatientInfo)
    Dim rs As New Recordset
    Dim sql As String
    Dim room As String
    
    'Gets all the EDParent locations and puts them into an array in the pat record.
    
    sql = "select el.effective_datetime_in,el.effective_datetime_out,el.room from encounter_location as el"
    sql = sql & " inner join encounter as e on (e.encounter_id=el.encounter_id)"
    sql = sql & " where e.acct_number=" & g_dbutil.SQL_String(pat.acct)
    sql = sql & " and el.unit_id=" & g_dbutil.SQL_Number(parent_id)
    sql = sql & " order by el.effective_datetime_in"
'    dprint sql
    rs.Open sql, g_cnADO
    Do While Not rs.EOF
        pat.num_locs = pat.num_locs + 1
        If (pat.num_locs < MAXLOCS) Then
            pat.locary(pat.num_locs).time_in = rs(0)
            pat.locary(pat.num_locs).time_out = rs(1)
            pat.locary(pat.num_locs).unit_id = 0
            pat.locary(pat.num_locs).toremove = True
            room = Trim(rs(2))
            If (Mid$(room, 1, 3) = "ETU") Then
                pat.locary(pat.num_locs).unit_id = vunit_id
                pat.locary(pat.num_locs).toremove = False
                pat.locary(pat.num_locs).meth_id = METH_ID_ED_VISIT
            End If
            If (Mid$(room, 1, 3) = "EDO") Then
                pat.locary(pat.num_locs).unit_id = iunit_id
                pat.locary(pat.num_locs).toremove = False
                pat.locary(pat.num_locs).meth_id = METH_ID_ED_INPATIENT
                If (Mid$(room, 4, 1) = "C") Then pat.locary(pat.num_locs).txarea = "CENTER"
                If (Mid$(room, 4, 1) = "E") Then pat.locary(pat.num_locs).txarea = "EAST"
                If (Mid$(room, 4, 1) = "N") Then pat.locary(pat.num_locs).txarea = "NORTH"
                If (Mid$(room, 4, 1) = "P") Then pat.locary(pat.num_locs).txarea = "PEDS"
                If (Mid$(room, 4, 1) = "S") Then pat.locary(pat.num_locs).txarea = "SOUTH"
                If (Mid$(room, 4, 1) = "W") Then pat.locary(pat.num_locs).txarea = "WEST"
            End If
        End If
        rs.MoveNext
    Loop
    rs.Close
    
    Exit Sub
    
End Sub

Private Sub SetLocationUnits(ByRef pat As PatientInfo)
    Dim rs As New Recordset
    Dim sql As String
    Dim evn1 As String
    Dim evn2dt As Date
    Dim evn6dt As Date
    Dim pv13 As String
    Dim pv144dt As Date
    Dim i As Integer
    
    'Assigns the EDChild unit id to the locations in the pat record.
    
    sql = "select ltrim(rtrim(substring(replace("
    sql = sql & "cast(substring(source_text, CHARINDEX('EVN|', source_text),200) as nvarchar(max)),'|',"
    sql = sql & "replicate(cast(' ' as nvarchar(max)),1000)),1001, 100))) as EVN1,"
    sql = sql & "ltrim(rtrim(substring( replace("
    sql = sql & "cast(substring(source_text, CHARINDEX('EVN|', source_text),200) as nvarchar(max)),'|',"
    sql = sql & "replicate(cast(' ' as nvarchar(max)),1000)),2001, 1000))) as EVN2,"
    sql = sql & "ltrim(rtrim(substring(replace("
    sql = sql & "cast(substring(source_text, CHARINDEX('EVN|', source_text),200) as nvarchar(max)),'|',"
    sql = sql & "replicate(cast(' ' as nvarchar(max)),1000)),6001, 1000))) as EVN6,"
    sql = sql & "ltrim(rtrim(substring(replace("
    sql = sql & "cast(substring(source_text, CHARINDEX('PV1|', source_text),200) as nvarchar(max)),'|',"
    sql = sql & "replicate(cast(' ' as nvarchar(max)),1000)),3001, 1000))) as PV13,"
    sql = sql & "ltrim(rtrim(substring(replace("
    sql = sql & "cast(substring(source_text, CHARINDEX('PV1|', source_text),200) as nvarchar(max)),'|',"
    sql = sql & "replicate(cast(' ' as nvarchar(max)),1000)),44001, 1000))) as PV144"
    sql = sql & " from event_log as el inner join encounter as e on (el.encounter_id=e.encounter_id)"
    sql = sql & " where e.acct_number=" & g_dbutil.SQL_String(pat.acct)
    sql = sql & " and event_type=1 and event_source=1 and event_category=4"
    sql = sql & " order by el.timestamp"
'    dprint sql
    rs.Open sql, g_cnADO
    
    Do While Not rs.EOF
        evn1 = Trim(rs("EVN1"))
        If IsNumeric(rs("EVN2")) Then
            evn2dt = g_util.CDateEx(rs("EVN2"))
        Else
            evn2dt = 0
        End If
        If IsNumeric(rs("EVN6")) Then
            evn6dt = g_util.CDateEx(rs("EVN6"))
        Else
            evn6dt = 0
        End If
        pv13 = Trim(rs("PV13"))  '  if ETU then visit   if EDO then inp
        If IsNumeric(rs("PV144")) Then
            pv144dt = g_util.CDateEx(rs("PV144"))
        Else
            pv144dt = 0
        End If
        i = 0
        Do While (i < pat.num_locs)
            i = i + 1
            MaybeSetUnitForLocation pat.locary(i), evn1, evn2dt, evn6dt, pv13, pv144dt
        Loop
        rs.MoveNext
    Loop
    rs.Close
    '
    ' Set Default Location here for when NO ADT messages
    ' --could set all locations here if special Room is entered when constructing ADT location manually
    ' --for example, make the Room=EDON --but would then need to get rooms in GetLocationHistory
    '
    
    Exit Sub
End Sub
Private Sub CondenseUnits(ByRef pat As PatientInfo)
    Dim i As Integer
    Dim savary(MAXLOCS)     As LocationData
    Dim n As Integer
    Dim j As Integer
    Dim tx As String
    
    'FIRST:  If there are any locations with unit_id=0, then make those locations equal to the previous
    '   We Know that all locations are ED parent.  these locations didn't get assigned due to A08.
    With pat
    If .num_locs <= 1 Then Exit Sub
    For i = 2 To .num_locs
        If (.locary(i).unit_id = 0) Then
            .locary(i).unit_id = .locary(i - 1).unit_id
            .locary(i).meth_id = .locary(i - 1).meth_id
            .locary(i).toremove = False
        End If
    Next i
    End With
    
'Give all same-methodology locations the same txarea
    With pat
    tx = ""
    For i = 1 To .num_locs
        If (.locary(i).meth_id = METH_ID_ED_VISIT) Then
            If (Trim(.locary(i).txarea) <> "") Then tx = Trim(.locary(i).txarea)
        End If
    Next i
    For i = 1 To .num_locs
        If (.locary(i).meth_id = METH_ID_ED_VISIT) Then
            .locary(i).txarea = tx
        End If
    Next i
    tx = ""
    For i = 1 To .num_locs
        If (.locary(i).meth_id = METH_ID_ED_INPATIENT) Then
            If (Trim(.locary(i).txarea) <> "") Then tx = Trim(.locary(i).txarea)
        End If
    Next i
    For i = 1 To .num_locs
        If (.locary(i).meth_id = METH_ID_ED_INPATIENT) Then
            .locary(i).txarea = tx
        End If
    Next i
    End With
    
    
' let's say there are 2 visit and 3 adj inp
' 1: visit  0800 - 0900 f                                  0800-1200
' 2: visit  0900 - 1200 f                                      ^        t
' 3: inp    1200 - 1300 f                  1200-1700           |        ^
' 4: inp    1300 - 1500 f    1300-1700         ^       t       |        |
' 5: inp    1500 - 1700 f       ^       t      |       ^       |        |
' loop makes these changes >>---+     --^  ----+     --+    ---+      --+
    
    With pat
    n = .num_locs
    If .num_locs <= 1 Then Exit Sub
    For i = .num_locs - 1 To 1 Step -1
        If (.locary(i).unit_id = .locary(i + 1).unit_id) And (.locary(i).time_out = .locary(i + 1).time_in) Then
            .locary(i).time_out = .locary(i + 1).time_out
            .locary(i).range = DateDiff("n", .locary(i).time_in, .locary(i).time_out)
            .locary(i + 1).toremove = True
            n = n - 1
        End If
    Next i
    End With
    
'at this point, n = the number of good locs.
    With pat
    If n < .num_locs Then
        j = 0
        For i = 1 To .num_locs
            If (Not .locary(i).toremove) Then
                j = j + 1
                savary(j) = .locary(i)
            End If
        Next i
        For i = 1 To n
            .locary(i) = savary(i)
        Next i
        .num_locs = n
    End If
    End With
    
    For i = 1 To pat.num_locs
        dprint "loc: " & i & " unit=" & pat.locary(i).unit_id & " meth=" & pat.locary(i).meth_id & " in=" & pat.locary(i).time_in & " out=" & pat.locary(i).time_out & " txarea=" & pat.locary(i).txarea
'        dprint "meth " & pat.locary(i).meth_id
'        dprint "range " & pat.locary(i).range
'        dprint "in " & pat.locary(i).time_in
'        dprint "out " & pat.locary(i).time_out
'        dprint "unit " & pat.locary(i).unit_id
'        dprint "txarea " & pat.locary(i).txarea
    Next i

End Sub

Private Sub MaybeSetUnitForLocation(ByRef p As LocationData, evn1 As String, evn2dt As Date, evn6dt As Date, pv13 As String, pv144dt As Date)
    'Very loose requirement to match up location to unit_id:
    '    If one of the times matches, then consider it the match.
    If (Mid$(evn1, 1, 3) <> "A01") And (Mid$(evn1, 1, 3) <> "A02") And _
       (Mid$(evn1, 1, 3) <> "A04") And _
       (Mid$(evn1, 1, 3) <> "A06") And (Mid$(evn1, 1, 3) <> "A07") Then Exit Sub
    If (p.unit_id <> 0) Then Exit Sub
    If DateDiff("n", p.time_in, evn2dt) = 0 Or DateDiff("n", p.time_in, evn6dt) = 0 Or DateDiff("n", p.time_in, pv144dt) = 0 Then
'        dprint "time match"
        If (Mid$(pv13, 1, 3) = "ETU") Or (Mid$(pv13, 1, 4) = "ETPR") Then
'            dprint "ETU"
            p.unit_id = vunit_id   'visit unit id
            p.meth_id = METH_ID_ED_VISIT
            p.toremove = False
        ElseIf (Mid$(pv13, 1, 3) = "EDO") Or (Mid$(pv13, 1, 4) = "EDPR") Then
'            dprint "EDO"
            p.txarea = ""
            If (Mid$(pv13, 1, 3) = "EDO") Then
                If (Mid$(pv13, 4, 1) = "C") Then p.txarea = "CENTER"
                If (Mid$(pv13, 4, 1) = "E") Then p.txarea = "EAST"
                If (Mid$(pv13, 4, 1) = "N") Then p.txarea = "NORTH"
                If (Mid$(pv13, 4, 1) = "P") Then p.txarea = "PEDS"
                If (Mid$(pv13, 4, 1) = "S") Then p.txarea = "SOUTH"
                If (Mid$(pv13, 4, 1) = "W") Then p.txarea = "WEST"
            End If
            p.unit_id = iunit_id   'inp unit id
            p.meth_id = METH_ID_ED_INPATIENT
            p.toremove = False
        End If
        p.range = DateDiff("n", p.time_in, p.time_out)
    End If

End Sub
Private Sub GetUnitNames()
    Dim rs As New Recordset
    Dim sql As String
    Dim i As Integer
    
    i = 0
    sql = "select unit_id,name from unit where unit_id in (" & g_dbutil.SQL_Number(parent_id) & "," & g_dbutil.SQL_Number(vunit_id) & "," & g_dbutil.SQL_Number(iunit_id) & ")"
    rs.Open sql, g_cnADO
    Do While Not rs.EOF
        i = i + 1
        g_units(i).unit_id = rs(0)
        g_units(i).name = Trim(rs(1))
        rs.MoveNext
    Loop
    rs.Close
End Sub
Public Function UnitName(UnitID As Long) As String
    Dim i As Integer
    Dim s As String
    s = ""
    For i = 1 To MAXLOCS - 1
        If g_units(i).unit_id = UnitID Then s = g_units(i).name
    Next i
    UnitName = s
End Function


'Version 2:  patient stays in the ED unit  (would alias ETPR and EDPR to the ED)
'
'ETUx  0700-0800                                                    (ED Visit from 0700-0900,  note that the "x" can change)
'ETPR 800 - 830
'ETUx 830 - 900
'Discharge or admit
'
'
'ETUx  0700-0800                                                    (ED Visit from 0700-0830)
'ETPR 800 - 830
'EDOx 0830-1100                                                     (ED Inpatient from 0830-1100)
'Discharge or admit
'
'
'ETUx  0700-0800                                                    (ED Visit  from 0700-0800)
'EDOx 0800-0900                                                     (ED Inpatient from 0800-1100)
'EDPR 900 - 930
'EDOx 930 - 1100
'Discharge or admit

'For these 3 scenarios above,  I will use these time ranges:
'"x" refers to documented time as given in the IBEX file
'and could be earlier than admission, or later than discharge.
'1.  Totally open - No time constraints
'
'2.  For ED Visit:     x until 0830
'For ED Inp:      0830 until x
'
'3.  For ED Visit:     x until 0800
'For ED Inp:      0800 until x
'
