Attribute VB_Name = "MayoEDVisit"
Option Explicit
'
' Mayo ED Visit (copied from Shands Inpatient 2.0)
'
' This processes one patient using the ED Visit methodology.
'
' All search functions use exact match for code.
'
' result_like looks for LIKE matches in the result, except where EXACT_MATCH_PREFIX is pre-appended to target.
' result_list looks for any one of a list of words exactly as the result.
'
Private Const MAX_INDS = 90
Private Const MAX_PROCS = 20
Public Const MAX_BUCKETS = 2880

Private Const EXACT_MATCH_PREFIX = "&!"
Private Const LIKE_PREFIX = "%!"
Private Const CHAR_COMMA = "||"


Private Type indicator_data
    checked     As Boolean
    group       As Integer
    also_mark   As String                   'not used at Arnot (handy with user-defined indicators?)
End Type

Private Type procdata                       'not used at Arnot
    pnum    As Integer
    start   As Date
    finish  As Date
    isvalid As Boolean 'used in combining procs
    pindex As Integer 'used in combining procs = the index of procs whose start/finish times are equal to this one
End Type
Private Type ocdata                       'not used at Arnot
    checked As Boolean
    pnum    As Integer
    start   As String
End Type
Private Type bucket_type
    count As Integer
    startdt As Date
    enddt As Date
End Type
Public Type bucket2_type
    eventdt As Date
    using_waiver As Boolean
End Type

    

Private inds(MAX_INDS)          As indicator_data

Private numprocs                As Integer
Private numoutcomes             As Integer
Private procs(MAX_PROCS)        As procdata
Private oc(MAX_PROCS)           As ocdata

Private WHERE_ENCOUNTER         As String
Private AND_UNIT                As String
Private AND_PULL_RANGE          As String
Private AND_OUTCOMES_RANGE      As String
Private AND_ARRIVAL             As String
Private BUCKET_CALC             As String

Private m_pat                   As PatientInfo
Private m_assist_count          As Integer
Private m_locindex              As Integer
Private m_islastloc              As Boolean

'Private bucket(MAX_BUCKETS)     As bucket_type  '24 hrs x 4 15-min ranges = 96 x n LOS days, say 3 days
Public bucket2(MAX_BUCKETS)     As bucket2_type  '24 hrs x 4 15-min ranges = 96 x n LOS days, say 3 days
Private num_buckets             As Integer

Private Enum SearchMode
    SearchDefault
    Searchpullrange             'search within the current pull     -- default
    SearchSinceArrival          'search since arrival to the unit
    SearchSinceAdmission        'search everything since admission to the hospital
    SearchOutcomesRange         'search within the current pull + 24 hours before
    SearchAssessments
End Enum

Private Enum CountMode
    CountAll
    CountFirst                  'stop after one is found
End Enum

Private Enum GetValueMode
    GetTotal
    GetMax
    GetLast
End Enum

Private Const FREQUENCY_BUCKET_SIZE = 20    'min

Private Enum Frequencies
    QNONE
    Q4H
    Q2H
    Q1H
    Q30M
End Enum

Private Type fmapRow
    los_high                As Single       'the LOS being testing
    freq(QNONE To Q30M)     As Integer      'the count required for each Q value
End Type

Private m_freq_map(6)       As fmapRow      '1,2,4,8,12,24 hours
Public b_filter            As String   'Filter for Buckets: all the triggers ORed together
Public b_excl              As String
Private bucketsize          As Integer
Public b2num               As Integer
Public num_waivers          As Integer


'This is the main entry point
'
Public Sub ProcessEDVisit(pat As PatientInfo, loc_index As Integer, islastloc As Boolean)
    On Error GoTo errHandler
    
    pat.meth_id = METH_ID_ED_VISIT
    m_pat = pat
    m_locindex = loc_index
    m_islastloc = islastloc
'    frmMain.SetProgress "Processing acct: " & m_pat.acct
    InitGroupsIfNeeded
    SetSQLConstants
    LoadFreqTable
    ResetIndicators
'    ResetProcs
'    ResetOutcomes

    CheckExploding
    Check_1
    Check_234
    Check_5
    Check_67
    Check_89
    Check_1011
    Check_1213
    Check_14
    Check_15
    Check_16
    Check_17
    Check_181920
    CheckCustom
    'CheckCustom
    HighestIndicatorInEachGroupWins

'    CheckProcs
'    CheckOutcomes

    If g_no_output Then Exit Sub
    OutputClass
'    OutputProcs
'    OutputOutcomes
    Exit Sub
    
errHandler:
    LogError Err.Description & " in " & Err.source, EVENT_CATEGORY_UNEXPECTED
    Exit Sub
    Resume  'debug
End Sub

Private Sub ResetIndicators()
    Dim i As Integer
    
    For i = 1 To MAX_INDS
        inds(i).checked = False
        inds(i).also_mark = ""
    Next i
    
    
End Sub

Private Sub ResetProcs()
    numprocs = 0
End Sub
Private Sub ResetOutcomes()
    numoutcomes = 0
End Sub

Private Sub InitGroupsIfNeeded()
    On Error GoTo errHandler
    
'    Static been_here As Boolean
    Dim rs As New Recordset
    Dim sql As String
    Dim inum As Integer
    
    'get indicator radio groups from the database
    sql = "SELECT INDICATOR_NUMBER, RADIO_GROUP FROM INDICATOR_DEFINITION WHERE METHODOLOGY_ID=" & m_pat.meth_id
    dvprint sql
    rs.Open sql, g_cnADO
    
    Do While Not rs.EOF
        inum = rs("INDICATOR_NUMBER")
        If (inum < MAX_INDS) Then
            inds(inum).group = g_dbutil.DBToInteger(rs("RADIO_GROUP"))  'could be null
        End If
        rs.MoveNext
    Loop
    
    rs.Close
    Exit Sub
    
errHandler:
    g_util.ThrowError "InitGroupsIfNeeded"
End Sub

Private Sub LoadFreqTableRow(m_freq As fmapRow, los_high As Single, values As String)
    Dim arr() As String, n As Integer, i As Integer
    
    m_freq.los_high = los_high

    n = g_util.SplitTextOnChar(values, ",", arr(), 0, 0)
    For i = 0 To n - 1
        m_freq.freq(i) = CInt(arr(i))
    Next i
End Sub

Private Sub LoadFreqTable()
                                 ' LOS,   None Q4h Q2h Q1h Q30m
    LoadFreqTableRow m_freq_map(1), 1, "    0,  0,  0,  0,  1"
    LoadFreqTableRow m_freq_map(2), 2, "    0,  0,  0,  1,  2"
    LoadFreqTableRow m_freq_map(3), 4, "    0,  1,  2,  3,  4"
    LoadFreqTableRow m_freq_map(4), 8, "    0,  2,  3,  4,  8"
    LoadFreqTableRow m_freq_map(5), 12, "   0,  2,  4,  6, 12"
    LoadFreqTableRow m_freq_map(6), 9999, " 0,  3,  6, 12, 24"         '24+
    
    'To read this table: if you have at least 6 asessmenets in a 12-hour period, you get Q1h
    '(row 5: LOS=12, column Q1h has a count of 6)
End Sub

Private Function FreqForCount(los_hours As Single, count As Integer) As Frequencies
    Dim i As Integer, j As Integer
    
    For i = 1 To UBound(m_freq_map)                         'go from LOS 1 to 24+
        If (los_hours <= m_freq_map(i).los_high) Then
            For j = Q30M To QNONE Step -1                   'search right to left
                If (count >= m_freq_map(i).freq(j)) Then
                    FreqForCount = j
                    Exit Function
                End If
            Next j
        End If
    Next i
    
    FreqForCount = QNONE
End Function

Private Sub SetSQLConstants()
    'These SQL constants are combined in different ways for searching
    WHERE_ENCOUNTER = " where (encounter_id = " & m_pat.encounter_id & ")"
'    AND_UNIT = " and (unit_id = " & m_pat.unit_id & ")"
    AND_UNIT = ""
'    AND_PULL_RANGE = " and (event_datetime <= " & g_dbutil.SQL_DateTime(m_pat.locary(m_locindex).time_out) & ")"
'    If m_islastloc Then AND_PULL_RANGE = ""
    If Not m_islastloc Then AND_PULL_RANGE = " and event_datetime <= " & g_dbutil.SQL_DateTime(m_pat.locary(m_locindex).time_out)
    If m_islastloc Then AND_PULL_RANGE = " and event_datetime >= " & g_dbutil.SQL_DateTime(m_pat.locary(m_locindex).time_in)
    If m_islastloc And m_locindex = 1 Then AND_PULL_RANGE = ""
    dprint "Data Range=[" & AND_PULL_RANGE & "]"
       
    AND_ARRIVAL = " and (event_datetime between " & g_dbutil.SQL_DateTime(m_pat.unit_arrival) & " and " & g_dbutil.SQL_DateTime(m_pat.pull_finish) & ")"
    AND_OUTCOMES_RANGE = " and (event_datetime between " & g_dbutil.SQL_DateTime(DateAdd("d", -1, m_pat.pull_start)) & " and " & g_dbutil.SQL_DateTime(m_pat.pull_finish) & ")"
    
    'What bucket number does the event belong to?
    BUCKET_CALC = "DateDiff(minute," & g_dbutil.SQL_DateTime(m_pat.pull_start) & ",event_datetime) / " & FREQUENCY_BUCKET_SIZE
End Sub

'Add this to all chart item queries to start the WHERE clause
'This is where the search depth is set
Private Function WhereBase(Optional search_mode As SearchMode = SearchDefault)
    Dim result As String

    Select Case search_mode
    Case Searchpullrange, SearchDefault
        result = WHERE_ENCOUNTER & AND_UNIT & AND_PULL_RANGE    'search within pull range
    Case SearchSinceArrival
        result = WHERE_ENCOUNTER & AND_UNIT & AND_ARRIVAL       'search since arrival to the unit
    Case SearchSinceAdmission
        result = WHERE_ENCOUNTER                                'search since admission to the hospital (any unit)
    Case SearchOutcomesRange
        result = WHERE_ENCOUNTER & AND_UNIT & AND_OUTCOMES_RANGE    'search within pull range+24hrs before
    Case SearchAssessments
        result = WHERE_ENCOUNTER & AND_UNIT     'search within 12 hour range
    End Select
    
    'Since we are looking for one patient in one facility, we don't have to filter by TC_source_ID.
    
    WhereBase = result
End Function

Private Function ValueIsAList(result As String) As Boolean
    ValueIsAList = (InStr(1, result, ",") > 0)                'is there a comma-separated list?
End Function

'Look for any of these fields.  Cat/desc/field = exact match.  Result = like match.
Private Function AndSimpleItemFilter(cat As String, code As String, desc As String, field As String, result_like As String) As String
    Dim result As String
    Dim pos As Integer
    
    If Len(cat) Then result = result & " and category=" & g_dbutil.SQL_String(cat)
    If Len(code) Then
        If Mid$(code, 1, 2) = LIKE_PREFIX Then
            result = result & " and code like " & g_dbutil.SQL_String(Mid$(code, 3, Len(code) - 2) & "%")
        ElseIf ValueIsAList(code) Then
            result = result & " and code in " & FormatCodeList(code)
        Else
            result = result & " and code=" & g_dbutil.SQL_String(code)
        End If
    End If
    If Len(desc) Then
        If InStr(desc, EXACT_MATCH_PREFIX) = 1 Then 'exact match
            result = result & " and description='" & Trim$(Mid$(desc, 3, Len(desc) - 2)) & "'"
        Else
            result = result & " and description like '%" & Trim$(desc) & "%'"
        End If
    End If
    If Len(field) Then
        If InStr(field, EXACT_MATCH_PREFIX) = 1 Then 'exact match
            result = result & " and field_name='" & Trim$(Mid$(field, 3, Len(field) - 2)) & "'"
        Else
            result = result & " and field_name like '%" & Trim$(field) & "%'"
        End If
    End If
    If Len(result_like) Then
        pos = InStr(1, result_like, CHAR_COMMA)
        If pos > 0 Then
            result_like = Mid$(result_like, 1, pos - 1) & "," & Mid$(result_like, pos + 2, Len(result_like) - pos - 1)
        End If

        If InStr(result_like, EXACT_MATCH_PREFIX) = 1 Then 'exact match
            result_like = Mid$(result_like, 3, Len(result_like) - Len(EXACT_MATCH_PREFIX))
            result = result & " and result= " & g_dbutil.SQL_String(result_like)
        ElseIf InStr(result_like, ";") = Len(result_like) Then
            result = result & " and (result like '%; " & Mid$(result_like, 1, Len(result_like) - 1) & "%' or result like '" & result_like & "%' or result='" & Mid$(result_like, 1, Len(result_like) - 1) & "')"
        Else
            result = result & " and result like '%" & Trim$(result_like) & "%'"
        End If
    End If

    AndSimpleItemFilter = result
End Function
Private Function FormatCodeList(code_list As String) As String
    Dim arr() As String
    Dim i As Integer, n As Integer
    Dim result As String

    If Len(code_list) = 0 Then Exit Function
    
    n = g_util.SplitTextOnChar(code_list, ",", arr(), 1, 0)
    
    result = "("
    
    For i = 1 To n
        result = result & g_dbutil.SQL_String(Trim$(arr(i)))
        If i < n Then result = result & ","
    Next i
    
    result = result & ")"
    
    FormatCodeList = result
End Function

'Look for a result that contains one of the words in the list
'The only drawback with this function is that we don't know what it found (for audits)
'Because of this, it is only used in as few places.
Private Function AndResultContains(result_list As String) As String
    Dim arr() As String
    Dim i As Integer, n As Integer
    Dim result As String
    Dim pos As Integer

    If Len(result_list) = 0 Then Exit Function
    
    n = g_util.SplitTextOnChar(result_list, ",", arr(), 1, 0)
    
    result = "and ((1=0)"   ' or (result=this) or (result=that)
    
    For i = 1 To n
        pos = InStr(1, arr(i), CHAR_COMMA)
        If pos > 0 Then
            arr(i) = Mid$(arr(i), 1, pos - 1) & "," & Mid(arr(i), pos + 2, Len(arr(i)) - pos - 1)
        End If
        result = result & " or (result like '" & "%" & Trim$(arr(i)) & "%')"
    Next i
    
    result = result & ")"
    
    AndResultContains = result
End Function

Private Function DescribeSearchDepth(search_mode As SearchMode) As String
    Dim result As String
    
    Select Case search_mode
    Case Searchpullrange, SearchDefault
        'result = "in pull range"
        result = ""                         'be quiet since this is default
    Case SearchSinceArrival
        result = "since arrival to unit"
    Case SearchSinceAdmission
        result = "since admission"
    Case SearchOutcomesRange
        result = "since 24hrs before pull"
    End Select
    
    DescribeSearchDepth = result
End Function

Private Function Describe(cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = Searchpullrange) As String
    Dim sql As String, and_filter As String
    Dim rs As New Recordset
    Dim result As String
    If Not g_debug Then Exit Function           'avoid extra overhead if not making a log

    result = "looking for"
    If Len(cat) Then result = result & " cat='" & cat & "'"
    If Len(code) Then result = result & " code='" & code & "'"
    If Len(desc) Then result = result & " desc='" & desc & "'"
    If Len(field) Then result = result & " field='" & field & "'"
    If Len(result_list) Then result = result & " result contains '" & result_list & "'"

    If ValueIsAList(result_list) Then
        and_filter = AndSimpleItemFilter(cat, code, desc, field, "") & AndResultContains(result_list)
    Else
        and_filter = AndSimpleItemFilter(cat, code, desc, field, result_list)
    End If
    
    sql = "select category,description,field_name,result from chart_item" & WhereBase(search_mode) & and_filter
    rs.Open sql, g_cnADO
    If rs.EOF Then
        result = result & "; not found " & DescribeSearchDepth(search_mode)
    Else
        result = result & "; found"
        'Add info for columns that were not specified and a value was found
        'If (Len(code) = 0 Or ValueIsAList(code)) And Len(rs("code")) Then result = result & " code='" & rs("code") & "'"
        If (Len(cat) = 0) And Len(rs("category")) Then result = result & " cat='" & rs("category") & "'"
        If (Len(desc) = 0) And Len(rs("description")) Then result = result & " desc='" & rs("description") & "'"
        If (Len(field) = 0) And Len(rs("field_name")) Then result = result & " field='" & rs("field_name") & "'"
        'Add the complete result found (we searched for a word or words)
        result = result & " result='" & rs("result") & "'"
        'Are there more results?  Just say how many; we aren't going to list them.
        If (rs.RecordCount = 2) Then
            result = result & " (1 more result)"
        ElseIf (rs.RecordCount > 2) Then
            result = result & " (" & rs.RecordCount - 1 & " more results)"
        End If
    End If
    rs.Close
    
    Describe = result
dvprint "ending describe"

End Function

'Set an indicator for this reason (low level)
Private Sub SetInd(inum As Integer, reason As String)
    If inds(inum).checked And Not g_debug Then Exit Sub       'already set and no log?

    inds(inum).checked = True
    dprint "Set Ind #" & inum & ": " & reason
End Sub

'Clear an indicator for this reason (low level)
Private Sub ClrInd(inum As Integer, reason As String)
    If Not inds(inum).checked And Not g_debug Then Exit Sub   'already clear and no log?
    
    inds(inum).checked = False
    dprint "Clr Ind #" & inum & ": " & reason
End Sub

'Count how many items have the search word in result_like
'This is simple and fast
Private Function CountSimpleResult(cat As String, code As String, desc As String, field As String, result_like As String, Optional search_mode As SearchMode = Searchpullrange, Optional trace As Boolean = True, Optional found_what As String) As Long
    Dim sql As String, and_filter As String
    Dim rs As New Recordset
    Dim count As Long
    Dim pos As Integer

    and_filter = AndSimpleItemFilter(cat, code, desc, field, result_like)
    sql = "select count(*) from chart_item" & WhereBase(search_mode) & and_filter
'    dvprint sql
    'Debug.Print sql
    rs.Open sql, g_cnADO
    count = rs(0)
    rs.Close
    
    'Normally CountSimpleResult will report what it finds; not eveyone wants this so the trace can be turned off
    found_what = Describe(cat, code, desc, field, result_like, search_mode) & IIf(count > 0, "; count=" & count, "")
    If trace Then dvprint found_what
    
    CountSimpleResult = count
End Function
Private Function CountUniqueCodesInList(codelist As String, retlist As String) As Long
    Dim sql As String, and_filter As String
    Dim rs As New Recordset
    Dim count As Long

    retlist = ""
    sql = "select distinct(code) from chart_item" & WhereBase & " and code in (" & codelist & ")"
    rs.Open sql, g_cnADO
    Do While Not rs.EOF
        If Not IsNull(rs(0)) Then
            count = count + 1
            retlist = retlist & rs(0) & " "
        End If
        rs.MoveNext
    Loop
    rs.Close
    
    'Normally CountSimpleResult will report what it finds; not eveyone wants this so the trace can be turned off
    
    CountUniqueCodesInList = count
End Function
Private Function CountCodeHitsInList(codelist As String, retlist As String) As Long
    Dim sql As String, and_filter As String
    Dim rs As New Recordset
    Dim count As Long

    retlist = ""
    sql = "select count(code),code from chart_item" & WhereBase & " and code in (" & codelist & ")"
    sql = sql & " group by code having count(code)>=2"
    rs.Open sql, g_cnADO
    Do While Not rs.EOF
        If Not IsNull(rs(0)) And Not IsNull(rs(1)) Then
            count = count + 1
            retlist = retlist & rs(1) & " = " & rs(0) & " times; "
        End If
        rs.MoveNext
    Loop
    rs.Close
    If Len(retlist) Then retlist = Mid$(retlist, 1, Len(retlist) - 2) & "."
    'Normally CountSimpleResult will report what it finds; not eveyone wants this so the trace can be turned off
    
    CountCodeHitsInList = count
End Function

'Count how many items have a result with one of the words given in the result_list
'Returns a description of what word was found
'This is more powerful than CountSimpleResult but it is slower
Private Function CountResultInList(cat As String, code As String, desc As String, field As String, result_list As String, search_mode As SearchMode, count_mode As CountMode, trace As Boolean, found_what As String) As Integer
    On Error GoTo errHandler
    Dim sql As String, and_filter As String, arr() As String
    Dim rs As New Recordset
    Dim i As Integer, n As Integer, count As Integer
    Dim pos As Long
    
    n = g_util.SplitTextOnChar(result_list, ",", arr(), 1, 0)
    
    'Do not filter by result here; we need all results
    and_filter = AndSimpleItemFilter(cat, code, desc, field, "")
    sql = "select category,description,field_name,result from chart_item" & WhereBase(search_mode) & and_filter
    sql = sql & " and result<>'' and result is not null"
    
    'Debug.Print sql
    rs.Open sql, g_cnADO
    Do While Not rs.EOF
        'Look for each search word in the result (case insensitive)
        For i = 1 To n
            arr(i) = Trim$(arr(i))          'we split on comma so there usually are leading blanks
            
            pos = InStr(1, arr(i), CHAR_COMMA)
            If pos > 0 Then
                arr(i) = Mid$(arr(i), 1, pos - 1) & "," & Mid(arr(i), pos + 2, Len(arr(i)) - pos - 1)
            End If
            
            If InStr(arr(i), EXACT_MATCH_PREFIX) = 1 Then
                arr(i) = Mid$(arr(i), 3, Len(arr(i)) - Len(EXACT_MATCH_PREFIX))
                pos = (rs("result") = arr(i))
            Else
                pos = InStr(1, rs("result"), arr(i), vbTextCompare)  'bad when looking for "RN" in "Non RN"
            End If
            If pos > 0 Then
                found_what = "found '" & arr(i) & "' in cat='" & rs("category") & "' desc='" & rs("description") & "' field='" & rs("field_name") & "' result='" & rs("result") & "'"
                If trace Then dvprint found_what
                count = count + 1
                If count_mode <> CountAll Then Exit Do  'stop counting
                Exit For                                'try next record
            End If
        Next i

        rs.MoveNext
    Loop
    
    If (count > 0) Then
        'We already printed what was found; maybe add how many?
        If trace And (count_mode = CountAll) Then dvprint "found " & count & " total"
    Else
        found_what = Describe(cat, code, desc, field, result_list, search_mode)       'not found
        If trace Then dvprint found_what
    End If
    
    rs.Close
    
    CountResultInList = count
    Exit Function
    
errHandler:
    g_util.ThrowError "CountResultInList"
    Resume   'debug
End Function

Private Function CountResultContains(cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = Searchpullrange, Optional trace As Boolean = True, Optional found_what As String) As Integer
    If ValueIsAList(result_list) Then
        CountResultContains = CountResultInList(cat, code, desc, field, result_list, search_mode, CountAll, trace, found_what)
    Else
        CountResultContains = CountSimpleResult(cat, code, desc, field, result_list, search_mode, trace, found_what)
    End If
End Function

Private Function ResultContains(cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = Searchpullrange, Optional trace As Boolean = True, Optional found_what As String) As Boolean
    'tracing can be turned off and you can ask for what was found instead of having it printed
    If ValueIsAList(result_list) Then
        ResultContains = (CountResultInList(cat, code, desc, field, result_list, search_mode, CountFirst, trace, found_what) > 0)
    Else
        ResultContains = (CountSimpleResult(cat, code, desc, field, result_list, search_mode, trace, found_what) > 0)
    End If
End Function


Private Function CountResultNotInList(cat As String, code As String, desc As String, field As String, result_list As String, search_mode As SearchMode, count_mode As CountMode, trace As Boolean, found_what As String) As Integer
    On Error GoTo errHandler
    Dim sql As String, and_filter As String, arr() As String
    Dim rs As New Recordset
    Dim i As Integer, n As Integer, count As Integer
    Dim found_one As Boolean

    n = g_util.SplitTextOnChar(result_list, ",", arr(), 1, 0)
    
    'Do not filter by result here; we need all results
    and_filter = AndSimpleItemFilter(cat, code, desc, field, "")
    sql = "select code, result from chart_item" & WhereBase(search_mode) & and_filter
    sql = sql & " and result<>'' and result is not null"
    'Debug.Print sql
    rs.Open sql, g_cnADO
    
    Do While Not rs.EOF
        'Look for each search word in the result (case insensitive)
        found_one = False
        
        For i = 1 To n
            arr(i) = Trim$(arr(i))          'we split on comma so there usually are leading blanks
            If InStr(1, rs("result"), arr(i), vbTextCompare) > 0 Then
                found_what = "found '" & arr(i) & "' in code='" & rs("code") & "' result='" & rs("result") & "' -- ignore this"
                If trace Then dvprint found_what
                found_one = True
                Exit For
            End If
        Next i

        If (Not found_one) Then
            'none of the words were found - good!
            found_what = Describe(cat, code, desc, field, rs("result"), search_mode) & " -- does not contain '" & result_list & "'"
            If trace Then dvprint found_what
            count = count + 1
            If count_mode <> CountAll Then Exit Do
        End If

        rs.MoveNext
    Loop
    
    If (count > 0) Then
        'We already printed what was found; maybe show the total?
        If trace And (count_mode = CountAll) Then dvprint "found " & count & " total"
    ElseIf (rs.RecordCount > 0) Then
        'We already printed what we ignored
    Else
        'Print what did not exist
        found_what = Describe(cat, code, desc, field, "", search_mode)      'not found
        If trace Then dvprint found_what
    End If
    
    rs.Close
    
    CountResultNotInList = count
    Exit Function
    
errHandler:
    g_util.ThrowError "CountResultNotInList"
    Resume   'debug
End Function

Private Function CountResultDoesNotContain(cat As String, code As String, desc As String, field As String, result_list As String, found_what As String, Optional search_mode As SearchMode = Searchpullrange, Optional trace As Boolean = True) As Integer
    CountResultDoesNotContain = CountResultNotInList(cat, code, desc, field, result_list, search_mode, CountAll, trace, found_what)
End Function

Private Function ResultDoesNotContain(cat As String, code As String, desc As String, field As String, result_list As String, found_what As String, Optional search_mode As SearchMode = Searchpullrange, Optional trace As Boolean = True) As Boolean
    ResultDoesNotContain = (CountResultNotInList(cat, code, desc, field, result_list, search_mode, False, trace, found_what) > 0)
End Function

'Set the indicator if the result contains one of the words in the result_list
Private Sub SetIndIfResultContains(inum As Integer, cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = Searchpullrange)
    Dim found_what As String
    'avoid more queries if the indicator is already set
    If inds(inum).checked Then Exit Sub
    
    'Turn trace off for ResultContains() so the result can be placed on the "SetInd#" line below
    If ResultContains(cat, code, desc, field, result_list, search_mode, False, found_what) Then
        SetInd inum, found_what             'echo here
    Else
        dvprint found_what                  'and here
    End If
End Sub

'Set the indicator if the result does not contain any of the words in result_list
Private Sub SetIndIfResultDoesNotContain(inum As Integer, cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = Searchpullrange)
    Dim found_what As String
    'avoid more queries if the indicator is already set
    If inds(inum).checked Then Exit Sub
    
    'Turn trace off for ResultDoesNotContain() and echo what was set below with SetInd
    If ResultDoesNotContain(cat, code, desc, field, result_list, found_what, search_mode, False) Then
        SetInd inum, found_what             'echo here
    Else
        dvprint found_what                  'and here
    End If
End Sub

'Clear the indicator if the result contains on of the words in the result_list
Private Sub ClrIndIfResultContains(inum As Integer, cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = Searchpullrange)
    Dim found_what As String
    'avoid more queries if the indicator is already clear
    If Not inds(inum).checked Then Exit Sub
    
    'Turn trace off for ResultContains() and echo what was set below with SetInd
    If ResultContains(cat, code, desc, field, result_list, search_mode, False, found_what) Then
        ClrInd inum, found_what             'echo here
    Else
        dvprint found_what                  'and here
    End If
End Sub


'These slightly smaller functions are meant for places where you aren't really looking for a chart result
Private Function Exists(cat As String, Optional code As String, Optional desc As String, Optional field As String, Optional result_list As String, Optional search_mode As SearchMode = Searchpullrange, Optional trace As Boolean = True, Optional found_what As String) As Boolean
    Exists = (CountResultContains(cat, code, desc, field, result_list, search_mode, trace, found_what) > 0)
End Function

Private Sub SetIndIfFound(inum As Integer, cat As String, Optional code As String, Optional desc As String, Optional field As String, Optional result_list As String, Optional search_mode As SearchMode = Searchpullrange)
    SetIndIfResultContains inum, cat, code, desc, field, result_list, search_mode
End Sub

Private Sub ClrIndIfFound(inum As Integer, cat As String, Optional code As String, Optional desc As String, Optional field As String, Optional result_list As String, Optional search_mode As SearchMode = Searchpullrange)
    ClrIndIfResultContains inum, cat, code, desc, field, result_list, search_mode
End Sub


'Get the max/total value from a result (usually in the middle of the text)
Private Function GetIntValue(get_mode As GetValueMode, cat As String, code As String, desc As String, field As String, result_like As String, Optional search_mode As SearchMode = Searchpullrange) As Integer
    Dim sql As String, and_filter As String, arr() As String, msg As String
    Dim rs As New Recordset
    Dim result As Integer, i As Integer, n As Integer, value As Integer
    Dim found_one As Boolean

    and_filter = AndSimpleItemFilter(cat, code, desc, field, result_like)
    sql = "select result from chart_item" & WhereBase(search_mode) & and_filter
    'Debug.Print sql
    rs.Open sql, g_cnADO
    
    'Look for a number in the result
    
    Do While Not rs.EOF
        n = g_util.SplitTextOnChar(rs(0), " ", arr(), 1, 0)
        For i = 1 To n
            'Look for 1st character numeric; IsNumeric("60min") will fail so look at 1st char only
            If IsNumeric(Left$(arr(i), 1)) Then
                value = val(arr(i))                         'Use Val; CInt will error on "60min"
                Select Case get_mode
                Case GetMax
                    result = g_util.Max(value, result)      'max
                Case GetTotal
                    result = result + value                 'total
                Case GetLast
                    result = value                          'last
                End Select
                
                'print what we are searching for (the first time)
                If Not found_one Then
                    dvprint Describe(cat, code, desc, field, result_like, search_mode)
                End If
                found_one = True
                'print each value found
                dvprint "  found numeric value " & result
                'Keep going in case there are more
            End If
        Next i
        rs.MoveNext
    Loop
    
    rs.Close
    
    If Not found_one Then
        'show what was not found
        If g_verbose Then dprint Describe(cat, code, desc, field, result_like, search_mode)
    End If
    
    GetIntValue = result
End Function

Private Function GetMaxValue(cat As String, code As String, desc As String, field As String, result_like As String, Optional search_mode As SearchMode = Searchpullrange) As Integer
    GetMaxValue = GetIntValue(GetMax, cat, code, desc, field, result_like, search_mode)
End Function

Private Function GetTotalValue(cat As String, code As String, desc As String, field As String, result_like As String, Optional search_mode As SearchMode = Searchpullrange) As Integer
    GetTotalValue = GetIntValue(GetTotal, cat, code, desc, field, result_like, search_mode)
End Function

'Get a result; returns True if found with return_result set
Private Function GetResult(cat As String, code As String, desc As String, field As String, return_result As String, Optional search_mode As SearchMode = Searchpullrange) As Boolean
    Dim rs As New Recordset
    Dim sql As String

    sql = "select result from chart_item" & WhereBase(search_mode) & AndSimpleItemFilter(cat, code, desc, field, "")
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        return_result = rs(0) & ""
    Else
        return_result = ""
    End If
    rs.Close

    dvprint Describe(cat, code, desc, field, "", search_mode)
    GetResult = (Len(return_result) > 0)
End Function
Private Function GetResultOfLatest(cat As String, code As String, desc As String, field As String, return_result As String, Optional search_mode As SearchMode = Searchpullrange) As Boolean
    Dim rs As New Recordset
    Dim sql As String
    Dim evdt As Date
    Dim done As Boolean

    sql = "select event_datetime,result from chart_item" & WhereBase(search_mode) & AndSimpleItemFilter(cat, code, desc, field, "")
    sql = sql & " order by event_datetime desc"
    rs.Open sql, g_cnADO
    
    return_result = ""
    done = False
    
    If Not rs.EOF Then evdt = rs(0)
    Do While Not rs.EOF And Not done
        If evdt = rs(0) Then
            return_result = return_result & rs(1) & ","
        Else
            done = True
        End If
        rs.MoveNext
    Loop
    rs.Close

    dvprint Describe(cat, code, desc, field, "", search_mode)
    GetResultOfLatest = (Len(return_result) > 0)

End Function
Private Sub CheckExploding()
    On Error GoTo errHandler
    Dim found_what As String


    dvprint "-----------------------"
    dvprint " Exploding triggers"
    dvprint "-----------------------"
    
    If ResultContains("", "", "", "", "TR Level 1,TR Level Red", , True, found_what) Then
        inds(1).checked = True
        inds(4).checked = True
        inds(11).checked = True
        inds(13).checked = True
        inds(14).checked = True
        inds(15).checked = True
        inds(16).checked = True
        dprint "TR Level 1/Red: Setting indicators 1,4,11,13,14,15,16."
    End If
    If ResultContains("", "", "", "", "TR Level 2,TR Level Yellow", , True, found_what) Then
        inds(1).checked = True
        inds(4).checked = True
        inds(11).checked = True
        inds(13).checked = True
        inds(14).checked = True
        inds(15).checked = True
        inds(16).checked = True
        dprint "TR Level 2/Yellow: Setting indicators 1,4,11,13,14,15,16."
    End If
    If ResultContains("", "", "", "", "Treatments in progress include cardiopulmonary resuscitation", , True, found_what) Then
        inds(1).checked = True
        inds(4).checked = True
        inds(11).checked = True
        inds(13).checked = True
        inds(14).checked = True
        inds(15).checked = True
        inds(16).checked = True
        dprint "Cardio Resusc: Setting indicators 1,4,11,13,14,15,16."
    End If
    If ResultContains("", "", "", "", "Refer to Cardiopulmonary Resuscitation Emergency Response Team Report", , True, found_what) Then
        inds(1).checked = True
        inds(4).checked = True
        inds(11).checked = True
        inds(13).checked = True
        inds(14).checked = True
        inds(15).checked = True
        inds(16).checked = True
        dprint "Cardio Resusc Emerg Response Team Report: Setting indicators 1,4,11,13,14,15,16."
    End If
    If ResultContains("", "", "", "", "Patient arrived intubated/apneic/pulseless or unresponsive", , True, found_what) Then
        inds(1).checked = True
        inds(4).checked = True
        inds(11).checked = True
        inds(13).checked = True
        inds(14).checked = True
        inds(16).checked = True
        dprint "Arrived intubated/apneic/pulseless/unresp: Setting indicators 1,4,11,13,14,16."
    End If
    If ResultContains("", "", "", "", "Patient requires immediate life saving intervention", , True, found_what) Then
        inds(1).checked = True
        inds(4).checked = True
        inds(11).checked = True
        inds(13).checked = True
        inds(14).checked = True
        inds(15).checked = True
        inds(16).checked = True
        dprint "Immediate life saving interventions: Setting indicators 1,4,11,13,14,15,16."
    End If
    If ResultContains("NURSING ASSESSMENT: SEXUAL ASSAULT", "", "", "", "", , True, found_what) Then
        inds(1).checked = True
        inds(3).checked = True
        inds(9).checked = True
        inds(14).checked = True
        inds(15).checked = True
        inds(17).checked = True
        dprint "Sexual Assault: Setting indicators 1,3,9,14,15,17."
    End If
    If ResultContains("", "", "", "", "Refer to CDM report Sexual Assault Exam Record", , True, found_what) Then
        inds(1).checked = True
        inds(3).checked = True
        inds(9).checked = True
        inds(14).checked = True
        inds(15).checked = True
        inds(17).checked = True
        dprint "CDM report Sexual Assault Exam Record: Setting indicators 1,3,9,14,15,17."
    End If
    If ResultContains("NURSING PROCEDURE: CODE RECORDER", "", "", "", "", , True, found_what) Then
        inds(4).checked = True
        inds(11).checked = True
        inds(13).checked = True
        inds(14).checked = True
        inds(16).checked = True
        dprint "Code Recorder: Setting indicators 4,11,13,14,16."
    End If
    
    Exit Sub
errHandler:
    g_util.ThrowError "CheckExploding"
    Resume  'debug
End Sub

Private Sub Check_1()
    On Error GoTo errHandler


    dvprint "-----------------------"
    dvprint "ED Visit 1. Initial Assessment > 20 min"
    dvprint "-----------------------"
    
    If inds(1).checked Then Exit Sub
    SetIndIfResultContains 1, "", "", "", "", "Initial Assessment 20 minutes or greater"
    SetIndIfResultContains 1, "", "", "", "", "Treatments in progress include cardiopulmonary resuscitation"
    SetIndIfResultContains 1, "", "", "", "", "Focused neuro assessment findings include patient unresponsive"
    SetIndIfResultContains 1, "", "", "", "", "Focused neuro assessment findings include patient responsive to painful stimuli"
    SetIndIfResultContains 1, "", "", "", "", "Complex assessment performed"
   
    If inds(1).checked Then Exit Sub
    SetIndIfAllResults 1, "", "", "", "", "Nursing home,Contacted to obtain patient history"
    SetIndIfAllResults 1, "", "", "", "", "Nursing home,Contacted to obtain patient medications"

    If inds(1).checked Then Exit Sub
    SetIndIfResultsConcurrent 1, "Multiple Trauma", "ESI Level 1"
    SetIndIfResultsConcurrent 1, "Multiple Trauma", "ESI Level 2"
    
    
    Exit Sub
errHandler:
    g_util.ThrowError "Check_1"
    Resume  'debug
End Sub
Private Sub SetADLCompleteWhenAge(agecond As String)  ' agecond example: "<=3"
    Dim sql As String
    Dim rs As New Recordset

'
'select case when round(age_at_admission,0,1) <=55 then 1 else 0 end from encounter where encounter_id=6990

    sql = "select case when round(age_at_admission,0,1) " & agecond & " then 1 else 0 end from encounter " & WHERE_ENCOUNTER
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        If rs(0) = 1 Then
            SetInd 4, "Age < 4 years"
        End If
    End If
    rs.Close
    
End Sub
Private Sub SetAgeFromChart()
    Dim rs As New Recordset
    Dim sql As String
    Dim pos As Integer, n As Integer
    Dim agestr  As String

    sql = "select result from chart_item" & WhereBase(SearchSinceAdmission) & AndSimpleItemFilter("", "", "", EXACT_MATCH_PREFIX & "Age", "")
    sql = sql & " order by event_datetime desc"
    rs.Open sql, g_cnADO
    
    If Not rs.EOF Then
        If Not IsNull(rs(0)) Then
            agestr = rs(0)
            If IsNumeric(agestr) Then
                m_pat.age = agestr
            End If
            pos = InStr(1, agestr, "D")
            If pos > 0 Then 'days
                n = Mid$(agestr, 1, pos - 1)
                m_pat.age = n \ 365
            End If
            pos = InStr(1, agestr, "W")
            If pos > 0 Then 'weeks
                n = Mid$(agestr, 1, pos - 1)
                m_pat.age = n \ 52
            End If
            pos = InStr(1, agestr, "M")
            If pos > 0 Then 'months
                n = Mid$(agestr, 1, pos - 1)
                m_pat.age = n \ 12
            End If
        End If
    End If
            
    rs.Close

End Sub
Private Sub Check_234()
    Dim orient As Integer
    On Error GoTo errHandler


    dvprint "-----------------------"
    dvprint "ED Visit 2. ADL - Self Care"
    dvprint "ED Visit 3. ADL - Assist"
    dvprint "ED Visit 4. ADL - Extended"
    dvprint "-----------------------"
    
    If inds(4).checked Then Exit Sub
    
    If m_pat.age = 0 Then
        SetAgeFromChart
    End If
    If m_pat.age = 0 Then
        SetInd 2, "DOB and Age not available: defaulting to self care."
    ElseIf m_pat.age <= 4 Then
        SetInd 4, "Age <= 4 years"
    End If
    
'    SetADLCompleteWhenAge ("<4")
    
    If inds(4).checked Then Exit Sub
    

    SetIndIfResultContains 4, "", "", "", "", "Treatments in progress include spinal precautions"
    SetIndIfResultContains 4, "", "", "", "", "Treatments in progress include patient intubated"
    SetIndIfResultContains 4, "", "", "", "", "Treatments in progress include ventilations assisted with"
    If inds(4).checked Then Exit Sub
    SetIndIfResultContains 4, "", "", "", "", "Treatments in progress include cardiopulmonary resuscitation"
    SetIndIfResultContains 4, "", "", "", "", "immobilized with long backboard"
    SetIndIfResultContains 4, "", "", "", "", "immobilized with head immobilization device"
    If inds(4).checked Then Exit Sub
    
    SetIndIfResultContains 4, "", "", "", "", "responsive to painful stimuli;"
    SetIndIfResultContains 4, "", "", "", "", "unresponsive;"
    SetIndIfResultContains 4, "", "", "", "", "unconscious;"
    If inds(4).checked Then Exit Sub
    
'FIELD: the indication for urinary catheter
    If inds(4).checked Then Exit Sub
    
    SetIndIfResultContains 4, "", "", "oxygen therapy", "", "via ventilator"
    SetIndIfResultContains 4, "", "", "oxygen therapy", "", "via BIPAP"
    SetIndIfResultContains 4, "", "", "oxygen therapy", "", "via CPAP"
    SetIndIfResultContains 4, "", "", "oxygen therapy", "", "via ambu bag"
    If inds(4).checked Then Exit Sub
    
    SetIndIfResultContains 4, "", "", "", "Glasgow Coma", EXACT_MATCH_PREFIX & "8"
    SetIndIfResultContains 4, "", "", "", "Glasgow Coma", EXACT_MATCH_PREFIX & "7"
    SetIndIfResultContains 4, "", "", "", "Glasgow Coma", EXACT_MATCH_PREFIX & "6"
    SetIndIfResultContains 4, "", "", "", "Glasgow Coma", EXACT_MATCH_PREFIX & "5"
    SetIndIfResultContains 4, "", "", "", "Glasgow Coma", EXACT_MATCH_PREFIX & "4"
    SetIndIfResultContains 4, "", "", "", "Glasgow Coma", EXACT_MATCH_PREFIX & "3"
    If inds(4).checked Then Exit Sub
    
    
    If m_pat.age >= 8 Then
    SetIndIfResultContains 4, "", "", "", "FOUR Score Assessment- Eye Response", "(0)"
    SetIndIfResultContains 4, "", "", "", "FOUR Score Assessment- Eye Response", "(1)"
    SetIndIfResultContains 4, "", "", "", "FOUR Score Assessment- Eye Response", "(2)"
    If inds(4).checked Then Exit Sub
    
    SetIndIfResultContains 4, "", "", "", "FOUR Score Assessment- Motor Response", "(0)"
    SetIndIfResultContains 4, "", "", "", "FOUR Score Assessment- Motor Response", "(1)"
    SetIndIfResultContains 4, "", "", "", "FOUR Score Assessment- Motor Response", "(2)"
    If inds(4).checked Then Exit Sub
    SetIndIfResultContains 4, "", "", "", "FOUR Score Assessment- Brainstem Reflexes", "(0)"
    SetIndIfResultContains 4, "", "", "", "FOUR Score Assessment- Brainstem Reflexes", "(1)"
    SetIndIfResultContains 4, "", "", "", "FOUR Score Assessment- Brainstem Reflexes", "(2)"
    SetIndIfResultContains 4, "", "", "", "FOUR Score Assessment- Brainstem Reflexes", "(3)"
    If inds(4).checked Then Exit Sub
    SetIndIfResultContains 4, "", "", "", "FOUR Score Assessment- Respiration", "(0)"
    SetIndIfResultContains 4, "", "", "", "FOUR Score Assessment- Respiration", "(1)"
    SetIndIfResultContains 4, "", "", "", "FOUR Score Assessment- Respiration", "(2)"
    SetIndIfResultContains 4, "", "", "", "FOUR Score Assessment- Respiration", "(3)"
    End If
    
    If inds(4).checked Then Exit Sub
    
    
    SetIndIfResultContains 4, "", "", "", "GCS Total", EXACT_MATCH_PREFIX & "8"
    SetIndIfResultContains 4, "", "", "", "GCS Total", EXACT_MATCH_PREFIX & "7"
    SetIndIfResultContains 4, "", "", "", "GCS Total", EXACT_MATCH_PREFIX & "6"
    SetIndIfResultContains 4, "", "", "", "GCS Total", EXACT_MATCH_PREFIX & "5"
    SetIndIfResultContains 4, "", "", "", "GCS Total", EXACT_MATCH_PREFIX & "4"
    SetIndIfResultContains 4, "", "", "", "GCS Total", EXACT_MATCH_PREFIX & "3"
    If inds(4).checked Then Exit Sub
    
    SetIndIfResultContains 4, "", "", "", "", "Inspection findings include shortening of leg"
    SetIndIfResultContains 4, "", "", "", "", "Inspection findings include external rotation of hip"
    If inds(4).checked Then Exit Sub
    
    SetIndIfResultContains 4, "NURSING ASSESSMENT: PRIMARY SURVEY TRAUMA", "", "AIRWAY AND C-SPINE", "", "Spine immobilized;"
    If inds(4).checked Then Exit Sub
    
    SetIndIfResultContains 4, "NURSING ASSESSMENT: PRIMARY SURVEY TRAUMA", "", "DISABILITY", "", "Movement; absent to the"
    SetIndIfResultContains 4, "NURSING ASSESSMENT: PRIMARY SURVEY TRAUMA", "", "DISABILITY", "", "Movement; decreased to the"
    If inds(4).checked Then Exit Sub
    SetIndIfResultContains 4, "NURSING ASSESSMENT: PRIMARY SURVEY TRAUMA", "", "DISABILITY", "", "decreased to the left"
    SetIndIfResultContains 4, "NURSING ASSESSMENT: PRIMARY SURVEY TRAUMA", "", "DISABILITY", "", "absent to the left"
    If inds(4).checked Then Exit Sub
    SetIndIfResultContains 4, "NURSING ASSESSMENT: PRIMARY SURVEY TRAUMA", "", "DISABILITY", "", "decreased to the right"
    SetIndIfResultContains 4, "NURSING ASSESSMENT: PRIMARY SURVEY TRAUMA", "", "DISABILITY", "", "absent to the right"
    If inds(4).checked Then Exit Sub
'    SetIndIfResultContains 4, "NURSING ASSESSMENT: PRIMARY SURVEY TRAUMA", "", "DISABILITY", "", "decreased to the left lower extremity"
'    SetIndIfResultContains 4, "NURSING ASSESSMENT: PRIMARY SURVEY TRAUMA", "", "DISABILITY", "", "absent to the left lower extremity"
'    If inds(4).checked Then Exit Sub
'    SetIndIfResultContains 4, "NURSING ASSESSMENT: PRIMARY SURVEY TRAUMA", "", "DISABILITY", "", "decreased to the right lower extremity"
'    SetIndIfResultContains 4, "NURSING ASSESSMENT: PRIMARY SURVEY TRAUMA", "", "DISABILITY", "", "absent to the right lower extremity"
'    If inds(4).checked Then Exit Sub
    
    
    SetIndIfResultContains 4, "", "", "", "", "Patient in spinal immobilization on arrival"
    SetIndIfResultContains 4, "", "", "", "", "Patient placed on long board"
    If inds(4).checked Then Exit Sub
    SetIndIfResultContains 4, "", "", "", "", "Gardner Wells tongs indicated for"
    SetIndIfResultContains 4, "", "", "", "", "Gardner Wells tongs applied by"
    If inds(4).checked Then Exit Sub
    
  
    SetIndIfResultContains 4, "NURSING PROCEDURE: GARDNER WELLS TONGS", "", "FOLLOW-UP", "", "After procedure" & CHAR_COMMA & " airway patent"
    SetIndIfResultContains 4, "NURSING PROCEDURE: GARDNER WELLS TONGS", "", "FOLLOW-UP", "", "After procedure" & CHAR_COMMA & " movement to extremities"
    If inds(4).checked Then Exit Sub
    SetIndIfResultContains 4, "NURSING PROCEDURE: GARDNER WELLS TONGS", "", "FOLLOW-UP", "", "After procedure" & CHAR_COMMA & " sensation to extremities"
    SetIndIfResultContains 4, "NURSING PROCEDURE: GARDNER WELLS TONGS", "", "FOLLOW-UP", "", "After procedure" & CHAR_COMMA & " airway not patent"
    If inds(4).checked Then Exit Sub
    SetIndIfResultContains 4, "NURSING PROCEDURE: GARDNER WELLS TONGS", "", "FOLLOW-UP", "", "After procedure" & CHAR_COMMA & " no movement to extremities"
    SetIndIfResultContains 4, "NURSING PROCEDURE: GARDNER WELLS TONGS", "", "FOLLOW-UP", "", "After procedure" & CHAR_COMMA & " sensation decreased to extremities"
    If inds(4).checked Then Exit Sub
    
    SetIndIfResultContains 4, "", "", "ICP MONITORING", "", ""
    If inds(4).checked Then Exit Sub
    SetIndIfResultContains 4, "NURSING PROCEDURE: ICP MONITORING", "", "FOLLOW-UP", "", ""
    SetIndIfResultContains 4, "NURSING PROCEDURE: INTUBATION", "", "INTUBATION", "", ""
    SetIndIfResultContains 4, "NURSING PROCEDURE: INTUBATION", "", "FOLLOW-UP", "", ""
    If inds(4).checked Then Exit Sub
    
    
    SetIndIfResultContains 4, "", "", "", "", "Mast suit applied in field"
    SetIndIfResultContains 4, "", "", "", "", "Mast suit applied in emergency department"
    SetIndIfResultContains 4, "", "", "", "", "Long board removed"
    If inds(4).checked Then Exit Sub

    SetIndIfResultContains 4, "NURSING Procedure: SURGICAL AIRWAY", "", "", "", "Tracheostomy;"
    SetIndIfResultContains 4, "NURSING Procedure: SURGICAL AIRWAY", "", "", "", "Cricothyrotomy;"
    If inds(4).checked Then Exit Sub
    
    SetIndIfResultContains 4, "NURSING Procedure: THORACOTOMY", "", "", "", ""
    SetIndIfResultContains 4, "NURSING Procedure: THROMBOLYTIC", "", "", "", ""
    If inds(4).checked Then Exit Sub
    
    SetIndIfResultContains 4, "NURSING Procedure: VENTILATOR", "", "Ventilator", "", ""
    SetIndIfResultContains 4, "NURSING PROCEDURE: VENTRICULOSTOMY", "", "VENTRICULOSTOMY", "", ""
    If inds(4).checked Then Exit Sub
    
    SetIndIfResultContains 4, "NURSING PROCEDURE: VENTRICULOSTOMY", "", "FOLLOW-UP", "", "After procedure, occlusive sterile dressing applied"
    SetIndIfResultContains 4, "NURSING PROCEDURE: VENTRICULOSTOMY", "", "FOLLOW-UP", "", "After procedure, head of bed elevated to 30 degrees"
    SetIndIfResultContains 4, "NURSING PROCEDURE: VENTRICULOSTOMY", "", "FOLLOW-UP", "", "After procedure, patient in neutral alignment"
    If inds(4).checked Then Exit Sub
    
    SetIndIfResultContains 4, "INTUBATION", "", "", "", ""
    If inds(4).checked Then Exit Sub
    
    SetIndIfResultContains 4, "", "", "", "O2 Sat", "On BIPAP"
    SetIndIfResultContains 4, "", "", "", "O2 Sat", "On Mech Ventilation"
    SetIndIfResultContains 4, "", "", "", "O2 Sat", "On CPAP"
    If inds(4).checked Then Exit Sub
    
    SetIndIfAllResults 4, "", "", "", "", "Patient assisted to cart,C-Collar/Spinal Immobilization"
    
    If inds(4).checked Then Exit Sub
    If inds(3).checked Then Exit Sub
    
    If m_pat.age <> 0 And m_pat.age >= 4 And m_pat.age < 8 Then
        SetInd 3, "Age 4-7 years"
        If inds(3).checked Then Exit Sub
    End If
    
'3
    SetIndIfResultContains 3, "", "", "", "the indication for urinary catheter", "immobilization"
    SetIndIfResultContains 3, "", "", "", "the indication for urinary catheter", "Sedated/Ventilated/Impaired Level"

    SetIndIfResultContains 3, "", "", "", "", "Treatments in progress include splint"
    SetIndIfResultContains 3, "", "", "", "", "Treatments in progress include patient on cardiac monitor"
    SetIndIfResultContains 3, "", "", "", "", "Treatments in progress include Oxygen"
    SetIndIfResultContains 3, "", "", "", "", "C-Collar/Spinal Immobilization"
    If inds(3).checked Then Exit Sub
    
    'SetIndIfAllResults 3, "", "", "", "", "Patient,confused"
    SetIndIfAllResults 3, "", "", "", "", "Patient arrives,via personal wheelchair"
    SetIndIfAllResults 3, "", "", "", "", "Patient arrives,via stretcher"
    SetIndIfAllResults 3, "", "", "", "", "Patient arrives,carried"
    If inds(3).checked Then Exit Sub
    
    SetIndIfResultContains 3, "", "", "", "", "Unsteady gait;"
    If inds(3).checked Then Exit Sub
    
   
    SetIndIfResultContains 3, "", "", "", "", "disoriented;"
    SetIndIfResultContains 3, "", "", "", "", "confused;"
    SetIndIfResultContains 3, "", "", "", "", "with contractures;"
    If inds(3).checked Then Exit Sub
    orient = 0
    If ResultContains("", "", "", "", "oriented to person;") Then orient = orient + 1
    If ResultContains("", "", "", "", "oriented to place;") Then orient = orient + 1
    If ResultContains("", "", "", "", "oriented to time;") Then orient = orient + 1
    If orient = 2 Then SetInd 3, "Two of 'oriented to'."
    
    
    SetIndIfResultContains 3, "", "", "", "", "Suicide precautions maintained"
    SetIndIfResultContains 3, "", "", "", "", "Seizure precautions:"
    SetIndIfResultContains 3, "", "", "", "", "One to one sitter utilized"
    SetIndIfResultContains 3, "", "", "", "", "Safety checks every 15 minutes"
    SetIndIfResultContains 3, "", "", "", "", "Safety reassessment every 15 minutes"
    If inds(3).checked Then Exit Sub
    

    
    SetIndIfResultContains 3, "", "", "", "", "Continuous bladder irrigation initiated"
    SetIndIfResultContains 3, "", "", "", "", "Simple foley inserted"
    SetIndIfResultContains 3, "", "", "", "", "Foley catheter with Criticore inserted"
    If inds(3).checked Then Exit Sub
    SetIndIfResultContains 3, "", "", "", "", "Complex foley inserted"
    SetIndIfResultContains 3, "", "", "", "", "Urine collected from suprapubic catheter"
    SetIndIfResultContains 3, "", "", "", "", "Urinary catheter present draining"
    SetIndIfResultContains 3, "", "", "", "", "Associated with indwelling urinary catheter present"
    If inds(3).checked Then Exit Sub
    
    
    SetIndIfResultContains 3, "NURSING ASSESSMENT: BACK", "", "BACK", "", "Weakness to;"
    If inds(3).checked Then Exit Sub

    SetIndIfAllResults 3, "", "", "", "", "Incontinent,of bowel"
    SetIndIfAllResults 3, "", "", "", "", "Incontinent,of bladder"
    SetIndIfAllResults 3, "", "", "", "", "Incontinent,of both bowel and bladder"
    If inds(3).checked Then Exit Sub
    SetIndIfResultContains 3, "", "", "", "", "Heart rhythm;"
    SetIndIfResultContains 3, "", "", "", "", "Heart rhythm normal sinus"
    SetIndIfAllResults 3, "", "", "", "", "Heart rhythm,atrial fibrillation with controlled ventricular response"
    SetIndIfAllResults 3, "", "", "", "", "Heart rhythm,atrial fibrillation with rapid ventricular response"
    If inds(3).checked Then Exit Sub
    SetIndIfAllResults 3, "", "", "", "", "Heart rhythm,atrial flutter"
    SetIndIfAllResults 3, "", "", "", "", "Heart rhythm,atrial tachycardia"
    SetIndIfAllResults 3, "", "", "", "", "Heart rhythm,paced"
    SetIndIfAllResults 3, "", "", "", "", "Heart rhythm,paroxysmal supraventricular tachycardia"
    If inds(3).checked Then Exit Sub
    SetIndIfAllResults 3, "", "", "", "", "Heart rhythm,sinus arrhythmia"
    SetIndIfAllResults 3, "", "", "", "", "Heart rhythm,sinus bradycardia"
    SetIndIfAllResults 3, "", "", "", "", "Heart rhythm,sinus tachycardia"
    SetIndIfAllResults 3, "", "", "", "", "Heart rhythm,first degree AV Block"
    SetIndIfAllResults 3, "", "", "", "", "Heart rhythm,second degree AV Block Type I"
    If inds(3).checked Then Exit Sub
    SetIndIfAllResults 3, "", "", "", "", "Heart rhythm,second degree AV Block Type II"
    SetIndIfAllResults 3, "", "", "", "", "Heart rhythm,third degree AV Block"
    SetIndIfAllResults 3, "", "", "", "", "Heart rhythm,junctional"
    
    SetIndIfResultContains 3, "", "", "", "", "Cardiac monitoring indicated for"
    If inds(3).checked Then Exit Sub
    
    SetIndIfAllResults 3, "", "", "", "", "Patient placed on cardiac monitor, showing normal sinus rhythm"
    SetIndIfAllResults 3, "", "", "", "", "Patient placed on cardiac monitor, showing atrial fibrillation"
    SetIndIfAllResults 3, "", "", "", "", "Patient placed on cardiac monitor, showing atrial fibrillation with controlled ventricular rate"
    If inds(3).checked Then Exit Sub
    SetIndIfAllResults 3, "", "", "", "", "Patient placed on cardiac monitor, showing atrial fibrillation with rapid ventricular response"
    SetIndIfAllResults 3, "", "", "", "", "Patient placed on cardiac monitor, showing atrial flutter"
    SetIndIfAllResults 3, "", "", "", "", "Patient placed on cardiac monitor, showing atrial tachycardia"
    If inds(3).checked Then Exit Sub
    SetIndIfAllResults 3, "", "", "", "", "Patient placed on cardiac monitor, showing paced rhythm"
    SetIndIfAllResults 3, "", "", "", "", "Patient placed on cardiac monitor, showing paroxysmal supraventricular tachycardia"
    SetIndIfAllResults 3, "", "", "", "", "Patient placed on cardiac monitor, showing sinus arrhythmia"
    If inds(3).checked Then Exit Sub
    SetIndIfAllResults 3, "", "", "", "", "Patient placed on cardiac monitor, showing sinus bradycardia"
    SetIndIfAllResults 3, "", "", "", "", "Patient placed on cardiac monitor, showing sinus tachycardia"
    SetIndIfAllResults 3, "", "", "", "", "Patient placed on cardiac monitor, showing first degree AV Block"
    If inds(3).checked Then Exit Sub
    SetIndIfAllResults 3, "", "", "", "", "Patient placed on cardiac monitor, showing second degree AV Block Type I"
    SetIndIfAllResults 3, "", "", "", "", "Patient placed on cardiac monitor, showing second degree AV Block Type II"
    SetIndIfAllResults 3, "", "", "", "", "Patient placed on cardiac monitor, showing third degree AV Block"
    If inds(3).checked Then Exit Sub
    SetIndIfAllResults 3, "", "", "", "", "Patient placed on cardiac monitor, showing junctional"

    
    SetIndIfResultContains 3, "", "", "oxygen therapy", "", "via nasal cannula"
    SetIndIfResultContains 3, "", "", "oxygen therapy", "", "via venti-mask"
    SetIndIfResultContains 3, "", "", "oxygen therapy", "", "via partial rebreather mask"
    If inds(3).checked Then Exit Sub
    SetIndIfResultContains 3, "", "", "oxygen therapy", "", "via non-rebreather mask"
    SetIndIfResultContains 3, "", "", "oxygen therapy", "", "via blow-By"
    SetIndIfResultContains 3, "", "", "oxygen therapy", "", "via simple mask"
    If inds(3).checked Then Exit Sub
    SetIndIfResultContains 3, "", "", "oxygen therapy", "", "via trach collar"
    SetIndIfResultContains 3, "", "", "oxygen therapy", "", "via non-invasive mask ventilation"
    If inds(3).checked Then Exit Sub
    
    SetIndIfResultContains 3, "NURSING ASSESSMENT: CVA ASSESSMENT TOOL", "", "CVA ASSESSMENT", "", "Foot press unequal"
    SetIndIfResultContains 3, "NURSING ASSESSMENT: CVA ASSESSMENT TOOL", "", "CVA ASSESSMENT", "", "flaccid on the left"
    SetIndIfResultContains 3, "NURSING ASSESSMENT: CVA ASSESSMENT TOOL", "", "CVA ASSESSMENT", "", "flaccid on the right"
    If inds(3).checked Then Exit Sub
    SetIndIfResultContains 3, "NURSING ASSESSMENT: CVA ASSESSMENT TOOL", "", "CVA ASSESSMENT", "", "Hand grasps unequal"
    SetIndIfResultContains 3, "NURSING ASSESSMENT: CVA ASSESSMENT TOOL", "", "CVA ASSESSMENT", "", "weak on the left"
    SetIndIfResultContains 3, "NURSING ASSESSMENT: CVA ASSESSMENT TOOL", "", "CVA ASSESSMENT", "", "weak on the right"
    If inds(3).checked Then Exit Sub
    SetIndIfResultContains 3, "NURSING ASSESSMENT: CVA ASSESSMENT TOOL", "", "CVA ASSESSMENT", "", "moderately weak on the left"
    SetIndIfResultContains 3, "NURSING ASSESSMENT: CVA ASSESSMENT TOOL", "", "CVA ASSESSMENT", "", "moderately weak on the right"
    If inds(3).checked Then Exit Sub
    
    SetIndIfResultContains 3, "", "", "", "Glasgow Coma", EXACT_MATCH_PREFIX & "9"
    SetIndIfResultContains 3, "", "", "", "Glasgow Coma", EXACT_MATCH_PREFIX & "10"
    If inds(3).checked Then Exit Sub
    SetIndIfResultContains 3, "", "", "", "Glasgow Coma", EXACT_MATCH_PREFIX & "11"
    SetIndIfResultContains 3, "", "", "", "Glasgow Coma", EXACT_MATCH_PREFIX & "12"
    If inds(3).checked Then Exit Sub
    
    
    SetIndIfResultContains 3, "", "", "", "", "Impaired mobility(3)"
    SetIndIfResultContains 3, "", "", "", "", "muscle strength 0"
    If inds(3).checked Then Exit Sub
    SetIndIfResultContains 3, "", "", "", "", "muscle strength 1"
    SetIndIfResultContains 3, "", "", "", "", "muscle strength 2"
    SetIndIfResultContains 3, "", "", "", "", "muscle strength 3"
    If inds(3).checked Then Exit Sub
    
    SetIndIfResultContains 3, "", "", "LEFT LOWER EXTREMITY", "", "Inspection findings include amputation"
    SetIndIfResultContains 3, "", "", "LEFT LOWER EXTREMITY", "", "Inspection findings include deformity"
    SetIndIfResultContains 3, "", "", "LEFT LOWER EXTREMITY", "", "Inspection findings include foreign body"
    If inds(3).checked Then Exit Sub
    SetIndIfResultContains 3, "", "", "RIGHT LOWER EXTREMITY", "", "Inspection findings include amputation"
    SetIndIfResultContains 3, "", "", "RIGHT LOWER EXTREMITY", "", "Inspection findings include deformity"
    SetIndIfResultContains 3, "", "", "RIGHT LOWER EXTREMITY", "", "Inspection findings include foreign body"
    If inds(3).checked Then Exit Sub
    
    SetIndIfResultContains 3, "", "", "", "", "Associated with nystagmus"
    'SetIndIfResultContains 3, "", "", "", "", "Associated with visual changes described as"
    If inds(3).checked Then Exit Sub
    
    SetIndIfResultContains 3, "", "", "", "", "History of falls (5)"
    SetIndIfResultContains 3, "", "", "", "", "Bed rest greater than 2 days (5)"
    SetIndIfResultContains 3, "", "", "", "", "Use of level of consciousness altering agents with mentation or cognitive changes (3)"
    If inds(3).checked Then Exit Sub
    SetIndIfResultContains 3, "", "", "", "", "Elimination problems(3)"
    SetIndIfResultContains 3, "", "", "", "", "Confusion (3)"
    SetIndIfResultContains 3, "", "", "", "", "Hendrich II Fall Risk assessment findings include patient confused, disoriented or impulsive"
    If inds(3).checked Then Exit Sub
    
    SetIndIfResultContains 3, "", "", "", "", "altered elimination;"
    SetIndIfResultContains 3, "", "", "", "", "dizziness or vertigo;"
    SetIndIfResultContains 3, "NURSING ASSESSMENT: FALL RISK", "", "HENDRICH II FALL RISK", "", "Multiple attempts" & CHAR_COMMA & " but successful"
    If inds(3).checked Then Exit Sub
    
    SetIndIfResultContains 3, "", "", "", "", "Unable to rise without assistance during test"
    
    SetIndIfResultContains 3, "", "", "", "GCS Total", EXACT_MATCH_PREFIX & "9"
    SetIndIfResultContains 3, "", "", "", "GCS Total", EXACT_MATCH_PREFIX & "10"
    If inds(3).checked Then Exit Sub
    SetIndIfResultContains 3, "", "", "", "GCS Total", EXACT_MATCH_PREFIX & "11"
    SetIndIfResultContains 3, "", "", "", "GCS Total", EXACT_MATCH_PREFIX & "12"
    If inds(3).checked Then Exit Sub
    
    SetIndIfResultContains 3, "", "", "NEURO PED", "", "Nystagmus;"
    SetIndIfResultContains 3, "", "", EXACT_MATCH_PREFIX & "NEURO", "", "Nystagmus;"
    If inds(3).checked Then Exit Sub
    
    'SetIndIfResultContains 3, "", "", "", "", "Visual changes described as"
    SetIndIfResultContains 3, "", "", "", "", "Focused neuro assessment findings include patient responsive to verbal stimuli"
    If inds(3).checked Then Exit Sub
    
    
    SetIndIfResultContains 3, "", "", "NEURO PED", "", "Hand grasps unequal"
    SetIndIfResultContains 3, "", "", "NEURO PED", "", "weak on the left"
    SetIndIfResultContains 3, "", "", "NEURO PED", "", "weak on the right"
    If inds(3).checked Then Exit Sub
    SetIndIfResultContains 3, "", "", "NEURO PED", "", "moderately weak on the left"
    SetIndIfResultContains 3, "", "", "NEURO PED", "", "moderately weak on the right"
    SetIndIfResultContains 3, "", "", "NEURO PED", "", "flaccid on the left"
    SetIndIfResultContains 3, "", "", "NEURO PED", "", "flaccid on the right"
    If inds(3).checked Then Exit Sub
    
    SetIndIfResultContains 3, "", "", EXACT_MATCH_PREFIX & "NEURO", "", "Hand grasps unequal"
    SetIndIfResultContains 3, "", "", EXACT_MATCH_PREFIX & "NEURO", "", "weak on the left"
    SetIndIfResultContains 3, "", "", EXACT_MATCH_PREFIX & "NEURO", "", "weak on the right"
    If inds(3).checked Then Exit Sub
    SetIndIfResultContains 3, "", "", EXACT_MATCH_PREFIX & "NEURO", "", "moderately weak on the left"
    SetIndIfResultContains 3, "", "", EXACT_MATCH_PREFIX & "NEURO", "", "moderately weak on the right"
    SetIndIfResultContains 3, "", "", EXACT_MATCH_PREFIX & "NEURO", "", "flaccid on the left"
    SetIndIfResultContains 3, "", "", EXACT_MATCH_PREFIX & "NEURO", "", "flaccid on the right"
    If inds(3).checked Then Exit Sub
    
    SetIndIfResultContains 3, "", "", "NEURO PED", "", "Associated with dizziness described as"
    SetIndIfResultContains 3, "", "", "NEURO PED", "", "Associated with motor ability changes"
    SetIndIfResultContains 3, "", "", "NEURO PED", "", "Associated with posturing"
    If inds(3).checked Then Exit Sub
    SetIndIfResultContains 3, "", "", "NEURO PED", "", "Associated with weakness"
    SetIndIfResultContains 3, "", "", EXACT_MATCH_PREFIX & "NEURO", "", "Associated with dizziness described as"
    SetIndIfResultContains 3, "", "", EXACT_MATCH_PREFIX & "NEURO", "", "Associated with motor ability changes"
    If inds(3).checked Then Exit Sub
    SetIndIfResultContains 3, "", "", EXACT_MATCH_PREFIX & "NEURO", "", "Associated with posturing"
    SetIndIfResultContains 3, "", "", EXACT_MATCH_PREFIX & "NEURO", "", "Associated with weakness"
    
    SetIndIfResultContains 3, "NURSING ASSESSMENT: FOCUSED", "", EXACT_MATCH_PREFIX & "NEURO", "", "Weakness;"
    SetIndIfResultContains 3, "", "", "", "", "Suicidal ideations present"
    If inds(3).checked Then Exit Sub
    
    SetIndIfResultContains 3, "", "", "LEFT LOWER EXTREMITY", "", "Inspection findings include signs of trauma"
    SetIndIfResultContains 3, "", "", "RIGHT LOWER EXTREMITY", "", "Inspection findings include signs of trauma"
    If inds(3).checked Then Exit Sub
    
    
    SetIndIfResultContains 3, "", "", "", "", "lower extremity assessment findings include signs of trauma"
    
    SetIndIfResultContains 3, "", "", "LOWER EXTREMITY TRAUMA", "", "Bleeding;"
    SetIndIfResultContains 3, "", "", "LOWER EXTREMITY TRAUMA", "", "Open fracture;"
    If inds(3).checked Then Exit Sub
    

    SetIndIfResultContains 3, "", "", "LEFT UPPER EXTREMITY", "", "Inspection findings include signs of trauma"
    SetIndIfResultContains 3, "", "", "LEFT UPPER EXTREMITY", "", "Inspection findings include amputation"
    SetIndIfResultContains 3, "", "", "LEFT UPPER EXTREMITY", "", "Inspection findings include deformity"
    If inds(3).checked Then Exit Sub
    SetIndIfResultContains 3, "", "", "LEFT UPPER EXTREMITY", "", "Inspection findings include foreign body"
    SetIndIfResultContains 3, "", "", "RIGHT UPPER EXTREMITY", "", "Inspection findings include signs of trauma"
    SetIndIfResultContains 3, "", "", "RIGHT UPPER EXTREMITY", "", "Inspection findings include amputation"
    If inds(3).checked Then Exit Sub
    SetIndIfResultContains 3, "", "", "RIGHT UPPER EXTREMITY", "", "Inspection findings include deformity"
    SetIndIfResultContains 3, "", "", "RIGHT UPPER EXTREMITY", "", "Inspection findings include foreign body"
    SetIndIfResultContains 3, "", "", "", "", "upper extremity assessment findings include signs of trauma"
    If inds(3).checked Then Exit Sub
    SetIndIfResultContains 3, "", "", "EXTREMITY UPPER TRAUMA", "", "Bleeding;"
    SetIndIfResultContains 3, "", "", "EXTREMITY UPPER TRAUMA", "", "Open fracture;"
    If inds(3).checked Then Exit Sub
'NURSING ASSESSMENT: EXTREMITY UPPER TRAUMA
'LEFT UPPER EXTREMITY    Notes   NULL    NULL    NULL    NULL
'bleeding from large gash in left anticubital area , pt has a 3 inch deep gash at this site.


    SetIndIfResultContains 3, "", "", "", "", "Suicidal ideations present"
    SetIndIfResultContains 3, "", "", "", "", EXACT_MATCH_PREFIX & "Reported overdose"
    SetIndIfResultContains 3, "", "", "", "", "Homicidal ideations present"
    If inds(3).checked Then Exit Sub
    
    'SetIndIfResultContains 3, "", "", "", "", "Transported via cart/stretcher"
    SetIndIfResultContains 3, "", "", "", "", "Patient assisted to bathroom with steady gait"
    SetIndIfResultContains 3, "", "", "", "", "Patient assisted to bathroom with unsteady gait"
    SetIndIfResultContains 3, "", "", "", "", "Patient re-positioned to dorsal recumbent position"
    If inds(3).checked Then Exit Sub
    SetIndIfResultContains 3, "", "", "", "", "Patient re-positioned to prone recumbent position"
    SetIndIfResultContains 3, "", "", "", "", "Patient re-positioned to trendelenberg position"
    SetIndIfResultContains 3, "", "", "", "", "Patient re-positioned to reverse trendelenberg position"
    SetIndIfResultContains 3, "", "", "", "", "Patient re-positioned to high Fowler"
    If inds(3).checked Then Exit Sub
    SetIndIfResultContains 3, "", "", "", "", "Patient re-positioned to Fowler"
    SetIndIfResultContains 3, "", "", "", "", "Patient re-positioned to semi-Fowler"
    SetIndIfResultContains 3, "", "", "", "", "Patient re-positioned to left side lying position"
    SetIndIfResultContains 3, "", "", "", "", "Patient re-positioned to right side lying position"
    If inds(3).checked Then Exit Sub
    SetIndIfResultContains 3, "", "", "", "", "Patient ambulating with UNsteady gait, requiring minimal assistance"
    SetIndIfResultContains 3, "", "", "", "", "Patient ambulating with UNsteady gait, requiring moderate assistance"
    SetIndIfResultContains 3, "", "", "", "", "Patient too weak or unsteady to ambulate"
    SetIndIfResultContains 3, "", "", "", "", "Patient unable to bear weight or ambulate"
    If inds(3).checked Then Exit Sub
    SetIndIfResultContains 3, "", "", "", "", "Patient ambulated with assistance"
    SetIndIfResultContains 3, "", "", "", "", "Patient was transferred to chair with assistance"
    SetIndIfResultContains 3, "", "", "", "", "Patient was transferred to bed/cart with assistance"
    SetIndIfResultContains 3, "", "", "", "", "Patient was transferred using lateral transfer mattress"
    If inds(3).checked Then Exit Sub
    SetIndIfResultContains 3, "", "", "", "", "Patient was transferred using mechanical lift"
    
    SetIndIfResultContains 3, "", "", "AMPUTATED BODY PART CARE", "", ""
    SetIndIfResultContains 3, "", "", "ARTERIAL LINE", "", ""
    SetIndIfResultContains 3, "", "", "BLOOD TRANSFUSION", "", ""
    SetIndIfResultContains 3, "", "", "", "", "Patient placed on cardiac monitor"
    If inds(3).checked Then Exit Sub
    
    
    SetIndIfResultContains 3, "NURSING PROCEDURE: IV ACCESS", "", "IV SITE 1", "", "Central Line;"
    SetIndIfResultContains 3, "NURSING PROCEDURE: IV ACCESS", "", "IV SITE 2", "", "Central Line;"
    If inds(3).checked Then Exit Sub
    SetIndIfResultContains 3, "NURSING PROCEDURE: IV ACCESS", "", "IV SITE 3", "", "Central Line;"
    SetIndIfResultContains 3, "NURSING PROCEDURE: IV ACCESS", "", "IV SITE 4", "", "Central Line;"
    SetIndIfResultContains 3, "", "", "DECONTAMINATION", "", ""
    If inds(3).checked Then Exit Sub
    
    SetIndIfResultContains 3, "", "", "DYSRHYTHMIA INTERVENTIONS", "", ""
    SetIndIfResultContains 3, "", "", "DEFIBRILLATION", "", ""
    SetIndIfResultContains 3, "", "", "CARDIOVERSION", "", ""
    If inds(3).checked Then Exit Sub
    SetIndIfResultContains 3, "", "", "PACING", "", ""
    SetIndIfResultContains 3, "", "", "ELIMINATION", "", ""
    SetIndIfResultContains 3, "", "", "GASTRIC TUBE", "", ""
    If inds(3).checked Then Exit Sub

    SetIndIfResultContains 3, "", "", "", "", "After procedure" & CHAR_COMMA & " gastric tube patent"
   
    SetIndIfResultContains 3, "NURSING PROCEDURE: GASTRIC TUBE", "", "", "", "After procedure" & CHAR_COMMA & " suction maintained"
    SetIndIfResultContains 3, "", "", "", "", "Gastric tube removed with tube intact"
    If inds(3).checked Then Exit Sub
    SetIndIfResultContains 3, "", "", "HYPERTHERMIA", "", ""
    SetIndIfResultContains 3, "NURSING PROCEDURE: HYPERTHERMIA", "", "", "After procedure" & CHAR_COMMA & " patient___core temperature is", ""
    SetIndIfResultContains 3, "NURSING PROCEDURE: HYPOTHERMIA", "", "", "After procedure" & CHAR_COMMA & " patient___core temperature is", ""
    If inds(3).checked Then Exit Sub
    SetIndIfResultContains 3, "", "", "HYPOTHERMIA", "", ""
    SetIndIfResultContains 3, "", "", "", "Tube feedings intake(ml)", ""
    SetIndIfResultContains 3, "", "", "JOINT CARE", "", ""
    If inds(3).checked Then Exit Sub
    
    SetIndIfResultContains 3, "NURSING PROCEDURE: JOINT CARE", "", "FOLLOW-UP", "", "After procedure" & CHAR_COMMA & " post reduction x-ray performed"
    SetIndIfResultContains 3, "NURSING PROCEDURE: JOINT CARE", "", "FOLLOW-UP", "", "After procedure" & CHAR_COMMA & " ice therapy applied"
    SetIndIfResultContains 3, "NURSING PROCEDURE: JOINT CARE", "", "FOLLOW-UP", "", "After procedure" & CHAR_COMMA & " capillary refill less than 2 seconds"
    If inds(3).checked Then Exit Sub
    SetIndIfResultContains 3, "NURSING PROCEDURE: JOINT CARE", "", "FOLLOW-UP", "", "After procedure" & CHAR_COMMA & " distal circulation intact"
    SetIndIfResultContains 3, "NURSING PROCEDURE: JOINT CARE", "", "FOLLOW-UP", "", "After procedure" & CHAR_COMMA & " distal motor function intact"
    SetIndIfResultContains 3, "NURSING PROCEDURE: JOINT CARE", "", "FOLLOW-UP", "", "After procedure" & CHAR_COMMA & " distal sensation intact"
    If inds(3).checked Then Exit Sub
    SetIndIfResultContains 3, "NURSING PROCEDURE: JOINT CARE", "", "FOLLOW-UP", "", "After procedure" & CHAR_COMMA & " distal pulses present"
    SetIndIfResultContains 3, "", "", "LUMBAR PUNCTURE", "", ""
    SetIndIfResultContains 3, "", "", "POSITIONING", "", ""
    If inds(3).checked Then Exit Sub
    SetIndIfResultContains 3, "NURSING PROCEDURE: RESTRAINT VIOLENT", "", "", "", ""
    SetIndIfResultContains 3, "NURSING PROCEDURE: RESTRAINT NONVIOLENT", "", "", "", ""
    SetIndIfResultContains 3, "NURSING PROCEDURE: RESTRAINT-BEHAVIORAL/VIOLENT", "", "", "", ""
    SetIndIfResultContains 3, "NURSING PROCEDURE: RESTRAINT-MEDICAL/NONVIOLENT", "", "", "", ""
    If inds(3).checked Then Exit Sub
    

    SetIndIfResultContains 3, "", "", "", "", "Splinting indicated for sprain care"
    SetIndIfResultContains 3, "", "", "", "", "Splinting indicated for strain care"
    If inds(3).checked Then Exit Sub
    SetIndIfResultContains 3, "", "", "", "", "Splinting indicated for fracture care"
    SetIndIfResultContains 3, "", "", "", "", "Soft cervical collar applied"
    If inds(3).checked Then Exit Sub
    
    SetIndIfResultContains 3, "", "", "SPLINTING", "", "Splint applied to;"
    
    SetIndIfResultContains 3, "", "", "", "", "Cast applied"
    SetIndIfResultContains 3, "", "", "", "", "Crutches given with instructions"
    If inds(3).checked Then Exit Sub
    SetIndIfResultContains 3, "", "", "", "", "Walker given with instructions"
    SetIndIfResultContains 3, "", "", "", "", "Cane given with instructions"
    
    If inds(3).checked Then Exit Sub

    SetIndIfResultContains 3, "", "", "", "", "Gastrostomy tube site care given"
    SetIndIfResultContains 3, "", "", "", "", "Jejunostomy tube site care given"
    SetIndIfResultContains 3, "", "", "", "", "Feeding tube site care given"
    If inds(3).checked Then Exit Sub
    SetIndIfResultContains 3, "", "", "TUBE CARE", "", "Site care given;"
    SetIndIfResultContains 3, "", "", "TUBE THORACOSTOMY", "", ""
    SetIndIfResultContains 3, "", "", "URINE COLLECTION FEMALE", "", "Manual irrigation performed"
    If inds(3).checked Then Exit Sub
    SetIndIfResultContains 3, "", "", "", "", "Continuous bladder irrigation initiated"
    SetIndIfResultContains 3, "", "", "URINE COLLECTION MALE", "", "Manual irrigation performed"
    SetIndIfResultContains 3, "", "", "", "", "ambulating with assistance"
    If inds(3).checked Then Exit Sub
    SetIndIfResultContains 3, "", "", "", "", "ambulating with crutches"
    SetIndIfResultContains 3, "", "", "", "", "ambulating with walker"
    SetIndIfResultContains 3, "", "", "", "", "ambulating with cane"
    If inds(3).checked Then Exit Sub
    SetIndIfResultContains 3, "", "", "DISCHARGE", "", "in a wheelchair;"
    SetIndIfResultContains 3, "", "", "DISCHARGE", "", "on a stretcher;"
    SetIndIfResultContains 3, "", "", "DISCHARGE", "", "carried;"
    If inds(3).checked Then Exit Sub
    SetIndIfResultContains 3, "", "", "", "", "Inability to ambulate;"
    SetIndIfResultContains 3, "", "", "TRIAGE REASSESSMENT", "", "C-Collar/Spinal Immobilization"
    SetIndIfResultContains 3, "", "", "TRIAGE REASSESSMENT", "", "Patient assisted to cart"
    SetIndIfResultContains 3, "", "", "TRIAGE REASSESSMENT", "", "Bathroom assistance given"
    
    If inds(3).checked Then Exit Sub
    SetIndIfResultContains 3, "", "", "POST ANGIOGRAM FOLLOW-UP", "", ""
    If inds(3).checked Then Exit Sub
    
    SetInd 2, "Defaulting to Self Care - No documentation found for Assist or Extended."
    
    Exit Sub
errHandler:
    g_util.ThrowError "Check_234"
    Resume  'debug
End Sub


Private Sub Check_5()
    On Error GoTo errHandler
    
    dvprint "-----------------------"
    dvprint "ED Visit 5. Communication Support"
    dvprint "-----------------------"
   
If inds(5).checked Then Exit Sub
    SetIndIfResultContains 5, "", "", "", "Communication Needs Requiring Additional Care", ""
    SetIndIfResultContains 5, "", "", "", "", "Interpreter Called"
    SetIndIfResultContains 5, "", "", "", "", "responsive to painful stimuli;"
    SetIndIfResultContains 5, "", "", "", "obtained through interpreter", ""
If inds(5).checked Then Exit Sub
    SetIndIfResultContains 5, "", "", EXACT_MATCH_PREFIX & "RESPIRATORY", "", "in short phrases;"
    SetIndIfResultContains 5, "", "", EXACT_MATCH_PREFIX & "RESPIRATORY", "", "in single words;"
    SetIndIfResultContains 5, "", "", EXACT_MATCH_PREFIX & "RESPIRATORY", "", "unable to speak;"
If inds(5).checked Then Exit Sub
    SetIndIfResultContains 5, "", "", EXACT_MATCH_PREFIX & "RESPIRATORY", "", "with hoarse voice;"
    SetIndIfResultContains 5, "", "", EXACT_MATCH_PREFIX & "RESPIRATORY", "", "with muffled voice;"
    
    SetIndIfResultContains 5, "", "", "RESPIRATORY/CHEST", "", "in short phrases;"
    SetIndIfResultContains 5, "", "", "RESPIRATORY/CHEST", "", "in single words;"
'    SetIndIfResultContains 5, "", "", "RESPIRATORY/CHEST", "", "unable to speak;"
'    SetIndIfResultContains 5, "", "", "", "", "unable to speak;"
    If ResultContains("", "", "", "", "unresponsive;") Or ResultContains("", "", "", "", "unconscious;") Then
        If ResultContains("", "", "", "", "unable to speak;") Then
            dprint "Suppressing <unable to speak> due to unconscious or unresponsive"
        End If
    Else
        SetIndIfResultContains 5, "", "", "", "", "unable to speak;"
    End If
If inds(5).checked Then Exit Sub
    SetIndIfResultContains 5, "", "", "RESPIRATORY/CHEST", "", "with hoarse voice;"
    SetIndIfResultContains 5, "", "", "RESPIRATORY/CHEST", "", "with muffled voice;"

    SetIndIfResultContains 5, "", "", "OXYGEN THERAPY", "", "via BIPAP"
    SetIndIfResultContains 5, "", "", "OXYGEN THERAPY", "", "via CPAP"
If inds(5).checked Then Exit Sub
    SetIndIfResultContains 5, "", "", "OXYGEN THERAPY", "", "via venti-mask"
    'SetIndIfResultContains 5, "", "", "OXYGEN THERAPY", "", "via partial rebreather mask"
    'SetIndIfResultContains 5, "", "", "OXYGEN THERAPY", "", "via non-rebreather mask"
If inds(5).checked Then Exit Sub
    'SetIndIfResultContains 5, "", "", "OXYGEN THERAPY", "", "via simple mask"
    SetIndIfResultContains 5, "", "", "OXYGEN THERAPY", "", "via trach collar"
    SetIndIfResultContains 5, "", "", "OXYGEN THERAPY", "", "via non-invasive mask ventilation"
If inds(5).checked Then Exit Sub
    
    SetIndIfResultContains 5, "", "", "CVA ASSESSMENT", "", "dysphasic;"
    SetIndIfResultContains 5, "", "", "CVA ASSESSMENT", "", "expressively aphasic;"
    SetIndIfResultContains 5, "", "", "CVA ASSESSMENT", "", "garbled;"
If inds(5).checked Then Exit Sub
    SetIndIfResultContains 5, "", "", "CVA ASSESSMENT", "", "incoherent;"
    SetIndIfResultContains 5, "", "", "CVA ASSESSMENT", "", "not verbally responsive;"
    SetIndIfResultContains 5, "", "", "CVA ASSESSMENT", "", "receptively aphasic;"
If inds(5).checked Then Exit Sub
    SetIndIfResultContains 5, "", "", "CVA ASSESSMENT", "", "slurred;"
    SetIndIfResultContains 5, "", "", "CVA ASSESSMENT", "", "slow;"
    SetIndIfResultContains 5, "", "", EXACT_MATCH_PREFIX & "NEURO", "", "dysphasic;"
If inds(5).checked Then Exit Sub
    SetIndIfResultContains 5, "", "", EXACT_MATCH_PREFIX & "NEURO", "", "expressively aphasic;"
    SetIndIfResultContains 5, "", "", EXACT_MATCH_PREFIX & "NEURO", "", "garbled;"
    SetIndIfResultContains 5, "", "", EXACT_MATCH_PREFIX & "NEURO", "", "incoherent;"
If inds(5).checked Then Exit Sub
    SetIndIfResultContains 5, "", "", EXACT_MATCH_PREFIX & "NEURO", "", "not verbally responsive;"
    SetIndIfResultContains 5, "", "", EXACT_MATCH_PREFIX & "NEURO", "", "receptively aphasic;"
    SetIndIfResultContains 5, "", "", EXACT_MATCH_PREFIX & "NEURO", "", "slurred;"
If inds(5).checked Then Exit Sub
    SetIndIfResultContains 5, "", "", EXACT_MATCH_PREFIX & "NEURO", "", "slow;"
    SetIndIfResultContains 5, "", "", "NEURO PED", "", "dysphasic;"
    SetIndIfResultContains 5, "", "", "NEURO PED", "", "expressively aphasic;"
If inds(5).checked Then Exit Sub
    SetIndIfResultContains 5, "", "", "NEURO PED", "", "garbled;"
    SetIndIfResultContains 5, "", "", "NEURO PED", "", "incoherent;"
    SetIndIfResultContains 5, "", "", "NEURO PED", "", "not verbally responsive;"
If inds(5).checked Then Exit Sub
    SetIndIfResultContains 5, "", "", "NEURO PED", "", "receptively aphasic;"
    SetIndIfResultContains 5, "", "", "NEURO PED", "", "slurred;"
    SetIndIfResultContains 5, "", "", "NEURO PED", "", "slow;"
If inds(5).checked Then Exit Sub
    SetIndIfResultContains 5, "", "", "", "", "Hearing deficit described as"
    SetIndIfResultContains 5, "", "", "", "", "able to speak short phrases"
    SetIndIfResultContains 5, "", "", "", "", "able to speak only single words"
If inds(5).checked Then Exit Sub
    SetIndIfResultContains 5, "", "", "", "", "hoarse;"
    SetIndIfResultContains 5, "", "", "", "", "with muffled voice"
If inds(5).checked Then Exit Sub
    SetIndIfResultContains 5, "", "", EXACT_MATCH_PREFIX & "NEURO", "", "Speech incoherent;"
    SetIndIfResultContains 5, "", "", EXACT_MATCH_PREFIX & "NEURO", "", "Silent;"
    SetIndIfResultContains 5, "", "", EXACT_MATCH_PREFIX & "NEURO", "", "Hysterical;"
If inds(5).checked Then Exit Sub
    SetIndIfResultContains 5, "", "", EXACT_MATCH_PREFIX & "NEURO", "", "Speech slurred;"
    SetIndIfResultContains 5, "", "", "", "", "Focused neuro assessment findings include patient responsive to painful stimuli"
    SetIndIfResultContains 5, "", "", "", "Translator used", ""
If inds(5).checked Then Exit Sub
    'SetIndIfResultContains 5, "", "", "", "O2 sat", "on Non-Rebreather 10 L"
    'SetIndIfResultContains 5, "", "", "", "O2 sat", "on Non-Rebreather 15 L"
    SetIndIfResultContains 5, "", "", "", "O2 sat", "on BIPAP"
If inds(5).checked Then Exit Sub
    SetIndIfResultContains 5, "", "", "", "O2 sat", "on Continuous Nebulizer"
    SetIndIfResultContains 5, "", "", "", "O2 sat", "on CPAP"
    SetIndIfResultContains 5, "", "", "", "O2 sat", "on Trach Collar"
If inds(5).checked Then Exit Sub
    SetIndIfResultContains 5, "", "", "", "", "Blindness identified as a Barrier to Learning"
    SetIndIfResultContains 5, "", "", "", "", "Complete Hearing Impairment identified as a Barrier to Learning"
    SetIndIfResultContains 5, "", "", "", "", "Partial Hearing Impairment identified as a Barrier to Learning"
If inds(5).checked Then Exit Sub
    SetIndIfResultContains 5, "", "", "", "", "Hearing Aid identified as a Barrier to Learning"
    'SetIndIfResultContains 5, "", "", "", "", "Other identified Barriers to Learning included"
    SetIndIfResultContains 5, "", "", "", "", "Used a Translator to address Barriers to Learning"
    SetIndIfResultContains 5, "", "", "TRIAGE REASSESSMENT", "", "interpreter called"
    SetIndIfResultContains 5, "", "", "", "interpreter", ""
    SetIndIfResultContains 5, "", "", "", "", "Communication assistance provided utilizing interpreter"
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_5"
    Resume  'debug
End Sub


Private Sub Check_67()
    On Error GoTo errHandler
    
    dvprint "-----------------------"
    dvprint "ED Visit 6. Safety Management q30min"
    dvprint "ED Visit 7. Safety Management 1-to-1"
    dvprint "-----------------------"
    
    If inds(7).checked Then Exit Sub
    SetIndIfResultContains 7, "", "", "", "", "One to one sitter utilized"
    If inds(7).checked Then Exit Sub
    
    SetIndIfResultContains 6, "", "", "", "", "Safety checks every 15 minutes"
    SetIndIfResultContains 6, "", "", "", "", "Safety checks every 30 minutes"
    SetIndIfResultContains 6, "", "", "", "", "Safety reassessment every 15 minutes"
    If inds(6).checked Then Exit Sub
'    SetIndIfResultContains 6, "", "", "PSYCH/SOCIAL", "", "agitated;"
    SetIndIfResultContains 6, "", "", "PSYCH/SOCIAL", "", "aggressive;"
    SetIndIfResultContains 6, "", "", "PSYCH/SOCIAL", "", "combative;"
    If inds(6).checked Then Exit Sub
    SetIndIfResultContains 6, "", "", "PSYCH/SOCIAL", "", "flight of ideas;"
    SetIndIfResultContains 6, "", "", "PSYCH/SOCIAL", "", "judgment impaired;"
    SetIndIfResultContains 6, "", "", "PSYCH/SOCIAL", "", "manic;"
    If inds(6).checked Then Exit Sub
    SetIndIfResultContains 6, "", "", "PSYCH/SOCIAL", "", "memory loss;"
    SetIndIfResultContains 6, "", "", "", "", "Patient was placed in restraints including"
    
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_67"
    Resume  'debug
End Sub

Private Sub Check_89()
    Dim reslist As String
    Dim found_what As String
    On Error GoTo errHandler
    
    dvprint "-----------------------"
    dvprint "ED Visit 8. Behavior/Emotional Management"
    dvprint "ED Visit 9. Behavior/Emotional Management >= 30min"
    dvprint "-----------------------"
    
    If inds(9).checked Then Exit Sub
    SetIndIfAllResults 9, "", "", "", "", "Behavioral Management needed and given,Intervention greater than 30 minutes,Family Intervention"
    SetIndIfAllResults 9, "", "", "", "", "Behavioral Management needed and given,Intervention greater than 30 minutes,Active Listening intervention"
    SetIndIfAllResults 9, "", "", "", "", "Behavioral Management needed and given,Intervention greater than 30 minutes,Limit Setting intervention"
    If inds(9).checked Then Exit Sub
    SetIndIfAllResults 9, "", "", "", "", "Behavioral Management needed and given,Intervention greater than 30 minutes,Reassurance intervention"
    SetIndIfAllResults 9, "", "", "", "", "Behavioral Management needed and given,Intervention greater than 30 minutes,Verbal communication intervention"
    SetIndIfAllResults 9, "", "", "", "", "Behavioral Management needed and given,Intervention greater than 30 minutes,Destimulation intervention"
    If inds(9).checked Then Exit Sub
    SetIndIfAllResults 9, "", "", "", "", "Behavioral Management needed and given,Intervention greater than 30 minutes,Diversion intervention"
    SetIndIfAllResults 9, "", "", "", "", "Behavioral Management needed and given,Intervention greater than 30 minutes,Music intervention"
    SetIndIfAllResults 9, "", "", "", "", "Behavioral Management needed and given,Intervention greater than 30 minutes,Reading intervention"
    If inds(9).checked Then Exit Sub
    SetIndIfAllResults 9, "", "", "", "", "Behavioral Management needed and given,Intervention greater than 30 minutes,Relaxation intervention"
    SetIndIfAllResults 9, "", "", "", "", "Behavioral Management needed and given,Intervention greater than 30 minutes,Chaplain intervention"
    If inds(9).checked Then Exit Sub
    SetIndIfAllResults 9, "", "", "", "", "Behavioral Management needed and given,Intervention greater than 30 minutes,Medication intervention"
    
    CheckOtherIntervention
    If inds(9).checked Then Exit Sub

    SetIndIfAllResults 9, "", "", "", "", "Emotional support needed and given,Intervention greater than 30 minutes,Family Intervention used"
    SetIndIfAllResults 9, "", "", "", "", "Emotional support needed and given,Intervention greater than 30 minutes,Active Listening intervention"
    If inds(9).checked Then Exit Sub
    SetIndIfAllResults 9, "", "", "", "", "Emotional support needed and given,Intervention greater than 30 minutes,Limit Setting intervention"
    SetIndIfAllResults 9, "", "", "", "", "Emotional support needed and given,Intervention greater than 30 minutes,Reassurance intervention"
    SetIndIfAllResults 9, "", "", "", "", "Emotional support needed and given,Intervention greater than 30 minutes,Verbal communication intervention"
    If inds(9).checked Then Exit Sub
    SetIndIfAllResults 9, "", "", "", "", "Emotional support needed and given,Intervention greater than 30 minutes,Destimulation intervention"
    SetIndIfAllResults 9, "", "", "", "", "Emotional support needed and given,Intervention greater than 30 minutes,Diversion intervention"
    SetIndIfAllResults 9, "", "", "", "", "Emotional support needed and given,Intervention greater than 30 minutes,Music intervention"
    If inds(9).checked Then Exit Sub
    SetIndIfAllResults 9, "", "", "", "", "Emotional support needed and given,Intervention greater than 30 minutes,Reading intervention"
    SetIndIfAllResults 9, "", "", "", "", "Emotional support needed and given,Intervention greater than 30 minutes,Relaxation intervention"
    If inds(9).checked Then Exit Sub
    SetIndIfAllResults 9, "", "", "", "", "Emotional support needed and given,Intervention greater than 30 minutes,Chaplain intervention"
    SetIndIfAllResults 9, "", "", "", "", "Emotional support needed and given,Intervention greater than 30 minutes,Medication intervention"
    If inds(9).checked Then Exit Sub
    If inds(8).checked Then Exit Sub
    
    SetIndIfResultContains 8, "", "", "CONSTITUTIONAL", "", "combative;"
    SetIndIfResultContains 8, "", "", "CONSTITUTIONAL", "", "uncooperative;"
    If inds(8).checked Then Exit Sub
    
    SetIndIfAllResults 8, "", "", "", "", "Behavioral Management needed and given,Family Intervention"
    SetIndIfAllResults 8, "", "", "", "", "Behavioral Management needed and given,Active Listening intervention"
    SetIndIfAllResults 8, "", "", "", "", "Behavioral Management needed and given,Limit Setting intervention"
    If inds(8).checked Then Exit Sub
    SetIndIfAllResults 8, "", "", "", "", "Behavioral Management needed and given,Reassurance intervention"
    SetIndIfAllResults 8, "", "", "", "", "Behavioral Management needed and given,Verbal communication intervention"
    SetIndIfAllResults 8, "", "", "", "", "Behavioral Management needed and given,Destimulation intervention"
    If inds(8).checked Then Exit Sub
    SetIndIfAllResults 8, "", "", "", "", "Behavioral Management needed and given,Diversion intervention"
    SetIndIfAllResults 8, "", "", "", "", "Behavioral Management needed and given,Music intervention"
    SetIndIfAllResults 8, "", "", "", "", "Behavioral Management needed and given,Reading intervention"
    If inds(8).checked Then Exit Sub
    SetIndIfAllResults 8, "", "", "", "", "Behavioral Management needed and given,Relaxation intervention"
    SetIndIfAllResults 8, "", "", "", "", "Behavioral Management needed and given,Chaplain intervention"
    If inds(8).checked Then Exit Sub
    SetIndIfAllResults 8, "", "", "", "", "Behavioral Management needed and given,Medication intervention"

    SetIndIfAllResults 8, "", "", "", "", "Emotional support needed and given,Family Intervention"
    SetIndIfAllResults 8, "", "", "", "", "Emotional support needed and given,Active Listening intervention"
    If inds(8).checked Then Exit Sub
    SetIndIfAllResults 8, "", "", "", "", "Emotional support needed and given,Limit Setting intervention"
    SetIndIfAllResults 8, "", "", "", "", "Emotional support needed and given,Reassurance intervention"
    SetIndIfAllResults 8, "", "", "", "", "Emotional support needed and given,Verbal communication intervention"
    If inds(8).checked Then Exit Sub
    SetIndIfAllResults 8, "", "", "", "", "Emotional support needed and given,Destimulation intervention"
    SetIndIfAllResults 8, "", "", "", "", "Emotional support needed and given,Diversion intervention"
    SetIndIfAllResults 8, "", "", "", "", "Emotional support needed and given,Music intervention"
    If inds(8).checked Then Exit Sub
    SetIndIfAllResults 8, "", "", "", "", "Emotional support needed and given,Reading intervention"
    SetIndIfAllResults 8, "", "", "", "", "Emotional support needed and given,Relaxation intervention"
    If inds(8).checked Then Exit Sub
    SetIndIfAllResults 8, "", "", "", "", "Emotional support needed and given,Chaplain intervention"
    SetIndIfAllResults 8, "", "", "", "", "Emotional support needed and given,Medication intervention"
    If inds(8).checked Then Exit Sub
    
    SetIndIfResultContains 8, "", "", EXACT_MATCH_PREFIX & "NEURO", "", "combative;"
    SetIndIfResultContains 8, "", "", EXACT_MATCH_PREFIX & "NEURO", "", "uncooperative;"
    If inds(8).checked Then Exit Sub
    
    SetIndIfResultContains 8, "", "", "", "", "uncooperative" & CHAR_COMMA & " smells of alcohol"
    SetIndIfResultContains 8, "", "", "PSYCH/SOCIAL", "", "agitated;"
    SetIndIfResultContains 8, "", "", "PSYCH/SOCIAL", "", "aggressive;"
    If inds(8).checked Then Exit Sub
    SetIndIfResultContains 8, "", "", "PSYCH/SOCIAL", "", "combative;"
    SetIndIfResultContains 8, "", "", "PSYCH/SOCIAL", "", "crying;"
    SetIndIfResultContains 8, "", "", "PSYCH/SOCIAL", "", "manic;"
    If inds(8).checked Then Exit Sub
    SetIndIfResultContains 8, "", "", "PSYCH/SOCIAL", "", "tearful;"
    SetIndIfResultContains 8, "", "", "LESS RESTRICTIVE MEASURES ATTEMPTED", "", ""

    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_89"
    Resume  'debug
End Sub

Private Sub Check_1011()
    On Error GoTo errHandler
    Dim found_what As String
    Dim reslist As String
    Dim ivfluid As Boolean, ivmed As Boolean

    dvprint "-----------------------"
    dvprint "ED Visit 10. Fluid Management"
    dvprint "ED Visit 11. Fluid Management q1 hr"
    dvprint "-----------------------"
    
    
    If inds(11).checked Then Exit Sub
    SetIndIfResultContains 11, "", "", "", "", "Refer to Chart Plus for sedation documentation"
    
    If ResultContains("", "", "", "", "IV FLUID infusion%ordered by") Then
        ivfluid = True
        dprint "Found: IV Fluid Infusion Ordered by"
    End If
    If ResultContains("", "", "", "", "IV MED infusion%ordered by") Then
        ivmed = True
        dprint "Found: IV Med Infusion Ordered by"
    End If
    
    'do 1-off triggers first
    'mt lines 894-906,909
    If ivfluid Then
        SetIndIfResultContains 11, "", "", "", "", "treatments in progress include patient on cardiac monitor"
        SetIndIfResultContains 11, "", "", "", "", "cardiac monitoring indicated for"
        SetIndIfResultContains 11, "", "", "", "", "heart rhythm"
        SetIndIfResultContains 11, "", "", "", "", "Patient placed on cardiac monitor"
        SetIndIfResultContains 11, "", "", "DYSRHYTHMIA INTERVENTIONS", "", ""
        SetIndIfResultContains 11, "", "", "DEFIBRILLATION", "", ""
        SetIndIfResultContains 11, "", "", "CARDIOVERSION", "", ""
        SetIndIfResultContains 11, "", "", "PACING", "", ""
        SetIndIfResultContains 11, "", "", "ARTERIAL LINE", "", ""
    End If
    If inds(11).checked Then Exit Sub
    
'    SetIndIfAllResults 11, "", "", "", "", "IV MED,ordered by,treatments in progress include patient on cardiac monitor"
'    SetIndIfAllResults 11, "", "", "", "", "IV MED,ordered by,cardiac monitoring indicated for"
'    SetIndIfAllResults 11, "", "", "", "", "IV MED,ordered by,heart rhythm"
'    SetIndIfAllResults 11, "", "", "", "", "IV MED,ordered by,Patient placed on cardiac monitor"
'    SetIndIfAllResults 11, "", "", "DYSRHYTHMIA INTERVENTIONS", "", "IV MED,ordered by"
'    SetIndIfAllResults 11, "", "", "DEFIBRILLATION", "", "IV MED,ordered by"
'    SetIndIfAllResults 11, "", "", "CARDIOVERSION", "", "IV MED,ordered by"
'    SetIndIfAllResults 11, "", "", "PACING", "", "IV MED,ordered by"
    If inds(11).checked Then Exit Sub
    If m_pat.age <= 7 Then
        If ivfluid Then SetInd 11, "Age<=7 + IV Fluid Infusion Ordered by"
    End If
    SetIndIfResultContains 11, "", "", "", "", "Blood transfusion indicated for"
    SetIndIfResultContains 11, "NURSING PROCEDURE: PROCEDURAL SEDATION", "", "PRE-SEDATION", "", ""
    SetIndIfResultContains 11, "NURSING PROCEDURE: PROCEDURAL SEDATION", "", "PREPROCEDURE ASSESSMENT", "", ""
    If inds(11).checked Then Exit Sub
    
    CheckBolus
'    SetIndIfAllResults 11, "", "", "ARTERIAL LINE", "", "IV FLUID infusion%ordered by"
'    SetIndIfAllResults 11, "", "", "ARTERIAL LINE", "", "IV MED,ordered by"
    
    If inds(11).checked Then Exit Sub
    found_what = "Fluid Management items"
'    SetupBuckets (70)
b_filter = ""
b_excl = ""
    BucketFluids
    FillBucket2
    ExecIntervals 11, 70 'ExecPutIntoBuckets
'    PrintBuckets
    If inds(11).checked Then Exit Sub
    
'    SetupBuckets (70)
'    BucketFluids
'    ExecPutIntoBuckets
'    If CheckBucketFreq(found_what) Then SetInd 11, found_what
'    PrintBuckets
'    If inds(11).checked Then Exit Sub
    
    
    If inds(11).checked Then Exit Sub
    
        SetIndIfResultContains 10, "", "", "URINE COLLECTION", "", "output amount (ml)"
        SetIndIfResultContains 10, "", "", "URINE COLLECTION", "", "Total urine output (ml)"
        SetIndIfResultContains 10, "", "", "URINE COLLECTION", "", "Total output (ml)"
        SetIndIfResultContains 10, "", "", "", "", "Urinary catheter present draining; Amount (ml)"
        SetIndIfResultContains 10, "", "", "", "", "Associated with indwelling urinary catheter present; with no urine output"
        SetIndIfResultContains 10, "", "", "", "", "Urine output at time of removal (ml)"
        SetIndIfResultContains 10, "", "", "", "", "Urine output (mL)"
        SetIndIfResultContains 10, "", "", "", "", "Intake is greater than output"
        SetIndIfResultContains 10, "", "", "", "", "Output is greater than intake"
    If inds(10).checked Then Exit Sub
    
    
    SetIndIfResultContains 10, "", "", "", "", "Total output (ml)"
    SetIndIfResultContains 10, "", "", "", "", "Total urine output (ml)"
    If inds(10).checked Then Exit Sub
    SetIndIfResultContains 10, "", "", "", "", "Urinary catheter present draining; Amount (ml)"
    SetIndIfResultContains 10, "", "", "", "", "Associated with indwelling urinary catheter present; with no urine output"
    SetIndIfResultContains 10, "", "", "", "the indication for urinary catheter", "Strict Ins"
    If inds(10).checked Then Exit Sub
    SetIndIfResultContains 10, "", "", "", "", "Bladder scan performed;"
    SetIndIfResultContains 10, "", "", "", "", "Continuous bladder irrigation initiated;"
    If ResultContains("", "", "", "the indication for urinary catheter", "other") And _
       ResultContains("", "", "", "", "monitor output") Then
        SetInd 10, "Per provider order, indication for urinary cath was: other, monitor output."
    End If
    If inds(10).checked Then Exit Sub
    SetIndIfAllResults 10, "", "", "", "", "IV FLUID infusion%ordered by"
    CheckIVMedInfusion
    SetIndIfResultContains 10, "", "", "ELIMINATION", "", "0,1,2,3,4,5,6,7,8,9"
    If inds(10).checked Then Exit Sub
    SetIndIfResultContains 10, "", "", "", "", "Gastric tube indicated for"
    SetIndIfResultContains 10, "", "", "INTAKE AND OUTPUT", "", "0,1,2,3,4,5,6,7,8,9"
    SetIndIfResultContains 10, "", "", "", "", "Initial paracentesis"
    If inds(10).checked Then Exit Sub
    SetIndIfResultContains 10, "", "", "", "", "Subsequent paracentesis"
    SetIndIfResultContains 10, "", "", "", "", "Open peritoneal lavage performed"
    SetIndIfResultContains 10, "", "", "", "", "Closed peritoneal lavage performed"
    If inds(10).checked Then Exit Sub
    SetIndIfResultContains 10, "", "", "PO CHALLENGE", "", ""
    SetIndIfResultContains 10, "", "", "", "", "Thoracentesis performed to the left chest; (mL)"
    SetIndIfResultContains 10, "", "", "", "", "Thoracentesis performed to the right chest; (mL)"
    If inds(10).checked Then Exit Sub
    SetIndIfResultContains 10, "", "", "", "", "Urine output at time of removal (ml)"
    SetIndIfResultContains 10, "", "", "", "", "Continuous bladder irrigation discontinued at "
    
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_1011"
    Resume  'debug
End Sub
Private Sub BucketFluids()
        PutIntoBuckets "", "", "URINE COLLECTION", "", "output amount (ml)"
        PutIntoBuckets "", "", "URINE COLLECTION", "", "Total urine output (ml)"
        PutIntoBuckets "", "", "URINE COLLECTION", "", "Total output (ml)"
        PutIntoBuckets "", "", "", "", "Urinary catheter present draining; Amount (ml)"
        PutIntoBuckets "", "", "", "", "Associated with indwelling urinary catheter present; with no urine output"
        PutIntoBuckets "", "", "", "", "Urine output at time of removal (ml)"
        PutIntoBuckets "", "", "", "", "Urine output (mL)"
        PutIntoBuckets "", "", "", "", "Intake is greater than output"
        PutIntoBuckets "", "", "", "", "Output is greater than intake"
'    PutIntoBuckets "NURSING PROCEDURE: PROCEDURAL SEDATION", "", "PRE-SEDATION", "", ""
'    PutIntoBuckets "NURSING PROCEDURE: PROCEDURAL SEDATION", "", "PREPROCEDURE ASSESSMENT", "", ""

End Sub
Private Sub Check_1213()
    On Error GoTo errHandler
    Dim ct As Integer
    Dim found_what As String
    Dim reslist As String
    
    dvprint "-----------------------"
    dvprint "ED Visit 12. Physiologic Assessment - q 1 Hr"
    dvprint "ED Visit 13. Physiologic Assessment - q 15 Min"
    dvprint "-----------------------"
    
    If inds(13).checked Then Exit Sub
    SetIndIfResultContains 13, "", "", "", "", "Refer to Chart Plus for sedation documentation"
    
    SetIndIfResultContains 13, "NURSING PROCEDURE: DYSRHYTHMIA INTERVENTIONS", "", "DYSRHYTHMIA INTERVENTIONS", "", ""
    SetIndIfResultContains 13, "NURSING PROCEDURE: DYSRHYTHMIA INTERVENTIONS", "", "DEFIBRILLATION", "", ""
    SetIndIfResultContains 13, "NURSING PROCEDURE: DYSRHYTHMIA INTERVENTIONS", "", "CARDIOVERSION", "", ""
    SetIndIfResultContains 13, "NURSING PROCEDURE: DYSRHYTHMIA INTERVENTIONS", "", "PACING", "", ""
    If inds(13).checked Then Exit Sub
    SetIndIfResultContains 13, "NURSING PROCEDURE: DYSRHYTHMIA INTERVENTIONS", "", EXACT_MATCH_PREFIX & "FOLLOW-UP", "", ""
    SetIndIfResultContains 13, "NURSING PROCEDURE: EXTUBATION", "", EXACT_MATCH_PREFIX & "EXTUBATION", "", ""
    SetIndIfResultContains 13, "NURSING PROCEDURE: EXTUBATION", "", EXACT_MATCH_PREFIX & "FOLLOW-UP", "", ""
    SetIndIfResultContains 13, "NURSING PROCEDURE: INTUBATION", "", EXACT_MATCH_PREFIX & "INTUBATION", "", ""
    SetIndIfResultContains 13, "NURSING PROCEDURE: INTUBATION", "", EXACT_MATCH_PREFIX & "FOLLOW-UP", "", ""
    SetIndIfResultContains 13, "NURSING PROCEDURE: PROCEDURAL SEDATION", "", "", "", ""
    
    SetIndIfResultContains 13, "", "", EXACT_MATCH_PREFIX & "SURGICAL AIRWAY", "", "Tracheostomy"
    SetIndIfResultContains 13, "", "", EXACT_MATCH_PREFIX & "SURGICAL AIRWAY", "", "Cricothyrotomy"
    SetIndIfResultContains 13, "NURSING PROCEDURE: THORACOTOMY", "", "", "", ""
    
    
    
    If inds(13).checked Then Exit Sub
    
    found_what = "Physiologic Assessment items q15"
'    SetupBuckets (15)
b_filter = ""
b_excl = ""
    BucketPhysio
    FillBucket2
    ExecIntervals 13, 20 'ExecPutIntoBuckets
'    If CheckBucketFreq(found_what) Then SetInd 13, found_what
'PrintBuckets
    If inds(13).checked Then Exit Sub

'    SetupBuckets (20)
'    BucketPhysio
'    ExecPutIntoBuckets
'    If CheckBucketFreq(found_what) Then SetInd 13, found_what
'PrintBuckets
'    If inds(13).checked Then Exit Sub

   
    found_what = "Physiologic Assessment items q1Hr"
'    If Check20As60BucketFreq(found_what) Then SetInd 12, found_what
    'SetupBuckets 12, 70
'    BucketPhysio
    ExecIntervals 12, 70
'    If CheckBucketFreq(found_what) Then SetInd 12, found_what
'PrintBuckets
    If inds(12).checked Then Exit Sub

'    SetupBuckets (70)
'    BucketPhysio
'    ExecPutIntoBuckets
'    If CheckBucketFreq(found_what) Then SetInd 12, found_what
'PrintBuckets
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_1213"
    Resume  'debug
End Sub
Private Sub BucketPhysio()
    Dim excl As String
'EXCLUDING these categories:'
'<section>HPI%</section>
'<section>REVIEW OF SYSTEMS</section>
'<section>PHYSICAL EXAM</section>
'<section>I/R/P</section>
'<section>O2SAT INTERPRETATION</section>
'<section>EKG INTERPRETATION</section>
'<section>LAB INTERPRETATION</section>
'<section>RADIOLOGY INTERPRETATION</section>
'<section>ATTENDING</section>
    
    PutIntoBuckets "VITAL SIGNS", "", "", "", ""
    PutIntoBuckets "NURSING ASSESSMENT: ALLERGIC REACTION", "", EXACT_MATCH_PREFIX & "RESPIRATORY", "", ""
    PutIntoBuckets "", "", EXACT_MATCH_PREFIX & "CARDIOVASCULAR", "", ""
    PutIntoBuckets "", "", EXACT_MATCH_PREFIX & "RESPIRATORY/CHEST", "", ""
    PutIntoBuckets "", "", EXACT_MATCH_PREFIX & "CIWA", "", ""
    PutIntoBuckets "", "", EXACT_MATCH_PREFIX & "CARDIAC MONITOR", "", ""
    PutIntoBuckets "", "", EXACT_MATCH_PREFIX & "RESPIRATORY PROCEDURE", "", ""
    PutIntoBuckets "", "", EXACT_MATCH_PREFIX & "OXYGEN THERAPY", "", ""
    PutIntoBuckets "", "", EXACT_MATCH_PREFIX & "CVA ASSESSMENT", "", ""
    PutIntoBuckets "", "", EXACT_MATCH_PREFIX & "FOUR SCORE ASSESSMENT", "", ""
    PutIntoBuckets "", "", EXACT_MATCH_PREFIX & "NEURO", "", ""
    PutIntoBuckets "", "", EXACT_MATCH_PREFIX & "GCS", "", ""
    PutIntoBuckets "", "", EXACT_MATCH_PREFIX & "RESPIRATORY FOLLOW UP", "", ""
    PutIntoBuckets "", "", EXACT_MATCH_PREFIX & "RESPIRATORY PROCEDURES", "", ""
    PutIntoBuckets "", "", EXACT_MATCH_PREFIX & "SEIZURE", "", ""
    PutIntoBuckets "", "", EXACT_MATCH_PREFIX & "RESPIRATORY/CHEST TRAUMA", "", ""
    PutIntoBuckets "", "", EXACT_MATCH_PREFIX & "NEURO PED", "", ""
    PutIntoBuckets "", "", EXACT_MATCH_PREFIX & "BREATHING", "", ""
    PutIntoBuckets "", "", EXACT_MATCH_PREFIX & "CIRCULATION", "", ""
    PutIntoBuckets "", "", EXACT_MATCH_PREFIX & "PARAMETERS", "", ""
    PutIntoBuckets "", "", EXACT_MATCH_PREFIX & "THORACIC TRAUMA", "", ""
    PutIntoBuckets "", "", EXACT_MATCH_PREFIX & "ABG", "", ""
    PutIntoBuckets "", "", "LOWER EXTREMITY", "", "assessment findings include capillary refill"
    PutIntoBuckets "", "", "LOWER EXTREMITY", "", "Skin color"
    PutIntoBuckets "", "", "LOWER EXTREMITY", "", "Skin temperature"
    PutIntoBuckets "", "", "LOWER EXTREMITY", "", "Distal sensation"
    PutIntoBuckets "", "", "LOWER EXTREMITY", "", "posterior tibia pulse is"
    PutIntoBuckets "", "", "LOWER EXTREMITY", "", "dorsalis pedis pulse is"
    PutIntoBuckets "", "", "UPPER EXTREMITY", "", "radial pulse is"
    PutIntoBuckets "", "", "UPPER EXTREMITY", "", "brachial pulse is"
    PutIntoBuckets "", "", "", "", "Respiratory Parameters"
    PutIntoBuckets "", "", EXACT_MATCH_PREFIX & "FOLLOW-UP", "", "After procedure, capillary refill"
    PutIntoBuckets "", "", EXACT_MATCH_PREFIX & "FOLLOW-UP", "", "After procedure, distal circulation"
    PutIntoBuckets "", "", EXACT_MATCH_PREFIX & "FOLLOW-UP", "", "After procedure, distal motor"
    PutIntoBuckets "", "", EXACT_MATCH_PREFIX & "FOLLOW-UP", "", "After procedure, distal sensation"
    PutIntoBuckets "", "", EXACT_MATCH_PREFIX & "FOLLOW-UP", "", "After procedure, change in distal circulation noted"
    PutIntoBuckets "", "", EXACT_MATCH_PREFIX & "FOLLOW-UP", "", "After procedure, change in distal motor function noted"
    PutIntoBuckets "", "", EXACT_MATCH_PREFIX & "FOLLOW-UP", "", "After procedure, change in distal sensation noted"
    PutIntoBuckets "", "", EXACT_MATCH_PREFIX & "FOLLOW-UP", "", "After procedure, distal pulses"
    PutIntoBuckets "NURSING PROCEDURE: NEURO CHECK", "", "", "", "Pupil"
    PutIntoBuckets "NURSING PROCEDURE: NEURO CHECK", "", "", "GCS Total", ""
    
'    PutIntoBuckets "", "", "DYSRHYTHMIA INTERVENTIONS", "", ""
'    PutIntoBuckets "", "", "DEFIBRILLATION", "", ""
'    If CheckBucketFreq(found_what) Then SetInd 13, found_what
'    If inds(13).checked Then Exit Sub
'    PutIntoBuckets "", "", "CARDIOVERSION", "", ""
'    PutIntoBuckets "NURSING PROCEDURE: DYSRHYTHMIA INTERVENTIONS", "", EXACT_MATCH_PREFIX & "FOLLOW-UP", "", ""
'    PutIntoBuckets "", "", EXACT_MATCH_PREFIX & "EXTUBATION", "", ""
'    If CheckBucketFreq(found_what) Then SetInd 13, found_what
'    If inds(13).checked Then Exit Sub
'    PutIntoBuckets "NURSING PROCEDURE: EXTUBATION", "", EXACT_MATCH_PREFIX & "FOLLOW-UP", "", ""
    PutIntoBuckets "", "", "", "", "Fetal heart"
'    PutIntoBuckets "", "", EXACT_MATCH_PREFIX & "INTUBATION", "", ""
'    If CheckBucketFreq(found_what) Then SetInd 13, found_what
'    If inds(13).checked Then Exit Sub
'    PutIntoBuckets "NURSING PROCEDURE: INTUBATION", "", EXACT_MATCH_PREFIX & "FOLLOW-UP", "", ""
    PutIntoBuckets "NURSING PROCEDURE: OXYGEN THERAPY", "", EXACT_MATCH_PREFIX & "FOLLOW-UP", "", ""
    PutIntoBuckets "NURSING PROCEDURE: IV ACCESS", "", "", "", "Neurovascular checks completed"
    PutIntoBuckets "", "", "RESPIRATORY INTERVENTIONS", "", ""
    PutIntoBuckets "NURSING PROCEDURE: RESPIRATORY INTERVENTIONS", "", EXACT_MATCH_PREFIX & "FOLLOW-UP", "", ""
'    PutIntoBuckets "", "", EXACT_MATCH_PREFIX & "SURGICAL AIRWAY", "", "Tracheostomy"
'    PutIntoBuckets "", "", EXACT_MATCH_PREFIX & "SURGICAL AIRWAY", "", "Cricothyrotomy"
    PutIntoBuckets "NURSING PROCEDURE: SURGICAL AIRWAY", "", EXACT_MATCH_PREFIX & "FOLLOW-UP", "", ""
    PutIntoBuckets "NURSING PROCEDURE: THORACENTESIS", "", EXACT_MATCH_PREFIX & "FOLLOW-UP", "", ""
'    PutIntoBuckets "NURSING PROCEDURE: THORACOTOMY", "", "", "", ""
    PutIntoBuckets "", "", "TUBE THORACOSTOMY", "", ""
    PutIntoBuckets "NURSING PROCEDURE: CHEST TUBE PLACEMENT", "", EXACT_MATCH_PREFIX & "FOLLOW-UP", "", ""
    PutIntoBuckets "", "", "", "Ventilator settings", "FIO2"
    PutIntoBuckets "", "", "", "Tidal volume", ""
    PutIntoBuckets "", "", "", "Mode", ""
    PutIntoBuckets "", "", "", "Rate", ""
    PutIntoBuckets "", "", "", "PEEP", ""
    PutIntoBuckets "", "", "", "", "Settings changed"
    PutIntoBuckets "", "", "", "", "End tidal CO2"
    PutIntoBuckets "NURSING PROCEDURE: VENTILATOR", "", EXACT_MATCH_PREFIX & "FOLLOW-UP", "", ""
'    PutIntoBuckets "NURSING PROCEDURE: PROCEDURAL SEDATION", "", "", "", ""
    PutIntoBuckets "", "", "", "Modified RASS Scale", ""
    PutIntoBuckets "NURSING ASSESSMENT: OBSTETRICS", "", "", "", "Fetal Heart Tones obtained via doppler"
    PutIntoBuckets "NURSING ASSESSMENT: OBSTETRICS", "", "", "", "Fetal Heart Tones obtained via ultrasound"
    PutIntoBuckets "NURSING ASSESSMENT: OBSTETRICS", "", "", "", "Baby B Fetal Heart Rate"
    PutIntoBuckets "NURSING ASSESSMENT: OBSTETRICS", "", "", "Fetal Heart Rate", ""
    PutIntoBuckets "NURSING ASSESSMENT: OBSTETRICS", "", "", "", "Fetal Heart rate variability absent"
    PutIntoBuckets "NURSING ASSESSMENT: OBSTETRICS", "", "", "", "Fetal Heart rate variability minimal"
    PutIntoBuckets "NURSING ASSESSMENT: OBSTETRICS", "", "", "", "Fetal Heart rate variability moderate"
    PutIntoBuckets "NURSING ASSESSMENT: OBSTETRICS", "", "", "", "Fetal Heart rate variability marked"
    PutIntoBuckets "NURSING ASSESSMENT: OBSTETRICS", "", "", "", "Accelerations present"
    PutIntoBuckets "NURSING ASSESSMENT: OBSTETRICS", "", "", "", "Accelerations absent"
    PutIntoBuckets "NURSING ASSESSMENT: OBSTETRICS", "", "", "", "Decelerations present"
    PutIntoBuckets "NURSING ASSESSMENT: OBSTETRICS", "", "", "", "Early decelerations"
    PutIntoBuckets "NURSING ASSESSMENT: OBSTETRICS", "", "", "", "Variable decelerations"
    PutIntoBuckets "NURSING ASSESSMENT: OBSTETRICS", "", "", "", "Late decelerations"
    
    PutIntoBuckets "", "", "POST ANGIOGRAM FOLLOW-UP", "", "dorsalis pedis pulse"
    PutIntoBuckets "", "", "POST ANGIOGRAM FOLLOW-UP", "", "radial pulse"
    PutIntoBuckets "", "", "POST ANGIOGRAM FOLLOW-UP", "", "color of extremity distal to site"
    PutIntoBuckets "", "", "POST ANGIOGRAM FOLLOW-UP", "", "temperature of extremity distal to site"
    PutIntoBuckets "", "", "POST ANGIOGRAM FOLLOW-UP", "", "capillary refill less than 2 seconds"
    PutIntoBuckets "", "", "POST ANGIOGRAM FOLLOW-UP", "", "capillary refill greater than 2 seconds"
    PutIntoBuckets "", "", "POST ANGIOGRAM FOLLOW-UP", "", "distal motor intact"
    PutIntoBuckets "", "", "POST ANGIOGRAM FOLLOW-UP", "", "change in distal motor function noted"
    PutIntoBuckets "", "", "POST ANGIOGRAM FOLLOW-UP", "", "distal sensation intact"
    PutIntoBuckets "", "", "POST ANGIOGRAM FOLLOW-UP", "", "change in distal sensation noted"
'<section>HPI%</section>
'<section>REVIEW OF SYSTEMS</section>
'<section>PHYSICAL EXAM</section>
'<section>I/R/P</section>
'<section>O2SAT INTERPRETATION</section>
'<section>EKG INTERPRETATION</section>
'<section>LAB INTERPRETATION</section>
'<section>RADIOLOGY INTERPRETATION</section>
'<section>ATTENDING</section>
    excl = " and category not like 'HPI%'"
    excl = excl & " and category<>'REVIEW OF SYSTEMS'"
    excl = excl & " and category<>'PHYSICAL EXAM'"
    excl = excl & " and category<>'I/R/P'"
    excl = excl & " and category<>'O2SAT INTERPRETATION'"
    excl = excl & " and category<>'EKG INTERPRETATION'"
    excl = excl & " and category<>'LAB INTERPRETATION'"
    excl = excl & " and category<>'RADIOLOGY INTERPRETATION'"
    excl = excl & " and category<>'ATTENDING'"
    b_excl = excl

End Sub


Private Sub QMedService()
    Dim sql As String, s As String
    Dim rs As New Recordset
    Dim ct As Integer
    Dim done_ct As Integer
    Dim mintime As Date
    Dim maxtime As Date
    Dim done As Boolean
    
    'SetIndIfResultContains 14, "MEDICATION SERVICE", "", "", "", "Documented as given by"

    sql = "select description,result from chart_item " & WhereBase & AndSimpleItemFilter("MEDICATION SERVICE", "", "", "", "Documented as given by")
    sql = sql & " and description not like '%sodium chloride%'"
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        If Not IsNull(rs(0)) Then s = rs(0)
        If Not IsNull(rs(1)) Then s = s & rs(1)
        SetInd 14, "Med Service with doc as given by: " & s
    End If
    rs.Close
End Sub

Private Sub Check_14()
    On Error GoTo errHandler
    Dim found_what As String
    Dim reslist As String
    
    dvprint "-----------------------"
    dvprint "ED Visit 14. Medication Management"
    dvprint "-----------------------"
    If inds(14).checked Then Exit Sub
    SetIndIfResultContains 14, "", "", "", "", "Refer to Chart Plus for sedation documentation"

    SetIndIfResultContains 14, "", "", "", "", "Treatments in progress include medication administered"
    SetIndIfResultContains 14, "", "", "", "", "Assess response of medications PTA"
    'following needs to exclude if description like sodium chloride
    'SetIndIfResultContains 14, "MEDICATION SERVICE", "", "", "", "Documented as given by"
    QMedService
    SetIndIfResultContains 14, "", "", EXACT_MATCH_PREFIX & "PRE-SEDATION", "", ""
    If inds(14).checked Then Exit Sub
    
'    SetIndIfAllResults 14, "", "", "PAIN", "", "Pain relieved by,Acetaminophen"
'    SetIndIfAllResults 14, "", "", "PAIN", "", "Pain relieved by,antibiotics"
'    SetIndIfAllResults 14, "", "", "PAIN", "", "Pain relieved by,antihistamines"
'    If inds(14).checked Then Exit Sub
'    SetIndIfAllResults 14, "", "", "PAIN", "", "Pain relieved by,Aspirin"
'    SetIndIfAllResults 14, "", "", "PAIN", "", "Pain relieved by,Epinephrine"
'    SetIndIfAllResults 14, "", "", "PAIN", "", "Pain relieved by,prescription medications"
'    If inds(14).checked Then Exit Sub
'    SetIndIfAllResults 14, "", "", "PAIN", "", "Pain relieved by,Ibuprofen"
'    SetIndIfAllResults 14, "", "", "PAIN", "", "Pain relieved by,inhalers"
'    SetIndIfAllResults 14, "", "", "PAIN", "", "Pain relieved by,IV fluids"
'    If inds(14).checked Then Exit Sub
'    SetIndIfAllResults 14, "", "", "PAIN", "", "Pain relieved by,nebulizer treatment"
'    SetIndIfAllResults 14, "", "", "PAIN", "", "Pain relieved by,Nitroglycerine"
'    SetIndIfAllResults 14, "", "", "PAIN", "", "Pain relieved by,opiate medications"
'    If inds(14).checked Then Exit Sub
'    SetIndIfAllResults 14, "", "", "PAIN", "", "Pain relieved by,over the counter medications"
'    SetIndIfAllResults 14, "", "", "PAIN", "", "Pain relieved by,pain medications"
'    SetIndIfAllResults 14, "", "", "PAIN", "", "Pain relieved by,sedative or hypnotic medications"
'    If inds(14).checked Then Exit Sub
'    SetIndIfAllResults 14, "", "", "PAIN", "", "Pain relieved by,steroids"
'    SetIndIfAllResults 14, "", "", "PAIN", "", "Pain relieved by,stool softeners"
'    SetIndIfAllResults 14, "", "", "PAIN", "", "Pain relieved by,topical anesthetics"
'    If inds(14).checked Then Exit Sub
'    SetIndIfAllResults 14, "", "", "PAIN", "", "Pain relieved by,topical medications"
    
    SetIndIfResultContains 14, "", "", "", "", "Patient given ALBUTEROL"
    SetIndIfResultContains 14, "", "", "", "", "Patient given ATROVENT"
    If inds(14).checked Then Exit Sub
    SetIndIfResultContains 14, "", "", "", "", "Patient given ALBUTEROL with ATROVENT"
    SetIndIfResultContains 14, "", "", "", "", "Patient given XOPENEX"
    SetIndIfResultContains 14, "", "", "", "", "Patient given COMBIVENT"
    If inds(14).checked Then Exit Sub
    SetIndIfResultContains 14, "", "", "", "", "Patient given RACEMIC EPINEPHRINE"


    SetIndIfResultContains 14, "", "", "", "", "Symptoms relieved by antihistamine"
    If inds(14).checked Then Exit Sub
    SetIndIfResultContains 14, "", "", "", "", "Symptoms relieved by benadryl"
    SetIndIfResultContains 14, "", "", "", "", "Symptoms relieved by epi-pen use"
    SetIndIfResultContains 14, "", "", "", "", "Symptoms relieved by over the counter medications"
    If inds(14).checked Then Exit Sub
    
    SetIndIfAllResults 14, "", "", "", "", "Poison control,Contacted for accidental ingestion"
    SetIndIfAllResults 14, "", "", "", "", "Poison control,Contacted for intentional ingestion"
    If inds(14).checked Then Exit Sub
    
    SetIndIfResultContains 14, "", "", "", "", "Topical anesthesia applied;"
    SetIndIfResultContains 14, "", "", "", "", "Local infiltration with;"
    SetIndIfResultContains 14, "", "", "", "", "Thrombolytic administration indicated for"
    SetIndIfResultContains 14, "", "", EXACT_MATCH_PREFIX & "NITROUS OXIDE ADMINISTRATION", "", ""
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_14"
    Resume  'debug
End Sub

Private Sub Check_15()
    
    On Error GoTo errHandler
    
    dvprint "-----------------------"
    dvprint "ED Visit 15. Wound/Injury Management"
    dvprint "-----------------------"
    
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "", "", "", "", "bleeding;"
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "", "", "", "Blood output(ml)", ""
    SetIndIfResultContains 15, "", "", "", "Chest Tube Right output(ml)", ""
    SetIndIfResultContains 15, "", "", "", "Chest Tube Left output(ml)", ""
    SetIndIfResultContains 15, "", "", "", "Drains/tubes output(ml)", ""
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "WOUND CARE", "", ""
    SetIndIfResultContains 15, "NURSING PROCEDURE: WOUND CARE", "", EXACT_MATCH_PREFIX & "FOLLOW-UP", "", ""
    If inds(15).checked Then Exit Sub
    
    SetIndIfResultContains 15, "NURSING ASSESSMENT: ABDOMEN TRAUMA", "", EXACT_MATCH_PREFIX & "MECHANISM OF INJURY", "", ""
    SetIndIfResultContains 15, "NURSING ASSESSMENT: BACK TRAUMA", "", EXACT_MATCH_PREFIX & "MECHANISM OF INJURY", "", ""
    SetIndIfResultContains 15, "NURSING ASSESSMENT: EXTREMITY LOWER TRAUMA", "", EXACT_MATCH_PREFIX & "MECHANISM OF INJURY", "", ""
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "NURSING ASSESSMENT: EXTREMITY UPPER TRAUMA", "", EXACT_MATCH_PREFIX & "MECHANISM OF INJURY", "", ""
    SetIndIfResultContains 15, "NURSING ASSESSMENT: EYE TRAUMA", "", EXACT_MATCH_PREFIX & "MECHANISM OF INJURY", "", ""
    SetIndIfResultContains 15, "NURSING ASSESSMENT: GENITOURINARY TRAUMA", "", EXACT_MATCH_PREFIX & "MECHANISM OF INJURY", "", ""
    SetIndIfResultContains 15, "NURSING ASSESSMENT: NASAL TRAUMA", "", EXACT_MATCH_PREFIX & "MECHANISM OF INJURY", "", ""
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "NURSING ASSESSMENT: NECK TRAUMA", "", EXACT_MATCH_PREFIX & "MECHANISM OF INJURY", "", ""
    SetIndIfResultContains 15, "NURSING ASSESSMENT: NEURO TRAUMA", "", EXACT_MATCH_PREFIX & "MECHANISM OF INJURY", "", ""
    SetIndIfResultContains 15, "NURSING ASSESSMENT: PRIMARY SURVEY TRAUMA", "", EXACT_MATCH_PREFIX & "MECHANISM OF INJURY", "", ""
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "NURSING ASSESSMENT: RESPIRATORY/CHEST TRAUMA", "", EXACT_MATCH_PREFIX & "MECHANISM OF INJURY", "", ""
    SetIndIfResultContains 15, "NURSING ASSESSMENT: SECONDARY SURVEY TRAUMA", "", EXACT_MATCH_PREFIX & "MECHANISM OF INJURY", "", ""
    SetIndIfResultContains 15, "NURSING ASSESSMENT: SKIN TRAUMA", "", EXACT_MATCH_PREFIX & "MECHANISM OF INJURY", "", ""
    SetIndIfResultContains 15, "NURSING ASSESSMENT: TRAUMA TRIAGE", "", EXACT_MATCH_PREFIX & "MECHANISM OF INJURY", "", ""
    If inds(15).checked Then Exit Sub
    
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "GENITOURINARY MALE TRAUMA", "", ""
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "GENITOURINARY FEMALE TRAUMA", "", ""
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "BACK TRAUMA", "", ""
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "NECK TRAUMA", "", ""
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "LOWER EXTREMITY TRAUMA", "", ""
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "EXTREMITY UPPER TRAUMA", "", ""
    SetIndIfResultContains 15, "NURSING ASSESSMENT: EYE TRAUMA", "", EXACT_MATCH_PREFIX & "EYES", "", ""
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "PELVIC TRAUMA MALE", "", ""
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "PELVIC TRAUMA FEMALE", "", ""
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "ABDOMEN TRAUMA", "", ""
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "NASAL TRAUMA", "", ""
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "RESPIRATORY/CHEST TRAUMA", "", ""
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "HEAD TRAUMA", "", ""
    If inds(15).checked Then Exit Sub
  
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "BURN LOCATION", "", ""
    SetIndIfResultContains 15, "NURSING PROCEDURE: ARTERIAL LINE", "", EXACT_MATCH_PREFIX & "ARTERIAL LINE", "", ""
    SetIndIfResultContains 15, "NURSING PROCEDURE: ARTERIAL LINE", "", EXACT_MATCH_PREFIX & "FOLLOW-UP", "", ""
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "ASSISTED MD", "", ""
    SetIndIfResultContains 15, "NURSING PROCEDURE: BURN CARE", "", EXACT_MATCH_PREFIX & "BURN CARE", "", ""
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "NURSING PROCEDURE: BURN CARE", "", EXACT_MATCH_PREFIX & "FOLLOW-UP", "", ""
    SetIndIfResultContains 15, "NURSING PROCEDURE: DRESSING", "", EXACT_MATCH_PREFIX & "DRESSING", "", ""
    SetIndIfResultContains 15, "NURSING PROCEDURE: DRESSING", "", EXACT_MATCH_PREFIX & "FOLLOW-UP", "", ""
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "NURSING PROCEDURE: GARDNER WELLS TONGS", "", EXACT_MATCH_PREFIX & "FOLLOW-UP", "", ""
    SetIndIfResultContains 15, "NURSING PROCEDURE: ICP MONITORING", "", EXACT_MATCH_PREFIX & "ICP MONITORING", "", ""
    SetIndIfResultContains 15, "NURSING PROCEDURE: ICP MONITORING", "", EXACT_MATCH_PREFIX & "FOLLOW-UP", "", ""
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "NURSING PROCEDURE: INCISION AND DRAINAGE", "", EXACT_MATCH_PREFIX & "I & D", "", ""
    SetIndIfResultContains 15, "NURSING PROCEDURE: INCISION AND DRAINAGE", "", EXACT_MATCH_PREFIX & "FOLLOW-UP", "", ""
    SetIndIfResultContains 15, "NURSING PROCEDURE: PERITONEAL LAVAGE", "", EXACT_MATCH_PREFIX & "PERITONEAL LAVAGE", "", ""
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "NURSING PROCEDURE: PERITONEAL LAVAGE", "", EXACT_MATCH_PREFIX & "FOLLOW-UP", "", ""
    SetIndIfResultContains 15, "NURSING PROCEDURE: SPLINTING", "", EXACT_MATCH_PREFIX & "SPLINTING", "", ""
    SetIndIfResultContains 15, "NURSING PROCEDURE: SPLINTING", "", EXACT_MATCH_PREFIX & "FOLLOW-UP", "", ""
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "NURSING PROCEDURE: SUBUNGUAL HEMATOMA", "", EXACT_MATCH_PREFIX & "FOLLOW-UP", "", ""
    SetIndIfResultContains 15, "NURSING PROCEDURE: SUTURE/STAPLE REMOVAL", "", EXACT_MATCH_PREFIX & "SUTURE/STAPLE REMOVAL", "", ""
    SetIndIfResultContains 15, "NURSING PROCEDURE: SUTURE/STAPLE REMOVAL", "", EXACT_MATCH_PREFIX & "FOLLOW-UP", "", ""
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "NURSING PROCEDURE: THORACENTESIS", "", EXACT_MATCH_PREFIX & "THORACENTESIS", "", ""
    SetIndIfResultContains 15, "NURSING PROCEDURE: THORACENTESIS", "", EXACT_MATCH_PREFIX & "FOLLOW-UP", "", ""
    SetIndIfResultContains 15, "NURSING PROCEDURE: VENTRICULOSTOMY", "", EXACT_MATCH_PREFIX & "VENTRICULOSTOMY", "", ""
    SetIndIfResultContains 15, "NURSING PROCEDURE: VENTRICULOSTOMY", "", EXACT_MATCH_PREFIX & "FOLLOW-UP", "", ""
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "NURSING PROCEDURE: JOINT CARE", "", EXACT_MATCH_PREFIX & "FOLLOW-UP", "", ""
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "LUMBAR PUNCTURE", "", ""
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "THORACOTOMY", "", ""
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "TRACHEOSTOMY CARE", "", ""
    If inds(15).checked Then Exit Sub
    
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "TRIAGE INTERVENTIONS", "", "C-Collar/Spinal Immobilization"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "TRIAGE INTERVENTIONS", "", "Elevation"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "TRIAGE INTERVENTIONS", "", "Ice"
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "", "", "", "", "Treatments in progress include spinal precautions"
    SetIndIfResultContains 15, "", "", "", "", "Treatments in progress include dressing"
    SetIndIfResultContains 15, "", "", "", "", "Treatments in progress include splint"
    SetIndIfResultContains 15, "", "", "ABDOMEN", "", "Incision(s);"
    SetIndIfResultContains 15, "", "", "ABDOMEN", "", "Lesions;"
    If inds(15).checked Then Exit Sub
    'SetIndIfResultContains 15, "", "", "GENITOURINARY FEMALE", "", "Associated with vaginal discharge present"
    'SetIndIfResultContains 15, "", "", "GENITOURINARY FEMALE", "", "Associated with vaginal bleeding described as"
    SetIndIfResultContains 15, "", "", "GENITOURINARY FEMALE", "", "Associated with vaginal foreign body"
    SetIndIfResultContains 15, "", "", "GENITOURINARY MALE", "", "Associated with foreign body"
    SetIndIfResultContains 15, "", "", "", "", "Urostomy site care given"
    If inds(15).checked Then Exit Sub
    'SetIndIfResultContains 15, "", "", "", "indication for urinary catheter", "Incontinent/Promote Wound Healing"
    SetIndIfResultContains 15, "", "", "GENITOURINARY", "", "with laceration;"
    SetIndIfResultContains 15, "", "", "GENITOURINARY", "", "with lesion(s);"
    SetIndIfResultContains 15, "", "", "", "", "urine ketchup in color"
    SetIndIfResultContains 15, "", "", "", "", "urine cherry in color"
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "", "", "", "", "Continuous bladder irrigation initiated"
    
    SetIndIfAllResults 15, "", "", "PELVIC EXAM", "", "Interventions include,packing inserted"
    SetIndIfAllResults 15, "", "", "PELVIC EXAM", "", "Interventions include,cautery performed"
    SetIndIfAllResults 15, "", "", "PELVIC EXAM", "", "Interventions include,suctioning"
    SetIndIfAllResults 15, "", "", "PELVIC EXAM", "", "Interventions include,curettage"
    If inds(15).checked Then Exit Sub
    SetIndIfAllResults 15, "", "", "PELVIC EXAM", "", "Interventions include,foreign body removal:"
    SetIndIfAllResults 15, "", "", "PELVIC EXAM", "", "Interventions include,intrauterine device removal"
    SetIndIfResultContains 15, "", "", "", "", "Hemoccult/Gastroccult indicated for patient vomiting blood"
    SetIndIfResultContains 15, "", "", "", "", "Hemoccult/Gastroccult indicated for tarry or bloody stools"
    If inds(15).checked Then Exit Sub
    
    SetIndIfAllResults 15, "", "", "PELVIC EXAM", "", "Exam findings include bleeding,with clots"
    SetIndIfAllResults 15, "", "", "PELVIC EXAM", "", "Exam findings include bleeding,with tissue"
    
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "RESPIRATORY", "", "Crepitus;"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "RESPIRATORY", "", "Subcutaneous emphysema;"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "RESPIRATORY", "", "Deformity;"
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "RESPIRATORY/CHEST", "", "Crepitus;"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "RESPIRATORY/CHEST", "", "Subcutaneous emphysema;"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "RESPIRATORY/CHEST", "", "Deformity;"
    If inds(15).checked Then Exit Sub
    
    SetIndIfResultContains 15, "", "", "SKIN", "", "Inspection findings include abrasion"
    SetIndIfResultContains 15, "", "", "SKIN", "", "Inspection findings include bite marks"
    SetIndIfResultContains 15, "", "", "SKIN", "", "Inspection findings include bleeding varicosity"
    SetIndIfResultContains 15, "", "", "SKIN", "", "Inspection findings include burn"
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "", "", "SKIN", "", "Inspection findings include ecchymosis"
    SetIndIfResultContains 15, "", "", "SKIN", "", "Inspection findings include foreign body"
    SetIndIfResultContains 15, "", "", "SKIN", "", "Inspection findings include incision(s)"
    SetIndIfResultContains 15, "", "", "SKIN", "", "Inspection findings include laceration"
    SetIndIfResultContains 15, "", "", "SKIN", "", "Inspection findings include lesion(s)"
    SetIndIfResultContains 15, "", "", "SKIN", "", "Inspection findings include pressure ulcer"
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "", "", "SKIN", "", "Inspection findings include puncture wound"
    SetIndIfResultContains 15, "", "", "SKIN", "", "Inspection findings include redness"
    SetIndIfResultContains 15, "", "", "SKIN", "", "Inspection findings include signs of infection"
    SetIndIfResultContains 15, "", "", "SKIN", "", "Inspection findings include signs of trauma"
    SetIndIfResultContains 15, "", "", "SKIN", "", "Inspection findings include swelling"
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "", "", "SKIN", "Inspection findings include", "Pressure ulcer"
    
    SetIndIfResultContains 15, "", "", "EXTREMITY", "", "Inspection findings include abrasion"
    SetIndIfResultContains 15, "", "", "EXTREMITY", "", "Inspection findings include bite marks"
    SetIndIfResultContains 15, "", "", "EXTREMITY", "", "Inspection findings include bleeding varicosity"
    SetIndIfResultContains 15, "", "", "EXTREMITY", "", "Inspection findings include burn"
    SetIndIfResultContains 15, "", "", "EXTREMITY", "", "Inspection findings include ecchymosis"
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "", "", "EXTREMITY", "", "Inspection findings include foreign body"
    SetIndIfResultContains 15, "", "", "EXTREMITY", "", "Inspection findings include incision(s)"
    SetIndIfResultContains 15, "", "", "EXTREMITY", "", "Inspection findings include laceration"
    SetIndIfResultContains 15, "", "", "EXTREMITY", "", "Inspection findings include lesion(s)"
    SetIndIfResultContains 15, "", "", "EXTREMITY", "", "Inspection findings include pressure ulcer"
    SetIndIfResultContains 15, "", "", "EXTREMITY", "", "Inspection findings include puncture wound"
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "", "", "EXTREMITY", "", "Inspection findings include redness"
    SetIndIfResultContains 15, "", "", "EXTREMITY", "", "Inspection findings include signs of infection"
    SetIndIfResultContains 15, "", "", "EXTREMITY", "", "Inspection findings include signs of trauma"
    SetIndIfResultContains 15, "", "", "EXTREMITY", "", "Inspection findings include swelling"
    SetIndIfResultContains 15, "", "", "EXTREMITY", "Inspection findings include", "Pressure ulcer"
    If inds(15).checked Then Exit Sub
    
    SetIndIfResultContains 15, "", "", "TRAUMA", "", "Inspection findings include abrasion"
    SetIndIfResultContains 15, "", "", "TRAUMA", "", "Inspection findings include bite marks"
    SetIndIfResultContains 15, "", "", "TRAUMA", "", "Inspection findings include bleeding varicosity"
    SetIndIfResultContains 15, "", "", "TRAUMA", "", "Inspection findings include burn"
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "", "", "TRAUMA", "", "Inspection findings include ecchymosis"
    SetIndIfResultContains 15, "", "", "TRAUMA", "", "Inspection findings include foreign body"
    SetIndIfResultContains 15, "", "", "TRAUMA", "", "Inspection findings include incision(s)"
    SetIndIfResultContains 15, "", "", "TRAUMA", "", "Inspection findings include laceration"
    SetIndIfResultContains 15, "", "", "TRAUMA", "", "Inspection findings include lesion(s)"
    SetIndIfResultContains 15, "", "", "TRAUMA", "", "Inspection findings include pressure ulcer"
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "", "", "TRAUMA", "", "Inspection findings include puncture wound"
    SetIndIfResultContains 15, "", "", "TRAUMA", "", "Inspection findings include redness"
    SetIndIfResultContains 15, "", "", "TRAUMA", "", "Inspection findings include signs of infection"
    SetIndIfResultContains 15, "", "", "TRAUMA", "", "Inspection findings include signs of trauma"
    SetIndIfResultContains 15, "", "", "TRAUMA", "", "Inspection findings include swelling"
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "", "", "TRAUMA", "Inspection findings include", "Pressure ulcer"
    
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "ENT", "", "Foreign body;"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "EAR", "", "Foreign body;"
    
    SetIndIfAllResults 15, "", "", "", "", "Teeth abnormal:,tooth"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "EAR", "", "Drainage from"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "ENT", "", "Drainage from"
    If inds(15).checked Then Exit Sub
    
    SetIndIfResultContains 15, "", "", "", "", "Tracheostomy present,Gastric tube present,Jejunostomy tube present,PEG tube present,Nephrostomy present,Suprapubic catheter present"
    SetIndIfResultContains 15, "", "", "", "", "Dialysis port present,Dialysis shunt present,Peritoneal dialysis catheter present,Central line catheter present"
    SetIndIfResultContains 15, "", "", "", "", "Port-a-cath present,Peripherally inserted central catheter present"
    If inds(15).checked Then Exit Sub
    
    SetIndIfResultContains 15, "", "", "EXTREMITY", "", "Inspection findings include amputation,Inspection findings include deformity,Inspection findings include external rotation of hip,Inspection findings include shortening of leg"

    
    
    SetIndIfAllResults 15, "", "", EXACT_MATCH_PREFIX & "EYES", "", "Associated with exposure to,acidic fluid"
    SetIndIfAllResults 15, "", "", EXACT_MATCH_PREFIX & "EYES", "", "Associated with exposure to,basic fluid"
    SetIndIfAllResults 15, "", "", EXACT_MATCH_PREFIX & "EYES", "", "Associated with exposure to,body fluids"
    SetIndIfAllResults 15, "", "", EXACT_MATCH_PREFIX & "EYES", "", "Associated with exposure to,unknown fluid"
    If inds(15).checked Then Exit Sub
    'SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "EYES", "", "Associated with drainage"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "EYES", "", "Associated with foreign body"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "EYES", "", "Foreign body sensation;"
    If inds(15).checked Then Exit Sub
    
'    SetIndIfResultContains 15, "", "", "", "", "Focused genitourinary assessment findings include complaint of blood in urine"
'    SetIndIfResultContains 15, "", "", "", "", "Focused genitourinary assessment findings include complaint of vaginal bleeding"
    SetIndIfResultContains 15, "", "", "", "", "Focused laceration assessment findings include laceration to"
    SetIndIfResultContains 15, "", "", "", "", "Focused musculoskeletal assessment findings include deformity"
    If inds(15).checked Then Exit Sub
    
    SetIndIfResultContains 15, "", "", "GENITOURINARY", "", "with bite marks;"
    SetIndIfResultContains 15, "", "", "GENITOURINARY", "", "with bruising;"
    SetIndIfResultContains 15, "", "", "GENITOURINARY", "", "with contusion;"
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "", "", "GENITOURINARY", "", "with erythema;"
    SetIndIfResultContains 15, "", "", "GENITOURINARY", "", "with laceration;"
    SetIndIfResultContains 15, "", "", "GENITOURINARY", "", "with lesion(s);"
    SetIndIfResultContains 15, "", "", "GENITOURINARY", "", "with swelling;"
    SetIndIfResultContains 15, "", "", "GENITOURINARY", "", "with signs of infection;"
    If inds(15).checked Then Exit Sub
    
    
    SetIndIfAllResults 15, "", "", "", "", "Scrotum,swollen on the"
    SetIndIfAllResults 15, "", "", "", "", "Scrotum,tender on the"
    SetIndIfAllResults 15, "", "", "", "", "Scrotum,discolored on the"
    SetIndIfAllResults 15, "", "", "", "", "Scrotum,with redness on the"
    If inds(15).checked Then Exit Sub
    SetIndIfAllResults 15, "", "", "", "", "Testicles,enlarged on the"
    SetIndIfAllResults 15, "", "", "", "", "Testicles,firm on the"
    SetIndIfAllResults 15, "", "", "", "", "Testicles,red on the"
    SetIndIfAllResults 15, "", "", "", "", "Testicles,swollen on the"
    SetIndIfAllResults 15, "", "", "", "", "Testicles,tender on the"
    If inds(15).checked Then Exit Sub
    
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "NOSE", "", "Associated with bleeding"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "NOSE", "", "Associated with foreign body"
    If inds(15).checked Then Exit Sub
    
    SetIndIfAllResults 15, "", "", EXACT_MATCH_PREFIX & "PAIN", "", "Pain relieved by,cool soaks"
    SetIndIfAllResults 15, "", "", EXACT_MATCH_PREFIX & "PAIN", "", "Pain relieved by,cool compresses"
    SetIndIfAllResults 15, "", "", EXACT_MATCH_PREFIX & "PAIN", "", "Pain relieved by,heat"
    If inds(15).checked Then Exit Sub
    SetIndIfAllResults 15, "", "", EXACT_MATCH_PREFIX & "PAIN", "", "Pain relieved by,ice"
    SetIndIfAllResults 15, "", "", EXACT_MATCH_PREFIX & "PAIN", "", "Pain relieved by,immobilization"
    SetIndIfAllResults 15, "", "", EXACT_MATCH_PREFIX & "PAIN", "", "Pain relieved by,tepid bath"
    If inds(15).checked Then Exit Sub
    
    SetIndIfResultContains 15, "NURSING ASSESSMENT: SEXUAL ASSAULT", "", "", "", "vaginal contact occurred with penis"
    SetIndIfResultContains 15, "NURSING ASSESSMENT: SEXUAL ASSAULT", "", "", "", "Vaginal contact occurred with finger"
    SetIndIfResultContains 15, "NURSING ASSESSMENT: SEXUAL ASSAULT", "", "", "", "Vaginal contact occurred with foreign object"
    SetIndIfResultContains 15, "NURSING ASSESSMENT: SEXUAL ASSAULT", "", "", "", "Anal contact occurred with penis"
    SetIndIfResultContains 15, "NURSING ASSESSMENT: SEXUAL ASSAULT", "", "", "", "Anal contact occurred with finger"
    SetIndIfResultContains 15, "NURSING ASSESSMENT: SEXUAL ASSAULT", "", "", "", "Anal contact occurred with foreign object"
    If inds(15).checked Then Exit Sub
    
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "ABDOMEN TRAUMA", "", "Abdominal trauma assessment findings include evisceration to"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "ABDOMEN TRAUMA", "", "Pelvis unstable;"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "ABDOMEN TRAUMA", "", "Bleeding;"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "ABDOMEN TRAUMA", "", "no tone to rectal sphincter;"
    If inds(15).checked Then Exit Sub
    
    SetIndIfResultContains 15, "NURSING ASSESSMENT: NASAL TRAUMA", "", "", "", "Associated with nasal discharge"
    If inds(15).checked Then Exit Sub
    
    SetIndIfResultContains 15, "NURSING ASSESSMENT: NEURO TRAUMA", "", "", "", "Teeth abnormal:"
    If inds(15).checked Then Exit Sub
    
    
    
    SetIndIfResultContains 15, "", "", "AIRWAY AND C-SPINE", "", "Spine immobilized"
    SetIndIfResultContains 15, "", "", "AIRWAY AND C-SPINE", "", "Maxillofacial trauma present"
    SetIndIfResultContains 15, "", "", "AIRWAY AND C-SPINE", "", "Cervical spine tenderness"
    If inds(15).checked Then Exit Sub
    
    
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "ENT TRAUMA", "", "Head trauma assessment findings include Battle"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "ENT TRAUMA", "", "Raccoon eyes;"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "ENT TRAUMA", "", "Malocclusion of jaw;"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "ENT TRAUMA", "", "Trismus present;"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "ENT TRAUMA", "", "Bleeding;"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "HEAD TRAUMA", "", "Head trauma assessment findings include Battle"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "HEAD TRAUMA", "", "Raccoon eyes;"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "HEAD TRAUMA", "", "Malocclusion of jaw;"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "HEAD TRAUMA", "", "Trismus present;"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "HEAD TRAUMA", "", "Bleeding;"
    If inds(15).checked Then Exit Sub
    
    SetIndIfResultContains 15, "NURSING ASSESSMENT: SECONDARY SURVEY TRAUMA", "", "", "", "Pupils not equally round and reactive to light"
    SetIndIfResultContains 15, "NURSING ASSESSMENT: SECONDARY SURVEY TRAUMA", "", "", "", "Teeth abnormal:"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "DENTAL", "", "Associated with malocclusion of jaw"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "DENTAL", "", "Associated with bleeding"
    SetIndIfAllResults 15, "", "", EXACT_MATCH_PREFIX & "DENTAL", "", "Associated with,swelling to the"
    If inds(15).checked Then Exit Sub
    
    SetIndIfResultContains 15, "", "", "", "", "trauma assessment findings include bleeding"
    SetIndIfResultContains 15, "", "", "", "", "Thoracic trauma assessment findings include chest wall movements asymmetrical"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "THORACIC TRAUMA", "", "Flail segment;"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "THORACIC TRAUMA", "", "Bleeding;"
    SetIndIfResultContains 15, "", "", "", "", "genitourinary trauma assessment findings include blood at meatus"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "RESPIRATORY/CHEST", "", "Deformity;"
    SetIndIfResultContains 15, "NURSING ASSESSMENT: SECONDARY SURVEY TRAUMA", "", EXACT_MATCH_PREFIX & "ABDOMEN", "", "Ecchymosis;"
    If inds(15).checked Then Exit Sub
    
    SetIndIfResultContains 15, "NURSING ASSESSMENT: SECONDARY SURVEY TRAUMA", "", "GENITOURINARY MALE", "", "Associated with discharge;"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "PAIN", "", "Pain relieved by; elevation"
    SetIndIfResultContains 15, "", "", "NURSES NOTES", "", "Ice packs applied"
    SetIndIfResultContains 15, "", "", "NURSES NOTES", "", "Extremity elevated"
    SetIndIfResultContains 15, "", "", "NURSES NOTES", "", "Warm packs applied"
    If inds(15).checked Then Exit Sub
    If ResultContains("NURSING PROCEDURE: ABG", "", EXACT_MATCH_PREFIX & "ABG", "", "brachial artery") _
      And ResultContains("NURSING PROCEDURE: ABG", "", EXACT_MATCH_PREFIX & "FOLLOW-UP", "", "") Then
        SetInd 15, "NURSING PROCEDURE: ABG + FOLLOW-UP + brachial artery"
    End If
    If ResultContains("NURSING PROCEDURE: ABG", "", EXACT_MATCH_PREFIX & "ABG", "", "radial artery") _
      And ResultContains("NURSING PROCEDURE: ABG", "", EXACT_MATCH_PREFIX & "FOLLOW-UP", "", "") Then
        SetInd 15, "NURSING PROCEDURE: ABG + FOLLOW-UP + radial artery"
    End If
    If ResultContains("NURSING PROCEDURE: ABG", "", EXACT_MATCH_PREFIX & "ABG", "", "femoral artery") _
      And ResultContains("NURSING PROCEDURE: ABG", "", EXACT_MATCH_PREFIX & "FOLLOW-UP", "", "") Then
        SetInd 15, "NURSING PROCEDURE: ABG + FOLLOW-UP + femoral artery"
    End If
      
'    SetIndIfResultContains 15, "NURSING PROCEDURE: ABG", "", EXACT_MATCH_PREFIX & "FOLLOW-UP", "", "radial artery"
'    SetIndIfResultContains 15, "NURSING PROCEDURE: ABG", "", EXACT_MATCH_PREFIX & "FOLLOW-UP", "", "femoral artery"
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "", "", "AMPUTATED BODY PART CARE", "", "Amputated body part"
    SetIndIfResultContains 15, "", "", "AMPUTATED BODY PART CARE", "", "Body part amputated"
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "NURSING PROCEDURE: BEDSIDE RADIOLOGY", "", "", "", "After procedure" & CHAR_COMMA & " ice therapy applied"
    SetIndIfResultContains 15, "NURSING PROCEDURE: IV ACCESS", "", "", "", "Central Line"
    
    
    SetIndIfResultContains 15, "NURSING PROCEDURE: IV ACCESS", "", "", "", "Swelling noted at IV site"
    SetIndIfResultContains 15, "NURSING PROCEDURE: IV ACCESS", "", "", "", "Redness noted at IV site"
    SetIndIfResultContains 15, "NURSING PROCEDURE: IV ACCESS", "", "", "", "Drainage noted at IV site"
'    SetIndIfResultContains 15, "NURSING PROCEDURE: IV ACCESS", "", "", "", "At IV site no swelling"
'    SetIndIfResultContains 15, "NURSING PROCEDURE: IV ACCESS", "", "", "", "At IV site no redness"
'    SetIndIfResultContains 15, "NURSING PROCEDURE: IV ACCESS", "", "", "", "At IV site no drainage"
'    SetIndIfResultContains 15, "NURSING PROCEDURE: IV ACCESS", "", "", "", "IV access flushes easily and patent"
    SetIndIfResultContains 15, "NURSING PROCEDURE: IV ACCESS", "", "", "", "Chest x-ray ordered to confirm placement"
    SetIndIfResultContains 15, "NURSING PROCEDURE: IV ACCESS", "", "", "", "Chest x-ray completed and placement confirmed"
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "", "", "", "", "Tube thoracostomy indicated"
    SetIndIfResultContains 15, "", "", "", "", "Tube thoracostomy inserted"
    SetIndIfResultContains 15, "", "", "", "", "Needle thoracostomy performed"
    If inds(15).checked Then Exit Sub
    
    
    
    SetIndIfResultContains 15, "NURSING PROCEDURE: ENT", "", "", "", "ENT care indicated for epistaxis control"
    SetIndIfResultContains 15, "NURSING PROCEDURE: ENT", "", "", "", "After procedure" & CHAR_COMMA & " bleeding from nose"
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "", "", "EYE CARE", "", "Irrigation performed"
    SetIndIfResultContains 15, "", "", "EYE CARE", "", "Foreign body removal performed"
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "NURSING PROCEDURE: GARDNER WELLS TONGS", "", "", "", "Gardner Wells tongs indicated for "
    SetIndIfResultContains 15, "NURSING PROCEDURE: GARDNER WELLS TONGS", "", "", "", "Gardner Wells tongs applied by"
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "NURSING PROCEDURE: JOINT CARE", "", "", "", "Joint care indicated for"
    SetIndIfResultContains 15, "NURSING PROCEDURE: JOINT CARE", "", "", "", "Reduction performed;"
    SetIndIfResultContains 15, "NURSING PROCEDURE: JOINT CARE", "", "", "", "Joint aspiration performed;"
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "NURSING PROCEDURE: MAST SUIT", "", "", "", "Mast suit indicated for pelvic fracture"
    SetIndIfResultContains 15, "NURSING PROCEDURE: MAST SUIT", "", "", "", "Mast suit indicated for long bone fracture to lower extremity"
    SetIndIfResultContains 15, "NURSING PROCEDURE: MAST SUIT", "", "", "", "After procedure" & CHAR_COMMA & " pelvic fracture stabilized"
    SetIndIfResultContains 15, "NURSING PROCEDURE: PARACENTESIS", "", "", "", "Initial paracentesis;"
    SetIndIfResultContains 15, "NURSING PROCEDURE: PARACENTESIS", "", "", "", "Subsequent paracentesis;"
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "NURSING PROCEDURE: PARACENTESIS", "", "", "", "After procedure" & CHAR_COMMA & " sterile dressing applied to site"
    SetIndIfResultContains 15, "NURSING PROCEDURE: PELVIC EXAM", "", "", "", "After procedure" & CHAR_COMMA & " bleeding continues"
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "", "", "", "", "Peripherally inserted central catheter (PICC)"
'    SetIndIfResultContains 15, "NURSING PROCEDURE: IV ACCESS", "", "", "", "Neurovascular checks completed"
'    SetIndIfResultContains 15, "NURSING PROCEDURE: IV ACCESS", "", "", "", "After removal"
'    SetIndIfResultContains 15, "NURSING PROCEDURE: IV ACCESS", "", "", "", "IV discontinued"
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "SPINE PRECAUTIONS", "", "Spinal precautions indicated"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "SPINE PRECAUTIONS", "", "Cervical collar applied"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "SPINE PRECAUTIONS", "", "Patient placed on long board"
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "NURSING PROCEDURE: SUBUNGUAL HEMATOMA", "", "", "", "Subungual hematoma released from"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "SURGICAL AIRWAY", "", "Tracheostomy;"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "SURGICAL AIRWAY", "", "Cricothyrotomy;"
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "", "", "", "", "Ring removal indicated for"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "TOOTH CARE", "", "cleansed of debris"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "TOOTH CARE", "", "Tooth reimplantation performed by"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "TOOTH CARE", "", "Tooth placed in"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "TOOTH CARE", "", "Container properly labeled and with patient"
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "NURSING PROCEDURE: TUBE CARE", "", "", "", "Dressing maintained"
    SetIndIfResultContains 15, "NURSING PROCEDURE: TUBE CARE", "", "", "", "Dressing changed"
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "", "", "", "", "The Burn Injury Report form was completed"
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "NURSING PROCEDURE: IV ACCESS", "", "", "", "Location of inserted PICC line"
    SetIndIfResultContains 15, "", "", "", "", "Peripherally Inserted Central Catheter (PICC);"
    SetIndIfResultContains 15, "NURSING PROCEDURE: IV ACCESS", "", "", "", "PICC (valved)"
    SetIndIfResultContains 15, "NURSING PROCEDURE: IV ACCESS", "", "", "", "A PICC line was DC"
    SetIndIfResultContains 15, "NURSING PROCEDURE: IV ACCESS", "", "", "", "Central Venous Catheter"
    SetIndIfResultContains 15, "NURSING PROCEDURE: IV ACCESS", "", "", "", "Implanted Vascular Access Device (IVAD)"
    SetIndIfResultContains 15, "NURSING PROCEDURE: IV ACCESS", "", "", "", "IVAD (valved)"
    SetIndIfResultContains 15, "NURSING PROCEDURE: IV ACCESS", "", "", "", "Hemodialysis/High Flow Cath"
    If inds(15).checked Then Exit Sub
    
    SetIndIfAllResults 15, "", "", "", "", "Urinary catheter present draining,hematuria"
    SetIndIfAllResults 15, "", "", "", "", "Urinary catheter present draining,red urine"
    If inds(15).checked Then Exit Sub
    
    SetIndIfAllResults 15, "", "", "", "", "Nasal assessment findings include nose,with deformity present"
    SetIndIfAllResults 15, "", "", "", "", "Nasal assessment findings include nose,depressed"
    SetIndIfAllResults 15, "", "", "", "", "Nasal assessment findings include nose,ecchymotic"
    SetIndIfAllResults 15, "", "", "", "", "Nasal assessment findings include nose,swollen"
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "", "", "", "", "vomiting gross blood;"
    SetIndIfAllResults 15, "", "", "", "", "Associated with diarrhea,gross blood"
    If inds(15).checked Then Exit Sub
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "TRIAGE REASSESSMENT", "", "Splinting applied"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "TRIAGE REASSESSMENT", "", "Elevation"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "TRIAGE REASSESSMENT", "", "Ice applied"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "TRIAGE REASSESSMENT", "", "Laceration irrigated"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "TRIAGE REASSESSMENT", "", "Wound irrigated"
    SetIndIfResultContains 15, "", "", EXACT_MATCH_PREFIX & "TRIAGE REASSESSMENT", "", "Dressing applied"
    
    If inds(15).checked Then Exit Sub
    SetIndIfAllResults 15, "NURSING ASSESSMENT: OBSTETRICS", "", "", "", "vaginal bleeding present,amount large"
    SetIndIfAllResults 15, "NURSING ASSESSMENT: OBSTETRICS", "", "", "", "vaginal bleeding present,clots are present"
    SetIndIfResultContains 15, "NURSING PROCEDURE: PERITONEAL DIALYSIS", "", "", "", "dressing applied to site"
    
    If inds(15).checked Then Exit Sub
    
    SetIndIfResultContains 15, "", "", "POST ANGIOGRAM FOLLOW-UP", "", "sterile dressing applied to insertion site,Radial compression device band intact"
    SetIndIfResultContains 15, "", "", "POST ANGIOGRAM FOLLOW-UP", "", "bleeding noted at site,no bleeding at site,swelling at site,bruising at site,bruit at site,hematoma at site,drainage at site"
    SetIndIfResultContains 15, "", "", "POST ANGIOGRAM FOLLOW-UP", "", "Air was removed from radial compression device band"
    
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_15"
    Resume  'debug
End Sub

Private Sub Check_16()
    On Error GoTo errHandler
    
    dvprint "-----------------------"
    dvprint "ED Visit 16. Urgent Intervention > 1 staff"
    dvprint "-----------------------"
    
    If inds(16).checked Then Exit Sub
    
    SetIndIfResultContains 16, "", "", "", EXACT_MATCH_PREFIX & "Trauma Patient", "TR Level 1"
    SetIndIfResultContains 16, "", "", "", EXACT_MATCH_PREFIX & "Trauma Patient", "TR Level Red"
    SetIndIfResultContains 16, "", "", "", EXACT_MATCH_PREFIX & "Trauma Patient", "TR Level 2"
    SetIndIfResultContains 16, "", "", "", EXACT_MATCH_PREFIX & "Trauma Patient", "TR Level Yellow"
    If inds(16).checked Then Exit Sub
    SetIndIfResultContains 16, "", "", EXACT_MATCH_PREFIX & "Complaint", "", "Multiple Trauma"
    SetIndIfResultContains 16, "", "", EXACT_MATCH_PREFIX & "Complaint", "", "Post Cardiac Arrest"
    SetIndIfResultContains 16, "", "", EXACT_MATCH_PREFIX & "Complaint", "", "Post Respiratory Arrest"
    If inds(16).checked Then Exit Sub
    SetIndIfResultContains 16, "", "", "", EXACT_MATCH_PREFIX & "Urgency", "ESI Level 1"
    SetIndIfResultContains 16, "", "", "", "", "Urgent intervention > 1 staff"
    If inds(16).checked Then Exit Sub
    ' Now exploding
    'SetIndIfResultContains 16, "NURSING PROCEDURE: CODE RECORDER", "", "", "", ""
    SetIndIfResultContains 16, "", "", "", "", "via ambu bag;"
    SetIndIfResultContains 16, "", "", EXACT_MATCH_PREFIX & "SURGICAL AIRWAY", "", "Tracheostomy;"
    If inds(16).checked Then Exit Sub
    SetIndIfResultContains 16, "", "", EXACT_MATCH_PREFIX & "SURGICAL AIRWAY", "", "Cricothyrotomy;"
    SetIndIfResultContains 16, "NURSING PROCEDURE: THORACOTOMY", "", "", "", ""
    SetIndIfResultContains 16, "", "", EXACT_MATCH_PREFIX & "THORACOTOMY", "", ""
        
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_16"
    Resume  'debug
End Sub
Private Sub Check_17()
    On Error GoTo errHandler
    
    dvprint "-----------------------"
    dvprint "ED Visit 17. Educational Needs"
    dvprint "-----------------------"
    
    If inds(17).checked Then Exit Sub
    
    SetIndIfResultContains 17, "", "", EXACT_MATCH_PREFIX & "DISCHARGE", "", "CRUTCH USE (EDU)"
    SetIndIfResultContains 17, "", "", "", "", "Teaching methods used included Return Demonstration"
    SetIndIfResultContains 17, "", "", "", "", "Teaching methods used included Interactive Demonstration"
    If inds(17).checked Then Exit Sub
    SetIndIfResultContains 17, "", "", "", "", "Complex discharge teaching performed"
    SetIndIfResultContains 17, "", "", "", "", "Aerochamber instructions given"
    SetIndIfResultContains 17, "", "", "", "", "Incentive spirometry teaching"
    If inds(17).checked Then Exit Sub
    SetIndIfResultContains 17, "", "", "", "", "Patient returned demonstration of use of aerochamber"
    SetIndIfResultContains 17, "", "", "", "", "Patient returned demonstration of use of incentive spirometry"
'    SetIndIfResultContains 17, "", "", "", "", "Patient given ALBUTEROL"
'    If inds(17).checked Then Exit Sub
'    SetIndIfResultContains 17, "", "", "", "", "Patient given ATROVENT"
'    SetIndIfResultContains 17, "", "", "", "", "Patient given XOPENEX"
'    SetIndIfResultContains 17, "", "", "", "", "Patient given COMBIVENT"
'    If inds(17).checked Then Exit Sub
'    SetIndIfResultContains 17, "", "", "", "", "Patient given RACEMIC EPINEPHRINE"
    SetIndIfResultContains 17, "", "", "", "", "Urine strainer given"
    SetIndIfResultContains 17, "", "", "", "", "Leg bag teaching given"
    If inds(17).checked Then Exit Sub
    SetIndIfResultContains 17, "", "", EXACT_MATCH_PREFIX & "DOMESTIC VIOLENCE", "", "Hotline number given"
    SetIndIfResultContains 17, "", "", EXACT_MATCH_PREFIX & "DOMESTIC VIOLENCE", "", "Shelter number given"
    If inds(17).checked Then Exit Sub
    SetIndIfResultContains 17, "", "", EXACT_MATCH_PREFIX & "DOMESTIC VIOLENCE", "", "Legal referral made"
    SetIndIfResultContains 17, "", "", EXACT_MATCH_PREFIX & "DOMESTIC VIOLENCE", "", "Domestic violence safety card given"
    SetIndIfResultContains 17, "", "", EXACT_MATCH_PREFIX & "DOMESTIC VIOLENCE", "", "Educational material given"
    If inds(17).checked Then Exit Sub
    SetIndIfResultContains 17, "", "", EXACT_MATCH_PREFIX & "DOMESTIC VIOLENCE", "", "Follow up appointment scheduled"
    SetIndIfResultContains 17, "", "", EXACT_MATCH_PREFIX & "DOMESTIC VIOLENCE", "", "Other referrals made"
    SetIndIfResultContains 17, "", "", "", "", "Identified Devices" & CHAR_COMMA & " Equipment or Treatments as a learning need"
    If inds(17).checked Then Exit Sub
'    SetIndIfResultContains 17, "", "", "", "", "Identified Medications as a learning need;"
    SetIndIfResultContains 17, "", "", "", "", "Identified Lifestyle" & CHAR_COMMA & " Environmental Changes as a learning need"
    SetIndIfResultContains 17, "", "", "", "", "Identified as a learning need"
        
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_17"
    Resume  'debug
End Sub
Private Sub Check_181920()
    On Error GoTo errHandler
    
    dvprint "-----------------------"
    dvprint "ED Visit 18. Admitted"
    dvprint "ED Visit 19. Transferred to another Facility"
    dvprint "ED Visit 20. Expired"
    dvprint "-----------------------"
    
    If inds(20).checked Then Exit Sub
'change so that the latest disposition type
    
    Qdisposition
'    SetIndIfResultContains 20, "", "", "", EXACT_MATCH_PREFIX & "Disposition Type", "Expired"
'    If inds(20).checked Then Exit Sub
'    SetIndIfResultContains 19, "", "", "", EXACT_MATCH_PREFIX & "Disposition Type", "Transfer to External Facility"
'    SetIndIfResultContains 19, "", "", "", EXACT_MATCH_PREFIX & "Disposition Type", "Transport to CRU"
'    SetIndIfResultContains 19, "", "", "", EXACT_MATCH_PREFIX & "Disposition Type", "Transport to Inpatient Rehab Facility"
'    If inds(19).checked Then Exit Sub
'    SetIndIfResultContains 18, "", "", "", EXACT_MATCH_PREFIX & "Disposition Type", "Inpatient Admit"
'    SetIndIfResultContains 18, "", "", "", EXACT_MATCH_PREFIX & "Disposition Type", "Observation Admit"
        
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_181920"
    Resume  'debug
End Sub
Private Sub CheckCustom()
    On Error GoTo errHandler
    
    dvprint "-----------------------"
    dvprint "ED Visit 90. Downtime documentation"
    dvprint "-----------------------"
    
    SetIndIfResultContains 90, "", "", "", "", "Refer to scanned documentation generated during System Downtime"
        
    Exit Sub
    
errHandler:
    g_util.ThrowError "CheckCustom"
    Resume  'debug
End Sub

Private Sub ProcessProc(pnum As Integer, code1 As String, code2 As String, code3 As String)
    Dim d As Date
'
'499872183
'499867279
'    If Exists("", "499867279", , , EXACT_MATCH_PREFIX & "RN") Then
'        If SumEventDateTime() > 1 Then
'        End If
'    End If

End Sub
'
'Private Sub CheckProcs()
'    Dim return_result As String
'    Dim proc8, proc9 As Boolean
'
'    On Error GoTo errHandler
'    dvprint "-----------------------"
'    dvprint "P1. 1-1 safety observation by non-RN"
'    dvprint "-----------------------"
'
'    ProcessProc 1, "OLAMBPROG8", "OLAMBPRO02", "OLAMBPR03"
'
'    dvprint "-----------------------"
'    dvprint "P2. Off unit accompanied by RN"
'    dvprint "-----------------------"
'
'    ProcessProc 2, "OLACUITY25", "OLACUITY30", "OLACUITY31"
'
'    dvprint "-----------------------"
'    dvprint "P3. Off unit accompanied by non-RN"
'    dvprint "-----------------------"
'
'    ProcessProc 3, "OLACUITY20", "OLACUITY32", "OLACUITY33"
'
'    dvprint "-----------------------"
'    dvprint "P4. Patient/family education by RN"
'    dvprint "-----------------------"
'
'    ProcessProc 4, "OLACUITY21", "OLACUITY45", "OLACUITY46"
'
'    dvprint "-----------------------"
'    dvprint "P5. Extensive wound management by RN"
'    dvprint "-----------------------"
'
'    ProcessProc 5, "OLACUITY22", "OLACUITY34", "OLACUITY35"
'
'    dvprint "-----------------------"
'    dvprint "P6. Extensive wound management by non-RN"
'    dvprint "-----------------------"
'
'    ProcessProc 6, "OLACUITY23", "OLACUITY36", "OLACUITY37"
'
'    dvprint "-----------------------"
'    dvprint "P7. Coordination of care by RN"
'    dvprint "-----------------------"
'
'    ProcessProc 7, "OLACUITY24", "OLACUITY43", "OLACUITY44"
'
'    dvprint "-----------------------"
'    dvprint "P8&P9. 1-1 or 2-1 by RN at bedside"
'    dvprint "-----------------------"
'
'    'olacuity59 and olacuity60 and ptunitmatchescode on both
'    proc8 = False
'    proc9 = False
'    If GetResult("", "OLACUITY59", "", "", return_result) Then
'        If PtUnitMatchesCode(return_result) Then
'            ProcessProc 8, "OLACUITY26", "OLACUITY38", "OLACUITY39"
'            If procs(numprocs).pnum = 8 Then proc8 = True
'            If GetResult("", "OLACUITY60", "", "", return_result) Then
'                If PtUnitMatchesCode(return_result) Then
'                    ProcessProc 9, "OLACUITY27", "OLACUITY41", "OLACUITY42"
'                    If procs(numprocs).pnum = 9 Then proc9 = True
'                End If
'            End If
'        End If
'    End If
'
'    If proc9 Then
'        If proc8 Then procs(numprocs - 1) = procs(numprocs)
'        With procs(numprocs)
'            .pindex = 0
'            .start = 0
'            .finish = 0
'            .isvalid = False
'            .pnum = 0
'        End With
'        numprocs = numprocs - 1
'    End If
'
''    dvprint "-----------------------"
''    dvprint "P8. 1-1 by RN at bedside"
''    dvprint "-----------------------"
''
''    'olacuity59 and ptunitmatchescode
''    If GetResult("", "OLACUITY59", "", "", return_result) Then
''        If PtUnitMatchesCode(return_result) Then
''            ProcessProc 8, "OLACUITY26", "OLACUITY38", "OLACUITY39"
''        End If
''    End If
'
'
'    Exit Sub
'
'errHandler:
'    g_util.ThrowError "CheckProcs"
'    Resume  'debug
'End Sub
'Private Sub CheckOutcomes()
'    Dim return_result As String
'
'    'How to handle multiple outcomes per pt, each with different times?
'    dvprint "-----------------------"
'    dvprint "Outcomes: FALL"
'    dvprint "-----------------------"
'    If GetResult("", "FALL", "", "", return_result, SearchOutcomesRange) Then
'        numoutcomes = numoutcomes + 1
'        With oc(numoutcomes)
'            .checked = True
'            .pnum = 1
'            .start = return_result
'        End With
'    End If
'
'    dvprint "-----------------------"
'    dvprint "Outcomes: MED"
'    dvprint "-----------------------"
'    If GetResult("", "MED", "", "", return_result, SearchOutcomesRange) Then
'        numoutcomes = numoutcomes + 1
'        With oc(numoutcomes)
'            .checked = True
'            .pnum = 2
'            .start = return_result
'        End With
'    End If
'
'    dvprint "-----------------------"
'    dvprint "Outcomes: PROC"
'    dvprint "-----------------------"
'    If GetResult("", "PROC", "", "", return_result, SearchOutcomesRange) Then
'        numoutcomes = numoutcomes + 1
'        With oc(numoutcomes)
'            .checked = True
'            .pnum = 21
'            .start = return_result
'        End With
'    End If
'
'    Exit Sub
'
'errHandler:
'    g_util.ThrowError "CheckOutcomes"
'    Resume  'debug
'End Sub


'Private Sub AtLeastOneADL()
'    If Not (inds(1).checked Or inds(2).checked Or inds(3).checked) Then
'        'Note: Heather wants "#2 ADL - Assist" to be the default.  (90% of patients)
'        SetInd 2, "at least one ADL"
'    End If
'End Sub

Private Sub HighestIndicatorInEachGroupWins()
    Dim i As Integer
    Dim g As Integer
    Dim highest_is_on As Boolean
    Dim ind_list As String

    dvprint "-----------------------"
    dprint "Select highest indicator in each group"
    
    g = 0
    highest_is_on = False
    For i = MAX_INDS To 1 Step -1
        If (inds(i).group > 0) Then
            If (inds(i).group <> g) Then
                'this is a new group
                g = inds(i).group
                highest_is_on = inds(i).checked
            Else
                'same group
                If highest_is_on Then
                    inds(i).checked = False             'uncheck a lower number
                Else
                    highest_is_on = inds(i).checked     'save this one
                End If
            End If
        End If
    Next i
    
    'Echo the indicators for an audit (no classification will be saved)
    If g_debug And g_no_output Then
        For i = 1 To MAX_INDS
            If inds(i).checked Then ind_list = ind_list & "," & i
        Next i
        dprint "Final list = " & Mid$(ind_list, 2)
    End If

End Sub

Private Sub OutputClass()
    Dim outstr As String, ind_list As String
    Dim i As Integer
    Dim txarea As String, return_result As String

    txarea = GetTxArea
        
    outstr = g_util.FixedWidth(m_pat.fac_code, 8)                                       '(facility code)
    outstr = outstr & "|" & g_util.FixedWidth(UnitName(m_pat.locary(m_locindex).unit_id), 16)
    outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(unit code)
    outstr = outstr & "|" & g_util.FixedWidth(txarea, 16)                       '(area code)
    outstr = outstr & "|" & g_util.FixedWidth("", 8)                        '(class date - give datetime instead)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.acct, 20)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.last_name, 32)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.first_name, 32)
    outstr = outstr & "|" & g_util.FixedWidth("", 32)                       '(middle)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.room, 8)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.bed, 4)
    outstr = outstr & "|" & Format$(m_pat.locary(m_locindex).time_in, "yyyymmddhhnn")      'class datetime
    outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(login)
    outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(employee)
    outstr = outstr & "|" & g_util.FixedWidth(CStr(m_pat.locary(m_locindex).meth_id), 4)
    outstr = outstr & "|" & g_util.FixedWidth("C", 1)                       'record type = class
    outstr = outstr & "|" & g_util.FixedWidth("", 4)                        '(stage)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.TC_source_id, 4)        'TC source ID
    outstr = outstr & "|" & g_util.FixedWidth(CStr(m_pat.locary(m_locindex).range), 4)
    outstr = outstr & "|"
    outstr = g_util.FixedWidth(outstr, 294)
    outstr = outstr & "|" & Format$(m_pat.locary(m_locindex).time_in, "yyyymmddhhnn")        'IN
    outstr = g_util.FixedWidth(outstr, 346)
    outstr = outstr & "|" & Format$(m_pat.locary(m_locindex).time_out, "yyyymmddhhnn")       'OUT
    outstr = g_util.FixedWidth(outstr, 377)
    outstr = outstr & "|"
    
    For i = 1 To MAX_INDS
        If (inds(i).checked) Then
            outstr = outstr & "Y"
            ind_list = ind_list & "," & i
        Else
            outstr = outstr & "N"
        End If
    Next i
    ind_list = Mid$(ind_list, InStr(1, ind_list, ",") + 1)                'strip leading comma
    
    Print #outfile, outstr
    return_result = "Classified: " & ind_list
    AddLogEntry EVENT_TYPE_INFO, return_result, EVENT_CATEGORY_PROCESSED
End Sub

Private Sub OutputProcs()
'    Dim i, j As Integer
'    Dim s, f As Date
'    Dim outstr As String, proc_list As String
'
'    For i = 1 To numprocs
'        procs(i).isvalid = True
'    Next i
'    For i = 1 To numprocs - 1
'        If procs(i).isvalid Then
'        For j = i + 1 To numprocs
'            If procs(j).isvalid And procs(j).start = procs(i).start And procs(j).finish = procs(i).finish Then   'this can be combined.
'                procs(j).isvalid = False
'                procs(j).pindex = i
'            End If
'        Next j
'        End If
'    Next i
'
'    For i = 1 To numprocs
'        If procs(i).isvalid Then
'        outstr = g_util.FixedWidth("", 8)                                       '(facility code)
'        outstr = outstr & "|" & g_util.FixedWidth(m_pat.unit_name, 16)
'        outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(unit code)
'        outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(area code)
'        outstr = outstr & "|" & g_util.FixedWidth("", 8)                        '(class date - give datetime instead)
'        outstr = outstr & "|" & g_util.FixedWidth(m_pat.acct, 20)
'        outstr = outstr & "|" & g_util.FixedWidth(m_pat.last_name, 32)
'        outstr = outstr & "|" & g_util.FixedWidth(m_pat.first_name, 32)
'        outstr = outstr & "|" & g_util.FixedWidth("", 32)                       '(middle)
'        outstr = outstr & "|" & g_util.FixedWidth(m_pat.room, 8)
'        outstr = outstr & "|" & g_util.FixedWidth(m_pat.bed, 4)
'        outstr = outstr & "|" & Format$(procs(i).start, "yyyymmddhhnn")       '204 procdt
'        outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(login)
'        outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(employee)
'        outstr = outstr & "|" & g_util.FixedWidth(CStr(m_pat.meth_id), 4)
'        outstr = outstr & "|" & g_util.FixedWidth("P", 1)                       'record type = class
'        outstr = outstr & "|"
'        outstr = outstr & Space$(294 - Len(outstr))
'        outstr = outstr & "|" & Format$(procs(i).start, "yyyymmddhhnn")       '296 procdt in
'        outstr = outstr & Space$(346 - Len(outstr))
'        If procs(i).finish = 0 Then
'            outstr = outstr & "|" & Space$(12)
'        Else
'            outstr = outstr & "|" & Format$(procs(i).finish, "yyyymmddhhnn")      '348 procdt out
'        End If
'        outstr = g_util.FixedWidth(outstr, 377)
'        outstr = outstr & "|NNNNNNNNN"
'        Mid$(outstr, 378 + procs(i).pnum, 1) = "Y"
'        proc_list = proc_list & "," & procs(i).pnum
'        For j = i + 1 To numprocs
'            If Not procs(j).isvalid And procs(j).pindex = i Then
'                Mid$(outstr, 378 + procs(j).pnum, 1) = "Y"
'                proc_list = proc_list & "," & procs(j).pnum
'            End If
'        Next j
'        proc_list = Mid$(proc_list, InStr(1, proc_list, ",") + 1)          'strip leading comma
'
'        Print #outfile, outstr
'
'        AddLogEntry EVENT_TYPE_INFO, "Procedure: " & proc_list, EVENT_CATEGORY_PROCESSED
'        End If 'isvalid
'    Next i
'
End Sub
Private Sub OutputOutcomes()
'    Dim i, j As Integer
'    Dim s, f As Date
'    Dim outstr As String
'    Dim octime As String
'
'    If numoutcomes = 0 Then Exit Sub
'
'    For i = 1 To numoutcomes
'        If (oc(i).checked) Then
'            outstr = g_util.FixedWidth("", 8)
'            outstr = outstr & "|" & g_util.FixedWidth(m_pat.unit_name, 16)
'            outstr = outstr & "|" & g_util.FixedWidth("", 16)
'            outstr = outstr & "|" & g_util.FixedWidth("", 16)
'            outstr = outstr & "|" & g_util.FixedWidth(oc(i).start, 12)
'            outstr = outstr & "|" & g_util.FixedWidth(m_pat.acct, 20)
'            outstr = outstr & "|" & g_util.FixedWidth(oc(i).pnum, 3)
'            Print #outfile2, outstr 'Print line to outcomesindicator.TXT
'        End If
'    Next i
End Sub


'Private Function ReturnAssessCount(codelist As String) As Integer
'    Dim sql As String
'    Dim rs As New Recordset
'    Dim ct As Integer
'    Dim done_ct As Integer
'    Dim mintime As Date
'    Dim maxtime As Date
'    Dim done As Boolean
'
'    ReturnAssessCount = 0
'
'    sql = "select count(distinct(event_datetime)) from chart_item " & WhereBase & AndSimpleItemFilter("", codelist, "", "", "")
'    sql = sql & " and event_datetime is not null"
'    'dvprint sql
'    rs.Open sql, g_cnADO
''    dvprint sql
'
'    If Not rs.EOF Then
'        If IsNull(rs(0)) Then
'            ct = 0
'            rs.Close
'            Exit Function
'        Else
'            ct = rs(0)
'            rs.Close
'        End If
'    Else
'        ct = rs(0)
'        rs.Close
'        Exit Function
'    End If
'
'    ReturnAssessCount = ct
'
'End Function

'Private Function SumEventDateTime(code As String, res As String) As Date
'    Dim sql As String
'    Dim sql2 As String
'    Dim total As Integer
'    Dim rs As New Recordset
'    Dim rs2 As New Recordset
'
'    sql = "select event_datetime from chart_item " & WhereBase
'    sql = sql & " and code=" & g_dbutil.SQL_String(code) & " and result=" & g_dbutil.SQL_String(res) & " and event_datetime is not null"
'    rs.Open sql, g_cnADO
'
'    Do While Not rs.EOF
'        If Not IsNull(rs(0)) Then
'            sql2 = "select sum(result) from chart_item " & WhereBase
'            sql2 = sql & " and code=" & g_dbutil.SQL_String(code) & " and event_datetime=" & g_dbutil.SQL_Date(rs(0))
'            rs2.Open sql2, g_cnADO
'            If Not rs2.EOF Then
'                If Not IsNull(rs2(0)) Then
'                    If IsNumeric(rs2(0)) Then
'                        total = total + rs2(0)
'                    End If
'                End If
'            End If
'        End If
'        rs.MoveNext
'    Loop
'    rs2.Close
'    rs.Close
'    SumEventDateTime = total
'End Function


'Private Function ReturnMaxCount(codelist As String, reslist As String, ByRef numbuckets As Integer) As Integer 'needs quotes in lists!
'    Dim sql As String
'    Dim rs As New Recordset
'    Dim ct As Integer
'    Dim done_ct As Integer
'    Dim mintime As Date
'    Dim maxtime As Date
'    Dim done As Boolean
'    Dim i As Integer
'    Dim j As Integer
'    Dim totals(12) As Boolean
'    Dim bucket(24) As bucket_type
'
'    ReturnMaxCount = 0
'    numbuckets = 0
'
'    'do an initial count to see if warrants continuing
'    sql = "select count(*) from chart_item " & WhereBase
'    sql = sql & " and code in (" & codelist & ")"
'    sql = sql & " and result in (" & reslist & ")"
'    sql = sql & " and event_datetime is not null"
'    rs.Open sql, g_cnADO
'    If Not rs.EOF Then
'        If rs(0) = 0 Then
'            rs.Close
'            Exit Function
'        End If
'    End If
'    rs.Close
'
'    'g_pull_start for g_range mins
'    'need to return number of hours between start and finish to get  hits/hour because start may be pt arrival time
'    '1. determine number of hours btwn start and finish.  sets the number of buckets
'    '2.
'    numbuckets = 24
'    For i = 1 To 24
'        bucket(i).startdt = DateAdd("h", i - 1, g_pull_start)
'        bucket(i).enddt = DateAdd("n", 59, bucket(i).startdt)
'        If bucket(i).enddt >= g_pull_finish Then
'            numbuckets = i 'may be less than 24 buckets
'            Exit For
'        End If
'    Next i
'
'    sql = "select "
'    For i = 1 To numbuckets
'        sql = sql & "sum(case when event_datetime between " & bucket(i).startdt & " and " & bucket(i).enddt & " then 1 else 0 end) as range" & i & ","
'    Next i
'    sql = Mid$(sql, 1, Len(sql) - 1) 'remove comma
'    sql = sql & " from chart_item " & WhereBase
'    sql = sql & " and code in (" & codelist & ")"
'    sql = sql & " and result in (" & reslist & ")"
'    sql = sql & " and event_datetime is not null"
'    rs.Open sql, g_cnADO
'
'    ct = 0
'    For i = 1 To numbuckets
'        bucket(i).count = rs(i - 1)
'        If bucket(i).count > 0 Then ct = ct + 1
'    Next i
'    rs.Close
'
'    ReturnMaxCount = ct
'
'    'do an initial count to see if warrants continuing; set the mintime of all codes
''    sql = "select sum(case when event_datetime between CONVERT(VARCHAR(10),GETDATE(),111) + ' 06:00' and CONVERT(VARCHAR(10),GETDATE(),111) + ' 06:59' then 1 else 0 end) as range24,"
''    sql = sql & "sum(case when event_datetime between CONVERT(VARCHAR(10),GETDATE(),111) + ' 05:00' and CONVERT(VARCHAR(10),GETDATE(),111) + ' 05:59' then 1 else 0 end) as range23,"
'
'End Function
'Private Function FormatCodeList(code_list As String) As String
'    Dim arr() As String
'    Dim i As Integer, n As Integer
'    Dim result As String
'
'    If Len(code_list) = 0 Then Exit Function
'
'    n = g_util.SplitTextOnChar(code_list, ",", arr(), 1, 0)
'
'    result = "("
'
'    For i = 1 To n
'        result = result & g_dbutil.SQL_String(Trim$(arr(i)))
'        If i < n Then result = result & ","
'    Next i
'
'    result = result & ")"
'
'    FormatCodeList = result
'End Function

Private Sub SetIndIfAllResults(ind As Integer, cat As String, code As String, desc As String, field As String, reslist As String)
    Dim arr() As String
    Dim i As Integer, n As Integer
    Dim result As String
    Dim sql As String
    Dim rs As New Recordset
    Dim and_filter As String
    
    and_filter = AndSimpleItemFilter(cat, "", desc, field, "")
    
    n = g_util.SplitTextOnChar(reslist, ",", arr(), 1, 0)
    
    result = ""
    
    For i = 1 To n
        result = result & " and result like '%" & Trim$(arr(i)) & "%'"
    Next i
    
    sql = "select count(*) from chart_item " & WhereBase & and_filter
    sql = sql & result
    'dvprint sql
    rs.Open sql, g_cnADO
'    dvprint sql
    If rs(0) > 0 Then SetInd ind, reslist
    rs.Close
    
End Sub
Private Sub SetIndIfResultsConcurrent(ind As Integer, res1 As String, res2 As String)
    Dim sql As String
    Dim rs As New Recordset
    Dim rs2 As New Recordset
    Dim done As Boolean
    
    sql = "select distinct(event_datetime) from chart_item " & WhereBase
    sql = sql & " and result like '%" & res1 & "%'"
    'dvprint sql
    rs.Open sql, g_cnADO
'    dvprint sql

    Do While Not rs.EOF And Not done
        sql = "select count(*) from chart_item " & WhereBase
        sql = sql & " and event_datetime=" & g_dbutil.SQL_DateTime(rs(0)) & " and result like '%" & res2 & "%'"
        'dvprint sql
        rs2.Open sql, g_cnADO
        If rs2(0) > 0 Then
            SetInd ind, res1 & ", " & res2
            done = True
        End If
        rs2.Close
        rs.MoveNext
    Loop
    rs.Close
    
End Sub

'Private Sub SetupBuckets(bmins As Integer)
'    Dim i As Integer
''b1 = pull_start to +70
''b2=  b1 end
'' 700 - 1022  range = 3h 22min = 202mins   202/70=2 -> 3 buckets
'' 700:700+70=810, 810:810+70=920,  920:finish
'    bucketsize = bmins
'    num_buckets = g_range \ bucketsize
'    If (g_range Mod bucketsize) <> 0 Then num_buckets = num_buckets + 1
'dvprint "initial num buckets=" & num_buckets
'    If num_buckets >= MAX_BUCKETS Then num_buckets = 24
'dvprint "final  num buckets=" & num_buckets
'    For i = 1 To num_buckets
'        bucket(i).startdt = DateAdd("n", (i - 1) * bucketsize, m_pat.pull_start)
'        bucket(i).count = 0
'    Next i
'    For i = 1 To num_buckets - 1
'        bucket(i).enddt = bucket(i + 1).startdt
'    Next i
'    bucket(num_buckets).enddt = m_pat.pull_finish
'    b_filter = ""
'    b_excl = ""
'
'End Sub

Private Sub PutIntoBuckets(cat As String, code As String, desc As String, field As String, result_list As String)
    Dim f As String
    
    f = AndSimpleItemFilter(cat, code, desc, field, result_list)
    f = "(" & Mid$(f, 6, Len(f) - 5) & ")"  'always starts with _and_
    If b_filter = "" Then
        b_filter = f  'start of b_filter
    Else
        b_filter = b_filter & " or " & f
    End If

End Sub

'Private Sub ExecPutIntoBuckets()
'    Dim sql As String
'    Dim rs As New Recordset
'    Dim count As Long, i As Integer
'    Dim pos As Integer
'    Dim s As String
'
'    b_filter = " and (" & b_filter & ")"
'
'    s = ""
'    For i = 1 To num_buckets
'        s = s & "sum(case when event_datetime>=" & g_dbutil.SQL_DateTime(bucket(i).startdt) & " and "
'        If i < num_buckets Then
'            s = s & "event_datetime<" & g_dbutil.SQL_DateTime(bucket(i).enddt) & " then 1 else 0 end) as range" & i & ","
'        Else
'            s = s & "event_datetime<=" & g_dbutil.SQL_DateTime(bucket(i).enddt) & " then 1 else 0 end) as range" & i & ","
'        End If
'    Next i
'    s = Mid$(s, 1, Len(s) - 1)  'remove last comma
'
''    and_filter = AndSimpleItemFilter(cat, code, desc, field, result_list)
'    sql = "select " & s
'    sql = sql & " from (select distinct(event_datetime) from chart_item " & WhereBase & b_filter & b_excl & ") as Q"
''dvprint sql
'    rs.Open sql, g_cnADO
'    If Not IsNull(rs(0)) Then
'        For i = 1 To num_buckets
'            bucket(i).count = bucket(i).count + rs(i - 1)
'        Next i
'    End If
'
'    rs.Close
'
'
'    s = ""
'    For i = 1 To num_buckets
'        dprint i & ":"
'        s = "select event_datetime,category,description,field_name from chart_item " & WhereBase & b_filter & b_excl
'        s = s & " and event_datetime>=" & g_dbutil.SQL_DateTime(bucket(i).startdt)
'        If i < num_buckets Then
'            s = s & " and event_datetime<" & g_dbutil.SQL_DateTime(bucket(i).enddt)
'        Else
'            s = s & " and event_datetime<=" & g_dbutil.SQL_DateTime(bucket(i).enddt)
'        End If
'        s = s & " order by event_datetime"
'        rs.Open s, g_cnADO
'        Do While Not rs.EOF
'            dprint "  " & rs(0) & ": " & g_dbutil.DBToString(rs(1)) & "; " & g_dbutil.DBToString(rs(2)) & "; " & g_dbutil.DBToString(rs(3))
'            rs.MoveNext
'        Loop
'        rs.Close
'    Next i
'
'    b_filter = ""
'    b_excl = ""
'End Sub
Private Sub FillBucket2()
    Dim sql As String
    Dim rs As New Recordset
    
    b_filter = " and (" & b_filter & ")"
    b2num = 0
    sql = "select distinct(event_datetime) from chart_item " & WhereBase & b_filter & b_excl & " order by event_datetime"
'dvprint sql
    rs.Open sql, g_cnADO
    Do While Not rs.EOF
        b2num = b2num + 1
        bucket2(b2num).eventdt = rs(0)
        rs.MoveNext
    Loop
    rs.Close
    

End Sub

Private Sub ExecIntervals(ind As Integer, imins As Integer)
    Dim sql As String
    Dim rs As New Recordset
    Dim i As Integer, j As Integer
    Dim addmins As Integer, istart As Integer, minupperidx As Integer
    Dim upperdt As Date
    Dim all_ok As Boolean
    Dim w As Integer, w2 As Integer
    Dim new_proposed_end_time As Date
    Dim used_waiver_on_end As Boolean
    Dim j_cannot_use_waiver As Integer  'idx of item where waiver cannot be used.
    

    If b2num <= 1 Then Exit Sub
    SetMaxWaivers imins
    'loop through each evdt looking for intervals of imins
    dprint "LOS=" & m_pat.locary(m_locindex).range & "; Exam length=" & m_pat.locary(m_locindex).range \ 2
    addmins = m_pat.locary(m_locindex).range \ 2
    For i = 1 To b2num - 1
        'reset waivers
        w = num_waivers
        
        istart = i
        upperdt = DateAdd("n", addmins, bucket2(i).eventdt)
        
        'what is the min evdt >= upperdt?
        minupperidx = 0
        For j = i + 1 To b2num
            If bucket2(j).eventdt >= upperdt Then
                minupperidx = j
'dvprint "min upper idx=" & minupperidx
'dvprint "min upper time=" & bucket2(j).eventdt
                j = b2num + 1
            End If
        Next j
'        If minupperidx = 0 And num_waivers > 0 Then 'half LOS not possible.  Try end time with a waiver if num_waivers>=1
'            new_proposed_end_time = DateAdd("n", imins, bucket2(b2num).eventdt) 'could exceed out time!
'            If new_proposed_end_time >= upperdt Then
'                minupperidx = b2num
'                w = w - 1
'                used_waiver_on_end = True
'            End If
'        End If
        If minupperidx = 0 Then  'half LOS not possible
            all_ok = False
            i = b2num 'end loop
        Else
            'now check intervals from i to minupperidx: a waiver may be in play at end
            j_cannot_use_waiver = 0
            all_ok = True
'dvprint "i start=" & i
'dvprint "i time=" & bucket2(i).eventdt
            For j = i To minupperidx - 1
'                all_ok = all_ok And (DateAdd("n", imins, bucket2(j).eventdt) >= bucket2(j + 1).eventdt)
'dvprint "j=" & j
'dvprint "j cannot use=" & j_cannot_use_waiver
'dvprint "Compare: " & bucket2(j).eventdt & " -> " & bucket2(j + 1).eventdt
                If DateAdd("n", imins, bucket2(j).eventdt) < bucket2(j + 1).eventdt Then
                    If w > 0 Then 'we can use a waiver
                      If j <> j_cannot_use_waiver Then 'we can
'dvprint "check within sequence"
                        If DateAdd("n", 2 * imins, bucket2(j).eventdt) >= bucket2(j + 1).eventdt Then
                            bucket2(j).using_waiver = True
                            j_cannot_use_waiver = j + 1
                            w = w - 1
                        Else
                            all_ok = False
                        End If
                      Else
                        all_ok = False
                      End If
                    Else
                      all_ok = False
                    End If
                End If
            Next j
            If all_ok Then
                i = b2num 'end loop
            Else
                'reset waivers
                For j = 1 To b2num
                    bucket2(j).using_waiver = False
                Next j
            End If
        End If
    Next i
    
    If all_ok Then
        SetInd ind, "Qualifies for q" & imins & "mins for duration of half-LOS=" & addmins & " minutes."
    Else
        dprint "Does not meet frequency criteria for indicator #" & ind
        'assign indexes for the dump to follow
        istart = 1
        minupperidx = b2num
    End If
    
    If num_waivers > w Then
        w2 = 0
        For j = 1 To b2num
            If bucket2(j).using_waiver Then
                w2 = w2 + 1
                dprint "Waiver " & w2 & ": " & DateAdd("n", imins, bucket2(j).eventdt)
            End If
        Next j
'        If used_waiver_on_end Then
'            w2 = w2 + 1
'            dprint "Waiver " & w2 & ": " & DateAdd("n", imins, bucket2(b2num).eventdt)
'        End If
    End If
    
    sql = "select event_datetime,category,description,field_name,result from chart_item " & WhereBase & b_filter & b_excl
    sql = sql & " and event_datetime between " & g_dbutil.SQL_DateTime(bucket2(istart).eventdt)
    sql = sql & " and " & g_dbutil.SQL_DateTime(bucket2(minupperidx).eventdt) & " order by event_datetime"
    'dvprint sql
    rs.Open sql, g_cnADO
    Do While Not rs.EOF
        dprint "  " & rs(0) & ": " & g_dbutil.DBToString(rs(1)) & "; " & g_dbutil.DBToString(rs(2)) & "; " & g_dbutil.DBToString(rs(3)) & "; " & g_dbutil.DBToString(rs(4))
        rs.MoveNext
    Loop
    rs.Close
    
    ResetWaivers
    
End Sub

'Private Function CheckBucketFreq(ByRef found_what As String) As Boolean
'    Dim i As Integer, j As Integer, last As Integer
'    Dim rangelen As Integer
'    Dim rangefilled As Boolean
'
'    'numbuckets  1   2   3   4   5  6   7
'    'div2        0   1   1   2   2  3   3
'    'majority    1   2   2   3   3  4   4  = numbuckets div 2 + 1
'    'intervals   x   2
'    'last        1   1   2   2   3  3   4 = (numbuckets-1) div 2 + 1
'    'mod 2       1   0   1   0   1  0   1
'    '^mod2       0   1   0   1   0  1   0
'
'    rangelen = num_buckets \ 2 + 1 'majority
'    If rangelen = 1 Then
'        CheckBucketFreq = False
'        Exit Function
'    End If
'    last = (num_buckets - 1) \ 2 + 1 'last start
'    For i = 1 To last
'        rangefilled = True
'        For j = 0 To rangelen - 1
'            If i + j <= num_buckets Then
'                rangefilled = rangefilled And (bucket(i + j).count > 0)
'            End If
'        Next j
'        If rangefilled Then i = last + 2
'    Next i
'    If rangefilled Then found_what = found_what & " found in at least half of LOS."
'    CheckBucketFreq = rangefilled
'
'End Function
'Private Sub PrintBuckets()
'    Dim i As Integer, j As Integer
'    Dim rangelen As Integer
'    Dim s As String
'
'    s = ""
'    dprint "Bucket Size=" & bucketsize & "; num buckets=" & num_buckets & "; contig needed=" & num_buckets \ 2 + 1
'    For i = 1 To num_buckets
'        s = s & "," & bucket(i).count
'    Next i
'    dprint s
'
'End Sub

'Private Function Check20As60BucketFreq(ByRef found_what As String) As Boolean
'    Dim i As Integer, j As Integer, adj As Integer
'    Dim rangelen As Integer
'    Dim rangefilled As Boolean
'    Dim num60 As Integer
'    Dim buck60() As Boolean
'
'dvprint "enter Vis checkas60"
'    num60 = num_buckets \ 3
'    If (num_buckets Mod 3) > 0 Then num60 = num60 + 1
'
'    ReDim buck60(num60)
'    For i = 1 To num60
'        buck60(i) = False
'        For j = (i - 1) * 3 + 1 To (i - 1) * 3 + 3
'            If j <= num_buckets Then
'                buck60(i) = buck60(i) Or (bucket(j).count > 0)
'            End If
'        Next j
'    Next i
'
'    rangelen = num60 \ 2 + 1
'    adj = (num60 - 1) \ 2 + 1
'    If (num60 Mod 2 > 0) Then
'        rangelen = rangelen + 1
'        adj = 1
'    End If
'    For i = 1 To adj
'        rangefilled = True
'        For j = 0 To rangelen - 1
'            If i + j <= num60 Then
'                rangefilled = rangefilled And buck60(i + j)
'            End If
'        Next j
'        If rangefilled Then i = adj + 2
'    Next i
'    If rangefilled Then found_what = found_what & " found in at least half of LOS."
'    Check20As60BucketFreq = rangefilled
'dvprint "Exit Vis checkas60"
'
'End Function

'20 20 20 20 20 20 20 20 20 20 20 20 20 20 20 20 20 20
'    1        1        1        1        1        1
Private Function GetTxArea() As String
    Dim sql As String
    Dim rs As New Recordset
    Dim t As String
    
    GetTxArea = ""

    sql = "select result from chart_item " & WHERE_ENCOUNTER & AND_UNIT & AndSimpleItemFilter("TREATMENT_AREA", "", "", "", "")
    dvprint sql
    rs.Open sql, g_cnADO
'    dvprint sql

    If Not rs.EOF Then
        If IsNull(rs(0)) Then
            rs.Close
            Exit Function
        Else
            t = rs(0)
            rs.Close
        End If
    Else
        rs.Close
    End If
    
    GetTxArea = t

End Function

Private Sub Qdisposition()
    Dim sql As String
    Dim res As String
    Dim rs As New Recordset
    Dim evdt As Date
'Look for latest disposition

'NEW ENHANCE:
'Final dispo goes to EDOB and not ED Visit if both locations exist.
'If only ed visit, then give it to ed visit.

'always get last one of these qualifying ones
'if has ED OBS (inpt), then no dispo in Visit.

'    sql = "select max(event_datetime) from chart_item " & WHERE_ENCOUNTER & AND_UNIT
'    rs.Open sql, g_cnADO
'    maxevdt = rs(0)
'    rs.Close
    sql = "select count(distinct(u.unit_id)) from chart_item as ci inner join unit as u on (u.unit_id=ci.unit_id)"
    sql = sql & " inner join unit_param as up on (u.unit_id=up.unit_id) " & WHERE_ENCOUNTER & AND_PULL_RANGE
    sql = sql & " and up.methodology_id=12 and up.expiration_datetime>getdate()"
    rs.Open sql, g_cnADO
    If rs(0) >= 1 Then 'there are ED OB units: Don't set dispo here.
        rs.Close
        Exit Sub
    End If
    rs.Close
    
    'Include Discharge as a place holder for time b/c max dispo time is the one taken.
    'If discharge, then dont trigger any dispo.  But if tied for same time, then other takes precedence.
    'note that ordering by time desc is key here.
    sql = "select result,event_datetime from chart_item " & WHERE_ENCOUNTER & AND_UNIT & AND_PULL_RANGE
    sql = sql & " and ((field_name='Disposition Type'"
    sql = sql & " and result in ('Expired','Transfer to External Facility','Inpatient Admit','Observation Admit','Admit-Pending UR Review','Discharge','Hospital Observation','Inpatient Admission','Hospital Observation (No ED OBS Bed)'))"
    sql = sql & " or (field_name='Disposition'"
    sql = sql & " and result in ('Transport to CRU','Transport to Inpatient Rehab Facility','Discharge')))"
    sql = sql & " order by event_datetime desc"
'    sql = sql & " and event_datetime='" & maxevdt & "'"
    rs.Open sql, g_cnADO
    evdt = 0
    Do While Not rs.EOF
        If rs(1) >= evdt Then
            evdt = rs(1)
            If IsNull(rs(0)) Then
                res = ""
            Else
                res = Trim$(rs(0))
                If res = "Expired" Then
                    SetInd 20, "Disposition Type: " & res
                ElseIf res = "Transfer to External Facility" Or res = "Transport to CRU" Or res = "Transport to Inpatient Rehab Facility" Then
                    SetInd 19, "Disposition Type: " & res
                ElseIf res = "Inpatient Admit" Or res = "Observation Admit" Or res = "Admit-Pending UR Review" Or res = "Hospital Observation" Or res = "Inpatient Admission" Or res = "Hospital Observation (No ED OBS Bed)" Then
                    SetInd 18, "Disposition Type: " & res
                End If
            End If
        End If
        rs.MoveNext
    Loop
    rs.Close
    
'    res = ""
'    If Not rs.EOF Then
'        If Not IsNull(rs(0)) Then res = rs(0)
'    End If
'    rs.Close
'    res = Trim$(res)
'    If res = "Discharge" Then 'dont set a dispo indicator
'    ElseIf res = "Expired" Then
'        SetInd 20, "Disposition Type: " & res
'    ElseIf res = "Transfer to External Facility" Or res = "Transport to CRU" Or res = "Transport to Inpatient Rehab Facility" Then
'        SetInd 19, "Disposition Type: " & res
'    ElseIf res = "Inpatient Admit" Or res = "Observation Admit" Or res = "Admit-Pending UR Review" Then
'        SetInd 18, "Disposition Type: " & res
'    End If
    

End Sub

Private Sub CheckBolus()
'ENHANCE SetIndIfAllResults 11, "MEDICATION SERVICE", "", "", "", "bolus,IV FLUID,ordered by" need bolus+ml
    Dim sql As String
    Dim rs As New Recordset
    Dim done As Boolean
    Dim dosepos As Integer, boluspos As Integer, widepos As Integer, ratepos As Integer, mlpos As Integer
    Dim ivpos As Integer
    Dim r As String, dosestr As String, ivstr As String
    Dim ratemlhr As Integer, rate_startpos As Integer, rate_endpos As Integer
    Dim ratemlhrstr As String
    
    
    sql = "select result,event_datetime from chart_item " & WhereBase & " and category='MEDICATION SERVICE' and "
    sql = sql & " result like '%iv fluid infusion%' and result like '%ordered by%'"
    'sql = sql & " (result like '%ml[^/]%')"
'dvprint sql
    rs.Open sql, g_cnADO
'    dvprint sql
    Do While Not rs.EOF 'And Not done
        r = rs(0)
        'check dose
        dosepos = InStr(1, r, "Dose", vbTextCompare)
        dosestr = ""
        ratepos = 0
dvprint "Dose pos=" & dosepos
        If dosepos > 0 Then
            dosestr = Mid$(r, dosepos, 80)
            boluspos = InStr(1, dosestr, " bolus", vbTextCompare) 'cannot be non-bolus
            widepos = InStr(1, dosestr, "wide open", vbTextCompare)
            ratepos = InStr(1, dosestr, "ml/hr", vbTextCompare)
'dvprint "Wide pos=" & widepos
'dvprint "Rate pos=" & ratepos
        End If
        If ratepos = 0 Then
            mlpos = InStr(1, dosestr, "ml", vbTextCompare)
'dvprint "ML pos=" & mlpos
            If mlpos > 0 Then 'continue
                ivpos = InStr(1, r, "IV Site #", vbTextCompare) 'iv site #n
                ivstr = ""
                If ivpos > 0 Then ivstr = Mid$(r, ivpos, Len(r) - ivpos + 1)
                boluspos = boluspos + InStr(1, ivstr, " bolus", vbTextCompare) 'cannot be non-bolus
'dvprint "Bolus pos=" & boluspos
            End If
        Else 'get rate ml/hr
            rate_endpos = InStrRev(dosestr, " ", ratepos, vbTextCompare)
            rate_startpos = InStrRev(dosestr, " ", rate_endpos - 1, vbTextCompare)
            ratemlhr = 0
'dvprint "Rate pos=" & ratepos
'dvprint "Rate start=" & rate_startpos
'dvprint "Rate end=" & rate_endpos
            If rate_endpos > rate_startpos Then
                ratemlhrstr = Mid$(dosestr, rate_startpos + 1, rate_endpos - rate_startpos - 1)
                If IsNumeric(ratemlhrstr) Then
                    ratemlhr = ratemlhrstr
'dvprint "Rate =" & ratemlhr
                End If
            'Example: "dose: 201 ml/hr"  ratepos=11 rate_endpos=10 rate_startpos=6  ratemlhr=7,3
            End If
        End If
        If ((boluspos > 0 Or widepos > 0) And mlpos > 0) Or ratemlhr > 0 Then
            If boluspos > 0 Then
                dosestr = "Bolus"
            ElseIf widepos > 0 Then
                dosestr = "Wide Open"
            ElseIf ratemlhr > 0 Then
                dosestr = "Rate=" & ratemlhr & " mL/hr"
            End If
            If ratepos = 0 Or ratemlhr >= 201 Then
                SetInd 11, rs(1) & ": IV Fluid infusion+ordered by+" & dosestr '& "..." & ivstr
            ElseIf ratepos > 0 And ratemlhr <= 200 Then
                SetInd 10, rs(1) & ": IV Fluid infusion+ordered by+" & dosestr '& "..." & ivstr
            End If
        End If
        'Dose:<LT>/B> Wide Open: 1000 mL : IV FLUID Infusion
        rs.MoveNext
    Loop
    rs.Close
    
'This below should not trigger this.
'Order: Ceftriaxone: 1 gm/10 mL Syringe - <LT>B>Dose:<LT>/B> 1 gram(s) : IV Push ** cefTRIAXone Inj (ceftriaxone sodium) [*] ** water for injection, sterile IV (water for injection,sterile) [*] Notes: **Pyxis Medication** Ordered by: Amy, M.D. Oneil Entered by: Amy, M.D. Oneil Sun Aug 11, 2013 14:07  Documented as given by: Dennis, R.N. Weivoda Sun Aug 11, 2013 14:40; IV SITE #1; IV SITE #1 IV fluids established; IV SITE #1 1st bag hung; amount 1 Liter; IV SITE #1 bolus of 1000 ml established

'good: wide open or bolus
'Order: sodium chloride 0.9 % IV (0.9 % sodium chloride) : 0.9 % : [1000 mL(s)] INTRAVENOUS SOLUTION - <LT>B>Dose:<LT>/B> Wide Open: 1000 mL : IV FLUID Infusion Notes: One liter bolus and then maintenace at 150 cc/hr Ordered by: Caitlin, M.D. Loprinzi Brauer Entered by: Caitlin, M.D. Loprinzi Brauer Sun Aug 11, 2013 13:15  Documented as given by: Crystal, R.N. Stensland Sun Aug 11, 2013 13:16; IV SITE #1; IV SITE #1 IV fluids established; amount 1 Liter; IV SITE #1 Rate of bolus; wide open
'Order: sodium chloride 0.9 % IV (0.9 % sodium chloride) : 0.9 % : [1000 mL(s)] INTRAVENOUS SOLUTION - <LT>B>Dose:<LT>/B> Bolus: 1000 mL : IV FLUID Infusion Ordered by: Alexandra, M.D. Messerli Entered by: Alexandra, M.D. Messerli Sun Aug 11, 2013 14:19; Acknowledged by: Emma, R.N. Schultz Sun Aug 11, 2013 14:48 Documented as given by: Erin, R.N. Salo Sun Aug 11, 2013 15:41; IV SITE #1; IV SITE #1 IV fluids established; IV SITE #1 1st bag hung; amount 1 Liter
'Order: sodium chloride 0.9 % IV (0.9 % sodium chloride) : 0.9 % : [1000 mL(s)] INTRAVENOUS SOLUTION - <LT>B>Dose:<LT>/B> 500 mL : IV FLUID Infusion Ordered by: Andy, M.D. Boggust Entered by: Karen, R.N. Koch Tue Aug 13, 2013 14:03  Documented as given by: Karen, R.N. Koch Tue Aug 13, 2013 14:03; IV SITE #1; IV SITE #1 IV fluids established; IV SITE #1 bolus of 500 ml established
'Order: sodium chloride 0.9 % IV (0.9 % sodium chloride) : 0.9 % : [1000 mL(s)] INTRAVENOUS SOLUTION - <LT>B>Dose:<LT>/B> Bolus: 1000 mL : IV FLUID Infusion Ordered by: Andy, M.D. Boggust Entered by: Karen, R.N. Koch Tue Aug 13, 2013 14:03  Documented as given by: Karen, R.N. Koch Tue Aug 13, 2013 14:13; IV SITE #2; IV fluids established; Rate of bolus; 1000 ml/hr
'Order: sodium chloride 0.9 % IV (0.9 % sodium chloride) : 0.9 % : [1000 mL(s)] INTRAVENOUS SOLUTION - <LT>B>Dose:<LT>/B> Wide Open: 1000 mL : IV FLUID Infusion Ordered by: Andy, M.D. Boggust Entered by: Karen, R.N. Koch Tue Aug 13, 2013 14:03  Documented as given by: Karen, R.N. Koch Tue Aug 13, 2013 14:14; IV SITE #3; IV fluids established; Rate of bolus; wide open; via rapid infuser; Fluid warmer used

'Order: sodium chloride 0.9 % IV (0.9 % sodium chloride) : 0.9 % : [1000 mL(s)] INTRAVENOUS SOLUTION - <LT>B>Dose:<LT>/B> 500 mL : IV FLUID Infusion Ordered by: Andy, M.D. Boggust Entered by: Karen, R.N. Koch Tue Aug 13, 2013 14:03  Documented as given by: Karen, R.N. Koch Tue Aug 13, 2013 14:03; IV SITE #1; IV SITE #1 IV fluids established; IV SITE #1 bolus of 500 ml established
'Order: sodium chloride 0.9 % IV (0.9 % sodium chloride) : 0.9 % : [1000 mL(s)] INTRAVENOUS SOLUTION - <LT>B>Dose:<LT>/B> Bolus: 1000 mL : IV FLUID Infusion Ordered by: Andy, M.D. Boggust Entered by: Karen, R.N. Koch Tue Aug 13, 2013 14:03  Documented as given by: Karen, R.N. Koch Tue Aug 13, 2013 14:13; IV SITE #2; IV fluids established; Rate of bolus; 1000 ml/hr
'Order: sodium chloride 0.9 % IV (0.9 % sodium chloride) : 0.9 % : [1000 mL(s)] INTRAVENOUS SOLUTION - <LT>B>Dose:<LT>/B> Wide Open: 1000 mL : IV FLUID Infusion Ordered by: Andy, M.D. Boggust Entered by: Karen, R.N. Koch Tue Aug 13, 2013 14:03  Documented as given by: Karen, R.N. Koch Tue Aug 13, 2013 14:14; IV SITE #3; IV fluids established; Rate of bolus; wide open; via rapid infuser; Fluid warmer used
'Order: sodium chloride 0.9 % IV (0.9 % sodium chloride) : 0.9 % : [1000 mL(s)] INTRAVENOUS SOLUTION - <LT>B>Dose:<LT>/B> 1000 mL : IV FLUID Infusion Ordered by: Andy, M.D. Boggust Entered by: Karen, R.N. Koch Tue Aug 13, 2013 14:24  Documented as given by: Karen, R.N. Koch Tue Aug 13, 2013 14:24; IV SITE #1; IV SITE #1 IV fluids established; IV SITE #1 bolus established; 999; ml

'BUT NOT:
'Order: sodium chloride 0.9 % IV (0.9 % sodium chloride) : 0.9 % : [1000 mL(s)] INTRAVENOUS SOLUTION - <LT>B>Dose:<LT>/B> 1000 mL/hr : IV FLUID Infusion Notes: bolus Ordered by: Andy, M.D. Boggust Entered by: Karen, R.N. Koch Tue Aug 13, 2013 14:16  Documented as given by: Karen, R.N. Koch Tue Aug 13, 2013 14:16; IV SITE #4; IV fluids established; IV bolus of 1000 ml established; Rate of bolus; 1000 ml/hr

'iv fluid infusion
'ml not ml/hr
'for bolus:   dose:  bolus or wide open xxx ml OR   in IV SITE #1 bolus of 1000 ml established  OR
'

'Order: sodium chloride 0.9 % IV (0.9 % sodium chloride) : 0.9 % : [1000 mL(s)] INTRAVENOUS SOLUTION - <LT>B>Dose:<LT>/B> 1000 mL : IV FLUID Infusion Ordered by: Andy, M.D. Boggust Entered by: Karen, R.N. Koch Wed Aug 14, 2013 11:18  Documented as given by: Karen, R.N. Koch Wed Aug 14, 2013 11:18; IV SITE #1; IV SITE #1 IV fluids established; IV SITE #1 Rate of infusion (non-bolus) Infusing at 300 ml/hr
'Order: sodium chloride 0.9 % IV (0.9 % sodium chloride) : 0.9 % : [1000 mL(s)] INTRAVENOUS SOLUTION - <LT>B>Dose:<LT>/B> 1000 mL : IV FLUID Infusion Ordered by: Peter, M.D. Smars Entered by: Karen, R.N. Koch Wed Aug 14, 2013 11:42  Documented as given by: Karen, R.N. Koch Wed Aug 14, 2013 11:43; IV SITE #2; IV fluids established; IV bolus of 1000 ml established; After bolus completed rate changed to KVO
End Sub
Private Sub CheckIVMedInfusion()
'ENHANCE SetIndIfAllResults 10, "", "", "", "", "IV MED infusion,ordered by"  Need at least 2 of these
    Dim sql As String
    Dim rs As New Recordset
    
    sql = "select count(distinct(event_datetime)) from chart_item " & WhereBase & " and "
    sql = sql & " result like '%iv med infusion%' and result like '%ordered by%'"
    'dvprint sql
    rs.Open sql, g_cnADO
'    dvprint sql
    If rs(0) > 1 Then SetInd 10, "At least 2 IV Med Infusion Ordered By found."
    rs.Close
    

End Sub

Private Sub SetMaxWaivers(imins As Integer)
    Dim qstr As String
    Dim rstr As String

    If imins = 20 Then 'q15
        If m_pat.locary(m_locindex).range < 80 Then
            num_waivers = 0
            rstr = "LOS less than 80min"
        ElseIf m_pat.locary(m_locindex).range >= 80 And m_pat.locary(m_locindex).range < 180 Then
            num_waivers = 1
            rstr = "LOS is 80-180min"
        ElseIf m_pat.locary(m_locindex).range > 180 Then
            num_waivers = 2
            rstr = "LOS is greater than 180min"
        End If
    ElseIf imins = 70 Then 'q1
        If m_pat.locary(m_locindex).range < 360 Then
            num_waivers = 0
            rstr = "LOS is less than 6hrs"
        Else
            num_waivers = m_pat.locary(m_locindex).range \ 360
            rstr = "LOS/360=" & m_pat.locary(m_locindex).range / 360
        End If
    ElseIf imins = 40 Then 'q30
        If m_pat.locary(m_locindex).range < 180 Then
            num_waivers = 0
            rstr = "LOS is less than 3hrs"
        Else
            num_waivers = m_pat.locary(m_locindex).range \ 180
            rstr = "LOS/180=" & m_pat.locary(m_locindex).range / 180
        End If
    End If
    If imins = 20 Then
        qstr = "q15min"
    ElseIf imins = 70 Then
        qstr = "q1hr"
    ElseIf imins = 40 Then
        qstr = "q30min"
    End If
    dprint qstr & ":Max waivers allowed=" & num_waivers & " (LOS=" & m_pat.locary(m_locindex).range & ") " & rstr

End Sub

Public Sub ResetWaivers()
    Dim i As Integer

    For i = 1 To MAX_BUCKETS
        bucket2(i).using_waiver = False
    Next i

End Sub
Private Sub CheckOtherIntervention()
    Dim sql As String
    Dim sql2 As String
    Dim rs As New Recordset
    Dim rs2 As New Recordset
    Dim ind8 As Boolean, ind9 As Boolean
    
    sql = "select event_datetime from chart_item " & WhereBase & " and field_name='Other intervention used'"
    rs.Open sql, g_cnADO
    Do While Not rs.EOF
        ind8 = False
        ind9 = False
        sql2 = "select count(*) from chart_item " & WhereBase & " and event_datetime=" & g_dbutil.SQL_DateTime(rs(0))
        sql2 = sql2 & " and (result like '%Behavioral Management needed and given%'"
        sql2 = sql2 & " or result like '%Emotional support needed and given%')"
        rs2.Open sql2, g_cnADO
        If rs2(0) > 0 Then
            ind8 = True
        End If
        rs2.Close
        
        If ind8 Then
            sql2 = "select count(*) from chart_item " & WhereBase & " and event_datetime=" & g_dbutil.SQL_DateTime(rs(0))
            sql2 = sql2 & " and (result like '%Behavioral Management needed and given%'"
            sql2 = sql2 & " or result like '%Emotional support needed and given%')"
            sql2 = sql2 & " and (result like '%Intervention greater than 30 minutes%')"
            rs2.Open sql2, g_cnADO
            If rs2(0) > 0 Then
                ind9 = True
            End If
            rs2.Close
        End If
        
        rs.MoveNext
    Loop
    rs.Close
   
    If ind9 Then
        SetInd 9, "Behav/Emotional support + Other intervention q30min"
    ElseIf ind8 Then
        SetInd 8, "Behav/Emotional support + Other intervention"
    End If
        

End Sub

