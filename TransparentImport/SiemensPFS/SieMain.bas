Attribute VB_Name = "SieMain"
Option Explicit
'The location of the input is determined by the first line of the dictionary.
'
' This is the main module for SiePFS
' File is formatted as:
'    <Header>
'       <Event>
'       <Event>
'         ...
'
Const TRANSP_FILENAME = "Transparent.txt"
Const TRANSP_INLOG_FILE_LIFE = 10 'days
Const TRANSP_OUTLOG_FILE_LIFE = 10 'days
Const TRANSP_DEBUG_FILE_LIFE = 10 'days
Const DIC_FNAME = "SIWINPFS"
Const DNLD_FNAME = "TCQUERY"

'HEADER RECORD
Const MAX_INDICATORS = 50

Const START_ACCT_NUM = 1
Const LEN_ACCT_NUM = 20

Const START_PTNAME = 21
Const LEN_PTNAME = 30

Const START_UNIT = 51
Const LEN_UNIT = 10

Const START_RM = 61
Const LEN_RM = 10

Const START_BED = 71
Const LEN_BED = 5

Const START_FACILITY = 76
Const LEN_FAC = 5

Const START_CLASS_DT = 81
Const LEN_DT = 14


'EVENT RECORD
Const START_EVENT_DT = 5
'Const LEN_DT = 14

Const START_EVENT = 20
Const LEN_EVENT = 15

Const START_EVENT_DSC = 35
Const LEN_EVENT_DSC = 40

Const START_FREQ = 75
Const LEN_FREQ = 10

Const START_TREAT = 85
Const LEN_TREAT = 16

Const START_RESULT = 101
Const LEN_RESULT = 75

Const MAX_RANGE = 1440  '24 hrs in minutes; this is overridden by the -range arg.

Const RANGE_DENOMINATOR = 24 * 60   'For proportioning the count of Med/Pulm/Cardio

Const MAX_UNIQUE_CHART_TIMES = 20

Dim g_util As New RegistryUtils

Dim infn As String
Dim fnames() As String
Dim outfn As String
Dim inlogfile As Integer
Dim inlogname As String
Dim outlogfile As Integer
Dim outlogname As String
Dim dbugfile As Integer
Dim dbugname As String

Private Type sieindicator
  indwinpfs As Integer  'winpfs indic number associated with this cerner event
  eventid As String     'Event_CD of this indicator
  chartkey As String    'look for this key in charted result
  gavefreq As Boolean   'file supplied the frequency; ignore subsequent freqs
  check_freq As Boolean 'flag to check frequency
  freq_basis As Integer 'frequency required for this indicator (in minutes)
  act_freq As Integer   'calculated frequency from data (in minutes)
  last_datetime As String 'last date time of event found in this patient
  num_found As Integer  'number of times this was found in this patient
  also_mark As String   'if this indicator is marked, then also mark these.
  charttime(MAX_UNIQUE_CHART_TIMES) As String
End Type

Private Type indicator_data
    checked As Boolean
    also_mark As String
End Type

Dim dicary() As sieindicator
Dim dicnum As Integer
Dim inDirPath As String  'path of input file
Dim winpfspath As String 'path of winpfs
Dim winlogpath As String 'path of winpfs\log
Dim winloadpath As String ' path of winpfs\load_me

Dim datafile As Integer
Dim outfile As Integer

Dim inds(MAX_INDICATORS) As indicator_data
Dim grps(MAX_INDICATORS) As Integer

Dim lastname As String
Dim firstname As String
Dim unitname As String
Dim roomname As String
Dim bedname As String
Dim acctnum As String
Dim classdt As String
Dim classdate As String
Dim classtime As String
Dim nowdt As Date
Dim intime As String
Dim saveintime As String

Dim range As Single  'number of minutes in the scope of time for freq.
Dim dbugon As Boolean
Dim suppressDbugLog As Boolean
Dim effdateon As Boolean
Dim effdate As String
Dim efftimeon As Boolean
Dim efftime As String
Dim pulltimeon As Boolean
Dim pulltime As String
Dim pulldateon As Boolean
Dim pulldate As String

Dim phmmarked As Boolean
Dim painassessed As Boolean

Sub Main()
    Const RANGE_PARAM = "-range="
    Const EFFECTIVE_PARAM = "-efftime="
    Const DBUG_ON = "-debug"
    Const ALTER_DATE = "-effdate="
    Const PULL_TIME = "-pulltime="
    Const PULL_DATE = "-pulldate="
    Dim n As Integer
    Dim i As Integer
    Dim cmdLine As String
    Dim epos As Integer
    Dim rpos As Integer
    Dim effective As String
    Dim h As String
    Dim t As Date
    Dim adpos As Integer
    
    '-efftime=hhmm  This is the time at which the classification is effective.
    '                 The date associated with this time is taken from header classdate.
    '                 If not specified, then -efftime is assumed to be the classtime
    '                 from the header.
    '-effdate=yyyymmdd   This is the date which, in combination with the
    '                 -effective time is used to specify the effective datetime.
    '                 If not specified, then -effdate is assumed to be the classdate
    '                 from the header.
    'Special value:  -date=yesterday means Now's yesterday.
        
    '-pulltime=hhmm  This is the time starting from which the pull is to
    '                look backwards from.
    '                If not specified, then this time is assumed to be the -effective time.
    '-pulldate=yyyymmdd This is the date of the pulltime.
    '                If not specified, then this is assumed to be Now's date.
    
    '-range=nnnn  This is the number of minutes backwards from the pull time
    '             that defines valid range of charting events.
    
    nowdt = Now
    cmdLine = LCase(Command$)
    
    rpos = InStr(cmdLine, RANGE_PARAM)
    epos = InStr(cmdLine, EFFECTIVE_PARAM)
    
    dbugon = (InStr(cmdLine, DBUG_ON) > 0)
    suppressDbugLog = False
    
    effdateon = (InStr(cmdLine, ALTER_DATE) > 0)
    If effdateon Then
        adpos = InStr(cmdLine, ALTER_DATE)
        If UCase$(Mid$(cmdLine, adpos + Len(ALTER_DATE), 8)) = "YESTERDA" Then
            effdate = Format(DateAdd("d", -1, g_util.DateOnly(nowdt)), "yyyymmdd")
        Else
            effdate = Mid$(cmdLine, adpos + Len(ALTER_DATE), 8) 'yyyymmdd
        End If
    End If
    
    efftimeon = (InStr(cmdLine, EFFECTIVE_PARAM) > 0)
    
    pulltimeon = (InStr(cmdLine, PULL_TIME) > 0)
    If pulltimeon Then
        pulltime = Mid$(cmdLine, InStr(cmdLine, PULL_TIME) + Len(PULL_TIME), 4)
    End If
    
    pulldateon = (InStr(cmdLine, PULL_DATE) > 0)
    If pulldateon Then
        pulldate = Mid$(cmdLine, InStr(cmdLine, PULL_DATE) + Len(PULL_DATE), 8)
    End If

    If epos > 0 Then
        effective = Mid$(cmdLine, InStr(cmdLine, EFFECTIVE_PARAM) + Len(EFFECTIVE_PARAM), 4)
    End If
    
    If rpos > 0 Then
        range = val(Mid$(cmdLine, InStr(cmdLine, RANGE_PARAM) + Len(RANGE_PARAM), 4))
    End If
    
    If range <= 0 Then
        range = MAX_RANGE
    End If
    
'    If effective = "" Then
'        effective = Format(nowdt, "hhnn")
'    End If
    
    intime = ""
    
    If (LoadDictionary) Then
        InitGroups
        MakeLogFiles
        dprint -1, 0, "range=" & range
        dprint -1, 0, "effdate=" & effdate
        dprint -1, 0, "efftime=" & effective
        dprint -1, 0, "pulltime=" & pulltime
        dprint -1, 0, "pulldate=" & pulldate
        If effective <> "" Then
            ' command parameter needs to be in form hhmm ONLY
            If IsNumeric(effective) Then
                If Len(effective) < 4 Then
                    If Len(effective) = 1 Then
                        effective = "000" & effective
                    ElseIf Len(effective) = 2 Then
                        effective = "00" & effective
                    Else
                        effective = "0" & effective
                    End If
                Else
                    effective = Mid$(effective, 1, 4)
                End If
                effective = Mid$(effective, 1, 2) & ":" & Mid$(effective, 3, 2)
                t = CDate(effective)
                If IsDate(t) Then
                    effective = Mid$(effective, 1, 2) & Mid$(effective, 4, 2)
                    i = year(g_util.DateOnly(FileDateTime(inlogname)))
                    intime = CStr(i) & Mid$(inlogname, Len(inlogname) - Len("mmdd.log") + 1, Len("mmdd")) & effective
                    If effdateon Then
                        intime = effdate & Mid$(intime, 9, 4)
                    End If
                    saveintime = intime
                End If
            End If
        End If
        n = GetAllInputFilenames
'        i = 0      ONLY PROCESS THE LATEST FILE 11/4/05
        i = n - 1  'ONLY PROCESS THE LATEST FILE 11/4/05
        While i < n
            i = i + 1
            infn = inDirPath + "\" + fnames(i)
            Process
        Wend
        CloseLogFiles
        DeleteOldLogs
    End If

End Sub
Private Function LoadDictionary() As Boolean
    Dim dicfn As String
    Dim dicfile As Integer
    Dim buf As String

    dicnum = 0
    dicfile = FreeFile
    dicfn = App.Path & "\" & DIC_FNAME & ".DAT"
    Open dicfn For Input As #dicfile
    While Not EOF(dicfile)
        dicnum = dicnum + 1
        If (dicnum = 1) Then
            Line Input #dicfile, inDirPath
        Else
            Line Input #dicfile, buf
            ReDim Preserve dicary(0 To dicnum - 1)
            dicary(dicnum - 1).indwinpfs = val(Mid$(buf, 1, 2))
            dicary(dicnum - 1).eventid = Trim$(Mid$(buf, 4, 17))
            dicary(dicnum - 1).chartkey = Trim$(Mid$(buf, 38, 75))
            dicary(dicnum - 1).check_freq = (Mid$(buf, 3, 1) = "*")
            dicary(dicnum - 1).freq_basis = val(Trim$(Mid$(buf, 22, 4)))
            dicary(dicnum - 1).also_mark = Trim$(Mid$(buf, 28, 10))
        End If
    Wend
    Close #dicfile
    dicnum = dicnum - 1
    LoadDictionary = True
End Function

Private Function GetAllInputFilenames() As Integer
    'The location is determined by the first line of the dictionary.
    Const INITIAL_BLOCK_SIZE = 24
    Dim i As Integer
    Dim infname As String
    
    outfn = winloadpath & "\" & TRANSP_FILENAME
    
    ReDim fnames(0 To INITIAL_BLOCK_SIZE) As String
    i = 0
    infname = Dir$(inDirPath + "\" & DNLD_FNAME & "*.TXT") 'returns ONLY the filename.  For wildcards.
    While infname <> ""
        i = i + 1
        If (i > UBound(fnames)) Then
            ReDim Preserve fnames(0 To i + INITIAL_BLOCK_SIZE) As String
        End If
        fnames(i) = infname
        infname = Dir$
    Wend
    
    BubbleSort fnames, i
    
    GetAllInputFilenames = i
    
End Function

Private Sub BubbleSort(arr As Variant, numEls As Integer)
    Dim lLoop1 As Integer
    Dim lLoop2 As Integer
    Dim lTemp As Variant
    
    For lLoop1 = numEls To LBound(arr) Step -1
       For lLoop2 = LBound(arr) + 1 To lLoop1
           If arr(lLoop2 - 1) > arr(lLoop2) Then
              lTemp = arr(lLoop2 - 1)
              arr(lLoop2 - 1) = arr(lLoop2)
              arr(lLoop2) = lTemp
           End If
       Next lLoop2
   Next lLoop1
End Sub

Private Sub DoPatientSummary()
    Dim highest_is_on As Boolean
    Dim g As Integer
    Dim i As Integer
    Dim p As Integer
    
    'Here is where you now have to go through the frequencies to
    'determine which indicators to mark for this pt.
    
    'First, set the act_freq to 1440 if num_found is 1
    'If .num_found > 0 AND .act_freq=0 then change .act_freq to 1440.
    'Leave Braden score alone because Braden score is the act_freq.
    For i = 1 To dicnum
        If dicary(i).num_found = 1 And dicary(i).eventid <> "BRDN TOT SCR" Then
            dicary(i).act_freq = 1440
        End If
    Next i
    
    CheckADLComplete
    CheckADLRehab
    CheckVitals
    CheckGCS
    CheckBraden
    CheckWound
    CheckExtCoord
    CheckMedComplex
    CheckFluids
    
    'Next, loop the dictionary to find the indicator according to frequency.
    For i = 1 To dicnum
        If dicary(i).num_found > 0 Then
            p = FindClosestFreq(i)
            If (p > 0) Then
                inds(dicary(p).indwinpfs).checked = True
                dprint dicary(p).indwinpfs, 1, "via fall through"
            
                If (dicary(p).num_found > 0 And Not inds(dicary(p).indwinpfs).checked) Then
                    dprint dicary(p).indwinpfs, -1, "***FAILED TO TURN ON***"
                End If
            
                If inds(dicary(p).indwinpfs).checked Then
                    If inds(dicary(p).indwinpfs).also_mark <> "" Then
                        'Only do the also mark if it originally arose from an also-mark ind.
                        ParseAlsoMark (inds(dicary(p).indwinpfs).also_mark)
                    End If
                End If
            End If
        End If
    Next i
    
    '03/23/06: ADL 2 is the minimum as decided in phone mtg on 3/22/06
'    If Not (inds(1).checked Or inds(2).checked Or inds(3).checked Or inds(4).checked) Then
'        dprint 2, 1, "via AtLeastOneADL"
'    End If
    'inds(2).checked = True
    
    'Now, if there are any mutually exclusive indicators marked
    'then remove the lesser ones.
    g = 0
    For i = MAX_INDICATORS To 1 Step -1
        If (grps(i) > 0) Then
            If (grps(i) <> g) Then
                g = grps(i)
                highest_is_on = inds(i).checked
            Else
                If highest_is_on Then
                    inds(i).checked = False
                Else
                    highest_is_on = inds(i).checked
                End If
            End If
        End If
    Next i
    
    'Make sure there is at least 1 ADL marked
    AtLeastOneADL
        
End Sub

Private Sub Process()
    Dim on_orders As Boolean
    
    Dim togo As Long, toread As Long
    Dim blocksize As Long
    Dim buf As String
    Dim p As Integer
    Dim i As Integer
    Dim start As Integer
    Dim d As String
    Dim formloading As Boolean
    Dim s As String
    Dim rows() As String
    Dim maxrow As Long
    Dim irow As Long
    Dim isodt As String
    Dim sNowdt As String
    Dim charting As String
    Dim prevcharting As String
    Dim res As String
    Dim cond1 As Boolean
    Dim cond2 As Boolean
    Dim rangedate As Date
    Dim rangedt As String
    Dim intimedate As Date
    Dim n As Integer
    Dim dupfound As Boolean
    Dim eventid As String
    Dim freq_given As Integer
    
    On Error GoTo errProcess
        
    formloading = True
'    cmdProcess.Enabled = False
'    cmdOutput.Enabled = False
    
    datafile = FreeFile
    Open infn For Input As #datafile
    s = Input(LOF(datafile), datafile)
    rows() = Split(s, vbCrLf)
    maxrow = UBound(rows)
    
    outfile = FreeFile
    Open outfn For Append As #outfile
    
    For irow = 0 To maxrow
        buf = rows(irow)
        Print #inlogfile, buf
        
        If Trim$(Mid$(buf, 1, 1)) = Chr$(12) Then 'do nothing -- pg break
        ElseIf Trim$(Mid$(buf, 1, 4)) = "" Then 'Event record
' There may be more than 1 dictionary entry with the same
' event code but with a different frequency.  So use the first occurrence
' of this event to keep the running frequency.  Then at then end,
' locate the appropriate indicator for the calculated frequency.
            freq_given = DetermineFreq(Trim$(Mid$(buf, START_FREQ, LEN_FREQ)))
            isodt = Trim$(Mid$(buf, START_EVENT_DT, LEN_DT))
            If pulltimeon Then
                If pulldateon Then
                    intimedate = DateSerial(Mid$(pulldate, 1, 4), Mid$(pulldate, 5, 2), Mid$(pulldate, 7, 2)) _
                               + TimeSerial(Mid$(pulltime, 1, 2), Mid$(pulltime, 3, 2), 0)
                Else
                    intimedate = g_util.DateOnly(nowdt) + TimeSerial(Mid$(pulltime, 1, 2), Mid$(pulltime, 3, 2), 0)
                End If
            Else
                If intime = "" Then
                    intimedate = Now 'just to make it valid
                Else
                    intimedate = DateSerial(Mid$(intime, 1, 4), Mid$(intime, 5, 2), Mid$(intime, 7, 2)) _
                            + TimeSerial(Mid$(intime, 9, 2), Mid$(intime, 11, 2), 0)
                End If
            End If
            'dprint -1, 0, "intimedate=" & intimedate
            rangedate = DateAdd("n", -range, intimedate) ' range minutes before intime
            rangedt = Format(rangedate, "yyyymmddhhnn")
            'dprint -1, 0, "rangedt=" & rangedt
'        Remove blinders for Newport -- rely entirely on data.
'            If rangedt > isodt Then
'                dprint -2, 0, buf
'            Else
                dprint -1, 0, buf
            eventid = Trim$(Mid(buf, START_EVENT, LEN_EVENT))
            If eventid = "BRDN TOT SCR" Then 'Braden score
                charting = " "
                p = DicIndex("BRDN TOT SCR", charting)
                If dicary(p).num_found > 0 Then
                    If dicary(p).act_freq > val(Trim$(Mid(buf, START_RESULT, LEN_RESULT))) Then
                        dicary(p).act_freq = val(Trim$(Mid(buf, START_RESULT, LEN_RESULT)))
                    End If
                Else
                    dicary(p).act_freq = val(Trim$(Mid(buf, START_RESULT, LEN_RESULT))) 'score
                End If
                dicary(p).num_found = dicary(p).num_found + 1
                dprint dicary(p).indwinpfs, -1, "num found = 1, freq=" & dicary(p).act_freq
            ElseIf eventid = "GCSGCSTOTAL" Then 'GC score
                charting = " "
                p = DicIndex("GCSGCSTOTAL", charting)
                If dicary(p).num_found > 0 Then
                    If dicary(p).act_freq > val(Trim$(Mid(buf, START_RESULT, LEN_RESULT))) Then
                        dicary(p).act_freq = val(Trim$(Mid(buf, START_RESULT, LEN_RESULT)))
                    End If
                Else
                    dicary(p).act_freq = val(Trim$(Mid(buf, START_RESULT, LEN_RESULT))) 'score
                End If
                dicary(p).num_found = dicary(p).num_found + 1
                dprint dicary(p).indwinpfs, -1, "num found = 1, freq=" & dicary(p).act_freq
            ElseIf eventid = "NRVSPNSCALE" Then 'pain assessed
                painassessed = True
            Else 'then some other event
            If InStr(eventid, "PHM") = 1 Then
                If (freq_given >= 240) Then
                    inds(14).checked = True
                ElseIf (freq_given >= 120) Then
                    inds(15).checked = True
                ElseIf (freq_given >= 1) Then
                    inds(16).checked = True
                End If
                phmmarked = True
            End If
            charting = Trim$(Mid(buf, START_RESULT, LEN_RESULT))
            prevcharting = charting
            p = DicIndex(eventid, charting)
            If eventid = "NUR11300027" Then
                inds(27).checked = True
                If charting <> "HOME" Then
                    inds(30).checked = True
                End If
            End If
            While p > 0 'for multiple charting results on the same event line (Cerner)
                If dicary(p).check_freq Then ' asterisk (*) is associated with dic item
                    If dicary(p).num_found = 0 Then
                        dicary(p).num_found = 1
                        dicary(p).last_datetime = Trim$(Mid(buf, START_EVENT_DT, LEN_DT))
                        dicary(p).charttime(dicary(p).num_found) = Trim$(Mid(buf, START_EVENT_DT, LEN_DT))
                        dicary(p).act_freq = 0
                        'If this is the only occurrence, then it will be changed
                        'to 1440 at summary time.
                        dprint dicary(p).indwinpfs, -1, "num found = 1, freq=" & dicary(p).act_freq & " Item=" & p
                        If freq_given > 0 Then
                            dicary(p).gavefreq = True
                            dicary(p).act_freq = freq_given
                        End If
                    ElseIf freq_given > 0 Then
                        dicary(p).num_found = dicary(p).num_found + 1
                        dicary(p).act_freq = freq_given
                    Else
                        'CalcFrequency sets act_freq
                        dicary(p).num_found = dicary(p).num_found + 1
                        If dicary(p).num_found <= MAX_UNIQUE_CHART_TIMES Then
                            dicary(p).charttime(dicary(p).num_found) = Trim$(Mid(buf, START_EVENT_DT, LEN_DT))
                        End If
                        If Not dicary(p).gavefreq Then
                            CalcFrequency p
                        End If
                        dprint dicary(p).indwinpfs, -1, "num found =" & dicary(p).num_found & ", freq=" & dicary(p).act_freq & " Item=" & p
                    End If
                Else
                    inds(dicary(p).indwinpfs).checked = True
                    dprint dicary(p).indwinpfs, 1, ""
                    If dicary(p).also_mark <> "" Then
                        ParseAlsoMark (dicary(p).also_mark)
                        dprint dicary(p).indwinpfs, 1, " Also-mark=" & dicary(p).also_mark
                    End If
                End If
                'Get the also-mark from the dictionary since this is its source.
                If dicary(p).also_mark <> "" Then
                    inds(dicary(p).indwinpfs).also_mark = dicary(p).also_mark
                End If
                
                If prevcharting = charting Then
                    p = 0
                Else
                    p = DicIndex(eventid, charting)
                End If
            Wend
            End If 'then some other event
        Else 'Header record
            If (Not formloading) Then 'end of the previous pt.  Output now.
                DoPatientSummary
                classdate = Mid$(classdt, 1, 8)
                classtime = Mid$(classdt, 9, 4)
                AssembleOutput
            End If
            dprint -1, 0, buf
            formloading = False
            InitIndicators
            ResetDictionaryFields
            ParsePatientInfo (buf)
            classdt = ""
            classdate = ""  'Mid$(buf, 21, 4) & Mid$(buf, 15, 2) & Mid$(buf, 18, 2)
            classtime = ""  'Mid$(buf, 26, 4)
            If efftimeon Then intime = saveintime
            phmmarked = False
            painassessed = False
        End If
        

        DoEvents
    Next

    DoPatientSummary
    classdate = Mid$(classdt, 1, 8)
    classtime = Mid$(classdt, 9, 4)
    AssembleOutput

    Close #datafile
    Close #outfile
    Kill infn
    
    Exit Sub
errProcess:
        
End Sub

Private Function DicIndex(id As String, ByRef s As String) As Integer
    Dim i As Integer
    Dim p As Integer
    
    ' inds(i).num_found is set only for dictionary items with asterisk (*)
    ' in column 3 of the dictionary file.

    DicIndex = 0
    For i = 1 To dicnum
        If (id = dicary(i).eventid) Then
            If (Trim$(dicary(i).chartkey) <> "") Then
                p = InStr(1, s, dicary(i).chartkey, vbTextCompare)
                If (p > 0) Then
                    DicIndex = i
                    'Remove the charting text in prep for another eval.
                    s = Replace(s, dicary(i).chartkey, "", , , vbTextCompare)
                    dprint 0, 0, "Dictionary item found=" & i & " id=" & id & " charting=" & s & " mapping to=" & dicary(i).indwinpfs
                    Exit For
                End If
            Else 'don't care about matching chartkey
                DicIndex = i
                dprint 0, 0, "Dictionary item found=" & i & " id=" & id & " charting=%don't care%" & " mapping to=" & dicary(i).indwinpfs
                Exit For
            End If
        End If
    Next i
End Function

Private Sub InitIndicators()
    Dim i As Integer
    
    For i = 1 To MAX_INDICATORS
        inds(i).checked = False
        inds(i).also_mark = ""
    Next i
    
End Sub
Private Sub InitGroups()
    Dim i As Integer
    
    For i = 1 To MAX_INDICATORS
        grps(i) = 0
    Next i
    
    For i = 1 To 4
        grps(i) = 1
    Next i
    For i = 8 To 9
        grps(i) = 2
    Next i
    For i = 11 To 13
        grps(i) = 3
    Next i
    For i = 14 To 16
        grps(i) = 4
    Next i
    For i = 17 To 19
        grps(i) = 5
    Next i
    For i = 20 To 22
        grps(i) = 6
    Next i
    For i = 24 To 26
        grps(i) = 7
    Next i
End Sub

Private Function ItemExists(col As Collection, key As String) As Boolean
    Dim dummy As Variant
    On Error Resume Next
    dummy = col.Item(key)
    ItemExists = (Err <> 5)
End Function

Private Sub AtLeastOneADL()

    If Not (inds(1).checked Or inds(2).checked Or inds(3).checked Or inds(4).checked) Then
        inds(2).checked = True
        dprint 1, 1, "via AtLeastOneADL"
    End If
End Sub

Private Sub ParseAlsoMark(s As String)
    Dim comma_pos As Integer
    Dim ind As Integer
    
    'for example:   29,31  is the also_mark string for RESTRAINTS
    
    Do
        comma_pos = InStr(s, ",")
        If (comma_pos = 0) Then
            ind = val(s)
        Else
            ind = val(Mid$(s, 1, comma_pos - 1))
            s = Mid$(s, comma_pos + 1, Len(s) - comma_pos)
        End If
        
        inds(ind).checked = True
        dprint ind, 1, "via Also-mark"
        
    Loop Until comma_pos = 0
End Sub

Private Sub ParsePatientInfo(s As String)
    Dim fullname As String
    Dim commapos As Integer
    
    acctnum = Trim$(Mid$(s, START_ACCT_NUM, LEN_ACCT_NUM))
    
    fullname = Trim$(Mid$(s, START_PTNAME, LEN_PTNAME))
    commapos = InStr(fullname, ",")
    If commapos = 0 Then
        lastname = fullname
        firstname = ""
    Else
        lastname = Mid$(fullname, 1, commapos - 1)
        firstname = Mid$(fullname, commapos + 1, Len(fullname) - commapos)
    End If
    
    unitname = Trim$(Mid$(s, START_UNIT, LEN_UNIT))
    roomname = Trim$(Mid$(s, START_RM, LEN_RM))
    bedname = Trim$(Mid$(s, START_BED, LEN_BED))
    If Not efftimeon Then intime = Trim$(Mid$(s, START_CLASS_DT, LEN_DT))
End Sub


Private Sub AssembleOutput()
    Dim outstr As String
    Dim i As Integer
    Dim din As Date
    Dim tin As Date
    Dim ok As Boolean
    
    ok = (acctnum <> "") And (unitname <> "")
'    ok = False
'    Select Case UCase$(unitname)
'        Case "A4W", "A6M", "A9M"
'            ok = True
'    End Select
    
    If Not ok Then
        Exit Sub
    End If
    
    intime = Mid$(intime, 1, 12)
    
    outstr = Space$(9) & unitname  '10
    outstr = outstr & Space$(26 - Len(outstr)) '26
    outstr = outstr & unitname '27
    outstr = outstr & Space$(60 - Len(outstr)) '60
    outstr = outstr & Mid$(intime, 1, 8) & Space$(1) '69
'    outstr = outstr & classdate & Space$(1)
    outstr = outstr & acctnum & Space$(20 - Len(acctnum) + 1) '90
    outstr = outstr & lastname & Space$(32 - Len(lastname) + 1) '123
    outstr = outstr & firstname & Space$(32 - Len(firstname) + 1) '156
    outstr = outstr & Space$(32 + 1) '189
    outstr = outstr & roomname & Space$(8 - Len(roomname) + 1) '198
    outstr = outstr & bedname & Space$(4 - Len(bedname) + 1) '203
    outstr = outstr & intime & Space$(1) 'classdatetime 216
'    outstr = outstr & classdate & classtime & Space$(1)
    outstr = outstr & Space$(78 + 1) '295
    outstr = outstr & intime & Space$(12 - Len(intime) + 1) '308
    outstr = outstr & Space$(70) '378
    For i = 1 To MAX_INDICATORS
        If (inds(i).checked) Then
            outstr = outstr & "Y"
        Else
            outstr = outstr & "N"
        End If
    Next i
    
    Print #outfile, outstr
    Print #outlogfile, outstr
End Sub


Private Sub CalcFrequency(p As Integer)
    Dim n As Integer
    Dim r As Single
    
'    r = range / RANGE_DENOMINATOR
    dicary(p).act_freq = range / dicary(p).num_found 'denom will never be zero
' Use 8 hours as basis
'    Select Case dicary(p).indwinpfs
'        Case 14 To 22 'indicators 14-22 are to be counted instead of freq.
'            If dicary(p).num_found >= 4 * r And dicary(p).num_found <= 12 * r Then
'                dicary(p).act_freq = 240
'            ElseIf dicary(p).num_found > 12 * r And dicary(p).num_found <= 24 * r Then
'                dicary(p).act_freq = 60
'            ElseIf dicary(p).num_found > 24 * r Then
'                dicary(p).act_freq = 30
'            Else
'                dicary(p).act_freq = 480
'            End If
'        Case Else   ' Other values.
'            dicary(p).act_freq = range / dicary(p).num_found 'denom will never be zero
'    End Select

End Sub

Private Function CerDateToISO(d As String) As String
    Dim century As String
    ' Yields a string of the format yyyymmddhhnn
    
    If val(Mid$(d, 7, 2)) <= 50 Then
        century = "20"
    Else
        century = "19"
    End If
    CerDateToISO = century & Mid$(d, 7, 2) & Mid$(d, 1, 2) & Mid$(d, 4, 2) _
     & Mid$(d, 10, 2) & Mid$(d, 13, 2)

End Function

Private Sub ResetDictionaryFields()
    Dim i As Integer
    
    For i = 1 To dicnum
        dicary(i).num_found = 0
        dicary(i).act_freq = 0
        dicary(i).last_datetime = ""
        dicary(i).gavefreq = False
    Next i

End Sub

Private Function FindClosestFreq(p As Integer) As Integer
    Dim i As Integer
    Dim keep As Integer
    
    keep = 0
    For i = 1 To dicnum
        If dicary(i).eventid = dicary(p).eventid And dicary(i).chartkey = dicary(p).chartkey Then
            If dicary(p).act_freq <= dicary(i).freq_basis Then
                keep = i
            End If
        End If
    Next i
    
    FindClosestFreq = keep
    
End Function


Private Sub CheckADLComplete()
    Dim charting As String
    Dim bathtotal As Integer
    Dim bed As Integer
    Dim feed As Integer
    Dim mouth As Integer
    Dim shower As Integer
    Dim gienteral As Integer
    
    suppressDbugLog = True
    
    charting = "Complete"
    bathtotal = DicIndex("NRADLBATH", charting)
    
    charting = "Yes"
    bed = DicIndex("NRADLBED", charting)
    
    charting = "Complete"
    feed = DicIndex("NRADLFEEDING", charting)
    
    charting = "Complete"
    mouth = DicIndex("NRADLMOUTHCR", charting)
    
    charting = "Complete"
    shower = DicIndex("NRADLSHOWER", charting)
    
    charting = "Yes"
    gienteral = DicIndex("NRGIENTERAL", charting)
    
    ' 3 or more of these qualifies for ADL Complete
    If Abs(dicary(bathtotal).num_found > 0) + _
       Abs(dicary(bed).num_found > 0) + _
       Abs(dicary(feed).num_found > 0) + _
       Abs(dicary(mouth).num_found > 0) + _
       Abs(dicary(shower).num_found > 0) + _
       Abs(dicary(gienteral).num_found) >= 3 Then
       inds(3).checked = True
    ElseIf dicary(gienteral).num_found > 0 Then
       inds(2).checked = True
    End If
    suppressDbugLog = False

End Sub
Private Sub CheckADLRehab()
    Dim charting As String
    Dim bath As Integer
    Dim feed As Integer
    Dim shower As Integer
    
    suppressDbugLog = True
    
    charting = "Assist"
    feed = DicIndex("NRADLFEEDING", charting)
    
    charting = "Rehab"
    bath = DicIndex("NRADLBATH", charting)
    
    charting = "Rehab"
    shower = DicIndex("NRADLSHOWER", charting)
    
    If ((dicary(bath).num_found > 0) Or (dicary(shower).num_found > 0)) And (dicary(feed).num_found > 0) Then
       inds(4).checked = True
    ElseIf (dicary(feed).num_found > 0) Then
       inds(2).checked = True
    End If
    suppressDbugLog = False

End Sub
Private Sub CheckVitals()
    Dim charting As String
    Dim vit As Integer
    
    suppressDbugLog = True
    
    charting = "Vitals"
    vit = DicIndex("VITAL01", charting)
    
    If (dicary(vit).num_found >= 16) Then
       inds(22).checked = True
       inds(18).checked = True
       inds(16).checked = True
    ElseIf (dicary(vit).num_found >= 8) Then
       inds(21).checked = True
       inds(18).checked = True
       inds(16).checked = True
    ElseIf (dicary(vit).num_found >= 4) Then
       inds(20).checked = True
       inds(16).checked = True
    ElseIf (dicary(vit).num_found >= 2) Then
       inds(20).checked = True
    End If
    
    suppressDbugLog = False

End Sub
Private Sub CheckGCS()
    Dim charting As String
    Dim gcs As Integer
    
    suppressDbugLog = True
    
    charting = ""
    gcs = DicIndex("GCSGCSTOTAL", charting)
    
    If (dicary(gcs).num_found > 0) And dicary(gcs).act_freq > 0 Then
       inds(20).checked = True
    End If
    suppressDbugLog = False

End Sub
Private Sub CheckBraden()
    Dim charting As String
    Dim brscale As Integer
    Dim brscore As Integer
    
    suppressDbugLog = True
    
    charting = "Yes"
    brscale = DicIndex("NRINBRASCAL", charting)
    
    charting = " "
    brscore = DicIndex("BRDN TOT SCR", charting)
    
    If (dicary(brscale).num_found > 0) And (dicary(brscore).num_found > 0) Then
        If dicary(brscore).act_freq <= 16 Then
            inds(23).checked = True
        End If
    End If
    suppressDbugLog = False

End Sub

Private Sub CheckWound()
    Const numwoundtypes = 5
    Dim charting As String
    Dim i As Integer
    Dim wtype(numwoundtypes) As Integer  ' 2, 2A, 2B, 2C, 2D
    Dim wulc1(numwoundtypes) As Integer
    Dim wulc2(numwoundtypes) As Integer
    Dim wulc3(numwoundtypes) As Integer
    Dim wulc4(numwoundtypes) As Integer
    Dim weam(numwoundtypes) As Integer
    Dim wsuffix(numwoundtypes) As String
    Dim wound_type_present As Boolean
    Dim ulcer1_present As Boolean
    Dim ulcer2_present As Boolean
    Dim ulcer3_present As Boolean
    Dim ulcer4_present As Boolean
    Dim heavy_present As Boolean
    
    suppressDbugLog = True
    
    wsuffix(1) = "2"
    wsuffix(2) = "2A"
    wsuffix(3) = "2B"
    wsuffix(4) = "2C"
    wsuffix(5) = "2D"
    
    For i = 1 To numwoundtypes
        charting = "OpenSurg"
        wtype(i) = DicIndex("NRINWNDTYPE" & wsuffix(i), charting)
    
        charting = "Stage 1"
        wulc1(i) = DicIndex("NRINWNDULC" & wsuffix(i), charting)
        charting = "Stage 2"
        wulc2(i) = DicIndex("NRINWNDULC" & wsuffix(i), charting)
        charting = "Stage 3"
        wulc3(i) = DicIndex("NRINWNDULC" & wsuffix(i), charting)
        charting = "Stage 4"
        wulc4(i) = DicIndex("NRINWNDULC" & wsuffix(i), charting)
        
        charting = "Heavy"
        weam(i) = DicIndex("NRINWNDEAM" & wsuffix(i), charting)
    Next i
    
    For i = 1 To numwoundtypes
        If dicary(wtype(i)).num_found > 0 Then
            wound_type_present = True
        End If
        If dicary(wulc1(i)).num_found > 0 Then
            ulcer1_present = True
        End If
        If dicary(wulc2(i)).num_found > 0 Then
            ulcer2_present = True
        End If
        If dicary(wulc3(i)).num_found > 0 Then
            ulcer3_present = True
        End If
        If dicary(wulc4(i)).num_found > 0 Then
            ulcer4_present = True
        End If
        If dicary(weam(i)).num_found > 0 Then
            heavy_present = True
        End If
    Next i
    
    If (ulcer1_present Or ulcer2_present) Then
        inds(24).checked = True
    End If
    
    If (ulcer3_present Or ulcer4_present) Then
        inds(25).checked = True
    End If
    
    If wound_type_present Then
        If heavy_present Then
            inds(25).checked = True
        Else
            inds(24).checked = True
        End If
    End If
    
    suppressDbugLog = False

End Sub


Private Sub MakeLogFiles()
    Const HKEY_LOCAL_MACHINE = &H80000002
    Dim dt As Variant
    
    If Not g_util.GetKeyValue(HKEY_LOCAL_MACHINE, "SOFTWARE\Quadramed\WinPFS", "Directory", winpfspath) Then
        winpfspath = App.Path
    End If

    If g_util.DirExists(winpfspath) Then
        winlogpath = winpfspath & "\log"
        winloadpath = winpfspath & "\load_me"
        If Not g_util.DirExists(winpfspath) Then
            MkDir$ (winlogpath)
        End If
    Else
        winlogpath = App.Path
        winloadpath = winlogpath
    End If
    
    dt = nowdt
    inlogname = winlogpath & "\TranspIn_" & Format$(dt, "mmdd") & ".log"
    outlogname = winlogpath & "\TranspOut_" & Format$(dt, "mmdd") & ".log"
    dbugname = winlogpath & "\DbugTransp_" & Format$(dt, "mmdd") & ".log"
    
    inlogfile = FreeFile
    Open inlogname For Append As #inlogfile
    Print #inlogfile, "**** WinPFS Transparent Classification Input    Time=" & nowdt & " ****"
    
    outlogfile = FreeFile
    Open outlogname For Append As #outlogfile
    Print #outlogfile, "**** WinPFS Transparent Classification Output    Time=" & nowdt & " ****"
    
    If dbugon Then
        dbugfile = FreeFile
        Open dbugname For Append As #dbugfile
        Print #dbugfile, "****TRANSPARENT TRANSLATION DEBUGGING MODE    Time=" & nowdt & " ****"
    End If
    
End Sub

Private Sub CloseLogFiles()

    Close inlogfile
    Close outlogfile
    If dbugon Then
        Close dbugfile
    End If

    'inlogfile's dt = mid$(inlogname,len(inlogname)-len("mmdd.txt")+1,len("mmddhh.txt"))
'    FileCopy outfn, winlogpath & "\TranspOut_" & Mid$(inlogname, Len(inlogname) - Len("mmddhh.txt") + 1, Len("mmddhh.txt"))

End Sub

Private Sub DeleteOldLogs()
    Dim temp As String
    Dim i As Integer
    Dim dt As Variant

    ' Delete old log files
    ' (allow the interface to be down for up to 5 days)
    dt = DateAdd("d", -TRANSP_INLOG_FILE_LIFE, nowdt)
    For i = 1 To 5
        temp = winlogpath & "\TranspIn_" & Format$(dt, "mmdd") & ".log"
        If g_util.FileExists(temp) Then
            Kill temp
        End If
        dt = DateAdd("d", -1, dt)
    Next i

    dt = DateAdd("d", -TRANSP_OUTLOG_FILE_LIFE, nowdt)
    For i = 1 To 5
        temp = winlogpath & "\TranspOut_" & Format$(dt, "mmdd") & ".log"
        If g_util.FileExists(temp) Then
            Kill temp
        End If
        dt = DateAdd("d", -1, dt)
    Next i
    
    dt = DateAdd("d", -TRANSP_DEBUG_FILE_LIFE, nowdt)
    For i = 1 To 5
        temp = winlogpath & "\DbugTransp_" & Format$(dt, "mmdd") & ".log"
        If g_util.FileExists(temp) Then
            Kill temp
        End If
        dt = DateAdd("d", -1, dt)
    Next i

End Sub


Private Sub dprint(ind As Integer, status As Integer, s As String)
    Dim sstat As String

    If Not dbugon Or suppressDbugLog Then
        Exit Sub
    End If
    
    If ind = -1 Then
        Print #dbugfile, s
    ElseIf ind = -2 Then
        Print #dbugfile, "The following event was Out of Range and was rejected."
        Print #dbugfile, s
    ElseIf ind = 0 Then
        Print #dbugfile, "    " & s
    Else
        If (status = 1) Or (status = -1) Then
            sstat = "-TURNED ON-"
        Else
            sstat = "   "
        End If
        Print #dbugfile, "    Indicator " & ind & sstat & s
    End If
            
End Sub

Private Function DetermineFreq(f As String) As Integer
    Dim v As Integer
    Dim i As Integer
    
    DetermineFreq = 0
    If f = "" Then
        DetermineFreq = 0
    Else
        If f Like "Q*H" Then
            i = InStr(f, "Q")  'Q2H  i=1
            f = Mid$(f, i + 1, Len(f) - i)  'f="2H"
            i = InStr(f, "H")   'i=2
            v = val(Mid$(f, 1, i - 1))  'f="2"
            DetermineFreq = 60 * v
        ElseIf f Like "Q*M" Then
            i = InStr(f, "Q")  'Q30M  i=1
            f = Mid$(f, i + 1, Len(f) - i)  'f="30M"
            i = InStr(f, "M")   'i=3
            v = val(Mid$(f, 1, i - 1))  'f="30"
            DetermineFreq = v
        End If
    End If
End Function
Private Sub CheckExtCoord()
    Dim charting As String
    Dim a As Integer
    Dim a1 As Integer
    Dim b As Integer
    Dim b1 As Integer
    Dim nur As Integer
    
    suppressDbugLog = True
    
    charting = "Discharge Planning"
    a = DicIndex("GED1 TOPIC", charting)
    
    charting = "Life Chg"
    a1 = DicIndex("GED1 SIGNIF", charting)
    
    charting = "Discharge Planning"
    b = DicIndex("GED2 TOPIC", charting)
    
    charting = "Life Chg"
    b1 = DicIndex("GED2 SIGNIF", charting)
    
    charting = "HOME"
    nur = DicIndex("NUR11300027", charting)

    If (dicary(nur).num_found > 0) Then
       inds(27).checked = True
       inds(30).checked = False
    End If
    
    If (dicary(a).num_found > 0) And (dicary(a1).num_found > 0) Then
       inds(30).checked = True
    End If
    
    If (dicary(b).num_found > 0) And (dicary(b1).num_found > 0) Then
       inds(30).checked = True
    End If
    
    If (dicary(a1).num_found > 0) Or (dicary(b1).num_found > 0) Then
       inds(28).checked = True
    End If
    suppressDbugLog = False

End Sub

Private Sub CheckMedComplex()
    If phmmarked And painassessed Then
        inds(15).checked = True
    End If
End Sub
Private Sub CheckFluids()
    Dim charting As String
    Dim a As Integer
    Dim a1 As Integer
    
    suppressDbugLog = True
    
    charting = ""
    a = DicIndex("NUR11502010", charting)
    
    charting = ""
    a1 = DicIndex("NRGUCATHETR2", charting)
    
    If (dicary(a).num_found > 0) And (dicary(a1).num_found > 0) Then
       inds(11).checked = True
    End If
    
    suppressDbugLog = False

End Sub

