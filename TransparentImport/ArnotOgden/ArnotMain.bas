Attribute VB_Name = "ArnotMain"
Option Explicit
'
' Arnot-Ogden transparent classification
'
' Each unit must set the "use transparent" flag.
'
' This is designed to be run as a background job, so never stop for user input -- that means no error MsgBox.
' All communication is done through log files and the event log; it is OK to print to the console.
'
' In order to be version independent, this program does not use any AcuityPlus dlls.
' Some AcuityPlus utility classes have been copied here.
'
' In order to work with the AcuityPlus Patient Selection and Transparent import:
'
'   * The program name must be TransparentMapping.exe, placed in AcuityPlus\bin
'   * The audit file is called TransparentAudit.log, placed in AcuityPlus\log
'   * The output file is Transparent.txt, placed in AcuityPlus\load_me
'   * Events are saved in the database event log
'
Public g_cnADO          As ADODB.Connection             'main DB connection
Public g_util           As New PFSUtility               'utility functions
Public g_dbutil         As New PFSDBUtility
Public g_event          As New PFSEventLog
Public g_display        As New PFSDisplayProperties
Public g_command        As String                       'command line (for db utility)
Public g_abort          As Boolean

Private Const TRANSP_FILENAME = "Transparent.txt"       'THE output file
Private Const TRANSP_AUDIT_LOG = "TransparentAudit.log" 'The debug/audit log
Private Const CHART_ITEM_LIFE = 30                      'days in the chart_item table

Private Const DEFAULT_RANGE = 1440                      '24 hrs in minutes; overridden by -range

Public Type PatientInfo
    unit_id             As Long
    encounter_id        As Long
    meth_id             As Long
    last_name           As String
    first_name          As String
    acct                As String
    age                 As Single                   'age (in years) at admission
    unit_name           As String
    room                As String
    bed                 As String
    unit_arrival        As Date
    effective           As Date                     'patient specific (may be > g_effdt)
    pull_start          As Date                     'patient specific (may be > g_pull_start)
    pull_finish         As Date                     'patient specific (may be < g_pull_finish)
    range               As Integer                  'patient specific (may be < g_range)
    TC_source_id        As Integer                  'TCP port (chart source)
End Type

Public g_debug          As Boolean                  '(brief) debug output?
Public g_verbose        As Boolean                  'verbose debug output?
Public g_no_output      As Boolean                  'audit file only?
Public g_no_delete      As Boolean                  'keep old chart items?
Public g_import_when_done As Boolean

Public g_effdt          As Date                     'effective datetime

Public outfile          As Integer                  'Transparent.txt
Public dbugfile         As Integer                  'TransparentLog.txt

Public gLogUnitID       As Long
Public gLogEncounterID  As Long
Public gLogSourceText   As String
Public gLogMapperVersion As String
Public gBriefAudit      As String                   'the complete brief audit
Public gVerboseAudit    As String                   'the complete verbose audit

Public g_pull_start     As Date
Public g_pull_finish    As Date                     'pull datetime
Public g_range          As Long

Private nowdt           As Date
Private debug_acct      As String                   'acct filter
Private debug_unit      As String                   'unit filter
Private debug_unit_id   As Long


Sub Main()
    On Error GoTo errHandler
    
    frmMain.Show vbModeless                         'progress window
    DoEvents
    
    Set g_cnADO = g_dbutil.NewRemoteConnection
    
    gLogSourceText = Command$
    LogInfo "Begin mapping and translation", EVENT_CATEGORY_STARTUP_SHUTDOWN
    gLogSourceText = ""
        
    ParseCommandLine
    SetMapperVersion
    
    OpenOutputFiles
    ProcessPatients
    CloseOutputFiles
    
    DeleteOldLogs
    DeleteOldChartItems

    LogInfo "Mapping complete", EVENT_CATEGORY_STARTUP_SHUTDOWN
    MaybeRunImport

normalExit:
    Unload frmMain                                  '(modeless)
    Exit Sub

errHandler:
    LogError Err.Description & " in " & Err.source
    LogInfo "Unexpected error - shut down", EVENT_CATEGORY_STARTUP_SHUTDOWN
    GoTo normalExit
    Resume  'debug
End Sub

Private Sub ParseCommandLine()
    On Error GoTo errHandler

    Dim argc As Integer, argv() As String, subarg() As String, tag As String, value As String
    Dim i As Integer, n As Integer
    Dim effdate As String, efftime As String
    Dim pulldate As String, pulltime As String
    Dim nowdate As String, nowtime As String
    
    '-effdate=yyyymmdd   This is the date which, in combination with the
    '                   -efftime time is used to specify the classification In time.
    '                   If not specified, -effdate is assumed to be the current date.
    'Special value:  -effdate=yesterday
    
    '-efftime=hhmm      Used to set the classification In time.
    '                   If not specified, then -efftime is assumed to be the current time.
        
    '-pulldate=yyyymmdd This is the date/time of the data pull time.
    '                   If not specified, then this is assumed to be the -effdate.
    '-pulltime=hhmm     This is the time starting from which the pull is to
    '                   look backwards from.
    '                   If not specified, then this time is assumed to be the -efftime.
    
    '-range=nnnn        This is the number of minutes backwards from the pull time
    '                   that defines valid g_range of charting events.
    '                   If not specified, this will default to 1440 (24 hours)

    '-debug             echo debug/audit info; save in audit log file
    '-brief             Minimal debug info -- sets -debug
    '-verbose           Lots of debugging info -- sets -debug
    '-n                 No output to transparent.txt - debug only
    '-audit             sets -debug and -n for brief audit only
    '-nodelete          do not delete old chart items
    '-import            run transparent import when done mapping
    '
    '-acct=12345        Process this patient only
    '-unit=5N           Process this unit only
    '-unit_id=1234      process this unit only
    
    nowdt = Now
    nowdate = Format$(nowdt, "yyyymmdd")
    nowtime = Format$(nowdt, "hhnn")
    
    argc = g_util.SplitTextOnChar(LCase$(Command$), " ", argv(), 1, 1)

    For i = 1 To argc
        n = g_util.SplitTextOnChar(argv(i), "=", subarg(), 1, 2)        '<tag>=<value>
        tag = subarg(1)
        value = subarg(2)
        
        Select Case tag
        Case "-acct"
            debug_acct = value
            
        Case "-audit"
            g_debug = True
            g_no_output = True
        
        Case "-brief"
            g_verbose = False
            g_debug = True
        
        Case "-debug"
            g_debug = True
        
        Case "-effdate"
            If (value = "yesterday") Then
                effdate = Format(DateAdd("d", -1, Date), "yyyymmdd")
            Else
                effdate = value
            End If
        
        Case "-efftime"
            efftime = Left$(value, 4)
            efftime = String$(4 - Len(efftime), "0") & efftime
        
        Case "-import"
            g_import_when_done = True
        
        Case "-n"
            g_no_output = True
        Case "-nodelete"
            g_no_delete = True

        Case "-pulldate"
            pulldate = value
        
        Case "-pulltime"
            pulltime = Left$(value, 4)
            pulltime = String$(4 - Len(pulltime), "0") & pulltime

        Case "-range"
            g_range = CInt(value)
            
        Case "-unit"
            debug_unit = value
        Case "-unit_id"
            debug_unit_id = CLng(value)
            
        Case "-verbose"
            g_verbose = True
            g_debug = True

        Case Else
            LogWarning "Unexpected argument: " & tag
        End Select
    Next i

    If Len(effdate) = 0 Then effdate = nowdate
    If Len(efftime) = 0 Then efftime = nowtime
    
    'Note: pulldate defaults to effdate, not nowdate
    If Len(pulldate) = 0 Then pulldate = effdate
    If Len(pulltime) = 0 Then pulltime = efftime

    If g_range = 0 Then g_range = DEFAULT_RANGE
    
    'Just print these in the debug window
    Debug.Print "Command line: " & Command$
    Debug.Print "effdate=" & effdate
    Debug.Print "efftime=" & efftime
    Debug.Print "pulldate=" & pulldate
    Debug.Print "pulltime=" & pulltime
    Debug.Print "range=" & g_range

    'class IN time
    g_effdt = g_util.CDateEx(effdate & efftime)
    'range for chart item queries
    g_pull_finish = g_util.CDateEx(pulldate & pulltime)
    g_pull_start = DateAdd("n", -g_range, g_pull_finish)
    
    frmMain.ShowParameters g_pull_finish, g_range, g_effdt
    DoEvents
    Exit Sub
    
errHandler:
    g_util.ThrowError "ParseCommandLine"
    Resume  'debug
End Sub

Private Sub SetMapperVersion()
    gLogMapperVersion = App.major & "." & App.minor & "." & App.revision
End Sub

Private Sub ProcessPatients()
    On Error GoTo errHandler
    
    Dim pat As PatientInfo
    Dim rs As New Recordset
    Dim sql As String, audit_header As String
    Dim count As Integer

    audit_header = _
        String$(80, "=") & vbCrLf & _
        "TRANSPARENT MAPPING AUDIT FILE                    Run Time=" & nowdt & vbCrLf & _
        "Version:   " & gLogMapperVersion & vbCrLf & _
        "Effective: " & g_effdt & vbCrLf & _
        "Pull:      " & g_pull_finish & vbCrLf & _
        "Range:     " & g_range & " (" & Round(g_range / 60, 1) & " hrs) starting " & g_pull_start & vbCrLf

    If (dbugfile <> 0) Then Print #dbugfile, audit_header

    '
    ' Make a list of all patients with chart items during the pull period.
    ' Include only units that have the "use transparent" flag set.
    ' Limit to one patient if -acct is given.  Limit to one unit with -unit.
    ' Left join encounter_location - the patient may be discharged at pull time.
    ' Look for the latest patient arrival to the unit before the pull time; ignore transfers within the unit.
    ' Look for a patient departure (if any) before the pull time; ignore transfers within the unit.
    sql = "" & _
        " SELECT LIST.UNIT_ID, LIST.ENCOUNTER_ID, LIST.TC_SOURCE_ID, PARAM.METHODOLOGY_ID," & _
        "     UNIT.NAME AS UNIT, EL.ROOM, EL.BED, EL.DATETIME_IN AS BED_ARRIVAL," & _
        "     ARRIVE.DATETIME_IN AS UNIT_ARRIVAL, DEPART.DATETIME_OUT AS UNIT_DEPARTURE," & _
        "     E.ACCT_NUMBER, P.LAST_NAME, P.FIRST_NAME," & _
        "     P.DOB, E.ADMISSION_DATETIME, E.REGISTRATION_DATETIME" & _
        " FROM (" & _
        "     SELECT DISTINCT CI.UNIT_ID, CI.ENCOUNTER_ID, CI.TC_SOURCE_ID" & _
        "     FROM CHART_ITEM AS CI" & _
        "     INNER JOIN UNIT ON (UNIT.UNIT_ID = CI.UNIT_ID)" & _
        "     WHERE UNIT.USE_TRANSPARENT_CLASSIFICATION='Y'" & _
        "     AND EVENT_DATETIME BETWEEN " & g_dbutil.SQL_DateTime(g_pull_start) & " AND " & g_dbutil.SQL_DateTime(g_pull_finish) & _
        " ) AS LIST"
    sql = sql & _
        " INNER JOIN UNIT ON (UNIT.UNIT_ID = LIST.UNIT_ID)" & _
        " INNER JOIN UNIT_PARAM AS PARAM ON (PARAM.UNIT_ID = UNIT.UNIT_ID) AND (" & g_dbutil.SQL_DateTime(g_pull_finish) & " BETWEEN PARAM.EFFECTIVE_REPORT_DATE AND EXPIRATION_REPORT_DATE)" & _
        " INNER JOIN ENCOUNTER AS E ON (E.ENCOUNTER_ID = LIST.ENCOUNTER_ID)" & _
        " INNER JOIN PERSON AS P ON (P.PERSON_ID = E.PERSON_ID)" & _
        " LEFT  JOIN ENCOUNTER_LOCATION AS EL ON (EL.ENCOUNTER_ID = E.ENCOUNTER_ID) AND (" & g_dbutil.SQL_DateTime(g_pull_finish) & " BETWEEN EL.EFFECTIVE_DATETIME_IN AND EL.EFFECTIVE_DATETIME_OUT)" & _
        " INNER JOIN (" & _
        "    SELECT WORKING_UNIT_ID AS UNIT_ID, ENCOUNTER_ID, MAX(DATETIME_IN) AS DATETIME_IN" & _
        "    FROM ENCOUNTER_LOCATION AS EL" & _
        "    WHERE WORKING_UNIT_ID IN (SELECT UNIT_ID FROM UNIT WHERE USE_TRANSPARENT_CLASSIFICATION='Y')" & _
        "    AND DATETIME_IN < " & g_dbutil.SQL_DateTime(g_pull_finish) & _
        "    AND IS_TRANSFER_IN='Y'" & _
        "    GROUP BY WORKING_UNIT_ID, ENCOUNTER_ID" & _
        " ) AS ARRIVE ON (ARRIVE.UNIT_ID = UNIT.UNIT_ID) AND (ARRIVE.ENCOUNTER_ID = E.ENCOUNTER_ID)" & _
        " LEFT JOIN (" & _
        "    SELECT WORKING_UNIT_ID AS UNIT_ID, ENCOUNTER_ID, MAX(DATETIME_OUT) AS DATETIME_OUT" & _
        "    FROM ENCOUNTER_LOCATION AS EL" & _
        "    WHERE WORKING_UNIT_ID IN (SELECT UNIT_ID FROM UNIT WHERE USE_TRANSPARENT_CLASSIFICATION='Y')" & _
        "    AND DATETIME_OUT < " & g_dbutil.SQL_DateTime(g_pull_finish) & _
        "    AND ((NEXT_WORKING_UNIT_ID<>WORKING_UNIT_ID) OR (NEXT_WORKING_UNIT_ID IS NULL))" & _
        "    GROUP BY WORKING_UNIT_ID, ENCOUNTER_ID" & _
        " ) AS DEPART ON (DEPART.UNIT_ID = UNIT.UNIT_ID) AND (DEPART.ENCOUNTER_ID = E.ENCOUNTER_ID)" & _
        " WHERE 1=1"
    
    If Len(debug_acct) Then
        sql = sql & " AND E.ACCT_NUMBER=" & g_dbutil.SQL_String(debug_acct)
    End If
    If Len(debug_unit) Then
        sql = sql & " AND UNIT.NAME=" & g_dbutil.SQL_String(debug_unit)
    End If
    If (debug_unit_id > 0) Then
        sql = sql & " AND UNIT.UNIT_ID=" & debug_unit_id
    End If
    sql = sql & " ORDER BY UNIT.NAME, P.LAST_NAME, P.FIRST_NAME, E.ACCT_NUMBER"
    
    'Debug.Print sql
    rs.Open sql, g_cnADO
    count = 0

    '
    ' Process all patients
    '
    Do While Not rs.EOF
        'Package the patient info (don't use unit/encounter objects)
        With pat
            .unit_id = rs("UNIT_ID")
            .encounter_id = rs("ENCOUNTER_ID")
            .meth_id = rs("METHODOLOGY_ID")
            .acct = rs("ACCT_NUMBER")
            .last_name = rs("LAST_NAME") & ""
            .first_name = rs("FIRST_NAME") & ""
            .unit_name = rs("UNIT") & ""
            .room = rs("ROOM") & ""
            .bed = rs("BED") & ""
            
            If Not IsNull(rs("DOB")) And Not IsNull(rs("ADMISSION_DATETIME")) Then
                .age = DateDiff("n", rs("DOB"), rs("ADMISSION_DATETIME")) / 60# / 24# / 365.24219
            ElseIf Not IsNull(rs("DOB")) And Not IsNull(rs("REGISTRATION_DATETIME")) Then
                .age = DateDiff("n", rs("DOB"), rs("REGISTRATION_DATETIME")) / 60# / 24# / 365.24219
            Else
                .age = 0
            End If
            
            .unit_arrival = rs("UNIT_ARRIVAL")
            .effective = g_util.Max(g_effdt, .unit_arrival)
            .pull_start = g_util.Max(g_pull_start, .unit_arrival)
            .pull_finish = g_util.Min(g_pull_finish, rs("UNIT_DEPARTURE"))
            If (.pull_finish < .pull_start) Then
                .pull_finish = g_pull_finish
            End If
            .range = DateDiff("n", .pull_start, .pull_finish)
            .TC_source_id = g_dbutil.DBToInteger(rs("TC_SOURCE_ID"))        'TCP port
        End With
        
        'Get ready for event log entries for this patient
        gLogUnitID = pat.unit_id
        gLogEncounterID = pat.encounter_id
        gLogSourceText = ""
        gBriefAudit = audit_header
        gVerboseAudit = audit_header
    
        'Process this patient
        count = count + 1
        frmMain.SetProgress "Processing patient " & count & " of " & rs.RecordCount
        
        dvprint ""
        dprint String$(80, "=")
        dprint "Patient " & count & " of " & rs.RecordCount & ": " & _
            pat.last_name & ", " & pat.first_name & _
            " in " & pat.unit_name & " " & pat.room & " " & pat.bed & _
            " acct=" & pat.acct & " age=" & Round(pat.age, 2)
        dprint "Here from " & pat.pull_start & " to " & pat.pull_finish & _
            "  (LOS=" & Round(pat.range / 60, 2) & ")"

        
        'Most sites will have one source of chart items
        'If there are multiple sources that need different mapping, this is the place to do it
        Select Case pat.TC_source_id
'        Case 123
'            Select Case pat.meth_id
'            Case METH_ID_INPATIENT
'                ProcessInpatient_123 pat
'            Case Else
'                LogWarning "Methodology " & pat.meth_id & " in unit " & pat.unit_name & " is not supported"
'            End Select
        
        Case Else

            Select Case pat.meth_id
            Case METH_ID_INPATIENT
                ProcessInpatient pat
            Case Else
                LogWarning "Methodology " & pat.meth_id & " in unit " & pat.unit_name & " is not supported"
            End Select

        End Select
        
        gLogUnitID = 0
        gLogEncounterID = 0

        If g_abort Then Exit Do
        
        rs.MoveNext
    Loop
    
    rs.Close
    
    If (count < 1) Then
        dprint ""
        If Len(debug_acct) Then
            LogWarning "The selected patient has no chart items in the given time range", EVENT_CATEGORY_UNEXPECTED
        Else
            LogWarning "No chart items found to process - have the unit(s) been enabled for transparent classification?", EVENT_CATEGORY_UNEXPECTED
        End If
    End If
    
    Exit Sub

errHandler:
    g_util.ThrowError "ProcessPatients"
    Resume  'debug
End Sub

'debug print - for brief log
Public Sub dprint(s As String)
    gBriefAudit = gBriefAudit & s & vbCrLf      'Always add to both audit reports
    gVerboseAudit = gVerboseAudit & s & vbCrLf
    
    If (dbugfile <> 0) Then                     'is the debug file open?
        Debug.Print s                           'add to debug window
        Print #dbugfile, s                      'add to debug log file
    End If
End Sub

'debug verbose print - for verbose log
Public Sub dvprint(s As String)
    gVerboseAudit = gVerboseAudit & s & vbCrLf  'only add to the verbose report
    
    If (dbugfile <> 0) And g_verbose Then       'is the debug file open?
        Debug.Print s                           'add to debug window
        Print #dbugfile, s                      'add to debug log file
    End If
End Sub

Private Sub OpenOutputFiles()
    On Error GoTo errHandler
    Dim winpfspath      As String               'path of winpfs
    Dim logpath         As String               'path of winpfs\log
    Dim loadpath        As String               'path of winpfs\load_me
    Dim outfilename     As String               'Transparent.txt
    Dim dbugname        As String               'TranspaentAudit.log
    Dim progress        As String
    
    If Not g_util.GetKeyValue(HKEY_LOCAL_MACHINE, "SOFTWARE\Quadramed\WinPFS", "Directory", winpfspath) Then
        'winpfspath = App.Path & "\.."
        winpfspath = "C:\qmdev\AcuityPlus\main"
    End If

    logpath = winpfspath & "\log"
    loadpath = winpfspath & "\load_me"
    
    If Not g_util.DirExists(logpath) Then
        progress = "mkdir " & logpath
        MkDir$ logpath
    End If
    If Not g_util.DirExists(loadpath) Then
        progress = "mkdir " & loadpath
        MkDir$ loadpath
    End If

    dbugname = logpath & "\" & TRANSP_AUDIT_LOG                 'TransparentAudit.log

    ' Create a log file when in debug mode
    If g_debug Then
        dbugfile = FreeFile
        progress = "open " & dbugfile
        Open dbugname For Output As #dbugfile                   'overwrite existing
    End If
    
    If g_no_output Then Exit Sub

    outfilename = loadpath & "\" & TRANSP_FILENAME              'load_me\transparent.txt
    outfile = FreeFile
    progress = "open " & outfilename
    Open outfilename For Append As #outfile                     'add to existing
    Exit Sub

errHandler:
    g_util.ThrowError "OpenOutputFiles", progress
    Resume  'debug
End Sub

Private Sub CloseOutputFiles()
    If (outfile <> 0) Then Close #outfile
    
    If (dbugfile <> 0) Then
        Close #dbugfile
        dbugfile = 0                                            'stop writing to debug file
    End If
End Sub

Private Sub DeleteOldLogs()
    If (g_no_output Or g_no_delete) Then Exit Sub
    
    'There are no more log files; history has been added to the event log
End Sub

Private Sub DeleteOldChartItems()
    On Error GoTo errHandler
    
    Dim sql As String
    Dim dt As Date

    If (g_no_output Or g_no_delete) Then Exit Sub
    
    dt = DateAdd("d", -CHART_ITEM_LIFE, g_effdt)
    sql = "DELETE FROM CHART_ITEM WHERE EVENT_DATETIME < " & g_dbutil.SQL_DateTime(dt)
    'Debug.Print sql
    g_cnADO.Execute sql
    Exit Sub
    
errHandler:
    g_util.ThrowError "DeleteOldChartItems"
    Resume  'debug
End Sub

Private Sub MaybeRunImport()
    On Error GoTo errHandler
    Dim rc As Long, path As String
    If Not g_import_when_done Then Exit Sub
    
    If Right$(App.path, 3) = "bin" Then
        path = App.path                         'runtime
    Else
        path = "C:\qmdev\AcuityPlus\main\bin"   'dev
    End If
    g_util.ExecuteAndWait path & "\" & TRANSPARENT_IMPORT_EXE_NAME, rc
    Exit Sub
    
errHandler:
    g_util.ThrowError "MaybeRunImport"
End Sub



Public Sub AddLogEntry(event_type As EventLogType, msg As String, _
    Optional event_category As EventLogCategory = EVENT_CATEGORY_NONE _
)
    On Error GoTo errHandler

    dprint msg                      'copy to debug log

    g_event.AddEventLogEntry EVENT_SOURCE_TRANSPARENT_MAPPING, event_type, event_category, _
        msg, gLogSourceText, 0, gLogUnitID, gLogEncounterID
    Exit Sub
    
errHandler:
    'no message boxes allowed (this is a command-line program)
    Debug.Print Err.Description
End Sub

Public Sub LogInfo(s As String, Optional event_category As EventLogCategory = EVENT_CATEGORY_NONE)
    AddLogEntry EVENT_TYPE_INFO, s, event_category
End Sub

Public Sub LogWarning(s As String, Optional event_category As EventLogCategory = EVENT_CATEGORY_VALIDATION)
    AddLogEntry EVENT_TYPE_WARNING, s, event_category
End Sub

Public Sub LogError(s As String, Optional event_category As EventLogCategory = EVENT_CATEGORY_VALIDATION)
    AddLogEntry EVENT_TYPE_ERROR, s, event_category
End Sub

