VERSION 5.00
Begin VB.Form frmMain 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Transparent Mapping"
   ClientHeight    =   1305
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   7920
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1305
   ScaleWidth      =   7920
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.Label lblParameters 
      Alignment       =   2  'Center
      Height          =   615
      Left            =   120
      TabIndex        =   1
      Top             =   60
      Width           =   7575
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblProgress 
      Alignment       =   2  'Center
      Caption         =   "progress"
      Height          =   435
      Left            =   120
      TabIndex        =   0
      Top             =   840
      Width           =   7575
      WordWrap        =   -1  'True
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'
' This little window shows the mapping progress if the program is run interactively
'
Private Sub Form_Load()
    g_display.SetFontInAllControls Me, g_display.MessageFont
    Me.Icon = g_util.QuadraMedIcon()
    Me.Caption = g_util.AppCaption()
    lblProgress.Caption = "loading..."
End Sub

Public Sub ShowParameters(pull_dt As Date, range As Long, eff_dt As Date)
    lblParameters.Caption = "Pull at " & pull_dt & ", range=" & range & " (" & Round(range / 60, 1) & " hrs)," & _
        " effective " & eff_dt
End Sub

Public Sub SetProgress(msg As String)
    lblProgress.Caption = msg
    DoEvents
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If (UnloadMode = vbFormControlMenu) Then
        If MsgBox("Do you want to cancel the mapping in progress?", vbQuestion + vbYesNo) = vbYes Then
            g_abort = True
        End If
        Cancel = 1
    End If
End Sub
