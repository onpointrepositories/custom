Attribute VB_Name = "ArnotInpatient"
Option Explicit
'
' Arnot-Ogden Inpatient
'
' This processes one patient using the Inpatient methodology.
'
' Most Arnot cateogories start with one of these prefixes:
' * PHx = Patient History - saved across visits
' * VHx = Visit History - valid for entire visit
' * NHx = Nothing history - not saved (shift only)
'
' All search functions use exact match for category, description and field name.
' result_like looks for one word somewhere in the result.
' result_list looks for any one of a list of words somewhere in the result.
'
Private Const MAX_INDS = 50
Private Const MAX_PROCS = 20

Private Type indicator_data
    checked     As Boolean
    group       As Integer
    also_mark   As String                   'not used at Arnot (handy with user-defined indicators?)
End Type

Private Type procdata                       'not used at Arnot
    pnum    As Integer
    start   As Date
    finish  As Date
End Type

Private inds(MAX_INDS)          As indicator_data

Private numprocs                As Integer
Private procs(MAX_PROCS)        As procdata

Private WHERE_ENCOUNTER         As String
Private AND_UNIT                As String
Private AND_PULL_RANGE          As String
Private AND_ARRIVAL             As String
Private BUCKET_CALC             As String

Private m_pat                   As PatientInfo
Private m_assist_count          As Integer

Private Enum SearchMode
    SearchDefault
    SearchPullRange             'search within the current pull     -- default
    SearchSinceArrival          'search since arrival to the unit
    SearchSinceAdmission        'search everything since admission to the hospital
End Enum

Private Enum MatchMode
    MatchExact                  'field='abc'
    MatchContains               'field like '%abc%'
End Enum

Private Enum CompareMode
    BinaryCompare               'case dependent
    TextCompare                 'case independent
End Enum

Private Enum CountMode
    CountAll
    CountFirst                  'stop after one is found
End Enum

Private Enum GetValueMode
    GetTotal
    GetMax
    GetLast
End Enum

Private Const FREQUENCY_BUCKET_SIZE = 20    'min

Private Enum Frequencies
    QNONE
    Q4H
    Q2H
    Q1H
    Q30M
End Enum

Private Type fmapRow
    los_high                As Single       'the LOS being testing
    freq(QNONE To Q30M)     As Integer      'the count required for each Q value
End Type

Private m_freq_map(6)       As fmapRow      '1,2,4,8,12,24 hours

Private m_result_match      As MatchMode    'equal/like
Private m_result_compare    As CompareMode  'case/no case

'
'This is the main entry point
'
Public Sub ProcessInpatient(pat As PatientInfo)
    On Error GoTo errHandler
    
    m_pat = pat
    
    m_result_match = MatchContains          'result like
    m_result_compare = TextCompare          'no case

    InitGroupsIfNeeded
    SetSQLConstants
    LoadFreqTable
    ResetIndicators
    ResetProcs

    Check_1_2_3
    Check_4
    Check_5_6
    Check_7
    Check_8
    Check_9_10
    Check_11_12
    Check_13
    Check_14_15_16_17
    Check_18
    Check_19_20
    Check_21
    Check_22
    AtLeastOneADL
    HighestIndicatorInEachGroupWins

    CheckProcs

    If g_no_output Then Exit Sub
    OutputClass
    OutputProcs
    Exit Sub
    
errHandler:
    LogError Err.Description & " in " & Err.source, EVENT_CATEGORY_UNEXPECTED
    Exit Sub
    Resume  'debug
End Sub

Private Sub ResetIndicators()
    Dim i As Integer
    
    For i = 1 To MAX_INDS
        inds(i).checked = False
        inds(i).also_mark = ""
    Next i
    
End Sub

Private Sub ResetProcs()
    numprocs = 0
End Sub

Private Sub InitGroupsIfNeeded()
    On Error GoTo errHandler
    
    Static been_here As Boolean
    Dim rs As New Recordset
    Dim sql As String
    Dim inum As Integer
    
    If been_here Then Exit Sub
    
    'get indicator radio groups from the database
    sql = "SELECT INDICATOR_NUMBER, RADIO_GROUP FROM INDICATOR_DEFINITION WHERE METHODOLOGY_ID=" & m_pat.meth_id
    rs.Open sql, g_cnADO
    
    Do While Not rs.EOF
        inum = rs("INDICATOR_NUMBER")
        If (inum < MAX_INDS) Then
            inds(inum).group = g_dbutil.DBToInteger(rs("RADIO_GROUP"))  'could be null
        End If
        rs.MoveNext
    Loop
    
    rs.Close
    Exit Sub
    
errHandler:
    g_util.ThrowError "InitGroupsIfNeeded"
End Sub

Private Sub LoadFreqTableRow(m_freq As fmapRow, los_high As Single, values As String)
    Dim arr() As String, n As Integer, i As Integer
    
    m_freq.los_high = los_high

    n = g_util.SplitTextOnChar(values, ",", arr(), 0, 0)
    For i = 0 To n - 1
        m_freq.freq(i) = CInt(arr(i))
    Next i
End Sub

Private Sub LoadFreqTable()
                                 ' LOS,   None Q4h Q2h Q1h Q30m
    LoadFreqTableRow m_freq_map(1), 1, "    0,  0,  0,  0,  1"
    LoadFreqTableRow m_freq_map(2), 2, "    0,  0,  0,  1,  2"
    LoadFreqTableRow m_freq_map(3), 4, "    0,  1,  2,  3,  4"
    LoadFreqTableRow m_freq_map(4), 8, "    0,  2,  3,  4,  8"
    LoadFreqTableRow m_freq_map(5), 12, "   0,  2,  4,  6, 12"
    LoadFreqTableRow m_freq_map(6), 9999, " 0,  3,  6, 12, 24"         '24+
    
    'To read this table: if you have at least 6 asessmenets in a 12-hour period, you get Q1h
    '(row 5: LOS=12, column Q1h has a count of 6)
End Sub

Private Function FreqForCount(los_hours As Single, count As Integer) As Frequencies
    Dim i As Integer, j As Integer
    
    For i = 1 To UBound(m_freq_map)                         'go from LOS 1 to 24+
        If (los_hours <= m_freq_map(i).los_high) Then
            For j = Q30M To QNONE Step -1                   'search right to left
                If (count >= m_freq_map(i).freq(j)) Then
                    FreqForCount = j
                    Exit Function
                End If
            Next j
        End If
    Next i
    
    FreqForCount = QNONE
End Function

Private Sub SetSQLConstants()
    'These SQL constants are combined in different ways for searching
    WHERE_ENCOUNTER = " where (encounter_id = " & m_pat.encounter_id & ")"
    AND_UNIT = " and (unit_id = " & m_pat.unit_id & ")"
    AND_PULL_RANGE = " and (event_datetime between " & g_dbutil.SQL_DateTime(m_pat.pull_start) & " and " & g_dbutil.SQL_DateTime(m_pat.pull_finish) & ")"
    AND_ARRIVAL = " and (event_datetime between " & g_dbutil.SQL_DateTime(m_pat.unit_arrival) & " and " & g_dbutil.SQL_DateTime(m_pat.pull_finish) & ")"
    
    'What bucket number does the event belong to?
    BUCKET_CALC = "DateDiff(minute," & g_dbutil.SQL_DateTime(m_pat.pull_start) & ",event_datetime) / " & FREQUENCY_BUCKET_SIZE
End Sub

'Add this to all chart item queries to start the WHERE clause
'This is where the search depth is set
Private Function WhereBase(Optional search_mode As SearchMode = SearchDefault)
    Dim result As String
    
    Select Case search_mode
    Case SearchPullRange, SearchDefault
        result = WHERE_ENCOUNTER & AND_UNIT & AND_PULL_RANGE    'search within pull range
    Case SearchSinceArrival
        result = WHERE_ENCOUNTER & AND_UNIT & AND_ARRIVAL       'search since arrival to the unit
    Case SearchSinceAdmission
        result = WHERE_ENCOUNTER                                'search since admission to the hospital (any unit)
    End Select
    
    'Since we are looking for one patient in one facility, we don't have to filter by TC_source_ID.
    
    WhereBase = result
End Function

Private Function ValueIsAList(result As String) As Boolean
    ValueIsAList = (InStr(1, result, ",") > 0)                'is this a comma-separated list?
End Function

'Generate the correct comparison based on the module flags
Private Function SQL_Compare(colname As String, value As String) As String
    Dim result As String, collate As String
    
    If (m_result_compare = BinaryCompare) Then
        collate = " COLLATE SQL_Latin1_General_CP1_CS_AS "
    End If
    
    If (m_result_match = MatchExact) Then
        result = colname & collate & "=" & g_dbutil.SQL_String(value)
    Else
        result = colname & collate & " like " & g_dbutil.SQL_String("%" & value & "%")
    End If

    SQL_Compare = result
End Function

'Do the correct comparison based on the module flags
Private Function VB_Equal(s As Variant, t As Variant) As Boolean
    Dim result As Boolean, cmp_mode As Integer
    
    If (m_result_compare = BinaryCompare) Then
        cmp_mode = vbBinaryCompare
    Else
        cmp_mode = vbTextCompare
    End If
    
    If (m_result_match = MatchExact) Then
        result = (StrComp(s, t, cmp_mode) = 0)
    Else
        result = (InStr(1, s, t, cmp_mode) > 0)
    End If

    VB_Equal = result
End Function


'Look for any of these fields.  Cat/desc/field = exact match.  Result = exact/like
Private Function AndSimpleItemFilter(cat As String, code As String, desc As String, field As String, result_like As String) As String
    Dim result As String
    
    If Len(cat) Then result = result & " and category=" & g_dbutil.SQL_String(cat)
    If Len(code) Then result = result & " and code=" & g_dbutil.SQL_String(code)
    If Len(desc) Then result = result & " and description=" & g_dbutil.SQL_String(desc)
    If Len(field) Then result = result & " and field_name=" & g_dbutil.SQL_String(field)
    If Len(result_like) Then result = result & " and " & SQL_Compare("result", result_like)

    AndSimpleItemFilter = result
End Function

'Look for a result that contains one of the words in the list
'The only drawback with this function is that we don't know what it found (for audits)
'Because of this, it is only used in as few places.
Private Function AndResultContains(result_list As String) As String
    Dim arr() As String
    Dim i As Integer, n As Integer
    Dim result As String

    If Len(result_list) = 0 Then Exit Function
    
    n = g_util.SplitTextOnChar(result_list, ",", arr(), 1, 0)
    
    result = "and ((1=0)"   '(1=0) in case the list is empty
    
    For i = 1 To n
        arr(i) = Trim$(arr(i))          'we split on comma so there usually are leading blanks
        result = result & " or (" & SQL_Compare("result", arr(i)) & ")"
    Next i
    
    result = result & ")"
    
    AndResultContains = result
End Function

Private Function DescribeSearchDepth(search_mode As SearchMode) As String
    Dim result As String
    
    Select Case search_mode
    Case SearchPullRange, SearchDefault
        'result = "in pull range"
        result = ""                         'be quiet since this is default
    Case SearchSinceArrival
        result = "since arrival to unit"
    Case SearchSinceAdmission
        result = "since admission"
    End Select
    
    DescribeSearchDepth = result
End Function

Private Function Describe(cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = SearchPullRange) As String
    Dim sql As String, and_filter As String
    Dim rs As New Recordset
    Dim result As String
   
    result = "looking for"
    If Len(cat) Then result = result & " cat='" & cat & "'"
    If Len(code) Then result = result & " code='" & code & "'"
    If Len(desc) Then result = result & " desc='" & desc & "'"
    If Len(field) Then result = result & " field='" & field & "'"
    If Len(result_list) Then
        result = result & " result" & IIf(m_result_match = MatchContains, " contains ", "=") & "'" & result_list & "'"
    End If

    If ValueIsAList(result_list) Then
        and_filter = AndSimpleItemFilter(cat, code, desc, field, "") & AndResultContains(result_list)
    Else
        and_filter = AndSimpleItemFilter(cat, code, desc, field, result_list)
    End If
    
    sql = "select category, code, description, field_name, result from chart_item" & WhereBase(search_mode) & and_filter
    rs.Open sql, g_cnADO
    If rs.EOF Then
        result = result & "; not found " & DescribeSearchDepth(search_mode)
    Else
        result = result & "; found"
        'Add info for columns that were not specified and a value was found
        If (Len(cat) = 0) And Len(rs("category")) Then result = result & " cat='" & rs("category") & "'"
        If (Len(code) = 0) And Len(rs("code")) Then result = result & " code='" & rs("code") & "'"
        If (Len(desc) = 0) And Len(rs("description")) Then result = result & " desc='" & rs("description") & "'"
        If (Len(field) = 0) And Len(rs("field_name")) Then result = result & " field='" & rs("field_name") & "'"
        'Add the complete result found (we searched for a word or words)
        result = result & " result='" & rs("result") & "'"
        'Are there more results?  Just say how many; we aren't going to list them.
        If (rs.RecordCount = 2) Then
            result = result & " (1 more result)"
        ElseIf (rs.RecordCount > 2) Then
            result = result & " (" & rs.RecordCount - 1 & " more results)"
        End If
    End If
    rs.Close
    
    Describe = result
End Function

'Set an indicator for this reason (low level)
Private Sub SetInd(inum As Integer, reason As String)
    If inds(inum).checked Then
        dvprint "Set Ind #" & inum & ": " & reason          'already set - repeat for verbose only
    Else
        inds(inum).checked = True
        dprint "Set Ind #" & inum & ": " & reason
    End If
End Sub

'Clear an indicator for this reason (low level)
Private Sub ClrInd(inum As Integer, reason As String)
    If inds(inum).checked Then
        dvprint "Clr Ind #" & inum & ": " & reason          'already clear - repeat verbose only
    Else
        inds(inum).checked = False
        dprint "Clr Ind #" & inum & ": " & reason
    End If
End Sub

'Count how many items have the search word in result_like
'This is simple and fast
Private Function CountSimpleResult(cat As String, code As String, desc As String, field As String, result_like As String, Optional search_mode As SearchMode = SearchPullRange, Optional trace As Boolean = True, Optional found_what As String) As Long
    Dim sql As String, and_filter As String
    Dim rs As New Recordset
    Dim count As Long

    and_filter = AndSimpleItemFilter(cat, code, desc, field, result_like)
    sql = "select count(*) from chart_item" & WhereBase(search_mode) & and_filter
    'Debug.Print sql
    rs.Open sql, g_cnADO
    count = rs(0)
    rs.Close
    
    'Normally CountSimpleResult will report what it finds; not eveyone wants this so the trace can be turned off
    found_what = Describe(cat, code, desc, field, result_like, search_mode) '       & IIf(count > 0, "; count=" & count, "")
    If trace Then dvprint found_what
    
    CountSimpleResult = count
End Function

'Count how many items have a result with one of the words given in the result_list
'Returns a description of what word was found
'This is more powerful than CountSimpleResult but it is slower
Private Function CountResultInList(cat As String, code As String, desc As String, field As String, result_list As String, search_mode As SearchMode, count_mode As CountMode, trace As Boolean, found_what As String) As Integer
    On Error GoTo errHandler
    Dim sql As String, and_filter As String, arr() As String
    Dim rs As New Recordset
    Dim i As Integer, n As Integer, count As Integer

    n = g_util.SplitTextOnChar(result_list, ",", arr(), 1, 0)
    
    'Do not filter by result here; we need all results
    and_filter = AndSimpleItemFilter(cat, code, desc, field, "")
    sql = "select category, code, field_name, result from chart_item" & WhereBase(search_mode) & and_filter
    'Debug.Print sql
    rs.Open sql, g_cnADO
    
    Do While Not rs.EOF
        'Look for each search word in the result
        For i = 1 To n
            arr(i) = Trim$(arr(i))          'we split on comma so there usually are leading blanks
            If VB_Equal(rs("result"), arr(i)) Then
                found_what = "found '" & arr(i) & "' in cat='" & rs("category") & "' code='" & rs("code") & "' field='" & rs("field_name") & "' result='" & rs("result") & "'"
                If trace Then dvprint found_what
                count = count + 1
                If count_mode <> CountAll Then Exit Do  'stop counting
                Exit For                                'try next record
            End If
        Next i

        rs.MoveNext
    Loop
    
    If (count > 0) Then
        'We already printed what was found; maybe add how many?
        If trace And (count_mode = CountAll) Then dvprint "found " & count & " total"
    Else
        found_what = Describe(cat, code, desc, field, result_list, search_mode)       'not found
        If trace Then dvprint found_what
    End If
    
    rs.Close
    
    CountResultInList = count
    Exit Function
    
errHandler:
    g_util.ThrowError "CountResultInList"
    Resume   'debug
End Function

Private Function CountResultContains(cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = SearchPullRange, Optional trace As Boolean = True, Optional found_what As String) As Integer
    If ValueIsAList(result_list) Then
        CountResultContains = CountResultInList(cat, code, desc, field, result_list, search_mode, CountAll, trace, found_what)
    Else
        CountResultContains = CountSimpleResult(cat, code, desc, field, result_list, search_mode, trace, found_what)
    End If
End Function

Private Function ResultContains(cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = SearchPullRange, Optional trace As Boolean = True, Optional found_what As String) As Boolean
    'tracing can be turned off and you can ask for what was found instead of having it printed
    If ValueIsAList(result_list) Then
        ResultContains = (CountResultInList(cat, code, desc, field, result_list, search_mode, CountFirst, trace, found_what) > 0)
    Else
        ResultContains = (CountSimpleResult(cat, code, desc, field, result_list, search_mode, trace, found_what) > 0)
    End If
End Function


Private Function CountResultNotInList(cat As String, code As String, desc As String, field As String, result_list As String, search_mode As SearchMode, count_mode As CountMode, trace As Boolean, found_what As String) As Integer
    On Error GoTo errHandler
    Dim sql As String, and_filter As String, arr() As String
    Dim rs As New Recordset
    Dim i As Integer, n As Integer, count As Integer
    Dim found_one As Boolean

    n = g_util.SplitTextOnChar(result_list, ",", arr(), 1, 0)
    
    'Do not filter by result here; we need all results
    and_filter = AndSimpleItemFilter(cat, code, desc, field, "")
    sql = "select category, field_name, result from chart_item" & WhereBase(search_mode) & and_filter
    'Debug.Print sql
    rs.Open sql, g_cnADO
    
    Do While Not rs.EOF
        'Look for each search word in the result (case insensitive)
        found_one = False
        
        For i = 1 To n
            arr(i) = Trim$(arr(i))          'we split on comma so there usually are leading blanks
            If VB_Equal(rs("result"), arr(i)) Then
                found_what = "found '" & arr(i) & "' in cat='" & rs("category") & "' field='" & rs("field_name") & "' result='" & rs("result") & "' -- ignore this"
                If trace Then dvprint found_what
                found_one = True
                Exit For
            End If
        Next i

        If (Not found_one) Then
            'none of the words were found - good!
            found_what = Describe(cat, code, desc, field, "", search_mode) & " -- does not contain '" & result_list & "'"
            If trace Then dvprint found_what
            count = count + 1
            If count_mode <> CountAll Then Exit Do
        End If

        rs.MoveNext
    Loop
    
    If (count > 0) Then
        'We already printed what was found; maybe show the total?
        If trace And (count_mode = CountAll) Then dvprint "found " & count & " total"
    ElseIf (rs.RecordCount > 0) Then
        'We already printed what we ignored
    Else
        'Print what did not exist
        found_what = Describe(cat, code, desc, field, "", search_mode)      'not found
        If trace Then dvprint found_what
    End If
    
    rs.Close
    
    CountResultNotInList = count
    Exit Function
    
errHandler:
    g_util.ThrowError "CountResultNotInList"
    Resume   'debug
End Function

Private Function CountResultDoesNotContain(cat As String, code As String, desc As String, field As String, result_list As String, found_what As String, Optional search_mode As SearchMode = SearchPullRange, Optional trace As Boolean = True) As Integer
    CountResultDoesNotContain = CountResultNotInList(cat, code, desc, field, result_list, search_mode, CountAll, trace, found_what)
End Function

Private Function ResultDoesNotContain(cat As String, code As String, desc As String, field As String, result_list As String, found_what As String, Optional search_mode As SearchMode = SearchPullRange, Optional trace As Boolean = True) As Boolean
    ResultDoesNotContain = (CountResultNotInList(cat, code, desc, field, result_list, search_mode, False, trace, found_what) > 0)
End Function

'Set the indicator if the result contains one of the words in the result_list
Private Sub SetIndIfResultContains(inum As Integer, cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = SearchPullRange)
    Dim found_what As String
    'avoid more queries if the indicator is already set
    If inds(inum).checked Then Exit Sub
    
    'Turn trace off for ResultContains() so the result can be placed on the "SetInd#" line below
    If ResultContains(cat, code, desc, field, result_list, search_mode, False, found_what) Then
        SetInd inum, found_what             'echo here
    Else
        dvprint found_what                  'and here
    End If
End Sub

'Set the indicator if the result does not contain any of the words in result_list
Private Sub SetIndIfResultDoesNotContain(inum As Integer, cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = SearchPullRange)
    Dim found_what As String
    'avoid more queries if the indicator is already set
    If inds(inum).checked Then Exit Sub
    
    'Turn trace off for ResultDoesNotContain() and echo what was set below with SetInd
    If ResultDoesNotContain(cat, code, desc, field, result_list, found_what, search_mode, False) Then
        SetInd inum, found_what             'echo here
    Else
        dvprint found_what                  'and here
    End If
End Sub

'Clear the indicator if the result contains on of the words in the result_list
Private Sub ClrIndIfResultContains(inum As Integer, cat As String, code As String, desc As String, field As String, result_list As String, Optional search_mode As SearchMode = SearchPullRange)
    Dim found_what As String
    'avoid more queries if the indicator is already clear
    If Not inds(inum).checked Then Exit Sub
    
    'Turn trace off for ResultContains() and echo what was set below with SetInd
    If ResultContains(cat, code, desc, field, result_list, search_mode, False, found_what) Then
        ClrInd inum, found_what             'echo here
    Else
        dvprint found_what                  'and here
    End If
End Sub


'These slightly smaller functions are meant for places where you aren't really looking for a chart result
Private Function Exists(cat As String, Optional code As String, Optional desc As String, Optional field As String, Optional result_list As String, Optional search_mode As SearchMode = SearchPullRange, Optional trace As Boolean = True, Optional found_what As String) As Boolean
    Exists = (CountResultContains(cat, code, desc, field, result_list, search_mode, trace, found_what) > 0)
End Function

Private Sub SetIndIfFound(inum As Integer, cat As String, Optional code As String, Optional desc As String, Optional field As String, Optional result_list As String, Optional search_mode As SearchMode = SearchPullRange)
    SetIndIfResultContains inum, cat, code, desc, field, result_list, search_mode
End Sub

Private Sub ClrIndIfFound(inum As Integer, cat As String, Optional code As String, Optional desc As String, Optional field As String, Optional result_list As String, Optional search_mode As SearchMode = SearchPullRange)
    ClrIndIfResultContains inum, cat, code, desc, field, result_list, search_mode
End Sub


'Get the max/total value from a result (usually in the middle of the text)
Private Function GetIntValue(get_mode As GetValueMode, cat As String, code As String, desc As String, field As String, result_like As String, Optional search_mode As SearchMode = SearchPullRange) As Integer
    Dim sql As String, and_filter As String, arr() As String, msg As String
    Dim rs As New Recordset
    Dim result As Integer, i As Integer, n As Integer, value As Integer
    Dim found_one As Boolean

    and_filter = AndSimpleItemFilter(cat, code, desc, field, result_like)
    sql = "select result from chart_item" & WhereBase(search_mode) & and_filter
    'Debug.Print sql
    rs.Open sql, g_cnADO
    
    'Look for a number in the result
    
    Do While Not rs.EOF
        n = g_util.SplitTextOnChar(rs(0), " ", arr(), 1, 0)
        For i = 1 To n
            'Look for 1st character numeric; IsNumeric("60min") will fail so look at 1st char only
            If IsNumeric(Left$(arr(i), 1)) Then
                value = val(arr(i))                         'Use Val; CInt will error on "60min"
                Select Case get_mode
                Case GetMax
                    result = g_util.Max(value, result)      'max
                Case GetTotal
                    result = result + value                 'total
                Case GetLast
                    result = value                          'last
                End Select
                
                'print what we are searching for (the first time)
                If Not found_one Then
                    dvprint Describe(cat, code, desc, field, result_like, search_mode)
                End If
                found_one = True
                'print each value found
                dvprint "  found numeric value " & result
                'Keep going in case there are more
            End If
        Next i
        rs.MoveNext
    Loop
    
    rs.Close
    
    If Not found_one Then
        'show what was not found
        If g_verbose Then dvprint Describe(cat, code, desc, field, result_like, search_mode)
    End If
    
    GetIntValue = result
End Function

Private Function GetMaxValue(cat As String, code As String, desc As String, field As String, result_like As String, Optional search_mode As SearchMode = SearchPullRange) As Integer
    GetMaxValue = GetIntValue(GetMax, cat, code, desc, field, result_like, search_mode)
End Function

Private Function GetTotalValue(cat As String, code As String, desc As String, field As String, result_like As String, Optional search_mode As SearchMode = SearchPullRange) As Integer
    GetTotalValue = GetIntValue(GetTotal, cat, code, desc, field, result_like, search_mode)
End Function

'Get a result; returns True if found with return_result set
Private Function GetResult(cat As String, code As String, desc As String, field As String, return_result As String, Optional search_mode As SearchMode = SearchPullRange) As Boolean
    Dim rs As New Recordset
    Dim sql As String

    sql = "select result from chart_item" & WhereBase(search_mode) & AndSimpleItemFilter(cat, code, desc, field, "")
    rs.Open sql, g_cnADO
    If Not rs.EOF Then
        return_result = rs(0) & ""
    Else
        return_result = ""
    End If
    rs.Close

    dvprint Describe(cat, code, desc, field, "", search_mode)
    GetResult = (Len(return_result) > 0)
End Function


Private Sub Check_1_2_3()
    On Error GoTo errHandler
    
    Dim bath_complete As Boolean, meal_complete As Boolean, meal_NPO As Boolean, activity_assist As Boolean
    Dim n As Integer
    Dim s As String, tmp As String

    dvprint "---------------"
    dvprint "1. ADL Self"
    dvprint "2. ADL Assist"
    dvprint "3. ADL Complete"
    dvprint "---------------"
    
    'Note: call these separately so the second one is not evaluated if the first one is true, etc.
    If ResultContains("Hygiene", "", "", "Performance", "complete") Then                'most here
        bath_complete = True
    ElseIf ResultContains("NHx ADL/Sleep/Rest", "", "", "Bathing", "complete") Then     'some here
        bath_complete = True
    End If
    
    If Exists("NHx Intake IV/Bld/Fdg", "", "Tube Fdg Amt/Type*", "", "") Then           'about 60%
        meal_complete = True
    ElseIf ResultContains("Diet", "", "", "Mealtime Assist", "manually fed") Then       'about 40%
        meal_complete = True
    End If
    
    meal_NPO = ResultContains("Diet", "", "", "Diet Type", "NPO")

    'Assist counts are not always given for these; must look for keywords
    tmp = "dog, hoyer, infant, needs assist, stretcher, toddler"
'    If ResultContains("Hygiene", "", "", "Level of Care", "assist") Then
'        activity_assist = True
    If ResultContains("", "", "", "Activity Needs", tmp) Then
        activity_assist = True
    ElseIf ResultContains("", "", "", "Activity Tolerance", tmp) Then
        activity_assist = True
    ElseIf ResultContains("VHx Activity Protocol", "", "", "Activity Assistance Needs", tmp, SearchSinceAdmission) Then
        activity_assist = True
    End If
        
    'These can tell us how many people are needed; stop if we reach 4.
    n = 0
    If (n < 4) Then                                 'Almost all here in activity protocol
        n = g_util.Max(GetMaxValue("VHx Activity Protocol", "", "", "Activity Assistance Needs", "people", SearchSinceAdmission), n)
    End If
    If (n < 4) Then
        n = g_util.Max(GetMaxValue("", "", "", "Activity Needs", "people"), n)
    End If
    If (n < 4) Then
        n = g_util.Max(GetMaxValue("", "", "", "Activity Tolerance", "assist"), n)
    End If
    If (n < 4) Then                                     '2011-04-07
        n = g_util.Max(GetMaxValue("", "", "", "Ability/Tol", "assist"), n)
    End If
    If (n < 4) Then
        n = g_util.Max(GetMaxValue("VHx Activity Protocol", "", "", "Activity Assistance Needs", "total assist", SearchSinceAdmission), n)
    End If
    m_assist_count = n                          'save for IND #5-6
    
    '
    ' 3. ADL complete
    '
    If (m_pat.age = 0) Then
        'unknown -- ignore it
    ElseIf (m_pat.age < 4) Then
        SetInd 3, "age < 4yr"                           '70% of ind #3
    End If

    If (bath_complete And meal_complete) Then
        SetInd 3, "complete bath and meal"              '21%
    End If
    If (bath_complete And meal_NPO) Then
        SetInd 3, "complete bath and meal NPO"          '9%
    End If

    If inds(3).checked Then Exit Sub
    
    '
    ' 2. ADL assist
    '
    If (m_assist_count >= 1) Then SetInd 2, "assist >= 1"       'approx 30% ind #2
    If bath_complete Then SetInd 2, "bath complete"             'approx 30%
    If meal_complete Then SetInd 2, "meal complete"             'approx 2.5%
    If activity_assist Then SetInd 2, "activity assist"         'approx 0.5%
    
    SetIndIfResultContains 2, "", "", "", "Performance", "assist, partial"
    SetIndIfResultContains 2, "", "", "", "Performed by", "assist"
    SetIndIfResultContains 2, "", "", "", "Mealtime Assist", "cut food, verbal directions"
    SetIndIfResultContains 2, "NHx ADL/Sleep/Rest", "", "", "", "assist"
    
    If inds(2).checked Then Exit Sub
    
    '
    ' 1. ADL self
    '
    SetIndIfResultContains 1, "", "", "", "Mealtime Assist", "self"             'most here
    SetIndIfResultContains 1, "", "", "", "Performance", "self"
    SetIndIfResultContains 1, "NHx ADL/Sleep/Rest", "", "", "", "independent"
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_1_2_3"
    Resume  'debug
End Sub


Private Sub Check_4()
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "4. ADL Rehab"
    dvprint "---------------"
    
    SetIndIfFound 4, "Eval Inpatient Physical Therapy", "", "", "Home Recommendations", "", SearchSinceAdmission
    SetIndIfFound 4, "Cardiac Rehab Evaluation", , , , , SearchSinceAdmission
    '**These not used?
    SetIndIfFound 4, "Activity Rehab"
    SetIndIfFound 4, "NHx RHB Inital Evaluation", , , , , SearchSinceAdmission
    SetIndIfFound 4, "PHx Speech Tx Goals", , , , , SearchSinceAdmission
    SetIndIfFound 4, "Restorative Rehab Referral", , , , , SearchSinceAdmission
    SetIndIfFound 4, "NHx RHB OT Initial Eval", , , , , SearchSinceAdmission
    SetIndIfFound 4, "NHx RHB ROM TREAT"
    SetIndIfFound 4, "NHx RHB All But ROM"
    SetIndIfFound 4, "NHx RHB All TREAT"
    SetIndIfFound 4, "NHx RHB TREAT All"
    SetIndIfFound 4, "NHx RHB Rounds"
    SetIndIfFound 4, "NHx RHB OT Treat"
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_4"
    Resume  'debug
End Sub


Private Sub Check_5_6()
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "5. ADL 2-3 Caregivers"
    dvprint "6. ADL 4 or more Caregivers"
    dvprint "---------------"
    
    If (m_assist_count >= 4) Then
        SetInd 6, "assistance 4 or more persons"
    ElseIf (m_assist_count >= 2) Then
        SetInd 5, "assistance 2-3 persons"
    Else
        dvprint "not set because assistance < 2 persons"            'add to audit
    End If
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_5_6"
    Resume  'debug
End Sub

Private Sub Check_7()
    On Error GoTo errHandler
'    Dim hard_of_hearing As Boolean, hearing_aid As Boolean
'    Dim blind As Boolean, ignore As Boolean
    Dim tmp As String

    dvprint "---------------"
    dvprint "7. Communication"
    dvprint "---------------"

    'This takes the guesswork out of levels of blind/deaf with glasses/hearing aid
    SetIndIfResultContains 7, "VHx Admitting Information", "", "", "Communication Impaired?", "yes", SearchSinceAdmission
    
    SetIndIfResultContains 7, "PHx Language Spoken", "", "", "Language Spoken", "sign language, spanish, other, w/gestures, w/board, nonverbal, unable to speak english", SearchSinceAdmission
    'These are great because they have been charted as a barrier:
    SetIndIfResultContains 7, "VHx Educ/Learning", "", "", "Barriers to Educ/Plan #2", "deaf, hard of hearing, hard to understand, language", SearchSinceAdmission
    
    SetIndIfResultDoesNotContain 7, "NHx Cognitive/Learning", "", "Speech*", "Ability", "has no difficulty speaking", SearchSinceAdmission
    
    ' "unable to determine" = communication problem
    SetIndIfResultContains 7, "NHx Neuro/Muscle", "", "", "Orientation", "unable to determine"

    SetIndIfResultDoesNotContain 7, "NHx Cognitive/Learning", "", "", "Speech_Speech*", "no abnormalities", SearchSinceAdmission
    SetIndIfResultContains 7, "VHx Educ/Learning", "", "", "Education Level", "illiterate", SearchSinceAdmission
    SetIndIfResultContains 7, "NHx Assessment", "", "Neurological Assessment", "", "phasia", SearchSinceAdmission
    

'    If (inds(7).checked) Then Exit Sub
'
'    '
'    'Blind(ness) may be charted, but it can be cancelled by glasses or blindness in only one eye.
'    '
'    blind = ResultContains("VHx Admitting Information", "", "", "Vision Hx", "blind", SearchSinceAdmission)
'    If (Not blind) Then
'        blind = ResultContains("NHx Sensory/Perceptual", "", "", "Visual Disturbance", "blind, blurred, tunnel", SearchSinceAdmission)
'    End If
'
'    If (blind) Then
'        'look for glasses for left/right
'        tmp = "glasses,left,right"
'        ignore = ResultContains("VHx Admitting Information", "", "", "Vision Hx", tmp, SearchSinceAdmission)
'        If (Not ignore) Then
'            ignore = ResultContains("NHx Sensory/Perceptual", "", "", "Visual Disturbance", tmp, SearchSinceAdmission)
'        End If
'
'        If (Not ignore) Then
'            SetInd 7, "blindness"
'        Else
'            dvprint "partial blindness + glasses -- ignore"
'        End If
'    End If
'
'    '
'    'Deaf(ness) may be charted, but it can be cancelled by hearing aids
'    '
'    hard_of_hearing = ResultContains("VHx Admitting Information", "", "", "Hearing Deficits", "deaf, hard of hearing", SearchSinceAdmission)
'    If (Not hard_of_hearing) Then
'        hard_of_hearing = ResultContains("NHx Sensory/Perceptual", "", "", "", "deaf, hard of hearing", SearchSinceAdmission)
'    End If
'
'    If (hard_of_hearing) Then
'        'Look for "hearing aid"
'        hearing_aid = ResultContains("VHx Admitting Information", "", "", "Hearing Deficits", "hearing aid", SearchSinceAdmission)
'        If (Not hearing_aid) Then
'            hearing_aid = ResultContains("NHx Sensory/Perceptual", "", "", "", "hearing aid", SearchSinceAdmission)
'        End If
'
'        If (Not hearing_aid) Then
'            SetInd 7, "deaf / hard of hearing"
'        Else
'            dvprint "hard of hearing with hearing aid -- ignore"
'        End If
'    End If
    
    Exit Sub

    
errHandler:
    g_util.ThrowError "Check_7"
    Resume  'debug
End Sub

Private Sub Check_8()
    On Error GoTo errHandler
    Dim tmp As String

    dvprint "---------------"
    dvprint "8. Cognitive Support"
    dvprint "---------------"

    'charted once at admission - very common
    SetIndIfResultContains 8, "VHx Educ/Learning", "", "", "Barriers to Educ/Plan #2", "cognitive, confused", SearchSinceAdmission
    
    tmp = "confused, disoriented, reoriented, forgetful, does not recognize parent"
    SetIndIfResultContains 8, "NHx Neuro/Muscle", "", "", "Orientation", tmp                    'common
    SetIndIfResultContains 8, "NHx Neuro/Muscle", "", "", "Loc", tmp                            'some
    SetIndIfResultContains 8, "NHx Education/Instruction", "", "", "Education Response", tmp    'some
    SetIndIfResultContains 8, "", "", "", "Shift Comment", tmp                                  'some
    SetIndIfResultContains 8, "NHx Cognitive/Learning", "", "", "Orientation", tmp
    
    '**not needed or redundant?
    'SetIndIfResultContains 8, "", "", "", "Activity Tolerance", tmp
    'SetIndIfResultContains 8, "", "", "", "Patient Status", tmp
    'SetIndIfResultContains 8, "NHx Education/Instruction", "", "", "Learning Readiness", "cognitive barrier"
    
    'Restraint based on MD order for impairment, confusion, etc.
    SetIndIfResultContains 8, "Restraint Application (Med/Surg Healing)", "", "", "Reason", "Neurological impairment due to anesthesia, Confusion, Altered level of consciousness"
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_8"
    Resume  'debug
End Sub

Private Sub CheckBehavior(count As Integer, desc As String)
    Dim los_hours As Single
    
    If (inds(10).checked) Then Exit Sub             'skip if highest already checked
    If (count = 0) Then Exit Sub

    los_hours = m_pat.range / 60#

    Select Case FreqForCount(los_hours, count)
    Case Q1H, Q30M
        SetInd 10, desc & " q1h"
    Case Else
        SetInd 9, desc
    End Select
End Sub

Private Sub Check_9_10()
    On Error GoTo errHandler
    Dim tmp As String

    dvprint "---------------"
    dvprint "9.  Behavior/Emotional Management"
    dvprint "10. Behavior/Emotional Mgmt - q 1 Hour"
    dvprint "---------------"

    '
    ' 10.       -- none found
    '
    'These are automatic Q1h
    SetIndIfResultContains 10, "NHx Psychological", "", "", "Anxiety Scale", "severe, panic"
    SetIndIfResultContains 10, "NHx Psychological", "", "", "Anxiety Relief Measure Results", "no relief"
    'Note: these restraints are specifically for behavior; restraints policy is q30m
    SetIndIfFound 10, "Behavioral Restraint Application (Behavioral Health Reasons)"
    SetIndIfFound 10, "Behavioral Restraint Maintenance (Behavioral Health Reasons)"
    If inds(10).checked Then Exit Sub
    
    'These are based on frequency
    CheckBehavior CountSimpleResult("Anxiety Support", "", "", "Method", ""), "Anxiety Support"
    CheckBehavior CountSimpleResult("Emotional Support", "", "", "Method", ""), "Emotional Support"
    If inds(10).checked Then Exit Sub
    
    
    '
    ' 9.
    '
    tmp = "agitated,anxious,confused,depressed,forgetful,deficiency,limited"
    SetIndIfResultContains 9, "", "", "", "Mental Status", tmp                              'most
    tmp = "anxious,combative,crying,depressed,fearful,inappropriate,uncooperative"
    SetIndIfResultContains 9, "", "", "", "Patient Status", tmp                             'some
    SetIndIfResultContains 9, "NHx Neuro/Muscle", "", "", "Activity/Behavior", tmp          'little
    SetIndIfResultContains 9, "NHx Cognitive/Learning", "", "", "General Behavior", tmp     '**not needed?
    tmp = "agitated,angry,anxious,bored,depressed,dissatisfied,emptiness,fearful,guilt,helpless,impatient,impending doom,inadequacy,irritable,lethargic,nervous,no control,powerless,rejected,sad,tearful,tense,can't relax,worried,worthless"
    SetIndIfResultContains 9, "NHx Feelings", "", "", "Feelings", tmp                       '**not needed?
    If inds(9).checked Then Exit Sub

    'some
    CheckBehavior CountSimpleResult("NHx Psychological", "", "", "Anxiety Relief Measures", ""), "Anxiety Relief Measures"
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_9_10"
    Resume  'debug
End Sub

Private Sub CheckSafety(count As Integer, desc As String)
    Dim los_hours As Single
    
    If (inds(12).checked) Then Exit Sub             'skip if highest already checked
    If (count = 0) Then Exit Sub

    los_hours = m_pat.range / 60#
    
    Select Case FreqForCount(los_hours, count)
    Case Q30M
        SetInd 12, desc & " q30min"
    Case Q1H, Q2H
        SetInd 11, desc & " q2h"
    Case Else
        dvprint desc & ": " & count & " in " & Round(los_hours, 0) & " hours is not enough"
    End Select
End Sub

Private Sub Check_11_12()
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "11. Safety Management - q 2 Hours"
    dvprint "12. Safety Management - q 30 Minutes"
    dvprint "---------------"

    '
    ' 12.
    '
    SetIndIfResultContains 12, "", "", "", "Patient Status", "sitter, adult supervision"    'very common
    If inds(12).checked Then Exit Sub
    
    '**not needed?  (none used)
    SetIndIfResultContains 12, "NHx Safety", "", "", "Safety Measures", "sitter"
    'Note: Arnot restraints policy is q30m
    SetIndIfFound 12, "Restraint Application (Med/Surg Healing)"
    SetIndIfFound 12, "Restraint Maintenance (Med/Surg Healing)"
    SetIndIfResultContains 12, "NHx Safety", "", "", "Safety Measures", "restraint"
    'SetIndIfResultContains 12, "", "", "Shift Comment", "restraint"     'could mean on or off
    If inds(12).checked Then Exit Sub
    SetIndIfResultContains 12, "Continuum of Care First Contact", "", "", "Referral Reason", "suicide attempt", SearchSinceAdmission
    If inds(12).checked Then Exit Sub

    'frequency based
    CheckSafety CountSimpleResult("NHx Safety", "", "", "Safety Measures", ""), "NHx Safety"
    If inds(12).checked Then Exit Sub

    '
    ' 11.
    '
    'search for persistent items
    SetIndIfResultContains 11, "NHx Safety", "", "", "Fall Prevention Program", "FPP", SearchSinceArrival   'some
    SetIndIfResultContains 11, "NHx Injury/Fall/Risk?", "", "Morse Fall Risk Assessment", "Intervention Level", "medium, high", SearchSinceArrival
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_11_12"
    Resume  'debug
End Sub

Private Sub Check_13()
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "13. Isolation"
    dvprint "---------------"
    
    SetIndIfFound 13, "Precautions/Isolation Maintenance"           'majority
    SetIndIfFound 13, "Precautions/Isolation Setup"                 'some
    ClrIndIfFound 13, "Isolation Discontinued"
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_13"
    Resume  'debug
End Sub

Private Sub CheckAssessment(count As Integer, desc As String)
    Dim los_hours As Single

    If (inds(17).checked) Then Exit Sub             'skip if highest already checked
    If (count = 0) Then Exit Sub                    'skip if none

    'Here's the mathematical way:
    '    freq_min = g_util.DivideWithoutError(m_pat.range, count)
    'The book, however, says that you did this frequency for the majority of the time period.
    'This could mean that you did it for only 51% of the period.  Use a lookup table instead.
    
    los_hours = m_pat.range / 60#
    
    Select Case FreqForCount(los_hours, count)
    Case Q30M
        SetInd 17, desc & " q30min"
    Case Q1H
        SetInd 16, desc & " q1h"
    Case Q2H
        SetInd 15, desc & " q2h"
    Case Q4H
        SetInd 14, desc & " q4h"
    Case QNONE
        dvprint desc & ": " & count & " in " & Round(los_hours, 0) & " hours is not enough"
    End Select

End Sub

Private Sub Check_14_15_16_17()
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "14. Assessment q4h"
    dvprint "15. Assessment q2h"
    dvprint "16. Assessment q1h"
    dvprint "17. Assessment q30min"
    dvprint "---------------"
    
    ' "like" assessments are grouped here

    CheckAssessment CardioManagementCount, "Cardio Management"          'most
    If inds(17).checked Then Exit Sub
    CheckAssessment MedicationManagementCount, "Medication Management"  'tie
    If inds(17).checked Then Exit Sub
    CheckAssessment FluidManagementCount, "Fluid Management"            'tie
    If inds(17).checked Then Exit Sub
    CheckAssessment PulmonaryManagementCount, "Pulmonary Management"    'some
    If inds(17).checked Then Exit Sub
    CheckAssessment NeuroManagementCount, "Neuro Management"            'less
    If inds(17).checked Then Exit Sub

    'Give ICU have a minimum of q1h
    If Right$(m_pat.unit_name, 3) = "ICU" Then SetInd 16, "ICU default" 'most #16

    If (inds(16).checked Or inds(17).checked) Then Exit Sub
    ' Look for q1h or lower
    
    '**not used?
    SetIndIfFound 16, "Medication Reconciliation Transfer"      '10/22/10; needs verification
    SetIndIfFound 16, "Sedation Check"                          '03/30/11; needs verification
    
    If (inds(15).checked Or inds(16).checked Or inds(17).checked) Then Exit Sub
    'Look for q4h

    'Make this a minimum of q4h no matter what the charted frequency is
    SetIndIfFound 14, "NHx CV - Cardiac Rhythm", "", "", "Rhythm"       'some
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_14_15_16_17"
    Resume  'debug
End Sub

Private Function AuditAssessment(debug_sql As String)
    On Error GoTo errHandler
    Dim rs As New Recordset
    Dim msg As String
    
    'Print the list of buckets and all assessments within each bucket
    rs.Open debug_sql, g_cnADO
    Do While Not rs.EOF                     'Bucket 00 <cat/desc/field>
        msg = ""
        If Len(rs("category")) Then msg = msg & " cat='" & rs("category") & "'"
        If Len(rs("code")) Then msg = msg & " code='" & rs("code") & "'"
        If Len(rs("description")) Then msg = msg & " desc='" & rs("description") & "'"
        If Len(rs("field_name")) Then msg = msg & " field='" & rs("field_name") & "'"

        dvprint "Bucket " & Format$(rs("bucket"), "00") & " " & msg
        rs.MoveNext
    Loop
    
    rs.Close
    Exit Function
    
errHandler:
    LogError "AuditAssessment: " & Err.Description, EVENT_CATEGORY_UNEXPECTED
End Function

Private Function CountAssessments(sql As String, debug_sql As String) As Integer
    On Error GoTo errHandler
    Dim result As Integer
    Dim rs As New Recordset
    
    If Len(sql) = 0 Then Exit Function

    'If g_verbose Then
        'Very detailed - for debugging - not needed in general
        'AuditAssessment debug_sql
    'End If
    
    'Count the number of (distinct) buckets from the union of assessments
    'Because of the union, the list is already distinct!
    rs.Open sql, g_cnADO
    result = rs.RecordCount
    dvprint result & " unique time buckets"
    rs.Close

    CountAssessments = result
    Exit Function
errHandler:
    LogError "CountAssessments: " & Err.Description, EVENT_CATEGORY_UNEXPECTED
    'continue processing
End Function

'Add another list of assessment buckets to the query
Private Sub AddBuckets(sql As String, debug_sql As String, cat As String, code As String, desc As String, field As String, Optional result_list As String = "")
    Dim s As String
    
    If (Len(sql) > 0) Then
        sql = sql & " union "
        debug_sql = debug_sql & " union "
    End If

    'Assign virtual bucket numbers to each assessment.  Bucket zero = pull_start time.
    
    'List bucket numbers only in the regular SQL
    sql = sql & "select " & BUCKET_CALC & _
        " from chart_item" & _
        WhereBase & AndSimpleItemFilter(cat, code, desc, field, "") & _
        AndResultContains(result_list)

    'List bucket and assessment name for audit
    debug_sql = debug_sql & "select " & BUCKET_CALC & " as bucket, category, code, description, field_name" & _
        " from chart_item" & _
        WhereBase & AndSimpleItemFilter(cat, code, desc, field, "") & _
        AndResultContains(result_list)
        
    'if (g_verbose) then
        'Say what we are looking for (and how many were found)
        If Len(result_list) Then
            If CountResultContains(cat, code, desc, field, result_list) Then
                'we just printed what was found or not found
            End If
        Else
            dvprint Describe(cat, code, desc, field, "")
        End If
    'End If
End Sub

Private Sub OrderBuckets(sql As String, debug_sql As String)
    'Because of the union, both lists are already ordered
End Sub

Private Function CardioManagementCount() As Integer
    Dim sql As String, debug_sql As String

    'Try to restict all searches to one field (the last argument below) or we will be flooded with results.
    'The extra results won't hurt the calc, but they will slow things down and clog the audit.

    dvprint "** Cardio Management **"
    AddBuckets sql, debug_sql, "", "", "", "Temp"
    AddBuckets sql, debug_sql, "", "", "", "Pulse"
    AddBuckets sql, debug_sql, "", "", "", "BP"
    AddBuckets sql, debug_sql, "Pulse Oximetry", "", "", "O2 Sat"
    AddBuckets sql, debug_sql, "NHx Assessment", "", "", "Cardiovascular Assessment"
    AddBuckets sql, debug_sql, "NHx Assessment", "", "", "Cardio/Respiratory Assessment"
    AddBuckets sql, debug_sql, "NHx CV - Cardiac Rhythm", "", "", "Rhythm"
    AddBuckets sql, debug_sql, "Post Heart Catheterization Assessment", "", "", "Location"
    AddBuckets sql, debug_sql, "Post PCI Assessment", "", "", "Location"       'percutaneous corinary intervention
    OrderBuckets sql, debug_sql

    CardioManagementCount = CountAssessments(sql, debug_sql)
End Function

Private Function NeuroManagementCount() As Integer
    Dim sql As String, debug_sql As String

    dvprint "** Neuro Management **"
    AddBuckets sql, debug_sql, "NHx Assessment", "", "", "Neurological Assessment"
    AddBuckets sql, debug_sql, "NHx Assessment", "", "", "Neurovascular Assessment"
    AddBuckets sql, debug_sql, "NHx Neuro/Muscle", "", "", "Loc"
    OrderBuckets sql, debug_sql

    NeuroManagementCount = CountAssessments(sql, debug_sql)
End Function

Private Function PulmonaryManagementCount() As Integer
    Dim sql As String, debug_sql As String

    dvprint ""
    dvprint "** Pulmonary Management **"
    AddBuckets sql, debug_sql, "", "", "", "Resp"
    'Note: just because we find Incentive Spirometer doesn't mean they did it; we have to search the result
    AddBuckets sql, debug_sql, "Routine Check Nursing", "", "", "Incentive Spirometer", "independent, encourage, observe, supervision, used, volume"
    'There are lot of fields in the assessment; limit to Cough to speed things up.
    AddBuckets sql, debug_sql, "Respiratory Therapy Assessment", "", "", "Cough"
    AddBuckets sql, debug_sql, "NHx Ped - Resp Assessment", "", "", "Cough"
    AddBuckets sql, debug_sql, "NHx Assessment", "", "", "Respiratory Pain Assessment"
    AddBuckets sql, debug_sql, "Airway Suction", "", "", "Performance"
    OrderBuckets sql, debug_sql

    PulmonaryManagementCount = CountAssessments(sql, debug_sql)
End Function

Private Function FluidManagementCount() As Integer
    Dim sql As String, debug_sql As String

    'The problem with I/O is that we will get a large number of measurements (q1h)
    'but no assessments that compare I/O?
    
    dvprint ""
    dvprint "** Fluid Management **"
    'Almost all I&O have Amt or Amount.  (don't have to list all the categories)
    AddBuckets sql, debug_sql, "", "", "", "Amt"
    AddBuckets sql, debug_sql, "", "", "", "Amount"
    AddBuckets sql, debug_sql, "", "", "", "Drainage Amt"
    'be specific here; "performance" is used in other areas
    AddBuckets sql, debug_sql, "Foley Output CA", "", "", "Performance"
    OrderBuckets sql, debug_sql

    FluidManagementCount = CountAssessments(sql, debug_sql)
End Function

Private Function MedicationManagementCount() As Integer
    Dim sql As String, debug_sql As String

    dvprint ""
    dvprint "** Medication Management **"
    AddBuckets sql, debug_sql, "PHx Hx - Medication", "", "", "Drug"
    AddBuckets sql, debug_sql, "NHx Sensory/Perceptual", "", "Pain Assessment*", "Pain Level"
    AddBuckets sql, debug_sql, "Morphine PCA", "", "", "Comment"
    AddBuckets sql, debug_sql, "Morphine PCA", "", "", "Sedation Level"
    AddBuckets sql, debug_sql, "Dilaudid (Hydromorphone) PCA", "", "", "Comment"
    AddBuckets sql, debug_sql, "Dilaudid (Hydromorphone) PCA", "", "", "Sedation Level"
    AddBuckets sql, debug_sql, "Insulin, Humalog - Sliding Scale", "", "", "Site"
    AddBuckets sql, debug_sql, "Bedside Glucose POC (Insulin Drip) ", "", "", "Glucose"
    AddBuckets sql, debug_sql, "Bedside Glucose POC", "", "", "Glucose"
    OrderBuckets sql, debug_sql

    MedicationManagementCount = CountAssessments(sql, debug_sql)
End Function


Private Sub Check_18()
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "18. Medication Preparation >= 20 Minutes"
    dvprint "---------------"
    
    SetIndIfResultContains 18, "NHx Activity Protocol", "", "", "Meds > 20 min", "yes"  'most here
    SetIndIfFound 18, "Transfusion for Inpatient"         '"Blood Transfusion for Inpatient" 04/01/11
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_18"
    Resume  'debug
End Sub

Private Sub CheckWound(total As Integer)
    If (inds(20).checked) Then Exit Sub             'skip if highest already checked

    If (total = 0) Then
        'skip it
    ElseIf (total >= 30) Then
        SetInd 20, "wound >= 30 min"
    Else
        SetInd 19, "wound < 30 min"
    End If
End Sub

Private Sub Check_19_20()
    On Error GoTo errHandler
    Dim n As Integer
    
    dvprint "---------------"
    dvprint "19. Wound/Injury Mgmt"
    dvprint "20. Wound/Injury Mgmt >= 30 Minutes"
    dvprint "---------------"

    If (m_pat.age * 365.24219 < 14) Then        '10/22/2010
        SetInd 19, "age < 2 weeks"
        Exit Sub
    End If

    'Total the wound care with duration
    n = GetTotalValue("Incision/Wound Care", "", "", "Duration", "")    'almost 100% found
    CheckWound n
    If inds(20).checked Then Exit Sub                                   'quick exit
    'Look for variations
    CheckWound n + _
        GetTotalValue("NHx Skin", "", "", "Duration", "") + _
        GetTotalValue("Dressing Change", "", "", "Duration", "")
    If inds(20).checked Or inds(19).checked Then Exit Sub
    
    'Look for wound care without a duration
    SetIndIfFound 19, "NHx Skin", "", "Incision/Wound*"
    SetIndIfFound 19, "NHx Skin", "", "Incision*"
    SetIndIfFound 19, "NHx Skin", "", "", "", "skin integrity impaired"
    SetIndIfFound 19, "Wound Care RX"
    SetIndIfFound 19, "Incision/Wound Care"
    'Look for maternal wound care, or something that imples it
    SetIndIfFound 19, "Postpartum Void Check", "", "", "Pericare"       '04/26/11
    SetIndIfFound 19, "NHx OB Delivery Incision"                        '05/10/11
    'SetIndIfResultDoesNotContain 19, "VHx OB Delivery & Incision Types", "", "", "Delivery Incision Type", "no incision"
    SetIndIfFound 19, "VHx OB Delivery & Incision Types"                '05/10/11
    SetIndIfFound 19, "NHx OB Pregnancy/Birth"                          '05/10/11
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_19_20"
    Resume  'debug
End Sub

Private Function EstimateEducationMins(chart_result As String) As Integer
    Dim mins As Integer, i As Integer, n As Integer, count As Integer
    Dim arr() As String

    'Most results have commas between subjects but some just run on without punctuation.
    'Split the results on commas but look for all keywords in each substring.
    
    '** Convert the search string to lower case and seach for lower case
    n = g_util.SplitTextOnChar(LCase$(chart_result), ",", arr(), 1, 0)

    For i = 1 To n
        If Len(arr(i)) > 0 Then
            If InStr(1, arr(i), "chf:") Then mins = mins + 40
            If InStr(1, arr(i), "guaiac") Then mins = mins + 5
            
            If InStr(1, arr(i), "incentive spirometer") Then mins = mins + 10
            If InStr(1, arr(i), "use of mdi") Then mins = mins + 15
            If InStr(1, arr(i), "peak flow") Then mins = mins + 10
            If InStr(1, arr(i), "svn") Then mins = mins + 5
            If InStr(1, arr(i), "volume expansion") Then mins = mins + 10
            If InStr(1, arr(i), "mucous clearing") Then mins = mins + 15
            
            If InStr(1, arr(i), "first time mother") Then mins = mins + 60
            If InStr(1, arr(i), "first time breastfeeding") Then mins = mins + 60
            'ignore all others
            count = count + 1           'count non-null substrings (beware empty commas)
        End If
    Next i
    
    If (mins = 0) Then mins = 5         'count * 5

    dvprint "Est " & mins & " mins for " & chart_result

    EstimateEducationMins = mins
End Function

Private Sub CheckEducation(total As Integer)
    If (inds(21).checked) Then Exit Sub             'skip if already checked

    If (total >= 60) Then
        SetInd 21, "education >= 60 min"
    End If
End Sub

Private Sub Check_21()
    On Error GoTo errHandler
    
    Dim mins As Integer, n As Integer
    Dim chart_result As String
    
    dvprint "---------------"
    dvprint "21. Healthcare Mgmt Education >= 1 Hour"
    dvprint "---------------"

    n = GetTotalValue("NHx Education/Instruction", "", "", "Duration of Teaching Nursing", "")  'almost 100%
    CheckEducation n
    If inds(21).checked Then Exit Sub

    mins = n + _
        GetTotalValue("NHx Education/Instruction", "", "", "Duration", "") + _
        GetTotalValue("NHx Education/Instruction(ET)", "", "", "Duration", "") + _
        GetTotalValue("NHx Education/Instruction", "", "", "Dietary Duration of teaching", "") + _
        GetTotalValue("NHx Activity Protocol", "", "", "Education Time", "")
    CheckEducation mins
    If inds(21).checked Then Exit Sub

    'No duration given?  Make an estimate
    If (mins = 0) Then
        If GetResult("NHx Education/Instruction", "", "", "Area of Instruction", chart_result) Then
            mins = mins + EstimateEducationMins(chart_result)
        End If
        If GetResult("NHx Education/Instruction(ET)", "", "", "Area of Instruction", chart_result) Then
            mins = mins + EstimateEducationMins(chart_result)
        End If
        If GetResult("NHx Education/Instruction(ET)", "", "", "Area of Instruction - Guaiac", chart_result) Then
            mins = mins + EstimateEducationMins(chart_result)
        End If
    End If
    
    CheckEducation mins
    If Not inds(21).checked Then
        dvprint "Education < 60 min; (" & mins & " min total)"
    End If
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_21"
    Resume  'debug
End Sub

Private Sub Check_22()
    On Error GoTo errHandler
    
    dvprint "---------------"
    dvprint "22. 1 to 1 Physiological Interv. >= 2 Hours"
    dvprint "---------------"
   
    SetIndIfResultContains 22, "NHx - Acuity Plus indicators", "", "", "Physio Intervention", "2 hrs or more"
    Exit Sub
    
errHandler:
    g_util.ThrowError "Check_22"
    Resume  'debug
End Sub



Private Sub CheckProcs()
    'No procedures are supported at this time
End Sub


Private Sub AtLeastOneADL()
    If Not (inds(1).checked Or inds(2).checked Or inds(3).checked) Then
        'Note: Heather wants "#2 ADL - Assist" to be the default.  (90% of patients)
        SetInd 2, "at least one ADL"
    End If
End Sub

Private Sub HighestIndicatorInEachGroupWins()
    Dim i As Integer
    Dim g As Integer
    Dim highest_is_on As Boolean
    Dim ind_list As String

    dvprint "---------------"
    dprint "Select highest indicator in each group"
    
    g = 0
    highest_is_on = False
    For i = MAX_INDS To 1 Step -1
        If (inds(i).group > 0) Then
            If (inds(i).group <> g) Then
                'this is a new group
                g = inds(i).group
                highest_is_on = inds(i).checked
            Else
                'same group
                If highest_is_on Then
                    inds(i).checked = False             'uncheck a lower number
                Else
                    highest_is_on = inds(i).checked     'save this one
                End If
            End If
        End If
    Next i
    
    'Echo the indicators for an audit (no classification will be saved)
    For i = 1 To MAX_INDS
        If inds(i).checked Then ind_list = ind_list & "," & i
    Next i
    dprint "Final list = " & Mid$(ind_list, 2)

End Sub

Private Sub OutputClass()
    On Error GoTo errHandler
    Dim outstr As String, ind_list As String, desc As String, str_pull_dt As String
    Dim i As Integer
    Dim tc_event_id As Long

    tc_event_id = g_dbutil.NextGID()                                        'get a unique id for this class
    str_pull_dt = Format$(m_pat.pull_finish, "yyyymmddhhnn")

    outstr = g_util.FixedWidth("", 8)                                       '(facility code)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.unit_name, 16)
    outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(unit code)
    outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(area code)
    outstr = outstr & "|" & g_util.FixedWidth("", 8)                        '(class date - give datetime instead)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.acct, 20)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.last_name, 32)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.first_name, 32)
    outstr = outstr & "|" & g_util.FixedWidth("", 32)                       '(middle)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.room, 8)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.bed, 4)
    outstr = outstr & "|" & g_util.FixedWidth(str_pull_dt, 12)              'class datetime (could change)
    outstr = outstr & "|" & g_util.FixedWidth("", 16)                       '(login)
    outstr = outstr & "|" & g_util.FixedWidth(str_pull_dt, 16)              '(employee)/(pull datetime)
    outstr = outstr & "|" & g_util.FixedWidth(CStr(m_pat.meth_id), 4)
    outstr = outstr & "|" & g_util.FixedWidth("C", 1)                       'record type = class
    outstr = outstr & "|" & g_util.FixedWidth("", 4)                        '(stage)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.TC_source_id, 4)        'TC source ID - (TCP port)
    outstr = outstr & "|" & g_util.FixedWidth(m_pat.range, 4)               'TC pull range
    outstr = outstr & "|" & g_util.FixedWidth(tc_event_id, 10)              'TC event ID
    outstr = outstr & "|"
    outstr = g_util.FixedWidth(outstr, 294)
    outstr = outstr & "|" & Format$(m_pat.effective, "yyyymmddhhnn")        'IN
    outstr = g_util.FixedWidth(outstr, 377)
    outstr = outstr & "|"
    
    For i = 1 To MAX_INDS
        If (inds(i).checked) Then
            outstr = outstr & "Y"
            ind_list = ind_list & "," & i
        Else
            outstr = outstr & "N"
        End If
    Next i
    ind_list = Mid$(ind_list, 2)                    'strip leading comma
    
    Print #outfile, outstr                          'output to transparent.txt
    
    'Save the selected indicators plus both audits in the event log; link with tc_event_id
    desc = "Classified: " & ind_list
    g_event.AddTransparentMappingEventLogEntry desc, gLogUnitID, gLogEncounterID, _
        tc_event_id, gLogMapperVersion, gBriefAudit, gVerboseAudit
    Exit Sub
    
errHandler:
    g_util.ThrowError "OutputClass"
    Resume  'debug
End Sub

Private Sub OutputProcs()
    On Error GoTo errHandler
    Dim i, j As Integer
    Dim outstr As String, proc_list As String, desc As String
    Dim tc_event_id As Long

    For i = 1 To numprocs
        tc_event_id = g_dbutil.NextGID()                                    'get a unique id for this proc

        outstr = Space$(9) & m_pat.unit_name  '10
        outstr = outstr & Space$(69 - Len(outstr))
        outstr = outstr & m_pat.acct
        outstr = outstr & Space$(89 - Len(outstr))
        outstr = outstr & "|" & g_util.FixedWidth(m_pat.acct, 20)           '90
        outstr = outstr & "|" & g_util.FixedWidth(m_pat.last_name, 32)
        outstr = outstr & "|" & g_util.FixedWidth(m_pat.first_name, 32)
        outstr = outstr & "|" & g_util.FixedWidth("", 32)                   '(middle)
        outstr = outstr & Space$(203 - Len(outstr))
        outstr = outstr & Format$(procs(i).start, "yyyymmddhhnn")           '204 proc dt
        outstr = outstr & Space$(255 - Len(outstr))
        outstr = outstr & "P"                                               '256 procedure type record
        outstr = outstr & "|" & g_util.FixedWidth("", 4)                    '(stage)
        outstr = outstr & "|" & g_util.FixedWidth(m_pat.TC_source_id, 4)    'TC source ID
        outstr = outstr & "|" & g_util.FixedWidth(m_pat.range, 4)           'TC pull range
        outstr = outstr & "|" & g_util.FixedWidth(tc_event_id, 10)          'TC event ID
        outstr = outstr & "|"
        outstr = outstr & Space$(295 - Len(outstr))
        outstr = outstr & Format$(procs(i).start, "yyyymmddhhnn")           '296 procdt in
        outstr = outstr & Space$(347 - Len(outstr))
        outstr = outstr & Format$(procs(i).finish, "yyyymmddhhnn")          '348 procdt out
        outstr = outstr & Space$(378 - Len(outstr))
        For j = 1 To MAX_PROCS
            If procs(i).pnum = j Then
                outstr = outstr & "Y"
                proc_list = proc_list & "," & j
            Else
                outstr = outstr & "N"
            End If
        Next j
        proc_list = Mid$(proc_list, 2)              'strip leading comma

        Print #outfile, outstr                      'output to transparent.txt
        
        'Save the selected procedures plus both audits in the event log; link with tc_event_id
        '(this assumes the procedure audit is mixed in with the class audit)
        desc = "Procedure: " & proc_list
        g_event.AddTransparentMappingEventLogEntry desc, gLogUnitID, gLogEncounterID, _
            tc_event_id, gLogMapperVersion, gBriefAudit, gVerboseAudit
    Next i

    Exit Sub

errHandler:
    g_util.ThrowError "OutputProcs"
    Resume  'debug
End Sub

