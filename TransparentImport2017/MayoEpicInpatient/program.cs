﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;                // for SqlConnection
using System.IO;                            // for Directory, Path, Stream
using System.Windows.Forms;                 // for Application (also add reference)
using PfsShared;                            // add a reference to Shared2 project
//
// Mayo Rochester Epic INPATIENT Transparent Mapping main program. 
// GOAL is q4 hours with 8 hours lookback
// NOTE: Each unit must set the "use transparent" flag.
//
// sample: -efftime=0700 -effdate=yesterday -pulltime=0700 -pulldate=today -range=1440
//
// This is designed to be run as a background job, so never stop for user input -- that means no error MsgBox.
// All communication is done through log files and the event log; it is OK to print to the console.
//
// In order to work with the AcuityPlus Patient Selection and Transparent import:
//
//   * The program name must be TransparentMapping.exe, placed in AcuityPlus\bin
//   * The audit file is called TransparentAudit.log, placed in AcuityPlus\log
//   * The output file is Transparent.txt, placed in AcuityPlus\load_me
//   * Events are saved in the database event log
//
namespace TransparentMapping
{
    public class PatientInfo
    {
        public int      encounter_id;
        public string   last_name;
        public string   first_name;
        public string   middle_name;
        public string   acct;
        public double   age;                    // age (in years) at admission
        public string   room;
        public string   bed;
        public DateTime unit_arrival;
        public DateTime unit_departure;
        public DateTime effective;              // patient specific (may be > g_effdt)
        public DateTime effective_out;              // patient specific (may be > g_effdt)
        public DateTime pull_start;             // patient specific (may be > g_pull_start)
        public DateTime pull_finish;            // patient specific (may be < g_pull_finish)
        public int      range;                  // patient specific (may be < g_range) - minutes
        public double   los_hours;              // hours during pull
        public int      TC_source_id;           // TCP port (chart source)
        // unit info
        public string   facilty_code;           // class import facility code
        public string   unit_name;
        public int      unit_id;
        public bool     is_ED;
        public bool     is_ICU;
        public int      meth_id;
        public string adt_unit_name;
        public DateTime sod;
        public int upid;
        public int ptype;
        public List<PatientLocation> patloclist;
        public int loc_idx;
        public bool is_isolation;
        //public string default_inds_str;         // "1,6,8"
        //public int default_ptype;
    }
    public struct med_data
    {
        public string code;
        public string descript;
        public string drugclass;
        public string result;
        public DateTime evdt;
    }
    public class PatientLocation
    {
        public int unit_id;
        public string unit_name;
        public int methid;
        public string room;
        public string bed;
        public string service;
        public DateTime in_time;
        public DateTime out_time;
        public int los_mins;
        public bool remove;  //flag for compacting locs
        public DateTime arr_time;
        public DateTime loc_start;
        public DateTime los_start;
        public DateTime los_end;
        public int special_unit_id;
        public bool is_last_loc;
        public int loc_idx;
    }


    public static class Program
    {

        const string    MAPPER_VERSION = "1.00";
        const string    TRANSP_FILENAME = "Transparent.txt";    // THE output file (for class import)
        const string    TRANSP_AUDIT_LOG = "IPTransparentAudit.log";  // Legacy debug/audit log
        const int       CHART_ITEM_LIFE = 360;                   // days in the chart_item table
        const int       DEFAULT_RANGE = 1440;                   // 24 hrs in minutes; override with -range

        public static bool g_abort;                         // stop!
        public static bool g_debug;                         // output to console?
        public static bool g_log;                           // output to log file? (legacy feature)
        public static bool g_no_output;                     // audit file only?
        public static bool g_is_test;                       // output to file w/dummy ids and no event log
        public static bool g_no_delete;                     // keep old chart items?
        public static bool g_stop_on_error;
        public static bool g_import_when_done;
        public static bool g_use_all_chart_items=false;
        public static bool g_do_MH = true; //do MH classifications
        public static bool g_do_OW = true; //do Other Workload

        public static DateTime      g_effdt;                // effective datetime

        public static StreamWriter  outfile;                // Transparent.txt
        public static StreamWriter  logfile;                // TransparentLog.txt

        public static int           gLogUnitID;
        public static int           gLogEncounterID;
        public static string        gLogSourceText;
        public static string        gLogMapperVersion;

        public static StringBuilder gBriefAudit;            // the complete brief audit
        public static StringBuilder gVerboseAudit;          // the complete verbose audit

        public static DateTime      g_pull_start;           // global pull start  (not limited by patient values)
        public static DateTime      g_pull_finish;          // global pull finish
        public static int           g_range;                // global range

        private static string   _this_acct;                 // acct filter
        private static string   _this_unit_name;            // unit name filter
        private static int      _this_unit_id;              // unit id filter
        private static string   _these_facs;     // -facs=2,5  facilities filter based on F.class_fac_code
          //example:  AND SUBSTRING(F.CLASSIFICATION_FACILITY_CODE, 1, 1) in (2,5)

        static string           _import_path;
        static string           _log_path;
        public static List<PatientLocation> patloclist;
        public static List<PatientLocation> patperioplist;
        static void Main(string[] args)
        {
            try
            {
                InitGlobals();

                ParseCommandLine(args);
                SetMapperVersion();

                gLogSourceText = String.Join(" ", args);           // add command line to log
                LogInfo("Begin mapping and translation", PFSEventLog.EventLogCategory.EVENT_CATEGORY_STARTUP_SHUTDOWN);
                gLogSourceText = "";

                OpenOutputFiles();
                ProcessPatients();

//                DeleteOldChartItems();

                LogInfo("Mapping complete", PFSEventLog.EventLogCategory.EVENT_CATEGORY_STARTUP_SHUTDOWN);

                //MaybeRunImport();
                CloseOutputFiles();
            }
            catch (Exception e)
            {
                Console.WriteLine("Unexpected Error: {0}", e.Message);
                Console.WriteLine("{0}", e.StackTrace);
                LogUnexpectedError(e.Message, e.StackTrace);
            }

            if (g_debug)
            {
                Console.WriteLine(Environment.NewLine);
                Console.Write("Press any key...");
                Console.ReadKey();
            }
        }

        static void InitGlobals()
        {
            gLogSourceText = "";
            gBriefAudit = new StringBuilder();
            gVerboseAudit = new StringBuilder();
        }

        static void ParseCommandLine(string[] args)
        {
            string nowdate, nowtime;
            string value;
            string effdate, efftime;
            string pulldate, pulltime;

            var nowdt = DateTime.Now;
            nowdate = nowdt.ToString("yyyyMMdd");
            nowtime = nowdt.ToString("HHmm");
            effdate = "";
            efftime = "";
            pulldate = "";
            pulltime = "";

            _import_path = PFSUtility.DefaultImportPath();          // ...\load_me
            _log_path = PFSUtility.DefaultLogPath();                // ...\log

            foreach (var arg in args)
            {
                var arr = arg.Split('=');
                value = (arr.GetUpperBound(0) > 0) ? arr[1] : "";

                switch (arr[0])
                {
                    case "-acct":                               // process this patient only
                        _this_acct = value;
                        break;
                        
                    case "-debug":                              // output to console and pause at end
                        g_debug = true;
                        break;

                    case "-effdate":                            // effective date: yyyymmdd
                        if (value == "yesterday")
                            effdate = DateTime.Now.AddDays(-1).ToString("yyyyMMdd");
                        else
                            effdate = value.Left(8);
                        break;
                    
                    case "-efftime":                            // effective time: HHMM or HH
                        efftime = value.Left(4);
                        efftime = efftime.PadRight(4,'0');
                        break;
                    
                    case "-import":                             // run transparent import when done
                        g_import_when_done = true;
                        break;

                    case "-log":                                // create TransparentAudit.log
                        g_log = true;                           // (verbose audit)
                        break;

                    case "-path":                               // override default import path
                        _import_path = value;
                        break;

                    case "-logpath":                               // override default import path
                        _log_path = value;
                        break;
                    case "-n":                                  // no output to transparent.txt 
                        g_no_output = true;                     // (audit only)
                        break;

                    case "-nodelete":                           // do not delete old chart items
                        g_no_delete = true;
                        break;

                    case "-pulldate":                           // pull date: yyyymmdd
                        if (value == "today")
                            pulldate = DateTime.Now.ToString("yyyyMMdd");
                        else
                            pulldate = value.Left(8);
                        break;
                    
                    case "-pulltime":                           // pull time: HHMM or HH
                        pulltime = value.Left(4);
                        pulltime = pulltime.PadRight(4,'0');
                        break;

                    case "-range":                              // range in minutes; default = 1440 = 24hr
                        g_range = value.ToInteger();
                        break;
                        
                    case "-stoponerror":
                        g_stop_on_error = true;
                        break;

                    case "-test":
                        g_is_test = true;
                        break;

                    case "-citest":
                        g_use_all_chart_items = true;
                        break;

                    case "-unit":                               // process this unit only
                        _this_unit_name = value;
                        break;
                    case "-unit_id":                            // process this unit only
                        _this_unit_id = value.ToInteger();
                        break;
                    case "-facs":                               // process these facs only
                        _these_facs = value;
                        break;
                    case "-omitMH":
                        g_do_MH = false;
                        break;
                    case "-omitOW":
                        g_do_OW = false;
                        break;

                    default:
                        Console.WriteLine("unexpected argument: {0}", arg);
                        break;
                }
            }

            if (!String.IsNullOrEmpty(effdate) && !effdate.StartsWith("20"))
            {
                if (_these_facs.StartsWith("6"))
                {
                    DateTime tempdt = PFSUtility.ISOToDateTime(nowdate);
                    if (efftime.StartsWith("23"))
                    {
                        tempdt = tempdt.AddDays(-1);
                        effdate = PFSUtility.DateTimeToISODate(tempdt);
                    }
                    else
                        effdate = nowdate;
                    pulldate = effdate;
                }
            }
            if (String.IsNullOrEmpty(effdate))
                effdate = nowdate;
            if (String.IsNullOrEmpty(efftime))
                efftime = nowtime;

            // Note: pulldate defaults to effdate, not nowdate
            if (String.IsNullOrEmpty(pulldate))
                pulldate = effdate;
            if (String.IsNullOrEmpty(pulltime))
                pulltime = efftime;

            if (g_range == 0)
                g_range = DEFAULT_RANGE;

            DebugTrace("Import path=", _import_path);
            DebugTrace("Log path=", _log_path);
            DebugTrace("effdate={0}", effdate);
            DebugTrace("efftime={0}", efftime);
            DebugTrace("pulldate={0}", pulldate);
            DebugTrace("pulltime={0}", pulltime);
            DebugTrace("range={0}", g_range);

            // class IN time
            g_effdt = PFSUtility.ISOToDateTime(effdate + efftime);
            // range for chart item queries
            g_pull_finish = PFSUtility.ISOToDateTime(pulldate + pulltime);
            g_pull_start = g_pull_finish.AddMinutes(-g_range);

        }

        static void OpenOutputFiles()
        {
            // Append to existing file
            outfile = new StreamWriter(Path.Combine(_import_path, TRANSP_FILENAME), true);

            if (g_log)
            {
                // re-write new file
                logfile = new StreamWriter(Path.Combine(_log_path, DateTime.Now.ToString("yyyyMMddHHmm") + TRANSP_AUDIT_LOG));

            }
        }

        static void CloseOutputFiles()
        {
            outfile.Close();
            
            if (logfile != null) {
                logfile.Close();
                logfile = null;
            }
        }

        static void SetMapperVersion()
        {
            gLogMapperVersion = MAPPER_VERSION;
        }

        static void ProcessPatients()
        {
            string sql, audit_header;
            int count, prev_enc_id = 0;
            //parent_id = 2817
            //'Prod env:
            //vunit_id = 43729784
            //iunit_id = 43730689
            // Make a list of all patients with chart items during the pull period.
            //
            // Include only units that have the "use transparent" flag set.
            // Limit to one patient if -acct is given.  Limit to one unit with -unit.
            // Left join encounter_location - the patient may be discharged at pull time.
            // Look for the latest patient arrival to the unit before the pull time; ignore transfers within the unit.
            // Look for a patient departure (if any) before the pull time; ignore transfers within the unit.

        sql =
            " SELECT LIST.UNIT_ID, LIST.ENCOUNTER_ID, PARAM.METHODOLOGY_ID,\n" +
            "     UNIT.NAME AS UNIT_NAME, EL.ROOM, EL.BED, EL.DATETIME_IN AS BED_ARRIVAL,\n" +
            "     F.CLASSIFICATION_FACILITY_CODE,\n" +
//            "     ARRIVE.DATETIME_IN AS UNIT_ARRIVAL, DEPART.DATETIME_OUT AS UNIT_DEPARTURE,\n" +
            "     ARRIVE.DATETIME_IN AS UNIT_ARRIVAL, ARRIVE.DATETIME_OUT AS UNIT_DEPARTURE,\n" +
            "     E.ACCT_NUMBER, P.LAST_NAME, P.FIRST_NAME, P.MIDDLE_NAME,\n" +
            "     E.AGE_AT_ADMISSION,\n" +
            "     UNIT.IS_ED, UNIT.IS_ICU,PARAM.START_OF_DAY,PARAM.UNIT_PARAM_ID\n" +
            " FROM (\n" +
            "     SELECT DISTINCT CI.UNIT_ID, CI.ENCOUNTER_ID\n" +
            "     FROM CHART_ITEM AS CI\n" +
            "     INNER JOIN UNIT ON (UNIT.UNIT_ID = CI.UNIT_ID)\n" +
            "     WHERE UNIT.USE_TRANSPARENT_CLASSIFICATION='Y' and (UNIT.IS_ED is null or UNIT.IS_ED <> 'Y')\n" +
            //"     AND EVENT_DATETIME BETWEEN " + PFSDBUtility.SQLDateTime(g_pull_start) + " AND " + PFSDBUtility.SQLDateTime(g_pull_finish) +
            " ) AS LIST" +
            " INNER JOIN UNIT ON (UNIT.UNIT_ID = LIST.UNIT_ID)\n" +
            " INNER JOIN UNIT_PARAM AS PARAM ON (PARAM.UNIT_ID = UNIT.UNIT_ID) AND (" + PFSDBUtility.SQLDateTime(g_pull_finish) + " BETWEEN PARAM.EFFECTIVE_DATETIME AND PARAM.EXPIRATION_DATETIME)\n" +
            " INNER JOIN FACILITY AS F ON (F.FACILITY_ID = UNIT.FACILITY_ID)\n" +
            " INNER JOIN ENCOUNTER AS E ON (E.ENCOUNTER_ID = LIST.ENCOUNTER_ID)\n" +
            " INNER JOIN PERSON AS P ON (P.PERSON_ID = E.PERSON_ID)\n" +
            " LEFT  JOIN ENCOUNTER_LOCATION AS EL ON (EL.ENCOUNTER_ID = E.ENCOUNTER_ID) AND (" + PFSDBUtility.SQLDateTime(g_pull_finish) + " BETWEEN EL.EFFECTIVE_DATETIME_IN AND EL.EFFECTIVE_DATETIME_OUT)\n" +
            " INNER JOIN (\n" +
//            "    SELECT WORKING_UNIT_ID AS UNIT_ID, ENCOUNTER_ID, MAX(DATETIME_IN) AS DATETIME_IN\n" +
            "    SELECT WORKING_UNIT_ID AS UNIT_ID, ENCOUNTER_ID, DATETIME_IN, DATETIME_OUT\n" +
            "    FROM ENCOUNTER_LOCATION AS EL\n" +
            "    WHERE WORKING_UNIT_ID IN (SELECT UNIT_ID FROM UNIT WHERE USE_TRANSPARENT_CLASSIFICATION='Y')\n" +
"    AND DATETIME_IN <= " + PFSDBUtility.SQLDateTime(g_pull_finish) +
"    AND (DATETIME_OUT is null or DATETIME_OUT >= " + PFSDBUtility.SQLDateTime(g_pull_start) + ")\n" +
            //            "    AND IS_TRANSFER_IN='Y'\n" +
            //            "    GROUP BY WORKING_UNIT_ID, ENCOUNTER_ID\n" +
            " ) AS ARRIVE ON (ARRIVE.UNIT_ID = UNIT.UNIT_ID) AND (ARRIVE.ENCOUNTER_ID = E.ENCOUNTER_ID)\n" +
            //" LEFT JOIN (\n" +
            //"    SELECT WORKING_UNIT_ID AS UNIT_ID, ENCOUNTER_ID, MAX(DATETIME_OUT) AS DATETIME_OUT\n" +
            //"    FROM ENCOUNTER_LOCATION AS EL\n" +
            //"    WHERE WORKING_UNIT_ID IN (SELECT UNIT_ID FROM UNIT WHERE USE_TRANSPARENT_CLASSIFICATION='Y')\n" +
            //"    AND DATETIME_OUT < " + PFSDBUtility.SQLDateTime(g_pull_finish) +
            //"    AND ((NEXT_WORKING_UNIT_ID<>WORKING_UNIT_ID) OR (NEXT_WORKING_UNIT_ID IS NULL))\n" +
            //"    GROUP BY WORKING_UNIT_ID, ENCOUNTER_ID\n" +
            //" ) AS DEPART ON (DEPART.UNIT_ID = UNIT.UNIT_ID) AND (DEPART.ENCOUNTER_ID = E.ENCOUNTER_ID)\n" +
            " WHERE (1=1)\n";

            if (!String.IsNullOrEmpty(_these_facs))
            {
                sql += " AND SUBSTRING(F.CLASSIFICATION_FACILITY_CODE,1,1) in (" + _these_facs + ")\n";
            }
            //AND SUBSTRING(F.CLASSIFICATION_FACILITY_CODE, 1, 1) in (2,5)

            if (!String.IsNullOrEmpty(_this_acct)) {
                sql += " AND E.ACCT_NUMBER=" + PFSDBUtility.SQLString(_this_acct) + "\n";
            }
            if (!String.IsNullOrEmpty(_this_unit_name)) {
                sql += " AND UNIT.NAME=" + PFSDBUtility.SQLString(_this_unit_name) + "\n";
            }
            if (_this_unit_id > 0) {
                sql += " AND UNIT.UNIT_ID=" + _this_unit_id + "\n";
            }
            sql += " ORDER BY E.ACCT_NUMBER";
            //sql += " ORDER BY UNIT.NAME, P.LAST_NAME, P.FIRST_NAME, E.ACCT_NUMBER, UNIT_ARRIVAL\n";

            VerboseAudit(sql);
            var db = PFSDBUtility.NewSqlConnection();
            var cmd = new SqlCommand(sql, db);
            cmd.CommandTimeout = 180;
            SqlDataReader dr = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);

            //
            // Process all patients
            //
            count = 0;
            var default_inds = new List<int>();
            //var default_inds_str = "";
            //var default_ptype = 0;
            patloclist = new List<PatientLocation>();

            while (dr.Read())
            {
                count++;
                Console.Write("\rProcessing patient {0}", count);

                var pat = new PatientInfo();
                pat.unit_id = PFSDBUtility.DBToInt(dr["UNIT_ID"]);
                pat.encounter_id = PFSDBUtility.DBToInt(dr["ENCOUNTER_ID"]);
                pat.meth_id = PFSDBUtility.DBToInt(dr["METHODOLOGY_ID"]);
                pat.acct = PFSDBUtility.DBToString(dr["ACCT_NUMBER"]);
                pat.last_name = PFSDBUtility.DBToString(dr["LAST_NAME"]);
                pat.first_name = PFSDBUtility.DBToString(dr["FIRST_NAME"]);
                pat.middle_name = PFSDBUtility.DBToString(dr["MIDDLE_NAME"]);
                pat.facilty_code = PFSDBUtility.DBToString(dr["CLASSIFICATION_FACILITY_CODE"]);
                pat.unit_name = PFSDBUtility.DBToString(dr["UNIT_NAME"]);
                pat.room = PFSDBUtility.DBToString(dr["ROOM"]);
                pat.bed = PFSDBUtility.DBToString(dr["BED"]);
                pat.age = PFSDBUtility.DBToDouble(dr["AGE_AT_ADMISSION"]);
                pat.unit_arrival = PFSDBUtility.DBToDateTime(dr["UNIT_ARRIVAL"]);
                pat.unit_departure = PFSDBUtility.DBToDateTime(dr["UNIT_DEPARTURE"]);
                Program.VerboseAudit("pat.unit_departure=" + pat.unit_departure.ToString() + "  pat.unit_arrival=" + pat.unit_arrival.ToString());
                pat.effective = PFSUtility.MaxDateTime(g_effdt, pat.unit_arrival);
                pat.pull_start = PFSUtility.MaxDateTime(g_pull_start, pat.unit_arrival);
                pat.sod = PFSDBUtility.DBToDateTime(dr["START_OF_DAY"]);
                pat.upid = PFSDBUtility.DBToInt(dr["UNIT_PARAM_ID"]);
                VerboseAudit("pat.pull_start=" + pat.pull_start+ " max of g_pull_start=" + g_pull_start + " and pat.unit_arrival=" + pat.unit_arrival);
                if (dr["UNIT_DEPARTURE"] == null)
                    pat.pull_finish = g_pull_finish;
                else
                    pat.pull_finish = PFSUtility.MinDateTime(g_pull_finish, pat.unit_departure);
                if (pat.pull_finish < pat.pull_start)
                {
                    pat.pull_finish = g_pull_finish;
                }
                //pat.range = (int)PFSUtility.DateDiffInMinutes(pat.pull_start, pat.pull_finish);
                //                pat.range = (int)PFSUtility.DateDiffInMinutes(pat.unit_arrival, pat.unit_departure);
                //                pat.range = (int)PFSUtility.DateDiffInMinutes(pat.pull_start, pat.pull_finish);

                //At 1100.Process 0300 - 1100.Time the classification for 0700
                //At 1500.Process 0700 - 1500.Time the classification for 1100
                //At 1900.Process 1100 - 1900.Time the classification for 1500
                //At 2300.Process 1500 - 2300.Time the classification for 1900
                //At 0300.Process 1900 - 0300.Time the classification for 2300
                //At 0700.Process 2300 - 0700.Time the classification for 0300 and duplicate for 0700.  (The 0700 will be over - written for the 1100 run)


                bool do_class = false;
                if (prev_enc_id != pat.encounter_id)
                {
                    VerboseAudit("prev_enc_id=" + prev_enc_id + "  pat.encounter_id=" + pat.encounter_id);
                    prev_enc_id = pat.encounter_id;
                    patloclist = GetPatientLocations(pat.encounter_id, g_pull_start, g_pull_finish, out pat.pull_start, out pat.pull_finish);
                    patperioplist = GetPeriopLocations(pat.encounter_id, g_pull_start, g_pull_finish);
                    pat.is_isolation = (GetIsolationPV2(pat.encounter_id, g_pull_finish) > 0);
                    foreach (var ploc in patloclist)
                    {
                        do_class = false;
                        Audit("unitid=" + ploc.unit_id + " start= " + ploc.in_time + " end=" + ploc.out_time);
                        //if (ploc.out_time < pat.pull_finish) pat.pull_finish = ploc.out_time;
                        //if (ploc.out_time == g_pull_finish || (pat.unit_departure <= g_pull_finish  && pat.unit_departure >= g_pull_finish.AddHours(-4)))
                        {

                            if (ploc.in_time > pat.pull_start) pat.pull_start = ploc.in_time;
                            if (ploc.out_time > pat.pull_finish) pat.pull_finish = ploc.out_time;
                            Audit("start=" + pat.pull_start + " finish= " + pat.pull_finish);
                            pat.unit_id = ploc.unit_id;
                            pat.unit_arrival = ploc.arr_time;
                            pat.unit_departure = ploc.out_time;
                            Program.VerboseAudit("pat.unit_departure=" + pat.unit_departure.ToString() + "  g_pull_finish=" + g_pull_finish.ToString());

                            if (g_pull_finish.Hour == 7)
                            {
                                do_class = true;
                                if (pat.unit_departure == DateTime.MinValue)
                                {
                                    if (pat.unit_arrival < g_pull_finish.AddHours(-2 * 4))
                                    {
                                        pat.effective = g_pull_finish.AddHours(-4);
                                        pat.effective_out = g_pull_finish;
                                        pat.range = g_range;   //4 * 60;
                                        Program.VerboseAudit("A: eff=" + pat.effective.ToString() + " effout=" + pat.effective_out.ToString());
                                    }
                                    else if (pat.unit_arrival < g_pull_finish)
                                    {
                                        pat.effective = pat.unit_arrival;
                                        pat.range = (int)PFSUtility.DateDiffInMinutes(pat.unit_arrival, g_pull_finish);
                                        pat.effective_out = g_pull_finish;
                                        Program.VerboseAudit("B: eff=" + pat.effective.ToString() + " effout=" + pat.effective_out.ToString());
                                    }
                                }
                                else if (pat.unit_departure >= g_pull_finish.AddHours(-4) && pat.unit_departure <= g_pull_finish)
                                {
                                    if (pat.unit_arrival < g_pull_finish.AddHours(-2 * 4))
                                    {
                                        pat.effective = g_pull_finish.AddHours(-4);
                                        pat.effective_out = g_pull_finish;
                                        pat.range = g_range;   //4 * 60;
                                        Program.VerboseAudit("C2: eff=" + pat.effective.ToString() + " effout=" + pat.effective_out.ToString());
                                    }
                                    else
                                    {
                                        pat.effective = pat.unit_arrival;
                                        pat.range = (int)PFSUtility.DateDiffInMinutes(pat.unit_arrival, pat.unit_departure);
                                        pat.effective_out = pat.pull_finish; // pat.unit_departure;
                                        Program.VerboseAudit("C: eff=" + pat.effective.ToString() + " effout=" + pat.effective_out.ToString());
                                    }
                                }
                                else if (pat.unit_departure >= g_pull_finish.AddHours(-4) && pat.unit_departure > g_pull_finish)
                                {
                                    if (pat.unit_arrival < g_pull_finish.AddHours(-2 * 4))
                                    {
                                        pat.effective = g_pull_finish.AddHours(-4);
                                        pat.range = g_range;
                                        pat.effective_out = g_pull_finish;
                                        Program.VerboseAudit("D2: eff=" + pat.effective.ToString() + " effout=" + pat.effective_out.ToString());
                                    }
                                    else
                                    {
                                        pat.effective = pat.unit_arrival;
                                        pat.range = (int)PFSUtility.DateDiffInMinutes(pat.unit_arrival, g_pull_finish);
                                        pat.effective_out = g_pull_finish;
                                        Program.VerboseAudit("D: eff=" + pat.effective.ToString() + " effout=" + pat.effective_out.ToString());

                                    }
                                }
                                else
                                {
                                    do_class = false;
                                    Program.VerboseAudit("X: eff=" + pat.effective.ToString() + " effout=" + pat.effective_out.ToString());
                                }
                            }
                            else
                            {
                                if (pat.unit_departure == DateTime.MinValue)
                                {
                                    Program.VerboseAudit("checkpoint 2");
                                    //don't classify if only on unit LT 2 hours
                                    if (pat.pull_start <= g_pull_finish.AddHours(-2))
                                    {
                                        Program.VerboseAudit("checkpoint 2a");
                                        if (pat.unit_arrival < g_pull_finish.AddHours(-4))
                                        {
                                            Program.VerboseAudit("checkpoint 2b");
                                            if (pat.unit_arrival >= g_pull_finish.AddHours(-8))
                                            {
                                                pat.effective = pat.unit_arrival;
                                                g_range = (int)PFSUtility.DateDiffInMinutes(pat.unit_arrival, g_pull_finish);
                                            }
                                            else
                                            {
                                                pat.effective = g_pull_finish.AddHours(-4);
                                            }
                                            pat.range = g_range;
                                            pat.effective_out = g_pull_finish;
                                            do_class = true;
                                            Program.VerboseAudit("E: eff=" + pat.effective.ToString() + " effout=" + pat.effective_out.ToString());
                                        }
                                        else
                                        {
                                            pat.effective = pat.unit_arrival;
                                            pat.range = g_range; // 4 * 60;
                                            pat.effective_out = g_pull_finish;
                                            do_class = true;
                                            Program.VerboseAudit("F: eff=" + pat.effective.ToString() + " effout=" + pat.effective_out.ToString());
                                        }
                                    }
                                }
                                else if (pat.unit_departure >= g_pull_finish.AddHours(-8))
                                {
                                    Program.VerboseAudit("checkpoint 3");
                                    if (pat.unit_arrival < g_pull_finish.AddHours(-2 * 4))
                                    {
                                        Program.VerboseAudit("checkpoint 3a");
                                        //pat.effective = g_pull_finish.AddHours(-4);
                                        pat.effective = PFSUtility.MaxDateTime(g_pull_finish.AddHours(-4), ploc.in_time);
                                        Program.VerboseAudit("3a: pat.effective=" + pat.effective + "  ploc.in_time=" + ploc.in_time);
                                        //pat.range = (int)PFSUtility.DateDiffInMinutes(g_pull_finish.AddHours(-4), pat.unit_departure);
                                        pat.range = 480;// (int)PFSUtility.DateDiffInMinutes(pat.unit_departure.AddHours(-4), pat.unit_departure);

                                        ////pat.effective = g_pull_finish.AddHours(-4);
                                        //Program.VerboseAudit("G: ploc.in_time=" + ploc.in_time + " ploc.out_time=" + ploc.out_time);
                                        //Program.VerboseAudit("G: g_pull_finish=" + g_pull_finish);

                                        //if (ploc.out_time < g_pull_finish.AddHours(-4))
                                        //    pat.effective = ploc.in_time;
                                        //else
                                        //    pat.effective = PFSUtility.MaxDateTime(g_pull_finish.AddHours(-g_range), ploc.in_time);
                                        ////pat.range = (int)PFSUtility.DateDiffInMinutes(g_pull_finish.AddHours(-4), pat.unit_departure);
                                        //pat.range = (int)PFSUtility.DateDiffInMinutes(pat.unit_departure.AddMinutes(-4), pat.unit_departure);


                                        pat.effective_out = pat.unit_departure;
                                        if (pat.pull_finish == g_pull_finish)
                                        {
                                            do_class = true;
                                            pat.unit_departure = DateTime.MinValue;
                                            pat.effective_out = g_pull_finish;
                                            pat.range = (int)PFSUtility.DateDiffInMinutes(pat.effective, pat.effective_out);
                                            Program.VerboseAudit("Gb: eff=" + pat.effective.ToString() + " effout=" + pat.effective_out.ToString());
                                            if (ploc.out_time <= pat.effective)
                                            {
                                                do_class = false;
                                                VerboseAudit("Omitting classification: loc out time is less than effective time.");
                                            }
                                        }
                                        if (pat.unit_departure > pat.effective)
                                        {
                                            do_class = true;
                                            Program.VerboseAudit("G: eff=" + pat.effective.ToString() + " effout=" + pat.effective_out.ToString());
                                        }
                                    }
                                    else if (pat.unit_arrival <= g_pull_finish.AddHours(-2))
                                    {
                                        var tm = TimeSpan.Parse("07:00:00");
                                        //pat.effective = pat.unit_arrival;
                                        pat.effective = PFSUtility.MaxDateTime(pat.unit_arrival, ploc.in_time);
                                        Program.VerboseAudit("H: pat.effective=" + pat.effective.ToString() + " pat.unit_arrival=" + pat.unit_arrival.ToString() + "  ploc.in_time=" + ploc.in_time);
                                        Program.VerboseAudit("H: pat.pull_finish=" + pat.pull_finish.ToString() + " g_pull_finish=" + g_pull_finish.ToString());
                                        pat.range = (int)PFSUtility.DateDiffInMinutes(pat.unit_arrival, pat.unit_departure);
                                        pat.effective_out = pat.unit_departure;
                                        Program.VerboseAudit("H: pat.effective_out=" + pat.effective_out.ToString());
                                        if (pat.pull_finish == g_pull_finish)
                                        {
                                            Program.VerboseAudit("H1: pat.effective_out=" + pat.effective_out.ToString());
                                            Program.VerboseAudit("H1: pat.unit_departure=" + pat.unit_departure.ToString() + "  g_pull_finish=" + g_pull_finish.ToString());
                                            Program.VerboseAudit("H1: pat.range=" + pat.range);
                                            pat.unit_departure = DateTime.MinValue;
                                            pat.effective_out = g_pull_finish;
                                            if (pat.effective.Date == pat.effective_out.Date && pat.effective.Hour < 7 && pat.effective_out.Hour >= 7)
                                                pat.effective = pat.effective.Date + tm;
                                            pat.range = (int)PFSUtility.DateDiffInMinutes(pat.effective, pat.effective_out);
                                            Program.VerboseAudit("H2: pat.effective_out=" + pat.effective_out.ToString());
                                            Program.VerboseAudit("H2: pat.unit_departure=" + pat.unit_departure.ToString());
                                            Program.VerboseAudit("H2: pat.range=" + pat.range);
                                        }
                                        do_class = true;
                                        Program.VerboseAudit("H: eff=" + pat.effective.ToString() + " effout=" + pat.effective_out.ToString());
                                    }

                                }

                            }

                            pat.los_hours = (int)PFSUtility.DateDiffInMinutes(pat.pull_start, pat.pull_finish) / 60.0;//pat.range / 60.0;
                            pat.TC_source_id = 5399; // PFSDBUtility.DBToInt(dr["TC_SOURCE_ID"]);        //TCP port
                            pat.is_ED = PFSDBUtility.DBToBool(dr["IS_ED"]);
                            pat.is_ICU = PFSDBUtility.DBToBool(dr["IS_ICU"]);
                            // Package the patient info
                            // NOTE: ''dr["FIRST_NAME"] as string'' looks great but it will save nulls.
                            //       use ''PFSDBUtility.DBToString(dr["FIRST_NAME"])'' to convert to empty strings.

                            // Get the list of default indicators on admission (if any)
                            //if (pat.unit_id != prev_unit_id)
                            //{
                            //    default_inds = GetUnitDefaultIndicators(pat.unit_id, g_pull_start, out default_inds_str, out default_ptype);
                            //    prev_unit_id = pat.unit_id;
                            //}
                            //pat.default_inds = default_inds;
                            //pat.default_inds_str = default_inds_str;
                            //pat.default_ptype = default_ptype;

                            // Get ready for event log entries for this patient
                            gLogUnitID = pat.unit_id;
                            gLogEncounterID = pat.encounter_id;
                            gLogSourceText = "";

                            // Reset both audit strings and make headers for both
                            gBriefAudit = new StringBuilder();
                            gVerboseAudit = new StringBuilder();
                            audit_header =
                                new String('=', 80) + Environment.NewLine +
                                "TRANSPARENT MAPPING AUDIT FILE                    Run Time=" + DateTime.Now + Environment.NewLine +
                                "Version:   " + gLogMapperVersion + Environment.NewLine +
                                "Pull:      " + g_pull_finish + Environment.NewLine +
                                "Effective: " + g_effdt + Environment.NewLine +
                                "Range:     " + g_range + " (" + Math.Round(g_range / 60.0, 1) + " hrs) starting " + g_pull_start;

                            // This will add to both brief and verbose audits
                            Audit(audit_header);
                            Audit(new String('=', 80));
                            Audit("Patient " + pat.last_name + ", " + pat.first_name + " " + pat.middle_name +
                                " in " + ploc.unit_name + " " + ploc.room + " " + ploc.bed +
                                " acct=" + pat.acct + " age=" + Math.Round(pat.age, 2));
                            //                Audit("Here from " + pat.effective + " to " + pat.effective.AddMinutes(pat.range) +
                            Audit("Here from " + pat.effective + " to " + pat.effective_out +
                                "  (.LOS=" + Math.Round(pat.los_hours, 2) + ")");
                            VerboseAudit("pat.pull_start=" + pat.pull_start.ToString() + " pat.pull_finish=" + pat.pull_finish.ToString());

                            //Audit("Here from " + pat.pull_start + " to " + pat.pull_finish +
                            //    "  (LOS=" + Math.Round(pat.los_hours, 2) + ")");
                            //} // prev_enc_id != pat.encounter_id
                            if (ploc.special_unit_id < 0 && ploc.is_last_loc)
                            {
                                do_class = false;
                                VerboseAudit("Omitting classification for this location: is special+is current.");
                            }

                            if (do_class)
                            {
                                pat.unit_name = ploc.unit_name;
                                pat.meth_id = ploc.methid;
                                pat.loc_idx = ploc.loc_idx;
                                try
                                {
                                    // Run the appropriate mapping for this unit and date
                                    //
                                    switch (ploc.methid)
                                    {
                                        case 18:
                                        case 20:
                                            var ev = new Inpatient();
                                            ev.ProcessPatient(pat);
                                            break;
                                        case 26:
                                            if (g_do_MH)
                                            {
                                                var emh = new MentalHealth();
                                                emh.ProcessPatient(pat);
                                            }
                                            break;
                                        default:
                                            LogWarning("Methodology " + pat.meth_id + " in unit " + pat.unit_name + " is not supported");
                                            break;
                                    }

                                    gLogUnitID = 0;
                                    gLogEncounterID = 0;
                                }
                                catch (Exception e)
                                {
                                    LogUnexpectedError(e.Message, e.StackTrace);
                                    // Stop or keep going?
                                    if (Program.g_stop_on_error) g_abort = true;
                                }
                            }
                            if (g_abort) break;
                        }
                    } //foreach
                }
            }

            Console.WriteLine();
            dr.Close();
            
            if (count < 1) {
                Audit("");
                if(!String.IsNullOrEmpty(_this_acct)) {
                    LogWarning("The selected patient has no chart items in the given time range");
                } else {
                    LogWarning("No chart items found to process - have the unit(s) been enabled for transparent classification?");
                }
            }   
        }

        static List<PatientLocation> GetPatientLocations(int encid, DateTime start_dt, DateTime end_dt, out DateTime patstart, out DateTime patfinish)
        {
            var result = new List<PatientLocation>();                   // make an empty list
            var db = PFSDBUtility.NewPfsDataContext();        // look for the unit default admission profile

            string sql="";
            sql = "select el.unit_id,el.effective_datetime_in,el.effective_datetime_out,el.room,el.bed,el.special_unit_id,u.name,up.methodology_id from encounter_location as el";
            sql += " inner join unit as u on (el.unit_id = u.unit_id)";
            sql += " left join unit_param as up on(u.unit_id = up.unit_id)";
            sql += " where el.encounter_id=" + encid;
            //sql += " and (el.special_unit_id is null or (el.special_unit_id is not null))";// and el.special_unit_id<>-6))";
            sql += " and getdate() between up.EFFECTIVE_DATETIME and up.EXPIRATION_DATETIME";
            sql += " and ( (('" + start_dt.ToString() + "'>=el.effective_datetime_in) and ('" + start_dt.ToString() + "'<=el.effective_datetime_out))";
            sql += " or (('" + end_dt.ToString() + "'>=el.effective_datetime_in) and ('" + end_dt.ToString() + "'<=el.effective_datetime_out))";
            sql += " or (('" + start_dt.ToString() + "'<el.effective_datetime_in) and ('" + end_dt.ToString() + "'>=el.effective_datetime_out)) )";
            sql += " order by el.effective_datetime_in";
            VerboseAudit("q1:"+sql);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd = new SqlCommand(sql, db2);
            SqlDataReader dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            VerboseAudit("start_dt=" + start_dt + "  end_dt=" + end_dt);
            while (dr2.Read())
            {
                var ptloc = new PatientLocation();
                ptloc.unit_id = PFSDBUtility.DBToInt(dr2["UNIT_ID"]);
                ptloc.unit_name = PFSDBUtility.DBToString(dr2["NAME"]);
                ptloc.room = PFSDBUtility.DBToString(dr2["ROOM"]);
                ptloc.bed = PFSDBUtility.DBToString(dr2["BED"]);
                ptloc.special_unit_id = PFSDBUtility.DBToInt(dr2["SPECIAL_UNIT_ID"]);
                if (dr2["METHODOLOGY_ID"] == null)
                    ptloc.methid = 0;
                else
                    ptloc.methid=PFSDBUtility.DBToInt(dr2["METHODOLOGY_ID"]);
                DateTime effin = PFSDBUtility.DBToDateTime(dr2["EFFECTIVE_DATETIME_IN"]);
                DateTime effout = PFSDBUtility.DBToDateTime(dr2["EFFECTIVE_DATETIME_OUT"]);

                VerboseAudit("effin=" + effin + "  effout=" + effout);

                if (effin <= start_dt)
                    ptloc.los_start = start_dt;
                else
                    ptloc.los_start = effin;

                if (effout <= end_dt)
                    ptloc.los_end = effout;
                else
                    ptloc.los_end = end_dt;



                if ((start_dt >= effin) && (start_dt < effout))
                {
                    ptloc.in_time = start_dt;
                    ptloc.loc_start = effin;
                }
                else
                {
                    ptloc.in_time = effin;
                    ptloc.loc_start = effin;
                }
                if ((end_dt >= effin) && (end_dt <= effout))
                {
                    ptloc.out_time = end_dt;
                }
                else
                {
                    ptloc.out_time = effout;
                    if (ptloc.out_time > end_dt) ptloc.out_time = end_dt;
                }
                if (ptloc.methid == 20 || ptloc.methid == 26)
                {
                    Audit("loc " + ptloc.unit_name + "  in= " + ptloc.in_time + "  out= " + ptloc.out_time + "  meth=" + ptloc.methid);
                    result.Add(ptloc);
                }
            }

        dr2.Close();


            //var query = from loc in db.ENCOUNTER_LOCATIONs
            //            join u in db.UNITs on loc.UNIT_ID equals u.UNIT_ID
            //            join up in db.UNIT_PARAMs on u.UNIT_ID equals up.UNIT_ID into xup
            //            from subup in xup.DefaultIfEmpty()
            //            where (loc.ENCOUNTER_ID == encid) && (u.UNIT_ID >= -1)
            //            && (g_pull_start >= subup.EFFECTIVE_DATETIME) && (g_pull_start < subup.EXPIRATION_DATETIME)
            //            && (
            //                ((start_dt >= loc.EFFECTIVE_DATETIME_IN) && (start_dt <= loc.EFFECTIVE_DATETIME_OUT))
            //                || ((end_dt >= loc.EFFECTIVE_DATETIME_IN) && (end_dt <= loc.EFFECTIVE_DATETIME_OUT))
            //                || ((start_dt < loc.EFFECTIVE_DATETIME_IN) && (end_dt >= loc.EFFECTIVE_DATETIME_OUT))
            //                )
            //            orderby loc.EFFECTIVE_DATETIME_IN
            //            select new
            //            {
            //                loc.UNIT_ID,
            //                //loc.ROOM,
            //                //loc.HOSPITAL_SERVICE_LVC,
            //                loc.EFFECTIVE_DATETIME_IN,
            //                loc.EFFECTIVE_DATETIME_OUT,
            //                u.NAME,
            //                subup.METHODOLOGY_ID
            //            };
            //foreach (var locitem in query)
            //{
            //    var ptloc = new PatientLocation();

            //    ptloc.unit_id = (int)locitem.UNIT_ID;
            //    ptloc.unit_name = (string)locitem.NAME;
            //    //ptloc.room = (string)locitem.ROOM;
            //    //ptloc.service = (string)locitem.HOSPITAL_SERVICE_LVC;
            //    if (locitem.METHODOLOGY_ID != null)
            //        ptloc.methid = (int)locitem.METHODOLOGY_ID;
            //    else
            //        ptloc.methid = 0;
            //    if ((start_dt >= (DateTime)locitem.EFFECTIVE_DATETIME_IN) && (start_dt < (DateTime)locitem.EFFECTIVE_DATETIME_OUT))
            //    {
            //        ptloc.in_time = start_dt;
            //        ptloc.loc_start = (DateTime)locitem.EFFECTIVE_DATETIME_IN;
            //    }
            //    else
            //    {
            //        ptloc.in_time = (DateTime)locitem.EFFECTIVE_DATETIME_IN;
            //        ptloc.loc_start = (DateTime)locitem.EFFECTIVE_DATETIME_IN;
            //    }
            //    if ((end_dt >= (DateTime)locitem.EFFECTIVE_DATETIME_IN) && (end_dt <= (DateTime)locitem.EFFECTIVE_DATETIME_OUT))
            //    {
            //        ptloc.out_time = end_dt;
            //    }
            //    else
            //    {
            //        ptloc.out_time = (DateTime)locitem.EFFECTIVE_DATETIME_OUT;
            //        if (ptloc.out_time > end_dt) ptloc.out_time = end_dt;
            //    }
            //    Audit("loc " + ptloc.unit_name + "  in= " + ptloc.in_time + "  out= " + ptloc.out_time + "  meth=" + ptloc.methid);

            //    result.Add(ptloc);
            //}
            PatientLocation[] locary = result.ToArray();
            int num_locs = 0;
            //  where there are adjacent same-locations, mark the duplicates as remove.
            for (int i = 0; i <= locary.GetUpperBound(0); i++)
            {
                num_locs++;
                locary[i].remove = false;
                if (i >= 1)
                {
                    if ((locary[i - 1].unit_id == locary[i].unit_id)
                        && (locary[i-1].special_unit_id != -6)
                        && (locary[i].special_unit_id != -6)
                        && (locary[i - 1].out_time == locary[i].in_time))
                        //&& (locary[i - 1].service == locary[i].service))
                    {
                        locary[i].remove = true;
                    }
                }
            }
            //  where there are several adjacent same-unit locations, need to combine them into 1 loc record.
            //  make the outdt of that record be the latest outdt
            int lastgoodidx = 0;
            int lastadj = 0;
            for (int i = 0; i <= locary.GetUpperBound(0); i++)
            {
                if (!locary[i].remove)
                {
                    lastgoodidx = i;
                    lastadj = i;
                }
                else
                {
                    if (lastadj == i - 1)
                    {
                        locary[lastgoodidx].out_time = locary[i].out_time;
                        lastadj = i;
                    }
                }
            }
            // get rid of the removes
            int numkeep = 0;
            for (int i = 0; i <= locary.GetUpperBound(0); i++)
            {
                //Audit("i=" + i + " remove=" + (locary[i].remove ? "true" : "false") + " unit_id=" + locary[i].unit_id + " intime=" + locary[i].in_time);
                if (!locary[i].remove)
                {
                    numkeep++;
                    locary[numkeep - 1] = locary[i]; //-1 for 0-based ary                
                }
            }
            if (numkeep < num_locs)
            {
                Array.Resize(ref locary, numkeep);
            }
            //now determine los_mins
            for (int i = 0; i <= locary.GetUpperBound(0); i++)
            {
                locary[i].los_mins = (int)PFSUtility.DateDiffInMinutes(locary[i].in_time, locary[i].out_time);
            }
            //    num_removed = 0
            //    'where there are adjacent same-locations, mark the duplicates as remove.
            //    For i = 1 To pat.num_loc
            //        pat.loc(i).remove = False
            //        If i >= 2 Then
            //        If pat.loc(i - 1).UnitID = pat.loc(i).UnitID Then
            //            pat.loc(i).remove = True
            //            num_removed = num_removed + 1
            //'            dvprint "Remove: " & i
            //        End If
            //        End If
            //    Next i

            //    'where there are several adjacent same-unit locations, need to combine them into 1 loc record.
            //    '  make the outdt of that record be the latest outdt
            //'1011  --    0731  1:1/1
            //'-1    0731  1301  2:2/'911   1301  1345  3: 3/3
            //'911   1345  ++    R  4: 3/3  3=4

            //    If pat.num_loc > 1 Then
            //        For i = 1 To pat.num_loc
            //            If Not pat.loc(i).remove Then
            //                lastgoodidx = i
            //                lastadj = i
            //            End If
            //            If i > 1 Then
            //                If pat.loc(i).remove Then
            //                    If lastadj = i - 1 Then
            //                        pat.loc(lastgoodidx).outdt = pat.loc(i).outdt
            //                        lastadj = i
            //'                        dvprint "change outdt of: " & lastgoodidx & " to " & i & ": " & g_dbutil.SQL_DateTime(pat.loc(i).outdt)
            //                    End If
            //                End If
            //            End If
            //        Next i
            //    End If

            //    num_done = 0
            //    'Finally remove all the duplicates (remove=true)
            //    '1=1,2=2,3=3
            //    For i = 1 To pat.num_loc
            //        If Not pat.loc(i).remove Then
            //            num_done = num_done + 1
            //            pat.loc(num_done) = pat.loc(i)
            //' dvprint pat.loc(num_done).UnitID & ": " & g_dbutil.SQL_DateTime(pat.loc(num_done).indt) & g_dbutil.SQL_DateTime(pat.loc(num_done).outdt)
            //        End If
            //    Next i
            //    pat.num_loc = pat.num_loc - num_removed

            // Now Get Unit Arrival Times
            patstart = g_pull_start;
            patfinish = g_pull_finish;
            for (int i = 0; i <= locary.GetUpperBound(0); i++)
            {
                locary[i].is_last_loc = false;
                locary[i].loc_idx = i;
                locary[i].arr_time = GetArrivalTime(encid, locary[i].unit_id, locary[i].in_time);
                if (i == 0) patstart = locary[i].in_time;
                if (i == locary.GetUpperBound(0))
                {
                    VerboseAudit("Set patfinish to " + locary[i].out_time);
                    patfinish = locary[i].out_time;
                }
                if (i == locary.GetUpperBound(0)) locary[i].is_last_loc = true;
                Audit("i=" + i + " remove=" + (locary[i].remove ? "true" : "false") + " unit_id=" + locary[i].unit_id + " intime=" + locary[i].in_time + " outtime=" + locary[i].out_time + " specialunitid="+locary[i].special_unit_id);
            }
            result = locary.ToList();
            return result;
        }

        static List<PatientLocation> GetPeriopLocations(int encid, DateTime start_dt, DateTime end_dt)
        {
            var result = new List<PatientLocation>();                   // make an empty list
            var db = PFSDBUtility.NewPfsDataContext();        // look for the unit default admission profile

            string sql = "";
            sql = "select el.unit_id,el.effective_datetime_in,el.effective_datetime_out,u.name from encounter_location as el";
            sql += " inner join unit as u on(el.unit_id = u.unit_id)";
            sql += " where el.special_unit_id in (-1,-5,-6) and el.encounter_id=" + encid;
            sql += " and ( (('" + start_dt.ToString() + "'>=el.effective_datetime_in) and ('" + start_dt.ToString() + "'<=el.effective_datetime_out))";
            sql += " or (('" + end_dt.ToString() + "'>=el.effective_datetime_in) and ('" + end_dt.ToString() + "'<=el.effective_datetime_out))";
            sql += " or (('" + start_dt.ToString() + "'<el.effective_datetime_in) and ('" + end_dt.ToString() + "'>=el.effective_datetime_out)) )";
            sql += " order by el.effective_datetime_in";
            VerboseAudit("q2:"+sql);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd = new SqlCommand(sql, db2);
            SqlDataReader dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr2.Read())
            {
                var ptloc = new PatientLocation();
                ptloc.unit_id = PFSDBUtility.DBToInt(dr2["UNIT_ID"]);
                ptloc.unit_name = PFSDBUtility.DBToString(dr2["NAME"]);
                DateTime effin = PFSDBUtility.DBToDateTime(dr2["EFFECTIVE_DATETIME_IN"]);
                DateTime effout = PFSDBUtility.DBToDateTime(dr2["EFFECTIVE_DATETIME_OUT"]);

                if ((start_dt >= effin) && (start_dt < effout))
                {
                    ptloc.in_time = start_dt;
                    ptloc.loc_start = effin;
                }
                else
                {
                    ptloc.in_time = effin;
                    ptloc.loc_start = effin;
                }
                if ((end_dt >= effin) && (end_dt <= effout))
                {
                    ptloc.out_time = end_dt;
                }
                else
                {
                    ptloc.out_time = effout;
                    if (ptloc.out_time > end_dt) ptloc.out_time = end_dt;
                }
                Audit("PERIOP loc " + ptloc.unit_name + "  in= " + ptloc.in_time + "  out= " + ptloc.out_time);
                result.Add(ptloc);
            }

            dr2.Close();

            PatientLocation[] locary = result.ToArray();
            result = locary.ToList();
            return result;
        }

        static int GetIsolationPV2(int encid, DateTime end_dt)
        {
            int msgcount = 0;
            var db = PFSDBUtility.NewPfsDataContext();        // look for the unit default admission profile

            string sql = "";
            sql = "select count(*) as ct from event_log where encounter_id=" + encid;
            sql += " and timestamp between '" + end_dt.AddHours(-24).ToString() + "' and '" + end_dt.ToString() + "'";
            sql += " and (source_text like '%evn|a0%pv2%airborne%' or ";
            sql += " source_text like '%evn|a0%pv2%contact%' or ";
            sql += " source_text like '%evn|a0%pv2%droplet%')";

            VerboseAudit("isoq:" + sql);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd = new SqlCommand(sql, db2);
            SqlDataReader dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr2.Read())
            {
                msgcount= PFSDBUtility.DBToInt(dr2["CT"]);
            }
            dr2.Close();

            return msgcount;
        }

        static DateTime GetArrivalTime(int encid, int unitid, DateTime intime)
        {
            DateTime arrtime = DateTime.MinValue;
            var db = PFSDBUtility.NewPfsDataContext();        // look for the unit default admission profile
            var query = from loc in db.ENCOUNTER_LOCATIONs
                        where (loc.UNIT_ID == unitid)
                        && (loc.ENCOUNTER_ID == encid)
                        && (loc.EFFECTIVE_DATETIME_IN <= intime)
                        orderby loc.EFFECTIVE_DATETIME_IN
                        select new
                        {
                            loc.UNIT_ID,
                            loc.EFFECTIVE_DATETIME_IN,
                            loc.EFFECTIVE_DATETIME_OUT
                        };

            // example: unit a is the target, but the query returned b as the first unit
            // loc b
            //     a
            //     a
            //     c
            bool first = true;
            DateTime prev_in = DateTime.MinValue;
            DateTime prev_out = DateTime.MinValue;
            foreach (var locitem in query)
            {
                if (first)
                {
                    first = false;
                    prev_in = (DateTime)locitem.EFFECTIVE_DATETIME_IN;
                    prev_out = (DateTime)locitem.EFFECTIVE_DATETIME_OUT;
                    arrtime = (DateTime)locitem.EFFECTIVE_DATETIME_IN;
                }
                else
                {
                    if (prev_out != (DateTime)locitem.EFFECTIVE_DATETIME_IN)
                    {
                        arrtime = (DateTime)locitem.EFFECTIVE_DATETIME_IN;
                    }
                    prev_in = (DateTime)locitem.EFFECTIVE_DATETIME_IN;
                    prev_out = (DateTime)locitem.EFFECTIVE_DATETIME_OUT;
                }
            }
            return arrtime;
        }


        //static List<int> GetUnitDefaultIndicators(int unit_id, DateTime pull_dt, out string default_inds_str, out int default_ptype)
        //{
        //    var result = new List<int>();                   // make an empty list
        //    default_inds_str = "<none>";
        //    default_ptype = 6;
        //    var db = PFSDBUtility.NewPfsDataContext();        // look for the unit default admission profile
        //    var query = from param in db.UNIT_PARAMs
        //                from profile in param.PATIENT_PROFILEs
        //                where (param.UNIT_ID == unit_id)
        //                && (pull_dt >= param.EFFECTIVE_DATETIME) && (pull_dt < param.EXPIRATION_DATETIME)
        //                && (profile.PROFILE_NUMBER == param.DEFAULT_ADMISSION_PROFILE)
        //                select new
        //                {
        //                    profile.INDICATORS              // comma-separated indicator list
        //                };
        //    foreach (var inds in query)
        //    {
        //        default_inds_str = inds.INDICATORS;
        //        string s = inds.INDICATORS;
        //        if (!String.IsNullOrEmpty(s))
        //        {
        //            var arr = s.Split(',');
        //            foreach (var t in arr)
        //            {
        //                if (t.IsNumeric())
        //                {                // add an indicator number to the list
        //                    result.Add(t.ToInteger());
        //                }
        //            }
        //        }
        //    }

        //    var query_ind_def = from ind_def in db.INDICATOR_DEFINITIONs
        //                        where (ind_def.METHODOLOGY_ID == PFSGlobal.METH_ID_APLUS_INPATIENT2) &&
        //                          result.Contains(ind_def.INDICATOR_NUMBER)
        //                        select new
        //                        {
        //                            ind_def.WEIGHT
        //                        };
        //    var score = 0.0;
        //    foreach (var wgts in query_ind_def)
        //    {
        //        score += wgts.WEIGHT;
        //    }
        //    DebugTrace("indicators=" + default_inds_str, "");
        //    DebugTrace("score=" + score.ToString(), "");

        //    var query_ptype = from ptype in db.PATIENT_TYPEs
        //                      where (ptype.METHODOLOGY_ID == PFSGlobal.METH_ID_APLUS_INPATIENT2)
        //                      orderby ptype.PATIENT_TYPE1
        //                      select new
        //                      {
        //                          ptype.PATIENT_TYPE1,
        //                          ptype.POINTS_HIVAL
        //                      };

        //    foreach (var ptypes in query_ptype)
        //    {
        //        //DebugTrace("type,hival=" + ptypes.PATIENT_TYPE1.ToString()+","+ptypes.POINTS_HIVAL.ToString(), "");
        //        if (score <= ptypes.POINTS_HIVAL)
        //        {
        //            if (default_ptype > ptypes.PATIENT_TYPE1)
        //            {
        //                default_ptype = ptypes.PATIENT_TYPE1;
        //            }
        //        }
        //    }
        //    DebugTrace("def patient type=" + default_ptype.ToString(), "");

        //    return result;
        //}


        static void DeleteOldChartItems()
        {
            string sql;
            DateTime dt;

            if (g_no_output || g_no_delete) return;

            DebugTrace("About to delete old char items...");
            DebugPause();                                       // give a chance to ^C in debug mode

            LogInfo("Delete old chart items...");
            dt = g_effdt.AddDays(-CHART_ITEM_LIFE);
            sql = "DELETE FROM CHART_ITEM WHERE EVENT_DATETIME < " + PFSDBUtility.SQLDateTime(dt);
            PFSDBUtility.ExecuteSQL(sql);
            LogInfo("Done");
        }

        //static void MaybeRunImport()
        //{
        //    if (! g_import_when_done) return;

        //    string path = Path.GetDirectoryName(Application.ExecutablePath);
        //    if (path.Right(3) != "bin") {                       // running in visual studio?
        //        path = @"C:\qmdev\AcuityPlus\main\bin";
        //    }
        //    if (Directory.Exists(path))
        //    {
        //        LogInfo("Running transparent import...");
        //        // Import will add its own log entries; no need to add any here
        //        int rc = PFSUtility.ExecuteAndWait(Path.Combine(path, PFSGlobal.TRANSPARENT_IMPORT_EXE_NAME));
        //        LogInfo("Done; result=" + rc);
        //    }
        //    else
        //    {
        //        LogWarning("Can't find " + path);
        //    }
        //}



        //=====================================================================
        // Audits and log files
        //=====================================================================
        // Print to console if debug is set
        public static void DebugTrace(string format, params object[] values)
        {
            if (g_debug)
            {
                Console.Write(DateTime.Now.ToString() + " ");
                Console.WriteLine(format, values);
            }
        }

        public static void DebugPause()
        {
            if (g_debug)
            {
                Console.Write("Press any key...");
                Console.ReadKey();
                Console.WriteLine("");
            }
        }

        // Save in both audit files
        static public void Audit(string s)
        {
            DebugTrace(s);
            if (logfile != null) logfile.WriteLine(s);
            gBriefAudit.AppendLine(s);                  // Add to both audit reports
            gVerboseAudit.AppendLine(s);
        }

        // Save in verbose audit only
        static public void VerboseAudit(string s)
        {
            DebugTrace(s);
            if (logfile != null) logfile.WriteLine(s);
            gVerboseAudit.AppendLine(s);                // Add to verbose audit only
        }

        public static void AddLogEntry(PFSEventLog.EventLogType type, string msg, PFSEventLog.EventLogCategory category)
        {
            Audit(msg);

            PFSEventLog.AddEventLogEntry(
                PFSEventLog.EventLogSource.EVENT_SOURCE_TRANSPARENT_MAPPING,
                type, category, msg, gLogSourceText, 0, gLogUnitID, gLogEncounterID);
        }

        public static void LogInfo(string msg, PFSEventLog.EventLogCategory category)
        {
            AddLogEntry(PFSEventLog.EventLogType.EVENT_TYPE_INFO, msg, category);
        }
        public static void LogInfo(string msg)
        {
            LogInfo(msg, PFSEventLog.EventLogCategory.EVENT_CATEGORY_PROCESSED);
        }

        public static void LogWarning(string msg, PFSEventLog.EventLogCategory category)
        {
            AddLogEntry(PFSEventLog.EventLogType.EVENT_TYPE_WARNING, msg, category);
        }
        public static void LogWarning(string msg)
        {
            LogWarning(msg, PFSEventLog.EventLogCategory.EVENT_CATEGORY_VALIDATION);
        }

        public static void LogError(string msg, PFSEventLog.EventLogCategory category)
        {
            AddLogEntry(PFSEventLog.EventLogType.EVENT_TYPE_ERROR, msg, category);
        }
        public static void LogError(string msg)
        {
            LogError(msg, PFSEventLog.EventLogCategory.EVENT_CATEGORY_VALIDATION);
        }
        public static void LogUnexpectedError(string msg, string stack_trace)
        {
            // Add the message and stack trace to event log; message only goes to screen
            gLogSourceText = stack_trace;
            AddLogEntry(PFSEventLog.EventLogType.EVENT_TYPE_ERROR, msg, PFSEventLog.EventLogCategory.EVENT_CATEGORY_UNEXPECTED);
            gLogSourceText = "";
            
            // Add the stack trace to screen and log files
            Audit(msg);
            Audit(stack_trace);
        }



    }
}
