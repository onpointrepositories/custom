﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;                     // for LINQ; add a reference
using System.Data.SqlClient;                // for SqlConnection
using System.IO;                            // for Directory, Path, Stream
using PfsShared;                            // add a reference to Shared2 project

// ============================================================================
// Mental Health transparent mapping -- GOES HERE --
// MAYO ROCHESTER EPIC
// ============================================================================
// This processes one patient.  Only one database query is used.  :)
//
// All search functions use exact match for category, description and field name.
// Codes and results can be a single word or a list of words.
// Codes default to exact match; prefix with CODE_CODE_LIKE_PREFIX if desired (single word only)
// Results default to like matching; prefix with RESULT_EXACT_MATCH_PREFIX if desired (single word only)
//
// All searches are case insensitive.
//
//
namespace TransparentMapping
{
    class MentalHealth
    {
        private const int MAX_INDS = 120;
        private const int MAX_PROCS = 20;

        private const string CODE_LIKE_PREFIX = "%!";       // use to make codes %like%
        private const string EXACT_MATCH_PREFIX = "&!";     // use to make desc/result exact match
        private const string CHAR_COMMA = "||";             // use to insert literal comma in word list
        private const string NOT_PREFIX = "!!";             // use to insert literal comma in word list
        private const string AVOID_NEGATIVE = "!;";
        private const string EXACT_SQL_PREFIX = "!SQL!";     // use to give sql constraint as given
        private const string STARTS_WITH = "[!";     // use to give sql constraint as given

        private struct indicator_data
        {
            public bool is_checked;
            public int radio_group;
        }

        private struct proc_data
        {
            public int procedure_number;
            public DateTime start;
            public DateTime finish;
            public bool remove;
        }

        // These are database CHART_ITEMs for this patient
        //private CHART_ITEM[] _chart_items_since_admission;
        //private CHART_ITEM[] _chart_items_since_unit_arrival;
        //private CHART_ITEM[] _chart_items_during_pull_period;
        //private CHART_ITEM[] _chart_items_pull_period_plus;
        //private CHART_ITEM[] _chart_items_since24hrs;
        //private CHART_ITEM[] _chart_items_since13hrs;
        private CHART_ITEM[] _chart_items_since25hrs;
        //private CHART_ITEM[] _chart_items_since9hrs;
        // These are database PROCEDURE_EVENTs for this patient
        private PROCEDURE_EVENT[] _procedure_events;

        private indicator_data[] _inds;
        private List<proc_data> _procs;
        //private List<proc_data> _outcomes;

        private const int DEFAULT_BUCKET_SIZE = 20;       //min

        private PatientInfo _pat;
        private int _bucket_size = DEFAULT_BUCKET_SIZE;
        private bool is_default;
        private string txarea;

        private bool exclude_periop_data = false;

        private enum SearchDepth
        {
            SearchDefault,
            SearchPullRange,            //search within the current pull     -- default
            SearchSinceArrival,         //search since arrival to the unit
            SearchSinceAdmission,        //search everything since admission to the hospital
            SearchPullPlus,
            SearchSince25Hrs,
            SearchSince24Hrs,
            SearchSince16Hrs,
            SearchSince13Hrs,
            SearchSince9Hrs
        }

        private enum CountMode
        {
            CountAll,
            CountFirst                  //stop after one is found
        }

        private enum GetValueMode
        {
            GetTotal,
            GetMax,
            GetLast
        }

        enum Frequencies
        {
            QNONE,
            Q4H,
            Q2H,
            Q1H,
            Q30M
        }

        private struct fmapRow
        {
            public double los_high;                       //the LOS being testing
            public int[] freq;                           //the count required for each Q value
        }
        private List<fmapRow> _freq_map;                    //1,2,4,8,12,24 hours

        private struct gBucket
        {
            public int bucket;
            public string code;
            public DateTime evdt;
            public bool using_waiver;
            public bool has_all_deps;
            public int num_addl_items;
        }
        private struct gGap
        {
            public int gap;
            public DateTime evdt1;
            public DateTime evdt2;
        }


        //
        // This is the main entry point
        //
        public void ProcessPatient(PatientInfo pat)
        {
            _pat = pat;
            bool use_default = false;
            bool no_chart_items_in_24hrs = false;

            InitIndicators(); // sets is_default
            InitProcs();
            if (!is_default)
            {
                LoadFreqTable();
                no_chart_items_in_24hrs = (LoadPatientChart() == 0);
                if (no_chart_items_in_24hrs)
                {
                    Program.Audit("No chart items received in past 24 hrs.");
                }
                else
                {
                    Check_1_2_3();
                    Check_4();
                    Check_5_6();
                    Check_7_8_9();
                    Check_10_11_12_13_14();
                    Check_15();
                    Check_16_17();
                    Check_18_19_20();
                    Check_21();
                    Check_22();
                    Check_23();
                    Check_24();
                    //CheckUserDefined();
                    AtLeastOneADL();
                }
            }

            if (!no_chart_items_in_24hrs)
            {

                HighestIndicatorInEachGroupWins();

                if (!is_default)
                {
                    CheckProcs();
                }

                if (Program.g_no_output) return;

                OutputClass(use_default);
                OutputProcs();
            }
        }


        private void InitIndicators()
        {
            // Make a new (empty) indicator array
            _inds = new indicator_data[MAX_INDS + 1];           // This 1 based so add one
            is_default = false;
            txarea = "";

            // get indicator radio groups from the database
            // ** (This database access can be replaced once we have a C# methodolgy cache)
            var db = PFSDBUtility.NewPfsDataContext();
            var query = from idef in db.INDICATOR_DEFINITIONs
                        where (idef.METHODOLOGY_ID == _pat.meth_id)
                        select idef;
            foreach (var idef in query)
            {
                if (idef.INDICATOR_NUMBER <= _inds.GetUpperBound(0))
                {
                    // (convert nulls to zero)
                    _inds[idef.INDICATOR_NUMBER].radio_group = PFSDBUtility.DBToInt(idef.RADIO_GROUP);
                }
            }
        }

        private void InitProcs()
        {
            _procs = new List<proc_data>();
            //_outcomes = new List<proc_data>();
        }

        private fmapRow LoadFreqTableRow(double los_high, string values)
        {
            fmapRow fmrow;

            fmrow.los_high = los_high;
            fmrow.freq = new int[Enum.GetNames(typeof(Frequencies)).Length];

            var arr = values.Split(',');
            for (int i = 0; i <= fmrow.freq.GetUpperBound(0); i++)
            {
                fmrow.freq[i] = arr[i].ToInteger();
            }
            return fmrow;
        }

        // These are the number of times you need to do something during a certain time period (LOS)
        // in order for it to be considered one of the frequencies listed.
        private void LoadFreqTable()
        {
            _freq_map = new List<fmapRow>();
            //                              LOS,  None Q4h Q2h Q1h Q30m
            _freq_map.Add(LoadFreqTableRow(1, "    0,  0,  0,  1,  2"));
            _freq_map.Add(LoadFreqTableRow(2, "    0,  0,  1,  2,  4"));
            _freq_map.Add(LoadFreqTableRow(4, "    0,  1,  2,  4,  8"));
            _freq_map.Add(LoadFreqTableRow(6, "    0,  2,  3,  6,  9"));
            _freq_map.Add(LoadFreqTableRow(8, "    0,  2,  4,  8, 12"));
            _freq_map.Add(LoadFreqTableRow(12, "   0,  3,  6, 12, 18"));
            _freq_map.Add(LoadFreqTableRow(16, "   0,  4,  8, 16, 24"));
            _freq_map.Add(LoadFreqTableRow(24, "   0,  6, 12, 24, 36"));
            //New freq table 2/5/14
            //q4	q2	q1	q30     q30
            //            Non-ICU	ICU & SD
            // 4	8	15	29	    36
            // 3	5	9	17	    24
            // 2	4	7	13	    19
            // 2	3	5	10	    13

            //to read this table: if you have at least 6 asessmenets in a 12-hour period, you get Q1h
            //(LOS=12, column Q1h has a count of 6)
        }

        private Frequencies FreqForCount(double los_hours, int count)
        {
            foreach (var fmrow in _freq_map)
            {
                if (los_hours <= fmrow.los_high)
                {
                    // Option: pro-rate the count upward if the LOS is less than the row we are interested in.
                    //         This will bump the count to what it might have been at the full LOS.
                    // Note: truncate the result; rounding inflates the value too much.
                    int prorated_count = (int)((fmrow.los_high / los_hours) * count);

                    // foreach goes low to high; go from high to low instead
                    for (int j = (int)Frequencies.Q30M; (j > (int)Frequencies.QNONE); j--)
                    { //search right to left
                        if (prorated_count >= fmrow.freq[j])
                        {
                            return (Frequencies)j;
                        }
                    } // next j
                }
            }

            return Frequencies.QNONE;
        }

        //
        // Load patient chart from the database (one DB access)
        // Load everything since admission
        //
        private int LoadPatientChart()
        {
            int ct_in_25hrs = 0;
            int ctperiop = 0;
            // Get the entire patient chart (all units and dates for this patient)
            var db = PFSDBUtility.NewPfsDataContext();
            var query = from item in db.CHART_ITEMs
                        where (item.ENCOUNTER_ID == _pat.encounter_id)
                        where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-25))
                        select item;
            // Save the result
            ct_in_25hrs = query.Count();
            Program.VerboseAudit("Since 25 hrs count=" + ct_in_25hrs);
            _chart_items_since25hrs = query.ToArray();

            // Convert all results to lower case for case insensitive comparisons
            foreach (var item in _chart_items_since25hrs)
            {
                if (item.CATEGORY != null) item.CATEGORY = item.CATEGORY.ToLower();
                if (item.CODE != null) item.CODE = item.CODE.ToLower();
                if (item.DESCRIPTION != null) item.DESCRIPTION = item.DESCRIPTION.ToLower();
                if (item.FIELD_NAME != null) item.FIELD_NAME = item.FIELD_NAME.ToLower();
                if (item.RESULT != null) item.RESULT = item.RESULT.ToLower();
                //mark items during periop
                foreach (var perioploc in Program.patperioplist)
                {
                    if (item.EVENT_DATETIME >= perioploc.in_time && item.EVENT_DATETIME <= perioploc.out_time)
                    {
                        item.UNIT_ID = -6;
                        ctperiop++;
                    }
                }

            }

            Program.VerboseAudit("Since 25 hrs count of periop/temp items=" + ctperiop);
            // Prepare more versions of the chart

            //var query2 = from item in _chart_items_since25hrs
            //             where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-13)) && (item.EVENT_DATETIME <= _pat.pull_finish)
            //             select item;
            //Program.VerboseAudit("Since 13 hrs count=" + query2.Count());
            //_chart_items_since13hrs = query2.ToArray();

            //query2 = from item in _chart_items_since25hrs
            //             //                     where (item.EVENT_DATETIME >= _pat.pull_start) && (item.EVENT_DATETIME <= _pat.pull_finish)
            //         where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-8)) && (item.EVENT_DATETIME <= _pat.pull_finish)
            //         select item;
            //_chart_items_during_pull_period = query2.ToArray();

            return ct_in_25hrs;
        }

        // Delay looking for patient procedures until they are wanted
        private void LoadPatientProceduresIfNeeded()
        {
            if (_procedure_events != null) return;

            var db = PFSDBUtility.NewPfsDataContext();
            var query = from proc in db.PROCEDURE_EVENTs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                        select proc;
            _procedure_events = query.ToArray();
        }



        // Started a new chart item query (of a certain depth)
        // Default is the chart during the pull range.  Options for unit arrival and admission to hospital.
        private IEnumerable<CHART_ITEM> StartNewQuery()
        {
            return StartNewQuery(SearchDepth.SearchDefault);
        }
        private IEnumerable<CHART_ITEM> StartNewQuery(SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;

            switch (search_depth)
            {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since25hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-8) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since25hrs where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-8) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                //    return (from item in _chart_items_since_unit_arrival select item);
                //case SearchDepth.SearchSinceAdmission:
                //    return (from item in _chart_items_since_admission select item);
                //case SearchDepth.SearchPullPlus:
                //    return (from item in _chart_items_pull_period_plus select item);
                case SearchDepth.SearchSince25Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since25hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                    else
                        return (from item in _chart_items_since25hrs where (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince24Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since25hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-24) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since25hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-24) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince16Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since25hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-16) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since25hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-16) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince13Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since25hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-13) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since25hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-13) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince9Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since25hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-9) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since25hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-9) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
            }
            return null;
        }

        // Is this search term a word or a list of words?
        private bool ValueIsAList(string s)
        {
            return (s != null) && s.Contains(',');                //is this a comma-separated list?
        }

        // Break apart a wordlist and prepare its members
        private string[] SplitOnCommaAndPrepareElements(string s)
        {
            // If a word list has a space after each comma, we will get leading blanks in each term
            var arr = s.Split(',');
            for (int i = 0; i <= arr.GetUpperBound(0); i++)
            {
                arr[i] = arr[i].Trim();                         // get rid of leading blanks
                arr[i] = arr[i].Replace(CHAR_COMMA, ",");       // insert a literal comma into search word
                arr[i] = arr[i].ToLower();                      // convert for case-insensitive comparisons
            }
            return arr;
        }

        // Add a basic chart item filter to a LINQ query
        //
        // cat = exact match
        // code_list = exact match; optional "like" match
        // desc_list = "like" match; optional exact match
        // field = exact match
        // result_list = "like" match; optional exact match
        //
        // These are comma-separated lists.  
        // Search for a literal comma by using CHAR_COMMA in the string.
        private IEnumerable<CHART_ITEM> AndItemFilter(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list)
        {
            // Converted all values to lower case for case-insensitive comparisons.
            if (!String.IsNullOrEmpty(cat))
            {
                cat = cat.ToLower();
                query = query.Where(e => e.CATEGORY == cat);
            }

            if (!String.IsNullOrEmpty(code_list))
            {
                code_list = code_list.ToLower();
                if (code_list.Left(2) == CODE_LIKE_PREFIX)
                {            // override for "like" match?
                    query = query.Where(e => e.CODE.Contains(code_list.Substring(2)));
                }
                else if (code_list.Left(2) == EXACT_MATCH_PREFIX)
                {
                    query = query.Where(e => e.CODE == code_list.Substring(2));
                }
                else if (ValueIsAList(code_list))
                {
                    query = AndCodeInList(query, code_list);            // find one of the words
                }
                else
                {
                    query = query.Where(e => e.CODE.Contains(code_list));      // find this word
                }
            }

            if (!String.IsNullOrEmpty(desc_list))
            {
                desc_list = desc_list.ToLower();
                if (desc_list.Left(2) == EXACT_MATCH_PREFIX)
                {          // exact match override?
                    query = query.Where(e => e.DESCRIPTION == desc_list.Substring(2));
                }
                else if (desc_list.Left(2) == NOT_PREFIX)
                {
                    // query = query.Where(e => !e.DESCRIPTION.Contains(desc_list.Substring(2)));
                    query = AndDescriptionNOTInList(query, desc_list.Substring(2));
                }
                else if (ValueIsAList(desc_list))
                {
                    query = AndDescriptionInList(query, desc_list);
                }
                else
                {
                    desc_list = desc_list.Replace(CHAR_COMMA, ",");     // look for literal comma requests
                    query = query.Where(e => e.DESCRIPTION.Contains(desc_list));
                }
            }

            if (!String.IsNullOrEmpty(field))
            {
                field = field.ToLower();
                query = query.Where(e => e.FIELD_NAME == field);
            }

            if (!String.IsNullOrEmpty(result_list))
            {
                query = query.Where(e => e.RESULT != null);
                result_list = result_list.ToLower();                        // make all results case insensitve
                if (result_list.Left(2) == EXACT_MATCH_PREFIX)
                {     // exact match override?
                    query = query.Where(e => e.RESULT == result_list.Substring(2));
                }
                else if (result_list.Left(2) == AVOID_NEGATIVE)
                {
                    query = query.Where(e => (!e.RESULT.ToLower().Contains("no " + result_list.Substring(2))) && (e.RESULT.ToLower().Contains(result_list.Substring(2)))); // == result_list.Substring(2)) || (e.RESULT.Contains(";" + result_list.Substring(2)))));
                }
                else if (ValueIsAList(result_list))
                {
                    query = AndResultInList(query, result_list);
                }
                else if (result_list.Left(2) == STARTS_WITH)
                {
                    query = query.Where(e => e.RESULT.StartsWith(result_list.Substring(2)));
                }
                else
                {
                    result_list = result_list.Replace(CHAR_COMMA, ",");     // look for literal comma requests
                    query = query.Where(e => e.RESULT.Contains(result_list));
                }
            }
            return query;
        }

        // Look for a code that is one of the words in the list.
        private IEnumerable<CHART_ITEM> AndCodeInList(IEnumerable<CHART_ITEM> query, string code_list)
        {
            if (String.IsNullOrEmpty(code_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(code_list);
            //            return query.Where(e => arr.Any(x => x.Contains(e.CODE))); // "like" match
            return query.Where(e => e.CODE.ContainsAny(arr)); // "like" match
        }

        // Look for a description that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndDescriptionInList(IEnumerable<CHART_ITEM> query, string desc_list)
        {
            if (String.IsNullOrEmpty(desc_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(desc_list);
            //return query.Where(e => arr.Any(x => x.Contains(e.DESCRIPTION))); // "like" match
            return query.Where(e => e.DESCRIPTION.ContainsAny(arr)); // "like" match
        }

        private IEnumerable<CHART_ITEM> AndDescriptionNOTInList(IEnumerable<CHART_ITEM> query, string desc_list)
        {
            if (String.IsNullOrEmpty(desc_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(desc_list);
            //return query.Where(e => arr.Any(x => x.Contains(e.DESCRIPTION))); // "like" match
            return query.Where(e => !e.DESCRIPTION.ContainsAny(arr)); // "like" match
        }


        // Look for a result that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultInList(IEnumerable<CHART_ITEM> query, string result_list)
        {
            bool use_start_with = false;

            if (String.IsNullOrEmpty(result_list)) return query;

            if (result_list.Left(2) == STARTS_WITH)
            {
                use_start_with = true;
                result_list = result_list.Substring(2);
            }

            var arr = SplitOnCommaAndPrepareElements(result_list);
            //            return query.Where(e => arr.Any(x => x.Contains(e.RESULT))); // "like" match
            if (use_start_with)
                return query.Where(e => arr.Any(item => e.RESULT.StartsWith(item)));
            else
                return query.Where(e => e.RESULT.ContainsAny(arr)); // "like" match

        }

        // Look for a result that does not contain one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultNotInList(IEnumerable<CHART_ITEM> query, string result_list)
        {
            if (String.IsNullOrEmpty(result_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(result_list);
            //            return query.Where(e => !arr.Any(x => x.Contains(e.RESULT))); // "like" match
            return query.Where(e => !e.RESULT.ContainsAny(arr)); // "like" match
        }

        private string DescribeSearchDepth(SearchDepth search_depth)
        {
            string result = "";

            switch (search_depth)
            {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    //result = "in pull range"
                    result = "";                         //be quiet since this is default
                    break;
                case SearchDepth.SearchSinceArrival:
                    result = "since arrival to unit";
                    break;
                case SearchDepth.SearchSinceAdmission:
                    result = "since admission";
                    break;
                case SearchDepth.SearchPullPlus:
                    result = "in LOS range plus 4 hours after";
                    break;
            }

            return result;
        }

        // Describe a query that has already been run
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Make a simple query and describe the results
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            var arr = query.ToArray();
            return Describe(arr, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private string LookingFor(string so_far, string what, string op, string value)
        {
            string result;
            if (String.IsNullOrEmpty(value)) return so_far;

            if (so_far == "")
                result = "looking for " + what;
            else
                result = so_far + " and " + what;

            if (value.Left(2) == EXACT_MATCH_PREFIX)
                result += "='" + value.Substring(2) + "'";
            else if (value.Left(2) == CODE_LIKE_PREFIX)
                result += " contains " + "'" + value.Substring(2) + "'";
            else if (ValueIsAList(value))
                result += " in '" + value + "'";
            else
                result += op + "'" + value + "'";

            return result;
        }
        private string Describe(CHART_ITEM[] arr, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string result = "";
            result = LookingFor(result, "cat", "=", cat);
            result = LookingFor(result, "code", "=", code_list);
            result = LookingFor(result, "desc", " contains ", desc_list);
            result = LookingFor(result, "field", "=", field);
            result = LookingFor(result, "result", " contains ", result_list);

            if (arr.Count() == 0)
            {
                result += "; not found " + DescribeSearchDepth(search_depth);
            }
            else
            {
                var e = arr[0];
                result = "FOUND: " + result + "; found";
                // We might have searched for a pattern or word list in several fields - show what was found
                if (e.CATEGORY != null) result += " cat='" + e.CATEGORY + "'";
                if (e.CODE != null) result += " code='" + e.CODE + "'";
                if (e.DESCRIPTION != null) result += " desc='" + e.DESCRIPTION + "'";
                if (e.FIELD_NAME != null) result += " field='" + e.FIELD_NAME + "'";
                if (e.RESULT != null) result += " result='" + e.RESULT + "'";
                // Are there more results?  Just say how many; we aren't going to list them.
                if (arr.Count() == 2)
                {
                    result += " (1 more result)";
                }
                else if (arr.Count() > 2)
                {
                    result += " (" + (arr.Count() - 1) + " more results)";
                }
            }

            return result;
        }


        //Set an indicator for this reason (low level)
        private void SetInd(int inum, string reason)
        {
            if (_inds[inum].is_checked)
            {
                Program.VerboseAudit("Set Ind #" + inum + ": " + reason);          //already set - repeat for verbose only
            }
            else
            {
                _inds[inum].is_checked = true;
                Program.Audit("Set Ind #" + inum + ": " + reason);
            }
        }

        //Clear an indicator for this reason (low level)
        private void ClrInd(int inum, string reason)
        {
            if (_inds[inum].is_checked)
            {
                Program.VerboseAudit("Clr Ind #" + inum + ": " + reason);          //already clear - repeat verbose only
            }
            else
            {
                _inds[inum].is_checked = false;
                Program.Audit("Clr Ind #" + inum + ": " + reason);
            }
        }

        // Count how many items meet the conditions
        // All items are listed
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountItems(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            int count = query.Count();

            // always return what was found
            found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
            // echo the result?
            if (trace) Program.VerboseAudit(found_what);

            return count;
        }


        // Count how many items meet the conditions
        // This has the option to list every entry or give a total
        private int CountResultInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what = "";
            string s = "";

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);

            var arr = SplitOnCommaAndPrepareElements(result_list);
            //Walker Schlundt
            //Set Ind #9: found '' in cat='' code='301870' field='' result='oriented x4' 

            foreach (var item in query)
            {
                // Figure out which of the search words were found
                for (int i = 0; i <= arr.GetUpperBound(0); i++)
                {
                    if (item.RESULT.Contains(arr[i]))
                    {
                        count++;
                        s = arr[i];
                        found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' code='" + item.CODE + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "'";
                        //if (trace) Program.VerboseAudit(found_what);
                        Program.VerboseAudit(found_what);
                    }
                }
                if (count_mode != CountMode.CountAll) break;        //stop counting

            }

            if (count > 0)
            {
                //We already printed what was found; maybe add how many?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            }
            else
            {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        // Count how many items meet the conditions
        // This has the option to list every entry or give a total
        private int CountResultInListEXCEPTList(string cat, string code_list, string desc_list, string field, string result_list, string negresult_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what = "";

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            query = AndResultNotInList(query, negresult_list);

            var arr = SplitOnCommaAndPrepareElements(result_list);

            foreach (var item in query)
            {
                // Figure out which of the search words were found
                var s = item.RESULT.ContainsWhich(arr);
                found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' code='" + item.CODE + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "'";
                if (trace) Program.VerboseAudit(found_what);
                count++;
                if (count_mode != CountMode.CountAll) break;        //stop counting
            }

            if (count > 0)
            {
                //We already printed what was found; maybe add how many?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            }
            else
            {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        // Count how many items contain ALL results in list
        private int CountResultInListAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what = "";

            var arr = SplitOnCommaAndPrepareElements(result_list);

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            for (int i = 0; i <= arr.GetUpperBound(0); i++)
            {
                if (arr[i].StartsWith(NOT_PREFIX))
                {
                    query = query.Where(e => !e.RESULT.Contains(arr[i].Substring(2)));
                }
                else
                {
                    query = query.Where(e => e.RESULT.Contains(arr[i]));
                }
            }

            count = query.Count();

            if (count > 0)
            {
                found_what = "found item with all results in '" + result_list + "' without those prefixed by !!";
                if (trace) Program.VerboseAudit(found_what);
            }
            else
            {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (ValueIsAList(result_list))
            {
                return CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what);
            }
            else
            {
                return CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what);
            }
        }

        // Is there an item with this result?
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContains(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (ValueIsAList(result_list))
            {
                return (CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            }
            else
            {
                return (CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
            }
        }

        // Result contains all in list
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (ValueIsAList(result_list))
            {
                return (CountResultInListAll(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            }
            else
            {
                found_what = "Not valid for non-list results.";
                return false;
            }
        }
        // Count how many items meet the conditions, with none of the results in result_list.
        // This has the option to say how many were found rather than list all.
        private int CountResultNotInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0, rec_count = 0;
            bool found_one;

            found_what = "";
            var arr = SplitOnCommaAndPrepareElements(result_list);         // also deals with CHAR_COMMA

            //Do not filter by result here; we need all results
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            foreach (var item in query)
            {
                rec_count++;
                //Look for each search word in the result
                found_one = false;

                foreach (string s in arr)
                {
                    if (String.Equals(item.RESULT, s))
                    {
                        found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "' -- ignore this";
                        Program.VerboseAudit(found_what);
                        found_one = true;
                        break;
                    }
                }

                if (!found_one)
                {
                    //none of the words were found - good!
                    found_what = Describe(cat, code_list, desc_list, field, "", search_depth) + " -- does not contain '" + result_list + "'";
                    Program.VerboseAudit(found_what);
                    count++;
                    if (count_mode != CountMode.CountAll) break;
                }
            }

            if (count > 0)
            {
                //We already printed what was found; maybe show the total?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            }
            else if (rec_count > 0)
            {
                //We already printed what we ignored
            }
            else
            {
                //Print what did not exist
                found_what = Describe(cat, code_list, desc_list, field, "", search_depth);      //not found
                Program.VerboseAudit(found_what);
            }

            return count;
        }

        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, string found_what)
        {
            // Search in the pull range with trace on
            return CountResultDoesNotContain(cat, code_list, desc_list, field, result_list, out found_what, SearchDepth.SearchDefault, true);
        }
        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, out string found_what, SearchDepth search_depth, bool trace)
        {
            return CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0;
        }

        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            return (CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0);
        }

        // Set the indicator if the conditions are met
        //
        private bool SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SearchDepth s = SearchDepth.SearchDefault;
            if (inum <= 3) s = SearchDepth.SearchSince25Hrs;
            if (inum == 4) s = SearchDepth.SearchSince25Hrs;
            if (inum == 5 || inum == 6) s = SearchDepth.SearchSince25Hrs;
            if (inum >= 7 && inum <= 9) s = SearchDepth.SearchSince25Hrs;
            if (inum >= 10 && inum <= 14) s = SearchDepth.SearchSince13Hrs;
            if (inum == 15) s = SearchDepth.SearchSince25Hrs;
            if (inum >= 16 && inum <= 20) s = SearchDepth.SearchSince9Hrs;
            if (inum == 21) s = SearchDepth.SearchSince25Hrs;
            if (inum == 22) s = SearchDepth.SearchSince25Hrs;
            if (inum == 23) s = SearchDepth.SearchSince9Hrs;
            if (inum == 24) s = SearchDepth.SearchSince9Hrs;
            return SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, s);
        }
        private bool SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            bool b = false;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return true;

            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                          //echo here - found
                b = true;
            }
            else
            {
                Program.VerboseAudit(found_what);                  //and here - not found
            }
            return b;
        }

        private void SetIndIfResultBetween(int inum, string cat, string code_list, string desc_list, string field, int loval, int hival)
        {
            SetIndIfResultBetween(inum, cat, code_list, desc_list, field, loval, hival, SearchDepth.SearchDefault);
        }

        private void SetIndIfResultBetween(int inum, string cat, string code_list, string desc_list, string field, int loval, int hival, SearchDepth search_depth)
        {
            int count = 0;
            string found_what = "";

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");
            foreach (var item in query)
            {
                if (item.RESULT.IsNumeric())
                {
                    if (item.RESULT.Val() >= loval && item.RESULT.Val() <= hival)
                    {
                        count++;
                        found_what = "found code=" + item.CODE + " with result=" + item.RESULT;
                        Program.VerboseAudit(found_what);
                    }
                }
            }

            if (count > 0)
            {
                SetInd(inum, found_what);
            }
        }


        // Special contains all
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContainsAll(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return;

            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                          //echo here - found
            }
            else
            {
                Program.VerboseAudit(found_what);                  //and here - not found
            }
        }
        // Set the indicator if the result does not contain any of the words in result_list
        //
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultDoesNotContain(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return;

            //Turn trace off for ResultDoesNotContain() and echo what was set below with SetInd
            if (ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                           //echo here - not found
            }
            else
            {
                Program.VerboseAudit(found_what);                   //and here - found
            }
        }

        // Clear the indicator if the result contains one of the words in the result_list
        //
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already clear
            if (!_inds[inum].is_checked) return;

            //Turn trace off for ResultContains() and echo what was set below with SetInd
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                ClrInd(inum, found_what);                           //echo here - found
            }
            else
            {
                Program.VerboseAudit(found_what);                   //and here - not found
            }
        }


        //These slightly smaller functions are meant for places where you aren't really looking for a chart result
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }

        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            return (CountResultContains(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
        }

        // Variations with optional parameters (can switch to optional parameters in VS 2010)
        //
        private void SetIndIfFound(int inum, string cat)
        {
            SetIndIfResultContains(inum, cat, "", "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list)
        {
            SetIndIfResultContains(inum, cat, code_list, "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private void ClrIndIfFound(int inum, string cat)
        {
            ClrIndIfResultContains(inum, cat, "", "", "", "", SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }


        // Get the max/total value from a result (usually in the middle of the text)
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetIntValue(get_mode, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            int value;
            int result = 0;
            bool found_one = false;

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);

            //Look for a number in the result

            foreach (var item in query)
            {
                // RESULT might be null so convert to empty string so Split won't blow up
                var arr = PFSDBUtility.DBToString(item.RESULT).Split(' ');
                foreach (string s in arr)
                {
                    //Look for 1st character numeric; IsNumeric("60min") will fail so look at 1st char only
                    if (s.Left(1).IsNumeric())
                    {
                        value = (int)s.Val();                           //Use Val; ToInteger will error on "60min"
                        switch (get_mode)
                        {
                            case GetValueMode.GetMax:
                                result = Math.Max(value, result);       //max
                                break;
                            case GetValueMode.GetTotal:
                                result += value;                        //total
                                break;
                            case GetValueMode.GetLast:
                                result = value;                         //last
                                break;
                        }

                        // print what we are searching for (the first time)
                        if (!found_one)
                        {
                            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
                        }
                        found_one = true;
                        // print each value found
                        Program.VerboseAudit("  found numeric value " + result);
                        //Keep going in case there are more
                    }
                }
            }

            if (!found_one)
            {
                //show what was not found
                Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
            }

            return result;
        }

        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetMaxValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return GetIntValue(GetValueMode.GetMax, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetTotalValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return GetIntValue(GetValueMode.GetTotal, cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Get a result; returns true if found with return_result
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result)
        {
            return GetResult(cat, code_list, desc_list, field, out return_result, SearchDepth.SearchDefault);
        }
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
            }
            else
            {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }
        private bool GetResultForInspection(string cat, string code_list, string desc_list, string field, string result, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result);

            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
            }
            else
            {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }

        private void GetResultforOther(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "Other");
            return_result = "";
            foreach (var item in query)
            {
                if (!String.Equals(item.RESULT, "Other"))
                {
                    return_result = item.RESULT;
                    //Program.VerboseAudit(found_what);
                    break;
                }

            }
            return;
        }

        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, out string return_result, out DateTime return_evdt)
        {
            return GetResultAndEVDT(cat, code_list, desc_list, field, out return_result, out return_evdt, SearchDepth.SearchDefault);
        }
        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, out string return_result, out DateTime return_evdt, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
                return_evdt = PFSDBUtility.DBToDateTime(query.First().EVENT_DATETIME);
            }
            else
            {
                return_result = "";
                return_evdt = DateTime.MinValue;
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }

        private bool GetEVDT(string cat, string code_list, string desc_list, string res, string field, int comparison, DateTime compevdt, out DateTime return_evdt)
        {
            return GetEVDT(cat, code_list, desc_list, field, res, comparison, compevdt, out return_evdt, SearchDepth.SearchDefault);
        }
        private bool GetEVDT(string cat, string code_list, string desc_list, string field, string res, int comparison, DateTime compevdt, out DateTime return_evdt, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, res);
            if (compevdt != DateTime.MinValue)
            {
                if (comparison == 0) // equal
                    query = query.Where(e => e.EVENT_DATETIME == compevdt);
                else if (comparison == 1) // GTE
                    query = query.Where(e => e.EVENT_DATETIME >= compevdt);
                else if (comparison == 2) // GT
                    query = query.Where(e => e.EVENT_DATETIME > compevdt);
                else if (comparison == 3) // LT
                    query = query.Where(e => e.EVENT_DATETIME < compevdt);
            }
            query = query.OrderByDescending(e => e.EVENT_DATETIME);
            Program.VerboseAudit("getevdt count=" + query.Count());
            if (query.Count() > 0)
            {
                return_evdt = PFSDBUtility.DBToDateTime(query.First().EVENT_DATETIME);
            }
            else
            {
                return_evdt = DateTime.MinValue;
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, res, search_depth));
            return (return_evdt != DateTime.MinValue);
        }

        //======================================================================================
        // Mapping starts here
        //======================================================================================

        private void Check_1_2_3()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 1. ADL Self");
            Program.VerboseAudit("MH 2. ADL Assist");
            Program.VerboseAudit("MH 3. ADL Complete/2+");
            Program.VerboseAudit("---------------");

            exclude_periop_data = true;

            //if (_pat.age < 4.0) SetInd(4, "Age < 4 years");
            //if (_pat.age >= 4.0 && _pat.age < 7.0) SetInd(2, "Age 4-6 years");

            string reslist = "";

            //reslist = "Ambulate in hall,Ambulate in room,Bathroom privileges,Bedrest,Bike,Chair,Commode,Dangle,Supervised exercise,Held,Stand at bedside,Tilt table,Turn,Up in chair,Wagon,In bed,bedpan";
            //bool B1 = Exists("", "9990000305560", "", "", reslist, SearchDepth.SearchSince25Hrs);
            //reslist = "1 Assist,2 Assist,3 Assist,>4 Assist,4 or more Assist";
            //bool B2 = Exists("", "9993040109530", "", "", reslist, SearchDepth.SearchSince25Hrs);
            //reslist = "Lying right side,Lying left side,Supine,Semi Fowler,Pillow support,Prone,Sitting,Standing,Up in chair,Micro turn left,Micro turn right,Do Not Turn,Unstable to turn,Turn right side,Turn left side";
            //bool B3 = Exists("", "9990000400604", "", "", reslist, SearchDepth.SearchSince25Hrs);
            //if (B1 && B2) SetInd(2, "B1 and B2");
            //if (B2 && B3) SetInd(2, "B2 and B3");

            string reslist1 = "Ambulate in hall,Ambulate in room,Bathroom privileges,Bedrest,Bike,Chair,Commode,Dangle,Supervised exercise,Held,Stand at bedside,Tilt table,Turn,Up in chair,Wagon,In bed,bedpan";
            string reslist2 = "1 Assist,2 Assist,3 Assist,>4 Assist,4 or more Assist";
            string reslist3 = "Lying right side,Lying left side,Supine,Semi Fowler,Pillow support,Prone,Sitting,Standing,Up in chair,Micro turn left,Micro turn right,Do Not Turn,Unstable to turn,Turn right side,Turn left side";
            if (CodependenceExists("9990000305560", reslist1, "9993040109530", reslist2, SearchDepth.SearchSince25Hrs))
                SetInd(2, "B1 and B2 have a matching time.");
            if (CodependenceExists("9993040109530", reslist2, "9990000400604", reslist3, SearchDepth.SearchSince25Hrs))
                SetInd(2, "B2 and B3 have a matching time.");


            //reslist = "Assist,Total Care";
            //bool B4 = Exists("", "9991025006475", "", "", reslist, SearchDepth.SearchSince25Hrs);
            //reslist = "Left side,Left tilt,Right side,Right tilt,Semi-fowler,Fowler,Knee chest,Chair,Rocking chair,Supine,Trendelenberg,Squatting,Standing,Kneeling,Walking,Prone";
            //bool B5 = Exists("", "1028000027", "", "", reslist, SearchDepth.SearchSince25Hrs);
            //if (B4 && B5) SetInd(2, "B4 and B5");

            reslist1 = "Assist,Total Care";
            reslist2 = "Left side,Left tilt,Right side,Right tilt,Semi-fowler,Fowler,Knee chest,Chair,Rocking chair,Supine,Trendelenberg,Squatting,Standing,Kneeling,Walking,Prone";
            if (CodependenceExists("9991025006475", reslist1, "1028000027", reslist2, SearchDepth.SearchSince25Hrs))
                SetInd(2, "B4 and B5 have a matching time.");


            //reslist = "1 Assist,2 Assist,3 Assist,>4 Assist,Supervised";
            //bool B6 = Exists("", "9990007060350", "", "", reslist, SearchDepth.SearchSince25Hrs);
            //reslist = "Bathed,Chlorhexidine,Showered,Bath in a bag,Catheter care,Foley care,Peri care,Hair washed,Hair dried/curled,Shaved";
            //bool B7 = Exists("", "9990000342030", "", "", reslist, SearchDepth.SearchSince25Hrs);
            //if (B6 && B7) SetInd(2, "B6 and B7");
            //reslist = "Partial assist,Complete assist";
            //bool B8 = Exists("", "9990000305650", "", "", reslist, SearchDepth.SearchSince25Hrs);
            //if (B7 && B8) SetInd(2, "B7 and B8");

            reslist1 = "1 Assist,2 Assist,3 Assist,>4 Assist";
            reslist2 = "Bathed,Chlorhexidine,Showered,Bath in a bag,Catheter care,Foley care,Peri care,Hair washed,Hair dried/curled,Shaved";
            reslist3 = "Partial assist,Complete assist";
            string reslist4 = "Complete bath,Partial bath,Wipes,Solution,Showered";
            if (CodependenceExists("9990007060350", reslist1, "9990000342030", reslist2, SearchDepth.SearchSince25Hrs))
                SetInd(2, "B6 and B7 have a matching time.");
            if (CodependenceExists("9990000342030", reslist2, "9990000305650", reslist3, SearchDepth.SearchSince25Hrs))
                SetInd(2, "B7 and B8 have a matching time.");
            if (CodependenceExists("9990007060350", reslist1, "9993040109700", reslist4, SearchDepth.SearchSince25Hrs))
                SetInd(2, "B6 and B81 have a matching time.");

            reslist = "Needs assist,Supervised,Total assist";
            SetIndIfResultContains(2, "", "9990000700380", "", "", reslist, SearchDepth.SearchSince25Hrs);
            reslist = "";
            bool c10 = SetIndIfResultContains(2, "", "3043040100003", "", "", reslist, SearchDepth.SearchSince25Hrs);
            bool c11 = SetIndIfResultContains(2, "", "9993040100004", "", "", reslist, SearchDepth.SearchSince25Hrs);
            bool c12 = SetIndIfResultContains(2, "", "3043040101422", "", "", reslist, SearchDepth.SearchSince25Hrs);
            bool c13 = SetIndIfResultContains(2, "", "9993040101423", "", "", reslist, SearchDepth.SearchSince25Hrs);

            bool b60 = SetIndIfResultContains(2, "", "9990304000118", "", "", "3,feeding", SearchDepth.SearchSince25Hrs);
            bool b61 = SetIndIfResultContains(2, "", "9990000002113", "", "", "3,feeding", SearchDepth.SearchSince25Hrs);

            reslist = "";
            bool ADLLDA10 = SetIndIfResultContains(2, "", "3045001094", "", "", reslist, SearchDepth.SearchSince25Hrs);
            reslist = "NG tube,OG tube,G-tube,J-tube,ND tube,OD tube,PEG-tube";
            bool ADLLDA11 = SetIndIfResultContains(2, "", "9991733565652", "", "", reslist, SearchDepth.SearchSince25Hrs);
            bool ADLLDA12 = SetIndIfResultContains(2, "", "9991733569855", "", "", "", SearchDepth.SearchSince25Hrs);
            bool ADLLDA13 = SetIndIfResultContains(2, "", "9993040304520", "", "", "", SearchDepth.SearchSince25Hrs);

            reslist = "Yes";
            bool c15 = SetIndIfResultContains(2, "", EXACT_MATCH_PREFIX + "23", "", "", reslist, SearchDepth.SearchSince25Hrs);
            reslist = "";
            bool c16 = SetIndIfResultContains(2, "", "3045001090", "", "", reslist, SearchDepth.SearchSince25Hrs);
            bool c17 = SetIndIfResultContains(2, "", "3045001089", "", "", reslist, SearchDepth.SearchSince25Hrs);
            reslist = "Yes";
            bool c18 = SetIndIfResultContains(2, "", EXACT_MATCH_PREFIX + "16", "", "", reslist, SearchDepth.SearchSince13Hrs);

            reslist = "";
            bool ADLLDA1 = SetIndIfResultContains(2, "", "3045001088", "", "", reslist, SearchDepth.SearchSince25Hrs);
            bool ADLLDA2 = SetIndIfResultContains(2, "", "9990007085310", "", "", reslist, SearchDepth.SearchSince25Hrs);
            bool ADLLDA3 = SetIndIfResultContains(2, "", "9993040104906", "", "", reslist, SearchDepth.SearchSince25Hrs);
            bool ADLLDA4 = SetIndIfResultContains(2, "", "9990000396150", "", "", reslist, SearchDepth.SearchSince25Hrs);
            bool ADLLDA5 = SetIndIfResultContains(2, "", "9990007085490", "", "", reslist, SearchDepth.SearchSince25Hrs);
            bool ADLLDA6 = SetIndIfResultContains(2, "", "9993040021276", "", "", reslist, SearchDepth.SearchSince25Hrs);

            reslist = "1 Assist,2 Assist,3 Assist,>4 Assist";
            bool B6 = Exists("", "9990007060350", "", "", reslist, SearchDepth.SearchSince25Hrs);
            reslist = "Done";
            bool B26 = Exists("", "9991600100064", "", "", reslist, SearchDepth.SearchSince25Hrs);
            if (B6 && B26) SetInd(2, "B6 and B26");

            reslist = "Stand-by assist ,One staff assist ,Two staff assist ";
            bool C27 = SetIndIfResultContains(2, "", "9991600100065", "", "", reslist, SearchDepth.SearchSince25Hrs);//c27
            reslist = "Stand-by assist,One staff assist,Two staff assist";
            bool C28 = SetIndIfResultContains(2, "", "9991600100066", "", "", reslist, SearchDepth.SearchSince25Hrs);//c28
            reslist = "Bedpan,Catheter,Commode,Incontinence pad";
            bool C29 = SetIndIfResultContains(2, "", "9991600100067", "", "", reslist, SearchDepth.SearchSince25Hrs);//c29
            reslist = "Bed bath";
            bool C30 = SetIndIfResultContains(2, "", "9991600100068", "", "", reslist, SearchDepth.SearchSince25Hrs);//c30

            reslist = "3";
            SetIndIfResultContains(2, "", "9993040000407", "", "", reslist, SearchDepth.SearchSince25Hrs);
            SetIndIfResultBetween(2, "", "3045001023", "", "", 9, 12, SearchDepth.SearchSince25Hrs);
            SetIndIfResultBetween(2, "", "9993040001207", "", "", 9, 12, SearchDepth.SearchSince25Hrs);

            reslist = "Digital stimulation,Enema,Manual evacuation/disimpaction";
            bool b77 = SetIndIfResultContains(2, "", "9993040109907", "", "", reslist, SearchDepth.SearchSince13Hrs);
            bool b78 = SetIndIfResultContains(2, "", "9990000016084", "", "", "", SearchDepth.SearchSince13Hrs);
            string found_what;
            bool b79 = Check_b79(out found_what);
            if (b79) SetInd(2, "Toileting: " + found_what);
            bool b80 = SetIndIfResultContains(2, "", "9990000305560", "", "", "bedpan", SearchDepth.SearchSince13Hrs);

            bool d77 = b77;
            bool d78 = b78;
            bool d79 = b79;
            bool d80 = b80;


            //FOR ADL Extended rows D-1 through D-19, must include charting selection from each of the bullets below on the shift:
            //   - D1 & D2 or D2&D3 or D4&D5(mobility)
            //   - D6 & 7 or D7&8 or D26 (hygiene)
            //   - Any of D - 9, D - 10, D - 11, D - 12, D - 13, ADL - LDA - 10 to 13(feeding)
            //   - any row between D - 15 to D - 18 or ADL - LDA - 1, 2, 3, 4, 5 or 6(toileting)
            //                OR
            // For lines D - 2 and D - 6, the selection is: 2 Assist, 3 Assist or > 4 Assist(See notes on those lines)
            //For lines D - 27 & D28 select for ""Two Staff Assist""(See notes on those lines)
            //

            reslist = "Ambulate in hall,Ambulate in room,Bathroom privileges,Bedrest,Bike,Chair,Commode,Dangle,Supervised exercise,Held,Stand at bedside,Tilt table,Turn,Up in chair,Wagon,In bed,bedpan";
            bool c1 = Exists("", "9990000305560", "", "", reslist, SearchDepth.SearchSince25Hrs);
            reslist = "1 Assist,2 Assist,3 Assist,>4 Assist,4 or more Assist";
            bool c2 = Exists("", "9993040109530", "", "", reslist, SearchDepth.SearchSince25Hrs);
            reslist = "Lying right side,Lying left side,Supine,Semi Fowler,Pillow support,Prone,Sitting,Standing,Up in chair,Micro turn left,Micro turn right,Do Not Turn,Unstable to turn,Turn right side,Turn left side";
            bool c3 = Exists("", "9990000400604", "", "", reslist, SearchDepth.SearchSince25Hrs);
            reslist = "Assist,Total Care";
            bool c4 = Exists("", "9991025006475", "", "", reslist, SearchDepth.SearchSince25Hrs);
            reslist = "Left side,Left tilt,Right side,Right tilt,Semi-fowler,Fowler,Knee chest,Chair,Rocking chair,Supine,Trendelenberg,Squatting,Standing,Kneeling,Walking,Prone";
            bool c5 = Exists("", "1028000027", "", "", reslist, SearchDepth.SearchSince25Hrs);
            reslist = "1 Assist,2 Assist,3 Assist,>4 Assist";
            bool c6 = Exists("", "9990007060350", "", "", reslist, SearchDepth.SearchSince25Hrs);
            reslist = "Bathed,Chlorhexidine,Showered,Bath in a bag,Catheter care,Foley care,Peri care";
            bool c7 = Exists("", "9990000342030", "", "", reslist, SearchDepth.SearchSince25Hrs);
            bool c81 = Exists("", "9993040109700", "", "", reslist4, SearchDepth.SearchSince25Hrs);
            reslist = "Partial assist,Complete assist";
            bool c8 = Exists("", "9990000305650", "", "", reslist, SearchDepth.SearchSince25Hrs);

            reslist = "Needs assist,Total assist";
            bool c9 = Exists("", "9990000700380", "", "", reslist, SearchDepth.SearchSince25Hrs);

            bool c19 = (ADLLDA1 || ADLLDA2 || ADLLDA3 || ADLLDA4 || ADLLDA5 || ADLLDA6);

            reslist = "Done";
            bool C26 = Exists("", "9991600100064", "", "", reslist, SearchDepth.SearchSince25Hrs);//c26

            bool c14 = ADLLDA10 || ADLLDA11 || ADLLDA12 || ADLLDA13;
            if ((c14 || c5))
            {
                reslist = "2 Assist,3 Assist,>4 Assist";
                SetIndIfResultContains(3, "", "9990007060350", "", "", reslist, SearchDepth.SearchSince25Hrs);
            }
            //   - C1 & C2 or C2&C3 or C4&C5(mobility)
            //   - C6 & 7 or C7&8(hygiene, please carry over for night shift)
            //   -Any of C-9, C - 10, C - 11, C - 12, C - 13, ADL - LDA - 10 or ADL-LDA - 11(feeding, please carry over for night shift)
            //   -any row between C - 15 to C-21 or ADL-LDA - 1, 2, 3, 4 ro 5(toileting)
            bool Cgroup1 = ((c1 && c2) || (c2 && c3) || (c4 && c5));
            bool Cgroup2 = ((c6 && (c7 || c81)) || ((c7 || c81) && c8) || (c6 && C26));
            bool Cgroup3 = (c9 || c10 || c11 || c12 || c13 || ADLLDA10 || ADLLDA11 || ADLLDA12 || ADLLDA13 || b60 || b61);
            bool Cgroup4 = (c15 || c16 || c17 || c18 || c19 || d77 || d78 || d79 || d80);
            Program.Audit("D groups: Group1=((d1 & d2) or (d2 & d3) or (d4 & d5))=" + (Cgroup1 ? "Y" : "N") + " Group2=((d6 & d7d81) or (d7d81 & d8) or (d6 & d26))=" + (Cgroup2 ? "Y" : "N") + " Group3=(any one of d9-d13,ADL-LDA 10-13)=" + (Cgroup3 ? "Y" : "N") + " Group4=(any one of d15-d19,ADL-LDA 1-6,d77-80)=" + (Cgroup4 ? "Y" : "N"));
            if (Cgroup1 && !Cgroup2)
            {
                Program.Audit("Mobility is true and Hygiene is false: Force Hygiene Dgroup2 to be true.");
                Cgroup2 = true;
            }
            int CgroupSUM = (Cgroup1 ? 1 : 0) + (Cgroup2 ? 1 : 0) + (Cgroup3 ? 1 : 0) + (Cgroup4 ? 1 : 0);
            if (CgroupSUM >= 3) SetInd(3, "All 4 D groups positive.");

            if ((C26 || C30) && C27 && (C28 || C29))
                SetInd(3, "[D26 or D30] and D27 and [D28 or D29]");

            reslist = "Strong stimuli to arouse";
            SetIndIfResultContains(3, "", "3045001046", "", "", reslist, SearchDepth.SearchSince25Hrs);//c33



            if (!_inds[1].is_checked && !_inds[2].is_checked && !_inds[3].is_checked)
            {
                SetInd(1, "Defaulting to ADL Self");
            }

        }

        private bool CodependenceExists(string code1, string reslist1, string code2, string reslist2, SearchDepth s)
        {
            int ct2 = 0;
            int codepct = 0;

            var query1 = StartNewQuery(s);
            query1 = AndItemFilter(query1, "", code1, "", "", reslist1);
            query1 = query1.OrderBy(e => e.EVENT_DATETIME);
            int ct1 = query1.Count();
            if (ct1 == 0) return false;

            foreach (var item in query1)
            {
                var query2 = StartNewQuery(s);
                query2 = AndItemFilter(query2, "", code2, "", "", reslist2);
                query2 = query2.Where(e => e.EVENT_DATETIME == item.EVENT_DATETIME);
                ct2 = query2.Count();
                if (ct2 > codepct) codepct = ct2;
            }
            
            return (codepct >= 1);
        }
        private bool Check_b79(out string found_what)
        {
            bool b = false;
            string d = "";
            var query = StartNewQuery(SearchDepth.SearchSince13Hrs);
            query = query.Where(e => e.CODE.ToUpper().StartsWith("MED"));
            query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;given") && (e.DESCRIPTION.ToUpper().Contains("PEG3350 100 GRAM-SOD SUL") || e.DESCRIPTION.ToUpper().Contains("PEG 3350-ELECTROLYTES 236")));
            query = query.OrderByDescending(e => e.EVENT_DATETIME);
            //query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;given"));
            b = (query.Count() > 0);
            if (b) d = query.First().DESCRIPTION;
            found_what = d;
            return b;
        }

        private void Check_4()
        {
            string reslist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 4. ADL Supervision");
            Program.VerboseAudit("---------------");
            exclude_periop_data = true;

            if (_inds[2].is_checked || _inds[3].is_checked) SetInd(4, "Rule to trigger ADL Supervision when ADL Assist or ADL Complete.");

            var reslist1 = "Ambulate in hall,Ambulate in room,Bathroom privileges,Bedrest,Bike,Chair,Commode,Dangle,Supervised exercise,Held,Stand at bedside,Tilt table,Turn,Up in chair,Wagon,In bed,bedpan";
            //bool E1 = Exists("", "9990000305560", "", "", reslist, SearchDepth.SearchSince25Hrs);
            var reslist3 = "Lying right side,Lying left side,Supine,Semi Fowler,Pillow support,Prone,Sitting,Standing,Up in chair,Micro turn left,Micro turn right,Do Not Turn,Unstable to turn,Turn right side,Turn left side";
            //bool E3 = Exists("", "9990000400604", "", "", reslist, SearchDepth.SearchSince25Hrs);
            var reslist2 = "Supervision required";
            //bool E2 = Exists("", "9993040109530", "", "", reslist, SearchDepth.SearchSince25Hrs);
            //if (E2 && (E1 || E3))
            //    SetInd(4, "E2 and (E1 or E3)");
            if (CodependenceExists("9993040109530", reslist2, "9990000305560", reslist1, SearchDepth.SearchSince25Hrs))
                SetInd(4, "E1 and E2 have a matching time.");
            if (CodependenceExists("9993040109530", reslist2, "9990000400604", reslist3, SearchDepth.SearchSince25Hrs))
                SetInd(4, "E1 and E3 have a matching time.");

            reslist = "Supervised";
            SetIndIfResultContains(4, "", "9990000700380", "", "", reslist, SearchDepth.SearchSince25Hrs);
            SetIndIfResultContains(4, "", "9993040009678", "", "", "", SearchDepth.SearchSince25Hrs);
            reslist = "Locked Bathroom,Post-prandial monitoring";
            SetIndIfResultContains(4, "", "9993040000395", "", "", reslist, SearchDepth.SearchSince25Hrs);

            //SetIndIfResultContains(4, "", "9990007060350", "", "", reslist, SearchDepth.SearchSince25Hrs);

            reslist1 = "Supervised";
            reslist2 = "Bathed,Chlorhexidine Bath,Showered,Bath in a bag,Catheter care";
            reslist2 += ",Foley care,Peri care,Hair washed,Nail care,Make-up applied,Hair dryed/curled,Shaved";
            if (CodependenceExists("9990007060350", reslist1, "9990000342030", reslist2, SearchDepth.SearchSince25Hrs))
                SetInd(4, "E6 and E7 have a matching time.");

        }


        private void Check_5_6()
        {
            string reslist;
            bool s1, s2, s3, s4;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 5. Cognitive Support");
            Program.VerboseAudit("MH 6. Cognitive Support q1H");
            Program.VerboseAudit("---------------");
            exclude_periop_data = true;

            reslist = "Confused";
            SetIndIfResultContains(5, "", "3045001046", "", "", reslist);


            reslist = "Oriented to person,Oriented to place,Oriented to time";
            if (!AllOriented("9990000301870"))
            {
                reslist = "Disoriented X4,Disoriented X 4,Disoriented to place,Disoriented to time,Disoriented to situation,Disoriented to person";
                SetIndIfResultContains(5, "", "9990000301870", "", "", reslist);

                //any UP TO 2 ONLY of these
                s1 = Exists("", "9990000301870", "", "", "Disoriented x3,Disoriented x 3", SearchDepth.SearchSince25Hrs);
                s2 = Exists("", "9990000301870", "", "", "Oriented to person", SearchDepth.SearchSince25Hrs);
                s3 = Exists("", "9990000301870", "", "", "Oriented to place", SearchDepth.SearchSince25Hrs);
                s4 = Exists("", "9990000301870", "", "", "Oriented to time", SearchDepth.SearchSince25Hrs);
                int CgroupSUM = (s1 ? 1 : 0) + (s2 ? 1 : 0) + (s3 ? 1 : 0) + (s4 ? 1 : 0);
                if (CgroupSUM >= 1 && CgroupSUM <= 2) SetInd(5, "One or Two of: Disoriented x3,Oriented to person,Oriented to place,Oriented to time");

            }

            if (!AllOriented("9993040105631"))
            {
                reslist = "Disoriented X4,Disoriented X 4,Disoriented to place,Disoriented to time,Disoriented to situation,Disoriented to person";
                SetIndIfResultContains(5, "", "9993040105631", "", "", reslist);

                //any UP TO 2 ONLY of these
                s1 = Exists("", "9993040105631", "", "", "Disoriented x3,Disoriented x 3", SearchDepth.SearchSince25Hrs);
                s2 = Exists("", "9993040105631", "", "", "Oriented to person", SearchDepth.SearchSince25Hrs);
                s3 = Exists("", "9993040105631", "", "", "Oriented to place", SearchDepth.SearchSince25Hrs);
                s4 = Exists("", "9993040105631", "", "", "Oriented to time", SearchDepth.SearchSince25Hrs);
                int CgroupSUM = (s1 ? 1 : 0) + (s2 ? 1 : 0) + (s3 ? 1 : 0) + (s4 ? 1 : 0);
                if (CgroupSUM >= 1 && CgroupSUM <= 2) SetInd(5, "One or Two of: Disoriented x3,Oriented to person,Oriented to place,Oriented to time");

            }


            SetIndIfResultContains(5, "", "9990000301880", "", "", AVOID_NEGATIVE + "Short term memory loss");
            reslist = "Unable to follow commands";
            SetIndIfResultContains(5, "", "9990000301880", "", "", reslist);
            reslist = "4,3,confused,inappropriate words";
            SetIndIfResultContains(5, "", "3045001024", "", "", reslist);
            SetIndIfResultBetween(5, "", "9990000398002", "", "", 4, 7, SearchDepth.SearchSince25Hrs);
            SetIndIfResultBetween(5, "", "9990000398004", "", "", 4, 7, SearchDepth.SearchSince25Hrs);
            SetIndIfResultBetween(5, "", "9990000398006", "", "", 4, 7, SearchDepth.SearchSince25Hrs);

            reslist = "Disoriented for place or person";
            SetIndIfResultContains(5, "", "9990000398010", "", "", reslist);
            reslist = "Hallucination,Illusions,Patient responding to internal Stimuli";
            SetIndIfResultContains(5, "", "9993040105625", "", "", reslist);
            reslist = "Auditory,Visual,Tactile,Command,Olfactory";
            SetIndIfResultContains(5, "", "9991540100181", "", "", reslist);
            reslist = "Confused,Disoriented,Incoherent";
            SetIndIfResultContains(5, "", "9993040105626", "", "", reslist);

            reslist = "Delusions,Paranoia";
            SetIndIfResultContains(5, "", "9993040105627", "", "", reslist);
            reslist = "Persecution,Grandeur,Obsessions,Religiosity,Phobias,Influence,Reference,Sexual delusions,Somatization,Thought broadcasting,Thought insertion,Thought withdrawal";
            SetIndIfResultContains(5, "", "9991540100180", "", "", reslist);
            reslist = "Disoriented X4";
            SetIndIfResultContains(5, "", "9993040105631", "", "", reslist);
            reslist = "Impaired short term memory";
            SetIndIfResultContains(5, "", "9991540100198", "", "", reslist);

            //if #12 or #13 gets triggered, then trigger #6.
            bool i16 = Exists("", "9990000301870", "", "", "Developmentally delayed", SearchDepth.SearchSince25Hrs);
            if (i16)
                SetIndIfResultContains(5, "", "9993040000410", "", "", "2,3");

            reslist = "Developmentally Delayed,Impaired retention";
            SetIndIfResultContains(5, "", "9993040105632", "", "", reslist);

        }
        private bool AllOriented(string code1)
        {
            int ct = 0;

            var query = StartNewQuery(SearchDepth.SearchSince25Hrs);    // add custom time range below
            query = query.Where(e => e.CODE.StartsWith(code1));
            query = query.Where(e => (e.RESULT.ToLower().StartsWith("oriented to person") || e.RESULT.ToLower().Contains(";oriented to person"))
            && (e.RESULT.ToLower().StartsWith("oriented to place") || e.RESULT.ToLower().Contains(";oriented to place"))
            && (e.RESULT.ToLower().StartsWith("oriented to time") || e.RESULT.ToLower().Contains(";oriented to time")));
            ct = query.Count();
            if (ct > 0)
                Program.VerboseAudit("All 3 Orientation found: " + query.Count());
            return (ct > 0);
        }

        private void Check_7_8_9()
        {
            string reslist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 7. Safety Management q30m");
            Program.VerboseAudit("MH 8. Safety Management q15m");
            Program.VerboseAudit("MH 9. Safety Management q5m");
            Program.VerboseAudit("---------------");
            exclude_periop_data = false;
            SetInd(8, "Default #8 for MH.");

            // 9993040009234 + Q30 min 
            CheckSafety();

            reslist = "24 hours";
            SetIndIfResultContains(9, "", "9990000300101", "", "", reslist);
            reslist = "4 hour,2 hour,1 hour";
            SetIndIfResultContains(9, "", "9990000300001", "", "", reslist);
            reslist = "1:1 Nsg care for seclusion for the first hour";
            reslist += ",Constant Nsg care for restraints for the first hour";
            reslist += ",Record behavior every 5 minutes";
            reslist += ",Review strengths";
            reslist += ",Administration of medication";
            reslist += ",Respiratory status assessed";
            reslist += ",Restraints checked every 15 minutes";
            reslist += ",RN hourly assessment";
            reslist += ",Offer fluids every 2 hours";
            reslist += ",Offer toileting every 2 hours";
            reslist += ",ROM to joints every 2 hours";
            reslist += ",Hygiene PRN";
            reslist += ",Meals and snacks offered";
            reslist += ",Other";
            SetIndIfResultContains(9, "", "9990007096219", "", "", reslist);
            reslist = "Implemented - Seclusion Treatment Policy";
            reslist += ",Implemented - Behavioral Restraint Policy";
            reslist += ",Procedure explained to patient";
            reslist += ",Reason for seclusion";
            reslist += ",Informed the patient of the goal";
            reslist += ",Mattress checked for dangerous items";
            reslist += ",check room for lighting";
            reslist += ",Dangerous items and jewelry removed";
            reslist += ",Respiratory status monitored";
            reslist += ",Pt assisted in achieving goal";
            SetIndIfResultContains(9, "", "9990007096227", "", "", reslist);
        }
        private int CheckSafety()
        {
            int ind = 0, ct = 0;
            string[] safety_list = { "q30", "q15", "q5", "Line of sight", "Continuous observation by RN with patient", "Continuous observation by non-RN staff with patient", "Continuous observation by two staff with patient" };

            var query = StartNewQuery(SearchDepth.SearchSince25Hrs);
            query = query.Where(e => e.CODE.StartsWith("9993040009234"));
            query = query.Where(e => safety_list.Any(item => e.RESULT.ToLower().StartsWith(item.ToLower())));
            query = query.OrderByDescending(e => e.EVENT_DATETIME);
            ct = query.Count();
            if (ct > 0)
            {
                string res = query.First().RESULT;
                ind = 9;
                if (res.ToLower().StartsWith("q30")) ind = 7;
                if (res.ToLower().StartsWith("q15")) ind = 8;
                SetInd(ind, "Latest result is = " + res);
            }
            return ind;
        }


        private void Check_10_11_12_13_14()
        {
            string reslist;
            //int ct;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 10. Behavior/Emotional Management");
            Program.VerboseAudit("MH 11. Behavior/Emotional Mgmt-q4H");
            Program.VerboseAudit("MH 12. Behavior/Emotional Mgmt-q2H");
            Program.VerboseAudit("MH 13. Behavior/Emotional Mgmt-q1H");
            Program.VerboseAudit("MH 14. Behavior/Emotional Mgmt-q30m");
            Program.VerboseAudit("---------------");
            exclude_periop_data = false;
            SetInd(10, "Default #10 for MH.");
            SetIndIfResultContains(12, "", "9993040105622", "", "", "Angry,Manic");

            //reslist = "Agitated,Behavior plan,Catatonic,Combative,Compulsive,Crying";
            //reslist += ",Demanding,Destructive,Disorganized,Echopraxic,Exit seeking,Fussy";
            //reslist += ",Guarded,Hostile,Hyperactive,Impulsive,Intrusive,Motor perseveration";
            //reslist += ",Oppositional,Pacing,Preoccupied,Disruptive,Psychomotor retardation";
            //reslist += ",Pushing limits,Reckless,Resistant to care,Rigid,Ritualistic,Tearful";
            //reslist += ",Staff seeking,Seclusive,Sexually inappropriate,Tics,Withdrawn,dismissive";
            //bool k1 = Exists("", "9993040105629", "", "", reslist, SearchDepth.SearchSince13Hrs);
            reslist = "Compulsive,Crying,Crying inappropriately,Impulsive,Resistant to care,Tearful,Withdrawn";
            SetIndIfResultContains(11, "", "9993040105629", "", "", reslist);
            reslist = "Multiple Staff for acute behavioral intervention";
            SetIndIfResultContains(11, "", "9993040000352", "", "", reslist);

            reslist = "Agitated,Catatonic,Delusional,Hallucinating,Pacing,Reckless,Seclusive,Staff Seeking,Hyperactive";
            SetIndIfResultContains(12, "", "9993040105629", "", "", reslist);
            reslist = "Demanding,Disruptive,Exit seeking,Pushing limits,Sexually inappropriate";
            SetIndIfResultContains(13, "", "9993040105629", "", "", reslist);
            reslist = "Combative,Destructive,Hostile";
            SetIndIfResultContains(14, "", "9993040105629", "", "", reslist);

            if (_pat.age < 18.0)
            {
                reslist = "Crying inappropriately,Demanding,Disruptive,Hyperactive,Intrusive,Pushing limits,Sexually inappropriate";
                SetIndIfResultContains(14, "", "9993040105629", "", "", reslist);
            }


            reslist = "Agitated,Combative,Compulsive,Crying,Destructive,Disorganized,";
            reslist += "Disruptive,Guarded,Hostile,Hyperactive,Intrusive,Oppositional,Pacing";
            reslist += ",Preoccupied,Pushing limits,Reckless,Resistant to care,Rigid,Ritualistic";
            reslist += ",Tearful,Staff seeking,Sexually inappropriate,Tics,Withdrawn";
            bool k3 = Exists("", "9993040009823", "", "", reslist, SearchDepth.SearchSince13Hrs);
            reslist = "Angry,Anxious,Apathetic,Depressed,Dysphoric,Elevated,Euphoric,Fearful,Guilty,Irritable,Manic,Sad";
            bool k4 = Exists("", "9993040105622", "", "", reslist, SearchDepth.SearchSince13Hrs);
            reslist = "Acts of violence,Threats of violence";
            bool k6 = Exists("", "9993040105640", "", "", reslist, SearchDepth.SearchSince13Hrs);
            reslist = "Disruptive";
            bool k7 = Exists("", "9993040000405", "", "", reslist, SearchDepth.SearchSince13Hrs);
            reslist = "Hallucination,Illusions,Patient responding to internal Stimuli";
            bool k8 = Exists("", "9993040105625", "", "", reslist, SearchDepth.SearchSince13Hrs);
            reslist = "Auditory,Visual,Tactile,Command,Olfactory";
            bool k9 = Exists("", "9991540100181", "", "", reslist, SearchDepth.SearchSince13Hrs);
            reslist = "Persecution,Grandeur,Obsessions,Religiosity,Phobias,Influence";
            reslist += ",Reference,Sexual delusions,Somatization,Thought broadcasting";
            reslist += ",Thought insertion,Thought withdrawal";
            bool k10 = Exists("", "9991540100180", "", "", reslist, SearchDepth.SearchSince13Hrs);

            reslist = "1:1 RN Time,1:1 Staff Time,Active listening,Assign a task,Aromatherapy,Assisted relaxation";
            reslist += ",Consistent response,Emotional support given,Exposure therapy,Limit setting,Medicated";
            reslist += ",Neutral response,Physical redirection,Quiet room,Reorientation";
            reslist += ",Time out,Verbal redirection,Following behavior safety plan";
            reslist += ",Multiple Staff for acute behavioral intervention,Other";
            //bool k2 = SetIndIfResultContains(10, "", "9993040000352", "", "", reslist);
            //if (k1 || k3 || k4 || k6 || k7 || k8 || k9 || k10)
            //{
            //    List<gBucket> buckets;
            //    buckets = new List<gBucket>();
            //    AddBuckets(buckets, "", "9993040000352", "", "", reslist, SearchDepth.SearchSince9Hrs);
            //    AnalyzeBuckets(buckets, 14, 40, "BH Staff Interventions q30m");
            //    AnalyzeBuckets(buckets, 13, 80, "BH Staff Interventions q1h");
            //    AnalyzeBuckets(buckets, 12, 150, "BH Staff Interventions q2h");
            //    AnalyzeBuckets(buckets, 11, 300, "BH Staff Interventions q4h");
            //}
            { // set ind based only on this 1 item 10/1/19
                if (_pat.age < 10.0)
                    SetIndIfResultContains(14, "", "9993040000352", "", "", "");

                List<gBucket> buckets;
                buckets = new List<gBucket>();
                AddBuckets(buckets, "", "9993040000352", "", "", reslist, SearchDepth.SearchSince13Hrs);
                AnalyzeBuckets(buckets, 14, 40, "BH Staff Interventions q30m");
                if (!_inds[14].is_checked)
                {
                    AnalyzeBuckets(buckets, 13, 80, "BH Staff Interventions q1h");
                    if (!_inds[13].is_checked)
                    {
                        AnalyzeBuckets(buckets, 12, 150, "BH Staff Interventions q2h");
                        if (!_inds[12].is_checked)
                            AnalyzeBuckets(buckets, 11, 300, "BH Staff Interventions q4h");
                    }
                }
            }


            reslist = "Yes,1";
            SetIndIfResultContains(10, "", "9993040105637", "", "", reslist);

            //            In addition to the possibility of the above rows(K - 1 to K - 10) 
            //charted every 30 minutes as noted in the frequency, the rows K-1 to K-7 
            //should automatically trigger indicator #14
            reslist = "4 hours,2 hours,1 hour";
            SetIndIfResultContains(14, "", "9990000300001", "", "", reslist);

            reslist = "Following behavior safety plan,Multiple Staff for acute behavioral intervention";
            SetIndIfResultContains(14, "", "9993040000352", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(14, "", "9993040006011", "", "", reslist);
            SetIndIfResultContains(14, "", "9993040006012", "", "", reslist);
            SetIndIfResultContains(14, "", "9993040006013", "", "", reslist);

            reslist = "1:1 Nsg care for seclusion for the first hour,Constant Nsg care for restraints for the first hour,Record behavior every 5 minutes,Review strengths/comfort measures - assist pt in reaching goal for discontinuation,Administration of medication - to help client regain previous level of functioning,Respiratory status assessed/documented each check - to ensure adequate air exchange,Restraints checked every 15 minutes - to prevent injury from cuffs constricting normal blood flow or irritation of skin,RN hourly assessment - mental, behavior and respiratory status - to minimize length of the procedure,Offer fluids every 2 hours - to provide elimination opportunities,Offer toileting every 2 hours - to provide elimination opportunities,ROM to joints every 2 hours - to prevent injury from occurring when client is in restraints,Hygiene PRN - to provide comfort and support,Meals and snacks offered - to maintain nutrition,Other";
            SetIndIfResultContains(14, "", "9990007096219", "", "", reslist);
            reslist = "Implemented - Seclusion Treatment Policy,Implemented - Behavioral Restraint Policy,Procedure explained to patient,Reason for seclusion/restraints explained,Informed the patient of the goal,Mattress checked for dangerous items,Mattress checked for dangerous items,check room for lighting, temperature and safety,Dangerous items and jewelry removed,Respiratory status monitored,Pt assisted in achieving goal for d/c";
            SetIndIfResultContains(14, "", "9990007096227", "", "", reslist);

            reslist = "Agitated,Verbally abusive,Tearful,Hallucinating,Delusional";
            SetIndIfResultContains(14, "", "9990000300113,9990000300018,9990000047549", "", "", reslist);
            reslist = "Other";
            SetIndIfResultContains(14, "", "9990000300018,9990000047549", "", "", reslist);

            //  3am run:  highest among 1900 class
            //  7am run:  highest among 1900, 2300 class
            // 11am run:  highest among 1900, 2300, 0300 class
            if (Program.g_pull_finish.Hour == 3 || Program.g_pull_finish.Hour == 7 || Program.g_pull_finish.Hour == 11)
            {
                int ind = 0;
                DateTime cedt = DateTime.MinValue;
                if (Program.g_pull_finish.Hour == 3)
                {
                    ind = GetMaxBehavInd(Program.g_pull_finish.AddHours(-8), Program.g_pull_finish, out cedt);
                }
                else if (Program.g_pull_finish.Hour == 7)
                {
                    ind = GetMaxBehavInd(Program.g_pull_finish.AddHours(-12), Program.g_pull_finish, out cedt);
                }
                else if (Program.g_pull_finish.Hour == 11)
                {
                    ind = GetMaxBehavInd(Program.g_pull_finish.AddHours(-16), Program.g_pull_finish, out cedt);
                }

                int currind = 10;
                if (_inds[10].is_checked) currind = 10;
                else if (_inds[11].is_checked) currind = 11;
                else if (_inds[12].is_checked) currind = 12;
                else if (_inds[13].is_checked) currind = 13;
                else if (_inds[14].is_checked) currind = 14;
//                Program.VerboseAudit("Current Behavioral indicator: " + currind);
                if (currind < ind)
                    SetInd(ind, "Current Behavioral indicator: " + currind + "; Promote indicator from classification datetime=" + cedt.ToString());
            }


            if (_inds[5].is_checked && (_inds[13].is_checked || _inds[14].is_checked))
                SetInd(6, "Promoting Cognitive Support to q1 Hour due to BehavMgt q1 or higher.");

        }

        private int GetMaxBehavInd(DateTime start, DateTime fin, out DateTime classdt)
        {
            int ind = 0;
            classdt = DateTime.MinValue;
            //get assessment indicator at location out time = loc_out_time
            var db = PFSDBUtility.NewPfsDataContext();        // look for the unit default admission profile
            var query = from ce in db.CLASSIFICATION_EVENTs
                        from ia in db.INDICATOR_ANSWERs
                        where (ce.CLASSIFICATION_EVENT_ID == ia.CLASSIFICATION_EVENT_ID)
                        && (ce.ENCOUNTER_ID == _pat.encounter_id)
                        && (ce.CLASSIFICATION_DATETIME >= start)
                        && (ce.CLASSIFICATION_DATETIME < fin)
                        && (ia.INDICATOR_NUMBER >= 10 && ia.INDICATOR_NUMBER <= 14)
                        select new
                        {
                            ia.INDICATOR_NUMBER,
                            ce.CLASSIFICATION_DATETIME
                        };
            if (query.Count() > 0)
            {
                //ind = query.First().INDICATOR_NUMBER;
                //classdt = query.First().CLASSIFICATION_DATETIME;
                foreach (var c in query)
                {
                    if (ind < c.INDICATOR_NUMBER)
                    {
                        ind = c.INDICATOR_NUMBER;
                        classdt = c.CLASSIFICATION_DATETIME;
                    }
                }
            }
            return ind;
        }

        // (this isn't really a Q1h count -- it is just a count) Make this a q1hr count
        //private int ReturnQ1HrCount(string code_list, string result_list)
        //{
        //    var buckets = new List<gBucket>();
        //    SetBucketSize(60);
        //    AddBuckets(buckets, "", code_list, "", "", result_list);
        //    return CountBuckets(buckets);
        //}

        //private bool IsQ1Hour(int count)
        //{
        //    return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q1H);
        //}
        //private bool IsQ2Hour(int count)
        //{
        //    return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q2H);
        //}

        private void Check_15()
        {
            string reslist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 15. Behavior Prevention Plan");
            Program.VerboseAudit("---------------");
            exclude_periop_data = false;

            reslist = "Following behavior safety plan";
            SetIndIfResultContains(15, "", "9993040000352", "", "", reslist);
            
            SetIndIfResultContains(15, "", "9993040009456", "", "", "BSP");
            SetIndIfResultContains(15, "", "99930408551", "", "", "");
            SetIndIfResultContains(15, "", "99930408552", "", "", "");
            SetIndIfResultContains(15, "", "99930408553", "", "", "");
            SetIndIfResultContains(15, "", "99930408554", "", "", "");
            SetIndIfResultContains(15, "", "99930408555", "", "", "");

        }

        private void Check_16_17()
        {
            string reslist;
            string codelist;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 16. Medication Management q4H");
            Program.VerboseAudit("MH 17. Medication Management q2H");
            Program.VerboseAudit("---------------");
            exclude_periop_data = true;

            List<gBucket> buckets;
            buckets = new List<gBucket>();
            reslist = "4 Ounces fruit juice,8 ounces fruit juice,IV / Medication";
            AddBuckets(buckets, "", "9993040000345", "", "", reslist, SearchDepth.SearchSince9Hrs);
            codelist = "9990000344170,3040007191,304000344160,30400012412,304000344150,9991070011101";
            AddBuckets(buckets, "", codelist, "", "", "", SearchDepth.SearchSince9Hrs);
            reslist = "No untoward effects noted,Use of reversal agent,Hypoxemia < 90";
            reslist += ",Hypotension of bradycardia requiring intervention";
            reslist += ",Respiratory failure requiring intervention";
            reslist += ",Cardiac arrest or death";
            reslist += ",Sedation recovery time > 60";
            reslist += ",Unplanned admission or higher level of care";
            reslist += ",Respiratory distress";
            reslist += ",Unanticipated need for anesthesia involvement";
            reslist += ",Inability to complete procedure";
            reslist += ",No responsible adult for discharge escort,Other";
            AddBuckets(buckets, "", "9991600100259", "", "", reslist, SearchDepth.SearchSince9Hrs);
            AddBuckets(buckets, "", "DIPS,ROUI,ORD06", "", "", "", SearchDepth.SearchSince9Hrs);

            if (CheckMeds(buckets))
                SetInd(16, "Med class 7,33,1,2,6 or named med");

            AnalyzeBuckets(buckets, 17, 150, "Medication Management q2H");
            AnalyzeBuckets(buckets, 16, 300, "Medication Management q4H");

            buckets = new List<gBucket>();
            int count = CheckPsychoTherapeutics(buckets);
            if (count >= 2)
            {
                SetInd(16, "Found " + count + " meds of one drugclass (37,38,44)");
                AnalyzeBuckets(buckets, 17, 150, "drugclass(37,38,44) Meds q2H");
                //AnalyzeBuckets(buckets, 16, 300, "drugclass(37,38,44) Meds q4H");
            }
        }

        private int CheckPsychoTherapeutics(List<gBucket> bucket_list)
        {
            string descript = "";
            string drugclass = "";
            string rte = "";
            string result = "";
            string[] meds_rate = { "newbag", "new bag", "rateverify", "rate verify", "restarted", "ratechange", "rate change", "started" };
            string[] medclass = { "37", "38", "44" };
            bool found = false;
            int ct = 0;
            int dclass1=0, dclass2=0, dclass3 = 0;

            var query = StartNewQuery(SearchDepth.SearchSince9Hrs);    // add custom time range below
            query = query.Where(e => e.CODE.ToLower().StartsWith("med"));
            query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;given"));
            query = query.Where(e => e.EVENT_DATETIME <= PFSUtility.MaxDateTime(_pat.unit_departure, _pat.effective_out));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            foreach (var item in query)
            {
                var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                if (arr.GetUpperBound(0) >= 3)
                {
                    descript = arr[0];
                    drugclass = arr[1];
                    rte = arr[3];
                    result = "";
                    if (arr.Length == 5) result = arr[4];
                    //Program.VerboseAudit("med desc: " + descript + "//" + "med class: " + drugclass + "//" + "med result: " + result);
                    //exclude #29, 35, 48
                    bool bres1 = result.ToLower().Contains("given");
                    if (drugclass == "37") dclass1++;
                    if (drugclass == "38") dclass2++;
                    if (drugclass == "44") dclass3++;
                    if ((medclass.Contains(drugclass)) && bres1)
                    {
                        Program.VerboseAudit("drugclass(37,38,44) med code: " + item.CODE + " desc: " + item.DESCRIPTION + "  evdt: " + item.EVENT_DATETIME.ToString());
                        ct++;
                        var b = new gBucket();
                        b.bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size);
                        b.code = item.CODE;
                        b.evdt = item.EVENT_DATETIME;
                        b.has_all_deps = true;
                        bucket_list.Add(b);
                    }
                }
            }
            return Math.Max(dclass1,Math.Max(dclass2,dclass3));

        }

        private bool CheckMeds(List<gBucket> bucket_list)
        {
            string descript = "";
            string drugclass = "";
            string rte = "";
            string result = "";
            string[] meds_rate = { "newbag", "new bag", "rateverify", "rate verify", "restarted", "ratechange", "rate change", "started" };
            string[] medclass = { "7", "33", "1", "2", "6" };
            bool found = false;

            string[] pamed1417 = { "Zofran","Ondansetron","Compazine","Prochlorperazine",
"Phenergan","Promethazine","Reglan","Metoclopramide","Marinol","Dronabinol","Ativan",
"Lorazepam","Imodium","Loperamide","Pepto-Bismol","Kaopectate","Bismuth subsalicylate",
"Lomotil","Atropine/diphenoxylate","Tincure of opium","Lactulose"};


            var query = StartNewQuery(SearchDepth.SearchSince9Hrs);    // add custom time range below
            query = query.Where(e => e.CODE.ToLower().StartsWith("med"));
            // query = query.Where(e => e.DESCRIPTION.Contains(";;;7;;;") || e.DESCRIPTION.Contains(";;;33;;;") || e.DESCRIPTION.Contains(";;;1;;;") || e.DESCRIPTION.Contains(";;;2;;;") || e.DESCRIPTION.Contains(";;;6;;;"));
            query = query.Where(e =>
                medclass.Any(item1 => e.DESCRIPTION.Contains(";;;" + item1 + ";;;")) ||
               pamed1417.Any(item2 => e.DESCRIPTION.ToUpper().StartsWith(item2.ToUpper())));
            query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;given") || e.DESCRIPTION.ToLower().Contains(";;;syringe") || e.DESCRIPTION.ToLower().ContainsAny(meds_rate) && (e.UNIT_ID == _pat.unit_id) || e.DESCRIPTION.ToLower().Contains(";;;new"));
            query = query.Where(e => e.EVENT_DATETIME <= PFSUtility.MaxDateTime(_pat.unit_departure, _pat.effective_out));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            //            Program.VerboseAudit("Num Meds found: " + query.Count());
            foreach (var item in query)
            {
                var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                if (arr.GetUpperBound(0) >= 3)
                {
                    descript = arr[0];
                    drugclass = arr[1];
                    rte = arr[3];
                    result = "";
                    if (arr.Length == 5) result = arr[4];
                }
                //Program.VerboseAudit("med desc: " + descript + "//" + "med class: " + drugclass + "//" + "med result: " + result);
                //exclude #29, 35, 48
                bool bres1 = false;
                bool bres2 = false;
                bool bclass1 = false;
                bool bclass2 = false;
                bool bclass3 = false;
                bool bmed1 = false;
                bres1 = result.ToLower().Contains("given");
                bres2 = result.ToLower().Contains("syringe") || result.ToLower().ContainsAny(meds_rate) || result.ToLower().Contains("newbag") || result.ToLower().Contains("new bag");
                bclass1 = (drugclass == "1") || (drugclass == "2") || (drugclass == "6");
                bclass2 = (drugclass == "7");
                bclass3 = (drugclass == "33") && (rte.ToLower() == "sc");
                bmed1 = pamed1417.Any(item2 => descript.ToUpper().StartsWith(item2.ToUpper()));
                if ((bres1 && (bclass2 || bclass3)) ||
                    ((bres1 || bres2) && bclass1) ||
                    bmed1)
                {
                    Program.VerboseAudit("====Med found====");
                    Program.VerboseAudit("med code: " + item.CODE + " desc: " + item.DESCRIPTION + "  evdt: " + item.EVENT_DATETIME.ToString());
                    found = true;
                    //SetInd(16, "Med class 7,33,1,2,6 or med name");
                    var b = new gBucket();
                    b.bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size);
                    b.code = item.CODE;
                    b.evdt = item.EVENT_DATETIME;
                    b.has_all_deps = true;
                    bucket_list.Add(b);
                }
            }
            return found;

        }


        private void CheckAssessment(int count, string desc)
        {
            //if (_inds[18].is_checked) return;          //skip if highest already checked
            if (count == 0) return;                    //skip if none

            // This should work the same as the original code:
            switch (FreqForCount(_pat.los_hours, count))
            {
                case Frequencies.Q30M:
                    SetInd(18, desc);
                    break;
                case Frequencies.Q1H:
                    SetInd(17, desc);
                    break;
                case Frequencies.Q2H:
                    SetInd(16, desc);
                    break;
                case Frequencies.Q4H:
                    SetInd(15, desc);
                    break;
                default:
                    break;
            }

        }

        private void ShowBuckets(List<gBucket> buckets)
        {
            string s = "";
            foreach (var e in buckets)
            {
                s = "(" + e.bucket + "," + e.code + "," + e.evdt.ToString() + ")";
                Program.VerboseAudit("Assessmt item:=" + s);
            }
        }

        private void Check_18_19_20()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 18. Physiological Assessment q4H");
            Program.VerboseAudit("MH 19. Physiological Assessment q2H");
            Program.VerboseAudit("MH 20. Physiological Assessment q1H");
            Program.VerboseAudit("---------------");

            exclude_periop_data = true;

            SetIndIfResultContains(20, "", "9991600100646", "", "", "");
            SetIndIfResultContains(20, "", "9991600100681", "", "", "");
            SetIndIfResultContains(20, "", "9991600100682", "", "", "");

            AnalyzeAssessments(18, 300);

        }

        private void AnalyzeAssessments(int ind, int bucket_size)
        {
            string codelist;
            string reslist;
            List<gBucket> buckets;

            SetBucketSize(bucket_size);

            //
            //VS group
            //
            //Need 3 values together: 
            //1.Pulse in PA - VS - 6 or PA-VS - 7
            //2.Resp in PA - VS - 8
            //3.BP in PA - VS - 9 or PA-VS - 10
            buckets = new List<gBucket>();
            codelist = "304987655,9990007096285";
            codelist += ",9993041120042,3045001041,9990304301280,9993045001043";
            codelist += ",9990304001499,3045001018,9993040103255";
            AddBuckets(buckets, "", codelist, "", "");
            Program.VerboseAudit("buckets count1=" + buckets.Count());

            bool has_trach = Exists("", "9990007070177,9990007070178,9990007070179", "", "", "", SearchDepth.SearchSince9Hrs);

            //if (_pat.age >= 8.0 && !has_trach)
            //    AddDependentBuckets(buckets, "304987657,3045001025", "", "3045001064,30454321", "", "304987666,9990304100017", "");
            //else
            //    AddDependentBuckets(buckets, "304987657,3045001025", "", "3045001064,30454321", "");

            if (_pat.age >= 8.0 && !has_trach)
            {
                codelist = "304987657,3045001025,3045001064,30454321,304987666,9990304100017,9993049900009";
                AddBuckets(buckets, "", codelist, "", "");
            }
            else
            {
                codelist = "304987657,3045001025,3045001064,30454321";
                AddBuckets(buckets, "", codelist, "", "");
            }

            Program.VerboseAudit("buckets count2=" + buckets.Count());
            reslist = "Non-rebreather mask,Nasal cannula,High flow,Aerosol mask";
            reslist += ",Bag valve device,BiPAP,Blow-by,CPAP,Face tent,Isolette,Oxyhood";
            reslist += ",Oxymask,Partial rebreather mask,Reservoir nasal cannula,Simple mask";
            reslist += ",T-piece,Trach mask,Ventilator,Venturi mask";
            reslist += ",Speaking valve,Incubator,Bag-self inflating,Bag-flow inflating,Helium oxygen,Sub-ambient Oxygen";
            AddDependentBuckets(buckets, "30454321,9990000006294", "", "3045001064", "", "3045001051", reslist);
            Program.VerboseAudit("buckets count3=" + buckets.Count());
            AnalyzeBuckets(buckets, ind, bucket_size, "VS");
            //ct = CountBuckets(buckets);
            //CheckAssessment(ct, "VS=" + ct);
            //ShowBuckets(buckets);

            //
            //Pulmonary group
            //
            buckets = new List<gBucket>();
            codelist = "3045001051";
            reslist = "Non-rebreather mask,Nasal cannula,High flow,Aerosol mask";
            reslist += ",Bag valve device,BiPAP,Blow-by,CPAP,Face tent,Isolette,Oxyhood";
            reslist += ",Oxymask,Partial rebreather mask,Reservoir nasal cannula,Simple mask";
            reslist += ",T-piece,Trach mask,Ventilator,Venturi mask";
            reslist += ",Speaking valve,Incubator,Bag-self inflating,Bag-flow inflating,Helium oxygen,Sub-ambient Oxygen";
            AddDependentBuckets(buckets, codelist, reslist, "30454321", "", "3045001064", "");

            codelist = "3045001052,3045001053";
            AddDependentBuckets(buckets, codelist, "", "30454321", "", "3045001064", "");
            codelist = "9990000302570";
            reslist = "Agonal,Apnea,Bradypnea,Cheyne-Stokes,Kussmaul";
            reslist += ",Obstructed,Periodic,Regular,Tachypnea";
            AddDependentBuckets(buckets, codelist, reslist, "3045001064", "");
            codelist = "9993040109339";
            reslist = "Regular,Irregular,Shallow,Deep";
            AddDependentBuckets(buckets, codelist, reslist, "3045001064", "");
            codelist = "9993040109337";
            reslist = "Labored,Unlabored";
            AddDependentBuckets(buckets, codelist, reslist, "3045001064", "");

            codelist = "9993040109338";
            reslist = "Abdominal muscle use,Accessory muscle use,Drooling,Gasping";
            reslist += ",Grunting,Head bobbing,Nasal flaring,Pursed lips,Retractions,Tripod position";
            AddDependentBuckets(buckets, codelist, reslist, "3045001064", "");

            codelist = "9990000302580";
            reslist = "Symmetrical,Asymmetrical,Trachea deviates right,Trachea midline";
            reslist += ",Trachea deviates left,Paradoxical,Sunken chest,Pigeon chest";
            reslist += ",Subcutaneous emphysema,No chest expansion,Cylinder shaped";
            reslist += ",Flattened,Right clavicular deformity,Left clavicular deformity";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000302390,9990000302400,9990000302410";
            reslist = "Clear,Diminished,Fine,Coarse,Rales,Rhonchi,Crackles";
            reslist += ",Expiratory wheezes,Inspiratory wheezes,Stridor,Pleural rub";
            reslist += ",Tubular,Absent";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000451120,9990000451080,9990000451040";
            reslist = "Copious,Large,Moderate,Small,None";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000315800";
            reslist = "Bed therapy,Cough assist,CPAP,EzPAP,Flutter valve";
            reslist += ",IPV device,Manual percussion,NT suction,PEP Therapy";
            reslist += ",Percusser,Postural drainage,Vibralung,Vest";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000325950";
            reslist = "Tollerated well,Tollerated fairly well,Tolerated poorly,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991733888883";
            reslist = "CPAP facial,CPAP gap present,CPAP nasal,Incubator oxygen";
            reslist += ",Laryngeal mask airway,Nasal cannula gap present,Nasal cannula high flow";
            reslist += ",Nasal cannula low flow,Nasal trumpet,Oral airway,Oxygen hood";
            reslist += ",Trach uncuffed,Trach cuffed,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991733666660";
            reslist = "Apnea,Bradycardia,Desaturation,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000344210,9990000316380,9990000344220,9990000344230,9990007096450";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "9990000344250";
            reslist = "Blow-by oxygen,Oxygen,Positive pressure ventilation,Self limiting,Suction,Tactile stimulation";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000344280";
            reslist = "Aminophylline,Caffeine,High flow oxygen,Intubated,Medication dose change,Nasal cannula,Nasal CPAP";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000006808,3041733124512,9991733666661";
            AddBuckets(buckets, "", codelist, "", "");

            if (!Exists("", "9993040000639", "", "", "Mask,Tubing,CPAP,BPAP,Patient able to manage equipment on own,Equipment inspected,Waiver signed,Other"))
            {
                codelist = "3045001108";
                reslist = "Bilevel,CPAP,Auto - Bilevel,Auto - CPAP,AVAPS,PCV";
                AddBuckets(buckets, "", codelist, "", "", reslist);

                codelist = "9993040000637";
                reslist = "Nasal pillows,Nasal mask,Nasal Prongs,Nasal pharyngeal";
                reslist += ",Full face mask,Performax,Total face mask,Endotracheal,Tracheostomy";
                AddBuckets(buckets, "", codelist, "", "", reslist);
            }

            codelist = "9991600100035,9991600100039";
            reslist = "Point of care,To lab";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            //   Must also have SPO2  (PA - VS - 11)  and [ RR(PA - VS - 8) or RR(PA - P - 43) ]
            codelist = "3045001117,9993040000635";
            AddDependentBuckets(buckets, codelist, "", "30454321", "", "3045001064,3045001128", "");

            codelist = "9991600100048";
            reslist = "Oral mouthpiece,Mask,Trach,Ventilator,NPPV,Blow-by,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990160238401,9990160238501";
            reslist = "Clear,Diminished,Absent,Crackles,Stridor,Wheeze,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040104500";
            reslist = "Scant,Small,Moderate,Copious,Thin,Thick,Clear,Blood tinged,Tan,White,Yellow";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000400613,9993040102736,9993040102752,9993040106219,9993040106220,9993040106221,9993040106222";
            AddBuckets(buckets, "", codelist, "", "", "");

            codelist = "9993040108077,9993040108078,9993040108079";
            AddBuckets(buckets, "", codelist, "", "", "");

            AnalyzeBuckets(buckets, ind, bucket_size, "Pulmonary");
            //ct = CountBuckets(buckets);
            //CheckAssessment(ct, "Pulmonary=" + ct);
            //ShowBuckets(buckets);

            //
            //Cardiovascular group
            //
            buckets = new List<gBucket>();
            codelist = "3045001065";
            reslist = "SR,SB,Sinus rhythm,Sinus bradycardia,Sinus tachycardia,Sinus arrest";
            reslist += ",Sinus arrhythmia,A-paced,V-paced,A-V paced,Atrial paced,Ventricular paced,A-V Sequential paced";
            reslist += ",Agonal,Asystole,A-fib,A-fib w/RVR,Atrial fibrillation";
            reslist += ",Atrial flutter,Heart block,Junctional accelerated,Junctional rhythm";
            reslist += ",Junctional tachycardia,PEA,SVT,Pulseless electrical activity,Supraventricular tachycardia,Ventricular fibrillation,Ventricular tachycardia";
            reslist += ",Torsades,VF,VT,Other,MAT,WAP";
            AddBuckets(buckets, "", codelist, "", "", reslist);
            reslist = EXACT_MATCH_PREFIX + "ST";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "3045001066";
            reslist = "1st degree AVB,2nd degree AVB (Mobitz I, Wenckebach)";
            reslist += ",2nd degree AVB (Mobitz II),3rd degree AVB,Bundle branch block,Idioventricular";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "3045001067";
            reslist = "PVCs,Premature ventricular contractions,Unifocal PVCs,Multifocal PVCs";
            reslist += ",Couplet PVCs,Triplet PVCs,Premature atrial contractions,PACs,Aberrent conduction";
            reslist += ",Ectopic beats,Fusion beats,Junctional escape beats,Non-conducted PACs";
            reslist += ",Paroxysmal atrial tachycardia,Paroxysmal supraventricular tachycardia,PSVT,PJCS";
            reslist += ",Premature junctional contractions,Ventricular escape beats";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040108650,9993040108651,9993040108652,9993040108653";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "9993040100446";
            reslist = "S1,S2,S3,S4,Click,Device,Distant,Friction rub";
            reslist += ",Gallop,Holosystolic murmur,Mechanical valve click,Muffled";
            reslist += ",Murmur,Pericardial rub,No adventitious heart sounds";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040101320";
            //reslist = "Off,A paced,V paced,A/V paced,AAI,AAI-DDD (MVP),AOO,DVI,DOO,DDD";
            //reslist += ",VVI,VOO,VDD,AAR,AAR-DDDR (MVP),AOOR,DVR,DOOR,DOI,DDDR,DDR,VVR";
            //reslist += ",VOOR,VDDR";
            reslist = "";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040101316,9993040101317";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "9993040001044";
            reslist = "Red,Pink,Pale,Dusky,Purple,Mottled,Ecchymotic,Soft,Firm";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040001045";
            reslist = "Greater than 3 seconds,Less than or equal to 3 seconds,Brisk,Sluggish";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040001048,9993040001049";
            reslist = "Verified,Absent";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000303200,9990000303210,9990000303220,3045001045,9990000303270,9990000303280";
            codelist += ",9990000303320,9990000303330,3045001012,3045001014,9990000303390";
            codelist += ",9990000303400,3045001013,3045001015";
            reslist = "0,+1,+2,+3,+4,Doppler";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "3045001004,3045001002,3045001003,3045001001";
            reslist = "Less than/equal to 2 seconds,Greater than/equal to 3 seconds,No return";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000302900,9993045006597,9990000302920,9993045006598,9993040101373";
            codelist += ",9993040101374,9993040101375,9993040101376,3040000000022,99930400000027";
            codelist += ",3040000000028,9993040000062,99930400000056,3040000000050,9993040000044";
            codelist += ",9993040000035,9993040000045,9993040000049,9993047096403,9993040000050";
            codelist += ",9993040000064,9993040000065,3045001056,3045001055";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "9991733888889";
            reslist = "Acrocyanosis,Capillary refill no return,Capillary refill sluggish (>2 seconds)";
            reslist += ",Circumoral,Cyanosis" + CHAR_COMMA + " centralized,Cyanosis" + CHAR_COMMA + " peripheral";
            reslist += ",Generalized,Localized,Mottled,Pale,Ruddy";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000302650";
            reslist = "Absent,Murmur,Murmer-intermittent,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991733888890";
            reslist = "0,+3,+1,Unequal,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991733999991";
            reslist = "Moderate,Non-pitting,Severe,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040100223";
            reslist = "Circumocular,Circumoral,Nailbed,Acrocyanosis,Facial";
            reslist += ",Generalized,Oral mucosa,Underlying,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "3045001000";
            reslist = "Normal (less than/equal to 2 seconds" + CHAR_COMMA + " all extremities)";
            reslist += ",Sluggish (greater than/equal to 3 seconds" + CHAR_COMMA + " all extremities)";
            reslist += ",No return,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000303180,9990000303240,9990000303300,9990000303370";
            reslist = "Acrocyanosis,Appropriate for ethnicity,Ashen,Black,Bronze";
            reslist += ",Dusky,Ecchymosis,Flushed,Gray,Jaundiced";
            AddBuckets(buckets, "", codelist, "", "", reslist);
            codelist = "9990007061360";
            reslist = "Capillary refill less than 3 sec,Cool fingers";
            reslist += ",Capillary refill greater than 3 sec,Dusky fingers";
            reslist += ",Numbness to fingers,Pale fingers";
            reslist += ",Tingling to fingers,Weakness to fingers";
            AddBuckets(buckets, "", codelist, "", "", reslist);
            codelist = "9993040103299,9993040103300";
            reslist = "Present,Absent";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040101321,9993040101322,9993040101323,9993040101324";
            reslist = "0,+1,+2,+3,+4,Doppler";
            AddBuckets(buckets, "", codelist, "", "", reslist);
            codelist = "99930434002,9993043034001";
            reslist = "0,+1,+2,+3,+4,Doppler,Sheath In";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040107859,9993040107862";
            AddBuckets(buckets, "", codelist, "", "", "");

            AnalyzeBuckets(buckets, ind, bucket_size, "Cardiovascular");

            //ct = CountBuckets(buckets);
            //CheckAssessment(ct, "Cardiovascular=" + ct);
            //ShowBuckets(buckets);

            //
            //Neurological group
            //
            buckets = new List<gBucket>();
            codelist = "9993040101409";
            reslist = "0/4,1/4,2/4,3/4,4/4";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "3045001007,3045001039,9993040006316,9990304006317,9993040006318";
            codelist += ",9993040006319,9993040006320,9990000301930,9990000301910";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "9990000301920,9990000301900";
            reslist = "Brisk,Sluggish,Nonreactive";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040101162,9993040101163";
            reslist = "Absent,Weak,Moderate,Strong,Contracture";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000002280,9990000002279,3045001017";
            reslist = "Present,Absent";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "3045001016,9993040101166,9993040101167";
            reslist = "Absent,Weak,Moderate,Strong,Contracture";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000301980,9990000301940,9990000302000,9990000301960";
            reslist = STARTS_WITH + "0,-1,-2,-3,-4,P,SP,NP,Stim,Pain,DC,DB";
            AddBuckets(buckets, "", codelist, "", "", reslist);
            reslist = "Responds to commands,Normal extension,Normal flexion,Tremors";
            reslist += ",Flaccid,Abnormal extension,Abnormal flexion";
            reslist += ",Movement to painful stimulus,No movement to painful stimulus";
            reslist += ",Non-purposeful movement,No tremor,Spastic";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000301990,9990000301950,9990000302010,9990000301970";
            reslist = "Decreased,No sensation,Numbness,Pain,Tingling,Full sensation";
            reslist += ",No numbness,No pain,No tingling";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040102775,9993040102776,9993040102777,9993040102778";
            reslist = "Normal power,Cannot overcome resistance,Overcomes gravity";
            reslist += ",Can overcome resistance,Flicker of muscle,None";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000302190,9990000302180";
            reslist = "C1,C2,C3,C4,C5,C6,C7,C8,T1,T2,T3,T4,T5,T6,T7,T8,T9,T10,T11,T12";
            reslist += ",L1,L2,L3,L4,L5,S1,S2,S3,S4,S5";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "3045001023,9993040001207";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "3045001080,3045001081,9990000398011";
            // Requires: RS (PA-N-38) and SpO2(PA - VS - 11) and Respirations (PA - VS - 8)
            //Requires: MASS (PA-N-39)  and SpO2(PA - VS - 11) and Respirations (PA - VS - 8) 
            AddDependentBuckets(buckets, "3045001080", "", "3045001081", "", "30454321", "", "3045001064", "");



            codelist = "3045001079";
            string reslistA = "-5,-4,-3,-2,-1,0,+1,+2,+3,+4"; //Any number between -5 and + 4
            codelist = "9993040104675";
            string reslistB = "S,1,2,3,4";
            // Requires: RASS (PA-N-40) and SpO2(PA - VS - 11) and Respirations (PA - VS - 8) 
            // Requires: POSS (PA-N-41) and SpO2(PA - VS - 11) and Respirations(PA - VS - 8) 
            AddDependentBuckets(buckets, "3045001079", reslistA, "9993040104675", reslistB, "30454321", "", "3045001064", "");

            codelist = "9990000398011";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "9993040001216,3045001011";
            reslist = "Positive,Negative";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000450430,9990000450420";
            reslist = "Present,Weak,Absent";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000450990,9990000450410";
            reslist = "Intact,Impaired,Absent";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040103168,9993040103169";
            reslist = "Absent,Present";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040109102,9990000450470";
            reslist = "Absent,Present,Weak";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000450480";
            reslist = "Absent,Present,Weak,Strong,Coordinated,Uncoordinated,Gag present,Gag absent";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "3045001047";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "9993040001091";
            reslist = "Blindness - right,Blindness - left,Blurred vision";
            reslist += ",Visual field cut- right side,Visual field cut- right upper";
            reslist += ",Visual field cut- right lower,Visual field cut- left side";
            reslist += ",Visual field cut- left upper,Visual field cut- left lower";
            reslist += ",Diplopia- Bilateral,Diplopia - right";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000002216";
            reslist = "Strabismus - right,Strabismus - left,Droopy eyelid - right";
            reslist += ",Droopy eyelid - left,Double vision - right,Double vision - left,Unable to assess";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000002217";
            reslist = "Double vision - right,Double vision - left";
            reslist += ",Unable to look downward and inward - right,Unable to look downward and inward - left";
            reslist += ",Unable to assess";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000002218";
            reslist = "Unable to chew - right,Unable to chew - left";
            reslist += ",Unable to clench - right,Unable to clench - left";
            reslist += ",Absence of sensation - right,Absence of sensation - left";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000002219";
            reslist = "Medial strabismus -  right,Medial strabismus -  left";
            reslist += ",Unable to look laterally - right,Unable to look laterally - left";
            reslist += ",Double vision - right,Double vision - left";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000002220";
            reslist = "Facial paralysis - right,Facial paralysis - left,Loss of taste - right,Loss of taste - left";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000002221";
            reslist = "Unable to hear - right,Unable to hear - left";
            reslist += ",Ringing in ear - right,Ringing in ear - left";
            reslist += ",Involuntary eye movement - right,Involuntary eye movement - left";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000002222";
            reslist = "Altered gag reflex";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000002223";
            reslist = "Weakness in turning head - right,Weakness in turning head - left";
            reslist += ",Unable to shrug - right,Unable to shrug - left";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000002224";
            reslist = "Deviation of tongue - right,Deviation of tongue - left";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040001057";
            AddBuckets(buckets, "", codelist, "", "");
            codelist = "9990000302300";
            reslist = "Clear,Cloudy,Serous,Sanguineous,Serosanguineous";
            reslist += ",Purulent,Cherry,Straw,Yellow";
            AddBuckets(buckets, "", codelist, "", "");
            codelist = "9990000002230,9990000304510,9993040109138";
            AddBuckets(buckets, "", codelist, "", "");


            codelist = "9991600100259";
            reslist = "No untoward effects noted,Use of reversal agent(s)";
            reslist += ",Hypoxemia < 90% for > 1 min,Hypotension of bradycardia requiring intervention";
            reslist += ",Respiratory failure requiring intervention,Cardiac arrest or death";
            reslist += ",Sedation recovery time > 60 min,Unplanned admission or higher level of care";
            reslist += ",Respiratory distress,Unanticipated need for anesthesia involvement";
            reslist += ",Inability to complete procedure,No responsible adult for discharge escort";
            reslist += ",Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "99910701000111";
            AddBuckets(buckets, "", codelist, "", "");


            // Must also contain and SpO2(PA - VS - 11) and Respirations (PA - VS - 8) "
            AddDependentBuckets(buckets, "9991070011101", "", "30454321", "", "3045001064", "");


            codelist = "9990000450560";
            reslist = "Aggression,Aura,Behavior pause,Bowel incontinence,Deja vu";
            reslist += ",Fearful,Giggles,Nausea,Oral Trauma,Smirks,Tremors,Urine incontinence,Vocalize";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000450570";
            reslist = "Eyes right,Eyes left,Head right,Head left,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000450580,9993040108685,9993040108686";
            reslist = "Head,Face,Eye,Hand,Arm,Leg,Foot,Jerking,Stiffening";
            reslist += ",Staring,Tonic-clonic Motion,Twitching,Drop";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000450590";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "9990000450600";
            reslist = "Somnolence,Aphasic,RUE paresis,LUE paresis,RLE paresis,LLE paresis";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000450620";
            reslist = "Aware of seizure,Word given,Word recalled,Word not recalled";
            reslist += ",Normal speech,Abnormal speech,Unable to respond";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040101196";
            reslist = "Generalized,Right,Left,Hand,Leg,Face,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040101198";
            reslist = "2,3,4,5,6,7,8,9,10"; // num > 1
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040100079";
            reslist = "Absent,Arching back or neck,Bicycling,Clonic jerking";
            reslist += ",Extension is greater than flexion,Jerky,Jittery/tremors";
            reslist += ",Lip smacking,Movements cease with containment,Movements continue despite containment";
            reslist += ",Rowing,Seizure activity,Tonic extension,Tonic flexion,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991733999990";
            reslist = "Bicycling,Eye deviation,Lip smacking,Movement ceases with containment";
            reslist += ",Movements continue despite containment,Tongue thrusting,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040102780";
            reslist = "Abnormal reflex,Extensionx,Frantic movement,Inconsolable";
            reslist += ",Lethargic,Medically paralyzed,Sedated,other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991140100006";
            reslist = "Quiet alert,Sleeping,Active alert,Lusty cry,Drowsy,Active with stimulation";
            reslist += ",Hoarse cry,Irritable,Jittery,Lethargic,Shrill cry";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040100076";
            reslist = "Hypertonic generalized,Hypertonic localized,Hypotonic generalized,Hypotonic localized,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000451750";
            reslist = "Absent,Present,Weak,Brisk,Clonus,Clonus Sustained";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000450440";
            reslist = "Absent,Asymmetric,Symmetric,UTA";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000450450";
            reslist = "Absent,Present,Weak,UTA";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000450460";
            reslist = "Absent,Present,Clonus,Hyperreflexive,Hyporeflexive,Weak,UTA";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040109103";
            reslist = "Absent,Present,Weak,Unable to assess";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040109104";
            reslist = "Absent,Asymmetrical,Symmetrical,Unable to assess";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991140100013,9991140100014";
            AddBuckets(buckets, "", codelist, "", "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, "Neurological");
            //ct = CountBuckets(buckets);
            //CheckAssessment(ct, "Neurologic=" + ct);
            //ShowBuckets(buckets);

            buckets = new List<gBucket>();
            codelist = "9993040108530,3045001091";
            AddBuckets(buckets, "", codelist, "", "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, "Drains");

        }

        private void AddDependentBuckets(List<gBucket> bucket_list, string codelist1, string reslist1, string codelist2, string reslist2)
        {
            AddDependentBuckets(bucket_list, codelist1, reslist1, codelist2, reslist2, "Phantom", "");
        }
        private void AddDependentBuckets(List<gBucket> bucket_list, string codelist1, string reslist1, string codelist2, string reslist2, string codelist3, string reslist3)
        {
            AddDependentBuckets(bucket_list, codelist1, reslist1, codelist2, reslist2, codelist3, reslist3, "Phantom", "");
        }

        private void AddDependentBuckets(List<gBucket> bucket_list, string codelist1, string reslist1, string codelist2, string reslist2, string codelist3, string reslist3, string codelist4, string reslist4)
        {
            bool dep3 = true;
            bool dep4 = true;
            // get the chart items for the assessments
            var query1 = StartNewQuery(SearchDepth.SearchDefault);
            query1 = AndItemFilter(query1, "", codelist1, "", "", reslist1);
            //Program.VerboseAudit("query1:" + query1.Count() + " bucketsize:" + _bucket_size);
            //foreach (var x in query1)
            //{
            //    var s1 = "(" + x.CODE + "," + x.EVENT_DATETIME.ToString() + ")";
            //    Program.VerboseAudit("query1a item:=" + s1);
            //}

            var query1b = (from item in query1
                               //                        select new { bnum = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size) }).Distinct();
                           select new { evdt = item.EVENT_DATETIME }
                           ).Distinct();
            //Program.VerboseAudit("query1b:" + query1b.Count());

            var query2 = StartNewQuery(SearchDepth.SearchDefault);
            query2 = AndItemFilter(query2, "", codelist2, "", "", reslist2);
            //Program.VerboseAudit("query2:" + query2.Count());
            //foreach (var x in query2)
            //{
            //    var s1 = "(" + x.CODE + "," + x.EVENT_DATETIME.ToString() + ")";
            //    Program.VerboseAudit("query2a item:=" + s1);
            //}
            var query2b = (from item in query2
                               //select new { bnum = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size) }).Distinct();
                           select new { evdt = item.EVENT_DATETIME }
                           ).Distinct();
            //Program.VerboseAudit("query2b:" + query2b.Count());

            if (codelist3.Trim() == "Phantom")
            {
                dep3 = false;
                codelist3 = "Hello this is a phantom code";
            }
            var query3 = StartNewQuery(SearchDepth.SearchDefault);
            query3 = AndItemFilter(query3, "", codelist3, "", "", reslist3);
            //Program.VerboseAudit("query3:" + query3.Count());
            //foreach (var x in query3)
            //{
            //    var s1 = "(" + x.CODE + "," + x.EVENT_DATETIME.ToString() + ")";
            //    Program.VerboseAudit("query3a item:=" + s1);
            //}
            var query3b = (from item in query3
                               //                           select new { bnum = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size) }).Distinct();
                           select new { evdt = item.EVENT_DATETIME }
                           ).Distinct();
            //Program.VerboseAudit("query3b:" + query3b.Count());


            if (codelist4.Trim() == "Phantom")
            {
                dep4 = false;
                codelist4 = "Hello this is a phantom code";
            }
            var query4 = StartNewQuery(SearchDepth.SearchDefault);
            query4 = AndItemFilter(query4, "", codelist4, "", "", reslist4);
            //Program.VerboseAudit("query4:" + query4.Count());
            var query4b = (from item in query4
                               //                           select new { bnum = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size) }).Distinct();
                           select new { evdt = item.EVENT_DATETIME }
                           ).Distinct();
            //Program.VerboseAudit("query4b:" + query4b.Count());

            // figure out what buckets the events belong to
            var query1a = from item1x in query1b
                          select new
                          {
                              bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item1x.evdt) / _bucket_size), //  bucket = item1x.bnum,
                              code = codelist1,
                              evdt = item1x.evdt // _pat.pull_start.AddMinutes(item1x.bnum * _bucket_size)
                          };
            //foreach (var x in query1a)
            //{
            //    var s = "(" + x.bucket + "," + x.code + "," + x.evdt.ToString() + ")";
            //    Program.VerboseAudit("query1a item:=" + s);
            //}
            var query2a = from item2x in query2b
                          select new
                          {
                              bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item2x.evdt) / _bucket_size), //bucket = item2x.bnum,
                              code = codelist2,
                              evdt = item2x.evdt // _pat.pull_start.AddMinutes(item2x.bnum * _bucket_size)
                          };
            //foreach (var x in query2a)
            //{
            //    var s = "(" + x.bucket + "," + x.code + "," + x.evdt.ToString() + ")";
            //    Program.VerboseAudit("query2a item:=" + s);
            //}
            var query3a = from item3x in query3b
                          select new
                          {
                              bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item3x.evdt) / _bucket_size),//item3x.bnum,
                              code = codelist3,
                              evdt = item3x.evdt //_pat.pull_start.AddMinutes(item3x.bnum * _bucket_size)
                          };
            //foreach (var x in query3a)
            //{
            //    var s = "(" + x.bucket + "," + x.code + "," + x.evdt.ToString() + ")";
            //    Program.VerboseAudit("query3a item:=" + s);
            //}
            var query4a = from item4x in query4b
                          select new
                          {
                              bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item4x.evdt) / _bucket_size), // item4x.bnum,
                              code = codelist4,
                              evdt = item4x.evdt //_pat.pull_start.AddMinutes(item4x.bnum * _bucket_size)
                          };

            int i = 0;
            // Add to the list IFF items in both lists occur in same bucket
            foreach (var item1 in query1a)
            {
                foreach (var item2 in query2a)
                {
                    if (item1.bucket == item2.bucket)
                    {
                        if (dep3)
                        {
                            foreach (var item3 in query3a)
                            {
                                if (item1.bucket == item3.bucket)
                                {
                                    if (dep4)
                                    {
                                        foreach (var item4b in query4a)
                                        {
                                            if (item1.bucket == item4b.bucket)
                                            {
                                                var b = new gBucket();
                                                b.bucket = item1.bucket;
                                                b.code = item1.code;
                                                b.evdt = item1.evdt;
                                                //b.has_all_deps = (item1.evdt == item2.evdt && item1.evdt == item3.evdt && item1.evdt == item4b.evdt);
                                                b.has_all_deps = (item1.evdt.AddMinutes(-15) <= item2.evdt && item1.evdt.AddMinutes(15) >= item2.evdt &&
                                                                  item1.evdt.AddMinutes(-15) <= item3.evdt && item1.evdt.AddMinutes(15) >= item3.evdt &&
                                                                  item1.evdt.AddMinutes(-15) <= item4b.evdt && item1.evdt.AddMinutes(15) >= item4b.evdt);
                                                if (b.has_all_deps)
                                                {
                                                    b.num_addl_items = 3;
                                                    gBucket f = bucket_list.Find(x => x.evdt == item1.evdt);
                                                    if (f.evdt != item1.evdt) bucket_list.Add(b);
                                                }
                                                //Program.VerboseAudit("4b:"+i++.ToString());
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (item1.bucket == item3.bucket)
                                        {
                                            var b = new gBucket();
                                            b.bucket = item1.bucket;
                                            b.code = item1.code;
                                            b.evdt = item1.evdt;
                                            //b.has_all_deps = (item1.evdt == item2.evdt && item1.evdt == item3.evdt);
                                            b.has_all_deps = (item1.evdt.AddMinutes(-15) <= item2.evdt && item1.evdt.AddMinutes(15) >= item2.evdt &&
                                                              item1.evdt.AddMinutes(-15) <= item3.evdt && item1.evdt.AddMinutes(15) >= item3.evdt);
                                            if (b.has_all_deps)
                                            {
                                                b.num_addl_items = 2;
                                                gBucket f = bucket_list.Find(x => x.evdt == item1.evdt);
                                                if (f.evdt != item1.evdt) bucket_list.Add(b);
                                            }
                                            //Program.VerboseAudit("3b:" + i++.ToString());
                                        }

                                    }
                                }
                            }
                        }
                        else
                        {
                            if (item1.bucket == item2.bucket)
                            {
                                var b = new gBucket();
                                b.bucket = item1.bucket;
                                b.code = item1.code;
                                b.evdt = item1.evdt;
                                //b.has_all_deps = (item1.evdt == item2.evdt);
                                b.has_all_deps = (item1.evdt.AddMinutes(-15) <= item2.evdt && item1.evdt.AddMinutes(15) >= item2.evdt);
                                if (b.has_all_deps)
                                {
                                    b.num_addl_items = 1;
                                    Program.VerboseAudit("  hasall=" + b.has_all_deps);
                                    gBucket f = bucket_list.Find(x => x.evdt == item1.evdt);
                                    if (f.evdt != item1.evdt) bucket_list.Add(b);
                                }
                                //Program.VerboseAudit("2b:" + i++.ToString());
                            }
                        }
                    }
                }
            }

        }



        private bool AnalyzeBuckets(List<gBucket> buckets, int ind, int bucketsize, string group)
        {
            return AnalyzeBuckets(buckets, ind, bucketsize, group, true);
        }

        private bool AnalyzeBuckets(List<gBucket> buckets, int ind, int bucketsize, string group, bool set_ind)
        {
            DateTime dt = DateTime.MinValue;
            int bnum = 0;
            List<gBucket> dtlist = new List<gBucket>();
            Program.VerboseAudit("---- Begin Assessment Group = " + group + " ----");

            bool all_ok = NEWAnalyzeBuckets(buckets, ind, bucketsize, group, set_ind);
            return all_ok;

        }
        private bool NEWAnalyzeBuckets(List<gBucket> buckets, int ind, int bucketsize, string group, bool set_ind)
        {
            DateTime dt = DateTime.MinValue;
            DateTime firstdt = DateTime.MinValue;
            bool specialq30 = false;

            int bnum = 0;
            int numbucket = -99;
            int numitems = 0;
            int numallitems = 0;
            List<gGap> gaplist = new List<gGap>();
            Program.VerboseAudit("----GAP Begin Assessment Group = " + group + " ---- bucketsizew=" + bucketsize);

            //Program.VerboseAudit("buckets count=" + buckets.Count());
            var b = buckets.OrderBy(e => e.evdt).ToList();
            //numitems = buckets.Count();

            DateTime lastq4dt = DateTime.MinValue;
            int numq4 = 1;

            foreach (var item in b)
            {
                Program.VerboseAudit(item.bucket + ") " + item.evdt.ToString() + " := " + item.code + " has all deps=" + item.has_all_deps);
                if (item.has_all_deps)
                    if (numitems == 0)
                    {
                        dt = item.evdt;
                        firstdt = dt;
                        lastq4dt = dt;
                    }
                    else
                    {
                        if (lastq4dt.AddMinutes(30) >= dt)
                        {
                            numq4++;
                            lastq4dt = dt;
                        }
                        var g = new gGap();
                        g.gap = (int)(PFSUtility.DateDiffInMinutes(dt, item.evdt));
                        g.evdt1 = dt;
                        g.evdt2 = item.evdt;
                        gaplist.Add(g);
                        Program.VerboseAudit("addgap dt=" + dt.ToString() + " evdt=" + item.evdt.ToString() + " gap="+g.gap);
                        dt = item.evdt;
                    }
                numitems++;
                numallitems = numallitems + 1 + item.num_addl_items;
            }
            Program.VerboseAudit("numitems=" + numitems + " numq4=" + numq4);
            if (numitems <= 2)
            {
                if (numq4 >= 2)
                {
                    if (set_ind && ind >= 18 && ind <= 20)
                    {
                        SetInd(18, "q4 based on number >=2 of items charted = " + numq4);
                        return true;
                    }
                }
                if (numitems == 2 && ind >= 16 && ind <= 17)
                {
                    gGap[] sgapary = gaplist.ToArray();
                    int k = 0;
                    for (k = 0; k <= sgapary.GetUpperBound(0); k++)
                    {
                        Program.VerboseAudit("k=" + k + " gap=" + sgapary[k].gap);
                    }
                    if (sgapary[0].gap >= 150 || sgapary[0].gap <= 5)
                        SetInd(16, "Two item gap =" + sgapary[0].gap);
                    else
                        SetInd(17, "Two item gap =" + sgapary[0].gap);
                }
                return false;
            }

            int i, j, setind = 0;
            int numgaps = 0;
            int gapsum = 0;
            double gapavg = 999.0, lo_gapavg = 999.0, altlo_gapavg = 999.0;
            bool lastgap = false;
            bool shortstop = false;
            int timegap = 0;
            int lastj;
            DateTime lo_evdt1 = DateTime.MinValue, lo_evdt2 = DateTime.MinValue;
            DateTime altlo_evdt1 = DateTime.MinValue, altlo_evdt2 = DateTime.MinValue;
            gGap[] gapary = gaplist.ToArray();
            string s = "";
            for (i = 0; i <= gapary.GetUpperBound(0); i++) s = s + "," + gapary[i].gap;
            if (numitems == 2 && numq4 >= 2) // numitems will always be > 2
            {
                numgaps = 1;
                gapsum = gapary[0].gap;
                gapavg = gapsum;
                lo_gapavg = gapavg;
                lo_evdt1 = gapary[0].evdt1;
                lo_evdt2 = gapary[gapary.GetUpperBound(0)].evdt2;
            }
            else
            {

                //numitems = 4
                //i = 0 j = 0[i].evdt1 = 9 / 4 / 2019 5:15:00 AM[j].evdt2 = 9 / 4 / 2019 9:32:00 AM

                for (i = 0; i <= gapary.GetUpperBound(0) && !shortstop; i++)
                {
                    numgaps = 0;
                    gapsum = 0;
                    gapavg = 999.0;
                    lastgap = false;
                    for (j = i; j <= gapary.GetUpperBound(0) && !lastgap; j++)
                    {
                        Program.VerboseAudit("i=" + i + " j=" + j + " [i].evdt1=" + gapary[i].evdt1 + " [j].evdt2=" + gapary[j].evdt2);
                        timegap = (int)(PFSUtility.DateDiffInMinutes(gapary[i].evdt1, gapary[j].evdt2));
                        if (timegap <= _pat.los_hours * 30 + 30)
                        {
                            numgaps++;
                            gapsum += gapary[j].gap;
                            Program.VerboseAudit("numgaps=" + numgaps + " [j].gap=" + gapary[j].gap + " gapsum=" + gapsum);
                        }
                        else
                        {
                            if (j == i)
                            {
                                lastj = i;
                                numgaps = 1;
                                gapsum = gapary[j].gap;
                            }
                            else
                                lastj = j - 1;
                            Program.VerboseAudit("Last Gap: i=" + i + " j=" + j + " [i].evdt1=" + gapary[i].evdt1 + " [lastj].evdt2=" + gapary[lastj].evdt2);
                            lastgap = true;
                            gapavg = 1.0 * gapsum / numgaps;
                            Program.VerboseAudit("gapavg=" + gapavg + " numgaps=" + numgaps + " gapsum=" + gapsum);
                            timegap = (int)(PFSUtility.DateDiffInMinutes(gapary[i].evdt1, gapary[lastj].evdt2));
                            if (gapavg < lo_gapavg && timegap >= .75 * _pat.los_hours * 30)
                            {
                                lo_gapavg = gapavg;
                                Program.VerboseAudit("Low gapavg=" + lo_gapavg + " btwn " + gapary[i].evdt1 + " and " + gapary[lastj].evdt2);
                                lo_evdt1 = gapary[i].evdt1;
                                lo_evdt2 = gapary[lastj].evdt2;
                            }
                            numgaps = 0;
                            gapsum = 0;
                        }

                    }
                    if (!lastgap)
                    {
                        shortstop = true;
                        j--; // take the j index back by 1 because it incremented at the end.
                        Program.VerboseAudit(".Last Gap: i=" + i + " j=" + j + " [i].evdt1=" + gapary[i].evdt1 + " [j].evdt2=" + gapary[j].evdt2);
                        gapavg = 1.0 * gapsum / numgaps;
                        Program.VerboseAudit(".gapavg=" + gapavg + " numgaps=" + numgaps + " gapsum=" + gapsum);
                        timegap = (int)(PFSUtility.DateDiffInMinutes(gapary[i].evdt1, gapary[gapary.GetUpperBound(0)].evdt2));
                        Program.VerboseAudit(".timegap=" + timegap + " .75halfLOS=" + .75 * _pat.los_hours * 30);
                        if (gapavg < lo_gapavg)
                        {
                            if (timegap >= .75 * _pat.los_hours * 30)
                            {
                                lo_gapavg = gapavg;
                                Program.VerboseAudit(".Low gapavg=" + lo_gapavg + " btwn " + gapary[i].evdt1 + " and " + gapary[gapary.GetUpperBound(0)].evdt2);
                                lo_evdt1 = gapary[i].evdt1;
                                lo_evdt2 = gapary[gapary.GetUpperBound(0)].evdt2;
                            }
                            else
                            {
                                altlo_gapavg = gapavg;
                                Program.VerboseAudit(".AltLow gapavg=" + lo_gapavg + " btwn " + gapary[i].evdt1 + " and " + gapary[gapary.GetUpperBound(0)].evdt2);
                                altlo_evdt1 = gapary[i].evdt1;
                                altlo_evdt2 = gapary[gapary.GetUpperBound(0)].evdt2;
                            }
                        }
                    }
                }
            }
            Program.VerboseAudit("Final Low gapavg=" + lo_gapavg + " btwn " + lo_evdt1.ToString() + " and " + lo_evdt2.ToString() + " " + (_pat.los_hours <= 4 ? 0 : 1) + "LOS=" + _pat.los_hours + " assmts:" + (_inds[15].is_checked ? 1 : 0) + (_inds[16].is_checked ? 1 : 0) + (_inds[17].is_checked ? 1 : 0) + (_inds[18].is_checked ? 1 : 0) + " gap[]: =" + s);
            Program.VerboseAudit("Alt   Low gapavg=" + altlo_gapavg + " btwn " + altlo_evdt1.ToString() + " and " + altlo_evdt2.ToString());
            //q4: 3 hr + gap average  in 6 - 8 hrs
            //  q2:   1 hr 30 - 2 hr 59m avg gap  in 4 hrs
            //  q1:   46 min - 1 hr 29 min avg gap in 4 hrs
            //  q30:  45 min or less avg gap in 4 hrs
            //            if (lo_gapavg <= 45)
            int sample_breadth = (int)(PFSUtility.DateDiffInMinutes(lo_evdt1, lo_evdt2));
            if (_pat.los_hours >= 4 && sample_breadth > 0.75 * _pat.los_hours * 30 && numitems >= 2)
            {
                if (lo_gapavg <= 80) setind = 20;  //q60
                else if (lo_gapavg <= 180) setind = 19; //q120
                else if (lo_gapavg <= 999) setind = 18;  //q240
                if (lo_gapavg <= 45) specialq30 = true;       //q30
            }
            else
            {
                if (numitems > 2 && sample_breadth >= 0.75 * _pat.los_hours * 30)
                {
                    if (lo_gapavg <= 60) setind = 20;  //q60
                    else if (lo_gapavg <= 120) setind = 19; //q120
                    else if (lo_gapavg <= 999) setind = 18;  //q240
                    if (lo_gapavg <= 30) specialq30 = true;       //q30
                }
                else if (numitems >= 2 && sample_breadth >= 30)
                {
                    if (sample_breadth >= 0.5 * _pat.los_hours * 30) setind = 19; //q120
                    else setind = 18;  //q240
                }
                else if (numitems >= 2)
                    setind = 18;
            }
            int imins = 0;
            bool all_ok = false;
            if (setind == 18) imins = 240;
            else if (setind == 19) imins = 120;
            else if (setind == 20) imins = 60;
            if (ind <= 17)
            {
                setind = 0;
                if ((bucketsize == 40 && specialq30)
                    || (bucketsize == 80 && imins <= 60)
                    || (bucketsize == 150 && imins <= 120)
                    || (bucketsize == 300 && imins <= 240))
                    setind = ind;
            }
            if (setind > 0)
            {
                if (set_ind)
                    SetInd(setind, "Qualifies for q" + imins + "mins for gap avg=" + lo_gapavg + "[" + (sample_breadth < 4 ? "<" : "") + "in " + sample_breadth + " mins] btwn " + lo_evdt1.ToString() + " and " + lo_evdt2.ToString());
                else //for when AnalyzeBuckets is asked NOT to set the indicator...
                    Program.VerboseAudit(group + "Qualifies for q" + imins + "mins for gap avg=" + lo_gapavg + "[" + (sample_breadth < 4 ? "<" : "") + "in " + sample_breadth + " mins] btwn " + lo_evdt1.ToString() + " and " + lo_evdt2.ToString());
            }
            if (ind >= 18 && ind <= 20 && setind >= 18) all_ok = true;
            if (ind <= 17 && setind == ind) all_ok = true;

            Program.VerboseAudit("---- GAP End Assessment Group = " + group + " ----");
            return all_ok;

        }


        private void SetBucketSize(int minutes)
        {
            _bucket_size = minutes;
        }

        //Add to the list of bucket numbers (redundant buckets are fine)
        private void AddBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field)
        {
            AddBuckets(bucket_list, cat, code_list, desc, field, "", SearchDepth.SearchDefault);
        }
        private void AddBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field, string result_list)
        {
            AddBuckets(bucket_list, cat, code_list, desc, field, result_list, SearchDepth.SearchDefault);

        }
        private void AddBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field, string result_list, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc, field, result_list);

            var query2 = (from item in query select new { item.EVENT_DATETIME, item.CODE });

            var query3 = from item in query2
                         select new
                         {
                             bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                             code = item.CODE,
                             evdt = item.EVENT_DATETIME
                         };
            // Add to the list
            foreach (var item in query3)
            {
                var b = new gBucket();
                b.bucket = item.bucket;
                b.code = item.code;
                b.evdt = item.evdt;
                b.has_all_deps = true;
                gBucket f = bucket_list.Find(x => x.evdt == item.evdt);
                if (f.evdt != item.evdt) bucket_list.Add(b);
            }

            // This is just for auditing: Say what we are looking for and how many were found
            if (!String.IsNullOrEmpty(result_list))
            {
                // print each word and if it was found or not
                int i = CountResultContains(cat, code_list, desc, field, result_list);
            }
            else
            {
                // print how many were found
                Program.VerboseAudit(Describe(cat, code_list, desc, field, ""));
            }
        }

        //private int CountBuckets(List<gBucket> bucket_list)       // bucket list: (ha ha)
        //{
        //    //int result = bucket_list.Distinct().Count();
        //    //if (result > 0) Program.VerboseAudit(result + " unique");
        //    //return result;
        //    int x = -99;
        //    int result = 0;
        //    //int result = bucket_list.Distinct().Count();
        //    var query = from b in bucket_list
        //                orderby b.bucket ascending
        //                select b;
        //    foreach (var b in query)
        //    {
        //        if (x != b.bucket)
        //        {
        //            result++;
        //            x = b.bucket;
        //        }
        //    }
        //    if (result > 0) Program.VerboseAudit(result + " unique");
        //    return result;
        //}

        private void Check_21()
        {
            string reslist;
            string codelist;
            string found_what;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 21. Fluid Management");
            Program.VerboseAudit("---------------");
            exclude_periop_data = false;

            SetIndIfResultContains(21, "", "3045001088", "", "", "");
            SetIndIfResultContains(21, "", "9990007085310", "", "", "");
            SetIndIfResultContains(21, "", "9993040104906", "", "", "");
            SetIndIfResultContains(21, "", "9990000396150", "", "", "");
            SetIndIfResultContains(21, "", "9990007085490", "", "", "");
            SetIndIfResultContains(21, "", "9993040021276", "", "", "");
            SetIndIfResultContains(21, "", "3045001094", "", "", "");

            reslist = "NG tube,OG tube,G-tube,J-tube,ND tube,OD tube,PEG-tube";
            SetIndIfResultContains(21, "", "9991733565652", "", "", reslist);

            SetIndIfResultContains(21, "", "9991733569855", "", "", "");
            SetIndIfResultContains(21, "", "9993040304520", "", "", "");

            //SetIndIfResultContains(21, "", EXACT_MATCH_PREFIX + "26", "", "", "");
            int ct = CountItems("", EXACT_MATCH_PREFIX + "26", "", "", "",SearchDepth.SearchSince25Hrs,true,out found_what);
            if (ct >= 2)
                SetInd(21, "P.O. intake count=" + ct + " found="+found_what);

            SetIndIfResultContains(21, "", "9990007070009", "", "", "");

            SetIndIfResultContains(21, "", EXACT_MATCH_PREFIX + "28", "", "", "");

            SetIndIfResultContains(21, "", "9993040100475", "", "", "");

            SetIndIfResultContains(21, "", EXACT_MATCH_PREFIX + "20", "", "", "");
            SetIndIfResultContains(21, "", EXACT_MATCH_PREFIX + "14", "", "", "");
            SetIndIfResultContains(21, "", "9993047073811", "", "", "");

            SetIndIfResultContains(21, "", EXACT_MATCH_PREFIX + "29", "", "", "");
            codelist = "3040001333,3040001334,9990000305290,3045001090,3045001089";
            codelist += ",9990160000252,9990160000253,3043040100003,9993040100004,3043040101422,9993040101423";
            SetIndIfResultContains(21, "", codelist, "", "", "");

        }

        private void Check_22()
        {
            string reslist;
            bool st1 = false;
            string piv;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 22. Wound/Injury Mgmt");
            Program.VerboseAudit("---------------");

            exclude_periop_data = false;

            st1 = ResultContains("", "9993041000044", "", "", EXACT_MATCH_PREFIX + "Stage I", SearchDepth.SearchSince25Hrs);

            piv = "Midline Dual Lumen Catheter,Midline Single Lumen Catheter,Peripheral IV";

            if (!st1)
            {
                reslist = "Stage II,Stage III,Stage IV,Unstageable,Deep tissue injury";
                SetIndIfResultContains(22, "", "9993041000044", "", "", reslist);

                reslist = "";
                SetIndIfResultContains(22, "", "9990000303750", "", "", reslist);
                SetIndIfResultContains(22, "", "9990007061190", "", "", reslist);
                SetIndIfResultContains(22, "", "9993046629812", "", "", reslist);
                SetIndIfResultContains(22, "", "9993040021267", "", "", reslist);
                SetIndIfResultContains(22, "", "9990007061270", "", "", reslist);

                SetIndIfResultContains(22, "", "9990000001493", "", "", reslist);
                SetIndIfResultContains(22, "", "9990000001494", "", "", reslist);
                SetIndIfResultContains(22, "", "9990007061240", "", "", reslist);
                SetIndIfResultContains(22, "", "9990000303780", "", "", reslist);
                SetIndIfResultContains(22, "", "9993040102846", "", "", reslist);
                SetIndIfResultContains(22, "", "9993040021269", "", "", reslist);

                SetIndIfResultContains(22, "", "9990000303800", NOT_PREFIX + piv, "", reslist);
                SetIndIfResultContains(22, "", "9990000303820", NOT_PREFIX + piv, "", reslist);
                SetIndIfResultContains(22, "", "9990000304850", NOT_PREFIX + piv, "", reslist);
                SetIndIfResultContains(22, "", "99930413000000,9993041000077,9993040021270,9990000303910", "", "", reslist);

            }

            reslist = "";
            SetIndIfResultContains(22, "", "9993040104036", "", "", reslist);
            SetIndIfResultContains(22, "", "9993040104037", "", "", reslist);
            SetIndIfResultContains(22, "", "9993040023765", "", "", reslist);
            SetIndIfResultContains(22, "", "9993040100564", "", "", reslist);
            SetIndIfResultContains(22, "", "9991420100006", "", "", reslist);
            SetIndIfResultContains(22, "", "9991420100007", "", "", reslist);
            SetIndIfResultContains(22, "", "9991420100009", "", "", reslist);
            SetIndIfResultContains(22, "", "9990000304500", "", "", reslist);
            SetIndIfResultContains(22, "", "9990000304510", "", "", reslist);
            SetIndIfResultContains(22, "", "9993040103946", "", "", reslist);
            SetIndIfResultContains(22, "", "9993040103947", "", "", reslist);
            SetIndIfResultContains(22, "", "9993040103948", "", "", reslist);
            SetIndIfResultContains(22, "", "9993040103951", "", "", reslist);
            SetIndIfResultContains(22, "", "9993040103954", "", "", reslist);
            SetIndIfResultContains(22, "", "9993040103957", "", "", reslist);
            SetIndIfResultContains(22, "", "9990007070177", "", "", reslist);
            SetIndIfResultContains(22, "", "9990007070178", "", "", reslist);
            SetIndIfResultContains(22, "", "9990007070179", "", "", reslist);
            SetIndIfResultContains(22, "", "9993040001021", "", "", reslist);
            SetIndIfResultContains(22, "", "9993040000088", "", "", reslist);
            SetIndIfResultContains(22, "", "9993040001022", "", "", reslist);
            SetIndIfResultContains(22, "", "9993040103228", "", "", reslist);
            SetIndIfResultContains(22, "", "9990007085420", "", "", reslist);
            SetIndIfResultContains(22, "", "9993040102851", "", "", reslist);
            SetIndIfResultContains(22, "", "9990007085260", "", "", reslist);

            SetIndIfResultContains(22, "", "9990000016070", "", "", reslist);
            SetIndIfResultContains(22, "", "9993040102649", "", "", reslist);
            SetIndIfResultContains(22, "", "9993040102644", "", "", reslist);
            SetIndIfResultContains(22, "", "9993040102648", "", "", reslist);
            SetIndIfResultContains(22, "", "9993040102660", "", "", reslist);
            SetIndIfResultContains(22, "", "9993040102643", "", "", reslist);
            SetIndIfResultContains(22, "", "9993040102662", "", "", reslist);
            SetIndIfResultContains(22, "", "9993040102742", "", "", reslist);
            SetIndIfResultContains(22, "", "9993040102748", "", "", reslist);
            SetIndIfResultContains(22, "", "9993040102741", "", "", reslist);
            SetIndIfResultContains(22, "", "9993040102744", "", "", reslist);
            SetIndIfResultContains(22, "", "9993040102747", "", "", reslist);
            SetIndIfResultContains(22, "", "9993040102750", "", "", reslist);
            SetIndIfResultContains(22, "", "9990000396120", "", "", reslist);
            SetIndIfResultContains(22, "", "9990007061290", "", "", reslist);
            SetIndIfResultContains(22, "", "9990000006333", "", "", reslist);
            SetIndIfResultContains(22, "", "9993040108525", "", "", reslist);

            if (!st1)
            {
                SetIndIfResultContains(22, "", "3045001032", NOT_PREFIX + piv, "", reslist);
                SetIndIfResultContains(22, "", "9990007096660", NOT_PREFIX + piv, "", reslist);
                SetIndIfResultContains(22, "", "9990000304850", NOT_PREFIX + piv, "", reslist);
                SetIndIfResultContains(22, "", "9990007073550", NOT_PREFIX + piv, "", reslist);
            }

            SetIndIfResultContains(22, "", "3045001041,9990304301280,9993045001043", "", "", reslist);
            SetIndIfResultContains(22, "", "9990000301270", "", "", reslist);

            SetIndIfResultContains(22, "", "9990007080680", "", "", reslist);
            SetIndIfResultContains(22, "", "9993040103205", "", "", reslist);
            SetIndIfResultContains(22, "", "9993040103206", "", "", reslist);
            SetIndIfResultContains(22, "", "9993040103209", "", "", reslist);
            SetIndIfResultContains(22, "", "9993040000199", "", "", reslist);
            SetIndIfResultContains(22, "", "9990304840001", "", "", reslist);
            SetIndIfResultContains(22, "", "9990304840002", "", "", reslist);
            SetIndIfResultContains(22, "", "9990000370090", "", "", reslist);
            SetIndIfResultContains(22, "", "9993040001044,9993040108605,9993040108606,9993040108607,9993040001046,9993040108614,9993040108615,9993040108616", "", "", reslist);
            SetIndIfResultContains(22, "", "9991180102055,9991180102056", "", "", "");

            reslist = "Traction";
            SetIndIfResultContains(22, "", "9990000304110", "", "", reslist);
            reslist = "Traction";
            SetIndIfResultContains(22, "", "9990000304130", "", "", reslist);
            reslist = "3-Ulceration with or without bleeding";
            SetIndIfResultContains(22, "", "9990304000122", "", "", reslist);
            reslist = "Ulcerations present";
            SetIndIfResultContains(22, "", "9990000002113", "", "", reslist);
            reslist = "Tea,Rusty,Peach,Cherry,Pink,Ketchup";
            SetIndIfResultContains(22, "", "9990000006298", "", "", reslist);
            reslist = "Peritoneal port";
            SetIndIfResultContains(22, "", "9993040401216", "", "", reslist);
            reslist = "Red,Pink,Pale,Dusky,Purple,Mottled,Ecchymotic,Soft,Firm";
            SetIndIfResultContains(22, "", "9993040001044", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(22, "", "3040001333", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(22, "", "3040001334", "", "", reslist);
            reslist = "Clean,No clot";
            SetIndIfResultContains(22, "", "9993040101378", "", "", reslist);
            reslist = "Done";
            SetIndIfResultContains(22, "", "9993040101377", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(22, "", "9991020100022", "", "", reslist);
            reslist = "Vaginal pack,Uterine balloon,Uterotonic agent given (see MAR),Oxygen on,Legs elevated,MD notified,Bimanual exam per MD,Labs ordered";
            SetIndIfResultContains(22, "", "9991020100568", "", "", reslist);
            reslist = "Placed,Present,Removed";
            SetIndIfResultContains(22, "", "9991020100569", "", "", reslist);

            reslist = "Applied (comment number),Changed  (comment number),Marked,Reinforced,Site care,Staples removed (comment number),Marked,Reinforced,Site care,Staples removed (comment number)";
            SetIndIfResultContains(22, "", "9991733888867", "", "", reslist);
            reslist = "Drainage,Malodorous,Red,moist,gel foam,triple dye";
            SetIndIfResultContains(22, "", "9991733888850", "", "", reslist);
            reslist = "Dry,Moist,Cannulated,Clamp off,Clamp on,Care done,2 cord vessels,3 cord vessels";
            SetIndIfResultContains(22, "", "9991140100024", "", "", reslist);
            reslist = "Gauze in place,Petroleum jelly applied,Petroleum jelly gauze applied,Other";
            SetIndIfResultContains(22, "", "9991140100026", "", "", reslist);
            reslist = "Bleeding,Edematous,Necrotic,Pink,Reddened,Serosanguinous drainage,Serous drainage,Other";
            SetIndIfResultContains(22, "", "9991140100027", "", "", reslist);
            reslist = "Bloody,Clots,Tarry";
            SetIndIfResultContains(22, "", EXACT_MATCH_PREFIX + "18", "", "", reslist);
            reslist = "Maroon,Red";
            SetIndIfResultContains(22, "", EXACT_MATCH_PREFIX + "17", "", "", reslist);

            reslist = "Loose";
            bool loose = Exists("", EXACT_MATCH_PREFIX + "18", "", "", reslist, SearchDepth.SearchSince25Hrs);
            reslist = "Black";
            bool black = Exists("", EXACT_MATCH_PREFIX + "17", "", "", reslist, SearchDepth.SearchSince25Hrs);
            if (loose && black)
                SetInd(22, "Stool: loose and black within the past 25 hrs.");
            reslist = "Bloody,Bright red,Coffee ground,Dark red";
            SetIndIfResultContains(22, "", "9990007085590", "", "", reslist);
            SetIndIfResultContains(22, "", "99930400003205", "", "", "");
            reslist = "Frenulectomy,Lacerated";
            SetIndIfResultContains(22, "", "9990000002114", "", "", reslist);
            SetIndIfResultContains(22, "", "9993040304870,9993040108530,3045001091", "", "", "");
            SetIndIfResultContains(22, "", "9993040102647,9993040102642,9993040102740,9993040102746", "", "", "");

            string codelist = "9990304301280,9993045001043,9993040108062,9993040108060,9993040108059,9993040108070,9993040108068,3040108067";
            SetIndIfResultContains(22, "", codelist, "", "", "");

            codelist = "9993040108108,9993040108102,9993040108114,9993040108109,9993040108103,";
            codelist += "9993040108115,9993040108110,9993040108104,9993040108116,9993040108111,";
            codelist += "9993040108105,9993040108117,9993040108112,9993040108106,9993040108118,";
            codelist += "9993040108113,9993040108119";
            SetIndIfResultContains(22, "", codelist, "", "", "");
            SetIndIfResultContains(22, "", EXACT_MATCH_PREFIX + "9993040108", "", "", "");

            codelist = "9993040109510,9993040109511,9993040108145,9993040108135,9993040108140";
            codelist += ",9993040108146,9993040108136,9993040108141,9993040108147,9993040108137";
            codelist += ",9993040108142,9993040108148,9993040108138,9993040108143,9993040108149";
            codelist += ",9993040108139,9993040108144,9993040108150";
            SetIndIfResultContains(22, "", codelist, "", "", "");


        }


        int EducMins(string timecode, string nursecode)
        {
            string res;
            int val = 0;
            if (ResultContains("", nursecode, "", "", "Nursing"))
            {
                if (GetResult("", timecode, "", "", out res))
                {
                    if (res.Left(1).IsNumeric())
                    {
                        val += (int)res.Val();
                    }
                }
            }
            return val;
        }

        private void Check_23()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 23. Patient/Family Education >= 1 Hour by RN");
            Program.VerboseAudit("---------------");
            exclude_periop_data = true;

            CheckEDUtab("3040019564");
            CheckEDUtab("3040018802");
            CheckEDUtab("3040019429");
            CheckEDUtab("3040019626");
            CheckEDUtab("3040019773");
            CheckEDUtab("3040020569");
            CheckEDUtab("3040018770");
            CheckEDUtab("3040019597");
            CheckEDUtab("3040019587");
            CheckEDUtab("3040018788");
            CheckEDUtab("3040018690");
            CheckEDUtab("3040019462");
            CheckEDUtab("3040020762");
            CheckEDUtab("3040020565");
            CheckEDUtab("3040020708");
            CheckEDUtab("3040021311");
            CheckEDUtab("3040020757");
            CheckEDUtab("3040019438");
            CheckEDUtab("3040018686");
            CheckEDUtab("3040020288");
            CheckEDUtab("3040020278");
            CheckEDUtab("3040019172");
            CheckEDUtab("3040020314");
            CheckEDUtab("3040019604");
            CheckEDUtab("3040020664");
            CheckEDUtab("3040019143");
            CheckEDUtab("3040018773");
            CheckEDUtab("3040018769");
            CheckEDUtab("3040019385");
            CheckEDUtab("3040018689");
            CheckEDUtab("3040020820");
            CheckEDUtab("3040020559");
            CheckEDUtab("3040020534");
            CheckEDUtab("3040020463");
            CheckEDUtab("3040019415");
            CheckEDUtab("3040020413");
            CheckEDUtab("3040018515");
            CheckEDUtab("3040020310");
            CheckEDUtab("3040020404");
            CheckEDUtab("3040020512");
            CheckEDUtab("3040018603");
            CheckEDUtab("3040020282");
            CheckEDUtab("3040018908");
            CheckEDUtab("3040020466");
            CheckEDUtab("3040020255");
            CheckEDUtab("3040020262");
            CheckEDUtab("3040020297");
            CheckEDUtab("3040019782");
            CheckEDUtab("3040019666");
            CheckEDUtab("3040020328");
            CheckEDUtab("3040018684");
            CheckEDUtab("3040002644");
            CheckEDUtab("3040001000");
            CheckEDUtab("3040019679");
            CheckEDUtab("3040020321");
            CheckEDUtab("3040020477");
            CheckEDUtab("3040018533");
            CheckEDUtab("3040019736");
            CheckEDUtab("3040018932");
            CheckEDUtab("3040019011");
            CheckEDUtab("3040018647");
            CheckEDUtab("3040000907");
            CheckEDUtab("3040019707");
            CheckEDUtab("3040018906");
            CheckEDUtab("3040020694");
            CheckEDUtab("3040019762");
            CheckEDUtab("3040018587");
            CheckEDUtab("3040018530");
            CheckEDUtab("3040020741");
            CheckEDUtab("3040019360");
            CheckEDUtab("3040019721");
            CheckEDUtab("3040019691");
            CheckEDUtab("3040019352");
            CheckEDUtab("3040018525");
            CheckEDUtab("3040018688");
            CheckEDUtab("3040020418");
            CheckEDUtab("3040018687");
            CheckEDUtab("3040018692");
            CheckEDUtab("3040018691");
            CheckEDUtab("3040018613");
            CheckEDUtab("3040018631");
            CheckEDUtab("3040020809");
            CheckEDUtab("3040020801");
            CheckEDUtab("3040018872");
            CheckEDUtab("3040020265");
            CheckEDUtab("3040020550");
            CheckEDUtab("3040018910");
            CheckEDUtab("3040020431");
            CheckEDUtab("3040020730");
            CheckEDUtab("3040020546");
            CheckEDUtab("3040020540");
            CheckEDUtab("3040020991");
            CheckEDUtab("3040020982");
            CheckEDUtab("3040020600");
            CheckEDUtab("3040020615");
            CheckEDUtab("3040020622");
            CheckEDUtab("3040020610");
            CheckEDUtab("3040018556");
            CheckEDUtab("3040020717");
            CheckEDUtab("3040019405");
            CheckEDUtab("3040018772");
            CheckEDUtab("3040020317");
            CheckEDUtab("3040018685");
            CheckEDUtab("3040020554");
            CheckEDUtab("3040019749");
            CheckEDUtab("3040019448");
            CheckEDUtab("3040004179");
            CheckEDUtab("3040020576");
            CheckEDUtab("3040020581");
            CheckEDUtab("3040020744");
            CheckEDUtab("3040019147");
            CheckEDUtab("3040020425");
            CheckEDUtab("3040020437");
            CheckEDUtab("3040019576");
            CheckEDUtab("3040018909");
            CheckEDUtab("3040020324");
            CheckEDUtab("3040019158");
            CheckEDUtab("3040018598");
            CheckEDUtab("3040018771");
            CheckEDUtab("3040020644");
            CheckEDUtab("3040021310");
            CheckEDUtab("3040020635");

        }

        private void CheckEDUtab(string educode)
        {
            int ub, i;
            string[] desc = new string[3];
            DateTime evdt;
            string cd1, cd2, res1, res2, topic1, topic2;

            //OBX | 2 | ST | EDU3040004179METHOD ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC STROKE EVERY MINUTE MATTERS MC6074| 1 | E |||||| F ||| 20170612113300
            //OBX | 4 | ST | EDU3040004179RESPONSE ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC STROKE EVERY MINUTE MATTERS MC6074| 1 | TB |||||| F ||| 20170612113300
            //OBX | 6 | ST | EDU3040004179METHOD ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC YOU CAN IMPROVE YOUR HEALTH MC2066 - 09 | 1 | E |||||| F ||| 20170612113300
            //OBX | 8 | ST | EDU3040004179RESPONSE ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC YOU CAN IMPROVE YOUR HEALTH MC2066 - 09 | 1 | TB |||||| F ||| 20170612113300
            //OBX | 10 | ST | EDU3040004179METHOD ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC HIGH BLOOD PRESSURE (HYPERTENSION)MC5056 | 1 | E |||||| F ||| 20170612113300
            //OBX | 12 | ST | EDU3040004179RESPONSE ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC HIGH BLOOD PRESSURE (HYPERTENSION)MC5056 | 1 | TB |||||| F ||| 20170612113300
            var query1 = StartNewQuery(SearchDepth.SearchSince9Hrs);
            query1 = AndItemFilter(query1, "", "EDU" + educode + "METHOD", "", "", "E,D,I");
            query1 = query1.Where(e => e.EVENT_DATETIME <= PFSUtility.MaxDateTime(_pat.unit_departure, _pat.effective_out));
            foreach (var item1 in query1)
            {
                evdt = item1.EVENT_DATETIME;
                res1 = item1.RESULT;
                cd1 = item1.CODE;
                var arr = item1.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                ub = arr.GetUpperBound(0);
                for (i = 0; (i <= Math.Min(ub, 2)); i++)
                    desc[i] = arr[i];
                if (ub >= 2)
                    topic1 = desc[2].Trim();
                else
                    topic1 = "";
                var query2 = StartNewQuery(SearchDepth.SearchSince9Hrs);
                query2 = AndItemFilter(query2, "", "EDU" + educode + "RESPONSE", "", "", "IP,TB,NR");
                query2 = query2.Where(e => e.EVENT_DATETIME == evdt);
                foreach (var item2 in query2)
                {
                    res2 = item2.RESULT;
                    cd2 = item2.CODE;
                    var arr2 = item2.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                    ub = arr.GetUpperBound(0);
                    for (i = 0; (i <= Math.Min(ub, 2)); i++)
                        desc[i] = arr[i];
                    if (ub >= 2)
                        topic2 = desc[2].Trim();
                    else
                        topic2 = "";
                    if (topic1 == topic2)
                    {
                        SetInd(23, "Found EDU" + educode + ": " + desc[0] + "^" + topic1 + " at:" + evdt.ToString() + " METHOD=" + res1 + " RESPONSE=" + res2);
                    }
                }

            }

        }

        private void Check_24()
        {
            string reslist = "";
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 24. Coordination of Care >= 1 Hour by RN");
            Program.VerboseAudit("---------------");
            exclude_periop_data = true;

            reslist = "Care conference";
            bool u1 = Exists("", "9990007060440", "", "", reslist, SearchDepth.SearchSince9Hrs);
            if (u1)
            {
                reslist = "60 min,75 min,90 min,120 min,180 min";
                SetIndIfResultContains(24, "", "9993040004578", "", "", reslist);
            }
            //reslist = "Home Health Care Agency,Agency";
            SetIndIfResultContains(24, "", "9993040108687", "", "", "");
        }


        private void Check_Activities()
        {

            Program.VerboseAudit("---------------");
            Program.VerboseAudit(" Activities");
            Program.VerboseAudit("---------------");


        }

        private void AtLeastOneADL()
        {
            Program.Audit("---------------");

            if (!(_inds[2].is_checked || _inds[3].is_checked))
            {
                SetInd(1, "Defaulting to ADL Self.");
            }
        }

        private void HighestIndicatorInEachGroupWins()
        {
            int i, g;
            bool highest_is_on;
            string ind_list;

            Program.Audit("Select highest indicator in each group");

            g = 0;
            highest_is_on = false;
            // Go from bottom up - highest (lowest) indicator in each group wins
            for (i = MAX_INDS; (i >= 1); i--)
            {
                if (_inds[i].radio_group > 0)
                {
                    if (_inds[i].radio_group != g)
                    {
                        //this is a new group
                        g = _inds[i].radio_group;
                        highest_is_on = _inds[i].is_checked;
                    }
                    else
                    {
                        //same group
                        if (highest_is_on)
                        {
                            _inds[i].is_checked = false;             //uncheck a lower number
                        }
                        else
                        {
                            highest_is_on = _inds[i].is_checked;     //save this one
                        }
                    }
                }
            } // next i

            //Echo the indicators for an Audit (no classification will be saved)
            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++)
            {
                if (_inds[i].is_checked) ind_list += "," + i;
            } // next i

            Program.Audit("Final list = " + ind_list.Substring(1));
            Program.Audit("---------------");
        }


        private void CheckProcs()
        {
            CheckProc_1_2();
            CheckProc_3();
            CheckProc_4();
            CheckProc_5();
            CheckProc_6();
            CheckProc_7();

        }
        private void CheckProc_1_2()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MHA1. 1-1 safety observation by RN");
            Program.VerboseAudit("MHA2. 1-1 safety observation by non-RN");
            Program.VerboseAudit("---------------");

            DateTime classout = DateTime.MinValue;

            DateTime return_evdt = DateTime.MinValue;
            var actlist = new List<CHART_ITEM>();

            string contrn = "Continuous observation by RN with patient";
            string contnonrn = "Continuous observation by non-RN staff with patient";
            string cont2 = "Continuous observation by two staff with patient";

            var query = StartNewQuery(SearchDepth.SearchSince13Hrs);    // add custom time range below
            query = query.Where(e => e.CODE == "9993040009234");
            query = query.OrderBy(e => e.EVENT_DATETIME);
            foreach (var item in query)
            {
                if (item.CODE == "9993040009234")
                {
                    // item 234 is valid  save its code,result,time
                    actlist.Add(item);
                    Program.VerboseAudit("Valid:" + item.CODE + "+" + item.RESULT + " " + item.EVENT_DATETIME.ToString());
                }
            }
            //Now we have a list of all the valid 9234 items' times.
            //Use this list to determine the start/stop times
            CHART_ITEM[] actary = actlist.ToArray();
            int num_acts = 0;
            int actnum = 1;
            //  where there are adjacent same-locations, mark the duplicates as remove.
            for (int i = 0; i <= actary.GetUpperBound(0); i++)
            {
                num_acts++;
                Program.VerboseAudit("i:" + i + " result=" + actary[i].RESULT + " evdt=" + actary[i].EVENT_DATETIME.ToString());
                if (actary[i].RESULT.ToLower().Contains(contrn.ToLower())
                    || actary[i].RESULT.ToLower().Contains(contnonrn.ToLower())
                    || actary[i].RESULT.ToLower().Contains(cont2.ToLower()))
                {
                    //mark record as start
                    actary[i].DESCRIPTION = "start";
                }
                else
                {
                    //mark record as stop
                    actary[i].DESCRIPTION = "stop";
                }
            }
            //Now the list has start and stop marks.
            //Distill these mark info into start-stop records
            var start_stop_list = new List<proc_data>();
            var ssrec = new proc_data();
            int start_idx = -99;
            int stop_idx = -1;
            int pnum = 0;
            int this_pnum = 0;
            int ssnum = 0;
            int last_start = -99;
            int last_stop = -99;
            int mark_last_idx, mark_last_start, mark_last_stop, mark_pnum;

            for (int i = 0; i <= actary.GetUpperBound(0); i++)
            {
                Program.VerboseAudit("i:" + i + " start_idx=" + start_idx + " stop_idx=" + stop_idx + " desc=" + actary[i].DESCRIPTION + " evdt=" + actary[i].EVENT_DATETIME.ToString());
                if (start_idx < 0)
                {
                    if (actary[i].DESCRIPTION == "start")
                    {
                        Program.VerboseAudit("found start");
                        start_idx = i;
                        last_start = i;
                        if (actary[i].RESULT.ToLower().Contains(contrn.ToLower())) pnum = 1;
                        if (actary[i].RESULT.ToLower().Contains(contnonrn.ToLower())) pnum = 2;
                        if (actary[i].RESULT.ToLower().Contains(cont2.ToLower())) pnum = 3;
                    }
                }
                else //if (start_idx >= 0)
                {
                    if (actary[i].RESULT.ToLower().Contains(contrn.ToLower())) this_pnum = 1;
                    if (actary[i].RESULT.ToLower().Contains(contnonrn.ToLower())) this_pnum = 2;
                    if (actary[i].RESULT.ToLower().Contains(cont2.ToLower())) this_pnum = 3;
                    if (actary[i].DESCRIPTION == "stop" || pnum != this_pnum)
                    {
                        Program.VerboseAudit("found stop");
                        stop_idx = i;
                        last_stop = i;

                        if (actary[start_idx].EVENT_DATETIME < _pat.pull_finish.AddHours(-8)
                            && actary[stop_idx].EVENT_DATETIME > _pat.pull_finish.AddHours(-8))
                            actary[start_idx].EVENT_DATETIME = _pat.pull_finish.AddHours(-8);
                        else if (actary[start_idx].EVENT_DATETIME < _pat.pull_finish.AddHours(-8)
                                 && actary[stop_idx].EVENT_DATETIME < _pat.pull_finish.AddHours(-8))
                            // do not add
                            actary[start_idx].EVENT_DATETIME = DateTime.MinValue;
                        if (actary[start_idx].EVENT_DATETIME > DateTime.MinValue)
                        {
                            ssrec.procedure_number = pnum;
                            ssrec.start = actary[start_idx].EVENT_DATETIME;
                            ssrec.finish = actary[stop_idx].EVENT_DATETIME;
                            start_stop_list.Add(ssrec);
                            ssnum++;
                        }
                        start_idx = -99;
                        stop_idx = -99;
                        if (pnum != this_pnum)
                        {
                            Program.VerboseAudit("found start due to change in obs");

                            mark_last_idx = start_idx;
                            mark_last_start = last_start;
                            mark_last_stop = last_stop;
                            mark_pnum = pnum;
                            pnum = this_pnum;
                            start_idx = i;
                            last_start = i;
                            last_stop = -99;
                        }
                    }
                }
            }
            Program.VerboseAudit("last_start=" + last_start + " last_stop=" + last_stop);
            if (last_start >= 0 && last_stop == -99 || ssnum >= 2) //&& last_stop >= 0 && last_stop < last_start)
            {
                ssrec.procedure_number = pnum;
                ssrec.start = actary[last_start].EVENT_DATETIME;
                //ssrec.start = _pat.pull_finish.AddHours(-4);
                ssrec.finish = _pat.pull_finish;
                if (ssrec.start.AddMinutes(60) < ssrec.finish)
                {
                    start_stop_list.Add(ssrec);
                    ssnum++;
                    Program.VerboseAudit("ssnuma=" + ssnum);
                }
                else
                    Program.VerboseAudit("ignore due to less than 1 hr starta=" + ssrec.start.ToString() + " to " + ssrec.finish.ToString());
            }
            else if (last_start >= 0 && last_stop == -99)
            { // no stop --end at pulltime 
                ssrec.procedure_number = pnum;
                //ssrec.start = actary[last_start].EVENT_DATETIME;
                ssrec.start = _pat.pull_finish.AddHours(-4);
                ssrec.finish = _pat.pull_finish;
                start_stop_list.Add(ssrec);
                ssnum++;
                Program.VerboseAudit("ssnumb=" + ssnum);
            }

            if (ssnum > 0)
            {
                int i;
                proc_data[] procary = start_stop_list.ToArray();
                for (i = 0; i <= procary.GetUpperBound(0); i++)
                { //change the times in the start stop list in order to take care of l.t. 1hr legs
                    Program.VerboseAudit("procary[i]: i=" + i + " start= " + procary[i].start.ToString() + " stop: " + procary[i].finish.ToString());
                    if (procary[i].start.AddMinutes(60) > procary[i].finish)
                    {
                        procary[i].procedure_number = 0;
                        if (i + 1 <= procary.GetUpperBound(0))
                        {
                            if (procary[i + 1].start == procary[i].finish)
                            {
                                procary[i + 1].start = procary[i].start;
                            }

                        }

                    }
                }
                //start_stop_list = procary.ToList();

                string sflag = "";
                if (ssnum > 1) sflag = "s";
                Program.VerboseAudit("Num start-stop" + sflag + "=" + ssnum);
                DateTime enddt;
                //foreach (var ss in start_stop_list)
                DateTime prev_start = DateTime.MinValue, prev_finish = DateTime.MinValue;
                for (i = 0; i <= procary.GetUpperBound(0); i++)
                {
                    Program.VerboseAudit("*procary[i]: i=" + i + " start= " + procary[i].start.ToString() + " stop: " + procary[i].finish.ToString());
                    if (prev_start == DateTime.MinValue) // then this is the first item
                    {
                        prev_start = procary[i].start;
                        prev_finish = procary[i].finish;
                    }
                    else if (procary[i].start == prev_start)
                    {
                        Program.VerboseAudit("prevstart=" + prev_start.ToString());
                        if (procary[i].finish >= prev_finish)
                        {
                            procary[i].remove = true;
                            prev_finish = procary[i].finish;
                            Program.VerboseAudit("REMOVE procary[i]: i=" + i + " start= " + procary[i].start.ToString() + " stop: " + procary[i].finish.ToString());
                        }
                    }
                    else
                    {
                        prev_start = procary[i].start;
                        prev_finish = procary[i].finish;
                    }
                }
                for (i = 0; i <= procary.GetUpperBound(0); i++)
                {
                    if (!procary[i].remove && procary[i].procedure_number > 0)
                    {
                        var proc = new proc_data();
                        proc.procedure_number = procary[i].procedure_number;
                        proc.start = procary[i].start;
                        proc.finish = procary[i].finish;
                        Program.VerboseAudit("ssproc=" + procary[i].procedure_number + " ss start: " + procary[i].start.ToString() + " ss stop: " + procary[i].finish.ToString());
                        if (procary[i].procedure_number == 3)
                        {
                            if (ProcExistsInDB(1, procary[i].start, out enddt) || ProcExistsInDB(2, procary[i].start, out enddt))
                            {
                                proc.start = enddt;
                                Program.VerboseAudit("endta=" + enddt.ToString());
                            }
                            if (proc.start < proc.finish)
                            {
                                if ((proc.start >= _pat.effective || proc.finish >= _pat.effective))
                                {
                                    Program.VerboseAudit("pnum=" + proc.procedure_number + "  start: " + proc.start.ToString() + "  finish: " + proc.finish.ToString());
                                    Program.VerboseAudit("ADDING 2 ACTIVITIES");
                                    proc.procedure_number = 1;
                                    _procs.Add(proc);
                                    _procs.Add(proc);
                                }
                            }

                        }
                        else
                        {
                            if (ProcExistsInDB(procary[i].procedure_number, procary[i].start, out enddt))
                            {
                                //ss start: 6 / 19 / 2019 9:15:00 PM ss stop: 6 / 20 / 2019 7:00:00 AM
                                //pnum = 2  start: 6 / 20 / 2019 7:00:00 AM finish: 6 / 20 / 2019 7:00:00 AM
                                proc.start = enddt;
                                Program.VerboseAudit("endtb=" + enddt.ToString());
                            }
                            if (proc.start < proc.finish)
                            {
                                if ((proc.start >= _pat.effective || proc.finish >= _pat.effective))
                                {
                                    if (proc.start.AddMinutes(60) > proc.finish)
                                    {
                                        Program.VerboseAudit("procary[i]: len<60min");
                                        if (i + 1 <= procary.GetUpperBound(0))
                                        {
                                            if (procary[i + 1].start == proc.finish)
                                            {
                                                proc.finish = proc.start.AddMinutes(60);
                                                procary[i + 1].start = proc.finish;
                                                Program.VerboseAudit("procary[i]: setting finish to=" + procary[i].finish.ToString());
                                                Program.VerboseAudit("procary[i]: setting start[i+1] to=" + procary[i + 1].start.ToString());
                                                if (procary[i + 1].start.AddMinutes(60) > procary[i + 1].finish)
                                                {
                                                    if (i + 2 <= procary.GetUpperBound(0))
                                                    {
                                                        if (procary[i + 2].start == procary[i + 1].finish)
                                                        {
                                                            procary[i + 1].finish = procary[i + 1].start.AddMinutes(60);
                                                            procary[i + 2].start = procary[i + 1].finish;
                                                            if (procary[i + 2].start.AddMinutes(60) > procary[i + 2].finish)
                                                                procary[i + 2].finish = procary[i + 2].start.AddMinutes(60);
                                                        }
                                                    }
                                                    else
                                                        procary[i + 1].finish = procary[i + 1].start.AddMinutes(60);
                                                }
                                            }
                                        }
                                        else
                                            proc.finish = proc.start.AddMinutes(60);
                                    }
                                    Program.VerboseAudit("pnum=" + proc.procedure_number + "  start: " + proc.start.ToString() + "  finish: " + proc.finish.ToString());
                                    _procs.Add(proc);
                                }
                            }
                        }
                    }
                }
            }

        }
        private bool ProcExistsInDB(int pnum, DateTime startdt, out DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            Program.VerboseAudit("ProcExistsInDB: pnum=" + pnum + " startdt=" + startdt.ToString());
            int ct = 0;
            enddt = DateTime.MinValue;
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            //&& (proc.PROCEDURE_DATETIME <= startdt)
                            && (proc.DEPARTURE_DATETIME > startdt)
                            && (((pnum > 2) && (ans.PROCEDURE_NUMBER == pnum)) || ((pnum <= 2) && (ans.PROCEDURE_NUMBER <= 2)))
                        orderby proc.DEPARTURE_DATETIME descending
                        select new { proc.DEPARTURE_DATETIME };
            ct = query.Count();
            if (ct > 0)
                enddt = (DateTime)query.First().DEPARTURE_DATETIME;
            Program.VerboseAudit("ProcExistsInDB: pnum=" + pnum + " returns " + ct);
            return ct > 0;
        }
        private void CheckProc_3()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MHA3. Off unit accompanied by RN");
            Program.VerboseAudit("---------------");
            string start_reslist;
            string stop_reslist;
            DateTime startdt, stopdt;

            // private bool GetEVDT(string cat, string code_list, string desc_list, string field, 
            //string res, 
            //int comparison, DateTime compevdt, out DateTime return_evdt, SearchDepth search_depth)
            bool suppress = false;
            foreach (var p in _procs)
            {
                suppress |= (p.procedure_number == 1 || p.procedure_number == 2);
            }
            if (suppress)
            {
                Program.VerboseAudit("Suppressing Activity #3 because Activity #1 or #2 present.");
                return;
            }

            DateTime return_evdt = DateTime.MinValue;

            start_reslist = "Left unit with RN";
            if (GetEVDT("", "9993040000355", "", "", start_reslist, 0, DateTime.MinValue, out startdt, SearchDepth.SearchSince13Hrs))
            {
                stop_reslist = "Returned to unit";
                if (GetEVDT("", "9993040000355", "", "", stop_reslist, 2, startdt, out stopdt, SearchDepth.SearchSince13Hrs))
                {
                    if (stopdt >= startdt.AddMinutes(60))
                    {
                        AddSimpleProc(3, startdt, stopdt.AddMinutes(15));
                    }
                }
                else
                {
                    stopdt = startdt.AddMinutes(60);
                    AddSimpleProc(3, startdt, stopdt);
                }
            }

        }
        private void CheckProc_4()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MHA4. Off unit accompanied by non-RN");
            Program.VerboseAudit("---------------");
            string start_reslist;
            string stop_reslist;
            DateTime startdt, stopdt;

            DateTime return_evdt = DateTime.MinValue;
            bool suppress = false;
            foreach (var p in _procs)
            {
                suppress |= (p.procedure_number == 1 || p.procedure_number == 2);
            }
            if (suppress)
            {
                Program.VerboseAudit("Suppressing Activity #4 because Activity #1 or #2 present.");
                return;
            }

            start_reslist = "Left unit with nursing unlicensed staff";
            if (GetEVDT("", "9993040000355", "", "", start_reslist, 0, DateTime.MinValue, out startdt, SearchDepth.SearchSince13Hrs))
            {
                stop_reslist = "Returned to unit";
                if (GetEVDT("", "9993040000355", "", "", stop_reslist, 2, startdt, out stopdt, SearchDepth.SearchSince13Hrs))
                {
                    if (stopdt >= startdt.AddMinutes(60))
                    {
                        AddSimpleProc(4, startdt, stopdt.AddMinutes(15));
                    }
                }
                else
                {
                    stopdt = startdt.AddMinutes(60);
                    AddSimpleProc(4, startdt, stopdt);
                }
            }

            //string reslist = "Ambulate in hall,Ambulate in room,Bathroom privileges,Bedrest,Bike,Chair,Commode";
            //reslist += ",Dangle,Supervised exercise,Held,Stand at bedside,Tilt table,Turn,Up in chair";
            //reslist += ",Wagon,In bed,Bedpan";
            //if (GetEVDT("", "9990000305560", "", "", reslist, 0, DateTime.MinValue, out startdt, SearchDepth.SearchSince13Hrs))
            //{
            //}

        }

        private void AddSimpleProc(int pnum, DateTime evdt, DateTime enddt)
        {
            if (ProcExists(pnum, evdt, enddt))
            {
                Program.Audit("Activity " + pnum + ": already exists");
            }
            else
            {
                if (ActivityFits(evdt, enddt))
                {
                    ProcOverlapsInDB_PEID(pnum, evdt, enddt); // then delete the db
                    var proc = new proc_data();
                    proc.procedure_number = pnum;
                    proc.start = evdt;
                    proc.finish = enddt;
                    _procs.Add(proc);
                    Program.Audit("Activity " + pnum + ": Found between " + evdt + " and " + enddt);
                }
            }
        }
        private bool ActivityFits(DateTime beg, DateTime fin)
        {
            bool ok = false;
            int unit_id = 0;
            string sql = "select el.unit_id from ENCOUNTER_LOCATION as el";
            sql += " where el.ENCOUNTER_ID=" + _pat.encounter_id;
            sql += " and el.SPECIAL_UNIT_ID is null";
            sql += " and el.EFFECTIVE_DATETIME_IN<='" + beg.ToString() + "'";
            sql += " and el.EFFECTIVE_DATETIME_OUT>='" + fin.ToString() + "'";

            Program.VerboseAudit(sql);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd = new SqlCommand(sql, db2);
            SqlDataReader dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr2.Read())
            {
                if (dr2["UNIT_ID"] != DBNull.Value)
                    unit_id = PFSDBUtility.DBToInt(dr2["UNIT_ID"]);
            }
            ok = (unit_id > 0);
            db2.Close();
            return ok;
        }
        private bool ProcOverlapsInDB_PEID(int pnum, DateTime startdt, DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            LoadPatientProceduresIfNeeded();
            bool overlap_exists = false;
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && ((proc.PROCEDURE_DATETIME <= startdt) && (enddt > proc.PROCEDURE_DATETIME))
                            //&& ( ! (proc.PROCEDURE_DATETIME == startdt) && (proc.DEPARTURE_DATETIME == enddt))
                            && (proc.CLASSIFIED_BY_ID < 0)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new { proc.PROCEDURE_EVENT_ID, proc.PROCEDURE_DATETIME, proc.DEPARTURE_DATETIME };
            overlap_exists = (query.Count() > 0);
            foreach (var a in query)
            {
                Program.VerboseAudit("Will Delete act: procdt=" + a.PROCEDURE_DATETIME.ToString() + "  depdt=" + a.DEPARTURE_DATETIME.ToString());
                Program.VerboseAudit("because it overlays startdt=" + startdt.ToString() + "  enddt=" + enddt.ToString());
                DeleteActivity(a.PROCEDURE_EVENT_ID);
            }
            //            peid = 0;
            return (overlap_exists);
        }
        private void DeleteActivity(int peid)
        {
            //            delete PROCEDURE_ANSWER where PROCEDURE_EVENT_ID=7211351
            //delete RPT_PROC_BY_DAY where PROCEDURE_EVENT_ID=7211351
            //delete PROCEDURE_EVENT where PROCEDURE_EVENT_ID=7211351
            if (peid == 0) return;

            Program.VerboseAudit("db ProcAnsw Deleting peid=" + peid);
            var db = PFSDBUtility.NewPfsDataContext();
            var query = from ia in db.PROCEDURE_ANSWERs
                        where (ia.PROCEDURE_EVENT_ID == peid)
                        select ia;
            if (query.Count() > 0)
            {
                var items = query.ToList();
                foreach (var item in items)
                    db.PROCEDURE_ANSWERs.DeleteOnSubmit(item);
                db.SubmitChanges();
            }

            Program.VerboseAudit("db RptProc Deleting peid=" + peid);
            var db2 = PFSDBUtility.NewPfsDataContext();
            var query2 = from r in db2.RPT_PROC_BY_DAYs
                         where (r.PROCEDURE_EVENT_ID == peid)
                         select r;
            if (query2.Count() > 0)
            {
                var items2 = query2.ToList();
                foreach (var item2 in items2)
                    db2.RPT_PROC_BY_DAYs.DeleteOnSubmit(item2);
                db2.SubmitChanges();
            }

            Program.VerboseAudit("db ProcEvent Deleting peid=" + peid);
            var db3 = PFSDBUtility.NewPfsDataContext();
            var query3 = from ce in db3.PROCEDURE_EVENTs
                         where (ce.PROCEDURE_EVENT_ID == peid)
                         select ce;
            if (query3.Count() > 0)
            {
                var items3 = query3.ToList();
                foreach (var item3 in items3)
                    db3.PROCEDURE_EVENTs.DeleteOnSubmit(item3);
                db3.SubmitChanges();
            }
        }

        private bool QueuedProcOverlaps(int pnum, DateTime startdt, DateTime enddt)
        {
            bool overlap = false;

            proc_data[] pary = _procs.ToArray();
            for (int i = 0; i <= pary.GetUpperBound(0); i++)
            {
                if (pary[i].procedure_number == pnum)
                {
                    //overlap if   p.start between stardt and enddt
                    //             p.finish between startdt and enddt
                    //             startdt >= p.start and enddt <= p.finish
                    if ((pary[i].start >= startdt) && (pary[i].start < enddt) ||
                        (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
                        (startdt >= pary[i].start) && (enddt <= pary[i].finish))
                    {
                        overlap = true;
                    }

                    if ((pary[i].start >= startdt) && (pary[i].start < enddt) && (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
                        (pary[i].start < startdt) && (pary[i].finish > startdt) && (pary[i].finish < enddt)
                        )
                    {
                        // the proc in the list is completely contained in startdt/endt pair
                        // change this proc to be the startdt/enddt
                        // overlap is still true, but the list item will be updated with the encompassing times
                        //Program.Audit("Procedure " + pnum + ": " + startdt + " - " + enddt + " will trump " + pary[i].start + " - " + pary[i].finish);
                        pary[i].start = startdt;
                        pary[i].finish = enddt;
                    }
                }
            }
            _procs = pary.ToList();
            return overlap;
        }


        private bool ProcExists(int pnum, DateTime startdt, DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && (proc.PROCEDURE_DATETIME == startdt)
                            && (proc.DEPARTURE_DATETIME == enddt)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new { proc.PROCEDURE_EVENT_ID };
            return (query.Count() > 0);
        }
        private void CheckProc_5()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MHA5. 1:1 Continuous Observation >1 Hour by RN");
            Program.VerboseAudit("---------------");
            DateTime startdt, enddt, evdt;
            int mins = 0;
            int ct = 0;
            DateTime return_evdt = DateTime.MinValue;

            string[] woundmins = { "> 180 minutes", "> 120 minutes", "> 90 minutes", "> 60 minutes" };
            string actor1 = "Unit Based Nurse";
            string actor2 = "Additional Nurse assist required";
            string actor3 = "Patient;completed;with assistance";
            int numactors = 0;

            var query1 = StartNewQuery(SearchDepth.SearchSince9Hrs);
            query1 = AndItemFilter(query1, "", "9993040006782", "", "", "");
            query1 = query1.Where(e => woundmins.Any(v => e.RESULT == v));
            query1 = query1.OrderBy(e => e.EVENT_DATETIME);

            var query1b = (from item in query1
                           select new { evdt = item.EVENT_DATETIME, res = item.RESULT }
                                       ).Distinct();
            foreach (var item1 in query1b)
            {
                numactors = 0;
                Program.VerboseAudit("Found result=" + item1.res + " at " + item1.evdt.ToString());
                var query2 = StartNewQuery(SearchDepth.SearchSince9Hrs);
                query2 = AndItemFilter(query2, "", "9993040021271", "", "", "Unit Based Nurse,Additional Nurse assist required,Patient;completed;with assistance");
                query2 = query2.Where(e => e.EVENT_DATETIME == item1.evdt);
                ct = query2.Count();
                if (ct > 0)
                    foreach (var item2 in query2)
                    {
                        Program.VerboseAudit("Found staff=" + item2.RESULT);
                        numactors = (item2.RESULT.ToUpper().Contains(actor1.ToUpper()) ? 1 : 0)
                            + (item2.RESULT.ToUpper().Contains(actor2.ToUpper()) ? 1 : 0)
                            + (item2.RESULT.ToUpper().Contains(actor3.ToUpper()) ? 1 : 0);
                        if (numactors == 1)
                        {
                            evdt = item1.evdt;
                            mins = (int)item1.res.Substring(2, item1.res.IndexOf("min") - 3).Val();
                            Program.Audit("Activity 5: Minutes=" + mins);
                            if (mins >= 60)
                                if (!QueuedProcOverlaps(5, evdt, evdt.AddMinutes(mins)))
                                    if (!ProcExistsInDB(5, evdt, out enddt))
                                    {
                                        var proc = new proc_data();
                                        proc.procedure_number = 5;
                                        proc.start = evdt;
                                        proc.finish = evdt.AddMinutes(mins);
                                        _procs.Add(proc);
                                        Program.Audit("Activity 5: Found at " + evdt + " for " + mins + " mins.");
                                    }
                        }
                    }
                else if (ct == 0)
                {
                    var query3 = StartNewQuery(SearchDepth.SearchSince9Hrs);
                    query3 = AndItemFilter(query3, "", "9993040021271", "", "", "");
                    query3 = query3.Where(e => e.EVENT_DATETIME == item1.evdt);
                    query3 = query3.Where(e => !e.RESULT.ToUpper().Contains(actor1.ToUpper())
                                            && !e.RESULT.ToUpper().Contains(actor2.ToUpper())
                                            && !e.RESULT.ToUpper().Contains(actor3.ToUpper()));
                    ct = query3.Count();
                    if (ct == 0)
                    {
                        evdt = item1.evdt;
                        mins = (int)item1.res.Substring(2, item1.res.IndexOf("min") - 3).Val();
                        Program.Audit("Activity 5: Minutes=" + mins);
                        if (mins >= 60)
                            if (!QueuedProcOverlaps(5, evdt, evdt.AddMinutes(mins)))
                                if (!ProcExistsInDB(5, evdt, out enddt))
                                {
                                    var proc = new proc_data();
                                    proc.procedure_number = 5;
                                    proc.start = evdt;
                                    proc.finish = evdt.AddMinutes(mins);
                                    _procs.Add(proc);
                                    Program.Audit("Activity 5: Found at " + evdt + " for " + mins + " mins.");
                                }
                    }
                }
            }


        }

        private void CheckProc_6()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MHA6. 1:1 Continuous Observation >1 Hour by non-RN");
            Program.VerboseAudit("---------------");
            //DoProc(6, "A_MHAcuExtensive");
        }

        private void CheckProc_7()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MHA7. 2:1 Continuous Observation >1 Hour by RN");
            Program.VerboseAudit("---------------");

            string[] airways = { "endotracheal tube","Esophageal - tracheal tube","laryngeal mask airway","nasopharyngeal airway",
                "oropharyngeal airway","tracheostomy","other"};
            string[] vents = { "bag - valve - mask","bag - valve - et tube","bag - valve - tracheostomy","ventilator",
                "bilevel positive airway pressure","continuous positive airway pressure","cpap nasal","cpap mask","positive pressure ventilation","other"};
            string[] events = { "respiratory arrest", "cardiac arrest", "unknown", "other" };

            var query = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
            query = query.Where(e => (e.CODE == "9991600100681") // && e.RESULT.ToLower().ContainsAny(airways))
                                      ||
                                     (e.CODE == "9991600100682") // && e.RESULT.ToLower().ContainsAny(vents))
                                      ||
                                     (e.CODE == "9991600100646" && e.RESULT.ToLower().ContainsAny(events))
                                );
            query = query.Where(e => e.EVENT_DATETIME > _pat.pull_finish.AddHours(-4));
            query = query.Where(e => e.EVENT_DATETIME <= _pat.pull_finish);
            query = query.OrderBy(e => e.EVENT_DATETIME);
            foreach (var item in query)
            {
                Program.VerboseAudit("Found " + item.CODE + " / " + item.DESCRIPTION + " / " + item.RESULT + " at " + item.EVENT_DATETIME.ToString());
                AddSimpleProc(7, item.EVENT_DATETIME, item.EVENT_DATETIME.AddHours(1));
            }

        }


        private const string DATETIME_FORMAT = "yyyyMMddHHmmss";              // ISO Date/Time w/o seconds

        private void OutputClass(bool use_default)
        {
            string outstr, ind_list, desc, str_pull_dt, str_in_dt, str_out_dt = "";
            int i, tc_event_id;

            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         212 242 304
            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1
            //1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901 3456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
            //1       |10 5            |                |                |        |10023121779         |ALEXANDER                       |GLORIA                          |                                |0508    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289530  |           |201403030400                                                                      |NNYNNNNYYNNYNNNYNNNYYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
            //1       |10 5            |                |                |        |10024544908         |ARMSTRONG                       |AARON                           |                                |0535    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289531  |           |201403030400                                                                      |NYNNYNNNNNNYNNNYNNNNYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
            //|0547    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289532  |           |201403030400                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN
            //|0547    |A   |201403030700|                |201403030700    |18  |C|    |3040|1440|18289532  |           |201403030700                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN

            if (Program.g_is_test)
                tc_event_id = 9999;
            else
                tc_event_id = PFSDBUtility.NextGID();                         //get a unique id for this class
                                                                              //            str_pull_dt = _pat.pull_finish.ToString(DATETIME_FORMAT);
            str_pull_dt = _pat.pull_finish.ToString(DATETIME_FORMAT);
            str_in_dt = _pat.effective.ToString(DATETIME_FORMAT);
            //if (_pat.unit_departure != null && _pat.unit_departure != DateTime.MinValue)
            if (_pat.effective_out == Program.g_pull_finish && _pat.unit_departure == DateTime.MinValue)
                str_out_dt = "";
            else
                str_out_dt = _pat.effective_out.ToString(DATETIME_FORMAT);

            outstr = _pat.facilty_code.FixedWidth(8);                       //(facility code)
            outstr += "|" + _pat.unit_name.FixedWidth(16);
            outstr += "|" + "".FixedWidth(16);                               //(unit code)
            outstr += "|" + txarea.FixedWidth(16);                               //(area code)
            outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
            outstr += "|" + _pat.acct.FixedWidth(20);
            outstr += "|" + _pat.last_name.FixedWidth(32);
            outstr += "|" + _pat.first_name.FixedWidth(32);
            outstr += "|" + _pat.middle_name.FixedWidth(32);
            outstr += "|" + _pat.room.FixedWidth(8);
            outstr += "|" + _pat.bed.FixedWidth(4);
            outstr += "|" + str_in_dt;        //IN
            outstr += "|" + "".FixedWidth(14);                               //(login)
            outstr = outstr.FixedWidth(249);
            outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
            outstr += "|" + "C".FixedWidth(1);                               //record type = class
            outstr += "|" + "".FixedWidth(4);                                //(stage)
            outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
            outstr += "|" + _pat.range.ToString().FixedWidth(4);             //TC pull range
            outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
            outstr += "|";
            outstr = outstr.FixedWidth(294);
            outstr += "|" + str_in_dt;        //IN
            outstr = outstr.FixedWidth(346);
            //if (_pat.unit_departure != null && _pat.unit_departure != DateTime.MinValue)
            //                outstr += "|" + _pat.unit_departure.ToString(DATETIME_FORMAT);        //OUT
            outstr += "|" + str_out_dt;        //OUT
            outstr = outstr.FixedWidth(377);
            outstr += "|";

            //if (use_default)
            //{ //make all is_checked = false and then mark defaults
            //    Program.VerboseAudit("Patient Will receive default indicators " + _pat.default_inds_str);
            //    for (i = 1; (i <= MAX_INDS); i++)
            //    {
            //        _inds[i].is_checked = false;
            //    }
            //    foreach (var ind in _pat.default_inds)
            //    {
            //        if (ind <= _inds.GetUpperBound(0))
            //        {
            //            _inds[ind].is_checked = true;
            //        }
            //    }
            //}

            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++)
            {
                if (_inds[i].is_checked)
                {
                    outstr += "Y";
                    ind_list += "," + i;
                }
                else
                {
                    outstr += "N";
                }
            } // next i
            ind_list = ind_list.Substring(1);                           //strip leading comma
                                                                        //                                                                                                   1                                                                                                   2                                                                                                   3
                                                                        //         1         2         3         4         5         6         7         8         9         0         1         2         3         4         5         6         7         8         9         0         1         2         3         4         5         6         7         8         9         0         1         2         3         4         5         6         7         8         9         0
                                                                        //1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
                                                                        //1       |DO6D            |                |                |        |2000192224892       |BEHNAM                          |KENDRA                          |LEE                             |RDO6311 |P   |20180717110000|                               |20  |C|    |5399|480 |56103278  |           |20180717110000                                     |                              |YNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN


            string str7am;
            Program.outfile.WriteLine(outstr);                          //output to transparent.txt
            if (Program.g_pull_finish.Hour == 7)
            {
                if (str_out_dt.Substring(8, 4) == "0700") //create the 7am at 3am
                {
                    str7am = outstr.Substring(0, 203) + Program.g_pull_finish.ToString(DATETIME_FORMAT) + outstr.Substring(217, 78) + Program.g_pull_finish.ToString(DATETIME_FORMAT) + " ".Repeat(69) + outstr.Substring(378, 120);
                    Program.outfile.WriteLine(str7am);
                }
            }

            Program.Audit("");
            desc = "Classified: " + ind_list;
            if (Program.g_is_test)
            {
                Program.Audit(desc);
            }
            else
            {
                //Save the selected indicators plus both Program.Audits in the event log; link with tc_event_id
                PFSEventLog.AddTransparentMappingEventLogEntry(
                    desc, Program.gLogUnitID, Program.gLogEncounterID,
                    tc_event_id, Program.gLogMapperVersion,
                    Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
            }
        }

        private void OutputProcs()
        {
            int i;
            string outstr, proc_list, desc;
            int tc_event_id;

            foreach (var proc in _procs)
            {
                if (Program.g_is_test)
                    tc_event_id = 9999;
                else
                    tc_event_id = PFSDBUtility.NextGID();                         //get a unique id for this proc

                outstr = _pat.facilty_code.FixedWidth(8);                       //(facility code)
                outstr += "|" + _pat.unit_name.FixedWidth(16);
                outstr += "|" + "".FixedWidth(16);                               //(unit code)
                outstr += "|" + txarea.FixedWidth(16);                               //(area code)
                outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
                outstr += "|" + _pat.acct.FixedWidth(20);
                outstr += "|" + _pat.last_name.FixedWidth(32);
                outstr += "|" + _pat.first_name.FixedWidth(32);
                outstr += "|" + _pat.middle_name.FixedWidth(32);
                outstr += "|" + _pat.room.FixedWidth(8);
                outstr += "|" + _pat.bed.FixedWidth(4);
                outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //204 proc dt
                outstr += "|" + "".FixedWidth(14);                               //(login)
                outstr = outstr.FixedWidth(249);
                outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
                outstr += "|" + "P".FixedWidth(1);                               //record type = class
                outstr += "|" + "".FixedWidth(4);                                //(stage)
                outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
                outstr += "|" + _pat.range.ToString().FixedWidth(4);             //TC pull range
                outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
                outstr += "|";
                outstr = outstr.FixedWidth(294);
                outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //296 procdt in
                outstr = outstr.FixedWidth(346);
                //if (_pat.unit_departure != null && _pat.unit_departure != DateTime.MinValue)
                //                outstr += "|" + _pat.unit_departure.ToString(DATETIME_FORMAT);        //OUT
                outstr += "|" + proc.finish.ToString(DATETIME_FORMAT);          //348 procdt out
                outstr = outstr.FixedWidth(377);
                outstr += "|";

                proc_list = "";
                for (i = 1; (i < MAX_PROCS); i++)
                {
                    if (proc.procedure_number == i)
                    {
                        outstr += "Y";
                        proc_list += "," + i;
                    }
                    else
                    {
                        outstr += "N";
                    }
                } // next i
                proc_list = proc_list.Substring(1);                             //strip leading comma

                Program.outfile.WriteLine(outstr);                              //output to transparent.txt

                desc = "Activities: " + proc_list;
                if (Program.g_is_test)
                {
                    Program.Audit(desc);
                }
                else
                {
                    //Save the selected procedures plus both Program.Audits in the event log; link with tc_event_id
                    //(this assumes the procedure Program.Audit is mixed in with the class Program.Audit)
                    PFSEventLog.AddTransparentMappingEventLogEntry(desc, Program.gLogUnitID, Program.gLogEncounterID,
                        tc_event_id, Program.gLogMapperVersion,
                        Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
                }
            } // next proc
        } // OutputProcs

    } //class MentalHealth
}
