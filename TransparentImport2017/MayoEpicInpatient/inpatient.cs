﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;                     // for LINQ; add a reference
using System.Data.SqlClient;                // for SqlConnection
using System.IO;                            // for Directory, Path, Stream
using PfsShared;                            // add a reference to Shared2 project

// ============================================================================
// Inpatient transparent mapping -- GOES HERE --
// MAYO ROCHESTER EPIC
// ============================================================================
// This processes one patient.  Only one database query is used.  :)
//
// All search functions use exact match for category, description and field name.
// Codes and results can be a single word or a list of words.
// Codes default to exact match; prefix with CODE_CODE_LIKE_PREFIX if desired (single word only)
// Results default to like matching; prefix with RESULT_EXACT_MATCH_PREFIX if desired (single word only)
//
// All searches are case insensitive.
//
//
namespace TransparentMapping
{
    class Inpatient
    {
        private const int MAX_INDS = 120;
        private const int MAX_PROCS = 20;

        private const string CODE_LIKE_PREFIX = "%!";       // use to make codes %like%
        private const string EXACT_MATCH_PREFIX = "&!";     // use to make desc/result exact match
        private const string CHAR_COMMA = "||";             // use to insert literal comma in word list
        private const string NOT_PREFIX = "!!";             // use to insert literal comma in word list
        private const string AVOID_NEGATIVE = "!;";
        private const string EXACT_SQL_PREFIX = "!SQL!";     // use to give sql constraint as given
        private const string STARTS_WITH = "[!";     // use to give sql constraint as given

        private struct indicator_data {
            public bool is_checked;
            public int radio_group;
            public int weight;
        }

        private struct proc_data {
            public int procedure_number;
            public DateTime start;
            public DateTime finish;
        }

        // These are database CHART_ITEMs for this patient
        //private CHART_ITEM[] _chart_items_since_admission;
        //private CHART_ITEM[] _chart_items_since_unit_arrival;
        //private CHART_ITEM[] _chart_items_during_pull_period;
        //private CHART_ITEM[] _chart_items_pull_period_plus;
        //private CHART_ITEM[] _chart_items_since24hrs;
        //private CHART_ITEM[] _chart_items_since13hrs;
        private CHART_ITEM[] _chart_items_since25hrs;
        //private CHART_ITEM[] _chart_items_since9hrs;
        // These are database PROCEDURE_EVENTs for this patient
        private PROCEDURE_EVENT[] _procedure_events;

        private indicator_data[] _inds;
        private List<proc_data> _procs;
        private List<proc_data> _outcomes;

        private const int DEFAULT_BUCKET_SIZE = 20;       //min

        private PatientInfo _pat;
        private int _bucket_size = DEFAULT_BUCKET_SIZE;
        private bool is_default;
        private string txarea;
        private bool periop_found_inpast13hrs = false;

        private bool exclude_periop_data = false;
        private enum SearchDepth {
            SearchDefault,
            SearchPullRange,            //search within the current pull     -- default
            SearchSinceArrival,         //search since arrival to the unit
            SearchSinceAdmission,        //search everything since admission to the hospital
            SearchPullPlus,
            SearchSince24Hrs,
            SearchSince13Hrs,
            SearchSince16Hrs,
            SearchSince25Hrs,
            SearchSince9Hrs,
            SearchSince4Hrs
        }

        private enum CountMode {
            CountAll,
            CountFirst                  //stop after one is found
        }

        private enum GetValueMode {
            GetTotal,
            GetMax,
            GetLast
        }

        enum Frequencies {
            QNONE,
            Q4H,
            Q2H,
            Q1H,
            Q30M
        }

        private struct fmapRow {
            public double los_high;                       //the LOS being testing
            public int[] freq;                           //the count required for each Q value
        }
        private List<fmapRow> _freq_map;                    //1,2,4,8,12,24 hours

        private struct gBucket
        {
            public int bucket;
            public string code;
            public DateTime evdt;
            public bool using_waiver;
            public bool has_all_deps; //has all dependents
            public int num_addl_items;
        }
        private struct gGap
        {
            public int gap;
            public DateTime evdt1;
            public DateTime evdt2;
        }

        private string[] meds_mr2026 = { "FENTANYL","HYDROMORPHONE","MORPHINE","DILTIAZEM",
                "DOPAMINE","NICARDIPINE","NITROGLYCERIN","BUMETANIDE","CALCIUM CHLORIDE",
                "DOBUTAMINE","LEVETIRACETAM","MILRINONE" };
        private string[] meds_mr5077 = {"ADENOSINE","ALPROSTADIL",
"CLEVIDIPINE","DEXMEDETOMIDINE","EPINEPHRINE","EPOPROSTENOL","ESMOLOL",
"ISOPROTERENOL","KETAMINE","LABETALOL","LIDOCAINE","LORAZEPAM",
"MIDAZOLAM","NALOXONE","NESIRITIDE","NITROPRUSSIDE","NOREPINEPHRINE",
"PENTOBARBITAL","PHENYLEPHRINE","PROCAINAMIDE","PROPOFOL","TACROLIMUS",
"TERBUTALINE","THEOPHYLLINE","TORSEMIDE","TREPROSTINIL","VASOPRESSIN","VERAPAMIL","AMIODARONE","RITUXIMAB","OXYTOCIN","DILTIAZEM",
        "DOPAMINE","NICARDIPINE","NITROGLYCERIN" };
        private string[] meds_rate = { "newbag", "new bag", "rateverify", "rate verify", "restarted", "ratechange", "rate change", "started", "continued" };
        private struct med2026and5077
        {
            public string name;
            public bool found;
        }
        //
        // This is the main entry point
        //
        public void ProcessPatient(PatientInfo pat)
        {
            _pat = pat;
            bool use_default = false;
            bool no_chart_items_in_24hrs = false;

            InitIndicators(); // sets is_default
            InitProcs();
            if (!is_default)
            {
                LoadFreqTable();
                CheckIfPeriopInPast13hrs();
                no_chart_items_in_24hrs = (LoadPatientChart() == 0);
                if (no_chart_items_in_24hrs)
                {
                    //SetInd(1, "No Charting Found in the past 24 hours. Defaulting to ADL Self.");
                    Program.Audit("No chart items received in past 24 hrs.");
                }
                else
                {
                    Check_1_2_3_4();
                    Check_5();
                    Check_6_7();
                    Check_8();
                    Check_9();
                    Check_10_11();
                    Check_12_13();
                    Check_14();
                    Check_15_16_17_18();
                    Check_19();
                    Check_20();
                    Check_21_22();
                    Check_23();
                    Check_24();
                    CheckUserDefined();
                    AtLeastOneADL();
                }
            }

            if (!no_chart_items_in_24hrs)
            {
                HighestIndicatorInEachGroupWins();

                if (!is_default)
                {
                    _pat.ptype = DeterminePtypeOfIndicators();
                    CheckProcs();
                    //CheckOutcomes();
                    if (Program.g_do_OW) CheckOtherWorkload();
                }

                if (Program.g_no_output) return;
                //if (_pat.default_ptype > DeterminePtypeOfIndicators())
                //{ // if the default pt type is higher than the pt type of this class
                //  // then if there is a default classification in the past 16 hrs
                //  // then make these indicators the default indicators.
                //    use_default = ExistDefaultInPast16hrs();
                //    if (use_default)
                //    {
                //        Program.VerboseAudit("Default indicators will be used");
                //    }
                //}
                OutputClass(use_default);
                OutputProcs();
                //OutputOutcomes();
            }
        }

        private void CheckIfPeriopInPast13hrs()
        {
            foreach (var perioploc in Program.patperioplist)
            {
                periop_found_inpast13hrs = periop_found_inpast13hrs
                    || (perioploc.in_time >= _pat.pull_finish.AddHours(-13))
                    || (perioploc.out_time >= _pat.pull_finish.AddHours(-13));
            }
        }

        private void InitIndicators()
        {
            // Make a new (empty) indicator array
            _inds = new indicator_data[MAX_INDS + 1];           // This 1 based so add one
            is_default = false;
            txarea = "";
            //if (_pat.los_hours <= 4.0)
            //{
            //    is_default = true;
            //    Program.VerboseAudit("Patient was here 4 hrs or less. Will receive default indicators " + _pat.default_inds_str);
            //    foreach (var ind in _pat.default_inds)
            //    {
            //        if (ind <= _inds.GetUpperBound(0))
            //        {
            //            _inds[ind].is_checked = true;
            //        }
            //    }
            //}


            // get indicator radio groups from the database
            // ** (This database access can be replaced once we have a C# methodolgy cache)
            var db = PFSDBUtility.NewPfsDataContext();
            var query = from idef in db.INDICATOR_DEFINITIONs
                        where (idef.METHODOLOGY_ID == _pat.meth_id)
                        select idef;
            foreach (var idef in query) {
                if (idef.INDICATOR_NUMBER <= _inds.GetUpperBound(0)) {
                    // (convert nulls to zero)
                    _inds[idef.INDICATOR_NUMBER].radio_group = PFSDBUtility.DBToInt(idef.RADIO_GROUP);
                    _inds[idef.INDICATOR_NUMBER].weight = PFSDBUtility.DBToInt(idef.WEIGHT);
                }
            }
        }

        private void InitProcs()
        {
            _procs = new List<proc_data>();
            _outcomes = new List<proc_data>();
        }

        private fmapRow LoadFreqTableRow(double los_high, string values)
        {
            fmapRow fmrow;

            fmrow.los_high = los_high;
            fmrow.freq = new int[Enum.GetNames(typeof(Frequencies)).Length];

            var arr = values.Split(',');
            for (int i = 0; i <= fmrow.freq.GetUpperBound(0); i++)
            {
                fmrow.freq[i] = arr[i].ToInteger();
            }
            return fmrow;
        }

        // These are the number of times you need to do something during a certain time period (LOS)
        // in order for it to be considered one of the frequencies listed.
        private void LoadFreqTable()
        {
            _freq_map = new List<fmapRow>();
            //                              LOS,  None Q4h Q2h Q1h Q30m
            _freq_map.Add(LoadFreqTableRow(1, "    0,  0,  0,  1,  2"));
            _freq_map.Add(LoadFreqTableRow(2, "    0,  0,  1,  2,  4"));
            _freq_map.Add(LoadFreqTableRow(4, "    0,  1,  2,  4,  8"));
            _freq_map.Add(LoadFreqTableRow(6, "    0,  2,  3,  6,  9"));
            _freq_map.Add(LoadFreqTableRow(8, "    0,  2,  4,  8, 12"));
            _freq_map.Add(LoadFreqTableRow(12, "   0,  3,  6, 12, 18"));
            _freq_map.Add(LoadFreqTableRow(16, "   0,  4,  8, 16, 24"));
            _freq_map.Add(LoadFreqTableRow(24, "   0,  6, 12, 24, 36"));
            //New freq table 2/5/14
            //q4	q2	q1	q30     q30
            //            Non-ICU	ICU & SD
            // 4	8	15	29	    36
            // 3	5	9	17	    24
            // 2	4	7	13	    19
            // 2	3	5	10	    13

            //to read this table: if you have at least 6 asessmenets in a 12-hour period, you get Q1h
            //(LOS=12, column Q1h has a count of 6)
        }

        //private Frequencies FreqForCount(double los_hours, int count)
        //{
        //    foreach (var fmrow in _freq_map) {
        //        if (los_hours <= fmrow.los_high) {
        //            // Option: pro-rate the count upward if the LOS is less than the row we are interested in.
        //            //         This will bump the count to what it might have been at the full LOS.
        //            // Note: truncate the result; rounding inflates the value too much.
        //            int prorated_count = (int)((fmrow.los_high / los_hours) * count);

        //            // foreach goes low to high; go from high to low instead
        //            for (int j = (int)Frequencies.Q30M; (j > (int)Frequencies.QNONE); j--) { //search right to left
        //                if (prorated_count >= fmrow.freq[j]) {
        //                    return (Frequencies)j;
        //                }
        //            } // next j
        //        }
        //    }

        //    return Frequencies.QNONE;
        //}

        //
        // Load patient chart from the database (one DB access)
        // Load everything since admission
        //
        private int LoadPatientChart()
        {
            int ct_in_25hrs = 0;
            int ctperiop = 0;
            // Get the entire patient chart (all units and dates for this patient)
            var db = PFSDBUtility.NewPfsDataContext();
            var query = from item in db.CHART_ITEMs
                        where (item.ENCOUNTER_ID == _pat.encounter_id)
                        where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-25))
                        select item;
            // Save the result
            ct_in_25hrs = query.Count();
            _chart_items_since25hrs = query.ToArray();

            // Convert all results to lower case for case insensitive comparisons
            foreach (var item in _chart_items_since25hrs) {
                item.SOURCE_TEXT = null;
                if (item.CATEGORY != null) item.CATEGORY = item.CATEGORY.ToLower();
                if (item.CODE != null) item.CODE = item.CODE.ToLower();
                if (item.DESCRIPTION != null) item.DESCRIPTION = item.DESCRIPTION.ToLower();
                if (item.FIELD_NAME != null) item.FIELD_NAME = item.FIELD_NAME.ToLower();
                if (item.RESULT != null) item.RESULT = item.RESULT.ToLower();
                foreach (var perioploc in Program.patperioplist)
                {
                    if (item.EVENT_DATETIME >= perioploc.in_time && item.EVENT_DATETIME <= perioploc.out_time)
                    {
                        item.UNIT_ID = -6;
                        ctperiop++;
                    }
                }
            }

            //Program.VerboseAudit("Since 25 hrs count of periop/temp items=" + ctperiop);

            // Prepare more versions of the chart
            //var query2 = from item in _chart_items_since_admission
            //             where (item.EVENT_DATETIME >= _pat.unit_arrival) && (item.EVENT_DATETIME <= _pat.pull_finish)
            //             select item;
            //_chart_items_since_unit_arrival = query2.ToArray();
            //var query2 = from item in _chart_items_since_admission
            //             where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-24)) && (item.EVENT_DATETIME <= _pat.pull_finish)
            //             select item;
            //ct_in_24hrs = query2.Count();
            //Program.Audit("Count=" + ct_in_24hrs + " between " + _pat.pull_finish.AddHours(-24).ToString() + "-and-" + _pat.pull_finish.ToString());
            //_chart_items_since24hrs = query2.ToArray();
            //var query2 = from item in _chart_items_since_admission
            //         where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-25)) && (item.EVENT_DATETIME <= _pat.pull_finish)
            //         select item;
            //Program.VerboseAudit("Since 25 hrs count=" + query2.Count());
            //_chart_items_since25hrs = query2.ToArray();

            //var query2 = from item in _chart_items_since25hrs
            //         where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-13)) && (item.EVENT_DATETIME <= _pat.pull_finish)
            //         select item;
            //Program.VerboseAudit("Since 13 hrs count=" + query2.Count());
            //_chart_items_since13hrs = query2.ToArray();

            //query2 = from item in _chart_items_since25hrs
            //             //                     where (item.EVENT_DATETIME >= _pat.pull_start) && (item.EVENT_DATETIME <= _pat.pull_finish)
            //         where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-8)) && (item.EVENT_DATETIME <= _pat.pull_finish)
            //         select item;
            //_chart_items_during_pull_period = query2.ToArray();

            //query2 = from item in _chart_items_since25hrs
            //             //                     where (item.EVENT_DATETIME >= _pat.pull_start) && (item.EVENT_DATETIME <= _pat.pull_finish.AddHours(4))
            //         where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-8)) && (item.EVENT_DATETIME <= _pat.pull_finish.AddHours(4))
            //         select item;
            //Program.VerboseAudit("Pull PeriodPlus count=" + query2.Count());
            //_chart_items_pull_period_plus = query2.ToArray();



            //query2 = from item in _chart_items_since13hrs
            //         where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-9)) && (item.EVENT_DATETIME <= _pat.pull_finish)
            //         select item;
            //Program.VerboseAudit("Since 9 hrs count=" + query2.Count());
            //_chart_items_since9hrs = query2.ToArray();

            return ct_in_25hrs;
        }

        // Delay looking for patient procedures until they are wanted
        private void LoadPatientProceduresIfNeeded()
        {
            if (_procedure_events != null) return;

            var db = PFSDBUtility.NewPfsDataContext();
            var query = from proc in db.PROCEDURE_EVENTs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                        where (proc.PROCEDURE_DATETIME >= _pat.pull_finish.AddHours(-24))
                        select proc;
            _procedure_events = query.ToArray();
        }



        // Started a new chart item query (of a certain depth)
        // Default is the chart during the pull range.  Options for unit arrival and admission to hospital.
        private IEnumerable<CHART_ITEM> StartNewQuery()
        {
            return StartNewQuery(SearchDepth.SearchDefault);
        }
        private IEnumerable<CHART_ITEM> StartNewQuery(SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;

            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since25hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_start && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since25hrs where (item.EVENT_DATETIME >= _pat.pull_start && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                //if (exclude_periop_data)
                //    return (from item in _chart_items_since25hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-8) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                //else
                //    return (from item in _chart_items_since25hrs where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-8) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                //    return (from item in _chart_items_since_unit_arrival select item);
                //case SearchDepth.SearchSinceAdmission:
                //    return (from item in _chart_items_since_admission select item);
                //case SearchDepth.SearchPullPlus:
                //    return (from item in _chart_items_pull_period_plus select item);
                case SearchDepth.SearchSince25Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since25hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                    else
                        return (from item in _chart_items_since25hrs where (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince24Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since25hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-24) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since25hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-24) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince16Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since25hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-16) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since25hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-16) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince13Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since25hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-13) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since25hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-13) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince9Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since25hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-9) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since25hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-9) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince4Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since25hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME > _pat.pull_finish.AddHours(-4) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since25hrs where item.EVENT_DATETIME > _pat.pull_finish.AddHours(-4) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
            }
            return null;
        }

        // Is this search term a word or a list of words?
        private bool ValueIsAList(string s)
        {
            return (s != null) && s.Contains(',');                //is this a comma-separated list?
        }

        // Break apart a wordlist and prepare its members
        private string[] SplitOnCommaAndPrepareElements(string s)
        {
            // If a word list has a space after each comma, we will get leading blanks in each term
            var arr = s.Split(',');
            for (int i = 0; i <= arr.GetUpperBound(0); i++) {
                arr[i] = arr[i].Trim();                         // get rid of leading blanks
                arr[i] = arr[i].Replace(CHAR_COMMA, ",");       // insert a literal comma into search word
                arr[i] = arr[i].ToLower();                      // convert for case-insensitive comparisons
            }
            return arr;
        }

        // Add a basic chart item filter to a LINQ query
        //
        // cat = exact match
        // code_list = exact match; optional "like" match
        // desc_list = "like" match; optional exact match
        // field = exact match
        // result_list = "like" match; optional exact match
        //
        // These are comma-separated lists.  
        // Search for a literal comma by using CHAR_COMMA in the string.
        private IEnumerable<CHART_ITEM> AndItemFilter(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list)
        {
            // Converted all values to lower case for case-insensitive comparisons.
            if (!String.IsNullOrEmpty(cat))
            {
                cat = cat.ToLower();
                query = query.Where(e => e.CATEGORY == cat);
            }

            if (!String.IsNullOrEmpty(code_list))
            {
                code_list = code_list.ToLower();
                if (code_list.Left(2) == CODE_LIKE_PREFIX)
                {            // override for "like" match?
                    query = query.Where(e => e.CODE.Contains(code_list.Substring(2)));
                }
                else if (code_list.Left(2) == EXACT_MATCH_PREFIX)
                {
                    query = query.Where(e => e.CODE == code_list.Substring(2));
                }
                else if (ValueIsAList(code_list))
                {
                    query = AndCodeInList(query, code_list);            // find one of the words
                }
                else
                {
                    query = query.Where(e => e.CODE.Contains(code_list));      // find this word
                }
            }

            if (!String.IsNullOrEmpty(desc_list))
            {
                desc_list = desc_list.ToLower();
                if (desc_list.Left(2) == EXACT_MATCH_PREFIX)
                {          // exact match override?
                    query = query.Where(e => e.DESCRIPTION == desc_list.Substring(2));
                }
                else if (desc_list.Left(2) == NOT_PREFIX)
                {
                    // query = query.Where(e => !e.DESCRIPTION.Contains(desc_list.Substring(2)));
                    query = AndDescriptionNOTInList(query, desc_list.Substring(2));
                }
                else if (ValueIsAList(desc_list))
                {
                    query = AndDescriptionInList(query, desc_list);
                }
                else
                {
                    desc_list = desc_list.Replace(CHAR_COMMA, ",");     // look for literal comma requests
                    query = query.Where(e => e.DESCRIPTION.Contains(desc_list));
                }
            }

            if (!String.IsNullOrEmpty(field))
            {
                field = field.ToLower();
                query = query.Where(e => e.FIELD_NAME == field);
            }

            if (!String.IsNullOrEmpty(result_list))
            {
                query = query.Where(e => e.RESULT != null);
                result_list = result_list.ToLower();                        // make all results case insensitve
                if (result_list.Left(2) == EXACT_MATCH_PREFIX)
                {     // exact match override?
                    query = query.Where(e => e.RESULT == result_list.Substring(2));
                }
                else if (result_list.Left(2) == AVOID_NEGATIVE)
                {
                    query = query.Where(e => (!e.RESULT.ToLower().Contains("no " + result_list.Substring(2))) && (e.RESULT.ToLower().Contains(result_list.Substring(2)))); // == result_list.Substring(2)) || (e.RESULT.Contains(";" + result_list.Substring(2)))));
                }
                else if (ValueIsAList(result_list))
                {
                    query = AndResultInList(query, result_list);
                }
                else if (result_list.Left(2) == STARTS_WITH)
                {
                    query = query.Where(e => e.RESULT.StartsWith(result_list.Substring(2)));
                }
                else
                {
                    result_list = result_list.Replace(CHAR_COMMA, ",");     // look for literal comma requests
                    query = query.Where(e => e.RESULT.Contains(result_list));
                }
            }
            return query;
        }

        // Look for a code that is one of the words in the list.
        private IEnumerable<CHART_ITEM> AndCodeInList(IEnumerable<CHART_ITEM> query, string code_list)
        {
            if (String.IsNullOrEmpty(code_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(code_list);
            //            return query.Where(e => arr.Any(x => x.Contains(e.CODE))); // "like" match
            return query.Where(e => e.CODE.ContainsAny(arr)); // "like" match
        }

        // Look for a description that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndDescriptionInList(IEnumerable<CHART_ITEM> query, string desc_list)
        {
            if (String.IsNullOrEmpty(desc_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(desc_list);
            //return query.Where(e => arr.Any(x => x.Contains(e.DESCRIPTION))); // "like" match
            return query.Where(e => e.DESCRIPTION.ContainsAny(arr)); // "like" match
        }
        private IEnumerable<CHART_ITEM> AndDescriptionNOTInList(IEnumerable<CHART_ITEM> query, string desc_list)
        {
            if (String.IsNullOrEmpty(desc_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(desc_list);
            //return query.Where(e => arr.Any(x => x.Contains(e.DESCRIPTION))); // "like" match
            return query.Where(e => !e.DESCRIPTION.ContainsAny(arr)); // "like" match
        }

        // Look for a result that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultInList(IEnumerable<CHART_ITEM> query, string result_list)
        {
            bool use_start_with = false;

            if (String.IsNullOrEmpty(result_list)) return query;

            if (result_list.Left(2) == STARTS_WITH)
            {
                use_start_with = true;
                result_list = result_list.Substring(2);
            }

            var arr = SplitOnCommaAndPrepareElements(result_list);
            //            return query.Where(e => arr.Any(x => x.Contains(e.RESULT))); // "like" match
            if (use_start_with)
                return query.Where(e => arr.Any(item => e.RESULT.StartsWith(item)));
            else
                return query.Where(e => e.RESULT.ContainsAny(arr)); // "like" match
                                                                    //query = query.Where(e => (meds_mr2026.Any(item => e.DESCRIPTION.ToUpper().StartsWith(item))
                                                                    //return query.Where(e => arr.Any(item => e.RESULT.ToLower().Contains(item.ToLower())));
        }

        // Look for a result that does not contain one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultNotInList(IEnumerable<CHART_ITEM> query, string result_list)
        {
            if (String.IsNullOrEmpty(result_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(result_list);
            //            return query.Where(e => !arr.Any(x => x.Contains(e.RESULT))); // "like" match
            return query.Where(e => !e.RESULT.ContainsAny(arr)); // "like" match
        }

        private string DescribeSearchDepth(SearchDepth search_depth)
        {
            string result = "";
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;

            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    //result = "in pull range"
                    result = "";                         //be quiet since this is default
                    break;
                case SearchDepth.SearchSinceArrival:
                    result = "since arrival to unit";
                    break;
                case SearchDepth.SearchSinceAdmission:
                    result = "since admission";
                    break;
                case SearchDepth.SearchPullPlus:
                    result = "in LOS range plus 4 hours after";
                    break;
                case SearchDepth.SearchSince24Hrs:
                    result = "since 24 hours ago";
                    break;
                case SearchDepth.SearchSince25Hrs:
                    result = "since 25 hours ago";
                    break;
                case SearchDepth.SearchSince16Hrs:
                    result = "since 16 hours ago";
                    break;
                case SearchDepth.SearchSince13Hrs:
                    result = "since 13 hours ago";
                    break;
                case SearchDepth.SearchSince9Hrs:
                    result = "since 9 hours ago";
                    break;
                case SearchDepth.SearchSince4Hrs:
                    result = "since 4 hours ago";
                    break;
            }

            return result;
        }

        // Describe a query that has already been run
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Make a simple query and describe the results
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            var arr = query.ToArray();
            return Describe(arr, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private string LookingFor(string so_far, string what, string op, string value)
        {
            string result;
            if (String.IsNullOrEmpty(value)) return so_far;

            if (so_far == "")
                result = "look for " + what;
            else
                result = so_far + " and " + what;

            if (value.Left(2) == EXACT_MATCH_PREFIX)
                result += "='" + value.Substring(2) + "'";
            else if (value.Left(2) == CODE_LIKE_PREFIX)
                result += " contains " + "'" + value.Substring(2) + "'";
            else if (ValueIsAList(value))
                result += " in '" + value + "'";
            else
                result += op + "'" + value + "'";

            return result;
        }
        private string Describe(CHART_ITEM[] arr, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string result = "";
            //result = LookingFor(result, "cat", "=", cat);
            result = LookingFor(result, "code", "=", code_list);
            result = LookingFor(result, "desc", " contains ", desc_list.Left(20));
            //result = LookingFor(result, "field", "=", field);
            result = LookingFor(result, "result", " contains ", result_list.Left(20));

            if (arr.Count() == 0) {
                result += "; not found " + DescribeSearchDepth(search_depth);
            } else {
                var e = arr[0];
                result = "FOUND: " + result + "; found";
                // We might have searched for a pattern or word list in several fields - show what was found
                //if (e.CATEGORY != null && e.CATEGORY != "") result += " cat='" + e.CATEGORY + "'";
                if (e.CODE != null && e.CODE != "") result += " code='" + e.CODE + "'";
                if (e.DESCRIPTION != null && e.DESCRIPTION != "") result += " desc='" + e.DESCRIPTION + "'";
                //if (e.FIELD_NAME != null && e.FIELD_NAME != "") result += " field='" + e.FIELD_NAME + "'";
                if (e.RESULT != null && e.RESULT != "") result += " result='" + e.RESULT + "'";
                // Are there more results?  Just say how many; we aren't going to list them.
                if (arr.Count() == 2) {
                    result += " (1 more result)";
                } else if (arr.Count() > 2) {
                    result += " (" + (arr.Count() - 1) + " more results)";
                }
            }

            return result;
        }


        //Set an indicator for this reason (low level)
        private void SetInd(int inum, string reason)
        {
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Set Ind #" + inum + ": " + reason);          //already set - repeat for verbose only
            } else {
                _inds[inum].is_checked = true;
                Program.Audit("Set Ind #" + inum + ": " + reason);
            }
        }

        //Clear an indicator for this reason (low level)
        private void ClrInd(int inum, string reason)
        {
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Clr Ind #" + inum + ": " + reason);          //already clear - repeat verbose only
            } else {
                _inds[inum].is_checked = false;
                Program.Audit("Clr Ind #" + inum + ": " + reason);
            }
        }

        // Count how many items meet the conditions
        // All items are listed
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountItems(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            int count = query.Count();

            // always return what was found
            found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
            // echo the result?
            if (trace) Program.VerboseAudit(found_what);

            return count;
        }


        // Count how many items meet the conditions
        // This has the option to list every entry or give a total
        private int CountResultInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            int count = 0;
            found_what = "";
            string s = "";

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            var arr = SplitOnCommaAndPrepareElements(result_list);
            //Walker Schlundt
            //Set Ind #9: found '' in cat='' code='301870' field='' result='oriented x4' 

            foreach (var item in query) {
                // Figure out which of the search words were found
                for (int i = 0; i <= arr.GetUpperBound(0); i++)
                {
                    if (item.RESULT.Contains(arr[i]))
                    {
                        count++;
                        s = arr[i];
                        found_what = "found '" + s + "' code='" + item.CODE + "' result='" + item.RESULT + "'";
                        //if (trace) Program.VerboseAudit(found_what);
                        Program.VerboseAudit(found_what);
                    }
                }
                if (count_mode != CountMode.CountAll) break;        //stop counting

            }

            if (count > 0) {
                //We already printed what was found; maybe add how many?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        // Count how many items meet the conditions
        // This has the option to list every entry or give a total
        private int CountResultInListEXCEPTList(string cat, string code_list, string desc_list, string field, string result_list, string negresult_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            int count = 0;
            found_what = "";

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            query = AndResultNotInList(query, negresult_list);

            var arr = SplitOnCommaAndPrepareElements(result_list);

            foreach (var item in query)
            {
                // Figure out which of the search words were found
                var s = item.RESULT.ContainsWhich(arr);
                found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' code='" + item.CODE + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "'";
                if (trace) Program.VerboseAudit(found_what);
                count++;
                if (count_mode != CountMode.CountAll) break;        //stop counting
            }

            if (count > 0)
            {
                //We already printed what was found; maybe add how many?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            }
            else
            {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        // Count how many items contain ALL results in list
        private int CountResultInListAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            int count = 0;
            found_what = "";

            var arr = SplitOnCommaAndPrepareElements(result_list);

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            Program.VerboseAudit("arr ub=" + arr.GetUpperBound(0));
            for (int i = 0; i <= arr.GetUpperBound(0); i++)
            {
                if (arr[i].StartsWith(NOT_PREFIX))
                {
                    //query = query.Where(e => !e.RESULT.Contains(arr[i].Substring(2)));
                    query = AndResultNotInList(query, arr[i]);
                }
                else
                {
                    Program.VerboseAudit(i + ":" + arr[i]);
                    //query = query.Where(e => e.RESULT.Contains(arr[i]));
                    query = AndResultInList(query, arr[i]);
                }
            }
            //            Program.VerboseAudit("out of for loop");

            count = query.Count();
            //          Program.VerboseAudit("query count = " +count);

            if (count > 0)
            {
                found_what = "found item with all results in '" + result_list + "' without those prefixed by !!";
                if (trace) Program.VerboseAudit(found_what);
            }
            else
            {
                // Describe what was *not* found
                //found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                //if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        private int CountResultInList1andNotInList2(string code_list, string result_list1, string result_list2, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            int count = 0;
            found_what = "";
            string s = "";

            Program.VerboseAudit("In crl1l2: " + code_list + " r1=" + result_list1 + " r2=" + result_list2);

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, "", code_list, "", "", result_list1);
            var arr = SplitOnCommaAndPrepareElements(result_list1);
            var arr2 = SplitOnCommaAndPrepareElements(result_list2);
            bool all_false;
            Program.VerboseAudit("query count: " + query.Count());

            foreach (var item in query)
            {
//                Program.VerboseAudit("query item: " + item.RESULT + " evdt="+item.EVENT_DATETIME);
                // Figure out which of the search words were found
                for (int i = 0; i <= arr.GetUpperBound(0); i++)
                {
                    if (item.RESULT.Contains(arr[i])) //good! this list1 item was found.
                    {
                        all_false = true; //assume the result doesn't contain any arr2 items
                        for (int j = 0; j <= arr2.GetUpperBound(0); j++)
                        {
                            if (item.RESULT.Contains(arr2[j]))//this arr2 item was found in the result
                                all_false = false;
                        }
                        if (all_false)
                        {
                            count++;
                            s = arr[i];
                            found_what = "found '" + s + "' code='" + item.CODE + "' result='" + item.RESULT + "'";
                            Program.VerboseAudit(found_what);
                        }
                    }
                }
                if (count_mode != CountMode.CountAll) break;        //stop counting

            }

            if (count > 0)
            {
                //We already printed what was found; maybe add how many?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            }
            else
            {
                // Describe what was *not* found
                found_what = Describe("", code_list, "", "", result_list1, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }


        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            if (ValueIsAList(result_list))
            {
                return CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what);
            } else {
                return CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what);
            }
        }

        // Is there an item with this result?
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContains(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            if (ValueIsAList(result_list))
            {
                return (CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            } else {
                return (CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
            }
        }

        // Result contains all in list
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            if (ValueIsAList(result_list))
            {
                return (CountResultInListAll(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            }
            else
            {
                found_what = "Not valid for non-list results.";
                return false;
            }
        }
        // Count how many items meet the conditions, with none of the results in result_list.
        // This has the option to say how many were found rather than list all.
        private int CountResultNotInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            int count = 0, rec_count = 0;
            bool found_one;

            found_what = "";
            var arr = SplitOnCommaAndPrepareElements(result_list);         // also deals with CHAR_COMMA

            //Do not filter by result here; we need all results
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            foreach (var item in query) {
                rec_count++;
                //Look for each search word in the result
                found_one = false;

                foreach (string s in arr) {
                    //                    if (String.Equals(item.RESULT, s)) {
                    if (item.RESULT.Contains(s))
                    {
                        found_what = "found '" + s + "' result='" + item.RESULT + "' -- exclude this";
                        Program.VerboseAudit(found_what);
                        found_one = true;
                        break;
                    }
                }

                if (!found_one) {
                    //none of the words were found - good!
                    found_what = Describe(cat, code_list, desc_list, field, "", search_depth) + " -- does not contain '" + result_list + "'";
                    Program.VerboseAudit(found_what);
                    count++;
                    if (count_mode != CountMode.CountAll) break;
                }
            }

            if (count > 0) {
                //We already printed what was found; maybe show the total?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else if (rec_count > 0) {
                //We already printed what we ignored
            } else {
                //Print what did not exist
                found_what = Describe(cat, code_list, desc_list, field, "", search_depth);      //not found
                Program.VerboseAudit(found_what);
            }

            return count;
        }

        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, string found_what)
        {
            // Search in the pull range with trace on
            return CountResultDoesNotContain(cat, code_list, desc_list, field, result_list, out found_what, SearchDepth.SearchDefault, true);
        }
        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, out string found_what, SearchDepth search_depth, bool trace)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            return CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0;
        }

        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            return (CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0);
        }

        // Set the indicator if the conditions are met
        //
        private bool SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SearchDepth s = SearchDepth.SearchDefault;
            if (inum == 5) s = SearchDepth.SearchSince16Hrs;
            if (inum == 6) s = SearchDepth.SearchSince13Hrs;
            if (inum == 7) s = SearchDepth.SearchSince13Hrs;
            if (inum == 8) s = SearchDepth.SearchSince16Hrs;
            if (inum == 9) s = SearchDepth.SearchSince16Hrs;
            if (inum == 10) s = SearchDepth.SearchSince13Hrs;
            if (inum == 11) s = SearchDepth.SearchSince9Hrs;
            if (inum == 12) s = SearchDepth.SearchSince16Hrs;
            if (inum == 13) s = SearchDepth.SearchSince16Hrs;
            if (inum == 14) s = SearchDepth.SearchSince25Hrs;
            if (inum == 19) s = SearchDepth.SearchSince9Hrs;
            if (inum == 20) s = SearchDepth.SearchSince9Hrs;
            if (inum == 21) s = SearchDepth.SearchSince16Hrs;
            if (inum == 22) s = SearchDepth.SearchSince13Hrs;
            if (inum == 23) s = SearchDepth.SearchSince9Hrs;
            if (inum == 24) s = SearchDepth.SearchSince9Hrs;
            return SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, s);

        }
        private bool SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            bool b = false;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return true;

            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what)) {
                SetInd(inum, found_what);                          //echo here - found
                b = true;
            } else {
                Program.VerboseAudit(found_what);                  //and here - not found
            }
            return b;
        }

        private bool SetIndIfResultBetween(int inum, string cat, string code_list, string desc_list, string field, int loval, int hival)
        {
            return SetIndIfResultBetween(inum, cat, code_list, desc_list, field, loval, hival, SearchDepth.SearchDefault);
        }

        private bool SetIndIfResultBetween(int inum, string cat, string code_list, string desc_list, string field, int loval, int hival, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            int count = 0;
            string found_what = "";
            bool ret = false;

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");
            foreach (var item in query)
            {
                Program.VerboseAudit("ResBetween: code=" + item.CODE + " result=" + item.RESULT);
                if (item.RESULT.IsNumeric())
                {
                    //Program.VerboseAudit("  result is numeric");
                    if (item.RESULT.Val() >= loval && item.RESULT.Val() <= hival)
                    {
                        count++;
                        found_what = "found code=" + item.CODE + " with result=" + item.RESULT;
                        Program.VerboseAudit(found_what);
                    }
                }
            }

            if (count > 0)
            {
                ret = true;
                SetInd(inum, found_what);
            }
            return ret;
        }


        // Special contains all
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SearchDepth s = SearchDepth.SearchDefault;
            if (inum == 8) s = SearchDepth.SearchSince24Hrs;
            SetIndIfResultContainsAll(inum, cat, code_list, desc_list, field, result_list, s);
        }
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return;

            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                          //echo here - found
            }
            else
            {
                Program.VerboseAudit(found_what);                  //and here - not found
            }
        }
        // Set the indicator if the result does not contain any of the words in result_list
        //
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultDoesNotContain(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return;

            //Turn trace off for ResultDoesNotContain() and echo what was set below with SetInd
            if (ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                           //echo here - not found
            } else {
                Program.VerboseAudit(found_what);                   //and here - found
            }
        }

        // Clear the indicator if the result contains one of the words in the result_list
        //
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            //avoid more queries if the indicator is already clear
            if (!_inds[inum].is_checked) return;

            //Turn trace off for ResultContains() and echo what was set below with SetInd
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what)) {
                ClrInd(inum, found_what);                           //echo here - found
            } else {
                Program.VerboseAudit(found_what);                   //and here - not found
            }
        }


        //These slightly smaller functions are meant for places where you aren't really looking for a chart result
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            return (CountResultContains(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
        }

        private bool ExistsResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            return (CountResultDoesNotContain(cat, code_list, desc_list, field, result_list, out found_what, search_depth, trace));
        }

        // Variations with optional parameters (can switch to optional parameters in VS 2010)
        //
        private void SetIndIfFound(int inum, string cat)
        {
            SetIndIfResultContains(inum, cat, "", "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list)
        {
            SetIndIfResultContains(inum, cat, code_list, "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private void ClrIndIfFound(int inum, string cat)
        {
            ClrIndIfResultContains(inum, cat, "", "", "", "", SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }


        // Get the max/total value from a result (usually in the middle of the text)
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetIntValue(get_mode, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            int value;
            int result = 0;
            bool found_one = false;

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);

            //Look for a number in the result

            foreach (var item in query) {
                // RESULT might be null so convert to empty string so Split won't blow up
                var arr = PFSDBUtility.DBToString(item.RESULT).Split(' ');
                foreach (string s in arr) {
                    //Look for 1st character numeric; IsNumeric("60min") will fail so look at 1st char only
                    if (s.Left(1).IsNumeric()) {
                        value = (int)s.Val();                           //Use Val; ToInteger will error on "60min"
                        switch (get_mode) {
                            case GetValueMode.GetMax:
                                result = Math.Max(value, result);       //max
                                break;
                            case GetValueMode.GetTotal:
                                result += value;                        //total
                                break;
                            case GetValueMode.GetLast:
                                result = value;                         //last
                                break;
                        }

                        // print what we are searching for (the first time)
                        if (!found_one) {
                            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
                        }
                        found_one = true;
                        // print each value found
                        //Program.VerboseAudit("  found numeric value " + result);
                        //Keep going in case there are more
                    }
                }
            }

            if (!found_one) {
                //show what was not found
                Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
            }

            return result;
        }

        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetMaxValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            return GetIntValue(GetValueMode.GetMax, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetTotalValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            return GetIntValue(GetValueMode.GetTotal, cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Get a result; returns true if found with return_result
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result)
        {
            return GetResult(cat, code_list, desc_list, field, out return_result, SearchDepth.SearchDefault);
        }
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0) {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
            } else {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }

        private bool GetLatestResult(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");
            query = query.OrderByDescending(e => e.EVENT_DATETIME);

            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
            }
            else
            {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }

        private bool GetResultForInspection(string cat, string code_list, string desc_list, string field, string result, out string return_result, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result);

            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
            }
            else
            {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }

        private void GetResultforOther(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "Other");
            return_result = "";
            foreach (var item in query)
            {
                if (!String.Equals(item.RESULT, "Other"))
                {
                    return_result = item.RESULT;
                    //Program.VerboseAudit(found_what);
                    break;
                }

            }
            return;
        }

        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, string res, out string return_result, out DateTime return_evdt)
        {
            return GetResultAndEVDT(cat, code_list, desc_list, field, res, out return_result, out return_evdt, SearchDepth.SearchDefault);
        }
        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, string res, out string return_result, out DateTime return_evdt, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, res);

            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
                return_evdt = PFSDBUtility.DBToDateTime(query.First().EVENT_DATETIME);
            }
            else
            {
                return_result = "";
                return_evdt = DateTime.MinValue;
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }
        private bool GetEVDT(string cat, string code_list, string desc_list, string res, string field, int comparison, DateTime compevdt, out DateTime return_evdt)
        {
            return GetEVDT(cat, code_list, desc_list, field, res, comparison, compevdt, out return_evdt, SearchDepth.SearchDefault);
        }
        private bool GetEVDT(string cat, string code_list, string desc_list, string field, string res, int comparison, DateTime compevdt, out DateTime return_evdt, SearchDepth search_depth)
        {
            bool sort_ascending = false;
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, res);
            if (compevdt != DateTime.MinValue)
            {
                if (comparison == 0) // equal
                    query = query.Where(e => e.EVENT_DATETIME == compevdt);
                else if (comparison == 1) // GTE
                    query = query.Where(e => e.EVENT_DATETIME >= compevdt);
                else if (comparison == 2) // GT
                    query = query.Where(e => e.EVENT_DATETIME > compevdt);
                else if (comparison == 22) // Least GT
                {
                    query = query.Where(e => e.EVENT_DATETIME > compevdt);
                    sort_ascending = true;
                }
                else if (comparison == 3) // LT
                    query = query.Where(e => e.EVENT_DATETIME < compevdt);
            }
            if (sort_ascending)
                query = query.OrderBy(e => e.EVENT_DATETIME);
            else
                query = query.OrderByDescending(e => e.EVENT_DATETIME);
            //Program.VerboseAudit("getevdt count=" + query.Count());
            if (query.Count() > 0)
            {
                return_evdt = PFSDBUtility.DBToDateTime(query.First().EVENT_DATETIME);
            }
            else
            {
                return_evdt = DateTime.MinValue;
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, res, search_depth));
            return (return_evdt != DateTime.MinValue);
        }


        //======================================================================================
        // Mapping starts here
        //======================================================================================

        private void Check_1_2_3_4()
        {
            Program.VerboseAudit("Default Search Scope = " + _pat.pull_finish.AddHours(-8).ToString() + " to " + _pat.pull_finish.ToString());

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("1. ADL Self");
            Program.VerboseAudit("2. ADL Assist");
            Program.VerboseAudit("3. ADL Extended");
            Program.VerboseAudit("4. ADL Complete");
            Program.VerboseAudit("---------------");

            // 4. COMPLETE CARE

            //            "ADL #1-4- Look back:
            //            - 13 Hr Mobility
            //-25 hr Hygiene
            //-13 Hr Toileting
            //-25 Hr Feeding
            //-13 Hrs - All Other rows"
            exclude_periop_data = false;

            if (_pat.age < 4.0)
                SetInd(4, "Age=" + _pat.age);
            else if (_pat.age < 7.0)
                SetInd(2, "Age=" + _pat.age);
            //if (_inds[4].is_checked) return;

            string reslist = "";

            reslist = "Ambulate in hall,Ambulate in room,Bathroom privileges,Bedrest,Bike,Chair,Commode,Dangle,Supervised exercise,Held,Stand at bedside,Tilt table,Turn,Up in chair,Wagon,In bed,bedpan";
            bool B1 = Exists("", "9990000305560", "", "", reslist, SearchDepth.SearchSince13Hrs);
            reslist = "1 Assist,2 Assist,3 Assist,>4 Assist,4 or more Assist,Supervision required";
            bool B2 = Exists("", "9993040109530", "", "", reslist, SearchDepth.SearchSince13Hrs);
            reslist = "Lying right side,Lying left side,Supine,Semi Fowler,Pillow support,Prone,Sitting,Standing,Up in chair,Micro turn left,Micro turn right,Do Not Turn,Unstable to turn,Turn right side,Turn left side";
            bool B3 = Exists("", "9990000400604", "", "", reslist, SearchDepth.SearchSince13Hrs);
            if (B1 && B2) SetInd(2, "B1 and B2");
            if (B2 && B3) SetInd(2, "B2 and B3");

            reslist = "Assist,Total Care";
            bool B4 = Exists("", "9991025006475", "", "", reslist, SearchDepth.SearchSince13Hrs);
            reslist = "Left side,Left tilt,Right side,Right tilt,Semi-fowler,Fowler,Knee chest,Chair,Rocking chair,Supine,Trendelenberg,Squatting,Standing,Kneeling,Walking,Prone";
            bool B5 = Exists("", "1028000027", "", "", reslist, SearchDepth.SearchSince13Hrs);
            if (B4 && B5) SetInd(2, "B4 and B5");

            reslist = "1 Assist,2 Assist,3 Assist,>4 Assist,Supervised";
            bool B6 = Exists("", "9990007060350", "", "", reslist, SearchDepth.SearchSince25Hrs);
            reslist = "Bathed,Chlorhexidine,Showered,Bath in a bag,Catheter care,Foley care,Peri care,Hair washed,Hair dried/curled,Shaved";
            bool B7 = Exists("", "9990000342030", "", "", reslist, SearchDepth.SearchSince25Hrs);
            reslist = "Complete bath,Partial bath,Wipes,Solution,Showered";
            bool B81 = Exists("", "9993040109700", "", "", reslist, SearchDepth.SearchSince25Hrs);
            if (B6 && (B7 || B81)) SetInd(2, "B6 and (B7 or B81)");
            
            reslist = "Partial assist,Complete assist";
            bool B8 = Exists("", "9990000305650", "", "", reslist, SearchDepth.SearchSince25Hrs);
            if (B7 && B8) SetInd(2, "B7 and B8");

            reslist = "Needs assist,Supervised,Total assist";
            SetIndIfResultContains(2, "", "9990000700380", "", "", reslist, SearchDepth.SearchSince25Hrs);
            reslist = "";
            bool c10 = SetIndIfResultContains(2, "", "3043040100003", "", "", reslist, SearchDepth.SearchSince25Hrs);
            bool c11 = SetIndIfResultContains(2, "", "9993040100004", "", "", reslist, SearchDepth.SearchSince25Hrs);
            bool c12 = SetIndIfResultContains(2, "", "3043040101422", "", "", reslist, SearchDepth.SearchSince25Hrs);
            bool c13 = SetIndIfResultContains(2, "", "9993040101423", "", "", reslist, SearchDepth.SearchSince25Hrs);

            bool b60 = SetIndIfResultContains(2, "", "9990304000118", "", "", "3,feeding", SearchDepth.SearchSince25Hrs);
            bool b61 = SetIndIfResultContains(2, "", "9990000002113", "", "", "3,feeding", SearchDepth.SearchSince25Hrs);

            reslist = "";
            bool ADLLDA10 = SetIndIfResultContains(2, "", "3045001094", "", "", reslist, SearchDepth.SearchSince25Hrs);
            reslist = "NG tube,OG tube,G-tube,J-tube,ND tube,OD tube,PEG-tube";
            bool ADLLDA11 = SetIndIfResultContains(2, "", "9991733565652", "", "", reslist, SearchDepth.SearchSince25Hrs);
            bool ADLLDA12 = SetIndIfResultContains(2, "", "9991733569855", "", "", "", SearchDepth.SearchSince25Hrs);
            bool ADLLDA13 = SetIndIfResultContains(2, "", "9993040304520", "", "", "", SearchDepth.SearchSince25Hrs);

            reslist = "Yes";
            bool c15 = SetIndIfResultContains(2, "", EXACT_MATCH_PREFIX + "23", "", "", reslist, SearchDepth.SearchSince13Hrs);
            reslist = "";
            bool c16 = SetIndIfResultContains(2, "", "3045001090", "", "", reslist, SearchDepth.SearchSince13Hrs);
            bool c17 = SetIndIfResultContains(2, "", "3045001089", "", "", reslist, SearchDepth.SearchSince13Hrs);
            reslist = "Yes";
            bool c18 = SetIndIfResultContains(2, "", EXACT_MATCH_PREFIX + "16", "", "", reslist, SearchDepth.SearchSince13Hrs);

            reslist = "";
            bool ADLLDA1 = SetIndIfResultContains(2, "", "3045001088", "", "", reslist, SearchDepth.SearchSince13Hrs);
            bool ADLLDA2 = SetIndIfResultContains(2, "", "9990007085310", "", "", reslist, SearchDepth.SearchSince13Hrs);
            bool ADLLDA3 = SetIndIfResultContains(2, "", "9993040104906", "", "", reslist, SearchDepth.SearchSince13Hrs);
            bool ADLLDA4 = SetIndIfResultContains(2, "", "9990000396150", "", "", reslist, SearchDepth.SearchSince13Hrs);
            bool ADLLDA5 = SetIndIfResultContains(2, "", "9990007085490", "", "", reslist, SearchDepth.SearchSince13Hrs);
            bool ADLLDA6 = SetIndIfResultContains(2, "", "9993040021276", "", "", reslist, SearchDepth.SearchSince13Hrs);

            reslist = "Amber,Clear,Cloudy,Dark,Fibrin,Light,Pink,Red,Yellow";
            bool c20 = SetIndIfResultContains(2, "", "9990000370180", "", "", reslist, SearchDepth.SearchSince13Hrs);
            reslist = "";
            bool c21 = SetIndIfResultContains(2, "", "3040011360", "", "", reslist, SearchDepth.SearchSince13Hrs);

            reslist = "";
            bool c22 = SetIndIfResultContains(2, "", "3040002155", "", "", reslist, SearchDepth.SearchSince13Hrs); //c22
            bool c23 = SetIndIfResultContains(2, "", "9993040011213", "", "", reslist, SearchDepth.SearchSince13Hrs); //c23
            bool c24 = SetIndIfResultContains(2, "", "9993041001004", "", "", reslist, SearchDepth.SearchSince13Hrs); //c24
            bool c25 = SetIndIfResultContains(2, "", "9993041001003", "", "", reslist, SearchDepth.SearchSince13Hrs); //c25
            bool c26 = SetIndIfResultContains(2, "", "3045001087", "", "", reslist, SearchDepth.SearchSince13Hrs); //c26


            reslist = "Done";
            bool B26 = Exists("", "9991600100064", "", "", reslist, SearchDepth.SearchSince13Hrs);
            if (B6 && B26) SetInd(2, "B6 and B26");

            reslist = "Stand-by assist ,One staff assist ,Two staff assist ";
            bool C27 = SetIndIfResultContains(2, "", "9991600100065", "", "", reslist, SearchDepth.SearchSince13Hrs);//c27
            reslist = "Stand-by assist,One staff assist,Two staff assist";
            bool C28 = SetIndIfResultContains(2, "", "9991600100066", "", "", reslist, SearchDepth.SearchSince13Hrs);//c28
            reslist = "Bedpan,Catheter,Commode,Incontinence pad";
            bool C29 = SetIndIfResultContains(2, "", "9991600100067", "", "", reslist, SearchDepth.SearchSince13Hrs);//c29
            reslist = "Bed bath";
            bool C30 = SetIndIfResultContains(2, "", "9991600100068", "", "", reslist, SearchDepth.SearchSince13Hrs);//c30

            reslist = "3";
            SetIndIfResultContains(2, "", "9993040000407", "", "", reslist, SearchDepth.SearchSince13Hrs);
            SetIndIfResultBetween(2, "", "3045001023", "", "", 9, 12, SearchDepth.SearchSince13Hrs);
            SetIndIfResultBetween(2, "", "9993040001207", "", "", 9, 12, SearchDepth.SearchSince13Hrs);


            bool mob1 = SetIndIfResultContains(2, "", "3045001036", "", "", "", SearchDepth.SearchSince13Hrs);
            bool mob2 = SetIndIfResultContains(2, "", "3045001034", "", "", "", SearchDepth.SearchSince13Hrs);
            bool mob3 = SetIndIfResultContains(2, "", "3045001038", "", "", "1:1,1:2,1:3", SearchDepth.SearchSince13Hrs);

            reslist = "Digital stimulation,Enema,Manual evacuation/disimpaction";
            bool b77 = SetIndIfResultContains(2, "", "9993040109907", "", "", reslist, SearchDepth.SearchSince13Hrs);
            bool b78 = SetIndIfResultContains(2, "", "9990000016084", "", "", "", SearchDepth.SearchSince13Hrs);
            string found_what;
            bool b79 = Check_b79(out found_what);
            if (b79) SetInd(2, "Toileting: " + found_what);
            bool b80 = SetIndIfResultContains(2, "", "9990000305560", "", "", "bedpan", SearchDepth.SearchSince13Hrs);

            bool c77 = b77;
            bool c78 = b78;
            bool c79 = b79;
            bool c80 = b80;

            bool d77 = b77;
            bool d78 = b78;
            bool d79 = b79;
            bool d80 = b80;

            //            "ADL #1-4- Look back:
            //-25 hr Hygiene
            //-25 Hr Feeding
            //-13 Hr Mobility
            //-13 Hr Toileting
            //-13 Hrs - All Other rows"

            // FOR ADL Extended rows C-1 through C-21, 
            //must include charting for 3 of the 4 bullets below on the shift:
            //   - C1 & C2 or C2&C3 or C4&C5(mobility)
            //   - C6 & 7 or C7&8(hygiene, please carry over for night shift)
            //   -Any of C-9, C - 10, C - 11, C - 12, C - 13, ADL - LDA - 10 or ADL-LDA - 11(feeding, please carry over for night shift)
            //   -any row between C - 15 to C-21 or ADL-LDA - 1, 2, 3, 4 ro 5(toileting)

            reslist = "Ambulate in hall,Ambulate in room,Bathroom privileges,Bedrest,Bike,Chair,commode,Dangle,Supervised exercise,Held,Stand at bedside,Tilt table,Turn,Up in chair,Wagon,In bed,bedpan";
            bool c1 = Exists("", "9990000305560", "", "", reslist, SearchDepth.SearchSince13Hrs);
            //if (c1) Program.VerboseAudit("c1=true");
            reslist = "1 Assist,2 Assist,3 Assist,>4 Assist,4 or more Assist";
            bool c2 = Exists("", "9993040109530", "", "", reslist, SearchDepth.SearchSince13Hrs);
            //if (c2) Program.VerboseAudit("c2=true");
            reslist = "Lying right side,Lying left side,Supine,Semi Fowler,Pillow support,Prone,Sitting,Standing,Up in chair,Micro turn left,Micro turn right,Do Not Turn,Unstable to turn,Turn right side,Turn left side";
            bool c3 = Exists("", "9990000400604", "", "", reslist, SearchDepth.SearchSince13Hrs);
            //if (c3) Program.VerboseAudit("c3=true");
            reslist = "Assist,Total Care";
            bool c4 = Exists("", "9991025006475", "", "", reslist, SearchDepth.SearchSince13Hrs);
            reslist = "Left side,Left tilt,Right side,Right tilt,Semi-fowler,Fowler,Knee chest,Chair,Rocking chair,Supine,Trendelenberg,Squatting,Standing,Kneeling,Walking,Prone";
            bool c5 = Exists("", "1028000027", "", "", reslist, SearchDepth.SearchSince13Hrs);
            reslist = "1 Assist,2 Assist,3 Assist,>4 Assist";

            bool c6 = Exists("", "9990007060350", "", "", reslist, SearchDepth.SearchSince25Hrs);
            reslist = "Bathed,Chlorhexidine,Showered,Bath in a bag,Catheter care,Foley care,Peri care";
            bool c7 = Exists("", "9990000342030", "", "", reslist, SearchDepth.SearchSince25Hrs);
            bool c81 = B81;
            reslist = "Partial assist,Complete assist";
            bool c8 = Exists("", "9990000305650", "", "", reslist, SearchDepth.SearchSince25Hrs);

            reslist = "Needs assist,Total assist";
            bool c9 = Exists("", "9990000700380", "", "", reslist, SearchDepth.SearchSince25Hrs);

            bool c19 = (ADLLDA1 || ADLLDA2 || ADLLDA3 || ADLLDA4 || ADLLDA5 || ADLLDA6);

            reslist = "Done";
            bool C26 = Exists("", "9991600100064", "", "", reslist, SearchDepth.SearchSince13Hrs);//c26

            //   - C1 & C2 or C2&C3 or C4&C5(mobility)
            //   - C6 & 7 or C7&8(hygiene, please carry over for night shift)
            //   -Any of C-9, C - 10, C - 11, C - 12, C - 13, ADL - LDA - 10 or ADL-LDA - 11(feeding, please carry over for night shift)
            //   -any row between C - 15 to C-21 or ADL-LDA - 1, 2, 3, 4 ro 5(toileting)
            bool Cgroup1 = ((c1 && c2) || (c2 && c3) || (c4 && c5) || mob1 || mob2 || mob3);
            bool Cgroup2 = ((c6 && (c7 || c81)) || ((c7 || c81) && c8) || (c6 && C26));
            bool Cgroup3 = (c9 || c10 || c11 || c12 || c13 || ADLLDA10 || ADLLDA11 || ADLLDA12 || ADLLDA13 || b60 || b61);
            bool Cgroup4 = (c15 || c16 || c17 || c18 || c19 || c20 || c21 || c22 || c23 || c24 || c25 || c77 || c78 || c79 || c80 || c26);
            Program.Audit("C groups: Group1=((c1 & c2) or (c2 & c3) or (c4 & c5) or (any of d50 d51 d52))=" + (Cgroup1 ? "Y" : "N") + " Group2=((c6 & c7c81) or (c7c81 & c8) or (c6 & c26))=" + (Cgroup2 ? "Y" : "N") + " Group3=(any one of c9-c13,ADL-LDA 10-13)=" + (Cgroup3 ? "Y" : "N") + " Group4=(any one of c15-c26,ADL-LDA 1-5)=" + (Cgroup4 ? "Y" : "N"));
            if (Cgroup1 && !Cgroup2)
            {
                Program.Audit("Mobility is true and Hygiene is false: Force Hygiene Cgroup2 to be true.");
                Cgroup2 = true;
            }
            int CgroupSUM = (Cgroup1 ? 1 : 0) + (Cgroup2 ? 1 : 0) + (Cgroup3 ? 1 : 0) + (Cgroup4 ? 1 : 0);
            if (CgroupSUM >= 3) SetInd(3, "3 or more C groups.");

            if ((C26 || C30) && C27 && (C28 || C29))
                SetInd(3, "[C26 or C30] and C27 and [C28 or C29]");

            reslist = "Strong stimuli to arouse";
            SetIndIfResultContains(3, "", "3045001046", "", "", reslist, SearchDepth.SearchSince13Hrs);//c33

            reslist = "Bedrest,Held,Stand at bedside,Tilt table,Turn,In bed,bedpan";
            bool d1 = Exists("", "9990000305560", "", "", reslist, SearchDepth.SearchSince13Hrs);

            bool d1x = false;
            if (!periop_found_inpast13hrs)
            {
                //removed commode on 3/12/20
                reslist = "Ambulate in hall,Ambulate in room,Bathroom privileges,Bike,Up ad lib";
                d1x = Exists("", "9990000305560", "", "", reslist, SearchDepth.SearchSince13Hrs);
            }


            reslist = "1 Assist,2 Assist,3 Assist,>4 Assist,4 or more Assist";
            bool d2 = Exists("", "9993040109530", "", "", reslist, SearchDepth.SearchSince13Hrs);
            reslist = "Lying right side,Lying left side,Supine,Semi Fowler,Pillow support,Prone,Sitting,Standing,Up in chair,Micro turn left,Micro turn right,Do Not Turn,Unstable to turn,Turn right side,Turn left side";
            bool d3 = Exists("", "9990000400604", "", "", reslist, SearchDepth.SearchSince13Hrs);
            reslist = "Continuous lateral rotation";
            string reslist2 = "Ambulate in hall,Ambulate in room,Bathroom privileges,Bike,Up ad lib,Chair,Commode,Dangle";
            //SetIndIfResultContains(4, "", "9990000400604", "", "", EXACT_MATCH_PREFIX + reslist, SearchDepth.SearchSince13Hrs);
            int ct = CountResultInList1andNotInList2("9990000400604", reslist, reslist2, SearchDepth.SearchSince13Hrs, CountMode.CountAll, true, out found_what);
            if (ct > 0)
                SetInd(4, "Continuous lateral rotation:" + found_what);

            reslist = "Total Care";
            bool d4 = Exists("", "9991025006475", "", "", reslist, SearchDepth.SearchSince13Hrs);
            reslist = "Left side,Left tilt,Right side,Right tilt,Semi-fowler,Fowler,Chair,Supine,Trendelenberg,Prone";
            bool d5 = Exists("", "1028000027", "", "", reslist, SearchDepth.SearchSince13Hrs);
            reslist = "1 Assist,2 Assist,3 Assist,>4 Assist";
            bool d6 = Exists("", "9990007060350", "", "", reslist, SearchDepth.SearchSince25Hrs);
            reslist = "Bathed,Bath in a bag,Catheter care,Chlorhexidine,Foley care,Peri care";
            bool d7 = Exists("", "9990000342030", "", "", reslist, SearchDepth.SearchSince25Hrs);
            bool d81 = B81;
            reslist = "Complete assist";
            bool d8 = Exists("", "9990000305650", "", "", reslist, SearchDepth.SearchSince25Hrs);

            reslist = "Total assist";
            bool d9 = Exists("", "9990000700380", "", "", reslist, SearchDepth.SearchSince25Hrs);

            reslist = "";
            bool d10 = Exists("", "3043040100003", "", "", reslist, SearchDepth.SearchSince25Hrs);
            bool d11 = Exists("", "9993040100004", "", "", reslist, SearchDepth.SearchSince25Hrs);
            bool d12 = Exists("", "3043040101422", "", "", reslist, SearchDepth.SearchSince25Hrs);
            bool d13 = Exists("", "9993040101423", "", "", reslist, SearchDepth.SearchSince25Hrs);
            bool d14 = (ADLLDA10 || ADLLDA11 || ADLLDA12 || ADLLDA13);

            bool d15 = c15;
            bool d16 = c16;
            bool d17 = c17;
            bool d18 = c18;
            bool d19 = c19;
            bool d20 = c20;

            reslist = "";
            bool d21 = Exists("", "3040011360", "", "", reslist, SearchDepth.SearchSince13Hrs);
            bool d22 = Exists("", "3040002155", "", "", reslist, SearchDepth.SearchSince13Hrs);
            bool d23 = Exists("", "9993040011213", "", "", reslist, SearchDepth.SearchSince13Hrs);
            bool d24 = Exists("", "9993041001004", "", "", reslist, SearchDepth.SearchSince13Hrs);
            bool d25 = Exists("", "9993041001003", "", "", reslist, SearchDepth.SearchSince13Hrs);
            bool d26 = C26;

            //"FOR ADL COMPLETE rows D-1 through D-25, must include charting for all of the bullets below on the shift:
            //- D1 & D2 or D2&D3 or D4&D5(mobility)
            //- D6 & 7 or D7&8(hygiene, please carry over for night shift)
            //-Any of D-9, D - 10, D - 11, D - 12, D - 13, D - 14(ADL - LDA - 10 or ADL - LDA - 11)(feeding, please carry over for night shift)
            //-any rows between D - 15 and D-25 or ADL-LDA - 1, 2,3,4,or 5)  (toileting)
            bool Dgroup1 = ((d1 && d2) || (d2 && d3) || (d4 && d5) || mob1 || mob2 || mob3);
            bool Dgroup2 = ((d6 && (d7 || d81)) || ((d7 || d81) && d8) || (d6 && d26));
            bool Dgroup3 = (d9 || d10 || d11 || d12 || d13 || d14 || b60 || b61);
            bool Dgroup4 = (d15 || d16 || d17 || d18 || d19 || d20 || d21 || d22 || d23 || d24 || d25 || c19 || d77 || d78 || c79 || d80 || c26);
            Program.Audit("D groups: Group1=((d1 & d2) or (d2 & d3) or (d4 & d5) or (any of d50 d51 d52))=" + (Dgroup1 ? "Y" : "N") + " Group2=((d6 & d7d81) or (d7d81 & d8) or (d6 & d26))=" + (Dgroup2 ? "Y" : "N") + " Group3=(any one of d9-d14,ADL-LDA 10-13)=" + (Dgroup3 ? "Y" : "N") + " Group4=(any one of d15-d25,ADL-LDA 1-5)=" + (Dgroup4 ? "Y" : "N"));
            if (d1x)
            {
                Program.Audit("D1 shows mobility = Force Mobility D Group1 to false.");
                Dgroup1 = false;
            }
            if (Dgroup1 && !Dgroup2)
            {
                Program.Audit("Mobility is true and Hygiene is false: Force Hygiene Dgroup2 to be true.");
                Dgroup2 = true;
            }
            int DgroupSUM = (Dgroup1 ? 1 : 0) + (Dgroup2 ? 1 : 0) + (Dgroup3 ? 1 : 0) + (Dgroup4 ? 1 : 0);
            if (DgroupSUM == 4) SetInd(4, "All four D Groups are true.");

            SetIndIfResultBetween(4, "", "3045001023", "", "", 0, 7, SearchDepth.SearchSince13Hrs);
            SetIndIfResultBetween(4, "", "9993040001207", "", "", 0, 7, SearchDepth.SearchSince13Hrs);
            reslist = "Unconscious with motor response,Unconscious with no motor response,Pharmacologically paralyzed";
            SetIndIfResultContains(4, "", "3045001046", "", "", reslist, SearchDepth.SearchSince13Hrs);

            //reslist = "Ambulate in hall,Ambulate in room,Bathroom privileges, Commode, Supervised exercise,Up ad lib";
            //d1 = Exists("", "9990000305560", "", "", reslist, SearchDepth.SearchSince13Hrs);
            if (!d1x)
            {
                reslist = "CPAP,Hi Flow O2";
                SetIndIfResultDoesNotContain(4, "", "3045001117", "", "", reslist, SearchDepth.SearchSince13Hrs);
                reslist = "PACU Ventilator Rows,Trilogy Ventilator Rows,HFO Ventilator Rows,Abbreviated Settings,NAVA Rows,HFJV Rows";
                SetIndIfResultContains(4, "", "9993040000635", "", "", reslist, SearchDepth.SearchSince13Hrs);
            }

            reslist = "Somnolence,RUE paresis ,RLE paresis,LUE paresis,LLE paresis";
            SetIndIfResultContains(4, "", "9990000450600", "", "", reslist, SearchDepth.SearchSince13Hrs);
            reslist = "Unresponsive,Loss Of Consciousness";
            SetIndIfResultContains(4, "", "9990160239301", "", "", reslist, SearchDepth.SearchSince13Hrs);
            reslist = "Respiratory arrest,Cardiac arrest,Unknown,Other";
            SetIndIfResultContains(4, "", "9991600100646", "", "", reslist, SearchDepth.SearchSince13Hrs);
            reslist = "";
            SetIndIfResultContains(4, "", "9993040001002", "", "", reslist, SearchDepth.SearchSince13Hrs);

            if (!_inds[3].is_checked && !_inds[4].is_checked)
            {
                //// "OR  any of these items repeated every 2 hours on the shift:
                ////C1 & C2
                ////C2 & C3
                ////C4 & C5(mobility)
                ////C6 & 7
                ////C7 & 8
                //string reslist1 = "Ambulate in hall,Ambulate in room,Bathroom privileges,Bedrest,Bike,Chair,Commode,Dangle,Supervised exercise,Held,Stand at bedside,Tilt table,Turn,Up in chair,Wagon,In bed,bedpan";
                //string reslist2 = "1 Assist,2 Assist,3 Assist,>4 Assist,4 or more Assist";
                //CheckQ2FreqForADLExt("C1 and C2", "9990000305560", reslist1, "9993040109530", reslist2);

                //reslist1 = "Lying right side,Lying left side,Supine,Semi Fowler,Pillow support,Prone,Sitting,Standing,Up in chair,Micro turn left,Micro turn right,Do Not Turn,Unstable to turn,Turn right side,Turn left side";
                //CheckQ2FreqForADLExt("C2 and C3", "9993040109530", reslist2, "9990000400604", reslist1);

                //reslist1 = "Assist,Total Care";
                //reslist2 = "Left side,Left tilt,Right side,Right tilt,Semi-fowler,Fowler,Knee chest,Chair,Rocking chair,Supine,Trendelenberg,Squatting,Standing,Kneeling,Walking";
                //CheckQ2FreqForADLExt("C4 and C5", "9991025006475", reslist1, "1028000027", reslist2);

                //reslist1 = "1 Assist,2 Assist,3 Assist,>4 Assist";
                //reslist2 = "Bathed,Chlorhexidine,Showered,Bath in a bag,Catheter care,Foley care,Peri care";
                //CheckQ2FreqForADLExt("C6 and C7", "9990007060350", reslist1, "9990000342030", reslist2);

                //reslist1 = "Partial assist,Complete assist";
                //CheckQ2FreqForADLExt("C7 and C8", "9990000342030", reslist2, "9990000305650", reslist1);

                ////C6 & 26(hygiene)
                ////C - 15
                ////C - 16
                ////C - 17
                ////C - 18
                ////C - 19(ADL - LDA - 1, 2, 3, 4, 5, 6)
                ////C - 20
                ////C - 21"	
                //reslist1 = "1 Assist,2 Assist,3 Assist,>4 Assist";
                //reslist2 = "Done";
                //CheckQ2FreqForADLExt("C6 and C26", "9990007060350", reslist1, "9991600100064", reslist2);

                //reslist1 = "Yes";
                //CheckQ2FreqForADLExt("C15", EXACT_MATCH_PREFIX + "23", reslist1, "", "");

                //CheckQ2FreqForADLExt("C16", "3045001090", "", "", "");
                //CheckQ2FreqForADLExt("C17", "3045001089", "", "", "");

                //reslist1 = "Yes";
                //CheckQ2FreqForADLExt("C18", EXACT_MATCH_PREFIX + "16", reslist1, "", "");

                //string codelist = "3045001088,9990007085310,9993040104906,9990000396150,9990007085490,9993040021276";
                //CheckQ2FreqForADLExt("C19", codelist, "", "", "");

                //reslist = "Amber,Clear,Cloudy,Dark,Fibrin,Light,Pink,Red,Yellow";
                //CheckQ2FreqForADLExt("C20", "9990000370180", reslist, "", "");

                //CheckQ2FreqForADLExt("C21", "3040011360", "", "", "");

            }

            if (!_inds[2].is_checked && !_inds[3].is_checked && !_inds[4].is_checked)
            {
                SetInd(1, "Defaulting to ADL Self");
            }

        }

        private bool Check_b79(out string found_what)
        {
            bool b = false;
            string d = "";

            var query = StartNewQuery(SearchDepth.SearchSince13Hrs);
            query = query.Where(e => e.CODE.ToUpper().StartsWith("MED"));
            query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;given") && (e.DESCRIPTION.ToUpper().Contains("PEG3350 100 GRAM-SOD SUL") || e.DESCRIPTION.ToUpper().Contains("PEG 3350-ELECTROLYTES 236")));
            query = query.OrderByDescending(e => e.EVENT_DATETIME);
            //query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;given"));
            b = (query.Count() > 0);
            if (b) d = query.First().DESCRIPTION;
            found_what = d;
            return b;
        }

        private void CheckQ2FreqForADLExt(string maprow, string code1, string res1, string code2, string res2)
        {
            if (_inds[3].is_checked || _inds[4].is_checked) return;
            var buckets = new List<gBucket>();
            if (code2 != "")
                AddDependentBuckets(buckets, code1, res1, code2, res2);
            else
                AddBuckets(buckets, "", code1, "", "", res1);
            if (AnalyzeBuckets(buckets, 3, 120, maprow, false))
                SetInd(3, maprow);
        }



        private void Check_5()
        {
            string reslist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("5. ADL Rehab");
            Program.VerboseAudit("---------------");

            exclude_periop_data = true;

            reslist = "Pt learning use of adaptive equipment,Pt learning swallowing techniques";//,Pt learning compensatory techniques";
            SetIndIfResultContains(5, "", "9993040410020", "", "", reslist);
            reslist = "Pt learning use of adaptive equipment";//,Pt learning compensatory techniques";
            SetIndIfResultContains(5, "", "9993040410021", "", "", reslist);
            reslist = "Pt learning bowel evacuation procedures,Pt learning bladder re-training procedures";//,Pt learning compensatory techniques";
            SetIndIfResultContains(5, "", "9993040410022", "", "", reslist);
            reslist = "Pt learning use of adaptive equipment";//,Pt learning compensatory techniques";
            SetIndIfResultContains(5, "", "9993040410023", "", "", reslist);
            reslist = "Apnea,Bradycardia,Continued irritability,Desat,Flaccid extremities-low tone,Flaccid jaw-low tone,Large emesis,O2 >10,Repeated cough,Repeated gag,Repeated head bobbing,Repeated hiccup,Sudden state change,Tachycardia,Tongue thrust,Other";
            SetIndIfResultContains(5, "", "9991733565671", "", "", reslist);
            reslist = "Cup feeding,Spoon feeding,Syringe feeding";
            SetIndIfResultContains(5, "", "9991025073970", "", "", reslist);
        }


        private void Check_6_7()
        {
            string reslist;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("6. ADL 2-3 Caregivers");
            Program.VerboseAudit("7. ADL 4 or more Caregivers");
            Program.VerboseAudit("---------------");

            exclude_periop_data = false;

            //if (_pat.age < 4.0)
            {
                reslist = "CPAP vent,ETT nasal,ETT oral";
                SetIndIfResultContains(6, "", "9991733888883", "", "", reslist);
                reslist = "CPAP,Hi Flow O2";
                SetIndIfResultDoesNotContain(6, "", "3045001117", "", "", reslist);
                reslist = "PACU Ventilator Rows,Trilogy Ventilator Rows,HFO Ventilator Rows,Abbreviated Settings,NAVA Rows,HFJV Rows";
                SetIndIfResultContains(6, "", "9993040000635", "", "", reslist);
            }

            reslist = "Ambulate in hall,Ambulate in room,Bathroom privileges,Chair,Commode,Dangle,Stand at bedside,Stroller,Tilt table,Turn,Up in chair,Wagon,In bed,bedpan";
            bool f1 = Exists("", "9990000305560", "", "", reslist, SearchDepth.SearchSince13Hrs);
            reslist = "2 Assist,3 Assist";
            bool f2 = Exists("", "9993040109530", "", "", reslist, SearchDepth.SearchSince13Hrs);
            if (f1 && f2) SetInd(6, "F1 and F2");

            reslist = "Lying right side,Lying left side,Supine,Semi Fowler,Pillow support,Prone,Sitting,Standing,Up in chair,Continuous lateral rotation,Micro turn left,Micro turn right,Do Not Turn,Unstable to turn,Turn right side,Turn left side";
            bool f3 = Exists("", "9990000400604", "", "", reslist, SearchDepth.SearchSince13Hrs);
            if (f2 && f3) SetInd(6, "F2 and F3");

            reslist = "2 Assist,3 Assist";
            bool f4 = Exists("", "9990007060350", "", "", reslist, SearchDepth.SearchSince13Hrs);
            reslist = "Bathed,Chlorhexidine,Showered,Bath in a bag,Catheter care,Peri care";
            bool f5 = Exists("", "9990000342030", "", "", reslist, SearchDepth.SearchSince13Hrs);
            if (f4 && f5) SetInd(6, "F4 and F5");

            reslist = "Two staff assist";
            SetIndIfResultContains(6, "", "9991600100065", "", "", reslist, SearchDepth.SearchSince13Hrs);


            reslist = "Ambulate in hall,Ambulate in room,Chair,Dangle,Stand at bedside,Tilt table,Turn,Up in chair,In bed,bedpan";
            bool g1 = Exists("", "9990000305560", "", "", reslist, SearchDepth.SearchSince13Hrs);
            reslist = ">4 Assist,4 or more Assist";
            bool g2 = Exists("", "9993040109530", "", "", reslist, SearchDepth.SearchSince13Hrs);
            if (g1 && g2) SetInd(7, "G1 and G2");

            reslist = "Lying right side,Lying left side,Supine,Semi Fowler,Pillow support,Prone,Sitting,Standing,Up in chair,Continuous lateral rotation,Micro turn left,Micro turn right,Turn right side,Turn left side";
            bool g3 = Exists("", "9990000400604", "", "", reslist, SearchDepth.SearchSince13Hrs);
            if (g2 && g3) SetInd(7, "G2 and G3");

            reslist = ">4 Assist";
            bool g4 = Exists("", "9990007060350", "", "", reslist, SearchDepth.SearchSince13Hrs);
            reslist = "Bathed,Chlorhexidine,Showered,Bath in a bag,Peri care";
            bool g5 = Exists("", "9990000342030", "", "", reslist, SearchDepth.SearchSince13Hrs);
            if (g4 && g5) SetInd(7, "G4 and G5");

        }

        private void Check_8()
        {
            string reslist;
            string found_what;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("8. Communication");
            Program.VerboseAudit("---------------");

            exclude_periop_data = true;

            reslist = "Artificial airway,Attempts to verbalize,Delayed responses,Dysphasia,Expressive aphasia,Garbled,Global aphasia,Incomprehensible,Nods/gestures appropriately,Non-verbal,Receptive aphasia,Slurred,Uses communication aid(s)";
            bool h1 = Exists("", "9990000301890", "", "", reslist, SearchDepth.SearchSince16Hrs);
            if (h1)
            {
                bool h1exact = Exists("", "9990000301890", "", "", EXACT_MATCH_PREFIX + "Artificial airway", SearchDepth.SearchSince16Hrs);
                if (h1exact)
                {
                    int h1r_ct = CountResultContains("", "3045001079", "", "", "-4,-5", SearchDepth.SearchSince16Hrs, true, out found_what);
                    //Program.VerboseAudit("Speech H1R: Sedation Score count=" + h1r_ct);
                    if (h1r_ct <= 1)
                        SetInd(8, "Speech H1R: Sedation Score less-than-2 count=" + h1r_ct);
                }
                else
                    SetInd(8, "Speech H1");
            }

            reslist = "Impaired vision- not corrected,Blind";
            bool h2 = Exists("", "9990000002106", "", "", reslist, SearchDepth.SearchSince16Hrs);
            reslist = "Impaired vision- not corrected,Blind";
            bool h3 = Exists("", "9990000002107", "", "", reslist, SearchDepth.SearchSince16Hrs);
            if (h2 && h3) SetInd(8, "H2 and H3");

            reslist = "Impaired hearing- not corrected,Acute hearing loss,Deaf";
            bool h4 = Exists("", "9990000002108", "", "", reslist, SearchDepth.SearchSince16Hrs);
            reslist = "Impaired hearing- not corrected,Acute hearing loss,Deaf";
            bool h5 = Exists("", "9990000002109", "", "", reslist, SearchDepth.SearchSince16Hrs);
            if (h4 && h5) SetInd(8, "H4 and H5");

            reslist = "Difficulty talking,Trach,Hoarse,Muffled,Speaking valve,Voice amplifier";
            SetIndIfResultContains(8, "", "9990000002115", "", "", reslist, SearchDepth.SearchSince16Hrs);
            reslist = "3";
            SetIndIfResultContains(8, "", "9990304000117", "", "", reslist, SearchDepth.SearchSince16Hrs);
            reslist = "3";
            SetIndIfResultContains(8, "", "3045001024", "", "", reslist, SearchDepth.SearchSince16Hrs);
            reslist = "Delayed,Impoverished,Mumbled,Mute,Overproductive,Pressured,Rambling,Rapid,Slurred,Slow,Stutter";
            bool h9 = SetIndIfResultContains(8, "", "9993040105630", "", "", reslist);
            reslist = "1,2,3";
            SetIndIfResultContains(8, "", "9990007090150", "", "", reslist);
            reslist = "1,2";
            SetIndIfResultContains(8, "", "9990007090160", "", "", reslist);
            reslist = "2,3";
            SetIndIfResultContains(8, "", "9990007090070", "", "", reslist);

            reslist = "Blindness - right,Blindness - left";
            SetIndIfResultContainsAll(8, "", "9993040001091", "", "", reslist, SearchDepth.SearchSince16Hrs);

            reslist = "Unable to hear - right,Unable to hear - left";
            SetIndIfResultContainsAll(8, "", "9990000002221", "", "", reslist, SearchDepth.SearchSince16Hrs);

            CheckInterpreterItems();

            reslist = "Mask,Tubing,CPAP,BPAP,Patient able to manage equipment on own,Equipment inspected (per site policy),Waiver signed (per site policy),Other";
            bool h23 = Exists("", "9993040000639", "", "", reslist, SearchDepth.SearchSince16Hrs);
            if (!h23 && _pat.age >= 2.0)
            {
                reslist = "Bilevel,Auto-Bilevel,AVAPS,PCV";
                SetIndIfResultContains(8, "", "3045001108", "", "", reslist, SearchDepth.SearchSince16Hrs);
                reslist = "Total face mask,Endotracheal,Tracheostomy";
                SetIndIfResultContains(8, "", "9993040000637", "", "", reslist, SearchDepth.SearchSince16Hrs);
            }

            SetIndIfResultContains(8, "", "9993040108584", "", "", "Yes");
            SetIndIfResultContains(8, "", "9993040108585", "", "", "Yes");
            SetIndIfResultContains(8, "", "9990007081420", "", "", "No");
            SetIndIfResultContains(8, "", "9990007085050", "", "", "Deaf");
            SetIndIfResultContains(8, "", "9990007085060", "", "", "Deaf");
            SetIndIfResultContains(8, "", "9991150000153", "", "", "Slow and slurred");

        }

        private void CheckInterpreterItems()
        {  //look back = 36 hrs
            string code;
            string descript;
            DateTime evdt;
            string res;
            bool done = false;

            string sql = "select ci.code,ci.description,ci.event_datetime,ci.result from chart_item as ci";
            sql += " where ci.encounter_id=" + _pat.encounter_id;
            sql += " and ((ci.code like '%9991733444441%' and (ci.result like '%in person%' or ci.result like '%ipad%' or ci.result like '%phone%' or ci.result like '%other%'))";
            sql += " or (ci.code like '%9993040109069%' and (ci.result like '%Admission%' or ci.result like '%Assessment%' or ci.result like '%consent%' or ci.result like '%discharge instructions%' or ci.result like '%education%' or ci.result like '%plan of care%' or ci.result like '%other%'))";
            sql += " or (ci.code like '%9993040109071%' and (ci.result like '%Hospital/clinic approved on site interpreter%' or ci.result like '%Telephone interpreter%' or ci.result like '%Video remote interpreter%' or ci.result like '%other%'))";
            sql += " or (ci.code like '%9993040108551%')";
            sql += " or (ci.code like '%9990007070581%' and (ci.result like '%Braille%' or ci.result like '%Communication board%' or ci.result like '%hearing aid%' or ci.result like '%interpreter%' or ci.result like '%sign language%'))";
            sql += " or (ci.code like '%9993040108693%' and (ci.result like '%yes%')))";
            sql += " and ci.event_datetime >='" + _pat.pull_finish.AddHours(-36).ToString() + "'";
            sql += " order by ci.event_datetime desc";
            //Program.VerboseAudit(sql);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd = new SqlCommand(sql, db2);
            SqlDataReader dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr2.Read() && !done)
            {
                code = PFSDBUtility.DBToString(dr2["CODE"]);
                descript = PFSDBUtility.DBToString(dr2["DESCRIPTION"]);
                evdt = PFSDBUtility.DBToDateTime(dr2["EVENT_DATETIME"]);
                res = PFSDBUtility.DBToString(dr2["RESULT"]);
                SetInd(8, "Found interpreter within 36 hrs: " + code + "/" + res + "/" + evdt.ToString());
                done = true;
            }
            db2.Close();

        }

        private bool AllOriented()
        {
            int ct = 0;

            var query = StartNewQuery(SearchDepth.SearchSince13Hrs);    // add custom time range below
            query = query.Where(e => e.CODE.StartsWith("9990000301870"));
            query = query.Where(e => (e.RESULT.ToLower().StartsWith("oriented to person") || e.RESULT.ToLower().Contains(";oriented to person"))
            && (e.RESULT.ToLower().StartsWith("oriented to place") || e.RESULT.ToLower().Contains(";oriented to place"))
            && (e.RESULT.ToLower().StartsWith("oriented to time") || e.RESULT.ToLower().Contains(";oriented to time")));
            ct = query.Count();
            if (ct > 0)
                Program.VerboseAudit("All 3 Orientation found: " + query.Count());
            return (ct > 0);
        }


        private void Check_9()
        {
            string reslist;
            bool s2, s3, s4;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("9. Cognitive Support");
            Program.VerboseAudit("---------------");

            exclude_periop_data = true;

            reslist = "Confused";
            SetIndIfResultContains(9, "", "3045001046", "", "", reslist);
            //reslist = "Oriented to person,Oriented to place,Oriented to time";
            if (!AllOriented())
            {
                reslist = "Disoriented X4,Disoriented X3,Disoriented X 4,Disoriented X 3,Disoriented to place,Disoriented to time,Disoriented to situation,Disoriented to person";
                SetIndIfResultContains(9, "", "9990000301870", "", "", reslist);

                s2 = Exists("", "9990000301870", "", "", STARTS_WITH + "Oriented to person", SearchDepth.SearchSince16Hrs);
                s2 |= Exists("", "9990000301870", "", "", ";Oriented to person", SearchDepth.SearchSince16Hrs);
                s3 = Exists("", "9990000301870", "", "", STARTS_WITH + "Oriented to place", SearchDepth.SearchSince16Hrs);
                s3 |= Exists("", "9990000301870", "", "", ";Oriented to place", SearchDepth.SearchSince16Hrs);
                s4 = Exists("", "9990000301870", "", "", STARTS_WITH + "Oriented to time", SearchDepth.SearchSince16Hrs);
                s4 |= Exists("", "9990000301870", "", "", ";Oriented to time", SearchDepth.SearchSince16Hrs);
                if (s2 || s3 || s4) SetInd(9, "At least one of (but not all): Oriented to person,Oriented to place,Oriented to time");

            }

            SetIndIfResultContains(9, "", "9990000301880", "", "", AVOID_NEGATIVE + "Short term memory loss");
            reslist = "Unable to follow commands,Poor attention";
            SetIndIfResultContains(9, "", "9990000301880", "", "", reslist);
            reslist = "4,3";
            SetIndIfResultContains(9, "", "3045001024", "", "", reslist);
            SetIndIfResultBetween(9, "", "9990000398002", "", "", 4, 7, SearchDepth.SearchSince16Hrs);
            SetIndIfResultBetween(9, "", "9990000398004", "", "", 4, 7, SearchDepth.SearchSince16Hrs);
            SetIndIfResultBetween(9, "", "9990000398006", "", "", 4, 7, SearchDepth.SearchSince16Hrs);
            reslist = "Disoriented for place or person";
            SetIndIfResultContains(9, "", "9990000398010", "", "", reslist);
            reslist = "Hallucination,Illusions,Patient responding to internal Stimuli";
            SetIndIfResultContains(9, "", "9993040105625", "", "", reslist);
            reslist = "Auditory (Comment),Visual (Comment),Tactile (Comment),Command,Olfactory";
            SetIndIfResultContains(9, "", "9991540100181", "", "", reslist);
            reslist = "Confused,Disoriented,Incoherent";
            SetIndIfResultContains(9, "", "9993040105626", "", "", reslist);
            reslist = "Delusions,Paranoia";
            SetIndIfResultContains(9, "", "9993040105627", "", "", reslist);
            reslist = "Persecution,Grandeur,Obsessions,Religiosity,Phobias,Influence,Reference,Sexual delusions,Somatization,Thought broadcasting,Thought insertion,Thought withdrawal";
            SetIndIfResultContains(9, "", "9991540100180", "", "", reslist);
            reslist = "Disoriented X4";
            SetIndIfResultContains(9, "", "9993040105631", "", "", reslist);
            reslist = "Impaired short term memory";
            SetIndIfResultContains(9, "", "9991540100198", "", "", reslist);

            bool i16 = Exists("", "9990000301870", "", "", "Developmentally delayed", SearchDepth.SearchSince16Hrs);
            if (i16)
                SetIndIfResultContains(9, "", "9993040000410", "", "", "2,3");

        }


        private void Check_10_11()
        {
            string reslist;
            bool is_peds = false;
            //int ct;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("10. Behavior/Emotional Management");
            Program.VerboseAudit("11. Behavior/Emotional Mgmt - q 1 Hour");
            Program.VerboseAudit("---------------");

            exclude_periop_data = false;
            is_peds = (_pat.age < 15.0);

            reslist = "1:1 RN Time,1:1 Staff Time,Assign a task,Aromatherapy,Assisted relaxation,Consistent response,Exposure therapy,Limit setting,Medicated,Neutral response,Physical redirection,Quiet room,Reorientation,Time out,Verbal redirection,Following behavior safety plan,Multiple Staff for acute behavioral intervention";
            reslist += ",Active listening,Emotional support given,Following behavior safety plan";
            bool j2 = Exists("", "9993040000352", "", "", reslist, SearchDepth.SearchSince13Hrs);
            if (is_peds && j2)
                SetInd(10, "Peds STAFF INTERVENTIONS found: 9993040000352");

            reslist = "Agitated,Behavior plan,Catatonic,Combative,Compulsive,Crying";
            reslist += ",Demanding,Destructive,Dismissive,Disorganized,Disruptive";
            reslist += ",Echopraxic,Exit seeking,Flat affect,Flight of ideas,Fussy";
            reslist += ",Guarded,Hostile,Hyperactive,Impulsive,Intrusive,Motor perseveration";
            reslist += ",Oppositional,Pacing,Preoccupied,Psychomotor retardation";
            reslist += ",Pushing limits,Reckless,Resistant to care,Rigid,Ritualistic";
            reslist += ",Tearful,Staff seeking,Seclusive,Sexually inappropriate,Tics,Withdrawn";
            reslist += ",Delusional,Hallucinating";
            bool j1 = Exists("", "9993040105629", "", "", reslist, SearchDepth.SearchSince13Hrs);
            bool k1 = j1;

            if (j2 || is_peds)
            {
                if (j1 && !is_peds) SetInd(10, "Behavior found: 9993040105629");
                reslist = "Agitated,Combative,Compulsive,Crying,Destructive,Disorganized,Disruptive";
                reslist += ",Guarded,Hostile,Hyperactive,Intrusive,Oppositional,Pacing,Preoccupied";
                reslist += ",Pushing limits,Reckless,Resistant to care,Rigid,Ritualistic,Tearful";
                reslist += ",Staff seeking,Sexually inappropriate,Tics,Withdrawn";
                SetIndIfResultContains(10, "", "9993040009823", "", "", reslist);
                //reslist = "Angry,Anxious,Apathetic,Depressed,Dysphoric,Elevated,Euphoric,Fearful,Guilty,Irritable,Manic,Sad";
                //SetIndIfResultContains(10, "", "9993040105622", "", "", reslist);
            }

            reslist = "Yes";
            SetIndIfResultContains(10, "", "9993040105637", "", "", reslist);
            reslist = "Acts of violence,Threats of violence";
            SetIndIfResultContains(10, "", "9993040105640", "", "", reslist);

            if (j2 || is_peds)
            {
                reslist = "Disruptive";
                SetIndIfResultContains(10, "", "9993040000405", "", "", reslist);
                reslist = "Hallucination,Illusions,Patient responding to internal Stimuli";
                SetIndIfResultContains(10, "", "9993040105625", "", "", reslist);
                reslist = "Auditory,Visual,Tactile,Command,Olfactory";
                SetIndIfResultContains(10, "", "9991540100181", "", "", reslist);
                reslist = "Persecution,Grandeur,Obsessions,Religiosity,Phobias,Influence,Reference,Sexual delusions,Somatization,Thought broadcasting,Thought insertion,Thought withdrawal";
                SetIndIfResultContains(10, "", "9991540100180", "", "", reslist);
            }

            reslist = "Pillow,Memory box,Bereavement package,Other";
            SetIndIfResultContains(10, "", "9991020100509", "", "", reslist);
            reslist = "Baby bracelet,Blanket,Clay molds,Clothing,Complimentary birth certificate,Crib card,Foot prints,Hand prints,Hat,Lock of hair,Photos,Pin,Prism,Tape measure,Other";
            SetIndIfResultContains(10, "", "9991020100510", "", "", reslist);
            reslist = "Done";
            SetIndIfResultContains(10, "", "9991020100506", "", "", reslist);

            reslist = "Blanket,Bundled,Gown changed,Cares clustered,Held / cuddled";
            reslist += ",Infant seat/ swing,Lotion,Music,Pacifier,Pain medication";
            reslist += ",Rocking,Sucrose,Swaddled,Touch,Toy,Other";
            SetIndIfResultContains(10, "", "9990000342040", "", "", reslist);
            SetIndIfResultContains(10, "", "9991733214522", "", "", "");
            SetIndIfResultContains(10, "", "9991733214523", "", "", "");

            SetIndIfResultBetween(10, "", "9993040100610", "", "", 1, 999, SearchDepth.SearchSince13Hrs);
            SetIndIfResultBetween(10, "", "9991733000016", "", "", 1, 999, SearchDepth.SearchSince13Hrs);

            reslist = "Agitated,Verbally abusive,Tearful,Hallucinating,Delusional";
            SetIndIfResultContains(10, "", "9990000300113,9990000300018,9990000047549", "", "", reslist);
            reslist = "Other";
            SetIndIfResultContains(10, "", "9990000300018,9990000047549", "", "", reslist);


            string codelist = "9993040000352";
            reslist = "1:1 RN Time,1:1 Staff Time,Assign a task,Aromatherapy,Assisted relaxation,Consistent response,Exposure therapy,Limit setting,Medicated (see MAR),Neutral response,Physical redirection,Quiet room,Reorientation,Time out,Verbal redirection";
            reslist += ",Active listening,Emotional support given,Following behavior safety plan";
            var buckets = new List<gBucket>();
            AddBuckets(buckets, "", codelist, "", "", reslist, SearchDepth.SearchSince9Hrs);
            bool k2a = AnalyzeBuckets(buckets, 11, 70, "K2a", false);

            //reslist = "Following behavior safety plan,Multiple Staff for acute behavioral intervention";
            reslist = "Multiple Staff for acute behavioral intervention";
            bool k2b = SetIndIfResultContains(11, "", "9993040000352", "", "", reslist);
            reslist = "Agitated,Combative,Compulsive,Crying,Destructive,Disorganized,Disruptive,Guarded,Hostile,Hyperactive,Intrusive,Oppositional,Pacing,Preoccupied,Pushing limits,Reckless,Resistant to care,Rigid,Ritualistic,Tearful,Staff seeking,Sexually inappropriate,Tics,Withdrawn";
            bool k3 = Exists("", "9993040009823", "", "", reslist, SearchDepth.SearchSince9Hrs);
            if (k2a && (k1 || k3))
                SetInd(11, "K1 and K2a -OR- K3 and K2a");

            reslist = "4 hours,2 hours,1 hour";
            SetIndIfResultContains(11, "", "9990000300001", "", "", reslist);
            reslist = "1:1 Nsg care for seclusion for the first hour,Constant Nsg care for restraints for the first hour,Record behavior every 5 minutes,Review strengths/comfort measures - assist pt in reaching goal for discontinuation,Administration of medication - to help client regain previous level of functioning,Respiratory status assessed/documented each check - to ensure adequate air exchange,Restraints checked every 15 minutes - to prevent injury from cuffs constricting normal blood flow or irritation of skin,RN hourly assessment - mental, behavior and respiratory status - to minimize length of the procedure,Offer fluids every 2 hours - to provide elimination opportunities,Offer toileting every 2 hours - to provide elimination opportunities,ROM to joints every 2 hours - to prevent injury from occurring when client is in restraints,Hygiene PRN - to provide comfort and support,Meals and snacks offered - to maintain nutrition,Other";
            SetIndIfResultContains(11, "", "9990007096219", "", "", reslist);
            reslist = "Implemented - Seclusion Treatment Policy,Implemented - Behavioral Restraint Policy,Procedure explained to patient,Reason for seclusion/restraints explained,Informed the patient of the goal,Mattress checked for dangerous items,Mattress checked for dangerous items,check room for lighting, temperature and safety,Dangerous items and jewelry removed,Respiratory status monitored,Pt assisted in achieving goal for d/c";
            SetIndIfResultContains(11, "", "9990007096227", "", "", reslist);

            buckets = new List<gBucket>();
            reslist = "Blanket,Bundled,Gown changed,Cares clustered,Held / cuddled";
            reslist += ",Infant seat/ swing,Lotion,Music,Pacifier,Pain medication";
            reslist += ",Rocking,Sucrose,Swaddled,Touch,Toy,Other";
            AddBuckets(buckets, "", "9990000342040", "", "", reslist, SearchDepth.SearchSince9Hrs);
            AddBuckets(buckets, "", "9991733214522,9991733214523", "", "", "", SearchDepth.SearchSince9Hrs);
            bool sucrose = AnalyzeBuckets(buckets, 11, 70, "Sucrose K-14,K-15,K-16", true);

            buckets = new List<gBucket>();
            AddBuckets(buckets, "", "9993040100610,9991733000016", "", "", "", SearchDepth.SearchSince9Hrs);
            AnalyzeBuckets(buckets, 11, 70, "Coping K-17,K-18", true);

            buckets = new List<gBucket>();
            reslist = "Agitated,Verbally abusive,Tearful,Hallucinating,Delusional";
            AddBuckets(buckets, "", "9990000300113,9990000300018,9990000047549", "", "", reslist, SearchDepth.SearchSince9Hrs);
            AddBuckets(buckets, "", "9990000300018,9990000047549", "", "", "Other", SearchDepth.SearchSince9Hrs);
            AnalyzeBuckets(buckets, 11, 70, "Psychological status K-19", true);

        }

        private bool IsQ1Freq()
        {
            bool ret = false;


            return ret;
        }

        // (this isn't really a Q1h count -- it is just a count) Make this a q1hr count
        //private int ReturnQ1HrCount(string code_list, string result_list)
        //{
        //    var buckets = new List<gBucket>();
        //    SetBucketSize(60);
        //    AddBuckets(buckets, "", code_list, "", "", result_list);
        //    return CountBuckets(buckets);
        //}

        //private bool IsQ1Hour(int count)
        //{
        //    return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q1H);
        //}
        //private bool IsQ2Hour(int count)
        //{
        //    return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q2H);
        //}

        // Use this if counting safety buckets
        //private void CheckSafety(int count, string desc)
        //{
        //    if (_inds[13].is_checked) return;             //skip if highest already checked
        //    if (count == 0) return;
        //    switch (FreqForCount(_pat.los_hours, count))
        //    {
        //    case Frequencies.Q30M:
        //        SetInd(13, desc + " q30min");
        //        break;
        //    case Frequencies.Q1H:
        //    case Frequencies.Q2H:
        //        SetInd(12, desc + " q2h");
        //        break;
        //    default:
        //        Program.VerboseAudit(desc + ": " + count + " in " + Math.Round(_pat.los_hours) + " hours is not enough");
        //        break;
        //    }
        //}

        private void FindLatest(string code, string reslist, out string res, out DateTime evdt)
        {
            res = "???";
            evdt = DateTime.MinValue;

            var query = StartNewQuery(SearchDepth.SearchSince16Hrs);
            query = AndItemFilter(query, "", code, "", "", reslist);
            CHART_ITEM ch = query.OrderByDescending(e => e.EVENT_DATETIME).FirstOrDefault();
            if (ch == null) return;
            res = ch.RESULT;
            evdt = ch.EVENT_DATETIME;
            Program.VerboseAudit("Latest result and time:" + ch.RESULT + ch.EVENT_DATETIME.ToString());

        }

        private void Check_12_13()
        {
            string reslist;
            bool is_peds = false;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("12. Safety Management - q 2 Hours");
            Program.VerboseAudit("13. Safety Management - q 30 Minutes");
            Program.VerboseAudit("---------------");

            exclude_periop_data = false;
            is_peds = (_pat.age < 15.0);

            reslist = "Impulsive,Lack of safety awareness,Does not use call light or ask for assistance,Danger to self or others,Danger from others,Exit seeking,Epilepsy monitoring,Grid/SEEG electrodes";
            bool l1 = Exists("", "9993040009123", "", "", reslist, SearchDepth.SearchSince16Hrs);

            string res = "";
            DateTime evdt = DateTime.MinValue;
            string reslist12 = "Q2 hour,Q1 hour";
            string reslist13 = "Q30 min,Q15 min,Q5 min,Line of sight,Continuous observation by RN with patient,Continuous observation by non-RN staff with patient,Continuous observation by two staff with patient";
            FindLatest("9993040009234", reslist12 + "," + reslist13, out res, out evdt);
            //Program.VerboseAudit("res=" + res + " evdt=" + evdt.ToString());
            bool l2 = (reslist12.ToLower().Contains(res.ToLower()));
            bool m2 = (reslist13.ToLower().Contains(res.ToLower()));

            if (l2 && is_peds)
                SetInd(12, "Peds L2");
            else if (l1 && l2)
                SetInd(12, "L1 and L2");

            reslist = "Start,Continue";
            SetIndIfResultContains(12, "", "9993040009567", "", "", reslist, SearchDepth.SearchSince16Hrs);
            reslist = "On";
            SetIndIfResultContains(12, "", "9990007096444", "", "", reslist, SearchDepth.SearchSince16Hrs);

            reslist = "NG tube,OG tube,G-tube,J-tube,ND tube,OD tube,PEG-tube";
            bool l5 = Exists("", "9991733565652", "", "", reslist, SearchDepth.SearchSince16Hrs);
            reslist = "Bolus per gravity,Bolus per pump";
            bool l6 = Exists("", "9990007074010", "", "", reslist, SearchDepth.SearchSince16Hrs);
            if (l5 && l6) SetInd(12, "L5 and L6");

            reslist = "Impulsive,Lack of safety awareness,Does not use call light or ask for assistance,Danger to self or others,Danger from others,Exit seeking,Epilepsy monitoring,Grid/SEEG electrodes";
            bool m1 = Exists("", "9993040009123", "", "", reslist, SearchDepth.SearchSince16Hrs);
            if (m2 && is_peds)
                SetInd(13, "Peds M2");
            else if (m1 && m2)
                SetInd(13, "M1 and M2");

            reslist = "24 hours";
            SetIndIfResultContains(13, "", "9990000300101", "", "", reslist);
            reslist = "4 hours,2 hours,1 hour";
            SetIndIfResultContains(13, "", "9990000300001", "", "", reslist);
            reslist = "1:1 Nsg care for seclusion for the first hour,Constant Nsg care for restraints for the first hour,Record behavior every 5 minutes,Review strengths/comfort measures - assist pt in reaching goal for discontinuation,Administration of medication - to help client regain previous level of functioning,Respiratory status assessed/documented each check - to ensure adequate air exchange,Restraints checked every 15 minutes - to prevent injury from cuffs constricting normal blood flow or irritation of skin,RN hourly assessment - mental, behavior and respiratory status - to minimize length of the procedure,Offer fluids every 2 hours - to provide elimination opportunities,Offer toileting every 2 hours - to provide elimination opportunities,ROM to joints every 2 hours - to prevent injury from occurring when client is in restraints,Hygiene PRN - to provide comfort and support,Meals and snacks offered - to maintain nutrition (document reason for not providing and meal or snack at regular times),Other";
            SetIndIfResultContains(13, "", "9990007096219", "", "", reslist);
            reslist = "Implemented - Seclusion Treatment Policy,Implemented - Behavioral Restraint Policy,Procedure explained to patient,Reason for seclusion/restraints explained,Informed the patient of the goal,Mattress checked for dangerous items,Mattress checked for dangerous items,check room for lighting, temperature and safety,Dangerous items and jewelry removed,Respiratory status monitored,Pt assisted in achieving goal for d/c";
            SetIndIfResultContains(13, "", "9990007096227", "", "", reslist);

            reslist = "NG tube,OG tube,G-tube,J-tube,ND tube,OD tube,PEG-tube";
            bool m7 = Exists("", "9991733565652", "", "", reslist, SearchDepth.SearchSince16Hrs);
            reslist = "Continous pump,Continuous pump";
            bool m8 = Exists("", "9990007074010", "", "", reslist, SearchDepth.SearchSince16Hrs);
            if (m7 && m8) SetInd(13, "M7 and M8");


        }


        private void Check_14()
        {
            string reslist, codelist;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("14. Isolation");
            Program.VerboseAudit("---------------");

            exclude_periop_data = false;

            codelist = EXACT_MATCH_PREFIX + "ISO1";
            SetIndIfResultContains(14, "", codelist, "", "", "");
            codelist = "ISO2,ISO4,ISO8,ISO9,ISO10";
            SetIndIfResultContains(14, "", codelist, "", "", "");
            reslist = "Contact,Airborne,Droplet,Modified Contact,Modified Droplet,Modified Airborne";
            SetIndIfResultContains(14, "", "ISOLATION", "", "", reslist);
            if (_pat.is_isolation) SetInd(14, "Isolation data found in ADT.");

        }

        //private void CheckAssessment(int count, string desc)
        //{
        //    //if (_inds[18].is_checked) return;          //skip if highest already checked
        //    if (count == 0) return;                    //skip if none

        //    // This should work the same as the original code:
        //    switch (FreqForCount(_pat.los_hours, count))
        //    {
        //        case Frequencies.Q30M:
        //            SetInd(18, desc);
        //            break;
        //        case Frequencies.Q1H:
        //            SetInd(17, desc);
        //            break;
        //        case Frequencies.Q2H:
        //            SetInd(16, desc);
        //            break;
        //        case Frequencies.Q4H:
        //            SetInd(15, desc);
        //            break;
        //        default:
        //            break;
        //    }

        //}

        private void ShowBuckets(List<gBucket> buckets)
        {
            string s = "";
            foreach (var e in buckets)
            {
                s = "(" + e.bucket + "," + e.code + "," + e.evdt.ToString() + ")";
                Program.VerboseAudit("Assessmt item:=" + s);
            }
        }


        private void Check_15_16_17_18()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("15. Assessment q4h");
            Program.VerboseAudit("16. Assessment q2h");
            Program.VerboseAudit("17. Assessment q1h");
            Program.VerboseAudit("18. Assessment q30min");
            Program.VerboseAudit("---------------");

            exclude_periop_data = true;

            if (_pat.age <= 1.0)
            {
                // if 304987663 has result < 1 kg wt
                // if 3045001120 >= 8 peep val
                string ret = "";
                if (GetLatestResult("", "304987663", "", "", out ret, SearchDepth.SearchSince24Hrs))
                {
                    if (ret.Val() < 1.0)
                    {
                        SetInd(18, "NICU baby <=1y.o. less than 1kg.");
                        SetInd(24, "NICU baby <=1y.o. less than 1kg.");
                        return;
                    }
                }
                ret = "";
                if (GetLatestResult("", "3045001120", "", "", out ret, SearchDepth.SearchSince24Hrs))
                {
                    if (ret.Val() > 8.0)
                    {
                        SetInd(18, "NICU baby <=1y.o. PEEP >= 8.0.");
                        SetInd(24, "NICU baby <=1y.o. PEEP >= 8.0.");
                        return;
                    }
                }
            }

            CountAssessments(18, 40);
            if (_inds[18].is_checked) return;
            CountAssessments(17, 80);
            //if (_inds[17].is_checked) return;
            //CountAssessments(16, 150);
            //if (_inds[16].is_checked) return;
            //CountAssessments(15, 300);

            int highest_assess = 0;
            if (_inds[15].is_checked) highest_assess = 15;
            if (_inds[16].is_checked) highest_assess = 16;
            if (_inds[17].is_checked) highest_assess = 17;

            int assess_ind = 0;
            DateTime classdt = DateTime.MinValue;
            if (Program.patloclist[_pat.loc_idx].special_unit_id < 0)
            {
                assess_ind = GetAssessInd(Program.patloclist[_pat.loc_idx].in_time, out classdt);
                if (assess_ind > highest_assess)
                {
                    SetInd(assess_ind, "Special_location: Setting Assessment Indicator to=" + assess_ind + " from classdt=" + classdt + " to classdt=" + _pat.effective.ToString() + " loc idx =" + _pat.loc_idx + " unit=" + Program.patloclist[_pat.loc_idx].unit_name + " intime=" + Program.patloclist[_pat.loc_idx].in_time.ToString() + " outtime=" + Program.patloclist[_pat.loc_idx].out_time.ToString());
                }
            }
            else if (_pat.loc_idx > 0)
                if (Program.patloclist[_pat.loc_idx - 1].special_unit_id < 0)
                {
                    assess_ind = GetAssessInd(Program.patloclist[_pat.loc_idx - 1].in_time, out classdt);
                    if (assess_ind > highest_assess)
                    {
                        SetInd(assess_ind, "After Special_location: Setting Assessment Indicator to=" + assess_ind + " from classdt=" + classdt + " to classdt=" + _pat.effective.ToString() + " loc idx=" + _pat.loc_idx + " unit=" + Program.patloclist[_pat.loc_idx].unit_name + " intime=" + Program.patloclist[_pat.loc_idx].in_time.ToString() + " outtime=" + Program.patloclist[_pat.loc_idx].out_time.ToString());
                    }
                }

        }

        private int GetAssessInd(DateTime loc_out_time, out DateTime classdt)
        {
            int ind = 0;
            classdt = DateTime.MinValue;
            //get assessment indicator at location out time = loc_out_time
            var db = PFSDBUtility.NewPfsDataContext();        // look for the unit default admission profile
            var query = from ce in db.CLASSIFICATION_EVENTs
                        from ia in db.INDICATOR_ANSWERs
                        where (ce.CLASSIFICATION_EVENT_ID == ia.CLASSIFICATION_EVENT_ID)
                        && (ce.ENCOUNTER_ID == _pat.encounter_id)
                        && (ce.EFFECTIVE_DATETIME_IN <= loc_out_time)
                        && (ia.INDICATOR_NUMBER >= 15 && ia.INDICATOR_NUMBER <= 18)
                        orderby ce.EFFECTIVE_DATETIME_IN descending
                        select new
                        {
                            ia.INDICATOR_NUMBER,
                            ce.CLASSIFICATION_DATETIME
                        };
            if (query.Count() > 0)
            {
                ind = query.First().INDICATOR_NUMBER;
                classdt = query.First().CLASSIFICATION_DATETIME;
            }
            return ind;
        }

        private bool IsRehab()
        {
            switch (_pat.unit_name)
            {
                case "137 - Rehab":
                    return true;
                default:
                    return false;
            }
        }


        //private bool IsICU()
        //{
        //    switch (_pat.unit_name)
        //    {
        //        case "ICU":
        //        case "3BR":
        //        case "CTIC":
        //        case "NIC":
        //        case "PCU":
        //        case "TICU":
        //        case "CCB":
        //        case "RICU":
        //        case "CCUS":
        //            return true;
        //        default:
        //            return false;
        //    }
        //}

        //private bool IsTele()
        //{
        //    switch (_pat.unit_name)
        //    {
        //        case "4EST":
        //        case "2NW":
        //        case "3AE":
        //        case "5MEH":
        //        case "5NW":
        //        case "6MEH":
        //        case "6NW":
        //        case "7MEH":
        //        case "B4W":
        //        case "B5S":
        //        case "B5W":
        //        case "2NOB":
        //        case "CPLX":
        //        case "TELMS":
        //        case "MAN4":
        //            return true;
        //        default:
        //            return false;
        //    }
        //}

        private void CountAssessments(int ind, int bucket_size)
        {

            SetBucketSize(bucket_size);

            if (ind >= 15 && ind <= 17) AnalyzeAssessments15_17(ind, bucket_size);
            if (ind == 18) AnalyzeAssessments18(ind, bucket_size);

        }

        private void AnalyzeAssessments15_17(int ind, int bucket_size)
        {
            string codelist;
            string reslist;
            List<gBucket> buckets;

            string proxy;
            string proxy_codelist;
            //Program.VerboseAudit("bucket size=" + bucket_size + "  _bucket size=" + _bucket_size);

            if (ind == 17) CheckInsulin();

            //
            //VS group
            //
            //Need 3 values together: 
            //1.Pulse in PA - VS - 6 or PA-VS - 7
            //2.Resp in PA - VS - 8
            //3.BP in PA - VS - 9 or PA-VS - 10

            //            '304987655','9990007096285'
            //,'9993041120042','3045001041'
            //,'9990304001499','3045001018','9993040103255'
            //,'304987657','3045001025','3045001064','30454321','304987666','9990304100017','9993049900009'
            //,'30454321','9990000006294','3045001064','3045001051'

            buckets = new List<gBucket>();
            codelist = "304987655,9990007096285";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, "Temperature");

            buckets = new List<gBucket>();
            //--REMOVE TEMP 09/09/19 codelist = "304987655,9990007096285";
            codelist = "9993041120042,3045001041,9990304301280,9993045001043";
            codelist += ",9990304001499,3045001018,9993040103255";
            AddBuckets(buckets, "", codelist, "", "");
            //Program.VerboseAudit("buckets count1=" + buckets.Count());
            //ShowBuckets(buckets);

            bool has_trach = Exists("", "9990007070177,9990007070178,9990007070179", "", "", "", SearchDepth.SearchSince9Hrs);

            if (_pat.age >= 8.0 && !has_trach)
                AddDependentBuckets(buckets, "304987657,3045001025", "", "3045001064,30454321", "", "304987666,3045001041,9990304301280,9993045001043,9990304100017,9993049900009", "");
            else
                AddDependentBuckets(buckets, "304987657,3045001025", "", "3045001064,30454321", "");
            //Program.VerboseAudit("buckets count2=" + buckets.Count());
            //ShowBuckets(buckets);
            reslist = "Non-rebreather mask,Nasal cannula,High flow,Aerosol mask";
            reslist += ",Bag valve device,BiPAP,Blow-by,CPAP,Face tent,Isolette,Oxyhood";
            reslist += ",Oxymask,Partial rebreather mask,Reservoir nasal cannula,Simple mask";
            reslist += ",T-piece,Trach mask,Ventilator,Venturi mask";
            reslist += ",Speaking valve,Incubator,Bag-self inflating,Bag-flow inflating,Helium oxygen,Sub-ambient Oxygen";
            AddDependentBuckets(buckets, "30454321,9990000006294", "", "3045001064", "", "3045001051", reslist);
            //Program.VerboseAudit("buckets count3=" + buckets.Count());
            //ShowBuckets(buckets);
            AnalyzeBuckets(buckets, ind, bucket_size, "VS");
            //ct = CountBuckets(buckets);
            //CheckAssessment(ct, "VS=" + ct);
            //ShowBuckets(buckets);

            //
            //Pulmonary group
            //
            buckets = new List<gBucket>();
            codelist = "3045001051";
            reslist = "Non-rebreather mask,Nasal cannula,High flow,Aerosol mask";
            reslist += ",Bag valve device,BiPAP,Blow-by,CPAP,Face tent,Isolette,Oxyhood";
            reslist += ",Oxymask,Partial rebreather mask,Reservoir nasal cannula,Simple mask";
            reslist += ",T-piece,Trach mask,Ventilator,Venturi mask";
            reslist += ",Speaking valve,Incubator,Bag-self inflating,Bag-flow inflating,Helium oxygen,Sub-ambient Oxygen";
            AddDependentBuckets(buckets, codelist, reslist, "30454321", "", "3045001064", "");

            codelist = "3045001052,3045001053";
            AddDependentBuckets(buckets, codelist, "", "30454321", "", "3045001064", "");
            codelist = "9990000302570";
            reslist = "Agonal,Apnea,Bradypnea,Cheyne-Stokes,Kussmaul";
            reslist += ",Obstructed,Periodic,Regular,Tachypnea";
            AddDependentBuckets(buckets, codelist, reslist, "3045001064", "");
            codelist = "9993040109339";
            reslist = "Regular,Irregular,Shallow,Deep";
            AddDependentBuckets(buckets, codelist, reslist, "3045001064", "");
            codelist = "9993040109337";
            reslist = "Labored,Unlabored";
            AddDependentBuckets(buckets, codelist, reslist, "3045001064", "");

            codelist = "9993040109338";
            reslist = "Abdominal muscle use,Accessory muscle use,Drooling,Gasping";
            reslist += ",Grunting,Head bobbing,Nasal flaring,Pursed lips,Retractions,Tripod position";
            AddDependentBuckets(buckets, codelist, reslist, "3045001064", "");

            codelist = "9990000302580";
            reslist = "Symmetrical,Asymmetrical,Trachea deviates right,Trachea midline";
            reslist += ",Trachea deviates left,Paradoxical,Sunken chest,Pigeon chest";
            reslist += ",Subcutaneous emphysema,No chest expansion,Cylinder shaped";
            reslist += ",Flattened,Right clavicular deformity,Left clavicular deformity";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000302390,9990000302400,9990000302410";
            reslist = "Clear,Diminished,Fine,Coarse,Rales,Rhonchi,Crackles";
            reslist += ",Expiratory wheezes,Inspiratory wheezes,Stridor,Pleural rub";
            reslist += ",Tubular,Absent";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000451120,9990000451080,9990000451040";
            reslist = "Copious,Large,Moderate,Small,None";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000315800";
            reslist = "Bed therapy,Cough assist,CPAP,EzPAP,Flutter valve";
            reslist += ",IPV device,Manual percussion,NT suction,PEP Therapy";
            reslist += ",Percusser,Postural drainage,Vibralung,Vest";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000325950";
            reslist = "Tollerated well,Tollerated fairly well,Tolerated poorly,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991733888883";
            reslist = "CPAP facial,CPAP gap present,CPAP nasal,Incubator oxygen";
            reslist += ",Laryngeal mask airway,Nasal cannula gap present,Nasal cannula high flow";
            reslist += ",Nasal cannula low flow,Nasal trumpet,Oral airway,Oxygen hood";
            reslist += ",Trach uncuffed,Trach cuffed,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991733666660";
            reslist = "Apnea,Bradycardia,Desaturation,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000344210,9990000316380,9990000344220,9990000344230,9990007096450";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "9990000344250";
            reslist = "Blow-by oxygen,Oxygen,Positive pressure ventilation,Self limiting,Suction,Tactile stimulation";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000344280";
            reslist = "Aminophylline,Caffeine,High flow oxygen,Intubated,Medication dose change,Nasal cannula,Nasal CPAP";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000006808,3041733124512,9991733666661";
            AddBuckets(buckets, "", codelist, "", "");

            if (!Exists("", "9993040000639", "", "", "Mask,Tubing,CPAP,BPAP,Patient able to manage equipment on own,Equipment inspected,Waiver signed,Other"))
            {
                codelist = "3045001108";
                reslist = "Bilevel,CPAP,Auto - Bilevel,Auto - CPAP,AVAPS,PCV";
                AddBuckets(buckets, "", codelist, "", "", reslist);

                codelist = "9993040000637";
                reslist = "Nasal pillows,Nasal mask,Nasal Prongs,Nasal pharyngeal";
                reslist += ",Full face mask,Performax,Total face mask,Endotracheal,Tracheostomy";
                AddBuckets(buckets, "", codelist, "", "", reslist);
            }

            codelist = "9991600100035,9991600100039";
            reslist = "Point of care,To lab";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            //   Must also have SPO2  (PA - VS - 11)  and [ RR(PA - VS - 8) or RR(PA - P - 43) ]
            codelist = "3045001117,9993040000635";
            AddDependentBuckets(buckets, codelist, "", "30454321", "", "3045001064,3045001128", "");

            codelist = "9991600100048";
            reslist = "Oral mouthpiece,Mask,Trach,Ventilator,NPPV,Blow-by,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990160238401,9990160238501";
            reslist = "Clear,Diminished,Absent,Crackles,Stridor,Wheeze,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040104500";
            reslist = "Scant,Small,Moderate,Copious,Thin,Thick,Clear,Blood tinged,Tan,White,Yellow";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000400613,9993040102736,9993040102752,9993040106219,9993040106220,9993040106221,9993040106222";
            AddBuckets(buckets, "", codelist, "", "", "");

            codelist = "9993040108077,9993040108078,9993040108079";
            AddBuckets(buckets, "", codelist, "", "", "");

            AnalyzeBuckets(buckets, ind, bucket_size, "Pulmonary");
            //ct = CountBuckets(buckets);
            //CheckAssessment(ct, "Pulmonary=" + ct);
            //ShowBuckets(buckets

            //
            //Cardiovascular group
            //
            buckets = new List<gBucket>();
            codelist = "3045001065";
            reslist = "SR,SB,Sinus rhythm,Sinus bradycardia,Sinus tachycardia,Sinus arrest";
            reslist += ",Sinus arrhythmia,A-paced,V-paced,A-V paced,Atrial paced,Ventricular paced,A-V Sequential paced";
            reslist += ",Agonal,Asystole,A-fib,A-fib w/RVR,Atrial fibrillation";
            reslist += ",Atrial flutter,Heart block,Junctional accelerated,Junctional rhythm";
            reslist += ",Junctional tachycardia,PEA,SVT,Pulseless electrical activity,Supraventricular tachycardia,Ventricular fibrillation,Ventricular tachycardia";
            reslist += ",Torsades,VF,VT,Other,MAT,WAP";
            AddBuckets(buckets, "", codelist, "", "", reslist);
            reslist = EXACT_MATCH_PREFIX + "ST";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "3045001066";
            reslist = "1st degree AVB,2nd degree AVB (Mobitz I, Wenckebach)";
            reslist += ",2nd degree AVB (Mobitz II),3rd degree AVB,Bundle branch block,Idioventricular";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "3045001067";
            reslist = "PVCs,Premature ventricular contractions,Unifocal PVCs,Multifocal PVCs";
            reslist += ",Couplet PVCs,Triplet PVCs,Premature atrial contractions,PACs,Aberrent conduction";
            reslist += ",Ectopic beats,Fusion beats,Junctional escape beats,Non-conducted PACs";
            reslist += ",Paroxysmal atrial tachycardia,Paroxysmal supraventricular tachycardia,PSVT,PJCS";
            reslist += ",Premature junctional contractions,Ventricular escape beats";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040108650,9993040108651,9993040108652,9993040108653";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "9993040100446";
            reslist = "S1,S2,S3,S4,Click,Device,Distant,Friction rub";
            reslist += ",Gallop,Holosystolic murmur,Mechanical valve click,Muffled";
            reslist += ",Murmur,Pericardial rub,No adventitious heart sounds";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040101320";
            //reslist = "Off,A paced,V paced,A/V paced,AAI,AAI-DDD (MVP),AOO,DVI,DOO,DDD";
            //reslist += ",VVI,VOO,VDD,AAR,AAR-DDDR (MVP),AOOR,DVR,DOOR,DOI,DDDR,DDR,VVR";
            //reslist += ",VOOR,VDDR";
            reslist = "";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040101316,9993040101317";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "9993040001044";
            reslist = "Red,Pink,Pale,Dusky,Purple,Mottled,Ecchymotic,Soft,Firm";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040001045";
            reslist = "Greater than 3 seconds,Less than or equal to 3 seconds,Brisk,Sluggish";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040001048,9993040001049";
            reslist = "Verified,Absent";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000303200,9990000303210,9990000303220,3045001045,9990000303270,9990000303280";
            codelist += ",9990000303320,9990000303330,3045001012,3045001014,9990000303390";
            codelist += ",9990000303400,3045001013,3045001015";
            reslist = "0,+1,+2,+3,+4,Doppler";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "3045001004,3045001002,3045001003,3045001001";
            reslist = "Less than/equal to 2 seconds,Greater than/equal to 3 seconds,No return";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000302900,9993045006597,9990000302920,9993045006598,9993040101373";
            codelist += ",9993040101374,9993040101375,9993040101376,3040000000022,99930400000027";
            codelist += ",3040000000028,9993040000062,99930400000056,3040000000050,9993040000044";
            codelist += ",9993040000035,9993040000045,9993040000049,9993047096403,9993040000050";
            codelist += ",9993040000064,9993040000065,3045001056,3045001055";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "9991733888889";
            reslist = "Acrocyanosis,Capillary refill no return,Capillary refill sluggish (>2 seconds)";
            reslist += ",Circumoral,Cyanosis" + CHAR_COMMA + " centralized,Cyanosis" + CHAR_COMMA + " peripheral";
            reslist += ",Generalized,Localized,Mottled,Pale,Ruddy";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000302650";
            reslist = "Absent,Murmur,Murmer-intermittent,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991733888890";
            reslist = "0,+3,+1,Unequal,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991733999991";
            reslist = "Moderate,Non-pitting,Severe,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040100223";
            reslist = "Circumocular,Circumoral,Nailbed,Acrocyanosis,Facial";
            reslist += ",Generalized,Oral mucosa,Underlying,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);


            codelist = "3045001000";
            reslist = "Normal (less than/equal to 2 seconds" + CHAR_COMMA + " all extremities)";
            reslist += ",Sluggish (greater than/equal to 3 seconds" + CHAR_COMMA + " all extremities)";
            reslist += ",No return,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000303180,9990000303240,9990000303300,9990000303370";
            reslist = "Acrocyanosis,Appropriate for ethnicity,Ashen,Black,Bronze";
            reslist += ",Dusky,Ecchymosis,Flushed,Gray,Jaundiced";
            AddBuckets(buckets, "", codelist, "", "", reslist);
            codelist = "9990007061360";
            reslist = "Capillary refill less than 3 sec,Cool fingers";
            reslist += ",Capillary refill greater than 3 sec,Dusky fingers";
            reslist += ",Numbness to fingers,Pale fingers";
            reslist += ",Tingling to fingers,Weakness to fingers";
            AddBuckets(buckets, "", codelist, "", "", reslist);
            codelist = "9993040103299,9993040103300";
            reslist = "Present,Absent";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040101321,9993040101322,9993040101323,9993040101324";
            reslist = "0,+1,+2,+3,+4,Doppler";
            AddBuckets(buckets, "", codelist, "", "", reslist);
            codelist = "99930434002,9993043034001";
            reslist = "0,+1,+2,+3,+4,Doppler,Sheath In";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040107859,9993040107862";
            AddBuckets(buckets, "", codelist, "", "", "");

            proxy = "9993040212345";
            proxy_codelist = "9993040100223,3045001000,3045001004,3045001002,3045001003,3045001001,9990000303200,";
            proxy_codelist += "9990000303210,9990000303220,3045001045,9993040101321,9993040101322,9993040101323,";
            proxy_codelist += "9993040101324,99930434002,9993043034001,9990000303270,9990000303280,9990000303320,";
            proxy_codelist += "9990000303330,3045001012,3045001014,9990000303390,9990000303400,3045001013,";
            proxy_codelist += "3045001015,9991733999991,9993040107859,9993040107862";
            if (CheckProxy(proxy, proxy_codelist, 8, "Peripheral vascular"))
                AddBuckets(buckets, "", proxy, "", "Proxy", "");

            proxy = "9993040312345";
            proxy_codelist = "9993040001044,9993040108605,9993040108606,9993040108607,9993040001045,";
            proxy_codelist += "9993040108611,9993040108612,9993040108613,9993040001048,9993040001049";
            if (CheckProxy(proxy, proxy_codelist, 8, "Flap"))
                AddBuckets(buckets, "", proxy, "", "Proxy", "");


            AnalyzeBuckets(buckets, ind, bucket_size, "Cardiovascular");
            //ct = CountBuckets(buckets);
            //CheckAssessment(ct, "Cardiovascular=" + ct);
            //ShowBuckets(buckets);

            //
            //Neurological group
            //
            buckets = new List<gBucket>();
            codelist = "9993040101409";
            reslist = "0/4,1/4,2/4,3/4,4/4";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "3045001007,3045001039,9993040006316,9990304006317,9993040006318";
            codelist += ",9993040006319,9993040006320,9990000301930,9990000301910";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "9990000301920,9990000301900";
            reslist = "Brisk,Sluggish,Nonreactive";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040101162,9993040101163";
            reslist = "Absent,Weak,Moderate,Strong,Contracture";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000002280,9990000002279,3045001017";
            reslist = "Present,Absent";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "3045001016,9993040101166,9993040101167";
            reslist = "Absent,Weak,Moderate,Strong,Contracture";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000301980,9990000301940,9990000302000,9990000301960";
            reslist = STARTS_WITH + "0,-1,-2,-3,-4,P,SP,NP,Stim,Pain,DC,DB";
            AddBuckets(buckets, "", codelist, "", "", reslist);
            reslist = "Responds to commands,Normal extension,Normal flexion,Tremors";
            reslist += ",Flaccid,Abnormal extension,Abnormal flexion";
            reslist += ",Movement to painful stimulus,No movement to painful stimulus";
            reslist += ",Non-purposeful movement,No tremor,Spastic";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000301990,9990000301950,9990000302010,9990000301970";
            reslist = "Decreased,No sensation,Numbness,Pain,Tingling,Full sensation";
            reslist += ",No numbness,No pain,No tingling";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040102775,9993040102776,9993040102777,9993040102778";
            reslist = "Normal power,Cannot overcome resistance,Overcomes gravity";
            reslist += ",Can overcome resistance,Flicker of muscle,None";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000302190,9990000302180";
            reslist = "C1,C2,C3,C4,C5,C6,C7,C8,T1,T2,T3,T4,T5,T6,T7,T8,T9,T10,T11,T12";
            reslist += ",L1,L2,L3,L4,L5,S1,S2,S3,S4,S5";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "3045001023,9993040001207";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "3045001080,3045001081,9990000398011";
            // Requires: RS (PA-N-38) and SpO2(PA - VS - 11) and Respirations (PA - VS - 8)
            //Requires: MASS (PA-N-39)  and SpO2(PA - VS - 11) and Respirations (PA - VS - 8) 
            AddDependentBuckets(buckets, "3045001080", "", "3045001081", "", "30454321", "", "3045001064", "");



            codelist = "3045001079";
            string reslistA = "-5,-4,-3,-2,-1,0,+1,+2,+3,+4"; //Any number between -5 and + 4
            codelist = "9993040104675";
            string reslistB = "S,1,2,3,4";
            // Requires: RASS (PA-N-40) and SpO2(PA - VS - 11) and Respirations (PA - VS - 8) 
            // Requires: POSS (PA-N-41) and SpO2(PA - VS - 11) and Respirations(PA - VS - 8) 
            AddDependentBuckets(buckets, "3045001079", reslistA, "9993040104675", reslistB, "30454321", "", "3045001064", "");

            codelist = "9990000398011";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "9993040001216,3045001011";
            reslist = "Positive,Negative";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000450430,9990000450420";
            reslist = "Present,Weak,Absent";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000450990,9990000450410";
            reslist = "Intact,Impaired,Absent";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040103168,9993040103169";
            reslist = "Absent,Present";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040109102,9990000450470";
            reslist = "Absent,Present,Weak";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000450480";
            reslist = "Absent,Present,Weak,Strong,Coordinated,Uncoordinated,Gag present,Gag absent";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "3045001047";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "9993040001091";
            reslist = "Blindness - right,Blindness - left,Blurred vision";
            reslist += ",Visual field cut- right side,Visual field cut- right upper";
            reslist += ",Visual field cut- right lower,Visual field cut- left side";
            reslist += ",Visual field cut- left upper,Visual field cut- left lower";
            reslist += ",Diplopia- Bilateral,Diplopia - right";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000002216";
            reslist = "Strabismus - right,Strabismus - left,Droopy eyelid - right";
            reslist += ",Droopy eyelid - left,Double vision - right,Double vision - left,Unable to assess";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000002217";
            reslist = "Double vision - right,Double vision - left";
            reslist += ",Unable to look downward and inward - right,Unable to look downward and inward - left";
            reslist += ",Unable to assess";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000002218";
            reslist = "Unable to chew - right,Unable to chew - left";
            reslist += ",Unable to clench - right,Unable to clench - left";
            reslist += ",Absence of sensation - right,Absence of sensation - left";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000002219";
            reslist = "Medial strabismus -  right,Medial strabismus -  left";
            reslist += ",Unable to look laterally - right,Unable to look laterally - left";
            reslist += ",Double vision - right,Double vision - left";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000002220";
            reslist = "Facial paralysis - right,Facial paralysis - left,Loss of taste - right,Loss of taste - left";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000002221";
            reslist = "Unable to hear - right,Unable to hear - left";
            reslist += ",Ringing in ear - right,Ringing in ear - left";
            reslist += ",Involuntary eye movement - right,Involuntary eye movement - left";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000002222";
            reslist = "Altered gag reflex";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000002223";
            reslist = "Weakness in turning head - right,Weakness in turning head - left";
            reslist += ",Unable to shrug - right,Unable to shrug - left";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000002224";
            reslist = "Deviation of tongue - right,Deviation of tongue - left";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040001057";
            AddBuckets(buckets, "", codelist, "", "");
            codelist = "9990000302300";
            reslist = "Clear,Cloudy,Serous,Sanguineous,Serosanguineous";
            reslist += ",Purulent,Cherry,Straw,Yellow";
            AddBuckets(buckets, "", codelist, "", "");
            codelist = "9990000002230,9990000304510,9993040109138";
            AddBuckets(buckets, "", codelist, "", "");


            codelist = "9991600100259";
            reslist = "No untoward effects noted,Use of reversal agent(s)";
            reslist += ",Hypoxemia < 90% for > 1 min,Hypotension of bradycardia requiring intervention";
            reslist += ",Respiratory failure requiring intervention,Cardiac arrest or death";
            reslist += ",Sedation recovery time > 60 min,Unplanned admission or higher level of care";
            reslist += ",Respiratory distress,Unanticipated need for anesthesia involvement";
            reslist += ",Inability to complete procedure,No responsible adult for discharge escort";
            reslist += ",Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "99910701000111";
            AddBuckets(buckets, "", codelist, "", "");


            // Must also contain and SpO2(PA - VS - 11) and Respirations (PA - VS - 8) "
            AddDependentBuckets(buckets, "9991070011101", "", "30454321", "", "3045001064", "");


            codelist = "9990000450560";
            reslist = "Aggression,Aura,Behavior pause,Bowel incontinence,Deja vu";
            reslist += ",Fearful,Giggles,Nausea,Oral Trauma,Smirks,Tremors,Urine incontinence,Vocalize";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000450570";
            reslist = "Eyes right,Eyes left,Head right,Head left,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000450580,9993040108685,9993040108686";
            reslist = "Head,Face,Eye,Hand,Arm,Leg,Foot,Jerking,Stiffening";
            reslist += ",Staring,Tonic-clonic Motion,Twitching,Drop";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000450590";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "9990000450600";
            reslist = "Somnolence,Aphasic,RUE paresis,LUE paresis,RLE paresis,LLE paresis";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000450620";
            reslist = "Aware of seizure,Word given,Word recalled,Word not recalled";
            reslist += ",Normal speech,Abnormal speech,Unable to respond";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040101196";
            reslist = "Generalized,Right,Left,Hand,Leg,Face,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040101198";
            reslist = "2,3,4,5,6,7,8,9,10"; // num > 1
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040100079";
            reslist = "Absent,Arching back or neck,Bicycling,Clonic jerking";
            reslist += ",Extension is greater than flexion,Jerky,Jittery/tremors";
            reslist += ",Lip smacking,Movements cease with containment,Movements continue despite containment";
            reslist += ",Rowing,Seizure activity,Tonic extension,Tonic flexion,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991733999990";
            reslist = "Bicycling,Eye deviation,Lip smacking,Movement ceases with containment";
            reslist += ",Movements continue despite containment,Tongue thrusting,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040102780";
            reslist = "Abnormal reflex,Extensionx,Frantic movement,Inconsolable";
            reslist += ",Lethargic,Medically paralyzed,Sedated,other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991140100006";
            reslist = "Quiet alert,Sleeping,Active alert,Lusty cry,Drowsy,Active with stimulation";
            reslist += ",Hoarse cry,Irritable,Jittery,Lethargic,Shrill cry";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040100076";
            reslist = "Hypertonic generalized,Hypertonic localized,Hypotonic generalized,Hypotonic localized,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000451750";
            reslist = "Absent,Present,Weak,Brisk,Clonus,Clonus Sustained";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000450440";
            reslist = "Absent,Asymmetric,Symmetric,UTA";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000450450";
            reslist = "Absent,Present,Weak,UTA";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000450460";
            reslist = "Absent,Present,Clonus,Hyperreflexive,Hyporeflexive,Weak,UTA";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040109103";
            reslist = "Absent,Present,Weak,Unable to assess";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040109104";
            reslist = "Absent,Asymmetrical,Symmetrical,Unable to assess";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991140100013,9991140100014,9993040109135";
            AddBuckets(buckets, "", codelist, "", "", "");


            proxy = "9993040112345";
            proxy_codelist = "3045001046,9990000301870,9990000301880,9990000301930,9990000301920,9990000301910,";
            proxy_codelist += "9990000301900,9993040101162,9993040101163,9990000002280,9990000002279,3045001017,";
            proxy_codelist += "3045001016,9993040101166,9993040101167,9990000301980,9990000301990,9993040102775,";
            proxy_codelist += "9990000301940,9990000301950,9993040102776,9990000302000,9990000302010,9993040102777,";
            proxy_codelist += "9990000301960,9990000301970,9993040102778,9990000302190,9990000302180";
            if (CheckProxy(proxy, proxy_codelist, 8, "Neurological"))
                AddBuckets(buckets, "", proxy, "", "Proxy", "");


            AnalyzeBuckets(buckets, ind, bucket_size, "Neurological");
            //ct = CountBuckets(buckets);
            //CheckAssessment(ct, "Neurologic=" + ct);
            //ShowBuckets(buckets);

            //
            //Medication Response group
            //
            //codelist = "9990000344170,3040007191,304000344160,30400012412,304000344150,9991733000016,9993040100610,DIPS,ORD06";
            buckets = new List<gBucket>();
            codelist = "304000344150,9991733000016,9993040100610,DIPS,ROUI";
            AddBuckets(buckets, "", codelist, "", "", "");

            codelist = "BDFPH,UPHB";
            AddBuckets(buckets, "", codelist, "", "", "");

            AddMedBuckets(buckets);
            //default q4 if present in last 13 hours, or higher by freq
            //MED____ ^ _____; ; ; 7
            //MED____ ^ _____; ; ; 33; ; ; _; ; ; SC
            //MED____^_____;;;1;;;
            //MED____ ^ _____; ; ; 2; ;:
            //MED____ ^ _____; ; ; 6; ;:"
            //   with:  ";;;Given ;;;Given/Down   ;;;Given/Other"
            AnalyzeBuckets(buckets, ind, bucket_size, "Medication Response");

            //
            //Diabetic Mgmt
            //
            buckets = new List<gBucket>();
            codelist = "9993040000345";
            AddBuckets(buckets, "", codelist, "", "", "fruit juice,iv/medication");
            codelist = "30400012412,9990000344170,3040007191,304000344160,ORD06";
            AddBuckets(buckets, "", codelist, "", "", "");
            AddMedBucketsDrugClass33(buckets);
            AnalyzeBuckets(buckets, ind, bucket_size, "Diabetic Mgmt");

            //
            //Fluid Status / GU/GI
            //
            buckets = new List<gBucket>();
            codelist = "9990007096390,9990000305290,3045001090,3045001089,3040011360,9993040108593";
            AddBuckets(buckets, "", codelist, "", "", "");
            codelist = "9990000370180";
            AddBuckets(buckets, "", codelist, "", "", "Amber,Clear,Cloudy,Dark,Fibrin,Light,Pink,Red,Yellow");
            codelist = "MED7318";
            AddBuckets(buckets, "", codelist, "newbag,ratechange", "", "");
            codelist = "9990000006298";
            AddBuckets(buckets, "", codelist, "", "", "Tea,Rusty,Peach,Cherry,Pink,Ketchup");
            AnalyzeBuckets(buckets, ind, bucket_size, "Fluid Status/GU/GI");

            //
            //Pain
            //
            buckets = new List<gBucket>();
            string code1 = "9990000301120";
            string reslist1 = "Heat applied,Cold applied,Compression,Aromatherapy,Cold pack,Cutaneous stimulation,Guided imagery,Herbal therapy,Warm moist pack,Warm pack";
            string code2 = "304987659,9990007090220,9990007090190,9993040104228,3045001058";
            code2 += ",9993040101151,9993040101157,9993040101160,9993040104125,9990007090780";
            AddDependentBuckets(buckets, code1, reslist1, code2, "");
            AnalyzeBuckets(buckets, ind, bucket_size, "Pain");

            //
            //OB assessment group
            //
            buckets = new List<gBucket>();
            codelist = "9991025006827";
            reslist = "Denies,Blurred,Floaters,Flashes,Decreased visual field,Hx. of visual disturbances";
            reslist += ",Unsure,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991025006826";
            reslist = "Denies,Present,Unsure";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991025006828";
            reslist = "Denies,Aching,Burning,Constant,Intermittent,Sharp,Unsure";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991025006829";
            reslist = "Denies,Present";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991025006830";
            reslist = "Denies,Nausea only,Vomiting,Dry Heaves";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000012301,9990000012302,9990000012303,9990000012305";
            reslist = "absent,diminished,normal,brisk,brisk/hyperactive,sustained";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000012306,9990000012304";
            reslist = "Absent,1 beat,2 beats,3 beats,4 beats,Sustained";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "1028000004,1028000001,9990102521012,1028000003,9991020100645,9991020100655";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "1028000007,1028000008,1028000009,9991020100648,9991020100657";
            reslist = "Normal,Bradycardia,Tachycardia,Indeterminate";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "1028000010,1028000011,1028000012";
            reslist = "Minimal,Moderate,Marked,Absent";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "1028000013,1028000014,1028000015";
            reslist = "15x15,10x10,Absent";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "1028000016,1028000017,1028000018";
            reslist = "None,Early,Variable,Late,Prolonged ,Intermittent,Recurrent";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991025051116,9991025111616";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "1028000022,1028000023,1028000024,9991020100652,9991020100661";
            reslist = "Category I,Category II,Category III";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000012331";
            reslist = "Reactive,Non-reactive";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000012332";
            reslist = "Reassuring,Non - reassuring";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "1028000040";
            reslist = "Intact,Spontaneous,AROM,PROM,PPROM,Bulging";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "128000043";
            reslist = "Clear,Meconium,Bloody,Purulent";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "128000045";
            reslist = "Scant,Small,Moderate,Large";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "1028000046";
            reslist = "Closed,Fingertip,1,2,3,4,5,6,7,8,9,Lip/rim,10";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991020100568";
            reslist = "Vaginal pack,Uterine balloon,Uterotonic agent given";
            reslist += ",Oxygen on,Legs elevated,MD notified,Bimanual exam per MD,Labs ordered";
            AddBuckets(buckets, "", codelist, "", "", reslist);
            codelist = "9991020100569";
            reslist = "Placed,Present,Removed";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000012127";
            reslist = "Firm,Firm with massage,Boggy";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000012128";
            reslist = "Midline,Right,Left";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000012309,9991025012136,9991025012137,9991025012139,9991025012144";
            AddBuckets(buckets, "", codelist, "", "", "");

            codelist = "9993040102317,9993040102318,9993040102319,9993040102320";
            AddBuckets(buckets, "", codelist, "", "", "");

            AnalyzeBuckets(buckets, ind, bucket_size, "OB");


            if (ind == 17)
            {
                SetIndIfResultContains(17, "", "331197", "", "", "");

                codelist = "9990008100010";
                reslist = "Continuous veno-venous hemofiltration,Continuous veno-venous hemodialysis";
                reslist += ",Continuous veno - venous hemodiafiltration,Slow continuous ultrafiltration";
                reslist += ",CVVH,CVVHD,CVVHDF,SCUF,Other";
                SetIndIfResultContains(17, "", codelist, "", "", reslist);

                codelist = "9990008100020";
                reslist = "Initiated,Continuous,Restarted,Off / System charge";
                reslist += ",Off / Recirculating,Off / Blood returned,Off / No blood returned";
                SetIndIfResultContains(17, "", codelist, "", "", reslist);
            }

            if (Exists("", "3045001047", "", "", "", SearchDepth.SearchSince24Hrs))
            {
                buckets = new List<gBucket>();
                codelist = "3045001023,9993040001207";
                AddBuckets(buckets, "", codelist, "", "", "");
                AnalyzeBuckets(buckets, ind, bucket_size, "NIHSS");
            }

            buckets = new List<gBucket>();
            codelist = "9993040108530,3045001091";
            AddBuckets(buckets, "", codelist, "", "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, "Drains");

        }

       private bool CheckProxy(string proxycode, string codelist, int withinhrsbeforeproxy, string pname)
        { // this only makes the Proxy eligible or not.  If eligible, then its FIELD_NAME will be "Proxy".
            // And then add this proxycode into the AddBuckets looking for FieldName=Proxy.
            bool realpresent = false;

            var query = StartNewQuery();
            query = AndItemFilter(query, "", proxycode, "", "", "");
            int proxyct = query.Count();

            if (proxyct == 0)
                return false;

            foreach (var p in query)
            {
                if (RealAssessExists(p.EVENT_DATETIME, codelist, withinhrsbeforeproxy))
                {
                    foreach (var item in _chart_items_since25hrs)
                    {
                        if (item.CODE == p.CODE && item.EVENT_DATETIME == p.EVENT_DATETIME)
                        {
                            item.FIELD_NAME = "Proxy";
                            Program.VerboseAudit("Valid proxy found: " + pname + " at: " + item.EVENT_DATETIME);
                        }
                    }

                }
            }

            return realpresent;
        }

        private bool RealAssessExists(DateTime evdt, string codelist, int lookbackhrs)
        { // Determine if there are any codelist items after evdt-lookbackhrs

            var query = StartNewQuery();
            query = AndItemFilter(query, "", codelist, "", "", "");
            query = query.Where(e => e.EVENT_DATETIME <= evdt);
            query = query.Where(e => e.EVENT_DATETIME >= evdt.AddHours(-lookbackhrs));
            int ct = query.Count();
            return (ct > 0);
        }

        private void AnalyzeAssessments18(int ind, int bucket_size)
        {
            int ct;
            string codelist;
            string reslist = "";
            string res = "";
            List<gBucket> buckets;
            bool paTM6 = false;

            int ivpushct = CheckIVPush();
            if (ivpushct > 0)
            {
                //Adenosine
                //Etomidate
                //Hydralazine
                //Metoprolol
                //Midazolam
                //Norepinephrine
                //Phenylephrine
                //Rocuronium
                if (_pat.age < 9.0)
                    SetInd(18, "IV Push Med found for patient age 8 or younger.");
                //....more mappings below using ivpushct....
            }


            bool mr5077 = (CheckMR5077() > 0);
            if (mr5077)
            {
                if (_pat.age < 9.0) //age 8 or less
                    SetInd(18, "MED RATE MR-50 to MR-80 with age 8 or less=" + _pat.age);
            }

            if (mr5077 || ivpushct > 0)
            {
                buckets = new List<gBucket>();
                string code1 = "304987657,3045001025";
                string code2 = "3045001064,3045001128,30454321";
                string code3 = "304987666,9990304100017,3045001041,9990304301280,9993045001043,9993049900009";
                AddDependentBuckets(buckets, code1, "", code2, "", code3, "");
                string foundstr = "";
                if (mr5077) foundstr += "FOUND: MedRate MR-50 to MR-80  ";
                if (ivpushct > 0) foundstr += "FOUND: IVPushMed";
                AnalyzeBuckets(buckets, ind, bucket_size, foundstr);
            }

            if (SetIndIfResultBetween(18, "", "3045001087", "", "", 30, 999))
                SetInd(24, "CRRT fluid removed >= 30.");

            if (CheckMR1014() > 0)
            {
                //Atracurium,Cisatracurium,Pancuronium,Rocuronium,Vecuronium
                SetInd(18, "MR-10 - MR-14 found with rateverify");
                SetInd(24, "MR-10 - MR-14 found with rateverify");
            }


            codelist = "3045001036,3045001034,3040000000050,3040000000043";
            SetIndIfResultContains(18, "", codelist, "", "", "");
            SetIndIfResultContains(24, "", codelist, "", "", "");

            codelist = "3045001038";
            SetIndIfResultContains(18, "", codelist, "", "", "1:1,1:2,1:3");
            SetIndIfResultContains(24, "", codelist, "", "", "1:1,1:2,1:3");

            ////Technology Management
            //buckets = new List<gBucket>();
            //codelist = "3045001036,3045001034";
            //reslist = "";
            //AddBuckets(buckets, "", codelist, "", "", reslist);
            //codelist = "3045001038";
            //reslist = "1:1,1:2,1:3";
            //AddBuckets(buckets, "", codelist, "", "", reslist);
            //AnalyzeBuckets(buckets, ind, bucket_size, "Technology Mgt");

            //If number value between PA-TM-6 and PA-TM-7 are greater than 30 different, then select assessment q30 minutes
            int val1 = -99;
            int val2 = -99;
            int diff = 0;
            if (GetLatestResult("", "9993041001004", "", "", out res, SearchDepth.SearchDefault))
            {
                if (res.Left(1).IsNumeric())
                {
                    val1 = (int)res.Val();
                }
            }
            if (GetLatestResult("", "9993041001003", "", "", out res, SearchDepth.SearchDefault))
            {
                if (res.Left(1).IsNumeric())
                {
                    val2 = (int)res.Val();
                }
            }
            if (val1 != -99 || val2 != -99)
            {
                paTM6 = true;
                SetInd(18, "CRRT: " + val1.ToString() + "/" + val2.ToString());
            }
            if (val1 != -99 && val2 != -99)
            {
                diff = Math.Abs(val1 - val2);
                if (diff >= 50)
                {
                    if (diff >= 50)
                        SetInd(24, "CRRT rate difference >=50: " + diff.ToString() + " : " + val1.ToString() + "/" + val2.ToString());
                }
            }

            ct = CheckMR20to77();
            if (ct >= 3)
            {
                SetInd(18, "MR-20 - MR-77 found count = " + ct);
                SetInd(24, "MR-20 - MR-77 found count = " + ct);
            }
            //codelist = "331222,331232,331227,331237,331036,3040101288,331202," + mr5077list;
            //var query1 = StartNewQuery();
            //query1 = AndItemFilter(query1, "", codelist, "", "", "");
            //query1 = query1.OrderBy(e => e.CODE).Distinct();
            //int count = query1.Count();
            //string s = "";
            //if (count >= 3)
            //{
            //    ct = 1;
            //    foreach (var item in query1)
            //    {
            //        if (ct <= 3)
            //        {
            //            s = s + "[" + item.CODE + item.EVENT_DATETIME.ToString() + "]";
            //            ct++;
            //        }
            //    }
            //    SetInd(18, "3 MR FOUND:" + s);
            //}

            if (mr5077)
            {
                //Program.VerboseAudit("Found at least one of MR-50 to MR-80");

                codelist = "3045001036,3045001034,3045001038";
                if (SetIndIfResultContains(18, "", codelist, "", "", reslist))

                    codelist = "9990008100010";
                reslist = "Continuous veno-venous hemofiltration,Continuous veno-venous hemodialysis";
                reslist += ",Continuous veno - venous hemodiafiltration,Slow continuous ultrafiltration";
                reslist += ",CVVH,CVVHD,CVVHDF,SCUF,Other";
                if (SetIndIfResultContains(18, "", codelist, "", "", reslist))
                    SetInd(24, "9990008100010");

                codelist = "9990008100020";
                reslist = "Initiated,Continuous,Restarted,Off / System charge";
                reslist += ",Off / Recirculating,Off / Blood returned,Off / No blood returned";
                if (SetIndIfResultContains(18, "", codelist, "", "", reslist))
                    SetInd(24, "9990008100020");
            }

            //if (paTM6)
            //{
            //    SetIndIfResultContains(18, "", "9993041001004", "", "", "");
            //    SetIndIfResultContains(18, "", "9993041001003", "", "", "");
            //}

            codelist = "9993040001002,9993040001003,9993040101000,9993040001093";
            SetIndIfResultContains(18, "", codelist, "", "", "");


            buckets = new List<gBucket>();
            codelist = "3045001056,3045001055";
            AddBuckets(buckets, "", codelist, "", "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, "P-12 PA-CV-59 or P-13 PA-CV-60");


            bool pap29 = false;
            bool pap30 = false;
            codelist = "3045001108";
            reslist = "Bilevel,CPAP,Auto - Bilevel,Auto - CPAP,AVAPS,PCV";
            if (Exists("", codelist, "", "", reslist))
            {
                pap29 = true;
                //Program.VerboseAudit("P-14 / PA-P-29");
            }
            codelist = "9993040000637";
            reslist = "Nasal pillows,Nasal mask,Nasal Prongs,Nasal pharyngeal,Full face mask";
            reslist += ",Performax,Total face mask,Endotracheal,Tracheostomy";
            if (Exists("", codelist, "", "", reslist))
            {
                pap30 = true;
                //Program.VerboseAudit("P-15 / PA-P-30 / U-11");
            }

            // "Fi02 number >60  (P-19/PA-P-3) and RR  number < 10 or > 28(PA-VS-8 or P-26 / PA-P-43)"
            int fio2 = 0;
            int pavs8 = 12; //choose something between 10 and 28
            int p26 = 12; //choose something between 10 and 28
            if (GetResult("", "3045001053", "", "", out res))
            {
                if (res.Left(1).IsNumeric())
                {
                    fio2 = (int)res.Val();
                    //Program.VerboseAudit("FiO2 = " + fio2);
                }
            }
            if (GetResult("", "3045001064", "", "", out res))
            {
                if (res.Left(1).IsNumeric())
                {
                    pavs8 = (int)res.Val();
                    //Program.VerboseAudit("PA-VS-8 = " + pavs8);
                }
            }
            if (GetResult("", "3045001128", "", "", out res))
            {
                if (res.Left(1).IsNumeric())
                {
                    p26 = (int)res.Val();
                    //Program.VerboseAudit("P-26 = " + p26);
                }
            }

            buckets = new List<gBucket>();
            codelist = "30454321"; //spo2
            AddBuckets(buckets, "", codelist, "", "", "");
            bool spo2 = AnalyzeBuckets(buckets, ind, bucket_size, "SpO2 (PA-VS-11) q30", false);

            buckets = new List<gBucket>();
            codelist = "3045001064,3045001128"; //resp rate
            AddBuckets(buckets, "", codelist, "", "", "");
            bool resprt = AnalyzeBuckets(buckets, ind, bucket_size, "Resp rate (PA-VS-8 or P-26/PA-P-43) q30", false);

            reslist = "Mask,Tubing,CPAP,BPAP,Patient able to manage equipment on own,Equipment inspected (per site policy),Waiver signed (per site policy),Other";
            bool h23 = Exists("", "9993040000639", "", "", reslist);
            if (!h23)
                if (pap29 || pap30)
                    if (spo2 && resprt)
                    {
                        if (fio2 >= 60 || (pavs8 <= 10 || pavs8 >= 28 || p26 <= 10 || p26 >= 28))
                            SetInd(18, "PA-P-29/30 or (FiO2 -OR- PA-VS-8/P-26 <10 or >28 PLUS SpO2 q30 and RR q30");
                        if (fio2 >= 60 && ((pavs8 <= 10 || pavs8 >= 28) || (p26 <= 10 || p26 >= 28)))
                            SetInd(24, "PA-P-29/30 and (FiO2 -OR- PA-VS-8/P-26 <10 or >28 PLUS SpO2 q30 and RR q30");
                    }

            //Must also have  FIO2 > 60(P - 19)
            //Peep > 10(P - 22)
            //Total RR > 35(PA - VS - 8 or P - 26 / PA - P - 43)
            //Peak > 25(P - 27)"
            int peep = 0;
            if (GetResult("", "3045001120", "", "", out res))
            {
                if (res.Left(1).IsNumeric())
                {
                    peep = (int)res.Val();
                    //Program.VerboseAudit("P-22 PEEP = " + peep);
                }
            }

            SetIndIfResultDoesNotContain(18, "", "9993040011344", "", "", "None");
            SetIndIfResultContains(18, "", "9993040102551", "", "", "");
            SetIndIfResultContains(18, "", "9993040102552", "", "", "");

            int peak = 0;
            if (GetResult("", "3045001119", "", "", out res))
            {
                if (res.Left(1).IsNumeric())
                {
                    peak = (int)res.Val();
                    //Program.VerboseAudit("P-27 PEAK = " + peak);
                }
            }

            if (_pat.age < 4.0)
                SetIndIfResultDoesNotContain(18, "", "3045001117", "", "", "CPAP,Hi Flow O2");
            int pap18_prereqs = (fio2 >= 60 ? 1 : 0) + (peep >= 10 ? 1 : 0) + ((pavs8 >= 35 || p26 >= 35) ? 1 : 0) + (peak > 25 ? 1 : 0);
            //            if (fio2 >= 60 && peep >= 10 && (pavs8 >= 35 || p26 >= 35) && peak > 25)
            if (pap18_prereqs >= 2)
            {
                if (_pat.age >= 4.0)
                    SetIndIfResultDoesNotContain(18, "", "3045001117", "", "", "CPAP,Hi Flow O2");
                reslist = "PACU Ventilator Rows,Trilogy Ventilator Rows,HFO Ventilator Rows";
                reslist += ",BPAP/CPAP Rows,Abbreviated Settings,NAVA Rows,HFJV Rows";
                SetIndIfResultContains(18, "", "9993040000635", "", "", reslist);

            }
            string found_what;
            bool r1 = ExistsResultDoesNotContain("", "3045001117", "", "", "CPAP,Hi Flow O2",SearchDepth.SearchDefault,true,out found_what);
            reslist = "PACU Ventilator Rows,Trilogy Ventilator Rows,HFO Ventilator Rows";
            reslist += ",BPAP/CPAP Rows,Abbreviated Settings,NAVA Rows,HFJV Rows";
            bool r2 = Exists("", "9993040000635", "", "", reslist);
            if (r1 || r2)
                SetIndIfResultContains(18, "", "9990000400604", "", "", "Continuous lateral rotation");

            SetIndIfResultContains(18, "", "9991010010010", "", "", "Reposition,Suction,Jaw thrust,Chin lift,Foreign object removal");
            //reslist = "Endotracheal tube,Esophageal - tracheal tube,Laryngeal mask airway,LMA,Nasopharyngeal airway,NPA,Oropharyngeal airway,OPA,Tracheostomy,Other";
            reslist = "";
            SetIndIfResultContains(18, "", "9991600100681", "", "", reslist);
            SetIndIfResultContains(18, "", "9991600100682", "", "", reslist);// "Bag-valve-mask,Bag - valve - ET tube,Bag - valve - tracheostomy,Ventilator,Bilevel positive airway pressure,BiPAP,Continuous positive airway pressure,CPAP,CPAP nasal,CPAP mask,Positive pressure ventilation,PPV,Other");
            SetIndIfResultContains(18, "", "9991600100646", "", "", "Respiratory arrest,Cardiac arrest,Unknown,Other");
            SetIndIfResultContains(18, "", "9991600100613", "", "", "");
            SetIndIfResultContains(18, "", "9991600100614", "", "", "");

            bool RRq30 = false;
            if (Exists("", "9991733888883", "", "", "CPAP facial,CPAP gap present,CPAP nasal"))
            {
                //Respiratory rate  (PA - VS - 8 or P - 26 / PA - P - 43)repeated q30
                buckets = new List<gBucket>();
                codelist = "3045001064,3045001128";
                AddBuckets(buckets, "", codelist, "", "", "");
                RRq30 = AnalyzeBuckets(buckets, ind, bucket_size, "9991733888883 + RR q30");
            }

            if (SetIndIfResultContains(18, "", "9991733888883", "", "", "CPAP vent,ETT nasal,ETT oral"))
                if (RRq30) SetInd(24, "RR q30 + CPAP vent,ETT nasal,ETT oral");

            if (Exists("", "9993040000407", "", "", "4"))
            {
                //   This selection with a vent mode PA - P - 34 or PA - P - 35
                SetIndIfResultDoesNotContain(18, "", "3045001117", "", "", "CPAP,Hi Flow O2");
                reslist = "PACU Ventilator Rows,Trilogy Ventilator Rows,HFO Ventilator Rows";
                reslist += ",BPAP/CPAP Rows,Abbreviated Settings,NAVA Rows,HFJV Rows";
                SetIndIfResultContains(18, "", "9993040000635", "", "", reslist);
            }

            buckets = new List<gBucket>();
            codelist = "9991733666660";
            AddBuckets(buckets, "", codelist, "", "", "Apnea,Bradycardia,Desaturation,Other");
            AnalyzeBuckets(buckets, ind, bucket_size, "P-38  PA-P-18");

            buckets = new List<gBucket>();
            codelist = "9990007096450";
            AddBuckets(buckets, "", codelist, "", "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, "P-39  PA-P-23");

            buckets = new List<gBucket>();
            codelist = "ORD06";
            AddBuckets(buckets, "", codelist, "", "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, "ORD06");


            int pap2 = 0;
            if (GetResult("", "3045001052", "", "", out res))
            {
                if (res.Left(1).IsNumeric())
                {
                    pap2 = (int)res.Val();
                    //Program.VerboseAudit("PA-P-2 = " + pap2);
                }
            }
            if (pap2 >= 30 && fio2 >= 70 && spo2 && resprt)
            {
                SetIndIfResultContains(18, "", "3045001051", "", "", "High flow cannula");
            }

            if (CheckMeds18() > 0) SetInd(18, "Found med Alteplase or Tenecteplase in the past 9 hours.");

            //NIHSS
            if (Exists("", "3045001047", "", "", "", SearchDepth.SearchSince24Hrs))
            {
                buckets = new List<gBucket>();
                codelist = "3045001023,9993040001207";
                AddBuckets(buckets, "", codelist, "", "", "");
                AnalyzeBuckets(buckets, ind, bucket_size, "NIHSS");
            }

        }

        private int CheckIVPush()
        {
            int ct = 0;
            //MED____^drug name;;;__;;;__;;;IV;;;Given    
            //Or    MED____^drug name;;;__;;;__;;;;;;Given

            string[] ivpush_meds = { "Adenosine", "Etomidate", "Hydralazine", "Metoprolol", "Midazolam", "Norepinephrine", "Phenylephrine", "Rocuronium",
            "Labetalol","Mannitol","Midazolam","SODIUM CHLORIDE 3"};

            //So for the yellow:
            //Working on double checking the list of med names and then I will update the table.
            //In the second spot following the ; ; ; there will be any number including “0”.
            //Then in the OBX 5 spot, it will say Rate Verify when the patient is on a drip that is being titrated.This should eliminate any one time pushes, etc. as they would not show as a Rate Verify.
            var query = StartNewQuery(SearchDepth.SearchSince9Hrs);    // add custom time range below
            query = query.Where(e => e.CODE.StartsWith("med"));
            query = query.Where(e => e.UNIT_ID == _pat.unit_id);
            query = query.Where(e => ivpush_meds.Any(item2 => e.DESCRIPTION.ToUpper().StartsWith(item2.ToUpper())));
            query = query.Where(e => e.DESCRIPTION.Contains("iv;;;given") || e.DESCRIPTION.Contains(";;;;;;given"));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            //Program.VerboseAudit("Num IV Push Meds found: " + query.Count());
            foreach (var item in query)
            {
                //Program.VerboseAudit("====Med found: IVPush meds====");
                //Program.VerboseAudit("med code: " + item.CODE + " desc: " + item.DESCRIPTION + "  evdt: " + item.EVENT_DATETIME.ToString());

                //var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                //if (arr.GetUpperBound(0) >= 4)
                //{
                //    result = arr[4];
                //    if (result.ToLower().ContainsAny(meds_rate))
                //    {
                //        ct++;
                //    }
                //}
                ct++;
            }
            if (ct > 0)
                Program.VerboseAudit("Found IVPush Med count=" + ct);
            return ct;
        }
        private int CheckMeds18()
        {
            int ct = 0;
            //MED____^drug name;;;__;;;__;;;IV;;;Given    
            //Or    MED____^drug name;;;__;;;__;;;;;;Given

            string[] iv_meds = { "Alteplase", "Tenecteplase" };

            var query = StartNewQuery(SearchDepth.SearchSince9Hrs);
            query = query.Where(e => e.CODE.StartsWith("med"));
            query = query.Where(e => e.UNIT_ID == _pat.unit_id);
            query = query.Where(e => iv_meds.Any(item2 => e.DESCRIPTION.ToUpper().StartsWith(item2.ToUpper())));
            query = query.Where(e => e.DESCRIPTION.ToLower().ContainsAny(meds_rate) || e.DESCRIPTION.ToLower().Contains("stopped"));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            foreach (var item in query)
            {
                Program.VerboseAudit("====CheckMed18 Med found:");
                Program.VerboseAudit("med code: " + item.CODE + " desc: " + item.DESCRIPTION + "  evdt: " + item.EVENT_DATETIME.ToString());
                ct++;
            }
            if (ct > 0)
                Program.VerboseAudit("Found CheckMed18 Med count=" + ct);
            return ct;
        }


        private void AddDependentBuckets(List<gBucket> bucket_list, string codelist1, string reslist1, string codelist2, string reslist2)
        {
            AddDependentBuckets(bucket_list, codelist1, reslist1, codelist2, reslist2, "Phantom", "");
        }
        private void AddDependentBuckets(List<gBucket> bucket_list, string codelist1, string reslist1, string codelist2, string reslist2, string codelist3, string reslist3)
        {
            AddDependentBuckets(bucket_list, codelist1, reslist1, codelist2, reslist2, codelist3, reslist3, "Phantom", "");
        }

        private void AddDependentBuckets(List<gBucket> bucket_list, string codelist1, string reslist1, string codelist2, string reslist2, string codelist3, string reslist3, string codelist4, string reslist4)
        {
            bool dep3 = true;
            bool dep4 = true;
            // get the chart items for the assessments
            var query1 = StartNewQuery(SearchDepth.SearchDefault);
            query1 = AndItemFilter(query1, "", codelist1, "", "", reslist1);
            //Program.VerboseAudit("query1:" + query1.Count() + " bucketsize:" + _bucket_size);
            //foreach (var x in query1)
            //{
            //    var s1 = "(" + x.CODE + "," + x.EVENT_DATETIME.ToString() + ")";
            //    Program.VerboseAudit("query1a item:=" + s1);
            //}

            var query1b = (from item in query1
                               //                        select new { bnum = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size) }).Distinct();
                           select new { evdt = item.EVENT_DATETIME }
                           ).Distinct();
            //Program.VerboseAudit("query1b:" + query1b.Count());

            var query2 = StartNewQuery(SearchDepth.SearchDefault);
            query2 = AndItemFilter(query2, "", codelist2, "", "", reslist2);
            //Program.VerboseAudit("query2:" + query2.Count());
            //foreach (var x in query2)
            //{
            //    var s1 = "(" + x.CODE + "," + x.EVENT_DATETIME.ToString() + ")";
            //    Program.VerboseAudit("query2a item:=" + s1);
            //}
            var query2b = (from item in query2
                               //select new { bnum = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size) }).Distinct();
                           select new { evdt = item.EVENT_DATETIME }
                           ).Distinct();
            //Program.VerboseAudit("query2b:" + query2b.Count());

            if (codelist3.Trim() == "Phantom")
            {
                dep3 = false;
                codelist3 = "Hello this is a phantom code";
            }
            var query3 = StartNewQuery(SearchDepth.SearchDefault);
            query3 = AndItemFilter(query3, "", codelist3, "", "", reslist3);
            //Program.VerboseAudit("query3:" + query3.Count());
            //foreach (var x in query3)
            //{
            //    var s1 = "(" + x.CODE + "," + x.EVENT_DATETIME.ToString() + ")";
            //    Program.VerboseAudit("query3a item:=" + s1);
            //}
            var query3b = (from item in query3
                               //                           select new { bnum = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size) }).Distinct();
                           select new { evdt = item.EVENT_DATETIME }
                           ).Distinct();
            //Program.VerboseAudit("query3b:" + query3b.Count());


            if (codelist4.Trim() == "Phantom")
            {
                dep4 = false;
                codelist4 = "Hello this is a phantom code";
            }
            var query4 = StartNewQuery(SearchDepth.SearchDefault);
            query4 = AndItemFilter(query4, "", codelist4, "", "", reslist4);
            //Program.VerboseAudit("query4:" + query4.Count());
            var query4b = (from item in query4
                               //                           select new { bnum = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size) }).Distinct();
                           select new { evdt = item.EVENT_DATETIME }
                           ).Distinct();
            //Program.VerboseAudit("query4b:" + query4b.Count());

            // figure out what buckets the events belong to
            var query1a = from item1x in query1b
                          select new
                          {
                              bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item1x.evdt) / _bucket_size), //  bucket = item1x.bnum,
                              code = codelist1,
                              evdt = item1x.evdt // _pat.pull_start.AddMinutes(item1x.bnum * _bucket_size)
                          };
            //foreach (var x in query1a)
            //{
            //    var s = "(" + x.bucket + "," + x.code + "," + x.evdt.ToString() + ")";
            //    Program.VerboseAudit("query1a item:=" + s);
            //}
            var query2a = from item2x in query2b
                          select new
                          {
                              bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item2x.evdt) / _bucket_size), //bucket = item2x.bnum,
                              code = codelist2,
                              evdt = item2x.evdt // _pat.pull_start.AddMinutes(item2x.bnum * _bucket_size)
                          };
            //foreach (var x in query2a)
            //{
            //    var s = "(" + x.bucket + "," + x.code + "," + x.evdt.ToString() + ")";
            //    Program.VerboseAudit("query2a item:=" + s);
            //}
            var query3a = from item3x in query3b
                          select new
                          {
                              bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item3x.evdt) / _bucket_size),//item3x.bnum,
                              code = codelist3,
                              evdt = item3x.evdt //_pat.pull_start.AddMinutes(item3x.bnum * _bucket_size)
                          };
            //foreach (var x in query3a)
            //{
            //    var s = "(" + x.bucket + "," + x.code + "," + x.evdt.ToString() + ")";
            //    Program.VerboseAudit("query3a item:=" + s);
            //}
            var query4a = from item4x in query4b
                          select new
                          {
                              bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item4x.evdt) / _bucket_size), // item4x.bnum,
                              code = codelist4,
                              evdt = item4x.evdt //_pat.pull_start.AddMinutes(item4x.bnum * _bucket_size)
                          };

            //string s = "BucketList1 for " + codelist1 + ": ";
            //foreach (var item in query1a)
            //{
            //    s += item.bucket + ",";
            //}
            //Program.VerboseAudit(s);

            //s = "BucketList2 for " + codelist2 + ": ";
            //foreach (var item in query2a)
            //{
            //    s += item.bucket + ",";
            //}
            //if (dep3)
            //{
            //    s = "BucketList3 for " + codelist3 + ": ";
            //    foreach (var item in query3a)
            //    {
            //        s += item.bucket + ",";
            //    }
            //}
            //Program.VerboseAudit(s);
            int i = 0;
            // Add to the list IFF items in both lists occur in same bucket
            foreach (var item1 in query1a)
            {
                foreach (var item2 in query2a)
                {
                    if (item1.bucket == item2.bucket)
                    {
                        if (dep3)
                        {
                            foreach (var item3 in query3a)
                            {
                                if (item1.bucket == item3.bucket)
                                {
                                    if (dep4)
                                    {
                                        foreach (var item4b in query4a)
                                        {
                                            if (item1.bucket == item4b.bucket)
                                            {
                                                var b = new gBucket();
                                                b.bucket = item1.bucket;
                                                b.code = item1.code;
                                                b.evdt = item1.evdt;
                                                //b.has_all_deps = (item1.evdt == item2.evdt && item1.evdt == item3.evdt && item1.evdt == item4b.evdt);
                                                b.has_all_deps = (item1.evdt.AddMinutes(-15) <= item2.evdt && item1.evdt.AddMinutes(15) >= item2.evdt &&
                                                                  item1.evdt.AddMinutes(-15) <= item3.evdt && item1.evdt.AddMinutes(15) >= item3.evdt &&
                                                                  item1.evdt.AddMinutes(-15) <= item4b.evdt && item1.evdt.AddMinutes(15) >= item4b.evdt);
                                                if (b.has_all_deps)
                                                {
                                                    b.num_addl_items = 3;
                                                    gBucket f = bucket_list.Find(x => x.evdt == item1.evdt);
                                                    if (f.evdt != item1.evdt) bucket_list.Add(b);
                                                }
                                                //Program.VerboseAudit("4b:"+i++.ToString());
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (item1.bucket == item3.bucket)
                                        {
                                            var b = new gBucket();
                                            b.bucket = item1.bucket;
                                            b.code = item1.code;
                                            b.evdt = item1.evdt;
                                            //b.has_all_deps = (item1.evdt == item2.evdt && item1.evdt == item3.evdt);
                                            b.has_all_deps = (item1.evdt.AddMinutes(-15) <= item2.evdt && item1.evdt.AddMinutes(15) >= item2.evdt &&
                                                              item1.evdt.AddMinutes(-15) <= item3.evdt && item1.evdt.AddMinutes(15) >= item3.evdt);
                                            if (b.has_all_deps)
                                            {
                                                b.num_addl_items = 2;
                                                gBucket f = bucket_list.Find(x => x.evdt == item1.evdt);
                                                if (f.evdt != item1.evdt) bucket_list.Add(b);
                                            }
                                            //Program.VerboseAudit("3b:" + i++.ToString());
                                        }

                                    }
                                }
                            }
                        }
                        else
                        {
                            //Program.VerboseAudit("item1:b=" + item1.bucket + " c=" + item1.code + " evdt=" + item1.evdt.ToString());
                            //Program.VerboseAudit("item2b:b=" + item2.bucket + " c=" + item2.code + " evdt=" + item2.evdt.ToString());
                            if (item1.bucket == item2.bucket)
                            {
                                var b = new gBucket();
                                b.bucket = item1.bucket;
                                b.code = item1.code;
                                b.evdt = item1.evdt;
                                //b.has_all_deps = (item1.evdt == item2.evdt);
                                b.has_all_deps = (item1.evdt.AddMinutes(-15) <= item2.evdt && item1.evdt.AddMinutes(15) >= item2.evdt);
                                if (b.has_all_deps)
                                {
                                    b.num_addl_items = 1;
                                    //Program.VerboseAudit("  hasall=" + b.has_all_deps);
                                    gBucket f = bucket_list.Find(x => x.evdt == item1.evdt);
                                    if (f.evdt != item1.evdt) bucket_list.Add(b);
                                }
                                //Program.VerboseAudit("2b:" + i++.ToString());
                            }
                        }
                    }
                }
            }

        }

        private void NEWAddDependentBuckets(List<gBucket> bucket_list, string codelist1, string reslist1, string codelist2, string reslist2, string codelist3, string reslist3, string codelist4, string reslist4)
        {
            bool dep3 = true;
            bool dep4 = true;
            // get the chart items for the assessments
            var query1 = StartNewQuery(SearchDepth.SearchDefault);
            query1 = AndItemFilter(query1, "", codelist1, "", "", reslist1);
            var query1b = (from item in query1
                           select new { evdt = item.EVENT_DATETIME, to_keep = false }
                           ).Distinct();



            var query2 = StartNewQuery(SearchDepth.SearchDefault);
            query2 = AndItemFilter(query2, "", codelist2, "", "", reslist2);
            var query2b = (from item in query2
                           select new { evdt = item.EVENT_DATETIME, to_keep = false }
                           ).Distinct();



            if (codelist3.Trim() == "Phantom")
            {
                dep3 = false;
                codelist3 = "Hello this is a phantom code";
            }
            var query3 = StartNewQuery(SearchDepth.SearchDefault);
            query3 = AndItemFilter(query3, "", codelist3, "", "", reslist3);
            var query3b = (from item in query3
                           select new { evdt = item.EVENT_DATETIME }
                           ).Distinct();



            if (codelist4.Trim() == "Phantom")
            {
                dep4 = false;
                codelist4 = "Hello this is a phantom code";
            }
            var query4 = StartNewQuery(SearchDepth.SearchDefault);
            query4 = AndItemFilter(query4, "", codelist4, "", "", reslist4);
            var query4b = (from item in query4
                           select new { evdt = item.EVENT_DATETIME }
                           ).Distinct();


            // figure out what buckets the events belong to
            var query1a = from item1x in query1b
                          select new
                          {
                              bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item1x.evdt) / _bucket_size), //  bucket = item1x.bnum,
                              code = codelist1,
                              evdt = item1x.evdt // _pat.pull_start.AddMinutes(item1x.bnum * _bucket_size)
                          };
            //foreach (var x in query1a)
            //{
            //    var s = "(" + x.bucket + "," + x.code + "," + x.evdt.ToString() + ")";
            //    Program.VerboseAudit("query1a item:=" + s);
            //}
            var query2a = from item2x in query2b
                          select new
                          {
                              bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item2x.evdt) / _bucket_size), //bucket = item2x.bnum,
                              code = codelist2,
                              evdt = item2x.evdt // _pat.pull_start.AddMinutes(item2x.bnum * _bucket_size)
                          };
            //foreach (var x in query2a)
            //{
            //    var s = "(" + x.bucket + "," + x.code + "," + x.evdt.ToString() + ")";
            //    Program.VerboseAudit("query2a item:=" + s);
            //}
            var query3a = from item3x in query3b
                          select new
                          {
                              bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item3x.evdt) / _bucket_size),//item3x.bnum,
                              code = codelist3,
                              evdt = item3x.evdt //_pat.pull_start.AddMinutes(item3x.bnum * _bucket_size)
                          };
            //foreach (var x in query3a)
            //{
            //    var s = "(" + x.bucket + "," + x.code + "," + x.evdt.ToString() + ")";
            //    Program.VerboseAudit("query3a item:=" + s);
            //}
            var query4a = from item4x in query4b
                          select new
                          {
                              bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item4x.evdt) / _bucket_size), // item4x.bnum,
                              code = codelist4,
                              evdt = item4x.evdt //_pat.pull_start.AddMinutes(item4x.bnum * _bucket_size)
                          };

            //string s = "BucketList1 for " + codelist1 + ": ";
            //foreach (var item in query1a)
            //{
            //    s += item.bucket + ",";
            //}
            //Program.VerboseAudit(s);

            //s = "BucketList2 for " + codelist2 + ": ";
            //foreach (var item in query2a)
            //{
            //    s += item.bucket + ",";
            //}
            //if (dep3)
            //{
            //    s = "BucketList3 for " + codelist3 + ": ";
            //    foreach (var item in query3a)
            //    {
            //        s += item.bucket + ",";
            //    }
            //}
            //Program.VerboseAudit(s);
            int i = 0;
            // Add to the list IFF items in both lists occur in same bucket
            foreach (var item1 in query1a)
            {
                foreach (var item2 in query2a)
                {
                    if (item1.bucket == item2.bucket)
                    {
                        if (dep3)
                        {
                            foreach (var item3 in query3a)
                            {
                                if (item1.bucket == item3.bucket)
                                {
                                    if (dep4)
                                    {
                                        foreach (var item4b in query4a)
                                        {
                                            if (item1.bucket == item4b.bucket)
                                            {
                                                var b = new gBucket();
                                                b.bucket = item1.bucket;
                                                b.code = item1.code;
                                                b.evdt = item1.evdt;
                                                gBucket f = bucket_list.Find(x => x.evdt == item1.evdt);
                                                if (f.evdt != item1.evdt) bucket_list.Add(b);
                                                //Program.VerboseAudit("4b:"+i++.ToString());
                                            }
                                        }
                                    }
                                    else
                                    {
                                        foreach (var item3b in query3a)
                                        {
                                            if (item1.bucket == item3b.bucket)
                                            {
                                                var b = new gBucket();
                                                b.bucket = item1.bucket;
                                                b.code = item1.code;
                                                b.evdt = item1.evdt;
                                                gBucket f = bucket_list.Find(x => x.evdt == item1.evdt);
                                                if (f.evdt != item1.evdt) bucket_list.Add(b);
                                                //Program.VerboseAudit("3b:" + i++.ToString());
                                            }
                                        }

                                    }
                                }
                            }
                        }
                        else
                        {
                            foreach (var item2b in query2a)
                            {
                                if (item1.bucket == item2b.bucket)
                                {
                                    var b = new gBucket();
                                    b.bucket = item1.bucket;
                                    b.code = item1.code;
                                    b.evdt = item1.evdt;
                                    gBucket f = bucket_list.Find(x => x.evdt == item1.evdt);
                                    if (f.evdt != item1.evdt) bucket_list.Add(b);
                                    //Program.VerboseAudit("2b:" + i++.ToString());
                                }
                            }
                        }
                    }
                }
            }

        }


        private int SetMaxWaivers(int imins)
        {
            string qstr = "";
            double losx = _pat.los_hours * 60;
            int num_waivers = 0;

            //Program.VerboseAudit("SetMaxWaivers");

            if (imins == 20)
            {//then 'q15
                if (losx >= 180)
                    num_waivers = 2;
                else if (losx >= 80)
                    num_waivers = 1;
                else
                    num_waivers = 0;
            }
            else if (imins == 80) // Then 'q1
            {
                if (losx < 360)
                    num_waivers = 0;
                else
                    num_waivers = (int)losx / 360;
            }
            else if (imins == 40) // Then 'q30
            {
                if (losx < 240)
                    num_waivers = 0;
                else
                    num_waivers = (int)losx / 240;
            }
            if (imins == 20)
                qstr = "q15min";
            else if (imins == 80)
                qstr = "q1hr";
            else if (imins == 40)
                qstr = "q30min";

            //Program.VerboseAudit(qstr + ":Max waivers allowed=" + num_waivers + " (LOS=" + losx + ")");

            return num_waivers;
        }

        private bool AnalyzeBuckets(List<gBucket> buckets, int ind, int bucketsize, string group)
        {
            return AnalyzeBuckets(buckets, ind, bucketsize, group, true);

        }

        //private bool AnalyzeBuckets(List<gBucket> buckets, int ind, int bucketsize, string group, bool set_ind)
        //{
        //    DateTime dt = DateTime.MinValue;
        //    int bnum = 0;
        //    List<gBucket> dtlist = new List<gBucket>();
        //    Program.VerboseAudit("---- Begin Assessment Group = " + group + " ---- bucketsize=" + bucketsize);

        //    //Program.VerboseAudit("buckets count=" + buckets.Count());
        //    var b = buckets.OrderBy(e => e.evdt).ToList();
        //    foreach (var item in b)
        //    {
        //        Program.VerboseAudit(item.bucket + ") " + item.evdt.ToString() + " := " + item.code);
        //        if (dt < item.evdt)
        //        {
        //            //add dt to ary
        //            bnum++;
        //            dtlist.Add(item);
        //            dt = item.evdt;
        //            Program.VerboseAudit(item.bucket + ") " + item.evdt.ToString() + " := " + item.code);// + "  [distinct times only]");
        //        }
        //    }

        //    int i, j, istart;
        //    double addmins = _pat.los_hours * 30.0; // 60/2.0;
        //    int minupperidx = 0;
        //    bool all_ok = false;
        //    int j_cannot_use_waiver = 0;
        //    int imins = bucketsize;
        //    int num_waivers = SetMaxWaivers(bucketsize);
        //    int w = num_waivers;
        //    DateTime upperdt;
        //    gBucket[] dtary = dtlist.ToArray();
        //    Program.VerboseAudit("addmins=" + addmins.ToString());
        //    for (i = 0; i <= bnum - 2; i++)
        //    {
        //        istart = i;
        //        upperdt = dtary[i].evdt.AddMinutes(addmins);
        //        //Program.VerboseAudit("i:=" + i + "  upperdt:=" + upperdt.ToString());

        //        //what is the min evdt >= upperdt?
        //        minupperidx = 0;
        //        if (addmins >= bucketsize / 2.0)
        //        {
        //            for (j = i + 1; j <= bnum - 1; j++)  //For j = i + 1 To b2num
        //            {
        //                //Program.VerboseAudit("dtary[" + j + "].evdt:=" + dtary[j].evdt.ToString());
        //                if (dtary[j].evdt >= upperdt)
        //                {
        //                    minupperidx = j;
        //                    //dvprint "min upper idx=" & minupperidx
        //                    //dvprint "min upper time=" & bucket2(j).eventdt
        //                    j = bnum - 1 + 1;
        //                }
        //            } // j
        //        }

        //        //Program.VerboseAudit("minupperidx:=" + minupperidx);
        //        if (minupperidx == 0)  //then half LOS not possible
        //        {
        //            all_ok = false;
        //            i = bnum - 1; //end loop
        //        }
        //        else
        //        {
        //            j_cannot_use_waiver = 0;
        //            all_ok = true;
        //            //Program.VerboseAudit("dtary[" + i + "].evdt:=" + dtary[i].evdt.ToString()); //dvprint "i time=" & bucket2(i).eventdt
        //            for (j = i; j <= minupperidx - 1; j++) //For j = i To minupperidx -1
        //            {
        //                if (dtary[j].evdt.AddMinutes(imins) < dtary[j + 1].evdt)
        //                    if (w > 0)  //then 'we can use a waiver
        //                        if (j != j_cannot_use_waiver) //  Then 'we can
        //                            if (dtary[j].evdt.AddMinutes(2 * imins) >= dtary[j + 1].evdt)
        //                            {
        //                                dtary[j].using_waiver = true;
        //                                j_cannot_use_waiver = j + 1;
        //                                w = w - 1;
        //                            }
        //                            else
        //                                all_ok = false;
        //                        else
        //                            all_ok = false;
        //                    else
        //                        all_ok = false;

        //            }

        //            if (all_ok)
        //                i = bnum; // 'end loop
        //            else
        //                //'reset waivers
        //                for (j = 0; j <= bnum - 1; j++)
        //                    dtary[j].using_waiver = false;
        //        }

        //    }

        //    if (all_ok)
        //        if (set_ind)
        //            SetInd(ind, "Qualifies for q" + imins + "mins for duration of half-LOS=" + addmins.ToString() + " minutes.");
        //        else
        //            Program.VerboseAudit(group + " qualifies for q" + imins + " minutes.");
        //    else
        //    {
        //        Program.VerboseAudit("Does not meet frequency criteria for indicator #" + ind);
        //        //            'assign indexes for the dump to follow
        //        istart = 0;
        //        minupperidx = bnum - 1;
        //    }

        //    if (num_waivers > w)
        //    {
        //        int w2 = 0;
        //        for (j = 0; j < bnum; j++)
        //            if (dtary[j].using_waiver)
        //            {
        //                w2++;
        //                Program.VerboseAudit("Waiver " + w2 + ": " + dtary[j].evdt.AddMinutes(imins).ToString());
        //            }
        //    }
        //    if (ind < 15 || ind > 18)
        //    {
        //        if (bnum >= 2 && _pat.los_hours <= 2)
        //        {
        //            all_ok = true;
        //            Program.VerboseAudit("At least two items for ind#" + ind + "  count=" + bnum);
        //        }
        //    }
        //    if (ind == 15 && !_inds[18].is_checked && !_inds[17].is_checked && !_inds[16].is_checked && !_inds[15].is_checked)
        //    {
        //        if (bnum >= 2) SetInd(15, "At least 2 items found in this assessment category.");
        //    }
        //    Program.VerboseAudit("---- End Assessment Group = " + group + " ----");
        //    return all_ok;

        //    //sql = "select event_datetime,category,description,field_name,result from chart_item " & WhereBase & b_filter & b_excl
        //    //sql = sql & " and event_datetime between " & g_dbutil.SQL_DateTime(bucket2(istart).eventdt)
        //    //sql = sql & " and " & g_dbutil.SQL_DateTime(bucket2(minupperidx).eventdt) & " order by event_datetime"
        //    //'dvprint sql
        //    //rs.Open sql, g_cnADO
        //    //Do While Not rs.EOF
        //    //    dprint "  " & rs(0) & ": " & g_dbutil.DBToString(rs(1)) & "; " & g_dbutil.DBToString(rs(2)) & "; " & g_dbutil.DBToString(rs(3)) & "; " & g_dbutil.DBToString(rs(4))
        //    //    rs.MoveNext
        //    //Loop
        //    //rs.Close


        //    //ResetWaivers

        //}

        private bool AnalyzeBuckets(List<gBucket> buckets, int ind, int bucketsize, string group, bool set_ind)
        {
            DateTime dt = DateTime.MinValue;
            int bnum = 0;
            int numbucket = -99;
            int numitems = 0;
            bool all_ok = false;
            List<gBucket> dtlist = new List<gBucket>();
            Program.VerboseAudit("----Begin Assessment Group = " + group + " ---- bucketsizew=" + bucketsize);

            all_ok = NEWAnalyzeBuckets(buckets, ind, bucketsize, group, set_ind);
            return all_ok;

            //Program.VerboseAudit("buckets count=" + buckets.Count());
            var b = buckets.OrderBy(e => e.evdt).ToList();
            //numitems = buckets.Count();
            foreach (var item in b)
            {
                if (dt < item.evdt)
                {
                    dt = item.evdt;
                    numitems++;
                //    Program.VerboseAudit(item.bucket + ") " + item.evdt.ToString() + " := " + item.code);
                }
                if (numbucket < item.bucket)
                {
                    //add dt to ary
                    bnum++;
                    dtlist.Add(item);
                    numbucket = item.bucket;
              //      Program.VerboseAudit(item.bucket + ")." + item.evdt.ToString() + " := " + item.code);// + "  [distinct times only]");
                }
            }
            //Program.VerboseAudit("numitems=" + numitems);

            int i, j, istart = 0;
            double addmins = _pat.los_hours * 30.0; // 60/2.0;
            int minupperidx = 0;
            int j_cannot_use_waiver = 0;
            int imins = bucketsize;
            int num_waivers = SetMaxWaivers(bucketsize);
            int w = num_waivers;
            DateTime upperdt;
            gBucket[] dtary = dtlist.ToArray();
            //Program.VerboseAudit("num_waivers=" + w);
            int numconsecutive = 0;
            int ilast = 0;
            int maxconsec = 1;

            for (i = 0; i < bnum; i++)
            {
                if (i == 0)
                {
                    istart = dtary[i].bucket;
                    ilast = istart;
                    numconsecutive = 1;
                }
                else
                {
                    if (dtary[i].bucket == ilast + 1)
                    {
                        ilast = dtary[i].bucket;
                        numconsecutive++;
                        maxconsec = Math.Max(numconsecutive, maxconsec);
                    }
                    else
                    {
                        if (w > 0)
                        {
                            if (dtary[i].bucket - (ilast + 1) == w)
                            {
                                Program.VerboseAudit("i= ) " + i + " bucket=" + dtary[i].bucket + " ilast=" + ilast + " numconsec=" + numconsecutive + " maxconsec=" + maxconsec + " waivers using w=" + w);
                                numconsecutive = numconsecutive + 1 + w;
                                w = 0;
                                ilast = dtary[i].bucket;
                                maxconsec = Math.Max(numconsecutive, maxconsec);
                            }
                            else if (dtary[i].bucket - (ilast + 1) == 1)
                            {
                                Program.VerboseAudit("i= ) " + i + " bucket=" + dtary[i].bucket + " ilast=" + ilast + " numconsec=" + numconsecutive + " maxconsec=" + maxconsec + " waivers using 1=" + w);
                                numconsecutive = numconsecutive + 1 + 1;
                                w = w - 1;
                                ilast = dtary[i].bucket;
                                maxconsec = Math.Max(numconsecutive, maxconsec);
                            }
                        }
                        else
                        {
                            istart = dtary[i].bucket;
                            ilast = istart;
                            numconsecutive = 1;
                            maxconsec = Math.Max(numconsecutive, maxconsec);
                        }
                    }
                }
                Program.VerboseAudit("i=) " + i + " bucket=" + dtary[i].bucket + " ilast=" + ilast + " numconsec=" + numconsecutive + " maxconsec=" + maxconsec);
            }

            //240 / 40 = 6 + 1 consecutive
            //240 /80=3+1 consecutive
            //480 /150=3
            //240/300=2
            if ((ind == 18 && maxconsec >= 6 * (_pat.los_hours / 8.0) && numitems >= 4)
                || (ind == 17 && maxconsec >= 4 * (_pat.los_hours / 8.0) && numitems >= 4)
                || (ind == 16 && maxconsec >= 3 * (_pat.los_hours / 8.0) && numitems >= 3)
                || (ind == 15 && maxconsec >= 2 * (_pat.los_hours / 8.0) && numitems >= 2)) all_ok = true;

            if (all_ok)
                if (set_ind)
                    SetInd(ind, "Qualifies for q" + imins + "mins for duration of half-LOS=" + addmins.ToString() + " minutes.  maxconsec=" + maxconsec);
                else
                    Program.VerboseAudit(group + " qualifies for q" + imins + " minutes.");
            else
            {
                Program.VerboseAudit("Does not meet frequency criteria for indicator #" + ind);
                //            'assign indexes for the dump to follow
                istart = 0;
                minupperidx = bnum - 1;
            }

            //if (num_waivers > w)
            //{
            //    int w2 = 0;
            //    for (j = 0; j < bnum; j++)
            //        if (dtary[j].using_waiver)
            //        {
            //            w2++;
            //            Program.VerboseAudit("Waiver " + w2 + ": " + dtary[j].evdt.AddMinutes(imins).ToString());
            //        }
            //}
            if (ind < 15 || ind > 18)
            {
                if (numitems >= 2 && _pat.los_hours <= 2)
                {
                    all_ok = true;
                    Program.VerboseAudit("At least two items for ind#" + ind + "  count=" + bnum);
                }
            }
            if (ind == 15 && !_inds[18].is_checked && !_inds[17].is_checked && !_inds[16].is_checked && !_inds[15].is_checked)
            {
                if (numitems >= 2) SetInd(15, "At least 2 items found in this assessment category.");
            }
            Program.VerboseAudit("---- End Assessment Group = " + group + " ----");

        }

        private bool NEWAnalyzeBuckets(List<gBucket> buckets, int ind, int bucketsize, string group, bool set_ind)
        {
            DateTime dt = DateTime.MinValue;
            DateTime firstdt = DateTime.MinValue;

            int bnum = 0;
            int numbucket = -99;
            int numitems = 0;
            int numallitems = 0;
            List<gGap> gaplist = new List<gGap>();
            Program.VerboseAudit("----GAP Begin Assessment Group = " + group + " ---- bucketsizew=" + bucketsize);

            //Program.VerboseAudit("buckets count=" + buckets.Count());
            var b = buckets.OrderBy(e => e.evdt).ToList();
            //numitems = buckets.Count();

            DateTime lastq4dt = DateTime.MinValue;
            int numq4 = 1;

            foreach (var item in b)
            {
                //Program.VerboseAudit(item.bucket + ") " + item.evdt.ToString() + " := " + item.code + " has all deps=" + item.has_all_deps);
                if (item.has_all_deps)
                    if (numitems == 0)
                    {
                        dt = item.evdt;
                        firstdt = dt;
                        lastq4dt = dt;
                    }
                    else
                    {
                        if (lastq4dt.AddMinutes(30) >= dt)
                        {
                            numq4++;
                            lastq4dt = dt;
                        }
                        var g = new gGap();
                        g.gap = (int)(PFSUtility.DateDiffInMinutes(dt, item.evdt));
                        g.evdt1 = dt;
                        g.evdt2 = item.evdt;
                        gaplist.Add(g);
                        dt = item.evdt;
                    }
                numitems++;
                numallitems = numallitems + 1 + item.num_addl_items;
            }
            //Program.VerboseAudit("numitems=" + numitems + " numq4=" + numq4);
            if (numitems <= 2)
            {
                if (numq4 >= 2)
                {
                    if (set_ind && ind >= 15 && ind <= 17)
                    {
                        SetInd(15, "q4 based on number >=2 of items charted = " + numq4);
                        return true;
                    }
                }
                return false;
            }

            int i, j, setind = 0;
            int numgaps = 0;
            int gapsum = 0;
            double gapavg = 999.0, lo_gapavg = 999.0, altlo_gapavg = 999.0;
            bool lastgap = false;
            bool shortstop = false;
            int timegap = 0;
            int lastj;
            DateTime lo_evdt1 = DateTime.MinValue, lo_evdt2 = DateTime.MinValue;
            DateTime altlo_evdt1 = DateTime.MinValue, altlo_evdt2 = DateTime.MinValue;
            gGap[] gapary = gaplist.ToArray();
            string s = "";
            for (i = 0; i <= gapary.GetUpperBound(0); i++) s = s + "," + gapary[i].gap;
            if (numitems == 2 && numq4 >= 2) // numitems will always be > 2
            {
                numgaps = 1;
                gapsum = gapary[0].gap;
                gapavg = gapsum;
                lo_gapavg = gapavg;
                lo_evdt1 = gapary[0].evdt1;
                lo_evdt2 = gapary[gapary.GetUpperBound(0)].evdt2;
            }
            else
            {

                //numitems = 4
                //i = 0 j = 0[i].evdt1 = 9 / 4 / 2019 5:15:00 AM[j].evdt2 = 9 / 4 / 2019 9:32:00 AM

                for (i = 0; i <= gapary.GetUpperBound(0) && !shortstop; i++)
                {
                    numgaps = 0;
                    gapsum = 0;
                    gapavg = 999.0;
                    lastgap = false;
                    for (j = i; j <= gapary.GetUpperBound(0) && !lastgap; j++)
                    {
                        //Program.VerboseAudit("i=" + i + " j=" + j + " [i].evdt1=" + gapary[i].evdt1 + " [j].evdt2=" + gapary[j].evdt2);
                        timegap = (int)(PFSUtility.DateDiffInMinutes(gapary[i].evdt1, gapary[j].evdt2));
                        if (timegap <= _pat.los_hours * 30 + 30)
                        {
                            numgaps++;
                            gapsum += gapary[j].gap;
                          //  Program.VerboseAudit("numgaps=" + numgaps + " [j].gap=" + gapary[j].gap + " gapsum=" + gapsum);
                        }
                        else
                        {
                            if (j == i)
                            {
                                lastj = i;
                                numgaps = 1;
                                gapsum = gapary[j].gap;
                            }
                            else
                                lastj = j - 1;
                            //Program.VerboseAudit("Last Gap: i=" + i + " j=" + j + " [i].evdt1=" + gapary[i].evdt1 + " [lastj].evdt2=" + gapary[lastj].evdt2);
                            lastgap = true;
                            gapavg = 1.0 * gapsum / numgaps;
                            //Program.VerboseAudit("gapavg=" + gapavg + " numgaps=" + numgaps + " gapsum=" + gapsum);
                            timegap = (int)(PFSUtility.DateDiffInMinutes(gapary[i].evdt1, gapary[lastj].evdt2));
                            if (gapavg < lo_gapavg && timegap >= .75 * _pat.los_hours * 30)
                            {
                                lo_gapavg = gapavg;
                              //  Program.VerboseAudit("Low gapavg=" + lo_gapavg + " btwn " + gapary[i].evdt1 + " and " + gapary[lastj].evdt2);
                                lo_evdt1 = gapary[i].evdt1;
                                lo_evdt2 = gapary[lastj].evdt2;
                            }
                            numgaps = 0;
                            gapsum = 0;
                        }

                    }
                    if (!lastgap)
                    {
                        shortstop = true;
                        j--; // take the j index back by 1 because it incremented at the end.
                        //Program.VerboseAudit(".Last Gap: i=" + i + " j=" + j + " [i].evdt1=" + gapary[i].evdt1 + " [j].evdt2=" + gapary[j].evdt2);
                        gapavg = 1.0 * gapsum / numgaps;
                        //Program.VerboseAudit(".gapavg=" + gapavg + " numgaps=" + numgaps + " gapsum=" + gapsum);
                        timegap = (int)(PFSUtility.DateDiffInMinutes(gapary[i].evdt1, gapary[gapary.GetUpperBound(0)].evdt2));
                        //Program.VerboseAudit(".timegap=" + timegap + " .75halfLOS=" + .75 * _pat.los_hours * 30);
                        if (gapavg < lo_gapavg)
                        {
                            if (timegap >= .75 * _pat.los_hours * 30)
                            {
                                lo_gapavg = gapavg;
                                //      Program.VerboseAudit(".Low gapavg=" + lo_gapavg + " btwn " + gapary[i].evdt1 + " and " + gapary[gapary.GetUpperBound(0)].evdt2);
                                lo_evdt1 = gapary[i].evdt1;
                                lo_evdt2 = gapary[gapary.GetUpperBound(0)].evdt2;
                            }
                            else
                            {
                                altlo_gapavg = gapavg;
                                //Program.VerboseAudit(".AltLow gapavg=" + lo_gapavg + " btwn " + gapary[i].evdt1 + " and " + gapary[gapary.GetUpperBound(0)].evdt2);
                                altlo_evdt1 = gapary[i].evdt1;
                                altlo_evdt2 = gapary[gapary.GetUpperBound(0)].evdt2;
                            }
                        }
                    }
                }
            }
            //Program.VerboseAudit("Final Low gapavg=" + lo_gapavg + " btwn " + lo_evdt1.ToString() + " and " + lo_evdt2.ToString() + " " + (_pat.los_hours <= 4 ? 0 : 1) + "LOS=" + _pat.los_hours + " assmts:" + (_inds[15].is_checked ? 1 : 0) + (_inds[16].is_checked ? 1 : 0) + (_inds[17].is_checked ? 1 : 0) + (_inds[18].is_checked ? 1 : 0) + " gap[]: =" + s);
            //Program.VerboseAudit("Alt   Low gapavg=" + altlo_gapavg + " btwn " + altlo_evdt1.ToString() + " and " + altlo_evdt2.ToString());
            //q4: 3 hr + gap average  in 6 - 8 hrs
            //  q2:   1 hr 30 - 2 hr 59m avg gap  in 4 hrs
            //  q1:   46 min - 1 hr 29 min avg gap in 4 hrs
            //  q30:  45 min or less avg gap in 4 hrs
            //            if (lo_gapavg <= 45)
            int sample_breadth = (int)(PFSUtility.DateDiffInMinutes(lo_evdt1, lo_evdt2));
            if (_pat.los_hours >= 4 && sample_breadth > 0.75 * _pat.los_hours * 30 && numitems >= 2)
            {
                if (lo_gapavg <= 45) setind = 18;       //q30
                else if (lo_gapavg <= 80) setind = 17;  //q60
                else if (lo_gapavg <= 180) setind = 16; //q120
                else if (lo_gapavg <= 999) setind = 15;  //q240
            }
            else
            {
                if (numitems > 2 && sample_breadth >= 0.75 * _pat.los_hours * 30)
                {
                    if (lo_gapavg <= 30) setind = 18;  //q60
                    else if (lo_gapavg <= 60) setind = 17;  //q60
                    else if (lo_gapavg <= 120) setind = 16; //q120
                    else if (lo_gapavg <= 999) setind = 15;  //q240
                }
                else if (numitems >= 2 && sample_breadth >= 30)
                {
                    if (sample_breadth >= 0.5 * _pat.los_hours * 30) setind = 16; //q120
                    else setind = 15;  //q240
                }
                else if (numitems >= 2)
                    setind = 15;
            }
            int imins = 0;
            bool all_ok = false;
            if (setind == 18) imins = 30;
            else if (setind == 17) imins = 60;
            else if (setind == 16) imins = 120;
            else if (setind == 15) imins = 240;
            if (ind == 11)
            {
                if (setind >= 17)
                    setind = 11;
                else
                    setind = 0;
            }
            if (ind >= 15 && ind <= 17)
            {
                if (setind == 18) setind = 17;
            }
            if (setind > 0)
            {
                if (set_ind)
                    SetInd(setind, "Qualifies for q" + imins + "mins for gap avg=" + lo_gapavg + "[" + (sample_breadth < 4 ? "<" : "") + "in " + sample_breadth + " mins] btwn " + lo_evdt1.ToString() + " and " + lo_evdt2.ToString());
                else
                    Program.VerboseAudit(group + "Qualifies for q" + imins + "mins for gap avg=" + lo_gapavg + "[" + (sample_breadth < 4 ? "<" : "") + "in " + sample_breadth + " mins] btwn " + lo_evdt1.ToString() + " and " + lo_evdt2.ToString());
            }
            if (ind == 18 && setind == 18) all_ok = true;
            if (ind >= 15 && ind <= 17 && setind >= 15) all_ok = true;
            if (ind == 11 && setind == 11) all_ok = true;
            if (ind == 3 && setind >= 16) all_ok = true;
            //double addmins = _pat.los_hours * 30.0; // 60/2.0;
            //int minupperidx = 0;
            //int j_cannot_use_waiver = 0;
            //int imins = bucketsize;
            //int num_waivers = SetMaxWaivers(bucketsize);
            //int w = num_waivers;
            //DateTime upperdt;
            ////Program.VerboseAudit("num_waivers=" + w);
            //int numconsecutive = 0;
            //int ilast = 0;
            //int maxconsec = 1;

            //for (i = 0; i < bnum; i++)
            //{
            //    if (i == 0)
            //    {
            //        istart = dtary[i].bucket;
            //        ilast = istart;
            //        numconsecutive = 1;
            //    }
            //    else
            //    {
            //        if (dtary[i].bucket == ilast + 1)
            //        {
            //            ilast = dtary[i].bucket;
            //            numconsecutive++;
            //            maxconsec = Math.Max(numconsecutive, maxconsec);
            //        }
            //        else
            //        {
            //            if (w > 0)
            //            {
            //                if (dtary[i].bucket - (ilast + 1) == w)
            //                {
            //                    Program.VerboseAudit("i= ) " + i + " bucket=" + dtary[i].bucket + " ilast=" + ilast + " numconsec=" + numconsecutive + " maxconsec=" + maxconsec + " waivers using w=" + w);
            //                    numconsecutive = numconsecutive + 1 + w;
            //                    w = 0;
            //                    ilast = dtary[i].bucket;
            //                    maxconsec = Math.Max(numconsecutive, maxconsec);
            //                }
            //                else if (dtary[i].bucket - (ilast + 1) == 1)
            //                {
            //                    Program.VerboseAudit("i= ) " + i + " bucket=" + dtary[i].bucket + " ilast=" + ilast + " numconsec=" + numconsecutive + " maxconsec=" + maxconsec + " waivers using 1=" + w);
            //                    numconsecutive = numconsecutive + 1 + 1;
            //                    w = w - 1;
            //                    ilast = dtary[i].bucket;
            //                    maxconsec = Math.Max(numconsecutive, maxconsec);
            //                }
            //            }
            //            else
            //            {
            //                istart = dtary[i].bucket;
            //                ilast = istart;
            //                numconsecutive = 1;
            //                maxconsec = Math.Max(numconsecutive, maxconsec);
            //            }
            //        }
            //    }
            //    Program.VerboseAudit("i=) " + i + " bucket=" + dtary[i].bucket + " ilast=" + ilast + " numconsec=" + numconsecutive + " maxconsec=" + maxconsec);
            //}

            ////240 / 40 = 6 + 1 consecutive
            ////240 /80=3+1 consecutive
            ////480 /150=3
            ////240/300=2
            //if ((ind == 18 && maxconsec >= 6 * (_pat.los_hours / 8.0) && numitems >= 4)
            //    || (ind == 17 && maxconsec >= 4 * (_pat.los_hours / 8.0) && numitems >= 4)
            //    || (ind == 16 && maxconsec >= 3 * (_pat.los_hours / 8.0) && numitems >= 3)
            //    || (ind == 15 && maxconsec >= 2 * (_pat.los_hours / 8.0) && numitems >= 2)) all_ok = true;

            //if (all_ok)
            //    if (set_ind)
            //        SetInd(ind, "Qualifies for q" + imins + "mins for duration of half-LOS=" + addmins.ToString() + " minutes.  maxconsec=" + maxconsec);
            //    else
            //        Program.VerboseAudit(group + " qualifies for q" + imins + " minutes.");
            //else
            //{
            //    Program.VerboseAudit("Does not meet frequency criteria for indicator #" + ind);
            //    //            'assign indexes for the dump to follow
            //    istart = 0;
            //    minupperidx = bnum - 1;
            //}

            ////if (num_waivers > w)
            ////{
            ////    int w2 = 0;
            ////    for (j = 0; j < bnum; j++)
            ////        if (dtary[j].using_waiver)
            ////        {
            ////            w2++;
            ////            Program.VerboseAudit("Waiver " + w2 + ": " + dtary[j].evdt.AddMinutes(imins).ToString());
            ////        }
            ////}
            //if (ind < 15 || ind > 18)
            //{
            //    if (numitems >= 2 && _pat.los_hours <= 2)
            //    {
            //        all_ok = true;
            //        Program.VerboseAudit("At least two items for ind#" + ind + "  count=" + bnum);
            //    }
            //}
            //if (ind == 15 && !_inds[18].is_checked && !_inds[17].is_checked && !_inds[16].is_checked && !_inds[15].is_checked)
            //{
            //    if (numitems >= 2) SetInd(15, "At least 2 items found in this assessment category.");
            //}
            Program.VerboseAudit("---- GAP End Assessment Group = " + group + " ----");
            return all_ok;

        }



        private void SetBucketSize(int minutes)
        {
            _bucket_size = minutes;
        }

        //Add to the list of bucket numbers (redundant buckets are fine)
        private void AddBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field)
        {
            AddBuckets(bucket_list, cat, code_list, desc, field, "", SearchDepth.SearchDefault);
        }
        private void AddBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field, string result_list)
        {
            AddBuckets(bucket_list, cat, code_list, desc, field, result_list, SearchDepth.SearchDefault);

        }
        private void AddBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field, string result_list, SearchDepth search_depth)
        {
            // Assign (virtual) bucket numbers to each assessment.  
            // Every x minutes after that is a new bucket.  Bucket zero = pull_start time.

            // get the chart items for the assessments
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc, field, result_list);

            // This step is needed for those who want to count the # within a bucket, like Shands #24.
            // This will have no effect on those that count buckets.
            //var query2 = (from item in query select new { item.EVENT_DATETIME }).Distinct();
            var query2 = (from item in query select new { item.EVENT_DATETIME, item.CODE });

            //foreach (var x in query2)
            //{
            //    Program.VerboseAudit("patpullstart=" + _pat.pull_start.ToString() + " itemevdt=" + x.EVENT_DATETIME.ToString() + " diff=" + (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, x.EVENT_DATETIME)) + " bucketsize=" + _bucket_size + " bucket=" + (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, x.EVENT_DATETIME) / _bucket_size));
            //}

            // figure out what buckets the events belong to
            //var query3 = from item in query2
            //             select new {
            //                bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size)
            //             };
            //// Add to the list
            //foreach (var item in query3) {
            //    bucket_list.Add(item.bucket);
            //}

            var query3 = from item in query2
                         select new
                         {
                             bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                             code = item.CODE,
                             evdt = item.EVENT_DATETIME
                         };
            // Add to the list
            foreach (var item in query3)
            {
                var b = new gBucket();
                b.bucket = item.bucket;
                b.code = item.code;
                b.evdt = item.evdt;
                b.has_all_deps = true;
                gBucket f = bucket_list.Find(x => x.evdt == item.evdt);
                if (f.evdt != item.evdt) bucket_list.Add(b);
            }

            // This is just for auditing: Say what we are looking for and how many were found
            if (!String.IsNullOrEmpty(result_list))
            {
                // print each word and if it was found or not
                int i = CountResultContains(cat, code_list, desc, field, result_list);
            }
            else
            {
                // print how many were found
                Program.VerboseAudit(Describe(cat, code_list, desc, field, ""));
            }
        }

        private void AddMedBuckets(List<gBucket> bucket_list)
        {
            string descript = "";
            string drugclass = "";
            string rte = "";
            string result = "";

            bool bres1 = false;
            bool bres2 = false;
            bool bclass1 = false;
            bool bclass2 = false;
            bool bclass3 = false;
            bool bmed1 = false;

            string[] medclass = { "7", "1", "2", "6" };

            string[] pamed1417 = { "Zofran","Ondansetron","Compazine","Prochlorperazine",
"Phenergan","Promethazine","Reglan","Metoclopramide","Marinol","Dronabinol","Ativan",
"Lorazepam","Imodium","Loperamide","Pepto-Bismol","Kaopectate","Bismuth subsalicylate",
"Lomotil","DIPHENOXYLATE-ATROPINE","OPIUM TINCTURE","Lactulose"};

            var query = StartNewQuery(SearchDepth.SearchSince13Hrs);    // add custom time range below
            query = query.Where(e => e.CODE.ToLower().StartsWith("med"));
            //query = query.Where(e => e.DESCRIPTION.Contains(";;;7;;;") || 
            //                         e.DESCRIPTION.Contains(";;;33;;;") || 
            //                         e.DESCRIPTION.Contains(";;;1;;;") || 
            //                         e.DESCRIPTION.Contains(";;;2;;;") || 
            //                         e.DESCRIPTION.Contains(";;;6;;;") || 
            query = query.Where(e =>
                medclass.Any(item1 => e.DESCRIPTION.Contains(";;;" + item1 + ";;;")) ||
               pamed1417.Any(item2 => e.DESCRIPTION.ToUpper().StartsWith(item2.ToUpper())));
            query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;given") || e.DESCRIPTION.ToLower().Contains(";;;syringe") || e.DESCRIPTION.ToLower().ContainsAny(meds_rate) && (e.UNIT_ID == _pat.unit_id) || e.DESCRIPTION.ToLower().Contains(";;;new"));
            query = query.Where(e => e.EVENT_DATETIME <= PFSUtility.MaxDateTime(_pat.unit_departure, _pat.effective_out));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            //            Program.VerboseAudit("Num Meds found: " + query.Count());
            foreach (var item in query)
            {
                var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                if (arr.GetUpperBound(0) >= 3)
                {
                    descript = arr[0];
                    drugclass = arr[1];
                    rte = arr[3];
                    result = "";
                    if (arr.Length == 5) result = arr[4];
                }
                //Program.VerboseAudit("med desc: " + descript + "//" + "med class: " + drugclass + "//" + "med result: " + result);
                //exclude #29, 35, 48
                bres1 = false;
                bres2 = false;
                bclass1 = false;
                bclass2 = false;
                bclass3 = false;
                bmed1 = false;

                bres1 = result.ToLower().Contains("given");
                bres2 = result.ToLower().Contains("syringe") || result.ToLower().ContainsAny(meds_rate) || result.ToLower().Contains("newbag") || result.ToLower().Contains("new bag");
                bclass1 = (drugclass == "1") || (drugclass == "2") || (drugclass == "6");
                bclass2 = (drugclass == "7");
                bclass3 = false; // (drugclass == "33") && (rte.ToLower() == "sc");
                bmed1 = pamed1417.Any(item2 => descript.ToUpper().StartsWith(item2.ToUpper()));
                if ((bres1 && (bclass2 || bclass3)) ||
                    ((bres1 || bres2) && bclass1) ||
                    bmed1)
                {
                    //Program.VerboseAudit("====Med found: bucket count A====");
                    //Program.VerboseAudit("med code: " + item.CODE + " desc: " + item.DESCRIPTION + "  evdt: " + item.EVENT_DATETIME.ToString());
                    SetInd(15, "Med class 7,1,2,6 or named med");
                }
            }

            // get the chart items for the assessments
            query = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
            query = query.Where(e => e.CODE.ToLower().StartsWith("med"));
            //query = query.Where(e => e.DESCRIPTION.Contains(";;;7;;;") || e.DESCRIPTION.Contains(";;;33;;;") || e.DESCRIPTION.Contains(";;;1;;;") || e.DESCRIPTION.Contains(";;;2;;;") || e.DESCRIPTION.Contains(";;;6;;;"));
            query = query.Where(e =>
               medclass.Any(item1 => e.DESCRIPTION.Contains(";;;" + item1 + ";;;")) ||
               pamed1417.Any(item2 => e.DESCRIPTION.ToUpper().StartsWith(item2.ToUpper())));
            query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;given") || e.DESCRIPTION.ToLower().Contains(";;;syringe") || e.DESCRIPTION.ToLower().ContainsAny(meds_rate) && (e.UNIT_ID == _pat.unit_id) || e.DESCRIPTION.ToLower().Contains(";;;new"));
            query = query.Where(e => e.EVENT_DATETIME <= PFSUtility.MaxDateTime(_pat.unit_departure, _pat.effective_out));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            //            Program.VerboseAudit("Num Meds found: " + query.Count());
            foreach (var item in query)
            {
                var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                if (arr.GetUpperBound(0) >= 3)
                {
                    descript = arr[0];
                    drugclass = arr[1];
                    rte = arr[3];
                    result = "";
                    if (arr.Length == 5) result = arr[4];
                }
                //Program.VerboseAudit("med desc: " + descript + "//" + "med class: " + drugclass + "//" + "med result: " + result);
                //exclude #29, 35, 48
                bres1 = false;
                bres2 = false;
                bclass1 = false;
                bclass2 = false;
                bclass3 = false;
                bmed1 = false;

                bres1 = result.ToLower().Contains("given");
                bres2 = result.ToLower().Contains("syringe") || result.ToLower().ContainsAny(meds_rate) || result.ToLower().Contains("newbag") || result.ToLower().Contains("new bag");
                bclass1 = (drugclass == "1") || (drugclass == "2") || (drugclass == "6");
                bclass2 = (drugclass == "7");
                bclass3 = false;  //(drugclass == "33") && (rte.ToLower() == "sc");
                bmed1 = pamed1417.Any(item2 => descript.ToUpper().StartsWith(item2.ToUpper()));
                if ((bres1 && (bclass2 || bclass3)) ||
                    ((bres1 || bres2) && bclass1) ||
                    bmed1)
                {
                    //Program.VerboseAudit("====Med found: bucket count B====");
                    //Program.VerboseAudit("med code: " + item.CODE + " desc: " + item.DESCRIPTION + "  evdt: " + item.EVENT_DATETIME.ToString());
                    //var query2 = (from item2 in query select new { item2.EVENT_DATETIME, item2.CODE });
                    //var query3 = from item3 in query2
                    //             select new
                    //             {
                    //                 bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item3.EVENT_DATETIME) / _bucket_size),
                    //                 code = item3.CODE,
                    //                 evdt = item3.EVENT_DATETIME
                    //             };
                    //foreach (var itemx in query3)
                    //{
                    //    var b = new gBucket();
                    //    b.bucket = itemx.bucket;
                    //    b.code = itemx.code;
                    //    b.evdt = itemx.evdt;
                    //    bucket_list.Add(b);
                    //}
                    var b = new gBucket();
                    b.bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size);
                    b.code = item.CODE;
                    b.evdt = item.EVENT_DATETIME;
                    b.has_all_deps = true;
                    gBucket f = bucket_list.Find(x => x.evdt == item.EVENT_DATETIME);
                    if (f.evdt != item.EVENT_DATETIME) bucket_list.Add(b);
                }
            }
        }

        private void AddMedBucketsDrugClass33(List<gBucket> bucket_list)
        {
            string descript = "";
            string drugclass = "";
            string rte = "";
            string result = "";

            bool bres1 = false;
            bool bres2 = false;
            bool bclass1 = false;
            bool bclass2 = false;
            bool bclass3 = false;
            bool bmed1 = false;

            var query = StartNewQuery(SearchDepth.SearchSince13Hrs);    // add custom time range below
            query = query.Where(e => e.CODE.ToLower().StartsWith("med"));
            //query = query.Where(e => e.DESCRIPTION.Contains(";;;7;;;") || 
            //                         e.DESCRIPTION.Contains(";;;33;;;") || 
            //                         e.DESCRIPTION.Contains(";;;1;;;") || 
            //                         e.DESCRIPTION.Contains(";;;2;;;") || 
            //                         e.DESCRIPTION.Contains(";;;6;;;") || 
            query = query.Where(e => e.DESCRIPTION.Contains(";;;33;;;"));
            query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;given") || e.DESCRIPTION.ToLower().Contains(";;;syringe") || e.DESCRIPTION.ToLower().ContainsAny(meds_rate) && (e.UNIT_ID == _pat.unit_id) || e.DESCRIPTION.ToLower().Contains(";;;new"));
            query = query.Where(e => e.EVENT_DATETIME <= PFSUtility.MaxDateTime(_pat.unit_departure, _pat.effective_out));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            //            Program.VerboseAudit("Num Meds found: " + query.Count());
            foreach (var item in query)
            {
                var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                if (arr.GetUpperBound(0) >= 3)
                {
                    descript = arr[0];
                    drugclass = arr[1];
                    rte = arr[3];
                    result = "";
                    if (arr.Length == 5) result = arr[4];
                }
                //Program.VerboseAudit("med desc: " + descript + "//" + "med class: " + drugclass + "//" + "med result: " + result);
                //exclude #29, 35, 48
                bres1 = false;
                bres2 = false;
                bclass1 = false;
                bclass2 = false;
                bclass3 = false;
                bmed1 = false;

                bres1 = result.ToLower().Contains("given");
                bclass3 = (drugclass == "33") && (rte.ToLower() == "sc");
                if (bres1 && bclass3)
                {
                    //Program.VerboseAudit("====Med found: Drug class 33 with Route=SC ==direct trigger for q4==");
                    //Program.VerboseAudit("med code: " + item.CODE + " desc: " + item.DESCRIPTION + "  evdt: " + item.EVENT_DATETIME.ToString());
                    SetInd(15, "Med class 33");
                }
            }

            // get the chart items for the assessments
            query = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
            query = query.Where(e => e.CODE.ToLower().StartsWith("med"));
            //query = query.Where(e => e.DESCRIPTION.Contains(";;;7;;;") || e.DESCRIPTION.Contains(";;;33;;;") || e.DESCRIPTION.Contains(";;;1;;;") || e.DESCRIPTION.Contains(";;;2;;;") || e.DESCRIPTION.Contains(";;;6;;;"));
            query = query.Where(e => e.DESCRIPTION.Contains(";;;33;;;"));
            query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;given") || e.DESCRIPTION.ToLower().Contains(";;;syringe") || e.DESCRIPTION.ToLower().ContainsAny(meds_rate) && (e.UNIT_ID == _pat.unit_id) || e.DESCRIPTION.ToLower().Contains(";;;new"));
            query = query.Where(e => e.EVENT_DATETIME <= PFSUtility.MaxDateTime(_pat.unit_departure, _pat.effective_out));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            //            Program.VerboseAudit("Num Meds found: " + query.Count());
            foreach (var item in query)
            {
                var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                if (arr.GetUpperBound(0) >= 3)
                {
                    descript = arr[0];
                    drugclass = arr[1];
                    rte = arr[3];
                    result = "";
                    if (arr.Length == 5) result = arr[4];
                }
                //Program.VerboseAudit("med desc: " + descript + "//" + "med class: " + drugclass + "//" + "med result: " + result);
                //exclude #29, 35, 48
                bres1 = false;
                bres2 = false;
                bclass1 = false;
                bclass2 = false;
                bclass3 = false;
                bmed1 = false;

                bres1 = result.ToLower().Contains("given");
                bclass3 = (drugclass == "33") && (rte.ToLower() == "sc");
                if (bres1 && bclass3)
                {
                    //Program.VerboseAudit("====Med found: Drug class 33 with Route=SC ==adding to freq eval==");
                    //Program.VerboseAudit("med code: " + item.CODE + " desc: " + item.DESCRIPTION + "  evdt: " + item.EVENT_DATETIME.ToString());
                    //var query2 = (from item2 in query select new { item2.EVENT_DATETIME, item2.CODE });
                    //var query3 = from item3 in query2
                    //             select new
                    //             {
                    //                 bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item3.EVENT_DATETIME) / _bucket_size),
                    //                 code = item3.CODE,
                    //                 evdt = item3.EVENT_DATETIME
                    //             };
                    //foreach (var itemx in query3)
                    //{
                    //    var b = new gBucket();
                    //    b.bucket = itemx.bucket;
                    //    b.code = itemx.code;
                    //    b.evdt = itemx.evdt;
                    //    bucket_list.Add(b);
                    //}
                    var b = new gBucket();
                    b.bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size);
                    b.code = item.CODE;
                    b.evdt = item.EVENT_DATETIME;
                    b.has_all_deps = true;
                    gBucket f = bucket_list.Find(x => x.evdt == item.EVENT_DATETIME);
                    if (f.evdt != item.EVENT_DATETIME) bucket_list.Add(b);
                }
            }
        }


        private int CountBuckets(List<gBucket> bucket_list)       // bucket list: (ha ha)
        {
            //int result = bucket_list.Distinct().Count();
            //if (result > 0) Program.VerboseAudit(result + " unique");
            //return result;
            int x = -99;
            int result = 0;
            //int result = bucket_list.Distinct().Count();
            var query = from b in bucket_list
                        orderby b.bucket ascending
                        select b;
            foreach (var b in query)
            {
                if (x != b.bucket)
                {
                    result++;
                    x = b.bucket;
                }
            }
            if (result > 0) Program.VerboseAudit(result + " unique");
            return result;
        }
        private void Check_19()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("19. Vascular Access Site Mgt q1 Hour");
            Program.VerboseAudit("---------------");

            exclude_periop_data = true;

            SetIndIfResultContains(19, "", "3045001087", "", "", "", SearchDepth.SearchSince9Hrs);

            if (_pat.age < 15.0)
            {
                if (CheckRateVerify() > 0)
                    SetInd(19, "Med with Rate for age=" + _pat.age.ToString());
            }

            //int ct = 0;
            //ct = CountResultContains("", "7096770", "", "", "Cap changed,Connections checked and tightened,Line pulled back,Tubing changed,Zeroed and calibrated,Leveled,Transducer changed,Other");
            //if (IsQ1Hour(ct))
            //    SetInd(19, "Line care intaosseous= " + ct);
            int ct = 0;
            SetIndIfResultContains(19, "", "3041150001701", "", "", "", SearchDepth.SearchSince9Hrs);

            string reslist = "Continuous veno-venous hemofiltration,Continuous veno-venous hemodialysis";
            reslist += ",Continuous veno-venous hemodiafiltration,Slow continuous ultrafiltration";
            reslist += ",CVVH,CVVHD,CVVHDF,SCUF,Other";
            SetIndIfResultContains(19, "", "9990008100010", "", "", reslist, SearchDepth.SearchSince9Hrs);

            reslist = "Initiated,Continuous,Restarted";
            SetIndIfResultContains(19, "", "9990008100020", "", "", reslist, SearchDepth.SearchSince9Hrs);

            SetIndIfResultContains(19, "", "9993041001004", "", "", "", SearchDepth.SearchSince9Hrs);
            SetIndIfResultContains(19, "", "9993041001003", "", "", "", SearchDepth.SearchSince9Hrs);

            if (!_inds[19].is_checked)
            {
                ct = CountKCl();
                if (ct >= 3) SetInd(19, "KCl New Bag found count=" + ct);
            }

            // THIS IS IN CHECKMED16:   Vasc Access q1 if all 3 of these are present: 
            //•	16(Drug Class)
            //•	IV(Route)
            //•	Started / Down
            //OBX | 1 | DT | MED124656 ^ ETOPOSIDE 20 MG / ML INTRAVENOUS SOLUTION; ; ; 16; ; ; 180; ; ; IV; ; ; Started / Down | Started / Down | 20190709133600 |||||| F ||| 20190709133600

            SetIndIfResultContains(19, "", "9993040000125", "", "", "", SearchDepth.SearchSince9Hrs);

            CheckRouteCath();

        }

        private void CheckRouteCath()
        {
            int ct = 0;
            //OBX|1|DT|MED9003 ^ ALTEPLASE 50 MG INTRAVENOUS SOLUTION;;;20;;;0.5;;;cath; ; ; RateVerify | RateVerify | 20190802000000 |||||| F ||| 20190802000000
            //OBX|1|DT|MED9003 ^ HEPARIN  MG INTRAVENOUS SOLUTION;;;20;;;0.5;;;cath; ; ; RateVerify | RateVerify | 20190802000000 |||||| F ||| 20190802000000
            var query = StartNewQuery(SearchDepth.SearchSince9Hrs);    // add custom time range below
            query = query.Where(e => e.CODE.StartsWith("med"));
            query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;cath"));
            query = query.Where(e => e.DESCRIPTION.ToUpper().Contains("ALTEPLASE") ||
                                     e.DESCRIPTION.ToUpper().Contains("TENECTEPLASE") ||
                                     e.DESCRIPTION.ToUpper().Contains("HEPARIN"));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            //Program.VerboseAudit("Med ALTEPLASE or HEPARIN with cath found: " + query.Count());
            foreach (var item in query)
            {
                //  Program.VerboseAudit("====Med found: ALTEPLASE or HEPARIN====");
                //Program.VerboseAudit("med code: " + item.CODE + " desc: " + item.DESCRIPTION + "  evdt: " + item.EVENT_DATETIME.ToString());

                var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                if (arr.GetUpperBound(0) >= 3)
                {
                    string rte = arr[3];
                    if (rte.ToLower() == "cath") ct++;
                }
            }
            if (ct > 0)
                SetInd(19, "Found ALTEPLASE or TENECTEPLASE or HEPARIN with route cath.");
        }

        private int CheckRateVerify()
        {
            string rte = "";
            string result = "";
            int ct = 0;
            bool route_ok = false;

            string[] list_routes = { "IV","IART","IO","epid","ARTERIAL","osse",
                                    "spin","cepd","cArt","ciOs","citi","sheath" };

            var query = StartNewQuery(SearchDepth.SearchSince9Hrs);    // add custom time range below
            query = query.Where(e => e.CODE.StartsWith("med"));
            query = query.Where(e => (e.DESCRIPTION.ToLower().ContainsAny(meds_rate) && e.UNIT_ID == _pat.unit_id) || e.DESCRIPTION.ToLower().Contains("newbag") || e.DESCRIPTION.ToLower().Contains("new bag"));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            //Program.VerboseAudit("Total Med with rateverify found: " + query.Count());
            foreach (var item in query)
            {
                route_ok = false;
                //  Program.VerboseAudit("====Med found: rate verify====");
                //Program.VerboseAudit("med code: " + item.CODE + " desc: " + item.DESCRIPTION + "  evdt: " + item.EVENT_DATETIME.ToString());

                var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                if (arr.GetUpperBound(0) >= 4)
                {
                    result = arr[4];
                    rte = arr[3];
                    foreach (var r in list_routes)
                    {
                        if (rte.ToLower().StartsWith(r.ToLower()))
                            route_ok = true;
                    }
                    if (result.ToLower().ContainsAny(meds_rate) && route_ok)
                    {
                        ct++;
                    }
                }
            }
            if (ct > 0)
                Program.VerboseAudit("Found Med with RateVerify/restarted/ratechange/newbag + GoodRoute: count=" + ct);
            return ct;
        }


        private int CheckMR20to77()
        {
            string descript = "";
            string result = "";
            int ct = 0;
            int medindex;

            //        private string[] meds_mr2026 = { "FENTANYL","HYDROMORPHONE","MORPHINE","DILTIAZEM",
            //                "DOPAMINE","NICARDIPINE","NITROGLYCERIN" };
            //        private bool[] found_med2026 = new bool[7];
            //        private string[] meds_mr5077 = {"ADENOSINE","ALPROSTADIL",
            //"CLEVIDIPINE","DEXMEDETOMIDINE","EPINEPHRINE","EPOPROSTENOL","ESMOLOL",
            //"ISOPROTERENOL","KETAMINE","LABETALOL","LIDOCAINE","LORAZEPAM",
            //"MIDAZOLAM","NALOXONE","NESIRITIDE","NITROPRUSSIDE","NOREPINEPHRINE",
            //"PENTOBARBITAL","PHENYLEPHRINE","PROCAINAMIDE","PROPOFOL","TACROLIMUS",
            //"TERBUTALINE","THEOPHYLLINE","TORSEMIDE","TREPROSTINIL","VASOPRESSIN","VERAPAMIL" };
            //        private bool[] found_med5077 = new bool[28];
            //        private string[] meds_rate = { "rateverify", "restarted", "ratechange" };

            med2026and5077[] found_med5077 = new med2026and5077[meds_mr5077.Length];
            med2026and5077[] found_med2026 = new med2026and5077[meds_mr2026.Length];

            for (int i = 0; i < found_med2026.Length; ++i)
            {
                found_med2026[i].found = false;
                found_med2026[i].name = meds_mr2026[i];
            }
            for (int i = 0; i < found_med5077.Length; ++i)
            {
                found_med5077[i].found = false;
                found_med5077[i].name = meds_mr5077[i];
            }

            var query = StartNewQuery(SearchDepth.SearchSince9Hrs);    // add custom time range below
            query = query.Where(e => e.CODE.StartsWith("med"));
            query = query.Where(e => e.UNIT_ID == _pat.unit_id);
            query = query.Where(e => (meds_mr2026.Any(item => e.DESCRIPTION.ToUpper().StartsWith(item))
                                   || meds_mr5077.Any(item2 => e.DESCRIPTION.ToUpper().StartsWith(item2)))
                                   && e.DESCRIPTION.ToLower().ContainsAny(meds_rate));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            //Program.VerboseAudit("Total Meds MR20-MR84 found: " + query.Count());
            foreach (var item in query)
            {
                //  Program.VerboseAudit("====Med found: 20-84====");
                //Program.VerboseAudit("med code: " + item.CODE + " desc: " + item.DESCRIPTION + "  evdt: " + item.EVENT_DATETIME.ToString());

                var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                if (arr.GetUpperBound(0) >= 4)
                {
                    result = arr[4];
                    if (result.ToLower().ContainsAny(meds_rate))
                    {
                        descript = arr[0];
                        foreach (var med in meds_mr2026)
                        {
                            if (descript.ToUpper().StartsWith(med))
                            {
                                medindex = Array.IndexOf(meds_mr2026, med);
                                found_med2026[medindex].found = true;
                            }
                        }
                        foreach (var med in meds_mr5077)
                        {
                            if (descript.ToUpper().StartsWith(med))
                            {
                                medindex = Array.IndexOf(meds_mr5077, med);
                                found_med5077[medindex].found = true;
                            }
                        }
                    }
                }
            }
            string[] peds_meds = { "DILTIAZEM", "DOPAMINE", "NICARDIPINE", "NITROGLYCERIN", "DOBUTAMINE", "LEVETIRACETAM", "MILRINONE" };
            for (int i = 0; i <= found_med2026.GetUpperBound(0); i++)
            {
                if (found_med2026[i].found)
                {
                    if (_pat.age < 9.0)
                    {
                        if (meds_mr2026[i].ContainsAny(peds_meds))
                            SetInd(18, "Found Med Rate " + meds_mr2026[i] + " for age: " + _pat.age);
                    }

                }
            }
            //Get the total number of unique meds; dont double count the meds that exist in both arrays.
            foreach (var m5077 in found_med5077)
            {
                if (m5077.found)
                {
                    ct++;
                    for (int i = 0; i <= found_med2026.GetUpperBound(0); i++)
                        if (m5077.name == found_med2026[i].name)
                            found_med2026[i].found = false;
                }
            }
            foreach (var m2026 in found_med2026)
            {
                if (m2026.found) ct++;
            }
            if (ct > 0)
                Program.VerboseAudit("Found MR-20 - MR-80 unique count=" + ct);
            return ct;
        }

        private int CheckMR5077()
        {
            string result = "";
            int ct = 0;
            //            OBX | 1 | DT | MED121505 ^ EPINEPHRINE 1 MG / ML INJECTION SOLUTION; ; ; 18; ; ; 0.02; ; ; IV; ; ; RateVerify | RateVerify | 20190118070000 |||||| F ||| 20190118070000
            //OBX | 1 | DT | MED93084 ^ AMIODARONE 50 MG / ML INTRAVENOUS SOLUTION; ; ; 22; ; ; 0.5; ; ; IV; ; ; RateVerify | RateVerify | 20190118070000 |||||| F ||| 20190118070000

            //So for the yellow:
            //Working on double checking the list of med names and then I will update the table.
            //In the second spot following the ; ; ; there will be any number including “0”.
            //Then in the OBX 5 spot, it will say Rate Verify when the patient is on a drip that is being titrated.This should eliminate any one time pushes, etc. as they would not show as a Rate Verify.
            var query = StartNewQuery(SearchDepth.SearchSince9Hrs);    // add custom time range below
            query = query.Where(e => e.CODE.StartsWith("med"));
            query = query.Where(e => e.UNIT_ID == _pat.unit_id);
            query = query.Where(e => meds_mr5077.Any(item2 => e.DESCRIPTION.ToUpper().StartsWith(item2)) && e.DESCRIPTION.ToLower().ContainsAny(meds_rate));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            //Program.VerboseAudit("Num Meds MR-50-80 found: " + query.Count());
            foreach (var item in query)
            {
                //Program.VerboseAudit("====Med found MR5080====");
                //Program.VerboseAudit("med code: " + item.CODE + " desc: " + item.DESCRIPTION + "  evdt: " + item.EVENT_DATETIME.ToString());

                var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                if (arr.GetUpperBound(0) >= 4)
                {
                    result = arr[4];
                    if (result.ToLower().ContainsAny(meds_rate))
                    {
                        ct++;
                    }
                }
            }
            if (ct > 0)
                Program.VerboseAudit("Found Med MR-50-80 RateVerify count=" + ct);
            return ct;
        }

        private int CheckInsulin()
        {
            string result = "";
            int ct = 0;

            var query = StartNewQuery(SearchDepth.SearchSince9Hrs);    // add custom time range below
            query = query.Where(e => e.CODE.StartsWith("med"));
            query = query.Where(e => e.UNIT_ID == _pat.unit_id);
            query = query.Where(e => e.DESCRIPTION.ToUpper().StartsWith("INSULIN") && e.DESCRIPTION.ToLower().ContainsAny(meds_rate));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            //Program.VerboseAudit("Num Meds MR-1 found: " + query.Count());
            foreach (var item in query)
            {
                //  Program.VerboseAudit("====Med found: insulin====");
                //Program.VerboseAudit("med code: " + item.CODE + " desc: " + item.DESCRIPTION + "  evdt: " + item.EVENT_DATETIME.ToString());

                var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                if (arr.GetUpperBound(0) >= 4)
                {
                    result = arr[4];
                    if (result.ToLower().ContainsAny(meds_rate))
                    {
                        ct++;
                    }
                }
            }
            if (ct > 0)
                SetInd(17, "Found Insulin MR-1 RateVerify count=" + ct);
            return ct;
        }
        //Atracurium,Cisatracurium,Pancuronium,Rocuronium,Vecuronium
        private int CheckMR1014()
        {
            string result = "";
            int ct = 0;
            string[] meds_mr1014 = { "Atracurium", "Cisatracurium", "Pancuronium", "Rocuronium", "Vecuronium" };

            var query = StartNewQuery(SearchDepth.SearchSince9Hrs);    // add custom time range below
            query = query.Where(e => e.CODE.StartsWith("med"));
            query = query.Where(e => e.UNIT_ID == _pat.unit_id);
            query = query.Where(e => meds_mr1014.Any(item2 => e.DESCRIPTION.ToUpper().StartsWith(item2.ToUpper())) && e.DESCRIPTION.ToLower().ContainsAny(meds_rate));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            Program.VerboseAudit("Num Meds MR-10-14 found: " + query.Count());
            foreach (var item in query)
            {
                //  Program.VerboseAudit("====Med found: MR 10-14====");
                //Program.VerboseAudit("med code: " + item.CODE + " desc: " + item.DESCRIPTION + "  evdt: " + item.EVENT_DATETIME.ToString());

                var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                if (arr.GetUpperBound(0) >= 4)
                {
                    result = arr[4];
                    if (result.ToLower().ContainsAny(meds_rate))
                    {
                        ct++;
                    }
                }
            }
            if (ct > 0)
                Program.VerboseAudit("Found Med MR-10-14 RateVerify count=" + ct);
            return ct;
        }

        private int CountKCl()
        {
            string descript = "";
            string drugclass = "";
            string rte = "";
            string result = "";
            int ct = 0;
            //MED____ ^ POTASSIUM CHLORIDE; ; ; __; ; ; __; ; ; IV

            //  CALCIUM CHLORIDE 100 MG / ML(10 %) INTRAVENOUS SYRINGE; ; ; 29; ; ; 1; ; ; IV; ; ; Given
            //  SODIUM BICARBONATE 8.4 % (1 MEQ / ML) INJECTION(WRAPPER); ; ; 29; ; ; 50; ; ; osse; ; ; Given
            var query = StartNewQuery(SearchDepth.SearchSince9Hrs);    // add custom time range below
            query = query.Where(e => e.CODE.StartsWith("med"));
            query = query.Where(e => e.DESCRIPTION.Contains("potassium chloride"));
            query = query.Where(e => e.EVENT_DATETIME <= PFSUtility.MaxDateTime(_pat.unit_departure, _pat.effective_out));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            //Program.VerboseAudit("Num Meds found: " + query.Count());
            foreach (var item in query)
            {
                //  Program.VerboseAudit("====Med found: KCl====");
                //Program.VerboseAudit("med code: " + item.CODE + " desc: " + item.DESCRIPTION + "  evdt: " + item.EVENT_DATETIME.ToString());

                var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                if (arr.GetUpperBound(0) >= 3)
                {
                    descript = arr[0];
                    drugclass = arr[1];
                    rte = arr[3];
                    if (arr.Length == 5) result = arr[4];
                }
                //Program.VerboseAudit("med desc: " + descript + "//" + "med class: " + drugclass + "//" + "med result: " + result);
                //exclude #29, 35, 48
                if ((result.ToLower() == "newbag" || result.ToLower() == "new bag") && (rte.ToLower() == "iv"))
                {
                    ct++;
                }
            }
            if (ct > 0)
                Program.VerboseAudit("KCl New Bag found count=" + ct);
            return ct;
        }

        private void Check_20()
        {
            string reslist;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("20. Medication Activity >= 20 minutes");
            Program.VerboseAudit("---------------");

            exclude_periop_data = true;

            CheckChemo16();
            if (_inds[20].is_checked) return;

            //int medct = CountMeds();
            //if (medct >= 5)
            //{
            //    reslist = "Coughs with swallowing,Unable to swallow";
            //    SetIndIfResultContains(20, "", "9990000002113", "", "", reslist);
            //reslist = "3-Unable to swallow";
            //SetIndIfResultContains(20, "", "9990304000118", "", "", reslist);
            //reslist = "No signs or symptoms";
            //if (!Exists("", "9993040108724", "", "", reslist))
            //{
            //    reslist = "Delayed swallow or interrupted swallow,Coughs, clears throat, chokes, gags up to 1 min after drinking,Unable to say,Wet voice when saying,Drools after swallowing,Desaturates 2,Swallows more than one time after drinking,Unable to assess";
            //    SetIndIfResultContains(20, "", "9993040108712", "", "", reslist);
            //}
            //if (!Exists("", "9993040108725", "", "", reslist))
            //{
            //    reslist = "Delayed swallow or interrupted swallow,Coughs, clears throat, chokes, gags up to 1 min after drinking,Unable to say,Wet voice when saying,Drools after swallowing,Desaturates 2,Swallows more than one time after drinking,Unable to assess";
            //    SetIndIfResultContains(20, "", "9993040108714", "", "", reslist);
            //}
            //}
            reslist = "Cryoprecipitate,Fresh Frozen Plasma,Platelets,Red Blood Cells,Other";
            SetIndIfResultContains(20, "", "9993040004071", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(20, "", "3041150001701", "", "", reslist);
            SetIndIfResultContains(20, "", "9993040401122", "", "", reslist);

            SetIndIfResultContains(20, "", "9993047000000", "", "", "Yes,1");

            //MED____^_____;;;__;;;___;;;gastric tube
            //MED____ ^ _____; ; ; __; ; ; ___; ; ; sm bowel tu"
            //            "Given
            //Given / Down
            //Given / Other"
            //LEVOTHYROXINE 50 MCG TABLET; ; ; 40; ; ; 50; ; ; gastric tube; ; ; Given
            //MIDODRINE 5 MG TABLET; ; ; 18; ; ; 10; ; ; sm bowel tu; ; ; Given
            int ct = 0;
            if (!_inds[20].is_checked)
            {
                ct = CheckGastricTube();
                if (ct >= 5) SetInd(20, "Med via Gastric or Sm Bowel Tube");
            }
            SetIndIfResultContains(20, "", "9990000042410", "", "", "");
            SetIndIfResultContains(20, "", "99900042410", "", "", "");
            CheckMedGiven(20, "Mycophenolate");
            CheckMedGiven(20, "Tacrolimus");

            SetIndIfResultContains(20, "", "9990000042640,990000042845,9990000042983,9990042776", "", "", "");

            CheckEyeDrops();

        }

        private void CheckEyeDrops()
        {
            //OBX|1|DT|MEDNOEICUID ^ AMPHOTERICIN B 1.5 MG / ML(0.15 %) OPHTHALMIC SOLUTION (CNR);;;13;;;1;;;left eye;;;Given | Given | 20190731145800 |||||| F ||| 20190731145800
            //Include when 5 results of Given when the drug route found in OBX 3 after the third; ; ; is one of the options under code
            //Both eyes,Left eye,Right eye,Opht
            string[] eyeroute = { "Both eyes", "Left eye", "Right eye", "Opht" };

            int ct = 0;
            var query = StartNewQuery(SearchDepth.SearchSince9Hrs);    // add custom time range below
            query = query.Where(e => e.CODE.StartsWith("med"));
            query = query.Where(e => eyeroute.Any(x =>
                e.DESCRIPTION.ToUpper().Contains(";;;" + x.ToUpper() + ";;;GIVEN")));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            //Program.VerboseAudit("Meds found with EYE route: " + query.Count());
            if (query.Count() >= 5)
            {
                foreach (var item in query)
                {
                    //      Program.VerboseAudit("med code: " + item.CODE + " desc: " + item.DESCRIPTION + "  evdt: " + item.EVENT_DATETIME.ToString());
                    ct++;
                }
            }
            if (ct >= 5)
                SetInd(20, "Found Eye Drops med count=" + ct);
        }

        private void CheckMedGiven(int ind, string desc)
        {

            string[] transplantmed_actions = { "given", "newbag", "new bag", "rateverify", "rate verify", "rate change", "ratechange", "restarted", "started/down" };

            for (int i = 0; i <= transplantmed_actions.GetUpperBound(0); i++)
            {
                transplantmed_actions[i] = ";;;" + transplantmed_actions[i];
            }

            int ct = 0;
            var query = StartNewQuery(SearchDepth.SearchSince9Hrs);
            query = AndItemFilter(query, "", "MED", desc, "", "");
            query = query.Where(e => e.DESCRIPTION.ToLower().ContainsAny(transplantmed_actions));
            ct = query.Count();
            if (ct > 0) SetInd(ind, "Found Med=" + desc);

        }

        private void CheckChemo16()
        {
            string descript = "";
            string drugclass = "";
            string rte = "";
            string result = "";
            bool done = false;
            int lookback19 = -9;

            string sql = "select ci.code,ci.description,ci.event_datetime from chart_item as ci";
            sql += " where ci.encounter_id=" + _pat.encounter_id + " and ci.code like 'MED%'";
            sql += " and ci.description like '%;;;16%'";
            sql += " and ci.event_datetime >='" + _pat.pull_finish.AddHours(-48).ToString() + "'";
            sql += " order by ci.event_datetime desc";
            //Program.VerboseAudit(sql);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd = new SqlCommand(sql, db2);
            SqlDataReader dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            var mrec = new med_data();
            bool eval20done = false;
            bool date12 = false;
            while (dr2.Read() && !done)
            {
                mrec.code = PFSDBUtility.DBToString(dr2["CODE"]);
                mrec.descript = PFSDBUtility.DBToString(dr2["DESCRIPTION"]);
                mrec.evdt = PFSDBUtility.DBToDateTime(dr2["EVENT_DATETIME"]);
                if (eval20done)
                    date12 = (mrec.evdt >= _pat.pull_finish.AddHours(lookback19));
                if (!eval20done || date12)
                {
                    var arr = mrec.descript.Split(new string[] { ";;;" }, StringSplitOptions.None);
                    if (arr.GetUpperBound(0) >= 1)
                    {
                        descript = arr[0];
                        drugclass = arr[1];

                        if (drugclass == "16")
                        {
                            if (!eval20done)
                            {
                                SetInd(20, "Found Chemo Med drugclass 16 code=" + mrec.code + " descr=" + mrec.descript + " at=" + mrec.evdt.ToString());
                                eval20done = true;
                                date12 = (mrec.evdt >= _pat.pull_finish.AddHours(lookback19));
                            }
                            if (date12)
                            {
                                if (arr.GetUpperBound(0) >= 3)
                                {
                                    rte = arr[3];
                                    if (arr.Length == 5) result = arr[4];
                                    if ((rte.ToLower() == "iv") && (result.ToLower().ContainsAny(meds_rate)))
                                    {
                                        SetInd(19, "Found Chemo Med drugclass 16/IV/started down/code=" + mrec.code + " descr=" + mrec.descript + " at=" + mrec.evdt.ToString());
                                        done = true;
                                    }
                                }
                            }

                        }

                    }
                }
            }
            dr2.Close();
            db2.Close();
            //Program.VerboseAudit("Chemo drugclass 16");
        }

        private int CheckGastricTube()
        {
            string descript = "";
            string drugclass = "";
            string rte = "";
            string result = "";
            int ct = 0;

            string[] list_gastric_tubes = { "gastric tube","sm bowel tu","gtt","ng","ftub",
                                        "pegt","g-tube","jtub","ng-tube","tube",
                                        "nj-tube","og-tube","pegj","ngtb" };

            //     gastric tube,sm bowel tu,GTT,NG,ftub,pegt,g-tube,jtub,ng-tube,Tube,nj-tube,og-tube,pegj,NGtb


            //LEVOTHYROXINE 50 MCG TABLET; ; ; 40; ; ; 50; ; ; gastric tube; ; ; Given
            //MIDODRINE 5 MG TABLET; ; ; 18; ; ; 10; ; ; sm bowel tu; ; ; Given

            var query = StartNewQuery(SearchDepth.SearchSince9Hrs);    // add custom time range below
            //Program.VerboseAudit("Num Since9hrs: " + query.Count());
            query = query.Where(e => e.CODE.ToLower().Contains("med"));
            //Program.VerboseAudit("Num Meds Since9hrs: " + query.Count());
            //query = query.Where(e => e.DESCRIPTION.ToLower().Contains("gastric tube") || e.DESCRIPTION.ToLower().Contains("sm bowel tu"));
            query = query.Where(e => list_gastric_tubes.Any(item => e.DESCRIPTION.ToLower().Contains(item)));
            //Program.VerboseAudit("Num Tube Meds Since9hrs: " + query.Count());
            query = query.Where(e => e.EVENT_DATETIME <= PFSUtility.MaxDateTime(_pat.unit_departure, _pat.effective_out));
            //Program.VerboseAudit("Num LessThanDepart Tube Meds Since9hrs: " + query.Count());
            query = query.OrderBy(e => e.EVENT_DATETIME);
            Program.VerboseAudit("Num Meds found: " + query.Count());
            foreach (var item in query)
            {
                //Program.VerboseAudit("====Med found: via tube====");
                //Program.VerboseAudit("med code: " + item.CODE + " desc: " + item.DESCRIPTION + "  evdt: " + item.EVENT_DATETIME.ToString());

                //HYDROMORPHONE 1 MG / ML ORAL LIQUID; ; ; 1; ; ; 0.5; ; ; sm bowel tu; ; ; Given
                var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                if (arr.GetUpperBound(0) >= 3)
                {
                    descript = arr[0];
                    drugclass = arr[1];
                    rte = arr[3];
                    if (arr.Length == 5) result = arr[4];
                }
                //Program.VerboseAudit("med desc: " + descript + "//" + "med class: " + drugclass + "//" + "med result: " + result);
                //exclude #29, 35, 48
                bool route_ok = false;
                foreach (var route in list_gastric_tubes)
                {
                    route_ok |= (rte.ToLower().StartsWith(route));
                }
                if (result.ToLower().Contains("given") && route_ok)
                {
                    ct++;
                }
            }

            //Program.VerboseAudit("Med via Tube");
            return ct;
        }

        private int CountMeds()
        {
            string descript = "";
            string drugclass = "";
            string result = "";
            int ct = 0;
            List<med_data> mlist = new List<med_data>();
            bool toadd = true;

            //  CALCIUM CHLORIDE 100 MG / ML(10 %) INTRAVENOUS SYRINGE; ; ; 29; ; ; 1; ; ; IV; ; ; Given
            //  SODIUM BICARBONATE 8.4 % (1 MEQ / ML) INJECTION(WRAPPER); ; ; 29; ; ; 50; ; ; osse; ; ; Given
            var query = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
            query = query.Where(e => e.CODE.StartsWith("med"));
            query = query.Where(e => e.EVENT_DATETIME <= PFSUtility.MaxDateTime(_pat.unit_departure, _pat.effective_out));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            //Program.VerboseAudit("Num Meds found: " + query.Count());
            foreach (var item in query)
            {
                //  Program.VerboseAudit("====Med found: count====");
                //Program.VerboseAudit("med code: " + item.CODE + " desc: " + item.DESCRIPTION + "  evdt: " + item.EVENT_DATETIME.ToString());

                var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                if (arr.GetUpperBound(0) >= 1)
                {
                    descript = arr[0];
                    drugclass = arr[1];
                    if (arr.Length == 5) result = arr[4];
                }
                //Program.VerboseAudit("med desc: " + descript + "//" + "med class: " + drugclass + "//" + "med result: " + result);
                //exclude #29, 35, 48
                if ((drugclass != "29") || (drugclass == "29" && (result.ToLower() == "given")) && (drugclass != "35") && (drugclass != "48"))
                {
                    var md = new med_data();
                    md.code = item.CODE.ToUpper();
                    md.descript = descript;
                    md.drugclass = drugclass;
                    md.result = result;
                    md.evdt = item.EVENT_DATETIME;
                    toadd = true;
                    //if (ct > 0)
                    //{ 
                    //    foreach (var m in mlist)
                    //    {
                    //        if (m.code == item.CODE.ToUpper()) toadd = false;
                    //    }
                    //}
                    if (toadd && (ct < 5))
                    {
                        mlist.Add(md);
                        ct++;
                    }

                }
            }

            int a = 0;
            //Program.VerboseAudit("Meds found=" + ct);
            foreach (var m in mlist)
            {
                //  Program.VerboseAudit("  " + a++ + ": code=" + m.code + "; drugclass=" + m.drugclass + "; time=" + m.evdt.ToString() + "; descript=" + m.descript + "; result=" + m.result);
            }
            return ct;
        }


        private void Check_21_22()
        {
            string reslist;
            bool st1 = false;
            string piv;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("21. Wound/Injury Mgmt");
            Program.VerboseAudit("22. Wound/Injury Mgmt >= 30 Minutes");
            Program.VerboseAudit("---------------");

            exclude_periop_data = false;

            st1 = ResultContains("", "9993041000044", "", "", EXACT_MATCH_PREFIX + "Stage I", SearchDepth.SearchSince16Hrs);

            piv = "Midline Dual Lumen Catheter,Midline Single Lumen Catheter,Peripheral IV";

            if (!st1)
            {
                reslist = "Stage II,Stage III,Stage IV,Unstageable,Deep tissue injury";
                SetIndIfResultContains(21, "", "9993041000044", "", "", reslist);

                reslist = "";
                SetIndIfResultContains(21, "", "9990000303750", "", "", reslist);
                SetIndIfResultContains(21, "", "9990007061190", "", "", reslist);
                SetIndIfResultContains(21, "", "9993046629812", "", "", reslist);
                SetIndIfResultContains(21, "", "9993040021267", "", "", reslist);
                SetIndIfResultContains(21, "", "9990007061270", "", "", reslist);

                SetIndIfResultContains(21, "", "9990000001493", "", "", reslist);
                SetIndIfResultContains(21, "", "9990000001494", "", "", reslist);
                SetIndIfResultContains(21, "", "9990007061240", "", "", reslist);
                SetIndIfResultContains(21, "", "9990000303780", "", "", reslist);
                SetIndIfResultContains(21, "", "9993040102846", "", "", reslist);
                SetIndIfResultContains(21, "", "9993040021269", "", "", reslist);

                SetIndIfResultContains(21, "", "9990000303800", NOT_PREFIX + piv, "", reslist);
                SetIndIfResultContains(21, "", "9990000303820", NOT_PREFIX + piv, "", reslist);
                SetIndIfResultContains(21, "", "9990000304850", NOT_PREFIX + piv, "", reslist);

                SetIndIfResultContains(21, "", "99930413000000,9993041000077,9993040021270,9990000303910", "", "", reslist);

            }

            reslist = "";
            SetIndIfResultContains(21, "", "9993040104036", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040104037", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040023765", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040100564", "", "", reslist);
            SetIndIfResultContains(21, "", "9991420100006", "", "", reslist);
            SetIndIfResultContains(21, "", "9991420100007", "", "", reslist);
            SetIndIfResultContains(21, "", "9991420100009", "", "", reslist);
            SetIndIfResultContains(21, "", "9990000304500", "", "", reslist);
            SetIndIfResultContains(21, "", "9990000304510", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040103946", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040103947", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040103948", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040103951", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040103954", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040103957", "", "", reslist);
            SetIndIfResultContains(21, "", "9990007070177", "", "", reslist);
            SetIndIfResultContains(21, "", "9990007070178", "", "", reslist);
            SetIndIfResultContains(21, "", "9990007070179", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040001021", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040000088", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040001022", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040103228", "", "", reslist);
            SetIndIfResultContains(21, "", "9990007085420", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040102851", "", "", reslist);
            SetIndIfResultContains(21, "", "9990007085260", "", "", reslist);

            SetIndIfResultContains(21, "", "9990000016070", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040102649", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040102644", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040102648", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040102660", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040102643", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040102662", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040102742", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040102748", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040102741", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040102744", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040102747", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040102750", "", "", reslist);
            SetIndIfResultContains(21, "", "9990000396120", "", "", reslist);
            SetIndIfResultContains(21, "", "9990007061290", "", "", reslist);
            SetIndIfResultContains(21, "", "9990000006333", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040108525", "", "", reslist);

            if (!st1)
            {
                SetIndIfResultContains(21, "", "3045001032", NOT_PREFIX + piv, "", reslist);
                SetIndIfResultContains(21, "", "9990007096660", NOT_PREFIX + piv, "", reslist);
                SetIndIfResultContains(21, "", "9990000304850", NOT_PREFIX + piv, "", reslist);
                SetIndIfResultContains(21, "", "9990007073550", NOT_PREFIX + piv, "", reslist);
            }

            SetIndIfResultContains(21, "", "3045001041,9990304301280,9993045001043", "", "", reslist);
            SetIndIfResultContains(21, "", "9990000301270", "", "", reslist);

            SetIndIfResultContains(21, "", "9990007080680", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040103205", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040103206", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040103209", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040000199", "", "", reslist);
            SetIndIfResultContains(21, "", "9990304840001", "", "", reslist);
            SetIndIfResultContains(21, "", "9990304840002", "", "", reslist);
            SetIndIfResultContains(21, "", "9990000370090", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040001044,9993040108605,9993040108606,9993040108607,9993040001046,9993040108614,9993040108615,9993040108616", "", "", reslist);
            SetIndIfResultContains(21, "", "9991180102055,9991180102056", "", "", "");

            reslist = "Traction";
            SetIndIfResultContains(21, "", "9990000304110", "", "", reslist);
            reslist = "Traction";
            SetIndIfResultContains(21, "", "9990000304130", "", "", reslist);
            reslist = "3-Ulceration with or without bleeding";
            SetIndIfResultContains(21, "", "9990304000122", "", "", reslist);
            reslist = "Ulcerations present";
            SetIndIfResultContains(21, "", "9990000002113", "", "", reslist);
            reslist = "Tea,Rusty,Peach,Cherry,Pink,Ketchup";
            SetIndIfResultContains(21, "", "9990000006298", "", "", reslist);
            reslist = "Peritoneal port";
            SetIndIfResultContains(21, "", "9993040401216", "", "", reslist);
            reslist = "Red,Pink,Pale,Dusky,Purple,Mottled,Ecchymotic,Soft,Firm";
            SetIndIfResultContains(21, "", "9993040001044", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "3040001333", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "3040001334", "", "", reslist);
            reslist = "Clean,No clot";
            SetIndIfResultContains(21, "", "9993040101378", "", "", reslist);
            reslist = "Done";
            SetIndIfResultContains(21, "", "9993040101377", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "9991020100022", "", "", reslist);
            reslist = "Vaginal pack,Uterine balloon,Uterotonic agent given (see MAR),Oxygen on,Legs elevated,MD notified,Bimanual exam per MD,Labs ordered";
            SetIndIfResultContains(21, "", "9991020100568", "", "", reslist);
            reslist = "Placed,Present,Removed";
            SetIndIfResultContains(21, "", "9991020100569", "", "", reslist);

            reslist = "Applied (comment number),Changed  (comment number),Marked,Reinforced,Site care,Staples removed (comment number),Marked,Reinforced,Site care,Staples removed (comment number)";
            SetIndIfResultContains(21, "", "9991733888867", "", "", reslist);
            reslist = "Drainage,Malodorous,Red,moist,gel foam,triple dye";
            SetIndIfResultContains(21, "", "9991733888850", "", "", reslist);
            reslist = "Dry,Moist,Cannulated,Clamp off,Clamp on,Care done,2 cord vessels,3 cord vessels";
            SetIndIfResultContains(21, "", "9991140100024", "", "", reslist);
            reslist = "Gauze in place,Petroleum jelly applied,Petroleum jelly gauze applied,Other";
            SetIndIfResultContains(21, "", "9991140100026", "", "", reslist);
            reslist = "Bleeding,Edematous,Necrotic,Pink,Reddened,Serosanguinous drainage,Serous drainage,Other";
            SetIndIfResultContains(21, "", "9991140100027", "", "", reslist);
            reslist = "Bloody,Clots,Tarry";
            SetIndIfResultContains(21, "", EXACT_MATCH_PREFIX + "18", "", "", reslist);
            reslist = "Maroon,Red";
            SetIndIfResultContains(21, "", EXACT_MATCH_PREFIX + "17", "", "", reslist);

            reslist = "Loose";
            bool loose = Exists("", EXACT_MATCH_PREFIX + "18", "", "", reslist, SearchDepth.SearchSince16Hrs);
            reslist = "Black";
            bool black = Exists("", EXACT_MATCH_PREFIX + "17", "", "", reslist, SearchDepth.SearchSince16Hrs);
            if (loose && black)
                SetInd(21, "Stool: loose and black within the past 16 hrs.");

            reslist = "Bloody,Bright red,Coffee ground,Dark red";
            SetIndIfResultContains(21, "", "9990007085590", "", "", reslist);
            SetIndIfResultContains(21, "", "99930400003205", "", "", "");

            reslist = "Chest washout";
            SetIndIfResultContains(22, "", "9993040001021", "", "", reslist);
            reslist = "> 30 minutes,> 60 minutes,> 90 minutes,> 120 minutes,> 180 minutes";
            if (Exists("", "9993040006782", "", "", reslist,SearchDepth.SearchSince13Hrs))
            {
                reslist = "Abdominal dressing,Alginate,Antimicrobial dressing,Cast,Cellular tissue based product,Charcoal,Collagen,Composite,Compression Wrap,Foam,Gauze,Gelling fiber/hydrofiber,Hemostatic dressing,Hydrocolloid,Hydrogel,Impregnated gauze,Impregnated Gauze wrap,Moist to dry,Moist to moist,non adherent,Packing,Petroleum gauze,Pressure dressing,Silicone dressing,Split gauze,Transparent dressing,Wound filler,Wound gel";
                SetIndIfResultContains(22, "", "9990000303800", "", "", reslist);
                SetIndIfResultContains(22, "", "9990000303820", "", "", "");
                reslist = "Clean,Dry,Intact,Changed,New drainage,Old drainage";
                SetIndIfResultContains(22, "", "9990000304850", "", "", reslist);
                SetIndIfResultContains(22, "", "9990000303910", "", "", reslist);
                SetIndIfResultContains(22, "", "9990007073550", "", "", "");
                SetIndIfResultContains(22, "", "9990007070178", "", "", "");
                SetIndIfResultContains(22, "", "9993040104037", "", "", "");
                SetIndIfResultContains(22, "", "9993040023765", "", "", "");

                SetIndIfResultContains(22, "", "9993040107858", "", "", "");
                SetIndIfResultContains(22, "", "9993040107859", "", "", "");
            }

            reslist = "Frenulectomy,Lacerated";
            SetIndIfResultContains(21, "", "9990000002114", "", "", reslist);

            SetIndIfResultContains(21, "", "9993040304870,9993040108530,3045001091", "", "", "");
            SetIndIfResultContains(21, "", "9993040102647,9993040102642,9993040102740,9993040102746", "", "", "");

            string codelist = "9993040108062,9993040108060,9993040108059,9993040108070,9993040108068,3040108067";
            SetIndIfResultContains(21, "", codelist, "", "", "");

            codelist = "9993040108108,9993040108102,9993040108114,9993040108109,9993040108103,";
            codelist += "9993040108115,9993040108110,9993040108104,9993040108116,9993040108111,";
            codelist += "9993040108105,9993040108117,9993040108112,9993040108106,9993040108118,";
            codelist += "9993040108113,9993040108119";
            SetIndIfResultContains(21, "", codelist, "", "", "");
            SetIndIfResultContains(21, "", EXACT_MATCH_PREFIX + "9993040108", "", "", "");

            CheckDrainLDA();

            codelist = "9993040109510,9993040109511,9993040108145,9993040108135,9993040108140";
            codelist += ",9993040108146,9993040108136,9993040108141,9993040108147,9993040108137";
            codelist += ",9993040108142,9993040108148,9993040108138,9993040108143,9993040108149";
            codelist += ",9993040108139,9993040108144,9993040108150";
            SetIndIfResultContains(21, "", codelist, "", "", "");


        }

        private void CheckDrainLDA()
        {
            string codelist = "9993040108530,3045001091";
            DateTime dt = DateTime.MinValue;
            var query1 = StartNewQuery(SearchDepth.SearchSince16Hrs);
            query1 = AndItemFilter(query1, "", codelist, "", "", "");
            query1 = query1.OrderBy(e => e.EVENT_DATETIME);
            foreach (var item in query1)
            { // for each start, find the number of records within 1 hr of it.
                if (dt == DateTime.MinValue)
                {
                    int ct = CountDrainLDAIn1Hour(codelist, item.EVENT_DATETIME);
                    if (ct >= 6)
                    {
                        SetInd(22, "Count of Drain LDAs within 1 hour=" + ct + " starting at: "+item.EVENT_DATETIME.ToString());
                        dt = item.EVENT_DATETIME;
                    }
                }
            }
            return;
        }

        private int CountDrainLDAIn1Hour(string codelist, DateTime startdt)
        {
            int ct = 0;
            var query1 = StartNewQuery(SearchDepth.SearchSince16Hrs);
            query1 = AndItemFilter(query1, "", codelist, "", "", "");
            query1 = query1.Where(e => e.EVENT_DATETIME >= startdt);
            query1 = query1.Where(e => e.EVENT_DATETIME <= startdt.AddMinutes(60));
            ct = query1.Count();
            return ct;

        }
        //private void CheckExtensiveWound()
        //{
        //    DateTime evdt = DateTime.MinValue;
        //    DateTime st_time = DateTime.MinValue;
        //    DateTime en_time = DateTime.MinValue;
        //    var query = StartNewQuery(SearchDepth.SearchDefault);
        //    query = AndCodeInList(query, "1375032519");
        //    query = AndResultInList(query, "extensive wound");
        //    query = query.OrderBy(e => e.EVENT_DATETIME);
        //    if (query.Count() == 0) return;
        //    foreach (var ci in query)
        //    {
        //        if (!_inds[22].is_checked)
        //        {
        //            evdt = ci.EVENT_DATETIME;
        //            st_time = GetResultTime("1375032547", evdt, 2);
        //            en_time = GetResultTime("1375032561", evdt, 2);
        //            if ((st_time != DateTime.MinValue) &&
        //                (en_time != DateTime.MinValue) && (st_time <= en_time))
        //            {
        //                if (st_time.AddMinutes(30) <= en_time)
        //                    SetInd(22, "Extensive Wound Management>=30min start:" + st_time.ToString() + " end:" + en_time.ToString());
        //            }
        //        }
        //    }
        //}

        // Use this if you can total the education time
        //private void CheckEducation(int total)
        //{
        //    if (_inds[23].is_checked) return;             //skip if already checked

        //    if (total >= 60) {
        //        SetInd(23, "education >= 60 min");
        //    }
        //}

        int EducMins(string timecode, string nursecode)
        {
            string res;
            int val = 0;
            if (ResultContains("", nursecode, "", "", "Nursing"))
            {
                if (GetResult("", timecode, "", "", out res))
                {
                    if (res.Left(1).IsNumeric())
                    {
                        val += (int)res.Val();
                    }
                }
            }
            return val;
        }

        private void Check_23()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("23. Healthcare Mgmt Education >= 1 Hour");
            Program.VerboseAudit("---------------");

            exclude_periop_data = false;

            CheckEDUtab("3040019564");
            CheckEDUtab("3040018802");
            CheckEDUtab("3040019429");
            CheckEDUtab("3040019626");
            CheckEDUtab("3040019773");
            CheckEDUtab("3040020569");
            CheckEDUtab("3040018770");
            CheckEDUtab("3040019597");
            CheckEDUtab("3040019587");
            CheckEDUtab("3040018788");
            CheckEDUtab("3040018690");
            CheckEDUtab("3040019462");
            CheckEDUtab("3040020762");
            CheckEDUtab("3040020565");
            CheckEDUtab("3040020708");
            CheckEDUtab("3040021311");
            CheckEDUtab("3040020757");
            CheckEDUtab("3040019438");
            CheckEDUtab("3040018686");
            CheckEDUtab("3040020288");
            CheckEDUtab("3040020278");
            CheckEDUtab("3040019172");
            CheckEDUtab("3040020314");
            CheckEDUtab("3040019604");
            CheckEDUtab("3040020664");
            CheckEDUtab("3040019143");
            CheckEDUtab("3040018773");
            CheckEDUtab("3040018769");
            CheckEDUtab("3040019385");
            CheckEDUtab("3040018689");
            CheckEDUtab("3040020820");
            CheckEDUtab("3040020559");
            CheckEDUtab("3040020534");
            CheckEDUtab("3040020463");
            CheckEDUtab("3040019415");
            CheckEDUtab("3040020413");
            CheckEDUtab("3040018515");
            CheckEDUtab("3040020310");
            CheckEDUtab("3040020404");
            CheckEDUtab("3040020512");
            CheckEDUtab("3040018603");
            CheckEDUtab("3040020282");
            CheckEDUtab("3040018908");
            CheckEDUtab("3040020466");
            CheckEDUtab("3040020255");
            CheckEDUtab("3040020262");
            CheckEDUtab("3040020297");
            CheckEDUtab("3040019782");
            CheckEDUtab("3040019666");
            CheckEDUtab("3040020328");
            CheckEDUtab("3040018684");
            CheckEDUtab("3040002644");
            CheckEDUtab("3040001000");
            CheckEDUtab("3040019679");
            CheckEDUtab("3040020321");
            CheckEDUtab("3040020477");
            CheckEDUtab("3040018533");
            CheckEDUtab("3040019736");
            CheckEDUtab("3040018932");
            CheckEDUtab("3040019011");
            CheckEDUtab("3040018647");
            CheckEDUtab("3040000907");
            CheckEDUtab("3040019707");
            CheckEDUtab("3040018906");
            CheckEDUtab("3040020694");
            CheckEDUtab("3040019762");
            CheckEDUtab("3040018587");
            CheckEDUtab("3040018530");
            CheckEDUtab("3040020741");
            CheckEDUtab("3040019360");
            CheckEDUtab("3040019721");
            CheckEDUtab("3040019691");
            CheckEDUtab("3040019352");
            CheckEDUtab("3040018525");
            CheckEDUtab("3040018688");
            CheckEDUtab("3040020418");
            CheckEDUtab("3040018687");
            CheckEDUtab("3040018692");
            CheckEDUtab("3040018691");
            CheckEDUtab("3040018613");
            CheckEDUtab("3040018631");
            CheckEDUtab("3040020809");
            CheckEDUtab("3040020801");
            CheckEDUtab("3040018872");
            CheckEDUtab("3040020265");
            CheckEDUtab("3040020550");
            CheckEDUtab("3040018910");
            CheckEDUtab("3040020431");
            CheckEDUtab("3040020730");
            CheckEDUtab("3040020546");
            CheckEDUtab("3040020540");
            CheckEDUtab("3040020991");
            CheckEDUtab("3040020982");
            CheckEDUtab("3040020600");
            CheckEDUtab("3040020615");
            CheckEDUtab("3040020622");
            CheckEDUtab("3040020610");
            CheckEDUtab("3040018556");
            CheckEDUtab("3040020717");
            CheckEDUtab("3040019405");
            CheckEDUtab("3040018772");
            CheckEDUtab("3040020317");
            CheckEDUtab("3040018685");
            CheckEDUtab("3040020554");
            CheckEDUtab("3040019749");
            CheckEDUtab("3040019448");
            CheckEDUtab("3040004179");
            CheckEDUtab("3040020576");
            CheckEDUtab("3040020581");
            CheckEDUtab("3040020744");
            CheckEDUtab("3040019147");
            CheckEDUtab("3040020425");
            CheckEDUtab("3040020437");
            CheckEDUtab("3040019576");
            CheckEDUtab("3040018909");
            CheckEDUtab("3040020324");
            CheckEDUtab("3040019158");
            CheckEDUtab("3040018598");
            CheckEDUtab("3040018771");
            CheckEDUtab("3040020644");
            CheckEDUtab("3040021310");
            CheckEDUtab("3040020635");

            CheckEDUtab("3048000084");
            //CheckEDUtab("1021111110");
            //CheckEDUtab("3048000419");
            CheckEDUtab("3040020431");
            CheckEDUtab("3040021208");

        } // end proc

    private void CheckEDUtab(string educode)
        {
            string[] demoeducodes = { "3040019666","3040019679","3040018932","3040019011","3040019707","3040019721","3040019691",
            "3040018910","3040020431","3040020730","3040020717","3040020425","3040019158",
            "3040020431","3040021208","3048000084"  };

            int ub, i;
            string[] desc = new string[3];
            DateTime evdt;
            string cd1, cd2, res1, res2, topic1, topic2;

            //OBX | 2 | ST | EDU3040004179METHOD ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC STROKE EVERY MINUTE MATTERS MC6074| 1 | E |||||| F ||| 20170612113300
            //OBX | 4 | ST | EDU3040004179RESPONSE ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC STROKE EVERY MINUTE MATTERS MC6074| 1 | TB |||||| F ||| 20170612113300
            //OBX | 6 | ST | EDU3040004179METHOD ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC YOU CAN IMPROVE YOUR HEALTH MC2066 - 09 | 1 | E |||||| F ||| 20170612113300
            //OBX | 8 | ST | EDU3040004179RESPONSE ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC YOU CAN IMPROVE YOUR HEALTH MC2066 - 09 | 1 | TB |||||| F ||| 20170612113300
            //OBX | 10 | ST | EDU3040004179METHOD ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC HIGH BLOOD PRESSURE (HYPERTENSION)MC5056 | 1 | E |||||| F ||| 20170612113300
            //OBX | 12 | ST | EDU3040004179RESPONSE ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC HIGH BLOOD PRESSURE (HYPERTENSION)MC5056 | 1 | TB |||||| F ||| 20170612113300
            var query1 = StartNewQuery(SearchDepth.SearchSince9Hrs);
            query1 = AndItemFilter(query1, "", "EDU" + educode + "METHOD", "", "", "E,D,I");
            query1 = query1.Where(e => e.EVENT_DATETIME <= PFSUtility.MaxDateTime(_pat.unit_departure, _pat.effective_out));
            foreach (var item1 in query1)
            {
                evdt = item1.EVENT_DATETIME;
                res1 = item1.RESULT;
                cd1 = item1.CODE;
                //Program.VerboseAudit("EDU: res=" + res1 + " code=" + cd1 + " evdt=" + evdt.ToString());

                var arr = item1.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                ub = arr.GetUpperBound(0);
                for (i = 0; (i <= Math.Min(ub, 2)); i++)
                    desc[i] = arr[i];
                if (ub >= 2)
                    topic1 = desc[2].Trim();
                else
                    topic1 = "";
                var query2 = StartNewQuery(SearchDepth.SearchSince9Hrs);
                query2 = AndItemFilter(query2, "", "EDU" + educode + "RESPONSE", "", "", "IP,TB,NR");
                query2 = query2.Where(e => e.EVENT_DATETIME == evdt);
                foreach (var item2 in query2)
                {
                    res2 = item2.RESULT;
                    cd2 = item2.CODE;
                    var arr2 = item2.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                    ub = arr.GetUpperBound(0);
                    for (i = 0; (i <= Math.Min(ub, 2)); i++)
                        desc[i] = arr[i];
                    if (ub >= 2)
                        topic2 = desc[2].Trim();
                    else
                        topic2 = "";
                    if (topic1 == topic2)
                    {
                        SetInd(23, "Found EDU" + educode + ": " + desc[0] + "^" + topic1 + " at:" + evdt.ToString() + " METHOD=" + res1 + " RESPONSE=" + res2);

                        if (res1.ToUpper().Contains("I".ToUpper()) ||
                                (educode.ContainsAny(demoeducodes) &&
                                   (res1.ToUpper().Contains("D".ToUpper()) ||
                                    res2.ToUpper().Contains("TBDEMO".ToUpper()))
                                )
                           )
                        {
                            AddEducActivity(evdt);
                        }
                    }
                }

            }

        }
        private void AddEducActivity(DateTime evdt)
        {
            DateTime enddt;
            Program.VerboseAudit("Activity 5: Found at " + evdt.AddHours(-1).ToString());
            if (!QueuedProcOverlaps(5, evdt.AddHours(-1), evdt))
                if (!ProcExistsInDB(5, evdt.AddHours(-1), out enddt))
                {
                    var proc = new proc_data();
                    proc.procedure_number = 5;
                    proc.start = evdt.AddHours(-1);
                    proc.finish = evdt;
                    _procs.Add(proc);
                    Program.Audit("Activity 5: Found at " + evdt);
                }
        }

        private void Check_24()
        {
            string reslist = "";
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("24. 1 to 1 Physiological Interv. >= 2 Hours");
            Program.VerboseAudit("---------------");

            exclude_periop_data = true;

            reslist = "";
            SetIndIfResultContains(24, "", "9993040001002", "", "", reslist);
            SetIndIfResultContains(24, "", "9993040001003", "", "", reslist);
            SetIndIfResultContains(24, "", "9993040101000", "", "", reslist);
            SetIndIfResultContains(24, "", "9993040001093", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(24, "", "9993040102551", "", "", reslist);
            SetIndIfResultContains(24, "", "9993040102552", "", "", reslist);

            //reslist = "Endotracheal tube,Esophageal - tracheal tube,Laryngeal mask airway,LMA,Nasopharyngeal airway,NPA,Oropharyngeal airway,OPA,Tracheostomy,Other";
            reslist = "";
            SetIndIfResultContains(24, "", "9991600100681", "", "", reslist);

            reslist = "Bag-valve-mask,Bag-valve-ET tube,Bag-valve-tracheostomy,Ventilator,Bilevel positive airway pressure,BiPAP,Continuous positive airway pressure,CPAP,CPAP nasal,CPAP mask,Positive pressure ventilation,PPV,Other";
            SetIndIfResultContains(24, "", "9991600100682", "", "", reslist);

            reslist = "Respiratory arrest,Cardiac arrest,Unknown,Other";
            SetIndIfResultContains(24, "", "9991600100646", "", "", reslist);

            if (_pat.age < 4.0)
            {
                reslist = "CPAP vent,ETT nasal,ETT oral";
                SetIndIfResultContains(24, "", "9991733888883", "", "", reslist);
                reslist = "CPAP,Hi Flow O2";
                SetIndIfResultDoesNotContain(24, "", "3045001117", "", "", reslist);
                reslist = "PACU Ventilator Rows,Trilogy Ventilator Rows,HFO Ventilator Rows,Abbreviated Settings,NAVA Rows,HFJV Rows";
                SetIndIfResultContains(24, "", "9993040000635", "", "", reslist);
            }
            //reslist = "4";
            //SetIndIfResultContains(24, "", "9993040000407", "", "", reslist);

            CheckNorepi();
        }

        private void CheckNorepi()
        {
            string str_rate = "";
            double dbl_rate = 0.0;
            //            Select #24 when >0.1

            //OBX | 1 | DT | MED128033 ^ NOREPINEPHRINE BITARTRATE 4 MG / 250 ML(16 MCG / ML) IN DEXTROSE 5 % IV; ; ; 18; ; ; 0.15; ; ; IV; ; ; NewBag
            //OBX | 1 | DT | MED128033 ^ NOREPINEPHRINE BITARTRATE 4 MG / 250 ML(16 MCG / ML) IN DEXTROSE 5 % IV; ; ; 18; ; ; 0.33; ; ; IV; ; ; NewBag
            //OBX | 1 | DT | MED128033 ^ NOREPINEPHRINE BITARTRATE 4 MG / 250 ML(16 MCG / ML) IN DEXTROSE 5 % IV; ; ; 18; ; ; 0.331; ; ; IV; ; ; RateVerify

            //                       Do not select #24 if value <0.1

            //OBX | 1 | DT | MED128033 ^ NOREPINEPHRINE BITARTRATE 4 MG / 250 ML(16 MCG / ML) IN DEXTROSE 5 % IV; ; ; 18; ; ; 0.01; ; ; IV; ; ; NewBag
            //OBX | 1 | DT | MED128033 ^ NOREPINEPHRINE BITARTRATE 4 MG / 250 ML(16 MCG / ML) IN DEXTROSE 5 % IV; ; ; 18; ; ; 0.07; ; ; IV; ; ; RateChange
            //OBX | 1 | DT | MED128033 ^ NOREPINEPHRINE BITARTRATE 4 MG / 250 ML(16 MCG / ML) IN DEXTROSE 5 % IV; ; ; 18; ; ; 0.08; ; ; IV; ; ; RateChange
            var query = StartNewQuery(SearchDepth.SearchSince9Hrs);    // add custom time range below
            query = query.Where(e => e.CODE.StartsWith("med"));
            query = query.Where(e => e.UNIT_ID == _pat.unit_id);
            query = query.Where(e => e.DESCRIPTION.ToUpper().StartsWith("NOREPINEPHRINE"));
            query = query.Where(e => e.DESCRIPTION.Contains(";;;iv;;;"));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            //Program.VerboseAudit("Norepinephrine found: " + query.Count());
            bool found = false;
            foreach (var item in query)
            {
                if (!found)
                {
                    //      Program.VerboseAudit("====Med found: Norepinephrine====");
                    //Program.VerboseAudit("med code: " + item.CODE + " desc: " + item.DESCRIPTION + "  evdt: " + item.EVENT_DATETIME.ToString());

                    var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                    if (arr.GetUpperBound(0) >= 3)
                    {
                        str_rate = arr[2];
                        dbl_rate = str_rate.ToDouble();
                        if (dbl_rate >= 0.1)
                        {
                            SetInd(24, "Norepinephrine rate=" + dbl_rate.ToString());
                            found = true;
                        }
                    }
                }
            }
            return;
        }

        private void CheckUserDefined()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("User-Defined indicators");
            Program.VerboseAudit("---------------");

            //reslist = "Patient arrived during downtime,Patient was cared for during downtime,Patient departed during downtime";
            //SetIndIfResultContains(99, "", "9991600100203", "", "", reslist);
            //reslist = "Yes";
            //SetIndIfResultContains(99, "", "9990000006437", "", "", reslist);
            //reslist = "Green,Yellow ,Red ,Black ";
            //SetIndIfResultContains(99, "", "9991600100265", "", "", reslist);
            //reslist = "Downtime";
            //SetIndIfResultContains(99, "", "9990007096330", "", "", reslist);


        }

        //private bool IsICU()
        //{
        //    switch (_pat.unit_name)
        //    {
        //        case "ICU":
        //        case "3BR":
        //        case "CTIC":
        //        case "NIC":
        //        case "PCU":
        //        case "TICU":
        //        case "CCB":
        //        case "RICU":
        //        case "CCUS":
        //            return true;
        //        default:
        //            return false;
        //    }
        //}

        //private bool IsTele()
        //{
        //    switch (_pat.unit_name)
        //    {
        //        case "4EST":
        //        case "2NW":
        //        case "3AE":
        //        case "5MEH":
        //        case "5NW":
        //        case "6MEH":
        //        case "6NW":
        //        case "7MEH":
        //        case "B4W":
        //        case "B5S":
        //        case "B5W":
        //        case "2NOB":
        //        case "CPLX":
        //        case "TELMS":
        //        case "MAN4":
        //            return true;
        //        default:
        //            return false;
        //    }
        //}



        //=====================================================================
        // Group like assessments into buckets of time
        // Each bucket counts as one assessment in the frequency count

        //private void AddDependentBuckets(List<gBucket> bucket_list, string codelist1, string reslist1, string codelist2, string reslist2)
        //{
        //    AddDependentBuckets(bucket_list, codelist1, reslist1, codelist2, reslist2, "", "");
        //}

        //private void AddDependentBuckets(List<gBucket> bucket_list, string codelist1, string reslist1, string codelist2, string reslist2, string codelist3,string reslist3)
        //{
        //    bool dep3 = true;
        //    // get the chart items for the assessments
        //    var query1 = StartNewQuery();
        //    query1 = AndItemFilter(query1, "", codelist1, "", "", reslist1);
        //    var query2 = StartNewQuery();
        //    query2 = AndItemFilter(query2, "", codelist2, "", "", reslist2);
        //    if (codelist3.Trim() == "")
        //    {
        //        dep3 = false;
        //        codelist3 = "Hello, this is a phantom code";
        //    }
        //    var query3 = StartNewQuery();
        //    query3 = AndItemFilter(query3, "", codelist3, "", "", reslist3);

        //    // figure out what buckets the events belong to
        //    var query1a = from item in query1
        //                  select new
        //                  {
        //                      bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
        //                      code = item.CODE,
        //                      evdt = item.EVENT_DATETIME
        //                  };
        //    var query2a = from item in query2
        //                  select new
        //                  {
        //                      bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
        //                      code = item.CODE,
        //                      evdt = item.EVENT_DATETIME
        //                  };
        //    var query3a = from item in query3
        //                      select new
        //                      {
        //                          bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
        //                          code = item.CODE,
        //                          evdt = item.EVENT_DATETIME
        //                      };

        //    string s = "BucketList1 for " + codelist1 + ": ";
        //    foreach (var item in query1a)
        //    {
        //        s += item.bucket + ",";
        //    }
        //    Program.VerboseAudit(s);

        //    s = "BucketList2 for " + codelist2 + ": ";
        //    foreach (var item in query2a)
        //    {
        //        s += item.bucket + ",";
        //    }
        //    if (dep3)
        //    {
        //        s = "BucketList3 for " + codelist3 + ": ";
        //        foreach (var item in query3a)
        //        {
        //            s += item.bucket + ",";
        //        }
        //    }
        //    Program.VerboseAudit(s);
        //    // Add to the list IFF items in both lists occur in same bucket
        //    foreach (var item1 in query1a)
        //    {
        //        foreach (var item2 in query2a)
        //        {
        //            if (item1.bucket == item2.bucket)
        //            {
        //                if (dep3)
        //                {
        //                    foreach (var item3 in query3a)
        //                    {
        //                        if (item1.bucket == item3.bucket)
        //                        {
        //                            var b = new gBucket();
        //                            b.bucket = item1.bucket;
        //                            b.code = item1.code;
        //                            b.evdt = item1.evdt;
        //                            bucket_list.Add(b);
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    foreach (var item3 in query2a)
        //                    {
        //                        if (item1.bucket == item2.bucket)
        //                        {
        //                            var b = new gBucket();
        //                            b.bucket = item1.bucket;
        //                            b.code = item1.code;
        //                            b.evdt = item1.evdt;
        //                            bucket_list.Add(b);
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }

        //}

        private void AddSimpleProc(int pnum, DateTime evdt, DateTime enddt)
        {
            DateTime enddt_out;
            Program.Audit("AddSimpleProc " + pnum + ": evdt=" + evdt + " enddt=" + enddt);
            bool existsindb = ProcExistsInDB(pnum, evdt, out enddt_out);

            if ((existsindb && enddt==enddt_out) && ProcExists(pnum, evdt, enddt_out))
            {
                Program.Audit("Activity " + pnum + ": already exists");
            }
            else
            {
                if (ActivityFits(evdt, enddt))
                {
                    if (pnum == 9)
                        ProcOverlapsInDB_PEID(pnum, evdt, enddt, true); // then delete the db
                    else
                        ProcOverlapsInDB_PEID(pnum, evdt, enddt, false); // then delete the db
                    var proc = new proc_data();
                    proc.procedure_number = pnum;
                    proc.start = evdt;
                    proc.finish = enddt;
                    _procs.Add(proc);
                    Program.Audit("Activity " + pnum + ": Found between " + evdt + " and " + enddt);
                }
            }
        }

        private bool ActivityFits(DateTime beg, DateTime fin)
        {
            bool ok = false;
            int unit_id = 0;

            fin = fin.AddHours(-1); //remove 1 hour to fit into location: in case location is short

            string sql = "select el.unit_id from ENCOUNTER_LOCATION as el";
            sql += " where el.ENCOUNTER_ID=" + _pat.encounter_id;
            //sql += " and el.SPECIAL_UNIT_ID is null";
            sql += " and el.EFFECTIVE_DATETIME_IN<='" + beg.ToString() + "'";
            sql += " and el.EFFECTIVE_DATETIME_OUT>='" + fin.ToString() + "'";

            Program.VerboseAudit(sql);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd = new SqlCommand(sql, db2);
            SqlDataReader dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr2.Read())
            {
                if (dr2["UNIT_ID"] != DBNull.Value)
                    unit_id = PFSDBUtility.DBToInt(dr2["UNIT_ID"]);
            }
            ok = (unit_id > 0);

            if (ok)
            {
                db2.Close();
                return ok;
            }
            //Now check if two adjacent same units contains the activity.
            int unitid1 = 0;
            int unitid2 = 0;
            sql = "select el.unit_id from ENCOUNTER_LOCATION as el";
            sql += " where el.ENCOUNTER_ID=" + _pat.encounter_id;
            sql += " and '" + beg.ToString() + "' between el.EFFECTIVE_DATETIME_IN and el.EFFECTIVE_DATETIME_OUT";
            cmd = new SqlCommand(sql, db2);
            dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr2.Read())
            {
                if (dr2["UNIT_ID"] != DBNull.Value)
                    unitid1 = PFSDBUtility.DBToInt(dr2["UNIT_ID"]);
            }
            sql = "select el.unit_id from ENCOUNTER_LOCATION as el";
            sql += " where el.ENCOUNTER_ID=" + _pat.encounter_id;
            sql += " and '" + fin.ToString() + "' between el.EFFECTIVE_DATETIME_IN and el.EFFECTIVE_DATETIME_OUT";
            cmd = new SqlCommand(sql, db2);
            dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr2.Read())
            {
                if (dr2["UNIT_ID"] != DBNull.Value)
                    unitid2 = PFSDBUtility.DBToInt(dr2["UNIT_ID"]);
            }
            ok = (unitid1 > 0 && unitid1 == unitid2);
            db2.Close();
            return ok;


        }

        //6	2019-09-18 15:00:00.000	2019-09-18 19:00:00.000
        //6	2019-09-19 07:00:00.000	2019-09-19 11:00:00.000

        private bool ActivityDuringClassType6(DateTime beg, DateTime fin, out DateTime classout)
        {
            bool ok = false;
            int pt = 0;
            classout = DateTime.MinValue;
            string sql = "select ce.PATIENT_TYPE,ce.effective_datetime_in,ce.effective_datetime_out from CLASSIFICATION_EVENT as ce";
            sql += " where ce.ENCOUNTER_ID=" + _pat.encounter_id;
            sql += " and ce.PATIENT_TYPE=6";
            sql += " and ('" + beg.ToString() + "' >=ce.EFFECTIVE_DATETIME_IN and '" + beg.ToString() + "' <ce.EFFECTIVE_DATETIME_OUT";
            sql += " or '" + beg.ToString() + "' <ce.EFFECTIVE_DATETIME_IN and '" + fin.ToString() + "' >ce.EFFECTIVE_DATETIME_OUT";
            sql += " or '" + fin.ToString() + "' >ce.EFFECTIVE_DATETIME_IN and '" + fin.ToString() + "' <=ce.EFFECTIVE_DATETIME_OUT)";
            Program.VerboseAudit(sql);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd = new SqlCommand(sql, db2);
            SqlDataReader dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr2.Read())
            {
                if (dr2["PATIENT_TYPE"] != DBNull.Value)
                {
                    pt = PFSDBUtility.DBToInt(dr2["PATIENT_TYPE"]);
                    classout = PFSDBUtility.DBToDateTime(dr2["EFFECTIVE_DATETIME_OUT"]);
                }
            }
            db2.Close();
            ok = (pt == 6);
            return ok;
        }

        private void AtLeastOneADL()
        {
            Program.Audit("---------------");

            if (!(_inds[2].is_checked || _inds[3].is_checked || _inds[4].is_checked)) {
                // Make "#2 ADL - Assist" the default.  (90% of patients)
                SetInd(1, "Defaulting to ADL Self due to lack of documentation.");
            }
        }

        private void HighestIndicatorInEachGroupWins()
        {
            int i, g;
            bool highest_is_on;
            string ind_list;

            Program.Audit("Select highest indicator in each group");

            g = 0;
            highest_is_on = false;
            // Go from bottom up - highest (lowest) indicator in each group wins
            for (i = MAX_INDS; (i >= 1); i--) {
                if (_inds[i].radio_group > 0) {
                    if (_inds[i].radio_group != g) {
                        //this is a new group
                        g = _inds[i].radio_group;
                        highest_is_on = _inds[i].is_checked;
                    } else {
                        //same group
                        if (highest_is_on) {
                            _inds[i].is_checked = false;             //uncheck a lower number
                        } else {
                            highest_is_on = _inds[i].is_checked;     //save this one
                        }
                    }
                }
            } // next i

            //Echo the indicators for an Audit (no classification will be saved)
            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++) {
                if (_inds[i].is_checked) ind_list += "," + i;
            } // next i

            Program.Audit("Final list = " + ind_list.Substring(1));
            Program.Audit("---------------");
        }


        private void CheckProcs()
        {
            CheckProc_1_2();
            //CheckProc_2();
            CheckProc_3();
            CheckProc_4();
            CheckProc_5();
            CheckProc_6();
            CheckProc_7();
            CheckProc_8();
            CheckProc_9();
            //CheckProc_10();
            CheckProc_11();

        }

        //private void DoProc(int pnum, string code)
        //{
        //    double mins = 0;
        //    string found_what;
        //    DateTime evdt;
        //    DateTime enddt = DateTime.MinValue;

        //    if (GetResultAndEVDT("", code, "", "", out found_what, out evdt))
        //    {
        //        //mins = 60.0 * found_what.ToDouble();
        //        if (found_what.Contains("180")) mins = 180;
        //        else if (found_what.Contains("120")) mins = 120;
        //        else if (found_what.Contains("90")) mins = 90;
        //        else if (found_what.Contains("60")) mins = 60;
        //        enddt = evdt.AddMinutes(mins);

        //        if (ProcExistsInDB(pnum, evdt, enddt))
        //        {
        //            Program.Audit("Activity " + pnum+ ": already exists");
        //        }
        //        else
        //        {
        //            if (!QueuedProcOverlaps(pnum, evdt, enddt))
        //            {
        //                var proc = new proc_data();
        //                proc.procedure_number = pnum;
        //                proc.start = evdt;
        //                proc.finish = enddt;
        //                _procs.Add(proc);
        //                Program.Audit("Activity " + pnum + ": Found " + code + " between " + evdt + " and " + enddt);
        //            }
        //        }

        //    }

        //}

        private bool ProcOverlapsInDB_PEID(int pnum, DateTime startdt, DateTime enddt, bool del_and_add_if_same_start)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            LoadPatientProceduresIfNeeded();
            bool overlap_exists = false;
            if (del_and_add_if_same_start)
            {
                var query = from proc in _procedure_events
                            from ans in proc.PROCEDURE_ANSWERs
                            where (proc.ENCOUNTER_ID == _pat.encounter_id)
                                && (proc.PROCEDURE_DATETIME == startdt) 
                                && (proc.CLASSIFIED_BY_ID < 0)
                                && (ans.PROCEDURE_NUMBER == pnum)
                            select new { proc.PROCEDURE_EVENT_ID, proc.PROCEDURE_DATETIME, proc.DEPARTURE_DATETIME };
                overlap_exists = (query.Count() > 0);
                foreach (var a in query)
                {
                    Program.VerboseAudit("Will Delete act: procdt=" + a.PROCEDURE_DATETIME.ToString() + "  depdt=" + a.DEPARTURE_DATETIME.ToString());
                    Program.VerboseAudit("because it has same start time=" + startdt.ToString() + "  enddt=" + enddt.ToString());
                    DeleteActivity(a.PROCEDURE_EVENT_ID);
                }

            }
            else
            {
                var query = from proc in _procedure_events
                            from ans in proc.PROCEDURE_ANSWERs
                            where (proc.ENCOUNTER_ID == _pat.encounter_id)
                                && ((proc.PROCEDURE_DATETIME <= startdt) && (enddt > proc.PROCEDURE_DATETIME))
                                //&& ( ! (proc.PROCEDURE_DATETIME == startdt) && (proc.DEPARTURE_DATETIME == enddt))
                                && (proc.CLASSIFIED_BY_ID < 0)
                                && (ans.PROCEDURE_NUMBER == pnum)
                            select new { proc.PROCEDURE_EVENT_ID, proc.PROCEDURE_DATETIME, proc.DEPARTURE_DATETIME };
                overlap_exists = (query.Count() > 0);
                foreach (var a in query)
                {
                    Program.VerboseAudit("Will Delete act.: procdt=" + a.PROCEDURE_DATETIME.ToString() + "  depdt=" + a.DEPARTURE_DATETIME.ToString());
                    Program.VerboseAudit("because it overlays startdt=" + startdt.ToString() + "  enddt=" + enddt.ToString());
                    DeleteActivity(a.PROCEDURE_EVENT_ID);
                }
            }
            //            peid = 0;
            return (overlap_exists);
        }
        private void DeleteActivity(int peid)
        {
            //            delete PROCEDURE_ANSWER where PROCEDURE_EVENT_ID=7211351
            //delete RPT_PROC_BY_DAY where PROCEDURE_EVENT_ID=7211351
            //delete PROCEDURE_EVENT where PROCEDURE_EVENT_ID=7211351
            string sql;
            if (peid == 0) return;
            string q;

            Program.VerboseAudit("db ProcAnsw Deleting peid=" + peid);
            //var db = PFSDBUtility.NewPfsDataContext();
            //var query = from ia in db.PROCEDURE_ANSWERs
            //            where (ia.PROCEDURE_EVENT_ID == peid)
            //            select ia;
            //if (query.Count() > 0)
            //{

            //    //var items = query.ToList();
            //    //foreach (var item in items)
            //    //    db.PROCEDURE_ANSWERs.DeleteOnSubmit(item);
            //    //db.SubmitChanges();
            //}
            //var dbsqla = PFSDBUtility.NewSqlConnection();
            //q = "DELETE FROM PROCEDURE_ANSWER WHERE PROCEDURE_EVENT_ID=" + peid;
            //SqlCommand cmd = new SqlCommand(q, dbsqla);
            //cmd.CommandTimeout = 90;
            //cmd.ExecuteNonQuery();
            sql = "DELETE FROM RPT_PROC_BY_DAY WHERE PROCEDURE_EVENT_ID=" + peid.ToString();
            PFSDBUtility.ExecuteSQL(sql);

            sql = "DELETE FROM PROCEDURE_ANSWER WHERE PROCEDURE_EVENT_ID=" + peid.ToString();
            PFSDBUtility.ExecuteSQL(sql);


            Program.VerboseAudit("db RptProc Deleting peid=" + peid);
            //var db2 = PFSDBUtility.NewPfsDataContext();
            //var query2 = from r in db2.RPT_PROC_BY_DAYs
            //             where (r.PROCEDURE_EVENT_ID == peid)
            //             select r;
            //if (query2.Count() > 0)
            //{
            //    //var items2 = query2.ToList();
            //    //foreach (var item2 in items2)
            //    //    db2.RPT_PROC_BY_DAYs.DeleteOnSubmit(item2);
            //    //db2.SubmitChanges();
            //}

            Program.VerboseAudit("db ProcEvent Deleting peid=" + peid);
            //var db3 = PFSDBUtility.NewPfsDataContext();
            //var query3 = from ce in db3.PROCEDURE_EVENTs
            //             where (ce.PROCEDURE_EVENT_ID == peid)
            //             select ce;
            //if (query3.Count() > 0)
            //{
            //    //var items3 = query3.ToList();
            //    //foreach (var item3 in items3)
            //    //    db3.PROCEDURE_EVENTs.DeleteOnSubmit(item3);
            //    //db3.SubmitChanges();
            //}
            sql = "DELETE FROM PROCEDURE_EVENT WHERE PROCEDURE_EVENT_ID=" + peid.ToString();
            PFSDBUtility.ExecuteSQL(sql);

        }



        private bool QueuedProcOverlaps(int pnum, DateTime startdt, DateTime enddt)
        {
            bool overlap = false;

            Program.VerboseAudit("Activity " + pnum + ": Check for dup at " + startdt.ToString() + " - " + enddt.ToString());
            proc_data[] pary = _procs.ToArray();
            for (int i = 0; i <= pary.GetUpperBound(0); i++)
            {
                if (pary[i].procedure_number == pnum)
                {
                    //overlap if   p.start between stardt and enddt
                    //             p.finish between startdt and enddt
                    //             startdt >= p.start and enddt <= p.finish
                    if ((pary[i].start >= startdt) && (pary[i].start < enddt) ||
                        (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
                        (startdt >= pary[i].start) && (enddt <= pary[i].finish))
                    {
                        overlap = true;
                    }

                    if ((pary[i].start >= startdt) && (pary[i].start < enddt) && (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
                        (pary[i].start < startdt) && (pary[i].finish > startdt) && (pary[i].finish < enddt)
                        )
                    {
                        // the proc in the list is completely contained in startdt/endt pair
                        // change this proc to be the startdt/enddt
                        // overlap is still true, but the list item will be updated with the encompassing times
                        //Program.Audit("Procedure " + pnum + ": " + startdt + " - " + enddt + " will trump " + pary[i].start + " - " + pary[i].finish);
                        pary[i].start = startdt;
                        pary[i].finish = enddt;
                    }
                }
            }
            _procs = pary.ToList();
            Program.VerboseAudit("Activity " + pnum + ": Check for dup returns " + overlap);
            return overlap;
        }

        //Valid:9993040009234+continuous observation by non-rn staff with patient 6/16/2019 10:15:00 PM
        //Valid:9993040009234+continuous observation by non-rn staff with patient 6/16/2019 11:00:00 PM
        //Valid:9993040009234+continuous observation by non-rn staff with patient 6/17/2019 12:08:00 AM
        //Valid:9993040009234+continuous observation by non-rn staff with patient 6/17/2019 1:30:00 AM
        //Valid:9993040009234+continuous observation by non-rn staff with patient 6/17/2019 2:01:00 AM
        //Valid:9993040009234+continuous observation by non-rn staff with patient 6/17/2019 4:14:00 AM
        //Valid:9993040009234+q2 hour 6/17/2019 5:13:00 AM
        //Valid:9993040009234+q2 hour 6/17/2019 6:25:00 AM
        //Valid:9993040009234+continuous observation by non-rn staff with patient 6/17/2019 8:00:00 AM
        //Valid:9993040009234+continuous observation by rn with patient 6/17/2019 11:00:00 AM
        //i:0 start_idx=-99 stop_idx=-1 desc=start evdt = 6 / 16 / 2019 10:15:00 PM
        //found start
        //i:1 start_idx=0 stop_idx=-1 desc=start evdt = 6 / 16 / 2019 11:00:00 PM
        //i:2 start_idx=0 stop_idx=-1 desc=start evdt = 6 / 17 / 2019 12:08:00 AM
        //i:3 start_idx=0 stop_idx=-1 desc=start evdt = 6 / 17 / 2019 1:30:00 AM
        //i:4 start_idx=0 stop_idx=-1 desc=start evdt = 6 / 17 / 2019 2:01:00 AM
        //i:5 start_idx=0 stop_idx=-1 desc=start evdt = 6 / 17 / 2019 4:14:00 AM
        //i:6 start_idx=0 stop_idx=-1 desc=stop evdt = 6 / 17 / 2019 5:13:00 AM
        //found stop
        //i:7 start_idx=-99 stop_idx=-99 desc=stop evdt = 6 / 17 / 2019 6:25:00 AM
        //i:8 start_idx=-99 stop_idx=-99 desc=start evdt = 6 / 17 / 2019 8:00:00 AM
        //found start
        //i:9 start_idx=8 stop_idx=-99 desc=start evdt = 6 / 17 / 2019 11:00:00 AM
        //Num start-stop=1
        //ss pnum = 2  start: 6/16/2019 10:15:00 PM finish: 6/17/2019 5:13:00 AM


        //11am:
        //ss pnum = 2  start: 6/17/2019 3:00:00 AM finish: 6/17/2019 5:13:00 AM
        //ss pnum=2  start: 6/17/2019 8:00:00 AM finish: 6/17/2019 11:00:00 AM
        //3pm:
        //ss pnum = 2  start: 6/17/2019 8:00:00 AM finish: 6/17/2019 3:00:00 PM
        private bool ProcExistsInDB(int pnum, DateTime startdt, out DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            Program.VerboseAudit("ProcExistsInDB: pnum=" + pnum + " startdt=" + startdt.ToString());
            int ct = 0;
            enddt = DateTime.MinValue;
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            //&& (proc.PROCEDURE_DATETIME <= startdt)
                            && (proc.DEPARTURE_DATETIME > startdt)
                            && (((pnum > 2) && (ans.PROCEDURE_NUMBER == pnum)) || ((pnum <= 2) && (ans.PROCEDURE_NUMBER <= 2)))
                        orderby proc.DEPARTURE_DATETIME descending
                        select new { proc.DEPARTURE_DATETIME };
            ct = query.Count();
            //Program.VerboseAudit("ProcExistsInDB: ct=" + ct);
            //if (ct > 0)
            //{
            //    foreach (var x in query)
            //    {
            //        Program.VerboseAudit("ProcExistsInDB: x=" + x.DEPARTURE_DATETIME);
            //    }
            //}
            if (ct > 0)
                enddt = (DateTime)query.First().DEPARTURE_DATETIME;
            //else //see if there is a saved proc that ended before this startdt
            //{
            //    query = from proc in _procedure_events
            //            from ans in proc.PROCEDURE_ANSWERs
            //            where (proc.ENCOUNTER_ID == _pat.encounter_id)
            //                //&& (proc.PROCEDURE_DATETIME <= startdt)
            //                && (proc.DEPARTURE_DATETIME > startdt.AddHours(-4))
            //                && (ans.PROCEDURE_NUMBER == pnum)
            //            orderby proc.DEPARTURE_DATETIME descending
            //            select new { proc.DEPARTURE_DATETIME };
            //    ct = query.Count();
            //    if (ct > 0)
            //        enddt = (DateTime)query.First().DEPARTURE_DATETIME;
            //}
            Program.VerboseAudit("ProcExistsInDB: pnum=" + pnum + " returns " + ct);
            return ct > 0;
        }

        private void CheckProc_1_2()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A1. 1-1 safety observation by RN");
            Program.VerboseAudit("A2. 1-1 safety observation by non-RN");
            Program.VerboseAudit("---------------");

            //string reslist;
            //string start_reslist;
            //string stop_reslist;
            //string return_result;
            //DateTime evdt, startdt, stopdt;
            bool risk_found = false;
            DateTime classout = DateTime.MinValue;
            bool typ6 = false;

            //if (_pat.ptype >= 6)
            //{
            //    Program.VerboseAudit("1-1 Observation activities will be suppressed due to Patient Type=" + _pat.ptype);
            //    return;
            //}

            DateTime return_evdt = DateTime.MinValue;
            var actlist = new List<CHART_ITEM>();

            string[] risks = { "Impulsive", "Lack of safety awareness",
                "Does not use call light or ask for assistance",
                "Danger to self or others",
                "Danger from others","Exit seeking","Epilepsy monitoring","Grid/SEEG electrodes" };

            string contrn = "Continuous observation by RN with patient";
            string contnonrn = "Continuous observation by non-RN staff with patient";
            string cont2 = "Continuous observation by two staff with patient";

            var query = StartNewQuery(SearchDepth.SearchSince13Hrs);    // add custom time range below
            query = query.Where(e => (e.CODE == "9993040009123" && risks.Any(v => e.RESULT.ToLower().Contains(v.ToLower())))
                                      ||
                                     (e.CODE == "9993040009234"));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            risk_found = (_pat.age < 5);
            foreach (var item in query)
            {
                if (item.CODE == "9993040009123")
                    risk_found = true;
            }
            if (risk_found)
            {
                foreach (var item in query)
                {
                    if (item.CODE == "9993040009234")
                    {
                        // item 234 is valid  save its code,result,time
                        actlist.Add(item);
                        Program.VerboseAudit("Valid:" + item.CODE + "+" + item.RESULT + " " + item.EVENT_DATETIME.ToString());
                    }
                }
            }
            //Now we have a list of all the valid 9234 items' times.
            //Use this list to determine the start/stop times
            CHART_ITEM[] actary = actlist.ToArray();
            int num_acts = 0;
            int actnum = 1;
            //  where there are adjacent same-locations, mark the duplicates as remove.
            for (int i = 0; i <= actary.GetUpperBound(0); i++)
            {
                num_acts++;
                Program.VerboseAudit("i:" + i + " result=" + actary[i].RESULT + " evdt=" + actary[i].EVENT_DATETIME.ToString());
                if (actary[i].RESULT.ToLower().Contains(contrn.ToLower())
                    || actary[i].RESULT.ToLower().Contains(contnonrn.ToLower())
                    || actary[i].RESULT.ToLower().Contains(cont2.ToLower()))
                {
                    //mark record as start
                    actary[i].DESCRIPTION = "start";
                }
                else
                {
                    //mark record as stop
                    actary[i].DESCRIPTION = "stop";
                }
            }
            //Now the list has start and stop marks.
            //Distill these mark info into start-stop records
            var start_stop_list = new List<proc_data>();
            var ssrec = new proc_data();
            int start_idx = -99;
            int stop_idx = -1;
            int pnum = 0;
            int this_pnum = 0;
            int ssnum = 0;
            int last_start = -99;
            int last_stop = -99;
            int mark_last_idx, mark_last_start, mark_last_stop, mark_pnum;

            for (int i = 0; i <= actary.GetUpperBound(0); i++)
            {
                Program.VerboseAudit("i:" + i + " start_idx=" + start_idx + " stop_idx=" + stop_idx + " desc=" + actary[i].DESCRIPTION + " evdt=" + actary[i].EVENT_DATETIME.ToString());
                if (start_idx < 0)
                {
                    if (actary[i].DESCRIPTION == "start")
                    {
                        Program.VerboseAudit("found start");
                        start_idx = i;
                        last_start = i;
                        if (actary[i].RESULT.ToLower().Contains(contrn.ToLower()))
                            if (_pat.ptype < 6) pnum = 1; else pnum = 0;
                        if (actary[i].RESULT.ToLower().Contains(contnonrn.ToLower()))
                            pnum = 2;
                        if (actary[i].RESULT.ToLower().Contains(cont2.ToLower()))
                            if (_pat.ptype < 6) pnum = 3; else pnum = 2;
                    }
                }
                else //if (start_idx >= 0)
                {
                    if (actary[i].RESULT.ToLower().Contains(contrn.ToLower()))
                        if (_pat.ptype < 6) this_pnum = 1; else this_pnum = 0;
                    if (actary[i].RESULT.ToLower().Contains(contnonrn.ToLower()))
                        this_pnum = 2;
                    if (actary[i].RESULT.ToLower().Contains(cont2.ToLower()))
                        if (_pat.ptype < 6) this_pnum = 3; else this_pnum = 2;
                    if (actary[i].DESCRIPTION == "stop" || pnum != this_pnum)
                    {
                        Program.VerboseAudit("found stop");
                        stop_idx = i;
                        last_stop = i;

                        if (actary[start_idx].EVENT_DATETIME < _pat.pull_finish.AddHours(-8)
                            && actary[stop_idx].EVENT_DATETIME > _pat.pull_finish.AddHours(-8))
                            actary[start_idx].EVENT_DATETIME = _pat.pull_finish.AddHours(-8);
                        else if (actary[start_idx].EVENT_DATETIME < _pat.pull_finish.AddHours(-8)
                                 && actary[stop_idx].EVENT_DATETIME < _pat.pull_finish.AddHours(-8))
                            // do not add
                            actary[start_idx].EVENT_DATETIME = DateTime.MinValue;
                        if (actary[start_idx].EVENT_DATETIME > DateTime.MinValue)
                        {
                            ssrec.procedure_number = pnum;
                            ssrec.start = actary[start_idx].EVENT_DATETIME;
                            ssrec.finish = actary[stop_idx].EVENT_DATETIME;
                            start_stop_list.Add(ssrec);
                            ssnum++;
                        }
                        start_idx = -99;
                        stop_idx = -99;
                        if (pnum != this_pnum)
                        {
                            Program.VerboseAudit("found start due to change in obs");

                            mark_last_idx = start_idx;
                            mark_last_start = last_start;
                            mark_last_stop = last_stop;
                            mark_pnum = pnum;
                            pnum = this_pnum;
                            start_idx = i;
                            last_start = i;
                            last_stop = -99;
                        }
                    }
                }
            }
            Program.VerboseAudit("last_start=" + last_start + " last_stop=" + last_stop);
            if (last_start >= 0 && last_stop == -99 || ssnum >= 2) //&& last_stop >= 0 && last_stop < last_start)
            {
                ssrec.procedure_number = pnum;
                ssrec.start = actary[last_start].EVENT_DATETIME;
                //ssrec.start = _pat.pull_finish.AddHours(-4);
                ssrec.finish = _pat.pull_finish;
                if (ssrec.start.AddMinutes(60) < ssrec.finish)
                {
                    start_stop_list.Add(ssrec);
                    ssnum++;
                    Program.VerboseAudit("ssnuma=" + ssnum);
                }
                else
                    Program.VerboseAudit("ignore due to less than 1 hr starta=" + ssrec.start.ToString() + " to " + ssrec.finish.ToString());
            }
            else if (last_start >= 0 && last_stop == -99)
            { // no stop --end at pulltime 
                ssrec.procedure_number = pnum;
                //ssrec.start = actary[last_start].EVENT_DATETIME;
                ssrec.start = _pat.pull_finish.AddHours(-4);
                ssrec.finish = _pat.pull_finish;
                start_stop_list.Add(ssrec);
                ssnum++;
                Program.VerboseAudit("ssnumb=" + ssnum);
            }

            if (ssnum > 0)
            {
                proc_data[] procary = start_stop_list.ToArray();
                for (int i = 0; i <= procary.GetUpperBound(0); i++)
                { //change the times in the start stop list in order to take care of l.t. 1hr legs
                    Program.VerboseAudit("procary[i]: i=" + i + " start= " + procary[i].start.ToString() + " stop: " + procary[i].finish.ToString());
                    if (procary[i].start.AddMinutes(60) > procary[i].finish)
                    {
                        procary[i].procedure_number = 0;
                        if (i + 1 <= procary.GetUpperBound(0))
                        {
                            if (procary[i + 1].start == procary[i].finish)
                            {
                                procary[i + 1].start = procary[i].start;
                            }

                        }

                    }
                }
                //start_stop_list = procary.ToList();

                string sflag = "";
                if (ssnum > 1) sflag = "s";
                Program.VerboseAudit("Num start-stop" + sflag + "=" + ssnum);
                DateTime enddt;
                //foreach (var ss in start_stop_list)
                for (int i = 0; i <= procary.GetUpperBound(0); i++)
                {
                    if (procary[i].procedure_number > 0)
                    {
                        var proc = new proc_data();
                        proc.procedure_number = procary[i].procedure_number;
                        proc.start = procary[i].start;
                        proc.finish = procary[i].finish;
                        Program.VerboseAudit("ssproc=" + procary[i].procedure_number + " ss start: " + procary[i].start.ToString() + " ss stop: " + procary[i].finish.ToString());
                        if (procary[i].procedure_number == 3)
                        {
                            if (ProcExistsInDB(1, procary[i].start, out enddt) || ProcExistsInDB(2, procary[i].start, out enddt))
                            {
                                proc.start = enddt;
                                Program.VerboseAudit("endta=" + enddt.ToString());
                            }
                            if (proc.start < proc.finish)
                            {
                                typ6 = ActivityDuringClassType6(proc.start, proc.finish, out classout);
                                proc.procedure_number = 2;
                                _procs.Add(proc);
                                if (typ6 && proc.finish > classout)
                                {
                                    proc.start = classout;
                                    Program.VerboseAudit("pnum6=" + proc.procedure_number + "  start: " + proc.start.ToString() + "  finish: " + proc.finish.ToString());
                                    Program.VerboseAudit("ADDING 2 ACTIVITIES 6");
                                    proc.procedure_number = 1;
                                    _procs.Add(proc);
                                    //proc.procedure_number = 2;
                                    //_procs.Add(proc);
                                }
                                if (!typ6 &&
                                    (_pat.ptype < 6 && (proc.start >= _pat.effective || proc.finish >= _pat.effective)))
                                {
                                    Program.VerboseAudit("pnum=" + proc.procedure_number + "  start: " + proc.start.ToString() + "  finish: " + proc.finish.ToString());
                                    Program.VerboseAudit("ADDING 2 ACTIVITIES");
                                    proc.procedure_number = 1;
                                    _procs.Add(proc);
                                    //proc.procedure_number = 2;
                                    //_procs.Add(proc);
                                }
                            }

                        }
                        else
                        {
                            if (ProcExistsInDB(procary[i].procedure_number, procary[i].start, out enddt))
                            {
                                //ss start: 6 / 19 / 2019 9:15:00 PM ss stop: 6 / 20 / 2019 7:00:00 AM
                                //pnum = 2  start: 6 / 20 / 2019 7:00:00 AM finish: 6 / 20 / 2019 7:00:00 AM
                                proc.start = enddt;
                                Program.VerboseAudit("endtb=" + enddt.ToString());
                            }
                            if (proc.start < proc.finish)
                            {
                                typ6 = ActivityDuringClassType6(proc.start, proc.finish, out classout);
                                //if (proc.procedure_number == 2 && typ6 && proc.finish > classout)
                                if (typ6 && proc.finish > classout)
                                {
                                    proc.start = classout;
                                    Program.VerboseAudit("pnum6=" + proc.procedure_number + "  start: " + proc.start.ToString() + "  finish: " + proc.finish.ToString());
                                    _procs.Add(proc);
                                }
                                else if (!typ6 &&
                                    (_pat.ptype < 6 && (proc.start >= _pat.effective || proc.finish >= _pat.effective)))
                                {
                                    if (proc.start.AddMinutes(60) > proc.finish)
                                    {
                                        Program.VerboseAudit("procary[i]: len<60min");
                                        if (i + 1 <= procary.GetUpperBound(0))
                                        {
                                            if (procary[i + 1].start == proc.finish)
                                            {
                                                proc.finish = proc.start.AddMinutes(60);
                                                procary[i + 1].start = proc.finish;
                                                Program.VerboseAudit("procary[i]: setting finish to=" + procary[i].finish.ToString());
                                                Program.VerboseAudit("procary[i]: setting start[i+1] to=" + procary[i + 1].start.ToString());
                                                if (procary[i + 1].start.AddMinutes(60) > procary[i + 1].finish)
                                                {
                                                    if (i + 2 <= procary.GetUpperBound(0))
                                                    {
                                                        if (procary[i + 2].start == procary[i + 1].finish)
                                                        {
                                                            procary[i + 1].finish = procary[i + 1].start.AddMinutes(60);
                                                            procary[i + 2].start = procary[i + 1].finish;
                                                            if (procary[i + 2].start.AddMinutes(60) > procary[i + 2].finish)
                                                                procary[i + 2].finish = procary[i + 2].start.AddMinutes(60);
                                                        }
                                                    }
                                                    else
                                                        procary[i + 1].finish = procary[i + 1].start.AddMinutes(60);
                                                }
                                            }
                                        }
                                        else
                                            proc.finish = proc.start.AddMinutes(60);
                                    }
                                    if (proc.start >= _pat.pull_start)
                                    {
                                        if (proc.start.AddMinutes(60) > _pat.unit_departure && _pat.unit_departure > DateTime.MinValue)
                                        {
                                            //this proc will be less that 60 mins because the out time will truncate it.
                                            //check if there is an earlier proc in this unit that ends at this proc.start time.
                                            //if there exists one, then modify it to end at this proc.finish time.
                                            Program.VerboseAudit("proc.start+1hr=" + proc.start.AddMinutes(60).ToString() + " _pat.unit_departure=" + _pat.unit_departure.ToString());
                                            AdjustHistProcEndTime(pnum, proc.start, proc.finish, _pat.unit_id);
                                        }
                                        else
                                        {
                                            Program.VerboseAudit("Apnum=" + proc.procedure_number + "  start: " + proc.start.ToString() + "  finish: " + proc.finish.ToString());
                                            _procs.Add(proc);
                                        }
                                    }
                                    else
                                    {
                                        Program.VerboseAudit("Bpnum=" + proc.procedure_number + "  start: " + proc.start.ToString() + "  finish: " + proc.finish.ToString());
                                        _procs.Add(proc);
                                    }

                                }
                            }
                        }
                    }
                }
            }
        }

        private bool AdjustHistProcEndTime(int pnum, DateTime procstart, DateTime procfinish, int unitid)
        {
            //this proc will be less that 60 mins because the out time will truncate it.
            //check if there is an earlier proc in this unit that ends at this proc.start time.
            //if there exists one, then modify it to end at this proc.finish time.
            Program.VerboseAudit("AdjustHistorical activity #" + pnum + " with endtime=" + procstart.ToString() + " to " + procfinish.ToString());
            int peid;
            DateTime hist_pstart;
            var db = PFSDBUtility.NewPfsDataContext();
            var query = from pe in db.PROCEDURE_EVENTs
                        from pa in db.PROCEDURE_ANSWERs
                        where (pe.ENCOUNTER_ID == _pat.encounter_id)
                        && (pe.DEPARTURE_DATETIME == procstart)
                        && (pa.PROCEDURE_EVENT_ID == pe.PROCEDURE_EVENT_ID)
                        && (pa.PROCEDURE_NUMBER == pnum)
                        && (pe.UNIT_ID == unitid)
                        select new
                        {
                            peid = pe.PROCEDURE_EVENT_ID,
                            pestart = pe.PROCEDURE_DATETIME,
                            peend = pe.DEPARTURE_DATETIME,
                            panum = pa.PROCEDURE_NUMBER
                        };
            if (query.Count() == 0)
            {
                Program.VerboseAudit("AdjustHistorical proc not found");
                return false;
            }
            Program.VerboseAudit("AdjustHistorical count=" + query.Count());
            var a = query.ToArray();
            for (int i=0; i <= a.GetUpperBound(0); i++)
            {
                Program.VerboseAudit("AdjustHistorical i=" + i + "panum=" + a[i].panum + " start=" + a[i].pestart + " end=" + a[i].peend);
                peid = a[i].peid;
                hist_pstart = a[i].pestart;

                DeleteActivity(peid);
                var proc = new proc_data();
                proc.procedure_number = pnum;
                proc.start = hist_pstart;
                proc.finish = procfinish;
                _procs.Add(proc);
            }

            return true;
        }


        private DateTime NextFinish(DateTime startdt)
        {
            DateTime dt = DateTime.MinValue;

            int b = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, startdt) / (4 * 60));
            dt = _pat.pull_start.AddMinutes((b + 1) * 240);
            Program.VerboseAudit("NextFinish: startdt=" + startdt.ToString() + " b=" + b + " dt=" + dt.ToString());
            return dt;
        }


        private bool ProcExists(int pnum, DateTime startdt, DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && (proc.PROCEDURE_DATETIME == startdt)
                            && (proc.DEPARTURE_DATETIME == enddt)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new { proc.PROCEDURE_EVENT_ID };
            return (query.Count() > 0);
        }


        private void CheckProc_3()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A3. Off unit accompanied by RN");
            Program.VerboseAudit("---------------");
            string start_reslist;
            string stop_reslist;
            DateTime startdt, stopdt;

            // private bool GetEVDT(string cat, string code_list, string desc_list, string field, 
            //string res, 
            //int comparison, DateTime compevdt, out DateTime return_evdt, SearchDepth search_depth)
            bool suppress = false;
            foreach (var p in _procs)
            {
                suppress |= (p.procedure_number == 1 || p.procedure_number == 2);
            }
            if (suppress)
            {
                Program.VerboseAudit("Suppressing Activity #3 because Activity #1 or #2 present.");
                return;
            }

            DateTime return_evdt = DateTime.MinValue;
            start_reslist = "Left unit with RN";
            int ret_type = 0;
            var query1 = StartNewQuery(SearchDepth.SearchSince13Hrs);
            query1 = AndItemFilter(query1, "", "9993040000355", "", "", start_reslist);
            query1 = query1.OrderBy(e => e.EVENT_DATETIME);
            foreach (var item in query1)
            {
                startdt = item.EVENT_DATETIME;
                stopdt = FindNextEvent(item.EVENT_DATETIME,out ret_type);
                if (ret_type == 1)
                { //return
                    if (startdt.AddMinutes(60) <= stopdt)
                        AddSimpleProc(3, startdt, stopdt);
                }
                else if (ret_type == 2)
                { //9990000305560 
                    if (startdt.AddMinutes(60) <= stopdt.AddMinutes(15))
                        AddSimpleProc(3, startdt, stopdt.AddMinutes(15));
                }
            }

            //            "Determine time inbetween the results. If return to unit is not found, default to 1 hour.

            //Except in instances where charting if found in AC4 - 3 within the hour.
            //  Add 15 minutes to the end of the finish timestamp."
            //9990000305560
            //string reslist = "Ambulate in hall,Ambulate in room,Bathroom privileges,Bedrest,Bike,Chair,Commode";
            //reslist += ",Dangle,Supervised exercise,Held,Stand at bedside,Tilt table,Turn,Up in chair";
            //reslist += ",Wagon,In bed,Bedpan";

        }

        private DateTime FindNextEvent(DateTime startdt, out int ret_type)
        {
            string reslist = "Ambulate in hall,Ambulate in room,Bathroom privileges,Bedrest,Bike,Chair,Commode";
            reslist += ",Dangle,Supervised exercise,Held,Stand at bedside,Tilt table,Turn,Up in chair";
            reslist += ",Wagon,In bed,Bedpan";
            string retlist = "Returned to unit";
            DateTime dt = DateTime.MinValue;
            ret_type = 0;
            var query1 = StartNewQuery(SearchDepth.SearchSince13Hrs);
            query1 = query1.Where(e => e.CODE.StartsWith("9993040000355") && e.RESULT.ToLower().Contains(retlist.ToLower())
            || e.CODE.StartsWith("9990000305560"));
            query1 = query1.Where(e => e.EVENT_DATETIME > startdt);
            query1 = query1.OrderBy(e => e.EVENT_DATETIME);
            if (query1.Count() == 0)
            {
                return dt;
            }
            string cd = query1.First().CODE;
            if (cd.StartsWith("9993040000355"))
            {
                ret_type = 1;
                dt = query1.First().EVENT_DATETIME;
            }
            if (cd.StartsWith("9990000305560"))
            {
                if (reslist.ToLower().Contains(query1.First().RESULT.ToLower()))
                {
                    ret_type = 2;
                    dt = query1.First().EVENT_DATETIME;
                }
            }

            return dt;
        }

        private void CheckProc_4()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A4. Off unit accompanied by non-RN");
            Program.VerboseAudit("---------------");
            string start_reslist;
            string stop_reslist;
            DateTime startdt, stopdt;

            DateTime return_evdt = DateTime.MinValue;
            bool suppress = false;
            foreach (var p in _procs)
            {
                suppress |= (p.procedure_number == 1 || p.procedure_number == 2);
            }
            if (suppress)
            {
                Program.VerboseAudit("Suppressing Activity #4 because Activity #1 or #2 present.");
                return;
            }

            start_reslist = "Left unit with nursing unlicensed staff";
            int ret_type = 0;
            var query1 = StartNewQuery(SearchDepth.SearchSince13Hrs);
            query1 = AndItemFilter(query1, "", "9993040000355", "", "", start_reslist);
            query1 = query1.OrderBy(e => e.EVENT_DATETIME);
            foreach (var item in query1)
            {
                startdt = item.EVENT_DATETIME;
                stopdt = FindNextEvent(item.EVENT_DATETIME, out ret_type);
                if (ret_type == 1)
                { //return
                    if (startdt.AddMinutes(60) <= stopdt)
                        AddSimpleProc(4, startdt, stopdt);
                }
                else if (ret_type == 2)
                { //9990000305560 
                    if (startdt.AddMinutes(60) <= stopdt.AddMinutes(15))
                        AddSimpleProc(4, startdt, stopdt.AddMinutes(15));
                }
            }

        }

        private void CheckProc_5()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A5. Patient/family education by RN");
            Program.VerboseAudit("---------------");

            DateTime evdt;
            string desc;
            string cd1;
            string[] educodes = { "3040019666","3040019679","3040018932","3040019011","3040019707","3040019721","3040019691",
            "3040018910","3040020431","3040020730","3040020717","3040020425","3040019158",
            "3040019666","3040019679",
"3040018932","3040019011","3040019707","3040019721","3040019691","3040018910","3040020431",
"3040020730","3040020717","3040020425","3040019158" };


            var query1 = StartNewQuery(SearchDepth.SearchSince9Hrs);
            query1 = AndItemFilter(query1, "", "EDU", "", "", "");
            query1 = query1.Where(e => educodes.Any(v => e.CODE.ToUpper().Contains(v+"METHOD")) && e.RESULT.ToUpper().Contains("D") 
                    || educodes.Any(v => e.CODE.ToUpper().Contains(v + "RESPONSE")) && e.RESULT.ToUpper().Contains("TBDEMO"));
            var query1b = (from item in query1
                           select new { evdt = item.EVENT_DATETIME, descr = item.DESCRIPTION.Substring(0, item.DESCRIPTION.IndexOf(";;;")) }
                                       ).Distinct();
            foreach (var item1 in query1b)
            {
                Program.VerboseAudit("EDU: Description=" + item1.descr + " ending at " + item1.evdt.ToString());
                AddEducActivity(item1.evdt);
            }


        }

        private bool WoundTypeGiven(DateTime evdt)
        {
            bool found = false;
            string reslist;

            var query = StartNewQuery(SearchDepth.SearchSince9Hrs);
            query = AndItemFilter(query, "", "9990000303820,9990007073550,9990007070178,9993040104037,9993040023765,9993040107858,9993040107859", "", "", "");
            query = query.Where(e => e.EVENT_DATETIME == evdt);
            found |= (query.Count() > 0);
            if (found) return true;

            var query3 = StartNewQuery(SearchDepth.SearchSince9Hrs);
            reslist = "Clean,Dry,Intact,Changed,New drainage,Old drainage";
            query3 = AndItemFilter(query3, "", "9990000304850,9990000303910", "", "", reslist);
            query3 = query3.Where(e => e.EVENT_DATETIME == evdt);
            found |= (query3.Count() > 0);
            if (found) return true;

            var query2 = StartNewQuery(SearchDepth.SearchSince9Hrs);
            reslist = "Abdominal dressing,Alginate,Antimicrobial dressing,Cast,Cellular tissue based product,Charcoal,Collagen,Composite,Compression Wrap,Foam,Gauze,Gelling fiber/hydrofiber,Hemostatic dressing,Hydrocolloid,Hydrogel,Impregnated gauze,Impregnated Gauze wrap,Moist to dry,Moist to moist,non adherent,Packing,Petroleum gauze,Pressure dressing,Silicone dressing,Split gauze,Transparent dressing,Wound filler,Wound gel";
            query2 = AndItemFilter(query2, "", "9990000303800", "", "", reslist);
            query2 = query2.Where(e => e.EVENT_DATETIME == evdt);
            found |= (query2.Count() > 0);

            return found;
        }

        private void CheckProc_6()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A6. Extensive wound management by RN");
            Program.VerboseAudit("---------------");
            string start_reslist;
            string fullreslist;
            string stop_reslist;
            DateTime startdt, enddt, evdt;
            bool proceed = false;
            int mins = 0;
            int ct = 0;
            DateTime return_evdt = DateTime.MinValue;

            string[] woundmins = { "> 180 minutes", "> 120 minutes", "> 90 minutes", "> 60 minutes" };
            string actor1 = "Unit Based Nurse";
            string actor2 = "Additional Nurse assist required";
            string actor3 = "Patient;completed;with assistance";
            int numactors = 0;

            var query1 = StartNewQuery(SearchDepth.SearchSince9Hrs);
            query1 = AndItemFilter(query1, "", "9993040006782", "", "", "");
            query1 = query1.Where(e => woundmins.Any(v => e.RESULT == v));
            query1 = query1.OrderBy(e => e.EVENT_DATETIME);

            var query1b = (from item in query1
                           select new { evdt = item.EVENT_DATETIME, res = item.RESULT }
                                       ).Distinct();
            foreach (var item1 in query1b)
            {
                if (WoundTypeGiven(item1.evdt))
                {
                    numactors = 0;
                    Program.VerboseAudit("Found result=" + item1.res + " at " + item1.evdt.ToString());
                    var query2 = StartNewQuery(SearchDepth.SearchSince9Hrs);
                    query2 = AndItemFilter(query2, "", "9993040021271", "", "", "Unit Based Nurse,Additional Nurse assist required,Patient;completed;with assistance");
                    query2 = query2.Where(e => e.EVENT_DATETIME == item1.evdt);
                    ct = query2.Count();
                    if (ct > 0)
                        foreach (var item2 in query2)
                        {
                            Program.VerboseAudit("Found staff=" + item2.RESULT);
                            numactors = (item2.RESULT.ToUpper().Contains(actor1.ToUpper()) ? 1 : 0)
                                + (item2.RESULT.ToUpper().Contains(actor2.ToUpper()) ? 1 : 0)
                                + (item2.RESULT.ToUpper().Contains(actor3.ToUpper()) ? 1 : 0);
                            if (numactors == 1)
                            {
                                evdt = item1.evdt;
                                mins = (int)item1.res.Substring(2, item1.res.IndexOf("min") - 3).Val();
                                Program.Audit("Activity 6: Minutes=" + mins);
                                if (mins >= 60)
                                    if (!QueuedProcOverlaps(6, evdt, evdt.AddMinutes(mins)))
                                        if (!ProcExistsInDB(6, evdt, out enddt))
                                        {
                                            var proc = new proc_data();
                                            proc.procedure_number = 6;
                                            proc.start = evdt;
                                            proc.finish = evdt.AddMinutes(mins);
                                            _procs.Add(proc);
                                            Program.Audit("Activity 6: Found at " + evdt + " for " + mins + " mins.");
                                        }
                            }
                        }
                    else if (ct == 0)
                    {
                        var query3 = StartNewQuery(SearchDepth.SearchSince9Hrs);
                        query3 = AndItemFilter(query3, "", "9993040021271", "", "", "");
                        query3 = query3.Where(e => e.EVENT_DATETIME == item1.evdt);
                        query3 = query3.Where(e => !e.RESULT.ToUpper().Contains("Unit Based Nurse".ToUpper())
                                                && !e.RESULT.ToUpper().Contains("Additional Nurse assist required".ToUpper())
                                                && !e.RESULT.ToUpper().Contains("Patient;completed;with assistance".ToUpper()));
                        ct = query3.Count();
                        if (ct == 0)
                        {
                            evdt = item1.evdt;
                            mins = (int)item1.res.Substring(2, item1.res.IndexOf("min") - 3).Val();
                            Program.Audit("Activity 6: Minutes=" + mins);
                            if (mins >= 60)
                                if (!QueuedProcOverlaps(6, evdt, evdt.AddMinutes(mins)))
                                    if (!ProcExistsInDB(6, evdt, out enddt))
                                    {
                                        var proc = new proc_data();
                                        proc.procedure_number = 6;
                                        proc.start = evdt;
                                        proc.finish = evdt.AddMinutes(mins);
                                        _procs.Add(proc);
                                        Program.Audit("Activity 6: Found at " + evdt + " for " + mins + " mins.");
                                    }
                        }
                    }
                } // woundtypegiven
            } //foreach


        }

        private void CheckProc_7()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A7. Extensive wound management by non-RN");
            Program.VerboseAudit("---------------");

            DateTime startdt, enddt, evdt;
            int mins = 0;
            int ct = 0;
            DateTime return_evdt = DateTime.MinValue;

            string[] woundmins = { "> 180 minutes", "> 120 minutes", "> 90 minutes", "> 60 minutes" };

            var query1 = StartNewQuery(SearchDepth.SearchSince9Hrs);
            query1 = AndItemFilter(query1, "", "9993040006782", "", "", "");
            query1 = query1.Where(e => woundmins.Any(v => e.RESULT == v));
            query1 = query1.OrderBy(e => e.EVENT_DATETIME);

            var query1b = (from item in query1
                           select new { evdt = item.EVENT_DATETIME, res = item.RESULT }
                                       ).Distinct();
            foreach (var item1 in query1b)
            {
                if (WoundTypeGiven(item1.evdt))
                {
                    Program.VerboseAudit("Found result=" + item1.res + " at " + item1.evdt.ToString());
                    var query2 = StartNewQuery(SearchDepth.SearchSince9Hrs);
                    query2 = AndItemFilter(query2, "", "9993040021271", "", "", "Additional unlicensed assist");
                    query2 = query2.Where(e => e.EVENT_DATETIME == item1.evdt);
                    ct = query2.Count();
                    if (ct > 0)
                        foreach (var item2 in query2)
                        {
                            Program.VerboseAudit("Found staff=" + item2.RESULT);
                            evdt = item1.evdt;
                            mins = (int)item1.res.Substring(2, item1.res.IndexOf("min") - 3).Val();
                            Program.Audit("Activity 7: Minutes=" + mins);
                            if (mins >= 60)
                                if (!QueuedProcOverlaps(7, evdt, evdt.AddMinutes(mins)))
                                    if (!ProcExistsInDB(7, evdt, out enddt))
                                    {
                                        var proc = new proc_data();
                                        proc.procedure_number = 7;
                                        proc.start = evdt;
                                        proc.finish = evdt.AddMinutes(mins);
                                        _procs.Add(proc);
                                        Program.Audit("Activity 7: Found at " + evdt + " for " + mins + " mins.");
                                    }
                        }
                }//woundtypegiven
            }//foreach
        }

        private void CheckProc_8()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A8. Coordination of care by RN");
            Program.VerboseAudit("---------------");
            DateTime evdt;
            DateTime enddt;
            string res;
            int mins = 0;
            int idx = 0;
            int ct2 = 0;

            var query1 = StartNewQuery(SearchDepth.SearchSince9Hrs);
            query1 = AndItemFilter(query1, "", "9993040004578", "", "", "");
//            query1 = query1.Where(e => e.RESULT.Substring(0, e.RESULT.IndexOf(" ")).Val() >= 60);
            query1 = query1.OrderBy(e => e.EVENT_DATETIME);

            
            foreach (var item1 in query1)
            {
                var query2 = StartNewQuery(SearchDepth.SearchSince9Hrs);
                query2 = AndItemFilter(query2, "", "9990007060440", "", "", "Care conference");
                query2 = query2.Where(e => e.EVENT_DATETIME == item1.EVENT_DATETIME);
                ct2 = query2.Count();

                if (ct2 > 0)
                {
                    idx = item1.RESULT.IndexOf(" ");
                    if (idx > 0)
                    {
                        mins = (int)item1.RESULT.Substring(0, idx).Val();
                        if (mins >= 60)
                        {
                            Program.VerboseAudit("Found result=" + item1.RESULT + " at " + item1.EVENT_DATETIME.ToString());
                            evdt = item1.EVENT_DATETIME;
                            //mins = (int)item1.RESULT.Substring(0, item1.RESULT.IndexOf(" ")).Val();
                            if (mins >= 60)
                                if (!QueuedProcOverlaps(8, evdt, evdt.AddMinutes(mins)))
                                    if (!ProcExistsInDB(8, evdt, out enddt))
                                    {
                                        var proc = new proc_data();
                                        proc.procedure_number = 8;
                                        proc.start = evdt;
                                        proc.finish = evdt.AddMinutes(mins);
                                        _procs.Add(proc);
                                        Program.Audit("Activity 8: Found at " + evdt + " for " + mins + " mins.");
                                    }
                        }
                    }
                }

            }
        }

        private void CheckProc_9()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A9. 1:1 RN at bedside");
            Program.VerboseAudit("---------------");

            string codelist = "9993040001033"; //,9993040001041";
            DoAppliedRemoved(codelist, "Leech therapy");
            codelist = "9993040001041";
            DoAppliedRemoved(codelist, "Maggot therapy");
            //2018 - 12 - 14 04:25:00.000 Leech Application or Removal    Applied
            //2018 - 12 - 14 08:00:00.000 Leech Application or Removal    Applied
            //2018 - 12 - 14 21:00:00.000 Leech Application or Removal    Removed
            //2018 - 12 - 14 23:00:00.000 Leech Application or Removal    Applied
            //2018 - 12 - 14 23:30:00.000 Leech Application or Removal    Applied
            //2018 - 12 - 15 00:00:00.000 Leech Application or Removal    Applied
            //2018 - 12 - 15 01:00:00.000 Leech Application or Removal    Removed
            //2018 - 12 - 15 02:00:00.000 Leech Application or Removal    Removed
            //2018 - 12 - 15 03:00:00.000 Leech Application or Removal    Applied
            //2018 - 12 - 15 04:00:00.000 Leech Application or Removal    Removed
            //2018 - 12 - 15 05:00:00.000 Leech Application or Removal    Applied
            //2018 - 12 - 15 06:00:00.000 Leech Application or Removal    Removed
            //2018 - 12 - 15 08:00:00.000 Leech Application or Removal    Removed


            //            9993040001560   num
            //9993040001561   1,2,3,4+
            //9993040000352 "Multiple Staff for acute behavioral 

            //9991025062116 + 60 minutes
            DateTime return_evdt;
            DateTime enddt;
            if (GetEVDT("", "9991025062116", "", "", "60", 0, DateTime.MinValue, out return_evdt, SearchDepth.SearchSince9Hrs))
            {
                if (return_evdt > DateTime.MinValue)
                {
                    if (!QueuedProcOverlaps(9, return_evdt, return_evdt.AddMinutes(60)))
                        if (!ProcExistsInDB(9, return_evdt, out enddt))
                        {
                            var proc = new proc_data();
                            proc.procedure_number = 9;
                            proc.start = return_evdt;
                            proc.finish = return_evdt.AddMinutes(60);
                            _procs.Add(proc);
                            Program.Audit("Activity 9: ATT Found at " + return_evdt + " for 1 hour.");
                        }

                }
            }

            //AC9-13
            //This should be added as a “1:1 RN at bedside” activity for 1 hour.
            //AC9 - 13  9   1:1 by RN at the bedside    Seclusion(V)       300036  9990000300036               Start

            if (GetEVDT("", "9990000300036", "", "", "start", 0, DateTime.MinValue, out return_evdt, SearchDepth.SearchSince9Hrs))
            {
                if (return_evdt > DateTime.MinValue)
                {
                    if (!QueuedProcOverlaps(9, return_evdt, return_evdt.AddMinutes(60)))
                        if (!ProcExistsInDB(9, return_evdt, out enddt))
                        {
                            var proc = new proc_data();
                            proc.procedure_number = 9;
                            proc.start = return_evdt;
                            proc.finish = return_evdt.AddMinutes(60);
                            _procs.Add(proc);
                            Program.Audit("Activity 9: Seclusion Found at " + return_evdt + " for 1 hour.");
                        }

                }

            }
            //string chemoname = "CARBOPLATIN";
            //AddChemoProc(chemoname);
            //chemoname = "OXALIPLATIN";
            //AddChemoProc(chemoname);
            string chemoname;
            AddChemoProc("platin");

            chemoname = "VSV-HIFN BETA-NIS INTRAVENOUS";
            AddChemoOneDay(chemoname);

            // Intubation should create activity for 1 hour: 5/20/21
            //  OBX|1|DT|RT24^INTUBATION||20210519110400||||||U|||20210519110400||
            DateTime startdt,stopdt;
            return_evdt = DateTime.MinValue;
            var query1 = StartNewQuery(SearchDepth.SearchSince9Hrs);
            query1 = AndItemFilter(query1, "", EXACT_MATCH_PREFIX+"RT24", "INTUBATION", "", "");
            query1 = query1.OrderBy(e => e.EVENT_DATETIME);
            foreach (var item in query1)
            {
                startdt = item.EVENT_DATETIME;
                stopdt = startdt.AddHours(1);
                AddSimpleProc(9, startdt, stopdt);
            }
        }

        private int ReturnProcEventID(int pnum, DateTime stdt)
        {
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && (proc.PROCEDURE_DATETIME == stdt)
                            && (proc.CLASSIFIED_BY_ID < 0)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new { proc.PROCEDURE_EVENT_ID };
            if (query.Count() > 0)
            {
                return query.First().PROCEDURE_EVENT_ID;
            }
            else
                return 0;
        }

    private void AddChemoProc(string chemoname)
        {
            int peid;
            DateTime stopdt = DateTime.MinValue;
            DateTime stdt = DateTime.MinValue;

            if (PreviouslyStartedChemo(chemoname, out stdt))
            {
                Program.VerboseAudit("_pat.pull_finish=" + _pat.pull_finish);
                var query2 = StartNewQuery(SearchDepth.SearchSince24Hrs);
                //query2 = AndItemFilter(query2, "", "MED", chemoname, "", "");
                query2 = AndItemFilter(query2, "", "MED", "", "", "");
                query2 = query2.Where(e => e.DESCRIPTION.ToLower().Contains("platin"));
                query2 = query2.Where(e => e.DESCRIPTION.ToLower().Contains("stopped"));
                query2 = query2.Where(e => e.ORDER_STATUS.ToLower() != "xx");
                query2 = query2.OrderByDescending(e => e.EVENT_DATETIME);
                if (query2.Count() > 0)
                {
                    peid = ReturnProcEventID(9, stdt.AddHours(-1));
                    DeleteActivity(peid);

                    stopdt = query2.First().EVENT_DATETIME;
                    Program.VerboseAudit("prev9: extension=" + stdt.ToString() + " end="+stopdt);
                    AddSimpleProc(9, stdt.AddHours(-1), stopdt.AddMinutes(15));

                    UpdateChemo(false, "platin", stdt, stopdt);
                }
            }
            else
            {
                var query1 = StartNewQuery(SearchDepth.SearchSince24Hrs);
                query1 = AndItemFilter(query1, "", "MED", "", "", "");
                //query1 = query1.Where(e => e.DESCRIPTION.ToUpper().Contains("DESENSITIZATION"));
                query1 = query1.Where(e => e.DESCRIPTION.ToLower().Contains("platin"));
                query1 = query1.Where(e => e.DESCRIPTION.ToLower().Contains("start"));
                //query1 = query1.Where(e => e.ORDER_STATUS.ToLower().Contains("x"));
                query1 = query1.OrderBy(e => e.EVENT_DATETIME);
                DateTime startdt = DateTime.MinValue;
                DateTime resumedt = DateTime.MinValue;
                Program.VerboseAudit("proc9 "+chemoname+" ...qct=" + query1.Count());
                foreach (var item1 in query1)
                {
                    if (resumedt <= startdt)
                        if (startdt < item1.EVENT_DATETIME)
                        {
                            if (ThreeStartsIn1Hr(chemoname, item1.EVENT_DATETIME))
                            {
                                startdt = item1.EVENT_DATETIME;
                                resumedt = FindFinishTime(chemoname, startdt);
                                DateTime s, f;
                                if (resumedt > DateTime.MinValue)
                                {
                                    Program.VerboseAudit("start=" + startdt.ToString() + " stopbefore=" + resumedt.ToString());
                                    s = startdt.AddMinutes(-60);
                                    if (resumedt.AddMinutes(15) > _pat.effective_out)
                                        f = _pat.effective_out;
                                    else
                                        f = resumedt.AddMinutes(15);
                                    AddSimpleProc(9, s, f);
                                    UpdateChemo(true, "platin", startdt,startdt);


                                }
                                else
                                {
                                    if (startdt < _pat.pull_finish)
                                    {
                                        s = startdt.AddMinutes(-60);
                                        f = _pat.pull_finish;
                                        AddSimpleProc(9, s, f);
                                        UpdateChemo(true, "platin", startdt,startdt);

                                    }
                                }
                            }
                        }
                } //foreach
            }

        }

        private void AddChemoOneDay(string chemoname)
        {
            int ct = 0;
            DateTime startdt = DateTime.MinValue;
            DateTime stopdt = DateTime.MinValue;

            var query1 = StartNewQuery(SearchDepth.SearchSince24Hrs);
            query1 = AndItemFilter(query1, "", "MED", chemoname, "", "");
            query1 = query1.Where(e => e.DESCRIPTION.ToLower().ContainsAny(meds_rate) || e.DESCRIPTION.ToLower().Contains("stopped"));
            query1 = query1.Where(e => !e.ORDER_STATUS.ToLower().Contains("x"));
            ct = query1.Count();
            Program.VerboseAudit("proc9 " + chemoname + " ...qct=" + ct);
            if (ct <= 0) return;

            CHART_ITEM ch = query1.OrderBy(e => e.EVENT_DATETIME).FirstOrDefault();
            startdt = ch.EVENT_DATETIME.Date.AddHours(7);
            stopdt = startdt.AddDays(1).AddMinutes(-1);
            Program.VerboseAudit("start=" + startdt + " stop=" + stopdt + " found start=" + ch.EVENT_DATETIME.ToString());
            AddSimpleProc(9, startdt, stopdt);
            UpdateChemo(false, chemoname, startdt, stopdt);
        }

        private void UpdateChemo(bool is_start, string chemoname, DateTime startdt,DateTime stopdt)
        {
            string strval="";
            var db = PFSDBUtility.NewSqlConnection();
            string q;
            if (is_start)
                strval = "ST";
            else
                strval = "XX";
            chemoname.ToLower();
            if (is_start)
                q = "UPDATE chart_item set ORDER_STATUS='" + strval + "' where encounter_id=" + _pat.encounter_id + " and event_datetime='" + startdt.ToString() + "' and code like 'med%' and description like '%" + chemoname + "%'";
            else
                q = "UPDATE chart_item set ORDER_STATUS='" + strval + "' where encounter_id=" + _pat.encounter_id + " and event_datetime>'" + startdt.ToString() + "' and event_datetime<='" + stopdt.ToString() + "' and code like 'med%' and description like '%" + chemoname + "%'";
            Program.VerboseAudit("upd chemo q=" + q);
            SqlCommand cmd = new SqlCommand(q, db);
            cmd.ExecuteNonQuery();
            db.Close();
        }

        private bool PreviouslyStartedChemo(string chemoname, out DateTime startdt)
        {
            bool ret = false;
            int ct = 0;
            startdt = DateTime.MinValue;

            var query1 = StartNewQuery(SearchDepth.SearchSince24Hrs);
            query1 = AndItemFilter(query1, "", "MED", "", "", "");
            query1 = query1.Where(e => e.DESCRIPTION.ToLower().Contains("platin"));
            query1 = query1.Where(e => e.ORDER_STATUS.ToLower() == "st");
            query1 = query1.OrderBy(e => e.EVENT_DATETIME);
            ct = query1.Count();
            ret = (ct > 0);
            if (ret) startdt = query1.First().EVENT_DATETIME;
            Program.VerboseAudit("previously started chemo q=" + ct + ": " + chemoname + " - " + startdt);
            return ret;
        }
        private bool ThreeStartsIn1Hr(string chemo,DateTime start)
        { // CHANGE to within 90 MINUTES 12/17/20
            bool ret = false;
            var query1 = StartNewQuery(SearchDepth.SearchSince16Hrs);
            query1 = AndItemFilter(query1, "", "MED", "", "", "");
            //query1 = query1.Where(e => e.DESCRIPTION.ToUpper().Contains("DESENSITIZATION"));
            query1 = query1.Where(e => e.DESCRIPTION.ToLower().Contains("platin"));
            query1 = query1.Where(e => e.DESCRIPTION.ToLower().Contains("start"));
            query1 = query1.Where(e => e.EVENT_DATETIME >= start && e.EVENT_DATETIME <= start.AddMinutes(90));
            ret = (query1.Count() >= 3);
            return ret;
        }
//08:22:00.000 CARBOPLATIN 0.0001 MG INJECTION DESENSITIZATION(CNR);;;16;;;0.0001;;;IV;;;Started / Down
//08:44:00.000 CARBOPLATIN 0.0001 MG INJECTION DESENSITIZATION(CNR);;;16;;;0;;;IV;;;Stopped
//08:47:00.000 CARBOPLATIN 0.0001 MG INJECTION DESENSITIZATION(CNR);;;16;;;0.001;;;IV;;;Started / Down
//09:07:00.000 CARBOPLATIN 0.0001 MG INJECTION DESENSITIZATION(CNR);;;16;;;0;;;IV;;;Stopped
//09:11:00.000 CARBOPLATIN 0.01 MG INJECTION DESENSITIZATION(CNR);;;16;;;0.01;;;IV;;;Started / Down
//09:33:00.000 CARBOPLATIN 0.01 MG INJECTION DESENSITIZATION(CNR);;;16;;;0;;;IV;;;Stopped
//09:59:00.000 CARBOPLATIN 0.01 MG INJECTION DESENSITIZATION(CNR);;;16;;;0;;;IV;;;Stopped
//10:03:00.000 CARBOPLATIN 10 MG / ML INTRAVENOUS SOLUTION (FOR DESENSITIZATION);;;16;;;24.9;;;IV;;;Started / Down
//13:20:00.000 CARBOPLATIN 10 MG / ML INTRAVENOUS SOLUTION (FOR DESENSITIZATION);;;16;;;0;;;IV;;;Stopped
//13:24:00.000 CARBOPLATIN 10 MG / ML INTRAVENOUS SOLUTION;;;16;;;390;;;IV;;;Started / Down
//17:41:00.000 CARBOPLATIN 10 MG / ML INTRAVENOUS SOLUTION;;;16;;;0;;;IV;;;Stopped
        private DateTime FindFinishTime(string chemo,DateTime startdt)
        {
            DateTime dt = DateTime.MinValue;
            var query1 = StartNewQuery(SearchDepth.SearchSince16Hrs);
            query1 = AndItemFilter(query1, "", "MED", "", "", "");
            //query1 = query1.Where(e => e.DESCRIPTION.ToUpper().Contains("DESENSITIZATION"));
            query1 = query1.Where(e => e.DESCRIPTION.ToLower().Contains("platin"));
            query1 = query1.Where(e => e.DESCRIPTION.ToLower().Contains("start"));
            query1 = query1.Where(e => e.EVENT_DATETIME >= startdt);
            query1 = query1.OrderBy(e => e.EVENT_DATETIME);
            foreach (var item in query1)
            { // for each start, find the latest stop after it by finding the next start time
              // and then finding the earliest stop before that start time.
                if (dt == DateTime.MinValue)
                {
                    DateTime dt1 = FindStartAfterStart(chemo,item.EVENT_DATETIME);
                    DateTime dt2 = FindStopBeforeStart(chemo,dt1);
                    if (dt2 > DateTime.MinValue)
                    {
                        Program.VerboseAudit("start=" + dt1.ToString() + " stopbefore=" + dt2.ToString());
                        if (dt2.AddMinutes(60) < dt1)
                        { // then stop.  Use this stop time
                            dt = dt2;
                        }
                    }
                }
            }
            return dt;
        }
        private DateTime FindStartAfterStart(string chemo,DateTime startdt)
        {
            DateTime dt = DateTime.MaxValue;
            var query1 = StartNewQuery(SearchDepth.SearchSince16Hrs);
            query1 = AndItemFilter(query1, "", "MED", "", "", "");
            //query1 = query1.Where(e => e.DESCRIPTION.ToUpper().Contains("DESENSITIZATION"));
            query1 = query1.Where(e => e.DESCRIPTION.ToLower().Contains("platin"));
            query1 = query1.Where(e => e.DESCRIPTION.ToLower().Contains("start"));
            query1 = query1.Where(e => e.EVENT_DATETIME > startdt);
            query1 = query1.OrderBy(e => e.EVENT_DATETIME);
            if (query1.Count() > 0)
                dt = query1.First().EVENT_DATETIME;
            return dt;
        }
        private DateTime FindStopBeforeStart(string chemo, DateTime startdt)
        {
            DateTime dt = DateTime.MinValue;
            var query1 = StartNewQuery(SearchDepth.SearchSince16Hrs);
            query1 = AndItemFilter(query1, "", "MED", "", "", "");
            //query1 = query1.Where(e => e.DESCRIPTION.ToUpper().Contains("DESENSITIZATION"));
            query1 = query1.Where(e => e.DESCRIPTION.ToLower().Contains("platin"));
            query1 = query1.Where(e => e.DESCRIPTION.ToLower().Contains("stop"));
            query1 = query1.Where(e => e.EVENT_DATETIME < startdt);
            query1 = query1.OrderByDescending(e => e.EVENT_DATETIME);
            if (query1.Count() > 0)
                dt = query1.First().EVENT_DATETIME;
            return dt;
        }

        private void DoAppliedRemoved(string code, string foundstr)
        {
            var start_stop_list = new List<proc_data>();
            var ssrec = new proc_data();

            bool foundappl = false;
            bool firstrec = true;

            var query1 = StartNewQuery(SearchDepth.SearchSince9Hrs);
            query1 = AndItemFilter(query1, "", code, "", "", "applied,removed");
            query1 = query1.OrderBy(e => e.EVENT_DATETIME);
            foreach (var item1 in query1)
            {
                Program.VerboseAudit("Found code=" + item1.CODE + " result=" + item1.RESULT + " at " + item1.EVENT_DATETIME.ToString());
                if (firstrec)
                {
                    firstrec = false;
                    if (item1.RESULT.ToLower() == "applied")
                    {
                        ssrec.start = item1.EVENT_DATETIME;
                        foundappl = true;
                    }
                    else // if (item1.RESULT.ToLower() == "removed")
                    {
                        ssrec.start = GetAppliedBeforeRemove(code,item1.EVENT_DATETIME);
                        ssrec.finish = item1.EVENT_DATETIME;
                        if (ssrec.finish > ssrec.start)
                        {
                            ssrec.procedure_number = 9;
                            foundappl = false;
                            start_stop_list.Add(ssrec);
                        }
                    }
                }
                else if (item1.RESULT.ToLower() == "applied")
                {
                    // if foundappl then forget that old applied. use this one.
                    ssrec.start = item1.EVENT_DATETIME;
                    foundappl = true;
                }
                else if (item1.RESULT.ToLower() == "removed")
                {
                    // if foundappl then forget that old applied. use this one.
                    if (foundappl)
                    {
                        ssrec.finish = item1.EVENT_DATETIME;
                        ssrec.procedure_number = 9;
                        foundappl = false;
                        start_stop_list.Add(ssrec);
                    }
                    // if not foundappl, then this is an extra removed.  ignore it.
                }
            }
            //Now we have a start stop list
            DateTime enddt;
            foreach (var s in start_stop_list)
            {
                Program.VerboseAudit("AC-9 Diag: start=" + s.start + " fin=" + s.finish);
                if (s.start.AddMinutes(60) <= s.finish)
                    if (!QueuedProcOverlaps(9, s.start, s.finish))
                        if (!ProcExistsInDB(9, s.start, out enddt))
                        {
                            var proc = new proc_data();
                            proc.procedure_number = 9;
                            proc.start = s.start;
                            proc.finish = s.finish;
                            _procs.Add(proc);
                            Program.Audit("Activity 9: " + foundstr + " found at " + s.start.ToString());
                        }
            }

        }

        private DateTime GetAppliedBeforeRemove(string code,DateTime remdt)
        {
            DateTime appldt = DateTime.MinValue;
            Program.VerboseAudit("Search for applied time before " + remdt.ToString());
            var query1 = StartNewQuery(SearchDepth.SearchSince25Hrs);
            query1 = AndItemFilter(query1, "", code, "", "", "applied");
            query1 = query1.Where(e => e.EVENT_DATETIME < remdt);
            query1 = query1.OrderByDescending(e => e.EVENT_DATETIME);
            if (query1.Count() > 0)
                appldt = query1.First().EVENT_DATETIME;
            return appldt;
        }
        private void CheckProc_10()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P10. 1:1 non-RN at bedside");
            Program.VerboseAudit("---------------");
            //DoProc(10, "A_MhAcu1:1UNonRN");
        }

        private void CheckProc_11()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A11. 2:1 by RN at bedside");
            Program.VerboseAudit("---------------");
           
        string[] airways= { "endotracheal tube","Esophageal - tracheal tube","laryngeal mask airway","nasopharyngeal airway",
                "oropharyngeal airway","tracheostomy","other"};
            string[] vents = { "bag - valve - mask","bag - valve - et tube","bag - valve - tracheostomy","ventilator",
                "bilevel positive airway pressure","continuous positive airway pressure","cpap nasal","cpap mask","positive pressure ventilation","other"};
            string[] events = { "respiratory arrest", "cardiac arrest", "unknown", "other" };

            var query = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
            query = query.Where(e => (e.CODE == "9991600100681") // && e.RESULT.ToLower().ContainsAny(airways))
                                      ||
                                     (e.CODE == "9991600100682") // && e.RESULT.ToLower().ContainsAny(vents))
                                      ||
                                     (e.CODE == "9991600100646" && e.RESULT.ToLower().ContainsAny(events))
                                      ||
                                     (e.CODE == "9993040001021" && e.RESULT.ToLower().Contains("cpr"))
                                      ||
                                     (e.CODE == "9993040001022" && e.RESULT.ToLower().Contains("cardiopulmonary arrest"))
                                      ||
                                     (e.CODE.ToUpper() == "MTP" && e.DESCRIPTION.ToUpper() == "MASSIVE BLOOD TRANSFUSION")
                                );
            query = query.Where(e => e.EVENT_DATETIME > _pat.pull_finish.AddHours(-4));
            query = query.Where(e => e.EVENT_DATETIME <= _pat.pull_finish);
            query = query.OrderBy(e => e.EVENT_DATETIME);
            foreach (var item in query)
            {
                if (item.CODE.ToUpper() != "MTP")
                {
                    Program.VerboseAudit("Found " + item.CODE + " / " + item.DESCRIPTION + " / " + item.RESULT + " at " + item.EVENT_DATETIME.ToString());
                    AddSimpleProc(11, item.EVENT_DATETIME, item.EVENT_DATETIME.AddHours(1));
                }
                if (item.CODE.ToUpper() == "MTP")
                {
                    Program.VerboseAudit("Found " + item.CODE + " / " + item.DESCRIPTION + " / " + item.RESULT + " at " + item.EVENT_DATETIME.ToString());
                    AddSimpleProc(11, item.EVENT_DATETIME, item.EVENT_DATETIME.AddHours(2));
                }
            }

            //See if this logic makes sense to you:
            //Look for these rows.  If you see, then this is the “Finish” 
            //time of the Activity. You can use any one of these rows, 
            //but only enter one activity for the timeframe.

            //OBX|1|NM|9990000042640^Total # of Bags Infused||11||||||F|||20190320125100||       Result: (Any number)
            //OBX|2|NM|990000042845^Total Volume Infused (mL)||610|mL|||||F|||20190320125100||   Result: (Any number)
            //OBX|3|NM|9990000042983^Number of bags remaining?||0||||||F|||20190320125100||      Result: (0)

            //Then look for this row within the last 8 hours. This would then be the “start” time of the activity.
            //OBX|1|ST|9993040108761^Order to proceed with Cellular Infusion||Yes||||||F|||20190320110000||     Result: (Yes)

            //If unable to locate above row, then use this one where the Count is “1”
            //OBX|1|NM|9990042776^Bag Count||1||||||F|||20190320113000||

            //So this patient would have a 2:1 by RN at the bedside procedure from 1100-1251.

            //These are from a patient on RO EI94/93 on 3/20/2019. 
            //Rochester will only see these patients on RO EI94/93 or RO MB3G.
            string reslist;
            DateTime end_evdt, start_evdt;
            bool found_end = false, found_beg=false;
            reslist = "";
            if (GetEVDT("", "9990000042640", "", "", reslist, 0, DateTime.MinValue, out end_evdt, SearchDepth.SearchSince9Hrs))
                found_end = true;
            else if (GetEVDT("", "990000042845", "", "", reslist, 0, DateTime.MinValue, out end_evdt, SearchDepth.SearchSince9Hrs))
                found_end = true;
            else if (GetEVDT("", "9990000042983", "", "", "0", 0, DateTime.MinValue, out end_evdt, SearchDepth.SearchSince9Hrs))
                found_end = true;
            if (found_end)
            { //look for start
                Program.VerboseAudit("infusion end time=" + end_evdt);
                Program.VerboseAudit("infusion end time adding 15 mins=" + end_evdt.AddMinutes(15));
                if (GetEVDT("", "9993040108761", "", "", "Yes", 3, end_evdt, out start_evdt, SearchDepth.SearchSince24Hrs))
                    found_beg = true;
                else if (GetEVDT("", "9990042776", "", "", "", 3, end_evdt, out start_evdt, SearchDepth.SearchSince24Hrs))
                    found_beg = true;
                if (found_end && found_beg)
                {
                    Program.VerboseAudit("infusion start time=" + start_evdt);
                    AddSimpleProc(11, start_evdt, end_evdt.AddMinutes(15));
                }
            }
        }

        private void CheckOutcomes()
        {
            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = AndItemFilter(query, "", "A_MHPostFalltime", "", "", "");
            foreach (var ch in query)
            {
                var outc = new proc_data();
                outc.procedure_number = 1;
                outc.start = ch.EVENT_DATETIME;
                _outcomes.Add(outc);
                Program.Audit("Outcomes 1: Found A_MHPostFalltime at " + outc.start);
            }
        }

        private void CheckOtherWorkload()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("Other Activity: ECMO");
            Program.VerboseAudit("---------------");
            //9993040001093
            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = AndItemFilter(query, "", "9993040101000", "", "", "");
            query = query.Where(e => e.UNIT_ID > 0);
            query = query.OrderBy(e => e.EVENT_DATETIME);
            bool added = false;
            foreach (var ch in query)
            {
                if (!added && ch.EVENT_DATETIME >= _pat.pull_finish.AddHours(-4))
                {
                    added = true;
                    Program.Audit("Adding ECMO Other Activity; charted at: " + ch.EVENT_DATETIME.ToString());
                    AddOtherWorkload(ch.UNIT_ID, _pat.pull_finish.AddHours(-4));
                }
            }
        }
        private void AddOtherWorkload(int unit_id, DateTime startdt)
        {
            var cn = PFSDBUtility.NewSqlConnection();
            string sql, columns = "", values = "";
            double thpwi = 5.0;
            int shflen = 8;
            DateTime starttm, finishtm;
            DateTime start, finish;
            int wkld_mins = 0, assess=0, interv = 0, consult = 0;
    
            columns = "OTHER_WORKLOAD_ID";
            columns += ",UNIT_ID";
            columns += ",SHIFT_DEFINITION_ID";
            columns += ",WORKLOAD_TYPE";
            columns += ",START_DATETIME";
            columns += ",FINISH_DATETIME";
            columns += ",METHODOLOGY_ID";
            columns += ",METHODOLOGY_WORKLOAD";
            columns += ",THPWI";
            columns += ",REC_HOURS_PER_HOUR";
            columns += ",CREATED_BY_ID";
            columns += ",CREATED_DATETIME";
            columns += ",LOS_HOURS";
            columns += ",COUNT20";
            columns += ",TOTAL_WORKLOAD_HOURS";
            columns += ",ASSESSMENT_HOURS,INTERVENTION_HOURS,CONSULTATION_HOURS";
            columns += ",REPORT_DATE";
            columns += ",TIMESTAMP";
            columns += ",TOTAL_VOLUME";
            columns += ",UNIT_PARAM_ID";

            long ow_id = PFSDBUtility.NextGID();

            DateTime rpt_date = startdt.Date;
            if (startdt.Hour < _pat.sod.Hour)
                rpt_date = rpt_date.AddDays(-1);

            long shfdefid = GetShiftDefID(_pat.upid, rpt_date, startdt, out shflen, out wkld_mins, out starttm, out finishtm,out thpwi,out assess,out interv,out consult);
            double methwkld = wkld_mins / 60.0 / thpwi;
            double rechrphr = wkld_mins / 60.0 / shflen;
            double wkld_hrs = wkld_mins / 60.0;
            start = rpt_date.Date + starttm.TimeOfDay;
            finish = start.AddHours(shflen);

            values = ow_id.ToString();
            values += "," + unit_id.ToString();
            values += "," + shfdefid.ToString();
            values += ",2";
            values += "," + PFSDBUtility.SQLDateTime(start);
            values += "," + PFSDBUtility.SQLDateTime(finish);
            values += ",20";
            values += "," + methwkld.ToString();
            values += "," + thpwi.ToString();
            values += "," + rechrphr.ToString();
            values += ",-3";
            values += "," + PFSDBUtility.SQLDateTime(DateTime.Now);
            values += "," + shflen.ToString();
            values += ",1"; //count20
            values += "," + wkld_hrs.ToString();
            values += "," + (wkld_hrs * assess / 100.0).ToString();
            values += "," + (wkld_hrs * interv / 100.0).ToString();
            values += "," + (wkld_hrs * consult / 100.0).ToString();
            values += "," + PFSDBUtility.SQLDateTime(rpt_date);
            values += "," + PFSDBUtility.SQLDateTime(DateTime.Now);
            values += ",1";
            values += "," + _pat.upid.ToString();

            sql = "INSERT INTO OTHER_WORKLOAD (" + columns + ") VALUES (" + values + ")";
            // Do not throw an error because this is the place where errors get recorded so it gets recursive.
            // Either try again or exit quietly instead.
            Program.VerboseAudit(sql);
            while (true)
            {
                try
                {
                    var db = PFSDBUtility.NewSqlConnection();
                    var cmd = new SqlCommand(sql, db);
                    cmd.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    string msg = e.Message.ToLower();
                    if (PFSDBUtility.IsFatalDatabaseError(msg))
                    {
                        return;                         // give up
                    }
                }
                break;
            }
        }

        private int GetShiftDefID(int upid, DateTime rptdt,DateTime startdt, out int shflen, out int wkld_mins, out DateTime starttm, out DateTime finishtm, out double thpwi, out int assess, out int interv, out int consult)
        {
            int shfid=0;
            string d="";

            shflen = 0;
            starttm = DateTime.MinValue;
            finishtm = DateTime.MinValue;
            thpwi = 5.0;
            assess = 0;
            interv = 0;
            consult = 0;
            wkld_mins = 0;

            DayOfWeek dow = rptdt.DayOfWeek;
            switch (dow)
            {
                case DayOfWeek.Sunday:
                    d = "Sunday";
                    break;
                case DayOfWeek.Monday:
                    d = "Monday";
                    break;
                case DayOfWeek.Tuesday:
                    d = "Tuesday";
                    break;
                case DayOfWeek.Wednesday:
                    d = "Wednesday";
                    break;
                case DayOfWeek.Thursday:
                    d = "Thursday";
                    break;
                case DayOfWeek.Friday:
                    d = "Friday";
                    break;
                case DayOfWeek.Saturday:
                    d = "Saturday";
                    break;
            }
            d += "_ID";
            string sql = "select shift_definition_id,paid_hours,workload_minutes,start_time,finish_time,thpwi,assessment_pct,intervention_pct,consultation_pct from SHIFT_DEFINITION as sd";
            sql += " inner join DAY_DEFINITION as dd on(dd.DAY_DEFINITION_ID = sd.DAY_DEFINITION_ID)";
            sql += " inner join UNIT_PARAM as up on(dd.UNIT_PARAM_ID = dd.UNIT_PARAM_ID)";
            sql += " inner join OTHER_WORKLOAD_DEFINITION as owd on(up.UNIT_PARAM_ID = owd.UNIT_PARAM_ID)";
            sql += " where sd.is_primary_shift='Y' and up.UNIT_PARAM_ID=" + upid.ToString();
            sql += " and owd.COLUMN_NUMBER=20";
            sql += " and up." + d + "= dd.DAY_DEFINITION_ID";
            sql += " and (" + startdt.Hour + " between datepart(hour,sd.start_time) and datepart(hour,sd.finish_time)";
            sql += " or (datepart(hour,sd.start_time) > datepart(hour,sd.finish_time)";
            sql += " and (" + startdt.Hour + ">=datepart(hour,sd.start_time) or " + startdt.Hour + "<datepart(hour,sd.finish_time))))";

            //(7 between datepart(hour, sd.start_time) and datepart(hour, sd.finish_time) or
            //(datepart(hour, sd.start_time) > datepart(hour, sd.finish_time) and
            // (7 >= datepart(hour, sd.start_time) or 7 < datepart(hour, sd.finish_time))))


            Program.VerboseAudit(sql);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd = new SqlCommand(sql, db2);
            SqlDataReader dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr2.Read())
            {
                shfid = PFSDBUtility.DBToInt(dr2["SHIFT_DEFINITION_ID"]);
                shflen = PFSDBUtility.DBToInt(dr2["PAID_HOURS"]);
                wkld_mins = PFSDBUtility.DBToInt(dr2["WORKLOAD_MINUTES"]);
                starttm = PFSDBUtility.DBToDateTime(dr2["START_TIME"]);
                finishtm = PFSDBUtility.DBToDateTime(dr2["FINISH_TIME"]);
                thpwi = PFSDBUtility.DBToDouble(dr2["THPWI"]);
                assess = PFSDBUtility.DBToInt(dr2["ASSESSMENT_PCT"]);
                interv = PFSDBUtility.DBToInt(dr2["INTERVENTION_PCT"]);
                consult = PFSDBUtility.DBToInt(dr2["CONSULTATION_PCT"]);
            }
            db2.Close();
            return shfid;
        }



        //        private const string DATETIME_FORMAT = "yyyyMMddHHmm";              // ISO Date/Time w/o seconds
        private const string DATETIME_FORMAT = "yyyyMMddHHmmss";              // ISO Date/Time w/o seconds

        private void OutputClass(bool use_default)
        {
            string outstr, ind_list, desc, str_pull_dt, str_in_dt, str_out_dt = "";
            int i, tc_event_id;

            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         212 242 304
            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1
            //1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901 3456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
            //1       |10 5            |                |                |        |10023121779         |ALEXANDER                       |GLORIA                          |                                |0508    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289530  |           |201403030400                                                                      |NNYNNNNYYNNYNNNYNNNYYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
            //1       |10 5            |                |                |        |10024544908         |ARMSTRONG                       |AARON                           |                                |0535    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289531  |           |201403030400                                                                      |NYNNYNNNNNNYNNNYNNNNYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
            //|0547    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289532  |           |201403030400                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN
            //|0547    |A   |201403030700|                |201403030700    |18  |C|    |3040|1440|18289532  |           |201403030700                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//            1RO | RO MB5BG / 6E |                |                |        | 2000180316769 | PARISIEN | GREYSON | LAWRENCE | RMB5514 | P | 20190319030000 |                               | 20 | C |    | 5399 | 480 | 62040560 |           | 20190319030000 | 20190319070000 | NNNYNNNNNNNNYNNNYNYNYNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN

            if (Program.g_is_test)
                tc_event_id = 9999;
            else
                tc_event_id = PFSDBUtility.NextGID();                         //get a unique id for this class
                                                                              //            str_pull_dt = _pat.pull_finish.ToString(DATETIME_FORMAT);
                                                                              //str_pull_dt = _pat.pull_finish.ToString(DATETIME_FORMAT);
                                                                              //if (_pat.unit_departure != null && _pat.unit_departure != DateTime.MinValue)
            if (_pat.effective_out == Program.g_pull_finish && _pat.unit_departure == DateTime.MinValue)
            {
                str_out_dt = "";
                str_in_dt = _pat.effective.ToString(DATETIME_FORMAT);
            }
            else
            {
                str_out_dt = _pat.effective_out.ToString(DATETIME_FORMAT);
                str_in_dt = _pat.effective.ToString(DATETIME_FORMAT);
            }

            outstr = _pat.facilty_code.FixedWidth(8);                       //(facility code)
            outstr += "|" + _pat.unit_name.FixedWidth(16);
            outstr += "|" + "".FixedWidth(16);                               //(unit code)
            outstr += "|" + txarea.FixedWidth(16);                               //(area code)
            outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
            outstr += "|" + _pat.acct.FixedWidth(20);
            outstr += "|" + _pat.last_name.FixedWidth(32);
            outstr += "|" + _pat.first_name.FixedWidth(32);
            outstr += "|" + _pat.middle_name.FixedWidth(32);
            outstr += "|" + _pat.room.FixedWidth(8);
            outstr += "|" + _pat.bed.FixedWidth(4);
                outstr += "|" + str_in_dt;        //IN
            outstr += "|" + "".FixedWidth(14);                               //(login)
            outstr = outstr.FixedWidth(249);
            outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
            outstr += "|" + "C".FixedWidth(1);                               //record type = class
            outstr += "|" + "".FixedWidth(4);                                //(stage)
            outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
            outstr += "|" + _pat.range.ToString().FixedWidth(4);             //TC pull range
            outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
            outstr += "|";
            outstr = outstr.FixedWidth(294);
            outstr += "|" + str_in_dt;        //IN
            outstr = outstr.FixedWidth(346);
            //if (_pat.unit_departure != null && _pat.unit_departure != DateTime.MinValue)
                //                outstr += "|" + _pat.unit_departure.ToString(DATETIME_FORMAT);        //OUT
                outstr += "|" + str_out_dt;        //OUT
            outstr = outstr.FixedWidth(377);
            outstr += "|";

            //if (use_default)
            //{ //make all is_checked = false and then mark defaults
            //    Program.VerboseAudit("Patient Will receive default indicators " + _pat.default_inds_str);
            //    for (i = 1; (i <= MAX_INDS); i++)
            //    {
            //        _inds[i].is_checked = false;
            //    }
            //    foreach (var ind in _pat.default_inds)
            //    {
            //        if (ind <= _inds.GetUpperBound(0))
            //        {
            //            _inds[ind].is_checked = true;
            //        }
            //    }
            //}

            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++) {
                if (_inds[i].is_checked) {
                    outstr += "Y";
                    ind_list += "," + i;
                } else {
                    outstr += "N";
                }
            } // next i
            ind_list = ind_list.Substring(1);                           //strip leading comma
                                                                        //                                                                                                   1                                                                                                   2                                                                                                   3
                                                                        //         1         2         3         4         5         6         7         8         9         0         1         2         3         4         5         6         7         8         9         0         1         2         3         4         5         6         7         8         9         0         1         2         3         4         5         6         7         8         9         0
                                                                        //1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
                                                                        //1       |DO6D            |                |                |        |2000192224892       |BEHNAM                          |KENDRA                          |LEE                             |RDO6311 |P   |20180717110000|                               |20  |C|    |5399|480 |56103278  |           |20180717110000                                     |                              |YNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN


            string str7am;
            Program.outfile.WriteLine(outstr);                          //output to transparent.txt
            if (Program.g_pull_finish.Hour == 7)
            {
                if (str_out_dt.Substring(8,4) == "0700") //create the 7am at 3am
                {
                    str7am = outstr.Substring(0, 203) + Program.g_pull_finish.ToString(DATETIME_FORMAT) + outstr.Substring(217, 78) + Program.g_pull_finish.ToString(DATETIME_FORMAT) + " ".Repeat(69) + outstr.Substring(378, 120);
                    Program.outfile.WriteLine(str7am); 
                }
            }

            Program.Audit("");
            desc = "Classified: " + ind_list;
            if (Program.g_is_test) {
                Program.Audit(desc);
            } else {
                //Save the selected indicators plus both Program.Audits in the event log; link with tc_event_id
                PFSEventLog.AddTransparentMappingEventLogEntry(
                    desc, Program.gLogUnitID, Program.gLogEncounterID,
                    tc_event_id, Program.gLogMapperVersion,
                    Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
            }
        }

        private int DeterminePtypeOfIndicators()
        {
            int i;
            var pscore = 0.0;
            int pt_type = 6;
            var indlist = new List<int>();
            string indstr = "";

            Program.VerboseAudit("Determine patient type of triggered indicators");

            for (i = 1; (i <= MAX_INDS); i++)
            {
                if (_inds[i].is_checked)
                {
                    indlist.Add(i);
                    indstr += i.ToString() + ",";
                    pscore += _inds[i].weight;
                }
            }

            Program.VerboseAudit("indicators=" + indstr + " score=" + pscore.ToString());

            var db = PFSDBUtility.NewPfsDataContext();
            var query_ptype = from ptype in db.PATIENT_TYPEs
                              where (ptype.METHODOLOGY_ID == _pat.meth_id)
                              orderby ptype.PATIENT_TYPE1
                              select new
                              {
                                  ptype.PATIENT_TYPE1,
                                  ptype.POINTS_HIVAL
                              };

            foreach (var ptypes in query_ptype)
            {
                //                Program.DebugTrace("type,hival=" + ptypes.PATIENT_TYPE1.ToString() + "," + ptypes.POINTS_HIVAL.ToString(), "");
                if (pscore <= ptypes.POINTS_HIVAL)
                {
                    if (pt_type > ptypes.PATIENT_TYPE1)
                    {
                        pt_type = ptypes.PATIENT_TYPE1;
                    }
                }
            }
            Program.VerboseAudit("patient type=" + pt_type.ToString());

            return pt_type;

        }


        //private bool ExistDefaultInPast16hrs()
        //{
        //    DateTime cdt1 = DateTime.MinValue;
        //    DateTime cdt2 = DateTime.MinValue;
        //    int cnt_all = 0;
        //    int cnt_def = 0;
        //    //get max class date of last non-default class = 1
        //    //get max class date of last default = 2
        //    // if 1 >= 2 then false
        //    // else
        //    //   if 2 > 1 and date is <= 16 hrs ago then true
        //    //   else false
        //    var db = PFSDBUtility.NewPfsDataContext();
        //    //var query = from ce in db.CLASSIFICATION_EVENTs
        //    //            where (ce.ENCOUNTER_ID == _pat.encounter_id)
        //    //            && (ce.CLASSIFIED_BY_ID != -2)
        //    //            && (System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(ce.CLASSIFICATION_DATETIME, Program.g_pull_start) <= 960)
        //    //            select ce;
        //    //cnt_all = query.Count();
        //    //if (cnt_all > 0)
        //    //{
        //    //    cdt1 = PFSUtility.DBToDateTime(query.Max(x => x.CLASSIFICATION_DATETIME));
        //    //    Program.VerboseAudit("Last regular classification was at:"+cdt1.ToString());
        //    //}
        //    //else {
        //    //    Program.VerboseAudit("No regular classifications within the past 16 hours");
        //    //}

        //    var query = from ce in db.CLASSIFICATION_EVENTs
        //                where (ce.ENCOUNTER_ID == _pat.encounter_id)
        //                && (ce.CLASSIFIED_BY_ID == -2)
        //                && (System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(ce.CLASSIFICATION_DATETIME, Program.g_pull_finish) <= 960)
        //                select ce;
        //    cnt_def = query.Count();

        //    if (cnt_def > 0)
        //    {
        //        cdt2 = PFSDBUtility.DBToDateTime(query.Max(x => x.CLASSIFICATION_DATETIME));
        //        Program.VerboseAudit("Last default classification was at:" + cdt2.ToString());
        //    }
        //    else
        //    {
        //        Program.VerboseAudit("No default classifications within the past 16 hours");
        //    }
        //    return (cnt_def > 0);

        //}


        private void OutputProcs()
        {
            int i;
            string outstr, proc_list, desc;
            int tc_event_id;

            foreach (var proc in _procs)
            {
                if (Program.g_is_test)
                    tc_event_id = 9999;
                else
                    tc_event_id = PFSDBUtility.NextGID();                         //get a unique id for this proc

                outstr = _pat.facilty_code.FixedWidth(8);                       //(facility code)
                outstr += "|" + _pat.unit_name.FixedWidth(16);
                outstr += "|" + "".FixedWidth(16);                               //(unit code)
                outstr += "|" + txarea.FixedWidth(16);                               //(area code)
                outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
                outstr += "|" + _pat.acct.FixedWidth(20);
                outstr += "|" + _pat.last_name.FixedWidth(32);
                outstr += "|" + _pat.first_name.FixedWidth(32);
                outstr += "|" + _pat.middle_name.FixedWidth(32);
                outstr += "|" + _pat.room.FixedWidth(8);
                outstr += "|" + _pat.bed.FixedWidth(4);
                outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //204 proc dt
                outstr += "|" + "".FixedWidth(14);                               //(login)
                outstr = outstr.FixedWidth(249);
                outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
                outstr += "|" + "P".FixedWidth(1);                               //record type = class
                outstr += "|" + "".FixedWidth(4);                                //(stage)
                outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
                outstr += "|" + _pat.range.ToString().FixedWidth(4);             //TC pull range
                outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
                outstr += "|";
                outstr = outstr.FixedWidth(294);
                outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //296 procdt in
                outstr = outstr.FixedWidth(346);
                //if (_pat.unit_departure != null && _pat.unit_departure != DateTime.MinValue)
                //                outstr += "|" + _pat.unit_departure.ToString(DATETIME_FORMAT);        //OUT
                outstr += "|" + proc.finish.ToString(DATETIME_FORMAT);          //348 procdt out
                outstr = outstr.FixedWidth(377);
                outstr += "|";

                proc_list = "";
                for (i = 1; (i < MAX_PROCS); i++)
                {
                    if (proc.procedure_number == i)
                    {
                        outstr += "Y";
                        proc_list += "," + i;
                    }
                    else
                    {
                        outstr += "N";
                    }
                } // next i
                proc_list = proc_list.Substring(1);                             //strip leading comma

                Program.outfile.WriteLine(outstr);                              //output to transparent.txt

                desc = "Activities: " + proc_list;
                if (Program.g_is_test)
                {
                    Program.Audit(desc);
                }
                else
                {
                    //Save the selected procedures plus both Program.Audits in the event log; link with tc_event_id
                    //(this assumes the procedure Program.Audit is mixed in with the class Program.Audit)
                    PFSEventLog.AddTransparentMappingEventLogEntry(desc, Program.gLogUnitID, Program.gLogEncounterID,
                        tc_event_id, Program.gLogMapperVersion,
                        Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
                }
            } // next proc
        }

    }
}


