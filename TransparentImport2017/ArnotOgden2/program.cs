﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;                // for SqlConnection
using System.IO;                            // for Directory, Path, Stream
using System.Windows.Forms;                 // for Application (also add reference)
using PfsShared;                            // add a reference to Shared2 project
//
// Arnot Ogden transparent mapping
//
// This mapping program supports Inpatient 1.0.  (more can be added)
//
namespace ArnotOgden2
{
    public class PatientInfo
    {
        public int      unit_id;
        public int      encounter_id;
        public int      meth_id;
        public string   last_name;
        public string   first_name;
        public string   middle_name;
        public string   acct;
        public double   age;                    // age (in years) at admission
        public string   unit_name;
        public string   room;
        public string   bed;
        public DateTime unit_arrival;
        public DateTime unit_departure;
        public DateTime effective;              // patient specific (may be > g_effdt)
        public DateTime pull_start;             // patient specific (may be > g_pull_start)
        public DateTime pull_finish;            // patient specific (may be < g_pull_finish)
        public double   range;                  // patient specific (may be < g_range)
        public int      TC_source_id;           // TCP port (chart source)
    }

    public static class Program
    {
        const string    TRANSP_FILENAME = "Transparent.txt";        // THE output file (for class import)
        const string    TRANSP_AUDIT_LOG = "TransparentAudit.log";  // The debug/audit log
        const int       CHART_ITEM_LIFE = 30;                   // days in the chart_item table
        const int       DEFAULT_RANGE = 1440;                   // 24 hrs in minutes; override with -range

        public static bool g_abort;                     // stop!
        public static bool g_debug;                     // output to console?
        public static bool g_log;                       // output to log file?
        public static bool g_no_output;                 // audit file only?
        public static bool g_no_delete;                 // keep old chart items?
        public static bool g_stop_on_error;
        public static bool g_import_when_done;

        public static DateTime      g_effdt;            // effective datetime

        public static StreamWriter  outfile;             // Transparent.txt
        public static StreamWriter  logfile;            // TransparentLog.txt

        public static int           gLogUnitID;
        public static int           gLogEncounterID;
        public static string        gLogSourceText;
        public static string        gLogMapperVersion;

        public static StringBuilder gBriefAudit;              // the complete brief audit
        public static StringBuilder gVerboseAudit;            // the complete verbose audit

        public static DateTime      g_pull_start;
        public static DateTime      g_pull_finish;          // pull datetime
        public static int           g_range;

        private static string   _this_acct;              // acct filter
        private static string   _this_unit_name;              // unit filter
        private static int      _this_unit_id;

        static string           _import_path;
        static string           _log_path;

        static void Main(string[] args)
        {
            try
            {
                InitGlobals();

                ParseCommandLine(args);
                SetMapperVersion();

                gLogSourceText = String.Join(" ", args);           // add command line to log
                LogInfo("Begin mapping and translation", PFSEventLog.EventLogCategory.EVENT_CATEGORY_STARTUP_SHUTDOWN);
                gLogSourceText = "";

                OpenOutputFiles();
                ProcessPatients();

                DeleteOldChartItems();

                LogInfo("Mapping complete", PFSEventLog.EventLogCategory.EVENT_CATEGORY_STARTUP_SHUTDOWN);

                MaybeRunImport();
                CloseOutputFiles();
            }
            catch (Exception e)
            {
                Console.WriteLine("Unexpected Error: {0}", e.Message);
                Console.WriteLine("{0}", e.StackTrace);
                LogUnexpectedError(e.Message, e.StackTrace);
            }

            if (g_debug)
            {
                Console.WriteLine(Environment.NewLine);
                Console.Write("Press any key...");
                Console.ReadKey();
            }
        }

        static void InitGlobals()
        {
            gLogSourceText = "";
            gBriefAudit = new StringBuilder();
            gVerboseAudit = new StringBuilder();
        }

        static void ParseCommandLine(string[] args)
        {
            string nowdate, nowtime;
            string value;
            string effdate, efftime;
            string pulldate, pulltime;

            var nowdt = DateTime.Now;
            nowdate = nowdt.ToString("yyyyMMdd");
            nowtime = nowdt.ToString("HHmm");
            effdate = "";
            efftime = "";
            pulldate = "";
            pulltime = "";

            _import_path = PFSUtility.DefaultImportPath();          // ...\load_me
            _log_path = PFSUtility.DefaultLogPath();                // ...\log

            foreach (var arg in args)
            {
                var arr = arg.Split('=');
                value = (arr.GetUpperBound(0) > 0) ? arr[1] : "";

                switch (arr[0])
                {
                    case "-acct":                               // process this patient only
                        _this_acct = value;
                        break;
                        
                    case "-debug":                              // output to console and pause at end
                        g_debug = true;
                        break;

                    case "-effdate":                            // effective date: yyyymmdd
                        if (value == "yesterday")
                            effdate = DateTime.Now.AddDays(-1).ToString("yyyyMMdd");
                        else
                            effdate = value.Left(8);
                        break;
                    
                    case "-efftime":                            // effective time: HHMM or HH
                        efftime = value.Left(4);
                        efftime = efftime.PadRight(4,'0');
                        break;
                    
                    case "-import":                             // run transparent import when done
                        g_import_when_done = true;
                        break;

                    case "-log":                                // create TransparentAudit.log
                        g_log = true;                           // (verbose audit)
                        break;

                    case "-path":                               // override default import path
                        _import_path = value;
                        break;

                    case "-n":                                  // no output to transparent.txt 
                        g_no_output = true;                     // (audit only)
                        break;

                    case "-nodelete":                           // do not delete old chart items
                        g_no_delete = true;
                        break;

                    case "-pulldate":                           // pull date: yyyymmdd
                        pulldate = value.Left(8);
                        break;
                    
                    case "-pulltime":                           // pull time: HHMM or HH
                        pulltime = value.Left(4);
                        pulltime = pulltime.PadRight(4,'0');
                        break;

                    case "-range":                              // range in minutes; default = 1440 = 24hr
                        g_range = value.ToInteger();
                        break;
                        
                    case "-stoponerror":
                        g_stop_on_error = true;
                        break;

                    case "-unit":                               // process this unit only
                        _this_unit_name = value;
                        break;
                    case "-unit_id":                            // process this unit only
                        _this_unit_id = value.ToInteger();
                        break;
                        
                    default:
                        Console.WriteLine("unexpected argument: {0}", arg);
                        break;
                }
            }

            if (String.IsNullOrEmpty(effdate))
                effdate = nowdate;
            if (String.IsNullOrEmpty(efftime))
                efftime = nowtime;

            // Note: pulldate defaults to effdate, not nowdate
            if (String.IsNullOrEmpty(pulldate))
                pulldate = effdate;
            if (String.IsNullOrEmpty(pulltime))
                pulltime = efftime;

            if (g_range == 0)
                g_range = DEFAULT_RANGE;

            DebugTrace("effdate={0}", effdate);
            DebugTrace("efftime={0}", efftime);
            DebugTrace("pulldate={0}", pulldate);
            DebugTrace("pulltime={0}", pulltime);
            DebugTrace("range={0}", g_range);

            // class IN time
            g_effdt = PFSUtility.ISOToDateTime(effdate + efftime);
            // range for chart item queries
            g_pull_finish = PFSUtility.ISOToDateTime(pulldate + pulltime);
            g_pull_start = g_pull_finish.AddMinutes(-g_range);

        }

        static void OpenOutputFiles()
        {
            // Append to existing file
            outfile = new StreamWriter(Path.Combine(_import_path, TRANSP_FILENAME), true);

            if (g_log) {
                // re-write new file
                logfile = new StreamWriter(Path.Combine(_log_path, TRANSP_AUDIT_LOG));
            }
        }

        static void CloseOutputFiles()
        {
            outfile.Close();
            
            if (logfile != null) {
                logfile.Close();
                logfile = null;
            }
        }

        static void SetMapperVersion()
        {
            gLogMapperVersion = PFSUtility.AppVersion();
        }

        static void ProcessPatients()
        {
            string sql, audit_header;
            int count;

            // Make a list of all patients with chart items during the pull period.
            //
            // Include only units that have the "use transparent" flag set.
            // Limit to one patient if -acct is given.  Limit to one unit with -unit.
            // Left join encounter_location - the patient may be discharged at pull time.
            // Look for the latest patient arrival to the unit before the pull time; ignore transfers within the unit.
            // Look for a patient departure (if any) before the pull time; ignore transfers within the unit.
            sql = 
                " SELECT LIST.UNIT_ID, LIST.ENCOUNTER_ID, LIST.TC_SOURCE_ID, PARAM.METHODOLOGY_ID,\n" +
                "     UNIT.NAME AS UNIT, EL.ROOM, EL.BED, EL.DATETIME_IN AS BED_ARRIVAL,\n" +
                "     ARRIVE.DATETIME_IN AS UNIT_ARRIVAL, DEPART.DATETIME_OUT AS UNIT_DEPARTURE,\n" +
                "     E.ACCT_NUMBER, P.LAST_NAME, P.FIRST_NAME, P.MIDDLE_NAME,\n" +
                "     E.AGE_AT_ADMISSION\n" +
                " FROM (\n" +
                "     SELECT DISTINCT CI.UNIT_ID, CI.ENCOUNTER_ID, CI.TC_SOURCE_ID\n" +
                "     FROM CHART_ITEM AS CI\n" +
                "     INNER JOIN UNIT ON (UNIT.UNIT_ID = CI.UNIT_ID)\n" +
                "     WHERE UNIT.USE_TRANSPARENT_CLASSIFICATION='Y'\n" +
                "     AND EVENT_DATETIME BETWEEN " + PFSDBUtility.SQLDateTime(g_pull_start) + " AND " + PFSDBUtility.SQLDateTime(g_pull_finish) +
                " ) AS LIST" +
                " INNER JOIN UNIT ON (UNIT.UNIT_ID = LIST.UNIT_ID)\n" +
                " INNER JOIN UNIT_PARAM AS PARAM ON (PARAM.UNIT_ID = UNIT.UNIT_ID) AND (" + PFSDBUtility.SQLDateTime(g_pull_finish) + " BETWEEN PARAM.EFFECTIVE_REPORT_DATE AND EXPIRATION_REPORT_DATE)\n" +
                " INNER JOIN ENCOUNTER AS E ON (E.ENCOUNTER_ID = LIST.ENCOUNTER_ID)\n" +
                " INNER JOIN PERSON AS P ON (P.PERSON_ID = E.PERSON_ID)\n" +
                " LEFT  JOIN ENCOUNTER_LOCATION AS EL ON (EL.ENCOUNTER_ID = E.ENCOUNTER_ID) AND (" + PFSDBUtility.SQLDateTime(g_pull_finish) + " BETWEEN EL.EFFECTIVE_DATETIME_IN AND EL.EFFECTIVE_DATETIME_OUT)\n" +
                " INNER JOIN (\n" +
                "    SELECT WORKING_UNIT_ID AS UNIT_ID, ENCOUNTER_ID, MAX(DATETIME_IN) AS DATETIME_IN\n" +
                "    FROM ENCOUNTER_LOCATION AS EL\n" +
                "    WHERE WORKING_UNIT_ID IN (SELECT UNIT_ID FROM UNIT WHERE USE_TRANSPARENT_CLASSIFICATION='Y')\n" +
                "    AND DATETIME_IN < " + PFSDBUtility.SQLDateTime(g_pull_finish) +
                "    AND IS_TRANSFER_IN='Y'\n" +
                "    GROUP BY WORKING_UNIT_ID, ENCOUNTER_ID\n" +
                " ) AS ARRIVE ON (ARRIVE.UNIT_ID = UNIT.UNIT_ID) AND (ARRIVE.ENCOUNTER_ID = E.ENCOUNTER_ID)\n" +
                " LEFT JOIN (\n" +
                "    SELECT WORKING_UNIT_ID AS UNIT_ID, ENCOUNTER_ID, MAX(DATETIME_OUT) AS DATETIME_OUT\n" +
                "    FROM ENCOUNTER_LOCATION AS EL\n" +
                "    WHERE WORKING_UNIT_ID IN (SELECT UNIT_ID FROM UNIT WHERE USE_TRANSPARENT_CLASSIFICATION='Y')\n" +
                "    AND DATETIME_OUT < " + PFSDBUtility.SQLDateTime(g_pull_finish) +
                "    AND ((NEXT_WORKING_UNIT_ID<>WORKING_UNIT_ID) OR (NEXT_WORKING_UNIT_ID IS NULL))\n" +
                "    GROUP BY WORKING_UNIT_ID, ENCOUNTER_ID\n" +
                " ) AS DEPART ON (DEPART.UNIT_ID = UNIT.UNIT_ID) AND (DEPART.ENCOUNTER_ID = E.ENCOUNTER_ID)\n" +
                " WHERE (1=1)\n";
            
            if (!String.IsNullOrEmpty(_this_acct)) {
                sql += " AND E.ACCT_NUMBER=" + PFSDBUtility.SQLString(_this_acct) + "\n";
            }
            if (!String.IsNullOrEmpty(_this_unit_name)) {
                sql += " AND UNIT.NAME=" + PFSDBUtility.SQLString(_this_unit_name) + "\n";
            }
            if (_this_unit_id > 0) {
                sql += " AND UNIT.UNIT_ID=" + _this_unit_id + "\n";
            }
            sql += " ORDER BY UNIT.NAME, P.LAST_NAME, P.FIRST_NAME, E.ACCT_NUMBER\n";

            //DebugTrace(sql);
            var db = PFSDBUtility.NewSqlConnection();
            var cmd = new SqlCommand(sql, db);
            SqlDataReader dr = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);

            //
            // Process all patients
            //
            count = 0;

            while (dr.Read())
            {
                count++;
                Console.Write("\rProcessing patient {0}", count);

                // Package the patient info
                // Note: <<dr["FIRST_NAME"] as string>> looks great but it will save nulls.
                //       use <<PFSDBUtility.DBToString(dr["FIRST_NAME"])>> instead.
                var pat = new PatientInfo();
                pat.unit_id = PFSDBUtility.DBToInt(dr["UNIT_ID"]);
                pat.encounter_id = PFSDBUtility.DBToInt(dr["ENCOUNTER_ID"]);
                pat.meth_id = PFSDBUtility.DBToInt(dr["METHODOLOGY_ID"]);
                pat.acct = PFSDBUtility.DBToString(dr["ACCT_NUMBER"]);
                pat.last_name = PFSDBUtility.DBToString(dr["LAST_NAME"]);
                pat.first_name = PFSDBUtility.DBToString(dr["FIRST_NAME"]);
                pat.middle_name = PFSDBUtility.DBToString(dr["MIDDLE_NAME"]);
                pat.unit_name = PFSDBUtility.DBToString(dr["UNIT"]);
                pat.room = PFSDBUtility.DBToString(dr["ROOM"]);
                pat.bed = PFSDBUtility.DBToString(dr["BED"]);
                pat.age = PFSDBUtility.DBToDouble(dr["AGE_AT_ADMISSION"]);                
                pat.unit_arrival = PFSDBUtility.DBToDateTime(dr["UNIT_ARRIVAL"]);
                pat.unit_departure = PFSDBUtility.DBToDateTime(dr["UNIT_DEPARTURE"]);
                pat.effective = PFSUtility.MaxDateTime(g_effdt, pat.unit_arrival);
                pat.pull_start = PFSUtility.MaxDateTime(g_pull_start, pat.unit_arrival);
                pat.pull_finish = PFSUtility.MinDateTime(g_pull_finish, pat.unit_departure);
                if (pat.pull_finish < pat.pull_start) {
                    pat.pull_finish = g_pull_finish;
                }
                pat.range = PFSUtility.DateDiffInMinutes(pat.pull_start, pat.pull_finish);
                pat.TC_source_id = PFSDBUtility.DBToInt(dr["TC_SOURCE_ID"]);        //TCP port
                
                // Get ready for event log entries for this patient
                gLogUnitID = pat.unit_id;
                gLogEncounterID = pat.encounter_id;
                gLogSourceText = "";

                // Reset both audit strings and make headers for both
                gBriefAudit = new StringBuilder();
                gVerboseAudit = new StringBuilder();
                audit_header =
                    new String('=', 80) + Environment.NewLine +
                    "TRANSPARENT MAPPING AUDIT FILE                    Run Time=" + DateTime.Now + Environment.NewLine +
                    "Version:   " + gLogMapperVersion + Environment.NewLine +
                    "Pull:      " + g_pull_finish + Environment.NewLine +
                    "Effective: " + g_effdt + Environment.NewLine +
                    "Range:     " + g_range + " (" + Math.Round(g_range / 60.0, 1) + " hrs) starting " + g_pull_start;

                // This will add to both brief and verbose audits
                Audit(audit_header);
                Audit("");
                Audit(new String('=', 80));
                Audit("Patient " + pat.last_name + ", " + pat.first_name + " " + pat.middle_name +
                    " in " + pat.unit_name + " " + pat.room + " " + pat.bed +
                    " acct=" + pat.acct + " age=" + Math.Round(pat.age, 2));
                Audit("Here from " + pat.pull_start + " to " + pat.pull_finish +
                    "  (LOS=" + Math.Round(pat.range/60.0, 2) + ")");

                // Process this patient
                // Catch any unexpected errors and continue with next patient.
                try
                {
                    // Run the appropriate mapping for this unit and date
                    //
                    switch (pat.meth_id)
                    {
                        case PFSGlobal.METH_ID_INPATIENT:
                            var ip = new ArnotInpatient();
                            ip.ProcessPatient(pat);
                            break;
                        default:
                            LogWarning("Methodology " + pat.meth_id + " in unit " + pat.unit_name + " is not supported");
                            break;
                    }

                    gLogUnitID = 0;
                    gLogEncounterID = 0;
                }
                catch (Exception e)
                {
                    LogUnexpectedError(e.Message, e.StackTrace);
                    // Stop or keep going?
                    if (Program.g_stop_on_error) g_abort = true;
                }

                if (g_abort) break;
            }

            Console.WriteLine();
            dr.Close();
            
            if (count < 1) {
                Audit("");
                if(!String.IsNullOrEmpty(_this_acct)) {
                    LogWarning("The selected patient has no chart items in the given time range");
                } else {
                    LogWarning("No chart items found to process - have the unit(s) been enabled for transparent classification?");
                }
            }   
        }

        static void DeleteOldChartItems()
        {
            string sql;
            DateTime dt;

            if (g_no_output || g_no_delete) return;

            DebugTrace("About to delete old char items...");
            DebugPause();                                       // give a chance to ^C in debug mode

            LogInfo("Delete old chart items...");
            dt = g_effdt.AddDays(-CHART_ITEM_LIFE);
            sql = "DELETE FROM CHART_ITEM WHERE EVENT_DATETIME < " + PFSDBUtility.SQLDateTime(dt);
            PFSDBUtility.ExecuteSQL(sql);
            LogInfo("Done");
        }

        static void MaybeRunImport()
        {
            if (! g_import_when_done) return;
            
            string path = Path.GetDirectoryName(Application.ExecutablePath);
            if (path.Right(3) != "bin") {                       // running in visual studio?
                path = @"C:\qmdev\AcuityPlus\main\bin";
            }
            if (Directory.Exists(path))
            {
                LogInfo("Running transparent import...");
                // Import will add its own log entries; no need to add any here
                int rc = PFSUtility.ExecuteAndWait(Path.Combine(path, PFSGlobal.TRANSPARENT_IMPORT_EXE_NAME), "");
                LogInfo("Done; result=" + rc);
            }
            else
            {
                LogWarning("Can't find " + path);
            }
        }



        //=====================================================================
        // Audits and log files
        //=====================================================================
        // Print to console if debug is set
        public static void DebugTrace(string format, params object[] values)
        {
            if (g_debug)
            {
                Console.Write(DateTime.Now.ToString() + " ");
                Console.WriteLine(format, values);
            }
        }

        public static void DebugPause()
        {
            if (g_debug)
            {
                Console.Write("Press any key...");
                Console.ReadKey();
                Console.WriteLine("");
            }
        }

        // Save in both audit files
        static public void Audit(string s)
        {
            DebugTrace(s);
            if (logfile != null) logfile.WriteLine(s);
            gBriefAudit.AppendLine(s);                  // Add to both audit reports
            gVerboseAudit.AppendLine(s);
        }

        // Save in verbose audit only
        static public void VerboseAudit(string s)
        {
            DebugTrace(s);
            if (logfile != null) logfile.WriteLine(s);
            gVerboseAudit.AppendLine(s);                // Add to verbose audit only
        }

        public static void AddLogEntry(PFSEventLog.EventLogType type, string msg, PFSEventLog.EventLogCategory category)
        {
            DebugTrace(msg);

            PFSEventLog.AddEventLogEntry(
                PFSEventLog.EventLogSource.EVENT_SOURCE_TRANSPARENT_MAPPING,
                type, category, msg, gLogSourceText, 0, gLogUnitID, gLogEncounterID);
        }

        public static void LogInfo(string msg, PFSEventLog.EventLogCategory category)
        {
            AddLogEntry(PFSEventLog.EventLogType.EVENT_TYPE_INFO, msg, category);
        }
        public static void LogInfo(string msg)
        {
            LogInfo(msg, PFSEventLog.EventLogCategory.EVENT_CATEGORY_PROCESSED);
        }

        public static void LogWarning(string msg, PFSEventLog.EventLogCategory category)
        {
            AddLogEntry(PFSEventLog.EventLogType.EVENT_TYPE_WARNING, msg, category);
        }
        public static void LogWarning(string msg)
        {
            LogWarning(msg, PFSEventLog.EventLogCategory.EVENT_CATEGORY_VALIDATION);
        }

        public static void LogError(string msg, PFSEventLog.EventLogCategory category)
        {
            AddLogEntry(PFSEventLog.EventLogType.EVENT_TYPE_ERROR, msg, category);
        }
        public static void LogError(string msg)
        {
            LogError(msg, PFSEventLog.EventLogCategory.EVENT_CATEGORY_VALIDATION);
        }
        public static void LogUnexpectedError(string msg, string stack_trace)
        {
            // Add the message and stack trace to event log; message only goes to screen
            gLogSourceText = stack_trace;
            AddLogEntry(PFSEventLog.EventLogType.EVENT_TYPE_ERROR, msg, PFSEventLog.EventLogCategory.EVENT_CATEGORY_UNEXPECTED);
            gLogSourceText = "";
            
            // Add the stack trace to screen and log files
            Audit(msg);
            Audit(stack_trace);
        }

    }
}
