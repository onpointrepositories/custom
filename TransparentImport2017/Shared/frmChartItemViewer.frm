VERSION 5.00
Object = "{50EB1B32-AA9B-11D0-B667-00A024C3C1C2}#2.0#0"; "resizer.dll"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{0D6234D1-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TODG6.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmChartItemViewer 
   Caption         =   "Chart Item Viewer"
   ClientHeight    =   7740
   ClientLeft      =   60
   ClientTop       =   600
   ClientWidth     =   13965
   Icon            =   "frmChartItemViewer.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   7740
   ScaleWidth      =   13965
   StartUpPosition =   2  'CenterScreen
   Begin OlectraResizer.SimpleContainer SimpleContainer2 
      Height          =   375
      Left            =   360
      TabIndex        =   20
      Top             =   6510
      Width           =   13455
      _ExtentX        =   23733
      _ExtentY        =   661
      Version         =   130816
      BeginProperty HorizontalSpacer {48AADD41-D2A0-11D0-B53A-0020AFD59EF6} 
         Version         =   130816
         Type            =   0
         Size            =   13455
      EndProperty
      BeginProperty VerticalSpacer {48AADD41-D2A0-11D0-B53A-0020AFD59EF6} 
         Version         =   130816
         Type            =   0
         Size            =   375
      EndProperty
      Begin VB.Label lblFootnote 
         Caption         =   "footnote"
         Height          =   255
         Left            =   0
         TabIndex        =   23
         Top             =   60
         Width           =   13215
      End
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   12480
      Top             =   120
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin OlectraResizer.SimpleContainer SimpleContainer1 
      Height          =   435
      Left            =   360
      TabIndex        =   8
      Top             =   6945
      Width           =   9255
      _ExtentX        =   16325
      _ExtentY        =   767
      Version         =   130816
      BeginProperty HorizontalSpacer {48AADD41-D2A0-11D0-B53A-0020AFD59EF6} 
         Version         =   130816
         Type            =   0
         Size            =   9255
      EndProperty
      BeginProperty VerticalSpacer {48AADD41-D2A0-11D0-B53A-0020AFD59EF6} 
         Version         =   130816
         Type            =   0
         Size            =   435
      EndProperty
      Begin VB.CommandButton cmdExport 
         Caption         =   "Export Chart"
         Height          =   375
         Left            =   6000
         TabIndex        =   19
         Top             =   0
         Width           =   1335
      End
      Begin VB.CommandButton cmdBrief 
         Caption         =   "Brief Audit"
         Height          =   375
         Left            =   3000
         TabIndex        =   16
         Top             =   0
         Width           =   1335
      End
      Begin VB.CommandButton cmdVerbose 
         Caption         =   "Verbose Audit"
         Default         =   -1  'True
         Height          =   375
         Left            =   4380
         TabIndex        =   15
         Top             =   0
         Width           =   1335
      End
      Begin VB.CommandButton cmdNext 
         Caption         =   "Next >"
         Height          =   375
         Left            =   1380
         TabIndex        =   11
         Top             =   0
         Width           =   1335
      End
      Begin VB.CommandButton cmdBack 
         Caption         =   "< Back"
         Height          =   375
         Left            =   0
         TabIndex        =   10
         Top             =   0
         Width           =   1335
      End
      Begin VB.CommandButton cmdClose 
         Cancel          =   -1  'True
         Caption         =   "Close"
         Height          =   375
         Left            =   7680
         TabIndex        =   9
         Top             =   0
         Width           =   1335
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Filter"
      Height          =   735
      Left            =   120
      TabIndex        =   5
      Top             =   0
      Width           =   9495
      Begin VB.PictureBox picXP1 
         Appearance      =   0  'Flat
         BackColor       =   &H0000FFFF&
         BorderStyle     =   0  'None
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   120
         ScaleHeight     =   495
         ScaleWidth      =   9255
         TabIndex        =   6
         Top             =   180
         Width           =   9255
         Begin VB.CommandButton cmdAdvanced 
            Caption         =   "Advanced"
            Height          =   375
            Left            =   7800
            TabIndex        =   14
            Top             =   60
            Width           =   1335
         End
         Begin VB.ComboBox cboClassification 
            Height          =   315
            Left            =   1800
            Style           =   2  'Dropdown List
            TabIndex        =   7
            Top             =   60
            Width           =   2295
         End
         Begin VB.Label Label1 
            Caption         =   "Classification"
            Height          =   315
            Left            =   0
            TabIndex        =   17
            Top             =   120
            Width           =   1815
         End
      End
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   375
      Left            =   0
      TabIndex        =   3
      Top             =   7365
      Width           =   13965
      _ExtentX        =   24633
      _ExtentY        =   661
      ShowTips        =   0   'False
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Object.Width           =   24104
         EndProperty
      EndProperty
      Enabled         =   0   'False
   End
   Begin MSAdodcLib.Adodc adoMain 
      Height          =   330
      Left            =   10320
      Top             =   120
      Visible         =   0   'False
      Width           =   1935
      _ExtentX        =   3413
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin OlectraResizer.ControlContainer ControlContainer1 
      Height          =   4950
      Left            =   120
      TabIndex        =   0
      Top             =   1500
      Width           =   13725
      _ExtentX        =   24209
      _ExtentY        =   8731
      Version         =   130816
      BeginProperty HorizontalSpacer {48AADD41-D2A0-11D0-B53A-0020AFD59EF6} 
         Version         =   130816
         BeginProperty MinimumSize {43167EA0-78AF-11D1-B608-006097CF02D2} 
            IsDefault       =   0   'False
            HasValue        =   -1  'True
            Value           =   8600
         EndProperty
      EndProperty
      BeginProperty VerticalSpacer {48AADD41-D2A0-11D0-B53A-0020AFD59EF6} 
         Version         =   130816
      EndProperty
      Begin TrueOleDBGrid60.TDBGrid grdChart 
         Height          =   4950
         Left            =   0
         OleObjectBlob   =   "frmChartItemViewer.frx":57E2
         TabIndex        =   2
         Top             =   0
         Width           =   13725
      End
   End
   Begin VB.Label Label2 
      Caption         =   "Notes:"
      Height          =   315
      Left            =   120
      TabIndex        =   24
      Top             =   840
      Width           =   975
   End
   Begin OlectraResizer.Spacer Spacer6 
      Height          =   60
      Left            =   4987
      TabIndex        =   22
      Top             =   6885
      Width           =   2100
      Version         =   130816
      Size            =   60
      MainAxis        =   2
      Orientation     =   0
      BeginProperty TopConnection {B2E77E02-0AA7-11D1-B572-006097CF02D2} 
         Version         =   130817
         Control         =   "SimpleContainer2"
         Edge            =   3
      EndProperty
      BeginProperty BottomConnection {B2E77E02-0AA7-11D1-B572-006097CF02D2} 
         Version         =   130817
         Control         =   "SimpleContainer1"
         Edge            =   1
      EndProperty
   End
   Begin OlectraResizer.Spacer Spacer5 
      Height          =   164
      Left            =   0
      TabIndex        =   21
      Top             =   6615
      Width           =   360
      Version         =   130816
      Size            =   360
      MainAxis        =   1
      BeginProperty LeftConnection {B2E77E02-0AA7-11D1-B572-006097CF02D2} 
         Version         =   130817
         Control         =   "frmChartItemViewer"
         IsTerminator    =   -1  'True
         IsContainer     =   -1  'True
      EndProperty
      BeginProperty RightConnection {B2E77E02-0AA7-11D1-B572-006097CF02D2} 
         Version         =   130817
         Control         =   "SimpleContainer2"
      EndProperty
   End
   Begin VB.Label lblNotes 
      Caption         =   "Notes"
      Height          =   615
      Left            =   1200
      TabIndex        =   18
      Top             =   840
      Width           =   12615
   End
   Begin OlectraResizer.Spacer Spacer4 
      Height          =   360
      Left            =   4906
      TabIndex        =   13
      Top             =   7380
      Width           =   164
      Version         =   130816
      Size            =   360
      MainAxis        =   1
      Orientation     =   0
      BeginProperty TopConnection {B2E77E02-0AA7-11D1-B572-006097CF02D2} 
         Version         =   130817
         Control         =   "SimpleContainer1"
         Edge            =   2
      EndProperty
      BeginProperty BottomConnection {B2E77E02-0AA7-11D1-B572-006097CF02D2} 
         Version         =   130817
         Control         =   "frmChartItemViewer"
         Edge            =   2
         IsTerminator    =   -1  'True
         IsContainer     =   -1  'True
      EndProperty
   End
   Begin OlectraResizer.Spacer Spacer3 
      Height          =   164
      Left            =   0
      TabIndex        =   12
      Top             =   7080
      Width           =   360
      Version         =   130816
      Size            =   360
      MainAxis        =   1
      BeginProperty LeftConnection {B2E77E02-0AA7-11D1-B572-006097CF02D2} 
         Version         =   130817
         Control         =   "frmChartItemViewer"
         IsTerminator    =   -1  'True
         IsContainer     =   -1  'True
      EndProperty
      BeginProperty RightConnection {B2E77E02-0AA7-11D1-B572-006097CF02D2} 
         Version         =   130817
         Control         =   "SimpleContainer1"
      EndProperty
   End
   Begin OlectraResizer.Spacer Spacer1 
      Height          =   60
      Left            =   6953
      TabIndex        =   4
      Top             =   6450
      Width           =   163
      Version         =   130816
      Size            =   60
      MainAxis        =   1
      Orientation     =   0
      BeginProperty TopConnection {B2E77E02-0AA7-11D1-B572-006097CF02D2} 
         Version         =   130817
         Control         =   "ControlContainer1"
         Edge            =   2
      EndProperty
      BeginProperty BottomConnection {B2E77E02-0AA7-11D1-B572-006097CF02D2} 
         Version         =   130817
         Control         =   "SimpleContainer2"
      EndProperty
   End
   Begin OlectraResizer.Spacer Spacer2 
      Height          =   164
      Left            =   13845
      TabIndex        =   1
      Top             =   3893
      Width           =   120
      Version         =   130816
      Size            =   120
      MainAxis        =   1
      BeginProperty LeftConnection {B2E77E02-0AA7-11D1-B572-006097CF02D2} 
         Version         =   130817
         Control         =   "ControlContainer1"
         Edge            =   2
         IsTerminator    =   -1  'True
      EndProperty
      BeginProperty RightConnection {B2E77E02-0AA7-11D1-B572-006097CF02D2} 
         Version         =   130817
         Control         =   "frmChartItemViewer"
         Edge            =   2
         IsTerminator    =   -1  'True
         IsContainer     =   -1  'True
      EndProperty
   End
End
Attribute VB_Name = "frmChartItemViewer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'
' Chart Item Viewer
'
' THIS WINDOW IS SHARED BY SYSTEM MANAGER AND PATIENT SELECTION
' Display and filtering are controlled by m_filter
'
Private Const MIN_WINDOW_WIDTH = 10300
Private Const MIN_WINDOW_HEIGHT = 5800

Private Enum ViewMode
    ViewModeFree
    ViewModePatient
End Enum

Private m_view_mode             As ViewMode
Private m_filter                As New PFSChartItemFilter
Private m_run_activation        As Boolean

'patient review mode - used to set the filter
Private m_unit_id               As Long
Private m_encounter_ids()       As Long
Private m_pat_index             As Integer                  'current patient
Private m_enc                   As New encounter            'current encounter
Private m_class_id              As Long                     'current classification id (or zero)
Private m_brief_audit_available As Boolean
Private m_verbose_audit_available As Boolean
Private m_notes_base            As String

Private cdlClass                As New PFSCodeDescList

Private m_tcutil                As New PFSTransparentUtility
Private m_initial_row_height    As Integer


Public Sub SetupPatientReview(unit_id As Long, encounter_ids() As Long)
    On Error GoTo errHandler
    m_view_mode = ViewModePatient
    
    m_unit_id = unit_id
    m_encounter_ids = encounter_ids
    cboClassification.Enabled = True
    cmdAdvanced.Enabled = True              'OK to enable.  The filter will restrict choices

    ResetFilter m_filter
    With m_filter
        .UnitID = unit_id
        .DisplayCount = False
        .DisplayUnit = False
        .DisplayAccount = False
    End With
    
    Me.WindowState = vbNormal
    m_run_activation = True
    Me.show vbModal
    'continued in Form_Activate
    Exit Sub
errHandler:
    g_util.MsgBoxError
End Sub

Public Sub SetupFreeMode()
    On Error GoTo errHandler
    m_view_mode = ViewModeFree
    
    m_unit_id = 0
    ReDim m_encounter_ids(0 To 0)
    cboClassification.Enabled = False
    cmdAdvanced.Enabled = True
    
    ResetFilter m_filter
    With m_filter
        .DisplayCount = True
        .PullDatetime = Now
        .RangeMins = 30
    End With
    
    Me.WindowState = vbNormal
    m_run_activation = True
    Me.show vbModal
    'continued in Form_Activate
    Exit Sub
errHandler:
    g_util.MsgBoxError
End Sub


Private Sub cboClassification_Click()
    On Error GoTo errHandler
    SelectClassification g_dbutil.DBToLong(cdlClass.Code(cboClassification.Text))   'could be zero for (all)
    SetButtonState
    Exit Sub

errHandler:
    g_util.MsgBoxError
End Sub

Private Sub Form_Load()
    On Error GoTo errHandler
    Dim rs As New Recordset
    Dim sql As String

    g_display.SetStdTheme Me, True
    'Set the status bar font to the Status (tooltip) font instead of the message font
    Set StatusBar1.font = g_display.StatusFont
    StatusBar1.height = StatusBar1.height * (g_display.StatusFont.Size / 8.25)
    
    lblNotes.caption = ""
    lblFootnote.caption = ""
    
    InitGrid
    m_initial_row_height = grdChart.RowHeight
    Exit Sub
errHandler:
    g_util.MsgBoxError "loading form"
End Sub

Private Sub Form_Activate()
    On Error GoTo errHandler
    
    If Not m_run_activation Then Exit Sub

    Select Case m_view_mode
    Case ViewModeFree
        SetButtonState
        cmdAdvanced_Click
    Case ViewModePatient
        SetPatient 1
    End Select
    
    m_run_activation = False
    Exit Sub

errHandler:
    g_util.MsgBoxError "form activate"
End Sub

Private Sub Form_Resize()
    If (Me.WindowState = vbNormal) Then
        Me.width = g_util.Max(Me.width, MIN_WINDOW_WIDTH)
        Me.height = g_util.Max(Me.height, MIN_WINDOW_HEIGHT)
    End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If (KeyCode = vbKeyF1) Then
        ShowHelp
        KeyCode = 0
    End If
End Sub

Private Sub grdChart_FetchCellStyle(ByVal Condition As Integer, ByVal Split As Integer, Bookmark As Variant, ByVal col As Integer, ByVal CellStyle As TrueOleDBGrid60.StyleDisp)
    Dim ts As Date
    'The grid is about to paint a cell
    
    'Debug.Print "FetchCellStyle " & grdChart.Columns(col).caption
    
    Select Case LCase$(grdChart.Columns(col).caption)
    Case "timestamp"
        If IsDate(m_filter.MappingDatetime) Then
            ts = grdChart.Columns(col).CellValue(Bookmark)
            Debug.Print "timestamp=" & ts, "mapping=" & m_filter.MappingDatetime
            If (ts > m_filter.MappingDatetime) Then
                CellStyle.BackColor = vbYellow         'added after mapping performed
            End If
        End If
    End Select
End Sub

Private Sub grdChart_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    'Copy the selected cell text to the status bar  ** do a pop-up instead?
    On Error Resume Next
    SetStatus grdChart.Columns(grdChart.col).CellValue(grdChart.Bookmark)
End Sub

Private Sub SetStatus(msg As String)
    StatusBar1.Panels(1).Text = msg
End Sub

Private Sub cmdAdvanced_Click()
    'The user can keep this dialog up and apply changes to this window
    'Lock the patient if we are in patient view mode
    frmChartItemFilter.Setup Me, m_filter, (m_view_mode = ViewModePatient)
    frmChartItemFilter.show vbModal, Me
End Sub

Private Sub cmdBack_Click()
    SetPatient m_pat_index - 1
End Sub

Private Sub cmdNext_Click()
    SetPatient m_pat_index + 1
End Sub

Private Sub cmdBrief_Click()
    m_tcutil.ShowAudit m_class_id, False
End Sub

Private Sub cmdVerbose_Click()
    m_tcutil.ShowAudit m_class_id, True
End Sub

Private Sub cmdExport_Click()
    ExportChart
End Sub

Private Sub cmdClose_Click()
    Unload Me
End Sub

Private Sub ShowHelp()
    g_help.ShowHelp hWnd, HELP_CHART_VIEWER
End Sub

Private Sub SetPatient(pat_index As Integer)
    On Error GoTo errHandler
    Dim sql As String
    
    If (pat_index < 1) Or (pat_index > UBound(m_encounter_ids)) Then
        Beep
        Exit Sub
    End If

    m_pat_index = pat_index
    m_enc.Load m_encounter_ids(m_pat_index)
    m_filter.EncounterID = m_enc.EncounterID
    
    SetCaption

    'Get classification dates for this patient
    'Set the pulldown and select the first - this will trigger a load grid
    sql = "select classification_event_id, classification_datetime from classification_event" & _
        " where encounter_id=" & m_enc.EncounterID & _
        " and IS_DELETED<>'Y'" & _
        " order by classification_datetime desc"
    cdlClass.LoadSQL sql, cdlInsertAllChoice
    
    cboClassification.Enabled = True                    'filter changes may have disabled this
    cdlClass.FillComboList cboClassification
    If (cboClassification.ListCount >= 2) Then
        cboClassification.ListIndex = 1                 'select latest classification
    Else
        cboClassification.ListIndex = 0                 'select (all)
    End If
    
    SetButtonState
    Exit Sub
    
errHandler:
    g_util.ThrowError "SetPatient"
End Sub

Private Sub SelectClassification(class_id As Long)
    On Error GoTo errHandler
    Dim tc_event_id As Long
    Dim sql As String, class_info As String
    Dim rs As New Recordset
    Dim class_timestamp As Date
    Dim patient_type As Integer
    
    'An ID of zero means "all"
    m_class_id = class_id

    'Default to entire chart
    With m_filter
        .PullDatetime = Null
        .RangeMins = 0
        .MappingDatetime = Null
    End With
    class_info = ""
    m_brief_audit_available = False
    m_verbose_audit_available = False
    
    ' Is a classification selected?  (could be "(all)")
    If (m_class_id <> 0) Then
        'Show one classification
        sql = "select tc_event_id, tc_pull_datetime, tc_pull_range, classification_datetime, patient_type, timestamp" & _
            " from classification_event where classification_event_id=" & m_class_id
        rs.Open sql, g_cnADO, adOpenStatic
        If Not rs.eof Then
            tc_event_id = g_dbutil.DBToLong(rs("tc_event_id"))
            class_timestamp = rs("timestamp")
            patient_type = rs("patient_type")
            With m_filter
                If IsDate(rs("tc_pull_datetime")) Then
                    .PullDatetime = rs("tc_pull_datetime")
                Else
                    .PullDatetime = rs("classification_datetime")
                End If
                .RangeMins = g_dbutil.DBToInteger(rs("tc_pull_range"))
            End With
        End If
        rs.Close
        
        'Find out when mapping was done (could be hours before the class timestamp and its import)
        'Default to classification time
        m_filter.MappingDatetime = class_timestamp
                    
        If (tc_event_id <> 0) Then
            sql = "select timestamp, description" & _
                " from event_log" & _
                " where tc_event_id=" & tc_event_id & _
                " and encounter_id=" & m_enc.EncounterID
            rs.Open sql, g_cnADO, adOpenStatic
            If Not rs.eof Then
                m_filter.MappingDatetime = rs("timestamp")
                class_info = rs("description") & ""                     'list of indicators mapped
            End If
            rs.Close
        End If
            
        'Enable the audit button(s)?
        m_tcutil.IsTransparentAuditSnapshotAvailable m_class_id, m_brief_audit_available, m_verbose_audit_available
        
    Else
        class_info = "Entire chart"
    End If
    
    m_notes_base = "Newest First"
    If Len(class_info) Then m_notes_base = m_notes_base & "; " & class_info
    If (patient_type <> 0) Then m_notes_base = m_notes_base & "; Patient Type " & patient_type

    ReloadGrid
    
    SetButtonState
    Exit Sub

errHandler:
    g_util.MsgBoxError
    Exit Sub
    Resume  'debug
End Sub


Private Function DescribeFilter(filter As PFSChartItemFilter) As String
    Dim result As String

    result = ""
    
    If IsDate(filter.PullDatetime) Then
        result = result & "; Pull=" & filter.PullDatetime
    End If
    If (filter.RangeMins <> 0) Then
        result = result & "; Range=" & filter.RangeMins & " (" & Round(filter.RangeMins / 60, 2) & " hrs)"
    End If
    If IsDate(filter.MappingDatetime) Then
        result = result & "; Mapping run at " & filter.MappingDatetime
    End If
    
    If Len(filter.Category) Then
        result = result & "; Category contains " & filter.Category
    End If
    If Len(filter.Code) Then
        result = result & "; Code contains " & filter.Code
    End If
    If Len(filter.Description) Then
        result = result & "; Description contains " & filter.Description
    End If
    If Len(filter.FieldName) Then
        result = result & "; FieldName contains " & filter.FieldName
    End If
    If Len(filter.result) Then
        result = result & "; Result contains " & filter.result
    End If
    If Len(filter.OrderControl) Then
        result = result & "; OrderControl contains " & filter.OrderControl
    End If
    If Len(filter.OrderID) Then
        result = result & "; OrderID contains " & filter.OrderID
    End If
    
    If Len(result) Then result = Mid$(result, 3)            'remove leading "; "
    DescribeFilter = result
End Function

Private Sub InitGrid()
    Const read_only = True
    
    'grdChart.DataMode = dbgBound             'must set in designer
    Set grdChart.DataSource = adoMain
    
    grdChart.AllowUpdate = Not read_only
    grdChart.AllowAddNew = Not read_only
    grdChart.AllowDelete = Not read_only
End Sub

Public Sub ReloadGrid()
    On Error GoTo errHandler
    Dim adjust As Single
    Dim i As Integer, cols As Integer, X As Integer
    Dim increase_row_height As Boolean
    Dim C As TrueOleDBGrid60.Column
    Dim total_width As Long
    
    g_util.CursorPushHourglass
    SetStatus "searching..."
    
    'Let the grid display a SELECT command
    adoMain.ConnectionString = g_dbutil.DefaultConnectionString()
    adoMain.CursorLocation = adUseClient        'highly recommended by TDBGrid
    adoMain.CommandTimeout = 20 * 60            'The default timeout is much too short [7367]
    adoMain.RecordSource = BuildSQL(m_filter)
    adoMain.mode = adModeRead
    adoMain.Refresh
    
    'The grid has just been re-configured by adoMain
    'Adjust some column widths; take into account the current font vs. the form design font
    adjust = grdChart.font.Size / 8#
    
    cols = grdChart.Columns.count
    For i = 0 To cols - 1
        With grdChart.Columns(i)
            Debug.Print "column "; i; " caption="; .caption
            Select Case LCase$(.caption)
            Case "timestamp", "event date/time", ""
                .width = 1700 * adjust
            Case "category"
                .width = 2000 * adjust
            Case "code"
                .width = 1000 * adjust
            Case "description"
                .width = 2300 * adjust
            Case "field", "field_name"
                .width = 1500 * adjust
            Case "result"
                .width = 4000 * adjust
                increase_row_height = True
            End Select
            
            total_width = total_width + .width
            If (i = cols - 1) Then
                'This is the last column; fill to the edge of the grid
                X = (grdChart.width - total_width - 900)
                If (X > 0) Then
                    .width = .width + X
                End If
            End If
        End With
    Next i
    
    'Look for columns whos cell style we want to modify on the fly
    For i = 0 To cols - 1
        With grdChart.Columns(i)
            Select Case LCase$(.caption)
            Case "timestamp"
                .FetchStyle = True
            End Select
        End With
    Next i
    
    If increase_row_height Then
        For Each C In grdChart.Columns
            C.WrapText = True
        Next
        grdChart.RowHeight = 2 * m_initial_row_height
    Else
        grdChart.RowHeight = m_initial_row_height
    End If
    
    If (m_view_mode = ViewModePatient) Then
        lblFootnote.caption = "* Items with a yellow timestamp were added after mapping was performed"
    Else
        lblFootnote.caption = ""
    End If
    SetStatus "done"
    SetButtonState
    
    If (m_view_mode = ViewModePatient) Then
        'Was anything done in the filter dialog to void the patient information?
        If (m_filter.UnitID <> m_unit_id) Or _
            (m_filter.EncounterID <> m_enc.EncounterID) _
        Then
            cboClassification.Enabled = False
            m_class_id = 0
            SetCaption                  'remove the patient name
            SetButtonState              'disable audit buttons
        End If
    End If
    
    If (m_view_mode = ViewModeFree) Then
        m_notes_base = grdChart.ApproxCount & " rows"
    End If
    
    lblNotes.caption = m_notes_base & "; " & DescribeFilter(m_filter)
    g_util.CursorPop
    Exit Sub
    
errHandler:
    'The adoMain control usually displays an error dialog (but not always)
    g_util.MsgBoxError "Reload Grid"
    g_util.CursorPopAll
    grdChart.ReBind                   'clear the grid
    Me.SetFocus                       'we are loosing focus!
    Exit Sub
    Resume  'debug
End Sub

Private Function BuildSQL(filter As PFSChartItemFilter) As String
    On Error GoTo errHandler
    Dim sql As String, fields As String, group_by As String, order_by As String
    Dim start_dt As Date, finish_dt As Date

    ' Choose what to show
    If filter.DisplayCount Then
        fields = fields & ",count(*) as 'Count(*)'"
        'group_by
        order_by = order_by & ",count(*) desc"
    End If
    If filter.DisplayUnit Then
        fields = fields & ",unit.name as Unit"
        group_by = group_by & ",unit.name"
        order_by = order_by & ",unit.name"
    End If
    If filter.DisplayAccount Then
        fields = fields & ",e.acct_number as Account"
        group_by = group_by & ",e.acct_number"
        order_by = order_by & ",e.acct_number"
    End If
    If filter.DisplayEventTime Then
        fields = fields & ",convert(char(19),ci.event_datetime,121) as [Event Date/Time]"
        group_by = group_by & ",convert(char(19),ci.event_datetime,121)"
        order_by = order_by & ",convert(char(19),ci.event_datetime,121)"
        If 1 Then order_by = order_by & " desc"             'newest first
    End If
    If filter.DisplayTimestamp Then
        fields = fields & ",convert(char(19),ci.timestamp,121) as [Timestamp]"
        group_by = group_by & ",convert(char(19),ci.timestamp,121)"
        order_by = order_by & ",convert(char(19),ci.timestamp,121)"
        If 1 Then order_by = order_by & " desc"             'newest first
    End If
    If filter.DisplayCategory Then
        fields = fields & ",ci.category as Category"
        group_by = group_by & ",category"
        order_by = order_by & ",category"
    End If
    If filter.DisplayCode Then
        fields = fields & ",ci.code as Code"
        group_by = group_by & ",ci.code"
        order_by = order_by & ",ci.code"
    End If
    If filter.DisplayDescription Then
        fields = fields & ",ci.description as Description"
        group_by = group_by & ",ci.description"
        order_by = order_by & ",ci.description"
    End If
    If filter.DisplayFieldName Then
        fields = fields & ",ci.field_name as Field"
        group_by = group_by & ",ci.field_name"
        order_by = order_by & ",ci.field_name"
    End If
    If filter.DisplayResult Then
        fields = fields & ",ci.result as Result"
        group_by = group_by & ",ci.result"
        order_by = order_by & ",ci.result"
    End If
    If filter.DisplayOrderControl Then
        fields = fields & ",ci.order_control + '/' + ci.order_id as [Order Control/ID]"
        group_by = group_by & ",ci.order_control + '/' + ci.order_id"
        order_by = order_by & ",ci.order_control + '/' + ci.order_id"
    End If

    
    sql = "select " & Mid$(fields, 2)              'strip leading comma
    sql = sql & " from chart_item as ci" & _
        " inner join unit on (unit.unit_id = ci.unit_id)" & _
        " inner join encounter as e on (e.encounter_id = ci.encounter_id)"
    
    sql = sql & " where 1=1"
    
    ' Choose what to filter
    ' (give the user a break and don't filter on blanks - use Len(Trim$)
    '
    If (filter.UnitID <> 0) Then
        sql = sql & " and unit.unit_id=" & filter.UnitID
    End If
    If (filter.EncounterID <> 0) Then
        sql = sql & " and ci.encounter_id=" & filter.EncounterID
    End If
    
    If IsDate(filter.PullDatetime) Then
        finish_dt = filter.PullDatetime
        sql = sql & " and ci.event_datetime <= " & g_dbutil.SQL_DateTime(finish_dt)
        If (filter.RangeMins > 0) Then
            start_dt = DateAdd("n", -filter.RangeMins, finish_dt)
            sql = sql & " and ci.event_datetime >= " & g_dbutil.SQL_DateTime(start_dt)
        End If
    End If
    
    If Len(filter.Category) Then
        sql = sql & " and ci.category like " & g_dbutil.SQL_String("%" & filter.Category & "%")
    End If
    If Len(filter.Code) Then
        sql = sql & " and ci.code like " & g_dbutil.SQL_String("%" & filter.Code & "%")
    End If
    If Len(filter.Description) Then
        sql = sql & " and ci.description like " & g_dbutil.SQL_String("%" & filter.Description & "%")
    End If
    If Len(filter.FieldName) Then
        sql = sql & " and ci.field_name like " & g_dbutil.SQL_String("%" & filter.FieldName & "%")
    End If
    If Len(filter.result) Then
        sql = sql & " and ci.result like " & g_dbutil.SQL_String("%" & filter.result & "%")
    End If
    If Len(filter.OrderControl) Then
        sql = sql & " and ci.order_control like " & g_dbutil.SQL_String("%" & filter.OrderControl & "%")
    End If
    If Len(filter.OrderID) Then
        sql = sql & " and ci.order_id like " & g_dbutil.SQL_String("%" & filter.OrderID & "%")
    End If

    sql = sql & " group by " & Mid$(group_by, 2)
    sql = sql & " order by " & Mid$(order_by, 2)        'strip leading comma

    Debug.Print sql
    BuildSQL = sql
    Exit Function
    
errHandler:
    g_util.MsgBoxError
    Exit Function
    Resume  'debug
End Function

Public Sub ResetFilter(filter As PFSChartItemFilter)
    With filter
        .DisplayAccount = True
        .DisplayCategory = True
        .DisplayCode = True
        .DisplayCount = True
        .DisplayDescription = True
        .DisplayEventTime = True
        .DisplayFieldName = True
        .DisplayOrderControl = True
        .DisplayOrderID = True
        .DisplayResult = True
        .DisplayTimestamp = True
        .DisplayUnit = True
        
        .UnitID = 0
        .EncounterID = 0
        .PullDatetime = Null
        .RangeMins = 30                 '0=infinite - avoid trouble if you reset and then OK
        .MappingDatetime = Null
        .Category = ""
        .Code = ""
        .Description = ""
        .FieldName = ""
        .result = ""
        .OrderControl = ""
        .OrderID = ""
    End With
End Sub


Private Sub SetCaption()
    Me.caption = "Chart Item Viewer"
    
    If (m_filter.EncounterID <> 0) Then
        'maybe reload - the encounter could have been changed in the filter dialog
        m_enc.Load m_filter.EncounterID
        Me.caption = Me.caption & " - " & m_enc.Patient.FullName
    End If
End Sub

Private Sub SetButtonState()
    cmdAdvanced.Enabled = True          'enable for search within a patient classification

    cmdBack.Enabled = (m_pat_index > 1)
    cmdNext.Enabled = (m_pat_index < UBound(m_encounter_ids))
    
    cmdBrief.Enabled = (m_class_id <> 0) And m_brief_audit_available
    cmdVerbose.Enabled = (m_class_id <> 0) And m_verbose_audit_available
    
    cmdExport.Enabled = (grdChart.ApproxCount > 0)
End Sub


Private Sub ExportChart()
    On Error GoTo errCancel
    Dim rc As VbMsgBoxResult
    CommonDialog1.CancelError = True
    CommonDialog1.flags = cdlOFNHideReadOnly
    CommonDialog1.filter = "All Files (*.*)|*.*|CSV Files(*.csv)|*.csv|"
    CommonDialog1.FilterIndex = 2
    CommonDialog1.ShowSave
    
    If g_util.FileExists(CommonDialog1.filename) Then
        rc = MsgBox(CommonDialog1.FileTitle & " already exists.  Do you want to overwrite it?", vbQuestion + vbYesNo)
        If (rc <> vbYes) Then Exit Sub
    End If
    
    On Error GoTo errHandler
    grdChart.ExportToDelimitedFile CommonDialog1.filename, dbgAllRows, ",", """", """"
    Exit Sub

errCancel:
    ' User pressed the Cancel button
    Exit Sub

errHandler:
    g_util.MsgBoxError
End Sub
