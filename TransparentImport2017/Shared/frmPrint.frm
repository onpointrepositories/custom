VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Object = "{4932CEF1-2CAA-11D2-A165-0060081C43D9}#2.0#0"; "Actbar2.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.2#0"; "MSCOMCTL.OCX"
Begin VB.Form frmPrint 
   Caption         =   "Print Preview"
   ClientHeight    =   7005
   ClientLeft      =   165
   ClientTop       =   270
   ClientWidth     =   11505
   Icon            =   "frmPrint.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   7005
   ScaleWidth      =   11505
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ImageList ImageList32 
      Left            =   6240
      Top             =   600
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   32
      ImageHeight     =   32
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   6
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrint.frx":0E42
            Key             =   "Save.ico"
         EndProperty
         BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrint.frx":20C4
            Key             =   "Print.ico"
         EndProperty
         BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrint.frx":3346
            Key             =   "Copy.ico"
         EndProperty
         BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrint.frx":4198
            Key             =   "ZoomOut.ico"
         EndProperty
         BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrint.frx":4FEA
            Key             =   "ZoomIn.ico"
         EndProperty
         BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmPrint.frx":5E3C
            Key             =   "Font.ico"
         EndProperty
      EndProperty
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   840
      Top             =   600
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin RichTextLib.RichTextBox txtReport 
      Height          =   1725
      Left            =   120
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   1260
      Width           =   7455
      _ExtentX        =   13150
      _ExtentY        =   3043
      _Version        =   393217
      Enabled         =   -1  'True
      ScrollBars      =   3
      TextRTF         =   $"frmPrint.frx":628E
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin ActiveBar2LibraryCtl.ActiveBar2 abrPrint 
      Height          =   525
      Left            =   0
      TabIndex        =   1
      Top             =   0
      Width           =   7575
      _LayoutVersion  =   1
      _ExtentX        =   13361
      _ExtentY        =   926
      _DataPath       =   ""
      Bands           =   "frmPrint.frx":6320
   End
   Begin VB.Label lblFont 
      BackColor       =   &H0000FFFF&
      Caption         =   "Printer Font"
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   1680
      TabIndex        =   0
      Top             =   720
      Width           =   1575
   End
End
Attribute VB_Name = "frmPrint"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'
' frmPrint -- plaintext print preview window
' Use this if you want to view all text at once in a text box with a scroll bar.
' Most are now using rptPrintText, which paginates the text.
'
' This form shows simple text in a text box.
' No headings or footers are added to the printout.
' The user can save as text.
'
' * SetText()
' * SetOrientation      (optional)
' * SetLeftMargin       (optional)
' * Show this form
'
Private m_is_loading    As Boolean
Private m_zoom_percent  As Single
Private m_orientation   As Integer
Private m_left_margin   As Single

Private m_collection1   As New Collection
Private m_collection2   As New Collection

'
' Call this to set the text in the window
'
Public Sub SetText(txt As String)
    txtReport.Text = txt                        'save the text
     SetOrientation cdlPortrait                 'default to portrait
End Sub

Public Sub SetOrientation(DefaultOrientation As PrinterOrientationConstants)
    m_orientation = DefaultOrientation
End Sub

Public Sub SetLeftMargin(inches As Single)
    m_left_margin = inches
End Sub


Private Sub Form_Load()
    Dim pct As Single, adjust As Single
    
    m_is_loading = True
    
    g_display.SetStdTheme Me
    'Many of our text reports rely on fixed spacing
    lblFont.font.name = "Courier New"
    
    'scale the window size to the display font size
    adjust = g_display.MessageFont.Size / 8.25
    Me.Width = Me.Width * adjust
    Me.Height = Me.Height * adjust
    
    With abrPrint
        .AlignToForm = True
        .ClientAreaControl = txtReport
        .AutoSizeChildren = ddASClientArea
    End With
    'Load the zoom box
    With abrPrint.Tools("mnuZoomPercent").CBList
        .AddItem "200%"
        .AddItem "150%"
        .AddItem "125%"
        .AddItem "100%"
        .AddItem "75%"
    End With
    '** This doesn't work??  It claims to make the change, but doesn't
    abrPrint.Tools("mnuZoomPercent").Width = abrPrint.Tools("mnuZoomPercent").Width * adjust
    
    g_toolbar.ConfigureActiveBar2 abrPrint, ImageList32, m_collection1, m_collection2

    txtReport.Locked = False                        'disable editing
    txtReport.RightMargin = 100000                  'don't word-wrap as text is zoomed
    
    'Set the default zoom (and display font)
    pct = 100 * adjust                              'get 103, 121, 150
    pct = Round(pct / 25#, 0) * 25#                 'round to the nearest multiple of 25
    SetZoom pct
    
    m_is_loading = False
End Sub


Private Sub abrPrint_ToolClick(ByVal Tool As ActiveBar2LibraryCtl.Tool)
    On Error GoTo errHandler

    Select Case Tool.name
    Case "mnuClose"
        Unload Me
    
    Case "mnuSaveAs"
        g_util.FileSaveAs CommonDialog1, txtReport.Text
    Case "mnuPrint"
        If SelectPrinter() Then
            PrintIt
        End If
    Case "mnuCopy"
        CopyToClipboard
    
    Case "mnuZoomIn"
        SetZoom m_zoom_percent + 25
    Case "mnuZoomOut"
        SetZoom m_zoom_percent - 25
    Case "mnuZoomPercent"
        'see TextChanged
    
    Case "mnuFont"
        SelectFont
    
    Case Else
        MsgBox "Unexpected menu item '" & Tool.name & "'", vbCritical + vbOKOnly, "Error"
    End Select

    Exit Sub
    
errHandler:
    g_util.MsgBoxError
End Sub


Private Sub abrPrint_TextChange(ByVal Tool As ActiveBar2LibraryCtl.Tool)
    On Error GoTo errHandler
    Dim s As String
    
    Select Case Tool.name
    Case "mnuZoomPercent"
        s = Tool.Text
        If Right$(s, 1) = "%" Then s = Left$(s, Len(s) - 1)
        If IsNumeric(s) Then
            SetZoom CInt(s)
        Else
            MsgBox "Please enter a number in the Zoom box", vbCritical + vbOKOnly, "Error"
            Tool.SetFocus
        End If
    Case Else
        MsgBox "Unexpected menu item '" & Tool.name & "'", vbCritical + vbOKOnly, "Error"
    End Select
    
    Exit Sub
    
errHandler:
    g_util.MsgBoxError
    Exit Sub
    Resume  'debug

End Sub


Private Sub PrintIt()
    Debug.Print Now & " begin print text"
    g_util.PrintText lblFont, txtReport.Text, m_left_margin
    Debug.Print Now & " end print text"
    
    'reset after each print
    m_left_margin = 0
End Sub

Private Function SelectPrinter() As Boolean
    Dim result As Boolean
    result = g_util.SelectPrinter(CommonDialog1, m_orientation)
    If (result) Then
        Debug.Print Now & " begin Sync printer"
        g_util.SyncPrinterFromText lblFont              'set printer font
        Debug.Print Now & " end Sync printer"
    End If
    SelectPrinter = result
End Function

Private Sub CopyToClipboard()
    Clipboard.Clear
    Clipboard.SetText txtReport.Text
End Sub

Private Sub SetZoom(pct As Single)
    Dim zoom_font_size As Single
    If (pct < 25) Then pct = 25
    If (pct > 500) Then pct = 500

    m_zoom_percent = pct
    'Change the display font size while leaving the print size alone
    zoom_font_size = pct * lblFont.font.Size / 100#
    
    'Note: do not set txtReport.Font = lblFont.Font: they will end up referring to the same
    '      object!  Copy values instead.
    txtReport.font.name = lblFont.font.name
    txtReport.font.Size = zoom_font_size
    txtReport.font.Bold = lblFont.font.Bold
    txtReport.font.Italic = lblFont.font.Italic

    Debug.Print "zoom=" & pct & "% print font=" & lblFont.font.Size & " request=" & zoom_font_size & " display=" & txtReport.font.Size

    'Update the combo box text if this was generated by in/out buttons or if the user entered a number
    abrPrint.Tools("mnuZoomPercent").Text = CStr(CInt(pct)) & "%"
End Sub

Private Sub SelectFont()
    g_util.SyncDialogFontFromText lblFont, CommonDialog1        'get font
    g_util.SelectFont CommonDialog1                             'select font
    g_util.SyncFontFromDialog lblFont, CommonDialog1            'set font

    'Update the display
    SetZoom m_zoom_percent
End Sub

