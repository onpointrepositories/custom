VERSION 5.00
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{0D6234D1-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TODG6.OCX"
Begin VB.Form frmLookupPatient 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Lookup Patient"
   ClientHeight    =   4275
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   9570
   Icon            =   "frmLookupPatient.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4275
   ScaleWidth      =   9570
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   375
      Left            =   4845
      TabIndex        =   5
      Top             =   3780
      Width           =   1215
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   375
      Left            =   3510
      TabIndex        =   4
      Top             =   3780
      Width           =   1215
   End
   Begin MSAdodcLib.Adodc adoMain 
      Height          =   330
      Left            =   120
      Top             =   3840
      Visible         =   0   'False
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   582
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin TrueOleDBGrid60.TDBGrid grdResults 
      Height          =   3195
      Left            =   120
      OleObjectBlob   =   "frmLookupPatient.frx":0E42
      TabIndex        =   3
      Top             =   480
      Width           =   12975
   End
   Begin VB.CommandButton cmdSearchAcct 
      Caption         =   "Search"
      Height          =   315
      Left            =   5520
      TabIndex        =   2
      Top             =   60
      Width           =   975
   End
   Begin VB.TextBox txtAccountNumber 
      Height          =   315
      Left            =   2880
      TabIndex        =   1
      Text            =   "Text1"
      Top             =   60
      Width           =   2535
   End
   Begin VB.Label Label1 
      Caption         =   "Account number (begins with):"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   90
      Width           =   2655
   End
End
Attribute VB_Name = "frmLookupPatient"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Enum GridColummns
    COL_LOCATION = 0
    COL_LAST_NAME
    COL_FIRST_NAME
    COL_AGE
    COL_SEX
    COL_ACCT
    NUMBER_OF_COLUMNS
End Enum

Private m_encounter_id As Long

Public Function LookupPatientEncounterID(parent As Form) As Long
    Me.show vbModal, parent
    
    LookupPatientEncounterID = m_encounter_id
End Function


Private Sub Form_Load()
    g_display.SetStdTheme Me
    
    txtAccountNumber.Text = ""
    
    Set grdResults.DataSource = adoMain
    grdResults.AllowUpdate = False
    grdResults.AllowAddNew = False
    grdResults.AllowDelete = False
End Sub

Private Sub cmdSearchAcct_Click()
    Dim sql As String
    Dim adjust As Single
    
    If Len(txtAccountNumber.Text) = 0 Then Exit Sub
    
    sql = "select top 100 current_location as Location, last_name as Last, first_name as First," & _
        "    round(age_at_admission,0) as Age, sex_lvc as Sex, acct_number as Acct, e.encounter_id" & _
        " from encounter as e" & _
        " inner join person as p on (p.person_id = e.person_id)" & _
        " where acct_number like " & g_dbutil.SQL_String(txtAccountNumber.Text & "%")

    Debug.Print sql
    adoMain.ConnectionString = g_dbutil.DefaultConnectionString()
    adoMain.CursorLocation = adUseClient        'highly recommended by TDBGrid
    adoMain.RecordSource = sql
    adoMain.mode = adModeRead
    adoMain.Refresh

    adjust = grdResults.font.Size / 8#          'adjust for any change in font size
    
    With grdResults.Columns(COL_AGE)
        .width = 600 * adjust
        .Alignment = dbgCenter
    End With
    
    With grdResults.Columns(COL_SEX)
        .width = 600 * adjust
        .Alignment = dbgCenter
    End With
    
    grdResults.Columns(COL_ACCT).width = 2000 * adjust
End Sub

Private Sub cmdOK_Click()
    Dim enc As encounter
    
    If (grdResults.row < 0) Then
        MsgBox "Please select a patient"
        Exit Sub
    End If
    m_encounter_id = CLng(grdResults.Columns(6).value)
    Unload Me
End Sub

Private Sub cmdCancel_Click()
    m_encounter_id = 0
    Unload Me
End Sub

Private Sub grdResults_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    On Error Resume Next                'ignore click on empty grid
    'Select the entire row when any part of the row is clicked on
    g_grdUtil.SelectRowColChange grdResults
End Sub

Private Sub grdResults_DblClick()
    cmdOK_Click
End Sub


