VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PFSActiveReport"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'
' PFSActiveReports: generic helper routines for Active Reports
'
Private Const SAVE_AS_CAPTION = "Save As..."


'Use these colors in reports (they are not in the built-in color menu)
Public Function ReportBlueColor() As Long
    ReportBlueColor = &HFFFFE0
End Function

Public Function ReportYellowColor() As Long
    ReportYellowColor = &HE0FFFF
End Function

Public Function ReportWhiteColor() As Long
    ReportWhiteColor = &HFFFFFF
End Function


Public Sub SetupPreviewWindow(ByRef rpt As Variant, ByVal modal As FormShowConstants, Optional ByVal ownerForm As Form = Nothing)
    On Error GoTo errHandler
    Static gen As rptGeneric
    
    If (gen Is Nothing) Then
        Set gen = New rptGeneric
    End If
    
    'Set a common icon and window title
    rpt.Icon = gen.Icon                                 'Use "Generic" report icon
    rpt.caption = g_util.AppCaption("Report Preview")   'Harris Healthcare AcuityPlus Report Preview X.X

    'Add an export button
    AddSaveAsButton rpt
    
    rpt.WindowState = vbMaximized
    
    If (Not ownerForm Is Nothing) Then
        rpt.show modal, ownerForm
    Else
        rpt.show modal
    End If
    Exit Sub

errHandler:
    g_util.ThrowError "SetupPreviewWindow"
    Resume  'debug
End Sub

'
' Add a "Save As" button to the ActiveReports preview toolbar
' The report must implement ActiveReport_ToolbarClick and call g_arutil.ToolbarClick
'
Public Sub AddSaveAsButton(ByVal rpt As ActiveReport)
    On Error Resume Next
    Dim frm As New frmCommonDialog

    rpt.Toolbar.Tools.Insert 2, SAVE_AS_CAPTION
    rpt.Toolbar.Tools.item(2).AddIcon frm.Icon
    rpt.Toolbar.Tools.item(2).Tooltip = "Export"

    rpt.Toolbar.Tools.Insert 3, ""
    rpt.Toolbar.Tools.item(3).Type = 2              'separator

    Unload frm
End Sub

Public Function FooterVersion() As String
    ' "QuadraMed" is already in the logo; that's why g_util.AppCaption won't work here
    FooterVersion = App.ProductName & " " & App.major & "." & App.minor
    
    If App.revision <> 0 Then
        FooterVersion = FooterVersion & "." & App.revision
    End If
End Function

'
' Call this from ActiveReport_ToolbarClick
'
Public Function ProcessToolbarClick(toolCaption As String, rpt As ActiveReport, document_name As String) As Boolean
    If toolCaption = SAVE_AS_CAPTION Then
        SaveReport rpt, document_name
        ProcessToolbarClick = True
    End If
End Function

Private Function MakeSafeDocumentName(document_name As String) As String
    Dim result As String
    
    'A file name cannot contain any of these characters: \ / : * ? " < > |
    result = document_name
    result = Replace$(result, "\", "_")
    result = Replace$(result, "/", "_")
    result = Replace$(result, ":", "_")
    result = Replace$(result, "*", "_")
    result = Replace$(result, "?", "_")
    result = Replace$(result, """", "_")
    result = Replace$(result, "<", "_")
    result = Replace$(result, ">", "_")
    result = Replace$(result, "|", "_")
    
    MakeSafeDocumentName = result
End Function

'
' Save an ActiveReport in any number of export formats
'
Public Sub SaveReport(rpt As ActiveReport, document_name As String)

    Dim frm             As New frmCommonDialog
    Dim cdgExport       As CommonDialog
    Dim oPDF            As ActiveReportsPDFExport.ARExportPDF
    Dim oEXL            As ActiveReportsExcelExport.ARExportExcel
    Dim oTXT            As ActiveReportsTextExport.ARExportText
    Dim oHTML           As ActiveReportsHTMLExport.HTMLexport
    Dim oRTF            As ActiveReportsRTFExport.ARExportRTF
    Dim oTIFF           As ActiveReportsTIFFExport.TIFFExport
    Dim default_path    As String

    On Error Resume Next 'ignore error if Export directory already exists
    MkDir App.Path & "\..\Export"

    On Error GoTo cancelExit                        'exit on Common Dialog Cancel
    
    Set cdgExport = frm.CommonDialog1               'get a common dialog control

    cdgExport.DialogTitle = "Save As"
    cdgExport.filter = "PDF Files (*.pdf)|*.pdf|" + _
                           "Excel Files (*.xls)|*.xls|" + _
                           "Text Files (*.txt)|*.txt|" + _
                           "HTML Files (*.html)|*.html|" + _
                           "RTF Files (*.rtf)|*.rtf|" + _
                           "TIFF Files (*.tiff)|*.tiff"

    cdgExport.FilterIndex = 1                       'default to PDF
    cdgExport.InitDir = App.Path & "\..\Export\"
    cdgExport.filename = MakeSafeDocumentName(document_name)
    cdgExport.flags = cdlOFNOverwritePrompt         'warn if file already exists
    cdgExport.CancelError = True                    'raise error on cancel
    If g_util.GetKeyValue(HKEY_CURRENT_USER, "Software\Microsoft\Windows\CurrentVersion\Explorer\Shell Folders", "Personal", default_path) Then
        'Default to "My Documents"
        cdgExport.InitDir = default_path
    End If

    cdgExport.ShowSave

    Select Case cdgExport.FilterIndex
        Case 1
            Set oPDF = New ActiveReportsPDFExport.ARExportPDF
            oPDF.filename = cdgExport.filename
            oPDF.Export rpt.Pages

        Case 2
            Set oEXL = New ActiveReportsExcelExport.ARExportExcel
            oEXL.filename = cdgExport.filename
            oEXL.Export rpt.Pages

        Case 3
            Set oTXT = New ActiveReportsTextExport.ARExportText
            oTXT.filename = cdgExport.filename
            oTXT.Export rpt.Pages

        Case 4
            Set oHTML = New ActiveReportsHTMLExport.HTMLexport
            oHTML.filename = cdgExport.filename
            oHTML.Export rpt.Pages

        Case 5
            Set oRTF = New ActiveReportsRTFExport.ARExportRTF
            oRTF.filename = cdgExport.filename
            oRTF.Export rpt.Pages

        Case 6
            Set oTIFF = New ActiveReportsTIFFExport.TIFFExport
            oTIFF.filename = cdgExport.filename
            oTIFF.Export rpt.Pages
        Case Else
            MsgBox "Unknown file format", vbCritical, "Unknown Format"
    End Select

normalExit:
    Set cdgExport = Nothing
    Unload frm
    Set frm = Nothing
    Exit Sub

cancelExit:
    Resume normalExit
End Sub

