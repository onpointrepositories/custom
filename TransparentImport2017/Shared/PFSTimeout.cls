VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PFSTimeout"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'
' This the PFS Timeout class.
'
' This class is shared by several applications so it does not make any exteral
' references.  It would be nice to make this an ActiveX DLL but this code needs
' to be in the same thread and process as the main program in order to work.
'
' The following files are required:
'   frmTimeout.frm                  'The "Application Locked" dialog
'   Timeout.bas                     'The callback routines
'   PFSTimeout.cls                  'This class
'
' Add this line of code to the main module:
'   Public g_timeout As New PFSTimeout
'
' * The main program or form should set the timeout delay.
' * The main program or form should set the "shutdown on timeout" option.
' * The main form should start a slow timer (10-15 second) to check for timeout.
' * Any form with a QueryUnload needs this line of code inserted at the top:
'       If g_timeout.QueryUnloadExitNow Then Exit Sub
' * If a form opens a dialog box in its QueryUnload event, it must call
'   QueryUnloadDialogOpen and QueryUnloadDialogClosed before and after the dialog.
'   This includes calling a subroutine that displays a dialog ("Save Changes?").
'   You can put these calls around the subroutine call.
'
' ***** DEVELOPERS:
' DO NOT PRESS THE 'STOP' BUTTON WHILE HOOKS ARE PRESENT -- THE VB IDE WILL DIE.
' You can remove the hooks three ways:
'   1) Set g_timeout.TimeoutDelay to zero
'   2) Call g_timeout.ShutdownNow
'   3) Set g_timeout to Nothing
'

'The default shutdown option is preferred but is not working well with the main program
Public Enum ShutdownOptions
    DEFAULT_SHUTDOWN                'let Shutdown decide
    UNLOAD_FORMS                    'should work but can hang
    END_COMMAND                     'should work but often doesn't
    EXIT_PROCESS                    'always works but CCOW context thinks we are still there
End Enum

Public Enum RestartOptions
    NO_RESTART
    WITH_RESTART
End Enum

Private m_timeout_delay         As Long         'How long to wait (seconds). 0 = disable
Private m_timeout_datetime      As Date         'When to timeout
Private m_ignore_timeout        As Boolean      'ignore timeout if it occurs?
Private m_prev_timeout_delay    As Long         'save previous delay
Private m_shutdown_on_timeout   As Boolean      'shutdown without asking?
Private m_shutting_down         As Boolean      'are we shutting down?


Private Sub Class_Initialize()
    'Nothing Special
End Sub

Private Sub Class_Terminate()
    'Fail safe: remove the hooks if no one else did.  This is the best reason for
    'all this code to be in a class.  If you don't remove the hooks before ending
    'the program, a GPF error will be generated and the IDE will crash.
    RemoveHooks
End Sub


Private Sub SetHooks()
    'Note: Redundant calls are ignored
    SetKeyboardHook
    SetMouseHook
End Sub

Private Sub RemoveHooks()
    'Note: Redundant calls are ignored
    RemoveKeyboardHook
    RemoveMouseHook
End Sub


'Get the timeout delay (in seconds)
Public Property Get TimeoutDelay() As Long
    TimeoutDelay = m_timeout_delay
End Property

'Set the timeout delay (in seconds).  Set to zero for no timeout.
Public Property Let TimeoutDelay(seconds As Long)
    m_prev_timeout_delay = m_timeout_delay
    m_timeout_delay = seconds
    
    ResetTimeout
    
    If (m_timeout_delay > 0) Then
        SetHooks                            'Start monitoring events
    Else
        RemoveHooks                         'Stop monitoring events
    End If
End Property

Public Function Enabled() As Boolean
    Enabled = (m_timeout_delay > 0)
End Function

Public Property Get IgnoreTimeout() As Boolean
    IgnoreTimeout = m_ignore_timeout
End Property

Public Property Let IgnoreTimeout(newval As Boolean)
    m_ignore_timeout = newval
End Property

Public Sub CancelTimeout()
    TimeoutDelay = 0
End Sub

Public Sub DisableTimeout()
    TimeoutDelay = 0
End Sub

Public Sub PauseTimeout()
    TimeoutDelay = 0
End Sub

Public Sub RestoreTimeout()
    TimeoutDelay = m_prev_timeout_delay
End Sub

Public Property Get ShutdownOnTimeout() As Boolean
    ShutdownOnTimeout = m_shutdown_on_timeout
End Property

Public Property Let ShutdownOnTimeout(newval As Boolean)
    m_shutdown_on_timeout = newval
End Property


'This is when we will timeout (approximately)
Public Property Get TimeoutDatetime() As Date
    TimeoutDatetime = m_timeout_datetime
End Property

'This is how long we have until we time out (approximately)
Public Function TimeRemaining() As Date
    Dim dt As Date
        
    dt = Now  'Do not use the server time because we need low overhead in the timeout checking and the actual time is not important.
    
    If Enabled And (dt < m_timeout_datetime) Then
        TimeRemaining = m_timeout_datetime - dt
    Else
        TimeRemaining = 0
    End If
End Function

'This is how long we have until we time out (approximately)
Public Function SecondsRemaining() As Long
    Dim dt As Date
        
    dt = Now  'Do not use the server time because we need low overhead in the timeout checking and the actual time is not important.
    
    If Enabled And (dt < m_timeout_datetime) Then
        SecondsRemaining = DateDiff("s", dt, m_timeout_datetime)
    Else
        SecondsRemaining = 0
    End If
End Function


'All forms should test this in their Form_QueryUnload event and exit immediately if true
Public Property Get QueryUnloadExitNow() As Boolean
    QueryUnloadExitNow = m_shutting_down
End Property

Public Property Get ShuttingDown() As Boolean
    ShuttingDown = m_shutting_down
End Property

'The timer on the main form should call this, but only if you want to lock
'the application on timeout.  We now log the user off, so this is no longer used.
Public Sub CheckForTimeout(frm As Form)
    If (m_timeout_delay <= 0) Then Exit Sub         'no delay is set
    If (m_ignore_timeout) Then Exit Sub             'ignore any timeout

    'Do not use the server time because we need low overhead in the timeout checking and the actual time is not important.
    If (Now > m_timeout_datetime) Then              'time to lock?
        TimeoutNow frm                              'do it
    End If
End Sub


'The mouse hook procedure calls this when the mouse moves.
Friend Sub MouseMove()
    'Check for timeout first in case the timer was disabled by an application-modal dialog.
    'This only happens in the IDE so skip it to save on overhead.
    'CheckForTimeout
    
    'Reset the timeout because the mouse moved.
    ResetTimeout
End Sub

'The keyboard hook procedure calls this when a key goes down.
Friend Sub KeyDown()
    'Check for timeout first in case the timer was disabled by an application-modal dialog.
    'This only happens in the IDE so skip it to save on overhead.
    'CheckForTimeout
    
    'Reset the timeout because a key was pressed.
    ResetTimeout
End Sub


Public Sub ResetTimeout()
    If (m_timeout_delay <= 0) Then Exit Sub

    'Calculate when to timeout next
    m_timeout_datetime = DateAdd("s", m_timeout_delay, Now)   'Do not use the server time because we need low overhead in the timeout checking and the actual time is not important.
End Sub

'
' This is no longer used - we shut down instead of locking the app.
'
Private Sub TimeoutNow(parent As Form)
    On Error GoTo errHandler
    Static processing_timeout As Boolean

    'Prevent multiple timeout dialogs.  This is only a problem if both the timer
    'and MouseMove decide the time is up.
    If processing_timeout Then Exit Sub
    
    processing_timeout = True
    
    If m_shutdown_on_timeout Then
        'Don't say anything -- just shut down
        ShutdownNow PFS_EXIT_TIMEOUT, DEFAULT_SHUTDOWN, NO_RESTART
        Exit Sub                                    'just in case we get here
    Else
        'Tell the user that the application has locked
        Dim frm As New frmTimeout
        frm.show vbModal
        
        'If we get here and the shutdown flag is set that means that the timeout
        'form just got unloaded and the thread of execution got back here.
        If m_shutting_down Then Exit Sub

        'If we make it here then the user gave the correct password and we
        'will unlock now and restore focus to the main form to the top.
        
        'Any of these calls may cause an error: you cannot, for example,
        'set focus to the main window if a modal dialog was up.
        On Error Resume Next
        
        'Bring the main window to the top
        If (parent.WindowState = vbMinimized) Then
            parent.WindowState = vbNormal
        End If
        parent.ZOrder 0
        parent.SetFocus
    End If

    ResetTimeout
    processing_timeout = False
    Exit Sub
    
errHandler:
    g_util.ThrowError "TimeoutNow"
    Resume  'debug
End Sub


'Shut down the application, now; No one must refuse to unload.
'
Public Sub ShutdownNow(ByVal exit_code As Long, ByVal shutdown_option As ShutdownOptions, ByVal restart_option As RestartOptions)
    On Error Resume Next

    'Are we already trying to shut down?  Avoid recursion.
    If (m_shutting_down) Then Exit Sub

    m_shutting_down = True

    'Remove the mouse and keyboard hooks - we must call this before we exit or
    'we will cause a GPF.  If there are no hooks this call will be ignored.
    RemoveHooks

    If (restart_option = WITH_RESTART) Then
        'Start another instance of the program; tell it to ignore our instance
        'and start anyway.  Go to our home dir again just to be sure the working
        'directory is correct.
        'Tell it to ask for login -- that's the only reason for a restart; this will
        'override an NT login setting.
        ChDrive App.Path
        ChDir App.Path
        Shell App.EXEName & " " & Command$ & " -ignoreprevinstance -login", vbNormalFocus
    End If
    
    'Choose DEFAULT_SHUTDOWN to let this routine decide how to shut down.
    If (shutdown_option = DEFAULT_SHUTDOWN) Then
        If (g_util.IsWindowsXPfamily) Then
            shutdown_option = EXIT_PROCESS             'guaranteed but error msg on Win7?
        Else
            shutdown_option = UNLOAD_FORMS             'preferred but less reliable
        End If
    End If
    
    Select Case shutdown_option
    Case UNLOAD_FORMS
        'This is the preferred method to end an application but it has drawbacks:
        '* Any reference to another form will keep the application running even if all windows are hidden.
        '* If any Form_QueryUnload() has an open dialog box ("save changes"), the application
        '  will hang when you try to unload any form.
        g_util.UnloadAllForms

    Case END_COMMAND
        g_util.UnloadAllForms
        'We aren't suppose to do this, but it works better than unloading the forms.
        ''End' usually works well in the IDE but not always in the exe.
        End
    
    Case EXIT_PROCESS
        g_util.UnloadAllForms
        'We aren't supposed to exit our own our process but here is the option.
        ' - We are supposed to let VB shut down
        ' - ExitProcess works great but it also kills the IDE.
        'Jan-2011: this causes a "stopped working" error message on Windows 7 (groan)
        If Right$(App.Path, 4) = "\bin" Then
            'ExitProcess is the preferred way to exit your own program.
            '(TerminateProcess is used to kill another program)
            g_util.ExitProcess exit_code                'Exit an exe
        Else
            End                                         'Call End when running under the IDE
        End If
    End Select

End Sub

