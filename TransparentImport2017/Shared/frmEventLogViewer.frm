VERSION 5.00
Object = "{4932CEF1-2CAA-11D2-A165-0060081C43D9}#2.0#0"; "Actbar2.ocx"
Object = "{50EB1B32-AA9B-11D0-B667-00A024C3C1C2}#2.0#0"; "resizer.dll"
Object = "{67397AA1-7FB1-11D0-B148-00A0C922E820}#6.0#0"; "MSADODC.OCX"
Object = "{0D6234D1-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TODG6.OCX"
Begin VB.Form frmEventLogViewer 
   Caption         =   "Event Log"
   ClientHeight    =   5760
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   13785
   Icon            =   "frmEventLogViewer.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MinButton       =   0   'False
   ScaleHeight     =   5760
   ScaleWidth      =   13785
   StartUpPosition =   2  'CenterScreen
   Begin MSAdodcLib.Adodc adoMain 
      Height          =   375
      Left            =   11040
      Top             =   0
      Visible         =   0   'False
      Width           =   1200
      _ExtentX        =   2117
      _ExtentY        =   661
      ConnectMode     =   0
      CursorLocation  =   3
      IsolationLevel  =   -1
      ConnectionTimeout=   15
      CommandTimeout  =   30
      CursorType      =   3
      LockType        =   3
      CommandType     =   8
      CursorOptions   =   0
      CacheSize       =   50
      MaxRecords      =   0
      BOFAction       =   0
      EOFAction       =   0
      ConnectStringType=   1
      Appearance      =   1
      BackColor       =   -2147483643
      ForeColor       =   -2147483640
      Orientation     =   0
      Enabled         =   -1
      Connect         =   ""
      OLEDBString     =   ""
      OLEDBFile       =   ""
      DataSourceName  =   ""
      OtherAttributes =   ""
      UserName        =   ""
      Password        =   ""
      RecordSource    =   ""
      Caption         =   "Adodc1"
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      _Version        =   393216
   End
   Begin ActiveBar2LibraryCtl.ActiveBar2 abrMain 
      Height          =   375
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   10455
      _LayoutVersion  =   1
      _ExtentX        =   18441
      _ExtentY        =   661
      _DataPath       =   ""
      Bands           =   "frmEventLogViewer.frx":0442
   End
   Begin OlectraResizer.ControlContainer ControlContainer1 
      Height          =   5220
      Left            =   120
      TabIndex        =   0
      Top             =   420
      Width           =   13545
      _ExtentX        =   23892
      _ExtentY        =   9208
      Version         =   130816
      BeginProperty HorizontalSpacer {48AADD41-D2A0-11D0-B53A-0020AFD59EF6} 
         Version         =   130816
      EndProperty
      BeginProperty VerticalSpacer {48AADD41-D2A0-11D0-B53A-0020AFD59EF6} 
         Version         =   130816
      EndProperty
      Begin TrueOleDBGrid60.TDBGrid grdResults 
         Height          =   5220
         Left            =   0
         OleObjectBlob   =   "frmEventLogViewer.frx":5170
         TabIndex        =   1
         Top             =   0
         Width           =   13545
      End
   End
   Begin OlectraResizer.Spacer Spacer2 
      Height          =   164
      Left            =   13665
      TabIndex        =   3
      Top             =   2948
      Width           =   120
      Version         =   130816
      Size            =   120
      MainAxis        =   1
      BeginProperty LeftConnection {B2E77E02-0AA7-11D1-B572-006097CF02D2} 
         Version         =   130817
         Control         =   "ControlContainer1"
         Edge            =   2
      EndProperty
      BeginProperty RightConnection {B2E77E02-0AA7-11D1-B572-006097CF02D2} 
         Version         =   130817
         Control         =   "frmEventLogViewer"
         IsTerminator    =   -1  'True
         IsContainer     =   -1  'True
      EndProperty
   End
   Begin OlectraResizer.Spacer Spacer1 
      Height          =   120
      Left            =   6811
      TabIndex        =   2
      Top             =   5640
      Width           =   164
      Version         =   130816
      Size            =   120
      MainAxis        =   1
      Orientation     =   0
      BeginProperty TopConnection {B2E77E02-0AA7-11D1-B572-006097CF02D2} 
         Version         =   130817
         Control         =   "ControlContainer1"
         Edge            =   3
      EndProperty
      BeginProperty BottomConnection {B2E77E02-0AA7-11D1-B572-006097CF02D2} 
         Version         =   130817
         Control         =   "frmEventLogViewer"
         Edge            =   3
         IsTerminator    =   -1  'True
         IsContainer     =   -1  'True
      EndProperty
   End
End
Attribute VB_Name = "frmEventLogViewer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'
' Event Viewer (interface logs)
'
' THIS WINDOW IS SHARED BY SYSTEM MANAGER AND PATIENT SELECTION
Private Enum GridColummns
    COL_TIMESTAMP = 0
    COL_SOURCE_CODE
    COL_SOURCE
    COL_TYPE
    COL_CATEGORY
    COL_DESCRIPTION
    COL_SOURCE_TEXT
    COL_TCP_PORT
    COL_UNIT
    COL_ENCOUNTER
    NUMBER_OF_COLUMNS
End Enum

Private m_filter            As New PFSEventLogFilter
Private m_initial_unit_id   As Long
Private m_initial_enc_id    As Long
Private m_desired_caption   As String

'
' Initial settings (optional)
'
Public Sub Setup(Optional unit_id As Long = 0, Optional encounter_id As Long = 0)
    m_initial_unit_id = unit_id                     'save for form_load
    m_initial_enc_id = encounter_id
End Sub


Private Sub SetCaption(title As String)
    m_desired_caption = title
End Sub

Private Sub Form_Load()
    On Error GoTo errHandler
    Dim desc As String

    g_display.SetStdTheme Me, True
    
    SetCaption ""
    Me.width = g_util.Min(16000, g_display.WorkAreaWidth)
    
    g_toolbar.ConfigureActiveBar2 abrMain
    'abrMain.Tools("miToday").Checked = True
    abrMain.Tools("miTop100").Checked = True
    abrMain.Tools("miNewestFirst").Checked = True

    ConfigureGrid

    ResetFilter
    m_filter.UnitID = m_initial_unit_id
    m_filter.encounter_id = m_initial_enc_id
    m_filter.Today = True
    desc = ""
    If (m_initial_unit_id <> 0) Then
        desc = desc & " Unit=" & m_initial_unit_id
    End If
    If (m_initial_enc_id <> 0) Then
        desc = desc & " Encounter=" & m_initial_enc_id
        'Turn off the time limit for encounter log
        'abrMain.Tools("miToday").Checked = False
        m_filter.Today = False
    End If
    SetCaption desc
    
    LoadEvents
    Exit Sub
    
errHandler:
    g_util.MsgBoxError
    Exit Sub
End Sub

Private Sub Form_Resize()
    abrMain.width = Me.ScaleWidth
End Sub

Private Sub abrMain_ToolClick(ByVal Tool As ActiveBar2LibraryCtl.Tool)
    On Error GoTo errHandler
    
    Select Case Tool.Name
    Case "miFile"
    Case "miExit"
        Unload Me
    
    Case "miView"
    Case "miViewAllEvents"
        ResetFilter
        SetCaption ""
        LoadEvents
    
    Case "miViewHL7"
        QuickSetFilter EVENT_SOURCE_HL7, EVENT_TYPE_NONE
        SetCaption "HL7 Log"
        LoadEvents
    Case "miViewImport"
        QuickSetFilter EVENT_SOURCE_BATCH_IMPORT, EVENT_TYPE_NONE
        SetCaption "Import Log"
        LoadEvents
    Case "miViewReportCalc"
        QuickSetFilter EVENT_SOURCE_REPORT_CALCULATIONS, EVENT_TYPE_NONE
        SetCaption "Report Calculation Log"
        LoadEvents
    Case "miViewTransparentMapping"
        QuickSetFilter EVENT_SOURCE_TRANSPARENT_MAPPING, EVENT_TYPE_NONE
        SetCaption "Transparent Mapping Log"
        LoadEvents
    Case "miViewTransparent"
        QuickSetFilter EVENT_SOURCE_TRANSPARENT_IMPORT, EVENT_TYPE_NONE
        SetCaption "Transparent Import Log"
        LoadEvents
    Case "miViewDataConsistency"
        QuickSetFilter EVENT_SOURCE_DATA_CONSISTENCY, EVENT_TYPE_NONE
        SetCaption "Data Consistency Log"
        LoadEvents
    
    Case "miViewHL7Errors"
        QuickSetFilter EVENT_SOURCE_HL7, EVENT_TYPE_ERROR
        SetCaption "HL7 Errors"
        LoadEvents
    Case "miViewImportErrors"
        QuickSetFilter EVENT_SOURCE_BATCH_IMPORT, EVENT_TYPE_ERROR
        SetCaption "Import Errors"
        LoadEvents
    Case "miViewReportCalcErrors"
        QuickSetFilter EVENT_SOURCE_REPORT_CALCULATIONS, EVENT_TYPE_ERROR
        SetCaption "Report Calculation Errors"
        LoadEvents
    Case "miViewTransparentMappingErrors"
        QuickSetFilter EVENT_SOURCE_TRANSPARENT_MAPPING, EVENT_TYPE_ERROR
        SetCaption "Transparent Mapping Errors"
        LoadEvents
    Case "miViewTransparentErrors"
        QuickSetFilter EVENT_SOURCE_TRANSPARENT_IMPORT, EVENT_TYPE_ERROR
        SetCaption "Transparent Import Errors"
        LoadEvents
    Case "miViewDataConsistencyErrors"
        QuickSetFilter EVENT_SOURCE_DATA_CONSISTENCY, EVENT_TYPE_ERROR
        SetCaption "Data Consistency Errors"
        LoadEvents
    
    Case "miViewHL7Reject"
        QuickSetFilter EVENT_SOURCE_HL7, EVENT_TYPE_REJECT
        SetCaption "HL7 Rejections"
        LoadEvents
    
    Case "miViewEncounter"
        ResetFilter
        m_filter.encounter_id = GetSelectedEncounterID()
        SetCaption "Encounter " & m_filter.encounter_id
        LoadEvents
    Case "miViewWizard"
        frmEventLogWizard.Setup Me, m_filter
        frmEventLogWizard.show vbModal, Me              'this will call back to reload events
        SetCaption "filtered"
    Case "miViewFilter"
        frmEventLogFilter.Setup Me, m_filter            'set up the filter dialog from last filter
        frmEventLogFilter.show vbModal, Me              'this will call back to reload events
        SetCaption "filtered"
    
'    Case "miToday"
'        Tool.Checked = Not Tool.Checked
'        LoadEvents
    Case "miTop100"
        Tool.Checked = Not Tool.Checked
        LoadEvents
    Case "miNewestFirst"
        Tool.Checked = Not Tool.Checked
        LoadEvents
    Case "miViewRefresh"
        LoadEvents
    
    Case "miReport"
    Case "miReportEventDetail"
        ShowEventDetail
    Case "miReportRelatedMessages"
        ShowRelatedMessages
    
    Case "miReportHL7Log"
        ShowInterfaceOrImportLog EVENT_SOURCE_HL7, GetSelectedTCPPort()
    Case "miReportEncounterHL7Log"
        ShowEncounterLog EVENT_SOURCE_HL7, False
    Case "miReportEncounterLogWithHL7"
        ShowEncounterLog EVENT_SOURCE_HL7, True
    Case "miReportEncounterRawHL7"
        ShowEncounterRawHL7
    Case "miReportEncounterADT"
        ShowEncounterADT
    Case "miReportEncounterChart"
        ShowEncounterChart
    Case "miReportEncounterOrders"
        ShowEncounterOrders
    
    Case "miReportImportLog"
        ShowInterfaceOrImportLog GetSelectedSource(), GetSelectedBatchNumber()
    Case "miReportEncounterImportLog"
        ShowEncounterLog GetSelectedSource(), False
        
    Case "miHelpEventViewer"
        g_help.ShowHelp hWnd, HELP_EVENT_LOG_VIEWER
    
    Case Else
        MsgBox "Unexpected menu item '" & Tool.Name & "'"
    End Select

    Exit Sub
    
errHandler:
    g_util.MsgBoxError
    Exit Sub
End Sub

Private Function GetSelectedValue(col_num As Integer) As Variant
    If (grdResults.row >= 0) Then
        GetSelectedValue = grdResults.Columns(col_num).value
    Else
        GetSelectedValue = Null
    End If
End Function

Private Function GetSelectedSource() As EventLogSource
    GetSelectedSource = g_dbutil.DBToLong(GetSelectedValue(COL_SOURCE_CODE))
End Function

Private Function GetSelectedEncounterID() As Long
    GetSelectedEncounterID = g_dbutil.DBToLong(GetSelectedValue(COL_ENCOUNTER))
End Function

Private Function GetSelectedBatchNumber() As Long
    GetSelectedBatchNumber = g_dbutil.DBToLong(GetSelectedValue(COL_TCP_PORT))          'TCP/Batch
End Function

Private Function GetSelectedTCPPort() As Long
    GetSelectedTCPPort = g_dbutil.DBToLong(GetSelectedValue(COL_TCP_PORT))
End Function

Private Sub SetMenuState()
    Dim Source As EventLogSource
    Dim enc_id As Long, batch_number As Long
    Dim row_selected As Boolean, has_encounter_id As Boolean, has_batch_number As Boolean
    Dim is_hl7_item As Boolean, is_import_item As Boolean
    
    row_selected = (grdResults.row >= 0)
    Source = GetSelectedSource
    enc_id = GetSelectedEncounterID
    batch_number = GetSelectedBatchNumber
    
    is_hl7_item = (Source = EVENT_SOURCE_HL7)
    is_import_item = (Source = EVENT_SOURCE_BATCH_IMPORT) Or (Source = EVENT_SOURCE_TRANSPARENT_IMPORT)
    has_encounter_id = (enc_id <> 0)
    has_batch_number = (batch_number <> 0)

    abrMain.Tools("miViewEncounter").Enabled = (enc_id <> 0)
    
    If row_selected Then
        'It is safe to query column values
        abrMain.Tools("miReportRelatedMessages").Enabled = (Len(grdResults.Columns(COL_SOURCE_TEXT).value) > 0)
    Else
        abrMain.Tools("miReportRelatedMessages").Enabled = False
    End If
    
    abrMain.Tools("miReportEventDetail").Enabled = row_selected
    
    abrMain.Tools("miReportHL7Log").Enabled = is_hl7_item
    abrMain.Tools("miReportEncounterHL7Log").Enabled = (is_hl7_item And has_encounter_id)
    abrMain.Tools("miReportEncounterLogWithHL7").Enabled = (is_hl7_item And has_encounter_id)
    abrMain.Tools("miReportEncounterRawHL7").Enabled = (is_hl7_item And has_encounter_id)
    abrMain.Tools("miReportEncounterADT").Enabled = (is_hl7_item And has_encounter_id)
    abrMain.Tools("miReportEncounterChart").Enabled = (is_hl7_item And has_encounter_id)
    abrMain.Tools("miReportEncounterOrders").Enabled = (is_hl7_item And has_encounter_id)
    
    abrMain.Tools("miReportImportLog").Enabled = (is_import_item And has_batch_number)
    abrMain.Tools("miReportEncounterImportLog").Enabled = (is_import_item And has_encounter_id)

End Sub

Private Sub grdResults_DblClick()
    ShowEventDetail
End Sub

Private Sub grdResults_RowColChange(LastRow As Variant, ByVal LastCol As Integer)
    'Select the entire row when any part of the row is clicked on
    g_grdUtil.SelectRowColChange grdResults
    SetMenuState
End Sub

Private Sub ConfigureGrid()
    Const read_only = True
    
    'grdResults.DataMode = dbgBound                 'must set in designer
    Set grdResults.DataSource = adoMain
    
    grdResults.AllowUpdate = (Not read_only)
    grdResults.AllowAddNew = (Not read_only)
    grdResults.AllowDelete = (Not read_only)
End Sub

Private Sub ResetFilter()
    'set defaults to get all events
    m_filter.TypeIDs = ""
    m_filter.Source = ""
    m_filter.Category = ""
    m_filter.Description = ""
    m_filter.SourceText = ""
    m_filter.UnitID = 0
    m_filter.TCPPort = 0
    m_filter.encounter_id = 0
    m_filter.StartDateTime = Date                   'midnight this morning - default
    m_filter.FinishDateTime = Now
    m_filter.UseStartDate = False                   'ignore the start date
    m_filter.UseFinishDate = False
End Sub

Private Sub QuickSetFilter(src As EventLogSource, typ As EventLogType)
    Static been_here As Boolean
    
    ResetFilter

    m_filter.Source = src
    
    If (typ = EVENT_TYPE_NONE) Then
        m_filter.TypeIDs = ""
    Else
        m_filter.TypeIDs = CStr(typ)
    End If
    
    If (Not been_here) Then
        'The first time, limit the results
        m_filter.Today = True
        abrMain.Tools("miTop100").Checked = True
        been_here = True
    End If
End Sub

Public Sub LoadEvents()
    On Error GoTo errHandler
    Dim sql As String, caption As String
    Dim adjust As Single
    
    g_util.CursorPushHourglass
    
    caption = App.ProductName & " Event Log" & IIf(Len(m_desired_caption), " - " & m_desired_caption, "")
    sql = BuildSQL(caption)
    Debug.Print sql
    Me.caption = caption

    adoMain.ConnectionString = g_dbutil.DefaultConnectionString()
    adoMain.CursorLocation = adUseClient        'highly recommended by TDBGrid
    adoMain.CommandTimeout = 20 * 60            'The default timeout is much too short [7367]
    adoMain.RecordSource = sql
    adoMain.mode = adModeRead
    adoMain.Refresh
    
    'The grid has just been re-configured
    'Adjust some column widths; take into account the current font vs. the form design font
    adjust = grdResults.font.Size / 8#
    grdResults.Columns(COL_TIMESTAMP).width = PFS_DATETIME_COLUMN_WIDTH * adjust
    grdResults.Columns(COL_SOURCE_CODE).Visible = False                 'hide the code
    grdResults.Columns(COL_SOURCE).width = 1800 * adjust                'HL7, Batch, DCC, etc.
    grdResults.Columns(COL_TYPE).width = 1000 * adjust                  'startup/info/error, etc.
    grdResults.Columns(COL_DESCRIPTION).width = 4500 * adjust           'what happened
    grdResults.Columns(COL_SOURCE_TEXT).width = 4500 * adjust           'HL7 message, DCC change dscription
    grdResults.Columns(COL_TCP_PORT).width = 800 * adjust
    
    g_util.CursorPop
    Exit Sub
    
errHandler:
    g_util.ThrowError "LoadEvents"
    Resume  'debug
End Sub

Private Function BuildSQL(caption As String) As String
    Dim sql As String

    caption = caption & " "

    sql = "select "
    If abrMain.Tools("miTop100").Checked Then
        caption = caption & "(top 250)"
        sql = sql & "top 250"                       'make it 250 instead of 100
    End If
    'Be sure to include milliseconds for proper ordering (for batch import)
    '* The source_text is a "text" data type; the grid blows up if this is > 64K chars.
    '  By calling substring we convert to varchar.  (now the grid clips to 8K no matter what, but that's OK)
    sql = sql & " convert(char(22),log.timestamp,121) as 'Timestamp'"               'yyyy-mm-dd hh:nn:ss.ss
    sql = sql & ",event_source as 'Source Code'"
    sql = sql & ",src.lookup_description as Source"
    sql = sql & ",typ.lookup_description as Type"
    sql = sql & ",cat.lookup_description as Category"
    sql = sql & ",log.description as Description"
    sql = sql & ",substring(log.source_text, 1, 32767) as 'Source Message/Text'"    '* see explanation above
    sql = sql & ",tcp_port as 'TCP/Batch'"
    sql = sql & ",vuf.unit_facility_name as 'Unit'"
    sql = sql & ",encounter_id as 'Encounter ID'"

    sql = sql & " from event_log as log" & _
        " left join lookup_value as src on (src.lookup_definition_key=66) and (src.lookup_code=log.event_source)" & _
        " left join lookup_value as typ on (typ.lookup_definition_key=67) and (typ.lookup_code=log.event_type)" & _
        " left join lookup_value as cat on (cat.lookup_definition_key=68) and (cat.lookup_code=log.event_category)" & _
        " left join view_unit_facility as vuf on (vuf.unit_id = log.unit_id)"
    
    sql = sql & " where 1=1"
    
    If m_filter.Today Then
        caption = caption & "(today)"
        sql = sql & " and timestamp >= " & g_dbutil.SQL_Date(Date)
    Else
        If m_filter.UseStartDate Then
            sql = sql & " and timestamp >= " & g_dbutil.SQL_DateTime(m_filter.StartDateTime)
        End If
        If m_filter.UseFinishDate Then
            sql = sql & " and timestamp <= " & g_dbutil.SQL_DateTime(m_filter.FinishDateTime)
        End If
    End If
    
    If m_filter.Source <> "" Then
        sql = sql & " and log.event_source=" & m_filter.Source
    End If
    If m_filter.TypeIDs <> "" Then
        sql = sql & " and log.event_type in (" & m_filter.TypeIDs & ")"
    End If
    If m_filter.Category <> "" Then
        sql = sql & " and log.event_category=" & m_filter.Category
    End If
    If m_filter.Description <> "" Then
        sql = sql & " and log.description like " & g_dbutil.SQL_String("%" & m_filter.Description & "%")
    End If
    If m_filter.SourceText <> "" Then
        sql = sql & " and log.source_text like " & g_dbutil.SQL_String("%" & m_filter.SourceText & "%")
    End If
    If m_filter.TCPPort <> 0 Then
        sql = sql & " and log.tcp_port=" & m_filter.TCPPort
    End If
    If m_filter.UnitID <> 0 Then
        sql = sql & " and log.unit_id=" & m_filter.UnitID
    End If
    If m_filter.encounter_id <> 0 Then
        sql = sql & " and log.encounter_id=" & m_filter.encounter_id
    End If
    
    If abrMain.Tools("miNewestFirst").Checked Then
        sql = sql & " order by timestamp desc"
    Else
        sql = sql & " order by timestamp"
    End If

    BuildSQL = sql
End Function

Private Sub ShowEventDetail()
    On Error GoTo errHandler
    Dim result As String, sql As String
    Dim rs As New Recordset
    Dim enc_id As Long
    
    If (grdResults.row < 0) Then Exit Sub
    
    result = grdResults.Columns(COL_TIMESTAMP).value & " " & _
        grdResults.Columns(COL_SOURCE).value & " " & _
        grdResults.Columns(COL_TYPE).value & " " & _
        grdResults.Columns(COL_CATEGORY).value & vbCrLf
    result = result & vbCrLf
    result = result & grdResults.Columns(COL_DESCRIPTION).value & vbCrLf
    result = result & vbCrLf
    result = result & grdResults.Columns(COL_SOURCE_TEXT).value & vbCrLf
    
    enc_id = GetSelectedEncounterID()
    If (enc_id > 0) Then
        sql = "select p.last_name, p.first_name, e.acct_number from encounter as e inner join person as p on (p.person_id = e.person_id) where encounter_id=" & enc_id
        rs.Open sql, g_cnADO, adOpenStatic
        If Not rs.eof Then
            result = result & vbCrLf
            result = result & "Patient " & rs("last_name") & ", " & rs("first_name") & "  acct=" & rs("acct_number")
        End If
        rs.Close
    End If

    frmPrint.SetText result
    frmPrint.show vbModal, Me
    Exit Sub
    
errHandler:
    g_util.ThrowError "ShowEventDetail"
    Resume  'debug
End Sub


Private Sub ShowRelatedMessages()
    On Error GoTo errHandler
    Dim sql As String, result As String
    Dim rs As New Recordset

    '
    ' Show all messages with the same source text
    ' (handy for HL7)
    '
    g_util.CursorPushHourglass

    sql = "select timestamp, typ.lookup_description as type_desc, description" & _
        " from event_log as log" & _
        " left join lookup_value as typ on (typ.lookup_definition_key=67) and (typ.lookup_code=log.event_type)" & _
        " where source_text like" & g_dbutil.SQL_String(left$(grdResults.Columns(COL_SOURCE_TEXT).value, 256) & "%") & _
        " order by timestamp"
    Debug.Print sql
    rs.Open sql, g_cnADO, adOpenStatic

    Do While Not rs.eof
        result = result & rs("timestamp") & " "
        result = result & "[" & left$(rs("type_desc"), 1) & "] "                '[E], etc.
        result = result & rs("description") & vbCrLf
        rs.MoveNext
    Loop

    rs.Close
    
    g_util.CursorPop
    
    frmPrint.SetText result
    frmPrint.show vbModal, Me
    Exit Sub
    
errHandler:
    g_util.ThrowError "ShowRelatedMessages"
    Resume  'debug
End Sub


' HL7 messages have <cr> line endings; change to CR/LF for windows
' Print a segment on new line
Private Function HL7PrettyPrint(msg As Variant) As String
    Dim result As String
    If IsNull(msg) Then Exit Function
    
    If InStr(1, msg, vbCrLf) > 1 Then               'already has cr/lf?  leave it alone
        result = msg
    Else
        result = Replace$(msg, vbCr, vbCrLf)        'replace cr with cr/lf
    End If
    
    If Asc(Right$(result, 1)) = 11 Then
        result = left(result, Len(result) - 1)
    End If
    If (Right$(result, 2) = vbCrLf) Then
        result = left$(result, Len(result) - 2)
    End If
    
    HL7PrettyPrint = result
End Function

Private Sub ShowEncounterLog(Source As EventLogSource, with_source_text As Boolean)
    On Error GoTo errHandler
    Dim sql As String, result As String
    Dim rs As New Recordset

    '
    ' Show the complete log for an encounter for the given interface
    '
    g_util.CursorPushHourglass

    sql = "select timestamp, typ.lookup_description as type_desc, description,event_category, source_text" & _
        " from event_log as log" & _
        " left join lookup_value as typ on (typ.lookup_definition_key=67) and (typ.lookup_code=log.event_type)" & _
        " where encounter_id=" & GetSelectedEncounterID() & _
        " and event_source=" & Source & _
        " order by timestamp"
    Debug.Print sql
    rs.Open sql, g_cnADO, adOpenStatic

    Do While Not rs.eof
        result = result & rs("timestamp") & " "
        result = result & "[" & left$(rs("type_desc"), 1) & "] "                '[E], etc.
        result = result & rs("description") & vbCrLf
        
        'Show the HL7/batch source only on the final "processed" message
        If (with_source_text) And (rs("event_category") = EVENT_CATEGORY_PROCESSED) Then
            result = result & HL7PrettyPrint(rs("source_text")) & vbCrLf
        End If
        rs.MoveNext
    Loop

    rs.Close
    
    g_util.CursorPop
    
    frmPrint.SetText result
    frmPrint.show vbModal, Me
    Exit Sub
    
errHandler:
    g_util.ThrowError "ShowEncounterLog"
    Resume  'debug
End Sub


Private Sub ShowEncounterRawHL7()
    On Error GoTo errHandler
    Dim sql As String, result As String
    Dim rs As New Recordset

    '
    ' Show the complete HL7 for an encounter; package with HL7 control chars
    '
    g_util.CursorPushHourglass

    'Get "processed" events only - ignore warnings and errors which may have
    'duplicate copies of the HL7 message.
    sql = "select source_text from event_log" & _
        " where encounter_id=" & GetSelectedEncounterID() & _
        " and event_source=" & EVENT_SOURCE_HL7 & _
        " and event_category=" & EVENT_CATEGORY_PROCESSED & _
        " and source_text is not null" & _
        " order by timestamp"
    Debug.Print sql
    rs.Open sql, g_cnADO, adOpenStatic

    Do While Not rs.eof
        result = result & chr(&HB)      'start block
        result = result & HL7PrettyPrint(rs("source_text"))
        result = result & chr(&HD)      'end block
        result = result & chr(&H1C)     'end data
        'result = result & vbCrLf       'double space
        rs.MoveNext
    Loop

    rs.Close
    
    g_util.CursorPop
    
    frmPrint.SetText result
    frmPrint.show vbModal, Me
    Exit Sub
    
errHandler:
    g_util.ThrowError "ShowEncounterLog"
    Resume  'debug
End Sub

Private Sub ParseHL7Message(msg As Variant, MSH() As String, segments() As String, field_separator As String)
    On Error Resume Next
    Dim n As Integer, i As Integer
   
    field_separator = Mid$(msg, 4, 1)                           'get the field separator
    
    '<cr> is the line separator
    n = g_util.SplitTextOnChar(msg, vbCr, segments(), 1, 4)     'get the segments
    
    'some messages have cr/lf; strip the lf
    For i = 1 To n
        If (left$(segments(i), 1) = vbLf) Then
            segments(i) = Mid$(segments(i), 2)
        End If
    Next i

    'get the message header
    n = g_util.SplitTextOnChar(segments(1), field_separator, MSH(), 0, 14)
End Sub

Private Sub ShowEncounterADT()
    On Error GoTo errHandler
    Dim sql As String, result As String, sep As String
    Dim prev_loc As String, curr_loc As String, prev_status As String, curr_status As String
    Dim MSH() As String, segments() As String, PV1() As String
    Dim cn As Connection
    Dim rs As New Recordset

    Set cn = g_dbutil.NewRemoteConnection()
    '
    ' Show the ADT parts of an encounter log.
    ' Strip messages down to just the ADT portion.
    '
    g_util.CursorPushHourglass

    'Get "processed" events only - ignore warnings and errors which may have
    'duplicate copies of the HL7 message.
    sql = "select source_text from event_log" & _
        " where encounter_id=" & GetSelectedEncounterID() & _
        " and event_source=" & EVENT_SOURCE_HL7 & _
        " and event_category=" & EVENT_CATEGORY_PROCESSED & _
        " and source_text is not null" & _
        " order by timestamp"
    Debug.Print sql
    rs.Open sql, cn, adOpenStatic

    Do While Not rs.eof
        ParseHL7Message rs("source_text"), MSH(), segments(), sep
        
        If (left$(MSH(8), 3) = "ADT") Then
            g_util.SplitTextOnChar segments(4), sep, PV1(), 0, 3
            curr_status = PV1(2)
            curr_loc = PV1(3)
            If (prev_status = "") Then prev_status = curr_status
            If (prev_loc = "") Then prev_loc = curr_loc
            
        
            result = result & segments(1) & vbCrLf          'MSH
            result = result & segments(2) & vbCrLf          'EVN
            result = result & segments(3) & vbCrLf          'PID
            result = result & segments(4) & vbCrLf          'PV1
            If (curr_status <> prev_status) Then
                result = result & "ZZZ|** status change" & vbCrLf
            End If
            If (curr_loc <> prev_loc) Then
                result = result & "ZZZ|** location change" & vbCrLf
            End If
            result = result & vbCrLf
        End If
        
        prev_status = curr_status
        prev_loc = curr_loc
        rs.MoveNext
    Loop

    rs.Close
    cn.Close
    
    If Len(result) = 0 Then
        result = "no ADT(?)"
    End If
    
    g_util.CursorPop
    
    frmPrint.SetText result
    frmPrint.show vbModal, Me
    Exit Sub
    
errHandler:
    g_util.ThrowError "ShowEncounterADT"
    Resume  'debug
End Sub

Private Function IsAChartSegment(segment As String) As Boolean
    Select Case left$(segment, 3)
        Case "OBR", "OBX", "ORC", "ZCH"
            IsAChartSegment = True
            Exit Function
    End Select
End Function

Private Function IsAnOrder(segment As String) As Boolean
    Select Case left$(segment, 3)
        Case "ORC"
            IsAnOrder = True
            Exit Function
    End Select
End Function

Private Function HasChartSegment(segments() As String) As Boolean
    Dim i As Integer
    
    For i = 1 To UBound(segments)
        If IsAChartSegment(segments(i)) Then
            HasChartSegment = True
            Exit Function
        End If
    Next i

End Function

Private Function HasAnOrder(segments() As String) As Boolean
    Dim i As Integer
    
    For i = 1 To UBound(segments)
        If IsAnOrder(segments(i)) Then
            HasAnOrder = True
            Exit Function
        End If
    Next i

End Function

Private Sub ShowEncounterChartWithOptions(orders_only As Boolean)
    On Error GoTo errHandler
    Dim sql As String, result As String, sep As String
    Dim MSH() As String, segments() As String
    Dim rs As New Recordset
    Dim show_pid As Boolean, show_it As Boolean
    Dim i As Integer

    '
    ' Show the ADT parts of an encounter log
    '
    g_util.CursorPushHourglass

    'Get "processed" events only - ignore warnings and errors which may have
    'duplicate copies of the HL7 message.
    sql = "select source_text from event_log" & _
        " where encounter_id=" & GetSelectedEncounterID() & _
        " and event_source=" & EVENT_SOURCE_HL7 & _
        " and event_category=" & EVENT_CATEGORY_PROCESSED & _
        " and source_text is not null" & _
        " order by timestamp"
    Debug.Print sql
    rs.Open sql, g_cnADO, adOpenStatic

    show_pid = True                             'show the PID once because it stays the same

    Do While Not rs.eof
        ParseHL7Message rs("source_text"), MSH(), segments(), sep
        
        If (orders_only) Then
            show_it = HasAnOrder(segments)
        Else
            show_it = HasChartSegment(segments)
        End If
        
        If (show_it) Then
            result = result & segments(1) & vbCrLf                      'MSH
            
            For i = 1 To UBound(segments)
                If show_pid And left$(segments(i), 3) = "PID" Then
                    result = result & segments(i) & vbCrLf
                    show_pid = False
                End If
                
                ' Show all charting around the order (not just orders in orders)
                If IsAChartSegment(segments(i)) Then
                    result = result & segments(i) & vbCrLf
                End If
            Next i
            
            result = result & vbCrLf
        End If
        
        rs.MoveNext
    Loop

    rs.Close
    
    If Len(result) = 0 Then
        If (orders_only) Then
            result = "no orders"
        Else
            result = "no charting"
        End If
    End If
    
    g_util.CursorPop
    
    frmPrint.SetText result
    frmPrint.show vbModal, Me
    Exit Sub
    
errHandler:
    g_util.ThrowError "ShowEncounterChartWithOptions"
    Resume  'debug
End Sub

Private Sub ShowEncounterChart()
    ShowEncounterChartWithOptions False
End Sub

Private Sub ShowEncounterOrders()
    ShowEncounterChartWithOptions True
End Sub

Private Sub ShowInterfaceOrImportLog(Source As EventLogSource, tcp_port As Long)
    On Error GoTo errHandler
    Dim sql As String, result As String, type_desc1 As String
    Dim rs As New Recordset

    '
    ' Show the import log for one TCP port or batch number
    ' (The TCP_Port column holds the batch number for imports)
    '
    g_util.CursorPushHourglass

    sql = "select timestamp, typ.lookup_description as type_desc, description, source_text" & _
        " from event_log as log" & _
        " left join lookup_value as typ on (typ.lookup_definition_key=67) and (typ.lookup_code=log.event_type)" & _
        " where event_source=" & Source
        
     If m_filter.Today Then
        caption = caption & "(today)"
        sql = sql & " and timestamp >= " & g_dbutil.SQL_Date(Date)
    Else
        If m_filter.UseStartDate Then
            sql = sql & " and timestamp >= " & g_dbutil.SQL_DateTime(m_filter.StartDateTime)
        End If
        If m_filter.UseFinishDate Then
            sql = sql & " and timestamp <= " & g_dbutil.SQL_DateTime(m_filter.FinishDateTime)
        End If
    End If
    
    sql = sql & " and   tcp_port=" & tcp_port
    
    Debug.Print sql
    rs.Open sql, g_cnADO, adOpenStatic

    If rs.RecordCount() > 1000 Then
       Dim response As Integer
        response = MsgBox("The report contains more than 1000 records. Would you want to proceed?", vbYesNo + vbQuestion, "Confirmation")
        If (response = vbNo) Then
            rs.Close
            g_util.CursorPop
            Exit Sub
        End If
    End If

    Do While Not rs.eof
        type_desc1 = left$(rs("type_desc"), 1)
        result = result & rs("timestamp") & " "
        result = result & "[" & type_desc1 & "] "                '[E], etc.
        result = result & rs("description")
        If (type_desc1 = "E") Then
            If (Right$(result, 1) = ".") Then result = left$(result, Len(result) - 1)
            result = result & ": " & vbTab & rs("source_text")
        End If
        result = result & vbCrLf
        rs.MoveNext
    Loop

    rs.Close
    
    g_util.CursorPop
    
    frmPrint.SetText result
    frmPrint.show vbModal, Me
    Exit Sub
    
errHandler:
    g_util.ThrowError "ShowInterfaceOrImportLog"
    Resume  'debug
End Sub

