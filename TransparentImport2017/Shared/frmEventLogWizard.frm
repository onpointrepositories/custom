VERSION 5.00
Begin VB.Form frmEventLogWizard 
   Caption         =   "Event Log Filter Wizard"
   ClientHeight    =   7290
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   5655
   LinkTopic       =   "Form1"
   ScaleHeight     =   7290
   ScaleWidth      =   5655
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox txtAcct 
      Height          =   315
      Left            =   2880
      TabIndex        =   26
      Top             =   6240
      Width           =   2655
   End
   Begin VB.Frame Frame3 
      Caption         =   "Timeframe"
      Height          =   1275
      Left            =   120
      TabIndex        =   17
      Top             =   4860
      Width           =   5415
      Begin VB.PictureBox Picture3 
         BorderStyle     =   0  'None
         Height          =   975
         Left            =   120
         ScaleHeight     =   975
         ScaleWidth      =   5175
         TabIndex        =   22
         Top             =   180
         Width           =   5175
         Begin VB.OptionButton optTimeframe 
            Caption         =   "All events in the log"
            Height          =   255
            Index           =   0
            Left            =   0
            TabIndex        =   18
            Top             =   60
            Width           =   5055
         End
         Begin VB.TextBox txtDayCount 
            Alignment       =   2  'Center
            Height          =   315
            Left            =   840
            TabIndex        =   23
            Text            =   "7"
            Top             =   660
            Width           =   615
         End
         Begin VB.OptionButton optTimeframe 
            Caption         =   "Last"
            Height          =   255
            Index           =   2
            Left            =   0
            TabIndex        =   21
            Top             =   660
            Width           =   855
         End
         Begin VB.OptionButton optTimeframe 
            Caption         =   "Today"
            Height          =   255
            Index           =   1
            Left            =   0
            TabIndex        =   19
            Top             =   360
            Width           =   5055
         End
         Begin VB.Label Label1 
            Caption         =   "days"
            Height          =   315
            Left            =   1560
            TabIndex        =   24
            Top             =   660
            Width           =   615
         End
      End
   End
   Begin VB.Frame Frame2 
      Caption         =   "Event Type"
      Height          =   1815
      Left            =   120
      TabIndex        =   10
      Top             =   2940
      Width           =   5415
      Begin VB.PictureBox Picture2 
         BorderStyle     =   0  'None
         Height          =   1515
         Left            =   120
         ScaleHeight     =   1515
         ScaleWidth      =   5175
         TabIndex        =   20
         Top             =   240
         Width           =   5175
         Begin VB.OptionButton optType 
            Caption         =   "Information"
            Height          =   315
            Index           =   1
            Left            =   0
            TabIndex        =   14
            Top             =   1200
            Width           =   5175
         End
         Begin VB.OptionButton optType 
            Caption         =   "Reject"
            Height          =   315
            Index           =   4
            Left            =   0
            TabIndex        =   13
            Top             =   900
            Width           =   5175
         End
         Begin VB.OptionButton optType 
            Caption         =   "Warning"
            Height          =   315
            Index           =   2
            Left            =   0
            TabIndex        =   12
            Top             =   600
            Width           =   5175
         End
         Begin VB.OptionButton optType 
            Caption         =   "Error"
            Height          =   315
            Index           =   3
            Left            =   0
            TabIndex        =   11
            Top             =   300
            Width           =   5175
         End
         Begin VB.OptionButton optType 
            Caption         =   "All"
            Height          =   255
            Index           =   0
            Left            =   0
            TabIndex        =   9
            Top             =   0
            Width           =   5175
         End
      End
   End
   Begin VB.Frame Frame1 
      Caption         =   "Event Source"
      Height          =   2775
      Left            =   120
      TabIndex        =   0
      Top             =   60
      Width           =   5415
      Begin VB.PictureBox Picture1 
         BorderStyle     =   0  'None
         Height          =   2535
         Left            =   120
         ScaleHeight     =   2535
         ScaleWidth      =   5175
         TabIndex        =   15
         Top             =   180
         Width           =   5175
         Begin VB.OptionButton optSource 
            Caption         =   "Data Consistency Check"
            Height          =   315
            Index           =   7
            Left            =   0
            TabIndex        =   8
            Top             =   2160
            Width           =   5100
         End
         Begin VB.OptionButton optSource 
            Caption         =   "Transparent Import"
            Height          =   315
            Index           =   6
            Left            =   0
            TabIndex        =   7
            Top             =   1860
            Width           =   5100
         End
         Begin VB.OptionButton optSource 
            Caption         =   "Transparent Mapping"
            Height          =   315
            Index           =   5
            Left            =   0
            TabIndex        =   6
            Top             =   1560
            Width           =   4860
         End
         Begin VB.OptionButton optSource 
            Caption         =   "Report Calculations"
            Height          =   315
            Index           =   4
            Left            =   0
            TabIndex        =   5
            Top             =   1260
            Width           =   5100
         End
         Begin VB.OptionButton optSource 
            Caption         =   "Batch Export"
            Height          =   315
            Index           =   3
            Left            =   0
            TabIndex        =   4
            Top             =   960
            Width           =   5175
         End
         Begin VB.OptionButton optSource 
            Caption         =   "HL7"
            Height          =   315
            Index           =   1
            Left            =   0
            TabIndex        =   2
            Top             =   360
            Width           =   5175
         End
         Begin VB.OptionButton optSource 
            Caption         =   "Batch Import"
            Height          =   315
            Index           =   2
            Left            =   0
            TabIndex        =   3
            Top             =   660
            Width           =   5175
         End
         Begin VB.OptionButton optSource 
            Caption         =   "(All)"
            Height          =   315
            Index           =   0
            Left            =   0
            TabIndex        =   1
            Top             =   60
            Width           =   5175
         End
         Begin VB.OptionButton Option1 
            Caption         =   "Any Interface"
            Height          =   315
            Left            =   0
            TabIndex        =   16
            Top             =   60
            Width           =   5175
         End
      End
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   375
      Left            =   3000
      TabIndex        =   28
      Top             =   6780
      Width           =   1215
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   375
      Left            =   4320
      TabIndex        =   29
      Top             =   6780
      Width           =   1215
   End
   Begin VB.CommandButton cmdApply 
      Caption         =   "Apply"
      Height          =   375
      Left            =   1680
      TabIndex        =   27
      Top             =   6780
      Width           =   1215
   End
   Begin VB.Label Label2 
      Caption         =   "Patient Name or Acct#"
      Height          =   315
      Left            =   120
      TabIndex        =   25
      Top             =   6300
      Width           =   2535
   End
End
Attribute VB_Name = "frmEventLogWizard"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'
' Event Log Wizard - quick set events filters
'

Private m_parent        As frmEventLogViewer
Private m_filter        As PFSEventLogFilter

Private m_source        As Integer
Private m_event_type    As Integer
Private m_timeframe     As Integer

Private Const TIMEFRAME_ALL = 0
Private Const TIMEFRAME_TODAY = 1
Private Const TIMEFRAME_LAST_X = 2


Public Sub Setup(parent As frmEventLogViewer, filter As PFSEventLogFilter)
    'save pointers to the parent form for Apply button
    Set m_parent = parent
    Set m_filter = filter
    
    'reset the form (ignore the current filter)
    optSource(0).value = True
    optType(0).value = True
    optTimeframe(TIMEFRAME_ALL).value = True
    txtAcct.Text = ""
    
    'Set the form from the filter
    If (m_filter.Today) Then optTimeframe(TIMEFRAME_TODAY).value = True
End Sub

Private Sub Form_Load()
    g_display.SetStdTheme Me, True
    
    Me.caption = App.ProductName & " Events Filter Wizard"
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set m_parent = Nothing
End Sub

Private Sub optSource_Click(Index As Integer)
    m_source = Index
End Sub

Private Sub optType_Click(Index As Integer)
    m_event_type = Index
End Sub

Private Sub optTimeframe_Click(Index As Integer)
    m_timeframe = Index
End Sub

Private Sub cmdOK_Click()
    cmdApply_Click
    Unload Me
End Sub

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdApply_Click()
    SetFilterFromForm
    m_parent.LoadEvents
End Sub

Private Sub SetFilterFromForm()
    On Error GoTo errHandler
    Dim type_ids As String
    
    'set defaults to get all events
    m_filter.TypeIDs = ""
    m_filter.Source = ""
    m_filter.Category = ""
    m_filter.Description = ""
    m_filter.SourceText = ""
    m_filter.UnitID = 0
    m_filter.TCPPort = 0
    m_filter.encounter_id = 0
    m_filter.Today = False
    m_filter.UseStartDate = False
    m_filter.UseFinishDate = False
    m_filter.StartDateTime = Date           'midnight this morning
    m_filter.FinishDateTime = Now

    'Now look at the option buttons
    If (m_source <> 0) Then
        m_filter.Source = m_source
    End If
    
    If (m_event_type <> 0) Then
        m_filter.TypeIDs = m_event_type
    End If
    
    Select Case m_timeframe
    Case TIMEFRAME_ALL
        '
    Case TIMEFRAME_TODAY
        m_filter.Today = True
    Case TIMEFRAME_LAST_X
        m_filter.UseStartDate = True
        m_filter.StartDateTime = Date - g_dbutil.DBToInteger(txtDayCount.Text)
    End Select

    m_filter.SourceText = txtAcct.Text

    Exit Sub
    
errHandler:
    g_util.MsgBoxError
    Exit Sub
    Resume  'debug
End Sub

