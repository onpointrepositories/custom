VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PFSGridUtility"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'
' TDBGridUtil: True DBGrid Utilities
'
Private m_shift As Integer


' You can't just set the number of columns that you want to have:
' You have to add or remove columns from the Columns collection.
'
Public Sub SetColumnCount(ByRef grd As TDBGrid, ByVal NCOLS As Long)
    Dim C As TrueOleDBGrid60.Column
    Dim save_left_col As Integer
    
    If (grd.Columns.count = NCOLS) Then Exit Sub

    save_left_col = grd.LeftCol
    'You can add new colummns without error but unless you delete all of them first,
    'the new columns will disappear when you ReBind!
    
    'Remove all existing colummns
    'Sometimes the remove command fails even though there are still columns in the grid.
    On Error Resume Next                            'ignore errors
    Do While (grd.Columns.count > 0)
        grd.Columns.Remove 0
        If (Err.Number <> 0) Then Exit Do           'avoid infinite loop on error
    Loop
    
    'Add missing columns
    Do While grd.Columns.count < NCOLS
        Set C = grd.Columns.Add(0)
        C.Visible = True                            'new columns are hidden by default
    Loop

    'Restore the left column (if valid).  The grid was scrolling to col 2 for some reason.
    On Error Resume Next
    grd.LeftCol = save_left_col
End Sub

Public Function IsColumnVisible(ByRef grd As TDBGrid, ByVal col As Long) As Boolean
    On Error GoTo errHandler
    IsColumnVisible = (col >= grd.LeftCol) And (col < grd.LeftCol + grd.VisibleCols)
    
    If IsColumnVisible Then
        'Technically the column is visible even if is just a sliver.  We want the entire column.
        IsColumnVisible = (grd.Columns(col).left + grd.Columns(col).width <= grd.width)
    End If
    Exit Function
errHandler:
    'We are getting mystery "overflow" errors here.  After a breakpoint/resume, there is no error.
      'g_util.ThrowError "IsColumnVisible"
      'Resume  'debug
    IsColumnVisible = True
End Function

Public Function IsRowVisible(ByRef grd As TDBGrid, ByVal row As Long) As Boolean
    IsRowVisible = (row >= grd.FirstRow) And (row <= grd.FirstRow + grd.VisibleRows)
End Function

'
' Map Xarray (absolute) row/cols to visible (scrolling) grids
'
Public Sub MakeXarrayRowVisible(ByRef grd As TDBGrid, ByVal row As Long)
    If IsRowVisible(grd, row) Then Exit Sub
    grd.FirstRow = row
End Sub

Public Sub MakeXarrayColVisible(ByRef grd As TDBGrid, ByVal col As Long)
    If IsColumnVisible(grd, col) Then Exit Sub
    grd.LeftCol = col
End Sub

Public Function XarrayRowForGridRow(ByRef grd As TDBGrid, ByVal row As Long) As Long
    XarrayRowForGridRow = grd.FirstRow + row
End Function

Public Function GridRowForXarrayRow(ByRef grd As TDBGrid, ByVal row As Long) As Long
    GridRowForXarrayRow = row - grd.FirstRow
End Function

Public Function GridColForXarrayCol(ByRef grd As TDBGrid, ByVal col As Long) As Long
    GridColForXarrayCol = col - grd.LeftCol
End Function

Public Sub SetFocusForXarrayRowCol(ByRef grd As TDBGrid, ByVal row As Long, ByVal col As Long)
    MakeXarrayRowVisible grd, row
    MakeXarrayColVisible grd, col
    
    grd.row = GridRowForXarrayRow(grd, row)
    grd.col = GridColForXarrayCol(grd, col)
End Sub


'
' Call this from the grid_LooseFocus method to accept the edit in progress.
' (The user may edit a value and then click on a Save button.)
'
' NOTE: The Update method doesn't get the edited value for the current column.
'
Public Sub AcceptEdits(ByRef grd As TDBGrid)
    On Error GoTo errHandler
    Dim col As Integer

'    Debug.Print "AcceptEdits, row="; grd.row
    
    If Not grd.DataChanged Then
        Debug.Print "no changes"
        Exit Sub
    End If

    'Move the cursor to finalize the edit (move to col 0 or 1)
    col = grd.col
    grd.col = IIf(col > 0, 0, IIf(grd.Columns.count > 1, 1, 0))
    'Save the changes
    grd.Update
    'Restore the cursor
    grd.col = col
    Exit Sub
    
errHandler:
    g_util.MsgBoxError "in AcceptEdits"
    Exit Sub
    Resume
End Sub


'
' Call this from the grd.MouseDown() event to support multiple row selection
'
' The RowColChange() event doesn't give you the shift key state so we
' have to get it from MouseDown and save it.
'
' The grid has not updated the Row & Col at this point -- we have to wait
' for RowColChange.
'
Public Sub SelectMouseDown(ByVal Shift As Integer)
    m_shift = Shift
End Sub

'
' Call this from the grd.RowColChange() event to support multiple row selection
'
' Allow the user to select a row by clicking anywhere within the row.
' Supports shift and ctrl-click as long as SelectMouseDown() is called first.
'
' Note: ctrl-click does not appear to work only because the grid is clearing the
' previous selection before this is called.
'
Public Sub SelectRowColChange(ByRef grd As TDBGrid)
    Static prev_row As Integer
    Static ignore_change As Boolean
    Dim row As Integer, curr_row As Integer, updown As Integer

    If (ignore_change) Then Exit Sub

    curr_row = grd.row
    If (curr_row < 0) Then Exit Sub         'no current row?
    
    If (m_shift = 0) Then
        'select current row only
        DeselectAllRows grd
        prev_row = curr_row
    ElseIf (m_shift And vbShiftMask) Then
        'leave prev_row alone to select all rows between
    ElseIf (m_shift And vbCtrlMask) Then
        'add current but leave previous rows selected
        prev_row = curr_row
    End If
    
    'note: this loop will count up or down because curr_row can be less than prev
    updown = Sgn(curr_row - prev_row)
    If (updown = 0) Then updown = 1
    For row = prev_row To curr_row Step updown
        AddSelectedRow grd, row
    Next row
    
    'If the grid gets rebuilt this routine will be called without a MouseDown
    'so m_shift and prev_row could be invalid.  Reset the shift state now
    'so a shift or ctrl select will only happen after another MouseDown.
    m_shift = 0
    
    'This next section makes double click work, but it kills the cursor arrows.
    
'    'Move the cursor off of the current row so RowColChange will fire again if
'    'the user clicks on the same cell twice.  Otherwise the row will not be
'    'selected again and it will be hard to double-click.
'    ignore_change = True
'    On Error Resume Next
'    If (grd.row > 0) Then
'        grd.row = grd.row - 1
'    Else
'        grd.row = 1
'    End If
'    'Make the RowColChange event fire now, before ignore_change is reset
'    DoEvents
'    ignore_change = False
End Sub

Public Sub AddSelectedRow(ByRef grd As TDBGrid, row As Integer)
    'GetBookmark only works relative to the current row (how annoying)
    grd.SelBookmarks.Add grd.GetBookmark(row - grd.row)
End Sub


Public Sub SelectOneRow(ByRef grd As TDBGrid, row As Integer)
    DeselectAllRows grd
    AddSelectedRow grd, row
    'grd.row = row                                              'blows up with scrollbar
    grd.Bookmark = grd.SelBookmarks.item(0)                     'move the row marker [46973]
End Sub

'
' Deselect all rows in the grid
'
Public Sub DeselectAllRows(ByRef grd As TDBGrid)
    With grd.SelBookmarks
        While (.count > 0)
            .Remove 0
        Wend
    End With
End Sub

'
' Select all rows in the grid
'
' Bugs:
'   The grid does not seem to be willing to get bookmarks for any rows prior to
'   the current row if the current row is above and off-screen.
'
Public Sub SelectAllRows(ByRef grd As TDBGrid)
    Dim row As Integer
    
    DeselectAllRows grd

    For row = 0 To grd.ApproxCount - 1
        AddSelectedRow grd, row
    Next row
End Sub


'
' Freeze the first n columns in the grid when side-scrolling
'
Public Sub FreezeColumns(ByRef grd As TDBGrid, ByVal UpToCol As Integer)
    On Error GoTo errHandler
    Dim s As TrueOleDBGrid60.Split
    Dim i As Integer, NumVisible As Integer

    If (grd.Splits.count < 2) Then
        'Split the grid into two views.
        Set s = grd.Splits.Add(0)
    End If
        
    'LEFT SIDE
    With grd.Splits(0)
        'Size the left split to the number of visible columns for that side
        NumVisible = 0
        For i = 0 To UpToCol
            If .Columns(i).Visible Then
                NumVisible = NumVisible + 1
            End If
        Next i

        .SizeMode = dbgNumberOfColumns
        .Size = NumVisible
        
        'Hide the columns after the frozen one
        For i = UpToCol + 1 To .Columns.count - 1
            .Columns(i).Visible = False
            .Columns(i).AllowSizing = False
        Next i
    End With

    'RIGHT SIDE
    With grd.Splits(1)
        'Hide the record selector column
        .RecordSelectors = False

        'Hide the frozen columns in the left split
        For i = 0 To UpToCol
            .Columns(i).Visible = False
            .Columns(i).AllowSizing = False
        Next i
    End With
    
    'All the user to tab from one side to the other
    grd.TabAcrossSplits = True
    
    'Set the current split to the right side
    '(without this, column sizing will not be enabled)
    grd.Split = 1
    Exit Sub
    
errHandler:
    g_util.ThrowError "FreezeColumns"
    Resume
End Sub


Public Sub UnfreezeColumns(ByRef grd As TDBGrid)
    On Error GoTo errHandler

    Do While (grd.Splits.count > 1)
        grd.Splits.Remove 0
    Loop
    Exit Sub
    
errHandler:
    g_util.ThrowError "UnfreezeColumns"
    Resume
End Sub

'
' Load a drop-down list for a grid column
'
Public Sub LoadDropdownList(ByRef grd As TDBGrid, col As Integer, sql As String, Optional add_empty_choice As Boolean = False)
    On Error GoTo errHandler
    
    Dim rs As New Recordset
    
    'Open the list of codes and descriptions
    rs.Open sql, g_cnADO, adOpenStatic
    
    With grd.Columns(col).ValueItems
        'Reset the drop-down list
        .Clear
    
        If add_empty_choice Then
            'Null causes an error, so use an empty string
            .Add NewValueItem(0, "")
        End If
        
        Do While Not rs.eof
            .Add NewValueItem(rs(0), rs(1))
            rs.MoveNext
        Loop
        
        .Translate = True
        
        'This isn't appropriate here, but the caller can set how tall the pulldown is like this:
        '.MaxComboItems = 10
    End With
        
    rs.Close
    Exit Sub
    
errHandler:
    g_util.ThrowError "LoadDropdownList"
End Sub

Private Function SelectCodeDescSQL(show_code_and_description As Boolean) As String
    Dim sql As String
    If (show_code_and_description) Then
        sql = " SELECT LOOKUP_CODE, LOOKUP_CODE + ' - ' + LOOKUP_DESCRIPTION"
    Else
        sql = " SELECT LOOKUP_CODE, LOOKUP_DESCRIPTION"
    End If
    SelectCodeDescSQL = sql
End Function

Public Sub LoadDropdownListFromLVC(ByRef grd As TDBGrid, col As Integer, lvc_key As LookupDefinitionKeys, Optional add_empty_choice As Boolean = False, Optional show_code_and_description As Boolean = False)
    Dim sql As String
    sql = SelectCodeDescSQL(show_code_and_description) & " FROM LOOKUP_VALUE WHERE LOOKUP_DEFINITION_KEY = " & lvc_key
    
    LoadDropdownList grd, col, sql, add_empty_choice
End Sub

Public Sub LoadDropdownListFromLVCWithUnitGroups(ByRef grd As TDBGrid, col As Integer, lvc_key As LookupDefinitionKeys, unit_id As Long, Optional add_empty_choice As Boolean = False, Optional show_code_and_description As Boolean = False)
    Dim sql As String
    
    'Show all items without a unit-group restriction, then add all items that belong to unit groups that this unit belongs to.
    sql = _
        SelectCodeDescSQL(show_code_and_description) & ",SEQUENCE" & _
        " FROM LOOKUP_VALUE" & _
        " WHERE LOOKUP_DEFINITION_KEY=" & g_dbutil.SQL_String(lvc_key) & _
        " AND UNIT_GROUP_ID IS NULL" & _
        "" & _
        " UNION" & _
        "" & _
        SelectCodeDescSQL(show_code_and_description) & ",SEQUENCE" & _
        " FROM LOOKUP_VALUE" & _
        " WHERE LOOKUP_DEFINITION_KEY=" & g_dbutil.SQL_String(lvc_key) & _
        " AND UNIT_GROUP_ID IN (" & _
        "     SELECT UNIT_GROUP_ID FROM UNIT_GROUP_UNIT WHERE UNIT_ID=" & unit_id & _
        " )" & _
        " ORDER BY SEQUENCE"

    LoadDropdownList grd, col, sql, add_empty_choice
End Sub


Public Function NewValueItem(val As Variant, desc As String) As ValueItem
    Dim result As New ValueItem
    result.value = val
    result.DisplayValue = desc
    Set NewValueItem = result
End Function



'==============================================================================
' SAVE CONTENTS IN A FILE
' This function is complementary to the one in PFSFlxGridUtility (but it doesn't always work)
'==============================================================================
'Public Sub SaveAsCSV(grd As TDBGrid, CommonDialog1 As Control, Optional with_headers As Boolean = False)
'    On Error GoTo errHandler
'    Dim filter As String, Result As String
'    Dim row As Long, col As Long
'
'    If (with_headers) Then
'        For col = 0 To grd.Columns.count
'            If (col <> 0) Then Result = Result & ","
'            Result = Result & """" & grd.Columns(col).caption & """"
'        Next col
'    End If
'
'    MakeXarrayRowVisible grd, 0
'
'    For row = 0 To grd.ApproxCount - 1
'        MakeXarrayRowVisible grd, row                   ' NOT WORKING WITH DATA CONTROL
'        grd.row = row - grd.FirstRow                    ' ERROR HERE
'        For col = 0 To grd.Columns.count - 1
'            If (col <> 0) Then Result = Result & ","
'            Result = Result & """" & grd.Columns(col).value & """"
'        Next col
'        Result = Result & vbCrLf
'    Next row
'
'    filter = "All Files (*.*)|*.*|Comma Separated(*.csv)|*.csv|"
'
'    g_util.FileSaveAs CommonDialog1, Result, filter
'    Exit Sub
'
'errHandler:
'    g_util.MsgBoxError , "SaveAs"
'    Exit Sub
'    Resume  'debug
'End Sub

Public Sub SaveAsHTML(grd As TDBGrid, CommonDialog1 As Control)
    On Error GoTo errHandler
    Dim filter As String, result As String
    
    filter = "All Files (*.*)|*.*|Hypertext Markup(*.html)|*.html|"
    
    If g_util.FileSaveAs(CommonDialog1, result, filter) Then
        grd.ExportToFile CommonDialog1.filename, False
    End If
    Exit Sub
    
errHandler:
    g_util.MsgBoxError , "SaveAs"
    Exit Sub
    Resume  'debug
End Sub


Public Sub HideEmptyColumns(grd As TDBGrid)
    On Error GoTo errHandler
    Dim row As Long, col As Long, save_row As Long, save_col As Long, save_firstRow As Long
    Dim any_data As Boolean

    save_firstRow = grd.FirstRow
    save_row = grd.row
    save_col = grd.col

    For col = 0 To grd.Columns.count - 1
        'Debug.Print "col=" & col, grd.Columns(col).caption
        If (grd.Columns(col).Visible) Then
        
            For row = 0 To grd.ApproxCount - 1
                grd.Bookmark = row
                any_data = False
            
                If Len(grd.Columns(col).value) Then
                    any_data = True
                    Exit For
                End If
            Next row
            
            If (Not any_data) Then
                grd.Columns(col).Visible = False
                grd.Columns(col).AllowSizing = False        'keep hidden
            End If
        
        End If
    Next col
    
    grd.FirstRow = save_firstRow
    grd.row = save_row
    grd.col = save_col
    Exit Sub

errHandler:
    g_util.ThrowError "HideEmptyColumns"
    Resume  'debug
End Sub
