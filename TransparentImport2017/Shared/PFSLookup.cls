VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PFSLookup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

' PFS Lookup Class:
'
'   Uses g_app.cnADO() and g_dbutil.SQL_String()
'
' These are the lookup keys used in the LOOKUP_VALUES table.
' Do not renumber these: they mirror what is in the database.
'
' These constants be used with PFSCodeDescList.LoadLVC to load a lookup list.
'
Public Enum LookupDefinitionKeys
    LVC_ADMISSION_SOURCE = 1
    LVC_ADMISSION_TYPE = 2
    LVC_DATA_SOURCE = 3
    LVC_DATA_TYPE = 4
    LVC_DISCHARGE_STATUS = 5
    LVC_EMPLOYMENT_STATUS = 6
    LVC_ETHNICITY = 7
    LVC_FACILITY_CLASSIFICATION = 8
    LVC_FACILITY_LOCATION = 9
    LVC_FACILITY_REGION = 10
    LVC_ED_PAYOR = 11
    LVC_GUARANTOR_RELATIONSHIP = 12
    LVC_HOSPITAL_SERVICE = 13
    LVC_INSURED_RELATIONSHIP = 14
    LVC_LANGUAGE = 15
    LVC_MARITAL_STATUS = 16
    LVC_NEXT_OF_KIN_RELATIONSHIP = 17
    LVC_PERINATAL_DESIGNATION = 18
    LVC_PROVIDER_ROLE = 19
    LVC_RACE = 20
    LVC_RELIGION = 21
    LVC_SERVICE_PERFORMED = 22
    LVC_SEX = 23
    LVC_SKILL_CONSOLIDATION = 24
    LVC_SPECIALTY = 25
    LVC_STAFFING_ADJUSTMENT = 26
    LVC_STAFFING_ENTRY_FORMAT = 27
    LVC_STAFFING_JOBTITLE_ENTRY_FORMAT = 28
    LVC_STAFFING_ROUNDING = 29
    LVC_STAFFING_REPORT_FORMAT = 30
    LVC_TRAUMA_DESIGNATION = 31
    LVC_VIP_CODE = 32
    LVC_VISIT_TYPE = 33
    LVC_YES_NO = 34
    LVC_YES_NO_UNKNOWN = 35
    LVC_PATIENT_CLASS = 36
    LVC_SECURITY_PERMISSION = 37
    LVC_PFS_CLINICAL_SPECIALTY = 38
    LVC_UNIT_GROUP_TYPE_LVC = 39
    LVC_COLOR_SCHEME = 40
    LVC_AMBULATORY_PAYER = 41
    LVC_AMBULATORY_PROVIDER = 42
    LVC_DISPOSITION = 43
    LVC_AUDIT_EVENT = 44
    LVC_AUDIT_REASON = 45
    LVC_STAFFING_REASON = 46                'used in Report Notes
    LVC_RECSTAFF_SOURCE = 47
    LVC_WORKLOAD_TYPE = 48
    LVC_WORKLOAD_COLUMN_NUMBER = 49
    LVC_STAFFING_RATIO_GROUPS = 50          'RN/Other, Licensed/Other
    LVC_STAFFING_RATIO_GROUP1 = 51          'RN, Other
    LVC_STAFFING_RATIO_GROUP2 = 52          'Licensed, Other
    LVC_COVERAGE_SKILL_LEVEL = 53           'RN, Licensed
    LVC_METHODOLOGY_GROUP = 54
    LVC_FINANCIAL_CLASS = 55
    LVC_PROJECTED_WORKLOAD = 56
    LVC_TOOLBAR_ICON_SIZE = 57
    LVC_OUTCOMES_DATA_FREQUENCY = 58
    LVC_GRAPH_STYLE = 59
    LVC_GRAPH_COLOR = 60
    LVC_OUTCOMES_ROLLUP = 61
    LVC_UNIT_PRINT_OPTIONS = 62
    LVC_PROCEDURE_SKILL_OPTIONS = 63        '<skill dist>, RN, Other (could include Licensed in the future)
    LVC_DATE_MODE = 64                      '(used in staffing Other tab and staff import)
    LVC_MINSTAFF_MODE = 65                  'used in rec staffing
    LVC_EVENT_SOURCE = 66                   'HL7, batch import/export, report calcs, etc.
    LVC_EVENT_TYPE = 67                     'info/warning/error
    LVC_EVENT_CATEGORY = 68
    LVC_DATA_CONSISTENCY_CHECKS = 69
    LVC_DCC_PATIENT_CHECKS = 70
    LVC_DCC_UNIT_CHECKS = 71
    LVC_PATSEL_DISPLAY_COLUMNS = 72
    LVC_COMPETENCY = 73
    LVC_STAFF_RATIO_REASON = 74             'used in staff assignment
    LVC_BIRTHWEIGHT_CATEGORY = 75
End Enum


Public Function CodeExists(key As LookupDefinitionKeys, Code As String) As Boolean
    ' Find out if a code exists in a given list
    Dim sql As String
    Dim rs As New Recordset, cn As Connection
    
    Set cn = g_dbutil.NewRemoteConnection()
    
    sql = "SELECT LOOKUP_CODE FROM LOOKUP_VALUE " _
        & "WHERE LOOKUP_DEFINITION_KEY=" & key _
        & " AND LOOKUP_CODE=" & g_dbutil.SQL_String(Code)
    Debug.Print sql
    rs.Open sql, cn, adOpenStatic
    CodeExists = (Not rs.eof)
    rs.Close
    
    cn.Close
End Function

