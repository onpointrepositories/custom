VERSION 5.00
Begin VB.Form frmAbout 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "About"
   ClientHeight    =   4170
   ClientLeft      =   45
   ClientTop       =   345
   ClientWidth     =   8415
   ClipControls    =   0   'False
   Icon            =   "frmPFSAbout.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4170
   ScaleWidth      =   8415
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Tag             =   "About Project1"
   Begin VB.CommandButton cmdTechSupport 
      Caption         =   "Support"
      Height          =   465
      Left            =   4320
      TabIndex        =   4
      Tag             =   "&System Info..."
      Top             =   3600
      Width           =   1455
   End
   Begin VB.CommandButton cmdOK 
      Cancel          =   -1  'True
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   465
      Left            =   2640
      TabIndex        =   0
      Tag             =   "OK"
      Top             =   3600
      Width           =   1455
   End
   Begin VB.Label lblPatch 
      BackStyle       =   0  'Transparent
      Caption         =   "Patch level"
      Height          =   315
      Left            =   600
      TabIndex        =   7
      Top             =   2280
      Width           =   5655
   End
   Begin VB.Label lblModule 
      BackStyle       =   0  'Transparent
      Caption         =   "Module"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   600
      TabIndex        =   6
      Top             =   1560
      Width           =   5655
   End
   Begin VB.Label lblProductVersion 
      BackColor       =   &H00FFFFFF&
      Caption         =   "AcuityPlus™ 0.0"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   20.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00988C4F&
      Height          =   495
      Left            =   3360
      TabIndex        =   5
      Top             =   360
      Width           =   3135
   End
   Begin VB.Image Image1 
      Height          =   1020
      Left            =   120
      Picture         =   "frmPFSAbout.frx":3AFA
      Top             =   120
      Width           =   3000
   End
   Begin VB.Label lblBuild 
      BackStyle       =   0  'Transparent
      Caption         =   "Build number"
      Height          =   315
      Left            =   600
      TabIndex        =   3
      Top             =   1920
      Width           =   5655
   End
   Begin VB.Label lblServer 
      BackStyle       =   0  'Transparent
      Caption         =   "Connected to "
      Height          =   315
      Left            =   600
      TabIndex        =   2
      Tag             =   "Version"
      Top             =   2640
      Width           =   5655
   End
   Begin VB.Label lblCopyright 
      BackStyle       =   0  'Transparent
      Caption         =   "Copyright"
      Height          =   315
      Left            =   600
      TabIndex        =   1
      Top             =   3000
      Width           =   7700
   End
End
Attribute VB_Name = "frmAbout"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'
' PFSAbout: standard "About" box
' All information comes from the application properties (App object)
'
Option Explicit

Private Sub Form_Load()
    On Error GoTo errHandler
    Dim dt As Variant
    Dim msg As String, version As String

    g_display.SetStdTheme Me

    Me.caption = "About"
    
    '
    ' This is the big banner
    '
    lblProductVersion.FontName = "Arial"
    lblProductVersion.FontSize = 20
    lblProductVersion.ForeColor = &H90864B      'powerpoint title
    lblProductVersion.caption = App.ProductName & "™ " & App.major & "." & App.minor

    '
    'Now the fine print
    '
    lblModule.caption = App.title()
    lblModule.FontBold = True

    version = App.major & "." & App.minor & "." & App.revision & " build " & BUILD_NUMBER
    On Error Resume Next
    dt = FileDateTime(App.Path & "\" & App.EXEName & ".exe")
    If IsDate(dt) Then
        msg = App.EXEName & ".exe " & version & "  " & dt
    Else
        'VB dev env; EXEName is the project name, not the target .exe
        'Change WinPFS (project) to AcuityPlus (exe) for better screen shots
        msg = App.EXEName
        If (msg = "WinPFS") Then msg = "AcuityPlus"
        msg = msg & " project " & version & "  (design environment)"
    End If
    lblBuild.caption = msg
    
    lblPatch.caption = g_util.AppPatchLevel("Workstation patch level")

    lblServer.caption = "Server=" & g_dbutil.ServerName() & ", Database=" & g_dbutil.DatabaseName()
    lblCopyright.caption = App.LegalCopyright
    Exit Sub
    
errHandler:
    g_util.MsgBoxError "loading About"
End Sub

Private Sub cmdOK_Click()
    Unload Me
End Sub

Private Sub cmdTechSupport_Click()
    MsgBox "Phone:" & vbTab & "877.823.7263" & vbCrLf & _
            "", _
            , App.CompanyName & " Technical Support"
End Sub

