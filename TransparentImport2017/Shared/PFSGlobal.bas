Attribute VB_Name = "PFSGlobal"
Option Explicit
'
' PFSGlobal Module
'
' This is a collection of global constants.
'
Public Const DATABASE_VERSION = 8.703                   'The current database version
Public Const BUILD_NUMBER = 6                           'The build number within a release
             'Don't forget to change PFSGlobal.cs and DBUpgrade.cs to match these!

Public Const MAX_PATIENT_TYPE = 8                       'The highest possible patient type
Public Const MAX_COMPLEXITY_TYPE = 5                    'The highest possible complexity type
Public Const CLASSIFICATION_MAX_HOURS_INTO_FUTURE = 8   'Allow classification no more than this much into the future
Public Const PROCEDURE_MAX_HOURS_INTO_FUTURE = 4        'Allow procedure no more than this much into the future
'
' Most classifications are valid for 24 hours or less; ED may go to 48 hours;
' Perinatal may go to 96 hours.  (That's what the form allows anyway.)  There are
' Queries in report calculations that need to choose limit to optimize the query.
Public Const CLASSIFICATION_MAX_LOS_DAYS = 28
Public Const PROCEDURE_MAX_LOS_DAYS = 2

Public Const MAXINT = 32767
'
' Regional Date/Time/Number formats (actual format set in control panel)
Public Const PFS_DATE_FORMAT = "Short Date"             'mm/dd/yyyy (format selected in control panel)
Public Const PFS_LONG_DATE_FORMAT = "Long Date"
Public Const PFS_TIME_FORMAT = "Long Time"              'hh:nn:ss am/pm
Public Const PFS_SHORT_TIME_FORMAT = "Short Time"       '24-hour hh:nn
Public Const PFS_DATETIME_FORMAT = "General Date"
Public Const PFS_CURRENCY_FORMAT = "Currency"
Public Const PFS_ISO_DATETIME_FORMAT = "yyyymmddhhnnss" 'suitable for ordering with strings

'This is the minimum column width needed to show the longest date/time format with
'all digits showing.  ex: "12/31/2001 12:30 PM" in the default font.
Public Const PFS_DATE_COLUMN_WIDTH = 950
Public Const PFS_DATETIME_COLUMN_WIDTH = 1900

'Error handling: use these to tell a validation error from an unexpected error
Public Const PFSERR_VALIDATION = vbObjectError + 31415926
Public Const PFSERR_UNKNOWN_UNIT = PFSERR_VALIDATION + 1
Public Const PFSERR_INACTIVE_UNIT = PFSERR_VALIDATION + 2
Public Const PFSERR_LOCATION_OUT_LT_IN = PFSERR_VALIDATION + 3
Public Const PFSERR_LAST = PFSERR_VALIDATION + 4

'You tried to update an existing row but someone else modified it before you
'saved your changes.
Public Const SQL_ERROR_UNKNOWN_TABLE_NAME = -2147217865
Public Const SQL_ERROR_CANNOT_LOCATE_ROW = -2147217864
Public Const SQL_ERROR_UNKNOWN_COLUMN_NAME = 3265
'Non-unique error codes!  Use Instr err.description instead.
'Public Const SQL_ERROR_PRIMARY_KEY = -2147217900
'Public Const SQL_ERROR_INVALID_COLUMN = -2147217900
'Public Const SQL_ERROR_DEADLOCK = -2147467259
'Public Const SQL_ERROR_COMMUNICATION_FAILURE = -2147467259
Public Const SQL_INSTR_DUPLICATE_KEY = "duplicate key"  'cannot insert duplicate key
Public Const SQL_INSTR_DEADLOCK = "deadlock"            'you are the deadlock victim - try again
Public Const SQL_INSTR_CANT_LOCK = "cannot obtain a LOCK" 'can't get a lock - try again

'Exit codes
Public Const PFS_EXIT_NORMAL = 0                    'normal exit
Public Const PFS_EXIT_TIMEOUT = vbObjectError + 1   'exit because of timeout

'Patient Class
Public Const PTCLASS_INPATIENT = "I"
Public Const PTCLASS_OUTPATIENT = "O"
Public Const PTCLASS_EMERGENCY = "E"
Public Const PTCLASS_UNKNOWN = "U"

'Data sources for Classification and Encounter
Public Const DATA_SOURCE_BATCH = "B"                'batch import
Public Const DATA_SOURCE_DEFAULT = "D"              'default patient type
Public Const DATA_SOURCE_HL7 = "H"                  'HL7 interface
Public Const DATA_SOURCE_ONLINE = "O"               'on-line classification and manual ADT
Public Const DATA_SOURCE_ONLINE_BY_NAME = "N"       'classify by name
Public Const DATA_SOURCE_ONLINE_BY_PROFILE = "P"    'classify by profile
Public Const DATA_SOURCE_ONLINE_BY_TYPE = "T"       'classify by type
Public Const DATA_SOURCE_SCANNER = "S"              'scanned in
Public Const DATA_SOURCE_XIMPORT = "TC"             'transparent classification

'Method used to distribute hours in recommended and alternate staffing
Public Const RECSTAFF_BY_PATIENT_TYPE = "PT"                    'by patient type
Public Const RECSTAFF_BY_COMPLEXITY = "CC"                      'by complexity of care
Public Const RECSTAFF_RATIO_CENSUS_BY_PATIENT_TYPE = "RC_PT"    'ratio by census with skill distribution by patient type
Public Const RECSTAFF_RATIO_CENSUS_BY_COMPLEXITY = "RC_CC"      'ratio by census with skill distrubution by complexity
Public Const RECSTAFF_RATIO_PATIENT_BY_PATIENT_TYPE = "RP_PT"   'ratio by patient with skill distribution by patient type
Public Const RECSTAFF_RATIO_PATIENT_BY_COMPLEXITY = "RP_CC"     'ratio by patient with skill distrubution by complexity

'Unit group types
Public Const UNIT_GROUP_INPATIENT = "I"
Public Const UNIT_GROUP_METHODOLGY_SPECIFIC = "M"
Public Const UNIT_GROUP_SINGLE_UNIT = "S"
Public Const UNIT_GROUP_UNRESTRICTED = "U"

'Report note types
Public Const REPORT_NOTE_STAFFING = 1
Public Const REPORT_NOTE_MONITOR = 2

'"Other" workload types
Public Const WORKLOAD_TYPE_TELEPHONE = 1
Public Const WORKLOAD_TYPE_OTHER = 2

'Skill Consolidation codes
Public Const SKILL_RN = "02"
Public Const SKILL_LICENSED = "03"
Public Const SKILL_UNLICENSED = "04"

'What procedure skill level is desired?  (procedure skill options converted to int)
'These need to match the skill consolidation codes as much as possible.  (see rec staffing)
Public Enum SkillLevel                  'skill consolidation code
    SKILL_LEVEL_DIST = 0                '00 (or blank) = use skill distribution table
    SKILL_LEVEL_UNUSED = 1              '01 = Provider
    SKILL_LEVEL_RN = 2                  '02 = RN
    SKILL_LEVEL_LIC = 3                 '03 = licensed
    SKILL_LEVEL_UNLIC = 4               '04 = unlicensed
                                        '05 = Clerical
                                        '06 = Technical
                                        '07 = Mgmt
                                        '08 = Resource
    SKILL_LEVEL_MAX = 4
End Enum

'Provider roles (physicians)
Public Const PROVIDER_ROLE_ADMITTING = "01"
Public Const PROVIDER_ROLE_DISCHARGE = "02"
Public Const PROVIDER_ROLE_ATTENDING = "03"
Public Const PROVIDER_ROLE_REFERRING = "04"
Public Const PROVIDER_ROLE_CONSULTING = "05"
Public Const PROVIDER_ROLE_SURGEON = "06"
Public Const PROVIDER_ROLE_ANESTHETIST = "07"

'Special Unit IDs
Public Const UNIT_ID_UNKNOWN = -1       'unit is not defined on our system
Public Const UNIT_ID_IN_TRANSIT = -2    'patient is between units but still under care
Public Const UNIT_ID_ON_LEAVE = -3      'patient is away for the weekend (long-term care)
Public Const UNIT_ID_OFF_SITE = -4      'patient is no longer under care (ambulatory)
Public Const UNIT_ID_TEMPORARY = -5     'patient is in X-ray or hallway.
Public Const UNIT_ID_OR = -6            'patient is in OR

'Special User IDs
Public Const USER_ID_UNKNOWN = -1       'user is not defined on our system
Public Const USER_ID_DEFAULT = -2       'default classification
Public Const USER_ID_IMPORT = -3        'class import
Public Const USER_ID_SCANNER = -4       'scanned classification
Public Const USER_ID_HL7 = -5           'HL7 interface

'Special methodology ids
Public Const METH_ID_DO_NOT_CLASSIFY = -1 '"Do Not Classify" methodology

'Methodology ids
Public Const METH_ID_MEDSURG_VI = 1
Public Const METH_ID_MENTAL_HEALTH_VI = 2
Public Const METH_ID_EMERGE = 3
Public Const METH_ID_PERINATAL = 4
Public Const METH_ID_AMBULATORY = 5
Public Const METH_ID_DIALYSIS = 6
Public Const METH_ID_PFSWM_INPATIENT = 7
Public Const METH_ID_PFSWM_MENTAL_HEALTH = 8
Public Const METH_ID_PFSWM_PERINATAL = 9
Public Const METH_ID_ED_TRIAGE3 = 10
Public Const METH_ID_ED_VISIT = 11
Public Const METH_ID_ED_INPATIENT = 12
Public Const METH_ID_ED_TRIAGE4 = 13
Public Const METH_ID_ED_TRIAGE5 = 14
Public Const METH_ID_INPATIENT = 15
Public Const METH_ID_APLUS_AMBULATORY = 16
Public Const METH_ID_APLUS_MENTAL_HEALTH = 17
Public Const METH_ID_APLUS_INPATIENT2 = 18
Public Const METH_ID_APLUS_PERINATAL1 = 19
Public Const METH_ID_APLUS_INPATIENT2_1 = 20
Public Const METH_ID_ED2_TRIAGE3 = 21
Public Const METH_ID_ED2_TRIAGE4 = 22
Public Const METH_ID_ED2_TRIAGE5 = 23
Public Const METH_ID_ED2_VISIT = 24
Public Const METH_ID_ED2_INPATIENT = 25

'These methodology ids form emerge sub-groups
Public Const METH_IDS_ED_TRIAGE = "10,13,14, 21,22,23"
Public Const METH_IDS_ED_VISIT = "3,11,24"
Public Const METH_IDS_ED_INPATIENT = "12,25"

'Custom Methodologies (these may trigger some special functions or behaviors)
Public Const METH_ID_CALGARY_OR = 101
Public Const METH_ID_CALGARY_RR = 102
Public Const METH_ID_CALGARY_LD = 103
Public Const METH_ID_BAYCREST_LT = 104

'Methodology groups for Report Selection - one character each
Public Const METH_GROUP_INPATIENT = "I"
Public Const METH_GROUP_EMERGENCY = "E"
Public Const METH_GROUP_PERINATAL = "P"
Public Const METH_GROUP_AMBULATORY = "A"
Public Const METH_GROUP_CUSTOM = "C"
Public Const METH_GROUP_OUTCOMES = "T"          'fake group to create a report tab
Public Const METH_GROUP_RATIO_BY_CENSUS = "R"   'fake group to create a report tab
Public Const METH_GROUP_RATIO_BY_PATIENT = "S"  'fake group to create a report tab

'Programs we might want to start
Public Const BIN_PATH = "..\bin\"
Public Const ATSTAFF_STAFFING_EXPORT_EXE = "CustomStaffExport.exe"
Public Const BATCH_INPUT_EXE_NAME = "BatchInput.exe"
Public Const CLASSIFICATION_EXPORT_EXE_NAME = "ClassExport.exe"
Public Const CUSTOM_EXPORT_ALBANY_EXE_NAME = "CustomWorkloadExport_Albany.exe"
Public Const CUSTOM_EXPORT_PARTNERS_EXE_NAME = "EPSiExport.exe"
Public Const CUSTOM_EXPORT_SHIFTWIZARD_EXE_NAME = "CustomAssignmentExport_SW.exe"
Public Const DATABOOK_EXTRACT_EXE_NAME = "DBExtract.exe"
Public Const FINANCE_EXPORT_EXE_NAME = "FinanceExport.exe"
Public Const FORECASTER_EXE_NAME = "StaffingForecaster.exe"
Public Const OUTCOMES_REPORT_BUILDER_EXE_NAME = "OutcomesReportBuilder.exe"
Public Const OUTCOMES_REPORT_GENERATOR_EXE_NAME = "OutcomesReportGenerator.exe"
Public Const REPORTS_EXE_NAME = "Reports.exe"
Public Const SCANTRON_EXE_NAME = "ScanClient.exe"
Public Const STAFFING_EXPORT_EXE_NAME = "StaffingExport.exe"
Public Const SYSMGR_EXE_NAME = "SysManager.exe"
Public Const TRANSPARENT_IMPORT_EXE_NAME = "TransparentImport.exe"
Public Const TRANSPARENT_MAPPING_EXE_NAME = "TransparentMapping.exe"

'System Manager node numbers for different "functions" (root of each subtree)
Public Const SYSMGR_NODE_TOP = 0
Public Const SYSMGR_NODE_SECURITY = 1000
Public Const SYSMGR_NODE_SYSPARAM = 2000
Public Const SYSMGR_NODE_DATAENTRY = 4000

'Captions of the Generic Checkboxes
Public Const CAPTION_MUST_CLASS_EVERY_SHIFT = "Must classify every shift"


'When we encrypt a password and fetch it from the database it looses some accuracy
Public Const PASSWORD_MAX_DIFFERENCE = 0.000000000001           '=1.0E-12 km 7/31/01

' Registry access
Public Const HKEY_CURRENT_USER = &H80000001
Public Const HKEY_LOCAL_MACHINE = &H80000002

' LogText event modes (report calculations)
Public Enum LogEventModes
    LOG_EVENT_MAJOR_SECTION
    LOG_EVENT_INFO
    LOG_EVENT_DETAIL
    LOG_EVENT_DETAIL_NOCR
    LOG_EVENT_SQL
    LOG_EVENT_WARNING
    LOG_EVENT_ERROR
    LOG_EVENT_UNIT_ID
End Enum

'Use these SQL commands with PFSCodeDescList to load a list of facilities or units.
'Use these if you want a list of everything regardless of the user's permissions.
'
'(exclude our hidden facility)
Public Const SQL_ALL_FACILITIES = _
     "SELECT FACILITY_ID, NAME FROM FACILITY WHERE FACILITY_ID > 0 ORDER BY SEQUENCE"
'(exclude our hidden units)
Public Const SQL_ALL_UNITS = _
     "SELECT UNIT_ID, UNIT_FACILITY_NAME" & _
    " FROM VIEW_UNIT_FACILITY" & _
    " WHERE UNIT_ID > 0" & _
    " ORDER BY FACILITY_SEQUENCE, UNIT_SEQUENCE"
'(exclude our special users)
Public Const SQL_ALL_USERS = _
    "SELECT USER_ID," & _
    " ISNULL(LAST_NAME, '') + ', ' + ISNULL(FIRST_NAME, '')" & _
    " + ' (' + LOGIN_NAME + ') '" & _
    " + CASE WHEN IS_ACTIVE = 'N' THEN '*' ELSE '' END" & _
    " FROM USERS WHERE (USER_ID > 0) AND (LOGIN_NAME <> 'winpfs')" & _
    " ORDER BY LAST_NAME, FIRST_NAME"

'Units for transfer: include our hidden units ("unknown", etc.) for manual ADT.
'Exclude sub-units since they look at their parent's patient list.
'Include active units only
Public Const SQL_UNITS_FOR_TRANSFER = _
    "SELECT UNIT_ID, UNIT_FACILITY_NAME" & _
    " FROM VIEW_UNIT_FACILITY" & _
    " WHERE PARENT_UNIT_ID IS NULL" & _
    " AND IS_ACTIVE='Y'" & _
    " ORDER BY FACILITY_SEQUENCE, UNIT_SEQUENCE"

Public Const SQL_UNIT_GROUP_INPATIENT = _
    "SELECT UNIT_GROUP_ID, DESCRIPTION, UNIT_GROUP_TYPE_LVC" & _
    " FROM UNIT_GROUP WHERE UNIT_GROUP_TYPE_LVC = 'I'" & _
    " ORDER BY DESCRIPTION"
    
Public Const SQL_UNIT_GROUP_METHODOLOGY = _
    "SELECT UNIT_GROUP_ID, DESCRIPTION, UNIT_GROUP_TYPE_LVC" & _
    " FROM UNIT_GROUP WHERE UNIT_GROUP_TYPE_LVC = 'M'" & _
    " ORDER BY DESCRIPTION"
    
Public Const SQL_UNIT_GROUP_UNRESTRICTED = _
    "SELECT UNIT_GROUP_ID, DESCRIPTION, UNIT_GROUP_TYPE_LVC" & _
    " FROM UNIT_GROUP WHERE UNIT_GROUP_TYPE_LVC = 'U'" & _
    " ORDER BY DESCRIPTION"

'Classification risk flags
Public Const RISK_FOR_FALLS_FLAG = "F"
Public Const RISK_MED_ERROR_FLAG = "M"
Public Const RISK_ALLERGY_FLAG = "A"

Public Const MAX_LVC_LENGTH = 16                'LVC lookup codes in database
Public Const MAX_ICD9_LENGTH = 8                'I9 codes in database

'IRRT module
Public Const NONE_OF_INDICATORS_INDICATOR_NAME = "None of the above"
Public Const NONE_OF_THESE_INDICATOR_ID = 999
Public Const INPATIENT_PROC_ID_ADD = 1000       'IRRT procedure and indicators ids
Public Const SURGICAL_PROC_ID_ADD = 2000
Public Const NON_SURGICAL_PROC_ID_ADD = 3000

'Report calcs
Public Const AUTO_RECALC_FN_NAME = "AUTO_REPORT_CALC"
Public Const AUTO_RECALC_FN_DESC = "Background Report Calculations"

'==============================================================================
' These declarations are for the ShowPreviousInstance function.
Private Declare Function FindWindow Lib "user32" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As Long
Private Declare Function EnumWindows Lib "user32" (ByVal lpEnumFunc As Long, ByVal lParam As Long) As Long
Private Declare Function GetClassName Lib "user32" Alias "GetClassNameA" (ByVal hWnd As Long, ByVal lpClassName As String, ByVal nMaxCount As Long) As Long
Private Declare Function GetWindowText Lib "user32" Alias "GetWindowTextA" (ByVal hWnd As Long, ByVal lpString As String, ByVal cch As Long) As Long
Private Declare Function GetWindowLong Lib "user32" Alias "GetWindowLongA" (ByVal hWnd As Long, ByVal nIndex As Long) As Long

Private Const GWL_hWndPARENT = (-8)

Private Const SW_SHOWMINIMIZED = 2
Private Const SW_SHOWNORMAL = 1

Private Declare Function BringWindowToTop Lib "user32" (ByVal hWnd As Long) As Long
Private Declare Function GetWindowPlacement Lib "user32" (ByVal hWnd As Long, lpwndpl As WINDOWPLACEMENT) As Long
Private Declare Function SetWindowPlacement Lib "user32" (ByVal hWnd As Long, lpwndpl As WINDOWPLACEMENT) As Long
    
Private Type POINTAPI    '8
        X As Long
        Y As Long
End Type

Private Type RECT    '16
        left    As Long
        top     As Long
        Right   As Long
        Bottom  As Long
End Type

Private Type WINDOWPLACEMENT ' size = 44
        Length As Long              '4
        flags As Long               '4
        showCmd As Long             '4
        ptMinPosition As POINTAPI   '8
        ptMaxPosition As POINTAPI   '8
        rcNormalPosition As RECT    '16
End Type

'==============================================================================
'These are for the EnumWindows function
Private foundSameWindow     As Boolean
Private myClassName         As String * 256
Private myWindowTitle       As String * 256
Private myWindowhWnd        As Long
Private existingWindowhWnd  As Long
Private excludeWindows() As String

Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any) As Long
Private Declare Function PostMessage Lib "user32" Alias "PostMessageA" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long

Private Const WM_CLOSE = &H10


'=============================================================================
' Show the previous instance of a program if it is already running
' Returns True if successful.
' This function must be in a module.
'=============================================================================
Public Function ShowPreviousInstance(ByVal hWnd As Long) As Boolean
    On Error GoTo errHandler
    
    Dim wp As WINDOWPLACEMENT

    myWindowhWnd = hWnd
    GetClassName myWindowhWnd, myClassName, 120
    GetWindowText myWindowhWnd, myWindowTitle, 120

    'Search the windows
    foundSameWindow = False
    EnumWindows AddressOf FindSameWindowCallback, 0
    
    If (foundSameWindow) Then
        wp.Length = Len(wp)                 'Len works like sizeof on structs
        GetWindowPlacement existingWindowhWnd, wp

        If (wp.showCmd = SW_SHOWMINIMIZED) Then
            wp.showCmd = SW_SHOWNORMAL
            SetWindowPlacement existingWindowhWnd, wp
        Else
            BringWindowToTop existingWindowhWnd
        End If
    End If
    
    ShowPreviousInstance = foundSameWindow
    Exit Function
    
errHandler:
    g_util.ThrowError "ShowPreviousInstance"
End Function


Private Function CompareLeft(s As String, substr As String) As Boolean
    CompareLeft = (left$(s, Len(substr)) = substr)
End Function


' This callback function must be in a module (.mod)
'
' To continue enumeration, the callback function must return TRUE;
' to stop enumeration, it must return FALSE.
'
Private Function FindSameWindowCallback(ByVal hWnd As Long, ByVal lParam As Long) As Boolean
    'Do not raise an error to Windows
    On Error Resume Next

    Dim ClassName As String * 256
    Dim windowTitle As String * 256
    Dim lRet As Long, cmpLength As Integer

    FindSameWindowCallback = True                           'continue enumerating
    
    ' Don't do any checking if it is this window.
    If (hWnd = myWindowhWnd) Then Exit Function
    
    lRet = GetClassName(hWnd, ClassName, 120)
    
    If (lRet <> 0) Then
        'Debug.Print "class name = "; Trim$(className)
        
        ' First check the class name
        ' (for some reason class names are different for the different instances
        ' that is why class name check has been commented out)
        
'        If (ClassName = myClassName) Then
            lRet = GetWindowText(hWnd, windowTitle, 120)
            If (lRet <> 0) Then
'                Debug.Print "class name='"; Trim$(ClassName); "' title=" '";trim$(windowTitle);"'"
                'Now check the window title.  There are two things to look out for:
                '1) We may add additional information to AppCaption.
                '2) System Manager ignores AppCaption and sets a window title depending on the root node.
                
                'Many windows add something after QuadraMed AcuityPlus.  The main program calls
                'this before adding anything so AppCation can be used to get the length of
                'the comparison.

                'System Manager does't use AppCaption so we can't compare against it.

                cmpLength = Len(g_util.AppCaption)
                If (left$(windowTitle, cmpLength) = left$(myWindowTitle, cmpLength)) Then
                    existingWindowhWnd = hWnd
                    foundSameWindow = True
                    FindSameWindowCallback = False          'stop enumerating
                    Exit Function
                End If
            End If
'        End If
    End If
    
End Function



'=============================================================================
' Close all same-product programs except this one
'=============================================================================
Public Sub CloseAppWindowsExceptMe(ByVal frm As Form)
    On Error GoTo errHandler
    Static been_here As Boolean

    If Not been_here Then
        been_here = True
        ReDim excludeWindows(3)
        excludeWindows(1) = "Microsoft"         'Visual Basic, Word, etc.
        excludeWindows(2) = "HL7 Interface"
        excludeWindows(3) = "Auto Report"
    End If
    
    If Not (frm Is Nothing) Then
        myWindowhWnd = frm.hWnd
    Else
        myWindowhWnd = 0
    End If
    
    'Search the windows
    EnumWindows AddressOf CloseWindowsCallback, 0
    Exit Sub
    
errHandler:
    g_util.ThrowError "CloseAppWindowsExceptMe"
End Sub

Private Function ExcludeThisWindow(title As String) As Boolean
    Dim i As Integer

    For i = 1 To UBound(excludeWindows)
        If InStr(title, excludeWindows(i)) > 0 Then
            ExcludeThisWindow = True
            Exit For
        End If
    Next i
End Function


' This callback function must be in a module (.bas)
Private Function CloseWindowsCallback(ByVal hWnd As Long, ByVal lParam As Long) As Boolean
    'Do not raise an error to Windows
    On Error Resume Next

    Dim ClassName As String * 256
    Dim windowTitle As String * 256
    Dim lRet As Long, cmpLength As Integer, i As Integer
    Dim rc As Long

    CloseWindowsCallback = True                           'continue enumerating

    ' Leave the calling window alone
    If (hWnd = myWindowhWnd) Then Exit Function
    
    lRet = GetWindowText(hWnd, windowTitle, 120)
    If (lRet <> 0) Then
'        Debug.Print "title="; Trim$(windowTitle); "'"
        If CompareLeft(windowTitle, App.CompanyName & " " & App.ProductName) Then
            If Not ExcludeThisWindow(windowTitle) Then
                'Don't wait for the other window - Post message instead of Send
                'rc = SendMessage(hWnd, WM_CLOSE, 0, ByVal 0&)
                rc = PostMessage(hWnd, WM_CLOSE, 0, 0)
            End If
        End If
    End If
    
End Function

