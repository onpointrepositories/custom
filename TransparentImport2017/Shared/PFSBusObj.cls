VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PFSBusObj"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'
' PFSBusObj: Business Objects report interface
'
Private boApp As busobj.Application
Private myDoc As busobj.Document


Public Sub PrintReport(filename As String)
    ProcessReport filename, False
End Sub

Public Sub PreviewReport(filename As String)
    ProcessReport filename, True
End Sub

Private Sub ProcessReport(filename As String, preview As Boolean)
    Dim i As Integer, status As String, msg As String
    
    On Error GoTo errHandler
    
    g_util.CursorPushHourglass
    
    If (boApp Is Nothing) Then
        If Not StartReader() Then
            MsgBox "Could not start Business Objects", vbCritical + vbOKOnly, "Error"
            g_util.CursorPop
            Exit Sub
        End If
    End If
           
    status = "Opening window"
    boApp.Window.State = Maximized
    boApp.Visible = True
    boApp.Interactive = True
    boApp.Window.Activate 'km: put bo window on top
    
    status = "Opening report"
    Set myDoc = boApp.Documents.Open(UserDocsPath() & "\" & filename)

    status = "Refreshing data"
    On Error Resume Next                    'ignore errors refreshing data
    myDoc.Refresh
    On Error GoTo errHandler

    If (preview) Then
        status = "Preview"
'        Set myComm = boApp.Commands.ItemID(FilePrintPreview)
'        km: No PrintPreview equivalent in BO 5.x -- user will see design mode.
    Else
        status = "Print"
        myDoc.Activate
        myDoc.PrintOut
    End If
    
ProcessReport_Exit:
    g_util.CursorPop
    Exit Sub

errHandler:
    msg = "Error " & status & ": " & Err.Description
    MsgBox msg, , "Error from Business Objects"
    GoTo ProcessReport_Exit
End Sub


Public Function StartReader() As Boolean
    On Error GoTo errHandler

    'The class names are the same so this application can only launch one version
    'of Business Objects.  The appropriate version must be selected in Project References.
    Set boApp = New busobj.Application
    
    'Start the Business Objects Reader and log in
    StartReader = boApp.LoginAs("pfs", "", False)
    StartReader = True
    Exit Function
errHandler:
    StartReader = False
    Exit Function
End Function

'
'Shut down the reader (if the user didn't already)
'
Public Sub ShutDownReader()
    On Error Resume Next

    'Never started?
    If (boApp Is Nothing) Then
        Exit Sub
    End If
    
    'Close Report
    If Not (myDoc Is Nothing) Then
        boApp.ActiveDocument.Close boDontSave
        Set myDoc = Nothing
    End If
    
    'Exit
    boApp.Quit
    Set boApp = Nothing
End Sub


Public Function BusinessObjectsIsInstalled() As Boolean
    Dim busobjPath As String
    
    'Look for BO 5.x
    g_util.GetKeyValue HKEY_LOCAL_MACHINE, _
            "software\microsoft\windows\currentversion\uninstall\businessobjects 5.0", _
            "InstallLocation", busobjPath
    
    If Len(busobjPath) = 0 Then
        'Look for BO 6.x
        g_util.GetKeyValue HKEY_LOCAL_MACHINE, _
            "SOFTWARE\Business Objects\Suite 6.0\default", _
            "BINDIR", busobjPath
    End If

    BusinessObjectsIsInstalled = (Len(busobjPath) > 0)
End Function


Public Function UserDocsPath() As String
    Dim result As String
    Dim i As Integer
 
    'Look for BO 5.x UserDocs
    g_util.GetKeyValue HKEY_LOCAL_MACHINE, _
            "SOFTWARE\Business Objects\Shared\General\5.0\Directories\Document", _
            "Directory", result
    If Len(result) > 0 Then
        'The final UserDocs folder name is separated from the rest by a nul character
        For i = 1 To Len(result)
            If (Asc(Mid(result, i)) = 0) Then
                Mid(result, i) = "\"
            End If
        Next i
    Else
        'BO 6.x reports are stored in WinPFS\UserDocs
        g_util.GetKeyValue HKEY_LOCAL_MACHINE, _
            "SOFTWARE\QuadraMed\WinPFS", _
            "Directory", result
        If Len(result) > 0 Then
            result = result & "\UserDocs"
        Else
            'Development UserDocs
            result = "C:\pfs\dev\source\UserDocs"
        End If
    End If

    UserDocsPath = result
End Function


Public Function ReportIsInstalled(fname As String) As Boolean
    ReportIsInstalled = g_util.FileExists(UserDocsPath() & "\" & fname)
End Function



