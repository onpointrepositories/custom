VERSION 5.00
Begin VB.Form frmPFSSplash 
   AutoRedraw      =   -1  'True
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   3  'Fixed Dialog
   ClientHeight    =   5535
   ClientLeft      =   45
   ClientTop       =   45
   ClientWidth     =   9720
   ControlBox      =   0   'False
   Icon            =   "frmSplash.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5535
   ScaleWidth      =   9720
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Visible         =   0   'False
   Begin VB.Label lblVersion 
      BackStyle       =   0  'Transparent
      Caption         =   "Version 0.0"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   21.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00696663&
      Height          =   615
      Left            =   2580
      TabIndex        =   2
      Top             =   3660
      Width           =   3495
   End
   Begin VB.Label lblProduct 
      BackStyle       =   0  'Transparent
      Caption         =   "AcuityPlus™"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   27.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H0090864B&
      Height          =   735
      Left            =   2160
      TabIndex        =   3
      Top             =   2880
      Width           =   4995
   End
   Begin VB.Label lblStatus 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      Caption         =   "Status message..."
      ForeColor       =   &H00362C00&
      Height          =   315
      Left            =   120
      TabIndex        =   1
      Top             =   5100
      Width           =   6015
      WordWrap        =   -1  'True
   End
   Begin VB.Label lblCopyright 
      Alignment       =   1  'Right Justify
      BackColor       =   &H00F0F0F0&
      BackStyle       =   0  'Transparent
      Caption         =   "Copyright"
      BeginProperty Font 
         Name            =   "Microsoft Sans Serif"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00362C00&
      Height          =   255
      Left            =   6720
      TabIndex        =   0
      Top             =   5340
      Width           =   2775
   End
   Begin VB.Image Image2 
      Height          =   5580
      Left            =   0
      Picture         =   "frmSplash.frx":1272
      Top             =   0
      Width           =   10260
   End
End
Attribute VB_Name = "frmPFSSplash"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'
' Standard splash screen
' All information comes from application properties (App object)
'
Private Declare Function InitCommonControls Lib "COMCTL32" () As Long

Public Sub SetStatus(message As String)
    lblStatus.caption = message
    Me.Refresh
End Sub


Private Sub Form_Initialize()
    'This is required when a manifest file is being used to load the XP look.
    'Most programs are not affected but programs with the splash screen blow up
    'without this.
    InitCommonControls
End Sub

Private Sub Form_Load()
    Dim msg As String
    
    'Leave the fonts alone; there is no background color or icon - just an image
    'g_display.SetStdTheme Me

    lblProduct.caption = App.ProductName & "™"
    lblVersion.caption = "Version " & App.major & "." & App.minor
    lblStatus.caption = ""
    
    lblCopyright.caption = App.LegalCopyright
    lblCopyright.Visible = False
End Sub

Private Sub Form_Activate()
    Dim i As Integer
    i = 1                                      '** Breakpoint here for a good screen shot
End Sub

