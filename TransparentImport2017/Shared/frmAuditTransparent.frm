VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmAuditTransparent 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Audit Transparent Mapping"
   ClientHeight    =   2715
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6030
   Icon            =   "frmAuditTransparent.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2715
   ScaleWidth      =   6030
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdVerbose 
      Caption         =   "Verbose"
      Height          =   375
      Left            =   1560
      TabIndex        =   8
      Top             =   2220
      Width           =   1335
   End
   Begin VB.CommandButton cmdClose 
      Cancel          =   -1  'True
      Caption         =   "Close"
      Height          =   375
      Left            =   4560
      TabIndex        =   3
      Top             =   2220
      Width           =   1335
   End
   Begin VB.CommandButton cmdBrief 
      Caption         =   "Brief"
      Default         =   -1  'True
      Height          =   375
      Left            =   120
      TabIndex        =   2
      Top             =   2220
      Width           =   1335
   End
   Begin VB.Frame Frame1 
      Caption         =   "Patient Information"
      Height          =   1995
      Left            =   120
      TabIndex        =   4
      Top             =   120
      Width           =   5775
      Begin VB.ComboBox cboClassDatetime 
         Height          =   315
         Left            =   1920
         TabIndex        =   10
         Text            =   "cboClassDatetime"
         Top             =   1500
         Width           =   2415
      End
      Begin VB.ComboBox cboAcct 
         Height          =   315
         Left            =   1920
         Style           =   2  'Dropdown List
         TabIndex        =   9
         Top             =   1080
         Width           =   3735
      End
      Begin VB.ComboBox cboUnit 
         Height          =   315
         Left            =   1920
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   240
         Width           =   2415
      End
      Begin MSComCtl2.DTPicker dtpDate 
         Height          =   315
         Left            =   1920
         TabIndex        =   1
         Top             =   660
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   556
         _Version        =   393216
         Format          =   105512961
         CurrentDate     =   40400
      End
      Begin VB.Label Label3 
         Caption         =   "Classification"
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   1560
         Width           =   1695
      End
      Begin VB.Label Unit 
         Caption         =   "Unit"
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   300
         Width           =   1695
      End
      Begin VB.Label Label2 
         Caption         =   "Date"
         Height          =   315
         Left            =   120
         TabIndex        =   6
         Top             =   720
         Width           =   1695
      End
      Begin VB.Label Label1 
         Caption         =   "Patient"
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   1140
         Width           =   1695
      End
   End
End
Attribute VB_Name = "frmAuditTransparent"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'
' Audit Transparent Mapping
' - get the transparent mapping log for the given classification.
' If not found we could
' - launch transparent mapping and display its log file
'
' THIS WINDOW IS SHARED BY SYSTEM MANAGER AND PATIENT SELECTION
'
Private cdlUnit             As New PFSCodeDescList
Private cdlAcct             As New PFSCodeDescList
Private cdlClass            As New PFSCodeDescList
Private m_tcutil            As New PFSTransparentUtility

Private m_initial_unit_id   As Long
Private m_initial_date      As Variant
Private m_initial_enc_id    As Long

Private m_class_id          As Long

Private is_loading          As Boolean


' Set the initial values called by patient selection with the current patient.
Public Sub SetupForPatient(unit_id As Long, display_date As Date, encounter_id As Long)
    m_initial_unit_id = unit_id                     'save for form_load
    m_initial_date = display_date
    m_initial_enc_id = encounter_id
    
    Me.show vbModal
End Sub

' The user is able to pick any unit, date and patient
Public Sub SetupFreeMode()
    m_initial_unit_id = 0
    m_initial_date = Null
    m_initial_enc_id = 0
    
    Me.show vbModal
End Sub


Private Sub Form_Load()
    On Error GoTo errHandler
    is_loading = True
    
    g_display.SetStdTheme Me, True

    cdlUnit.LoadSQL SQL_ALL_UNITS
    cdlUnit.FillComboBox cboUnit
    If (m_initial_unit_id <> 0) Then
        cdlUnit.SelectComboBoxItem cboUnit, CStr(m_initial_unit_id)
    End If
    
    If IsDate(m_initial_date) Then
        dtpDate.value = m_initial_date
    Else
        dtpDate.value = Now
    End If
    
    LoadPatients

    If (m_initial_enc_id <> 0) Then
        cdlAcct.SelectComboBoxItem cboAcct, CStr(m_initial_enc_id)
    End If
    
    LoadClassifcationList
    
    'Lock in the unit & date (when called from patient selection)
    If Not g_user.Security.HasPermission(secSuperUser) Then
        cboUnit.Enabled = False
        dtpDate.Enabled = False
    End If
        
    SetButtonState
    is_loading = False
    Exit Sub
    
errHandler:
    g_util.MsgBoxError "form load"
    Exit Sub
    Resume
End Sub

Private Sub cboUnit_Click()
    If is_loading Then Exit Sub
    LoadPatients
End Sub

Private Sub dtpDate_Change()
    If is_loading Then Exit Sub
    LoadPatients
End Sub

Private Sub cboAcct_Click()
    If is_loading Then Exit Sub
    LoadClassifcationList
End Sub

Private Sub cboClassDatetime_Click()
    m_class_id = g_dbutil.DBToLong(cdlClass.Code(cboClassDatetime.Text))
    SetButtonState
End Sub


Private Sub cmdBrief_Click()
    m_tcutil.ShowAudit cdlClass.Code(cboClassDatetime.Text), False
End Sub

Private Sub cmdVerbose_Click()
    m_tcutil.ShowAudit cdlClass.Code(cboClassDatetime.Text), True
End Sub

Private Sub cmdClose_Click()
    Unload Me
End Sub

' Load patients for the unit/date - show acct# + name; return encounter_id
Private Sub LoadPatients()
    On Error GoTo errHandler
    Dim rs As New Recordset
    Dim sql As String
    Dim unit_id As Long
    Dim str_enc_id As String

    g_util.CursorPushHourglass

    unit_id = g_dbutil.DBToLong(cdlUnit.Code(cboUnit.Text))
    str_enc_id = cdlAcct.Code(cboAcct.Text)             'save current patient

    'Check both the selected unit and its parent - if a sub-unit is picked, the patients
    'will be in the parent.
    sql = "SELECT E.ENCOUNTER_ID, E.ACCT_NUMBER + ' - ' + P.LAST_NAME + ', ' + ISNULL(P.FIRST_NAME,'') + ' ' + ISNULL(P.MIDDLE_NAME,'')" & _
        " FROM ENCOUNTER AS E" & _
        " INNER JOIN PERSON AS P ON (P.PERSON_ID = E.PERSON_ID)" & _
        " WHERE E.ENCOUNTER_ID IN (" & _
        "   SELECT DISTINCT ENCOUNTER_ID FROM ENCOUNTER_LOCATION" & _
        "   WHERE UNIT_ID IN (" & _
        "       SELECT UNIT_ID        FROM UNIT WHERE UNIT_ID=" & unit_id & _
        "       UNION" & _
        "       SELECT PARENT_UNIT_ID FROM UNIT WHERE UNIT_ID=" & unit_id & _
        "   )" & _
        "   AND " & g_dbutil.SQL_Date(dtpDate.value) & " BETWEEN REPORT_DATE_IN AND REPORT_DATE_OUT" & _
        " )" & _
        " ORDER BY E.ACCT_NUMBER"
    Debug.Print sql
    cdlAcct.LoadSQL sql
    cdlAcct.FillComboBox cboAcct
    cdlAcct.SelectComboBoxItem cboAcct, str_enc_id      'restore selection (if still there)

    g_util.CursorPop
    Exit Sub
errHandler:
    g_util.MsgBoxError
    Exit Sub
    Resume  'debug
End Sub

'Load classications for the account
Private Sub LoadClassifcationList()
    Dim rs As New Recordset
    Dim sql As String
    Dim unit_id As Long, enc_id As Long

    unit_id = g_dbutil.DBToLong(cdlUnit.Code(cboUnit.Text))
    enc_id = g_dbutil.DBToLong(cdlAcct.Code(cboAcct.Text))

    'Check the unit and its children.
    'If a parent unit is selected only the children will have classifications
    sql = "select classification_event_id, classification_datetime from classification_event" & _
        " where unit_id IN (" & _
        "     select unit_id from unit where unit_id=" & unit_id & _
        "     union" & _
        "     select unit_id from unit where parent_unit_id=" & unit_id & _
        " )" & _
        " and encounter_id=" & enc_id & _
        " and IS_DELETED<>'Y'" & _
        " order by classification_datetime desc"
    cdlClass.LoadSQL sql
    cdlClass.FillComboBox cboClassDatetime
End Sub

Private Sub SetButtonState()
    Dim brief As Boolean, verbose As Boolean
    m_tcutil.IsTransparentAuditSnapshotAvailable m_class_id, brief, verbose
    cmdBrief.Enabled = brief
    cmdVerbose.Enabled = verbose
End Sub
