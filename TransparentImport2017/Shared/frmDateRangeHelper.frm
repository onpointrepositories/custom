VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmDateRangeHelper 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Date Range Helper"
   ClientHeight    =   6060
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   5805
   Icon            =   "frmDateRangeHelper.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6060
   ScaleWidth      =   5805
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.PictureBox picGroup 
      Appearance      =   0  'Flat
      ForeColor       =   &H80000008&
      Height          =   2895
      Left            =   120
      ScaleHeight     =   2865
      ScaleWidth      =   5505
      TabIndex        =   13
      Top             =   120
      Width           =   5535
      Begin VB.Frame fraCount 
         Caption         =   "Count"
         Height          =   675
         Left            =   120
         TabIndex        =   25
         Top             =   780
         Width           =   5295
         Begin VB.TextBox txtCount 
            Height          =   315
            Left            =   240
            TabIndex        =   26
            Text            =   "1"
            Top             =   240
            Width           =   735
         End
         Begin MSComCtl2.UpDown updCount 
            Height          =   435
            Left            =   1080
            TabIndex        =   27
            Top             =   180
            Width           =   255
            _ExtentX        =   450
            _ExtentY        =   767
            _Version        =   393216
            Enabled         =   -1  'True
         End
      End
      Begin VB.Frame fraUnits 
         Caption         =   "Unit of Measure (for Count)"
         Height          =   1215
         Left            =   120
         TabIndex        =   20
         Top             =   1500
         Width           =   5295
         Begin VB.PictureBox Picture2 
            BorderStyle     =   0  'None
            Height          =   915
            Left            =   120
            ScaleHeight     =   915
            ScaleWidth      =   5055
            TabIndex        =   21
            Top             =   240
            Width           =   5055
            Begin VB.OptionButton optDateRangeUnits 
               Caption         =   "Months"
               Height          =   315
               Index           =   2
               Left            =   0
               TabIndex        =   24
               Top             =   600
               Width           =   1695
            End
            Begin VB.OptionButton optDateRangeUnits 
               Caption         =   "Weeks"
               Height          =   315
               Index           =   1
               Left            =   0
               TabIndex        =   23
               Top             =   300
               Width           =   1695
            End
            Begin VB.OptionButton optDateRangeUnits 
               Caption         =   "Days"
               Height          =   315
               Index           =   0
               Left            =   0
               TabIndex        =   22
               Top             =   0
               Width           =   1695
            End
         End
      End
      Begin VB.Frame fraEndpoint 
         Caption         =   "Endpoint"
         Height          =   675
         Left            =   120
         TabIndex        =   14
         Top             =   60
         Width           =   5295
         Begin VB.PictureBox Picture1 
            BorderStyle     =   0  'None
            Height          =   375
            Left            =   120
            ScaleHeight     =   375
            ScaleWidth      =   4995
            TabIndex        =   15
            Top             =   240
            Width           =   4995
            Begin VB.OptionButton optDateRangeAnchor 
               Caption         =   "Ending"
               Height          =   315
               Index           =   1
               Left            =   1200
               TabIndex        =   17
               Top             =   0
               Width           =   1215
            End
            Begin VB.OptionButton optDateRangeAnchor 
               Caption         =   "Starting"
               Height          =   315
               Index           =   0
               Left            =   0
               TabIndex        =   16
               Top             =   0
               Width           =   1215
            End
            Begin MSComCtl2.DTPicker dtpDate 
               Height          =   315
               Left            =   2400
               TabIndex        =   18
               Top             =   0
               Width           =   1335
               _ExtentX        =   2355
               _ExtentY        =   556
               _Version        =   393216
               Format          =   106233857
               CurrentDate     =   41047
            End
            Begin VB.Label lblDayOfWeek 
               Caption         =   "DOW"
               Height          =   315
               Left            =   3960
               TabIndex        =   19
               Top             =   60
               Width           =   1035
            End
         End
      End
   End
   Begin VB.Frame fraQuarters 
      Caption         =   "Quarter"
      Height          =   1575
      Left            =   120
      TabIndex        =   4
      Top             =   3060
      Width           =   5535
      Begin VB.PictureBox picXP_quarters 
         BorderStyle     =   0  'None
         Height          =   1275
         Left            =   120
         ScaleHeight     =   1275
         ScaleWidth      =   5295
         TabIndex        =   5
         Top             =   180
         Width           =   5295
         Begin VB.TextBox txtCalendar 
            Height          =   315
            Left            =   1320
            TabIndex        =   28
            Text            =   "Text1"
            Top             =   60
            Width           =   1335
         End
         Begin VB.OptionButton optStartYear 
            Caption         =   "Fiscal"
            Height          =   315
            Index           =   1
            Left            =   120
            TabIndex        =   12
            Top             =   420
            Width           =   1215
         End
         Begin VB.CommandButton cmdQ1 
            Caption         =   "Q1"
            Height          =   375
            Left            =   0
            TabIndex        =   10
            Top             =   840
            Width           =   1215
         End
         Begin VB.CommandButton cmdQ2 
            Caption         =   "Q2"
            Height          =   375
            Left            =   1320
            TabIndex        =   9
            Top             =   840
            Width           =   1215
         End
         Begin VB.CommandButton cmdQ3 
            Caption         =   "Q3"
            Height          =   375
            Left            =   2640
            TabIndex        =   8
            Top             =   840
            Width           =   1215
         End
         Begin VB.CommandButton cmdQ4 
            Caption         =   "Q4"
            Height          =   375
            Left            =   3960
            TabIndex        =   7
            Top             =   840
            Width           =   1335
         End
         Begin VB.OptionButton optStartYear 
            Caption         =   "Calendar"
            Height          =   315
            Index           =   0
            Left            =   120
            TabIndex        =   6
            Top             =   60
            Width           =   1215
         End
         Begin MSComCtl2.DTPicker dtpFiscal 
            Height          =   315
            Left            =   1320
            TabIndex        =   11
            Top             =   420
            Width           =   1335
            _ExtentX        =   2355
            _ExtentY        =   556
            _Version        =   393216
            Format          =   106233857
            CurrentDate     =   41047
         End
      End
   End
   Begin VB.Frame Frame3 
      Caption         =   "Result"
      Height          =   675
      Left            =   120
      TabIndex        =   2
      Top             =   4680
      Width           =   5535
      Begin VB.Label lblResultDateRange 
         Alignment       =   2  'Center
         Caption         =   "<date range>"
         Height          =   315
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   5295
      End
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   375
      Left            =   2993
      TabIndex        =   1
      Top             =   5520
      Width           =   1335
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   375
      Left            =   1433
      TabIndex        =   0
      Top             =   5520
      Width           =   1335
   End
End
Attribute VB_Name = "frmDateRangeHelper"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'
' Date Range helper dialog
' This form does not unload itself so the caller can get all settings if desired for persistent behavior.
' See Reports/frmReportParameters for an example; basically, you need to SetOptions, EditDateRange, GetOptions
' and UnloadForm.
'
Private m_result        As Boolean
Private m_start         As Date
Private m_finish        As Date
Private m_unload_now    As Boolean

Private m_rutil         As New PFSReportUtility

Public Enum DateRangeAnchor
    AnchorStartDate
    AnchorEndingDate
End Enum

Public Enum DateRangeUnits
    DateRangeDays
    DateRangeWeeks
    DateRangeMonths
End Enum

Public Enum DateRangeStartYearMode
    DateRangeStartYearCalendar = 0
    DateRangeStartYearFiscal = 1
End Enum


Public Sub SetOptions(ByVal anchor As DateRangeAnchor, ByVal count As Integer, ByVal units As DateRangeUnits, ytd_start_mode As DateRangeStartYearMode)
    On Error GoTo errHandler
    optDateRangeAnchor(anchor).value = True
    txtCount.text = g_util.Max(CStr(count), 1)
    optDateRangeUnits(units).value = True
    optStartYear(ytd_start_mode).value = True
    Exit Sub
errHandler:
    g_util.ThrowError "SetOptions"
    Resume  'debug
End Sub

Public Sub GetOptions(ByRef anchor As DateRangeAnchor, ByRef count As Integer, ByRef units As DateRangeUnits, ByRef ytd_start_mode As DateRangeStartYearMode)
    Dim n As Integer
    
    For n = AnchorStartDate To AnchorEndingDate
        If optDateRangeAnchor(n).value Then
            anchor = n
            Exit For
        End If
    Next
    
    count = g_dbutil.DBToInteger(txtCount.text)

    For n = DateRangeDays To DateRangeMonths
        If optDateRangeUnits(n).value Then
            units = n
            Exit For
        End If
    Next
    
    For n = DateRangeStartYearCalendar To DateRangeStartYearFiscal
        If optStartYear(n).value Then
            ytd_start_mode = n
            Exit For
        End If
    Next
    
End Sub

'
' Call this and the form will show itself
'
Public Function EditDateRange(ByRef start As Date, ByRef finish As Date) As Boolean
    On Error GoTo errHandler
    
    ' Get the endpoint from the main date range
    If (optDateRangeAnchor(AnchorStartDate).value) Then
        SetDateAnchor start
    Else
        SetDateAnchor finish
    End If

    m_result = False
    
    Me.Show vbModal

    If m_result Then
        start = m_start
        finish = m_finish
    End If
    
    EditDateRange = m_result
    Exit Function
    
errHandler:
    g_util.MsgBoxError
End Function


Public Sub UnloadForm()
    m_unload_now = True
    Unload Me
End Sub

Private Sub Form_Load()
    g_display.SetStdTheme Me, , SETFONT_RESIZE_DTPICKER
    'defaults
    SetOptions AnchorEndingDate, 1, DateRangeDays, DateRangeStartYearCalendar
    updCount.value = 1
    updCount.Min = 1
    updCount.Max = 9999                 '[8713]
    SetDateAnchor date
    txtCalendar.text = year(date)
    dtpFiscal.value = m_rutil.FiscalStartDateForEndDate(date)
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If Not m_unload_now Then
        Cancel = True                   'stay loaded so caller can GetOptions
        Me.Hide
    End If
End Sub

Private Sub SetDateAnchor(dt As Date)
    dtpDate.value = dt
    dtpFiscal.value = m_rutil.FiscalStartDateForEndDate(dt)
    dtpDate_Change                      'setting the value doesn't fire the change event
End Sub

Private Sub dtpDate_Change()
    lblDayOfWeek.caption = Format$(dtpDate.value, "ddd")
    UpdateDateRange
End Sub

Private Sub optDateRangeAnchor_Click(index As Integer)
    UpdateDateRange
End Sub

Private Sub optDateRangeUnits_Click(index As Integer)
    UpdateDateRange
End Sub

Private Sub txtCount_GotFocus()
    txtCount.SelStart = 0
    txtCount.SelLength = Len(txtCount.text)
End Sub

Private Sub txtCount_Change()
    updCount.value = g_util.Min(g_util.Max(g_dbutil.DBToLong(txtCount.text), 1), updCount.Max)
    UpdateDateRange
End Sub

Private Sub updCount_Change()
    txtCount.text = updCount.value
End Sub

Private Sub txtCalendar_GotFocus()
    optStartYear(0).value = True
End Sub
Private Sub dtpFiscal_GotFocus()
    optStartYear(1).value = True
End Sub

Private Sub cmdQ1_Click()
    CalculateQuarter 1
End Sub

Private Sub cmdQ2_Click()
    CalculateQuarter 2
End Sub

Private Sub cmdQ3_Click()
    CalculateQuarter 3
End Sub

Private Sub cmdQ4_Click()
    CalculateQuarter 4
End Sub


Private Sub cmdOK_Click()
    m_result = True
    Unload Me
End Sub

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub CalculateQuarter(ByVal quarter As Integer)
    On Error GoTo errHandler
    Dim ytd_start As Date
    If optStartYear(DateRangeStartYearCalendar).value Then
        ValidateCalendarYear
        ytd_start = g_util.MakeDate(CInt(txtCalendar.text), 1, 1)
    Else
        ytd_start = dtpFiscal.value
    End If
    
    quarter = g_util.Max(quarter, 1)
    quarter = g_util.Min(quarter, 4)
    
    m_start = DateAdd("m", 3 * (quarter - 1), ytd_start)
    
    m_finish = DateAdd("m", 3, m_start)
    m_finish = DateAdd("d", -1, m_finish)
    
    ShowDateRange
    Exit Sub
errHandler:
    g_util.MsgBoxError
End Sub

Private Sub ValidateCalendarYear()
    If IsNumeric(txtCalendar.text) Then Exit Sub
    Err.Raise PFSERR_VALIDATION, , "Invalid calendar year"
End Sub

Private Sub CalculateDateRange(ByRef start As Date, ByRef finish As Date)
    Dim direction As Integer, count As Integer
    Dim dt1 As Date, dt2 As Date
    
    If (optDateRangeAnchor(AnchorStartDate).value) Then
        direction = 1
    Else
        direction = -1
    End If
    
    dt1 = dtpDate.value
    count = g_util.Max(g_dbutil.DBToInteger(txtCount.text), 1)      'at least 1 of something

    If (optDateRangeUnits(DateRangeDays).value) Then
        dt2 = DateAdd("d", direction * (count - 1), dt1)
    
    ElseIf (optDateRangeUnits(DateRangeWeeks).value) Then
        dt2 = DateAdd("d", direction * (7 * count - 1), dt1)
    
    ElseIf (optDateRangeUnits(DateRangeMonths).value) Then
        dt2 = DateAdd("m", count, dt1)
        If (direction = 1) Then
            'go forward to end of month
            dt2 = DateAdd("m", count, dt1)                      'overshoot by a month
            dt2 = DateAdd("d", -DatePart("d", dt2) + 1, dt2)    'set the day to 1
            dt2 = DateAdd("d", -1, dt2)                         'back up to last day of month
        Else
            dt2 = DateAdd("m", -(count - 1), dt1)               'back up one less month
            dt2 = DateAdd("d", -DatePart("d", dt2) + 1, dt2)    'set the day to 1
        End If
    
    End If

    If (dt1 < dt2) Then
        start = dt1
        finish = dt2
    Else
        start = dt2
        finish = dt1
    End If
    
End Sub

Private Sub UpdateDateRange()
    CalculateDateRange m_start, m_finish
    ShowDateRange
End Sub

Private Sub ShowDateRange()
    lblResultDateRange.caption = Format$(m_start, "ddd") & " " & m_start & " - " & Format$(m_finish, "ddd") & " " & m_finish
End Sub
