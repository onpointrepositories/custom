VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PFSToolbar"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'
' PFSToolbar: configure the toolbar look & feel
'
' Each form will need two ImageLists and two Collections.
' The ImageListx32 must contain the 32x32 icons, with the icon filenames in the Key.
' ActiveBar must be pre-loaded with 16x16 icons with the icon file name in TagVariant.
' The toolbar name must begin with 'tbr'.
'

'Do not renumber these!  They must match valus in the database.
Public Enum PFSToolbarIconSize
    TB_SMALL_ICONS = 0
    TB_LARGE_ICONS = 1
    TB_DEFAULT = TB_LARGE_ICONS
End Enum

Public GrabHandleStyle      As ActiveBar2LibraryCtl.GrabHandleStyles
Public IconSize             As PFSToolbarIconSize
Public XPLook               As Boolean
Public MenuAnimation        As ActiveBar2LibraryCtl.MenuStyles

Private m_large_icons       As Boolean


Public Property Get LargeIcons() As Boolean
    LargeIcons = m_large_icons
End Property


Private Sub Class_Initialize()
    'GrabHandleStyle = ddGSDot                  'dotted bar (like Office 2003)
    GrabHandleStyle = ddGSNone                  'Don't allow detach
    IconSize = TB_DEFAULT
    XPLook = True
    MenuAnimation = ddMSAnimateNone
End Sub


Public Sub ConfigureActiveBar2( _
    abr As ActiveBar2, _
    Optional ImageList32 As ImageList, _
    Optional collection1 As Collection, Optional collection2 As Collection _
)
    On Error GoTo errHandler
    Dim oBand As ActiveBar2LibraryCtl.Band

    If (abr.XPLook <> True) Then
        MsgBox "The toolbar XPLook is not set.  (Can only be set at design time)", vbInformation + vbOKOnly
    End If

    'Use the system font in the menu bar and toolbar
    abr.MenuFontStyle = ddMSCustom                  'DDMSSystem
    Set abr.font = g_display.MenuFont               'label/button font
    Set abr.ControlFont = g_display.MenuFont        'text/combo box font

    'Do not allow the user to customize the menu or toolbars
    'Seting this to True says that we will provide a customization dialog (and we don't)
    '(applies to double-clicking on any gray area in the toolbar)
    abr.UserDefinedCustomization = True
    
    m_large_icons = (IconSize = TB_LARGE_ICONS)

    'We aren't supposed to change this at runtime but it seems to work quite well.
    '(ActiveBar knowledge base)
    'Let's leave it alone and use whatever was set at design time.
    'abr.XPLook = XPLook        '**
    
    If (ImageList32 Is Nothing) Then
        'Let the toolbar expand the small icons if requested.
        'This will double the size of the 16x16 icons and distort them
        abr.LargeIcons = m_large_icons
    ElseIf abr.XPLook Then
        LoadIconsXPLook abr, m_large_icons, ImageList32, collection1, collection2
    Else
        'LoadIconsWin2KLook abr, m_large_icons, ImageList16, ImageList32
        LoadIconsXPLook abr, m_large_icons, ImageList32, collection1, collection2
    End If
    
    
    '** Adjust label height with icon size?  The labels are not centering vertically
    '** It doesn't appear to allow this.
    
    '** Adjust combobox width with font size?  (It adjusts its own height)
    '** The drop-down button needs to get wider.

    For Each oBand In abr.Bands
        If (oBand.Type = ddBTMenuBar) Or (oBand.Type = ddBTNormal) Then
        With oBand
            .GrabHandleStyle = GrabHandleStyle
            'Don't allow undocking or customization
            'To disable customization we need to enable it here and then ignore it.
            .flags = ddBFFixed + ddBFCustomize + ddBFStretch

            'If the window is too small, show the rest of the buttons on a pull-down.
            'The problem is, this also shows an Add/Remove buttons customization menu;
            'do we care?
            .DisplayMoreToolsButton = True      'show "more" buttons and add/remove
            '.DisplayMoreToolsButton = False     'cannot reach buttons past the window frame
        End With
        End If
    Next oBand

    abr.MenuAnimation = MenuAnimation

    'In case the icon size changed
    abr.RecalcLayout
    Exit Sub
    
errHandler:
    g_util.ThrowError "ConfigureActiveBar2"
    Resume  'debug
End Sub


Private Function IsThisAToolbar(Band As ActiveBar2LibraryCtl.Band) As Boolean
    'Process toolbars only.  The name must start with tbr.
    IsThisAToolbar = (Left$((Band.Name), 3) = "tbr")
End Function

Private Function ToolIconFilename(Tool As ActiveBar2LibraryCtl.Tool) As String
    Dim result As String
    
    result = Tool.TagVariant & ""               'could be null or empty
    
    If Len(result) > 0 Then
        If Asc(Right$(result, 1)) = 0 Then
            'Strings set at design time have a trailing nul
            result = Left$(result, Len(result) - 1)     'strip the trailing nul
        End If
    End If

    ToolIconFilename = result
End Function

Private Function ImageExists(iml As ImageList, image_key As String) As Boolean
    On Error GoTo errHandler
    Dim i As Long

    i = iml.ListImages.item(image_key).Index
    ImageExists = True
    Exit Function
    
errHandler:
    ImageExists = False
End Function


'Private Sub LoadIconsWin2KLook(abr As ActiveBar2, large_icons As Boolean, ImageList16 As ImageList, ImageList32 As ImageList)
'    On Error GoTo errHandler
'
'    Dim oBand As ActiveBar2LibraryCtl.Band
'    Dim oTool As ActiveBar2LibraryCtl.Tool
'    Dim icon_name As String, desc As String
'
'    'Save the 16x16 icons the first time through
'    '
'    desc = "Saving 16x16 icons"
'    If ImageList16.ListImages.count = 0 Then
'        For Each oBand In abr.Bands
'            If IsThisAToolbar(oBand) Then
'                For Each oTool In oBand.Tools
'                    icon_name = ToolIconFilename(oTool)
'                    If Len(icon_name) > 4 Then
'                        If Not ImageExists(ImageList16, icon_name) Then
'                            ImageList16.ListImages.Add , icon_name, oTool.GetPicture(ddITNormal)
'                        End If
'                    End If
'                Next oTool
'            End If
'        Next oBand
'    End If
'
'    'Set large or small icons in the toolbar only; keep small icons in the menu
'    '
'    desc = "loading icons"
'    For Each oBand In abr.Bands
'        If IsThisAToolbar(oBand) Then
'            For Each oTool In oBand.Tools
'                icon_name = ToolIconFilename(oTool)
'                If Len(icon_name) > 4 Then
'                    If large_icons Then
'                        oTool.SetPicture ddITNormal, ImageList32.ListImages.item(icon_name).Picture
'                    Else
'                        oTool.SetPicture ddITNormal, ImageList16.ListImages.item(icon_name).Picture
'                    End If
'                End If
'            Next oTool
'        End If
'    Next oBand
'
'    Exit Sub
'
'errHandler:
'    g_util.MsgBoxError desc & ": " & icon_name
'    Resume Next
'End Sub


Private Sub LoadIconsXPLook( _
    abr As ActiveBar2, _
    large_icons As Boolean, ImageList32 As ImageList, _
    coll_16 As Collection, coll_32 As Collection _
)
    On Error GoTo errHandler
    
    Dim oBand As ActiveBar2LibraryCtl.Band
    Dim oTool As ActiveBar2LibraryCtl.Tool
    Dim iTool As Integer
    Dim newTool As ActiveBar2LibraryCtl.Tool
    Dim icon_name As String, desc As String
    Dim is_tool_enabled As Boolean
    Dim is_tool_visible As Boolean

    If coll_16 Is Nothing Then Set coll_16 = New Collection
    If coll_32 Is Nothing Then Set coll_32 = New Collection

    'Problem: Tools with the XP Look only generate their hover and disabled icons once.
    'Problem: The Visible attribute is applied to all tools with the same ID; this means
    '         that menu items will disappear along with toolbar items if we try to put both
    '         16x and 32x tools in the toolbar and hide one set or the other.

    'Create the 16x16 & 32x32 tool collections the first time.
    'Replace .ico with _16 or _32 in the icon filename.
    
    If coll_16.count = 0 Then
        desc = "Saving 16x16 icons"
        For Each oBand In abr.Bands
            If IsThisAToolbar(oBand) Then
                For Each oTool In oBand.Tools
                    icon_name = ToolIconFilename(oTool)
                    If Len(icon_name) > 4 Then
                        If Right$(icon_name, 4) = ".ico" Then
                            'Create a new button with a 32x32 icon; add to the tools collection
                            '(This does not put the tool in the toolbar; this is at the abr level)
                            Set newTool = abr.Tools.Add(oTool.id, oTool.Name)

                            newTool.TagVariant = Left$(icon_name, Len(icon_name) - 4) & "_32"
                            newTool.ToolTipText = oTool.ToolTipText
                            newTool.Visible = oTool.Visible
                            
                            If ImageExists(ImageList32, icon_name) Then
                                newTool.SetPicture ddITNormal, ImageList32.ListImages(icon_name).Picture
                            Else
                                newTool.SetPicture ddITNormal, oTool.GetPicture(ddITNormal)
                            End If

                            oTool.TagVariant = Left$(icon_name, Len(icon_name) - 4) & "_16"
                            
                            coll_16.Add oTool, oTool.Name
                            coll_32.Add newTool, newTool.Name
                        End If
                    End If
                Next oTool
            End If
        Next oBand
    End If
    
    desc = "Loading requested icons"
    For Each oBand In abr.Bands
        If IsThisAToolbar(oBand) Then
            For iTool = 0 To oBand.Tools.count - 1
                Set oTool = oBand.Tools(iTool)
                
                is_tool_enabled = oTool.Enabled
                is_tool_visible = oTool.Visible
                icon_name = ToolIconFilename(oTool)
                
                If Len(icon_name) > 4 Then
                    If large_icons Then
                        If Right$(icon_name, 3) = "_16" Then
                            oBand.Tools.Remove iTool
                            oBand.Tools.Insert iTool, coll_32.item(oTool.Name)
                        End If
                    Else
                        If Right$(icon_name, 3) = "_32" Then
                            oBand.Tools.Remove iTool
                            oBand.Tools.Insert iTool, coll_16.item(oTool.Name)
                        End If
                    End If
                    
                    oBand.Tools(iTool).Enabled = is_tool_enabled
                    oBand.Tools(iTool).Visible = is_tool_visible
                End If
            Next iTool
        End If
    Next oBand
    
    Exit Sub
    
errHandler:
    g_util.MsgBoxError desc & ": " & icon_name
    Resume Next
End Sub

