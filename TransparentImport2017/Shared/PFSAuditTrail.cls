VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PFSAuditTrail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'
'PFSAuditTrail: Add to the audit trail
'

'Audit trail event ids
'
'>> These should match what is in LOOKUP_VALUES for lookup list #44     <<
'>>                                                                     <<
'>> IF YOU ADD TO THIS LIST, BE SURE TO ADD TO THE DATABASE AS WELL.    <<
'>> DO NOT CHANGE ANY EXISTING NUMBERS!                                 <<
'
Public Enum AuditTrailEventID
    AUDIT_NOTHING = 0
    
    'login/logout
    AUDIT_LOGIN = 1
    AUDIT_LOGOUT = 2
    AUDIT_LOGIN_ERROR = 3
    
    'manual ADT
    AUDIT_ADMIT = 10
    AUDIT_TRANSFER = 11
    AUDIT_DISCHARGE = 12
    AUDIT_UPDATE = 13
    
    AUDIT_CANCEL_ADMIT = 20
    AUDIT_CANCEL_TRANSFER = 21
    AUDIT_CANCEL_DISCHARGE = 22
    
    'manual entry
    AUDIT_CLASSIFY = 30
    AUDIT_PROCEDURE = 31
    AUDIT_MONITOR = 32
    AUDIT_EDIT_CLASSIFICATION = 33
    AUDIT_EDIT_PROCEDURE = 34
    AUDIT_EDIT_MONITOR = 35
    
    'manual delete
    AUDIT_DELETE_CLASSIFICATION = 40
    AUDIT_DELETE_MONITOR = 41
    AUDIT_DELETE_PROCEDURE = 42
    AUDIT_DELETE_ENCOUNTER = 43
    
    'soft delete/restore
    AUDIT_SOFT_DELETE_CLASSIFICATION = 180
    AUDIT_SOFT_DELETE_PROCEDURE = 181
    AUDIT_SOFT_DELETE_DO_NOT_CLASSIFY = 182
    AUDIT_SOFT_RESTORE_CLASSIFICATION = 185
    AUDIT_SOFT_RESTORE_PROCEDURE = 186
    AUDIT_SOFT_RESTORE_DO_NOT_CLASSIFY = 187
    
    'reports
    AUDIT_RUN_REPORT = 50
    
    'report notes
    AUDIT_ADD_REPORT_NOTE = 60
    AUDIT_EDIT_REPORT_NOTE = 61
    AUDIT_DELETE_REPORT_NOTE = 62
    
    'staffing data entry
    AUDIT_SAVE_SCHEDULED_STAFFING = 70
    AUDIT_COPY_SCHEDULED_STAFFING = 71
    AUDIT_DELETE_SCHEDULED_STAFFING = 72
    
    AUDIT_SAVE_ACTUAL_STAFFING = 80
    AUDIT_COPY_ACTUAL_STAFFING = 81
    AUDIT_DELETE_ACTUAL_STAFFING = 82
    
    'Misc actions
    AUDIT_SELECT_UNIT_MAIN = 90                 'Select unit (main window)

    'Manage Users
    AUDIT_NEW_USER = 100
    AUDIT_ACTIVATE_USER = 101
    AUDIT_DEACTIVATE_USER = 102
        
    'Outcomes Module
    AUDIT_OUTCOMES_CLASSIFY = 110
    AUDIT_OUTCOMES_EDIT_CLASSIFICATION = 111
    AUDIT_OUTCOMES_DELETE_CLASSIFICATION = 112

    'Do Not Classify
    AUDIT_DO_NOT_CLASSIFY = 120
    AUDIT_EDIT_DO_NOT_CLASSIFY = 121
    AUDIT_DELETE_DO_NOT_CLASSIFY = 122
    
    'Support functions
    AUDIT_RUN_SQL = 130
    
    'Other workload (a.k.a Other Activity)
    AUDIT_ADD_OTHER_WORKLOAD = 140
    AUDIT_EDIT_OTHER_WORKLOAD = 141
    AUDIT_DELETE_OTHER_WORKLOAD = 142

    'Manage Security Groups
    AUDIT_ADD_GROUP = 150
    AUDIT_DELETE_GROUP = 151
    AUDIT_USER_ADDED_TO_GROUP = 152
    AUDIT_USER_REMOVED_FROM_GROUP = 153
    AUDIT_FUNCTION_ADDED_TO_GROUP = 154
    AUDIT_FUNCTION_REMOVED_FROM_GROUP = 155
    AUDIT_REPORT_ADDED_TO_GROUP = 156
    AUDIT_REPORT_REMOVED_FROM_GROUP = 157
    AUDIT_UNIT_ADDED_TO_GROUP = 158
    AUDIT_UNIT_REMOVED_FROM_GROUP = 159

    'Assignment Sheet
    AUDIT_SAVE_ASSIGNMENT_SHEET = 170
    AUDIT_UPDATE_ASSIGNMENT_SHEET = 171
    AUDIT_DELETE_ASSIGNMENT_SHEET = 172
End Enum


' Add an item to the audit trail
'   * AuditEvent is required.
'   * User may be zero but should always be filled in except for a login failure.
'   * Unit and encounter ids may be zero.
'   * Description, ReasonCode and FreeText may all be null.
'
Public Sub AddAuditTrailItem( _
    ByVal AuditEventID As AuditTrailEventID, _
    ByVal UserID As Long, _
    ByVal UnitID As Long, _
    ByVal EncounterID As Long, _
    ByVal ReportID As Long, _
    ByRef Description As Variant, _
    ByRef ReasonCode As Variant, _
    ByRef FreeText As Variant _
)
    On Error GoTo errHandler

    Dim sql As String
    Dim cn As Connection

    Set cn = g_dbutil.NewRemoteConnection()

    sql = "INSERT INTO AUDIT_TRAIL (" & _
         "      TIMESTAMP, AUDIT_EVENT_ID, USER_ID, UNIT_ID, ENCOUNTER_ID, REPORT_ID," & _
         "      DESCRIPTION, AUDIT_REASON_LVC, REASON" & _
         " ) VALUES (" & _
                "GETDATE()," & _
                AuditEventID & "," & _
                g_dbutil.SQL_ZeroToNull(UserID) & "," & _
                g_dbutil.SQL_ZeroToNull(UnitID) & "," & _
                g_dbutil.SQL_ZeroToNull(EncounterID) & "," & _
                g_dbutil.SQL_ZeroToNull(ReportID) & "," & _
                g_dbutil.SQL_String(Description) & "," & _
                g_dbutil.SQL_String(ReasonCode) & "," & _
                g_dbutil.SQL_String(FreeText) & _
         " )"
    Debug.Print "Audit event=" & AuditEventID & _
        " user=" & UserID & " unit=" & UnitID & " enc=" & EncounterID & " report=" & ReportID & _
        " desc=" & Description & _
        " reason=" & ReasonCode & " text=" & FreeText
    cn.Execute sql
    cn.Close
    Exit Sub

errHandler:
    If InStr(1, Err.Description, SQL_INSTR_DUPLICATE_KEY) Then
        'Try again with a new timestamp
        Resume
    End If
    g_util.ThrowError "AddAuditTrailItem"
    Resume
End Sub


