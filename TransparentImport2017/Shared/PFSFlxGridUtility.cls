VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PFSFlxGridUtility"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'
' Utility functions for MSFlexGrid
'
' For basic editing, put a grid and a hidden text box on your form.
' * Call grd_EnterCell from the grid's EnterCell event.
' * Call the txt_Key events from the textbox events.
' * Update the grid cell from the textbox Change event.
'
Private m_shift_key_down            As Boolean


Public Sub SetHeadingInfo(grd As MSFlexGrid, col As Integer, title As String, width As Integer, Optional is_visible As Boolean = True, Optional align As Integer = flexAlignLeftCenter)
    With grd
        .TextMatrix(0, col) = title
        .ColWidth(col) = width * g_display.StatusFont.Size / 8.25
        .ColAlignment(col) = align
        
        '.ColIsVisible(col) = is_visible                'not supported!
        If Not is_visible Then
            .TextMatrix(0, col) = ""
            .ColWidth(col) = 0
            .AllowUserResizing = flexResizeNone         'this is needed to keep it hidden
        End If
    End With
End Sub


'==============================================================================
' USER EDITING OF CELL VALUES
'==============================================================================
Public Function ShiftKeyIsDown() As Boolean
    ShiftKeyIsDown = m_shift_key_down
End Function

Public Sub PositionTextBoxOnActiveCell(grd As MSFlexGrid, txt As TextBox)
    With txt
        'Note: the text box has a minimum height and may not get small enough if the grid row
        '      is not as tall as the text box wants it to be.
        .Move grd.left + grd.CellLeft, grd.top + grd.CellTop, grd.CellWidth, grd.CellHeight
        .Text = grd.Text
        .SelStart = 0
        .SelLength = Len(.Text)
        
        'Match the look of the grid
        .BackColor = grd.BackColor
        .ForeColor = grd.ForeColor
        .font = grd.font
        .Appearance = 0                     'flat
        .BorderStyle = 1                    'single
    
        .Visible = True
        'SetFocus will fail if we aren't on screen -- ignore error
        On Error Resume Next
        .SetFocus
    End With

End Sub

'
' Use these functions to implement text editing and cursor movement within the grid.
' Call them from the grid event they are named after.
'
Public Sub grd_EnterCell(grd As MSFlexGrid, txt As TextBox, loading_grid As Boolean)
    If loading_grid Then Exit Sub               'set this flag will loading the grid

    PositionTextBoxOnActiveCell grd, txt
End Sub


Public Sub txt_KeyDown(grd As MSFlexGrid, txt As TextBox, KeyCode As Integer, Shift As Integer)
    Dim row As Integer, col As Integer
    Dim row_direction As Integer, col_direction As Integer

    'Bail out if grid is empty
    If (grd.rows < 2) Or (grd.cols < 2) Then Exit Sub
    
    row_direction = 0
    col_direction = 0
    'Debug.Print "keycode: " & KeyCode & "  Time:  " & Now
    
    Select Case KeyCode
    Case vbKeyDown
        row_direction = 1
    Case vbKeyUp
        row_direction = -1
    Case vbKeyLeft
        col_direction = -1
    Case vbKeyRight
        col_direction = 1
    Case vbKeyReturn
        col_direction = 1
    'Note: Tab only works if txtEditCell is the only control with a tabstop
    Case vbKeyTab
        If (Shift) Then
            col_direction = -1
        Else
            col_direction = 1
        End If
    Case vbKeyShift
        m_shift_key_down = True
    End Select
        
    If (row_direction = 0) And (col_direction = 0) Then Exit Sub

    'Do not wrap the cursor when moving up and down
    row = grd.row + row_direction
    row = g_util.Max(row, 1)
    row = g_util.Min(row, grd.rows - 1)

    'Wrap to the prev/next row when moving left or right
    col = grd.col + col_direction
    If (col < 1) Then
        'wrap up a row if not already on first row
        If (row <= 1) Then
            col = 1
        Else
            col = grd.cols - 1
            row = row - 1
        End If
    ElseIf (col >= grd.cols) Then
        'wrap down a row if not already on last row
        If (row = grd.rows - 1) Then
            col = grd.cols - 1
        Else
            col = 1
            row = row + 1
        End If
    End If

    'This key has been processed; don't let the text box get it.
    KeyCode = 0
    
    'This will fire grd_EnterCell to position the text box
    grd.row = row
    grd.col = col
End Sub


Public Sub txt_KeyUp(grd As MSFlexGrid, txt As TextBox, KeyCode As Integer, Shift As Integer)
    If (KeyCode = vbKeyShift) Then m_shift_key_down = False
End Sub


'==============================================================================
' SAVE CONTENTS IN A FILE
'==============================================================================
Public Sub SaveAsCSV(grd As MSFlexGrid, CommonDialog1 As Control, Optional with_headers As Boolean = False)
    On Error GoTo errHandler
    Dim filter As String, result As String
    Dim row As Long, col As Long, start_row As Long

    start_row = IIf(with_headers, 0, 1)

    For row = start_row To grd.rows - 1
        For col = 0 To grd.cols - 1
            If (col <> 0) Then result = result & ","
            result = result & """" & grd.TextMatrix(row, col) & """"
        Next col
        result = result & vbCrLf
    Next row
    
    filter = "All Files (*.*)|*.*|Comma Separated(*.csv)|*.csv|"
    
    g_util.FileSaveAs CommonDialog1, result, filter
    Exit Sub
    
errHandler:
    g_util.MsgBoxError , "SaveAs"
End Sub
