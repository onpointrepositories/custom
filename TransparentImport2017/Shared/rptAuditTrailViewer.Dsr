VERSION 5.00
Begin {9EB8768B-CDFA-44DF-8F3E-857A8405E1DB} rptAuditTrailViewer 
   Caption         =   "Audit Trail Viewer"
   ClientHeight    =   5430
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   16260
   Icon            =   "rptAuditTrailViewer.dsx":0000
   StartUpPosition =   2  'CenterScreen
   _ExtentX        =   28681
   _ExtentY        =   9578
   SectionData     =   "rptAuditTrailViewer.dsx":06C2
End
Attribute VB_Name = "rptAuditTrailViewer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'
' Audit Trail viewer
'
Private m_row       As Long
Private m_res       As Variant      'matrix of data
Private m_colored   As Boolean
Private Const YLW = &HE0FFFF
Private Const WHT = &HFFFFFF

Public Sub Setup(ownerForm As Form, res() As Variant, date_range As String, top_recs As Boolean)
    g_util.CursorPushHourglass
    m_res = res
    lblDateRange.caption = date_range
    If top_recs Then lblDateRange.caption = lblDateRange.caption & " (first " & UBound(m_res, 1) & " records)"

    ConfigColumns
    g_util.CursorPop
    
    g_arutil.SetupPreviewWindow Me, vbModal, ownerForm
End Sub

Private Sub ConfigColumns()
    Dim i               As Integer
    Dim j               As Integer
    Dim left            As Long
    Dim col_empty       As Boolean
    Dim vis_col_width   As Long
        
    SetTextBoxWidth
    
    'timestamp
    col_empty = ColumnIsEmpty(1)
    lblTimestamp.left = left:   lblTimestamp.Visible = Not col_empty
    txtTimestamp.left = left:   txtTimestamp.Visible = Not col_empty
    txtTimestamp.width = lblTimestamp.width
    If Not col_empty Then left = left + txtTimestamp.width
    
    'event
    col_empty = ColumnIsEmpty(2)
    lblEvent.left = left:   lblEvent.Visible = Not col_empty
    txtEvent.left = left:   txtEvent.Visible = Not col_empty
    txtEvent.width = lblEvent.width
    If Not col_empty Then left = left + txtEvent.width
    
    'user
    col_empty = ColumnIsEmpty(3)
    lblUser.left = left:   lblUser.Visible = Not col_empty
    txtUser.left = left:   txtUser.Visible = Not col_empty
    txtUser.width = lblUser.width
    If Not col_empty Then left = left + txtUser.width
    
    'unit
    col_empty = ColumnIsEmpty(4)
    lblUnit.left = left:   lblUnit.Visible = Not col_empty
    txtUnit.left = left:   txtUnit.Visible = Not col_empty
    txtUnit.width = lblUnit.width
    If Not col_empty Then left = left + txtUnit.width
    
    'patient
    col_empty = ColumnIsEmpty(5)
    lblPatient.left = left:   lblPatient.Visible = Not col_empty
    txtPatient.left = left:   txtPatient.Visible = Not col_empty
    txtPatient.width = lblPatient.width
    If Not col_empty Then left = left + txtPatient.width
    
    'reason
    col_empty = ColumnIsEmpty(6)
    lblReason.left = left:   lblReason.Visible = Not col_empty
    txtReason.left = left:   txtReason.Visible = Not col_empty
    txtReason.width = lblReason.width
    If Not col_empty Then left = left + txtReason.width
    
    'free text
    col_empty = ColumnIsEmpty(7)
    lblFreeText.left = left:   lblFreeText.Visible = Not col_empty
    txtFreeText.left = left:   txtFreeText.Visible = Not col_empty
    txtFreeText.width = lblFreeText.width
    If Not col_empty Then left = left + txtFreeText.width
    
    'report
    col_empty = ColumnIsEmpty(8)
    lblReport.left = left:   lblReport.Visible = Not col_empty
    txtReport.left = left:   txtReport.Visible = Not col_empty
    txtReport.width = lblReport.width
    If Not col_empty Then left = left + txtReport.width
    
    'description
    col_empty = ColumnIsEmpty(9)
    lblDescription.left = left:   lblDescription.Visible = Not col_empty
    txtDescription.left = left:   txtDescription.Visible = Not col_empty
    txtDescription.width = lblDescription.width
    If Not col_empty Then left = left + txtDescription.width
    
    'align heareds
    lblTitle.width = left
    lblDateRange.width = left

End Sub

Private Sub SetTextBoxWidth()
    Dim tot_wght As Integer
    Const MAX_WIDTH = 12000             'usable page width
    Const TMSTMP_WIDTH = 2090

    'Fill the available space; eliminate empty columns
    If Not ColumnIsEmpty(2) Then tot_wght = tot_wght + 13   'event
    If Not ColumnIsEmpty(3) Then tot_wght = tot_wght + 10   'user
    If Not ColumnIsEmpty(4) Then tot_wght = tot_wght + 9    'unit
    If Not ColumnIsEmpty(5) Then tot_wght = tot_wght + 11   'patient
    If Not ColumnIsEmpty(6) Then tot_wght = tot_wght + 14   'reason
    If Not ColumnIsEmpty(7) Then tot_wght = tot_wght + 18   'text
    If Not ColumnIsEmpty(8) Then tot_wght = tot_wght + 10   'report
    If Not ColumnIsEmpty(9) Then tot_wght = tot_wght + 18   'desc
    lblTimestamp.width = TMSTMP_WIDTH
    lblEvent.width = MAX_WIDTH * (13 / tot_wght)
    lblUser.width = MAX_WIDTH * (10 / tot_wght)
    lblUnit.width = MAX_WIDTH * (9 / tot_wght)
    lblPatient.width = MAX_WIDTH * (11 / tot_wght)
    lblReason.width = MAX_WIDTH * (14 / tot_wght)
    lblFreeText.width = MAX_WIDTH * (18 / tot_wght)
    lblReport.width = MAX_WIDTH * (10 / tot_wght)
    lblDescription.width = MAX_WIDTH * (18 / tot_wght)
End Sub

Private Function ColumnIsEmpty(col As Integer) As Boolean
    Dim i As Long
    
    For i = 1 To UBound(m_res, 1)
        If Len(m_res(i, col)) > 0 Then Exit Function
    Next i
    ColumnIsEmpty = True
End Function

Private Sub ActiveReport_DataInitialize()
    On Error GoTo errHandler
    fields.Add "Timestamp"
    fields.Add "Event"
    fields.Add "User"
    fields.Add "Unit"
    fields.Add "Patient"
    fields.Add "Reason"
    fields.Add "FreeText"
    fields.Add "Report"
    fields.Add "Description"
    m_row = 0
    Exit Sub
errHandler:
    g_util.MsgBoxError
End Sub

Private Sub ActiveReport_FetchData(eof As Boolean)
    m_row = m_row + 1
    eof = (m_row > UBound(m_res))
    If eof Or UBound(m_res) = 0 Then Exit Sub

    fields("Timestamp") = m_res(m_row, 1)
    fields("Event") = m_res(m_row, 2)
    fields("User") = m_res(m_row, 3)
    fields("Unit") = m_res(m_row, 4)
    fields("Patient") = m_res(m_row, 5)
    fields("Reason") = m_res(m_row, 6)
    fields("FreeText") = m_res(m_row, 7)
    fields("Report") = m_res(m_row, 8)
    If Len(m_res(m_row, 9)) > 255 Then
        fields("Description") = left$(m_res(m_row, 9), 255) & "..."
    Else
        fields("Description") = m_res(m_row, 9)
    End If
End Sub

Private Sub ActiveReport_ToolbarClick(ByVal Tool As DDActiveReports2.DDTool)
    g_arutil.ProcessToolbarClick Tool.caption, Me, Me.caption
End Sub

Private Sub PageFooter_Format()
    lblVersion.caption = g_arutil.FooterVersion()
    RunDate.Text = "Run Date: " & g_dbutil.LocalDateTime()
End Sub

Private Sub Detail_Format()
    Dim ctl As Object
    
    On Error Resume Next
    
    'Alternate rows get color
    If m_colored = True Then
        For Each ctl In Detail.Controls
            ctl.BackColor = WHT
        Next
        m_colored = False
    Else
        For Each ctl In Detail.Controls
            ctl.BackColor = YLW
        Next
        m_colored = True
    End If
End Sub

Private Sub Detail_BeforePrint()

    Dim ctl As Object
    Dim max_height As Integer
    Dim i As Integer
    
    On Error Resume Next
    
    'Get the tallest text box in the row
    For Each ctl In Detail.Controls
        If Mid$(ctl.Name, 1, 3) = "txt" Then
            i = i + 1
            If i = 1 Then
                max_height = ctl.height
            Else
                If max_height < ctl.height Then max_height = ctl.height
            End If
        End If
    Next
    
    'Make all the same height
    For Each ctl In Detail.Controls
        If Mid$(ctl.Name, 1, 3) = "txt" Then
            ctl.height = max_height
        End If
    Next
End Sub

