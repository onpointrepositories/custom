VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PFSDisplayProperties"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'
' PFSDisplayProperties.cls: fetch the Windows display properties
'
' REQUIRES: Microsoft Windows Controls-2 6.0 component
'
' These properties can be used to change fonts in the program to match what
' has been selected in the windows control panel.
'
' You can also figure out the usable screen size.
'

'These are bitmasks - increase by powers of two
Public Enum SetFontOptions
    SETFONT_RESIZE_NONE = 0
    SETFONT_RESIZE_DTPICKER = 1
    SETFONT_RESIZE_TEXTBOX = 2

    SETFONT_RESIZE_ALL_BOXES = 3
End Enum

'
'Constants and types from WIN32API.TXT
'

' Logical Font
Private Const LF_FACESIZE = 32
Private Const LF_FULLFACESIZE = 64

Private Type LOGFONT
        lfHeight As Long
        lfWidth As Long
        lfEscapement As Long
        lfOrientation As Long
        lfWeight As Long
        lfItalic As Byte
        lfUnderline As Byte
        lfStrikeOut As Byte
        lfCharSet As Byte
        lfOutPrecision As Byte
        lfClipPrecision As Byte
        lfQuality As Byte
        lfPitchAndFamily As Byte
        lfFaceName(1 To LF_FACESIZE) As Byte
End Type

Private Type NONCLIENTMETRICS
        cbSize As Long
        iBorderWidth As Long
        iScrollWidth As Long
        iScrollHeight As Long
        iCaptionWidth As Long
        iCaptionHeight As Long
        lfCaptionFont As LOGFONT
        iSMCaptionWidth As Long
        iSMCaptionHeight As Long
        lfSMCaptionFont As LOGFONT
        iMenuWidth As Long
        iMenuHeight As Long
        lfMenuFont As LOGFONT
        lfStatusFont As LOGFONT
        lfMessageFont As LOGFONT
End Type

Private Type ICONMETRICS
    cbSize As Long
    iHorzSpacing As Long
    iVertSpacing As Long
    iTitleWrap As Long
    lfFont As LOGFONT
End Type

Private Type RECT
        left As Long
        top As Long
        Right As Long
        Bottom As Long
End Type

Private Const SPI_GETNONCLIENTMETRICS = 41
Private Const SPI_GETICONMETRICS = 45
Private Const SPI_GETWORKAREA = 48

Private Declare Function SystemParametersInfo Lib "user32" Alias "SystemParametersInfoA" (ByVal uAction As Long, ByVal uParam As Long, ByRef lpvParam As Any, ByVal fuWinIni As Long) As Long


Private m_ncm               As NONCLIENTMETRICS
Private m_iconm             As ICONMETRICS
Private m_workarea          As RECT

Private m_border_width      As Single           'converted to TWIPS
Private m_caption_height    As Single
Private m_menu_height       As Single

Private m_wka_left          As Single           'converted to TWIPS
Private m_wka_top           As Single
Private m_wka_width         As Single
Private m_wka_height        As Single

Private m_caption_font      As StdFont
Private m_menu_font         As StdFont
Private m_status_font       As StdFont          'AKA ToolTip
Private m_message_font      As StdFont
Private m_icon_font         As StdFont


Private Sub Class_Initialize()
    Load
End Sub


Private Sub Load()
    On Error GoTo errHandler
    Dim result As Long
    
    m_ncm.cbSize = Len(m_ncm)
    result = SystemParametersInfo( _
                SPI_GETNONCLIENTMETRICS, _
                0, _
                m_ncm, 0)
    
    m_iconm.cbSize = Len(m_iconm)
    result = SystemParametersInfo( _
                SPI_GETICONMETRICS, _
                0, _
                m_iconm, 0)

    result = SystemParametersInfo( _
                SPI_GETWORKAREA, _
                0, _
                m_workarea, 0)

    With m_ncm
        'The iBorderWidth does not include the middle(1) and shadow(2)
        m_border_width = (1 + 2 + .iBorderWidth) * Screen.TwipsPerPixelY
        m_caption_height = .iCaptionHeight * Screen.TwipsPerPixelY
        m_menu_height = .iMenuHeight * Screen.TwipsPerPixelY
        
        Set m_caption_font = ConvertFont("caption", .lfCaptionFont)
        Set m_menu_font = ConvertFont("menu", .lfMenuFont)
        Set m_status_font = ConvertFont("status", .lfStatusFont)
        Set m_message_font = ConvertFont("message", .lfMessageFont)
    End With
    
    With m_workarea
        m_wka_left = .left * Screen.TwipsPerPixelX
        m_wka_top = .top * Screen.TwipsPerPixelY
        m_wka_width = (.Right - .left) * Screen.TwipsPerPixelX
        m_wka_height = (.Bottom - .top) * Screen.TwipsPerPixelY
    End With
        
    Set m_icon_font = ConvertFont("icon", m_iconm.lfFont)
    Exit Sub
    
errHandler:
    g_util.ThrowError "PFSDisplayProperties"
    Resume  'debug
End Sub

Private Function ConvertFont(desc As String, lf As LOGFONT) As StdFont
    Dim result As New StdFont
    
    With lf
        'Must convert the font name from ANSI to Unicode
        'Convert pixels to points; divide points by 20 to get TWIPS.
        'Debug.Print "  height = "; .lfHeight; "  weight(bold) = "; .lfWeight; "  italic = "; .lfItalic
        
        result.Weight = .lfWeight               '400=regular, 700=bold
        result.Italic = (.lfItalic <> 0)        '1 or 0
        result.Size = (-.lfHeight) * Screen.TwipsPerPixelY / 20#
        result.Name = StrConv(lf.lfFaceName, vbUnicode)
    End With
    
    Set ConvertFont = result
End Function

Public Sub Refresh()
    Load
End Sub


Public Function BorderWidth() As Single
    BorderWidth = m_border_width
End Function

Public Function CaptionHeight() As Single
    CaptionHeight = m_caption_height
End Function

Public Function MenuHeight() As Single
    MenuHeight = m_menu_height
End Function


Public Function WorkAreaLeft() As Single
    WorkAreaLeft = m_wka_left
End Function

Public Function WorkAreaTop() As Single
    WorkAreaTop = m_wka_top
End Function

Public Function WorkAreaWidth() As Single
    WorkAreaWidth = m_wka_width
End Function

Public Function WorkAreaHeight() As Single
    WorkAreaHeight = m_wka_height
End Function


Public Function CaptionFont() As StdFont
    Set CaptionFont = m_caption_font
End Function

Public Function IconFont() As StdFont
    Set IconFont = m_icon_font
End Function

Public Function MenuFont() As StdFont
    Set MenuFont = m_menu_font
End Function

Public Function MessageFont() As StdFont
    Set MessageFont = m_message_font
End Function

Public Function StatusFont() As StdFont
    Set StatusFont = m_status_font
End Function

Public Function ToolTipFont() As StdFont
    Set ToolTipFont = m_status_font
End Function


Private Function UseFlatWhiteTheme() As Boolean
    'UseFlatWhiteTheme = True
    UseFlatWhiteTheme = g_util.IsWindows8OrLater()
End Function

Private Sub DoSetTheme(ByRef frm As Form, ByRef font As StdFont, resize_options As SetFontOptions)
    'Not all controls have all properties -- keep going on error
    On Error Resume Next
    
    Dim ctl As Control
    Dim adjust_size As Boolean
    Dim old_font_size As Single, adjust As Single

    Const WC_LIGHT_BLUE = &HFFC0C0

    If UseFlatWhiteTheme() Then
        frm.BackColor = vbWhite
    End If
    
    ' This contains all controls on the form, even nested ones
    '
    For Each ctl In frm.Controls
        'Debug.Print "control.name=" & ctl.name
        old_font_size = ctl.font.Size

        If 0 Then
            'This will copy a pointer to the font in all controls - don't do it.
            Set ctl.font = font
        Else
            'This will copy the font attributes to the existing font object in each control
            ctl.font.Name = font.Name
            ctl.font.Size = font.Size
            ctl.font.Bold = font.Bold
            ctl.font.Italic = font.Italic
        End If
        
        adjust = font.Size / old_font_size

        'Combo box resize themselves to match the font; date pickers and text boxes do not.
        'The "correct" adjustment end up being too big and different for each control.
        
        adjust_size = False
        'If (resize_options And SETFONT_RESIZE_COMBOBOX) And (TypeOf ctl Is ComboBox) Then adjust_size = True
        If (resize_options And SETFONT_RESIZE_DTPICKER) And (TypeOf ctl Is DTPicker) Then
            adjust_size = True
            If (adjust > 1) Then adjust = 0.9 * adjust
        End If
        If (resize_options And SETFONT_RESIZE_TEXTBOX) And (TypeOf ctl Is TextBox) Then
            adjust_size = True
            If (adjust > 1) Then adjust = 0.7 * adjust
        End If

        If adjust_size Then
            ctl.width = ctl.width * adjust
            ctl.height = ctl.height * adjust
        End If

        'hide all the (yellow) helper picture boxes - set to form color
        If (TypeOf ctl Is PictureBox) Then
            ctl.BackColor = frm.BackColor
        End If
        
        If UseFlatWhiteTheme() Then
            ctl.Appearance = 0                              'flat
            
            ctl.BackColor = vbWhite
            
            'TrueDBGrid
            'ctl.InactiveBackColor = vbWhite                'only works on column headings
            
            'FlxGrid
            ctl.BackColorBkg = vbWhite
    
            'TabStrip - no color choices for the unused areas

        End If
NextCtl:
    Next ctl

    Exit Sub
    
errHandler:
    Resume NextCtl
End Sub

'depricated
'Public Sub SetFontInAllControls(ByRef frm As Form, ByRef font As StdFont, Optional resize_options As SetFontOptions = SETFONT_RESIZE_NONE)
'    DoSetTheme frm, font, resize_options                   'redirect to StdTheme
'End Sub

'
' Set a theme - always load the system font; hide the control helper picture boxes
' * Maybe flatten controls and set white background
'
Public Sub SetStdTheme(ByRef frm As Form, Optional load_harris_icon As Boolean = False, Optional resize_options As SetFontOptions = SETFONT_RESIZE_NONE)
    On Error GoTo errHandler

    If (load_harris_icon) Then
        'VB will only store a 16 color icon in the form designer but we can load a 32-bit color icon
        'at runtime.  This looks much better.
        'Set frm.Icon = g_util.QuadraMedIcon
        Set frm.Icon = g_util.HarrisIcon
    End If
    
    DoSetTheme frm, MessageFont, resize_options
    Exit Sub
errHandler:
    g_util.ThrowError "SetStdTheme"
End Sub

