VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PFSHelp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'
' PFSHelp: display online help
'
' Note:
' VB has a built-in help support for the F1 key and help context ids in forms,
' but it doesn't support menu or tool-bar "Help" items.  For this reason we
' catch the F1 key and call help ourselves.  This will also allow us to use
' different help viewers in the future.
'
' These contexts give a high-level view of what help is desired.
' The caller does not need to know how it works.
'
Public Enum PFSHelpContext
    HELP_NONE = 0
    
    HELP_INDEX = 5
    HELP_PATIENT_SELECTION = 25
    HELP_NEONATAL_DATA_ENTRY = 210
    HELP_FIND_PATIENTS = 30
    HELP_DELETE_ENCOUNTERS = 290
    
    HELP_CLASSIFICATION = 35
    HELP_EDIT_CLASS = 310
    HELP_VIEW_CLASS = 320
    HELP_DO_NOT_CLASSIFY = 330
    HELP_NEW_ACTIVITY = 340
    HELP_EDIT_ACTIVITY = 350
    
    HELP_CLASS_BY_PROFILE = 125
    HELP_CLASS_BY_TYPE = 127
    HELP_MONITOR = 130
    
    HELP_OTHER_WORKLOAD = 110
    
    HELP_SCHEDULED_STAFFING = 40
    HELP_ACTUAL_STAFFING = 45

    HELP_MANUAL_ADT = 95
    HELP_MANUAL_ADMIT = 120
    HELP_MANUAL_UPDATE = 260
    HELP_MANUAL_TRANSFER = 270
    HELP_MANUAL_DISCHARGE = 280
    
    HELP_AUDIT_TRAIL = 85
    HELP_AUDIT_REPORT_CALCS = 82
    HELP_EVENT_LOG_VIEWER = 115
    HELP_EVENT_FILTER = 117
    HELP_DATA_CONSISTENCY_CHECKS = 200
    
    HELP_STAFFING_FORECASTER = 100
    HELP_DISCHARGE_MANAGEMENT = 87
    
    HELP_IRRT_SETUP_TEST = 135
    HELP_IRRT_ASSIGN_TEST = 140
    HELP_IRRT_MANAGEMENT_REPORTS = 155
    HELP_IRRT_START_TEST = 145
    HELP_IRRT_VIEW_TEST_RESULTS = 150
    
    HELP_SYSTEM_MANAGER = 10
    HELP_SECURITY = 20
    HELP_SYSTEM_PARAMETERS = 15
    HELP_DATA_ENTRY = 50
    HELP_BATCH_COPY_UNIT_PARAMETERS = 300
    HELP_CHANGE_ANY_PASSWORD = 360
    
    HELP_STAFF_ASSIGNMENT = 180
    HELP_ASSIGNMENT_TEAM_MANAGER = 230
    HELP_ASSIGNMENT_ACTIVITY_MANAGER = 250
    HELP_ASSIGNMENT_RN_RATIO_MANAGER = 390
    HELP_ASSIGNMENT_EDIT_STAFF = 240
    HELP_ASSIGNMENT_OPTIONS = 220
    HELP_ASSIGNMENT_ADD_ACTIVITY = 370
    HELP_ASSIGNMENT_ADD_PINNED_PATIENT = 380
    
    HELP_STAFFING_NOTES = 105
    HELP_CHART_VIEWER = 190
    HELP_REPORTS = 55
    
    HELP_OUTCOMES_DATA_ENTRY = 160
    HELP_OUTCOMES_CLASSIFICATION = 165
    HELP_OUTCOMES_REPORT_BUILDER = 167
    HELP_OUTCOMES_REPORT_GENERATOR = 170

    HELP_DATABOOK_EXTRACT = 90
    HELP_CLASS_EXPORT = 70
    HELP_FINANCE_EXPORT = 60
    HELP_STAFF_EXPORT = 65
End Enum

'
' The dwData argument must be supplied with the appropriate value for the command:
'   HtmlHelp hwnd, "HHDemo.chm", HH_DISPLAY_TOPIC, ByVal "Sample.htm"
'   HtmlHelp hwnd, "HHDemo.chm", HH_DISPLAY_TOPIC, 0
'   HtmlHelp hwnd, "HHDemo.chm", HH_HELP_CONTEXT, ByVal 100&
'
' If the topic is not found, you will get a "page not found" error in the help window.
' If the context is not found, the help window does not appear.
'
' You can find the display topic by right-buttoning on a help page and asking
' for Properties.  The topic will be imbedded in the path string.
'
Private Declare Function HtmlHelp Lib "HHCtrl.ocx" Alias "HtmlHelpA" _
    (ByVal hWndCaller As Long, _
     ByVal pszFile As String, _
     ByVal uCommand As Long, _
     dwData As Any) As Long

Private Const HH_DISPLAY_TOPIC  As Long = &H0
Private Const HH_HELP_CONTEXT   As Long = &HF

Private Declare Function SendMessage Lib "user32" Alias "SendMessageA" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal wParam As Long, lParam As Any) As Long
Private Declare Function PostMessage Lib "user32" Alias "PostMessageA" (ByVal hWnd As Long, ByVal wMsg As Long, ByVal wParam As Long, ByVal lParam As Long) As Long

Private Const WM_CLOSE = &H10

'include ..\bin in the path so this will work in design mode too
Private Const MAIN_HELPFILE = "..\bin\AcuityPlus.chm"             'formerly WinPFS.chm

Private hWndHelp As Long


Private Sub Class_Terminate()
    On Error Resume Next
    Dim rc As Long
    
    'Close the help window if it is still open
    If hWndHelp <> 0 Then
        hWndHelp = 0
        
        'Don't wait for the other window - Post message instead of Send
        'rc = SendMessage(hWndHelp, WM_CLOSE, 0, ByVal 0&)
        rc = PostMessage(hWndHelp, WM_CLOSE, 0, 0)
    End If
End Sub


Public Sub ShowHelp(ByVal hWndParent As Long, ByVal pfs_context As PFSHelpContext)
    Dim helpfile As String
    Dim topic As String
    Dim context As Long

    'To find display topics in a help file, display the page you want
    'and right-button to get Properities.  The topic is imbedded in
    'the address string.  (You can drag select it and copy with ctrl-C.)
    '
    'Context numbers do not appear to visible in the page properties.
    'Context numbers are better because they are permanent where page
    'names may change.
    
    'In case may have more than one help file at some point
    helpfile = MAIN_HELPFILE

    topic = "introduc.htm"
    context = pfs_context

    If (context = 0) Then
        hWndHelp = HtmlHelp(hWndParent, helpfile, HH_DISPLAY_TOPIC, ByVal topic)
    Else
        hWndHelp = HtmlHelp(hWndParent, helpfile, HH_HELP_CONTEXT, ByVal context)
    End If

    If (hWndHelp = 0) Then
        If Not g_util.FileExists(helpfile) Then
            MsgBox "Could not find help file", vbExclamation
        ElseIf (context <> 0) Then
            MsgBox "Could not find help context " & context, vbExclamation
        Else
            MsgBox "Could not find help topic " & topic, vbExclamation
        End If
    End If
End Sub

