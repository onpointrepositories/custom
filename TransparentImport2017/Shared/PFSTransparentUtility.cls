VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PFSTransparentUtility"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'
' Transparent Classification utility
'
Private Const ALLOW_RECREATE_AUDIT = True       're-create audit if snapshot is not available?

'
' This is used to enable the audit buttons
'
Public Sub IsTransparentAuditSnapshotAvailable(class_id As Long, brief_exists As Boolean, verbose_exists As Boolean)
    On Error GoTo errHandler
    Dim cn As Connection
    Dim rs As New Recordset
    Dim sql As String, cmd As String
    Dim can_recreate As Boolean

    Set cn = g_dbutil.NewRemoteConnection

    brief_exists = False
    verbose_exists = False
    
    'join to event log with encounter to enable that index; then look for tc_event_id.
    'if this works, the classifications will have event snapshots.
    'Since this is for a specific class ID, don't exclude it if it is deleted (IS_DELETED)
    sql = "select " & _
        "    (case when brief_audit is null then 0 else 1 end) as brief_exists" & _
        "    ,(case when verbose_audit is null then 0 else 1 end) as verbose_exists" & _
        "    ,TC_Pull_DateTime" & _
        " from classification_event as c" & _
        " inner join event_log as log" & _
        "   on (log.encounter_id = c.encounter_id) and (log.tc_event_id = c.tc_event_id)" & _
        " where classification_event_id=" & class_id
    rs.Open sql, cn
    
    If Not rs.eof Then
    
        brief_exists = (rs("brief_exists") <> 0)
        verbose_exists = (rs("verbose_exists") <> 0)
    
    ElseIf (ALLOW_RECREATE_AUDIT) Then
        rs.Close
        
        'The classification must have the pull range filled in and the mapping program
        'must be present.
        sql = "select TC_pull_range" & _
            " from classification_event" & _
            " where classification_event_id=" & class_id
        rs.Open sql, cn
        
        If Not rs.eof Then
            cmd = App.Path & "\..\bin\" & TRANSPARENT_MAPPING_EXE_NAME
            can_recreate = (g_util.FileExists(cmd) And (IsNumeric(rs("TC_pull_range"))))
            If (can_recreate) Then
                brief_exists = can_recreate
                verbose_exists = can_recreate
            End If
        End If
        
    End If
    
    rs.Close
    cn.Close
    Exit Sub
    
errHandler:
    g_util.MsgBoxError "Is audit available"
    Exit Sub
    Resume  'debug
End Sub

'
' Display a classification transparent audit
'
Public Sub ShowAudit(class_id As Long, verbose As Boolean)
    On Error GoTo errHandler
    Dim cn As Connection
    Dim rs As New Recordset
    Dim sql As String, cmd As String, desc As String, acct_num As String
    Dim logname1 As String, logname2 As String
    Dim pull_dt As Date, eff_dt As Date, range As Integer
    Dim got_it As Boolean, can_recreate As Boolean
    Dim enc_id As Long, tc_event_id As Long, returncode As Long, unit_id As Long
    Dim frm As New frmPrint

    Set cn = g_dbutil.NewRemoteConnection

    '==========================================================================
    ' Get the classification transparent mapping info
    '==========================================================================
    'Since this is for a specific class ID, don't exclude it if it is deleted (IS_DELETED)
    sql = "select e.encounter_id, e.acct_number, unit_id, classification_datetime, effective_datetime_in, TC_Event_ID, TC_Pull_Datetime, TC_pull_range" & _
        " from classification_event as c" & _
        " inner join encounter as e on (e.encounter_id = c.encounter_id)" & _
        " where classification_event_id=" & class_id
    rs.Open sql, cn
    If Not rs.eof Then
        enc_id = rs("encounter_id")
        acct_num = rs("acct_number")
        unit_id = rs("unit_id")
        tc_event_id = g_dbutil.DBToLong(rs("tc_event_id"))      'could be null
        eff_dt = rs("effective_datetime_in")
        'In older systems, the TC_Pull_DateTime may not be filled in
        pull_dt = IIf(IsDate(rs("TC_Pull_DateTime")), rs("TC_Pull_DateTime"), rs("classification_datetime"))
        range = g_dbutil.DBToInteger(rs("TC_pull_range"))
        If (range = 0) Then range = 1440
    End If
    
    rs.Close

    If (range = 0) Then
        MsgBox "Could not load classification", vbCritical
        Exit Sub
    End If
    
    '==========================================================================
    'Maybe (in the future) will will copy the brief audit to the classification itself for
    'long term storage.  (and we can display here)
    '==========================================================================
    '... get from class event ...

    '==========================================================================
    ' Look for the audit snapshot in the event log
    '==========================================================================
    If (tc_event_id <> 0) Then
        If (verbose) Then
            sql = "select verbose_audit as txt,"
        Else
            sql = "select brief_audit as txt,"
        End If
        sql = sql & _
            "   mapper_version" & _
            " from event_log" & _
            " where encounter_id=" & enc_id & _
            " and tc_event_id=" & tc_event_id
        rs.Open sql, cn, adOpenStatic
        
        If Not rs.eof Then
            If Not IsNull(rs("txt")) Then
                frm.SetText rs("txt")
                frm.show vbModal
                got_it = True
            End If
        End If
        
        rs.Close
    End If
    
    If got_it Then Exit Sub

    '==========================================================================
    ' Maybe run the transparent audit program to generate an audit file
    '==========================================================================
    logname1 = App.Path & "\..\log\TransparentAudit.log"            'original location
    logname2 = g_util.DefaultLogPath() & "\TransparentAudit.log"    'new location
    
    cmd = App.Path & "\..\bin\" & TRANSPARENT_MAPPING_EXE_NAME
    can_recreate = (ALLOW_RECREATE_AUDIT And g_util.FileExists(cmd))

    desc = "The audit snapshot is not available"
    If can_recreate Then
        'just do it - don't ask
        'If (MsgBox(desc & ".  Would you like to recreate the audit?", vbYesNo) <> vbYes) Then Exit Sub
    Else
        MsgBox desc & " (sorry)", vbExclamation
        Exit Sub
    End If
    
    If (verbose) Then
        cmd = cmd & " -verbose"
    Else
        cmd = cmd & " -brief"
    End If
    cmd = cmd & " -n"                               'no output to transparent.txt
    cmd = cmd & " -unit_id=" & unit_id
    cmd = cmd & " -acct=" & acct_num
    cmd = cmd & " -pulldate=" & Format$(pull_dt, "yyyymmdd")
    cmd = cmd & " -pulltime=" & Format$(pull_dt, "hhnn")
    cmd = cmd & " -effdate=" & Format$(eff_dt, "yyyymmdd")
    cmd = cmd & " -efftime=" & Format$(eff_dt, "hhnn")
    cmd = cmd & " -range=" & range

    On Error Resume Next
    Kill logname1
    Kill logname2
    
    On Error GoTo errHandler
    Debug.Print cmd
    g_util.ExecuteAndWait cmd, returncode
    
    If g_util.FileExists(logname1) Then
        Shell "Notepad " & logname1, vbNormalFocus
    ElseIf g_util.FileExists(logname2) Then
        Shell "Notepad " & logname2, vbNormalFocus
    Else
        MsgBox "File does not exist: TransparentAudit.log", vbCritical + vbOKOnly, "Unexpected Error"
    End If
    
    cn.Close
    Exit Sub
    
errHandler:
    g_util.MsgBoxError "Showing Audit"
    Exit Sub
    Resume  'debug
End Sub

