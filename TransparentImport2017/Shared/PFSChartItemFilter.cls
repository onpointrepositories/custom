VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PFSChartItemFilter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'
' Chart Item Filter for the Chart Item Viewer
' This is to pass values between the viewer and the filter dialog
'

' FILTERS
Public UnitID               As Long
Public EncounterID          As Long
Public PullDatetime         As Variant      'datetime or null
Public MappingDatetime      As Variant      'datetime or null
Public RangeMins            As Long         '0=max
Public Code                 As String
Public Category             As String
Public Description          As String
Public FieldName            As String
Public OrderControl         As String
Public OrderID              As String
Public Result               As String

' DISPLAY COLUMNS
Public DisplayCount         As Boolean
Public DisplayUnit          As Boolean
Public DisplayAccount       As Boolean
Public DisplayEventTime     As Boolean
Public DisplayTimestamp     As Boolean
Public DisplayCode          As Boolean
Public DisplayCategory      As Boolean
Public DisplayDescription   As Boolean
Public DisplayFieldName     As Boolean
Public DisplayOrderControl  As Boolean
Public DisplayOrderID       As Boolean
Public DisplayResult        As Boolean
