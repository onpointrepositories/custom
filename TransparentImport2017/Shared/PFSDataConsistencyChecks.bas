Attribute VB_Name = "PFSDataConsistencyChecks"
Option Explicit
'
' Data Consistency Checks
'
' 1) Find problems          - option to log findings in event log
' 2) Fix selected problems  - option to log fixes in event log
'
' (this needs to be in a module in order to return an array of DCCElement)
'

'
' Data Consistency Check function ids
'
' These should match what is in LOOKUP_VALUES for lookup list #69
' ==== DO NOT CHANGE ANY EXISTING NUMBERS! ====
'
' IF YOU ADD TO THIS LIST, BE SURE TO ADD TO THE DATABASE AS WELL.
'
Public Enum DataConsistencyCheckID
    DCC_NOTHING = 0
    '
    'Patient-level checks
    '
    DCC_ZERO_LENGTH_CLASS = 1
    DCC_ZERO_LENGTH_PROC = 2

    DCC_CLASS_BEGINS_BEFORE_ARRIVAL = 3                 '(ends before departure)
    DCC_PROC_BEGINS_BEFORE_ARRIVAL = 4                  '  (no redirection)
    
    DCC_CLASS_ENDS_AFTER_DEPARTURE = 5                  '(begins before departure)
    DCC_PROC_ENDS_AFTER_DEPARURE = 6                    '  (no redirection)
    
    DCC_HANGING_CLASS = 7                               '(patient not in unit)
    DCC_HANGING_PROC = 8                                '  (no redirection)
    
    DCC_OVERLAPPING_CLASS = 9
    DCC_OPEN_ENDED_PROC = 10                            '(more than a day old)
    
    DCC_CLASS_HAS_WRONG_UNIT = 11                       'unit mismatch - event was not redirected
    DCC_PROC_HAS_WRONG_UNIT = 12                        '
    
    DCC_DUPLICATE_CLASS = 13
    'DUPLICATE PROCS ARE ALLOWED
    
    DCC_OVERLAPPING_OP_PROC_CLASS = 15                  'Perinatal OP proc only
    DCC_OVERLAPPING_OP_PROC = 16                        '    "         "
    
    DCC_ACTIVITY_BELOW_METHODOLOGY_MINIMUM_LOS = 17     '(a comment)
    DCC_PROC_MORE_THAN_24_HOURS = 18                    '(a warning)
    
    DCC_REDIRECTED_CLASS_BEGINS_BEFORE_ADMISSION = 50
    DCC_REDIRECTED_PROC_BEGINS_BEFORE_ADMISSION = 51
    
    DCC_REDIRECTED_CLASS_ENDS_AFTER_DISCHARGE = 52
    DCC_REDIRECTED_PROC_ENDS_AFTER_DISCHARGE = 53
    
    DCC_REDIRECTED_CLASS_MISSING_OUT = 54
    DCC_RECIRECTED_PROC_MISSING_OUT = 55
    
    DCC_REDIRECTED_CLASS_ZERO_LENGTH = 56
    DCC_RECIRECTED_PROC_ZERO_LENGTH = 57
    
    'Unit level checks
    '
    DCC_INVALID_DAY = 101                               'As reported in RPT_VALID_DAY
    DCC_UNCLASSIFIED_HOURS = 102                        'Class hours < actual hours
    DCC_OVERCLASSIFIED_HOURS = 103                      'Class hours > actual hours
End Enum

' These lists are for the user to select what tests to run (Each is a separate query)
' These are powers of two so they can be combined

' These should match what is in LOOKUP_VALUES for lookup list #70
Public Enum DCCPatientShortList
    DCC_RUN_PATIENT_CLASS_IN_OUT = 1
    DCC_RUN_PATIENT_PROC_IN_OUT = 2
    DCC_RUN_PATIENT_OVERLAPPING_CLASS = 4
    DCC_RUN_PATIENT_DUPLICATE_CLASS = 8
    DCC_RUN_PATIENT_OPEN_ENDED_PROC = 16
    DCC_RUN_PATIENT_REDIRECTED_CLASS_IN_OUT = 32
    DCC_RUN_PATIENT_REDIRECTED_PROC_IN_OUT = 64
End Enum

' These should match what is in LOOKUP_VALUES for lookup list #71
Public Enum DCCUnitShortList
    DCC_RUN_UNIT_VALID_DAY = 1
    DCC_RUN_UNIT_CLASS_VS_ACTUAL_HOURS = 2
End Enum

Private Enum DCCEventType
    DCC_NoEvent
    DCC_ClassEvent
    DCC_PROCEvent
End Enum

Public Enum DCCSuggestion
    DCC_SUGGEST_NOTHING
    DCC_SUGGEST_ADD_ACTUAL_STAFF
    DCC_SUGGEST_CLASSIFY_PATIENTS
    DCC_SUGGEST_CHANGE_UNIT
    DCC_SUGGEST_DELETE
    DCC_SUGGEST_EDIT_BY_HAND
    DCC_SUGGEST_RUN_UNCLASSIFIED_DETAIL
    DCC_SUGGEST_RUN_PATIENT_LEVEL_TESTS
    DCC_SUGGEST_SET_IN_TIME
    DCC_SUGGEST_SET_OUT_TIME
End Enum

Public Enum DCCResult
    DCC_RESULT_NONE
    DCC_RESULT_FIXED
    DCC_RESULT_CANNOT_FIX
    DCC_RESULT_ERROR
End Enum

Public Type DCCElement
    event_type              As DCCEventType
    problem_LVC             As String               'DataConsistencyCheckID; lookup #69
    unit_id                 As Long
    location_unit_id        As Long
    encounter_id            As Long
    class_id                As Long
    proc_id                 As Long
    unit_name               As String
    patient_name            As String
    patient_acct            As String
    report_date             As Date
    event_datetime          As Date
    stage_name              As String
    effective_in            As Variant              '(not used in unit-level tests)
    effective_out           As Variant
    in_location_unit_id     As Long
    out_location_unit_id    As Long
    in_patient_location     As String
    out_patient_location    As String
    num_hours               As Single               'number of extra hours found
    suggestion_code         As DCCSuggestion
    incorrect_time          As Variant
    correct_time            As Variant
    can_fix                 As Boolean
    lock_checkbox           As Boolean
    is_checked              As Boolean
    result                  As DCCResult
    result_details          As String
End Type

Private Const REALLY_FIX = True                    'set to False for pretend fixes

Private m_unit_id           As Long
Private m_start             As Date
Private m_finish            As Date

Private m_patient_tests     As Long
Private m_unit_tests        As Long
Private m_results()         As DCCElement
Private m_progress_bar      As ProgressBar
Private m_processing        As Boolean
Private m_stop_processing   As Boolean
Private cdlDCCChecks        As New PFSCodeDescList

Private m_cutil             As New PFSClassifyUtility


' This is a callback function from DCC_SearchForProblems
Public Sub DCC_StopProcessing()
    m_stop_processing = True
End Sub

'==============================================================================
' Main entry point #1:
' Look for selected issues
'==============================================================================
Public Sub DCC_SearchForProblems( _
    unit_ids() As Long, _
    start As Date, finish As Date, _
    patient_tests As DCCPatientShortList, _
    unit_tests As DCCUnitShortList, _
    log_findings As Boolean, _
    progress_bar As ProgressBar, _
    Results() As DCCElement _
)
    On Error GoTo errHandler
    Dim ux As Integer, unit_id As Long
    Dim n As Integer

    If (m_processing) Then Exit Sub             'prevent recursion
    m_processing = True
    
    m_patient_tests = patient_tests
    m_unit_tests = unit_tests
    Set m_progress_bar = progress_bar
    m_stop_processing = False

    '
    ' In order to show a fine-grained progress bar and allow the user to cancel at any time,
    ' we will perform one unit/test/date at a time.
    '
    n = UBound(unit_ids) * (DateDiff("d", start, finish) + 1) * (CountBits(patient_tests) + CountBits(unit_tests))
    m_progress_bar.value = 0
    m_progress_bar.Max = n
    
    ReDim m_results(0 To 0)

    For ux = 1 To UBound(unit_ids)
        ' Set up for unit
        m_unit_id = unit_ids(ux)
        m_start = start
        m_finish = finish
        
        MaybeRunOneTest DCC_RUN_UNIT_VALID_DAY, 0
        MaybeRunOneTest DCC_RUN_UNIT_CLASS_VS_ACTUAL_HOURS, 0
            
        MaybeRunOneTest 0, DCC_RUN_PATIENT_CLASS_IN_OUT
        MaybeRunOneTest 0, DCC_RUN_PATIENT_PROC_IN_OUT
        MaybeRunOneTest 0, DCC_RUN_PATIENT_OVERLAPPING_CLASS
        MaybeRunOneTest 0, DCC_RUN_PATIENT_DUPLICATE_CLASS
        MaybeRunOneTest 0, DCC_RUN_PATIENT_OPEN_ENDED_PROC
        MaybeRunOneTest 0, DCC_RUN_PATIENT_REDIRECTED_CLASS_IN_OUT
        MaybeRunOneTest 0, DCC_RUN_PATIENT_REDIRECTED_PROC_IN_OUT

        If (m_stop_processing) Then Exit For
    Next ux
    
    m_processing = False
    Results = m_results
    Exit Sub

errHandler:
    m_processing = False
    g_util.ThrowError "DCC_SearchForProblems"
    Resume  'debug
End Sub

Private Function CountBits(ByVal bitmask As Long) As Integer
    Dim i As Integer
    For i = 1 To 32
        If (bitmask And 1) Then
            CountBits = CountBits + 1
        End If
        bitmask = bitmask \ 2       'shift right
    Next i
End Function



Private Sub MaybeRunOneTest(this_unit_test As Long, this_patient_test As Long)
    On Error GoTo errHandler
    Dim dt As Date
    
    If (m_stop_processing) Then Exit Sub
    
    If (this_patient_test And m_patient_tests) = 0 And _
       (this_unit_test And m_unit_tests) = 0 _
    Then
        'this test was not requested
        Exit Sub
    End If
    
    '
    ' We are about to run one test
    ' All tests should accept a date range - this will decide how many dates to process at a time
    '

    '================================================================
    ' fast tests go here and do the entire date range in one command
    '================================================================
    Select Case this_unit_test
    Case DCC_RUN_UNIT_VALID_DAY
        CheckUnitValidDay m_unit_id, m_start, m_finish
    Case DCC_RUN_UNIT_CLASS_VS_ACTUAL_HOURS
        CheckUnitClassVsActualHours m_unit_id, m_start, m_finish
    End Select

    Select Case this_patient_test
    Case DCC_RUN_PATIENT_OPEN_ENDED_PROC
        CheckOpenEndedProc m_unit_id, m_start, m_finish
    Case DCC_RUN_PATIENT_REDIRECTED_CLASS_IN_OUT
        CheckRedirectedClassInOut m_unit_id, m_start, m_finish
    Case DCC_RUN_PATIENT_REDIRECTED_PROC_IN_OUT
        CheckRedirectedProcInOut m_unit_id, m_start, m_finish
    End Select
    
    'Give the user a chance to press the Stop button; the window will call DCC_StopProcessing
    DoEvents
    If (m_stop_processing) Then Exit Sub
    
    '======================================================
    ' Slower tests can take a while; do one day at a time
    '======================================================
    For dt = m_start To m_finish
        
        Select Case this_patient_test
        Case DCC_RUN_PATIENT_CLASS_IN_OUT
            CheckClassInOut m_unit_id, dt, dt
        Case DCC_RUN_PATIENT_PROC_IN_OUT
            CheckProcInOut m_unit_id, dt, dt
            CheckProcLOS m_unit_id, dt, dt
        Case DCC_RUN_PATIENT_OVERLAPPING_CLASS
            CheckOverlappingClass m_unit_id, dt, dt
            CheckOverlappingOpProcClass m_unit_id, dt, dt
            CheckOverlappingOpProc m_unit_id, dt, dt
        Case DCC_RUN_PATIENT_DUPLICATE_CLASS
            CheckDuplicateClass m_unit_id, dt, dt
        End Select
        
        'Give the user a chance to press the Stop button; the window will call DCC_StopProcessing
        DoEvents
        If (m_stop_processing) Then Exit For
    Next dt
    
    Exit Sub
    
errHandler:
    g_util.ThrowError "MaybeRunOneTest"
    Resume  'debug
End Sub


Private Function GrowResultsBy(grow_by As Integer) As Long
    On Error GoTo errHandler
    Dim last_loc As Long
    last_loc = UBound(m_results)
    If (grow_by > 0) Then
        ReDim Preserve m_results(0 To last_loc + grow_by)
    End If
    GrowResultsBy = last_loc
    Exit Function
errHandler:
    g_util.ThrowError "GrowResults"
End Function

Private Sub ReplaceMacrosInSQL(sql As String, event_type As DCCEventType, unit_id As Long, start_date As Date, finish_date As Date)
    If (event_type = DCC_ClassEvent) Then
        sql = Replace(sql, "@TABLE_NAME", "CLASSIFICATION_EVENT")
        sql = Replace(sql, "@ID_COLUMN", "CLASSIFICATION_EVENT_ID")
    Else
        sql = Replace(sql, "@TABLE_NAME", "PROCEDURE_EVENT")
        sql = Replace(sql, "@ID_COLUMN", "PROCEDURE_EVENT_ID")
    End If
    sql = Replace(sql, "@UNIT_ID", unit_id)
    sql = Replace(sql, "@START_DATE", g_dbutil.SQL_Date(start_date))
    sql = Replace(sql, "@FINISH_DATE", g_dbutil.SQL_Date(finish_date))
    sql = Replace(sql, "\n", vbCrLf)
End Sub

'==============================================================================
' Check for invalid class/proc in/out times vs ADT locations
'==============================================================================
Private Function ClassProcInOutSQL(event_type As DCCEventType, unit_id As Long, start_date As Date, finish_date As Date) As String
    On Error GoTo errHandler
    Dim sql As String
    
    'This query only looks for records that were NOT redirected to another unit.
    'The basic idea is to look for a valid encounter location at each end of the class/proc.
    '@Table names and @column names will be resolved at the bottom.
    'This is a SLOW query
    sql = _
        " SELECT EV.REPORT_DATE, EV.UNIT_ID, UNIT.NAME AS UNIT_NAME, UNIT.PARENT_UNIT_ID, EV.LOCATION_UNIT_ID,\n" & _
        "     EV.@ID_COLUMN, EV.ENCOUNTER_ID,\n" & _
        "     EV.LOS_HOURS,\n" & _
        "     EV.EFFECTIVE_DATETIME_IN,\n" & _
        "     EV.EFFECTIVE_DATETIME_OUT,\n" & _
        "     STAGE.SHORT_NAME AS STAGE_NAME,\n" & _
        "     E.ACCT_NUMBER,\n" & _
        "     PERSON.LAST_NAME + ', ' + ISNULL(PERSON.FIRST_NAME,'') + ' ' + ISNULL(PERSON.MIDDLE_NAME,'') AS PATIENT_NAME,\n" & _
        "     ADT_IN.START AS ADT_IN_START,\n" & _
        "     ADT_IN.FINISH AS ADT_IN_FINISH,\n" & _
        "     ADT_IN.UNIT_ID AS ADT_IN_UNIT_ID,\n" & _
        "     ADT_IN.PATIENT_LOCATION AS ADT_IN_PATIENT_LOCATION,\n" & _
        "     ADT_OUT.START AS ADT_OUT_START,\n" & _
        "     ADT_OUT.FINISH AS ADT_OUT_FINISH,\n" & _
        "     ADT_OUT.UNIT_ID AS ADT_OUT_UNIT_ID,\n" & _
        "     ADT_OUT.PATIENT_LOCATION AS ADT_OUT_PATIENT_LOCATION,\n" & _
        "     NEXT_EVENT.START AS NEXT_EVENT_START,\n" & _
        "     OVERLAP_EVENT.UNIT_ID AS OVERLAP_UNIT_ID,\n"

    If (event_type = DCC_ClassEvent) Then
        sql = sql & " EV.CLASSIFICATION_DATETIME AS EVENT_DATETIME, EV.DATETIME_OUT\n"
    Else
        sql = sql & " EV.PROCEDURE_DATETIME AS EVENT_DATETIME,      EV.DEPARTURE_DATETIME AS DATETIME_OUT\n"
    End If

    sql = sql & _
        " FROM @TABLE_NAME AS EV\n\n"

    sql = sql & _
        " LEFT JOIN (\n" & _
        "     -- find the ADT location in effect at the start of the event (if any)\n" & _
        "     -- Join up to three transfers within the unit to get correct finish time for location\n" & _
        "     -- The last location may not have a DATETIME_OUT, so use EFFECTIVE_DATETIME_OUT\n" & _
        "     SELECT EV.@ID_COLUMN, LOC1.UNIT_ID,\n" & _
        "         LOC1.UNIT_NAME + ' ' + ISNULL(LOC1.ROOM,'') + ' ' + ISNULL(LOC1.BED,'') AS PATIENT_LOCATION,\n" & _
        "         LOC1.EFFECTIVE_DATETIME_IN AS START,\n" & _
        "         ISNULL(LOC4.EFFECTIVE_DATETIME_OUT, ISNULL(LOC3.EFFECTIVE_DATETIME_OUT, ISNULL(LOC2.EFFECTIVE_DATETIME_OUT, LOC1.EFFECTIVE_DATETIME_OUT))) AS FINISH\n" & _
        "     FROM @TABLE_NAME AS EV\n" & _
        "     INNER JOIN ENCOUNTER_LOCATION AS LOC1\n" & _
        "         ON (LOC1.ENCOUNTER_ID = EV.ENCOUNTER_ID)\n" & _
        "         AND (EV.EFFECTIVE_DATETIME_IN >= LOC1.EFFECTIVE_DATETIME_IN) AND (EV.EFFECTIVE_DATETIME_IN < LOC1.EFFECTIVE_DATETIME_OUT)\n" & _
        "     LEFT JOIN ENCOUNTER_LOCATION AS LOC2\n" & _
        "       ON (LOC2.ENCOUNTER_ID = LOC1.ENCOUNTER_ID) AND (LOC2.DATETIME_IN = LOC1.DATETIME_OUT) AND (LOC2.UNIT_ID = LOC1.UNIT_ID)\n" & _
        "     LEFT JOIN ENCOUNTER_LOCATION AS LOC3\n" & _
        "       ON (LOC3.ENCOUNTER_ID = LOC2.ENCOUNTER_ID) AND (LOC3.DATETIME_IN = LOC2.DATETIME_OUT) AND (LOC3.UNIT_ID = LOC2.UNIT_ID)\n" & _
        "     LEFT JOIN ENCOUNTER_LOCATION AS LOC4\n" & _
        "       ON (LOC4.ENCOUNTER_ID = LOC3.ENCOUNTER_ID) AND (LOC4.DATETIME_IN = LOC3.DATETIME_OUT) AND (LOC4.UNIT_ID = LOC3.UNIT_ID)\n" & _
        "     WHERE (EV.REPORT_DATE between @START_DATE AND @FINISH_DATE)\n" & _
        "     AND EV.UNIT_ID=@UNIT_ID\n" & _
        "     AND (EV.UNIT_ID = EV.LOCATION_UNIT_ID)          -- no redirection\n" & _
        " ) AS ADT_IN ON (ADT_IN.@ID_COLUMN = EV.@ID_COLUMN)\n\n"

    sql = sql & _
        " LEFT JOIN (\n" & _
        "     -- Find the ADT location in effect at the END of the event (if any)\n" & _
        "     -- Join up to three transfers within the unit (prior) to get correct start time for location\n" & _
        "     SELECT @ID_COLUMN, LOC1.UNIT_ID,\n" & _
        "         LOC1.UNIT_NAME + ' ' + ISNULL(LOC1.ROOM,'') + ' ' + ISNULL(LOC1.BED,'') AS PATIENT_LOCATION,\n" & _
        "         ISNULL(LOC4.DATETIME_IN, ISNULL(LOC3.DATETIME_IN, ISNULL(LOC2.DATETIME_IN, LOC1.DATETIME_IN))) AS START,\n" & _
        "         LOC1.EFFECTIVE_DATETIME_OUT AS FINISH\n" & _
        "     FROM @TABLE_NAME AS EV\n" & _
        "     INNER JOIN ENCOUNTER_LOCATION AS LOC1\n" & _
        "         ON (LOC1.ENCOUNTER_ID = EV.ENCOUNTER_ID)\n" & _
        "         AND (EV.EFFECTIVE_DATETIME_OUT > LOC1.EFFECTIVE_DATETIME_IN) AND (EV.EFFECTIVE_DATETIME_OUT <= LOC1.EFFECTIVE_DATETIME_OUT)\n" & _
        "     LEFT JOIN ENCOUNTER_LOCATION AS LOC2\n" & _
        "       ON (LOC2.ENCOUNTER_ID = LOC1.ENCOUNTER_ID) AND (LOC2.DATETIME_OUT = LOC1.DATETIME_IN) AND (LOC2.UNIT_ID = LOC1.UNIT_ID)\n" & _
        "     LEFT JOIN ENCOUNTER_LOCATION AS LOC3\n" & _
        "       ON (LOC3.ENCOUNTER_ID = LOC2.ENCOUNTER_ID) AND (LOC3.DATETIME_OUT = LOC2.DATETIME_IN) AND (LOC3.UNIT_ID = LOC2.UNIT_ID)\n" & _
        "     LEFT JOIN ENCOUNTER_LOCATION AS LOC4\n" & _
        "       ON (LOC4.ENCOUNTER_ID = LOC3.ENCOUNTER_ID) AND (LOC4.DATETIME_OUT = LOC3.DATETIME_IN) AND (LOC4.UNIT_ID = LOC3.UNIT_ID)\n" & _
        "     WHERE (EV.REPORT_DATE between @START_DATE AND @FINISH_DATE)\n" & _
        "     AND EV.UNIT_ID=@UNIT_ID\n" & _
        "     AND (EV.UNIT_ID = EV.LOCATION_UNIT_ID)          -- no redirection\n" & _
        " ) AS ADT_OUT ON (ADT_OUT.@ID_COLUMN = EV.@ID_COLUMN)\n\n"

    sql = sql & _
        " LEFT JOIN (\n" & _
        "    -- Find the next event(E2) that comes after this one(E1) in the same unit.\n" & _
        "    -- There might be more than one so find the lowest time.\n" & _
        "    SELECT E1.@ID_COLUMN, MIN(E2.EFFECTIVE_DATETIME_IN) AS START\n" & _
        "    FROM @TABLE_NAME AS E1\n" & _
        "    INNER JOIN @TABLE_NAME AS E2\n" & _
        "      ON (E2.UNIT_ID = E1.UNIT_ID)\n" & _
        "      AND (E2.ENCOUNTER_ID = E1.ENCOUNTER_ID)\n" & _
        "      AND (E2.EFFECTIVE_DATETIME_IN >= E1.EFFECTIVE_DATETIME_IN)\n" & _
        "      AND (E2.@ID_COLUMN <> E1.@ID_COLUMN)\n" & _
        "    WHERE E1.UNIT_ID=@UNIT_ID\n" & _
        "    AND E1.REPORT_DATE BETWEEN @START_DATE AND @FINISH_DATE\n" & _
        "    GROUP BY E1.@ID_COLUMN\n" & _
        " ) AS NEXT_EVENT ON (NEXT_EVENT.@ID_COLUMN = EV.@ID_COLUMN)\n"

    sql = sql & _
        " LEFT JOIN (\n" & _
        "    -- Look for an overlap event(E2) from any unit for the same encounter.\n" & _
        "    SELECT E1.@ID_COLUMN, MIN(E2.UNIT_ID) AS UNIT_ID, MIN(E2.EFFECTIVE_DATETIME_IN) AS START\n" & _
        "    FROM @TABLE_NAME AS E1\n" & _
        "    INNER JOIN @TABLE_NAME AS E2\n" & _
        "      ON  (E2.ENCOUNTER_ID = E1.ENCOUNTER_ID)\n" & _
        "      AND (E1.EFFECTIVE_DATETIME_IN >= E2.EFFECTIVE_DATETIME_IN)\n" & _
        "      AND (E1.EFFECTIVE_DATETIME_IN < E2.EFFECTIVE_DATETIME_OUT)\n" & _
        "      AND (E2.@ID_COLUMN <> E1.@ID_COLUMN)\n" & _
        "    WHERE E1.UNIT_ID=@UNIT_ID\n" & _
        "    AND E1.REPORT_DATE BETWEEN @START_DATE AND @FINISH_DATE\n" & _
        "    GROUP BY E1.@ID_COLUMN\n" & _
        " ) AS OVERLAP_EVENT ON (OVERLAP_EVENT.@ID_COLUMN = EV.@ID_COLUMN)\n"

    sql = sql & _
        " INNER JOIN ENCOUNTER AS E ON (E.ENCOUNTER_ID = EV.ENCOUNTER_ID)\n" & _
        " INNER JOIN PERSON ON (PERSON.PERSON_ID = E.PERSON_ID)\n" & _
        " INNER JOIN UNIT ON (UNIT.UNIT_ID = EV.UNIT_ID)\n" & _
        " LEFT  JOIN STAGE ON (STAGE.METHODOLOGY_ID = EV.METHODOLOGY_ID) AND (STAGE.STAGE = EV.STAGE)\n" & _
        " WHERE EV.REPORT_DATE BETWEEN @START_DATE AND @FINISH_DATE\n" & _
        " AND EV.UNIT_ID=@UNIT_ID\n" & _
        " AND EV.IS_DELETED<>'Y'\n" & _
        " AND (EV.UNIT_ID = EV.LOCATION_UNIT_ID)              -- no redirected records\n" & _
        "\n" & _
        " ORDER BY UNIT.NAME, EV.EFFECTIVE_DATETIME_IN"

    ReplaceMacrosInSQL sql, event_type, unit_id, start_date, finish_date

    ClassProcInOutSQL = sql
    Exit Function
    
errHandler:
    g_util.ThrowError "ClassProcInOutSQL"
End Function


Private Sub CheckClassProcInOut(event_type As DCCEventType, unit_id As Long, start_date As Date, finish_date As Date)
    On Error GoTo errHandler
    Dim rs As New Recordset
    Dim sql As String
    Dim n As Long
    Dim adt_in_finish As Variant, adt_out_finish As Variant, next_event_start As Variant, possible_out As Variant
    Dim eff_in As Date, eff_out As Date, rpt As Date, rpt2 As Date
    Dim shift_id As Long, overlap_unit_id As Long, parent_unit_id As Long
    Dim dummy1 As Long, dummy2 As Long, dummy3 As Long

    m_progress_bar.value = m_progress_bar.value + DateDiff("d", start_date, finish_date) + 1
    
    sql = ClassProcInOutSQL(event_type, unit_id, start_date, finish_date)
    rs.Open sql, g_cnADO, adOpenStatic

    n = GrowResultsBy(rs.RecordCount)           'returns last used slot
    
    Do While Not rs.eof
        n = n + 1                               'ready for next
        With m_results(n)
            .event_type = event_type
            .unit_id = rs("UNIT_ID")
            .location_unit_id = rs("LOCATION_UNIT_ID")
            .unit_name = rs("UNIT_NAME")
            .report_date = rs("REPORT_DATE")
            .event_datetime = rs("EVENT_DATETIME")
            .encounter_id = rs("ENCOUNTER_ID")
            .patient_acct = rs("ACCT_NUMBER")
            .patient_name = rs("PATIENT_NAME")
            .stage_name = rs("STAGE_NAME") & ""
            .effective_in = rs("EFFECTIVE_DATETIME_IN")
            .effective_out = rs("EFFECTIVE_DATETIME_OUT")
            .num_hours = rs("LOS_HOURS")
            
            'Look out for nulls
            .in_patient_location = rs("ADT_IN_PATIENT_LOCATION") & ""
            .in_location_unit_id = g_dbutil.DBToLong(rs("ADT_IN_UNIT_ID"))
            .out_patient_location = rs("ADT_OUT_PATIENT_LOCATION") & ""
            .out_location_unit_id = g_dbutil.DBToLong(rs("ADT_OUT_UNIT_ID"))
            overlap_unit_id = g_dbutil.DBToLong(rs("OVERLAP_UNIT_ID"))
            parent_unit_id = g_dbutil.DBToLong(rs("PARENT_UNIT_ID"))
            
            adt_in_finish = rs("ADT_IN_FINISH")                     'null OK
            adt_out_finish = rs("ADT_OUT_FINISH")                   'null OK
            next_event_start = rs("NEXT_EVENT_START")               'null OK

            If (event_type = DCC_ClassEvent) Then
                .class_id = rs("CLASSIFICATION_EVENT_ID")
            Else
                .proc_id = rs("PROCEDURE_EVENT_ID")
            End If
        
            '
            ' Diagnose - the order is important!
            '
            If (.num_hours = 0) Then
                .problem_LVC = IIf(event_type = DCC_ClassEvent, DCC_ZERO_LENGTH_CLASS, DCC_ZERO_LENGTH_PROC)
                
                possible_out = Null
                .num_hours = 0
                
                If (event_type = DCC_ClassEvent) Then
                    'This solution only applies to classifications
                    If IsDate(next_event_start) Then
                        'Does the next classification start before the current location ends?
                        If (next_event_start <= adt_in_finish) Then
                            possible_out = next_event_start             'link to it
                        End If
                    End If
                    If Not IsDate(possible_out) And IsDate(adt_in_finish) Then
                        'No classification is after this one; pad out the day/location if it
                        'goes beyond the start of the classification.
                        If (adt_out_finish > .effective_in) Then
                            possible_out = adt_in_finish
                        End If
                    End If
                End If
                
                If IsDate(possible_out) Then
                    'Find out what the effective Out time would be if no Out time is given.
                    'This will be the maximum allowable OUT time.
                    m_cutil.CalcEffectiveInOutTimes .unit_id, .event_datetime, .effective_in, Null, Null, _
                                                    eff_in, eff_out, rpt, rpt2, dummy1, dummy2, dummy3
                    .incorrect_time = .effective_out
                    .correct_time = g_util.Min(possible_out, eff_out)
                    .num_hours = DateDiff("s", .effective_in, .correct_time) / 3600#
                    .suggestion_code = DCC_SUGGEST_SET_OUT_TIME
                End If
                
                'Even if we found a time to use, this still might end up zero-length
                If (.num_hours = 0) Then
                    .suggestion_code = DCC_SUGGEST_DELETE
                    .incorrect_time = Null
                    .correct_time = Null
                End If
                
                .can_fix = True
            
            ElseIf (.in_location_unit_id = 0) And (.out_location_unit_id = 0) Then
                'The patient was not *any* unit at this time
                .problem_LVC = IIf(event_type = DCC_ClassEvent, DCC_HANGING_CLASS, DCC_HANGING_PROC)
                .suggestion_code = DCC_SUGGEST_DELETE
                .can_fix = True
            
            ElseIf (.in_location_unit_id <> .unit_id) And (.out_location_unit_id <> .unit_id) Then
                'Event has wrong unit ID - neither in/out unit matches
                If (parent_unit_id <> 0) Then
                    'This is part of an ED sub-unit and it is normal for the classification unit to be
                    'different from the parent (ADT location) unit.
                    n = n - 1
                Else
                    .problem_LVC = IIf(event_type = DCC_ClassEvent, DCC_CLASS_HAS_WRONG_UNIT, DCC_PROC_HAS_WRONG_UNIT)
                    If (.in_location_unit_id = overlap_unit_id) Then
                        'This was probably imported into the old unit just before the patient was transferred.
                        'There is an overlapping classification so we should delete this one.
                        .suggestion_code = DCC_SUGGEST_DELETE
                        .can_fix = True
                    Else
                        'This was probably left hanging after a cancel transfer, and then a new transfer was added
                        'There is no overlapping classification so we can just change the unit.
                        .suggestion_code = DCC_SUGGEST_CHANGE_UNIT
                        .can_fix = True
                    End If
                End If

            ElseIf (.in_location_unit_id <> .unit_id) And (.out_location_unit_id = .unit_id) Then
                'This event ends in the correct unit but doesn't begin there
                .problem_LVC = IIf(event_type = DCC_ClassEvent, DCC_CLASS_BEGINS_BEFORE_ARRIVAL, DCC_PROC_BEGINS_BEFORE_ARRIVAL)
                .incorrect_time = .effective_in
                .correct_time = rs("ADT_OUT_START")
                .num_hours = DateDiff("s", .effective_in, .correct_time) / 3600#
                .suggestion_code = DCC_SUGGEST_SET_IN_TIME
                .can_fix = True
            
            ElseIf (.in_location_unit_id = .unit_id) And (.out_location_unit_id <> .unit_id) Then
                'This event begins in the correct unit but doesn't end there
                .problem_LVC = IIf(event_type = DCC_ClassEvent, DCC_CLASS_ENDS_AFTER_DEPARTURE, DCC_PROC_ENDS_AFTER_DEPARURE)
                .incorrect_time = .effective_out
                .correct_time = adt_in_finish
                .num_hours = DateDiff("s", .correct_time, .effective_out) / 3600#
                .suggestion_code = DCC_SUGGEST_SET_OUT_TIME
                If IsNull(rs("DATETIME_OUT")) Then
                    .result_details = "(Missing Out time)"
                End If
                .can_fix = True
            Else
                'No problems detected
                n = n - 1
            End If
        
        End With

        rs.MoveNext
    Loop
    
    rs.Close

    ReDim Preserve m_results(0 To n)
    Exit Sub
    
errHandler:
    g_util.ThrowError "CheckClassProcInOut"
    Resume  'debug
End Sub

Private Sub CheckClassInOut(unit_id As Long, start_date As Date, finish_date As Date)
    On Error GoTo errHandler
    CheckClassProcInOut DCC_ClassEvent, unit_id, start_date, finish_date
    Exit Sub
errHandler:
    g_util.ThrowError "CheckClassInOut"
End Sub

Private Sub CheckProcInOut(unit_id As Long, start_date As Date, finish_date As Date)
    On Error GoTo errHandler
    CheckClassProcInOut DCC_PROCEvent, unit_id, start_date, finish_date
    Exit Sub
errHandler:
    g_util.ThrowError "CheckProcInOut"
End Sub


Private Sub CheckOverlappingClass(unit_id As Long, start_date As Date, finish_date As Date)
    On Error GoTo errHandler
    Dim rs As New Recordset
    Dim sql As String
    Dim n As Long
    Dim other_datetime_out As Variant
    
    m_progress_bar.value = m_progress_bar.value + DateDiff("d", start_date, finish_date) + 1
    '
    ' Check for overlapping classifications in the same unit.
    ' Do not check for duplicate classifications here or we will end up making them zero
    ' length to fix the overlap (instead of deleting one of them).
    ' This query will return all classifications that overlap, even if only one of two needs a fix.
    ' (SLOW query)
    '
    sql = " SELECT EV.REPORT_DATE, EV.UNIT_ID, UNIT.NAME AS UNIT_NAME, EV.LOCATION_UNIT_ID,\n" & _
        "     EV.CLASSIFICATION_EVENT_ID, EV.CLASSIFICATION_DATETIME AS EVENT_DATETIME, EV.ENCOUNTER_ID,\n" & _
        "     EV.EFFECTIVE_DATETIME_IN, EV.EFFECTIVE_DATETIME_OUT,\n" & _
        "     STAGE.SHORT_NAME AS STAGE_NAME,\n" & _
        "     NEXT_CLASS.DATETIME_IN AS NEXT_DATETIME_IN,\n" & _
        "     NEXT_CLASS.CLASSIFICATION_DATETIME AS NEXT_CLASS_DATETIME,\n" & _
        "     OTHER_CLASS.DATETIME_OUT AS OTHER_DATETIME_OUT,\n" & _
        "     E.ACCT_NUMBER,\n" & _
        "     PERSON.LAST_NAME + ', ' + ISNULL(PERSON.FIRST_NAME,'') + ' ' + ISNULL(PERSON.MIDDLE_NAME,'') AS PATIENT_NAME\n" & _
        " FROM CLASSIFICATION_EVENT AS EV\n"
    sql = sql & _
        " INNER JOIN (\n" & _
        "    -- Find the next classification that overlaps this one.\n" & _
        "    -- There might be more than one so find the lowest time.\n" & _
        "    -- There other one might have the same start time, so exclude the current one.\n" & _
        "    SELECT C1.CLASSIFICATION_EVENT_ID,\n" & _
        "       MIN(C2.EFFECTIVE_DATETIME_IN) AS DATETIME_IN,\n" & _
        "       MIN(C2.CLASSIFICATION_DATETIME) AS CLASSIFICATION_DATETIME\n" & _
        "    FROM CLASSIFICATION_EVENT AS C1\n" & _
        "    INNER JOIN CLASSIFICATION_EVENT AS C2\n" & _
        "      ON (C2.UNIT_ID = C1.UNIT_ID)\n" & _
        "      AND (C2.ENCOUNTER_ID = C1.ENCOUNTER_ID)\n" & _
        "      AND (C2.EFFECTIVE_DATETIME_IN >= C1.EFFECTIVE_DATETIME_IN)\n" & _
        "      AND (C2.EFFECTIVE_DATETIME_IN < C1.EFFECTIVE_DATETIME_OUT)\n" & _
        "      AND (C2.CLASSIFICATION_EVENT_ID <> C1.CLASSIFICATION_EVENT_ID)\n" & _
        "      AND (C2.IS_DELETED<>'Y')\n" & _
        "    WHERE C1.UNIT_ID=@UNIT_ID\n" & _
        "    AND C1.REPORT_DATE BETWEEN @START_DATE AND @FINISH_DATE\n" & _
        "    AND C1.IS_DELETED<>'Y'\n" & _
        "    GROUP BY C1.CLASSIFICATION_EVENT_ID\n" & _
        " ) AS NEXT_CLASS\n" & _
        " ON (NEXT_CLASS.CLASSIFICATION_EVENT_ID = EV.CLASSIFICATION_EVENT_ID)\n"
    sql = sql & _
        " LEFT JOIN (\n" & _
        "    -- Look for a classification with OUT = this class time.\n" & _
        "    -- This is a strong indication that our IN time is wrong.\n" & _
        "    SELECT C1.CLASSIFICATION_EVENT_ID, MAX(C2.EFFECTIVE_DATETIME_OUT) AS DATETIME_OUT\n" & _
        "    FROM CLASSIFICATION_EVENT AS C1\n" & _
        "    INNER JOIN CLASSIFICATION_EVENT AS C2\n" & _
        "      ON (C2.UNIT_ID = C1.UNIT_ID)\n" & _
        "      AND (C2.ENCOUNTER_ID = C1.ENCOUNTER_ID)\n" & _
        "      AND (C2.EFFECTIVE_DATETIME_OUT = C1.CLASSIFICATION_DATETIME)\n" & _
        "      AND (C2.CLASSIFICATION_EVENT_ID <> C1.CLASSIFICATION_EVENT_ID)\n" & _
        "      AND (C2.IS_DELETED<>'Y')\n" & _
        "    WHERE C1.UNIT_ID=@UNIT_ID\n" & _
        "    AND C1.REPORT_DATE BETWEEN @START_DATE AND @FINISH_DATE\n" & _
        "    AND C1.IS_DELETED<>'Y'\n" & _
        "    GROUP BY C1.CLASSIFICATION_EVENT_ID\n" & _
        " ) AS OTHER_CLASS\n" & _
        " ON (OTHER_CLASS.CLASSIFICATION_EVENT_ID = EV.CLASSIFICATION_EVENT_ID)\n"
    sql = sql & _
        " INNER JOIN ENCOUNTER AS E ON (E.ENCOUNTER_ID = EV.ENCOUNTER_ID)\n" & _
        " INNER JOIN PERSON ON (PERSON.PERSON_ID = E.PERSON_ID)\n" & _
        " INNER JOIN UNIT ON (UNIT.UNIT_ID = EV.UNIT_ID)\n" & _
        " LEFT  JOIN STAGE ON (STAGE.METHODOLOGY_ID = EV.METHODOLOGY_ID) AND (STAGE.STAGE = EV.STAGE)\n" & _
        " WHERE EV.REPORT_DATE BETWEEN @START_DATE AND @FINISH_DATE\n" & _
        " AND EV.UNIT_ID=@UNIT_ID\n" & _
        " AND EV.IS_DELETED<>'Y'\n" & _
        " AND (EV.UNIT_ID = EV.LOCATION_UNIT_ID)              -- no redirected records\n" & _
        " ORDER BY UNIT.NAME, EV.ENCOUNTER_ID, EV.EFFECTIVE_DATETIME_IN"

    ReplaceMacrosInSQL sql, DCC_ClassEvent, unit_id, start_date, finish_date
    rs.Open sql, g_cnADO, adOpenStatic

    n = GrowResultsBy(rs.RecordCount)           'returns last used slot
    
    Do While Not rs.eof
        n = n + 1                               'ready for next
        
        With m_results(n)
            .event_type = DCC_ClassEvent
            .unit_id = rs("UNIT_ID")
            .location_unit_id = rs("LOCATION_UNIT_ID")
            .unit_name = rs("UNIT_NAME")
            .report_date = rs("REPORT_DATE")
            .event_datetime = rs("EVENT_DATETIME")
            .encounter_id = rs("ENCOUNTER_ID")
            .patient_acct = rs("ACCT_NUMBER")
            .patient_name = rs("PATIENT_NAME")
            .stage_name = rs("STAGE_NAME") & ""
            .class_id = rs("CLASSIFICATION_EVENT_ID")
            .effective_in = rs("EFFECTIVE_DATETIME_IN")
            .effective_out = rs("EFFECTIVE_DATETIME_OUT")
            other_datetime_out = rs("OTHER_DATETIME_OUT")         'null OK
            
            'Diagnosis:
            .problem_LVC = DCC_OVERLAPPING_CLASS

            If IsDate(other_datetime_out) And (other_datetime_out = .event_datetime) Then
                '
                ' Suggest a change to IN time
                '
                .incorrect_time = .effective_in
                .correct_time = .event_datetime
                .num_hours = DateDiff("s", .correct_time, .incorrect_time) / 3600#
                .suggestion_code = DCC_SUGGEST_SET_IN_TIME
                .can_fix = True

                If (.effective_in = .correct_time) Then
                    'The "fix" will do nothing - IN is already set to the "correct" time
                    If (rs("NEXT_CLASS_DATETIME") = rs("NEXT_DATETIME_IN")) Then
                        'Two records start at the same time and the next one has no fix - show both
                        .suggestion_code = DCC_SUGGEST_EDIT_BY_HAND
                        .can_fix = False
                    Else
                        'The next record will get the fix; this one can be left alone.
                        'Remove this from the list
                        n = n - 1
                    End If
                End If
            Else
                '
                ' Suggest a change to OUT time
                '
                .incorrect_time = .effective_out
                .correct_time = rs("NEXT_DATETIME_IN")
                .num_hours = DateDiff("s", .correct_time, .effective_out) / 3600#
                .suggestion_code = DCC_SUGGEST_SET_OUT_TIME
                .can_fix = True
                
                If (rs("NEXT_CLASS_DATETIME") <> rs("NEXT_DATETIME_IN")) Then
                    'The other record is going to be modified - leave this one alone
                    'Remove this from the list
                    n = n - 1
                End If
            End If
        End With

        rs.MoveNext
    Loop
    
    rs.Close
    
    ReDim Preserve m_results(0 To n)
    Exit Sub

errHandler:
    g_util.ThrowError "CheckOverlappingClass"
    Resume  'debug
End Sub

Private Sub CheckDuplicateClass(unit_id As Long, start_date As Date, finish_date As Date)
    On Error GoTo errHandler
    Dim rs As New Recordset
    Dim sql As String
    Dim n As Long
    
    m_progress_bar.value = m_progress_bar.value + DateDiff("d", start_date, finish_date) + 1
    '
    ' Check for duplicate classifications in the same unit.
    '
    ' (SLOW query)
    '
    sql = " SELECT EV.REPORT_DATE, EV.UNIT_ID, UNIT.NAME AS UNIT_NAME, EV.LOCATION_UNIT_ID,\n" & _
        "     EV.CLASSIFICATION_EVENT_ID, CLASSIFICATION_DATETIME AS EVENT_DATETIME, EV.ENCOUNTER_ID,\n" & _
        "     EV.EFFECTIVE_DATETIME_IN, EV.EFFECTIVE_DATETIME_OUT, EV.LOS_HOURS,\n" & _
        "     STAGE.SHORT_NAME AS STAGE_NAME,\n" & _
        "     E.ACCT_NUMBER,\n" & _
        "     PERSON.LAST_NAME + ', ' + ISNULL(PERSON.FIRST_NAME,'') + ' ' + ISNULL(PERSON.MIDDLE_NAME,'') AS PATIENT_NAME\n" & _
        " FROM CLASSIFICATION_EVENT AS EV\n"
    sql = sql & _
        " INNER JOIN (\n" & _
        "    -- Find an ealier classification (C2) with the same in/out\n" & _
        "    -- There may be several, but the first (C2) will not be selected.\n" & _
        "    SELECT C1.CLASSIFICATION_EVENT_ID, MIN(C2.EFFECTIVE_DATETIME_IN) AS DATETIME_IN\n" & _
        "    FROM CLASSIFICATION_EVENT AS C1\n" & _
        "    INNER JOIN CLASSIFICATION_EVENT AS C2\n" & _
        "      ON (C2.UNIT_ID = C1.UNIT_ID)\n" & _
        "      AND (C2.ENCOUNTER_ID = C1.ENCOUNTER_ID)\n" & _
        "      AND (C2.EFFECTIVE_DATETIME_IN = C1.EFFECTIVE_DATETIME_IN)\n" & _
        "      AND (C2.EFFECTIVE_DATETIME_OUT = C1.EFFECTIVE_DATETIME_OUT)\n" & _
        "      AND (C2.CLASSIFICATION_EVENT_ID < C1.CLASSIFICATION_EVENT_ID)\n" & _
        "      AND (C2.IS_DELETED<>'Y')\n" & _
        "    WHERE C1.UNIT_ID=@UNIT_ID\n" & _
        "    AND C1.REPORT_DATE BETWEEN @START_DATE AND @FINISH_DATE\n" & _
        "    AND C1.IS_DELETED<>'Y'\n" & _
        "    GROUP BY C1.CLASSIFICATION_EVENT_ID\n" & _
        " ) AS DUP_CLASS\n" & _
        " ON (DUP_CLASS.CLASSIFICATION_EVENT_ID = EV.CLASSIFICATION_EVENT_ID)\n"
    sql = sql & _
        " INNER JOIN ENCOUNTER AS E ON (E.ENCOUNTER_ID = EV.ENCOUNTER_ID)\n" & _
        " INNER JOIN PERSON ON (PERSON.PERSON_ID = E.PERSON_ID)\n" & _
        " INNER JOIN UNIT ON (UNIT.UNIT_ID = EV.UNIT_ID)\n" & _
        " LEFT  JOIN STAGE ON (STAGE.METHODOLOGY_ID = EV.METHODOLOGY_ID) AND (STAGE.STAGE = EV.STAGE)\n" & _
        " WHERE EV.REPORT_DATE BETWEEN @START_DATE AND @FINISH_DATE\n" & _
        " AND EV.UNIT_ID=@UNIT_ID\n" & _
        " AND EV.IS_DELETED<>'Y'\n" & _
        " ORDER BY UNIT.NAME, EV.ENCOUNTER_ID, EV.EFFECTIVE_DATETIME_IN"

    ReplaceMacrosInSQL sql, DCC_ClassEvent, unit_id, start_date, finish_date
    rs.Open sql, g_cnADO, adOpenStatic

    n = GrowResultsBy(rs.RecordCount)           'returns last used slot
    
    Do While Not rs.eof
        n = n + 1                               'ready for next
        
        With m_results(n)
            .event_type = DCC_ClassEvent
            .unit_id = rs("UNIT_ID")
            .location_unit_id = rs("LOCATION_UNIT_ID")
            .unit_name = rs("UNIT_NAME")
            .report_date = rs("REPORT_DATE")
            .event_datetime = rs("EVENT_DATETIME")
            .encounter_id = rs("ENCOUNTER_ID")
            .patient_acct = rs("ACCT_NUMBER")
            .patient_name = rs("PATIENT_NAME")
            .stage_name = rs("STAGE_NAME") & ""
            .class_id = rs("CLASSIFICATION_EVENT_ID")
            .effective_in = rs("EFFECTIVE_DATETIME_IN")
            .effective_out = rs("EFFECTIVE_DATETIME_OUT")
            
            'Diagnose:
            .problem_LVC = DCC_DUPLICATE_CLASS
            .num_hours = rs("LOS_HOURS")
            .suggestion_code = DCC_SUGGEST_DELETE
            .can_fix = True
        End With

        rs.MoveNext
    Loop
    
    rs.Close
    Exit Sub

errHandler:
    g_util.ThrowError "CheckDuplicateClass"
    Resume  'debug
End Sub


Private Sub CheckOpenEndedProc(unit_id As Long, start_date As Date, finish_date As Date)
    On Error GoTo errHandler
    Dim rs As New Recordset
    Dim sql As String
    Dim n As Long
    
    m_progress_bar.value = m_progress_bar.value + DateDiff("d", start_date, finish_date) + 1
    '
    ' This is pretty simple, really - any (non-redirected) procedure with no OUT time gets listed
    ' This is a FAST query.
    '
    sql = " SELECT EV.REPORT_DATE, EV.UNIT_ID, UNIT.NAME AS UNIT_NAME, EV.LOCATION_UNIT_ID,\n" & _
        "     PROCEDURE_EVENT_ID, PROCEDURE_DATETIME AS EVENT_DATETIME, EV.ENCOUNTER_ID,\n" & _
        "     EV.EFFECTIVE_DATETIME_IN, EV.EFFECTIVE_DATETIME_OUT,\n" & _
        "     EV.LOS_HOURS,\n" & _
        "     STAGE.SHORT_NAME AS STAGE_NAME,\n" & _
        "     E.ACCT_NUMBER,\n" & _
        "     PERSON.LAST_NAME + ', ' + ISNULL(PERSON.FIRST_NAME,'') + ' ' + ISNULL(PERSON.MIDDLE_NAME,'') AS PATIENT_NAME\n" & _
        " FROM PROCEDURE_EVENT AS EV\n" & _
        " INNER JOIN ENCOUNTER AS E ON (E.ENCOUNTER_ID = EV.ENCOUNTER_ID)\n" & _
        " INNER JOIN PERSON ON (PERSON.PERSON_ID = E.PERSON_ID)\n" & _
        " INNER JOIN UNIT ON (UNIT.UNIT_ID = EV.UNIT_ID)\n" & _
        " LEFT  JOIN STAGE ON (STAGE.METHODOLOGY_ID = EV.METHODOLOGY_ID) AND (STAGE.STAGE = EV.STAGE)\n" & _
        " WHERE EV.REPORT_DATE BETWEEN @START_DATE AND @FINISH_DATE\n" & _
        " AND EV.UNIT_ID=@UNIT_ID\n" & _
        " AND (EV.UNIT_ID = EV.LOCATION_UNIT_ID)              -- no redirected records\n" & _
        " AND EV.DEPARTURE_DATETIME IS NULL\n" & _
        " AND EV.IS_DELETED<>'Y'\n" & _
        " ORDER BY UNIT.NAME, EV.EFFECTIVE_DATETIME_IN\n"
    
    ReplaceMacrosInSQL sql, DCC_PROCEvent, unit_id, start_date, finish_date
    rs.Open sql, g_cnADO, adOpenStatic

    n = GrowResultsBy(rs.RecordCount)           'returns last used slot
    
    Do While Not rs.eof
        n = n + 1                               'ready for next
        
        With m_results(n)
            .event_type = DCC_PROCEvent
            .unit_id = rs("UNIT_ID")
            .location_unit_id = rs("LOCATION_UNIT_ID")
            .unit_name = rs("UNIT_NAME")
            .report_date = rs("REPORT_DATE")
            .event_datetime = rs("EVENT_DATETIME")
            .encounter_id = rs("ENCOUNTER_ID")
            .patient_acct = rs("ACCT_NUMBER")
            .patient_name = rs("PATIENT_NAME")
            .stage_name = rs("STAGE_NAME") & ""
            .num_hours = rs("LOS_HOURS")
            .proc_id = rs("PROCEDURE_EVENT_ID")
            'Don't show the effective times; just show the warning
            .effective_in = Null                        'rs("EFFECTIVE_DATETIME_IN")
            .effective_out = Null                       'rs("EFFECTIVE_DATETIME_OUT")
            
            'Diagnose:
            .problem_LVC = DCC_OPEN_ENDED_PROC
            .suggestion_code = DCC_SUGGEST_EDIT_BY_HAND
            .result_details = "Verify and set the procedure finish time;"
            .can_fix = False                    'they need to figure out what the OUT time should be
        End With

        rs.MoveNext
    Loop
    
    rs.Close
    Exit Sub

errHandler:
    g_util.ThrowError "CheckOpenEndedProc"
    Resume  'debug
End Sub

Private Sub CheckProcLOS(unit_id As Long, start_date As Date, finish_date As Date)
    On Error GoTo errHandler
    Dim rs As New Recordset
    Dim sql As String
    Dim n As Long
    
    'CheckProcInOut already bumped this
    'm_progress_bar.value = m_progress_bar.value + DateDiff("d", start_date, finish_date) + 1
    
    '
    ' Quick checks on procedure LOS: < minimum or > 24 hour
    ' This is a FAST query.
    '
    sql = " SELECT EV.REPORT_DATE, EV.UNIT_ID, UNIT.NAME AS UNIT_NAME, EV.LOCATION_UNIT_ID,\n" & _
        "     PROCEDURE_EVENT_ID, PROCEDURE_DATETIME AS EVENT_DATETIME, EV.ENCOUNTER_ID,\n" & _
        "     EV.EFFECTIVE_DATETIME_IN, EV.EFFECTIVE_DATETIME_OUT,\n" & _
        "     EV.LOS_HOURS,EV.REPORT_THIS_WORKLOAD,\n" & _
        "     STAGE.SHORT_NAME AS STAGE_NAME,\n" & _
        "     E.ACCT_NUMBER,\n" & _
        "     PERSON.LAST_NAME + ', ' + ISNULL(PERSON.FIRST_NAME,'') + ' ' + ISNULL(PERSON.MIDDLE_NAME,'') AS PATIENT_NAME\n" & _
        " FROM PROCEDURE_EVENT AS EV\n" & _
        " INNER JOIN ENCOUNTER AS E ON (E.ENCOUNTER_ID = EV.ENCOUNTER_ID)\n" & _
        " INNER JOIN PERSON ON (PERSON.PERSON_ID = E.PERSON_ID)\n" & _
        " INNER JOIN UNIT ON (UNIT.UNIT_ID = EV.UNIT_ID)\n" & _
        " LEFT  JOIN STAGE ON (STAGE.METHODOLOGY_ID = EV.METHODOLOGY_ID) AND (STAGE.STAGE = EV.STAGE)\n" & _
        " WHERE EV.REPORT_DATE BETWEEN @START_DATE AND @FINISH_DATE\n" & _
        " AND EV.UNIT_ID=@UNIT_ID\n" & _
        " AND (EV.UNIT_ID = EV.LOCATION_UNIT_ID)              -- no redirected records\n" & _
        " AND ((EV.REPORT_THIS_WORKLOAD='N') OR (EV.LOS_HOURS > 24))\n" & _
        " AND EV.IS_DELETED<>'Y'\n" & _
        " ORDER BY UNIT.NAME, EV.EFFECTIVE_DATETIME_IN\n"
    
    ReplaceMacrosInSQL sql, DCC_PROCEvent, unit_id, start_date, finish_date
    rs.Open sql, g_cnADO, adOpenStatic

    n = GrowResultsBy(rs.RecordCount)           'returns last used slot
    
    Do While Not rs.eof
        n = n + 1                               'ready for next
        
        With m_results(n)
            .event_type = DCC_PROCEvent
            .unit_id = rs("UNIT_ID")
            .location_unit_id = rs("LOCATION_UNIT_ID")
            .unit_name = rs("UNIT_NAME")
            .report_date = rs("REPORT_DATE")
            .event_datetime = rs("EVENT_DATETIME")
            .encounter_id = rs("ENCOUNTER_ID")
            .patient_acct = rs("ACCT_NUMBER")
            .patient_name = rs("PATIENT_NAME")
            .stage_name = rs("STAGE_NAME") & ""
            .num_hours = rs("LOS_HOURS")
            .proc_id = rs("PROCEDURE_EVENT_ID")
            .effective_in = rs("EFFECTIVE_DATETIME_IN")
            .effective_out = rs("EFFECTIVE_DATETIME_OUT")
            
            'Diagnose:
            If (rs("REPORT_THIS_WORKLOAD") = "N") Then
                .problem_LVC = DCC_ACTIVITY_BELOW_METHODOLOGY_MINIMUM_LOS
                .suggestion_code = DCC_SUGGEST_NOTHING
                .can_fix = False                    'they need to figure out what the OUT time should be
            ElseIf (.num_hours > 24) Then
                .problem_LVC = DCC_PROC_MORE_THAN_24_HOURS
                .suggestion_code = DCC_SUGGEST_EDIT_BY_HAND
                .result_details = "Validate that this activity was > 24 hours;"
                .can_fix = False                    'they need to figure out what the OUT time should be
            End If
        End With

        rs.MoveNext
    Loop
    
    rs.Close
    Exit Sub

errHandler:
    g_util.ThrowError "CheckProcLOS"
    Resume  'debug
End Sub

Private Sub CheckOverlappingOpProc(unit_id As Long, start_date As Date, finish_date As Date)
    On Error GoTo errHandler
    Dim rs As New Recordset
    Dim sql As String
    Dim n As Long
    Dim Unit              As UnitDefinition
    Dim meth              As methodology

    'This test is for Perinatal outpatient procedures only
    Set Unit = g_ucache.Unit(m_unit_id)
    Set meth = Unit.UnitParametersForDate(m_finish).methodology
    If (Not meth.IsPerinatal) Then Exit Sub
    
    'Can only increment if this is a separate test in the list
    'm_progress_bar.value = m_progress_bar.value + DateDiff("d", start_date, finish_date) + 1
    
    '
    ' Check for overlapping outpatient PROCEDUREs in the same unit.
    ' Do not check for duplicate PROCEDUREs here or we will end up making them zero
    ' length to fix the overlap (instead of deleting one of them).
    ' This query will return all PROCEDUREs that overlap, even if only one of two needs a fix.
    ' (SLOW query)
    '
    sql = " SELECT EV.REPORT_DATE, EV.UNIT_ID, UNIT.NAME AS UNIT_NAME, EV.LOCATION_UNIT_ID,\n" & _
        "     EV.PROCEDURE_EVENT_ID, EV.PROCEDURE_DATETIME AS EVENT_DATETIME, EV.ENCOUNTER_ID,\n" & _
        "     EV.EFFECTIVE_DATETIME_IN, EV.EFFECTIVE_DATETIME_OUT,\n" & _
        "     STAGE.SHORT_NAME AS STAGE_NAME,\n" & _
        "     NEXT_PROC.DATETIME_IN AS NEXT_DATETIME_IN,\n" & _
        "     NEXT_PROC.PROCEDURE_DATETIME AS NEXT_PROC_DATETIME,\n" & _
        "     E.ACCT_NUMBER,\n" & _
        "     PERSON.LAST_NAME + ', ' + ISNULL(PERSON.FIRST_NAME,'') + ' ' + ISNULL(PERSON.MIDDLE_NAME,'') AS PATIENT_NAME\n" & _
        " FROM PROCEDURE_EVENT AS EV\n"
    sql = sql & _
        " INNER JOIN (\n" & _
        "    -- Find the next PROCEDURE that overlaps this one.\n" & _
        "    -- There might be more than one so find the lowest time.\n" & _
        "    -- There other one might have the same start time, so exclude the current one.\n" & _
        "    SELECT P1.PROCEDURE_EVENT_ID,\n" & _
        "       MIN(P2.PROCEDURE_DATETIME) AS PROCEDURE_DATETIME,\n" & _
        "       MIN(P2.EFFECTIVE_DATETIME_IN) AS DATETIME_IN\n" & _
        "    FROM PROCEDURE_EVENT AS P1\n" & _
        "    INNER JOIN PROCEDURE_EVENT AS P2\n" & _
        "      ON (P2.UNIT_ID = P1.UNIT_ID)\n" & _
        "      AND (P2.ENCOUNTER_ID = P1.ENCOUNTER_ID)\n" & _
        "      AND (P2.EFFECTIVE_DATETIME_IN >= P1.EFFECTIVE_DATETIME_IN)\n" & _
        "      AND (P2.EFFECTIVE_DATETIME_IN < P1.EFFECTIVE_DATETIME_OUT)\n" & _
        "      AND (P2.PROCEDURE_EVENT_ID <> P1.PROCEDURE_EVENT_ID)\n" & _
        "      AND (P2.IS_DELETED<>'Y')\n" & _
        "    WHERE P1.UNIT_ID=@UNIT_ID\n" & _
        "    AND P1.REPORT_DATE BETWEEN @START_DATE AND @FINISH_DATE\n" & _
        "    AND P1.STAGE=0\n" & _
        "    AND P1.IS_DELETED<>'Y'\n" & _
        "    GROUP BY P1.PROCEDURE_EVENT_ID\n" & _
        " ) AS NEXT_PROC\n" & _
        " ON (NEXT_PROC.PROCEDURE_EVENT_ID = EV.PROCEDURE_EVENT_ID)\n"
    sql = sql & _
        " INNER JOIN ENCOUNTER AS E ON (E.ENCOUNTER_ID = EV.ENCOUNTER_ID)\n" & _
        " INNER JOIN PERSON ON (PERSON.PERSON_ID = E.PERSON_ID)\n" & _
        " INNER JOIN UNIT ON (UNIT.UNIT_ID = EV.UNIT_ID)\n" & _
        " LEFT  JOIN STAGE ON (STAGE.METHODOLOGY_ID = EV.METHODOLOGY_ID) AND (STAGE.STAGE = EV.STAGE)\n" & _
        " WHERE EV.REPORT_DATE BETWEEN @START_DATE AND @FINISH_DATE\n" & _
        " AND EV.UNIT_ID=@UNIT_ID\n" & _
        " AND (EV.UNIT_ID = EV.LOCATION_UNIT_ID)              -- no redirected records\n" & _
        " AND EV.IS_DELETED<>'Y'\n" & _
        " ORDER BY UNIT.NAME, EV.ENCOUNTER_ID, EV.EFFECTIVE_DATETIME_IN"

    ReplaceMacrosInSQL sql, DCC_PROCEvent, unit_id, start_date, finish_date
    rs.Open sql, g_cnADO, adOpenStatic

    n = GrowResultsBy(rs.RecordCount)           'returns last used slot
    
    Do While Not rs.eof
        n = n + 1                               'ready for next
        
        With m_results(n)
            .event_type = DCC_PROCEvent
            .unit_id = rs("UNIT_ID")
            .location_unit_id = rs("LOCATION_UNIT_ID")
            .unit_name = rs("UNIT_NAME")
            .report_date = rs("REPORT_DATE")
            .event_datetime = rs("EVENT_DATETIME")
            .encounter_id = rs("ENCOUNTER_ID")
            .patient_acct = rs("ACCT_NUMBER")
            .patient_name = rs("PATIENT_NAME")
            .stage_name = rs("STAGE_NAME") & ""
            .proc_id = rs("PROCEDURE_EVENT_ID")
            .effective_in = rs("EFFECTIVE_DATETIME_IN")
            .effective_out = rs("EFFECTIVE_DATETIME_OUT")
            
            'Diagnosis:
            .problem_LVC = DCC_OVERLAPPING_OP_PROC

            ' Suggest a change to OUT time
            .incorrect_time = .effective_out
            .correct_time = rs("NEXT_DATETIME_IN")
            .num_hours = DateDiff("s", .correct_time, .effective_out) / 3600#
            .suggestion_code = DCC_SUGGEST_SET_OUT_TIME
            .can_fix = True
        End With

        rs.MoveNext
    Loop
    
    rs.Close
    
    ReDim Preserve m_results(0 To n)
    Exit Sub

errHandler:
    g_util.ThrowError "CheckOverlappingOpProc"
    Resume  'debug
End Sub

Private Sub CheckOverlappingOpProcClass(unit_id As Long, start_date As Date, finish_date As Date)
    On Error GoTo errHandler
    Dim rs As New Recordset
    Dim sql As String
    Dim n As Long
    Dim next_datetime_in As Date, next_datetime_out As Date
    Dim Unit              As UnitDefinition
    Dim meth              As methodology

    'This test is for Perinatal outpatient procedure only
    Set Unit = g_ucache.Unit(m_unit_id)
    Set meth = Unit.UnitParametersForDate(m_finish).methodology
    If (Not meth.IsPerinatal) Then Exit Sub
    
    'Can only increment if this is a separate test in the list
    'm_progress_bar.value = m_progress_bar.value + DateDiff("d", start_date, finish_date) + 1
    
    '==========================================================================
    ' Check for an outpatient procedure followed by an overlapping classification.
    ' (Classification starts after procedure)
    ' (SLOW query)
    '==========================================================================
    sql = " SELECT EV.REPORT_DATE, EV.UNIT_ID, UNIT.NAME AS UNIT_NAME, EV.LOCATION_UNIT_ID,\n" & _
        "     EV.PROCEDURE_EVENT_ID, EV.PROCEDURE_DATETIME AS EVENT_DATETIME, EV.ENCOUNTER_ID,\n" & _
        "     EV.EFFECTIVE_DATETIME_IN, EV.EFFECTIVE_DATETIME_OUT,\n" & _
        "     STAGE.SHORT_NAME AS STAGE_NAME,\n" & _
        "     NEXT_CLASS.DATETIME_IN AS NEXT_DATETIME_IN,\n" & _
        "     E.ACCT_NUMBER,\n" & _
        "     PERSON.LAST_NAME + ', ' + ISNULL(PERSON.FIRST_NAME,'') + ' ' + ISNULL(PERSON.MIDDLE_NAME,'') AS PATIENT_NAME\n" & _
        " FROM PROCEDURE_EVENT AS EV\n"
    sql = sql & _
        " INNER JOIN (\n" & _
        "    -- Find any classification that overlaps this procedure (and starts after).\n" & _
        "    -- There might be more than one so find the lowest time (group).\n" & _
        "    SELECT P1.PROCEDURE_EVENT_ID,\n" & _
        "       MIN(C2.EFFECTIVE_DATETIME_IN) AS DATETIME_IN,\n" & _
        "       MIN(C2.CLASSIFICATION_DATETIME) AS CLASSIFICATION_DATETIME\n" & _
        "    FROM PROCEDURE_EVENT AS P1\n" & _
        "    INNER JOIN CLASSIFICATION_EVENT AS C2\n" & _
        "      ON (C2.UNIT_ID = P1.UNIT_ID)\n" & _
        "      AND (C2.ENCOUNTER_ID = P1.ENCOUNTER_ID)\n" & _
        "      AND (C2.EFFECTIVE_DATETIME_IN >= P1.EFFECTIVE_DATETIME_IN)\n" & _
        "      AND (C2.EFFECTIVE_DATETIME_IN < P1.EFFECTIVE_DATETIME_OUT)\n" & _
        "      AND (C2.IS_DELETED<>'Y')\n" & _
        "    WHERE P1.UNIT_ID=@UNIT_ID\n" & _
        "    AND P1.REPORT_DATE BETWEEN @START_DATE AND @FINISH_DATE\n" & _
        "    AND P1.STAGE=0\n" & _
        "    AND P1.IS_DELETED<>'Y'\n" & _
        "    AND C2.EFFECTIVE_DATETIME_IN > P1.EFFECTIVE_DATETIME_IN\n" & _
        "    GROUP BY P1.PROCEDURE_EVENT_ID\n" & _
        " ) AS NEXT_CLASS\n" & _
        " ON (NEXT_CLASS.PROCEDURE_EVENT_ID = EV.PROCEDURE_EVENT_ID)\n"
    sql = sql & _
        " INNER JOIN ENCOUNTER AS E ON (E.ENCOUNTER_ID = EV.ENCOUNTER_ID)\n" & _
        " INNER JOIN PERSON ON (PERSON.PERSON_ID = E.PERSON_ID)\n" & _
        " INNER JOIN UNIT ON (UNIT.UNIT_ID = EV.UNIT_ID)\n" & _
        " LEFT  JOIN STAGE ON (STAGE.METHODOLOGY_ID = EV.METHODOLOGY_ID) AND (STAGE.STAGE = EV.STAGE)\n" & _
        " WHERE EV.REPORT_DATE BETWEEN @START_DATE AND @FINISH_DATE\n" & _
        " AND EV.UNIT_ID=@UNIT_ID\n" & _
        " AND (EV.UNIT_ID = EV.LOCATION_UNIT_ID)              -- no redirected records\n" & _
        " AND (EV.IS_DELETED<>'Y')\n" & _
        " ORDER BY UNIT.NAME, EV.ENCOUNTER_ID, EV.EFFECTIVE_DATETIME_IN"

    ReplaceMacrosInSQL sql, DCC_ClassEvent, unit_id, start_date, finish_date
    rs.Open sql, g_cnADO, adOpenStatic

    n = GrowResultsBy(rs.RecordCount)           'returns last used slot
    
    Do While Not rs.eof
        n = n + 1                               'ready for next
        
        With m_results(n)
            .event_type = DCC_ClassEvent
            .unit_id = rs("UNIT_ID")
            .location_unit_id = rs("LOCATION_UNIT_ID")
            .unit_name = rs("UNIT_NAME")
            .report_date = rs("REPORT_DATE")
            .event_datetime = rs("EVENT_DATETIME")
            .encounter_id = rs("ENCOUNTER_ID")
            .patient_acct = rs("ACCT_NUMBER")
            .patient_name = rs("PATIENT_NAME")
            .stage_name = rs("STAGE_NAME") & ""
            .proc_id = rs("PROCEDURE_EVENT_ID")
            .effective_in = rs("EFFECTIVE_DATETIME_IN")
            .effective_out = rs("EFFECTIVE_DATETIME_OUT")
            
            next_datetime_in = rs("NEXT_DATETIME_IN")
            
            'Diagnosis:
            .problem_LVC = DCC_OVERLAPPING_OP_PROC_CLASS

            If (next_datetime_in > .effective_in) Then
                ' The classification starts after the procedure.  This is normal.
                ' Suggest a change to procedure OUT time
                .incorrect_time = .effective_out
                .correct_time = rs("NEXT_DATETIME_IN")
                .num_hours = DateDiff("s", .correct_time, .effective_out) / 3600#
                .suggestion_code = DCC_SUGGEST_SET_OUT_TIME
                .can_fix = True
            Else
                ' The classification should be moved; don't touch the procedure
                ' Remove this from the list
                n = n - 1
            End If
        End With

        rs.MoveNext
    Loop
    
    rs.Close
    
    
    '==========================================================================
    ' Check for classification that overlaps an outpatient procedure
    ' (procedure starts at same time or comes after)
    ' (SLOW query)
    '==========================================================================
    sql = " SELECT EV.REPORT_DATE, EV.UNIT_ID, UNIT.NAME AS UNIT_NAME, EV.LOCATION_UNIT_ID,\n" & _
        "     EV.CLASSIFICATION_EVENT_ID, CLASSIFICATION_DATETIME AS EVENT_DATETIME, EV.ENCOUNTER_ID,\n" & _
        "     EV.EFFECTIVE_DATETIME_IN, EV.EFFECTIVE_DATETIME_OUT,\n" & _
        "     STAGE.SHORT_NAME AS STAGE_NAME,\n" & _
        "     NEXT_PROC.DATETIME_IN AS NEXT_DATETIME_IN,\n" & _
        "     NEXT_PROC.DATETIME_OUT AS NEXT_DATETIME_OUT,\n" & _
        "     E.ACCT_NUMBER,\n" & _
        "     PERSON.LAST_NAME + ', ' + ISNULL(PERSON.FIRST_NAME,'') + ' ' + ISNULL(PERSON.MIDDLE_NAME,'') AS PATIENT_NAME\n" & _
        " FROM CLASSIFICATION_EVENT AS EV\n"
    sql = sql & _
        " INNER JOIN (\n" & _
        "    -- Find any outpatient procedure that overlaps this classification.\n" & _
        "    -- There might be more than one so find the lowest time (group).\n" & _
        "    SELECT C1.CLASSIFICATION_EVENT_ID,\n" & _
        "       MIN(P2.EFFECTIVE_DATETIME_IN) AS DATETIME_IN,\n" & _
        "       MAX(P2.EFFECTIVE_DATETIME_OUT) AS DATETIME_OUT\n" & _
        "    FROM CLASSIFICATION_EVENT AS C1\n" & _
        "    INNER JOIN PROCEDURE_EVENT AS P2\n" & _
        "      ON (P2.UNIT_ID = C1.UNIT_ID)\n" & _
        "      AND (P2.ENCOUNTER_ID = C1.ENCOUNTER_ID)\n" & _
        "      AND (P2.EFFECTIVE_DATETIME_IN >= C1.EFFECTIVE_DATETIME_IN)\n" & _
        "      AND (P2.EFFECTIVE_DATETIME_IN < C1.EFFECTIVE_DATETIME_OUT)\n" & _
        "      AND (P2.IS_DELETED<>'Y')\n" & _
        "    WHERE C1.UNIT_ID=@UNIT_ID\n" & _
        "    AND C1.REPORT_DATE BETWEEN @START_DATE AND @FINISH_DATE\n" & _
        "    AND C1.IS_DELETED<>'Y'\n" & _
        "    AND P2.STAGE=0\n" & _
        "    GROUP BY C1.CLASSIFICATION_EVENT_ID\n" & _
        " ) AS NEXT_PROC\n" & _
        " ON (NEXT_PROC.CLASSIFICATION_EVENT_ID = EV.CLASSIFICATION_EVENT_ID)\n"
    sql = sql & _
        " INNER JOIN ENCOUNTER AS E ON (E.ENCOUNTER_ID = EV.ENCOUNTER_ID)\n" & _
        " INNER JOIN PERSON ON (PERSON.PERSON_ID = E.PERSON_ID)\n" & _
        " INNER JOIN UNIT ON (UNIT.UNIT_ID = EV.UNIT_ID)\n" & _
        " LEFT  JOIN STAGE ON (STAGE.METHODOLOGY_ID = EV.METHODOLOGY_ID) AND (STAGE.STAGE = EV.STAGE)\n" & _
        " WHERE EV.REPORT_DATE BETWEEN @START_DATE AND @FINISH_DATE\n" & _
        " AND EV.UNIT_ID=@UNIT_ID\n" & _
        " AND (EV.UNIT_ID = EV.LOCATION_UNIT_ID)              -- no redirected records\n" & _
        " AND (EV.IS_DELETED<>'Y')\n" & _
        " ORDER BY UNIT.NAME, EV.ENCOUNTER_ID, EV.EFFECTIVE_DATETIME_IN"

    ReplaceMacrosInSQL sql, DCC_ClassEvent, unit_id, start_date, finish_date
    rs.Open sql, g_cnADO, adOpenStatic

    n = GrowResultsBy(rs.RecordCount)           'returns last used slot
    
    Do While Not rs.eof
        n = n + 1                               'ready for next
        
        With m_results(n)
            .event_type = DCC_ClassEvent
            .unit_id = rs("UNIT_ID")
            .location_unit_id = rs("LOCATION_UNIT_ID")
            .unit_name = rs("UNIT_NAME")
            .report_date = rs("REPORT_DATE")
            .event_datetime = rs("EVENT_DATETIME")
            .encounter_id = rs("ENCOUNTER_ID")
            .patient_acct = rs("ACCT_NUMBER")
            .patient_name = rs("PATIENT_NAME")
            .stage_name = rs("STAGE_NAME") & ""
            .class_id = rs("CLASSIFICATION_EVENT_ID")
            .effective_in = rs("EFFECTIVE_DATETIME_IN")
            .effective_out = rs("EFFECTIVE_DATETIME_OUT")
            
            next_datetime_in = rs("NEXT_DATETIME_IN")
            next_datetime_out = rs("NEXT_DATETIME_OUT")
            
            'Diagnosis:
            .problem_LVC = DCC_OVERLAPPING_OP_PROC_CLASS

            If (next_datetime_in = .effective_in) Then
                ' The procedure starts at the same time
                ' Suggest a change to class IN time
                .incorrect_time = .effective_in
                .correct_time = next_datetime_out
                .num_hours = DateDiff("s", .correct_time, .effective_out) / 3600#
                .suggestion_code = DCC_SUGGEST_SET_IN_TIME
                .can_fix = True
            ElseIf (next_datetime_in > .effective_in) Then
                ' The procedure is during the classification
                .suggestion_code = DCC_SUGGEST_EDIT_BY_HAND
                .result_details = "Is this the correct stage for the procedure?"
                .can_fix = False
            End If
        End With

        rs.MoveNext
    Loop
    
    rs.Close
    
    
    
    
    ReDim Preserve m_results(0 To n)
    Exit Sub

errHandler:
    g_util.ThrowError "CheckOverlappingOpProcClass"
    Resume  'debug
End Sub


'==============================================================================
' Check for invalid redirected class/proc in/out times vs admit/discharge
'==============================================================================
Private Function RedirectedClassProcInOutSQL(event_type As DCCEventType, unit_id As Long, start_date As Date, finish_date As Date) As String
    On Error GoTo errHandler
    Dim sql As String
    
    'This query only looks for records that were redirected to another unit.
    'Look for an encounter location in case the event was redirected back into a unit where the patient was.
    ' (FAST query)
    sql = _
        " SELECT EV.REPORT_DATE, EV.UNIT_ID, UNIT.NAME AS UNIT_NAME, EV.LOCATION_UNIT_ID,\n" & _
        "     EV.@ID_COLUMN, EV.ENCOUNTER_ID,\n" & _
        "     EV.LOS_HOURS,\n" & _
        "     EV.EFFECTIVE_DATETIME_IN,\n" & _
        "     EV.EFFECTIVE_DATETIME_OUT,\n" & _
        "     STAGE.SHORT_NAME AS STAGE_NAME,\n" & _
        "     E.ACCT_NUMBER,\n" & _
        "     PERSON.LAST_NAME + ', ' + ISNULL(PERSON.FIRST_NAME,'') + ' ' + ISNULL(PERSON.MIDDLE_NAME,'') AS PATIENT_NAME,\n" & _
        "     E.REGISTRATION_DATETIME,\n" & _
        "     E.ADMISSION_DATETIME,\n" & _
        "     E.DEPARTURE_DATETIME,\n" & _
        "     E.DISCHARGE_DATETIME,\n" & _
        "     EL.EFFECTIVE_DATETIME_IN AS LOC_IN,\n" & _
        "     EL.EFFECTIVE_DATETIME_OUT AS LOC_OUT,\n"

    If (event_type = DCC_ClassEvent) Then
        sql = sql & " EV.CLASSIFICATION_DATETIME AS EVENT_DATETIME, EV.DATETIME_OUT\n"
    Else
        sql = sql & " EV.PROCEDURE_DATETIME AS EVENT_DATETIME,      EV.DEPARTURE_DATETIME AS DATETIME_OUT\n"
    End If

    sql = sql & _
        " FROM @TABLE_NAME AS EV\n" & _
        " INNER JOIN ENCOUNTER AS E ON (E.ENCOUNTER_ID = EV.ENCOUNTER_ID)\n" & _
        " LEFT JOIN ENCOUNTER_LOCATION AS EL\n" & _
        "   -- see where the patient was at this time (any unit)\n" & _
        "   ON (EL.ENCOUNTER_ID = EV.ENCOUNTER_ID)\n" & _
        "   AND (EL.EFFECTIVE_DATETIME_IN <= EV.EFFECTIVE_DATETIME_IN)\n" & _
        "   AND (EL.EFFECTIVE_DATETIME_OUT > EV.EFFECTIVE_DATETIME_IN)\n" & _
        " INNER JOIN PERSON ON (PERSON.PERSON_ID = E.PERSON_ID)\n" & _
        " INNER JOIN UNIT ON (UNIT.UNIT_ID = EV.UNIT_ID)\n" & _
        " LEFT  JOIN STAGE ON (STAGE.METHODOLOGY_ID = EV.METHODOLOGY_ID) AND (STAGE.STAGE = EV.STAGE)\n" & _
        " WHERE EV.REPORT_DATE BETWEEN @START_DATE AND @FINISH_DATE\n" & _
        " AND EV.UNIT_ID=@UNIT_ID\n" & _
        " AND (EV.UNIT_ID <> EV.LOCATION_UNIT_ID)              -- redirected records only\n" & _
        " AND (EV.IS_DELETED<>'Y')\n" & _
        "\n" & _
        " ORDER BY UNIT.NAME, EV.EFFECTIVE_DATETIME_IN"

    ReplaceMacrosInSQL sql, event_type, unit_id, start_date, finish_date

    RedirectedClassProcInOutSQL = sql
    Exit Function
    
errHandler:
    g_util.ThrowError "RedirectedClassProcInOutSQL"
    Resume  'debug
End Function


Private Sub CheckRedirectedClassProcInOut(event_type As DCCEventType, unit_id As Long, start_date As Date, finish_date As Date)
    On Error GoTo errHandler
    Dim rs As New Recordset
    Dim sql As String
    Dim n As Long
    Dim visit_start As Variant, visit_finish As Variant, loc_in As Variant, loc_out As Variant

    m_progress_bar.value = m_progress_bar.value + DateDiff("d", start_date, finish_date) + 1
    
    sql = RedirectedClassProcInOutSQL(event_type, unit_id, start_date, finish_date)
    rs.Open sql, g_cnADO, adOpenStatic

    n = GrowResultsBy(rs.RecordCount)           'returns last used slot
    
    Do While Not rs.eof
        n = n + 1                               'ready for next
        With m_results(n)
            .event_type = event_type
            .unit_id = rs("UNIT_ID")
            .location_unit_id = rs("LOCATION_UNIT_ID")
            .unit_name = rs("UNIT_NAME")
            .report_date = rs("REPORT_DATE")
            .event_datetime = rs("EVENT_DATETIME")
            .encounter_id = rs("ENCOUNTER_ID")
            .patient_acct = rs("ACCT_NUMBER")
            .patient_name = rs("PATIENT_NAME")
            .stage_name = rs("STAGE_NAME") & ""
            .effective_in = rs("EFFECTIVE_DATETIME_IN")
            .effective_out = rs("EFFECTIVE_DATETIME_OUT")
            .num_hours = rs("LOS_HOURS")
            
            'g_util.Min/Max deal with nulls; start should have a value; finish could be null
            visit_start = g_util.Min(rs("REGISTRATION_DATETIME"), rs("ADMISSION_DATETIME"))
            visit_finish = g_util.Max(rs("DEPARTURE_DATETIME"), rs("DISCHARGE_DATETIME"))
            'see if the patient was in a unit somewhere (could be outside of the visit times)
            loc_in = rs("LOC_IN")
            loc_out = rs("LOC_OUT")

            If (event_type = DCC_ClassEvent) Then
                .class_id = rs("CLASSIFICATION_EVENT_ID")
            Else
                .proc_id = rs("PROCEDURE_EVENT_ID")
            End If
        
            'Diagnose
            If (.num_hours = 0) Then
                'This is so easy to do with redirection (for class anyway) - do not offer to delete
                'This needss to be looked at.
                .problem_LVC = IIf(event_type = DCC_ClassEvent, DCC_REDIRECTED_CLASS_ZERO_LENGTH, DCC_RECIRECTED_PROC_ZERO_LENGTH)
                .suggestion_code = DCC_SUGGEST_NOTHING
                .can_fix = False
            ElseIf IsNull(rs("DATETIME_OUT")) Then
                'Missing OUT time
                .problem_LVC = IIf(event_type = DCC_ClassEvent, DCC_REDIRECTED_CLASS_MISSING_OUT, DCC_RECIRECTED_PROC_MISSING_OUT)
                .suggestion_code = DCC_SUGGEST_NOTHING
                .can_fix = False
            ElseIf IsDate(loc_in) Then
                'The patient was in a location at the time of this event
                n = n - 1                           'skip it
            ElseIf (.effective_in < visit_start) Then
                'The patient was not in the hospital yet
                .problem_LVC = IIf(event_type = DCC_ClassEvent, DCC_REDIRECTED_CLASS_BEGINS_BEFORE_ADMISSION, DCC_REDIRECTED_PROC_BEGINS_BEFORE_ADMISSION)
                .incorrect_time = .effective_in
                .correct_time = visit_start
                .num_hours = DateDiff("s", .effective_in, visit_start) / 3600#
                .suggestion_code = DCC_SUGGEST_SET_IN_TIME
                .can_fix = True
            ElseIf IsDate(visit_finish) And (.effective_out > visit_finish) Then
                'The patient left the hospital
                .problem_LVC = IIf(event_type = DCC_ClassEvent, DCC_REDIRECTED_CLASS_ENDS_AFTER_DISCHARGE, DCC_REDIRECTED_PROC_ENDS_AFTER_DISCHARGE)
                .incorrect_time = .effective_out
                .correct_time = visit_finish
                .num_hours = DateDiff("s", visit_finish, .effective_out) / 3600#
                .suggestion_code = DCC_SUGGEST_SET_OUT_TIME
                .can_fix = True
            Else
                'No problems detected
                n = n - 1
            End If
        
        End With

        rs.MoveNext
    Loop
    
    rs.Close

    ReDim Preserve m_results(0 To n)
    Exit Sub
    
errHandler:
    g_util.ThrowError "CheckRedirectedClassProcInOut"
    Resume  'debug
End Sub

Private Sub CheckRedirectedClassInOut(unit_id As Long, start_date As Date, finish_date As Date)
    On Error GoTo errHandler
    CheckRedirectedClassProcInOut DCC_ClassEvent, unit_id, start_date, finish_date
    Exit Sub
errHandler:
    g_util.ThrowError "CheckRedirectedClassInOut"
End Sub

Private Sub CheckRedirectedProcInOut(unit_id As Long, start_date As Date, finish_date As Date)
    On Error GoTo errHandler
    CheckRedirectedClassProcInOut DCC_PROCEvent, unit_id, start_date, finish_date
    Exit Sub
errHandler:
    g_util.ThrowError "CheckRedirectedProcInOut"
End Sub


Private Sub CheckUnitValidDay(unit_id As Long, start_date As Date, finish_date As Date)
    On Error GoTo errHandler
    Dim rs As New Recordset
    Dim sql As String
    Dim n As Long
    
    m_progress_bar.value = m_progress_bar.value + DateDiff("d", m_start, m_finish) + 1

    ' Look at the Valid Day flag and explain what is missing
    ' (FAST query)
    sql = "SELECT REPORT_DATE, UNIT.NAME AS UNIT_NAME, PRORATED_WORKLOAD\n" & _
        " FROM RPT_VALID_DAY AS VALID\n" & _
        " INNER JOIN UNIT ON (UNIT.UNIT_ID=VALID.UNIT_ID)\n" & _
        " WHERE VALID.UNIT_ID=" & unit_id & "\n" & _
        " AND REPORT_DATE BETWEEN " & g_dbutil.SQL_Date(start_date) & " AND " & g_dbutil.SQL_Date(finish_date) & "\n" & _
        " AND IS_VALID_DAY<>'Y'\n" & _
        " ORDER BY REPORT_DATE, UNIT.NAME"
    sql = Replace(sql, "\n", vbCrLf)
    rs.Open sql, g_cnADO, adOpenStatic
    
    n = GrowResultsBy(rs.RecordCount)
    
    Do While Not rs.eof
        n = n + 1
        With m_results(n)
            .event_type = DCC_NoEvent
            .problem_LVC = DCC_INVALID_DAY
            .unit_id = m_unit_id
            .location_unit_id = .unit_id
            .unit_name = rs("UNIT_NAME")
            .report_date = rs("REPORT_DATE")
            .event_datetime = .report_date
            .suggestion_code = IIf(g_dbutil.DBToDouble(rs("PRORATED_WORKLOAD")) > 0, DCC_SUGGEST_ADD_ACTUAL_STAFF, DCC_SUGGEST_CLASSIFY_PATIENTS)
            .can_fix = False
        End With

        rs.MoveNext
    Loop
    
    rs.Close
    Exit Sub

errHandler:
    g_util.ThrowError "CheckUnitValidDay"
    Resume  'debug
End Sub

Private Sub CheckUnitClassVsActualHours(unit_id As Long, start_date As Date, finish_date As Date)
    Dim rs As New Recordset
    Dim sql As String
    Dim n As Long, diff As Double
    
    m_progress_bar.value = m_progress_bar.value + DateDiff("d", m_start, m_finish) + 1

    ' Compare ADT hours to class hours
    ' (FAST query)
    sql = "SELECT REPORT_DATE, UNIT.NAME AS UNIT_NAME,\n" & _
        "   HR24_PATIENT_HOURS AS CLASSIFIED_HOURS,\n" & _
        "   (INPATIENT_LOS_HOURS + OUTPATIENT_LOS_HOURS) AS ADT_HOURS,\n" & _
        "   HR24_PATIENT_HOURS - (INPATIENT_LOS_HOURS + OUTPATIENT_LOS_HOURS) AS DIFF\n" & _
        " FROM RPT_VALID_DAY AS VALID\n" & _
        " INNER JOIN UNIT ON (UNIT.UNIT_ID=VALID.UNIT_ID)\n" & _
        " WHERE VALID.UNIT_ID=" & unit_id & "\n" & _
        " AND REPORT_DATE BETWEEN " & g_dbutil.SQL_Date(start_date) & " AND " & g_dbutil.SQL_Date(finish_date) & "\n" & _
        " AND ABS(HR24_PATIENT_HOURS - (INPATIENT_LOS_HOURS + OUTPATIENT_LOS_HOURS)) > 0.01\n" & _
        " ORDER BY REPORT_DATE, UNIT.NAME"
    sql = Replace(sql, "\n", vbCrLf)
    rs.Open sql, g_cnADO, adOpenStatic
    
    n = GrowResultsBy(rs.RecordCount)
    
    Do While Not rs.eof
        n = n + 1
        With m_results(n)
            .event_type = DCC_NoEvent
            .unit_id = m_unit_id
            .location_unit_id = .unit_id
            .unit_name = rs("UNIT_NAME")
            .report_date = rs("REPORT_DATE")
            .event_datetime = .report_date
            .can_fix = False
            
            diff = g_dbutil.DBToDouble(rs("DIFF"))
            .num_hours = Round(diff, 2)

            If (diff < 0) Then
                .problem_LVC = DCC_UNCLASSIFIED_HOURS
                .suggestion_code = DCC_SUGGEST_RUN_UNCLASSIFIED_DETAIL
            Else
                .problem_LVC = DCC_OVERCLASSIFIED_HOURS
                .suggestion_code = DCC_SUGGEST_RUN_PATIENT_LEVEL_TESTS
            End If
        End With

        rs.MoveNext
    Loop
    
    rs.Close
    Exit Sub

errHandler:
    g_util.ThrowError "CheckUnitClassVsActualHours"
End Sub


'==============================================================================
' Main entry point #2:
' Attempts to fix all selected problems
' Updates the results
'==============================================================================
Public Sub DCC_FixSelectedProblems( _
    log_fixes As Boolean, _
    progress_bar As ProgressBar, _
    dcc() As DCCElement _
)
    On Error GoTo errHandler
    Dim i As Long
    Dim action_taken As String
    
    If (m_processing) Then Exit Sub         'prevent recursion
    m_processing = True
    
    If (cdlDCCChecks.ItemCount = 0) Then
        cdlDCCChecks.LoadLVC LVC_DATA_CONSISTENCY_CHECKS
    End If
    
    progress_bar.value = 0
    progress_bar.Max = UBound(dcc)
    
    For i = 1 To UBound(dcc)
        progress_bar.value = i
        
        With dcc(i)
            If (.is_checked) And (.can_fix) And (.result = DCC_RESULT_NONE) Then
                
                Select Case .suggestion_code
                Case DCC_SUGGEST_NOTHING
                    .result = DCC_RESULT_CANNOT_FIX
                Case DCC_SUGGEST_ADD_ACTUAL_STAFF
                    .result = DCC_RESULT_CANNOT_FIX
                Case DCC_SUGGEST_CLASSIFY_PATIENTS
                    .result = DCC_RESULT_CANNOT_FIX
                
                Case DCC_SUGGEST_CHANGE_UNIT
                    If (.event_type = DCC_ClassEvent) Then
                        ChangeClassUnit dcc(i)
                    Else
                        ChangeProcUnit dcc(i)
                    End If
                
                Case DCC_SUGGEST_DELETE
                    If (.event_type = DCC_ClassEvent) Then
                        DeleteClass dcc(i)
                    Else
                        DeleteProc dcc(i)
                    End If
                
                Case DCC_SUGGEST_EDIT_BY_HAND
                    .result = DCC_RESULT_CANNOT_FIX
                Case DCC_SUGGEST_RUN_UNCLASSIFIED_DETAIL
                    .result = DCC_RESULT_CANNOT_FIX
                Case DCC_SUGGEST_RUN_PATIENT_LEVEL_TESTS
                    .result = DCC_RESULT_CANNOT_FIX
                
                Case DCC_SUGGEST_SET_IN_TIME
                    If (.event_type = DCC_ClassEvent) Then
                        ChangeClassInTime dcc(i)
                    Else
                        ChangeProcInTime dcc(i)
                    End If
                    
                Case DCC_SUGGEST_SET_OUT_TIME
                    If (.event_type = DCC_ClassEvent) Then
                        ChangeClassOutTime dcc(i)
                    Else
                        ChangeProcOutTime dcc(i)
                    End If
                
                End Select
                
                ' Add fixes and unexpected errors to the event log
                '
                If (log_fixes) And (.result <> DCC_RESULT_CANNOT_FIX) Then
                    g_event.AddEventLogEntry EVENT_SOURCE_DATA_CONSISTENCY, _
                        IIf(.result = DCC_RESULT_ERROR, EVENT_TYPE_ERROR, EVENT_TYPE_INFO), _
                        IIf(.result = DCC_RESULT_ERROR, EVENT_CATEGORY_UNEXPECTED, EVENT_CATEGORY_PROCESSED), _
                        cdlDCCChecks.desc(.problem_LVC) & " - " & DCC_DescribeResult(.result), _
                        .result_details, 0, .unit_id, .encounter_id
                End If
            End If
        End With

        DoEvents
        If (m_stop_processing) Then Exit Sub
    Next i

    m_processing = False
    Exit Sub

errHandler:
    m_processing = False
    g_util.ThrowError "DCC_FixSelectedProblems"
    Resume  'debug
End Sub


Private Sub DeleteClass(dccInfo As DCCElement)
    On Error GoTo errHandler
    Dim Class As New ClassifyEvent, inds As String
    
    Class.Load dccInfo.class_id
    inds = Class.StringOutputIndicators
    
    If (REALLY_FIX) Then Class.Delete
    dccInfo.result = DCC_RESULT_FIXED
    dccInfo.result_details = "Deleted (" & inds & ")"
    Exit Sub

errHandler:
    dccInfo.result = DCC_RESULT_ERROR
    dccInfo.result_details = Err.Description
End Sub

Private Sub DeleteProc(dccInfo As DCCElement)
    On Error GoTo errHandler
    Dim proc As New ProcedureEvent, inds As String
    
    proc.Load dccInfo.proc_id
    inds = proc.StringOutputProcedures

    If (REALLY_FIX) Then proc.Delete
    dccInfo.result = DCC_RESULT_FIXED
    dccInfo.result_details = "Deleted (" & inds & ")"
    Exit Sub

errHandler:
    dccInfo.result = DCC_RESULT_ERROR
    dccInfo.result_details = Err.Description
End Sub

Private Sub ChangeClassUnit(dccInfo As DCCElement)
    On Error GoTo errHandler
    Dim Class As New ClassifyEvent
    Class.Load dccInfo.class_id
    dccInfo.result_details = "Changed unit from " & Class.Unit.Name & " to " & dccInfo.in_patient_location
    Class.UnitID = dccInfo.in_location_unit_id
    If (REALLY_FIX) Then Class.SaveNoUpdateExisting
    dccInfo.result = DCC_RESULT_FIXED
    Exit Sub
errHandler:
    dccInfo.result = DCC_RESULT_ERROR
    dccInfo.result_details = Err.Description
End Sub

Private Sub ChangeProcUnit(dccInfo As DCCElement)
    On Error GoTo errHandler
    Dim proc As New ProcedureEvent
    proc.Load dccInfo.proc_id
    dccInfo.result_details = "Changed unit from " & proc.Unit.Name & " to " & dccInfo.in_patient_location
    proc.UnitID = dccInfo.in_location_unit_id
    If (REALLY_FIX) Then proc.SaveNoUpdateExisting
    dccInfo.result = DCC_RESULT_FIXED
    Exit Sub
errHandler:
    dccInfo.result = DCC_RESULT_ERROR
    dccInfo.result_details = Err.Description
End Sub

Private Sub ChangeClassInTime(dccInfo As DCCElement)
    On Error GoTo errHandler
    Dim Class As New ClassifyEvent
    Class.Load dccInfo.class_id
    Class.DateTimeIn = dccInfo.correct_time
    Class.AdjustMiscTimesForValidation
    If (REALLY_FIX) Then Class.SaveNoUpdateExisting
    dccInfo.result = DCC_RESULT_FIXED
    dccInfo.result_details = "Changed IN from " & dccInfo.incorrect_time & " to " & dccInfo.correct_time
    Exit Sub
errHandler:
    dccInfo.result = DCC_RESULT_ERROR
    dccInfo.result_details = Err.Description
End Sub

Private Sub ChangeProcInTime(dccInfo As DCCElement)
    On Error GoTo errHandler
    Dim proc As New ProcedureEvent
    proc.Load dccInfo.proc_id
    proc.ProcedureDateTime = dccInfo.correct_time
    If (REALLY_FIX) Then proc.SaveNoUpdateExisting
    dccInfo.result = DCC_RESULT_FIXED
    dccInfo.result_details = "Changed IN from " & dccInfo.incorrect_time & " to " & dccInfo.correct_time
    Exit Sub
errHandler:
    dccInfo.result = DCC_RESULT_ERROR
    dccInfo.result_details = Err.Description
End Sub

Private Sub ChangeClassOutTime(dccInfo As DCCElement)
    On Error GoTo errHandler
    Dim Class As New ClassifyEvent
    Class.Load dccInfo.class_id
    Class.DateTimeOut = dccInfo.correct_time
    Class.AdjustMiscTimesForValidation
    If (REALLY_FIX) Then Class.SaveNoUpdateExisting
    dccInfo.result = DCC_RESULT_FIXED
    dccInfo.result_details = "Changed OUT from " & dccInfo.incorrect_time & " to " & dccInfo.correct_time
    Exit Sub
errHandler:
    dccInfo.result = DCC_RESULT_ERROR
    dccInfo.result_details = Err.Description
End Sub

Private Sub ChangeProcOutTime(dccInfo As DCCElement)
    On Error GoTo errHandler
    Dim proc As New ProcedureEvent
    proc.Load dccInfo.proc_id
    proc.DepartureDateTime = dccInfo.correct_time
    If (REALLY_FIX) Then proc.SaveNoUpdateExisting
    dccInfo.result = DCC_RESULT_FIXED
    dccInfo.result_details = "Changed OUT from " & dccInfo.incorrect_time & " to " & dccInfo.correct_time
    Exit Sub
errHandler:
    dccInfo.result = DCC_RESULT_ERROR
    dccInfo.result_details = Err.Description
End Sub


Public Function DCC_DescribeSuggestion(dccInfo As DCCElement) As String
    Dim result As String
    
    Select Case dccInfo.suggestion_code
    Case DCC_SUGGEST_NOTHING
        result = ""
    Case DCC_SUGGEST_ADD_ACTUAL_STAFF
        result = "Add actual staffing"
    Case DCC_SUGGEST_CHANGE_UNIT
        result = "Change unit to " & dccInfo.in_patient_location
    Case DCC_SUGGEST_CLASSIFY_PATIENTS
        result = "Classify patients"
    Case DCC_SUGGEST_DELETE
        result = "Delete"
    Case DCC_SUGGEST_EDIT_BY_HAND
        result = "edit by hand"
    Case DCC_SUGGEST_RUN_PATIENT_LEVEL_TESTS
        result = "Run patient-level tests"
    Case DCC_SUGGEST_RUN_UNCLASSIFIED_DETAIL
        result = "Run Unclassified Detail report"
    Case DCC_SUGGEST_SET_IN_TIME
        result = "Set IN = " & dccInfo.correct_time
    Case DCC_SUGGEST_SET_OUT_TIME
        result = "Set OUT = " & dccInfo.correct_time
    End Select

    DCC_DescribeSuggestion = result
End Function

Public Function DCC_DescribeResult(Code As DCCResult)
    Dim result As String
    
    Select Case Code
    Case DCC_RESULT_NONE
        result = ""
    Case DCC_RESULT_FIXED
        result = "fixed"
    Case DCC_RESULT_CANNOT_FIX
        result = "cannot fix"
    Case DCC_RESULT_ERROR
        result = "error"
    End Select

    DCC_DescribeResult = result
End Function
