VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PFSCodeDescList"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'
' This class makes it easy to manipulate code/description lookup lists.
' Data is pulled from cn.
' Note: All codes are converted to strings.
'
' Sample code:
'
'   Dim cdl as PFSCodeDescList, aCode as String
'
'   cdl.LoadLVC LVC_ADMISSION_CODE, cdlShowCodeWithDesc
'   cdl.FillComboBox cboAdmitCodes
'
'   aCode = cdl.code (cboAdmitCodes.Text)   'Get the code for selected item
'

'These options can be combined (they are bit flags)
Public Enum cdlOptions
    cdlNoOptionSelected = 0                 'Default is to show description only
    cdlShowCodeWithDesc = 1                 'Display code and description
    cdlShowCodeOnly = 2                     'Display code only
    
    cdlInsertAllChoice = 4                  'Add (all) to the list
    cdlInsertNoneChoice = 8                 'Add (none) to the list
    cdlInsertNullStringChoice = 16          'Add a null string to the list
    
    cdlOrderByCode = 32                     'Order by code
    cdlOrderByDesc = 64                     'Order by description
    
    cdlLookupByCodeOnly = 128               'This option allows non-unique descriptions
End Enum

Private m_Code      As Collection           'list of codes
Private m_Desc      As Collection           'list of descriptions
Private m_options   As cdlOptions

Private Sub Class_Initialize()
    Set m_Code = New Collection
    Set m_Desc = New Collection
End Sub

Private Sub Class_Terminate()
    Set m_Code = Nothing
    Set m_Desc = Nothing
End Sub


Public Sub Reset()
    Set m_Code = New Collection
    Set m_Desc = New Collection
    m_options = cdlNoOptionSelected
End Sub

'
' Add a code/description pair to the list
'
Friend Sub DoAddCodeDesc(ByVal sCode As String, ByVal sDesc As String)
    On Error GoTo errHandler

    'Make it so you can look up by code or desc
    'Be sure to add strings, not variants or numbers.  (numbers will be mistaken for an index)
    If (m_options And cdlLookupByCodeOnly) Then
        'Don't add to lookup by code
    Else
        m_Code.Add sCode, sDesc                   'item=code, key=desc
    End If
    
    m_Desc.Add sDesc, sCode                       'item=desc, key=code
    Exit Sub

errHandler:
    'exit quietly - duplicate key, for example
    Exit Sub
End Sub

Friend Sub AddCodeDesc(ByVal sCode As String, ByVal sDesc As String)
    On Error GoTo errHandler
    Dim Sequence As Integer, new_desc As String
    
    ' Lookup code by description is most common - we need the code for an item from a list.
    ' The problem is the description collection does not allow duplicate descriptions, even if the code is unique.
    ' This will detect this problem and append a sequence number to the description if needed.
    For Sequence = 0 To 99
        new_desc = sDesc & IIf(Sequence = 0, "", "(" & Sequence & ")")
        
        If (Code(new_desc) = "") Then
            DoAddCodeDesc sCode, new_desc
            Exit Sub
        End If
    Next Sequence

    'failed to add
    Exit Sub

errHandler:
    Exit Sub
End Sub


Private Sub AddOrderClause(ByRef sql As String, ByVal code_col As String, ByVal desc_col As String, ByVal options As Integer)
    If (options And cdlShowCodeWithDesc) Then
        'We're showing codes: order by code
        options = (options Or cdlOrderByCode)
    End If
    
    If (options And cdlOrderByCode) Then
        sql = sql & " ORDER BY " & code_col
    ElseIf (options And cdlOrderByDesc) Then
        sql = sql & " ORDER BY " & desc_col
    Else
        sql = sql & " ORDER BY SEQUENCE"
    End If
End Sub
    
'
' Find a code given the description, or index (1-n)
'
Public Function Code(ByRef sDesc As Variant) As String
    On Error GoTo errHandler
    Code = m_Code(sDesc)
    Exit Function
    
errHandler:
    Code = ""                                   'invalid index or description
    Exit Function
End Function

'
' Find a description given the code, or index (1-n)
' NOTE: Use Tools->Procedure Attributes to make this the default method
'
Public Function desc(ByRef sCode As Variant) As String
Attribute desc.VB_UserMemId = 0
    On Error GoTo errHandler
    desc = m_Desc(sCode)
    Exit Function
    
errHandler:
    desc = ""                                   'invalid index or code
    Exit Function
End Function

'
'Return the description if available, otherwise return the code
'
Public Function DescOrCode(ByRef Code As Variant) As String
    On Error GoTo errHandler
    DescOrCode = m_Desc(Code)
    Exit Function
errHandler:
    DescOrCode = Code
    Exit Function
End Function

'
' Find the index of a code (1-n)
'
Public Function IndexOfCode(ByVal sCode As String) As Long
    Dim i As Long
    
    On Error GoTo errHandler

    For i = 1 To m_Code.count
        If m_Code.Item(i) = sCode Then
            IndexOfCode = i
            Exit Function
        End If
    Next i
    IndexOfCode = -1
    Exit Function
    
errHandler:
    IndexOfCode = -1
    Exit Function
End Function
'
' Find the code of an index
'
Public Function CodeOfIndex(ByVal index As Long) As String
    On Error GoTo errHandler

    CodeOfIndex = m_Code.Item(index)
    Exit Function
errHandler:
    CodeOfIndex = "0"
End Function

'
' Find the listbox index of a code (0 - n-1)
'
Public Function ListIndexOfCode(ByVal sCode As String) As Long
    ListIndexOfCode = IndexOfCode(sCode) - 1
End Function

'
' Fill a Combo Box
'(declare ByVal so SecurityCodeDescList will compile)
Public Sub FillComboBox(ByVal cbo As ComboBox)
    FillComboOrList cbo
End Sub

'
' Fill a ListBox
'(declare ByVal so SecurityCodeDescList will compile)
Public Sub FillListBox(ByVal lst As ListBox)
    FillComboOrList lst
End Sub

Private Sub FillComboOrList(ByRef ctl As Control)
    'Fill a combo box or a list box with the descriptions
    Dim textItem As Variant
    With ctl
        .Clear                                  'clear the list
        For Each textItem In m_Desc
            .AddItem textItem                   'add items
        Next
        If (.ListCount > 0) Then .ListIndex = 0 'select first item
    End With
End Sub

'
' Fill an ActiveBar ComboList
' (Note: declare ctl as Variant so projects without ActiveBar will compile)
'
Public Sub FillComboList(ByRef ctl As Variant)
    'Fill a combo box or a list box with the descriptions
    Dim textItem As Variant
    With ctl
        .Clear                                  'clear the list
        For Each textItem In m_Desc
            .AddItem textItem                   'add items
        Next
        'If (.Count > 0) Then .Index = 0        'There is a Count property, but no .Index
    End With
End Sub

'
' Return the number of items in the list
'
Public Function ItemCount() As Integer
    ItemCount = m_Code.count
End Function

'
' Load the Code/Desc list using the SQL command provided.
' The command can do anything as long as the first two columns are
' code and description.  An optional third column will be added to the description.
'
Public Sub LoadSQL(ByVal sql As String, Optional ByVal options As cdlOptions = cdlNoOptionSelected)
    
    Dim rs As New Recordset, cn As Connection
    Dim Code As String, desc As String
    
    Me.Reset
    m_options = options
    
    'Add optional entries for a null code
    'These must be mutually exclusive because they all share the same code and the collection won't allow that
    If (options And cdlInsertAllChoice) Then
        AddCodeDesc "", "(all)"
    ElseIf (options And cdlInsertNoneChoice) Then
        AddCodeDesc "", "(none)"
    ElseIf (options And cdlInsertNullStringChoice) Then
        AddCodeDesc "", ""
    End If
    
    Set cn = g_dbutil.NewRemoteConnection()
    'Debug.Print sql
    rs.Open sql, cn, adOpenStatic
    
    Do While Not rs.EOF
        Code = rs(0) & ""
        desc = rs(1) & ""
        If Len(desc) = 0 Then
            desc = "(null)"
        End If
        If (rs.Fields.count = 3) Then
            If Not IsNull(rs(2)) Then
                'add an extended description
                desc = desc & "  (" & rs(2) & ")"
            End If
        End If

        If (options And cdlShowCodeWithDesc) Then
            AddCodeDesc Code, Code & " - " & desc
        ElseIf (options And cdlShowCodeOnly) Then
            AddCodeDesc Code, Code
        Else
            AddCodeDesc Code, desc
        End If
        rs.MoveNext
    Loop
    
    rs.Close
    cn.Close
End Sub


'
' Load the Code/Desc list using the value item list provided.
' (Note: declare valitems as Variant so projects without TrueDBGrid will compile)
'
Public Sub LoadValueItems(ByRef valitems As Variant)
    Dim i As Integer
    
    Me.Reset
    
    For i = 0 To valitems.count - 1
        AddCodeDesc valitems(i).value, valitems(i).DisplayValue
    Next i
End Sub

'
' Load a lookup value code list
'
Public Sub LoadLVC(ByVal lvc_key As Integer, Optional ByVal options As cdlOptions = cdlNoOptionSelected)
    Dim where_clause As String
      
    where_clause = "LOOKUP_DEFINITION_KEY = " & lvc_key
    
    LoadCodeDesc "LOOKUP_VALUE", "LOOKUP_CODE", "LOOKUP_DESCRIPTION", where_clause, options
End Sub

'
' Load a table with code and desc
' This is similar to LoadSQL, only it will add an ORDER BY clause for you.
'
Public Sub LoadCodeDesc( _
    ByVal table As String, ByVal code_col As String, ByVal desc_col As String, _
    Optional ByVal where_clause As String, _
    Optional ByVal options As cdlOptions = cdlNoOptionSelected _
)
    Dim sql As String
    
    sql = "SELECT " & code_col & ", " & desc_col & " FROM " & table
    If Len(where_clause) > 0 Then
        sql = sql & " WHERE " & where_clause
    End If
    
    AddOrderClause sql, code_col, desc_col, options
    LoadSQL sql, options
End Sub


Private Function AddCodeToString(s As String, Code As String)
    Dim result As String
    
    result = s
    If Len(result) > 0 Then
        result = result & ","
    End If
    
    'All codes have been stored as strings but SQL will care.
    'If it looks numeric, treat it as numeric.
    'Look out for lists with "(none)" choices and remove them here.
    If IsNumeric(Code) Then
        result = result & Code                      ' 123,456,789
    ElseIf Len(Code) > 0 Then
        result = result & "'" & Code & "'"          ' 'RN','LPN','NA'
    End If
    
    AddCodeToString = result
End Function

'
' Return a comma-separated list of ids from the selected list items.
' Strings are quoted - this makes a valid SQL list.
' Returns a zero if no items are selected (to avoid SQL syntax errors).
'
'(declare ByVal so SecurityCodeDescList will compile)
Public Function GetSelectedIDString(ByVal lst As ListBox) As String
    Dim result As String
    Dim i As Integer
    
    For i = 0 To lst.ListCount - 1
        If lst.Selected(i) Then
            result = AddCodeToString(result, m_Code(lst.list(i)))
        End If
    Next i
    
    If Len(result) = 0 Then result = "0"

    GetSelectedIDString = result
End Function

'
' Return a comma-separated list of all ids in the list
'
Public Function GetIDString() As String
    Dim result As String, Code As Variant

    For Each Code In m_Code
        result = AddCodeToString(result, CStr(Code))
    Next Code

    GetIDString = result
End Function


'
' Return an array of ids from selected list items (1-n).
' Returns a zero dimensioned array if no items are selected.
'
'(declare ByVal so SecurityCodeDescList will compile)
Public Sub GetSelectedIDArray(ByVal lst As ListBox, ByRef arr() As String)
    Dim result As String
    Dim i As Integer, count As Integer
    
    ReDim arr(0 To lst.SelCount)

    For i = 0 To lst.ListCount - 1
        If lst.Selected(i) Then
            count = count + 1
            arr(count) = m_Code(lst.list(i))
        End If
    Next i
End Sub


'
' Select all items in the list with matching codes
'
'(declare ByVal so SecurityCodeDescList will compile)
Public Sub SelectIDString(ByVal lst As ListBox, ByRef id_list As String)
    Dim arr() As String
    
    g_util.SplitTextOnChar id_list, ",", arr(), 1, 0
    SelectIDArray lst, arr()
End Sub


'
' Select all items in the list with matching codes
' Quoted strings are supported (i.e. a value SQL list)
'
'(declare ByVal so SecurityCodeDescList will compile)
Public Sub SelectIDArray(ByVal lst As ListBox, ByRef arr() As String)
    Dim i As Integer, idx As Integer, Code As String
    
    For i = 0 To lst.ListCount - 1
        lst.Selected(i) = False
    Next i
    
    For i = 1 To UBound(arr)
        ' Strip the quotes off a string
        Code = arr(i)
        If (Left$(Code, 1) = "'") Then Code = Mid$(Code, 2)
        If (Right$(Code, 1) = "'") Then Code = Left$(Code, Len(Code) - 1)
        
        idx = ListIndexOfCode(Code)
        If (idx >= 0) Then
            lst.Selected(idx) = True
        End If
    Next i
End Sub

'
'
'(declare ByVal so SecurityCodeDescList will compile)
Public Sub SelectComboBoxItem(ByVal cbo As ComboBox, ByVal Code As String)
    Dim idx As Integer
    
    idx = ListIndexOfCode(Code)
    If (idx >= 0) And (idx < cbo.ListCount) Then
        cbo.ListIndex = idx
    End If
End Sub



'
' Add support for a For...Each loop.
' Note: Use Tools->Procedure Attributes to hide this member and set its ID to -4.
'
Public Function NewEnum() As IUnknown
Attribute NewEnum.VB_MemberFlags = "40"
    Set NewEnum = m_Desc.[_NewEnum]
End Function

