VERSION 5.00
Begin VB.Form frmTimeout 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Timeout"
   ClientHeight    =   2625
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5460
   Icon            =   "frmTimeout.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2625
   ScaleWidth      =   5460
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdLoginAnother 
      Caption         =   "Login Another"
      Height          =   375
      Left            =   2700
      TabIndex        =   6
      Top             =   2160
      Width           =   1275
   End
   Begin VB.PictureBox Picture1 
      AutoSize        =   -1  'True
      BorderStyle     =   0  'None
      Height          =   480
      Left            =   120
      Picture         =   "frmTimeout.frx":06C2
      ScaleHeight     =   480
      ScaleWidth      =   480
      TabIndex        =   5
      Top             =   120
      Width           =   480
   End
   Begin VB.CommandButton cmdShutdown 
      Cancel          =   -1  'True
      Caption         =   "Shut Down"
      Height          =   375
      Left            =   4080
      TabIndex        =   4
      Top             =   2160
      Width           =   1275
   End
   Begin VB.CommandButton cmdContinue 
      Caption         =   "Continue"
      Default         =   -1  'True
      Height          =   375
      Left            =   1320
      TabIndex        =   3
      Top             =   2160
      Width           =   1275
   End
   Begin VB.TextBox txtPassword 
      Height          =   315
      IMEMode         =   3  'DISABLE
      Left            =   1740
      PasswordChar    =   "*"
      TabIndex        =   1
      Top             =   1680
      Width           =   3555
   End
   Begin VB.Label Label1 
      Caption         =   "Password:"
      Height          =   255
      Left            =   780
      TabIndex        =   2
      Top             =   1740
      Width           =   855
   End
   Begin VB.Label lblLockedBy 
      Caption         =   "Locked By"
      Height          =   1515
      Left            =   780
      TabIndex        =   0
      Top             =   60
      Width           =   4515
      WordWrap        =   -1  'True
   End
End
Attribute VB_Name = "frmTimeout"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'
' Application Timeout dialog box:
' This is shared by more than one application.
' g_user and g_timeout must be defined.
'
Private Sub Form_Load()
    Dim msg As String
    
    g_display.SetStdTheme Me, True
    Me.caption = "Application Locked"

    msg = App.ProductName & " " & App.title & " has been locked and can only" & _
        " be unlocked by " & g_user.LoginName & " (" & g_user.FullName & ")." & _
         vbCrLf & vbCrLf & _
        "Enter your password and press 'Continue' to unlock the application." & _
        "  Press 'Login Another' to login as another user." & _
        "  Press 'Shutdown' to shut the application down."
    lblLockedBy.caption = msg
End Sub

Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    If (g_timeout.QueryUnloadExitNow) Then Exit Sub
    
    If (UnloadMode = vbFormControlMenu) Then
        'Ignore the 'X' button on the dialog
        Beep
        Cancel = True
    End If
End Sub


Private Sub cmdContinue_Click()
    If Not g_user.IsValidPassword(txtPassword.Text) Then
        MsgBox "The password is incorrect.", , Me.caption
        txtPassword.SetFocus
        Exit Sub
    End If
    
    'OK, unlock the application.
    Unload Me
End Sub

Private Sub cmdLoginAnother_Click()
    'Login as another user (shutdown and restart)
    g_timeout.ShutdownNow PFS_EXIT_TIMEOUT, DEFAULT_SHUTDOWN, WITH_RESTART
End Sub

Private Sub cmdShutdown_Click()
    'Shutdown and do not restart
    g_timeout.ShutdownNow PFS_EXIT_TIMEOUT, DEFAULT_SHUTDOWN, NO_RESTART
End Sub

