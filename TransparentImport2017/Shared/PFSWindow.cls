VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PFSWindow"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'
' PFSWindow: Abstract window class
'
' All PFS Main windows need to implement this class.
'
' There are several ways to close a window:
'   The user presses 'X' on the window frame
'   The user double clicks on the window frame control box
'   The user chooses Close from the window frame control box
'   The form unloads itself
'   The application unloads the form
'
' In all cases, the window needs to check if changes need to be saved and
' then ask the user if they should be saved.  The key is to hook the
' QueryUnload method.  Any form can stop itself from being unloaded by
' setting Cancel to True inside this method.  The main application has
' a pointer to each main window so no window should unload itself.
'
' Copy this code to Form_QueryUnload:
'
'Private Sub Form_QueryUnload(Cancel As Integer, UnloadMode As Integer)
'    If g_timeout.QueryUnloadExitNow Then Exit Sub       'unload during shutdown
'
'    If OKToClose() Then             ' Ask to save changes
'        Cancel = True               ' OK but don't unload anyway
'        Me.Hide                     ' Hide instead
'    Else
'        Cancel = True               ' Don't unload; don't do anything
'    End If
'End Sub

'User preference bitmasks
'(VB allows a public enum but not constants so these are in an enum)
Public Enum UserPreferencesChangedEnum
    PREF_COLORS_CHANGED = 1
    PREF_TOOLBAR_CHANGED = 2
    PREF_RESERVED2 = 4
    PREF_RESERVED3 = 8
    ' ...
End Enum

Public Function FunctionName() As String
    'What is your function?  (Patient Selection, Classication, Actual Staffing, etc.)
    'This is like your window title only without any added info
End Function

Public Function OKToClose() As Boolean
    ' This should be called from Form_QueryUnload if the user closes the window.
    ' This will also be called by the main application when it wants to shut
    ' down.  If any window returns False, the application will not shut down.
    '
    ' If there are no changes that need to be saved, return True
    '
    ' If changes need to be saved, the user should be asked if they
    ' want to save changes and choose from Yes/No/Cancel.
    '
    ' If the user presses Yes or No, return True (OK to close)
    ' If the user presses Cancel, return False   (not OK to close)
    '
'Example:
'   if (need_save_changes) then
'       result = ask_save_changes()
'   else
'       result = true
'   endif
End Function

Public Function QueryUnsavedChanges() As String
    'This is different from OKToClose in that there is no user interaction.
    'CCOW needs to know if we have any unsaved changes or not.
    '
    'Reply with a null string if there are no unsaved changes.
    'The description should be suitable for a message box.
    '   Actual Stafing: "There is unsaved data"
    '   Classification: "There is an unsaved classification"
    '   Class by Name:  "There are unsaved classifications"
End Function

Public Sub CCOWStateChanged()
    'The CCOW state changed -- Update any CCOW icons
End Sub
 
Public Sub UserChanged()
    'The logged in user changed... most windows should hide themselves.
    'The current user is in g_user.
End Sub
 
Public Sub UnitChanged()
    'The selected unit has changed... refresh screen with new unit
    'The current unit is in g_unit.
End Sub

Public Sub DateChanged()
    'The global display date has changed (if you care)
End Sub

Public Sub PatientChanged()
    'A new patient has been selected... refresh screen if applicable
    'The selected patient (and encounter) is in g_app.CCOW.
End Sub

Public Sub DataChanged()
    'patient data changed... refresh your screen if applicable (same unit and date)
End Sub
 
Public Sub UserPreferencesChanged(bitmask As Long)
    'User Preferences changed... refresh screen with new colors
End Sub
