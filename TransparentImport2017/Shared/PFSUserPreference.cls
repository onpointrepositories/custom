VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PFSUserPreference"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'
' PFSUserPreference: load, save and define user preferences
'
'User Preference Keys

'DO NOT CHANGE these numbers -- they are stored in the database!
'These are not bitflags; they just need to be unique numbers.
Public Enum UserPreferenceKeys
    USERPREF_COLOR_SCHEME = 1
    USERPREF_ICON_SIZE = 2
    USERPREF_PASSWD_EXPIRE_WARN_DATE = 3                'date of last warning; for one warning per day
    'Assignment Module
    USERPREF_ASSIGN_AUTO_CONTINUITY_ON_NEW = 100
    USERPREF_ASSIGN_AUTO_REFRESH_ON_OPEN = 101
    USERPREF_ASSIGN_AUTO_ASSIGN_NEW_PATIENTS_ON_REFRESH = 102
    USERPREF_ASSIGN_HONOR_CONTINUITY = 103
    USERPREF_ASSIGN_SHOW_TEAM_MEMBERS = 104
    USERPREF_ASSIGN_PULL_UNASSIGNED_PATENTS = 105
    USERPREF_ASSIGN_SHOW_OPTIONS_ON_NEW = 106
    USERPREF_ASSIGN_SHOW_PATIENT_TYPE_INSTEAD_OF_WORKLOAD = 107
    USERPREF_ASSIGN_SHOW_COMPETENCY_IN_GRID = 108
    'Other
End Enum


'
'Get a user preference.  Return the default if the key is not found.
'
Public Function GetUserPreference(user_id As Long, key As UserPreferenceKeys, default As String) As String
    Dim rs As New Recordset, cn As Connection, sql As String

    Set cn = g_dbutil.NewRemoteConnection()
    sql = "SELECT PREFERENCE_VALUE FROM USER_PREFERENCE" & _
          " WHERE USER_ID = " & user_id & _
          " AND   PREFERENCE_KEY = " & g_dbutil.SQL_String(key)
    rs.Open sql, cn, adOpenStatic
    If Not rs.eof Then
        GetUserPreference = rs(0) & ""
    Else
        GetUserPreference = default
    End If
    rs.Close
    cn.Close
End Function

'
'Set a user preference
'
Public Sub SetUserPreference(user_id As Long, key As UserPreferenceKeys, value As String)
    Dim sql As String

    If (user_id = 0) Or Len(key) = 0 Then Exit Sub
    
    Set cn = g_dbutil.NewRemoteConnection()
    sql = "DELETE FROM USER_PREFERENCE" & _
          " WHERE USER_ID = " & user_id & _
          " AND   PREFERENCE_KEY = " & g_dbutil.SQL_String(key)
    On Error Resume Next
    cn.Execute sql
    On Error GoTo 0

    sql = "INSERT INTO USER_PREFERENCE (" & _
          "     USER_ID, PREFERENCE_KEY, PREFERENCE_VALUE" & _
          ") VALUES (" & _
          "     " & user_id & "," & _
                g_dbutil.SQL_String(key) & "," & _
                g_dbutil.SQL_String(value) & _
          ")"
    cn.Execute sql
    cn.Close
End Sub


