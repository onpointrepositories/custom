VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PFSEventLogFilter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'
' Event Log Filter for the Event Log Viewer
' This is to pass values between the viewer and the filter dialog
'
Public Source           As String
Public TypeIDs          As String               '1,2,3
Public Category         As String
Public Description      As String               'search string
Public SourceText       As String
Public TCPPort          As Long
Public UnitID           As Long
Public encounter_id     As Long
Public Today            As Boolean
Public UseStartDate     As Boolean
Public UseFinishDate    As Boolean
Public StartDateTime    As Date
Public FinishDateTime   As Date

