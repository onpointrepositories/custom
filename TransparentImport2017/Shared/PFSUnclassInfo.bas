Attribute VB_Name = "PFSUnclassInfo"
Option Explicit
'
' Unclassified Patients
'
Enum enReportType
    ByDay = 1
    ByShift = 2
    AllShifts = 3
End Enum

Public Type UnclassifiedInfo
    seq                                 As Long
    unit_name                           As String
    unit_id                             As Long
    patient_name                        As String
    acct_number                         As String
    service_episode                     As String
    enc_id                              As Long
    room                                As String
    bed                                 As String
    shift_name                          As String
    start                               As Date
    finish                              As Date
    hours                               As Single
    classified                          As Boolean
    default_class                       As Boolean
    do_not_classify                     As Boolean
End Type

Private Type ClassOrShiftInfo
    enc_id                              As Long
    shift_name                          As String
    shift_seq                           As Integer
    start                               As Date
    finish                              As Date
    default_class                       As Boolean
    do_not_classify                     As Boolean
End Type

Private m_start                         As Date
Private m_finish                        As Date
Private m_unit                          As New UnitDefinition
Private m_report_mode                   As enReportType
Private m_shift_name                    As String
Private m_transfer_out_patients_only    As Boolean
Private m_must_class_every_shift        As Boolean

Private m_sft()                         As ClassOrShiftInfo 'shifts
Private m_loc()                         As UnclassifiedInfo 'locations
Private m_cls()                         As ClassOrShiftInfo 'classifications
Private m_res()                         As UnclassifiedInfo 'result

Private Const DEFAULT_PROCEDURE_DURATION = 1

Private Const SEQ_DELETED = 0           'set .seq to delete a record

'
'This function splits each patient location within given datetime range by shifts
'(if needed), defines if each period was classified or not, calculates hours,
'gets location information.
'
' WARNING: this function returns all patients; you must then examine each .classified flag.
'          (why was this done?)(for the default classification detail report)(not a good reason?)
'
Public Sub UnclassifiedEncounters( _
    unit_id As Long, start As Date, finish As Date, _
    unclassified_info() As UnclassifiedInfo, _
    transfer_out_patients_only As Boolean, _
    by_shift As Boolean, _
    Optional shift_name As String, _
    Optional must_class_every_shift As Boolean _
)
    On Error GoTo errHandler
    
    'setup-------------------------------------------------------------
    m_unit.Load unit_id
    m_start = start
    m_finish = finish
    m_transfer_out_patients_only = transfer_out_patients_only 'discharged and transferred out patients only
    m_shift_name = shift_name
    m_must_class_every_shift = must_class_every_shift
    
    If (by_shift Or must_class_every_shift) Then
        If Len(shift_name) > 0 Then
            m_report_mode = ByShift
        Else
            m_report_mode = AllShifts
        End If
    Else
        m_report_mode = ByDay
    End If
    
    ReDim unclassified_info(0 To 0)
    ReDim m_sft(0 To 0)
    ReDim m_loc(0 To 0)
    ReDim m_cls(0 To 0)
    ReDim m_res(0 To 0)
    
    '---------------------------------------------------------------------------
    'get shifts within report period if needed
    If (m_report_mode <> ByDay) Then GetShifts
    
    'get encounter locations within datetime range and slice them by shift if needed
    GetLocations
    'no locations? exit with empty result array
    If UBound(m_loc) = 0 Then Exit Sub
    
    'Get classifications for all encounter locations within datetime range merging adjoining classifications
    'This includes Perinatal outpatient procedures.
    GetClassInfo
    
    'complete result array, merging locations and classifications
    FillInResultArray

    'merge same bed/room locations with adjoining times
    MergeAdjoiningRecords
    
    RemoveZeroLengthRecords
    
    're-order result array by time, account
    ReOrderResultArray
    
    'copy result array to the final array skipping deleted records
    GetFinalArray unclassified_info
    Exit Sub
    
errHandler:
    g_util.MsgBoxError
End Sub


Private Sub GetShifts()
    Dim day_num     As Integer
    Dim shift_num   As Integer
    Dim shift_seq   As Integer
    Dim cur_date    As Date
    
    On Error GoTo errHandler
    
    For day_num = 1 To DateDiff("d", m_start, m_finish) + 1
        cur_date = g_util.DateOnly(DateAdd("d", day_num - 1, m_start))
        shift_seq = 0
        With m_unit.DayDefinitionForDate(cur_date)
            'Get all primary shifts or just the named shift
            For shift_num = 1 To .ShiftCount
                If (m_report_mode = AllShifts And .Shift(shift_num).IsPrimaryShift) _
                Or (m_report_mode = ByShift And .Shift(shift_num).name = m_shift_name) Then
                    ReDim Preserve m_sft(0 To UBound(m_sft) + 1)
                    shift_seq = shift_seq + 1
                    m_sft(UBound(m_sft)).start = .Shift(shift_num).StartDateTime(cur_date)
                    m_sft(UBound(m_sft)).finish = .Shift(shift_num).FinishDateTime(cur_date)
                    m_sft(UBound(m_sft)).shift_name = .Shift(shift_num).name
                    m_sft(UBound(m_sft)).shift_seq = shift_seq
                End If
            Next shift_num
        End With
    Next day_num
    
'    For day_num = 1 To UBound(m_sft)
'        Debug.Print day_num, m_sft(day_num).shift_name, m_sft(day_num).start, m_sft(day_num).finish
'    Next
    Exit Sub

errHandler:
    g_util.ThrowError "GetShifts"
    Resume  'debug
End Sub
'

Private Sub GetLocations()
    Dim sql             As String
    Dim rs              As New Recordset
    
    On Error GoTo errHandler

    'Get list of EncounterInfo and unit name
    '(We could limit the span of the search (see next function) but we really do want to
    ' find patients that have been here for years.)
    sql = "SELECT EL.ENCOUNTER_ID, EL.DATETIME_IN, EL.DATETIME_OUT, EL.ROOM, EL.BED," & _
         "   ISNULL(P.LAST_NAME,'') + ', ' + ISNULL(P.FIRST_NAME,'') AS PATIENT_NAME," & _
         "   E.ACCT_NUMBER, E.SERVICE_EPISODE_ID," & _
         "   EL.WORKING_UNIT_ID" & _
         " FROM ENCOUNTER_LOCATION AS EL" & _
         " INNER JOIN ENCOUNTER AS E ON (E.ENCOUNTER_ID = EL.ENCOUNTER_ID)" & _
         " INNER JOIN PERSON    AS P ON (P.PERSON_ID = E.PERSON_ID)" & _
         " WHERE EL.WORKING_UNIT_ID = " & m_unit.UnitID & _
         " AND EL.EFFECTIVE_DATETIME_IN < " & g_dbutil.SQL_DateTime(m_finish) & _
         " AND EL.EFFECTIVE_DATETIME_OUT >  " & g_dbutil.SQL_DateTime(m_start)

    If m_transfer_out_patients_only Then
        sql = sql & " AND EL.DATETIME_OUT IS NOT NULL"
    End If
    
    sql = sql & " ORDER BY E.ENCOUNTER_ID, EL.DATETIME_IN"      'order by encounter for merge

    Debug.Print sql
    rs.Open sql, g_cnADO, adOpenStatic, adLockReadOnly
    
    'make an array of encounter locations
    Do While Not rs.eof
        AddLocations rs
        rs.MoveNext
    Loop
        
    CloseRecordset rs
    Exit Sub
    
errHandler:
    g_util.ThrowError "GetLocations"
End Sub

Private Sub AddLocations(rs As Recordset)
    'For report by day locations will be restricted by report period only; their number will be
    'equal to the record count.
    'For report by shift locations will be sliced by shifts so their number may exceed record count.
    Dim sft     As Integer 'shifts
    Dim start As Date
    Dim finish As Date
    
    On Error GoTo errHandler
    
    'restrict location by report period
    start = g_util.Max(rs("DATETIME_IN"), m_start)
    
    If IsDate(rs("DATETIME_OUT")) Then
        finish = g_util.Min(m_finish, rs("DATETIME_OUT"))
    Else
        finish = m_finish
    End If
    
    If (m_report_mode = ByDay) Then
        'add this location to the list as unclassified
        AddRecordToLocationArray rs, start, finish, False
    Else
        'slice this location by shifts and add each slice to the list
        For sft = 1 To UBound(m_sft)
            With m_sft(sft)
                'is this shift overlapping this location?
                If g_util.Min(.finish, finish) > g_util.Max(.start, start) Then
                    'yes - add record to the location array
                    AddRecordToLocationArray rs, g_util.Max(.start, start), _
                        g_util.Min(.finish, finish), False, .shift_name
                Else
                    'no - ignore this shift
                End If
            End With
        Next sft
    End If
    Exit Sub
errHandler:
    g_util.ThrowError "AddLocations"
End Sub

Private Sub AddRecordToLocationArray(rs As Recordset, start As Date, finish As Date, _
                        classified As Boolean, Optional shift_name As String)
    ReDim Preserve m_loc(0 To UBound(m_loc) + 1)
    
    With m_loc(UBound(m_loc))
        .seq = UBound(m_loc)
        .patient_name = g_dbutil.DBToString(rs("PATIENT_NAME"))
        .acct_number = g_dbutil.DBToString(rs("ACCT_NUMBER"))
        .service_episode = g_dbutil.DBToString(rs("SERVICE_EPISODE_ID"))
        .enc_id = g_dbutil.DBToLong(rs("ENCOUNTER_ID"))
        .room = g_dbutil.DBToString(rs("ROOM"))
        .bed = g_dbutil.DBToString(rs("BED"))
        .start = start
        .finish = finish
        If (m_report_mode <> ByDay) Then .shift_name = shift_name
        .classified = classified
        
'        Debug.Print "add loc: enc= "; .enc_id, .start, .finish
'        MaybeBreakHere .enc_id
    End With
End Sub

Private Sub GetClassInfo()
    Dim sql             As String
    Dim rs              As New Recordset
    Dim start_limit     As Date
    Dim finish_limit    As Date
    
    On Error GoTo errHandler
    
    'This query takes much too long unless we restrict the in & out range.
    start_limit = DateAdd("d", -CLASSIFICATION_MAX_LOS_DAYS, m_start)
    finish_limit = DateAdd("d", CLASSIFICATION_MAX_LOS_DAYS, m_finish)
    
    ' CLASSIFICATIONS --------------------------------
    sql = "SELECT C.ENCOUNTER_ID, "
    If (Not m_must_class_every_shift) Then
         sql = sql & _
        " C.EFFECTIVE_DATETIME_IN AS START," & _
        " C.EFFECTIVE_DATETIME_OUT AS FINISH, "
    Else
        'must classify every shift: clip the classification at end of shift
        'The shift ID is the shift in effect at the start of the classification (at least it should be)[6137]
        'Note that the classification may have started during the shift, the FINISH calc can't use the class effective time.
         sql = sql & _
        " C.EFFECTIVE_DATETIME_IN AS START," & _
        " DATEADD(hh, SH.PAID_HOURS + SH.UNPAID_HOURS, CONVERT(CHAR(10), C.CLASSIFICATION_DATE, 110)  + ' ' + CONVERT(CHAR(8), SH.START_TIME, 108)) AS FINISH, "
    End If
    sql = sql & _
         " C.CLASSIFIED_BY_ID, C.REPORT_THIS_WORKLOAD, C.METHODOLOGY_ID" & _
         " FROM CLASSIFICATION_EVENT AS C"
    If (m_must_class_every_shift) Then
         sql = sql & _
         " INNER JOIN SHIFT_DEFINITION SH ON SH.SHIFT_DEFINITION_ID = C.SHIFT_DEFINITION_ID "
    End If
    sql = sql & _
         " WHERE C.UNIT_ID IN (SELECT UNIT_ID FROM UNIT WHERE UNIT_ID=" & m_unit.UnitID & " OR PARENT_UNIT_ID=" & m_unit.UnitID & ")" & _
         " AND C.EFFECTIVE_DATETIME_IN  >=" & g_dbutil.SQL_DateTime(start_limit) & _
         " AND C.EFFECTIVE_DATETIME_IN  < " & g_dbutil.SQL_DateTime(m_finish) & _
         " AND C.EFFECTIVE_DATETIME_OUT > " & g_dbutil.SQL_DateTime(m_start) & _
         " AND C.EFFECTIVE_DATETIME_OUT <=" & g_dbutil.SQL_DateTime(finish_limit) & _
         " AND C.IS_DELETED<>'Y'"
    'Do not filter by shift -- the shift above is the start shift -- the class could span shifts
    
    sql = sql & " UNION "
    
    ' PROCEDURES (Perinatal Outpatient only) ------------------------------------
    ' Only Perinatal allows a patient procedure without a classification, but it must
    ' be an outpatient procedure (stage 0).
    sql = sql & " SELECT P.ENCOUNTER_ID,"
    If (Not m_must_class_every_shift) Then
         sql = sql & _
         " P.EFFECTIVE_DATETIME_IN AS START, " & _
         " P.EFFECTIVE_DATETIME_OUT AS FINISH, "
    Else
         sql = sql & _
        " P.PROCEDURE_DATETIME AS START, " & _
        " DATEADD(hh, SH.PAID_HOURS + SH.UNPAID_HOURS, CONVERT(CHAR(10), P.PROCEDURE_DATE, 110)  + ' ' + CONVERT(CHAR(8), SH.START_TIME, 108)) AS FINISH, "
    End If
    sql = sql & _
         " P.CLASSIFIED_BY_ID, P.REPORT_THIS_WORKLOAD, P.METHODOLOGY_ID" & _
         " FROM PROCEDURE_EVENT AS P"
    If (m_must_class_every_shift) Then
         sql = sql & _
         " INNER JOIN SHIFT_DEFINITION AS SH ON (SH.SHIFT_DEFINITION_ID = P.SHIFT_DEFINITION_ID)"
    End If
    sql = sql & " INNER JOIN METHODOLOGY AS M ON (M.METHODOLOGY_ID = P.METHODOLOGY_ID)"
    sql = sql & _
         " WHERE P.UNIT_ID IN (SELECT UNIT_ID FROM UNIT WHERE UNIT_ID=" & m_unit.UnitID & " OR PARENT_UNIT_ID=" & m_unit.UnitID & ")" & _
         " AND P.EFFECTIVE_DATETIME_IN  >=" & g_dbutil.SQL_DateTime(start_limit) & _
         " AND P.EFFECTIVE_DATETIME_IN  < " & g_dbutil.SQL_DateTime(m_finish) & _
         " AND P.EFFECTIVE_DATETIME_OUT > " & g_dbutil.SQL_DateTime(m_start) & _
         " AND P.EFFECTIVE_DATETIME_OUT <=" & g_dbutil.SQL_DateTime(finish_limit) & _
         " AND (M.METHODOLOGY_GROUP_LVC='P')" & _
         " AND (P.STAGE=0)" & _
         " AND P.IS_DELETED<>'Y'"

    'sql = sql & " ORDER BY START"
    sql = sql & " ORDER BY ENCOUNTER_ID, START"                     'order by encounter for merge

    Debug.Print sql
    rs.Open sql, g_cnADO, adOpenStatic, adLockReadOnly
    'add records to the classification array
    Do While Not rs.eof
        If ValidEncounterID(g_dbutil.DBToLong(rs("ENCOUNTER_ID"))) Then
            AddRecordToClassArray rs
        End If
        rs.MoveNext
    Loop
    CloseRecordset rs
    Exit Sub

errHandler:
    g_util.ThrowError "GetClassInfo"
End Sub

Private Function ValidEncounterID(enc_id As Long) As Boolean
    Dim i As Long
    
    For i = 1 To UBound(m_loc)
        If m_loc(i).enc_id = enc_id Then
            ValidEncounterID = True
            Exit Function
        End If
    Next i
End Function

Private Sub AddRecordToClassArray(rs As Recordset)
    ReDim Preserve m_cls(0 To UBound(m_cls) + 1)
    With m_cls(UBound(m_cls))
        .enc_id = g_dbutil.DBToLong(rs("ENCOUNTER_ID"))
        .start = rs("START")
        .finish = rs("FINISH")
        .default_class = (g_dbutil.DBToLong(rs("CLASSIFIED_BY_ID")) = USER_ID_DEFAULT)
        .do_not_classify = (g_dbutil.DBToLong(rs("METHODOLOGY_ID")) = METH_ID_DO_NOT_CLASSIFY)
        
'        Debug.Print "add class: enc= "; .enc_id, .start, .finish
'        MaybeBreakHere .enc_id
    End With
End Sub

Private Sub FillInResultArray()
    Dim loc         As Long 'location number
    Dim cls         As Long 'classification number
    Dim init_rec    As Long
    
    On Error GoTo errHandler
    For loc = 1 To UBound(m_loc)
        With m_loc(loc)
            MaybeBreakHere .enc_id
            'initially suppose this location is totally unclassified
            'if no classification then whole location will remain unclassified
            AddRecordToResultArray loc, .start, .finish, False, False, False
            'keep init record number
            init_rec = UBound(m_res)
            
            'Look for classifications that overlap this location
            For cls = 1 To UBound(m_cls)
                If m_cls(cls).enc_id = .enc_id Then ' identify encounter
                    MaybeBreakHere .enc_id
                    If (m_cls(cls).start < .finish) And (m_cls(cls).finish > .start) Then
                        'classification overlaps location
                        If m_cls(cls).start <= .start Then
                            'class starts earlier than location does
                            If m_cls(cls).finish < .finish Then
                                'class finishes earlier than location does;
                                'split location - add left part as classified segment,...
                                AddRecordToResultArray loc, .start, m_cls(cls).finish, True, m_cls(cls).default_class, m_cls(cls).do_not_classify
                                '...correct right part
                                m_res(init_rec).start = m_cls(cls).finish
                                '...and cut off rest part of this location.
                                m_loc(loc).start = m_cls(cls).finish
                            Else
                                'class completely overlaps location;
                                'mark it as classified...
                                m_res(init_rec).classified = True
                                m_res(init_rec).default_class = m_cls(cls).default_class
                                m_res(init_rec).do_not_classify = m_cls(cls).do_not_classify
                                '...and go to the next location
                                Exit For
                            End If
                        Else
                            'class starts later than location does
                            If m_cls(cls).finish < .finish Then
                                'class finishes earlier than location does;
                                'now we have classified period in the middle of location so we have to:
                                '...add new unclassified location up to class start,...
                                AddRecordToResultArray loc, .start, m_cls(cls).start, False, False, False
                                '...add new classified location in middle,...
                                AddRecordToResultArray loc, m_cls(cls).start, m_cls(cls).finish, True, m_cls(cls).default_class, m_cls(cls).do_not_classify
                                '...correct right part,...
                                m_res(init_rec).start = m_cls(cls).finish
                                '...and cut off rest part of this location.
                                .start = m_cls(cls).finish
                            Else
                                'class finishes later than location does;
                                'split location - correct left (unclassified) part...
                                m_res(init_rec).finish = m_cls(cls).start
                                '...and add right part as new classified record.
                                AddRecordToResultArray loc, m_cls(cls).start, .finish, True, m_cls(cls).default_class, m_cls(cls).do_not_classify
                                'once this class finishes later then this location then
                                'go to the next location
                                Exit For
                            End If
                        End If
                    End If
                End If
            Next cls
            
        End With
    Next loc
    
'    Debug.Print "result array"
'    For loc = 1 To UBound(m_res)
'        With m_res(loc)
'            Debug.Print .enc_id, g_util.FixedWidth(.patient_name, 24), .shift_name, .start, .finish, "C? " & .classified, .default_class
'        End With
'    Next loc
    Exit Sub

errHandler:
    g_util.ThrowError "FillInResultArray"
    Resume   'debug
End Sub

Private Sub AddRecordToResultArray(loc As Long, _
                                   start As Date, _
                                   finish As Date, _
                                   classified As Boolean, _
                                   default_class As Boolean, _
                                   do_not_classify As Boolean _
                                   )
    ReDim Preserve m_res(0 To UBound(m_res) + 1)
    With m_res(UBound(m_res))
        .seq = UBound(m_res)
        .patient_name = m_loc(loc).patient_name
        .acct_number = m_loc(loc).acct_number
        .service_episode = m_loc(loc).service_episode
        .enc_id = m_loc(loc).enc_id
        .room = m_loc(loc).room
        .bed = m_loc(loc).bed
        .shift_name = m_loc(loc).shift_name
        .start = start
        .finish = finish
        .hours = (DateDiff("s", .start, .finish) / 3600)
        .classified = classified
        .default_class = default_class
        .do_not_classify = do_not_classify
        
'        Debug.Print "add result: enc= "; .enc_id, .start, .finish
'        MaybeBreakHere .enc_id
    End With
End Sub

Private Sub CloseRecordset(rs As Recordset)
    rs.Close
    Set rs = Nothing
End Sub

Private Sub MergeAdjoiningRecords()
    Dim i As Long
    
    On Error GoTo errHandler
    
    For i = 2 To UBound(m_res)
        'Merge all locations in "day" mode plus (when not in day mode) the same shift
        If (m_report_mode = ByDay) _
            Or (m_res(i).shift_name = m_res(i - 1).shift_name) _
        Then
            'Note: merge transfers within the unit (don't look at room/bed)
            If m_res(i).enc_id = m_res(i - 1).enc_id _
                And m_res(i).unit_id = m_res(i - 1).unit_id _
                And m_res(i).start = m_res(i - 1).finish _
                And m_res(i).classified = m_res(i - 1).classified _
                And m_res(i).default_class = m_res(i - 1).default_class _
                And m_res(i).do_not_classify = m_res(i - 1).do_not_classify _
            Then
                'Use previous record start
                m_res(i).start = m_res(i - 1).start
                ' kill previous record
                m_res(i - 1).seq = SEQ_DELETED
            End If
        End If
    Next i
'    For i = 1 To UBound(m_res)
'        Debug.Print m_res(i).enc_id, g_util.FixedWidth(m_res(i).patient_name, 24), m_res(i).start, m_res(i).classified, m_res(i).default_class, m_res(i).do_not_classify, m_res(i).seq
'    Next i
    Exit Sub

errHandler:
    g_util.ThrowError "MergeAdjoiningRecords"
End Sub

Private Sub RemoveZeroLengthRecords()
    Dim i As Long
    
    On Error GoTo errHandler
    
    For i = 1 To UBound(m_res)
        If (m_res(i).start = m_res(i).finish) Then
            m_res(i).seq = SEQ_DELETED
        End If
    Next i
    Exit Sub

errHandler:
    g_util.ThrowError "RemoveZeroLengthRecords"
End Sub


Private Sub ReOrderResultArray()
    'reorder just by date/time
    Dim i   As Long
    Dim j   As Long
    Dim t   As UnclassifiedInfo
    Dim keep_going As Boolean
    
    On Error GoTo errHandler
    
DoItAgain:
    keep_going = False
    
    For i = 1 To UBound(m_res) - 1
        j = i + 1
        If MoveRecordDown(i, j) Then
            t = m_res(i)
            m_res(i) = m_res(j)
            m_res(j) = t
            keep_going = True
        End If
    Next i
    
    If keep_going Then GoTo DoItAgain
    
    Exit Sub

errHandler:
    g_util.ThrowError "ReOrderResultArray"
End Sub

Private Function MoveRecordDown(i As Long, j As Long) As Boolean
    'check time, acct
    If (m_res(j).start < m_res(i).start) Then
        MoveRecordDown = True
    ElseIf (m_res(j).start = m_res(i).start) Then
        If (m_res(j).acct_number < m_res(i).acct_number) Then
            MoveRecordDown = True
        End If
    End If
End Function


Private Sub GetFinalArray(unclassified_info() As UnclassifiedInfo)
    Dim i As Long
    
    On Error GoTo errHandler
    
    For i = 1 To UBound(m_res)
        If (m_res(i).seq <> SEQ_DELETED) Then
            ReDim Preserve unclassified_info(0 To UBound(unclassified_info) + 1)
            
            'copy record
            unclassified_info(UBound(unclassified_info)) = m_res(i)
            
            With unclassified_info(UBound(unclassified_info))
                .seq = UBound(unclassified_info)
                .hours = (DateDiff("s", .start, .finish) / 3600)
                .unit_id = m_unit.UnitID
                .unit_name = m_unit.name
            End With
        End If
    Next i
    
'    For i = 1 To UBound(unclassified_info)
'        With unclassified_info(i)
'            Debug.Print .unit_name, .shift_name, .patient_name, .default_class, .do_not_classify, .start, .finish
'        End With
'    Next i
    Exit Sub

errHandler:
    g_util.ThrowError "GetFinalArray"
End Sub


Private Sub MaybeBreakHere(enc_id As Long)
    Dim i As Integer
    If (enc_id = 7130793) Then
        i = 1                    'break here
    End If
End Sub
