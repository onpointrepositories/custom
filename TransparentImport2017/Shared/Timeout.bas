Attribute VB_Name = "Timeout"
Option Explicit
'
'Timeout module: The mouse and keyboard hook callback procedures live here.
'These procedures must be in a .BAS module otherwise we would put them
'in the timeout class.
'
'See the PFSTimeout class for important information.
'This relies on the main module to declare g_timeout.

Private Declare Function SetWindowsHookEx Lib "user32" Alias "SetWindowsHookExA" (ByVal idHook As Long, ByVal lpfn As Long, ByVal hmod As Long, ByVal dwThreadId As Long) As Long
Private Declare Function UnhookWindowsHookEx Lib "user32" (ByVal hHook As Long) As Long
Private Declare Function CallNextHookEx Lib "user32" (ByVal hHook As Long, ByVal ncode As Long, ByVal wParam As Long, lParam As Any) As Long

'Hook constants
Private Const WH_KEYBOARD = 2
Private Const WH_MOUSE = 7

Private Const HC_ACTION = 0
Private Const HC_NOREMOVE = 3

'Message constants
Private Const WM_KEYDOWN = &H100
Private Const WM_MOUSEMOVE = &H200

Private Const WHEEL_DELTA = 120

Private Type POINTAPI
        X               As Long
        Y               As Long
End Type

Private Type MOUSEHOOKSTRUCT
        pt              As POINTAPI
        hWnd            As Long
        wHitTestCode    As Long
        dwExtraInfo     As Long
End Type


'The PrevHook values will be zero if there was no previous hook so we need an
'additional flag to remember if we have installed these hooks or not.
Private lpPrevMouseHook     As Long
Private lpPrevKeyboardHook  As Long
Private MouseIsHooked       As Boolean
Private KeyboardIsHooked    As Boolean


Public Sub SetMouseHook()
    If MouseIsHooked Then
        'Hook only once
    Else
        lpPrevMouseHook = SetWindowsHookEx(WH_MOUSE, AddressOf MouseHookProc, 0, App.ThreadID)
        MouseIsHooked = True
    End If
End Sub

Public Sub RemoveMouseHook()
    Dim result As Long
    If MouseIsHooked Then
        result = UnhookWindowsHookEx(lpPrevMouseHook)
        lpPrevMouseHook = 0
        MouseIsHooked = False
    Else
        'Ignore extra attempts to unhook the mouse
    End If
End Sub

Private Function MouseHookProc(ByVal uCode As Long, ByVal wParam As Long, lParam As MOUSEHOOKSTRUCT) As Long
    'Do not throw any errors at Windows
    On Error Resume Next
    
    If uCode = HC_ACTION Then
        'Restrict ourselves to MouseMouse for less overhead
        If wParam = WM_MOUSEMOVE Then
            g_timeout.MouseMove
        End If
    End If
    
    MouseHookProc = CallNextHookEx(lpPrevMouseHook, uCode, wParam, lParam)
End Function


Public Sub SetKeyboardHook()
    If KeyboardIsHooked Then
        'Hook only once
    Else
        lpPrevKeyboardHook = SetWindowsHookEx(WH_KEYBOARD, AddressOf KeyboardHookProc, 0, App.ThreadID)
        KeyboardIsHooked = True
    End If
End Sub

Public Sub RemoveKeyboardHook()
    Dim result As Long
    If KeyboardIsHooked Then
        result = UnhookWindowsHookEx(lpPrevKeyboardHook)
        lpPrevKeyboardHook = 0
        KeyboardIsHooked = False
    Else
        'Ignore extra attempts to unhook the Keyboard
    End If
End Sub

Private Function KeyboardHookProc(ByVal uCode As Long, ByVal wParam As Long, ByVal lParam As Long) As Long
    'Do not throw any errors at Windows
    On Error Resume Next
    
    If uCode = HC_ACTION Then
        'Restrict ourselves to KeyDown for less overhead
        'Note: wParam does not contain the message code WM_KEYDOWN
        '      They "key direction" flag is in the high-order bit of lParam.
        If (lParam And &H80000000) = &H0 Then
            g_timeout.KeyDown
        End If
    End If

    KeyboardHookProc = CallNextHookEx(lpPrevKeyboardHook, uCode, wParam, lParam)
End Function

