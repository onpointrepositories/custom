VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmChartItemFilter 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Chart Item Filter"
   ClientHeight    =   6030
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6870
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6030
   ScaleWidth      =   6870
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   375
      Left            =   5520
      TabIndex        =   33
      Top             =   5520
      Width           =   1215
   End
   Begin VB.CheckBox chkCount 
      Caption         =   "Count(*)"
      Height          =   315
      Left            =   120
      TabIndex        =   32
      Top             =   300
      Width           =   2055
   End
   Begin VB.CommandButton cmdLookupPatient 
      Caption         =   "Lookup"
      Height          =   315
      Left            =   4680
      TabIndex        =   31
      Top             =   1020
      Width           =   975
   End
   Begin VB.CommandButton cmdClearPatient 
      Caption         =   "Clear"
      Height          =   315
      Left            =   5760
      TabIndex        =   30
      Top             =   1020
      Width           =   975
   End
   Begin VB.CommandButton cmdReset 
      Caption         =   "Reset"
      Height          =   375
      Left            =   120
      TabIndex        =   27
      Top             =   5520
      Width           =   1245
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   375
      Left            =   4200
      TabIndex        =   26
      Top             =   5520
      Width           =   1215
   End
   Begin VB.CommandButton cmdApply 
      Caption         =   "Apply"
      Height          =   375
      Left            =   2880
      TabIndex        =   25
      Top             =   5520
      Width           =   1215
   End
   Begin VB.TextBox txtOrderControl 
      Height          =   315
      Left            =   2280
      TabIndex        =   23
      Top             =   4620
      Width           =   2295
   End
   Begin VB.TextBox txtOrderID 
      Height          =   315
      Left            =   2280
      TabIndex        =   22
      Top             =   4980
      Width           =   2295
   End
   Begin VB.CheckBox chkOrderControl 
      Caption         =   "Order Control"
      Height          =   315
      Left            =   120
      TabIndex        =   21
      Top             =   4620
      Width           =   2055
   End
   Begin VB.CheckBox chkCategory 
      Caption         =   "Category"
      Height          =   315
      Left            =   120
      TabIndex        =   18
      Top             =   2820
      Width           =   2055
   End
   Begin VB.CheckBox chkUnit 
      Caption         =   "Unit"
      Height          =   315
      Left            =   120
      TabIndex        =   17
      Top             =   660
      Width           =   2055
   End
   Begin VB.CheckBox chkAcct 
      Caption         =   "Account Number"
      Height          =   315
      Left            =   120
      TabIndex        =   16
      Top             =   1020
      Width           =   2055
   End
   Begin VB.CheckBox chkEventDatetime 
      Caption         =   "Event Date/Time"
      Height          =   315
      Left            =   120
      TabIndex        =   15
      Top             =   2100
      Width           =   2055
   End
   Begin VB.TextBox txtPullTime 
      Height          =   315
      Left            =   4080
      TabIndex        =   13
      Top             =   1380
      Width           =   1455
   End
   Begin VB.TextBox txtRange 
      Height          =   315
      Left            =   2280
      TabIndex        =   12
      Top             =   1740
      Width           =   1695
   End
   Begin VB.CheckBox chkDesc 
      Caption         =   "Description"
      Height          =   315
      Left            =   120
      TabIndex        =   11
      Top             =   3540
      Width           =   2055
   End
   Begin VB.CheckBox chkCode 
      Caption         =   "Code"
      Height          =   315
      Left            =   120
      TabIndex        =   10
      Top             =   3180
      Width           =   2055
   End
   Begin VB.CheckBox chkField 
      Caption         =   "Field Name"
      Height          =   315
      Left            =   120
      TabIndex        =   9
      Top             =   3900
      Width           =   2055
   End
   Begin VB.CheckBox chkResult 
      Caption         =   "Result"
      Height          =   315
      Left            =   120
      TabIndex        =   8
      Top             =   4260
      Width           =   2055
   End
   Begin VB.TextBox txtAcct 
      Height          =   315
      Left            =   2280
      TabIndex        =   7
      Top             =   1020
      Width           =   2295
   End
   Begin VB.TextBox txtCategory 
      Height          =   315
      Left            =   2280
      TabIndex        =   6
      Top             =   2820
      Width           =   2295
   End
   Begin VB.TextBox txtCode 
      Height          =   315
      Left            =   2280
      TabIndex        =   5
      Top             =   3180
      Width           =   2295
   End
   Begin VB.TextBox txtDescription 
      Height          =   315
      Left            =   2280
      TabIndex        =   4
      Top             =   3540
      Width           =   2295
   End
   Begin VB.TextBox txtField 
      Height          =   315
      Left            =   2280
      TabIndex        =   3
      Top             =   3900
      Width           =   2295
   End
   Begin VB.TextBox txtResult 
      Height          =   315
      Left            =   2280
      TabIndex        =   2
      Top             =   4260
      Width           =   2295
   End
   Begin VB.ComboBox cboUnit 
      Height          =   315
      Left            =   2280
      Style           =   2  'Dropdown List
      TabIndex        =   1
      Top             =   660
      Width           =   2295
   End
   Begin VB.CheckBox chkTimestamp 
      Caption         =   "Timestamp"
      Height          =   315
      Left            =   120
      TabIndex        =   0
      Top             =   2460
      Width           =   2055
   End
   Begin MSComCtl2.DTPicker dtpPullDate 
      Height          =   315
      Left            =   2280
      TabIndex        =   14
      Top             =   1380
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   556
      _Version        =   393216
      CheckBox        =   -1  'True
      Format          =   145752065
      CurrentDate     =   40400
   End
   Begin VB.Label Label1 
      Caption         =   "(zero = max)"
      Height          =   255
      Left            =   4080
      TabIndex        =   34
      Top             =   1800
      Width           =   1935
   End
   Begin VB.Label lblDisplay 
      Caption         =   "Display"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   120
      TabIndex        =   29
      Top             =   0
      Width           =   2055
   End
   Begin VB.Label lblFilter 
      Caption         =   "Filter"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   315
      Left            =   2280
      TabIndex        =   28
      Top             =   0
      Width           =   2295
   End
   Begin VB.Label Label3 
      Caption         =   "Order ID"
      Height          =   315
      Left            =   360
      TabIndex        =   24
      Top             =   5040
      Width           =   1815
   End
   Begin VB.Label lblPullTime 
      Alignment       =   1  'Right Justify
      Caption         =   "Pull Time"
      Height          =   255
      Left            =   360
      TabIndex        =   20
      Top             =   1440
      Width           =   1815
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      Caption         =   "Range (mins)"
      Height          =   255
      Left            =   360
      TabIndex        =   19
      Top             =   1800
      Width           =   1815
   End
End
Attribute VB_Name = "frmChartItemFilter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'
' Chart Item Filter
'
Private m_parent As frmChartItemViewer
Private m_filter As PFSChartItemFilter

Private cdlUnit         As New PFSCodeDescList
Private m_any_changes   As Boolean


Public Sub Setup(parent As frmChartItemViewer, filter As PFSChartItemFilter, lock_patient As Boolean)
    'save pointers to the parent form
    Set m_parent = parent
    Set m_filter = filter

    SetFormFromFilter
    
    If lock_patient Then
        'The chart item viewer is showing a patient classification
        'We can seach within these items but we are not allowed to change
        'the unit, patient or range.
        cboUnit.Enabled = False
        cmdLookupPatient.Enabled = False
        cmdClearPatient.Enabled = False
        
        'Even in "locked" mode you can pick "(all)" dates.  Enable date range [37111]
'        dtpPullDate.Enabled = False
'        txtPullTime.Enabled = False
'        txtRange.Enabled = False
        
        cmdReset.Enabled = False
    End If
End Sub


Private Sub Form_Load()
    On Error GoTo errHandler
    Dim i As Integer
    
    Me.caption = App.ProductName & " Chart Item Filter"

    g_display.SetStdTheme Me, True
    lblDisplay.FontBold = True
    lblFilter.FontBold = True

    cdlUnit.LoadSQL SQL_ALL_UNITS, cdlInsertAllChoice
    
    cdlUnit.FillComboBox cboUnit
    
    txtAcct.Enabled = False                     'disable for manual entry
    Exit Sub
errHandler:
    g_util.MsgBoxError
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set m_parent = Nothing
End Sub

'Look for accelerator events.  Form.KeyPreview must be True.
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If (KeyCode = vbKeyF1) Then
        g_help.ShowHelp hWnd, HELP_CHART_VIEWER
        KeyCode = 0
        Shift = 0
    End If
End Sub

Private Sub cmdLookupPatient_Click()
    Dim enc_id As Long

    enc_id = frmLookupPatient.LookupPatientEncounterID(Me)
    
    If (enc_id <> m_filter.EncounterID) Then
        SetEncounter enc_id
    End If
End Sub

Private Sub cmdClearPatient_Click()
    SetEncounter 0
End Sub

Private Sub txtPullTime_LostFocus()
    txtPullTime.Text = Format$(g_util.ParseTime(txtPullTime.Text), PFS_TIME_FORMAT)
End Sub

Private Sub cmdOK_Click()
    If ApplyChanges Then
        Unload Me
    End If
End Sub

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdApply_Click()
    ApplyChanges
End Sub

Private Sub cmdReset_Click()
    m_parent.ResetFilter m_filter
    SetFormFromFilter
End Sub

Private Function ApplyChanges() As Boolean
    If Validate() Then
        SetFilterFromForm
        m_parent.ReloadGrid
        ApplyChanges = True
    End If
End Function

Private Sub SetEncounter(enc_id As Long)
    Dim enc As New encounter
    
    m_filter.EncounterID = enc_id

    If enc_id <> 0 Then
        enc.Load enc_id
        txtAcct.Text = enc.AcctNumber
    Else
        txtAcct.Text = ""
    End If
End Sub

Private Sub SetFormFromFilter()
    On Error GoTo errHandler
    Dim i As Integer, n As Integer
    Dim arr() As String
    
    g_util.SetCheckbox chkCount, m_filter.DisplayCount
    g_util.SetCheckbox chkUnit, m_filter.DisplayUnit
    g_util.SetCheckbox chkAcct, m_filter.DisplayAccount
    g_util.SetCheckbox chkEventDatetime, m_filter.DisplayEventTime
    g_util.SetCheckbox chkTimestamp, m_filter.DisplayTimestamp
    g_util.SetCheckbox chkCategory, m_filter.DisplayCategory
    g_util.SetCheckbox chkCode, m_filter.DisplayCode
    g_util.SetCheckbox chkDesc, m_filter.DisplayDescription
    g_util.SetCheckbox chkField, m_filter.DisplayFieldName
    g_util.SetCheckbox chkResult, m_filter.DisplayResult
    g_util.SetCheckbox chkOrderControl, m_filter.DisplayOrderControl
    
    cdlUnit.SelectComboBoxItem cboUnit, g_dbutil.ZeroToNull(m_filter.UnitID) & ""
    SetEncounter m_filter.EncounterID
    dtpPullDate.value = Date                    'if PullDatetime is null this will be the gray text
    dtpPullDate.value = m_filter.PullDatetime
    txtPullTime.Text = FormatTimeboxValue(m_filter.PullDatetime)
    txtRange.Text = m_filter.RangeMins
    txtCategory.Text = m_filter.Category
    txtCode.Text = m_filter.Code
    txtDescription.Text = m_filter.Description
    txtField.Text = m_filter.FieldName
    txtResult.Text = m_filter.result
    txtOrderControl.Text = m_filter.OrderControl
    txtOrderID.Text = m_filter.OrderID
    Exit Sub
    
errHandler:
    g_util.MsgBoxError "SetFormFromFilter"
    Exit Sub
    Resume  'debug
End Sub

Private Sub SetFilterFromForm()
    On Error GoTo errHandler
    
    m_filter.DisplayCount = g_util.IsChecked(chkCount)
    m_filter.DisplayUnit = g_util.IsChecked(chkUnit)
    m_filter.DisplayAccount = g_util.IsChecked(chkAcct)
    m_filter.DisplayEventTime = g_util.IsChecked(chkEventDatetime)
    m_filter.DisplayTimestamp = g_util.IsChecked(chkTimestamp)
    m_filter.DisplayCategory = g_util.IsChecked(chkCategory)
    m_filter.DisplayCode = g_util.IsChecked(chkCode)
    m_filter.DisplayDescription = g_util.IsChecked(chkDesc)
    m_filter.DisplayFieldName = g_util.IsChecked(chkField)
    m_filter.DisplayResult = g_util.IsChecked(chkResult)
    m_filter.DisplayOrderControl = g_util.IsChecked(chkOrderControl)
    
    m_filter.UnitID = g_dbutil.DBToLong(cdlUnit.Code(cboUnit.Text))
    m_filter.EncounterID = m_filter.EncounterID     'already set
    m_filter.PullDatetime = g_util.CombineDateTimeString(dtpPullDate.value, txtPullTime.Text)
    m_filter.RangeMins = g_dbutil.DBToLong(txtRange.Text)
    m_filter.Category = txtCategory.Text
    m_filter.Code = txtCode.Text
    m_filter.Description = txtDescription.Text
    m_filter.FieldName = txtField.Text
    m_filter.result = txtResult.Text
    m_filter.OrderControl = txtOrderControl.Text
    m_filter.OrderID = txtOrderID.Text
    Exit Sub
    
errHandler:
    g_util.MsgBoxError "SetFilterFromForm"
    Exit Sub
    Resume  'debug
End Sub

Private Function FormatTimeboxValue(v_dt As Variant) As String
    Dim dt As Date
    
    If IsDate(v_dt) Then
        dt = v_dt
    Else
        dt = Now
    End If
    
    FormatTimeboxValue = Format$(dt, PFS_TIME_FORMAT)
End Function

Private Function Validate() As Boolean
    If _
        g_util.IsChecked(chkCount) Or _
        g_util.IsChecked(chkUnit) Or _
        g_util.IsChecked(chkAcct) Or _
        g_util.IsChecked(chkEventDatetime) Or _
        g_util.IsChecked(chkTimestamp) Or _
        g_util.IsChecked(chkCategory) Or _
        g_util.IsChecked(chkCode) Or _
        g_util.IsChecked(chkDesc) Or _
        g_util.IsChecked(chkField) Or _
        g_util.IsChecked(chkResult) Or _
        g_util.IsChecked(chkOrderControl) _
    Then
        'OK
    Else
        MsgBox "Please select a column to display", vbCritical + vbOKOnly, Me.caption
        Exit Function
    End If

    'more tests?
    
    Validate = True
End Function
