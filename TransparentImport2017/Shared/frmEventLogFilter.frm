VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmEventLogFilter 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Events Filter"
   ClientHeight    =   7245
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7095
   Icon            =   "frmEventLogFilter.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   7245
   ScaleWidth      =   7095
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame Frame2 
      Caption         =   "Notes"
      Height          =   1035
      Left            =   120
      TabIndex        =   34
      Top             =   5520
      Width           =   6855
      Begin VB.Label Label10 
         Caption         =   "1. Search here for results like unknown, missing and invalid."
         Height          =   255
         Left            =   120
         TabIndex        =   36
         Top             =   300
         Width           =   6615
      End
      Begin VB.Label Label11 
         Caption         =   "2. Search here for the patient name, account number, unit or chart keywords"
         Height          =   255
         Left            =   120
         TabIndex        =   35
         Top             =   600
         Width           =   6615
      End
   End
   Begin VB.CheckBox chkToday 
      Caption         =   "Today"
      Height          =   315
      Left            =   120
      TabIndex        =   33
      Top             =   3900
      Width           =   2295
   End
   Begin VB.CommandButton cmdClearPatient 
      Caption         =   "Clear"
      Height          =   315
      Left            =   6000
      TabIndex        =   32
      Top             =   3000
      Width           =   975
   End
   Begin VB.TextBox txtPatientName 
      Enabled         =   0   'False
      Height          =   315
      Left            =   2400
      TabIndex        =   29
      Text            =   "Text1"
      Top             =   3000
      Width           =   2415
   End
   Begin VB.CommandButton cmdLookupPatient 
      Caption         =   "Lookup"
      Height          =   315
      Left            =   4920
      TabIndex        =   28
      Top             =   3000
      Width           =   975
   End
   Begin VB.CommandButton cmdApply 
      Caption         =   "Apply"
      Height          =   375
      Left            =   3120
      TabIndex        =   26
      Top             =   6720
      Width           =   1215
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   375
      Left            =   5790
      TabIndex        =   25
      Top             =   6720
      Width           =   1215
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "OK"
      Default         =   -1  'True
      Height          =   375
      Left            =   4470
      TabIndex        =   24
      Top             =   6720
      Width           =   1215
   End
   Begin VB.CommandButton cmdRestoreDefaults 
      Caption         =   "Restore Defaults"
      Height          =   375
      Left            =   120
      TabIndex        =   23
      Top             =   6720
      Width           =   1935
   End
   Begin VB.ComboBox cboFinishMode 
      Height          =   315
      Left            =   2400
      TabIndex        =   21
      Text            =   "Combo1"
      Top             =   4680
      Width           =   1455
   End
   Begin MSComCtl2.DTPicker dtpStartDate 
      Height          =   315
      Left            =   3960
      TabIndex        =   20
      Top             =   4260
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   556
      _Version        =   393216
      Format          =   144703489
      CurrentDate     =   39955
   End
   Begin VB.ComboBox cboStartMode 
      Height          =   315
      Left            =   2400
      TabIndex        =   17
      Text            =   "Combo1"
      Top             =   4260
      Width           =   1455
   End
   Begin VB.TextBox txtTCPPort 
      Height          =   315
      Left            =   2400
      TabIndex        =   16
      Text            =   "Text1"
      Top             =   5100
      Width           =   4575
   End
   Begin VB.TextBox txtSourceText 
      Height          =   315
      Left            =   2400
      TabIndex        =   14
      Text            =   "Text1"
      Top             =   2580
      Width           =   4575
   End
   Begin VB.TextBox txtDescription 
      Height          =   315
      Left            =   2400
      TabIndex        =   13
      Text            =   "Text1"
      Top             =   2160
      Width           =   4575
   End
   Begin VB.ComboBox cboUnit 
      Height          =   315
      Left            =   2400
      TabIndex        =   12
      Text            =   "Combo1"
      Top             =   3420
      Width           =   4455
   End
   Begin VB.ComboBox cboCategory 
      Height          =   315
      Left            =   2400
      TabIndex        =   8
      Text            =   "Combo1"
      Top             =   1740
      Width           =   4575
   End
   Begin VB.ComboBox cboSource 
      Height          =   315
      Left            =   2400
      TabIndex        =   6
      Text            =   "Combo1"
      Top             =   60
      Width           =   4575
   End
   Begin VB.Frame Frame1 
      Caption         =   "Event Types"
      Height          =   1095
      Left            =   120
      TabIndex        =   0
      Top             =   540
      Width           =   6855
      Begin VB.CheckBox chkEventType 
         Caption         =   "Check1"
         Height          =   255
         Index           =   3
         Left            =   2520
         TabIndex        =   4
         Top             =   660
         Width           =   2295
      End
      Begin VB.CheckBox chkEventType 
         Caption         =   "Check1"
         Height          =   255
         Index           =   2
         Left            =   2520
         TabIndex        =   3
         Top             =   300
         Width           =   2295
      End
      Begin VB.CheckBox chkEventType 
         Caption         =   "Check1"
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   2
         Top             =   660
         Width           =   2295
      End
      Begin VB.CheckBox chkEventType 
         Caption         =   "Check1"
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   1
         Top             =   300
         Width           =   2295
      End
   End
   Begin MSComCtl2.DTPicker dtpFinishDate 
      Height          =   315
      Left            =   3960
      TabIndex        =   22
      Top             =   4680
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   556
      _Version        =   393216
      Format          =   144703489
      CurrentDate     =   39955
   End
   Begin MSComCtl2.DTPicker dtpStartTime 
      Height          =   315
      Left            =   5520
      TabIndex        =   30
      Top             =   4260
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   556
      _Version        =   393216
      Format          =   144703490
      CurrentDate     =   39955
   End
   Begin MSComCtl2.DTPicker dtpFinishTime 
      Height          =   315
      Left            =   5520
      TabIndex        =   31
      Top             =   4680
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   556
      _Version        =   393216
      Format          =   144703490
      CurrentDate     =   39955
   End
   Begin VB.Label Label9 
      Caption         =   "Encounter:"
      Height          =   255
      Left            =   120
      TabIndex        =   27
      Top             =   3060
      Width           =   1815
   End
   Begin VB.Label Label8 
      Caption         =   "To:"
      Height          =   255
      Left            =   120
      TabIndex        =   19
      Top             =   4740
      Width           =   1095
   End
   Begin VB.Label Label7 
      Caption         =   "From:"
      Height          =   255
      Left            =   120
      TabIndex        =   18
      Top             =   4320
      Width           =   1095
   End
   Begin VB.Label Label6 
      Caption         =   "TCP Port /  Batch Number:"
      Height          =   255
      Left            =   120
      TabIndex        =   15
      Top             =   5160
      Width           =   2295
   End
   Begin VB.Label Label5 
      Caption         =   "Unit:"
      Height          =   255
      Left            =   120
      TabIndex        =   11
      Top             =   3480
      Width           =   1815
   End
   Begin VB.Label Label4 
      Caption         =   "Source (contains): *2"
      Height          =   255
      Left            =   120
      TabIndex        =   10
      Top             =   2640
      Width           =   2295
   End
   Begin VB.Label Label3 
      Caption         =   "Description (contains): *1"
      Height          =   255
      Left            =   120
      TabIndex        =   9
      Top             =   2220
      Width           =   2295
   End
   Begin VB.Label Label2 
      Caption         =   "Category:"
      Height          =   255
      Left            =   120
      TabIndex        =   7
      Top             =   1800
      Width           =   1815
   End
   Begin VB.Label Label1 
      Caption         =   "Source:"
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   120
      Width           =   1815
   End
End
Attribute VB_Name = "frmEventLogFilter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
'
' Event Viewer Filter
'
Private Const NUM_EVENT_TYPES = 4

Private Const DATE_MODE_ALL = "A"
Private Const DATE_MODE_DATE = "D"

Private m_parent            As frmEventLogViewer
Private m_filter            As PFSEventLogFilter

Private cdlSource           As New PFSCodeDescList
Private cdlType             As New PFSCodeDescList
Private cdlCategory         As New PFSCodeDescList
Private cdlUnit             As New PFSCodeDescList
Private cdlStartDateMode    As New PFSCodeDescList
Private cdlFinishDateMode   As New PFSCodeDescList

Private m_encounter_id      As Long

Private m_any_changes       As Boolean

Public Sub Setup(parent As frmEventLogViewer, filter As PFSEventLogFilter)
    'save pointers to the parent form
    Set m_parent = parent
    Set m_filter = filter

    SetFormFromFilter
End Sub

Public Sub SetDefaults()
    Dim i As Integer
    
    For i = 0 To NUM_EVENT_TYPES - 1
        chkEventType(i).value = vbChecked
    Next i

    cboSource.ListIndex = 0
    cboCategory.ListIndex = 0
    txtDescription.Text = ""
    txtSourceText.Text = ""
    txtPatientName.Text = ""
    m_encounter_id = 0
    txtTCPPort.Text = ""
    cboUnit.ListIndex = 0
    
    chkToday.value = vbChecked
    cboStartMode.ListIndex = 0
    cboFinishMode.ListIndex = 0
    dtpStartDate.value = Date
    dtpStartTime.value = Now()
    dtpFinishDate.value = Date
    dtpFinishTime.value = Now()

    SetAnyChanges True
End Sub

Private Sub Form_Load()
    Dim i As Integer
    
    Me.caption = App.ProductName & " Events Filter"

    g_display.SetStdTheme Me, True

    cdlType.LoadLVC LVC_EVENT_TYPE
    cdlSource.LoadLVC LVC_EVENT_SOURCE, cdlInsertAllChoice
    cdlCategory.LoadLVC LVC_EVENT_CATEGORY, cdlInsertAllChoice
    cdlUnit.LoadSQL SQL_ALL_UNITS, cdlInsertAllChoice
    
    cdlStartDateMode.AddCodeDesc DATE_MODE_ALL, "All Events"
    cdlStartDateMode.AddCodeDesc DATE_MODE_DATE, "First Event"

    cdlFinishDateMode.AddCodeDesc DATE_MODE_ALL, "All Events"
    cdlFinishDateMode.AddCodeDesc DATE_MODE_DATE, "Last Event"
    
    For i = 0 To NUM_EVENT_TYPES - 1
        chkEventType(i).caption = cdlType.desc(i + 1)
        chkEventType(i).value = vbChecked
    Next i

    cdlSource.FillComboBox cboSource
    cdlCategory.FillComboBox cboCategory
    cdlUnit.FillComboBox cboUnit
    cdlStartDateMode.FillComboBox cboStartMode
    cdlFinishDateMode.FillComboBox cboFinishMode
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set m_parent = Nothing
End Sub

'Look for accelerator events.  Form.KeyPreview must be True.
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If (KeyCode = vbKeyF1) Then
        g_help.ShowHelp hWnd, HELP_EVENT_FILTER
        KeyCode = 0
        Shift = 0
    End If
End Sub

'
' look for changes to any value
'
Private Sub cboCategory_Click()
    SetAnyChanges True
End Sub

Private Sub cboFinishMode_Click()
    SetAnyChanges True
    SetContolState
End Sub

Private Sub cboSource_Click()
    SetAnyChanges True
End Sub

Private Sub cboStartMode_Click()
    SetAnyChanges True
    SetContolState
End Sub

Private Sub cboUnit_Click()
    SetAnyChanges True
End Sub

Private Sub chkEventType_Click(Index As Integer)
    SetAnyChanges True
End Sub

Private Sub chkToday_Click()
    SetAnyChanges True
    SetContolState
End Sub

Private Sub dtpStartDate_Click()
    SetAnyChanges True
End Sub
Private Sub dtpStartTime_Click()
    SetAnyChanges True
End Sub

Private Sub dtpFinishDate_Click()
    SetAnyChanges True
End Sub
Private Sub dtpFinishTime_Click()
    SetAnyChanges True
End Sub

Private Sub txtDescription_KeyPress(KeyAscii As Integer)
    SetAnyChanges True
End Sub

Private Sub txtSourceText_KeyPress(KeyAscii As Integer)
    SetAnyChanges True
End Sub

Private Sub txtTCPPort_KeyPress(KeyAscii As Integer)
    SetAnyChanges True
End Sub

'
' Command buttons
'
Private Sub cmdLookupPatient_Click()
    Dim enc_id As Long

    enc_id = frmLookupPatient.LookupPatientEncounterID(Me)
    
    If (enc_id <> m_encounter_id) Then
        SetEncounter enc_id
        SetAnyChanges True
    End If
End Sub

Private Sub cmdClearPatient_Click()
    SetEncounter 0
    SetAnyChanges True
End Sub

Private Sub cmdRestoreDefaults_Click()
    SetDefaults
End Sub

Private Sub cmdOK_Click()
    If m_any_changes Then cmdApply_Click
    Unload Me
End Sub

Private Sub cmdCancel_Click()
    Unload Me
End Sub

Private Sub cmdApply_Click()
    SetFilterFromForm
    m_parent.LoadEvents
    SetAnyChanges False
End Sub


'
' Internal
'
Private Sub SetContolState()
    Dim is_today As Boolean
    is_today = g_util.IsChecked(chkToday)
    
    cboStartMode.Enabled = (Not is_today)
    cboFinishMode.Enabled = (Not is_today)
    
    dtpStartDate.Enabled = (Not is_today) And (cboStartMode.ListIndex <> 0)
    dtpStartTime.Enabled = (Not is_today) And (cboStartMode.ListIndex <> 0)

    dtpFinishDate.Enabled = (Not is_today) And (cboFinishMode.ListIndex <> 0)
    dtpFinishTime.Enabled = (Not is_today) And (cboFinishMode.ListIndex <> 0)
End Sub

Private Sub SetAnyChanges(changes As Boolean)
    m_any_changes = changes
    cmdApply.Enabled = (m_any_changes)
End Sub

Private Sub SetEncounter(enc_id As Long)
    Dim enc As New encounter
    
    m_encounter_id = enc_id
    
    If enc_id <> 0 Then
        enc.Load enc_id
        txtPatientName.Text = enc.Patient.FullName
    Else
        txtPatientName.Text = ""
    End If
End Sub


Private Sub SetFormFromFilter()
    On Error GoTo errHandler
    Dim i As Integer, n As Integer
    Dim arr() As String
    
    'The event codes are 1-based but the checkboxes are zero based
    n = g_util.SplitTextOnChar(m_filter.TypeIDs, ",", arr(), 1, 0)
    If (n > 0) Then
        For i = 0 To NUM_EVENT_TYPES - 1
            chkEventType(i).value = vbUnchecked
        Next i
        For i = 1 To n
            chkEventType(CInt(arr(i)) - 1).value = vbChecked
        Next i
    Else
        'No selection was made = check all event types
        For i = 0 To NUM_EVENT_TYPES - 1
            chkEventType(i).value = vbChecked
        Next i
    End If

    cdlSource.SelectComboBoxItem cboSource, m_filter.Source
    cdlCategory.SelectComboBoxItem cboCategory, m_filter.Category
    txtDescription.Text = m_filter.Description
    txtSourceText.Text = m_filter.SourceText
    SetEncounter m_filter.encounter_id
    txtTCPPort.Text = g_dbutil.ZeroToNull(m_filter.TCPPort) & ""
    cdlUnit.SelectComboBoxItem cboUnit, m_filter.UnitID
    
    g_util.SetCheckbox chkToday, m_filter.Today
    cboStartMode.ListIndex = IIf(m_filter.UseStartDate, 1, 0)
    cboFinishMode.ListIndex = IIf(m_filter.UseFinishDate, 1, 0)
    dtpStartDate.value = m_filter.StartDateTime
    dtpStartTime.value = m_filter.StartDateTime
    dtpFinishDate.value = m_filter.FinishDateTime
    dtpFinishTime.value = m_filter.FinishDateTime
    
    SetAnyChanges False
    Exit Sub
    
errHandler:
    g_util.MsgBoxError
    Exit Sub
    Resume  'debug
End Sub

Private Sub SetFilterFromForm()
    On Error GoTo errHandler
    Dim s As String
    Dim i As Integer
    
    'The checkboxes are zero based but the event codes are 1-based
    s = ""
    For i = 0 To NUM_EVENT_TYPES - 1
        If (chkEventType(i).value = vbChecked) Then
            If Len(s) > 0 Then s = s & ","
            s = s & CStr(i + 1)
        End If
    Next i
    m_filter.TypeIDs = s

    m_filter.Source = cdlSource.Code(cboSource.Text)
    m_filter.Category = cdlCategory.Code(cboCategory.Text)
    m_filter.Description = txtDescription.Text
    m_filter.SourceText = txtSourceText.Text
    m_filter.UnitID = g_dbutil.DBToLong(cdlUnit.Code(cboUnit.Text))
    m_filter.TCPPort = g_dbutil.DBToLong(txtTCPPort.Text)       'null to zero
    m_filter.encounter_id = m_encounter_id
    
    m_filter.Today = g_util.IsChecked(chkToday)
    m_filter.UseStartDate = (cdlStartDateMode.Code(cboStartMode.Text) = DATE_MODE_DATE)
    m_filter.UseFinishDate = (cdlFinishDateMode.Code(cboFinishMode.Text) = DATE_MODE_DATE)
    m_filter.StartDateTime = DateValue(dtpStartDate.value) + TimeValue(dtpStartTime.value)
    m_filter.FinishDateTime = DateValue(dtpFinishDate.value) + TimeValue(dtpFinishTime.value)
    Exit Sub
    
errHandler:
    g_util.MsgBoxError
    Exit Sub
    Resume  'debug
End Sub

