VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmAuditTrailViewer 
   Caption         =   "Audit Trail Viewer"
   ClientHeight    =   5505
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11670
   Icon            =   "frmAuditTrailViewer.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   ScaleHeight     =   5505
   ScaleWidth      =   11670
   StartUpPosition =   2  'CenterScreen
   Begin VB.Frame fraAccountNumber 
      Caption         =   "Account Number"
      Height          =   735
      Left            =   6000
      TabIndex        =   20
      Top             =   0
      Width           =   5415
      Begin VB.TextBox txtAccountNumber 
         Enabled         =   0   'False
         Height          =   285
         Left            =   2880
         TabIndex        =   22
         Top             =   240
         Width           =   2415
      End
      Begin VB.CheckBox chkAccountNumber 
         Caption         =   "This Account Number only:"
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Top             =   270
         Width           =   2655
      End
   End
   Begin VB.Frame fraDateRange 
      Caption         =   "Date Range"
      Height          =   735
      Left            =   120
      TabIndex        =   10
      Top             =   0
      Width           =   5775
      Begin MSComCtl2.DTPicker dtpFrom 
         Height          =   315
         Left            =   2280
         TabIndex        =   12
         Top             =   240
         Width           =   1335
         _ExtentX        =   2355
         _ExtentY        =   556
         _Version        =   393216
         Format          =   184483841
         CurrentDate     =   37875
      End
      Begin VB.PictureBox picXP 
         BackColor       =   &H0040FFFF&
         BorderStyle     =   0  'None
         Height          =   435
         Left            =   120
         ScaleHeight     =   435
         ScaleWidth      =   3375
         TabIndex        =   17
         Top             =   240
         Width           =   3375
         Begin VB.OptionButton optFrom 
            Caption         =   "From"
            Height          =   375
            Left            =   1440
            TabIndex        =   19
            Top             =   0
            Width           =   735
         End
         Begin VB.OptionButton optAllDates 
            Caption         =   "All Dates"
            Height          =   375
            Left            =   0
            TabIndex        =   18
            Top             =   0
            Width           =   1095
         End
      End
      Begin MSComCtl2.DTPicker dtpTo 
         Height          =   315
         Left            =   4200
         TabIndex        =   11
         Top             =   240
         Width           =   1455
         _ExtentX        =   2566
         _ExtentY        =   556
         _Version        =   393216
         Format          =   184483841
         CurrentDate     =   37875
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Caption         =   "to"
         Height          =   255
         Left            =   3720
         TabIndex        =   13
         Top             =   300
         Width           =   375
      End
   End
   Begin VB.Frame fraEventTypes 
      Caption         =   "Event Types"
      Height          =   4035
      Left            =   120
      TabIndex        =   7
      Top             =   780
      Width           =   3690
      Begin VB.ListBox lstEvents 
         Enabled         =   0   'False
         Height          =   2985
         Left            =   120
         Style           =   1  'Checkbox
         TabIndex        =   9
         Top             =   600
         Width           =   3435
      End
      Begin VB.CheckBox chkAllEvents 
         Caption         =   "All Events"
         Height          =   315
         Left            =   120
         TabIndex        =   8
         Top             =   240
         Value           =   1  'Checked
         Width           =   1815
      End
   End
   Begin VB.Frame fraUnits 
      Caption         =   "Units"
      Height          =   4035
      Left            =   3960
      TabIndex        =   4
      Top             =   780
      Width           =   3690
      Begin VB.CheckBox chkAllUnits 
         Caption         =   "All Units"
         Height          =   315
         Left            =   120
         TabIndex        =   6
         Top             =   240
         Value           =   1  'Checked
         Width           =   1815
      End
      Begin VB.ListBox lstUnits 
         Enabled         =   0   'False
         Height          =   2985
         Left            =   120
         Style           =   1  'Checkbox
         TabIndex        =   5
         Top             =   600
         Width           =   3435
      End
   End
   Begin VB.Frame fraUsers 
      Caption         =   "Users"
      Height          =   4035
      Left            =   7800
      TabIndex        =   2
      Top             =   780
      Width           =   3690
      Begin VB.CheckBox chkActiveUsersOnly 
         Caption         =   "Active Users Only"
         Enabled         =   0   'False
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   3660
         Width           =   1935
      End
      Begin VB.CheckBox chkAllUsers 
         Caption         =   "All Users"
         Height          =   315
         Left            =   120
         TabIndex        =   14
         Top             =   240
         Value           =   1  'Checked
         Width           =   1695
      End
      Begin VB.ListBox lstUsers 
         Enabled         =   0   'False
         Height          =   2760
         Left            =   120
         Style           =   1  'Checkbox
         TabIndex        =   3
         Top             =   600
         Width           =   3435
      End
      Begin VB.Label lblInactiveUsers 
         Alignment       =   1  'Right Justify
         Caption         =   "* inactive users"
         Height          =   255
         Left            =   2040
         TabIndex        =   16
         Top             =   3660
         Width           =   1575
      End
   End
   Begin VB.CommandButton cmdReport 
      Caption         =   "Report"
      Default         =   -1  'True
      Height          =   435
      Left            =   4080
      TabIndex        =   1
      Top             =   4980
      Width           =   1335
   End
   Begin VB.CommandButton cmdClose 
      Cancel          =   -1  'True
      Caption         =   "Close"
      Height          =   435
      Left            =   6195
      TabIndex        =   0
      Top             =   4980
      Width           =   1335
   End
End
Attribute VB_Name = "frmAuditTrailViewer"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const MIN_WINDOW_HEIGHT = 4500
Private Const MIN_WINDOW_WIDTH = 12000

Private cdlEvents   As New PFSCodeDescList
Private cdlUsers    As New PFSCodeDescList
Private Const WARNING_ROWS = 1000
Private Const MARGIN = 120
Private m_ignore_click As Boolean

Private m_initial_acct  As String

'
'Select an account number before showing the form (optional)
'
Public Sub Setup(Optional encounter_id As Long = 0)
    Dim sql As String
    Dim rs As New Recordset
    
    If (encounter_id <> 0) Then
        sql = "select acct_number from encounter where encounter_id=" & encounter_id
        rs.Open sql, g_cnADO, adOpenStatic
        If Not rs.eof Then
            m_initial_acct = rs(0)
        End If
        rs.Close
    Else
        m_initial_acct = ""
    End If
End Sub

Private Sub Form_Load()
    On Error GoTo errHandler
    Me.caption = App.ProductName & " Audit Trail"

    g_display.SetStdTheme Me, True, SETFONT_RESIZE_ALL_BOXES

    'load list of audit events
    cdlEvents.LoadLVC LVC_AUDIT_EVENT
    cdlEvents.FillListBox lstEvents
    
    'fill the list of units for the user to choose from
    g_user.Security.UnitsCDL.FillListBox lstUnits
    
    'load users
    cdlUsers.LoadSQL SQL_ALL_USERS
    cdlUsers.FillListBox lstUsers
        
    'set default dates
    optFrom.value = True
    dtpTo.value = Date
    dtpFrom.value = Date
    
    If Len(m_initial_acct) Then
        chkAccountNumber.value = vbChecked
        txtAccountNumber.Text = m_initial_acct
        optAllDates.value = True
    Else
        chkAccountNumber.value = vbUnchecked
        txtAccountNumber.Text = ""
    End If
    
    Exit Sub
    
errHandler:
    g_util.MsgBoxError "loading audit trail"
End Sub

Private Sub Form_Resize()
    On Error GoTo errHandler
    
    If (Me.WindowState = vbMinimized) Then Exit Sub

    If Me.height < MIN_WINDOW_HEIGHT Then
        Me.height = MIN_WINDOW_HEIGHT
        Exit Sub
    End If
    If Me.width < MIN_WINDOW_WIDTH Then
        Me.width = MIN_WINDOW_WIDTH
        Exit Sub
    End If
    
    cmdClose.top = Me.ScaleHeight - cmdClose.height - MARGIN
    cmdReport.top = cmdClose.top
    cmdReport.left = (Me.ScaleWidth - cmdReport.width * 3) / 2
    cmdClose.left = cmdReport.left + cmdReport.width * 2
    
    fraUnits.height = cmdClose.top - fraUnits.top - MARGIN
    fraEventTypes.height = fraUnits.height
    fraUsers.height = fraUnits.height
    lstEvents.height = fraEventTypes.height - lstEvents.top - MARGIN
    lstUnits.height = lstEvents.height
    
    lstUsers.height = lstEvents.height - lblInactiveUsers.height + MARGIN / 2
    lblInactiveUsers.top = lstUsers.top + lstUsers.height + MARGIN
    chkActiveUsersOnly.top = lblInactiveUsers.top
    
    fraEventTypes.left = MARGIN
    fraEventTypes.width = (Me.ScaleWidth - 4 * MARGIN) / 3
    fraUnits.left = fraEventTypes.left + fraEventTypes.width + MARGIN
    fraUnits.width = fraEventTypes.width
    fraUsers.left = fraUnits.left + fraUnits.width + MARGIN
    fraUsers.width = fraEventTypes.width
    
    lstEvents.width = fraEventTypes.width - 2 * MARGIN
    lstUnits.width = lstEvents.width
    lstUsers.width = lstEvents.width

    'fraDateRange.Width = Me.ScaleWidth - 2 * margin
    fraAccountNumber.width = Me.ScaleWidth - fraDateRange.width - 3 * MARGIN
    Exit Sub

errHandler:
    g_util.MsgBoxError
End Sub

'Look for accelerator events.  Form.KeyPreview must be True.
Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If (KeyCode = vbKeyF1) Then
        g_help.ShowHelp hWnd, HELP_AUDIT_TRAIL
        KeyCode = 0
        Shift = 0
    End If
End Sub

Private Sub chkAccountNumber_Click()
    txtAccountNumber.Enabled = (chkAccountNumber.value = vbChecked)
    If txtAccountNumber.Enabled Then
        On Error Resume Next                'ignore error if not on screen yet
        txtAccountNumber.SetFocus
    Else
        txtAccountNumber.Text = ""
    End If
End Sub

Private Sub chkAllEvents_Click()
    RefreshSelection lstEvents, Not (chkAllEvents.value = 0)
End Sub

Private Sub chkAllUnits_Click()
    RefreshSelection lstUnits, Not (chkAllUnits.value = 0)
End Sub

Private Sub chkAllUsers_Click()
    RefreshSelection lstUsers, Not (chkAllUsers.value = 0)
    m_ignore_click = True
    chkActiveUsersOnly.value = vbUnchecked
    chkActiveUsersOnly.Enabled = (chkAllUsers.value = vbUnchecked)
    m_ignore_click = False
End Sub

Private Sub chkActiveUsersOnly_Click()
    If Not m_ignore_click Then
        ReloadUsers (chkActiveUsersOnly.value = 1)
    End If
End Sub

Private Sub cmdReport_Click()
    On Error GoTo errHandler
    
    Dim cn          As Connection
    Dim rs          As New Recordset
    Dim recs        As Long
    Dim result()    As Variant
    Dim i           As Long
    Dim j           As Long
    Dim date_range  As String
    
    g_util.CursorPushHourglass
    
    Set cn = g_dbutil.NewRemoteConnection
    rs.Open ReportSQL, cn, adOpenStatic, adLockReadOnly
    g_util.CursorPop
    
    recs = RecordsToOutput(rs.RecordCount)
    If recs < 0 Then
        'do nothing; user has been already warned
    ElseIf recs = 0 Then
        MsgBox "There are no records with this combination of Dates, Events, Units and Users", vbInformation, "No Data"
    Else
        'do
        ReDim result(recs, rs.fields.count)
        For i = 1 To UBound(result, 1)
            For j = 1 To UBound(result, 2)
                result(i, j) = rs(j - 1)
            Next j
            rs.MoveNext
        Next i
        
        If optAllDates.value = True Then
            date_range = "All Dates"
        Else
            date_range = "Date: " & dtpFrom.value & " to " & dtpTo.value
        End If
        
        rptAuditTrailViewer.Setup Me, result(), date_range, (rs.RecordCount > recs)
    End If
    rs.Close
    cn.Close
    
    Exit Sub
errHandler:
    g_util.MsgBoxError "generating audit trail report", "cmdReport_Click"
End Sub

Private Function RecordsToOutput(recs As Long) As Long
    Dim response    As Variant
    
    If recs >= WARNING_ROWS Then
startOver:
        RecordsToOutput = -1
        response = InputBox("The query returned " & recs & " records. " & _
        "How many records would you like to view?", "Number of records to output", recs)
        If Len(response) = 0 Then
            'cancel
        ElseIf Not IsNumeric(response) Then
            MsgBox "You have entered an invalid number � please try again.", _
            vbExclamation, "Invalid Number"
            GoTo startOver
        ElseIf CLng(response) <= 0 Or CLng(response) > recs Then
            MsgBox "You have entered an invalid number � please try again.", _
            vbExclamation, "Invalid Number"
            GoTo startOver
        Else
            RecordsToOutput = CLng(response)
        End If
    Else
        RecordsToOutput = recs
    End If
End Function

Private Sub cmdClose_Click()
    Unload Me
End Sub

Private Sub RefreshSelection(lst As ListBox, select_items As Boolean)
    On Error GoTo errHandler
    Dim i As Integer
    
    lst.Enabled = Not select_items
    For i = 0 To lst.ListCount - 1
        lst.selected(i) = False
    Next i
    lst.ListIndex = 0
    Exit Sub

errHandler:
    g_util.MsgBoxError
End Sub

Private Sub ReloadUsers(active_only As Boolean)
    On Error GoTo errHandler
    Dim i As Integer
    
    g_util.CursorPushHourglass
    
    With lstUsers
        .Clear
        cdlUsers.LoadSQL SQL_ALL_USERS
        cdlUsers.FillListBox lstUsers
        
        If active_only Then
            Do While i < .ListCount
                If Mid$(.list(i), Len(.list(i))) = "*" Then
                    .RemoveItem (i)
                Else
                    i = i + 1
                End If
            Loop
            .Refresh
        End If
    End With
    
    g_util.CursorPop
    Exit Sub

errHandler:
    g_util.MsgBoxError
End Sub

Private Function ReportSQL() As String
    Dim sql As String
    
 sql = "SELECT A.TIMESTAMP AS TIMESTAMP," & _
    " L1.LOOKUP_DESCRIPTION AS EVENT, " & _
    " ISNULL(USERS.LAST_NAME,'') + ', ' + ISNULL(USERS.FIRST_NAME,'') + ' ' + ISNULL(USERS.MIDDLE_NAME,'')  AS USERS, " & _
    " UNIT.NAME AS UNIT," & _
    " CASE WHEN P.LAST_NAME IS NOT NULL THEN (P.LAST_NAME + ', ' + ISNULL(P.FIRST_NAME, '') + ' ' + ISNULL(P.MIDDLE_NAME,''))" & _
    "     ELSE CASE WHEN A.AUDIT_EVENT_ID BETWEEN 30 AND 39 THEN '<deleted>'" & _
    "       ELSE NULL" & _
    " END END AS PATIENT, " & _
    " L2.LOOKUP_DESCRIPTION AS REASON, " & _
    " A.REASON AS FREE_TEXT," & _
    " R.DESCRIPTION AS REPORT," & _
    " A.DESCRIPTION AS DESCRIPTION " & _
    " FROM AUDIT_TRAIL AS A "
    
    sql = sql & _
    " LEFT JOIN LOOKUP_VALUE AS L1 ON (L1.LOOKUP_CODE = A.AUDIT_EVENT_ID AND L1.LOOKUP_DEFINITION_KEY = " & LVC_AUDIT_EVENT & ")" & _
    " LEFT JOIN LOOKUP_VALUE AS L2 ON (L2.LOOKUP_CODE = A.AUDIT_REASON_LVC AND L2.LOOKUP_DEFINITION_KEY = " & _
    "   (CASE WHEN A.AUDIT_EVENT_ID IN (60, 61, 62) THEN " & LVC_STAFFING_REASON & " ELSE " & LVC_AUDIT_REASON & " END)) " & _
    " LEFT JOIN USERS ON USERS.USER_ID = A.USER_ID " & _
    " LEFT JOIN UNIT ON UNIT.UNIT_ID = A.UNIT_ID " & _
    " LEFT JOIN ENCOUNTER AS E ON E.ENCOUNTER_ID = A.ENCOUNTER_ID " & _
    " LEFT JOIN PERSON AS P ON P.PERSON_ID = E.PERSON_ID " & _
    " LEFT JOIN REPORT AS R ON R.REPORT_ID = A.REPORT_ID " & _
    " WHERE "
    
    'datetime range
    If Not optAllDates.value Then
        sql = sql & " A.TIMESTAMP >= " & g_dbutil.SQL_DateTime(dtpFrom.value) & _
        " AND A.TIMESTAMP <= " & g_dbutil.SQL_DateTime(dtpTo.value + 1) & _
        " AND "
    End If
    'units
    If g_util.IsChecked(chkAllUnits) Then
        sql = sql & " (A.UNIT_ID IN " & ids(lstUnits, True) & " OR A.UNIT_ID IS NULL)"
    Else
        sql = sql & " A.UNIT_ID IN " & ids(lstUnits, False)
    End If
    
    'events
    If Not g_util.IsChecked(chkAllEvents) Then
        sql = sql & " AND A.AUDIT_EVENT_ID IN " & ids(lstEvents, False, cdlEvents)
    End If
    'users
    If Not g_util.IsChecked(chkAllUsers) Then
        sql = sql & " AND A.USER_ID IN " & ids(lstUsers, False, cdlUsers)
    End If
    'filter by account number if needed
    If g_util.IsChecked(chkAccountNumber) Then
        sql = sql & _
        " AND E.ACCT_NUMBER = " & g_dbutil.SQL_String(txtAccountNumber.Text)
    End If
    'order by date
    sql = sql & " ORDER BY A.TIMESTAMP DESC"
    ReportSQL = sql
End Function

Private Function ids(lst As ListBox, all_items As Boolean, Optional cdl As PFSCodeDescList) As String
    Dim i   As Integer
    Dim id  As Variant
    
    ids = "("
    For i = 0 To lst.ListCount - 1
        If lst.selected(i) Or all_items Then
            If cdl Is Nothing Then
                id = g_user.Security.UnitsCDL.Code(lst.list(i))
            Else
                id = cdl.Code(lst.list(i))
            End If
            
            If IsNumeric(id) Then
                If Len(ids) > 1 Then ids = ids & ","
                ids = ids & CLng(id)
            End If
        End If
    Next i
    If ids = "(" Then
        ids = "(0)"
    Else
        ids = ids & ")"
    End If
End Function
