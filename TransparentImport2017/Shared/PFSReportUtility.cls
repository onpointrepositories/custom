VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "PFSReportUtility"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'
' PFSReportUtility: Generic Report utility classes
' These public functions are for anyone to use.
'

Public Function FiscalStartDateForEndDate(dt As Date) As Date
    On Error GoTo errHandler
    Dim rs As New Recordset, cn As Connection
    Dim sql As String
    Dim result As Date
    
    result = g_util.MakeDate(year(dt), 1, 1)        'default to start of year
    
    Set cn = g_dbutil.NewRemoteConnection
    'Each facility can have a different fiscal year and we don't know which one to select
    'Get the latest
    sql = "SELECT MAX(YTD_START_DATE) FROM FACILITY_PARAM"
    rs.Open sql, cn
    If Not rs.EOF Then
        If Not IsNull(rs(0)) Then                   'MAX might return null [58957]
            result = rs(0)                          'get the latest fiscal year entered
        End If
    End If
    rs.Close
    
    'The FY start may be out of date; keep bumping by a year until we go too far
    Do While (result < dt)
        result = DateAdd("yyyy", 1, result)
    Loop
    
    'This is the FY start closest to the reference date (and less than the reference date)
    result = DateAdd("yyyy", -1, result)
    
    FiscalStartDateForEndDate = result
    cn.Close
    Exit Function
    
errHandler:
    g_util.ThrowError "FiscalStartDateForEndDate"
End Function

'
' Calculate the unique patient census for a time range
'
' Do not change this to use the report date: this may be called on to calculate
' the value for any time range.
Public Function UniqueCensusForRange(ByVal unit_id As Long, ByVal start As Date, ByVal finish As Date) As Integer
    On Error GoTo errHandler
    Dim rs As New Recordset, cn As Connection, sql As String
    Dim result As Integer
    
    Set cn = g_dbutil.NewRemoteConnection()

    sql = "SELECT COUNT(DISTINCT ENCOUNTER_ID)" & _
        " FROM ENCOUNTER_LOCATION" & _
        " WHERE UNIT_ID=" & unit_id & _
        " AND EFFECTIVE_DATETIME_IN  < " & g_dbutil.SQL_DateTime(finish) & _
        " AND EFFECTIVE_DATETIME_OUT > " & g_dbutil.SQL_DateTime(start)
    rs.Open sql, cn
    result = rs(0)
    rs.Close
    cn.Close
    UniqueCensusForRange = result
    Exit Function
    
errHandler:
    g_util.ThrowError "UniqueCensusForRange"
End Function


'
' Calculate the actual (bed and arrival) census for a time range
'
' Do not change this to use the report date: this may be called on to calculate
' the value for one shift only.
Public Function ActualCensusForRange(ByVal unit_id As Long, ByVal start As Date, ByVal finish As Date) As Integer
    On Error GoTo errHandler
    Dim rs As New Recordset, cn As Connection, sql As String
    Dim result As Integer
    
    Set cn = g_dbutil.NewRemoteConnection()
    
    sql = ActualCensusSQL(unit_id, start, finish, False)
    rs.Open sql, cn, adOpenStatic, adLockReadOnly
    result = rs.RecordCount
    rs.Close

    AdjustActualCensusForDNC unit_id, start, finish, result
    
    cn.Close
    ActualCensusForRange = result
    Exit Function
    
errHandler:
    g_util.ThrowError "ActualCensusForRange"
End Function


Public Function ActualCensusSQL(ByVal unit_id As Long, ByVal start As Date, ByVal finish As Date, include_in_out_detail As Boolean) As String
    On Error GoTo errHandler
    
    Dim sql As String
    '
    ' Note: our definition of 'actual' census counts every arrival so if a
    ' patient leaves and returns during the time period, they will be counted twice.
    ' Do not group by encounter id so we can count patient arrivals.
    '
    '(We could limit the span of the search (see next function) but we really do want to
    ' find patients that have been here for years.)
    sql = "SELECT ENCOUNTER_ID"
    If (include_in_out_detail) Then
        sql = sql & ", DATETIME_IN, DATETIME_OUT"
    End If
    sql = sql & _
        " FROM ENCOUNTER_LOCATION" & _
        " WHERE UNIT_ID=" & unit_id & _
        " AND EFFECTIVE_DATETIME_IN  < " & g_dbutil.SQL_DateTime(finish) & _
        " AND EFFECTIVE_DATETIME_OUT > " & g_dbutil.SQL_DateTime(start)
    If (1 = 1) Then
        'Do not count zero-length stays [7611]
        sql = sql & " AND EFFECTIVE_DATETIME_IN <> EFFECTIVE_DATETIME_OUT"
    End If
    If (Not include_in_out_detail) Then
        'Be sure to include only arrivals unless the transfer within the unit was before the start time
        sql = sql & " AND ((IS_TRANSFER_IN='Y') OR (EFFECTIVE_DATETIME_IN < " & g_dbutil.SQL_DateTime(start) & "))"
    End If
    If (include_in_out_detail) Then
        'order for log file
        sql = sql & " ORDER BY DATETIME_IN, ENCOUNTER_ID"
    End If

    ActualCensusSQL = sql
    Exit Function

errHandler:
    g_util.ThrowError "ActualCensusSQL"
End Function


Private Sub AdjustActualCensusForDNC( _
    ByVal unit_id As Long, ByVal dt_start As Date, ByVal dt_finish As Date, _
    ByRef actual_census As Integer _
)
    On Error GoTo errHandler

    Dim rs As New Recordset, cn As Connection, sql As String
    Dim start_limit As Date, finish_limit As Date
    Dim encounter_id As Long, prev_encounter_id As Long, methodology_id As Long
    Dim dnc_flag As Boolean, class_flag As Boolean

    Set cn = g_dbutil.NewRemoteConnection()
    
    Debug.Print "Subtract DNC records from actual census from " & dt_start & " to " & dt_finish
    '
    ' Find out who had DNC records during the time period and get all
    ' classifications for them.
    '
    start_limit = DateAdd("d", -CLASSIFICATION_MAX_LOS_DAYS, dt_start)
    finish_limit = DateAdd("d", CLASSIFICATION_MAX_LOS_DAYS, dt_finish)

    sql = "SELECT ENCOUNTER_ID, METHODOLOGY_ID" & _
         " FROM CLASSIFICATION_EVENT" & _
         " WHERE ENCOUNTER_ID IN (" & _
         "    SELECT ENCOUNTER_ID" & _
         "    FROM CLASSIFICATION_EVENT" & _
         "    WHERE UNIT_ID=" & unit_id & _
         "    AND EFFECTIVE_DATETIME_IN  >= " & g_dbutil.SQL_DateTime(start_limit) & _
         "    AND EFFECTIVE_DATETIME_IN  <  " & g_dbutil.SQL_DateTime(dt_finish) & _
         "    AND EFFECTIVE_DATETIME_OUT >  " & g_dbutil.SQL_DateTime(dt_start) & _
         "    AND EFFECTIVE_DATETIME_OUT <= " & g_dbutil.SQL_DateTime(finish_limit) & _
         "    AND METHODOLOGY_ID=" & METH_ID_DO_NOT_CLASSIFY & _
         " )" & _
         " AND UNIT_ID=" & unit_id & _
         " AND EFFECTIVE_DATETIME_IN  >= " & g_dbutil.SQL_DateTime(start_limit) & _
         " AND EFFECTIVE_DATETIME_IN  <  " & g_dbutil.SQL_DateTime(dt_finish) & _
         " AND EFFECTIVE_DATETIME_OUT >  " & g_dbutil.SQL_DateTime(dt_start) & _
         " AND EFFECTIVE_DATETIME_OUT <= " & g_dbutil.SQL_DateTime(finish_limit) & _
         " ORDER BY ENCOUNTER_ID, EFFECTIVE_DATETIME_IN"
    
    Debug.Print sql
    rs.Open sql, cn
    
    'Decrement the actual census if the patient had no regular classifications this period.
    
    Do While Not rs.EOF
        encounter_id = rs("ENCOUNTER_ID")
        methodology_id = rs("METHODOLOGY_ID")
    
        If (encounter_id <> prev_encounter_id) Then
            'reset flags for new encounter
            dnc_flag = False
            class_flag = False
        End If
    
        Debug.Print "Encounter " & encounter_id & " ";

        If (methodology_id = METH_ID_DO_NOT_CLASSIFY) Then
            Debug.Print "Do Not Classify: ";
            'The patient has a DNC this time period
            If (dnc_flag) Then
                'The patient has more than one DNC in the same period
                Debug.Print "(ignore two in a row)"                '[7619]
            Else
                dnc_flag = True
                
                'subtract from actual census if the patient has not been classified this period
                If Not class_flag Then
                    Debug.Print "(decrement actual census)"
                    actual_census = actual_census - 1
                Else
                    Debug.Print "(no adjustment)"
                End If
            End If
        Else
            Debug.Print "Regular classification: ";
            'The patient has a regular classification this time period
            class_flag = True
            
            'Did a DNC prior to this decrement the census?
            If dnc_flag Then
                'restore the actual census
                Debug.Print "(restore actual census)"
                actual_census = actual_census + 1
                'reset the DNC flag in case there are more regular classifications
                dnc_flag = False
            Else
                Debug.Print "(no adjustment)"
            End If
        End If
    
        prev_encounter_id = encounter_id
        rs.MoveNext
    Loop

    rs.Close
    cn.Close
    Exit Sub

errHandler:
    g_util.ThrowError "AdjustActualCensusForDNC"
    Resume  'debug
End Sub



'
' Calculate the classification census for a time range
'
' Do not change this to use the report date: this may be called on to calculate
' the value for one shift only.
Public Function ClassCensusForRange(ByVal unit_id As Long, ByVal start As Date, ByVal finish As Date) As Integer
    On Error GoTo errHandler
    
    Dim rs As New Recordset, cn As Connection, sql As String
    Dim start_limit As Date, finish_limit As Date
    Dim result As Integer
    
    Set cn = g_dbutil.NewRemoteConnection()
    
    ' Create start & finish limits so the query optimizer can use the index.
    start_limit = DateAdd("d", -CLASSIFICATION_MAX_LOS_DAYS, start)
    finish_limit = DateAdd("d", CLASSIFICATION_MAX_LOS_DAYS, finish)

    '
    ' Classifications:
    ' A patient may be classified more than once, so count unique encounters only.
    '
    sql = "SELECT COUNT(DISTINCT ENCOUNTER_ID)" & _
          " FROM CLASSIFICATION_EVENT" & _
          " WHERE UNIT_ID=" & unit_id & _
          " AND EFFECTIVE_DATETIME_IN  >= " & g_dbutil.SQL_DateTime(start_limit) & _
          " AND EFFECTIVE_DATETIME_IN  <  " & g_dbutil.SQL_DateTime(finish) & _
          " AND EFFECTIVE_DATETIME_OUT >  " & g_dbutil.SQL_DateTime(start) & _
          " AND EFFECTIVE_DATETIME_OUT <= " & g_dbutil.SQL_DateTime(finish_limit) & _
          " AND REPORT_THIS_WORKLOAD='Y'" & _
          " AND IS_DELETED<>'Y'"
    rs.Open sql, cn, adOpenStatic, adLockReadOnly
    result = rs(0)
    rs.Close
    
    cn.Close
    ClassCensusForRange = result
    Exit Function
    
errHandler:
    g_util.ThrowError "ClassCensusForRange"
End Function


'
' Calculate the procedure census for a time range
'
' Do not change this to use the report date: this may be called on to calculate
' the value for one shift only.
' [8396] include all stages but continue to check the "report this" flag
Public Function ProcCensusForRange(ByVal unit_id As Long, ByVal start As Date, ByVal finish As Date) As Integer
    On Error GoTo errHandler
    
    Dim rs As New Recordset, cn As Connection, sql As String
    Dim start_limit As Date, finish_limit As Date
    Dim result As Integer
    
    Set cn = g_dbutil.NewRemoteConnection()
    
    ' Create start & finish limits so the query optimizer can use the index.
    start_limit = DateAdd("d", -PROCEDURE_MAX_LOS_DAYS, start)
    finish_limit = DateAdd("d", PROCEDURE_MAX_LOS_DAYS, finish)
    
    '
    ' A patient may have more than one procedure, so count unique encounters only.
    '
    sql = "SELECT COUNT(DISTINCT ENCOUNTER_ID)" & _
          " FROM PROCEDURE_EVENT" & _
          " WHERE UNIT_ID=" & unit_id & _
          " AND EFFECTIVE_DATETIME_IN  >= " & g_dbutil.SQL_DateTime(start_limit) & _
          " AND EFFECTIVE_DATETIME_IN  <  " & g_dbutil.SQL_DateTime(finish) & _
          " AND EFFECTIVE_DATETIME_OUT >  " & g_dbutil.SQL_DateTime(start) & _
          " AND EFFECTIVE_DATETIME_OUT <= " & g_dbutil.SQL_DateTime(finish_limit) & _
          " AND REPORT_THIS_WORKLOAD='Y'" & _
          " AND IS_DELETED<>'Y'"
    rs.Open sql, cn, adOpenStatic, adLockReadOnly
    result = rs(0)
    rs.Close
    
    cn.Close
    ProcCensusForRange = result
    Exit Function
    
errHandler:
    g_util.ThrowError "ProcCensusForRange"
End Function


'
' Calculate the combined classification and procedure census for a time range
'
Public Function ClassProcCensusForRange(ByVal unit_id As Long, ByVal start As Date, ByVal finish As Date, Optional treatment_area As String = "~") As Integer
    On Error GoTo errHandler
    
    Dim rs As New Recordset, cn As Connection, sql As String, and_treatment_area As String
    Dim start_limit As Date, finish_limit As Date
    Dim result As Integer
    
    Set cn = g_dbutil.NewRemoteConnection()
    
    ' Create start & finish limits so the query optimizer can use the index.
    start_limit = DateAdd("d", -CLASSIFICATION_MAX_LOS_DAYS, start)
    finish_limit = DateAdd("d", CLASSIFICATION_MAX_LOS_DAYS, finish)

    If (treatment_area <> "~") Then
        If (treatment_area = "") Then
            and_treatment_area = " AND UNIT_AREA_CODE IS NULL"
        Else
            and_treatment_area = " AND UNIT_AREA_CODE=" & g_dbutil.SQL_String(treatment_area)
        End If
    Else
        'no treatment area filter
    End If
    '
    ' A patient may have both classifications and procedures.
    ' Combine the results and count unique patients.  (the union will do this)
    '
    sql = "SELECT ENCOUNTER_ID" & _
          " FROM CLASSIFICATION_EVENT" & _
          " WHERE UNIT_ID=" & unit_id & _
          " AND EFFECTIVE_DATETIME_IN  >= " & g_dbutil.SQL_DateTime(start_limit) & _
          " AND EFFECTIVE_DATETIME_IN  <  " & g_dbutil.SQL_DateTime(finish) & _
          " AND EFFECTIVE_DATETIME_OUT >  " & g_dbutil.SQL_DateTime(start) & _
          " AND EFFECTIVE_DATETIME_OUT <= " & g_dbutil.SQL_DateTime(finish_limit) & _
          " AND REPORT_THIS_WORKLOAD='Y'" & _
          " AND IS_DELETED<>'Y'" & _
            and_treatment_area & _
          " UNION" & _
          " SELECT ENCOUNTER_ID" & _
          " FROM PROCEDURE_EVENT" & _
          " WHERE UNIT_ID=" & unit_id & _
          " AND EFFECTIVE_DATETIME_IN  >= " & g_dbutil.SQL_DateTime(start_limit) & _
          " AND EFFECTIVE_DATETIME_IN  <  " & g_dbutil.SQL_DateTime(finish) & _
          " AND EFFECTIVE_DATETIME_OUT >  " & g_dbutil.SQL_DateTime(start) & _
          " AND EFFECTIVE_DATETIME_OUT <= " & g_dbutil.SQL_DateTime(finish_limit) & _
          " AND REPORT_THIS_WORKLOAD='Y'" & _
          " AND IS_DELETED<>'Y'" & _
            and_treatment_area
    rs.Open sql, cn, adOpenStatic, adLockReadOnly

    result = rs.RecordCount
    rs.Close

    cn.Close
    ClassProcCensusForRange = result
    Exit Function
    
errHandler:
    g_util.ThrowError "ClassProcCensusForRange"
End Function


'
' How many classifications were add during the time range?
'
Public Function ClassifiedDuringRange(ByVal unit_id As Long, ByVal start As Date, ByVal finish As Date) As Integer
    On Error GoTo errHandler
    
    Dim rs As New Recordset, cn As Connection, sql As String
    Dim result As Integer
    
    Set cn = g_dbutil.NewRemoteConnection()
    '
    ' A patient may be classified more than once, so count unique encounters only.
    '
    sql = "SELECT COUNT(DISTINCT ENCOUNTER_ID)" & _
          " FROM CLASSIFICATION_EVENT" & _
          " WHERE UNIT_ID=" & unit_id & _
          " AND CLASSIFICATION_DATETIME >= " & g_dbutil.SQL_DateTime(start) & _
          " AND CLASSIFICATION_DATETIME <  " & g_dbutil.SQL_DateTime(finish) & _
          " AND REPORT_THIS_WORKLOAD='Y'" & _
          " AND IS_DELETED<>'Y'"

    rs.Open sql, cn, adOpenStatic, adLockReadOnly
    result = rs(0)
    rs.Close

    cn.Close
    ClassifiedDuringRange = result
    Exit Function
    
errHandler:
    g_util.ThrowError "ClassCensusForRange"
End Function



'
' Returns the number of hours to divide by for patient LOS census.
'
' Note: we are dividing by the shift paid hours to get equivalent staff beacuse
' that is all based on paid hours but we need to divide by this when counting
' patients.
'
Public Function ShiftEquivalentHoursForCensus(ByVal paid_hours As Double) As Double
    On Error GoTo errHandler

    Static rpt_paid_hours() As Double
    Static rpt_shift_length() As Double
    Static been_here As Boolean
    Dim i As Integer, low_bound As Double, high_bound As Double

    If (Not been_here) Then
        ShiftEquivalentLoadValues rpt_paid_hours(), rpt_shift_length()
        been_here = True
    End If
    
    low_bound = -1#
    For i = 1 To UBound(rpt_paid_hours)
        high_bound = rpt_paid_hours(i)
        If (paid_hours > low_bound) And (paid_hours <= high_bound) Then
            ShiftEquivalentHoursForCensus = rpt_shift_length(i)
            Exit Function
        End If
        low_bound = high_bound
    Next i

    ShiftEquivalentHoursForCensus = 24#
    Exit Function

errHandler:
    g_util.ThrowError "ShiftEquivalentHoursForCensus"
End Function


Private Sub ShiftEquivalentLoadValues(paid_hours() As Double, shift_length() As Double)
    On Error GoTo errHandler
    
    Dim rs As New Recordset, cn As Connection, sql As String
    Dim count As Integer, i As Integer

    Set cn = g_dbutil.NewRemoteConnection()
    
    sql = "SELECT PAID_HOURS_HIVAL, REPORT_SHIFT_LENGTH" & _
         " FROM RPT_EQUIV_SHIFT_LENGTH" & _
         " ORDER BY PAID_HOURS_HIVAL"
    rs.Open sql, cn, adOpenStatic

    count = rs.RecordCount
    ReDim paid_hours(0 To count)
    ReDim shift_length(0 To count)

    For i = 1 To count
        paid_hours(i) = rs(0)
        shift_length(i) = rs(1)
        rs.MoveNext
    Next i

    rs.Close
    cn.Close
    Exit Sub
    
errHandler:
    g_util.ThrowError "ShiftEquivalentLoadValues"
End Sub



'
' How many inpatients should be monitored in a two-week period?
'
Public Function RecPatientsToMonitor(ByVal daily_census As Integer, methodology_id As Long) As Integer
    Dim result As Integer

    '09/05: recommend 10% for all methodologies [5262]
    result = Round(daily_census * 10 / 100, 0)
    If (result < 3) Then result = 3

    RecPatientsToMonitor = result
End Function


'
' Are all of the units in this unit group in the same facility?
'
Public Function UnitsAreInSameFacility(ByVal unit_group_id As Long) As Boolean
    Static prev_unit_group_id As Long
    Static prev_result As Boolean
    Dim rs As New Recordset, cn As Connection
    Dim sql As String, result As String

    If (unit_group_id = prev_unit_group_id) Then
        UnitsAreInSameFacility = prev_result
        Exit Function
    End If

    Set cn = g_dbutil.NewRemoteConnection()
    
    sql = "SELECT DISTINCT FACILITY_ID FROM UNIT WHERE UNIT_ID IN (" & _
         "     SELECT UNIT_ID FROM UNIT_GROUP_UNIT" & _
         "     WHERE  UNIT_GROUP_ID = " & unit_group_id & _
         " )"
    rs.Open sql, cn, adOpenStatic
    
    result = (rs.RecordCount = 1)
    
    rs.Close

    prev_unit_group_id = unit_group_id
    prev_result = result
    
    cn.Close
    UnitsAreInSameFacility = result
End Function

'
' What should we put in the report heading when multiple units are present?
' Show the facility name if all units belong to the same facility.
' Show the enterprise name if there is more than one facility.
'
Public Function FacilityNameForUnitGroups(ByVal unit_group_ids As String) As String
    Static prev_unit_group_ids As String
    Static prev_result As String
    Dim rs As New Recordset, cn As Connection
    Dim sql As String, result As String

    If (unit_group_ids = prev_unit_group_ids) Then
        FacilityNameForUnitGroups = prev_result
        Exit Function
    End If
    
    Set cn = g_dbutil.NewRemoteConnection()
    
    sql = "SELECT NAME FROM FACILITY WHERE FACILITY_ID IN (" & _
         "     SELECT FACILITY_ID FROM UNIT WHERE UNIT_ID IN (" & _
         "         SELECT UNIT_ID FROM UNIT_GROUP_UNIT" & _
         "         WHERE  UNIT_GROUP_ID IN (" & unit_group_ids & ")" & _
         "     )" & _
         " )" & _
         " GROUP BY NAME"
    rs.Open sql, cn, adOpenStatic
    If rs.RecordCount = 1 Then
        result = rs(0) & ""             '(in case it is null)
    Else
        'There are multiple facilities or no units - show the enterprise name
        rs.Close
        rs.Open "SELECT NAME FROM ENTERPRISE", cn
        If Not rs.EOF Then
            result = rs(0) & ""         '(it could be null - has been)
        End If
    End If
    
    rs.Close

    prev_unit_group_ids = unit_group_ids
    prev_result = result
    
    cn.Close
    FacilityNameForUnitGroups = result
End Function

Public Function FacilityNameForUnitGroup(ByVal unit_group_id As Long) As String
    FacilityNameForUnitGroup = FacilityNameForUnitGroups(CStr(unit_group_id))
End Function


Public Function InpatientAcuity(ByVal total_workload As Double, ByVal total_los_hours As Double) As Double
    'workload is acuity for 24-hour period; divide by the number of equivalent 24-hour patients.
    InpatientAcuity = g_util.DivideWithoutError(total_workload, (total_los_hours / 24#))
End Function

Public Function EDAcuity(ByVal total_workload As Double, ByVal class_census As Double) As Double
    'workload is the acuity; get the average
    EDAcuity = g_util.DivideWithoutError(total_workload, class_census)
End Function

Public Function LOSAcuity(ByVal total_workload As Double, ByVal total_los_hours As Double) As Double
    'workload is acuity * LOS; divide by LOS to get acuity
    LOSAcuity = g_util.DivideWithoutError(total_workload, total_los_hours)
End Function

'Calculate the Acuity for a day report (24-hour periods)
'Inpatient and Perinatal use pro-rated workload.
'
Public Function Acuity( _
    ByRef meth As Methodology, _
    ByVal flat_workload As Double, _
    ByVal pro_rated_workload As Double, _
    ByVal total_los_hours As Double, _
    ByVal class_census As Double _
) As Double

    Dim result          As Double
    
    'Bail out if there is no methodology
    If meth Is Nothing Then Exit Function
    
    Select Case meth.WorkloadType
        Case WorkloadType24hr                                           'med/surg, mental health
            result = InpatientAcuity(pro_rated_workload, total_los_hours)
        Case WorkloadTypeLOS
            If meth.MethodologyGroupLVC = METH_GROUP_PERINATAL Then
                result = LOSAcuity(pro_rated_workload, total_los_hours)
            Else                                                        'ambulatory
                result = LOSAcuity(flat_workload, total_los_hours)
            End If
        Case WorkloadTypeED                                             'ED
            result = EDAcuity(flat_workload, class_census)
        Case Else
            MsgBox "Unexpected methodology workload type " & meth.WorkloadType
    End Select

    Acuity = result
End Function

'
' Calculate the Acuity for a shift report
' Everyone uses flat workload
'
Public Function ShiftAcuity( _
    ByRef meth As Methodology, _
    ByVal flat_workload As Double, _
    ByVal pro_rated_workload As Double, _
    ByVal total_los_hours As Double, _
    ByVal class_census As Double _
) As Double

    Dim result          As Double
    
    'Bail out if there is no methodology
    If meth Is Nothing Then Exit Function
    
    Select Case meth.WorkloadType
        Case WorkloadType24hr                                       'med/surg, mental health
            result = InpatientAcuity(flat_workload, total_los_hours)
        Case WorkloadTypeLOS                                        'Amb/Dial, Perinatal, A+Emerge
            result = LOSAcuity(flat_workload, total_los_hours)
        Case WorkloadTypeED                                         'Emerge
            result = EDAcuity(flat_workload, class_census)
        Case Else
            MsgBox "Unexpected methodology workload type " & meth.WorkloadType
    End Select

    ShiftAcuity = result
End Function

Public Function MultiClassIDs( _
    start As Date, finish As Date, by_shift As Boolean, _
    unit_group_id As Long, unit_id As Long, shift_name As String _
) As String
    Dim rs As New Recordset, cn As Connection
    Dim sql As String, result As String
    
    Set cn = g_dbutil.NewRemoteConnection()
        
    sql = "SELECT CLASS.CLASSIFICATION_EVENT_ID FROM CLASSIFICATION_EVENT AS CLASS " & _
        " INNER JOIN (SELECT C.ENCOUNTER_ID, C.UNIT_ID, C.STAGE, C.REPORT_DATE"
    
    If by_shift Then
        sql = sql & ", C.SHIFT_DEFINITION_ID "
    End If
    sql = sql & " FROM CLASSIFICATION_EVENT AS C" & _
        " INNER JOIN ENCOUNTER_LOCATION AS L" & _
        "   ON (L.ENCOUNTER_ID = C.ENCOUNTER_ID)" & _
        "   AND (C.EFFECTIVE_DATETIME_IN BETWEEN L.EFFECTIVE_DATETIME_IN AND L.EFFECTIVE_DATETIME_OUT)" & _
        " INNER JOIN UNIT_GROUP_UNIT AS U" & _
        "   ON (U.UNIT_ID = C.UNIT_ID)"
    If by_shift Then
        sql = sql & " INNER JOIN SHIFT_DEFINITION AS S" & _
            " ON (S.SHIFT_DEFINITION_ID = C.SHIFT_DEFINITION_ID)"
    End If
    
    sql = sql & _
    " WHERE C.REPORT_DATE BETWEEN " & g_dbutil.SQL_Date(start) & " AND " & g_dbutil.SQL_Date(finish) & _
    " AND C.REPORT_THIS_WORKLOAD='Y'" & _
    " AND C.IS_DELETED<>'Y'" & _
    " AND U.UNIT_GROUP_ID = " & unit_group_id
    If unit_id > 0 Then
        sql = sql & " AND C.UNIT_ID = " & unit_id
    End If
    If by_shift And Len(shift_name) > 0 Then
        sql = sql & " AND S.NAME = '" & shift_name & "'"
    End If
    
    sql = sql & " GROUP BY C.ENCOUNTER_ID, C.UNIT_ID, C.STAGE, C.REPORT_DATE "
    If by_shift Then
        sql = sql & ", C.SHIFT_DEFINITION_ID "
    End If

    sql = sql & _
    " HAVING COUNT(*) > 1) AS MULTI_CLASS_VIEW " & _
    " ON MULTI_CLASS_VIEW.ENCOUNTER_ID = CLASS.ENCOUNTER_ID " & _
    " AND MULTI_CLASS_VIEW.UNIT_ID = CLASS.UNIT_ID " & _
    " AND (MULTI_CLASS_VIEW.STAGE = CLASS.STAGE    OR (MULTI_CLASS_VIEW.STAGE IS NULL AND CLASS.STAGE IS NULL)) " & _
    " AND MULTI_CLASS_VIEW.REPORT_DATE = CLASS.REPORT_DATE "
    If by_shift Then
        sql = sql & "AND MULTI_CLASS_VIEW.SHIFT_DEFINITION_ID = CLASS.SHIFT_DEFINITION_ID "
    End If
    
    'gather the ids
    rs.Open sql, cn, adOpenStatic, adLockReadOnly
    If (rs.RecordCount > 0) Then
        Do While Not rs.EOF
            result = result & "," & rs(0)
            rs.MoveNext
        Loop
        result = Mid$(result, 2)            'strip leading comma
    Else
        result = "0"
    End If
    
    rs.Close
    cn.Close

    MultiClassIDs = result
End Function

Public Function ClassifiedCensusForRange( _
    ByVal unit_id As Long, ByVal start As Date, _
    ByVal finish As Date, ByVal by_shift As Boolean, shift_name As String _
) As Long
    Dim rs As New Recordset, cn As Connection
    Dim sql As String
    
    Set cn = g_dbutil.NewRemoteConnection()
    
    'number of classified patients on this unit for this shift
    sql = "SELECT COUNT(DISTINCT C.ENCOUNTER_ID) FROM CLASSIFICATION_EVENT AS C" & _
        " INNER JOIN UNIT ON (UNIT.UNIT_ID = C.UNIT_ID)"
    If by_shift Then
        sql = sql & " INNER JOIN SHIFT_DEFINITION S" & _
            " ON (S.SHIFT_DEFINITION_ID = C.SHIFT_DEFINITION_ID)"
    End If
    sql = sql & _
        " WHERE C.REPORT_DATE BETWEEN " & g_dbutil.SQL_Date(start) & " AND " & g_dbutil.SQL_Date(finish)
    If by_shift Then
        sql = sql & " AND S.NAME = '" & shift_name & "'"
    End If
    sql = sql & " AND C.REPORT_THIS_WORKLOAD='Y'" & _
        " AND C.IS_DELETED<>'Y'"
    'include both the unit and its sub-units
    sql = sql & " AND ((UNIT.UNIT_ID=" & unit_id & ") OR (UNIT.PARENT_UNIT_ID=" & unit_id & "))"

    rs.Open sql, cn, adOpenStatic, adLockReadOnly
    ClassifiedCensusForRange = rs(0)
    rs.Close
    cn.Close
End Function

Public Function MultiClassCensusForRange( _
    ByVal unit_group_id As Long, ByVal unit_id As Long, _
    ByVal start As Date, ByVal finish As Date, _
    ByVal by_shift As Boolean, shift_name As String _
) As Long
    
    Dim rs As New Recordset, cn As Connection
    Dim sql As String
    Dim multi_class_ids As String
    
    Set cn = g_dbutil.NewRemoteConnection()
    
    'number of patients with multiple classifications on this
    multi_class_ids = MultiClassIDs(start, finish, by_shift, unit_group_id, unit_id, shift_name)
    
    sql = "SELECT COUNT(DISTINCT ENCOUNTER_ID) FROM CLASSIFICATION_EVENT" & _
    " WHERE CLASSIFICATION_EVENT_ID IN (" & multi_class_ids & ")" & _
    " AND IS_DELETED<>'Y'"

    rs.Open sql, cn, adOpenStatic, adLockReadOnly
    MultiClassCensusForRange = rs(0)
    rs.Close
    cn.Close
End Function

Public Function EditClassCensusForRange( _
    ByVal unit_id As Long, ByVal start As Date, _
    ByVal finish As Date, ByVal by_shift As Boolean, shift_name As String _
) As Long
    Dim rs As New Recordset, cn As Connection
    Dim sql As String
    
    Set cn = g_dbutil.NewRemoteConnection()
    
    'number of patients with edited classification
    sql = " SELECT COUNT(DISTINCT ENCOUNTER_ID) FROM CLASSIFICATION_EVENT AS C"
    If by_shift Then
        sql = sql & " INNER JOIN SHIFT_DEFINITION S" & _
        " ON S.SHIFT_DEFINITION_ID = C.SHIFT_DEFINITION_ID "
    End If
    sql = sql & " WHERE CORRECTED_BY_ID IS NOT NULL " & _
    " AND REPORT_DATE BETWEEN " & g_dbutil.SQL_Date(start) & _
    " AND " & g_dbutil.SQL_Date(finish) & _
    " AND REPORT_THIS_WORKLOAD='Y'" & _
    " AND C.IS_DELETED<>'Y'" & _
    " AND UNIT_ID = " & unit_id
    If by_shift Then
        sql = sql & " AND S.NAME = '" & shift_name & "'"
    End If

    rs.Open sql, cn, adOpenStatic, adLockReadOnly
    EditClassCensusForRange = rs(0)
    rs.Close
    cn.Close
End Function

Public Function MethodologyIsPerinatal(unit_group_id As Long) As Boolean
    Dim rs As New Recordset, cn As Connection
    Dim sql As String
    
    Set cn = g_dbutil.NewRemoteConnection()
    sql = "SELECT MAX(METHODOLOGY_GROUP_LVC) FROM METHODOLOGY AS M" & _
    " INNER JOIN UNIT_GROUP U ON (U.METHODOLOGY_ID = M.METHODOLOGY_ID)" & _
    " AND U.UNIT_GROUP_ID = " & unit_group_id
    
    rs.Open sql, cn, adOpenStatic, adLockReadOnly
    
    If Not rs.EOF Then
        MethodologyIsPerinatal = (g_dbutil.DBToString(rs(0)) = METH_GROUP_PERINATAL)
    End If
    rs.Close
    cn.Close
End Function


'(Job Title Parameters do not save their job skill)
'
Public Function JobSkillForJobTitle(job_title As String) As String
    Static collJobTitleJobSkill As Collection
    Dim rs As New Recordset, cn As Connection
    
    If collJobTitleJobSkill Is Nothing Then
        'Note: PFSCodeDescList does not work here because multiple items (skills) have the same value
        Set collJobTitleJobSkill = New Collection
        Set cn = g_dbutil.NewRemoteConnection()
        Set rs = New Recordset
        rs.Open "SELECT JOB_TITLE_CODE, JOB_SKILL_CODE FROM JOB_TITLE", cn
        Do While Not rs.EOF
            collJobTitleJobSkill.Add CStr(rs(1)), CStr(rs(0))               'item=skill, key=job_code
            rs.MoveNext
        Loop
        rs.Close
        cn.Close
    End If

    On Error Resume Next                    'if not found
    JobSkillForJobTitle = collJobTitleJobSkill.Item(job_title)
End Function


'
' Get the skill level (rn, licensed, other) from the job skill code
'
Public Function SkillLevelForSkillCode(job_skill As String) As SkillLevel
    Static cdlSkillLevelJobSkill As PFSCodeDescList
    Dim result As SkillLevel

    If cdlSkillLevelJobSkill Is Nothing Then
        Set cdlSkillLevelJobSkill = New PFSCodeDescList
                
        'map skill codes to skill levels
        cdlSkillLevelJobSkill.LoadSQL _
            "SELECT LOOKUP_VALUE.LOOKUP_CODE, JOB_SKILL.JOB_SKILL_CODE" & _
            " FROM JOB_SKILL LEFT JOIN LOOKUP_VALUE" & _
            " ON (LOOKUP_VALUE.LOOKUP_DEFINITION_KEY =" & LVC_SKILL_CONSOLIDATION & ")" & _
            " AND (LOOKUP_VALUE.LOOKUP_CODE = JOB_SKILL.SKILL_CONSOLIDATION_LVC)"
    End If

    On Error Resume Next                'if not found
    result = g_dbutil.DBToInteger(cdlSkillLevelJobSkill.Code(job_skill))

    If (result < SKILL_LEVEL_RN) Or (result > SKILL_LEVEL_UNLIC) Then
        result = SKILL_LEVEL_UNUSED
    End If

    SkillLevelForSkillCode = result
End Function
