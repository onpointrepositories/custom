VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "UnitPrintOptions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'Unit print options
'
'This small enum class exists independent of the Unit object so the
'kids can include it.
'

'All options are a power of two so they can be added together in a bitmask.
'These numbers can be changed but they must match what is in LOOKUP_VALUES for list #62
'
Public Enum UnitPrintOptionsEnum
    PrintNone = 0
    printall = -1
    'Unit
    PrintBaseUnitInfo = 1
    PrintTreatmentAreas = 2
    'Unit Parameter
    PrintBaseParamInfo = 4
    PrintLOSOutliers = 8
    PrintFinanceExportCodes = 16
    PrintAgeMISPercents = 32
    PrintPatientProfiles = 64
    PrintProcedureStaffRatios = 128
    PrintADTWorkload = 2097152
    PrintOtherWorkload = 131072
    PrintRoomGroups = 4194304         '** highest in use
    PrintWeeklySchedule = 262144
    'Day Definition
    PrintBaseDayDefInfo = 524288
    PrintShiftDef = 256
    PrintShiftDist = 512
    PrintSkillDist = 1024
    PrintComplexityDist = 2048
    PrintJobTitleParam = 4096
    PrintStaffingParam = 8192
    PrintStaffByCensus = 16384
    PrintStaffMISPercents = 32768
    PrintStaffingRatios = 65536
    PrintMinStaffByHour = 1048576
End Enum
