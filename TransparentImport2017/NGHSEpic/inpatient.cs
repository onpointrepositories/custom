﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;                     // for LINQ; add a reference
using System.Data.SqlClient;                // for SqlConnection
using System.IO;                            // for Directory, Path, Stream
using PfsShared;                            // add a reference to Shared2 project

// ============================================================================
// Inpatient transparent mapping -- GOES HERE --
// NGHS
// ============================================================================
// This processes one patient.  Only one database query is used.  :)
//
// All search functions use exact match for category, description and field name.
// Codes and results can be a single word or a list of words.
// Codes default to exact match; prefix with CODE_CODE_LIKE_PREFIX if desired (single word only)
// Results default to like matching; prefix with RESULT_EXACT_MATCH_PREFIX if desired (single word only)
//
// All searches are case insensitive.
//
// PULL Changes as of 2/12/18....
//  7am 24 hrs   3pm=8hours, 7pm=12 hrs, 3am=8hrs
//  7am 24 hrs   3pm=8hours, 11pm=12 hrs, 3am=8hrs  ALL 24 HRS
//
namespace TransparentMapping
{
    class Inpatient
    {
        private const int MAX_INDS = 110;
        private const int MAX_PROCS = 20;

        private const string CODE_LIKE_PREFIX = "%!";       // use to make codes %like%
        private const string EXACT_MATCH_PREFIX = "&!";     // use to make desc/result exact match
        private const string CHAR_COMMA = "||";             // use to insert literal comma in word list
        private const string NOT_PREFIX = "!!";             // use to insert literal comma in word list
        private const string EXACT_SQL_PREFIX = "!SQL!";     // use to give sql constraint as given

        private struct indicator_data {
            public bool is_checked;
            public int radio_group;
        }

        private struct proc_data {
            public int procedure_number;
            public DateTime start;
            public DateTime finish;
        }

        // These are database CHART_ITEMs for this patient
        private CHART_ITEM[] _chart_items_since_admission;
        private CHART_ITEM[] _chart_items_since_unit_arrival;
        private CHART_ITEM[] _chart_items_during_pull_period;
        private CHART_ITEM[] _chart_items_pull_period_plus;
        // These are database PROCEDURE_EVENTs for this patient
        private PROCEDURE_EVENT[] _procedure_events;

        private indicator_data[] _inds;
        private List<proc_data> _procs;
        private List<proc_data> _outcomes;

        private const int DEFAULT_BUCKET_SIZE = 20;       //min

        private PatientInfo _pat;
        private int _bucket_size = DEFAULT_BUCKET_SIZE;
        private bool is_default;
        private string txarea;


        private enum SearchDepth {
            SearchDefault,
            SearchPullRange,            //search within the current pull     -- default
            SearchSinceArrival,         //search since arrival to the unit
            SearchSinceAdmission,        //search everything since admission to the hospital
            SearchPullPlus
        }

        private enum CountMode {
            CountAll,
            CountFirst                  //stop after one is found
        }

        private enum GetValueMode {
            GetTotal,
            GetMax,
            GetLast
        }

        enum Frequencies {
            QNONE,
            Q4H,
            Q2H,
            Q1H,
            Q30M
        }

        private struct fmapRow {
            public double los_high;                       //the LOS being testing
            public int[] freq;                           //the count required for each Q value
        }
        private List<fmapRow> _freq_map;                    //1,2,4,8,12,24 hours

        private struct gBucket
        {
            public int bucket;
            public string code;
            public DateTime evdt;
        }


        //
        // This is the main entry point
        //
        public void ProcessPatient(PatientInfo pat)
        {
            _pat = pat;
            bool use_default = false;

            InitIndicators();           // sets is_default
            InitProcs();
            if (!is_default)
            {
                LoadFreqTable();
                LoadPatientChart();
                Check_1_2_3_4();
                Check_5();
                Check_6_7();
                Check_8();
                Check_9();
                Check_10_11();
                Check_12_13();
                Check_14();
                Check_15_16_17_18();
                Check_19();
                Check_20();
                Check_21_22();
                Check_23();
                Check_24();
                CheckUserDefined();
                Check_TxArea();
                AtLeastOneADL();
            }

            HighestIndicatorInEachGroupWins();

            //if (!is_default)
            //{
            //    CheckProcs();
            //    CheckOutcomes();
            //}

            if (Program.g_no_output) return;
            if (_pat.default_ptype > DeterminePtypeOfIndicators())
            { // if the default pt type is higher than the pt type of this class
                // then if there is a default classification in the past 16 hrs
                // then make these indicators the default indicators.
                use_default = ExistDefaultInPast16hrs();
                if (use_default)
                {
                    Program.VerboseAudit("Default indicators will be used");
                }
            }
            OutputClass(use_default);
            //OutputProcs();
            //OutputOutcomes();
        }


        private void InitIndicators()
        {
            // Make a new (empty) indicator array
            _inds = new indicator_data[MAX_INDS + 1];           // This 1 based so add one
            is_default = false;
            txarea = "";
            if (_pat.los_hours <= 4.0)
            {
                is_default = true;
                Program.VerboseAudit("Patient was here 4 hrs or less. Will receive default indicators " + _pat.default_inds_str);
                foreach (var ind in _pat.default_inds)
                {
                    if (ind <= _inds.GetUpperBound(0))
                    {
                        _inds[ind].is_checked = true;
                    }
                }
            }


                // get indicator radio groups from the database
                // ** (This database access can be replaced once we have a C# methodolgy cache)
                var db = PFSDBUtility.NewPfsDataContext();
            var query = from idef in db.INDICATOR_DEFINITIONs
                        where (idef.METHODOLOGY_ID == _pat.meth_id)
                        select idef;
            foreach (var idef in query) {
                if (idef.INDICATOR_NUMBER <= _inds.GetUpperBound(0)) {
                    // (convert nulls to zero)
                    _inds[idef.INDICATOR_NUMBER].radio_group = PFSDBUtility.DBToInt(idef.RADIO_GROUP);
                }
            }
        }

        private void InitProcs()
        {
            _procs = new List<proc_data>();
            _outcomes = new List<proc_data>();
        }

        private fmapRow LoadFreqTableRow(double los_high, string values)
        {
            fmapRow fmrow;

            fmrow.los_high = los_high;
            fmrow.freq = new int[Enum.GetNames(typeof(Frequencies)).Length];

            var arr = values.Split(',');
            for (int i = 0; i <= fmrow.freq.GetUpperBound(0); i++)
            {
                fmrow.freq[i] = arr[i].ToInteger();
            }
            return fmrow;
        }

        // These are the number of times you need to do something during a certain time period (LOS)
        // in order for it to be considered one of the frequencies listed.
        private void LoadFreqTable()
        {
            _freq_map = new List<fmapRow>();
            //                              LOS,  None Q4h Q2h Q1h Q30m
            _freq_map.Add(LoadFreqTableRow(1, "    0,  0,  0,  1,  2"));
            _freq_map.Add(LoadFreqTableRow(2, "    0,  0,  1,  2,  4"));
            _freq_map.Add(LoadFreqTableRow(4, "    0,  1,  2,  4,  8"));
            _freq_map.Add(LoadFreqTableRow(6, "    0,  2,  3,  6,  9"));
            _freq_map.Add(LoadFreqTableRow(8, "    0,  2,  4,  8, 12"));
            _freq_map.Add(LoadFreqTableRow(12, "   0,  3,  6, 12, 18"));
            _freq_map.Add(LoadFreqTableRow(16, "   0,  4,  8, 16, 24"));
            _freq_map.Add(LoadFreqTableRow(24, "   0,  6, 12, 24, 36"));
            //New freq table 2/5/14
            //q4	q2	q1	q30     q30
            //            Non-ICU	ICU & SD
            // 4	8	15	29	    36
            // 3	5	9	17	    24
            // 2	4	7	13	    19
            // 2	3	5	10	    13

            //to read this table: if you have at least 6 asessmenets in a 12-hour period, you get Q1h
            //(LOS=12, column Q1h has a count of 6)
        }

        private Frequencies FreqForCount(double los_hours, int count)
        {
            foreach (var fmrow in _freq_map) {
                if (los_hours <= fmrow.los_high) {
                    // Option: pro-rate the count upward if the LOS is less than the row we are interested in.
                    //         This will bump the count to what it might have been at the full LOS.
                    // Note: truncate the result; rounding inflates the value too much.
                    int prorated_count = (int)((fmrow.los_high / los_hours) * count);

                    // foreach goes low to high; go from high to low instead
                    for (int j = (int)Frequencies.Q30M; (j > (int)Frequencies.QNONE); j--) { //search right to left
                        if (prorated_count >= fmrow.freq[j]) {
                            return (Frequencies)j;
                        }
                    } // next j
                }
            }

            return Frequencies.QNONE;
        }

        //
        // Load patient chart from the database (one DB access)
        // Load everything since admission
        //
        private void LoadPatientChart()
        {
            // Get the entire patient chart (all units and dates for this patient)
            var db = PFSDBUtility.NewPfsDataContext();
            var query = from item in db.CHART_ITEMs
                        where (item.ENCOUNTER_ID == _pat.encounter_id)
                        select item;
            if (_pat.recreate)                  // ignore docs added after the pull time
            {                                   // re-create a historic mapping
                query = query.Where(e => e.TIMESTAMP <= _pat.pull_finish);
            }
            // Save the result
            _chart_items_since_admission = query.ToArray();

            // Convert all results to lower case for case insensitive comparisons
            foreach (var item in _chart_items_since_admission) {
                if (item.CATEGORY != null) item.CATEGORY = item.CATEGORY.ToLower();
                if (item.CODE != null) item.CODE = item.CODE.ToLower();
                if (item.DESCRIPTION != null) item.DESCRIPTION = item.DESCRIPTION.ToLower();
                if (item.FIELD_NAME != null) item.FIELD_NAME = item.FIELD_NAME.ToLower();
                if (item.RESULT != null) item.RESULT = item.RESULT.ToLower();
            }

            // Prepare two more versions of the chart
            var query2 = from item in _chart_items_since_admission
                         where (item.EVENT_DATETIME >= _pat.unit_arrival) && (item.EVENT_DATETIME <= _pat.pull_finish)
                         select item;
            _chart_items_since_unit_arrival = query2.ToArray();

            query2 = from item in _chart_items_since_admission
                     where (item.EVENT_DATETIME >= _pat.pull_start) && (item.EVENT_DATETIME <= _pat.pull_finish)
                     select item;
            _chart_items_during_pull_period = query2.ToArray();
            query2 = from item in _chart_items_since_admission
                     where (item.EVENT_DATETIME >= _pat.pull_start) && (item.EVENT_DATETIME <= _pat.pull_finish.AddHours(4))
                     select item;
            _chart_items_pull_period_plus = query2.ToArray();
        }

        // Delay looking for patient procedures until they are wanted
        private void LoadPatientProceduresIfNeeded()
        {
            if (_procedure_events != null) return;

            var db = PFSDBUtility.NewPfsDataContext();
            var query = from proc in db.PROCEDURE_EVENTs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                        select proc;
            _procedure_events = query.ToArray();
        }



        // Started a new chart item query (of a certain depth)
        // Default is the chart during the pull range.  Options for unit arrival and admission to hospital.
        private IEnumerable<CHART_ITEM> StartNewQuery()
        {
                return StartNewQuery(SearchDepth.SearchDefault);
        }
        private IEnumerable<CHART_ITEM> StartNewQuery(SearchDepth search_depth)
        {

            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    return (from item in _chart_items_during_pull_period select item);
                case SearchDepth.SearchSinceArrival:
                    return (from item in _chart_items_since_unit_arrival select item);
                case SearchDepth.SearchSinceAdmission:
                    return (from item in _chart_items_since_admission select item);
                case SearchDepth.SearchPullPlus:
                    return (from item in _chart_items_pull_period_plus select item);
            }
            return null;
        }

        // Is this search term a word or a list of words?
        private bool ValueIsAList(string s)
        {
            return (s != null) && s.Contains(',');                //is this a comma-separated list?
        }

        // Break apart a wordlist and prepare its members
        private string[] SplitOnCommaAndPrepareElements(string s)
        {
            // If a word list has a space after each comma, we will get leading blanks in each term
            var arr = s.Split(',');
            for (int i = 0; i <= arr.GetUpperBound(0); i++) {
                arr[i] = arr[i].Trim();                         // get rid of leading blanks
                arr[i] = arr[i].Replace(CHAR_COMMA, ",");       // insert a literal comma into search word
                arr[i] = arr[i].ToLower();                      // convert for case-insensitive comparisons
            }
            return arr;
        }

        // Add a basic chart item filter to a LINQ query
        //
        // cat = exact match
        // code_list = exact match; optional "like" match
        // desc_list = "like" match; optional exact match
        // field = exact match
        // result_list = "like" match; optional exact match
        //
        // These are comma-separated lists.  
        // Search for a literal comma by using CHAR_COMMA in the string.
        private IEnumerable<CHART_ITEM> AndItemFilter(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list)
        {
            // Converted all values to lower case for case-insensitive comparisons.
            if (!String.IsNullOrEmpty(cat))
            {
                cat = cat.ToLower();
                query = query.Where(e => e.CATEGORY == cat);
            }

            if (!String.IsNullOrEmpty(code_list))
            {
                code_list = code_list.ToLower();
                if (code_list.Left(2) == CODE_LIKE_PREFIX)
                {            // override for "like" match?
                    query = query.Where(e => e.CODE.Contains(code_list.Substring(2)));
                }
                else if (code_list.Left(2) == EXACT_MATCH_PREFIX)
                {
                    query = query.Where(e => e.CODE == code_list.Substring(2));
                }
                else if (ValueIsAList(code_list))
                {
                    query = AndCodeInList(query, code_list);            // find one of the words
                }
                else
                {
                    query = query.Where(e => e.CODE == code_list);      // find this word
                }
            }

            if (!String.IsNullOrEmpty(desc_list))
            {
                desc_list = desc_list.ToLower();
                if (desc_list.Left(2) == EXACT_MATCH_PREFIX)
                {          // exact match override?
                    query = query.Where(e => e.DESCRIPTION == desc_list.Substring(2));
                }
                else if (ValueIsAList(desc_list))
                {
                    query = AndDescriptionInList(query, desc_list);
                }
                else
                {
                    desc_list = desc_list.Replace(CHAR_COMMA, ",");     // look for literal comma requests
                    query = query.Where(e => e.DESCRIPTION.Contains(desc_list));
                }
            }

            if (!String.IsNullOrEmpty(field))
            {
                field = field.ToLower();
                query = query.Where(e => e.FIELD_NAME == field);
            }

            if (!String.IsNullOrEmpty(result_list))
            {
                query = query.Where(e => e.RESULT != null);
                result_list = result_list.ToLower();                        // make all results case insensitve
                if (result_list.Left(2) == EXACT_MATCH_PREFIX)
                {     // exact match override?
                    query = query.Where(e => e.RESULT == result_list.Substring(2));
                }
                else if (ValueIsAList(result_list))
                {
                    query = AndResultInList(query, result_list);
                }
                else
                {
                    result_list = result_list.Replace(CHAR_COMMA, ",");     // look for literal comma requests
                    query = query.Where(e => e.RESULT.Contains(result_list));
                }
            }
            return query;
        }

        // Look for a code that is one of the words in the list.
        private IEnumerable<CHART_ITEM> AndCodeInList(IEnumerable<CHART_ITEM> query, string code_list)
        {
            if (String.IsNullOrEmpty(code_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(code_list);
//            return query.Where(e => arr.Any(x => x.Contains(e.CODE))); // "like" match
            return query.Where(e => e.CODE.ContainsAny(arr)); // "like" match
        }

        // Look for a description that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndDescriptionInList(IEnumerable<CHART_ITEM> query, string desc_list)
        {
            if (String.IsNullOrEmpty(desc_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(desc_list);
            //return query.Where(e => arr.Any(x => x.Contains(e.DESCRIPTION))); // "like" match
            return query.Where(e => e.DESCRIPTION.ContainsAny(arr)); // "like" match
        }

        // Look for a result that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultInList(IEnumerable<CHART_ITEM> query, string result_list)
        {
            if (String.IsNullOrEmpty(result_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(result_list);
//            return query.Where(e => arr.Any(x => x.Contains(e.RESULT))); // "like" match
            return query.Where(e => e.RESULT.ContainsAny(arr)); // "like" match
        }

        // Look for a result that does not contain one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultNotInList(IEnumerable<CHART_ITEM> query, string result_list)
        {
            if (String.IsNullOrEmpty(result_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(result_list);
//            return query.Where(e => !arr.Any(x => x.Contains(e.RESULT))); // "like" match
            return query.Where(e => !e.RESULT.ContainsAny(arr)); // "like" match
        }

        private string DescribeSearchDepth(SearchDepth search_depth)
        {
            string result = "";

            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    //result = "in pull range"
                    result = "";                         //be quiet since this is default
                    break;
                case SearchDepth.SearchSinceArrival:
                    result = "since arrival to unit";
                    break;
                case SearchDepth.SearchSinceAdmission:
                    result = "since admission";
                    break;
                case SearchDepth.SearchPullPlus:
                    result = "in LOS range plus 4 hours after";
                    break;
            }

            return result;
        }

        // Describe a query that has already been run
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Make a simple query and describe the results
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            var arr = query.ToArray();
            return Describe(arr, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private string LookingFor(string so_far, string what, string op, string value)
        {
            string result;
            if (String.IsNullOrEmpty(value)) return so_far;

            if (so_far == "")
                result = "looking for " + what;
            else
                result = so_far + " and " + what;

            if (value.Left(2) == EXACT_MATCH_PREFIX)
                result += "='" + value.Substring(2) + "'";
            else if (value.Left(2) == CODE_LIKE_PREFIX)
                result += " contains " + "'" + value.Substring(2) + "'";
            else if (ValueIsAList(value))
                result += " in '" + value + "'";
            else
                result += op + "'" + value + "'";

            return result;
        }
        private string Describe(CHART_ITEM[] arr, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string result = "";
            result = LookingFor(result, "cat", "=", cat);
            result = LookingFor(result, "code", "=", code_list);
            result = LookingFor(result, "desc", " contains ", desc_list);
            result = LookingFor(result, "field", "=", field);
            result = LookingFor(result, "result", " contains ", result_list);

            if (arr.Count() == 0) {
                result += "; not found " + DescribeSearchDepth(search_depth);
            } else {
                var e = arr[0];
                result = "FOUND: " + result + "; found";
                // We might have searched for a pattern or word list in several fields - show what was found
                if (e.CATEGORY != null) result += " cat='" + e.CATEGORY + "'";
                if (e.CODE != null) result += " code='" + e.CODE + "'";
                if (e.DESCRIPTION != null) result += " desc='" + e.DESCRIPTION + "'";
                if (e.FIELD_NAME != null) result += " field='" + e.FIELD_NAME + "'";
                if (e.RESULT != null) result += " result='" + e.RESULT + "'";
                // Are there more results?  Just say how many; we aren't going to list them.
                if (arr.Count() == 2) {
                    result += " (1 more result)";
                } else if (arr.Count() > 2) {
                    result += " (" + (arr.Count() - 1) + " more results)";
                }
            }

            return result;
        }


        //Set an indicator for this reason (low level)
        private void SetInd(int inum, string reason)
        {
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Set Ind #" + inum + ": " + reason);          //already set - repeat for verbose only
            } else {
                _inds[inum].is_checked = true;
                Program.Audit("Set Ind #" + inum + ": " + reason);
            }
        }

        //Clear an indicator for this reason (low level)
        private void ClrInd(int inum, string reason)
        {
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Clr Ind #" + inum + ": " + reason);          //already clear - repeat verbose only
            } else {
                _inds[inum].is_checked = false;
                Program.Audit("Clr Ind #" + inum + ": " + reason);
            }
        }

        // Count how many items meet the conditions
        // All items are listed
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountItems(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            int count = query.Count();

            // always return what was found
            found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
            // echo the result?
            if (trace) Program.VerboseAudit(found_what);

            return count;
        }


        // Count how many items meet the conditions
        // This has the option to list every entry or give a total
        private int CountResultInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what = "";
            string s = "";

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);

            var arr = SplitOnCommaAndPrepareElements(result_list);
//Walker Schlundt
//Set Ind #9: found '' in cat='' code='301870' field='' result='oriented x4' 

            foreach (var item in query) {
                // Figure out which of the search words were found
                for (int i = 0; i <= arr.GetUpperBound(0); i++)
                {
                    if (item.RESULT.Contains(arr[i]))
                    {
                        count++;
                        s = arr[i];
                        found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' code='" + item.CODE + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "'";
                        //if (trace) Program.VerboseAudit(found_what);
                        Program.VerboseAudit(found_what);
                    }
                }
                if (count_mode != CountMode.CountAll) break;        //stop counting

            }

            if (count > 0) {
                //We already printed what was found; maybe add how many?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        // Count how many items meet the conditions
        // This has the option to list every entry or give a total
        private int CountResultInListEXCEPTList(string cat, string code_list, string desc_list, string field, string result_list, string negresult_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what = "";

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            query = AndResultNotInList(query, negresult_list);

            var arr = SplitOnCommaAndPrepareElements(result_list);

            foreach (var item in query)
            {
                // Figure out which of the search words were found
                var s = item.RESULT.ContainsWhich(arr);
                found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' code='" + item.CODE + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "'";
                if (trace) Program.VerboseAudit(found_what);
                count++;
                if (count_mode != CountMode.CountAll) break;        //stop counting
            }

            if (count > 0)
            {
                //We already printed what was found; maybe add how many?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            }
            else
            {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        // Count how many items contain ALL results in list
        private int CountResultInListAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what = "";

            var arr = SplitOnCommaAndPrepareElements(result_list);

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            for (int i = 0; i <= arr.GetUpperBound(0); i++)
            {
                if (arr[i].StartsWith(NOT_PREFIX))
                {
                    query = query.Where(e => !e.RESULT.Contains(arr[i].Substring(2)));
                }
                else
                {
                    query = query.Where(e => e.RESULT.Contains(arr[i]));
                }
            }

            count = query.Count();

            if (count > 0)
            {
                found_what = "found item with all results in '" + result_list + "' without those prefixed by !!";
                if (trace) Program.VerboseAudit(found_what);
            }
            else
            {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (ValueIsAList(result_list))
            {
                return CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what);
            } else {
                return CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what);
            }
        }

        // Is there an item with this result?
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContains(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (ValueIsAList(result_list))
            {
                return (CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            } else {
                return (CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
            }
        }

        // Result contains all in list
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (ValueIsAList(result_list))
            {
                return (CountResultInListAll(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            }
            else
            {
                found_what = "Not valid for non-list results.";
                return false;
            }
        }
        // Count how many items meet the conditions, with none of the results in result_list.
        // This has the option to say how many were found rather than list all.
        private int CountResultNotInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0, rec_count = 0;
            bool found_one;

            found_what = "";
            var arr = SplitOnCommaAndPrepareElements(result_list);         // also deals with CHAR_COMMA

            //Do not filter by result here; we need all results
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            foreach (var item in query) {
                rec_count++;
                //Look for each search word in the result
                found_one = false;

                foreach (string s in arr) {
                    if (String.Equals(item.RESULT, s)) {
                        found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "' -- ignore this";
                        Program.VerboseAudit(found_what);
                        found_one = true;
                        break;
                    }
                }

                if (!found_one) {
                    //none of the words were found - good!
                    found_what = Describe(cat, code_list, desc_list, field, "", search_depth) + " -- does not contain '" + result_list + "'";
                    Program.VerboseAudit(found_what);
                    count++;
                    if (count_mode != CountMode.CountAll) break;
                }
            }

            if (count > 0) {
                //We already printed what was found; maybe show the total?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else if (rec_count > 0) {
                //We already printed what we ignored
            } else {
                //Print what did not exist
                found_what = Describe(cat, code_list, desc_list, field, "", search_depth);      //not found
                Program.VerboseAudit(found_what);
            }

            return count;
        }

        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, string found_what)
        {
            // Search in the pull range with trace on
            return CountResultDoesNotContain(cat, code_list, desc_list, field, result_list, out found_what, SearchDepth.SearchDefault, true);
        }
        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, out string found_what, SearchDepth search_depth, bool trace)
        {
            return CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0;
        }

        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            return (CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0);
        }

        // Set the indicator if the conditions are met
        //
        private bool SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);

        }
        private bool SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            bool b = false;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return true;

            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what)) {
                SetInd(inum, found_what);                          //echo here - found
                b = true;
            } else {
                Program.VerboseAudit(found_what);                  //and here - not found
            }
            return b;
        }
        // Special contains all
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContainsAll(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return;

            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                          //echo here - found
            }
            else
            {
                Program.VerboseAudit(found_what);                  //and here - not found
            }
        }
        // Set the indicator if the result does not contain any of the words in result_list
        //
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultDoesNotContain(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return;

            //Turn trace off for ResultDoesNotContain() and echo what was set below with SetInd
            if (ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                           //echo here - not found
            } else {
                Program.VerboseAudit(found_what);                   //and here - found
            }
        }

        // Clear the indicator if the result contains one of the words in the result_list
        //
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already clear
            if (!_inds[inum].is_checked) return;

            //Turn trace off for ResultContains() and echo what was set below with SetInd
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what)) {
                ClrInd(inum, found_what);                           //echo here - found
            } else {
                Program.VerboseAudit(found_what);                   //and here - not found
            }
        }


        //These slightly smaller functions are meant for places where you aren't really looking for a chart result
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            return (CountResultContains(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
        }

        // Variations with optional parameters (can switch to optional parameters in VS 2010)
        //
        private void SetIndIfFound(int inum, string cat)
        {
            SetIndIfResultContains(inum, cat, "", "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list)
        {
            SetIndIfResultContains(inum, cat, code_list, "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private void ClrIndIfFound(int inum, string cat)
        {
            ClrIndIfResultContains(inum, cat, "", "", "", "", SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }


        // Get the max/total value from a result (usually in the middle of the text)
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetIntValue(get_mode, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            int value;
            int result = 0;
            bool found_one = false;

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);

            //Look for a number in the result

            foreach (var item in query) {
                // RESULT might be null so convert to empty string so Split won't blow up
                var arr = PFSDBUtility.DBToString(item.RESULT).Split(' ');
                foreach (string s in arr) {
                    //Look for 1st character numeric; IsNumeric("60min") will fail so look at 1st char only
                    if (s.Left(1).IsNumeric()) {
                        value = (int)s.Val();                           //Use Val; ToInteger will error on "60min"
                        switch (get_mode) {
                            case GetValueMode.GetMax:
                                result = Math.Max(value, result);       //max
                                break;
                            case GetValueMode.GetTotal:
                                result += value;                        //total
                                break;
                            case GetValueMode.GetLast:
                                result = value;                         //last
                                break;
                        }

                        // print what we are searching for (the first time)
                        if (!found_one) {
                            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
                        }
                        found_one = true;
                        // print each value found
                        Program.VerboseAudit("  found numeric value " + result);
                        //Keep going in case there are more
                    }
                }
            }

            if (!found_one) {
                //show what was not found
                Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
            }

            return result;
        }

        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetMaxValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return GetIntValue(GetValueMode.GetMax, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetTotalValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return GetIntValue(GetValueMode.GetTotal, cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Get a result; returns true if found with return_result
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result)
        {
            return GetResult(cat, code_list, desc_list, field, out return_result, SearchDepth.SearchDefault);
        }
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0) {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
            } else {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }
        private bool GetResultForInspection(string cat, string code_list, string desc_list, string field, string result, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result);

            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
            }
            else
            {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }

        private void GetResultforOther(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "Other");
            return_result = "";
            foreach (var item in query)
            {
                if (!String.Equals(item.RESULT, "Other"))
                {
                    return_result = item.RESULT;
                    //Program.VerboseAudit(found_what);
                    break;
                }

            }
            return;
        }

        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, out string return_result, out DateTime return_evdt)
        {
            return GetResultAndEVDT(cat, code_list, desc_list, field, out return_result, out return_evdt, SearchDepth.SearchDefault);
        }
        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, out string return_result, out DateTime return_evdt, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
                return_evdt = PFSDBUtility.DBToDateTime(query.First().EVENT_DATETIME);
            }
            else
            {
                return_result = "";
                return_evdt = DateTime.MinValue;
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }


        //======================================================================================
        // Mapping starts here
        //======================================================================================

        private void Check_1_2_3_4()
        {
            string found_what;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("1. ADL Self");
            Program.VerboseAudit("2. ADL Assist");
            Program.VerboseAudit("3. ADL Extended");
            Program.VerboseAudit("4. ADL Complete");
            Program.VerboseAudit("---------------");

            // 4. COMPLETE CARE

            if (_pat.age < 4.0)
            {
                SetInd(4, "Age < 4 years");
            }
            if (_inds[4].is_checked) return;

            string reslist = "";
            
            //if (Exists("", "305560", "", "", "Up ad lib"))
            //{
            //    SetIndIfResultContains(1, "", "305570", "", "", "Patient independent");
            //    SetIndIfResultContains(1, "", "7060350", "", "", "Independent");
            //    SetIndIfResultContains(1, "", "700380", "", "", "Able to feed self");
            //}

            if (Exists("", "305560", "", "", "Up ad lib"))
            {
                SetIndIfResultContains(1, "", "305570", "", "", "Patient independent");
                SetIndIfResultContains(1, "", "7060350", "", "", "Independent");
                SetIndIfResultContains(1, "", "7060350", "", "", "Minimal assist");
                SetIndIfResultContains(1, "", "700380", "", "", "Able to feed self");
            }
            if (Exists("", "7060350", "", "", "Independent") ||
                Exists("", "7060350", "", "", "Minimal assist"))
                SetIndIfResultContains(1, "", "342030", "", "", "Bath,Shower");

            reslist = "Stand by assist,One person assist";
            SetIndIfResultContains(2, "", "305570", "", "", reslist);
            reslist = "Maximum assist,Moderate assist";
            SetIndIfResultContains(2, "", "7060350", "", "", reslist);
            //reslist = "Dangle on side of bed,Pivot to chair,Up to bedside commode,sit to stand";
            //SetIndIfResultContains(2, "", "305560", "", "", reslist);

            reslist = "Able to feed self with adaptive devices,Needs assist,Needs set up,Observation";
            reslist = "Needs assist,Needs set up,Observation";
            SetIndIfResultContains(2, "", "700380", "", "", reslist);

            //SetIndIfResultContains(2, "", "8190", "", "", "");
            //SetIndIfResultContains(2, "", "8192", "", "", "");
            //SetIndIfResultContains(2, "", "8189", "", "", "");
            //SetIndIfResultContains(2, "", "7387", "", "", "");

            //int ct = CountResultContains("", "305560", "", "", "Up to bedside commode");
            //ct += CountResultContains("", "61", "", "", "");
            //ct += CountResultContains("", "305280", "", "", "Yes");
            //ct += CountResultContains("", "3040103156", "", "", "Yes");
            ////ct += CountResultContains("", "3040102830", "", "", "");
            //ct += CountResultContains("", "3040102724", "", "", "Incontinence");

//ANY 3 of 4:  toilet, feeding, bathing, activity
            reslist = "Stand by assist,One person assist,Two person assist,Complete assist,Person Assist";
            int ctA = CountResultContains("", "305570", "", "", reslist);
            reslist = "Maximum assist,Moderate assist";
            ctA += CountResultContains("", "7060350", "", "", reslist);
            ctA += CountResultInListEXCEPTList("", "7060350", "", "", "Dependent", "Independent", SearchDepth.SearchDefault, CountMode.CountAll, true, out found_what);
            reslist = "Dangle on side of bed,Pivot to chair,Sit to stand,Up to bedside commode";
            ctA += CountResultContains("", "305560", "", "", reslist);

            reslist = "NPO,Needs assist,Needs set up,Observation,Total assist,1:1 Feeding initiated by nurse,1:1 Nurse feeding";
            int ctB = CountResultContains("", "700380", "", "", reslist);
            ctB += CountResultContains("", "700360", "", "", "NPO");

            reslist = "Bathed,Bath - Medicated,Shower,Toileting offered";
            int ctC = CountResultContains("", "342030", "", "", reslist);

            int ctD = CountResultContains("", "3040102724", "", "", "Incontinence");
            string codelist = "305280,305280,3040103156,3040103156";
            ctD += CountResultContains("", codelist, "", "", "Yes");
            int ct = ((ctA > 0) ? 1 : 0) + ((ctB > 0) ? 1 : 0) + ((ctC > 0) ? 1 : 0) + ((ctD > 0) ? 1 : 0);
            if (ct >= 3)
                SetInd(3, "Count of Extended Assist categories (need 3/4) =" + ct);
            
            bool catA = Exists("", "3040108949", "", "", "Comatose,Pharmaceutically paralyzed,Sedated");
            catA |= Exists("", "315170", "", "", "Yes");
            //catA |= Exists("", "3040102639", "", "", "Intubated");
            if (catA)
                SetInd(4, "Category A item found.");

            bool catB = Exists("", "305570", "", "", "Complete assist");
            catB |= Exists("", "342030", "", "", "Bathed,Bath - medicated");
            catB |= Exists("", "7060350", "", "", "Maximum assist"); 
            //catB |= Exists("", "7060350", "", "", "Dependent"); //independent
            ct = CountResultInListEXCEPTList("", "7060350", "", "", "Dependent", "Independent", SearchDepth.SearchDefault, CountMode.CountAll, true, out found_what);
            if (ct > 0) catB=true;
            catB |= Exists("", "305280", "", "", "Yes");
            catB |= Exists("", "3040103156", "", "", "Yes");
            //catB |= Exists("", "3040102830", "", "", "");
            catB |= Exists("", "3040102724", "", "", "Incontinence");

            bool catC = Exists("", "7659", "", "", "Aspiration,Patient condition,Per MD order");
            catC |= Exists("", "700380", "", "", "Observation,Total assist,1:1 Feeding initiated by nurse,1:1 Nurse feeding");
            //catC |= Exists("", "3040102639", "", "", "Intubated");
            if (catB && catC)
                SetInd(4, "Category B and C items found.");
            else if (catB || catC)
                SetInd(3, "Category B or Category C item found.");

            //if (!_inds[1].is_checked && !_inds[3].is_checked && !_inds[4].is_checked)
            //{
            //    SetInd(2, "Defaulting to ADL Partial");
            //}


        }


        private void Check_5()
        {
            string codelist;
            string found_what;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("5. ADL Rehab");
            Program.VerboseAudit("---------------");

            if (IsRehab()) SetInd(5, "Rehab unit");

            SetIndIfResultContains(5, "", "3040102790", "", "", "Bowel training");

            //SetIndIfResultContains(5, "", "8212", "", "", "LLE,RLE,Applied,Removed");
            int ct = CountResultInListEXCEPTList("", "8212", "", "", "LLE,RLE,Applied", "Removed,Discontinue", SearchDepth.SearchDefault, CountMode.CountAll, true, out found_what);
            if (ct > 0) SetInd(5, found_what);

            codelist = "7060510,7060540,7060570,7060600";
            //SetIndIfResultContains(5, "", "7060540", "", "", "Inplace,Started,Restarted,Off,Discontinued,Other");
            ct = CountResultInListEXCEPTList("", codelist, "", "", "Inplace,Started,Restarted,Off", "Discontinue", SearchDepth.SearchDefault, CountMode.CountAll, true, out found_what);
            if (ct > 0) SetInd(5, found_what);

            if (!Exists("", codelist, "", "", "Discontinue"))
            {
                SetIndIfResultContains(5, "", "703820", "", "", "Yes,No,Applied,Removed");
            }

            SetIndIfResultContains(5, "", "30403000591", "", "", "ROM");

        }


        private void Check_6_7()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("6. ADL 2-3 Caregivers");
            Program.VerboseAudit("7. ADL 4 or more Caregivers");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(6, "", "305570", "", "", "Two person assist");
            SetIndIfResultContains(6, "", "305570", "", "", "3");
            SetIndIfResultContains(7, "", "305570", "", "", "4,5,6");

        }

        private void Check_8()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("8. Communication");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(8, "", "301890", "", "", "Expressive aphasia,Receptive aphasia,Global aphasia,Incomprehensible,Nods/gestures appropriately,Intubated,Trached,Uses written communication,Language barrier",SearchDepth.SearchSinceAdmission);
            SetIndIfResultContains(8, "", "2108", "", "", "Deaf", SearchDepth.SearchSinceAdmission);
            SetIndIfResultContains(8, "", "2109", "", "", "Deaf", SearchDepth.SearchSinceAdmission);
            SetIndIfResultContains(8, "", "2115", "", "", "Trach,Voice amplifier", SearchDepth.SearchSinceAdmission);

        }

        private void Check_9()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("9. Cognitive Support");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(9, "", "301870", "", "", "Disoriented X4,Disoriented to place,Disoriented to time,Disoriented to situation,Disoriented to person");
            SetIndIfResultContains(9, "", "301880", "", "", "Long term memory loss,Poor attention/concentration,Short term memory loss,Unable to follow commands",SearchDepth.SearchSinceAdmission);
            //SetIndIfResultContains(9, "", "30403000591", "", "", "Orientation");
            SetIndIfResultContains(9, "", "3040109606", "", "", "");
            SetIndIfResultContains(9, "", "3040109607", "", "", "");
            SetIndIfResultContains(9, "", "3040109608", "", "", "");
            SetIndIfResultContains(9, "", "3040109611", "", "", "");
            //SetIndIfResultContains(9, "", "3040104509", "", "", "");
            //SetIndIfResultContains(9, "", "3040104650", "", "", "");

        }

        private void Check_10_11()
        {
            string reslist, found_what;
            //int ct;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("10. Behavior/Emotional Management");
            Program.VerboseAudit("11. Behavior/Emotional Mgmt - q 1 Hour");
            Program.VerboseAudit("---------------");

            bool catA = Exists("", "301880", "", "", "Impulsive,Poor judgement,Poor safety awareness,Poor attention/concentration");
            catA |= Exists("", "304200", "", "", "Agitated,Anxious,Angry,Affect inconsistent with mood,Aggressive physically,Aggressive verbally,Cursing,Delusions,Incongruent,Flat affect,Hallucinations,Irritable,Labile,Sad,Non-compliant,Not interactive,Restless,Tearful,Uncooperative,Verbal abuse,Wandering,Withdrawn");
            catA |= Exists("", "304210", "", "", "Anxious,Affect inconsistent with mood,Aggressive physically,Aggressive verbally,Flat affect,Non-compliant,Non-supportive,Tearful,Uncooperative,Verbal");
            catA |= Exists("", "304220", "", "", "Anxious,Affect inconsistent with mood,Aggressive physically,Aggressive verbally,Flat affect,Non-compliant,Non-supportive,Tearful,Uncooperative,Verbal");
            bool catB = Exists("", "7599", "", "", "Family conference,Limits set,Encouraged Expression,Extended Support,Holding,Medication,Reassured,Referred to clergy,Visitor Restriction,Requested visit from Hosp Chaplain");
            if (catA && catB)
                SetInd(10, "Behavioral items found in Group A and Group B");

            //if 7599 with 'done every 1 hour' then trigger 11

            //MUST contain any from Group A AND 
            //    Done every 1 hour AND 
            //    any other from Group B OTHERWISE Behavior / Emotional Management
            int ct = 0;
            ct = CountResultContains("", "301880", "", "", "Impulsive,Poor judgement,Poor safety awareness,Poor attention/concentration");
            ct += CountResultContains("", "304200", "", "", "Agitated,Anxious,Angry,Affect inconsistent with mood,Aggressive physically,Aggressive verbally,Cursing,Delusions,Incongruent,Flat affect,Hallucinations,Irritable,Labile,Sad,Non-compliant,Not interactive,Restless,Tearful,Uncooperative,Verbal abuse,Wandering,Withdrawn");
            ct += CountResultContains("", "304210", "", "", "Anxious,Affect inconsistent with mood,Aggressive physically,Aggressive verbally,Flat affect,Non-compliant,Non-supportive,Tearful,Uncooperative,Verbal");
            ct += CountResultContains("", "304220", "", "", "Anxious,Affect inconsistent with mood,Aggressive physically,Aggressive verbally,Flat affect,Non-compliant,Non-supportive,Tearful,Uncooperative,Verbal");
            if (Exists("", "7599", "", "", "Family conference,Limits set,Encouraged Expression,Extended Support,Holding,Medication,Reassured,Referred to clergy,Visitor Restriction,Requested visit from Hosp Chaplain"))
            {
                if (IsQ1Hour(ct))
                    SetInd(11, "Found item in Group B with Item count is q1Hr from Group A=" + ct);
            }


        }

        // (this isn't really a Q1h count -- it is just a count) Make this a q1hr count
        private int ReturnQ1HrCount(string code_list, string result_list)
        {
            var buckets = new List<gBucket>();
            SetBucketSize(60);
            AddBuckets(buckets, "", code_list, "", "", result_list);
            return CountBuckets(buckets);
        }

        private bool IsQ1Hour(int count)
        {
            return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q1H);
        }
        private bool IsQ2Hour(int count)
        {
            return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q2H);
        }

        // Use this if counting safety buckets
        //private void CheckSafety(int count, string desc)
        //{
        //    if (_inds[13].is_checked) return;             //skip if highest already checked
        //    if (count == 0) return;
        //    switch (FreqForCount(_pat.los_hours, count))
        //    {
        //    case Frequencies.Q30M:
        //        SetInd(13, desc + " q30min");
        //        break;
        //    case Frequencies.Q1H:
        //    case Frequencies.Q2H:
        //        SetInd(12, desc + " q2h");
        //        break;
        //    default:
        //        Program.VerboseAudit(desc + ": " + count + " in " + Math.Round(_pat.los_hours) + " hours is not enough");
        //        break;
        //    }
        //}

        private void Check_12_13()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("12. Safety Management - q 2 Hours");
            Program.VerboseAudit("13. Safety Management - q 30 Minutes");
            Program.VerboseAudit("---------------");

            int res = GetMaxValue("", "305110", "", "", "");
            if (res >= 60)
            {
                SetInd(12, "Fall score=" + res);
                SetInd(69, "Fall score=" + res);
            }
            //SetIndIfResultContains(12, "", "7060390", "", "", "Fall Risk");
            //SetIndIfResultContains(12, "", "30403000308", "", "", "Post skull flap protective helment on");
            SetIndIfResultContains(12, "", "300114,300113,300020,300018", "", "", "");

            SetIndIfResultContains(13, "", "706390", "", "", "Suicide,Elopement");
            SetIndIfResultContains(13, "", "506570", "", "", "Yes");
            SetIndIfResultContains(13, "", "7599", "", "", "Sitter at bedside");

            //int ct = 0;
            //ct = CountResultContains("", "", "", "", "Agitated/restless,Verbally abusive,Tearful,Hallucinating,Delusional,Subdued,Asleep,Confused,Other");
            //if (IsQ2Hour(ct))
            //    SetInd(12, "Restraints count= " + ct);

        }

        private void Check_14()
        {
            string found_what;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("14. Isolation");
            Program.VerboseAudit("---------------");

            //SetIndIfResultContains(14, "", "7060020", "", "", "Isolation");
            //SetIndIfResultContains(14, "", "7060390", "", "", "Neutropenic,Radiation");
            SetIndIfResultContains(14, "", "3040102696", "", "", "Good handwashing,Housekeeping cleans room first each day,Masks in room for patient use,No enemas,No fresh flowers,No IM injections,No urinary catheters,No visitors allowed,Positive pressure airflow room,Private room (Obtain MD order),Provide equipment that stays in room,Other");
            
            int ct = CountResultInListEXCEPTList("", "30403000628", "", "", "Initiated,Maintained", "Discontinue", SearchDepth.SearchDefault, CountMode.CountAll, true, out found_what);
            if (ct > 0) SetInd(14, found_what);

        }

        private void CheckAssessment(int count, string desc)
        {
            //if (_inds[18].is_checked) return;          //skip if highest already checked
            if (count == 0) return;                    //skip if none

            // This should work the same as the original code:
            switch (FreqForCount(_pat.los_hours, count))
            {
                case Frequencies.Q30M:
                    SetInd(18, desc);
                    break;
                case Frequencies.Q1H:
                    SetInd(17, desc);
                    break;
                case Frequencies.Q2H:
                    SetInd(16, desc);
                    break;
                case Frequencies.Q4H:
                    SetInd(15, desc);
                    break;
                default:
                    break;
            }

        }

        private void ShowBuckets(List<gBucket> buckets)
        {
            string s = "";
            foreach (var e in buckets)
            {
                s = "(" + e.bucket + "," + e.code + "," + e.evdt.ToString() + ")";
                Program.VerboseAudit("Assessmt item:=" + s);
            }
        }


        private void Check_15_16_17_18()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("15. Assessment q4h");
            Program.VerboseAudit("16. Assessment q2h");
            Program.VerboseAudit("17. Assessment q1h");
            Program.VerboseAudit("18. Assessment q30min");
            Program.VerboseAudit("---------------");

            CountAssessments(30);               // always do q30 buckets--but use different freq tables for icu vs non-icu.

        }

        private bool IsRehab()
        {
            switch (_pat.unit_name)
            {
                case "137 - Rehab":
                    return true;
                default:
                    return false;
            }
        }


        //private bool IsICU()
        //{
        //    switch (_pat.unit_name)
        //    {
        //        case "ICU":
        //        case "3BR":
        //        case "CTIC":
        //        case "NIC":
        //        case "PCU":
        //        case "TICU":
        //        case "CCB":
        //        case "RICU":
        //        case "CCUS":
        //            return true;
        //        default:
        //            return false;
        //    }
        //}

        //private bool IsTele()
        //{
        //    switch (_pat.unit_name)
        //    {
        //        case "4EST":
        //        case "2NW":
        //        case "3AE":
        //        case "5MEH":
        //        case "5NW":
        //        case "6MEH":
        //        case "6NW":
        //        case "7MEH":
        //        case "B4W":
        //        case "B5S":
        //        case "B5W":
        //        case "2NOB":
        //        case "CPLX":
        //        case "TELMS":
        //        case "MAN4":
        //            return true;
        //        default:
        //            return false;
        //    }
        //}


        private void CountAssessments(int bucket_size)
        {
            int ct;
            string codelist;
            string reslist;
            List<gBucket> buckets;

            SetBucketSize(bucket_size);

            //buckets = new List<gBucket>();
            //AddBuckets(buckets, "", "7812497", "", "", "Endotracheal,Nasal,Nasal Tracheal Suction,Oral Suction,Tracheal Suction");
            //codelist = "117646,117590,117589,";
            //codelist += "117531,17343783,305994900,117638,141615,142506593,12499648,";
            //codelist += "117509,117498,117496,117584,305994913,305994918,3059991317,";
            //codelist += "117544,292718281,305995220,130920968,350850553,";
            //codelist += "305056309,305056294,305056394,633685506,305056509,305056484,";
            //codelist += "481751481,481752177,304208931,316381365,305056531,316346132,";
            //codelist += "305056541,117533,117643,304208961,7812336,18802339,305994957,";
            //codelist += "7812341,7812342,7812343,7812365,18802341,7812366,12499648,";
            //codelist += "305995290,7812367,305995300,7812371,7812372,7812373,7812374,7812375,";
            //codelist += "7812376,7812377,7812464,7812465,7812466,7812467,7812468,7812469,";
            //codelist += "7812470,7812493,18802342,7812494,7812495,7812496,";
            //codelist += "7812506,7812522,18802343,7812531,18802344,7812540,18802345,";
            //codelist += "758359245,325564484,302915761,302915771,592033203,592033208,";
            //codelist += "592033213,345372930,412500531,302916056,302916076,758629716,";
            //codelist += "758608948,758642066,758669207,303692843,383992195,303692914,";
            //codelist += "383992207,303693055,383992201,772531865,772522579,303693186,";
            //codelist += "303693191,303693196,303693201,303693206,303693211,303693216,";
            //codelist += "303693221,7812529,305056504,406392859,406392865,303693351,";
            //codelist += "303693371,303693346";
            //AddBuckets(buckets, "", codelist, "", "");
            //AddBuckets(buckets, "", "MED", "", "Heparin,Bivalrudin,Argatroban,Sodium Bicarb");
            //ct = CountBuckets(buckets);
            //CheckAssessment(ct, "All assessments count=" + ct);
            //ShowBuckets(buckets);

            buckets = new List<gBucket>();
            //AddDependentBuckets(buckets, "117531,17343783", "117590,117589");
            codelist = EXACT_MATCH_PREFIX + "5";
            AddBuckets(buckets, "", codelist, "", "");
            codelist = EXACT_MATCH_PREFIX + "6";
            AddBuckets(buckets, "", codelist, "", "");
            codelist = EXACT_MATCH_PREFIX + "8";
            AddBuckets(buckets, "", codelist, "", "");
            codelist = EXACT_MATCH_PREFIX + "9";
            AddBuckets(buckets, "", codelist, "", "");
            codelist = EXACT_MATCH_PREFIX + "14106";
            AddBuckets(buckets, "", codelist, "", "");
            codelist = EXACT_MATCH_PREFIX + "14107";
            AddBuckets(buckets, "", codelist, "", "");
            codelist = EXACT_MATCH_PREFIX + "301260";
            AddBuckets(buckets, "", codelist, "", "");
            codelist = EXACT_MATCH_PREFIX + "301280";
            AddBuckets(buckets, "", codelist, "", "");
            codelist += "450010,30402999013,3040104659,3040108953,30403000161,7090010,7060180,3040104228";
            AddBuckets(buckets, "", codelist, "", "");
            codelist = EXACT_MATCH_PREFIX + "7429";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Vitals=" + ct);
            ShowBuckets(buckets);

            //SetIndIfResultContains(15, "", "301860", "", "", "New agitation,Alert,Responds to Voice,Responds to Pain,Unresponsive,Awake,Quiet,Drowsy,Easily aroused,Eyes do not open to stimulation,Pharmacologically paralyzed,Sedated,Sleeping,Other");
            //SetIndIfResultContains(15, "", "302380", "", "", "WDL=Within Defined Limits,X=Exceptions to WDL");
            //SetIndIfResultContains(15, "", "303010", "", "", "WDL=Within Defined Limits,X=Exceptions to WDL");
            //SetIndIfResultContains(15, "", "304360", "", "", "WDL=Within Defined Limits,X=Exceptions to WDL");
            //SetIndIfResultContains(15, "", "304370", "", "", "WDL=Within Defined Limits,X=Exceptions to WDL");
            //SetIndIfResultContains(15, "", "304420", "", "", "WDL=Within Defined Limits,X=Exceptions to WDL");
            //SetIndIfResultContains(15, "", "304430", "", "", "WDL=Within Defined Limits,X=Exceptions to WDL");
            //SetIndIfResultContains(15, "", "400630", "", "", "WDL=Within Defined Limits,X=Exceptions to WDL");
            //SetIndIfResultContains(15, "", "2105", "", "", "WDL=Within Defined Limits,X=Exceptions to WDL");


            buckets = new List<gBucket>();
            codelist = EXACT_MATCH_PREFIX + "301320";
            AddBuckets(buckets, "", codelist, "", "");
            codelist = EXACT_MATCH_PREFIX + "2240";
            AddBuckets(buckets, "", codelist, "", "");
            codelist = EXACT_MATCH_PREFIX + "8";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Cardiac=" + ct);
            ShowBuckets(buckets);


            buckets = new List<gBucket>();
            codelist = "3040104228,3040104659,3040100967,301130,301180,7060860,7060910";
            codelist += ",7061000,1170,7061120,401610,401620,401630,401640,3040103243,3040103244,3040103245";
            codelist += ",3040101151,3040101157,3040101160";
            AddBuckets(buckets, "", codelist, "", "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Pain=" + ct);
            ShowBuckets(buckets);
            //if (_inds[18].is_checked) return;

            //buckets = new List<gBucket>();
            ////codelist = "12499648,117509,7812365,18802341,7812366,12499648,305995290,7812367,";
            ////codelist += "305995300,7812371,7812372,7812373,7812374,7812375,7812376,7812377,";
            ////codelist += "7812464,7812465,7812466,7812467,7812468,7812469,7812470,303031350,";
            ////codelist += "325754541,303031329,532319071,345591779,303693186,303693191,303693196,";
            ////codelist += "303693201,303693206,303693211,303693216,303693221";
            ////AddBuckets(buckets, "", codelist, "", "");
            //AddDependentBuckets(buckets, "305994900", "117638");
            //codelist = "7812493,18802342,7812494,7812495,7812496,142506593";
            //AddBuckets(buckets, "", codelist, "", "");
            //reslist = "Endotracheal,Nasal,Oral Suction,Tracheal Suction";
            //AddBuckets(buckets, "", "7812497", "", "", reslist);
            //ct = CountBuckets(buckets);
            //CheckAssessment(ct, "Respiratory=" + ct);
            //ShowBuckets(buckets);
            ////if (_inds[18].is_checked) return;

            buckets = new List<gBucket>();
            AddBuckets(buckets, "", "3040104125", "", "");
            codelist = EXACT_MATCH_PREFIX + "2239";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Neurological=" + ct);
            ShowBuckets(buckets);
            if (_inds[18].is_checked) return;

            //buckets = new List<gBucket>();
            ////codelist = "7812540,18802345,304015183,345603431";
            //codelist = "7812540,18802345,303692843,383992195,303692914,383992207";
            //codelist += ",303693055,383992201,772531865,772522579";
            //AddBuckets(buckets, "", codelist, "", "");
            //ct = CountBuckets(buckets);
            //CheckAssessment(ct, "Wound=" + ct);
            //ShowBuckets(buckets);
            ////if (_inds[18].is_checked) return;

            //buckets = new List<gBucket>();
            //codelist = "130920968,350850553";
            //AddBuckets(buckets, "", codelist, "", "");
            ////reslist = "Heparin,Bivalrudin,Argatroban,Sodium Bicarb";
            ////AddBuckets(buckets, "", "MED", reslist, "");
            //ct = CountBuckets(buckets);
            //CheckAssessment(ct, "Medications=" + ct);
            //ShowBuckets(buckets);
            ////if (_inds[18].is_checked) return;

            //buckets = new List<gBucket>();
            //codelist = "305056394,633685506,305056509,481751481,481752177,304208931,316381365,";
            //codelist += "305056531,316346132,117533,117643,304208961,7812529,305056504,406392859,406392865";
            //AddBuckets(buckets, "", codelist, "", "");
            //reslist = "APD,Biliary,Blake,CAPD,Drain,Constavac,Davol,G-J Tube,G-Tube,";
            //reslist += "Hemovac,JP,J-Tube,PEG tube,Pelvic Drain,Pendrose Drain,";
            //reslist += "Pericardial,T-Tube,Other";
            //AddBuckets(buckets, "", "305056269", "", "", reslist);
            //AddBuckets(buckets, "", "305056309,305056294,305056484,305056541", "", "", "");
            //ct = CountBuckets(buckets);
            //CheckAssessment(ct, "Fluid Mgt=" + ct);
            //ShowBuckets(buckets);
            ////if (_inds[18].is_checked) return;



            //buckets = new List<gBucket>();
            //codelist = "7812506,7812522,18802343,7812531,18802344";
            //AddBuckets(buckets, "", codelist, "", "");
            //ct = CountBuckets(buckets);
            //CheckAssessment(ct, "GI/GU=" + ct);
            //ShowBuckets(buckets);
            ////if (_inds[18].is_checked) return;
        }

        //=====================================================================
        // Group like assessments into buckets of time
        // Each bucket counts as one assessment in the frequency count

        private void AddDependentBuckets(List<gBucket> bucket_list, string codelist1, string codelist2)
        {
            // get the chart items for the assessments
            var query1 = StartNewQuery();
            query1 = AndItemFilter(query1, "", codelist1, "", "", "");
            var query2 = StartNewQuery();
            query2 = AndItemFilter(query2, "", codelist2, "", "", "");

            // figure out what buckets the events belong to
            var query1a = from item in query1
                          select new
                          {
                              bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                              code = item.CODE,
                              evdt = item.EVENT_DATETIME
                          };
            var query2a = from item in query2
                          select new
                          {
                              bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                              code = item.CODE,
                              evdt = item.EVENT_DATETIME
                          };
            string s = "BucketList1 for " + codelist1 + ": ";
            foreach (var item in query1a)
            {
                s += item.bucket + ",";
            }
            Program.VerboseAudit(s);

            s = "BucketList2 for " + codelist2 + ": ";
            foreach (var item in query2a)
            {
                s += item.bucket + ",";
            }
            Program.VerboseAudit(s);
            // Add to the list IFF items in both lists occur in same bucket
            foreach (var item1 in query1a)
            {
                foreach (var item2 in query2a)
                {
                    if (item1.bucket == item2.bucket)
                    {
                        var b = new gBucket();
                        b.bucket = item1.bucket;
                        b.code = item1.code;
                        b.evdt = item1.evdt;
                        bucket_list.Add(b);
                    }
                }
            }

        }


        private void SetBucketSize(int minutes)
        {
            _bucket_size = minutes;
        }

        //Add to the list of bucket numbers (redundant buckets are fine)
        private void AddBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field)
        {
            AddBuckets(bucket_list, cat, code_list, desc, field, "");
        }
        private void AddBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field, string result_list)
        {
            // Assign (virtual) bucket numbers to each assessment.  
            // Every x minutes after that is a new bucket.  Bucket zero = pull_start time.

            // get the chart items for the assessments
            var query = StartNewQuery();
            query = AndItemFilter(query, cat, code_list, desc, field, result_list);

            // This step is needed for those who want to count the # within a bucket, like Shands #24.
            // This will have no effect on those that count buckets.
            //var query2 = (from item in query select new { item.EVENT_DATETIME }).Distinct();
            var query2 = (from item in query select new { item.EVENT_DATETIME, item.CODE });

            // figure out what buckets the events belong to
            //var query3 = from item in query2
            //             select new {
            //                bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size)
            //             };
            //// Add to the list
            //foreach (var item in query3) {
            //    bucket_list.Add(item.bucket);
            //}

            var query3 = from item in query2
                         select new
                         {
                             bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                             code = item.CODE,
                             evdt = item.EVENT_DATETIME
                         };
            // Add to the list
            foreach (var item in query3)
            {
                var b = new gBucket();
                b.bucket = item.bucket;
                b.code = item.code;
                b.evdt = item.evdt;
                bucket_list.Add(b);
            }

            // This is just for auditing: Say what we are looking for and how many were found
            if (!String.IsNullOrEmpty(result_list))
            {
                // print each word and if it was found or not
                int i = CountResultContains(cat, code_list, desc, field, result_list);
            }
            else
            {
                // print how many were found
                Program.VerboseAudit(Describe(cat, code_list, desc, field, ""));
            }
        }

        private int CountBuckets(List<gBucket> bucket_list)       // bucket list: (ha ha)
        {
            //int result = bucket_list.Distinct().Count();
            //if (result > 0) Program.VerboseAudit(result + " unique");
            //return result;
            int x = -99;
            int result = 0;
            //int result = bucket_list.Distinct().Count();
            var query = from b in bucket_list
                        orderby b.bucket ascending
                        select b;
            foreach (var b in query)
            {
                if (x != b.bucket)
                {
                    result++;
                    x = b.bucket;
                }
            }
            if (result > 0) Program.VerboseAudit(result + " unique");
            return result;
        }

        private void Check_19()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("19. Vascular Access Site Mgt q1 Hour");
            Program.VerboseAudit("---------------");

            //int ct = 0;
            //ct = CountResultContains("", "7096770", "", "", "Cap changed,Connections checked and tightened,Line pulled back,Tubing changed,Zeroed and calibrated,Leveled,Transducer changed,Other");
            //if (IsQ1Hour(ct))
            //    SetInd(19, "Line care intaosseous= " + ct);

            SetIndIfResultContains(19, "", "3040100091,3040100092,3040100093", "", "", "");
            string reslist = "Connections checked and tightened,Line pulled back,Tubing changed,Zeroed and calibrated,Leveled";
            SetIndIfResultContains(19, "", "7096770", "", "", reslist);

            if (_pat.age < 5.0)
                SetIndIfResultContains(19, "", "304840", "", "", "");


        }

        private void Check_20()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("20. Medication Activity >= 20 minutes");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(20, "", "3040104331", "", "", "");
            SetIndIfResultContains(20, "", "3040104332", "", "", "");
            SetIndIfResultContains(20, "", "3040104333", "", "", "");
            SetIndIfResultContains(20, "", "3040104334", "", "", "");
            SetIndIfResultContains(20, "", "3040104335", "", "", "");
            SetIndIfResultContains(20, "", "1600005076", "", "", "");


        }

        private void Check_21_22()
        {
            string codelist;
            string found_what="";
            int ct;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("21. Wound/Injury Mgmt");
            Program.VerboseAudit("22. Wound/Injury Mgmt >= 30 Minutes");
            Program.VerboseAudit("---------------");

            if (_pat.dob.AddDays(10) >= DateTime.Now) SetInd(21, "Infant is 10 days old or less. DOB=" + _pat.dob.ToString());

            SetIndIfResultContains(21, "", "30403000601", "", "", "Circumcision care");
            //SetIndIfResultContains(21, "", "3041216601", "", "", "Betadine Rinse,Hydrogel dressing");
            SetIndIfResultContains(21, "", "3041216501", "", "", "Bleeding,Blisters,Cracked,Abrasion");
            SetIndIfResultContains(21, "", "3041215901", "", "", "Bleeding,Blisters,Cracked,Abrasion");
            SetIndIfResultContains(21, "", "3041215801", "", "", "Lesions");
            SetIndIfResultContains(21, "", "3041215701", "", "", "Lesions");
            SetIndIfResultContains(21, "", "2108", "", "", "Abrasions,Foreign body,Lacerations,Trauma/injury");
            SetIndIfResultContains(21, "", "2109", "", "", "Abrasions,Foreign body,Lacerations,Trauma/injury");
            SetIndIfResultContains(21, "", "2110", "", "", "Abrasion,Eccymosis,Epistaxis,Laceration,Packing in place,Nasal Balloon in place,Swelling,Ulceration");
            SetIndIfResultContains(21, "", "3070", "", "", "Atrial wire,Ventricular wire,Grounded wire,X1,X2,X3,X4,Pulse generator on,Pulse generator off,Protected,Other");
            SetIndIfResultContains(21, "", "7484", "", "", "Pin Care Done");
            SetIndIfResultContains(21, "", "7487", "", "", "Drainage,Ace wrap intact,Dressing applied,Dressing changed,Dressing Dry and Intact,Pin care");
            SetIndIfResultContains(21, "", "302840", "", "", "Epicardial,Transcutaneous,Transvenous,Other");
            SetIndIfResultContains(21, "", "304140", "", "", "Bright red,Red");
            SetIndIfResultContains(21, "", "304150", "", "", "Blood clots");
            ct = CountResultInListEXCEPTList("", "304900", "", "", "Trauma/injury", "No Trauma/injury", SearchDepth.SearchDefault, CountMode.CountAll, true, out found_what);
            if (ct > 0) SetInd(21, found_what);
            ct = CountResultInListEXCEPTList("", "304940", "", "", "Trauma/injury", "No Trauma/injury", SearchDepth.SearchDefault, CountMode.CountAll, true, out found_what);
            if (ct > 0) SetInd(21, found_what);
            //SetIndIfResultContains(21, "", "304900", "", "", "Trauma/injury"); //no trauma/injury
            //SetIndIfResultContains(21, "", "304940", "", "", "Trauma/injury");
            SetIndIfResultContains(21, "", "304190", "", "", "Injury/trauma,Lesions,Bleeding,Thrombosed,Excoriation");
            SetIndIfResultContains(21, "", "306470", "", "", "Abrasion,Burn,Laceration,Tear,Weeping");
            SetIndIfResultContains(21, "", "340270", "", "", "Red,Red streaks");
            SetIndIfResultContains(21, "", "340300", "", "", "Black,Blood clots,Blood-tinged,Bloody,Coffee ground");
            SetIndIfResultContains(21, "", "400610", "", "", "Bloody,Tarry");
            //SetIndIfResultContains(21, "", "451130", "", "", "Bloody,Pink tinged");
            SetIndIfResultContains(21, "", "160010476", "", "", "Lacerations,Abrasions,Bruising,Hematoma");
            SetIndIfResultContains(21, "", "1600100472", "", "", "Unusual bleeding,Purulent drainage,Clots,Bloody");
            SetIndIfResultContains(21, "", "1600100474", "", "", "Unusual bleeding,Clots,Bloody");
            //SetIndIfResultContains(21, "", "3040101202", "", "", "Rectal bleeding");
            //SetIndIfResultContains(21, "", "3040102714", "", "", "Colostomy,Ileostomy,Urostomy");
            //SetIndIfResultContains(21, "", "3040102713", "", "", "Urine ,Stool,Ostomy ,Drain/feeding tubes,Dialysis ");
            //SetIndIfResultContains(21, "", "3040102773", "", "", "Ear care,Eye care,Eye patch applied,Ice,Nasal care,Nasal packing,Nasal packing removed,Suction,Other");
            
            string reslist = "Cast,Splint,Immobilizer,Neck Brace,Left arm,Right arm,Left hand,Right hand,Finger,Left leg,Right leg,Left foot,Right foot,Body Cast";
            SetIndIfResultContains(21, "", "7401", "", "", reslist);

            SetIndIfResultContains(21, "", "7484", "", "", "Pin care done");

            reslist = "Abrasion,Burn,Excoriation,Fissure,Laceration,Weeping";
            SetIndIfResultContains(21, "", "306470", "", "", "Pin care done");

            SetIndIfResultContains(21, "", "3040102773", "", "", "Nasal packing");

            SetIndIfResultContains(21, "", "3040102719", "", "", "Tracheostomy");
            SetIndIfResultContains(21, "", "304170", "", "", "Bleeding,Foreign object,Injury/trauma,Lesion,Menstruating,Prolapsed uterus,Rash,Swelling");

            SetIndIfResultContains(21, "", "2235", "", "", "WNL,Clean,Dressing Dry and Intact,Dry,Edematous,Leaking,Painful,Reddened,Bleeding,Bloody drainage,Serous drainage,Serosanguineous drainage,Purulent discharge,Odor present,Purse string sutures,Steri-strips,Other (Comment)");
            SetIndIfResultContains(21, "", "8093", "", "", "Initial eval,No change,Healing,Healed,Deteriorating,Other (comment)");
            SetIndIfResultContains(21, "", "303750", "", "", "Adipose tissue,Blister,Bulging,Clean,Covered w/dressing"+CHAR_COMMA+" not assessed,Dry,Intact-skin,Bleeding,Black eschar,Brown,Dark edges,Drainage,Dusky,Epithelialization,Excoriated,Exposed bone,Exposed hardware,Exposed muscle,Exposed tendon,Fragile,Granulation - none,Granulation - red,Granulation - hyper,Green,Incision - approximated,Incision - unapproximated,Light purple,Nonblanchable erythema,Painful,Pale tissue,Pink tissue,Red tissue,Sunken,Tan slough,Tract,Tunneling,Tunnel - new,Tunnels - connecting,Undermining,White tissue,Yellow Slough,UTA,Unable to assess,Other");
            //SetIndIfResultContains(21, "", "304840", "", "", "Clean,Dry-no bleeding/hematoma,Intact,Bleeding,Draining,Edematous,Extravasated,Frank bleeding,Hematoma-firm,Hematoma-soft,Leaking,Oozing,Pink,Positional,Pressure line monitoring,Redness,s/p sheath,Soft,Tender,Other (Comment)");
            reslist = "Abnormal,Blisters,Clean,Dry,Intact,Black,Blanchable erythema,Bleeding,Brown,Burgundy";
            reslist += ",Dark edges,Denuded,Ecchymotic,Edematous,Epithelialization,Erythematous,Excoriated";
            reslist += ",Flaky,Fragile,Hyperpigmented,Induration,Maceration,Non - blanchable erythema,Painful,Pale";
            reslist += ",Pink,Purple,Rash,Reddened,Tan,Undermining,White,Hemosiderin staining,Unable to assess,Other";
            SetIndIfResultContains(21, "", "7061190", "", "", reslist);
            reslist = "Applied,New,Changed,Checked,Cleaned,Culture obtained,Debrided,Expressed drainage";
            reslist += ",Heat applied,Hydrotherapy,Ice applied,Irrigated,Reinforced,Removed";
            reslist += ",Silver Nitrate,Site Care,Other Interventions";
            SetIndIfResultContains(21, "", "303820", "", "", reslist);

            SetIndIfResultContains(21, "", "7085420", "", "", "Bleeding,Blistered,Ecchymotic,Edema/swelling,Healing,Leaking at site,Reddened,Scabbed,Other (Comment)");
            SetIndIfResultContains(21, "", "1420100005", "", "", "Black foam,White foam,Gauze,Non-adherent,Other (Comment)");
            SetIndIfResultContains(21, "", "3040103946", "", "", "Clean,Dry,Intact,Bleeding,Draining,Ecchymotic,Edematous,Red,Tender,Other (Comment)");
            SetIndIfResultContains(21, "", "3040103949", "", "", "Clean,Dry,Intact,Bleeding,Draining,Ecchymotic,Edematous,Red,Tender,Other (Comment)");
            SetIndIfResultContains(21, "", "3040103952", "", "", "Clean,Dry,Intact,Bleeding,Draining,Ecchymotic,Edematous,Red,Tender,Other (Comment)");
            SetIndIfResultContains(21, "", "3040103955", "", "", "Clean,Dry,Intact,Bleeding,Draining,Ecchymotic,Edematous,Red,Tender,Other (Comment)");
            SetIndIfResultContains(21, "", "30403000167", "", "", "Antifungal cream,Barrier cream,Barrier ring,Barrier - no sting,Crusted w/ostomy powder,Crusted w/antifungal powder,Hydrocolloid,Marathon skin protectant,Skin prep,Other (comment)");
            SetIndIfResultContains(21, "", "336410", "", "", "");

        }

        //private void CheckExtensiveWound()
        //{
        //    DateTime evdt = DateTime.MinValue;
        //    DateTime st_time = DateTime.MinValue;
        //    DateTime en_time = DateTime.MinValue;
        //    var query = StartNewQuery(SearchDepth.SearchDefault);
        //    query = AndCodeInList(query, "1375032519");
        //    query = AndResultInList(query, "extensive wound");
        //    query = query.OrderBy(e => e.EVENT_DATETIME);
        //    if (query.Count() == 0) return;
        //    foreach (var ci in query)
        //    {
        //        if (!_inds[22].is_checked)
        //        {
        //            evdt = ci.EVENT_DATETIME;
        //            st_time = GetResultTime("1375032547", evdt, 2);
        //            en_time = GetResultTime("1375032561", evdt, 2);
        //            if ((st_time != DateTime.MinValue) &&
        //                (en_time != DateTime.MinValue) && (st_time <= en_time))
        //            {
        //                if (st_time.AddMinutes(30) <= en_time)
        //                    SetInd(22, "Extensive Wound Management>=30min start:" + st_time.ToString() + " end:" + en_time.ToString());
        //            }
        //        }
        //    }
        //}

        // Use this if you can total the education time
        //private void CheckEducation(int total)
        //{
        //    if (_inds[23].is_checked) return;             //skip if already checked

        //    if (total >= 60) {
        //        SetInd(23, "education >= 60 min");
        //    }
        //}

        int EducMins(string timecode, string nursecode)
        {
            string res;
            int val = 0;
            if (ResultContains("", nursecode, "", "", "Nursing"))
            {
                if (GetResult("", timecode, "", "", out res))
                {
                    if (res.Left(1).IsNumeric())
                    {
                        val += (int)res.Val();
                    }
                }
            }
            return val;
        }

        private void Check_23()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("23. Healthcare Mgmt Education >= 1 Hour");
            Program.VerboseAudit("---------------");
            string res;
            int val = 0;
            int value = 0;

            //value += EducMins("Angina/MI Length of Instruct", "Angina/MI Discipline");
            //value += EducMins("Anticoagulation Length of Instr", "Anticoagulation Discipline Educa");
            //value += EducMins("Bone Marrow Length of Instruct", "Bone Marrow Discipline");
            //value += EducMins("Cardiac Rehab Len of Instruct", "Cardiac Rehab Discipline");
            //value += EducMins("Central Line Len of Instruct", "Central Line Discipline");
            //value += EducMins("Chemo/Onc Length of Instruct", "Chemo/Oncology Discipline");
            //value += EducMins("Diabetes Length of Instruct", "Diabetes Discipline");
            //value += EducMins("Dialysis Length of Instruct", "Dialysis Discipline");
            //value += EducMins("Generic Med Length of Instruct", "General Medical Discipline");
            //value += EducMins("GenericSurgery Len of Instruct", "Surgery Discipline");
            //value += EducMins("Heart Failure Len of Instruct", "Heart Failure Discipline");
            //value += EducMins("Heart/PTCA Length of Instruct", "Heart Cath/PTCA Discipline");
            //value += EducMins("Infection Ctrl Len of Instruct", "Infection Control Discipline");
            ////value += EducMins("Medication Length of Instruct");
            //value += EducMins("Open Heart Surgery Len of Instr", "Open Heart Surgery Discipline Ed");
            //value += EducMins("Nutrition Length of Instruct", "Nutrition Discipline");
            //value += EducMins("Ostomy/Stoma Length of Instruct", "Ostomy/Stoma Discipline");
            //value += EducMins("Rad Therapy/TBI Len of Instruct", "Radiation Tx/TBI Discipline");
            //value += EducMins("Respiratory Tx Len of Instruct", "Resp Treatments Discipline");
            //value += EducMins("Stroke Length of Instruct", "Stroke Discipline");
            //value += EducMins("Therapy Srvcs Len of Instruct", "Physical Therapy Discipline");
            //value += EducMins("Tracheostomy Len of Instructi", "Tracheostomy Discipline");
            //value += EducMins("Trans Post-Op/DC Len of Instruct", "Trans Post-Op/DC Discipline");
            //value += EducMins("Urinary Cath Len of Instruct", "Urinary Cath Discipline");
            //value += EducMins("VAD/MCAD Length of Instruct", "VAD/MCAD Discipline");
            //value += EducMins("Wound Length of Instruct", "Wound Instruction Discipline");
            //value += EducMins("End of Life Date/Time of Instruc", "End of Life Discipline");


            //if (value >= 60) SetInd(23, "Total combined instruction time=" + value + " mins");

        }

        private void Check_24()
        {
            string s;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("24. 1 to 1 Physiological Interv. >= 2 Hours");
            Program.VerboseAudit("---------------");

            if (SetIndIfResultContains(24, "", "8100020", "", "", "Initiated,Continuous")) SetInd(79, "CRRT");
            //SetIndIfResultContains(24, "", "1375032575", "", "", "");
            //SetIndIfResultContains(24, "", "1122231967", "", "", "");
            //SetIndIfResultContains(24, "", "1375032575", "", "", "");
            SetIndIfResultContains(24, "", EXACT_MATCH_PREFIX+"6382", "", "", "Impella 2.5,Impella 5.0,Impella CP,Impella RP");

            // pulse + respirations + bp   represents complete set of vitals
            // need 9 q2hr

            //8   Heart Rate    (PULSE)

            //9   Resp

            //14106   Systolic BP for 3rd Party Systems
            //14107   Diastolic BP for 3rd Party Systems

            //301260  Arterial Line BP
            //301280  Arterial Line BP 2

            //6   Temp
            //450010  Core(Body) Temperature

            //5   BP
            CheckCountIn2Hrs(EXACT_MATCH_PREFIX + "5", "BP");
            //CheckCountIn2Hrs(EXACT_MATCH_PREFIX + "8", "Heart Rate");
            //CheckCountIn2Hrs(EXACT_MATCH_PREFIX + "9", "Resp");
            //CheckCountIn2Hrs("14106,14107", "BP for 3rd party system");
            //CheckCountIn2Hrs("301260,301280", "Arterial Line BP");
            //CheckCountIn2Hrs(EXACT_MATCH_PREFIX + "6,450010", "Temp");

        }

        private void CheckCountIn2Hrs(string codelist, string desc)
        {
            string s;
            SetBucketSize(60);                                  // set one hour buckets
            //===A===
            var bucketsA = new List<gBucket>();                      // list of bucket numbers (for each match)
            AddBuckets(bucketsA, "", codelist, "", "", "");
            var queryA = bucketsA.GroupBy(x => x.bucket)                 // group by the hour (x is an int)
                               .Select(g => new { Hour = g.Key, Count = g.Count() })
                               .OrderBy(e => e.Hour);
            var arrA = queryA.ToArray();

            s = "";
            foreach (var e in arrA)
            {
                s += ",(" + e.Hour + "," + e.Count + ")";
            }
            if (s.Left(1) == ",") s = s.Substring(1);
            Program.VerboseAudit("VS Hourly Counts for " + desc + ": " + s);
            //===A===
            for (int i = 0; (i < arrA.GetUpperBound(0)); i++)
            {
                // Are these two hours in a row? (beware missing hours)
                if (arrA[i].Hour == arrA[i + 1].Hour - 1)
                {
                    // Add these two consecutive hours
                    if (arrA[i].Count + arrA[i + 1].Count >= 9)
                    {
                        SetInd(24, "For codes:" + codelist + "The counts=" + arrA[i].Count + "+" + arrA[i + 1].Count + " for the 2 consecutive hours of " + i + " and " + (i + 1).ToString());
                    }
                }
            }

        }


        private void CheckUserDefined()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("User-Defined indicators");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(61, "", "30403000327", "", "", "Yes,Dual Sign Off - Yes",SearchDepth.SearchSinceAdmission);
            SetIndIfResultContains(61, "", "30403000160", "", "", "Yes,Dual Sign Off - Yes",SearchDepth.SearchSinceAdmission);
            SetIndIfResultContains(69, "", "30403000326", "", "", "Floor mats,Helmet,Hip pads,Gait belt,Bed alarm,Chair alarm,Refused by the patient");
            //SetIndIfResultContains(69, "", "305110", "", "", "");
            SetIndIfResultContains(69, "", "7060390", "", "", "Fall Risk");
            SetIndIfResultContains(95, "", "7462", "", "", "");
            SetIndIfResultContains(95, "", "3040102607", "", "", "HFOV=High frequency oscillation ventilation");
            SetIndIfResultContains(96, "", "316210", "", "", "");
            SetIndIfResultContains(101, "", "7462", "", "", "");
            SetIndIfResultContains(101, "", "3040101409", "", "", "0/4 twitches,1/4 twitches,2/4 twitches,3/4 twitches,4/4 twitches");
            SetIndIfResultContains(101, "", "3040102607", "", "", "HFOV=High frequency oscillation ventilation");
            SetIndIfResultContains(102, "", "30403000340", "", "", "CVOR");
            SetIndIfResultContains(109, "", "30403000340", "", "", "Cath lab");
            SetIndIfResultContains(110, "", "30403000340", "", "", "Cath lab");

        }

        //private bool IsICU()
        //{
        //    switch (_pat.unit_name)
        //    {
        //        case "ICU":
        //        case "3BR":
        //        case "CTIC":
        //        case "NIC":
        //        case "PCU":
        //        case "TICU":
        //        case "CCB":
        //        case "RICU":
        //        case "CCUS":
        //            return true;
        //        default:
        //            return false;
        //    }
        //}

        //private bool IsTele()
        //{
        //    switch (_pat.unit_name)
        //    {
        //        case "4EST":
        //        case "2NW":
        //        case "3AE":
        //        case "5MEH":
        //        case "5NW":
        //        case "6MEH":
        //        case "6NW":
        //        case "7MEH":
        //        case "B4W":
        //        case "B5S":
        //        case "B5W":
        //        case "2NOB":
        //        case "CPLX":
        //        case "TELMS":
        //        case "MAN4":
        //            return true;
        //        default:
        //            return false;
        //    }
        //}



        //=====================================================================
        // Group like assessments into buckets of time
        // Each bucket counts as one assessment in the frequency count

        private void AddDependentBuckets(List<gBucket> bucket_list, string codelist1, string reslist1, string codelist2, string reslist2)
        {
            AddDependentBuckets(bucket_list, codelist1, reslist1, codelist2, reslist2, "", "");
        }

        private void AddDependentBuckets(List<gBucket> bucket_list, string codelist1, string reslist1, string codelist2, string reslist2, string codelist3,string reslist3)
        {
            bool dep3 = true;
            // get the chart items for the assessments
            var query1 = StartNewQuery();
            query1 = AndItemFilter(query1, "", codelist1, "", "", reslist1);
            var query2 = StartNewQuery();
            query2 = AndItemFilter(query2, "", codelist2, "", "", reslist2);
            if (codelist3.Trim() == "")
            {
                dep3 = false;
                codelist3 = "Hello, this is a phantom code";
            }
            var query3 = StartNewQuery();
            query3 = AndItemFilter(query3, "", codelist3, "", "", reslist3);

            // figure out what buckets the events belong to
            var query1a = from item in query1
                          select new
                          {
                              bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                              code = item.CODE,
                              evdt = item.EVENT_DATETIME
                          };
            var query2a = from item in query2
                          select new
                          {
                              bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                              code = item.CODE,
                              evdt = item.EVENT_DATETIME
                          };
            var query3a = from item in query3
                              select new
                              {
                                  bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                                  code = item.CODE,
                                  evdt = item.EVENT_DATETIME
                              };
            
            string s = "BucketList1 for " + codelist1 + ": ";
            foreach (var item in query1a)
            {
                s += item.bucket + ",";
            }
            Program.VerboseAudit(s);

            s = "BucketList2 for " + codelist2 + ": ";
            foreach (var item in query2a)
            {
                s += item.bucket + ",";
            }
            if (dep3)
            {
                s = "BucketList3 for " + codelist3 + ": ";
                foreach (var item in query3a)
                {
                    s += item.bucket + ",";
                }
            }
            Program.VerboseAudit(s);
            // Add to the list IFF items in both lists occur in same bucket
            foreach (var item1 in query1a)
            {
                foreach (var item2 in query2a)
                {
                    if (item1.bucket == item2.bucket)
                    {
                        if (dep3)
                        {
                            foreach (var item3 in query3a)
                            {
                                if (item1.bucket == item3.bucket)
                                {
                                    var b = new gBucket();
                                    b.bucket = item1.bucket;
                                    b.code = item1.code;
                                    b.evdt = item1.evdt;
                                    bucket_list.Add(b);
                                }
                            }
                        }
                        else
                        {
                            foreach (var item3 in query2a)
                            {
                                if (item1.bucket == item2.bucket)
                                {
                                    var b = new gBucket();
                                    b.bucket = item1.bucket;
                                    b.code = item1.code;
                                    b.evdt = item1.evdt;
                                    bucket_list.Add(b);
                                }
                            }
                        }
                    }
                }
            }

        }

        private void Check_TxArea()
        {
            string fndlist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("Treatment Area");
            Program.VerboseAudit("---------------");
            //C2B	355
            //N3G	305
            //N6G	27218005
            //S1B	27218006
            //S2E	530265
            //S3B	265
            //S3E	415
            //S4B	465
            //S5E	440

            //if (_pat.unit_id == 305 || _pat.unit_id == 465 || _pat.unit_id == 27218006)
            //{
            //    // N3G,S4B,S1B
            //    fndlist = "MCK_TOTAL";
            //    if (ResultContains("NIH", fndlist, "", "", ""))
            //    {
            //        txarea = "Stroke";
            //    }
            //}

            //if (_pat.unit_id == 355)
            //{
            //    fndlist = "FND110771,FND104755,FND110770";
            //    if (ResultContains("", fndlist, "", "", ""))
            //    {
            //        txarea = "Surgical Cardiac";
            //    }
            //}
            //if (_pat.unit_id == 440)
            //{
            //    fndlist = "FND101541,FND108401,FND101512,FND101542,FND108400";
            //    if (ResultContains("CAPD3", fndlist, "", "", ""))
            //    {
            //        txarea = "Renal";
            //    }
            //    else
            //    {
            //        fndlist = "FND103792,FND103795,FND103793,FND103794";
            //        if (ResultContains("RENAL1122", fndlist, "", "", ""))
            //        {
            //            txarea = "Renal";
            //        }
            //    }
            //}
            //if (_pat.unit_id == 530265 || _pat.unit_id == 415)
            //{
            //    fndlist = "FND103374";
            //    if (ResultContains("PEENT8 ", fndlist, "", "", ""))
            //    {
            //        txarea = "Cath with Sheath";
            //    }
            //}

            if (_pat.unit_id == 265)
            {
                if (_pat.age >= 16.0)
                    txarea = "Adult Pt";
                else
                    txarea = "Pedi Pt";
            }
        }


        private void AtLeastOneADL()
        {
            Program.Audit("---------------");
            if (_inds[1].is_checked)
            {
                var list = new List<int> { 305, 355 }; // N$G C2A
                if (list.Contains(_pat.unit_id))
                {
                    SetInd(2, "Promoting to ADL partial care for this unit.");
                }
            }

            if (!(_inds[2].is_checked || _inds[3].is_checked || _inds[4].is_checked)) {
                // Make "#2 ADL - Assist" the default.  (90% of patients)
                SetInd(2, "Defaulting to ADL Self due to lack of documentation.");
            }
        }

        private void HighestIndicatorInEachGroupWins()
        {
            int i, g;
            bool highest_is_on;
            string ind_list;

            Program.Audit("Select highest indicator in each group");
            
            g = 0;
            highest_is_on = false;
            // Go from bottom up - highest (lowest) indicator in each group wins
            for (i = MAX_INDS; (i >= 1); i--) {
                if (_inds[i].radio_group > 0) {
                    if (_inds[i].radio_group != g) {
                        //this is a new group
                        g = _inds[i].radio_group;
                        highest_is_on = _inds[i].is_checked;
                    } else {
                        //same group
                        if (highest_is_on) {
                            _inds[i].is_checked = false;             //uncheck a lower number
                        } else {
                            highest_is_on = _inds[i].is_checked;     //save this one
                        }
                    }
                }
            } // next i
            
            //Echo the indicators for an Audit (no classification will be saved)
            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++) {
                if (_inds[i].is_checked) ind_list += "," + i;
            } // next i

            Program.Audit("Final list = " + ind_list.Substring(1));
            Program.Audit("---------------");
        }


        private void CheckProcs()
        {
            //CheckProc_1();
            //CheckProc_2();
            //CheckProc_3();
            //CheckProc_4();
            //CheckProc_5();
            //CheckProc_6();
            //CheckProc_7();
            //CheckProc_8();
            //CheckProc_9();
            //CheckProc_10();
            //CheckProc_11();

        }

        private void DoProc(int pnum, string code)
        {
            double mins = 0;
            string found_what;
            DateTime evdt;
            DateTime enddt = DateTime.MinValue;

            if (GetResultAndEVDT("", code, "", "", out found_what, out evdt))
            {
                mins = 60.0 * found_what.ToDouble();
                enddt = evdt.AddMinutes(mins);

                if (ProcExistsInDB(pnum, evdt, enddt))
                {
                    Program.Audit("Procedure " + pnum+ ": already exists");
                }
                else
                {
                    if (!QueuedProcOverlaps(pnum, evdt, enddt))
                    {
                        var proc = new proc_data();
                        proc.procedure_number = pnum;
                        proc.start = evdt;
                        proc.finish = enddt;
                        _procs.Add(proc);
                        Program.Audit("Procedure " + pnum + ": Found " + code + " between " + evdt + " and " + enddt);
                    }
                }

            }

        }

        private bool ProcExistsInDB(int pnum, DateTime startdt, DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && (proc.PROCEDURE_DATETIME == startdt)
                            && (proc.DEPARTURE_DATETIME == enddt)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new { proc.PROCEDURE_EVENT_ID };
            return (query.Count() > 0);
        }

        private bool QueuedProcOverlaps(int pnum, DateTime startdt, DateTime enddt)
        {
            bool overlap = false;

            proc_data[] pary = _procs.ToArray();
            for (int i = 0; i <= pary.GetUpperBound(0); i++)
            {
                if (pary[i].procedure_number == pnum)
                {
                    //overlap if   p.start between stardt and enddt
                    //             p.finish between startdt and enddt
                    //             startdt >= p.start and enddt <= p.finish
                    if ((pary[i].start >= startdt) && (pary[i].start < enddt) ||
                        (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
                        (startdt >= pary[i].start) && (enddt <= pary[i].finish))
                    {
                        overlap = true;
                    }

                    if ((pary[i].start >= startdt) && (pary[i].start < enddt) && (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
                        (pary[i].start < startdt) && (pary[i].finish > startdt) && (pary[i].finish < enddt)
                        )
                    {
                        // the proc in the list is completely contained in startdt/endt pair
                        // change this proc to be the startdt/enddt
                        // overlap is still true, but the list item will be updated with the encompassing times
                        //Program.Audit("Procedure " + pnum + ": " + startdt + " - " + enddt + " will trump " + pary[i].start + " - " + pary[i].finish);
                        pary[i].start = startdt;
                        pary[i].finish = enddt;
                    }
                }
            }
            _procs = pary.ToList();
            return overlap;
        }


        private void CheckProc_1()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P1. 1-1 safety observation by RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_2()
        {
            string nowstr;
            string toddtstr;
            string yesdtstr;
            string timea ="";
            DateTime timea_startdt, timea_enddt;
            DateTime timeb_startdt, timeb_enddt;
            DateTime nowdt = _pat.pull_finish;              // "now" is pull time
            DateTime yesdt;
            
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P2. 1-1 safety observation by non-RN");
            Program.VerboseAudit("---------------");

            nowstr = nowdt.ToString("yyyyMMddHHmm");
            yesdt = nowdt.AddDays(-1);
            toddtstr = nowdt.ToString("yyyyMMdd");
            yesdtstr = yesdt.ToString("yyyyMMdd");
            
            //when is now? (yesterday/today)
            //yes 7am -- yes 7p  -- tod 7a -- tod 7p
            //                                   A                   B
            //if nowdt >= tod7pm then check  tod 7am-tod 7pm and tod 7pm-tom 7a
            //if nowdt >= tod7am then check  yes 7p - tod 7a     tod 7a-tod 7p
            //if nowdt >= yes7pm then check  yes 7a-yes 7p       yes 7p-tod 7a
            if (nowstr.CompareTo(toddtstr + "1900") >= 0) {
                timea = toddtstr + "0700";
            } else if (nowstr.CompareTo(toddtstr + "0700") >= 0) {
                timea = yesdtstr + "1900";
            } else if (nowstr.CompareTo(yesdtstr + "1900") >= 0) {
                timea = yesdtstr + "0700";
            }
            
            timea_startdt = PFSUtility.ISOToDateTime(timea);
            timea_enddt = timea_startdt.AddHours(12);
            MaybeAddSitter(timea_startdt, timea_enddt);

            timeb_startdt = timea_enddt;
            timeb_enddt = timeb_startdt.AddHours(12);
            MaybeAddSitter(timeb_startdt, timeb_enddt);
        }

        private void MaybeAddSitter(DateTime startdt, DateTime enddt)
        {
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);    // add custom time range below
            query = query.Where(e => (e.EVENT_DATETIME >= startdt) && (e.EVENT_DATETIME < enddt));
            query = AndCodeInList(query, "Sitter");
            query = AndResultInList(query,"continued, initiated");
            query = AndResultNotInList(query, "discontinued");

            if (query.Count() > 0) {
                if (ProcExists(2, startdt, enddt)) {
                    Program.Audit("Procedure 2: already exists");
                } else {
                    var proc = new proc_data();
                    proc.procedure_number = 2;
                    proc.start = startdt;
                    proc.finish = enddt;
                    _procs.Add(proc);
                    Program.Audit("Procedure 2: Found Sitter between " + startdt + " and " + enddt);
                }
            }
            
        }

        private bool ProcExists(int pnum, DateTime startdt, DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && (proc.PROCEDURE_DATETIME == startdt)
                            && (proc.DEPARTURE_DATETIME == enddt)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new {proc.PROCEDURE_EVENT_ID};
            return (query.Count() > 0);
        }


        private void CheckProc_3()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P3. Off unit accompanied by RN");
            Program.VerboseAudit("---------------");
            DoProc(3, "A_MHAcuOffUnit");
        }

        private void CheckProc_4()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P4. Off unit accompanied by non-RN");
            Program.VerboseAudit("---------------");
            DoProc(4, "A_MHAcuOffUNonRN");
        }

        private void CheckProc_5()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P5. Patient/family education by RN");
            Program.VerboseAudit("---------------");
            DoProc(5, "A_MHAcuPtFamEduc");
        }

        private void CheckProc_6()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P6. Extensive wound management by RN");
            Program.VerboseAudit("---------------");
            DoProc(6, "A_MHAcuExtensive");
        }

        private void CheckProc_7()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P7. Extensive wound management by non-RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_8()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P8. Coordination of care by RN");
            Program.VerboseAudit("---------------");
            DoProc(8, "A_MHAcuCoordinat");
        }

        private void CheckProc_9()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P9 1:1 RN at bedside");
            Program.VerboseAudit("---------------");
            DoProc(9, "A_MHAcu1:1byURN");
        }

        private void CheckProc_10()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P10. 1:1 non-RN at bedside");
            Program.VerboseAudit("---------------");
            DoProc(10, "A_MhAcu1:1UNonRN");
        }

        private void CheckProc_11()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P11. 2:1 by RN at bedside");
            Program.VerboseAudit("---------------");
            DoProc(11, "A_MHAcu2:1by URN");
        }

        private void CheckOutcomes()
        {
            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = AndItemFilter(query, "", "A_MHPostFalltime", "", "", "");
            foreach (var ch in query)
            {
                var outc = new proc_data();
                outc.procedure_number = 1;
                outc.start = ch.EVENT_DATETIME;
                _outcomes.Add(outc);
                Program.Audit("Outcomes 1: Found A_MHPostFalltime at " + outc.start);
            }
        }
        

        private const string DATETIME_FORMAT = "yyyyMMddHHmm";              // ISO Date/Time w/o seconds

        private void OutputClass(bool use_default)
        {
            string outstr, ind_list, desc, str_pull_dt;
            int i, tc_event_id;

            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         212 242 304
            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1
            //1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901 3456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
            //1       |10 5            |                |                |        |10023121779         |ALEXANDER                       |GLORIA                          |                                |0508    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289530  |           |201403030400                                                                      |NNYNNNNYYNNYNNNYNNNYYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//1       |10 5            |                |                |        |10024544908         |ARMSTRONG                       |AARON                           |                                |0535    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289531  |           |201403030400                                                                      |NYNNYNNNNNNYNNNYNNNNYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//|0547    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289532  |           |201403030400                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//|0547    |A   |201403030700|                |201403030700    |18  |C|    |3040|1440|18289532  |           |201403030700                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN

            if (Program.g_is_test)
                tc_event_id = 9999;
            else
                tc_event_id = PFSDBUtility.NextGID();                         //get a unique id for this class
                                                                              //            str_pull_dt = _pat.pull_finish.ToString(DATETIME_FORMAT);
            str_pull_dt = _pat.pull_finish.ToString(DATETIME_FORMAT);

            outstr = _pat.facilty_code.FixedWidth(8);                       //(facility code)
            outstr += "|" + _pat.unit_name.FixedWidth(16);
            outstr += "|" + "".FixedWidth(16);                               //(unit code)
            outstr += "|" + txarea.FixedWidth(16);                               //(area code)
            outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
            outstr += "|" + _pat.acct.FixedWidth(20);
            outstr += "|" + _pat.last_name.FixedWidth(32);
            outstr += "|" + _pat.first_name.FixedWidth(32);
            outstr += "|" + _pat.middle_name.FixedWidth(32);
            outstr += "|" + _pat.room.FixedWidth(8);
            outstr += "|" + _pat.bed.FixedWidth(4);
            outstr += "|" + str_pull_dt.FixedWidth(12);                      //class datetime (could change)
            outstr += "|" + "".FixedWidth(16);                               //(login)
            outstr += "|" + str_pull_dt.FixedWidth(16);                      //(employee)/(pull datetime)
            outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
            outstr += "|" + "C".FixedWidth(1);                               //record type = class
            outstr += "|" + "".FixedWidth(4);                                //(stage)
            outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
            outstr += "|" + _pat.range.ToString().FixedWidth(4);             //TC pull range
            outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
            outstr += "|";
            outstr = outstr.FixedWidth(294);
            outstr += "|" + _pat.effective.ToString(DATETIME_FORMAT);        //IN
            outstr = outstr.FixedWidth(377);
            outstr += "|";

            if (use_default)
            { //make all is_checked = false and then mark defaults
                Program.VerboseAudit("Patient Will receive default indicators " + _pat.default_inds_str);
                for (i = 1; (i <= MAX_INDS); i++)
                {
                    _inds[i].is_checked = false;
                }
                foreach (var ind in _pat.default_inds)
                {
                    if (ind <= _inds.GetUpperBound(0))
                    {
                        _inds[ind].is_checked = true;
                    }
                }
            }

            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++) {
                if (_inds[i].is_checked) {
                    outstr += "Y";
                    ind_list += "," + i;
                } else {
                    outstr += "N";
                }
            } // next i
            ind_list = ind_list.Substring(1);                           //strip leading comma
  
            Program.outfile.WriteLine(outstr);                          //output to transparent.txt

            Program.Audit("");
            desc = "Classified: " + ind_list;
            if (Program.g_is_test) {
                Program.Audit(desc);
            } else {
                //Save the selected indicators plus both Program.Audits in the event log; link with tc_event_id
                PFSEventLog.AddTransparentMappingEventLogEntry(
                    desc, Program.gLogUnitID, Program.gLogEncounterID,
                    tc_event_id, Program.gLogMapperVersion,
                    Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
            }
        }

        private int DeterminePtypeOfIndicators()
        {
            int i;
            var pscore = 0.0;
            int pt_type = 6;
            var indlist = new List<int>();
            string indstr = "";

            Program.VerboseAudit("Determine patient type of triggered indicators");

            for (i = 1; (i <= MAX_INDS); i++)
            {
                if (_inds[i].is_checked)
                {
                    indlist.Add(i);
                    indstr += i.ToString() + ",";
                }
            }

            var db = PFSDBUtility.NewPfsDataContext();
            var query_ind_def = from ind_def in db.INDICATOR_DEFINITIONs
                                where (ind_def.METHODOLOGY_ID == PFSGlobal.METH_ID_APLUS_INPATIENT2) &&
                                  indlist.Contains(ind_def.INDICATOR_NUMBER)
                                select new
                                {
                                    ind_def.WEIGHT
                                };
            foreach (var wgts in query_ind_def)
            {
                pscore += wgts.WEIGHT;
            }
            Program.VerboseAudit("indicators=" + indstr);
            Program.VerboseAudit("score=" + pscore.ToString());

            var query_ptype = from ptype in db.PATIENT_TYPEs
                              where (ptype.METHODOLOGY_ID == PFSGlobal.METH_ID_APLUS_INPATIENT2)
                              orderby ptype.PATIENT_TYPE1
                              select new
                              {
                                  ptype.PATIENT_TYPE1,
                                  ptype.POINTS_HIVAL
                              };

            foreach (var ptypes in query_ptype)
            {
                //                Program.DebugTrace("type,hival=" + ptypes.PATIENT_TYPE1.ToString() + "," + ptypes.POINTS_HIVAL.ToString(), "");
                if (pscore <= ptypes.POINTS_HIVAL)
                {
                    if (pt_type > ptypes.PATIENT_TYPE1)
                    {
                        pt_type = ptypes.PATIENT_TYPE1;
                    }
                }
            }
            Program.VerboseAudit("patient type=" + pt_type.ToString());

            return pt_type;

        }


        private bool ExistDefaultInPast16hrs()
        {
            DateTime cdt1 = DateTime.MinValue;
            DateTime cdt2 = DateTime.MinValue;
            int cnt_all = 0;
            int cnt_def = 0;
            //get max class date of last non-default class = 1
            //get max class date of last default = 2
            // if 1 >= 2 then false
            // else
            //   if 2 > 1 and date is <= 16 hrs ago then true
            //   else false
            var db = PFSDBUtility.NewPfsDataContext();
            //var query = from ce in db.CLASSIFICATION_EVENTs
            //            where (ce.ENCOUNTER_ID == _pat.encounter_id)
            //            && (ce.CLASSIFIED_BY_ID != -2)
            //            && (System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(ce.CLASSIFICATION_DATETIME, Program.g_pull_start) <= 960)
            //            select ce;
            //cnt_all = query.Count();
            //if (cnt_all > 0)
            //{
            //    cdt1 = PFSUtility.DBToDateTime(query.Max(x => x.CLASSIFICATION_DATETIME));
            //    Program.VerboseAudit("Last regular classification was at:"+cdt1.ToString());
            //}
            //else {
            //    Program.VerboseAudit("No regular classifications within the past 16 hours");
            //}

            var query = from ce in db.CLASSIFICATION_EVENTs
                        where (ce.ENCOUNTER_ID == _pat.encounter_id)
                        && (ce.CLASSIFIED_BY_ID == -2)
                        && (System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(ce.CLASSIFICATION_DATETIME, Program.g_pull_finish) <= 960)
                        select ce;
            cnt_def = query.Count();

            if (cnt_def > 0)
            {
                cdt2 = PFSDBUtility.DBToDateTime(query.Max(x => x.CLASSIFICATION_DATETIME));
                Program.VerboseAudit("Last default classification was at:" + cdt2.ToString());
            }
            else
            {
                Program.VerboseAudit("No default classifications within the past 16 hours");
            }
            return (cnt_def > 0);

        }


        //private void OutputProcs()
        //{
        //    int i;
        //    string outstr, proc_list, desc;
        //    int tc_event_id;

        //    foreach(var proc in _procs) {
        //        if (Program.g_is_test)
        //            tc_event_id = 9999;
        //        else
        //            tc_event_id = PFSDBUtility.NextGID();                         //get a unique id for this proc

        //        outstr = _pat.facilty_code.FixedWidth(8);
        //        outstr += "|" + _pat.unit_name;                                 //10
        //        outstr = outstr.FixedWidth(68);
        //        outstr += "|" + _pat.acct.FixedWidth(20);                       //90
        //        outstr += "|" + _pat.last_name.FixedWidth(32);
        //        outstr += "|" + _pat.first_name.FixedWidth(32);
        //        outstr += "|" + _pat.middle_name.FixedWidth(32);
        //        outstr = outstr.FixedWidth(202);
        //        outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //204 proc dt
        //        outstr = outstr.FixedWidth(254);
        //        outstr += "|P";                                                 //256 procedure type record
        //        outstr += "|" + "".FixedWidth(4);                               //(stage)
        //        outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);     //TC source ID
        //        outstr += "|" + _pat.range.ToString().FixedWidth(4);            //TC pull range
        //        outstr += "|" + tc_event_id.ToString().FixedWidth(10);          //TC event ID
        //        outstr += "|";
        //        outstr = outstr.FixedWidth(294);
        //        outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //296 procdt in
        //        outstr = outstr.FixedWidth(346);
        //        outstr += "|" + proc.finish.ToString(DATETIME_FORMAT);          //348 procdt out
        //        outstr = outstr.FixedWidth(377);
        //        outstr += "|";

        //        proc_list = "";
        //        for (i = 1; (i < MAX_PROCS); i++) {
        //            if (proc.procedure_number == i) {
        //                outstr += "Y";
        //                proc_list += "," + i;
        //            } else {
        //                outstr += "N";
        //            }
        //        } // next i
        //        proc_list = proc_list.Substring(1);                             //strip leading comma

        //        Program.outfile.WriteLine(outstr);                              //output to transparent.txt

        //        desc = "Procedures: " + proc_list;
        //        if (Program.g_is_test) {
        //            Program.Audit(desc);
        //        } else {
        //            //Save the selected procedures plus both Program.Audits in the event log; link with tc_event_id
        //            //(this assumes the procedure Program.Audit is mixed in with the class Program.Audit)
        //            PFSEventLog.AddTransparentMappingEventLogEntry(desc, Program.gLogUnitID, Program.gLogEncounterID,
        //                tc_event_id, Program.gLogMapperVersion,
        //                Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
        //        }
        //    } // next proc
        //}

    }
}
