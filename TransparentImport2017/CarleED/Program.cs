﻿//
//   OBX|||ADT10^PLACE IN ED OBSERVATION STATUS^C4|||||||||||20180110134541-0600
//  Above is signal for transition to ED inpatient
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;                // for SqlConnection
using System.IO;                            // for Directory, Path, Stream
using System.Windows.Forms;                 // for Application (also add reference)
using PfsShared;                            // add a reference to Shared2 project
//using System.Data;
//using System.Data.OleDb;
//
// Mt Sinai ED Transparent Mapping main program.
// NOTE: Each unit must set the "use transparent" flag.
//
// sample: -efftime=0700 -effdate=yesterday -pulltime=0700 -pulldate=today -range=1440
//
// This is designed to be run as a background job, so never stop for user input -- that means no error MsgBox.
// All communication is done through log files and the event log; it is OK to print to the console.
//
// In order to work with the AcuityPlus Patient Selection and Transparent import:
//
//   * The program name must be TransparentMapping.exe, placed in AcuityPlus\bin
//   * The audit file is called TransparentAudit.log, placed in AcuityPlus\log
//   * The output file is Transparent.txt, placed in AcuityPlus\load_me
//   * Events are saved in the database event log
//
namespace TransparentMapping
{
    public class LocationData
    {
        public DateTime time_in;
        public DateTime time_out;
        public int unit_id;
        public int meth_id;
        public int range;
        public bool toremove;
        public string txarea;
    }   

    public class PatientInfo
    {
        public int      encounter_id;
        public string   last_name;
        public string   first_name;
        public string   middle_name;
        public string   acct;
        public double   age;                    // age (in years) at admission
        public string   room;
        public string   bed;
        public DateTime unit_arrival;
        public DateTime unit_departure;
        public DateTime effective;              // patient specific (may be > g_effdt)
        public DateTime pull_start;             // patient specific (may be > g_pull_start)
        public DateTime pull_finish;            // patient specific (may be < g_pull_finish)
        public int      range;                  // patient specific (may be < g_range) - minutes
        public double   los_hours;              // hours during pull
        public int      TC_source_id;           // TCP port (chart source)
        // unit info
        public string   facilty_code;           // class import facility code
        public string   unit_name;
        public int      unit_id;
        public bool     is_ED;
        public bool     is_ICU;
        public int      meth_id;
        public string adt_unit_name;
        public int num_locs;
        public LocationData[] locary = new LocationData[20];
        public bool readyforEDclass;
        public DateTime EDInpTime;
        public string disch_stat;
        public bool was_admitted;
        //public List<int> default_inds;          // unit admission profile indicators for recent arrivals
        //public string default_inds_str;         // "1,6,8"
        public void InitLocArray()
        {
            for (int i = 0; i < locary.Length; ++i)
                locary[i] = new LocationData();
        }
    }
    public struct med_data
    {
        public string code;
        public string descript;
        public string drugclass;
        public DateTime evdt;
    }
    public class EDchild
    {
        public int parent_unitid;
        public int meth_id;
        public int child_unitid;
        public string child_name;
    }


    public static class Program
    {

        const string    MAPPER_VERSION = "1.00";
        const string    TRANSP_FILENAME = "Transparent.txt";    // THE output file (for class import)
        const string    TRANSP_AUDIT_LOG = "TransparentAudit.log";  // Legacy debug/audit log
        const int       CHART_ITEM_LIFE = 30;                   // days in the chart_item table
        const int       DEFAULT_RANGE = 1440;                   // 24 hrs in minutes; override with -range

        public static bool g_abort;                         // stop!
        public static bool g_debug;                         // output to console?
        public static bool g_log;                           // output to log file? (legacy feature)
        public static bool g_no_output;                     // audit file only?
        public static bool g_is_test;                       // output to file w/dummy ids and no event log
        public static bool g_no_delete;                     // keep old chart items?
        public static bool g_stop_on_error;
        public static bool g_import_when_done;

        public static DateTime      g_effdt;                // effective datetime

        public static StreamWriter  outfile;                // Transparent.txt
        public static StreamWriter  logfile;                // TransparentLog.txt

        public static int           gLogUnitID;
        public static int           gLogEncounterID;
        public static string        gLogSourceText;
        public static string        gLogMapperVersion;

        public static StringBuilder gBriefAudit;            // the complete brief audit
        public static StringBuilder gVerboseAudit;          // the complete verbose audit

        public static DateTime      g_pull_start;           // global pull start  (not limited by patient values)
        public static DateTime      g_pull_finish;          // global pull finish
        public static int           g_range;                // global range

        private static string   _this_acct;                 // acct filter
        private static string   _this_unit_name;            // unit name filter
        private static int      _this_unit_id;              // unit id filter
        private static int      _lookback=-2;                // number of days to look back

        static string           _import_path;
        static string           _log_path;
        private static PatientInfo _pat;

        // Chart_Item data being processed
        private static int _encounter_id;          // (int is 32 bit so this is OK)
        private static int _sequence;              // sequence# for dummy codes
        private static DateTime _start_dt = DateTime.Now.AddHours(-24);
        private static DateTime _finish_dt = DateTime.Now;
        private static string _treatment_area;
        private static ENCOUNTER_LOCATION[] _locations;    // for unit_id lookup
        private static string _category;
        private static string _fndcode;
        private static string _descript;
        private static string _group;
        private static string _result;
        private static string _perfdate;
        private static int seq = 0; // DateTime.Now.Hour * 10;

        public static List<EDchild> EDchildren;


        static void Main(string[] args)
        {
            try
            {
                //OpenXLS();
                InitGlobals();

                ParseCommandLine(args);
                SetMapperVersion();

                gLogSourceText = String.Join(" ", args);           // add command line to log
                LogInfo("Begin mapping and translation", PFSEventLog.EventLogCategory.EVENT_CATEGORY_STARTUP_SHUTDOWN);
                gLogSourceText = "";

                OpenOutputFiles();
                EDchildren = GetEDChildren();
                ShowEDChildren();
                ProcessPatients();

                

//                DeleteOldChartItems();

                LogInfo("Mapping complete", PFSEventLog.EventLogCategory.EVENT_CATEGORY_STARTUP_SHUTDOWN);

                MaybeRunImport();
                CloseOutputFiles();
            }
            catch (Exception e)
            {
                Console.WriteLine("Unexpected Error: {0}", e.Message);
                Console.WriteLine("{0}", e.StackTrace);
                LogUnexpectedError(e.Message, e.StackTrace);
            }

            if (g_debug)
            {
                Console.WriteLine(Environment.NewLine);
                Console.Write("Press any key...");
                Console.ReadKey();
            }
        }

        static void InitGlobals()
        {
            gLogSourceText = "";
            gBriefAudit = new StringBuilder();
            gVerboseAudit = new StringBuilder();
            _pat = new PatientInfo();
            _pat.InitLocArray();
        }

        static void ParseCommandLine(string[] args)
        {
            string nowdate, nowtime;
            string value;
            string effdate, efftime;
            string pulldate, pulltime;

            var nowdt = DateTime.Now;
            nowdate = nowdt.ToString("yyyyMMdd");
            nowtime = nowdt.ToString("HHmm");
            effdate = "";
            efftime = "";
            pulldate = "";
            pulltime = "";

            _import_path = PFSUtility.DefaultImportPath();          // ...\load_me
            _log_path = PFSUtility.DefaultLogPath();                // ...\log

            foreach (var arg in args)
            {
                var arr = arg.Split('=');
                value = (arr.GetUpperBound(0) > 0) ? arr[1] : "";

                switch (arr[0])
                {
                    case "-acct":                               // process this patient only
                        _this_acct = value;
                        break;
                        
                    case "-debug":                              // output to console and pause at end
                        g_debug = true;
                        break;

                    case "-effdate":                            // effective date: yyyymmdd
                        if (value == "yesterday")
                            effdate = DateTime.Now.AddDays(-1).ToString("yyyyMMdd");
                        else
                            effdate = value.Left(8);
                        break;
                    
                    case "-efftime":                            // effective time: HHMM or HH
                        efftime = value.Left(4);
                        efftime = efftime.PadRight(4,'0');
                        break;
                    
                    case "-import":                             // run transparent import when done
                        g_import_when_done = true;
                        break;

                    case "-log":                                // create TransparentAudit.log
                        g_log = true;                           // (verbose audit)
                        break;

                    case "-path":                               // override default import path
                        _import_path = value;
                        break;

                    case "-logpath":                               // override default import path
                        _log_path = value;
                        break;
                    case "-n":                                  // no output to transparent.txt 
                        g_no_output = true;                     // (audit only)
                        break;

                    case "-nodelete":                           // do not delete old chart items
                        g_no_delete = true;
                        break;

                    case "-pulldate":                           // pull date: yyyymmdd
                        if (value == "today")
                            pulldate = DateTime.Now.ToString("yyyyMMdd");
                        else
                            pulldate = value.Left(8);
                        break;
                    
                    case "-pulltime":                           // pull time: HHMM or HH
                        pulltime = value.Left(4);
                        pulltime = pulltime.PadRight(4,'0');
                        break;

                    case "-range":                              // range in minutes; default = 1440 = 24hr
                        g_range = value.ToInteger();
                        break;
                        
                    case "-stoponerror":
                        g_stop_on_error = true;
                        break;

                    case "-test":
                        g_is_test = true;
                        break;

                    case "-unit":                               // process this unit only
                        _this_unit_name = value;
                        break;
                    case "-unit_id":                            // process this unit only
                        _this_unit_id = value.ToInteger();
                        break;
                    case "-lookback":
                        _lookback = -Math.Abs(value.ToInteger());
                        break;
                        
                    default:
                        Console.WriteLine("unexpected argument: {0}", arg);
                        break;
                }
            }

            if (String.IsNullOrEmpty(effdate))
                effdate = nowdate;
            if (String.IsNullOrEmpty(efftime))
                efftime = nowtime;

            // Note: pulldate defaults to effdate, not nowdate
            if (String.IsNullOrEmpty(pulldate))
                pulldate = effdate;
            if (String.IsNullOrEmpty(pulltime))
                pulltime = efftime;

            if (g_range == 0)
                g_range = DEFAULT_RANGE;

            DebugTrace("Import path=", _import_path);
            DebugTrace("Log path=", _log_path);
            DebugTrace("effdate={0}", effdate);
            DebugTrace("efftime={0}", efftime);
            DebugTrace("pulldate={0}", pulldate);
            DebugTrace("pulltime={0}", pulltime);
            DebugTrace("range={0}", g_range);

            // class IN time
            g_effdt = PFSUtility.ISOToDateTime(effdate + efftime);
            // range for chart item queries
            g_pull_finish = PFSUtility.ISOToDateTime(pulldate + pulltime);
            g_pull_start = g_pull_finish.AddMinutes(-g_range);

        }

        static void OpenOutputFiles()
        {
            // Append to existing file
            outfile = new StreamWriter(Path.Combine(_import_path, TRANSP_FILENAME), true);
            
            if (g_log) {
                // re-write new file
                logfile = new StreamWriter(Path.Combine(_log_path, DateTime.Now.ToString("yyyyMMddHHmm") + TRANSP_AUDIT_LOG));
            }


        }

        //static void OpenXLS()
        //{
        //        string filename = Path.GetFileName("C:\\Users\\kmasumoto\\Documents\\2TC\\Mt Sinai MSSN\\ED\\Data Acuity Feed_April.xlsx");// to get filename
        //        string conStr = string.Empty;
        //        string extension = Path.GetExtension(filename);// get extension
        //        if (extension == ".xls" || extension == ".xlsx")
        //        {
        //            switch (extension)
        //            {
        //                case ".xls": //Excel 1997-2003
        //                    conStr = "Provider=Microsoft.Jet.OLEDB.4.0;" + "Data Source='" + "C:\\Users\\kmasumoto\\Documents\\2TC\\Mt Sinai MSSN\\ED\\Data Acuity Feed_April.xlsx" + "';" + "Extended Properties=Excel 8.0;";
        //                    break;
        //                case ".xlsx": //Excel 2007
        //                    conStr = "Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source='" + "C:\\Users\\kmasumoto\\Documents\\2TC\\Mt Sinai MSSN\\ED\\Data Acuity Feed_April.xlsx" + "';" + "Extended Properties=Excel 8.0;";
        //                    break;
        //                default:
        //                    break;
        //            }

        //            OleDbConnection connExcel = new OleDbConnection(conStr.ToString());
        //            OleDbCommand cmdExcel = new OleDbCommand();
        //            OleDbDataAdapter oda = new OleDbDataAdapter();
        //            connExcel.Open();

        //            DataTable dtExcelSchema;
        //            dtExcelSchema = connExcel.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
        //            string SheetName = dtExcelSchema.Rows[0]["Sheet1"].ToString();
        //            //ViewState["SheetName"] = SheetName;
        //            //Selecting Values from the first sheet
        //            //Sheet name must be as Sheet1
        //            OleDbDataAdapter da = new OleDbDataAdapter("SELECT * From [" + SheetName + "]", conStr.ToString()); // to fetch data from excel 
        //            da.Fill(dtExcelSchema);
        //        for (int i = 0; i <= dtExcelSchema.Rows[0].ItemArray.Length - 1; i++)
        //        {
        //            MessageBox.Show(dtExcelSchema.Rows[0].ItemArray[i] + " -- " + dtExcelSchema.Rows[0].Table.Columns[i]);
        //        }
        //    }
        //}

        static void CloseOutputFiles()
        {
            outfile.Close();
            
            if (logfile != null) {
                logfile.Close();
                logfile = null;
            }
        }

        static void SetMapperVersion()
        {
            gLogMapperVersion = MAPPER_VERSION;
        }


//UNITS TO CONSIDER FOR TC
//parent units:
//select* from unit where is_ed='y' and is_active = 'y' and parent_unit_id is null and USE_TRANSPARENT_CLASSIFICATION = 'y'

//select* from unit where PARENT_UNIT_ID=57012483 

//select* from unit_param where unit_id in (57013039,57013040) and getdate() between EFFECTIVE_DATETIME and EXPIRATION_DATETIME

//select* from METHODOLOGY 24=visit 25=inpt

        static void ProcessPatients()
        {
            string sql, audit_header;
            int count, prev_unit_id, prev_enc_id = 0;
            //parent_id = 2817
            //'Prod env:
            //vunit_id = 43729784
            //iunit_id = 43730689
            // Make a list of all patients with chart items during the pull period.
            //
            // Include only units that have the "use transparent" flag set.
            // Limit to one patient if -acct is given.  Limit to one unit with -unit.
            // Left join encounter_location - the patient may be discharged at pull time.
            // Look for the latest patient arrival to the unit before the pull time; ignore transfers within the unit.
            // Look for a patient departure (if any) before the pull time; ignore transfers within the unit.
            sql =
"select p.last_name,p.first_name,e.acct_number,e.age_at_admission,el.encounter_id"
+ ",u.unit_id,u.name,f.CLASSIFICATION_FACILITY_CODE,el.ADT_UNIT_NAME,el.DATETIME_IN,el.DATETIME_OUT as UNIT_DEPARTURE,el.room,el.bed"
+ ",ce.CLASSIFICATION_EVENT_ID,ce.unit_id as class_unit_id\n"
+ ",ARRIVE.MINDATETIME_IN,ARRIVE.MAXDATETIME_IN,DEPART.MAXDATETIME_OUT,DISCHARGE_STATUS_LVC as dcstat,0 as QUERY_TYPE\n"
+ "from encounter_location as el\n"
+ "inner join unit as u on (el.UNIT_ID=u.UNIT_ID)\n"
+ "inner join facility as f on (f.FACILITY_ID=u.FACILITY_ID)\n"
+ "inner join ENCOUNTER as e on (el.ENCOUNTER_ID=e.ENCOUNTER_ID)\n"
+ "inner join PERSON as p on (e.person_id=p.PERSON_ID)\n"
//+ "left join CLASSIFICATION_EVENT as ce on (el.encounter_id=ce.encounter_id) and (ce.EFFECTIVE_DATETIME_IN between el.EFFECTIVE_DATETIME_IN and el.EFFECTIVE_DATETIME_OUT or ce.EFFECTIVE_DATETIME_OUT between el.EFFECTIVE_DATETIME_IN and el.EFFECTIVE_DATETIME_OUT)\n"
+ "left join CLASSIFICATION_EVENT as ce on (el.encounter_id=ce.encounter_id) and (ce.METHODOLOGY_ID in (23,24,25,11,12))\n"
+ "left JOIN (\n"
+ "SELECT WORKING_UNIT_ID AS UNIT_ID, ENCOUNTER_ID, MIN(DATETIME_IN) as MINDATETIME_IN,MAX(DATETIME_IN) AS MAXDATETIME_IN\n"
+ " FROM ENCOUNTER_LOCATION AS EL\n"
 + "WHERE WORKING_UNIT_ID IN(SELECT UNIT_ID FROM UNIT WHERE USE_TRANSPARENT_CLASSIFICATION = 'Y')\n"
 //+ "AND DATETIME_IN <" + PFSDBUtility.SQLDateTime(g_pull_finish) + "\n"
 //+ "AND DATETIME_IN >" + PFSDBUtility.SQLDateTime(g_pull_finish.AddDays(-10)) + "\n"
 //--AND((NEXT_WORKING_UNIT_ID <> WORKING_UNIT_ID) OR(NEXT_WORKING_UNIT_ID IS NULL))\n"
 + "GROUP BY WORKING_UNIT_ID, ENCOUNTER_ID\n"
 + ") AS ARRIVE ON(ARRIVE.UNIT_ID = U.UNIT_ID) AND(ARRIVE.ENCOUNTER_ID = E.ENCOUNTER_ID)\n"
+ "left JOIN (\n"
 + "SELECT WORKING_UNIT_ID AS UNIT_ID, ENCOUNTER_ID, DATETIME_IN as depDTIN, MAX(DATETIME_OUT) AS MAXDATETIME_OUT\n"
 + "FROM ENCOUNTER_LOCATION AS EL\n"
 + "WHERE WORKING_UNIT_ID IN(SELECT UNIT_ID FROM UNIT WHERE USE_TRANSPARENT_CLASSIFICATION = 'Y')\n"
 + "AND DATETIME_OUT <" + PFSDBUtility.SQLDateTime(g_pull_finish) + "\n"
 //--AND((NEXT_WORKING_UNIT_ID <> WORKING_UNIT_ID) OR(NEXT_WORKING_UNIT_ID IS NULL))\n"
 + "GROUP BY WORKING_UNIT_ID, ENCOUNTER_ID, DATETIME_IN\n"
 + ") AS DEPART ON(DEPART.UNIT_ID = U.UNIT_ID) AND(DEPART.ENCOUNTER_ID = E.ENCOUNTER_ID)\n"
//+ "where u.unit_id=2817 and u.is_ed='y' and u.use_transparent_classification='y'\n"
+ "where u.is_ed='y' and u.use_transparent_classification='y' and u.parent_unit_id is null and u.is_active='y'\n"
+ "and (DEPART.MAXDATETIME_OUT>=ARRIVE.MAXDATETIME_IN) and (ce.METHODOLOGY_ID is null)\n"
+ "and (el.DATETIME_IN=ARRIVE.MAXDATETIME_IN)\n"
+ "and (el.DATETIME_OUT = DEPART.MAXDATETIME_OUT)\n"
+ "and (DATEADD(HH, 4, DEPART.MAXDATETIME_OUT) <= " + PFSDBUtility.SQLDateTime(g_pull_finish) + ")\n";
            //+ "(el.datetime_out is null or el.datetime_in>=dateadd(d,-3," + PFSDBUtility.SQLDateTime(g_pull_start) + ")) and (ce.METHODOLOGY_ID is null)\n";
            if (String.IsNullOrEmpty(_this_acct))
            {
                sql += " and (el.datetime_out is not null and el.datetime_in>=dateadd(d," + _lookback.ToString() + "," + PFSDBUtility.SQLDateTime(g_pull_finish) + "))\n";
            }

            if (!String.IsNullOrEmpty(_this_acct)) {
                sql += " AND E.ACCT_NUMBER=" + PFSDBUtility.SQLString(_this_acct) + "\n";
            }
            if (!String.IsNullOrEmpty(_this_unit_name)) {
                sql += " AND U.NAME=" + PFSDBUtility.SQLString(_this_unit_name) + "\n";
            }
            if (_this_unit_id > 0) {
                sql += " AND UNIT.UNIT_ID=" + _this_unit_id + "\n";
            }
sql += "UNION\n";
sql += "select p.last_name,p.first_name,e.acct_number,e.age_at_admission,el.encounter_id"
+ ",u.unit_id,u.name,f.CLASSIFICATION_FACILITY_CODE,el.ADT_UNIT_NAME,el.DATETIME_IN,el.DATETIME_OUT as UNIT_DEPARTURE,el.room,el.bed"
+ ",ce.CLASSIFICATION_EVENT_ID,ce.unit_id as class_unit_id\n"
+ ",ARRIVE.MINDATETIME_IN,ARRIVE.MAXDATETIME_IN,DEPART.MAXDATETIME_OUT,DISCHARGE_STATUS_LVC as dcstat,9 as QUERY_TYPE\n"
+ "from encounter_location as el\n"
+ "inner join unit as u on (el.UNIT_ID=u.UNIT_ID)\n"
+ "inner join facility as f on (f.FACILITY_ID=u.FACILITY_ID)\n"
+ "inner join ENCOUNTER as e on (el.ENCOUNTER_ID=e.ENCOUNTER_ID)\n"
+ "inner join PERSON as p on (e.person_id=p.PERSON_ID)\n"
//+ "left join CLASSIFICATION_EVENT as ce on (el.encounter_id=ce.encounter_id) and (ce.EFFECTIVE_DATETIME_IN between el.EFFECTIVE_DATETIME_IN and el.EFFECTIVE_DATETIME_OUT or ce.EFFECTIVE_DATETIME_OUT between el.EFFECTIVE_DATETIME_IN and el.EFFECTIVE_DATETIME_OUT)\n"
//+ "left join CLASSIFICATION_EVENT as ce on (el.encounter_id = ce.encounter_id) and (ce.EFFECTIVE_DATETIME_IN <= el.EFFECTIVE_DATETIME_IN and ce.EFFECTIVE_DATETIME_OUT >= el.EFFECTIVE_DATETIME_OUT)\n"
+ "left join CLASSIFICATION_EVENT as ce on (el.encounter_id = ce.encounter_id) and (ce.EFFECTIVE_DATETIME_IN <= el.EFFECTIVE_DATETIME_IN)\n"
+ "left JOIN (\n"
+ "SELECT WORKING_UNIT_ID AS UNIT_ID, ENCOUNTER_ID, MIN(DATETIME_IN) as MINDATETIME_IN,MAX(DATETIME_IN) AS MAXDATETIME_IN\n"
+ " FROM ENCOUNTER_LOCATION AS EL\n"
 + "WHERE WORKING_UNIT_ID IN(SELECT UNIT_ID FROM UNIT WHERE USE_TRANSPARENT_CLASSIFICATION = 'Y')\n"
 + "AND DATETIME_IN <" + PFSDBUtility.SQLDateTime(g_pull_finish) + "\n"
 //--AND((NEXT_WORKING_UNIT_ID <> WORKING_UNIT_ID) OR(NEXT_WORKING_UNIT_ID IS NULL))\n"
 + "GROUP BY WORKING_UNIT_ID, ENCOUNTER_ID\n"
 + ") AS ARRIVE ON(ARRIVE.UNIT_ID = U.UNIT_ID) AND(ARRIVE.ENCOUNTER_ID = E.ENCOUNTER_ID)\n"
+ "left JOIN (\n"
 + "SELECT WORKING_UNIT_ID AS UNIT_ID, ENCOUNTER_ID, DATETIME_IN as depDTIN, MAX(DATETIME_OUT) AS MAXDATETIME_OUT\n"
 + "FROM ENCOUNTER_LOCATION AS EL\n"
 + "WHERE WORKING_UNIT_ID IN(SELECT UNIT_ID FROM UNIT WHERE USE_TRANSPARENT_CLASSIFICATION = 'Y')\n"
 //+ "AND DATETIME_OUT <" + PFSDBUtility.SQLDateTime(g_pull_finish) + "\n"
 //+ "AND DATETIME_IN >" + PFSDBUtility.SQLDateTime(g_pull_finish.AddDays(-10)) + "\n"
 //--AND((NEXT_WORKING_UNIT_ID <> WORKING_UNIT_ID) OR(NEXT_WORKING_UNIT_ID IS NULL))\n"
 + "GROUP BY WORKING_UNIT_ID, ENCOUNTER_ID, DATETIME_IN\n"
 + ") AS DEPART ON(DEPART.UNIT_ID = U.UNIT_ID) AND(DEPART.ENCOUNTER_ID = E.ENCOUNTER_ID)\n"
//+ "where u.unit_id=2817 and u.is_ed='y' and u.use_transparent_classification='y'\n"
+ "where u.is_ed='y' and u.use_transparent_classification='y' and u.parent_unit_id is null and u.is_active='y'\n"
+ "and (DEPART.MAXDATETIME_OUT>=ARRIVE.MAXDATETIME_IN) and (ce.METHODOLOGY_ID is null)\n"
+ "and (el.DATETIME_IN=ARRIVE.MAXDATETIME_IN)\n"
+ "and (el.DATETIME_OUT = DEPART.MAXDATETIME_OUT) and (DATEADD(HH, 4, DEPART.MAXDATETIME_OUT) <= " + PFSDBUtility.SQLDateTime(g_pull_finish) + ")";
            //+ "(el.datetime_out is null or el.datetime_in>=dateadd(d,-3," + PFSDBUtility.SQLDateTime(g_pull_start) + ")) and (ce.METHODOLOGY_ID is null)\n";
            if (String.IsNullOrEmpty(_this_acct))
            {
                sql += " and (el.datetime_out is not null and el.datetime_in>=dateadd(d," + _lookback.ToString() + "," + PFSDBUtility.SQLDateTime(g_pull_finish) + "))\n";
            }

            if (!String.IsNullOrEmpty(_this_acct))
            {
                sql += " AND E.ACCT_NUMBER=" + PFSDBUtility.SQLString(_this_acct) + "\n";
            }
            if (!String.IsNullOrEmpty(_this_unit_name))
            {
                sql += " AND U.NAME=" + PFSDBUtility.SQLString(_this_unit_name) + "\n";
            }
            if (_this_unit_id > 0)
            {
                sql += " AND UNIT.UNIT_ID=" + _this_unit_id + "\n";
            }

            sql += " ORDER BY E.ACCT_NUMBER, QUERY_TYPE, MAXDATETIME_OUT desc, MAXDATETIME_IN desc, U.NAME, P.LAST_NAME, P.FIRST_NAME\n";

 VerboseAudit(sql);
            var db = PFSDBUtility.NewSqlConnection(120);
            var cmd = new SqlCommand(sql, db);
            SqlDataReader dr = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);




        //    public T DeadlockRetryHelper<T>(Func<T> repositoryMethod, int maxRetries)
        //{
        //    int retryCount = 0;

        //    while (retryCount < maxRetries)
        //    {
        //        try
        //        {
        //            return repositoryMethod();
        //        }
        //        catch (SqlException e) // This example is for SQL Server, change the exception type/logic if you're using another DBMS
        //        {
        //            if (e.Number == 1205)  // SQL Server error code for deadlock
        //            {
        //                retryCount++;
        //            }
        //            else
        //            {
        //                throw;  // Not a deadlock so throw the exception
        //            }
        //            // Add some code to do whatever you want with the exception once you've exceeded the max. retries
        //        }
        //    }
        //}

        //
        // Process all patients
        //
        count = 0;
            prev_unit_id = 0;
            var default_inds = new List<int>();
            var default_inds_str = "";
            int _queryType = 0;

            while (dr.Read())
            {

                // Package the patient info
                // NOTE: ''dr["FIRST_NAME"] as string'' looks great but it will save nulls.
                //       use ''PFSDBUtility.DBToString(dr["FIRST_NAME"])'' to convert to empty strings.
                var pat = new PatientInfo();
                _queryType = PFSDBUtility.DBToInt(dr["QUERY_TYPE"]);
                VerboseAudit("Query Type =" + _queryType);
                pat.encounter_id = PFSDBUtility.DBToInt(dr["ENCOUNTER_ID"]);
                if (pat.encounter_id != prev_enc_id)
                {
                    prev_enc_id = pat.encounter_id;
                    count++;
                    Console.Write("\rProcessing patient {0}", count);
                    pat.InitLocArray();
                pat.adt_unit_name = PFSDBUtility.DBToString(dr["ADT_UNIT_NAME"]);
                pat.unit_id = PFSDBUtility.DBToInt(dr["UNIT_ID"]);
                pat.encounter_id = PFSDBUtility.DBToInt(dr["ENCOUNTER_ID"]);
                pat.meth_id = 24; // PFSDBUtility.DBToInt(dr["METHODOLOGY_ID"]);
                pat.acct = PFSDBUtility.DBToString(dr["ACCT_NUMBER"]);
                pat.last_name = PFSDBUtility.DBToString(dr["LAST_NAME"]);
                pat.first_name = PFSDBUtility.DBToString(dr["FIRST_NAME"]);
                pat.middle_name = "";// PFSDBUtility.DBToString(dr["MIDDLE_NAME"]);
                pat.facilty_code = PFSDBUtility.DBToString(dr["CLASSIFICATION_FACILITY_CODE"]);
                pat.unit_name = PFSDBUtility.DBToString(dr["NAME"]);
                pat.room = PFSDBUtility.DBToString(dr["ROOM"]);
                pat.bed = PFSDBUtility.DBToString(dr["BED"]);
                    pat.disch_stat= PFSDBUtility.DBToString(dr["DCSTAT"]);
                    pat.age = PFSDBUtility.DBToDouble(dr["AGE_AT_ADMISSION"]);                
                pat.unit_arrival = PFSDBUtility.DBToDateTime(dr["MINDATETIME_IN"]);
                    VerboseAudit("pat.unit_arrival=MINDATETIME_IN=" + pat.unit_arrival);
                //pat.unit_departure = PFSDBUtility.DBToDateTime(dr["UNIT_DEPARTURE"]);
                pat.unit_departure = PFSDBUtility.DBToDateTime(dr["MAXDATETIME_OUT"]);
                    VerboseAudit("pat.unit_departure=MAXDATETIME_OUT=" + pat.unit_departure);
                    pat.effective = PFSUtility.MaxDateTime(g_effdt, pat.unit_arrival);
                //pat.pull_start = PFSUtility.MaxDateTime(g_pull_start, pat.unit_arrival);
                pat.pull_start = pat.unit_arrival;
                //pat.pull_finish = PFSUtility.MinDateTime(g_pull_finish, pat.unit_departure);
                pat.pull_finish = pat.unit_departure;
                if (pat.pull_finish < pat.pull_start) {
                    pat.pull_finish = g_pull_finish;
                }
                pat.range = (int)PFSUtility.DateDiffInMinutes(pat.unit_arrival, pat.unit_departure);
                pat.los_hours = pat.range / 60.0;
                //pat.TC_source_id = PFSUtility.DBToInt(dr["TC_SOURCE_ID"]);        //TCP port
                //pat.is_ED = PFSUtility.DBToBool(dr["IS_ED"]);
                //pat.is_ICU = PFSUtility.DBToBool(dr["IS_ICU"]);

                // Get the list of default indicators on admission (if any)
                //if (pat.unit_id != prev_unit_id) {
                //    default_inds = GetUnitDefaultIndicators(pat.unit_id, g_pull_start, out default_inds_str);
                //    prev_unit_id = pat.unit_id;
                //}
                //pat.default_inds = default_inds;
                //pat.default_inds_str = default_inds_str;
                
                // Get ready for event log entries for this patient
                gLogUnitID = pat.unit_id;
                gLogEncounterID = pat.encounter_id;
                gLogSourceText = "";

                // Reset both audit strings and make headers for both
                gBriefAudit = new StringBuilder();
                gVerboseAudit = new StringBuilder();
                audit_header =
                    new String('=', 80) + Environment.NewLine +
                    "TRANSPARENT MAPPING AUDIT FILE                    Run Time=" + DateTime.Now + Environment.NewLine +
                    "Version:   " + gLogMapperVersion + Environment.NewLine +
                    "Pull:      " + g_pull_finish + Environment.NewLine +
                    "Effective: " + g_effdt + Environment.NewLine +
                    "Range:     " + g_range + " (" + Math.Round(g_range / 60.0, 1) + " hrs) starting " + g_pull_start;

                // This will add to both brief and verbose audits
                Audit(audit_header);
                Audit(new String('=', 80));
                Audit("Patient " + pat.last_name + ", " + pat.first_name + " " + pat.middle_name +
                    " in " + pat.unit_name + " " + pat.room + " " + pat.bed +
                    " acct=" + pat.acct + " age=" + Math.Round(pat.age, 2));
                Audit("Here from " + pat.pull_start + " to " + pat.pull_finish +
                    "  (LOS=" + Math.Round(pat.los_hours, 2) + ")");
                Audit("pat.unit_departure = " + pat.unit_departure.ToString());
                _pat = pat;

                DateTime evdt = GetEDObsStart();
                //if (pat.encounter_id != prev_enc_id)
                //{
                    bool readyforEDclass = GetLocationHistory(); //sets ready for ed class
                    if (readyforEDclass)
                    {
                        pat.was_admitted=PatientHasInptLocation();
                        // Process this patient
                        // Catch any unexpected errors and continue with next patient.
                        try
                        {
                            // Run the appropriate mapping for this unit and date
                            //
                            //GetPreArrivalItems();

                            switch (pat.meth_id)
                            {
                                case 24:
                                    var etr = new EDTriage();
                                    var ev = new EDVisit();
                                    var ei = new EDInpatient();
                                    etr.ProcessPatient(pat);
                                    if (evdt != DateTime.MinValue && _queryType != 9)
                                    {
                                        pat.unit_departure = evdt;
                                        pat.pull_finish = pat.unit_departure;
                                        pat.range = (int)PFSUtility.DateDiffInMinutes(pat.unit_arrival, pat.unit_departure);
                                        pat.los_hours = pat.range / 60.0;
                                        Audit("REVISED LOS based on Observation Start Time...");
                                        Audit("Here from " + pat.pull_start + " to " + pat.pull_finish +
                                            "  (LOS=" + Math.Round(pat.los_hours, 2) + ")");
                                        _pat = pat;
                                    }
                                    if (_queryType == 9)
                                    {
                                        pat.unit_arrival = GetMREDArrival(pat.unit_departure);
                                        VerboseAudit("pat.unit_departure="+ pat.unit_departure + " ==> pat.unit_arrival=" + pat.unit_arrival);
                                        pat.effective = PFSUtility.MaxDateTime(g_effdt, pat.unit_arrival);
                                        pat.pull_start = pat.unit_arrival;
                                        VerboseAudit("Query Type 99999");
                                        pat.range = (int)PFSUtility.DateDiffInMinutes(pat.unit_arrival, pat.unit_departure);
                                        pat.los_hours = pat.range / 60.0;
                                        Audit("REVISED LOS due to Second ED Stay...");
                                        Audit("Here from " + pat.pull_start + " to " + pat.pull_finish +
                                            "  (LOS=" + Math.Round(pat.los_hours, 2) + ")");
                                        _pat = pat;
                                    }
                                    if (pat.unit_arrival != DateTime.MinValue)
                                    {
                                        pat.meth_id = 24;
                                        ev.ProcessPatient(pat);
                                    }
                                    if (evdt != DateTime.MinValue && _queryType != 9) // only process EDInpt if Obs time != mindt
                                    {
                                        pat.meth_id = 25;
                                        pat.unit_arrival = evdt;
                                        pat.unit_departure = PFSDBUtility.DBToDateTime(dr["MAXDATETIME_OUT"]);
                                        pat.effective = PFSUtility.MaxDateTime(g_effdt, pat.unit_arrival);
                                        pat.pull_start = pat.unit_arrival;
                                        pat.pull_finish = pat.unit_departure;
                                        if (pat.pull_finish < pat.pull_start)
                                        {
                                            pat.pull_finish = g_pull_finish;
                                        }
                                        pat.range = (int)PFSUtility.DateDiffInMinutes(pat.unit_arrival, pat.unit_departure);
                                        pat.los_hours = pat.range / 60.0;
                                        gBriefAudit = new StringBuilder();
                                        gVerboseAudit = new StringBuilder();
                                        audit_header =
                                            new String('=', 80) + Environment.NewLine +
                                            "TRANSPARENT MAPPING AUDIT FILE                    Run Time=" + DateTime.Now + Environment.NewLine +
                                            "Version:   " + gLogMapperVersion + Environment.NewLine +
                                            "Pull:      " + g_pull_finish + Environment.NewLine +
                                            "Effective: " + g_effdt + Environment.NewLine +
                                            "Range:     " + g_range + " (" + Math.Round(g_range / 60.0, 1) + " hrs) starting " + g_pull_start;

                                        // This will add to both brief and verbose audits
                                        Audit(audit_header);
                                        Audit(new String('=', 80));
                                        Audit("Patient " + pat.last_name + ", " + pat.first_name + " " + pat.middle_name +
                                            " in " + pat.unit_name + " " + pat.room + " " + pat.bed +
                                            " acct=" + pat.acct + " age=" + Math.Round(pat.age, 2));
                                        Audit("Here from " + pat.pull_start + " to " + pat.pull_finish +
                                            "  (LOS=" + Math.Round(pat.los_hours, 2) + ")");
                                        _pat = pat;
                                        ei.ProcessPatient(pat);
                                    }
                                    break;
                                case 25:
                                    break;
                                default:
                                    LogWarning("Methodology " + pat.meth_id + " in unit " + pat.unit_name + " is not supported");
                                    break;
                            }

                            gLogUnitID = 0;
                            gLogEncounterID = 0;
                        }
                        catch (Exception e)
                        {
                            LogUnexpectedError(e.Message, e.StackTrace);
                            // Stop or keep going?
                            if (Program.g_stop_on_error) g_abort = true;
                        }
                    }
                } //ready for ED class
                if (g_abort) break;
            }

            Console.WriteLine();
            dr.Close();
            
            if (count < 1) {
                Audit("");
                if(!String.IsNullOrEmpty(_this_acct)) {
                    LogWarning("The selected patient has no chart items in the given time range");
                } else {
                    LogWarning("No chart items found to process - have the unit(s) been enabled for transparent classification?");
                }
            }   
        }

        static bool GetLocationHistory()
        {
            string sql;
            string room;
            string dt;
            bool readyforEDclass = true;

            //    'Gets all the EDParent locations and puts them into an array in the pat record.
            sql = "select el.effective_datetime_in as EFFECTIVE_DATETIME_IN,el.effective_datetime_out,el.room from encounter_location as el";
            sql += " inner join encounter as e on (e.encounter_id=el.encounter_id)";
            sql += " inner join unit as u on (el.unit_id=u.unit_id)";
            sql += " where e.acct_number=" + PFSDBUtility.SQLString(_pat.acct);
            //sql += " and el.unit_id=" + 2817.ToString(); // parent_id;
            sql += " and u.is_ed='y' and u.use_transparent_classification='y' and u.is_active='y'";
            sql += " order by el.effective_datetime_in";
            VerboseAudit("GetLocation sql=" + sql);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd2 = new SqlCommand(sql, db2);
            SqlDataReader dr2 = cmd2.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            _pat.num_locs = 0;
            while (dr2.Read())
            {
                if (_pat.num_locs > 19)
                    VerboseAudit("GetLocationHistory num locations exceeds 20");
                else
                    _pat.num_locs++;
                _pat.locary[_pat.num_locs].time_in = PFSDBUtility.DBToDateTime(dr2["EFFECTIVE_DATETIME_IN"]);
                _pat.locary[_pat.num_locs].time_out = PFSDBUtility.DBToDateTime(dr2["EFFECTIVE_DATETIME_OUT"]);
                _pat.locary[_pat.num_locs].unit_id = 0;
                _pat.locary[_pat.num_locs].toremove = true;
                if (PFSDBUtility.DBToDateTime(dr2["EFFECTIVE_DATETIME_OUT"]) == DateTime.MinValue)
                    readyforEDclass = false;
                //            room = Trim(rs(2))
                //            If(Mid$(room, 1, 3) = "ETU") Then
                //               pat.locary(pat.num_locs).unit_id = vunit_id
                //               pat.locary(pat.num_locs).toremove = False
                //               pat.locary(pat.num_locs).meth_id = METH_ID_ED_VISIT
                //           End If
                //           If(Mid$(room, 1, 3) = "EDO") Then
                //              pat.locary(pat.num_locs).unit_id = iunit_id
                //              pat.locary(pat.num_locs).toremove = False
                //              pat.locary(pat.num_locs).meth_id = METH_ID_ED_INPATIENT
                //              If(Mid$(room, 4, 1) = "C") Then pat.locary(pat.num_locs).txarea = "CENTER"
                //                If(Mid$(room, 4, 1) = "E") Then pat.locary(pat.num_locs).txarea = "EAST"
                //                If(Mid$(room, 4, 1) = "N") Then pat.locary(pat.num_locs).txarea = "NORTH"
                //                If(Mid$(room, 4, 1) = "P") Then pat.locary(pat.num_locs).txarea = "PEDS"
                //                If(Mid$(room, 4, 1) = "S") Then pat.locary(pat.num_locs).txarea = "SOUTH"
                //                If(Mid$(room, 4, 1) = "W") Then pat.locary(pat.num_locs).txarea = "WEST"
                //            End If
                //        End If
                //        rs.MoveNext
            } //dr read
            dr2.Close();
            return readyforEDclass;
        }

        static bool PatientHasInptLocation()
        {
            string sql;
            string room;
            string dt;
            bool readyforEDclass = true;

            //    'Gets all the EDParent locations and puts them into an array in the pat record.
            sql = "select count(*) from encounter_location as el";
            sql += " inner join encounter as e on (e.encounter_id=el.encounter_id)";
            sql += " inner join unit as u on (el.unit_id=u.unit_id)";
            sql += " where e.acct_number=" + PFSDBUtility.SQLString(_pat.acct);
            sql += " and u.is_ed<>'y'";
            VerboseAudit("CountInptLocations sql=" + sql);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd2 = new SqlCommand(sql, db2);
            Int32 count = (Int32)cmd2.ExecuteScalar();
            VerboseAudit("CountInptLocations count=" + count);
            return (count > 0);
        }

            static DateTime GetEDObsStart()
        {
            string sql;
            DateTime evdt = DateTime.MinValue;

            sql = "select max(ci.event_datetime) as EFFECTIVE_DATETIME";
            sql += " from chart_item as ci";
            sql += " inner join encounter as e on (e.encounter_id=ci.encounter_id)";
            sql += " where e.acct_number=" + PFSDBUtility.SQLString(_pat.acct);
            sql += " and ci.code=" + PFSDBUtility.SQLString("ADT10");
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd2 = new SqlCommand(sql, db2);
            SqlDataReader dr2 = cmd2.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr2.Read())
            {
                evdt = PFSDBUtility.DBToDateTime(dr2["EFFECTIVE_DATETIME"]);
            } //dr read
            dr2.Close();
            VerboseAudit("ED Obs Start Time=" + evdt.ToString());
            return evdt;
        }

        static DateTime GetMREDArrival(DateTime depdt)
        {
            string sql;
            DateTime dt = DateTime.MinValue;
            DateTime arrdt = DateTime.MinValue;
            int uid = 0;
            bool done = false;

            sql = "select el.EFFECTIVE_DATETIME_IN,el.unit_id";
            sql += " from ENCOUNTER_LOCATION as el";
            sql += " inner join encounter as e on (e.encounter_id=el.encounter_id)";
            sql += " inner join unit as u on (el.unit_id=u.unit_id)";
            sql += " where e.acct_number=" + PFSDBUtility.SQLString(_pat.acct);
            sql += " and el.EFFECTIVE_DATETIME_OUT <= " + PFSDBUtility.SQLDateTime(depdt);
            sql += " and u.is_ed='y' and u.use_transparent_classification='y' and u.is_active='y'";
            sql += " order by el.EFFECTIVE_DATETIME_IN desc";
            VerboseAudit(sql);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd2 = new SqlCommand(sql, db2);
            SqlDataReader dr2 = cmd2.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr2.Read() && !done)
            {
                uid = PFSDBUtility.DBToInt(dr2["UNIT_ID"]);
                dt = PFSDBUtility.DBToDateTime(dr2["EFFECTIVE_DATETIME_IN"]);
                //if (uid != 2817)
                //{
                //    done = true;
                //    arrdt = dt;
                //}
                done = true;
                arrdt = dt;
            } //dr read
            dr2.Close();
            VerboseAudit("Most Recent ED Arrival=" + arrdt.ToString());
            return arrdt;
        }


        static bool GetPreArrivalItems()
        {
            string sql;
            string room;
            string dt;
            bool readyforEDclass = true;
            int car;

            //  Gets and saves all Chart Items that were sent pre-arrival before the A04.
            //  These are in the form of rejected R01 messages (rejected because pt not yet registered).
            //  Up to 2 hours before A04.

    sql = "select timestamp,";
    sql += " case when CHARINDEX('OBX|1', source_text) > 0 then";
    sql += " ltrim(rtrim(substring(replace(";
    sql += " cast(substring(source_text, CHARINDEX('OBX|1', source_text), 200) as nvarchar(max)), '|',";
    sql += " replicate(cast(' ' as nvarchar(max)), 1000)), 3001, 100))) else null end as OBX1,";

    sql += " case when CHARINDEX('OBX|1', source_text) > 0 then";
    sql += " ltrim(rtrim(substring(replace(";
    sql += " cast(substring(source_text, CHARINDEX('OBX|1', source_text), 200) as nvarchar(max)), '|',";
    sql += " replicate(cast(' ' as nvarchar(max)), 1000)), 5001, 1000))) else null end as OBX1_5,";

    //        sql = sql & "ltrim(rtrim(substring(replace("
    //sql = sql & "cast(substring(source_text, CHARINDEX('PV1|', source_text),200) as nvarchar(max)),'|',"
    //sql = sql & "replicate(cast(' ' as nvarchar(max)),1000)),44001, 1000))) as PV144"


            sql += " case when CHARINDEX('OBX|2', source_text) > 0 then";
    sql += " ltrim(rtrim(substring(replace(";
    sql += " cast(substring(source_text, CHARINDEX('OBX|2', source_text), 200) as nvarchar(max)), '|',";
    sql += " replicate(cast(' ' as nvarchar(max)), 1000)), 3001, 100))) else null end as OBX2,";

            sql += " case when CHARINDEX('OBX|2', source_text) > 0 then";
            sql += " ltrim(rtrim(substring(replace(";
            sql += " cast(substring(source_text, CHARINDEX('OBX|2', source_text), 200) as nvarchar(max)), '|',";
            sql += " replicate(cast(' ' as nvarchar(max)), 1000)), 5001, 1000))) else null end as OBX2_5";

            sql += " from EVENT_LOG where TIMESTAMP between dateadd(hour,-2," + PFSDBUtility.SQLDateTime(_pat.unit_arrival) + ") and " + PFSDBUtility.SQLDateTime(_pat.unit_arrival);
    sql += " and (description like 'R01%' or description like 'O01%') and description like '%" + _pat.acct + "%'";
    sql += " and event_source = 1 and event_type = 4 and event_category = 4";
            Program.VerboseAudit(sql);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd2 = new SqlCommand(sql, db2);
            SqlDataReader dr2 = cmd2.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            var pfs = PFSDBUtility.NewPfsDataContext();
            while (dr2.Read())
            {
                _descript = "";
                if (dr2["OBX1"] != null)
                {
                    // ci.timestamp = PFSDBUtility.DBToDateTime(dr2["TIMESTAMP"]);
                    // ci.event_datetime=ci.timestamp;
                    _fndcode = PFSDBUtility.DBToString(dr2["OBX1"]);
                    car = _fndcode.IndexOf('^', 0);
                    if (car > 0)
                    {
                        _fndcode = _fndcode.Left(car);
                        _result = PFSDBUtility.DBToString(dr2["OBX1_5"]);
                        _descript = _fndcode.Substring(car);
                        SaveChartItem(pfs);
                    }
                    // ci.sequence=seq++;
                }
                if (dr2["OBX2"] != null)
                {
                    // ci.timestamp = PFSDBUtility.DBToDateTime(dr2["TIMESTAMP"]);
                    // ci.event_datetime=ci.timestamp;
                    _fndcode = PFSDBUtility.DBToString(dr2["OBX2"]);
                    car = _fndcode.IndexOf('^', 0);
                    if (car > 0)
                    {
                        _fndcode = _fndcode.Left(car);
                        _result = PFSDBUtility.DBToString(dr2["OBX2_5"]);
                        _descript = _fndcode.Substring(car);
                        SaveChartItem(pfs);
                    }
                    // ci.sequence=seq++;
                }
            } //dr read
            dr2.Close();
            return readyforEDclass;
        }

        static void SaveChartItem(PfsDataContext pfs)
        {
            //DateTime pdate = PFSUtility.ISOToDateTime(_perfdate);
            int ct = 0;
            string sql = "select count(*) from chart_item as ci";
            sql += " inner join encounter as e on (e.encounter_id=ci.encounter_id)";
            sql += " where e.acct_number=" + PFSDBUtility.SQLString(_pat.acct);
            sql += " and ci.code=" + PFSDBUtility.SQLString(_fndcode);
            //Program.VerboseAudit(sql);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd2 = new SqlCommand(sql, db2);
            SqlDataReader dr2 = cmd2.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr2.Read())
            {
                ct = dr2.GetInt32(0);
            } //dr read
            dr2.Close();
            if (ct > 0) return;

            var ci = new CHART_ITEM();
            //string format = "yyyyMMddHHmm";
            //if (_perfdate.Length == 12)
            //    ci.EVENT_DATETIME = DateTime.ParseExact(_perfdate, format, System.Globalization.CultureInfo.InvariantCulture);
            //else
            ci.EVENT_DATETIME = _pat.unit_arrival;
            ci.ENCOUNTER_ID = _pat.encounter_id;
            ci.UNIT_ID = _pat.unit_id;
            ci.CODE = _fndcode;
            Program.VerboseAudit("adding code=" + _fndcode);
            //ci.CATEGORY = _category;
            ci.DESCRIPTION = _descript;
            //ci.FIELD_NAME = _group;
            if (_result == null) _result = "";
            ci.RESULT = _result;
            ci.TIMESTAMP = _pat.unit_arrival;
            ci.SEQUENCE = (short)seq++;
            //ci.SOURCE_TEXT = _sourcefn;

            pfs.CHART_ITEMs.InsertOnSubmit(ci);
            SubmitOverwrite(pfs);

        }

        static void SubmitOverwrite(PfsDataContext pfs)
        {
            bool moreToSubmit = true;
            do
            {
                try
                {
                    pfs.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
                    moreToSubmit = false;
                }
                catch (System.Data.Linq.ChangeConflictException)
                {
                    foreach (System.Data.Linq.ObjectChangeConflict occ in pfs.ChangeConflicts)
                    {
                        // All database values overwrite current values with 
                        //values from database
                        occ.Resolve(System.Data.Linq.RefreshMode.OverwriteCurrentValues);
                    }
                }
            }
            while (moreToSubmit);

        }

        static List<int> GetUnitDefaultIndicators(int unit_id, DateTime pull_dt, out string default_inds_str) 
        {
            var result = new List<int>();                   // make an empty list
            default_inds_str = "<none>";
            var db = PFSDBUtility.NewPfsDataContext();        // look for the unit default admission profile
            var query = from param in db.UNIT_PARAMs
                        from profile in param.PATIENT_PROFILEs
                        where (param.UNIT_ID == unit_id)
                        && (pull_dt >= param.EFFECTIVE_DATETIME) && (pull_dt < param.EXPIRATION_DATETIME)
                        && (profile.PROFILE_NUMBER == param.DEFAULT_ADMISSION_PROFILE)
                        select new {
                            profile.INDICATORS              // comma-separated indicator list
                        };
            foreach (var inds in query) {
                default_inds_str = inds.INDICATORS;
                string s = inds.INDICATORS;
                if (!String.IsNullOrEmpty(s)) {
                    var arr = s.Split(',');
                    foreach (var t in arr) {
                        if (t.IsNumeric()) {                // add an indicator number to the list
                            result.Add(t.ToInteger());
                        }
                    }
                }
            }
            return result;
        }


        //UNITS TO CONSIDER FOR TC
        //parent units:
        //select* from unit where is_ed='y' and is_active = 'y' and parent_unit_id is null and USE_TRANSPARENT_CLASSIFICATION = 'y'

        //select* from unit where PARENT_UNIT_ID=57012483 

        //select* from unit_param where unit_id in (57013039,57013040) and getdate() between EFFECTIVE_DATETIME and EXPIRATION_DATETIME

        //select* from METHODOLOGY 24=visit 25=inpt

        static List<EDchild> GetEDChildren()
        {
            string sql;
//            EDchild childdata = new EDchild();

            var childlist = new List<EDchild>();                   // make an empty list

            sql = "select u1.unit_id as parentid,up.methodology_id,u2.unit_id as childid,u2.name as childname from unit as u1";
            sql += " inner join unit as u2 on (u1.unit_id = u2.PARENT_UNIT_ID)";
            sql += " inner join unit_param as up on (u2.unit_id = up.UNIT_ID)";
            sql += " where u1.is_ed = 'y' and u1.is_active = 'y' and u1.parent_unit_id is null and u1.USE_TRANSPARENT_CLASSIFICATION = 'y'";
            sql += " and u2.is_active = 'y' and u2.USE_TRANSPARENT_CLASSIFICATION = 'y'";
            sql += " and getdate() between up.EFFECTIVE_DATETIME and up.EXPIRATION_DATETIME";
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd2 = new SqlCommand(sql, db2);
            SqlDataReader dr2 = cmd2.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr2.Read())
            {
                EDchild childdata = new EDchild();
                childdata.parent_unitid = PFSDBUtility.DBToInt(dr2["parentid"]);
                childdata.meth_id = PFSDBUtility.DBToInt(dr2["METHODOLOGY_ID"]);
                childdata.child_unitid = PFSDBUtility.DBToInt(dr2["childid"]);
                childdata.child_name = PFSDBUtility.DBToString(dr2["childname"]);
                //VerboseAudit(childdata.child_name);
                childlist.Add(childdata);
            } //dr read
            dr2.Close();
            //VerboseAudit("ED Obs Start Time=" + evdt.ToString());
            return childlist;
        }
        static void ShowEDChildren()
        {
            foreach (var item in EDchildren)
            {
                VerboseAudit(item.parent_unitid + " / " + item.meth_id + " / " + item.child_unitid + " / " + item.child_name);
            }
        }

      

        static void DeleteOldChartItems()
        {
            string sql;
            DateTime dt;

            if (g_no_output || g_no_delete) return;

            DebugTrace("About to delete old char items...");
            DebugPause();                                       // give a chance to ^C in debug mode

            LogInfo("Delete old chart items...");
            dt = g_effdt.AddDays(-CHART_ITEM_LIFE);
            sql = "DELETE FROM CHART_ITEM WHERE EVENT_DATETIME < " + PFSDBUtility.SQLDateTime(dt);
            PFSDBUtility.ExecuteSQL(sql);
            LogInfo("Done");
        }

        static void MaybeRunImport()
        {
            if (! g_import_when_done) return;
            
            string path = Path.GetDirectoryName(Application.ExecutablePath);
            if (path.Right(3) != "bin") {                       // running in visual studio?
                path = @"C:\qmdev\AcuityPlus\main\bin";
            }
            if (Directory.Exists(path))
            {
                LogInfo("Running transparent import...");
                // Import will add its own log entries; no need to add any here
                int rc = PFSUtility.ExecuteAndWait(Path.Combine(path, PFSGlobal.TRANSPARENT_IMPORT_EXE_NAME));
                LogInfo("Done; result=" + rc);
            }
            else
            {
                LogWarning("Can't find " + path);
            }
        }



        //=====================================================================
        // Audits and log files
        //=====================================================================
        // Print to console if debug is set
        public static void DebugTrace(string format, params object[] values)
        {
            if (g_debug)
            {
                Console.Write(DateTime.Now.ToString() + " ");
                Console.WriteLine(format, values);
            }
        }

        public static void DebugPause()
        {
            if (g_debug)
            {
                Console.Write("Press any key...");
                Console.ReadKey();
                Console.WriteLine("");
            }
        }

        // Save in both audit files
        static public void Audit(string s)
        {
            DebugTrace(s);
            if (logfile != null) logfile.WriteLine(s);
            gBriefAudit.AppendLine(s);                  // Add to both audit reports
            gVerboseAudit.AppendLine(s);
        }

        // Save in verbose audit only
        static public void VerboseAudit(string s)
        {
            DebugTrace(s);
            if (logfile != null) logfile.WriteLine(s);
            gVerboseAudit.AppendLine(s);                // Add to verbose audit only
        }

        public static void AddLogEntry(PFSEventLog.EventLogType type, string msg, PFSEventLog.EventLogCategory category)
        {
            Audit(msg);

            PFSEventLog.AddEventLogEntry(
                PFSEventLog.EventLogSource.EVENT_SOURCE_TRANSPARENT_MAPPING,
                type, category, msg, gLogSourceText, 0, gLogUnitID, gLogEncounterID);
        }

        public static void LogInfo(string msg, PFSEventLog.EventLogCategory category)
        {
            AddLogEntry(PFSEventLog.EventLogType.EVENT_TYPE_INFO, msg, category);
        }
        public static void LogInfo(string msg)
        {
            LogInfo(msg, PFSEventLog.EventLogCategory.EVENT_CATEGORY_PROCESSED);
        }

        public static void LogWarning(string msg, PFSEventLog.EventLogCategory category)
        {
            AddLogEntry(PFSEventLog.EventLogType.EVENT_TYPE_WARNING, msg, category);
        }
        public static void LogWarning(string msg)
        {
            LogWarning(msg, PFSEventLog.EventLogCategory.EVENT_CATEGORY_VALIDATION);
        }

        public static void LogError(string msg, PFSEventLog.EventLogCategory category)
        {
            AddLogEntry(PFSEventLog.EventLogType.EVENT_TYPE_ERROR, msg, category);
        }
        public static void LogError(string msg)
        {
            LogError(msg, PFSEventLog.EventLogCategory.EVENT_CATEGORY_VALIDATION);
        }
        public static void LogUnexpectedError(string msg, string stack_trace)
        {
            // Add the message and stack trace to event log; message only goes to screen
            gLogSourceText = stack_trace;
            AddLogEntry(PFSEventLog.EventLogType.EVENT_TYPE_ERROR, msg, PFSEventLog.EventLogCategory.EVENT_CATEGORY_UNEXPECTED);
            gLogSourceText = "";
            
            // Add the stack trace to screen and log files
            Audit(msg);
            Audit(stack_trace);
        }

    }
}
