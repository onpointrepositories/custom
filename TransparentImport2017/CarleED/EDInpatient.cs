﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;                     // for LINQ; add a reference
using System.Data.SqlClient;                // for SqlConnection
using System.IO;                            // for Directory, Path, Stream
using PfsShared;                            // add a reference to Shared2 project

// ============================================================================
// ED Inpatient transparent mapping -- GOES HERE --
// Mt Sinai (from Mayo)
// ============================================================================
// This processes one patient.  Only one database query is used.  :)
//
// All search functions use exact match for category, description and field name.
// Codes and results can be a single word or a list of words.
// Codes default to exact match; prefix with CODE_CODE_LIKE_PREFIX if desired (single word only)
// Results default to like matching; prefix with RESULT_EXACT_MATCH_PREFIX if desired (single word only)
//
// All searches are case insensitive.
//
//
namespace TransparentMapping
{
    class EDInpatient
    {
        private const int MAX_INDS = 50;
        private const int MAX_PROCS = 20;

        private const string CODE_LIKE_PREFIX = "%!";       // use to make codes %like%
        private const string EXACT_MATCH_PREFIX = "&!";     // use to make desc/result exact match
        private const string CHAR_COMMA = "||";             // use to insert literal comma in word list
        private const string NOT_PREFIX = "!!";             // use to insert literal comma in word list
        private const string EXACT_SQL_PREFIX = "!SQL!";     // use to give sql constraint as given
        private const string AVOID_NEGATIVE = "!;";
        private const string STARTS_WITH = "[!";     // use to give sql constraint as given

        private struct indicator_data {
            public bool is_checked;
            public int radio_group;
        }

        private struct proc_data {
            public int procedure_number;
            public DateTime start;
            public DateTime finish;
        }
        private struct gBucket
        {
            public int bucket;
            public string code;
            public DateTime evdt;
            public bool using_waiver;
        }

        // These are database CHART_ITEMs for this patient
        private CHART_ITEM[] _chart_items_since_admission;
        private CHART_ITEM[] _chart_items_since_unit_arrival;
        private CHART_ITEM[] _chart_items_during_pull_period;
        private CHART_ITEM[] _chart_items_pull_period_plus;
        // These are database PROCEDURE_EVENTs for this patient
        private PROCEDURE_EVENT[] _procedure_events;

        private indicator_data[] _inds;
        private List<proc_data> _procs;
        private List<proc_data> _outcomes;

        private const int DEFAULT_BUCKET_SIZE = 20;       //min

        private PatientInfo _pat;
        private int _bucket_size = DEFAULT_BUCKET_SIZE;
        private bool is_default;
        private bool isEDonly = false;


        private enum SearchDepth {
            SearchDefault,
            SearchPullRange,            //search within the current pull     -- default
            SearchSinceArrival,         //search since arrival to the unit
            SearchSinceAdmission,        //search everything since admission to the hospital
            SearchPullPlus
        }

        private enum CountMode {
            CountAll,
            CountFirst                  //stop after one is found
        }

        private enum GetValueMode {
            GetTotal,
            GetMax,
            GetLast
        }

        enum Frequencies {
            QNONE,
            Q1H,
            Q30M
        }

        private struct fmapRow {
            public double los_high;                       //the LOS being testing
            public int[] freq;                           //the count required for each Q value
        }
        private List<fmapRow> _freq_map;                    //1,2,4,8,12,24 hours


        //
        // This is the main entry point
        //
        public void ProcessPatient(PatientInfo pat)
        {
            _pat = pat;

            InitIndicators(); // sets is_default
            InitProcs();
            isEDonly = OnlyHasED();
            if (!is_default)
            {
                LoadFreqTable();
                LoadPatientChart();
                //123 4 5 67 89 10 1112 13 14 151617
                Check_123();
                Check_4();
                Check_5();
                Check_67();
                Check_89();
                Check_10();
                Check_1112();
                Check_13();
                Check_14();
                Check_151617();
            }

            HighestIndicatorInEachGroupWins();

            //if (!is_default)
            //{
            //    CheckProcs();
            //    CheckOutcomes();
            //}

            if (Program.g_no_output) return;
            OutputClass();
            //OutputProcs();
            //OutputOutcomes();
        }


        private void InitIndicators()
        {
            // Make a new (empty) indicator array
            _inds = new indicator_data[MAX_INDS + 1];           // This 1 based so add one
            is_default = false;
            //if (_pat.los_hours <= 4.0) {
            //    is_default = true;
            //    Program.VerboseAudit("Patient was here 4 hrs or less. Will receive default indicators " + _pat.default_inds_str);
            //    foreach (var ind in _pat.default_inds) {
            //        if (ind <= _inds.GetUpperBound(0)) {
            //            _inds[ind].is_checked = true;
            //        }
            //    }
            //}


            // get indicator radio groups from the database
            // ** (This database access can be replaced once we have a C# methodolgy cache)
            var db = PFSDBUtility.NewPfsDataContext();
            var query = from idef in db.INDICATOR_DEFINITIONs
                        where (idef.METHODOLOGY_ID == _pat.meth_id)
                        select idef;
            foreach (var idef in query) {
                if (idef.INDICATOR_NUMBER <= _inds.GetUpperBound(0)) {
                    // (convert nulls to zero)
                    _inds[idef.INDICATOR_NUMBER].radio_group = PFSDBUtility.DBToInt(idef.RADIO_GROUP);
                }
            }
        }

        private void InitProcs()
        {
            _procs = new List<proc_data>();
            _outcomes = new List<proc_data>();
        }

        private fmapRow LoadFreqTableRow(double los_high, string values)
        {
            fmapRow fmrow;

            fmrow.los_high = los_high;
            fmrow.freq = new int[Enum.GetNames(typeof(Frequencies)).Length];

            var arr = values.Split(',');
            for (int i = 0; i <= fmrow.freq.GetUpperBound(0); i++)
            {
                fmrow.freq[i] = arr[i].ToInteger();
            }
            return fmrow;
        }

        // These are the number of times you need to do something during a certain time period (LOS)
        // in order for it to be considered one of the frequencies listed.
        private void LoadFreqTable()
        {
            _freq_map = new List<fmapRow>();
            //                              LOS,  None Q1h Q30m
            _freq_map.Add(LoadFreqTableRow(1, "   0,  1, 2"));
            _freq_map.Add(LoadFreqTableRow(3, "   0,  2, 4"));
            _freq_map.Add(LoadFreqTableRow(6, "   0,  3, 6"));
            _freq_map.Add(LoadFreqTableRow(12, "   0,  6, 12"));
            _freq_map.Add(LoadFreqTableRow(24, "   0,  12, 24"));
            _freq_map.Add(LoadFreqTableRow(48, "   0,  24, 48"));
            _freq_map.Add(LoadFreqTableRow(72, "   0,  36, 72"));
            _freq_map.Add(LoadFreqTableRow(96, "   0,  48, 96"));
            _freq_map.Add(LoadFreqTableRow(192, "  0,  96, 192"));
            _freq_map.Add(LoadFreqTableRow(384, "  0,  192, 384"));
            _freq_map.Add(LoadFreqTableRow(768, "  0,  384, 768"));
        }

        private Frequencies FreqForCount(double los_hours, int count)
        {
            foreach (var fmrow in _freq_map) {
                if (los_hours <= fmrow.los_high) {
                    // Option: pro-rate the count upward if the LOS is less than the row we are interested in.
                    //         This will bump the count to what it might have been at the full LOS.
                    // Note: truncate the result; rounding inflates the value too much.
                    int prorated_count = (int)((fmrow.los_high / los_hours) * count);

                    // foreach goes low to high; go from high to low instead
                    for (int j = (int)Frequencies.Q30M; (j >= (int)Frequencies.QNONE); j--) { //search right to left
                        if (prorated_count >= fmrow.freq[j]) {
                            return (Frequencies)j;
                        }
                    } // next j
                }
            }

            return Frequencies.QNONE;
        }

        //
        // Load patient chart from the database (one DB access)
        // Load everything since admission
        //
        private void LoadPatientChart()
        {
            // Get the entire patient chart (all units and dates for this patient)
            var db = PFSDBUtility.NewPfsDataContext();
            var query = from item in db.CHART_ITEMs
                        where (item.ENCOUNTER_ID == _pat.encounter_id)
                        select item;
            // Save the result
            _chart_items_since_admission = query.ToArray();

            // Convert all results to lower case for case insensitive comparisons
            foreach (var item in _chart_items_since_admission) {
                if (item.CATEGORY != null) item.CATEGORY = item.CATEGORY.ToLower();
                if (item.CODE != null) item.CODE = item.CODE.ToLower();
                if (item.DESCRIPTION != null) item.DESCRIPTION = item.DESCRIPTION.ToLower();
                if (item.FIELD_NAME != null) item.FIELD_NAME = item.FIELD_NAME.ToLower();
                if (item.RESULT != null) item.RESULT = item.RESULT.ToLower();
            }

            // Prepare two more versions of the chart
            var query2 = from item in _chart_items_since_admission
                         where (item.EVENT_DATETIME >= _pat.unit_arrival) && (item.EVENT_DATETIME <= _pat.pull_finish)
                         select item;
            _chart_items_since_unit_arrival = query2.ToArray();

            query2 = from item in _chart_items_since_admission
                     where (item.EVENT_DATETIME >= _pat.pull_start) && (item.EVENT_DATETIME <= _pat.pull_finish)
                     select item;
            _chart_items_during_pull_period = query2.ToArray();
            query2 = from item in _chart_items_since_admission
                     where (item.EVENT_DATETIME >= _pat.pull_start) && (item.EVENT_DATETIME <= _pat.pull_finish.AddHours(4))
                     select item;
            _chart_items_pull_period_plus = query2.ToArray();
        }

        // Delay looking for patient procedures until they are wanted
        private void LoadPatientProceduresIfNeeded()
        {
            if (_procedure_events != null) return;

            var db = PFSDBUtility.NewPfsDataContext();
            var query = from proc in db.PROCEDURE_EVENTs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                        select proc;
            _procedure_events = query.ToArray();
        }



        // Started a new chart item query (of a certain depth)
        // Default is the chart during the pull range.  Options for unit arrival and admission to hospital.
        private IEnumerable<CHART_ITEM> StartNewQuery()
        {
            if (isEDonly)
                return StartNewQuery(SearchDepth.SearchSinceAdmission);
            else
                return StartNewQuery(SearchDepth.SearchDefault);
        }
        private IEnumerable<CHART_ITEM> StartNewQuery(SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;

            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    return (from item in _chart_items_during_pull_period select item);
                case SearchDepth.SearchSinceArrival:
                    return (from item in _chart_items_since_unit_arrival select item);
                case SearchDepth.SearchSinceAdmission:
                    return (from item in _chart_items_since_admission select item);
                case SearchDepth.SearchPullPlus:
                    return (from item in _chart_items_pull_period_plus select item);
            }
            return null;
        }

        // Is this search term a word or a list of words?
        private bool ValueIsAList(string s)
        {
            return (s != null) && s.Contains(',');                //is this a comma-separated list?
        }

        // Break apart a wordlist and prepare its members
        private string[] SplitOnCommaAndPrepareElements(string s)
        {
            // If a word list has a space after each comma, we will get leading blanks in each term
            var arr = s.Split(',');
            for (int i = 0; i <= arr.GetUpperBound(0); i++) {
                arr[i] = arr[i].Trim();                         // get rid of leading blanks
                arr[i] = arr[i].Replace(CHAR_COMMA, ",");       // insert a literal comma into search word
                arr[i] = arr[i].ToLower();                      // convert for case-insensitive comparisons
            }
            return arr;
        }

        // Add a basic chart item filter to a LINQ query
        //
        // cat = exact match
        // code_list = exact match; optional "like" match
        // desc_list = "like" match; optional exact match
        // field = exact match
        // result_list = "like" match; optional exact match
        //
        // These are comma-separated lists.  
        // Search for a literal comma by using CHAR_COMMA in the string.
        private IEnumerable<CHART_ITEM> AndItemFilter(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list)
        {
            // Converted all values to lower case for case-insensitive comparisons.
            if (!String.IsNullOrEmpty(cat)) {
                cat = cat.ToLower();
                query = query.Where(e => e.CATEGORY == cat);
            }

            if (!String.IsNullOrEmpty(code_list)) {
                code_list = code_list.ToLower();
                if (code_list.Left(2) == EXACT_MATCH_PREFIX) {          // exact match override?
                    query = query.Where(e => e.CODE == code_list.Substring(2));
                } else if (code_list.Left(2) == CODE_LIKE_PREFIX)
                {            // override for "like" match?
                    query = query.Where(e => e.CODE.Contains(code_list.Substring(2)));
                } else if (ValueIsAList(code_list)) {
                    query = AndCodeInList(query, code_list);            // find one of the words
                } else {
                    query = query.Where(e => e.CODE.Contains(code_list));      // find this word
                }
            }

            if (!String.IsNullOrEmpty(desc_list)) {
                bool special1 = false; // (desc_list == ";ROUTE=IV");
                desc_list = desc_list.ToLower();
                if (desc_list.Left(2) == EXACT_MATCH_PREFIX) {          // exact match override?
                    query = query.Where(e => e.DESCRIPTION == desc_list.Substring(2));
                } else if (ValueIsAList(desc_list)) {
                    query = AndDescriptionInList(query, desc_list);
                } else {
                    desc_list = desc_list.Replace(CHAR_COMMA, ",");     // look for literal comma requests
                    query = query.Where(e => e.DESCRIPTION.Contains(desc_list));
                    if (special1)
                    {
                        query = query.Where(e => !e.DESCRIPTION.Contains(";ROUTE=IV SLOW PUSH"));
                        query = query.Where(e => !e.DESCRIPTION.Contains(";ROUTE=IV PUSH"));
                    }

                }
            }

            if (!String.IsNullOrEmpty(field)) {
                field = field.ToLower();
                query = query.Where(e => e.FIELD_NAME == field);
            }

            if (!String.IsNullOrEmpty(result_list)) {
                result_list = result_list.ToLower();                        // make all results case insensitve
                if (result_list.Left(2) == EXACT_MATCH_PREFIX) {     // exact match override?
                    query = query.Where(e => e.RESULT == result_list.Substring(2));
                }
                else if (result_list.Left(2) == AVOID_NEGATIVE)
                {
                    query = query.Where(e => (!e.RESULT.ToLower().Contains("no " + result_list.Substring(2))) && (e.RESULT.ToLower().Contains(result_list.Substring(2)))); // == result_list.Substring(2)) || (e.RESULT.Contains(";" + result_list.Substring(2)))));
                }
                else if (ValueIsAList(result_list)) {
                    query = AndResultInList(query, result_list);
                }
                else if (result_list.Left(2) == STARTS_WITH)
                {
                    query = query.Where(e => e.RESULT.StartsWith(result_list.Substring(2)));
                }
                else
                {
                    result_list = result_list.Replace(CHAR_COMMA, ",");     // look for literal comma requests
                    query = query.Where(e => e.RESULT.Contains(result_list));
                }
            }
            return query;
        }

        // Look for a code that is one of the words in the list.
        private IEnumerable<CHART_ITEM> AndCodeInList(IEnumerable<CHART_ITEM> query, string code_list)
        {
            if (String.IsNullOrEmpty(code_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(code_list);
            return query.Where(e => e.CODE.ContainsAny(arr));  // use like match. Exact match = arr.Contains(e.CODE));   
        }

        // Look for a description that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndDescriptionInList(IEnumerable<CHART_ITEM> query, string desc_list)
        {
            if (String.IsNullOrEmpty(desc_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(desc_list);
            return query.Where(e => e.DESCRIPTION.ContainsAny(arr));    // "like" match
        }

        // Look for a result that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultInList(IEnumerable<CHART_ITEM> query, string result_list)
        {
            bool use_start_with = false;

            if (String.IsNullOrEmpty(result_list)) return query;

            if (result_list.Left(2) == STARTS_WITH)
            {
                use_start_with = true;
                result_list = result_list.Substring(2);
            }

            var arr = SplitOnCommaAndPrepareElements(result_list);

            if (use_start_with)
                return query.Where(e => arr.Any(item => e.RESULT.StartsWith(item)));
            else
                return query.Where(e => e.RESULT.ContainsAny(arr)); // "like" match
        }

        // Look for a result that does not contain one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultNotInList(IEnumerable<CHART_ITEM> query, string result_list)
        {
            if (String.IsNullOrEmpty(result_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(result_list);
            return query.Where(e => !e.RESULT.ContainsAny(arr));        // "like" match
        }

        private string DescribeSearchDepth(SearchDepth search_depth)
        {
            string result = "";
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;

            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    //result = "in pull range"
                    result = "";                         //be quiet since this is default
                    break;
                case SearchDepth.SearchSinceArrival:
                    result = "since arrival to unit";
                    break;
                case SearchDepth.SearchSinceAdmission:
                    result = "since admission";
                    break;
                case SearchDepth.SearchPullPlus:
                    result = "in LOS range plus 4 hours after";
                    break;
            }

            return result;
        }

        // Describe a query that has already been run
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Make a simple query and describe the results
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            var arr = query.ToArray();
            return Describe(arr, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private string LookingFor(string so_far, string what, string op, string value)
        {
            string result;
            if (String.IsNullOrEmpty(value)) return so_far;

            if (so_far == "")
                result = "looking for " + what;
            else
                result = so_far + " and " + what;

            if (value.Left(2) == EXACT_MATCH_PREFIX)
                result += "='" + value.Substring(2) + "'";
            else if (value.Left(2) == CODE_LIKE_PREFIX)
                result += " contains " + "'" + value.Substring(2) + "'";
            else if (ValueIsAList(value))
                result += " in '" + value + "'";
            else
                result += op + "'" + value + "'";

            return result;
        }
        private string Describe(CHART_ITEM[] arr, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            string result = "";
            result = LookingFor(result, "cat", "=", cat);
            result = LookingFor(result, "code", "=", code_list);
            result = LookingFor(result, "desc", " contains ", desc_list);
            result = LookingFor(result, "field", "=", field);
            result = LookingFor(result, "result", " contains ", result_list);

            if (arr.Count() == 0) {
                result += "; not found " + DescribeSearchDepth(search_depth);
            } else {
                var e = arr[0];
                result = "FOUND: " + result + "; found";
                // We might have searched for a pattern or word list in several fields - show what was found
                if (e.CATEGORY != null) result += " cat='" + e.CATEGORY + "'";
                if (e.CODE != null) result += " code='" + e.CODE + "'";
                if (e.DESCRIPTION != null) result += " desc='" + e.DESCRIPTION + "'";
                if (e.FIELD_NAME != null) result += " field='" + e.FIELD_NAME + "'";
                if (e.RESULT != null) result += " result='" + e.RESULT + "'";
                // Are there more results?  Just say how many; we aren't going to list them.
                if (arr.Count() == 2) {
                    result += " (1 more result)";
                } else if (arr.Count() > 2) {
                    result += " (" + (arr.Count() - 1) + " more results)";
                }
            }

            return result;
        }


        //Set an indicator for this reason (low level)
        private void SetInd(int inum, string reason)
        {
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Set Ind #" + inum + ": " + reason);          //already set - repeat for verbose only
            } else {
                _inds[inum].is_checked = true;
                Program.Audit("Set Ind #" + inum + ": " + reason);
            }
        }

        //Clear an indicator for this reason (low level)
        private void ClrInd(int inum, string reason)
        {
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Clr Ind #" + inum + ": " + reason);          //already clear - repeat verbose only
            } else {
                _inds[inum].is_checked = false;
                Program.Audit("Clr Ind #" + inum + ": " + reason);
            }
        }

        // Count how many items meet the conditions
        // All items are listed
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountItems(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            int count = query.Count();

            // always return what was found
            found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
            // echo the result?
            if (trace) Program.VerboseAudit(found_what);

            return count;
        }

        // Count how many items meet the conditions
        // This has the option to list every entry or give a total
        private int CountResultInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what = "";

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);

            var arr = SplitOnCommaAndPrepareElements(result_list);

            foreach (var item in query) {
                // Figure out which of the search words were found
                var s = item.RESULT.ContainsWhich(arr);
                found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' code='" + item.CODE + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "'";
                if (trace) Program.VerboseAudit(found_what);
                count++;
                if (count_mode != CountMode.CountAll) break;        //stop counting
            }

            if (count > 0) {
                //We already printed what was found; maybe add how many?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        // Count how many items meet the conditions
        // This has the option to list every entry or give a total
        private int CountResultInListEXCEPTList(string cat, string code_list, string desc_list, string field, string result_list, string negresult_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what = "";

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            query = AndResultNotInList(query, negresult_list);

            var arr = SplitOnCommaAndPrepareElements(result_list);

            foreach (var item in query)
            {
                // Figure out which of the search words were found
                var s = item.RESULT.ContainsWhich(arr);
                found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' code='" + item.CODE + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "'";
                if (trace) Program.VerboseAudit(found_what);
                count++;
                if (count_mode != CountMode.CountAll) break;        //stop counting
            }

            if (count > 0)
            {
                //We already printed what was found; maybe add how many?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            }
            else
            {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        // Count how many items contain ALL results in list
        private int CountResultInListAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what = "";

            var arr = SplitOnCommaAndPrepareElements(result_list);

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            for (int i = 0; i <= arr.GetUpperBound(0); i++)
            {
                if (arr[i].StartsWith(NOT_PREFIX))
                {
                    query = query.Where(e => !e.RESULT.Contains(arr[i].Substring(2)));
                }
                else
                {
                    query = query.Where(e => e.RESULT.Contains(arr[i]));
                }
            }

            count = query.Count();

            if (count > 0)
            {
                found_what = "found item with all results in '" + result_list + "' without those prefixed by !!";
                if (trace) Program.VerboseAudit(found_what);
            }
            else
            {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            if (ValueIsAList(result_list))
            {
                return CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what);
            } else {
                return CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what);
            }
        }

        // Is there an item with this result?
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContains(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            if (ValueIsAList(result_list))
            {
                return (CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            } else {
                return (CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
            }
        }

        // Result contains all in list
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            if (ValueIsAList(result_list))
            {
                return (CountResultInListAll(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            }
            else
            {
                found_what = "Not valid for non-list results.";
                return false;
            }
        }
        // Count how many items meet the conditions, with none of the results in result_list.
        // This has the option to say how many were found rather than list all.
        private int CountResultNotInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0, rec_count = 0;
            bool found_one;

            found_what = "";
            var arr = SplitOnCommaAndPrepareElements(result_list);         // also deals with CHAR_COMMA

            //Do not filter by result here; we need all results
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            foreach (var item in query) {
                rec_count++;
                //Look for each search word in the result
                found_one = false;

                foreach (string s in arr) {
                    if (String.Equals(item.RESULT, s)) {
                        found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "' -- ignore this";
                        Program.VerboseAudit(found_what);
                        found_one = true;
                        break;
                    }
                }

                if (!found_one) {
                    //none of the words were found - good!
                    found_what = Describe(cat, code_list, desc_list, field, "", search_depth) + " -- does not contain '" + result_list + "'";
                    Program.VerboseAudit(found_what);
                    count++;
                    if (count_mode != CountMode.CountAll) break;
                }
            }

            if (count > 0) {
                //We already printed what was found; maybe show the total?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else if (rec_count > 0) {
                //We already printed what we ignored
            } else {
                //Print what did not exist
                found_what = Describe(cat, code_list, desc_list, field, "", search_depth);      //not found
                Program.VerboseAudit(found_what);
            }

            return count;
        }

        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, string found_what)
        {
            // Search in the pull range with trace on
            return CountResultDoesNotContain(cat, code_list, desc_list, field, result_list, out found_what, SearchDepth.SearchDefault, true);
        }
        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, out string found_what, SearchDepth search_depth, bool trace)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            return CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0;
        }

        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            return (CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0);
        }

        // Set the indicator if the conditions are met
        //
        private bool SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);

        }
        private bool SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            bool b = false;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return true;

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what)) {
                SetInd(inum, found_what);                          //echo here - found
                b = true;
            } else {
                Program.VerboseAudit(found_what);                  //and here - not found
            }
            return b;
        }
        // Special contains all
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContainsAll(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return;

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                          //echo here - found
            }
            else
            {
                Program.VerboseAudit(found_what);                  //and here - not found
            }
        }
        // Set the indicator if the result does not contain any of the words in result_list
        //
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultDoesNotContain(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return;

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            //Turn trace off for ResultDoesNotContain() and echo what was set below with SetInd
            if (ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                           //echo here - not found
            } else {
                Program.VerboseAudit(found_what);                   //and here - found
            }
        }

        // Clear the indicator if the result contains one of the words in the result_list
        //
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already clear
            if (!_inds[inum].is_checked) return;

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            //Turn trace off for ResultContains() and echo what was set below with SetInd
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what)) {
                ClrInd(inum, found_what);                           //echo here - found
            } else {
                Program.VerboseAudit(found_what);                   //and here - not found
            }
        }


        //These slightly smaller functions are meant for places where you aren't really looking for a chart result
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            return (CountResultContains(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
        }

        // Variations with optional parameters (can switch to optional parameters in VS 2010)
        //
        private void SetIndIfFound(int inum, string cat)
        {
            SetIndIfResultContains(inum, cat, "", "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list)
        {
            SetIndIfResultContains(inum, cat, code_list, "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private void ClrIndIfFound(int inum, string cat)
        {
            ClrIndIfResultContains(inum, cat, "", "", "", "", SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }


        // Get the max/total value from a result (usually in the middle of the text)
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetIntValue(get_mode, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            int value;
            int result = 0;
            bool found_one = false;

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);

            //Look for a number in the result

            foreach (var item in query) {
                // RESULT might be null so convert to empty string so Split won't blow up
                var arr = PFSDBUtility.DBToString(item.RESULT).Split(' ');
                foreach (string s in arr) {
                    //Look for 1st character numeric; IsNumeric("60min") will fail so look at 1st char only
                    if (s.Left(1).IsNumeric()) {
                        value = (int)s.Val();                           //Use Val; ToInteger will error on "60min"
                        switch (get_mode) {
                            case GetValueMode.GetMax:
                                result = Math.Max(value, result);       //max
                                break;
                            case GetValueMode.GetTotal:
                                result += value;                        //total
                                break;
                            case GetValueMode.GetLast:
                                result = value;                         //last
                                break;
                        }

                        // print what we are searching for (the first time)
                        if (!found_one) {
                            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
                        }
                        found_one = true;
                        // print each value found
                        Program.VerboseAudit("  found numeric value " + result);
                        //Keep going in case there are more
                    }
                }
            }

            if (!found_one) {
                //show what was not found
                Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
            }

            return result;
        }

        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetMaxValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            return GetIntValue(GetValueMode.GetMax, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetTotalValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            return GetIntValue(GetValueMode.GetTotal, cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Get a result; returns true if found with return_result
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result)
        {
            return GetResult(cat, code_list, desc_list, field, out return_result, SearchDepth.SearchDefault);
        }
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0) {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
            } else {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }
        private void GetResultforOther(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "Other");
            return_result = "";
            foreach (var item in query)
            {
                if (!String.Equals(item.RESULT, "Other"))
                {
                    return_result = item.RESULT;
                    //Program.VerboseAudit(found_what);
                    break;
                }

            }
            return;
        }

        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, out string return_result, out DateTime return_evdt)
        {
            return GetResultAndEVDT(cat, code_list, desc_list, field, out return_result, out return_evdt, SearchDepth.SearchDefault);
        }
        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, out string return_result, out DateTime return_evdt, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
                return_evdt = PFSDBUtility.DBToDateTime(query.First().EVENT_DATETIME);
            }
            else
            {
                return_result = "";
                return_evdt = DateTime.MinValue;
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }


        //======================================================================================
        // Mapping starts here
        //======================================================================================

        //1	ADL - Self Care	1
        //2	ADL - Assist	1
        //3	ADL - Extended	1
        //4	ADL - Two or More	NULL
        //5	Communication Support	NULL
        //6	Safety Management - q 30 Minutes	6
        //7	Safety Management - q 15 Minutes	6
        //8	Behavior/Emotional Management	8
        //9	Behavior/Emotional Management >= 30 Minutes	8
        //10	Fluid Management	NULL
        //11	Physiologic Assessment - q 1 Hour	11
        //12	Physiologic Assessment - q 30 Minutes	11
        //13	Wound/Injury Management	NULL
        //14	Medication Management >= 5 Medications	14
        //15	Admitted	15
        //16	Transferred to another Facility	15
        //17	Expired	15

        private void Check_123()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Inpt 1. ADL - Self Care");
            Program.VerboseAudit("ED Inpt 2. ADL - Assist");
            Program.VerboseAudit("ED Inpt 3. ADL - Extended");
            Program.VerboseAudit("---------------");

            string reslist, res;
            reslist = "Ambulate in hall,Ambulate in room,Bathroom privileges,";
            reslist += "Bedrest,Bike,Chair,Commode,Dangle,Supervised exercise,Held,";
            reslist += "Stand at bedside,Tilt table,Turn,Up in chair,Wagon,In bed,bedpan";
            SetIndIfResultContains(2, "", "9990000305560", "", "", reslist);

            reslist = "1 Assist,2 Assist,3 Assist,>4 Assist,Supervision required,4 or more Assist";
            SetIndIfResultContains(2, "", "9993040109530", "", "", reslist);

            reslist = "Lying right side,Lying left side,Supine,Semi Fowler,Pillow support,Prone,";
            reslist += "Sitting,Standing,Up in chair,Continuous lateral rotation,Micro turn left,";
            reslist += "Micro turn right,Do Not Turn,Unstable to turn,turn right side,turn left side";
            SetIndIfResultContains(2, "", "9990000400604", "", "", reslist);

            reslist = "Assist,Total Care";
            SetIndIfResultContains(2, "", "9991025006475", "", "", reslist);

            reslist = "Left side,Left tilt,Right side,Right tilt,Semi-fowler,Fowler,Knee chest,";
            reslist += "Chair,Rocking chair,Supine,Trendelenberg,Squatting,Standing,Kneeling,Walking,Prone";
            SetIndIfResultContains(2, "", "1028000027", "", "", reslist);

            reslist = "1 Assist,2 Assist,3 Assist,>4 Assist,Supervised (Comment)";
            SetIndIfResultContains(2, "", "9990007060350", "", "", reslist);

            reslist = "Bathed,Chlorhexidine,Shower,Foley care,Peri care,";
            reslist += "Hair washed,Hair dried or curled,Shaved";
            SetIndIfResultContains(2, "", "9990000342030", "", "", reslist);

            reslist = "Partial assist,Complete assist";
            SetIndIfResultContains(2, "", "9990000305650", "", "", reslist);

            reslist = "Needs assist,Supervised,Total assist";
            SetIndIfResultContains(2, "", "9990000700380", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(2, "", "3043040100003", "", "", reslist);
            SetIndIfResultContains(2, "", "9993040100004", "", "", reslist);
            SetIndIfResultContains(2, "", "3043040101422", "", "", reslist);
            SetIndIfResultContains(2, "", "9993040101423", "", "", reslist);

            SetIndIfResultContains(2, "", EXACT_MATCH_PREFIX + "23", "", "", "Yes,1");
            reslist = "";
            SetIndIfResultContains(2, "", "3045001090", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(2, "", "3045001089", "", "", reslist);
            SetIndIfResultContains(2, "", EXACT_MATCH_PREFIX + "16", "", "", "Yes,1");
            reslist = "Amber,Clear,Cloudy,Dark,Fibrin,Light,Pink,Red,Yellow";
            SetIndIfResultContains(2, "", "9990000370180", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(2, "", "3040011360", "", "", "");
            reslist = "";
            SetIndIfResultContains(2, "", "9991600100064", "", "", "Done");
            reslist = "Stand-by assist,One staff assist,Two staff assist,1 staff assist,2 staff assist,lift";
            SetIndIfResultContains(2, "", "9991600100065", "", "", reslist);
            SetIndIfResultContains(2, "", "9991600100066", "", "", reslist);
            reslist = "Bedpan,Catheter,Commode,Incontinence pad";
            SetIndIfResultContains(2, "", "9991600100067", "", "", reslist);
            reslist = "Bed bath";
            SetIndIfResultContains(2, "", "9991600100068", "", "", reslist);
            reslist = "3";
            SetIndIfResultContains(2, "", "9993040000407", "", "", reslist);

            if (GetResult("", "3045001023", "", "", out res))
            {
                if (res.Left(1).IsNumeric())
                {
                    int value = (int)res.Val();
                    if ((value >= 9) && (value <= 12)) SetInd(2, "3045001023 value=" + res);
                }
            }
            if (GetResult("", "9993040001207", "", "", out res))
            {
                if (res.Left(1).IsNumeric())
                {
                    int value = (int)res.Val();
                    if ((value >= 9) && (value <= 12)) SetInd(2, "9993040001207 value=" + res);
                }
            }

            reslist = "";
            SetIndIfResultContains(2, "", "3045001046", "", "", "Confused");
            reslist = "Disoriented X4,Disoriented X3,Disoriented X 4,Disoriented X 3,Disoriented to person,Disoriented to place,Disoriented to time,Disoriented to situation";
            SetIndIfResultContains(2, "", "9990000301870", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(2, "", "9990000398010", "", "", "4");
            reslist = "Blind,Nystagmus";
            SetIndIfResultContains(2, "", "9990000002106", "", "", reslist);
            SetIndIfResultContains(2, "", "9990000002107", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(2, "", "9990007090070", "", "", "2,3");
            reslist = "Blindness - right,Blindness - left";
            SetIndIfResultContains(2, "", "9993040001091", "", "", reslist);
            reslist = "Strabismus - right,Strabismus - left";
            SetIndIfResultContains(2, "", "9990000002216", "", "", reslist);
            reslist = "1,2,3,4,Amputation or joint fusion";
            SetIndIfResultContains(2, "", "9990007090090", "", "", reslist);
            SetIndIfResultContains(2, "", "9990007090100", "", "", reslist);
            SetIndIfResultContains(2, "", "9990007090110", "", "", reslist);
            SetIndIfResultContains(2, "", "9990007090120", "", "", reslist);
            SetIndIfResultContains(2, "", "9990007090130", "", "", reslist);
            reslist = "Tremors,Flaccid,Abnormal extension,";
            reslist += "Abnormal flexion,No movement to painful stimulus,";
            reslist += "Non-purposeful movement,Spastic";
            SetIndIfResultContains(2, "", "9990000301980", "", "", reslist);
            SetIndIfResultContains(2, "", "9990000301940", "", "", reslist);
            SetIndIfResultContains(2, "", "9990000302000", "", "", reslist);
            SetIndIfResultContains(2, "", "9990000301960", "", "", reslist);
            string codelist = "9990000301980,9990000301940,9990000302000,9990000301960";
            reslist = STARTS_WITH + "0,-1,-2,-3,-4,P,SP,NP,Stim,Pain,DC,DB";
            SetIndIfResultContains(2, "", codelist, "", "", reslist);


            reslist = AVOID_NEGATIVE + "Injury/Trauma";
            SetIndIfResultContains(2, "", "9990000304100", "", "", reslist);
            reslist = "Limited movement,Deformity,Paralysis,Rotated,Immobilizer";
            SetIndIfResultContains(2, "", "9990000304100", "", "", reslist);
            SetIndIfResultContains(2, "", "9990000304120", "", "", reslist);
            reslist = AVOID_NEGATIVE + "Injury/Trauma";
            SetIndIfResultContains(2, "", "9990000303930", "", "", reslist);
            reslist = "Limited movement,Deformity";
            SetIndIfResultContains(2, "", "9990000303930", "", "", reslist);
            SetIndIfResultContains(2, "", "9990000303970", "", "", reslist);
            SetIndIfResultContains(2, "", "9990000304010", "", "", reslist);
            SetIndIfResultContains(2, "", "9990000304050", "", "", reslist);
            SetIndIfResultContains(2, "", "9990000304090", "", "", reslist);
            reslist = "Cock up wrist velcro,Ulnar gutter,Volar,Arm aluminum,Short arm,Long arm,";
            reslist += "Short arm fiberglass,Long arm fiberglass,Sugar tong,Double sugar tong- arm,";
            reslist += "Short leg,Long leg,Short leg fiberglass,Long leg fiberglass,Sugar tong leg,";
            reslist += "Robert Jones,Modified Robert Jones,Air cast stirrup,Posterior ankle,Cadillac (stirrup/posterior combo)";
            SetIndIfResultContains(2, "", "9991600100112", "", "", reslist);
            reslist = "Boxer,Colles,Cylinder,Short arm,Long arm,Long arm hanging,Short arm fiberglass,";
            reslist += "Long arm fiberglass,Navicular fiberglass,Short arm navicular,Long arm navicular,";
            reslist += "Short leg,Long leg,Short leg fiberglass,Long leg fiberglass,Cylinder fiberglass,";
            reslist += "Patella with bearing,Spica single,Spica double,Spica single fiberglass,Spica double fiberglass,";
            reslist += "Reinforce" + CHAR_COMMA + " minor,Reinforce" + CHAR_COMMA + " major,";
            reslist += "Reinforced fiberglass,Monovalve,BiValve,Window,Window by tech,Other";
            SetIndIfResultContains(2, "", "9991600100115", "", "", reslist);
            reslist = "Boot,Shoe,Post-op,Walking,Cam walker,Air Cast,Other";
            SetIndIfResultContains(2, "", "9991600100117", "", "", reslist);
            reslist = "Shoulder immobilizer,Walk wraps,Humeral fracture brace,Knee immobilizer,Neoprene knee brace,Other";
            SetIndIfResultContains(2, "", "9991600100118", "", "", reslist);
            reslist = "Abductor pillow,Arm elevation pillow,Arm sling,Rib belt,Clavicle shoulder strap,Dressing, soft wrap knee,Elastic Wrist";
            SetIndIfResultContains(2, "", "9991600100119", "", "", reslist);
            reslist = "Complaining of back injury,Complaining of head/neck injury,Fall,Injury,";
            reslist += "MVC,Sporting accident,UTA=Unable to assess,Other";
            SetIndIfResultContains(2, "", "9991600100122", "", "", reslist);
            reslist = "Rigid cervical collar,Backboard,Soft cervical collar,Head blocks or towels,Cervical immobilization device,Other";
            SetIndIfResultContains(2, "", "9991600100123", "", "", reslist);
            reslist = "Skeletal,Skin,Cervical,Other";
            SetIndIfResultContains(2, "", "9991600000289", "", "", reslist);
            reslist = "Yes,No,1,0";
            SetIndIfResultContains(2, "", "9991600100062", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(2, "", "9990160101903", "", "", "Yes,1");
            reslist = "Carried,Crutches,Restrained,Stretcher,Stroller";
            SetIndIfResultContains(2, "", "9991600100022", "", "", reslist);
            //reslist = "Sinus rhythm,Sinus bradycardia,Sinus tachycardia,Sinus arrest,Sinus arrhythmia,";
            //reslist += "Atrial paced,Ventricular paced,A-V Sequential paced,Agonal,Asystole,Atrial fibrillation,";
            //reslist += "Atrial fibrillation w/rapid ventricular response,Atrial flutter,Heart block,";
            //reslist += "Junctional accelerated,Junctional rhythm,Junctional tachycardia,Pulseless electrical activity,";
            //reslist += "Supraventricular tachycardia,Torsades de Pointes,Ventricular fibrillation,Ventricular tachycardia";
            //SetIndIfResultContains(2, "", "3045001065", "", "", reslist);

            reslist = "Mask,Tubing,CPAP,BPAP,Patient able to manage equipment on own,";
            reslist += "Equipment inspected (per site policy),Waiver signed (per site policy),Other";
            if (!Exists("", "9993040000639", "", "", reslist))
            {
                reslist = "Bilevel,CPAP,Auto-Bilevel,Auto-CPAP,AVAPS,PCV";
                SetIndIfResultContains(2, "", "3045001108", "", "", reslist);
                reslist = "Nasal pillows,Nasal mask,Nasal Prongs;,Nasal pharyngeal,Full face mask,";
                reslist += "Performax,Total face mask,Endotracheal,Tracheostomy";
                SetIndIfResultContains(2, "", "9993040000637", "", "", reslist);

                reslist = "Delayed swallow or interrupted swallow,";
                reslist += "Coughs" + CHAR_COMMA + " clears throat" + CHAR_COMMA + " chokes" + CHAR_COMMA + " gags up to 1 min after drinking,";
                reslist += "Unable to say \"ah\" or count to 3 immediately after drinking,";
                reslist += "Wet voice when saying \"ah\" or counting to 2,";
                reslist += "Drools after swallowing,";
                reslist += "Desaturates 2% or > from baseline seconds after swallow,";
                reslist += "Swallows more than one time after drinking,";
                reslist += "Unable to assess (Comment)";
                SetIndIfResultContains(2, "", "9993040108712", "", "", reslist);
            }

            reslist = "Digital stimulation,Enema,Manual evacuation/disimpaction";
            bool b77 = SetIndIfResultContains(2, "", "9993040109907", "", "", reslist);
            bool b78 = SetIndIfResultContains(2, "", "9990000016084", "", "", "");
            string found_what;
            bool b79 = Check_b79(out found_what);
            if (b79) SetInd(2, "Toileting: "+ found_what);
            bool b80 = SetIndIfResultContains(2, "", "9990000305560", "", "", "bedpan");

            bool c77 = b77;
            bool c78 = b78;
            bool c79 = b79;
            bool c80 = b80;

            //
            //ADL 3
            //
            reslist = "Ambulate in hall,Ambulate in room,Bathroom privileges,Bedrest,Bike,";
            reslist += "Chair,Commode,Dangle,Supervised exercise,Held,Stand at bedside,Tilt table,Turn,Up in chair,Wagon,In bed,bedpan";
            bool D1 = Exists("", "9990000305560", "", "", reslist);

            reslist = "1 Assist,2 Assist,3 Assist,>4 Assist,4 or more Assist";
            bool D2 = Exists("", "9993040109530", "", "", reslist);

            reslist = "Lying right side,Lying left side,Supine,Semi Fowler,Pillow support,";
            reslist += "Prone,Sitting,Standing,Up in chair,Continuous lateral rotation,Micro turn left,";
            reslist += "Micro turn right,Do Not Turn,Unstable to turn,turn right side,turn left side";
            bool D3 = Exists("", "9990000400604", "", "", reslist);

            reslist = "Assist,Total Care";
            bool D4 = Exists("", "9991025006475", "", "", reslist);

            reslist = "Left side,Left tilt,Right side,Right tilt,Semi-fowler,Fowler,";
            reslist += "Knee chest,Chair,Rocking chair,Supine,Trendelenberg,Squatting,Standing,Kneeling,Walking,Prone";
            bool D5 = Exists("", "1028000027", "", "", reslist);

            reslist = "1 Assist,2 Assist,3 Assist,>4 Assist";
            bool D6 = Exists("", "9990007060350", "", "", reslist);

            reslist = "Bathed,Chlorhexidine,Shower,Foley care,Peri care";
            bool D7 = Exists("", "9990000342030", "", "", reslist);

            reslist = "Partial assist,Complete assist";
            bool D8 = Exists("", "9990000305650", "", "", reslist);

            reslist = "Needs assist,Total assist";
            bool D9 = Exists("", "9990000700380", "", "", reslist);

            reslist = "";
            bool D10 = Exists("", "3043040100003", "", "", reslist);
            bool D11 = Exists("", "9993040100004", "", "", reslist);
            bool D12 = Exists("", "3043040101422", "", "", reslist);
            bool D13 = Exists("", "9993040101423", "", "", reslist);

            bool D15 = Exists("", EXACT_MATCH_PREFIX + "23", "", "", "Yes,1");
            reslist = "";
            bool D16 = Exists("", "3045001090", "", "", reslist);
            bool D17 = Exists("", "3045001089", "", "", reslist);
            bool D18 = Exists("", EXACT_MATCH_PREFIX + "16", "", "", "Yes,1");

            bool D19 = false;

            reslist = "Amber,Clear,Cloudy,Dark,Fibrin,Light,Pink,Red,Yellow";
            bool D20 = Exists("", "9990000370180", "", "", reslist);

            reslist = "";
            bool D21 = Exists("", "3040011360", "", "", reslist);

            bool Dgroup1 = ((D1 && D2) || (D2 && D3) || (D4 && D5));
            bool Dgroup2 = ((D6 && D7) || (D7 && D8));
            bool Dgroup3 = (D9 || D10 || D11 || D12 || D13);
            bool Dgroup4 = (D15 || D16 || D17 || D18 || D20 || D21 || c77 || c78 || c79 || c80);
            //"FOR ADL Extended rows D-1 through D-19, must include charting for 3 of the 4 bullets below on the visit:
            //- D1&D2 or D2&D3 or D4&D5   (mobility)
            //- D6&7 or D7&D8 (hygiene)
            //- Any of D-9, D-10, D-11, D-12, D-13, ADL-LDA-21 or ADL-LDA-25 (feeding)
            //- any of D-15, D-16, D-17, D-18, D-20, D-21 or ADL-LDA-12 to  ADL-LDA-20 (toileting)"	
            if ((Dgroup1 ? 1 : 0) + (Dgroup2 ? 1 : 0) + (Dgroup3 ? 1 : 0) + (Dgroup4 ? 1 : 0) >= 3)
            {
                SetInd(3, "Three or more ADL D-groups present: Group1=" + (Dgroup1 ? 1 : 0) + " Group2=" + (Dgroup2 ? 1 : 0) + " Group3=" + (Dgroup3 ? 1 : 0) + " Group4=" + (Dgroup4 ? 1 : 0));
            }

            bool D26 = Exists("", "9991600100064", "", "", "Done");
            reslist = "Stand-by assist,One staff assist,Two staff assist,1 staff assist,2 staff assist,lift";
            bool D27 = Exists("", "9991600100065", "", "", reslist);
            bool D28 = Exists("", "9991600100066", "", "", reslist);
            reslist = "Bedpan,Catheter,Commode,Incontinence pad";
            bool D29 = Exists("", "9991600100067", "", "", reslist);
            bool D30 = Exists("", "9991600100068", "", "", "Bed bath");
            if ((D26 || D30) && (D27) && (D28 || D29))
            {
                SetInd(3, "All ED ADL D-groups present: D26=" + (D26 ? 1 : 0) + " D27=" + (D27 ? 1 : 0) + " D28=" + (D28 ? 1 : 0) + " D29=" + (D29 ? 1 : 0) + " D30=" + (D30 ? 1 : 0));
            }

            SetIndIfResultContains(3, "", "9993040000407", "", "", "4");
            if (GetResult("", "3045001023", "", "", out res))
            {
                if (res.Left(1).IsNumeric())
                {
                    int value = (int)res.Val();
                    if ((value <= 8)) SetInd(3, "3045001023 value=" + res);
                }
            }
            if (GetResult("", "9993040001207", "", "", out res))
            {
                if (res.Left(1).IsNumeric())
                {
                    int value = (int)res.Val();
                    if ((value <= 8)) SetInd(3, "9993040001207 value=" + res);
                }
            }
            reslist = "Strong stimuli to arouse,";
            reslist += "Unconscious with motor response (withdrawal, flexion, extension),";
            reslist += "Unconscious with no motor response,";
            reslist += "Pharmacologically paralyzed";
            SetIndIfResultContains(3, "", "3045001046", "", "", reslist);

            SetIndIfResultContains(3, "", "3045001080", "", "", "6");
            SetIndIfResultContains(3, "", "3045001081", "", "", "0,1");
            if (GetResult("", "3045001079", "", "", out res))
            {
                if (res.Left(1).IsNumeric())
                {
                    int value = (int)res.Val();
                    if ((value == -3) || (value == -4) || (value == -5)) SetInd(3, "3045001079 value=" + res);
                }
            }
            SetIndIfResultContains(3, "", "9993040104675", "", "", "4");
            SetIndIfResultContains(3, "", "3045001117", "", "", ""); //any characters
            reslist = "PACU Ventilator Rows,Trilogy Ventilator Rows,HFO Ventilator Rows,";
            reslist += "BPAP/CPAP Rows,Abbreviated Settings,NAVA Rows,HFJV Rows";
            SetIndIfResultContains(3, "", "9993040000635", "", "", reslist);
            reslist = "Somnolence,RUE paresis,RLE paresis,LUE paresis,LLE paresis";
            SetIndIfResultContains(3, "", "9990000450600", "", "", reslist);
            reslist = "Unresponsive,Paralized,Sedated,Loss Of Consciousness";
            SetIndIfResultContains(3, "", "9990160239301", "", "", reslist);
            reslist = "Respiratory arrest,Cardiac arrest,Unknown,Other";
            SetIndIfResultContains(3, "", "9991600100646", "", "", reslist);
            reslist = "Reposition,Suction,Jaw thrust,Chin lift,Foreign object removal";
            SetIndIfResultContains(3, "", "9991010010010", "", "", reslist);
            reslist = "Endotracheal tube,Esophageal - tracheal tube,Laryngeal mask airway (LMA),Nasopharyngeal airway (NPA),Oropharyngeal airway (OPA),Tracheostomy,Other";
            SetIndIfResultContains(3, "", "9991600100681", "", "", reslist);
            reslist = "Bag-valve-mask,Bag-valve-ET tube,Bag-valve-tracheostomy,Ventilator,";
            reslist += "Bilevel positive airway pressure (BiPAP),Continuous positive airway pressure (CPAP),";
            reslist += "CPAP nasal,CPAP mask,Positive pressure ventilation (PPV),Other";
            SetIndIfResultContains(3, "", "9991600100682", "", "", reslist);
            SetIndIfResultContains(3, "", "9990000016054", "", "", "1");
            reslist = "Level red,Level yellow";
            SetIndIfResultContains(3, "", "9991600100426", "", "", reslist);
            SetIndIfResultContains(3, "", "9990160100151", "", "", reslist);

            SetIndIfResultContains(3, "", "9991600100681", "", "", "");
            SetIndIfResultContains(3, "", "9991600100682", "", "", "");


            AtLeastOneADL();

        }

        private bool Check_b79(out string found_what)
        {
            bool b = false;
            string d = "";
            var query = StartNewQuery();
            query = query.Where(e => e.CODE.ToUpper().StartsWith("MED"));
            query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;given") && (e.DESCRIPTION.ToUpper().Contains("PEG3350 100 GRAM-SOD SUL") || e.DESCRIPTION.ToUpper().Contains("PEG 3350-ELECTROLYTES 236")));
            query = query.OrderByDescending(e => e.EVENT_DATETIME);
            //query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;given"));
            b = (query.Count() > 0);
            if (b) d = query.First().DESCRIPTION;
            found_what = d;
            return b;
        }

        private void Check_4()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Inpt 4. ADL - Two or more");
            Program.VerboseAudit("---------------");
            string reslist = "", res;

            reslist = "2 Assist,3 Assist,4 Assist,4 or more Assist";
            if (ResultContains("", "9993040109530", "", "", reslist))
            {
                res = "Ambulate in hall,Ambulate in room,Bathroom privileges,Chair,Commode,Dangle,Stand at bedside,Stroller,Tilt table,Turn,Up in chair,Wagon,In bed,bedpan";
                SetIndIfResultContains(4, "", "9990000305560", "", "", res);
                res = "Lying right side,Lying left side,Supine,Semi Fowler,Pillow support,Prone,Sitting,Standing,Up in chair,Continuous lateral rotation,Micro turn left,Micro turn right,Do Not Turn,Unstable to turn,turn right side,turn left side";
                SetIndIfResultContains(4, "", "9990000400604", "", "", res);
            }
            reslist = "2 Assist,3 Assist,4 Assist";
            if (ResultContains("", "9990007060350", "", "", reslist))
            {
                res = "Bathed,Chlorhexidine,Shower,Bath in a bag,Catheter care,Peri care";
                SetIndIfResultContains(4, "", "9990000342030", "", "", res);
            }

            SetIndIfResultContains(4, "", "9991600100065", "", "", "Two staff assist");

        }


        private void Check_5()
        {
            string reslist = "";            
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Inpt 5. Communication Support");
            Program.VerboseAudit("---------------");


            reslist = "In person,iPad,Phone,Other";
            SetIndIfResultContains(5, "", "9991733444441", "", "", reslist);
            reslist = "Admission,Assessment,Consent,Discharge instructions,Education,Plan of care,Other";
            SetIndIfResultContains(5, "", "9993040109069", "", "", reslist);
            reslist = "Hospital/clinic approved on site interpreter,Telephone interpreter,Video remote interpreter,Other";
            SetIndIfResultContains(5, "", "9993040109071", "", "", reslist);

            SetIndIfResultContains(5, "", "9993040108551", "", "", "");
            reslist = "Braille,Communication board,Hearing aid,Interpreter,Sign language";
            SetIndIfResultContains(5, "", "9990007070581", "", "", reslist);

            SetIndIfResultContains(5, "", "9993040108693", "", "", "Yes,1");


            reslist = "Artificial airway,Attempts to verbalize,Delayed responses,Dysphasia,";
reslist += "Expressive aphasia,Garbled,Global aphasia,Incomprehensible,Nods/gestures appropriately,";
reslist += "Non-verbal,Receptive aphasia,Slurred,Uses communication aid(s)";
SetIndIfResultContains(5,"","9990000301890","","",reslist);	   

reslist = "Impaired vision- not corrected,Blind";
bool F2 = Exists("","9990000002106","","",reslist);	   
bool F3 = Exists("","9990000002107","","",reslist);	   
            if (F2 && F3) SetInd(5, "Both F2 and F3 present.");

reslist = "Impaired hearing- not corrected,Acute hearing loss,Deaf";
bool F4 = Exists("","9990000002108","","",reslist);	   
bool F5 = Exists("","9990000002109","","",reslist);
if (F4 && F5) SetInd(5, "Both F4 and F5 present.");

reslist = "Difficulty talking,Trach,Hoarse,Muffled,Speaking valve,Voice amplifier";
            SetIndIfResultContains(5,"","9990000002115","","",reslist);	   

SetIndIfResultContains(5,"","9990304000117","","","3");	   

SetIndIfResultContains(5,"","3045001024","","","3");	   
reslist = "Delayed,Impoverished,Mumbled,Mute,Overproductive,Pressured,Rambling,Rapid,Slurred,Stutter";
SetIndIfResultContains(5,"","9993040105630","","",reslist);	   

SetIndIfResultContains(5,"","9990007090150","","","1,2,3");	   
SetIndIfResultContains(5,"","9990007090160","","","1,2");	   
SetIndIfResultContains(5,"","9990007090070","","","2,3");

  
SetIndIfResultContainsAll(5,"","9993040001091","","","Blindness - right,Blindness - left");	   

SetIndIfResultContainsAll(5,"","9990000002221","","","Unable to hear - right,Unable to hear - left");	   
reslist = "In person,iPad,Phone,Other";
SetIndIfResultContains(5,"","9991733444441","","",reslist);	   
reslist = "Admission,Assessment,Consent,Discharge instructions,Education,Plan of care,Other";
SetIndIfResultContains(5,"","9993040109069","","",reslist);	   
reslist = "Hospital/clinic approved on site interpreter,Telephone interpreter,Video remote interpreter,Other";
SetIndIfResultContains(5,"","9993040109071","","",reslist);	   

SetIndIfResultContains(5,"","9993040108551","","","");
reslist = "Braille,Communication board,Hearing aid,Interpreter (Legal),Sign language";
SetIndIfResultContains(5,"","9990007070581","","",reslist);	   

SetIndIfResultContains(5,"","9993040108693","","", "Yes,1");

            reslist = "Mask,Tubing,CPAP,BPAP,Patient able to manage equipment on own,Equipment inspected (per site policy),Waiver signed (per site policy),Other";
bool F23 = Exists("", "9993040000639", "", "", reslist);
if (!F23)
{
    reslist = "Bilevel,CPAP,Auto-Bilevel,Auto-CPAP,AVAPS,PCV";
    SetIndIfResultContains(5, "", "3045001108", "", "", reslist);
    reslist = "Full face mask,Performax,Total face mask,Endotracheal,Tracheostomy";
    SetIndIfResultContains(5, "", "9993040000637", "", "", reslist);
}
            SetIndIfResultContains(5, "", "9993040108584", "", "", "Yes");
            SetIndIfResultContains(5, "", "9993040108585", "", "", "Yes");
            SetIndIfResultContains(5, "", "9990007081420", "", "", "No");
            SetIndIfResultContains(5, "", "9990007085050", "", "", "Deaf");
            SetIndIfResultContains(5, "", "9990007085060", "", "", "Deaf");


        }


        private void Check_67()
        {
            string reslist = "";
            bool is_peds = false;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Inpt 6. Safety Management - q 30 min");
            Program.VerboseAudit("ED Inpt 7. Safety Management - q 15 min");
            Program.VerboseAudit("---------------");

            is_peds = (_pat.age <= 14.0);

            SetIndIfResultContains(6, "", "9993040009123", "", "", reslist);
            SetIndIfResultContains(6, "", "9993040009234", "", "", reslist);


            SetIndIfResultContains(7, "", "9993040009123", "", "", reslist);
            SetIndIfResultContains(7, "", "9993040009234", "", "", reslist);
            SetIndIfResultContains(7, "", "9990000300101", "", "", reslist);
            SetIndIfResultContains(7, "", "9990000300001", "", "", reslist);
            SetIndIfResultContains(7, "", "9990007096219", "", "", reslist);
            SetIndIfResultContains(7, "", "9990007096227", "", "", reslist);

            reslist = "Impulsive,Lack of safety awarenesss,Does not use call light or ask for assistance,Danger to self or others,Danger from others,Exit seeking,Epilepsy monitoring,Grid/SEEG electrodes";
            bool G1 = Exists("", "9993040009123", "", "", reslist);
            reslist = "Q30 min,Q15 min,Q5 min,Line of sight";
            bool G2 = Exists("", "9993040009234", "", "", reslist);
            if (G1 && G2) SetInd(6, "Both G1 and G2 present.");
            if (is_peds && G2)
                SetInd(6, "Peds and G2");

            SetIndIfResultContains(6, "", "9990000300101", "", "", "24 hours");
            reslist = "4 hours (Age 18 and older),2 hours (Age 9 to 17),1 hour (Age 8 and younger)";
            SetIndIfResultContains(6, "", "9990000300001", "", "", reslist);

            reslist = "1:1 Nsg care for seclusion for the first hour,";
reslist += ",Constant Nsg care for restraints for the first hour";
reslist += ",Record behavior every 5 minutes";
reslist += ",Review strengths/comfort measures - assist pt in reaching goal for discontinuation";
reslist += ",Administration of medication - to help client regain previous level of functioning";
reslist += ",Respiratory status assessed/documented each check - to ensure adequate air exchange";
reslist += ",Restraints checked every 15 minutes - to prevent injury from cuffs constricting normal blood flow or irritation of skin";
reslist += ",RN hourly assessment - mental"+CHAR_COMMA+" behavior and respiratory status - to minimize length of the procedure";
reslist += ",Offer fluids every 2 hours - to provide elimination opportunities";
reslist += ",Offer toileting every 2 hours - to provide elimination opportunities";
reslist += ",ROM to joints every 2 hours - to prevent injury from occurring when client is in restraints";
reslist += ",Hygiene PRN - to provide comfort and support";
reslist += ",Meals and snacks offered - to maintain nutrition (document reason for not providing and meal or snack at regular times)";
reslist += ",Other";
            SetIndIfResultContains(6, "", "9990007096219", "", "", reslist);

            reslist = "Implemented - Seclusion Treatment Policy";
reslist += ",Implemented - Behavioral Restraint Policy";
reslist += ",Procedure explained to patient";
reslist += ",Reason for seclusion/restraints explained";
reslist += ",Informed the patient of the goal\\Mattress checked for dangerous items";
reslist += ",Mattress checked for dangerous items";
reslist += ",check room for lighting"+CHAR_COMMA+" temperature and safety";
reslist += ",Dangerous items and jewelry removed";
reslist += ",Respiratory status monitored";
reslist += ",Pt assisted in achieving goal for d/c";
            SetIndIfResultContains(6, "", "9990007096227", "", "", reslist);

            reslist = "Impulsive,Lack of safety awarenesss,Does not use call light or ask for assistance,Danger to self or others,Danger from others,Exit seeking,Epilepsy monitoring,Grid/SEEG electrodes";
            bool H1 = Exists("", "9993040009123", "", "", reslist);
            reslist = "Continuous observation by RN with patient,";
reslist += ",Continuous observation by non-RN staff with patient";
reslist += ",Continuous observation by two staff with patient";
            bool H2 = Exists("", "9993040009234", "", "", reslist);
            if (H1 && H2) SetInd(7, "Both H1 and H2 present.");
            if (is_peds && H2)
                SetInd(7, "Peds and H2");

        }

        private void Check_89()
        {
            string reslist;
            bool is_peds = false;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Inpt 8. Behavior/Emotional Mgt");
            Program.VerboseAudit("ED Inpt 9. Behavior/Emotional Mgt >= 30min");
            Program.VerboseAudit("---------------");

            is_peds = (_pat.age <= 14.0);

            reslist = "Agitated,Behavior plan,Catatonic,Combative,Compulsive,Crying";
            reslist += ",Demanding,Destructive,Disorganized,Echopraxic,Exit seeking,Fussy";
            reslist += ",Guarded,Hostile,Hyperactive,Impulsive,Intrusive,Motor perseveration,Oppositional";
            reslist += ",Pacing,Preoccupied,Disruptive,Psychomotor retardation,Pushing limits,Reckless";
            reslist += ",Resistant to care,Rigid,Ritualistic,Tearful,Staff seeking,Seclusive,Sexually inappropriate,Tics,Withdrawn,flat affect,flight of ideas,dismissive";
            reslist += ",Delusional,Hallucinating";
            bool I1 = Exists("", "9993040105629", "", "", reslist);

            reslist = "1:1 RN Time,1:1 Staff Time,Assign a task,Aromatherapy,Assisted relaxation,Consistent response,Exposure therapy,Limit setting,Medicated,Neutral response,Physical redirection,Quiet room,Reorientation,Time out,Verbal redirection,Following behavior safety plan,Multiple Staff for acute behavioral intervention";
            reslist += ",Active listening,Emotional support given";
            bool I2 = Exists("", "9993040000352", "", "", reslist);
            if (is_peds && I2)
                SetInd(8, "Peds and I2");

            reslist = "Agitated,Combative,Compulsive,Crying,Destructive,Disorganized,Disruptive";
            reslist += ",Guarded,Hostile,Hyperactive,Intrusive,Oppositional,Pacing,Preoccupied";
            reslist += ",Pushing limits,Reckless,Resistant to care,Rigid,Ritualistic,Tearful";
            reslist += ",Staff seeking,Sexually inappropriate,Tics,Withdrawn";
            bool I3 = Exists("", "9993040009823", "", "", reslist);
            if (is_peds && I3)
                SetInd(8, "Peds and I3");

            if (I2 && (I1 || I3)) SetInd(8, "Both I2 and (I1 or I3) present.");


            //reslist = "Assign a task,Aromatherapy,Assisted relaxation";
            //reslist += ",Consistent response,Exposure therapy,Limit setting,Medicated (see MAR)";
            //reslist += ",Neutral response,Physical redirection,Quiet room,Reorientation,Time out";
            //reslist = "Following behavior safety plan,Multiple Staff for acute behavioral intervention";
            //J2 = J2 || Exists("", "9993040000352", "", "", reslist);

            //reslist = "Angry,Anxious,Apathetic,Depressed,Dysphoric,Elevated,Euphoric";
            //reslist += ",Fearful,Guilty,Irritable,Manic,Sad";
            //bool I4 = Exists("", "9993040105622", "", "", reslist);
            //if (I4 && J2) SetInd(8, "Both I4 and J2 present.");

            SetIndIfResultContains(8, "", "9993040105637", "", "", "Yes,1");
            SetIndIfResultContains(8, "", "9993040105640", "", "", "Acts of violence (Comment),Threats of violence (Comment)");

            SetIndIfResultContains(8, "", "9993040000405", "", "", "Disruptive");

            reslist = "Hallucination,Illusions,Patient responding to internal Stimuli";
            SetIndIfResultContains(8, "", "9993040105625", "", "", reslist);

            reslist = "Auditory (Comment),Visual (Comment),Tactile (Comment),Command,Olfactory (Comment)";
            SetIndIfResultContains(8, "", "9991540100181", "", "", reslist);

            reslist = "Persecution,Grandeur,Obsessions,Religiosity,Phobias,Influence";
            reslist += ",Reference,Sexual delusions,Somatization,Thought broadcasting,Thought insertion,Thought withdrawal";
            SetIndIfResultContains(8, "", "9991540100180", "", "", reslist);

            reslist = "Blanket,Bundled,Gown changed,Cares clustered,Held / cuddled";
            reslist += ",Infant seat/ swing,Lotion,Music,Pacifier,Pain medication";
            reslist += ",Rocking,Sucrose,Swaddled,Touch,Toy,Other";
            SetIndIfResultContains(8, "", "9990000342040", "", "", reslist);
            SetIndIfResultContains(9, "", "9991733214522", "", "", "");
            SetIndIfResultContains(9, "", "9991733214523", "", "", "");


            reslist = "Combative,Destructive,Hostile,Disruptive,Pushing limits,Sexually inappropriate";
            SetIndIfResultContains(9, "", "9993040105629", "", "", reslist);

            bool J2 = CheckResultFor11RNTime("", "9993040000352", "", "", reslist, SearchDepth.SearchDefault);

            //            reslist = "Agitated,Behavior plan,Catatonic,Compulsive,Crying,Demanding";
            //reslist += ",Disorganized,Echopraxic,Exit seeking,Fussy,Guarded,Hyperactive";
            //reslist += ",Impulsive,Intrusive,Motor perseveration,Oppositional,Pacing,Preoccupied";
            //reslist += ",Psychomotor retardation,Reckless,Resistant to care,Rigid,Ritualistic";
            //reslist += ",Tearful,Staff seeking,Seclusive,Tics,Withdrawn,flat affect,flight of ideas,dismissive";
            //            reslist += ",Delusional,Hallucinating";
            bool J1b = I1;
            if (J1b && J2) SetInd(9, "Both J1b and J2 present.");
            if (is_peds && J2)
                SetInd(9, "Peds and J2");


            reslist = "Agitated,Combative,Compulsive,Crying,Destructive,Disorganized";
            reslist += ",Disruptive,Guarded,Hostile,Hyperactive,Intrusive,Oppositional,Pacing";
            reslist += ",Preoccupied,Pushing limits,Reckless,Resistant to care,Rigid,Ritualistic";
            reslist += ",Tearful,Staff seeking,Sexually inappropriate,Tics,Withdrawn";
            bool J3 = Exists("", "9993040009823", "", "", reslist);
            if (J3 && J2) SetInd(9, "Both J3 and J2 present.");

            SetIndIfResultContains(9, "", "9990000300001", "", "", "4 hours,2 hours,1 hour");

            reslist = "1:1 Nsg care for seclusion for the first hour";
            reslist += ",Constant Nsg care for restraints for the first hour";
            reslist += ",Record behavior every 5 minutes";
            reslist += ",Review strengths/comfort measures";
            reslist += ",Administration of medication";
            reslist += ",Respiratory status assessed/documented each check";
            reslist += ",Restraints checked every 15 minutes";
            reslist += ",RN hourly assessment";
            reslist += ",Offer fluids every 2 hours";
            reslist += ",Offer toileting every 2 hours";
            reslist += ",ROM to joints every 2 hours";
            reslist += ",Hygiene PRN";
            reslist += ",Meals and snacks offered";
            reslist += ",Other";
            SetIndIfResultContains(9, "", "9990007096219", "", "", reslist);

            reslist = "Implemented-Seclusion Treatment Policy,Seclusion Treatment Policy";
            reslist += ",Implemented-Behavioral Restraint Policy,Behavioral Restraint Policy";
            reslist += ",Procedure explained to patient";
            reslist += ",Reason for seclusion/restraints explained";
            reslist += ",Informed the patient of the goal\\Mattress checked for dangerous items";
            reslist += ",Mattress checked for dangerous items";
            reslist += ",check room for lighting" + CHAR_COMMA + " temperature and safety";
            reslist += ",Dangerous items and jewelry removed";
            reslist += ",Respiratory status monitored";
            reslist += ",Pt assisted in achieving goal for d/c";
            SetIndIfResultContains(9, "", "9990007096227", "", "", reslist);

            reslist = "Pillow,Memory box,Bereavement package,Other";
            SetIndIfResultContains(9, "", "9991020100509", "", "", reslist);

            reslist = "Baby bracelet,Blanket,Clay molds,Clothing,Complimentary birth certificate";
            reslist += ",Crib card,Foot prints,Hand prints,Hat,Lock of hair,Photos";
            reslist += ",Pin,Prism,Tape measure,Other";
            SetIndIfResultContains(9, "", "9991020100510", "", "", reslist);

            SetIndIfResultContains(9, "", "9991020100506", "", "", "Done");

            reslist = "Agitated,Verbally abusive,Tearful,Hallucinating,Delusional";
            SetIndIfResultContains(9, "", "9990000300113,9990000300018,9990000047549", "", "", reslist);
            reslist = "Other";
            SetIndIfResultContains(9, "", "9990000300018,9990000047549", "", "", reslist);


        }
        private bool CheckResultFor11RNTime(string cat, string code_list, string desc_list, string field, string result, SearchDepth search_depth)
        {
            bool qualifies = false;
            int ct = 0;
            //            "Selection of
            //1:1 RN Time (# minutes) with time > 30 minutes 

            //Or

            //Selection of:
            //Following behavior safety plan(See notes)
            //or

            //Selection of:
            //Multiple Staff for acute behavioral intervention"


            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "Following behavior safety plan,Multiple Staff for acute behavioral intervention,1:1 RN Time");
            ct = query.Count();
            foreach (var item in query)
            {
                if (!qualifies)
                {
                    qualifies |= (item.RESULT.ToLower().Contains("Following behavior safety plan".ToLower()));
                    qualifies |= (item.RESULT.ToLower().Contains("Multiple Staff for acute behavioral intervention".ToLower()));
                }
            }
            if (!qualifies && (ct > 0)) // then 1:1 RN are the only results.  need to check their NTE.
            {
                // Query to look for OBX + NTE
                //select patindex('%|9993040000352%NTE|%', SOURCE_TEXT),
                //      substring(SOURCE_TEXT, patindex('%|9993040000352%NTE|%', SOURCE_TEXT), 240) from EVENT_LOG
                //where source_text like '%9993040000352%NTE|%' and timestamp>'2/12/19'

                //OBX|1|ST|9993040000352^Staff Interventions||1:1 Staff Time(# minutes)||||||F|||20190211203400||IDMPROD20979444^BIRCHLER^SARAH^M
                //NTE|1||60
                DateTime tstamp;
                foreach (var item in query)
                {
                    if (!qualifies && item.RESULT.ToLower().Contains("1:1 RN Time".ToLower()))
                    {
                        tstamp = PFSDBUtility.DBToDateTime(item.TIMESTAMP);
                        qualifies = CheckForNTE(tstamp);
                    }

                }

            }

            return qualifies;
        }


        private bool CheckForNTE(DateTime tstamp)
        {
            bool qualifies = false;
            int v = 0;
            //event_log timestamp is always later than the chartitem timestamp it created.
            string sql = "select substring(SOURCE_TEXT, patindex('%|9993040000352%NTE|%', SOURCE_TEXT)-3, 240) from EVENT_LOG";
            sql += " where encounter_id=" + _pat.encounter_id.ToString();
            sql += " and timestamp>=" + PFSDBUtility.SQLDateTime(tstamp);
            sql += " and timestamp<" + PFSDBUtility.SQLDateTime(tstamp.AddMinutes(3));
            sql += " and source_text like '%9993040000352%NTE|%'";
            var db = PFSDBUtility.NewSqlConnection();
            var cmd = new SqlCommand(sql, db);
            SqlDataReader dr = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr.Read())
            {
                string s = PFSDBUtility.DBToString(dr[0]);
                int posNTE = s.IndexOf("NTE|",0);
                int posOBX = s.IndexOf("OBX|",0);
                if ((posNTE > 0) && (posOBX > 0) && (posNTE < posOBX)) // then this NTE is associated with the item
                {
                    //look for the value in NTE-3
                    string str_posNTE = s.Substring(posNTE, s.Length - posNTE);
                    //s = s.Substring(posNTE, 32);
                    var arr = str_posNTE.Split(new string[] { "|" }, StringSplitOptions.None);
                    if (arr.GetUpperBound(0) >= 3)
                    {
                        int numpos = arr[3].IndexOf(" ");
                        if (numpos <= 0)
                            numpos = arr[3].IndexOf("OBX");
                        string numstr;
                        if (numpos <= 0)
                            numstr = arr[3];
                        else
                            numstr = arr[3].Substring(0, numpos);
                        //Program.VerboseAudit("numpos=" + numpos + " arr3.len="+arr[3].Length + " numstr=" + numstr);
                        if (numstr.IsNumeric())
                        {
                            v = (int)numstr.Val();
                        }
                        Program.VerboseAudit("NTE-3=" + arr[3] + " v=" + v);
                        qualifies |= (v >= 30);
                    }
                }
            }
            dr.Close();
            return qualifies;
        }

        private void Check_10()
        {
            string reslist = "";

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Inpt 10. Fluid Management");
            Program.VerboseAudit("---------------");

            CountFluids();
            SetIndIfResultContains(10, "", EXACT_MATCH_PREFIX+"26", "", "", reslist);
            SetIndIfResultContains(10, "", "9990007070009", "", "", reslist);
            SetIndIfResultContains(10, "", EXACT_MATCH_PREFIX + "28", "", "", reslist);
            SetIndIfResultContains(10, "", "9993040100475", "", "", reslist);
            SetIndIfResultContains(10, "", EXACT_MATCH_PREFIX + "20", "", "", reslist);
            SetIndIfResultContains(10, "", EXACT_MATCH_PREFIX + "14", "", "", reslist);
            SetIndIfResultContains(10, "", "9993047073811", "", "", reslist);
            SetIndIfResultContains(10, "", EXACT_MATCH_PREFIX + "29", "", "", reslist);
            SetIndIfResultContains(10, "", "3040001333", "", "", reslist);
            SetIndIfResultContains(10, "", "3040001334", "", "", reslist);
            SetIndIfResultContains(10, "", "9990000305290", "", "", reslist);
            SetIndIfResultContains(10, "", "3045001090", "", "", reslist);
            SetIndIfResultContains(10, "", "3045001089", "", "", reslist);
            SetIndIfResultContains(10, "", "9990160000252", "", "", reslist);
            SetIndIfResultContains(10, "", "9990160000253", "", "", reslist);
            SetIndIfResultContains(10, "", "3043040100003", "", "", reslist);
            SetIndIfResultContains(10, "", "9993040100004", "", "", reslist);
            SetIndIfResultContains(10, "", "3043040101422", "", "", reslist);
            SetIndIfResultContains(10, "", "9993040101423", "", "", reslist);

        }

        private void CountFluids()
        {
            string descript;
            string drugclass;
            bool newbag = false;
            //OBX|1|DT|MED9811^DEXTROSE 2.5 % AND 0.45 % SODIUM CHLORIDE INTRAV;;;29;;;50;;;IV;;;NewBag|NewBag|20170609120700||||||F|||20170609120700

            var query = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
            query = query.Where(e => e.CODE.StartsWith("med"));
            query = query.Where(e => e.EVENT_DATETIME >= _pat.unit_arrival);
            query = query.Where(e => e.EVENT_DATETIME <= _pat.unit_departure);
            query = query.OrderBy(e => e.EVENT_DATETIME);
            foreach (var item in query)
            {
                var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                descript = arr[0];
                drugclass = arr[1];
                newbag = (arr[4].ToLower() == "newbag" || arr[4].ToLower() == "new bag");
                if ((drugclass == "29") && (newbag))
                {
                    SetInd(10, "Found Fluids in Meds with NewBag:" + item.EVENT_DATETIME.ToString() + "; drugclass=" + drugclass + "; descript=" + descript);
                }
            }

        }


        private void Check_1112()
        {
            string reslist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Inpt 11. Physiologic Assessment q 1 hr");
            Program.VerboseAudit("ED Inpt 12. Physiologic Assessment q 30 min");
            Program.VerboseAudit("---------------");

            CountAssessments(70, 11);
            CountAssessments(40, 12);

            SetIndIfResultContains(12,"","3045001046","","","Pharmacologically paralyzed");
SetIndIfResultContains(12,"","9990000016054","","","1");
SetIndIfResultContains(12,"","9991600100426","","","Level red,Level yellow");
SetIndIfResultContains(12,"","9990160100151","","","Level red,Level yellow");
SetIndIfResultContains(12,"","9991600100335","","","STEMI,Arrest,Medical resuscitation");

SetIndIfResultContains(12,"","3045001117","","","");
            reslist = "PACU Ventilator Rows,Trilogy Ventilator Rows,HFO Ventilator Rows";
reslist += ",BPAP/CPAP Rows,Abbreviated Settings,NAVA Rows,HFJV Rows";
SetIndIfResultContains(12,"","9993040000635","","",reslist);

            reslist = "Mask,Tubing,CPAP,BPAP";
reslist += ",Patient able to manage equipment on own";
reslist += ",Equipment inspected (per site policy)";
reslist += ",Waiver signed (per site policy),Other";
bool N11 = Exists("","9993040000639","","",reslist);
            if (!N11)
            {
            reslist = "Bilevel,CPAP,Auto-Bilevel,Auto-CPAP,AVAPS,PCV";
SetIndIfResultContains(12,"","3045001108","","",reslist);
            reslist = "Nasal pillows,Nasal mask,Nasal Prongs,Nasal pharyngeal";
reslist += ",Full face mask,Performax,Total face mask,Endotracheal,Tracheostomy";
SetIndIfResultContains(12,"","9993040000637","","",reslist);
            }
reslist = "Respiratory arrest,Cardiac arrest,Unknown,Other";
SetIndIfResultContains(12,"","9991600100646","","",reslist);
reslist = "SVT,Supraventricular tachycardia,VT,Ventricular tachycardia,A-fib w/RVR,Atrial fibrillation,Other";
SetIndIfResultContains(12,"","9991600100613","","",reslist);
reslist = "Sinus,SR,SB,Sinus rhythm,Sinus bradycardia,Sinus tachycardia,A-paced,V-paced";
reslist += ",A-V paced,Atrial paced,Ventricular paced,A-V Sequential paced,A-fib,A-fib w/RVR,Atrial fibrillation";
reslist += ",Atrial flutter,Heart block,Junctional,SVT,Supraventricular tachycardia,Torsades";
reslist += ",Asystole,PEA,Pulseless electrical activity,VF,VT,Ventricular fibrillation,Ventricular tachycardia,Other";
SetIndIfResultContains(12,"","9991600100614","","",reslist);
reslist = EXACT_MATCH_PREFIX + "ST";
SetIndIfResultContains(12, "", "9991600100614", "", "", reslist);
reslist = "Initiated,Settings Change,Discontinued";
SetIndIfResultContains(12,"","9991600100147","","",reslist);
reslist = "Reposition,Suction,Jaw thrust,Chin lift,Foreign object removal";
SetIndIfResultContains(12,"","9991010010010","","",reslist);
reslist = "Endotracheal tube,Esophageal - tracheal tube,Laryngeal mask airway (LMA)";
reslist += ",Nasopharyngeal airway (NPA),Oropharyngeal airway (OPA),Tracheostomy,Other";
SetIndIfResultContains(12,"","9991600100681","","",reslist);
reslist = "Bag-valve-mask,Bag-valve-ET tube,Bag-valve-tracheostomy,Ventilator";
reslist += ",Bilevel positive airway pressure (BiPAP),Continuous positive airway pressure (CPAP)";
reslist += ",CPAP nasal,CPAP mask,Positive pressure ventilation (PPV),Other";
SetIndIfResultContains(12,"","9991600100682","","",reslist);
reslist = "To Cardiac Cath. Lab,To Operating Room,Admitted,Transferred,Other";
SetIndIfResultContains(12,"","9991601001082","","",reslist);
reslist = "Altered mental status,Change in level of consciousness,Stroke symptoms";
reslist += ",Chest pain,ST elevation,Dysrhythmia,Bradycardia,Tachycardia,Shortness of breath";
reslist += ",Bradypnea,Tachypnea,Hypoxemia,Hyperthermia or fever,Hypothermia,Hypotension";
reslist += ",Hypertension,Decreased MAP,Increased MAP,Increased FiO2,Acute bleeding";
reslist += ",Acute change in urine output,RN or MD Concern (Comments),Peer consultation";
reslist += ",Failure to respond to treatment,Other";
SetIndIfResultContains(12,"","9991600100632","","",reslist);

SetIndIfResultContains(12,"","9991020100386","","","");

reslist = "None,Dystocia,Preterm labor,Abruptio placentae,Placenta previa,Placental retention";
reslist += ",Cord prolapse,Uterine rupture,Amniotic embolism,Other";
SetIndIfResultContains(12,"","9990000012126","","",reslist);

reslist = "CPAP facial,CPAP gap present,CPAP nasal,CPAP vent,ETT nasal,ETT oral";
reslist += ",Incubator oxygen,Laryngeal mask airway,Nasal cannula gap present";
reslist += ",Nasal cannula high flow,Nasal cannula low flow";
SetIndIfResultContains(12,"","9991733888883","","",reslist);

SetIndIfResultContains(12,"","3045001036","","","");
SetIndIfResultContains(12,"","3045001034","","","");

SetIndIfResultContains(12,"","3045001038","","","1:1,1:2,1:3");
reslist = "Continuous veno-venous hemofiltration";
reslist += ",Continuous veno-venous hemodialysis";
reslist += ",Continuous veno-venous hemodiafiltration";
reslist += ",Slow continuous ultrafiltration";
SetIndIfResultContains(12, "", "9990008100010", "", "", reslist);
reslist = "Initiated,Continuous,Restarted,Off/System charge,Off/Recirculating";
reslist += ",Off/Blood returned,Off/No blood returned,Discontinued";
SetIndIfResultContains(12,"","9990008100020","","",reslist);

SetIndIfResultContains(12,"","9993041001004","","","");
SetIndIfResultContains(12,"","9993041001003","","","");
            SetIndIfResultContains(12, "", "9991600100681", "", "", "");
            SetIndIfResultContains(12, "", "9991600100682", "", "", "");

        }

        private void CountAssessments(int bucket_size, int ind)
        {
            int ct;
            string codelist;
            string reslist;
            List<gBucket> buckets;

            SetBucketSize(bucket_size);

            //
            //VS group
            //
            buckets = new List<gBucket>();
            codelist = "304987655,9990007096285,304987657,3045001025,3045001064,304987666";
            codelist += ",9990304100017,9993049900009,30454321,9990000006294,9993041120042,3045001041";
            codelist += ",9990304001499,3045001018,9993040103255";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, "VS");
            //ct = CountBuckets(buckets);
            //CheckAssessment(ct, "VS=" + ct);
            //ShowBuckets(buckets);

            //
            //Pulmonary group
            //
            buckets = new List<gBucket>();
            codelist = "3045001051";
            reslist = "Non-rebreather mask,Nasal cannula,High flow,Aerosol mask";
            reslist += ",Bag valve device,BiPAP,Blow-by,CPAP,Face tent,Isolette,Oxyhood";
            reslist += ",Oxymask,Partial rebreather mask,Reservoir nasal cannula,Simple mask";
            reslist += ",T-piece,Trach mask,Ventilator,Venturi mask";
            reslist += ",Speaking valve,Incubator,Bag-self inflating,Bag-flow inflating,Helium oxygen,Sub-ambient Oxygen";
            AddBuckets(buckets, "", codelist, "", "", reslist);
            codelist = "3045001052,3045001053";
            AddBuckets(buckets, "", codelist, "", "");
            codelist = "9990000302570";
            reslist = "Agonal,Apnea,Bradypnea,Cheyne-Stokes,Kussmaul";
            reslist += ",Obstructed,Periodic,Regular,Tachypnea";
            AddBuckets(buckets, "", codelist, "", "", reslist);
            codelist = "9993040109339";
            reslist = "Regular,Irregular,Shallow,Deep";
            AddBuckets(buckets, "", codelist, "", "", reslist);
            codelist = "9993040109337";
            reslist = "Labored,Unlabored";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040109338";
            reslist = "Abdominal muscle use,Accessory muscle use,Drooling,Gasping";
            reslist += ",Grunting,Head bobbing,Nasal flaring,Pursed lips,Retractions,Tripod position";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000302580";
            reslist = "Symmetrical,Asymmetrical,Trachea deviates right,Trachea midline";
            reslist += ",Trachea deviates left,Paradoxical,Sunken chest,Pigeon chest";
            reslist += ",Subcutaneous emphysema,No chest expansion,Cylinder shaped";
            reslist += ",Flattened,Right clavicular deformity,Left clavicular deformity";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000302390,9990000302400,9990000302410";
            reslist = "Clear,Diminished,Fine,Coarse,Rales,Rhonchi,Crackles";
            reslist += ",Expiratory wheezes,Inspiratory wheezes,Stridor,Pleural rub";
            reslist += ",Tubular,Absent";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000451120,9990000451080,9990000451040";
            reslist = "Copious,Large,Moderate,Small,None";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000315800";
            reslist = "Bed therapy,Cough assist,CPAP,EzPAP,Flutter valve";
            reslist += ",IPV device,Manual percussion,NT suction,PEP Therapy";
            reslist += ",Percusser,Postural drainage,Vibralung,Vest";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000325950";
            reslist = "Tollerated well,Tollerated fairly well,Tolerated poorly,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);
            codelist = "9991733666660";
            reslist = "Apnea,Bradycardia,Desaturation,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000344210,9990000316380,9990000344220,9990000344230,9990007096450";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "9990000344250";
            reslist = "Blow-by oxygen,Oxygen,Positive pressure ventilation,Self limiting,Suction,Tactile stimulation";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000344280";
            reslist = "Aminophylline,Caffeine,High flow oxygen,Intubated,Medication dose change,Nasal cannula,Nasal CPAP";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000006808,3041733124512,9991733666661";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "9991600100035,9991600100039";
            reslist = "Point of care,To lab";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991600100048";
            reslist = "Oral mouthpiece,Mask,Trach,Ventilator,NPPV,Blow-by,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990160238401,9990160238501";
            reslist = "Clear,Diminished,Absent,Crackles,Stridor,Wheeze,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);
            codelist = "9993040104500";
            reslist = "Scant,Small,Moderate,Copious,Thin,Thick,Clear,Blood tinged,Tan,White,Yellow";
            AddBuckets(buckets, "", codelist, "", "", reslist);
            codelist = "9990000400613,9993040102736,9993040102752,9993040106219,9993040106220,9993040106221,9993040106222";
            AddBuckets(buckets, "", codelist, "", "", "");

            codelist = "9993040108077,9993040108078,9993040108079";
            AddBuckets(buckets, "", codelist, "", "", "");

            AnalyzeBuckets(buckets, ind, bucket_size, "Pulmonary");
            //ct = CountBuckets(buckets);
            //CheckAssessment(ct, "Pulmonary=" + ct);
            //ShowBuckets(buckets);

            //
            //Cardiovascular group
            //
            buckets = new List<gBucket>();
            codelist = "3045001065";
            reslist = EXACT_MATCH_PREFIX + "SR";
            AddBuckets(buckets, "", codelist, "", "", reslist);
            reslist = EXACT_MATCH_PREFIX + "SB";
            AddBuckets(buckets, "", codelist, "", "", reslist);
            reslist = EXACT_MATCH_PREFIX + "ST";
            AddBuckets(buckets, "", codelist, "", "", reslist);
            reslist = "Sinus rhythm,Sinus bradycardia,Sinus tachycardia,Sinus arrest,Sinus arrhythmia,A-paced,V-paced,A-V paced,Atrial paced,Ventricular paced,A-V Sequential paced";
            reslist += ",Agonal,Asystole,A-fib,A-fib w/RVR,Atrial fibrillation";
            reslist += ",Atrial flutter,Heart block,Junctional accelerated,Junctional rhythm";
            reslist += ",Junctional tachycardia,PEA,SVT";
            reslist += ",Pulseless electrical activity,Supraventricular tachycardia,VF,VT,Ventricular fibrillation,Ventricular tachycardia,Other,MAT,WAP";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "3045001066";
            reslist = "1st degree AVB,2nd degree AVB (Mobitz I, Wenckebach)";
            reslist += ",2nd degree AVB (Mobitz II),3rd degree AVB,Bundle branch block,Idioventricular";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "3045001067";
            reslist = "PVCs,Premature ventricular contractions,Unifocal PVCs,Multifocal PVCs";
            reslist += ",Couplet PVCs,Triplet PVCs,PACs,Premature atrial contractions,Aberrent conduction";
            reslist += ",Ectopic beats,Fusion beats,Junctional escape beats,Non-conducted PACs";
            reslist += ",Paroxysmal atrial tachycardia,Paroxysmal supraventricular tachycardia,PSVT,PJCS";
            reslist += ",Premature junctional contractions,Ventricular escape beats";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040108650,9993040108651,9993040108652,9993040108653";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "9993040100446";
            reslist = "S1,S2,S3,S4,Click,Device,Distant,Friction rub";
            reslist += ",Gallop,Holosystolic murmur,Mechanical valve click,Muffled";
            reslist += ",Murmur,Pericardial rub,No adventitious heart sounds";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040101320";
            //reslist = "Off,A paced,V paced,A/V paced,AAI,AAI-DDD (MVP),AOO,DVI,DOO,DDD";
            //reslist += ",VVI,VOO,VDD,AAR,AAR-DDDR (MVP),AOOR,DVR,DOOR,DOI,DDDR,DDR,VVR";
            //reslist += ",VOOR,VDDR";
            reslist = "";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040101316,9993040101317";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "9993040001044";
            reslist = "Red,Pink,Pale,Dusky,Purple,Mottled,Ecchymotic,Soft,Firm";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040001045";
            reslist = "Greater than 3 seconds,Less than or equal to 3 seconds,Brisk,Sluggish";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040001048,9993040001049";
            reslist = "Verified,Absent";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000303200,9990000303210,9990000303220,3045001045,9990000303270,9990000303280";
            codelist += ",9990000303320,9990000303330,3045001012,3045001014,9990000303390";
            codelist += ",9990000303400,3045001013,3045001015";
            reslist = "0,+1,+2,+3,+4,Doppler";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "3045001004,3045001002,3045001003,3045001001";
            reslist = "Less than/equal to 2 seconds,Greater than/equal to 3 seconds,No return";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000302900,9993045006597,9990000302920,9993045006598,9993040101373";
            codelist += ",9993040101374,9993040101375,9993040101376,3040000000022,99930400000027";
            codelist += ",3040000000028,9993040000062,99930400000056,3040000000050,9993040000044";
            codelist += ",9993040000035,9993040000045,9993040000049,9993047096403,9993040000050";
            codelist += ",9993040000064,9993040000065,3045001056,3045001055";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "9991733888889";
            reslist = "Acrocyanosis,Capillary refill no return,Capillary refill sluggish (>2 seconds)";
            reslist += ",Circumoral,Cyanosis" + CHAR_COMMA + " centralized,Cyanosis" + CHAR_COMMA + " peripheral";
            reslist += ",Generalized,Localized,Mottled,Pale,Ruddy";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000302650";
            reslist = "Absent,Murmur,Murmer-intermittent,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991733888890";
            reslist = "0,+3,+1,Unequal,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991733999991";
            reslist = "Moderate,Non-pitting,Severe,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040100223";
            reslist = "Circumocular,Circumoral,Nailbed,Acrocyanosis,Facial";
            reslist += ",Generalized,Oral mucosa,Underlying,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "3045001000";
            reslist = "Normal (less than/equal to 2 seconds" + CHAR_COMMA + " all extremities)";
            reslist += ",Sluggish (greater than/equal to 3 seconds" + CHAR_COMMA + " all extremities)";
            reslist += ",No return,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000303180,9990000303240,9990000303300,9990000303370";
            reslist = "Acrocyanosis,Appropriate for ethnicity,Ashen,Black,Bronze";
            reslist += ",Dusky,Ecchymosis,Flushed,Gray,Jaundiced";
            AddBuckets(buckets, "", codelist, "", "", reslist);
            codelist = "9990007061360";
            reslist = "Capillary refill less than 3 sec,Cool fingers";
            reslist += ",Capillary refill greater than 3 sec,Dusky fingers";
            reslist += ",Numbness to fingers,Pale fingers";
            reslist += ",Tingling to fingers,Weakness to fingers";
            AddBuckets(buckets, "", codelist, "", "", reslist);
            codelist = "9993040103299,9993040103300";
            reslist = "Present,Absent";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040101321,9993040101322,9993040101323,9993040101324";
            reslist = "0,+1,+2,+3,+4,Doppler";
            AddBuckets(buckets, "", codelist, "", "", reslist);
            codelist = "99930434002,9993043034001";
            reslist = "0,+1,+2,+3,+4,Doppler,Sheath In";
            AddBuckets(buckets, "", codelist, "", "", reslist);
            codelist = "9993040107859,9993040107862";
            AddBuckets(buckets, "", codelist, "", "", "");

            AnalyzeBuckets(buckets, ind, bucket_size, "Cardiovascular");
            //ct = CountBuckets(buckets);
            //CheckAssessment(ct, "Cardiovascular=" + ct);
            //ShowBuckets(buckets);

            //
            //Neurological group
            //
            buckets = new List<gBucket>();
            codelist = "9993040101409";
            reslist = "0/4,1/4,2/4,3/4,4/4";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "3045001007,3045001039,9993040006316,9990304006317,9993040006318";
            codelist += ",9993040006319,9993040006320,9990000301930,9990000301910";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "9990000301920,9990000301900";
            reslist = "Brisk,Sluggish,Nonreactive";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040101162,9993040101163";
            reslist = "Absent,Weak,Moderate,Strong,Contracture";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000002280,9990000002279,3045001017";
            reslist = "Present,Absent";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "3045001016,9993040101166,9993040101167";
            reslist = "Absent,Weak,Moderate,Strong,Contracture";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000301980,9990000301940,9990000302000,9990000301960";
            reslist = "Responds to commands,Normal extension,Normal flexion,Tremors";
            reslist += ",Flaccid,Abnormal extension (Decerebrate),Abnormal flexion (Decorticate)";
            reslist += ",Movement to painful stimulus,No movement to painful stimulus";
            reslist += ",Non-purposeful movement,No tremor,Spastic";
            AddBuckets(buckets, "", codelist, "", "", reslist);
            reslist = STARTS_WITH + "0,-1,-2,-3,-4,P,SP,NP,Stim,Pain,DC,DB";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000301990,9990000301950,9990000302010,9990000301970";
            reslist = "Decreased,No sensation,Numbness,Pain,Tingling,Full sensation";
            reslist += ",No numbness,No pain,No tingling";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040102775,9993040102776,9993040102777,9993040102778";
            reslist = "Normal power,Cannot overcome resistance,Overcomes gravity";
            reslist += ",Can overcome resistance,Flicker of muscle,None";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000302190,9990000302180";
            reslist = "C1,C2,C3,C4,C5,C6,C7,C8,T1,T2,T3,T4,T5,T6,T7,T8,T9,T10,T11,T12";
            reslist += ",L1,L2,L3,L4,L5,S1,S2,S3,S4,S5";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "3045001023,9993040001207,3045001080,3045001081,9990000398011";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "3045001079";
            reslist = "-5,-4,-3,-2,-1,0,+1,+2,+3,+4"; //Any number between -5 and + 4
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040104675";
            reslist = "S,1,2,3,4";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040001216,3045001011";
            reslist = "Positive,Negative";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000450430,9990000450420";
            reslist = "Present,Weak,Absent";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000450990,9990000450410";
            reslist = "Intact,Impaired,Absent";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040103168,9993040103169";
            reslist = "Absent,Present";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040109102,9990000450470";
            reslist = "Absent,Present,Weak";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000450480";
            reslist = "Absent,Present,Weak,Strong,Coordinated,Uncoordinated,Gag present,Gag absent";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "3045001047";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "9993040001091";
            reslist = "Blindness - right,Blindness - left,Blurred vision";
            reslist += ",Visual field cut- right side,Visual field cut- right upper";
            reslist += ",Visual field cut- right lower,Visual field cut- left side";
            reslist += ",Visual field cut- left upper,Visual field cut- left lower";
            reslist += ",Diplopia- Bilateral,Diplopia - right";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000002216";
            reslist = "Strabismus - right,Strabismus - left,Droopy eyelid - right";
            reslist += ",Droopy eyelid - left,Double vision - right,Double vision - left,Unable to assess";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000002217";
            reslist = "Double vision - right,Double vision - left";
            reslist += ",Unable to look downward and inward - right,Unable to look downward and inward - left";
            reslist += ",Unable to assess";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000002218";
            reslist = "Unable to chew - right,Unable to chew - left";
            reslist += ",Unable to clench - right,Unable to clench - left";
            reslist += ",Absence of sensation - right,Absence of sensation - left";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000002219";
            reslist = "Medial strabismus -  right,Medial strabismus -  left";
            reslist += ",Unable to look laterally - right,Unable to look laterally - left";
            reslist += ",Double vision - right,Double vision - left";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000002220";
            reslist = "Facial paralysis - right,Facial paralysis - left,Loss of taste - right,Loss of taste - left";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000002221";
            reslist = "Unable to hear - right,Unable to hear - left";
            reslist += ",Ringing in ear - right,Ringing in ear - left";
            reslist += ",Involuntary eye movement - right,Involuntary eye movement - left";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000002222";
            reslist = "Altered gag reflex";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000002223";
            reslist = "Weakness in turning head - right,Weakness in turning head - left";
            reslist += ",Unable to shrug - right,Unable to shrug - left";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000002224";
            reslist = "Deviation of tongue - right,Deviation of tongue - left";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040001057";
            AddBuckets(buckets, "", codelist, "", "");
            codelist = "9990000302300";
            reslist = "Clear,Cloudy,Serous,Sanguineous,Serosanguineous";
            reslist += ",Purulent,Cherry,Straw,Yellow";
            AddBuckets(buckets, "", codelist, "", "");
            codelist = "9990000002230,9990000304510,9993040109138";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "9991600100259";
            reslist = "No untoward effects noted,Use of reversal agent(s)";
            reslist += ",Hypoxemia < 90% for > 1 min,Hypotension of bradycardia requiring intervention";
            reslist += ",Respiratory failure requiring intervention,Cardiac arrest or death";
            reslist += ",Sedation recovery time > 60 min,Unplanned admission or higher level of care";
            reslist += ",Respiratory distress,Unanticipated need for anesthesia involvement";
            reslist += ",Inability to complete procedure,No responsible adult for discharge escort";
            reslist += ",Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991070011101,99910701000111";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "9990000450560";
            reslist = "Aggression,Aura,Behavior pause,Bowel incontinence,Deja vu";
            reslist += ",Fearful,Giggles,Nausea,Oral Trauma,Smirks,Tremors,Urine incontinence,Vocalize";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000450570";
            reslist = "Eyes right,Eyes left,Head right,Head left,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000450580,9993040108685,9993040108686";
            reslist = "Head,Face,Eye,Hand,Arm,Leg,Foot,Jerking,Stiffening";
            reslist += ",Staring,Tonic-clonic Motion,Twitching,Drop";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000450590";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "9990000450600";
            reslist = "Somnolence,Aphasic,RUE paresis,LUE paresis,RLE paresis,LLE paresis";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000450620";
            reslist = "Aware of seizure,Word given,Word recalled,Word not recalled";
            reslist += ",Normal speech,Abnormal speech,Unable to respond";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040101196";
            reslist = "Generalized,Right,Left,Hand,Leg,Face,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040101198";
            reslist = "2,3,4,5,6,7,8,9,10"; // num > 1
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040100079";
            reslist = "Absent,Arching back or neck,Bicycling,Clonic jerking";
            reslist += ",Extension is greater than flexion,Jerky,Jittery/tremors";
            reslist += ",Lip smacking,Movements cease with containment,Movements continue despite containment";
            reslist += ",Rowing,Seizure activity,Tonic extension,Tonic flexion,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991733999990";
            reslist = "Bicycling,Eye deviation,Lip smacking,Movement ceases with containment";
            reslist += ",Movements continue despite containment,Tongue thrusting,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040102780";
            reslist = "Abnormal reflex,Extensionx,Frantic movement,Inconsolable";
            reslist += ",Lethargic,Medically paralyzed,Sedated,other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991140100006";
            reslist = "Quiet alert,Sleeping,Active alert,Lusty cry,Drowsy,Active with stimulation";
            reslist += ",Hoarse cry,Irritable,Jittery,Lethargic,Shrill cry";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040100076";
            reslist = "Hypertonic generalized,Hypertonic localized,Hypotonic generalized,Hypotonic localized,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000451750";
            reslist = "Absent,Present,Weak,Brisk,Clonus,Clonus Sustained";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000450440";
            reslist = "Absent,Asymmetric,Symmetric,UTA";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000450450";
            reslist = "Absent,Present,Weak,UTA";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000450460";
            reslist = "Absent,Present,Clonus,Hyperreflexive,Hyporeflexive,Weak,UTA";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040109103";
            reslist = "Absent,Present,Weak,Unable to assess";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040109104";
            reslist = "Absent,Asymmetrical,Symmetrical,Unable to assess";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991140100013,9991140100014";
            //reslist = "Boggy,Bulging,Caput saecundum,Cephalohematoma,Closed,Depressed";
            //reslist += ",Sunken,Flat,Full,Soft,Sutures approximated";
            AddBuckets(buckets, "", codelist, "", "", reslist);
            AnalyzeBuckets(buckets, ind, bucket_size, "Neurologic");
            //ct = CountBuckets(buckets);
            //CheckAssessment(ct, "Neurologic=" + ct);
            //ShowBuckets(buckets);

            //
            //OB assessment group
            //
            buckets = new List<gBucket>();
            codelist = "9991025006827";
            reslist = "Denies,Blurred,Floaters,Flashes,Decreased visual field,Hx. of visual disturbances";
            reslist += ",Unsure,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991025006826";
            reslist = "Denies,Present,Unsure";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991025006828";
            reslist = "Denies,Aching,Burning,Constant,Intermittent,Sharp,Unsure";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991025006829";
            reslist = "Denies,Present";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991025006830";
            reslist = "Denies,Nausea only,Vomiting,Dry Heaves";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000012301,9990000012302,9990000012303,9990000012305";
            reslist = "absent,diminished,normal,brisk,brisk/hyperactive,sustained";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000012306,9990000012304";
            reslist = "Absent,1 beat,2 beats,3 beats,4 beats,Sustained";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "1028000004,1028000001,9990102521012,1028000003,9991020100645,9991020100655";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "1028000007,1028000008,1028000009,9991020100648,9991020100657";
            reslist = "Normal,Bradycardia,Tachycardia,Indeterminate";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "1028000010,1028000011,1028000012";
            reslist = "Minimal,Moderate,Marked,Absent";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "1028000013,1028000014,1028000015";
            reslist = "15x15,10x10,Absent";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "1028000016,1028000017,1028000018";
            reslist = "None,Early,Variable,Late,Prolonged ,Intermittent,Recurrent";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991025051116,9991025111616";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "1028000022,1028000023,1028000024,9991020100652,9991020100661";
            reslist = "Category I,Category II,Category III";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000012331";
            reslist = "Reactive,Non-reactive";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000012332";
            reslist = "Reassuring,Non - reassuring";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "1028000040";
            reslist = "Intact,Spontaneous,AROM,PROM,PPROM,Bulging";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "128000043";
            reslist = "Clear,Meconium,Bloody,Purulent";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "128000045";
            reslist = "Scant,Small,Moderate,Large";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "1028000046";
            reslist = "Closed,Fingertip,1,2,3,4,5,6,7,8,9,Lip/rim,10";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991020100568";
            reslist = "Vaginal pack,Uterine balloon,Uterotonic agent given (see MAR)";
            reslist += ",Oxygen on,Legs elevated,MD notified,Bimanual exam per MD,Labs ordered";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991020100569";
            reslist = "Placed,Present,Removed";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000012127";
            reslist = "Firm,Firm with massage,Boggy";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000012128";
            reslist = "Midline,Right,Left";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000012309,9991025012136,9991025012137,9991025012139,9991025012144";
            AddBuckets(buckets, "", codelist, "", "", "");

            codelist = "9993040102317,9993040102318,9993040102319,9993040102320";
            AddBuckets(buckets, "", codelist, "", "", "");

            AnalyzeBuckets(buckets, ind, bucket_size, "OB Assess");
            //ct = CountBuckets(buckets);
            //CheckAssessment(ct, "OB Assess=" + ct);
            //ShowBuckets(buckets);
            buckets = new List<gBucket>();
            codelist = "9993040108530,3045001091";
            AddBuckets(buckets, "", codelist, "", "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, "Drains");

        }
        private int SetMaxWaivers(int imins)
        {
            string qstr = "";
            string rstr = "";
            double losx = _pat.los_hours * 60;
            int num_waivers = 0;

            if (imins == 20)
            {//then 'q15
                if (losx >= 180)
                    num_waivers = 2;
                else if (losx >= 80)
                    num_waivers = 1;
                else
                    num_waivers = 0;
            }
            else if (imins == 70) // Then 'q1
            {
                if (losx < 360)
                    num_waivers = 0;
                else
                    num_waivers = (int)losx / 360;
            }
            else if (imins == 40) // Then 'q30
            {
                if (losx < 180)
                    num_waivers = 0;
                else
                    num_waivers = (int)losx / 180;
            }
            if (imins == 20)
                qstr = "q15min";
            else if (imins == 70)
                qstr = "q1hr";
            else if (imins == 40)
                qstr = "q30min";
            Program.VerboseAudit(qstr + ":Max waivers allowed=" + num_waivers + " (LOS=" + losx + ")");

            return num_waivers;
        }

        private void AnalyzeBuckets(List<gBucket> buckets, int ind, int bucketsize, string group)
        {
            DateTime dt = DateTime.MinValue;
            int bnum = 0;
            List<gBucket> dtlist = new List<gBucket>();
            Program.VerboseAudit("---- Begin Assessment Group = " + group + " ----");

            var b = buckets.OrderBy(e => e.evdt).ToList();
            foreach (var item in b)
            {
                Program.VerboseAudit(item.evdt.ToString() + " := " + item.code);
                if (dt < item.evdt)
                {
                    //add dt to ary
                    bnum++;
                    dtlist.Add(item);
                    dt = item.evdt;
                }
            }

            int i, j, istart;
            double addmins = _pat.los_hours * 30.0; // 60/2.0;
            int minupperidx = 0;
            bool all_ok = false;
            int j_cannot_use_waiver = 0;
            int imins = bucketsize;
            int num_waivers = SetMaxWaivers(bucketsize);
            int w = num_waivers;
            DateTime upperdt;
            gBucket[] dtary = dtlist.ToArray();
            for (i = 0; i <= bnum - 2; i++)
            {
                istart = i;
                upperdt = dtary[i].evdt.AddMinutes(addmins);
                //Program.VerboseAudit("i:="+i+"  upperdt:=" + upperdt.ToString());

                //what is the min evdt >= upperdt?
                minupperidx = 0;
                if (addmins >= bucketsize / 2.0)
                {
                    for (j = i + 1; j <= bnum - 1; j++)  //For j = i + 1 To b2num
                    {
                        //Program.VerboseAudit("dtary[" + j + "].evdt:=" + dtary[j].evdt.ToString());
                        if (dtary[j].evdt >= upperdt)
                        {
                            minupperidx = j;
                            //dvprint "min upper idx=" & minupperidx
                            //dvprint "min upper time=" & bucket2(j).eventdt
                            j = bnum - 1 + 1;
                        }
                    } // j
                }

                //Program.VerboseAudit("minupperidx:=" + minupperidx);
                if (minupperidx == 0)  //then half LOS not possible
                {
                    all_ok = false;
                    i = bnum - 1; //end loop
                }
                else
                {
                    j_cannot_use_waiver = 0;
                    all_ok = true;
                    //Program.VerboseAudit("dtary[" + i + "].evdt:=" + dtary[i].evdt.ToString()); //dvprint "i time=" & bucket2(i).eventdt
                    for (j = i; j <= minupperidx - 1; j++) //For j = i To minupperidx -1
                    {
                        if (dtary[j].evdt.AddMinutes(imins) < dtary[j + 1].evdt)
                            if (w > 0)  //then 'we can use a waiver
                                if (j != j_cannot_use_waiver) //  Then 'we can
                                    if (dtary[j].evdt.AddMinutes(2 * imins) >= dtary[j + 1].evdt)
                                    {
                                        dtary[j].using_waiver = true;
                                        j_cannot_use_waiver = j + 1;
                                        w = w - 1;
                                    }
                                    else
                                        all_ok = false;
                                else
                                    all_ok = false;
                            else
                                all_ok = false;

                    }

                    if (all_ok)
                        i = bnum; // 'end loop
                    else
                        //'reset waivers
                        for (j = 0; j <= bnum - 1; j++)
                            dtary[j].using_waiver = false;
                }

            }

            if (all_ok)
                SetInd(ind, "Qualifies for q" + imins + "mins for duration of half-LOS=" + addmins.ToString() + " minutes.");
            else
            {
                Program.VerboseAudit("Does not meet frequency criteria for indicator #" + ind);
                //            'assign indexes for the dump to follow
                istart = 0;
                minupperidx = bnum - 1;
            }

            if (num_waivers > w)
            {
                int w2 = 0;
                for (j = 0; j < bnum; j++)
                    if (dtary[j].using_waiver)
                    {
                        w2++;
                        Program.VerboseAudit("Waiver " + w2 + ": " + dtary[j].evdt.AddMinutes(imins).ToString());
                    }
            }

            Program.VerboseAudit("---- End Assessment Group = " + group + " ----");

            //sql = "select event_datetime,category,description,field_name,result from chart_item " & WhereBase & b_filter & b_excl
            //sql = sql & " and event_datetime between " & g_dbutil.SQL_DateTime(bucket2(istart).eventdt)
            //sql = sql & " and " & g_dbutil.SQL_DateTime(bucket2(minupperidx).eventdt) & " order by event_datetime"
            //'dvprint sql
            //rs.Open sql, g_cnADO
            //Do While Not rs.EOF
            //    dprint "  " & rs(0) & ": " & g_dbutil.DBToString(rs(1)) & "; " & g_dbutil.DBToString(rs(2)) & "; " & g_dbutil.DBToString(rs(3)) & "; " & g_dbutil.DBToString(rs(4))
            //    rs.MoveNext
            //Loop
            //rs.Close


            //ResetWaivers



        }


        private void CheckAssessment(int count, string desc)
        {
            //if (_inds[12].is_checked) return;          //skip if highest already checked
            if (count == 0) return;                    //skip if none

            // This should work the same as the original code:
            switch (FreqForCount(_pat.los_hours, count))
            {
                case Frequencies.Q30M:
                    SetInd(12, desc);
                    break;
                case Frequencies.Q1H:
                    SetInd(11, desc);
                    break;
                //case Frequencies.Q2H:
                //    SetInd(16, desc);
                //    break;
                //case Frequencies.Q4H:
                //    SetInd(15, desc);
                //    break;
                default:
                    break;
            }

        }

        private void ShowBuckets(List<gBucket> buckets)
        {
            string s = "";
            foreach (var e in buckets)
            {
                s = "(" + e.bucket + "," + e.code + "," + e.evdt.ToString() + ")";
                Program.VerboseAudit("Assessmt item:=" + s);
            }
        }


        // (this isn't really a Q1h count -- it is just a count) Make this a q1hr count
        private int ReturnQ1HrCount(string code_list, string result_list)
        {
            var buckets = new List<gBucket>();
            SetBucketSize(60);
            AddBuckets(buckets, "", code_list, "", "", result_list);
            return CountBuckets(buckets);
        }

        private bool IsQ1Hour(int count)
        {
            return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q1H);
        }
        private bool IsQ30(int count)
        {
            return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q30M);
        }

        // Use this if counting safety buckets
        //private void CheckSafety(int count, string desc)
        //{
        //    if (_inds[13].is_checked) return;             //skip if highest already checked
        //    if (count == 0) return;
        //    switch (FreqForCount(_pat.los_hours, count))
        //    {
        //    case Frequencies.Q30M:
        //        SetInd(13, desc + " q30min");
        //        break;
        //    case Frequencies.Q1H:
        //    case Frequencies.Q2H:
        //        SetInd(12, desc + " q2h");
        //        break;
        //    default:
        //        Program.VerboseAudit(desc + ": " + count + " in " + Math.Round(_pat.los_hours) + " hours is not enough");
        //        break;
        //    }
        //}

        private void Check_13()
        {
            string reslist;
            int ct = 0;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Inpt 13. Wound/Injury Management");
            Program.VerboseAudit("---------------");

            //LDA tab mappings  
            //LDA tab mappings
            bool stage1 = ResultContains("", "9993041000044", "", "", EXACT_MATCH_PREFIX + "Stage I");
            bool piv = CheckPIV();
            if (!stage1)
            {
                reslist = "Stage II,Stage III,Stage IV,Unstageable,Deep tissue injury";
                SetIndIfResultContains(13, "", "9993041000044", "", "", reslist);
                reslist = "";
                SetIndIfResultContains(13, "", "9990000303750", "", "", reslist);
                SetIndIfResultContains(13, "", "9990007061190", "", "", reslist);
                SetIndIfResultContains(13, "", "9993046629812", "", "", reslist);
                SetIndIfResultContains(13, "", "9990007061270", "", "", reslist);
                SetIndIfResultContains(13, "", "9990000001493", "", "", reslist);
                SetIndIfResultContains(13, "", "9990000001494", "", "", reslist);
                SetIndIfResultContains(13, "", "9990007061240", "", "", reslist);
                SetIndIfResultContains(13, "", "9990000303780", "", "", reslist);
                SetIndIfResultContains(13, "", "9993040102846", "", "", reslist);
                SetIndIfResultContains(13, "", "99930413000000,9993041000077,9993040021270,9990000303910", "", "", reslist);

                if (!piv)
                {
                    reslist = "";
                    SetIndIfResultContains(13, "", "9990000303800", "", "", reslist);
                    SetIndIfResultContains(13, "", "9990000303820", "", "", reslist);
                    SetIndIfResultContains(13, "", "9990000304850", "", "", reslist);
                }
            }

            reslist = "";
            SetIndIfResultContains(13, "", "9993040104036", "", "", reslist);
            SetIndIfResultContains(13, "", "9993040104037", "", "", reslist);
            SetIndIfResultContains(13, "", "9993040023765", "", "", reslist);
            SetIndIfResultContains(13, "", "9993040100564", "", "", reslist);
            SetIndIfResultContains(13, "", "9991420100006", "", "", reslist);
            SetIndIfResultContains(13, "", "9991420100007", "", "", reslist);
            SetIndIfResultContains(13, "", "9991420100009", "", "", reslist);
            SetIndIfResultContains(13, "", "9990000304500", "", "", reslist);
            SetIndIfResultContains(13, "", "9990000304510", "", "", reslist);
            SetIndIfResultContains(13, "", "9993040103946", "", "", reslist);
            SetIndIfResultContains(13, "", "9993040103947", "", "", reslist);
            SetIndIfResultContains(13, "", "9993040103948", "", "", reslist);
            SetIndIfResultContains(13, "", "9993040103951", "", "", reslist);
            SetIndIfResultContains(13, "", "9993040103954", "", "", reslist);
            SetIndIfResultContains(13, "", "9993040103957", "", "", reslist);
            SetIndIfResultContains(13, "", "9990007070177", "", "", reslist);
            SetIndIfResultContains(13, "", "9990007070178", "", "", reslist);
            SetIndIfResultContains(13, "", "9990007070179", "", "", reslist);
            SetIndIfResultContains(13, "", "9993040001021", "", "", reslist);
            SetIndIfResultContains(13, "", "9993040000088", "", "", reslist);
            SetIndIfResultContains(13, "", "9993040001022", "", "", reslist);
            SetIndIfResultContains(13, "", "9993040103228", "", "", reslist);
            SetIndIfResultContains(13, "", "9990007085420", "", "", reslist);
            SetIndIfResultContains(13, "", "9990000016070", "", "", reslist);
            SetIndIfResultContains(13, "", "9993040102649", "", "", reslist);
            SetIndIfResultContains(13, "", "9993040102644", "", "", reslist);
            SetIndIfResultContains(13, "", "9993040102648", "", "", reslist);
            SetIndIfResultContains(13, "", "9993040102660", "", "", reslist);
            SetIndIfResultContains(13, "", "9993040102643", "", "", reslist);
            SetIndIfResultContains(13, "", "9993040102662", "", "", reslist);
            SetIndIfResultContains(13, "", "9993040102742", "", "", reslist);
            SetIndIfResultContains(13, "", "9993040102748", "", "", reslist);
            SetIndIfResultContains(13, "", "9993040102741", "", "", reslist);
            SetIndIfResultContains(13, "", "9993040102744", "", "", reslist);
            SetIndIfResultContains(13, "", "9993040102747", "", "", reslist);
            SetIndIfResultContains(13, "", "9993040102750", "", "", reslist);
            SetIndIfResultContains(13, "", "9990000396120", "", "", reslist);
            SetIndIfResultContains(13, "", "9990007061290", "", "", reslist);
            SetIndIfResultContains(13, "", "9990000006333", "", "", reslist);
            SetIndIfResultContains(13, "", "9993040108525", "", "", reslist);
            if (!piv)
            {
                SetIndIfResultContains(13, "", "3045001032", "", "", reslist);
                SetIndIfResultContains(13, "", "9990007096660", "", "", reslist);
                SetIndIfResultContains(13, "", "9990000304850", "", "", reslist);
                SetIndIfResultContains(13, "", "9990007073550", "", "", reslist);
            }
            SetIndIfResultContains(13, "", "3045001041", "", "", reslist);
            SetIndIfResultContains(13, "", "9990000301270", "", "", reslist);
            SetIndIfResultContains(13, "", "9990007080680", "", "", reslist);
            SetIndIfResultContains(13, "", "9993040103205", "", "", reslist);
            SetIndIfResultContains(13, "", "9993040103206", "", "", reslist);
            SetIndIfResultContains(13, "", "9993040103209", "", "", reslist);
            SetIndIfResultContains(13, "", "9993040000199", "", "", reslist);
            SetIndIfResultContains(13, "", "9990304840001", "", "", reslist);
            SetIndIfResultContains(13, "", "9990304840002", "", "", reslist);
            SetIndIfResultContains(13, "", "9990000370090", "", "", reslist);


            reslist = AVOID_NEGATIVE + "Injury/trauma";
            SetIndIfResultContains(13, "", "9990000304110", "", "", reslist);
            reslist = "Traction,fracture";
            SetIndIfResultContains(13, "", "9990000304110", "", "", reslist);
            reslist = AVOID_NEGATIVE + "Injury/trauma";
            SetIndIfResultContains(13, "", "9990000304130", "", "", reslist);
            reslist = "Injury/trauma,Traction,fracture";
            SetIndIfResultContains(13, "", "9990000304130", "", "", reslist);
            SetIndIfResultContains(13, "", "9990304000122", "", "", "3");
            reslist = "Tea,Rusty,Peach,Cherry,Pink,Ketchup";
            SetIndIfResultContains(13, "", "9990000006298", "", "", reslist);

            SetIndIfResultContains(13, "", "9993040401216", "", "", "Peritoneal port");
            reslist = "Red,Pink,Pale,Dusky,Purple,Mottled,Ecchymotic,Soft,Firm";
            SetIndIfResultContains(13, "", "9993040001044", "", "", reslist);

            SetIndIfResultContains(13, "", "3040001333", "", "", "");
            SetIndIfResultContains(13, "", "3040001334", "", "", "");

            SetIndIfResultContains(13, "", "9993040101378", "", "", "Clean,No clot");
            SetIndIfResultContains(13, "", "9993040101377", "", "", "Done");
            SetIndIfResultContains(13, "", "9991020100022", "", "", "");
            reslist = "Vaginal pack,Uterine balloon,Uterotonic agent given (see MAR)";
            reslist += ",Oxygen on,Legs elevated,MD notified,Bimanual exam per MD,Labs ordered";
            SetIndIfResultContains(13, "", "9991020100568", "", "", reslist);
            reslist = "Placed,Present,Removed";
            SetIndIfResultContains(13, "", "9991020100569", "", "", reslist);
            reslist = "Applied (comment number),Changed  (comment number),Marked";
            reslist += ",Reinforced,Site care,Staples removed (comment number)";
            SetIndIfResultContains(13, "", "9991733888867", "", "", reslist);
            reslist = "Drainage,Malodorous,Red,moist,gel foam,triple dye";
            SetIndIfResultContains(13, "", "9991733888850", "", "", reslist);
            reslist = "Dry,Moist,Cannulated,Clamp off,Clamp on,Care done,2 cord vessels,3 cord vessels";
            SetIndIfResultContains(13, "", "9991140100024", "", "", reslist);
            reslist = "Gauze in place,Petroleum jelly applied,Petroleum jelly gauze applied,Other";
            SetIndIfResultContains(13, "", "9991140100026", "", "", reslist);
            reslist = "Bleeding,Edematous,Necrotic,Pink,Reddened";
            reslist += ",Serosanguinous drainage,Serous drainage,Other";
            SetIndIfResultContains(13, "", "9991140100027", "", "", reslist);

            SetIndIfResultContains(13, "", "9991601001106", "", "", "");
            SetIndIfResultContains(13, "", "9991601001122", "", "", "");
            reslist = "Finger aluminum,Finger guard aluminum,Finger padded aluminum";
            reslist += ",Finger frog aluminum,Cock up wrist velcro,Ulnar gutter,Volar";
            reslist += ",Arm aluminum,Short arm,Long arm,Short arm fiberglass,Long arm fiberglass";
            reslist += ",Sugar tong,Double sugar tong- arm,Short leg,Long leg,Short leg fiberglass";
            reslist += ",Long leg fiberglass,Sugar tong leg,Robert Jones,Modified Robert Jones";
            reslist += ",Air cast stirrup,Posterior ankle,Cadillac (stirrup/posterior combo),Other";
            SetIndIfResultContains(13, "", "9991600100112", "", "", reslist);
            reslist = "Boxer,Colles,Cylinder,Short arm,Long arm,Long arm hanging";
            reslist += ",Short arm fiberglass,Long arm fiberglass,Navicular fiberglass";
            reslist += ",Short arm navicular,Long arm navicular,Short leg,Long leg,Short leg fiberglass";
            reslist += ",Long leg fiberglass,Cylinder fiberglass,Patella with bearing,Spica single";
            reslist += ",Spica double,Spica single fiberglass,Spica double fiberglass";
            reslist += ",Reinforce" + CHAR_COMMA + " minor,Reinforce" + CHAR_COMMA + " major,Reinforced fiberglass,Monovalve";
            reslist += ",BiValve,Window,Window by tech,Other";
            SetIndIfResultContains(13, "", "9991600100115", "", "", reslist);
            reslist = "Boot,Shoe,Post-op,Walking,Cam walker,Air Cast,Other";
            SetIndIfResultContains(13, "", "9991600100117", "", "", reslist);
            reslist = "Shoulder immobilizer,Walk wraps,Humeral fracture brace";
            reslist += ",Knee immobilizer,Neoprene knee brace,Other";
            SetIndIfResultContains(13, "", "9991600100118", "", "", reslist);
            reslist = "Abductor pillow,Arm elevation pillow,Elastic bandage, 2 inch";
            reslist += ",Elastic bandage" + CHAR_COMMA + " 3 inch,Elastic bandage" + CHAR_COMMA + " 4 inch,Arm sling";
            reslist += ",Rib belt,Clavicle shoulder strap,Dressing" + CHAR_COMMA + " soft wrap knee";
            reslist += ",Other,Elastic Wrist";
            SetIndIfResultContains(13, "", "9991600100119", "", "", reslist);
            reslist = "Complaining of back injury,Complaining of head/neck injury";
            reslist += ",Fall,Injury,MVC,Sporting accident,UTA=Unable to assess,Other";
            SetIndIfResultContains(13, "", "9991600100122", "", "", reslist);
            reslist = "Rigid cervical collar,Backboard,Soft cervical collar";
            reslist += ",Head blocks or towels,Cervical immobilization device,Other";
            SetIndIfResultContains(13, "", "9991600100123", "", "", reslist);
            reslist = "Skeletal,Skin,Cervical,Other";
            SetIndIfResultContains(13, "", "9991600000289", "", "", reslist);
            reslist = "Suture repair,Wound care,Fracture care,Joint immobilization,Joint reduction";
            SetIndIfResultContains(13, "", "9990000008445", "", "", reslist);

            SetIndIfResultContains(13, "", "9991600100348", "", "", "");
            // any is fine

            reslist = "Level red,Level yellow,Non-activation (Green)";
            SetIndIfResultContains(13, "", "9991600100426", "", "", reslist);
            SetIndIfResultContains(13, "", "9990160100151", "", "", reslist);
            reslist = AVOID_NEGATIVE + "Trauma/Injury";
            SetIndIfResultContains(13, "", "9990000304900", "", "", reslist);
            SetIndIfResultContains(13, "", "9990000002106", "", "", reslist);
            SetIndIfResultContains(13, "", "9990000002107", "", "", reslist);
            SetIndIfResultContains(13, "", "9990000002108", "", "", reslist);
            SetIndIfResultContains(13, "", "9990000002109", "", "", reslist);
            SetIndIfResultContains(13, "", "9993040108746", "", "", reslist);
            SetIndIfResultContains(13, "", "9990000304930", "", "", reslist);
            SetIndIfResultContains(13, "", "9990000002117", "", "", reslist);
            SetIndIfResultContains(13, "", "9990000304940", "", "", reslist);
            SetIndIfResultContains(13, "", "9990000305690", "", "", reslist);
            SetIndIfResultContains(13, "", "9990000305690", "", "", "bleeding");

            reslist = "Ulcerations present,bleeding";
            SetIndIfResultContains(13, "", "9990000002113", "", "", reslist);

            reslist = AVOID_NEGATIVE + "Injury/Trauma";
            SetIndIfResultContains(13, "", "9990000304100", "", "", reslist);
            SetIndIfResultContains(13, "", "9990000304120", "", "", reslist);
            SetIndIfResultContains(13, "", "9990000303930", "", "", reslist);
            SetIndIfResultContains(13, "", "9990000303970", "", "", reslist);
            SetIndIfResultContains(13, "", "9990000304010", "", "", reslist);
            SetIndIfResultContains(13, "", "9990000304050", "", "", reslist);
            SetIndIfResultContains(13, "", "9990000304090", "", "", reslist);
            SetIndIfResultContains(13, "", "9990000450250", "", "", reslist);
            SetIndIfResultContains(13, "", "9990000450260", "", "", reslist);
            SetIndIfResultContains(13, "", "9990000304190", "", "", reslist);
            reslist = "fracture";
            SetIndIfResultContains(13, "", "9990000304100", "", "", reslist);
            SetIndIfResultContains(13, "", "9990000304120", "", "", reslist);
            SetIndIfResultContains(13, "", "9990000303930", "", "", reslist);
            SetIndIfResultContains(13, "", "9990000303970", "", "", reslist);
            SetIndIfResultContains(13, "", "9990000304010", "", "", reslist);
            SetIndIfResultContains(13, "", "9990000304050", "", "", reslist);
            SetIndIfResultContains(13, "", "9990000304090", "", "", reslist);
            SetIndIfResultContains(13, "", "9990000450250", "", "", reslist);
            SetIndIfResultContains(13, "", "9990000450260", "", "", reslist);

            SetIndIfResultContains(13, "", "9990000303940,9990000303980,9990000303990,9990000304020,9990000304060,9990000304070,9991601450250", "", "", reslist);

            reslist = "Lesion,Fistula,Foreign body";
            SetIndIfResultContains(13, "", "9990000304190", "", "", reslist);

            reslist = "Bloody,Bright red,Coffee ground,Dark red";
            SetIndIfResultContains(13, "", "9990007085590", "", "", reslist);
            SetIndIfResultContains(13, "", "99930400003205", "", "", "");
            reslist = "Frenulectomy,Lacerated";
            SetIndIfResultContains(13, "", "9990000002114", "", "", reslist);
            SetIndIfResultContains(13, "", "9993040304870,9993040108530,3045001091", "", "", "");
            SetIndIfResultContains(13, "", "9993040102647,9993040102642,9993040102740,9993040102746", "", "", "");
            string codelist = "3045001041,9993040108062,9993040108060,9993040108059,9993040108070,9993040108068,3040108067";
            SetIndIfResultContains(13, "", codelist, "", "", "");

            codelist = "9993040108108,9993040108102,9993040108114,9993040108109,9993040108103,";
            codelist += "9993040108115,9993040108110,9993040108104,9993040108116,9993040108111,";
            codelist += "9993040108105,9993040108117,9993040108112,9993040108106,9993040108118,";
            codelist += "9993040108113,9993040108119";
            SetIndIfResultContains(13, "", codelist, "", "", "");
            SetIndIfResultContains(13, "", EXACT_MATCH_PREFIX + "9993040108", "", "", "");

            codelist = "9993040109510,9993040109511,9993040108145,9993040108135,9993040108140";
            codelist += ",9993040108146,9993040108136,9993040108141,9993040108147,9993040108137";
            codelist += ",9993040108142,9993040108148,9993040108138,9993040108143,9993040108149";
            codelist += ",9993040108139,9993040108144,9993040108150";
            SetIndIfResultContains(13, "", codelist, "", "", "");


        }

        private bool CheckPIV()
        {
            var query = StartNewQuery(SearchDepth.SearchPullPlus);
            query = AndItemFilter(query, "", "", "Placement,Removal,Dressing", "", "");
            query = AndItemFilter(query, "", "", "Midline Dual Lumen Catheter,Midline Single Lumen Catheter,Peripheral IV", "", "");
            query = query.Where(e => e.EVENT_DATETIME >= _pat.unit_arrival);
            query = query.Where(e => e.EVENT_DATETIME <= _pat.unit_departure.AddMinutes(15));
            int ct = query.Count();
            return (ct > 0);
        }

        //private void CheckAssessment(int count, string desc)
        //{
        //    if (_inds[18].is_checked) return;          //skip if highest already checked
        //    if (count == 0) return;                    //skip if none

        //    // This should work the same as the original code:
        //    switch (FreqForCount(_pat.los_hours, count)) {
        //        case Frequencies.Q30M:
        //            SetInd(18, desc);
        //            break;
        //        case Frequencies.Q1H:
        //            SetInd(17, desc);
        //            break;
        //        case Frequencies.Q2H:
        //            SetInd(16, desc);
        //            break;
        //        case Frequencies.Q4H:
        //            SetInd(15, desc);
        //            break;
        //        default:
        //            break;
        //    }

        //}

        private void Check_14()
        {
            string reslist = "";

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Inpt 14. Medication Management >= 5 Meds");
            Program.VerboseAudit("---------------");

            int ct = 0;
            int medct = 0;

            // COUNT OF MEDS > 5 goes here
            medct = CountMeds();

            reslist = "4 Ounces fruit juice,8 ounces fruit juice,IV/Medication (See MAR)";
            ct += CountResultContains("", "9993040000345", "", "", reslist);
            ct += CountResultContains("", "9990000344170", "", "", "");
            ct += CountResultContains("", "3040007191", "", "", "");

            if (ct + medct >= 5) SetInd(14, "Total Med Count=" + (ct + medct));


        }

        private int CountMeds()
        {
            string descript;
            string drugclass;
            int ct = 0;
            List<med_data> mlist = new List<med_data>();
            bool toadd = true;

            var query = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
            query = query.Where(e => e.CODE.StartsWith("med"));
            query = query.Where(e => e.EVENT_DATETIME >= _pat.unit_arrival);
            query = query.Where(e => e.EVENT_DATETIME <= _pat.unit_departure);
            query = query.OrderBy(e => e.EVENT_DATETIME);
            foreach (var item in query)
            {
                var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                descript = arr[0];
                drugclass = arr[1];
                //exclude #29, 35, 48
                if ((drugclass != "29") && (drugclass != "35") && (drugclass != "48"))
                {
                    var md = new med_data();
                    md.code = item.CODE.ToUpper();
                    md.descript = descript;
                    md.drugclass = drugclass;
                    md.evdt = item.EVENT_DATETIME;
                    toadd = true;
                    if (ct > 0)
                    {
                        foreach (var m in mlist)
                        {
                            if (m.code == item.CODE.ToUpper() && m.evdt == item.EVENT_DATETIME) toadd = false;
                        }
                    }
                    if (toadd && (ct < 5))
                    {
                        mlist.Add(md);
                        ct++;
                    }

                }
            }

            int a = 0;
            Program.VerboseAudit("Meds found=" + ct);
            foreach (var m in mlist)
            {
                Program.VerboseAudit("  " + a++ + ": code=" + m.code + "; drugclass=" + m.drugclass + "; time=" + m.evdt.ToString() + "; descript=" + m.descript);
            }
            return ct;
        }

        private void Check_151617()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Inpt 15. Admitted");
            Program.VerboseAudit("ED Inpt 16. Transferred to another Facility");
            Program.VerboseAudit("ED Inpt 17. Expired");
            Program.VerboseAudit("---------------");


        }

        //private bool IsICU()
        //{
        //    switch (_pat.unit_name)
        //    {
        //        case "ICU":
        //        case "3BR":
        //        case "CTIC":
        //        case "NIC":
        //        case "PCU":
        //        case "TICU":
        //        case "CCB":
        //        case "RICU":
        //        case "CCUS":
        //            return true;
        //        default:
        //            return false;
        //    }
        //}

        //private bool IsTele()
        //{
        //    switch (_pat.unit_name)
        //    {
        //        case "4EST":
        //        case "2NW":
        //        case "3AE":
        //        case "5MEH":
        //        case "5NW":
        //        case "6MEH":
        //        case "6NW":
        //        case "7MEH":
        //        case "B4W":
        //        case "B5S":
        //        case "B5W":
        //        case "2NOB":
        //        case "CPLX":
        //        case "TELMS":
        //        case "MAN4":
        //            return true;
        //        default:
        //            return false;
        //    }
        //}


        //private void CountAssessments(int bucket_size)
        //{
        //    int ct;
        //    string codelist;
        //    string reslist;
        //    List<int> buckets;

        //    SetBucketSize(bucket_size);

        //    buckets = new List<int>();

        //    codelist = "97036712,118064,118066,118058,118132,305991507,118520,14305,142506594";
        //    codelist += ",12293217,118193,118068,118011,118146,306182838,306182805,305991322";
        //    codelist += ",118498,29718286,306327425,131172918,353080778";
        //    AddBuckets(buckets, "", codelist, "", "");
        //    reslist = "APD - All Purpose Drain (AZ),Biliary,Blake,CAPD,Drain,Constavac,Davol,G-J Tube,G-Tube,Hemovac";
        //    reslist += ",JP,J-Tube,PEG tube,Pelvic Drain,Pendrose Drain,Pericardial,T-Tube,Other";
        //    AddBuckets(buckets, "", "305142088", "", "", reslist);
        //    codelist = "305433923,633686082,305433661,481751506,481752199,304541034,316381370,305434227";
        //    codelist += ",316380803,119127,119141,304541048,7847308,18674661,306327593,7849724";
        //    codelist += ",7849861,7849891,7851959,18674834,7852147,12293217,30600122,7852257";
        //    codelist += ",306001049,7853949,7931900,7852980,7852983,7853157,7853190,7931902";
        //    codelist += ",7853207,7903597,7853235,7903643,7853418,7853426,7853430,7866522";
        //    codelist += ",18674837,7866633,14197072,7932422";
        //    AddBuckets(buckets, "", codelist, "", "");
        //    AddBuckets(buckets, "", "7868669", "", "", "Endotracheal,Nasal,Nasal Tracheal Suction,Oral Suction,Tracheal Suction");
        //    codelist = "7870437,7904435,18674857,7932312,18674862,7907957,18674869";
        //    codelist += ",303031350,325754541,303031329,532319071,345591779,304015183";
        //    codelist += ",345603431,303722796,303722810,303722826,303722841,303722857";
        //    codelist += ",303722871,303722885,303722900,790715,305433789,383694315";
        //    codelist += ",383695492,303722345,303693371,303722782";
        //    AddBuckets(buckets, "", codelist, "", "");
            
        //    ct = CountBuckets(buckets);
        //    CheckAssessment(ct, "All assessments count=" + ct);
        //    if (_inds[18].is_checked) return;


        //    //buckets = new List<int>();
        //    //codelist = "MH_ORD_Respiratory";
        //    //AddBuckets(buckets, "", codelist, "", "");
        //    //ct = CountBuckets(buckets);
        //    //CheckAssessment(ct, "Pulmonary=" + ct);
        //    //if (_inds[18].is_checked) return;

        //    //buckets = new List<int>();
        //    //codelist = "MH_ORD_VitalSigns";
        //    //AddBuckets(buckets, "", codelist, "", "");
        //    //ct = CountBuckets(buckets);
        //    //CheckAssessment(ct, "VS=" + ct);
        //    //if (_inds[18].is_checked) return;

        //    //buckets = new List<int>();
        //    //codelist = "CARDRHYTHM";
        //    //AddBuckets(buckets, "", codelist, "", "");
        //    //ct = CountBuckets(buckets);
        //    //CheckAssessment(ct, "CardRhythm=" + ct);
        //    //if (_inds[18].is_checked) return;

        //    //buckets = new List<int>();
        //    //codelist = "A_GlasgowComaScl,SC_S_ASMT_FregNeuroChks,A_MHPNeuroTube";
        //    //AddBuckets(buckets, "", codelist, "", "");
        //    //ct = CountBuckets(buckets);
        //    //CheckAssessment(ct, "Neuro=" + ct);
        //    //if (_inds[18].is_checked) return;

        //    //buckets = new List<int>();
        //    //codelist = "A_MHMPain,A_MHPain,A_Pain";
        //    //AddBuckets(buckets, "", codelist, "", "");
        //    //ct = CountBuckets(buckets);
        //    //CheckAssessment(ct, "Pain=" + ct);
        //    //if (_inds[18].is_checked) return;


        //}

        //=====================================================================
        // Group like assessments into buckets of time
        // Each bucket counts as one assessment in the frequency count

        private void SetBucketSize(int minutes)
        {
            _bucket_size = minutes;
        }

        //Add to the list of bucket numbers (redundant buckets are fine)
        private void AddBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field)
        {
            AddBuckets(bucket_list, cat, code_list, desc, field, "");
        }
        private void AddBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field, string result_list)
        {
            // Assign (virtual) bucket numbers to each assessment.  
            // Every x minutes after that is a new bucket.  Bucket zero = pull_start time.

            // get the chart items for the assessments
            var query = StartNewQuery(SearchDepth.SearchPullPlus);
            query = AndItemFilter(query, cat, code_list, desc, field, result_list);
            query = query.Where(e => e.EVENT_DATETIME >= _pat.unit_arrival);
            query = query.Where(e => e.EVENT_DATETIME <= _pat.unit_departure.AddMinutes(15));

            // This step is needed for those who want to count the # within a bucket, like Shands #24.
            // This will have no effect on those that count buckets.
            var query2 = (from item in query select new { item.EVENT_DATETIME, item.CODE });

            // figure out what buckets the events belong to
            var query3 = from item in query2
                         select new
                         {
                             bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                             code = item.CODE,
                             evdt = item.EVENT_DATETIME
                         };
            // Add to the list
            foreach (var item in query3)
            {
                var b = new gBucket();
                b.bucket = item.bucket;
                b.code = item.code;
                b.evdt = item.evdt;
                bucket_list.Add(b);
            }

            // This is just for auditing: Say what we are looking for and how many were found
            if (!String.IsNullOrEmpty(result_list)) {
                // print each word and if it was found or not
                int i = CountResultContains(cat, code_list, desc, field, result_list);
            } else {
                // print how many were found
                Program.VerboseAudit(Describe(cat, code_list, desc, field, ""));
            }
        }

        private int CountBuckets(List<gBucket> bucket_list)       // bucket list: (ha ha)
        {
            int x = -99;
            int result = 0;
            var query = from b in bucket_list
                        orderby b.bucket ascending
                        select b;
            foreach (var b in query)
            {
                if (x != b.bucket)
                {
                    result++;
                    x = b.bucket;
                }
            }
            if (result > 0) Program.VerboseAudit(result + " unique");
            return result;

        }


        private void AtLeastOneADL()
        {
            Program.Audit("---------------");
            if (!(_inds[1].is_checked || _inds[2].is_checked || _inds[3].is_checked)) {
                // Make #1 ADL default for EDInpt
                SetInd(1, "Defaulting to ADL Self due to lack of documentation.");
            }
        }

        private void HighestIndicatorInEachGroupWins()
        {
            int i, g;
            bool highest_is_on;
            string ind_list;

            Program.Audit("Select highest indicator in each group");
            
            g = 0;
            highest_is_on = false;
            // Go from bottom up - highest (lowest) indicator in each group wins
            for (i = MAX_INDS; (i >= 1); i--) {
                if (_inds[i].radio_group > 0) {
                    if (_inds[i].radio_group != g) {
                        //this is a new group
                        g = _inds[i].radio_group;
                        highest_is_on = _inds[i].is_checked;
                    } else {
                        //same group
                        if (highest_is_on) {
                            _inds[i].is_checked = false;             //uncheck a lower number
                        } else {
                            highest_is_on = _inds[i].is_checked;     //save this one
                        }
                    }
                }
            } // next i
            
            //Echo the indicators for an Audit (no classification will be saved)
            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++) {
                if (_inds[i].is_checked) ind_list += "," + i;
            } // next i

            Program.Audit("Final list = " + ind_list.Substring(1));
            Program.Audit("---------------");
        }


        private void CheckProcs()
        {
            //CheckProc_1();
            //CheckProc_2();
            //CheckProc_3();
            //CheckProc_4();
            //CheckProc_5();
            //CheckProc_6();
            //CheckProc_7();
            //CheckProc_8();
            //CheckProc_9();
            //CheckProc_10();
            //CheckProc_11();

        }

        private void DoProc(int pnum, string code)
        {
            double mins = 0;
            string found_what;
            DateTime evdt;
            DateTime enddt = DateTime.MinValue;

            if (GetResultAndEVDT("", code, "", "", out found_what, out evdt))
            {
                mins = 60.0 * found_what.ToDouble();
                enddt = evdt.AddMinutes(mins);

                if (ProcExistsInDB(pnum, evdt, enddt))
                {
                    Program.Audit("Procedure " + pnum+ ": already exists");
                }
                else
                {
                    if (!QueuedProcOverlaps(pnum, evdt, enddt))
                    {
                        var proc = new proc_data();
                        proc.procedure_number = pnum;
                        proc.start = evdt;
                        proc.finish = enddt;
                        _procs.Add(proc);
                        Program.Audit("Procedure " + pnum + ": Found " + code + " between " + evdt + " and " + enddt);
                    }
                }

            }

        }

        private bool OnlyHasED()
        {
            var db = PFSDBUtility.NewPfsDataContext();
            var query = from el in db.ENCOUNTER_LOCATIONs
                        join u in db.UNITs on el.UNIT_ID equals u.UNIT_ID
                        where (el.ENCOUNTER_ID == _pat.encounter_id)
                        where (u.IS_ED.ToString().ToUpper() == "N")
                        select new { u.NAME };
            return (query.Count() == 0);
        }

        private bool ProcExistsInDB(int pnum, DateTime startdt, DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && (proc.PROCEDURE_DATETIME == startdt)
                            && (proc.DEPARTURE_DATETIME == enddt)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new { proc.PROCEDURE_EVENT_ID };
            return (query.Count() > 0);
        }

        private bool QueuedProcOverlaps(int pnum, DateTime startdt, DateTime enddt)
        {
            bool overlap = false;

            proc_data[] pary = _procs.ToArray();
            for (int i = 0; i <= pary.GetUpperBound(0); i++)
            {
                if (pary[i].procedure_number == pnum)
                {
                    //overlap if   p.start between stardt and enddt
                    //             p.finish between startdt and enddt
                    //             startdt >= p.start and enddt <= p.finish
                    if ((pary[i].start >= startdt) && (pary[i].start < enddt) ||
                        (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
                        (startdt >= pary[i].start) && (enddt <= pary[i].finish))
                    {
                        overlap = true;
                    }

                    if ((pary[i].start >= startdt) && (pary[i].start < enddt) && (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
                        (pary[i].start < startdt) && (pary[i].finish > startdt) && (pary[i].finish < enddt)
                        )
                    {
                        // the proc in the list is completely contained in startdt/endt pair
                        // change this proc to be the startdt/enddt
                        // overlap is still true, but the list item will be updated with the encompassing times
                        //Program.Audit("Procedure " + pnum + ": " + startdt + " - " + enddt + " will trump " + pary[i].start + " - " + pary[i].finish);
                        pary[i].start = startdt;
                        pary[i].finish = enddt;
                    }
                }
            }
            _procs = pary.ToList();
            return overlap;
        }


        private void CheckProc_1()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P1. 1-1 safety observation by RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_2()
        {
            string nowstr;
            string toddtstr;
            string yesdtstr;
            string timea ="";
            DateTime timea_startdt, timea_enddt;
            DateTime timeb_startdt, timeb_enddt;
            DateTime nowdt = _pat.pull_finish;              // "now" is pull time
            DateTime yesdt;
            
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P2. 1-1 safety observation by non-RN");
            Program.VerboseAudit("---------------");

            nowstr = nowdt.ToString("yyyyMMddHHmm");
            yesdt = nowdt.AddDays(-1);
            toddtstr = nowdt.ToString("yyyyMMdd");
            yesdtstr = yesdt.ToString("yyyyMMdd");
            
            //when is now? (yesterday/today)
            //yes 7am -- yes 7p  -- tod 7a -- tod 7p
            //                                   A                   B
            //if nowdt >= tod7pm then check  tod 7am-tod 7pm and tod 7pm-tom 7a
            //if nowdt >= tod7am then check  yes 7p - tod 7a     tod 7a-tod 7p
            //if nowdt >= yes7pm then check  yes 7a-yes 7p       yes 7p-tod 7a
            if (nowstr.CompareTo(toddtstr + "1900") >= 0) {
                timea = toddtstr + "0700";
            } else if (nowstr.CompareTo(toddtstr + "0700") >= 0) {
                timea = yesdtstr + "1900";
            } else if (nowstr.CompareTo(yesdtstr + "1900") >= 0) {
                timea = yesdtstr + "0700";
            }
            
            timea_startdt = PFSUtility.ISOToDateTime(timea);
            timea_enddt = timea_startdt.AddHours(12);
            MaybeAddSitter(timea_startdt, timea_enddt);

            timeb_startdt = timea_enddt;
            timeb_enddt = timeb_startdt.AddHours(12);
            MaybeAddSitter(timeb_startdt, timeb_enddt);
        }

        private void MaybeAddSitter(DateTime startdt, DateTime enddt)
        {
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);    // add custom time range below
            query = query.Where(e => (e.EVENT_DATETIME >= startdt) && (e.EVENT_DATETIME < enddt));
            query = AndCodeInList(query, "Sitter");
            query = AndResultInList(query,"continued, initiated");
            query = AndResultNotInList(query, "discontinued");

            if (query.Count() > 0) {
                if (ProcExists(2, startdt, enddt)) {
                    Program.Audit("Procedure 2: already exists");
                } else {
                    var proc = new proc_data();
                    proc.procedure_number = 2;
                    proc.start = startdt;
                    proc.finish = enddt;
                    _procs.Add(proc);
                    Program.Audit("Procedure 2: Found Sitter between " + startdt + " and " + enddt);
                }
            }
            
        }

        private bool ProcExists(int pnum, DateTime startdt, DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && (proc.PROCEDURE_DATETIME == startdt)
                            && (proc.DEPARTURE_DATETIME == enddt)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new {proc.PROCEDURE_EVENT_ID};
            return (query.Count() > 0);
        }


        private void CheckProc_3()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P3. Off unit accompanied by RN");
            Program.VerboseAudit("---------------");
            DoProc(3, "A_MHAcuOffUnit");
        }

        private void CheckProc_4()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P4. Off unit accompanied by non-RN");
            Program.VerboseAudit("---------------");
            DoProc(4, "A_MHAcuOffUNonRN");
        }

        private void CheckProc_5()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P5. Patient/family education by RN");
            Program.VerboseAudit("---------------");
            DoProc(5, "A_MHAcuPtFamEduc");
        }

        private void CheckProc_6()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P6. Extensive wound management by RN");
            Program.VerboseAudit("---------------");
            DoProc(6, "A_MHAcuExtensive");
        }

        private void CheckProc_7()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P7. Extensive wound management by non-RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_8()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P8. Coordination of care by RN");
            Program.VerboseAudit("---------------");
            DoProc(8, "A_MHAcuCoordinat");
        }

        private void CheckProc_9()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P9 1:1 RN at bedside");
            Program.VerboseAudit("---------------");
            DoProc(9, "A_MHAcu1:1byURN");
        }

        private void CheckProc_10()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P10. 1:1 non-RN at bedside");
            Program.VerboseAudit("---------------");
            DoProc(10, "A_MhAcu1:1UNonRN");
        }

        private void CheckProc_11()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P11. 2:1 by RN at bedside");
            Program.VerboseAudit("---------------");
            DoProc(11, "A_MHAcu2:1by URN");
        }

        private void CheckOutcomes()
        {
            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = AndItemFilter(query, "", "A_MHPostFalltime", "", "", "");
            foreach (var ch in query)
            {
                var outc = new proc_data();
                outc.procedure_number = 1;
                outc.start = ch.EVENT_DATETIME;
                _outcomes.Add(outc);
                Program.Audit("Outcomes 1: Found A_MHPostFalltime at " + outc.start);
            }
        }

        private string GetEDChildName()
        {
            EDchild edchildrec = new EDchild();
            edchildrec = Program.EDchildren.Find(e => e.parent_unitid == _pat.unit_id && e.meth_id == _pat.meth_id);
            return edchildrec.child_name;
        }

        private const string DATETIME_FORMAT = "yyyyMMddHHmm";              // ISO Date/Time w/o seconds

        private void OutputClass()
        {
            string outstr, ind_list, desc, str_pull_dt;
            int i, tc_event_id;

            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         212 242 304
            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1
            //1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901 3456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
            //1       |10 5            |                |                |        |10023121779         |ALEXANDER                       |GLORIA                          |                                |0508    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289530  |           |201403030400                                                                      |NNYNNNNYYNNYNNNYNNNYYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//1       |10 5            |                |                |        |10024544908         |ARMSTRONG                       |AARON                           |                                |0535    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289531  |           |201403030400                                                                      |NYNNYNNNNNNYNNNYNNNNYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//|0547    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289532  |           |201403030400                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//|0547    |A   |201403030700|                |201403030700    |18  |C|    |3040|1440|18289532  |           |201403030700                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN

            if (Program.g_is_test)
                tc_event_id = 9999;
            else
                tc_event_id = PFSDBUtility.NextGID();                         //get a unique id for this class
//            str_pull_dt = _pat.pull_finish.ToString(DATETIME_FORMAT);
            str_pull_dt = _pat.unit_arrival.ToString(DATETIME_FORMAT);
            string unitname = GetEDChildName();

            outstr = _pat.facilty_code.FixedWidth(8);                       //(facility code)
            outstr += "|" + unitname.FixedWidth(16); // _pat.unit_name.FixedWidth(16);
            outstr += "|" + "".FixedWidth(16);                               //(unit code)
            outstr += "|" + "".FixedWidth(16);                               //(area code)
            outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
            outstr += "|" + _pat.acct.FixedWidth(20);
            outstr += "|" + _pat.last_name.FixedWidth(32);
            outstr += "|" + _pat.first_name.FixedWidth(32);
            outstr += "|" + _pat.middle_name.FixedWidth(32);
            outstr += "|" + _pat.room.FixedWidth(8);
            outstr += "|" + _pat.bed.FixedWidth(4);
          outstr += "|" + str_pull_dt.FixedWidth(12);                      //class datetime (could change)
            outstr += "|" + "".FixedWidth(16);                               //(login)
          outstr += "|" + str_pull_dt.FixedWidth(16);                      //(employee)/(pull datetime)
            outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
            outstr += "|" + "C".FixedWidth(1);                               //record type = class
            outstr += "|" + "".FixedWidth(4);                                //(stage)
            outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
            outstr += "|" + _pat.range.ToString().FixedWidth(4);             //TC pull range
            outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
            outstr += "|";
            outstr = outstr.FixedWidth(294);
          outstr += "|" + _pat.unit_arrival.ToString(DATETIME_FORMAT);        //IN
            outstr = outstr.FixedWidth(377);
            outstr += "|";
            
            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++) {
                if (_inds[i].is_checked) {
                    outstr += "Y";
                    ind_list += "," + i;
                } else {
                    outstr += "N";
                }
            } // next i
            ind_list = ind_list.Substring(1);                           //strip leading comma
  
            Program.outfile.WriteLine(outstr);                          //output to transparent.txt

            Program.Audit("");
            desc = "Classified: " + ind_list;
            if (Program.g_is_test) {
                Program.Audit(desc);
            } else {
                //Save the selected indicators plus both Program.Audits in the event log; link with tc_event_id
                PFSEventLog.AddTransparentMappingEventLogEntry(
                    desc, Program.gLogUnitID, Program.gLogEncounterID,
                    tc_event_id, Program.gLogMapperVersion,
                    Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
            }
        }

        //private void OutputProcs()
        //{
        //    int i;
        //    string outstr, proc_list, desc;
        //    int tc_event_id;

        //    foreach(var proc in _procs) {
        //        if (Program.g_is_test)
        //            tc_event_id = 9999;
        //        else
        //            tc_event_id = PFSDBUtility.NextGID();                         //get a unique id for this proc

        //        outstr = _pat.facilty_code.FixedWidth(8);
        //        outstr += "|" + _pat.unit_name;                                 //10
        //        outstr = outstr.FixedWidth(68);
        //        outstr += "|" + _pat.acct.FixedWidth(20);                       //90
        //        outstr += "|" + _pat.last_name.FixedWidth(32);
        //        outstr += "|" + _pat.first_name.FixedWidth(32);
        //        outstr += "|" + _pat.middle_name.FixedWidth(32);
        //        outstr = outstr.FixedWidth(202);
        //        outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //204 proc dt
        //        outstr = outstr.FixedWidth(254);
        //        outstr += "|P";                                                 //256 procedure type record
        //        outstr += "|" + "".FixedWidth(4);                               //(stage)
        //        outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);     //TC source ID
        //        outstr += "|" + _pat.range.ToString().FixedWidth(4);            //TC pull range
        //        outstr += "|" + tc_event_id.ToString().FixedWidth(10);          //TC event ID
        //        outstr += "|";
        //        outstr = outstr.FixedWidth(294);
        //        outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //296 procdt in
        //        outstr = outstr.FixedWidth(346);
        //        outstr += "|" + proc.finish.ToString(DATETIME_FORMAT);          //348 procdt out
        //        outstr = outstr.FixedWidth(377);
        //        outstr += "|";
                
        //        proc_list = "";
        //        for (i = 1; (i < MAX_PROCS); i++) {
        //            if (proc.procedure_number == i) {
        //                outstr += "Y";
        //                proc_list += "," + i;
        //            } else {
        //                outstr += "N";
        //            }
        //        } // next i
        //        proc_list = proc_list.Substring(1);                             //strip leading comma

        //        Program.outfile.WriteLine(outstr);                              //output to transparent.txt

        //        desc = "Procedures: " + proc_list;
        //        if (Program.g_is_test) {
        //            Program.Audit(desc);
        //        } else {
        //            //Save the selected procedures plus both Program.Audits in the event log; link with tc_event_id
        //            //(this assumes the procedure Program.Audit is mixed in with the class Program.Audit)
        //            PFSEventLog.AddTransparentMappingEventLogEntry(desc, Program.gLogUnitID, Program.gLogEncounterID,
        //                tc_event_id, Program.gLogMapperVersion,
        //                Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
        //        }
        //    } // next proc
        //}

    }
}
