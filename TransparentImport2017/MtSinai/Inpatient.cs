﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;                     // for LINQ; add a reference
using System.Data.SqlClient;                // for SqlConnection
using System.IO;                            // for Directory, Path, Stream
using PfsShared;                            // add a reference to Shared2 project

// ============================================================================
// Inpatient transparent mapping -- GOES HERE --
// Mt Sinai Allscripts
// ============================================================================
// This processes one patient.  Only one database query is used.  :)
//
// All search functions use exact match for category, description and field name.
// Codes and results can be a single word or a list of words.
// Codes default to exact match; prefix with CODE_CODE_LIKE_PREFIX if desired (single word only)
// Results default to like matching; prefix with RESULT_EXACT_MATCH_PREFIX if desired (single word only)
//
// All searches are case insensitive.
//
//
namespace TransparentMapping
{
    class Inpatient
    {
        private const int MAX_INDS = 120;
        private const int MAX_PROCS = 20;

        private const string CODE_LIKE_PREFIX = "%!";       // use to make codes %like%
        private const string EXACT_MATCH_PREFIX = "&!";     // use to make desc/result exact match
        private const string CHAR_COMMA = "||";             // use to insert literal comma in word list
        private const string NOT_PREFIX = "!!";             // use to insert literal comma in word list
        private const string AVOID_NEGATIVE = "!;";
        private const string EXACT_SQL_PREFIX = "!SQL!";     // use to give sql constraint as given
        private const string STARTS_WITH = "[!";     // use to give sql constraint as given

        private struct indicator_data {
            public bool is_checked;
            public int radio_group;
            public int weight;
        }

        private struct proc_data {
            public int procedure_number;
            public DateTime start;
            public DateTime finish;
        }

        // These are database CHART_ITEMs for this patient
        private CHART_ITEM[] _chart_items_since_admission;
        //private CHART_ITEM[] _chart_items_since_unit_arrival;
        //private CHART_ITEM[] _chart_items_during_pull_period;
        //private CHART_ITEM[] _chart_items_pull_period_plus;
        //private CHART_ITEM[] _chart_items_since24hrs;
        //private CHART_ITEM[] _chart_items_since13hrs;
        private CHART_ITEM[] _chart_items_since24hrs;
        //private CHART_ITEM[] _chart_items_since9hrs;
        // These are database PROCEDURE_EVENTs for this patient
        private PROCEDURE_EVENT[] _procedure_events;

        private indicator_data[] _inds;
        private List<proc_data> _procs;
        private List<proc_data> _outcomes;

        private const int DEFAULT_BUCKET_SIZE = 60;       //min

        private PatientInfo _pat;
        private int _bucket_size = DEFAULT_BUCKET_SIZE;
        private bool is_default;
        private string txarea;
        private bool periop_found_inpast13hrs = false;

        private bool exclude_periop_data = false;
        private bool exclude_periph_iv = false;
        private bool coma = false;
        private bool g_toi4 = false;
        private bool g_gitube_attempted = false;
        private bool g_gitube = false;
        private DateTime loc_in;
        private DateTime loc_out;
        private DateTime loc_arrtime;
        int rassct12 = 0;
        int rassct34 = 0;
        int rassctLTzero = 0;
        private LOAtypePrecision[] aryloa = new LOAtypePrecision[10];
        private int numloa=0;
        private LOAtypePrecision[] ary_hemodial = new LOAtypePrecision[5];
        private int numhemodial = 0;
        private LOAtypePrecision[] aryclassdnc = new LOAtypePrecision[10];
        private int numclassdnc = 0;
        private bool g_lda6yo = false;
        private bool g_iabp = false;

        private string assessgrouplabel = "";

        private enum SearchDepth {
            SearchDefault,
            SearchPullRange,//search within the current pull     -- default
            SearchSinceArrival,         //search since arrival to the unit
            SearchSinceAdmission,        //search everything since admission to the hospital
            SearchPullPlus,
            SearchSince24Hrs,
            SearchSince13Hrs,
            SearchSince12Hrs,
            SearchSince16Hrs,
            SearchSince9Hrs,
            SearchSince4Hrs
        }

        private enum CountMode {
            CountAll,
            CountFirst                  //stop after one is found
        }

        private enum GetValueMode {
            GetTotal,
            GetMax,
            GetLast
        }

        enum Frequencies {
            QNONE,
            Q4H,
            Q2H,
            Q1H,
            Q30M
        }

        private struct fmapRow {
            public double los_high;                       //the LOS being testing
            public int[] freq;                           //the count required for each Q value
        }
        private List<fmapRow> _freq_map;                    //1,2,4,8,12,24 hours

        private struct gBucket
        {
            public int bucket;
            public string code;
            public DateTime evdt;
            public bool using_waiver;
            public bool has_all_deps; //has all dependents
            public int num_addl_items;
            public string description;
        }
        private struct gGap
        {
            public int gap;
            public DateTime evdt1;
            public DateTime evdt2;
        }

        private struct MedChartItem
        {
            public string code;
            public string orderid;
            public DateTime evdt;
            public bool valid;
        }
        //private string[] meds_rate = { "newbag", "new bag", "rateverify", "rate verify", "restarted", "ratechange", "rate change", "started", "continued" };
        //private struct med2026and5077
        //{
        //    public string name;
        //    public bool found;
        //}
        //
        // This is the main entry point
        //
        public void ProcessPatient(PatientInfo pat)
        {
            _pat = pat;
            bool use_default = false;
            bool no_chart_items_in_24hrs = false;
            bool loa_exists = false;
            bool do_class = true;

            InitIndicators(); // sets is_default
            InitProcs();
            if (!is_default)
            {
                LoadFreqTable();
                //CheckIfPeriopInPast13hrs();
                no_chart_items_in_24hrs = (LoadPatientChart() == 0);
                Program.VerboseAudit("New query default scope = " + loc_in + " to " + loc_out);
                if (no_chart_items_in_24hrs)
                    Program.Audit("No chart items received in past 24 hrs.");
                if (!Program.g_onlydnc && do_class)
                {
                    Check_1_2_3_4();
                    Check_5();
                    Check_6_7();
                    Check_8();
                    Check_9();
                    Check_10_11();
                    Check_12_13();
                    Check_14();
                    Check_15_16_17_18();
                    Check_19();
                    Check_20();
                    Check_21_22();
                    Check_23();
                    Check_24();
                    CheckUserDefined();
                    AtLeastOneADL();
                }
            }

            //if (!no_chart_items_in_24hrs)
            {
                if (!Program.g_onlydnc && do_class)
                {
                    HighestIndicatorInEachGroupWins();

                    if (!is_default)
                    {
                        _pat.ptype = DeterminePtypeOfIndicators();
                        //Program.sttimer();
                        if (!Program.g_noactivities) CheckProcs();
                        //Program._t4 = Program.entimer("t4=", Program._t4);
                        //CheckOutcomes();
                        //if (Program.g_do_OW) CheckOtherWorkload();
                    }

                    if (Program.g_no_output) return;
                    //if (_pat.default_ptype > DeterminePtypeOfIndicators())
                    //{ // if the default pt type is higher than the pt type of this class
                    //  // then if there is a default classification in the past 16 hrs
                    //  // then make these indicators the default indicators.
                    //    use_default = ExistDefaultInPast16hrs();
                    //    if (use_default)
                    //    {
                    //        Program.VerboseAudit("Default indicators will be used");
                    //    }
                    //}
                    OutputClassAndDNCTimes();
                    OutputClassAndDNC(do_class);
                    if (!Program.g_noactivities) OutputProcs();
                }
                else
                {
                    //if (!Program.g_noactivities) OutputDNC();
                }
                //OutputOutcomes();
            }
        }

        private void CheckIfPeriopInPast13hrs()
        {
            foreach (var perioploc in Program.patperioplist)
            {
                periop_found_inpast13hrs = periop_found_inpast13hrs
                    || (perioploc.in_time >= _pat.pull_finish.AddHours(-13))
                    || (perioploc.out_time >= _pat.pull_finish.AddHours(-13));
            }
        }

        private void InitIndicators()
        {
            // Make a new (empty) indicator array
            _inds = new indicator_data[MAX_INDS + 1];           // This 1 based so add one
            is_default = false;
            txarea = "";
            //if (_pat.los_hours <= 4.0)
            //{
            //    is_default = true;
            //    Program.VerboseAudit("Patient was here 4 hrs or less. Will receive default indicators " + _pat.default_inds_str);
            //    foreach (var ind in _pat.default_inds)
            //    {
            //        if (ind <= _inds.GetUpperBound(0))
            //        {
            //            _inds[ind].is_checked = true;
            //        }
            //    }
            //}


            // get indicator radio groups from the database
            // ** (This database access can be replaced once we have a C# methodolgy cache)
            var db = PFSDBUtility.NewPfsDataContext();
            var query = from idef in db.INDICATOR_DEFINITIONs
                        where (idef.METHODOLOGY_ID == _pat.meth_id)
                        select idef;
            foreach (var idef in query) {
                if (idef.INDICATOR_NUMBER <= _inds.GetUpperBound(0)) {
                    // (convert nulls to zero)
                    _inds[idef.INDICATOR_NUMBER].radio_group = PFSDBUtility.DBToInt(idef.RADIO_GROUP);
                    _inds[idef.INDICATOR_NUMBER].weight = PFSDBUtility.DBToInt(idef.WEIGHT);
                }
            }
        }

        private void InitProcs()
        {
            _procs = new List<proc_data>();
            _outcomes = new List<proc_data>();
        }

        private fmapRow LoadFreqTableRow(double los_high, string values)
        {
            fmapRow fmrow;

            fmrow.los_high = los_high;
            fmrow.freq = new int[Enum.GetNames(typeof(Frequencies)).Length];

            var arr = values.Split(',');
            for (int i = 0; i <= fmrow.freq.GetUpperBound(0); i++)
            {
                fmrow.freq[i] = arr[i].ToInteger();
            }
            return fmrow;
        }

        // These are the number of times you need to do something during a certain time period (LOS)
        // in order for it to be considered one of the frequencies listed.
        private void LoadFreqTable()
        {
            _freq_map = new List<fmapRow>();
            //                              LOS,  None Q4h Q2h Q1h Q30m
            _freq_map.Add(LoadFreqTableRow(1, "    0,  0,  0,  1,  2"));
            _freq_map.Add(LoadFreqTableRow(2, "    0,  0,  1,  2,  4"));
            _freq_map.Add(LoadFreqTableRow(4, "    0,  1,  2,  4,  8"));
            _freq_map.Add(LoadFreqTableRow(6, "    0,  2,  3,  6,  9"));
            _freq_map.Add(LoadFreqTableRow(8, "    0,  2,  4,  8, 12"));
            _freq_map.Add(LoadFreqTableRow(12, "   0,  3,  6, 12, 18"));
            _freq_map.Add(LoadFreqTableRow(16, "   0,  4,  8, 16, 24"));
            _freq_map.Add(LoadFreqTableRow(24, "   0,  6, 12, 24, 36"));
            //New freq table 2/5/14
            //q4	q2	q1	q30     q30
            //            Non-ICU	ICU & SD
            // 4	8	15	29	    36
            // 3	5	9	17	    24
            // 2	4	7	13	    19
            // 2	3	5	10	    13

            //to read this table: if you have at least 6 asessmenets in a 12-hour period, you get Q1h
            //(LOS=12, column Q1h has a count of 6)
        }

        private int LoadPatientChart()
        {
            foreach (var p in Program.patloclist)
            {
                if (p.loc_idx == _pat.loc_idx)
                {
                    loc_in = p.in_time;
                    loc_out = p.out_time;
                    loc_arrtime = p.arr_time;
                }
            }
            _pat.los_hours = PFSUtility.DateDiffInMinutes(loc_in, loc_out) / 60.0;
            Program.VerboseAudit("LoadChart los=" + _pat.los_hours);
            //Program.VerboseAudit("LoadChart unit=" + p.unit_name + " locidx=" + p.loc_idx + " in=" + p.in_time + " out=" + p.out_time);

            int ct_in_24hrs = 0;
            int ctperiop = 0;
            // Get the entire patient chart (all units and dates for this patient)
            var dba = PFSDBUtility.NewPfsDataContext();
            var queryall = from item in dba.CHART_ITEMs
                           where (item.ENCOUNTER_ID == _pat.encounter_id)
                           where (item.EVENT_DATETIME <= loc_out)
                         orderby item.EVENT_DATETIME
                         select item;
            var querya = from g in queryall
                         select g;
            // Save the result
            _chart_items_since_admission = querya.ToArray();
            Program.VerboseAudit("Count since adm=" + querya.Count());
            

            var query = from item in _chart_items_since_admission
                        where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-24) 
                               && (item.EVENT_DATETIME <= _pat.pull_finish))
                        select item;
            // Save the result
            ct_in_24hrs = query.Count();
            _chart_items_since24hrs = query.ToArray();

            // Convert all results to lower case for case insensitive comparisons
            foreach (var item in _chart_items_since24hrs) {
                item.SOURCE_TEXT = null;
                if (item.CATEGORY != null) item.CATEGORY = item.CATEGORY.ToLower();
                if (item.CODE != null) item.CODE = item.CODE.ToLower();
                if (item.DESCRIPTION != null) item.DESCRIPTION = item.DESCRIPTION.ToLower();
                if (item.FIELD_NAME != null) item.FIELD_NAME = item.FIELD_NAME.ToLower();
                if (item.RESULT != null) item.RESULT = item.RESULT.ToLower();
            }

            Program.VerboseAudit("Since 24 hrs count items=" + ct_in_24hrs);
            Program.VerboseAudit("Since 24 hrs count of HD items=" + ctperiop);

            return ct_in_24hrs;
        }

        // Delay looking for patient procedures until they are wanted
        private void LoadPatientProceduresIfNeeded()
        {
            if (_procedure_events != null) return;

            var db = PFSDBUtility.NewPfsDataContext();
            var query = from proc in db.PROCEDURE_EVENTs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                        where (proc.PROCEDURE_DATETIME >= _pat.pull_finish.AddHours(-24))
                        select proc;
            _procedure_events = query.ToArray();
        }



        // Started a new chart item query (of a certain depth)
        // Default is the chart during the pull range.  Options for unit arrival and admission to hospital.
        private IEnumerable<CHART_ITEM> StartNewQuery()
        {
            return StartNewQuery(SearchDepth.SearchDefault);
        }
        private IEnumerable<CHART_ITEM> StartNewQuery(SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;

            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    if (exclude_periop_data)
                        //return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_start && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= loc_in && (item.EVENT_DATETIME <= loc_out)) select item);
                        //return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-12)) select item);
                    else
                        //return (from item in _chart_items_since24hrs where (item.EVENT_DATETIME >= _pat.pull_start && item.EVENT_DATETIME <= _pat.pull_finish) select item);
                        return (from item in _chart_items_since24hrs where (item.EVENT_DATETIME >= loc_in && item.EVENT_DATETIME <= loc_out) select item);
                        //return (from item in _chart_items_since24hrs where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-12)) select item);
                //if (exclude_periop_data)
                //    return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-8) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                //else
                //    return (from item in _chart_items_since24hrs where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-8) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                //    return (from item in _chart_items_since_unit_arrival select item);
                case SearchDepth.SearchSinceAdmission:
                    return (from item in _chart_items_since_admission select item);
                //case SearchDepth.SearchPullPlus:
                //    return (from item in _chart_items_pull_period_plus select item);
                case SearchDepth.SearchSince24Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                    else
                        return (from item in _chart_items_since24hrs where (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince16Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-16) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since24hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-16) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince13Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-13) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since24hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-13) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince12Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-12) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since24hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-12) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince9Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-9) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since24hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-9) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince4Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME > _pat.pull_finish.AddHours(-4) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since24hrs where item.EVENT_DATETIME > _pat.pull_finish.AddHours(-4) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
            }
            return null;
        }

        // Is this search term a word or a list of words?
        private bool ValueIsAList(string s)
        {
            return (s != null) && s.Contains(',');                //is this a comma-separated list?
        }

        // Break apart a wordlist and prepare its members
        private string[] SplitOnCommaAndPrepareElements(string s)
        {
            // If a word list has a space after each comma, we will get leading blanks in each term
            var arr = s.Split(',');
            for (int i = 0; i <= arr.GetUpperBound(0); i++) {
                arr[i] = arr[i].Trim();                         // get rid of leading blanks
                arr[i] = arr[i].Replace(CHAR_COMMA, "");       // insert a literal comma into search word
                arr[i] = arr[i].ToLower();                      // convert for case-insensitive comparisons
            }
            return arr;
        }

        // Add a basic chart item filter to a LINQ query
        //
        // cat = exact match
        // code_list = exact match; optional "like" match
        // desc_list = "like" match; optional exact match
        // field = exact match
        // result_list = "like" match; optional exact match
        //
        // These are comma-separated lists.  
        // Search for a literal comma by using CHAR_COMMA in the string.
        private IEnumerable<CHART_ITEM> AndItemFilter(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list)
        {
            // Converted all values to lower case for case-insensitive comparisons.
            if (!String.IsNullOrEmpty(cat))
            {
                cat = cat.ToLower();
                query = query.Where(e => e.CATEGORY.ToLower() == cat);
            }

            if (!String.IsNullOrEmpty(code_list))
            {
                code_list = code_list.ToLower();
                if (code_list.Left(2) == CODE_LIKE_PREFIX)
                {            // override for "like" match?
                    query = query.Where(e => e.CODE.Contains(code_list.Substring(2)));
                }
                else if (code_list.Left(2) == EXACT_MATCH_PREFIX)
                {
                    query = query.Where(e => e.CODE == code_list.Substring(2));
                }
                else if (ValueIsAList(code_list))
                {
                    query = AndCodeInList(query, code_list);            // find one of the words
                }
                else
                {
                    query = query.Where(e => e.CODE.StartsWith(code_list));      // find this word
                }
            }

            if (!String.IsNullOrEmpty(desc_list))
            {
                desc_list = desc_list.ToLower();
                if (desc_list.Left(2) == EXACT_MATCH_PREFIX)
                {          // exact match override?
                    query = query.Where(e => e.DESCRIPTION == desc_list.Substring(2));
                }
                else if (desc_list.Left(2) == NOT_PREFIX)
                {
                    // query = query.Where(e => !e.DESCRIPTION.Contains(desc_list.Substring(2)));
                    query = AndDescriptionNOTInList(query, desc_list.Substring(2));
                }
                else if (ValueIsAList(desc_list))
                {
                    query = AndDescriptionInList(query, desc_list);
                }
                else
                {
                    desc_list = desc_list.Replace(CHAR_COMMA, "");     // look for literal comma requests
                    query = query.Where(e => e.DESCRIPTION.Contains(desc_list));
                }
            }

            if (!String.IsNullOrEmpty(field))
            {
                field = field.ToLower();
                query = query.Where(e => e.FIELD_NAME == field);
            }

            if (!String.IsNullOrEmpty(result_list))
            {
                query = query.Where(e => e.RESULT != null);
                result_list = result_list.ToLower();                        // make all results case insensitve
                if (result_list.Left(2) == EXACT_MATCH_PREFIX)
                {     // exact match override?
                    query = query.Where(e => e.RESULT == result_list.Substring(2));
                }
                else if (result_list.Left(2) == AVOID_NEGATIVE)
                {
                    query = query.Where(e => (!e.RESULT.Contains("no " + result_list.Substring(2))) && ((e.RESULT == result_list.Substring(2)) || (e.RESULT.Contains(";" + result_list.Substring(2)))));
                }
                else if (ValueIsAList(result_list))
                {
                    query = AndResultInList(query, result_list);
                }
                else if (result_list.Left(2) == STARTS_WITH)
                {
                    query = query.Where(e => e.RESULT.StartsWith(result_list.Substring(2)));
                }
                else
                {
                    result_list = result_list.Replace(CHAR_COMMA, "");     // look for literal comma requests
                    query = query.Where(e => e.RESULT.Replace(",", "").Contains(result_list));
                }
            }
            return query;
        }

        // Look for a code that is one of the words in the list.
        private IEnumerable<CHART_ITEM> AndCodeInList(IEnumerable<CHART_ITEM> query, string code_list)
        {
            if (String.IsNullOrEmpty(code_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(code_list);
            //            return query.Where(e => arr.Any(x => x.Contains(e.CODE))); // "like" match
            return query.Where(e => e.CODE.ContainsAny(arr)); // "like" match
        }

        // Look for a description that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndDescriptionInList(IEnumerable<CHART_ITEM> query, string desc_list)
        {
            if (String.IsNullOrEmpty(desc_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(desc_list);
            //return query.Where(e => arr.Any(x => x.Contains(e.DESCRIPTION))); // "like" match
            query=query.Where(e => e.DESCRIPTION.ContainsAny(arr)); // "like" match
            //Program.VerboseAudit("AndDescriptionInList ct=" + query.Count() + " desclist="+desc_list);
            return query;
            //return query.Where(e => e.DESCRIPTION.ContainsAny(arr)); // "like" match
        }
        private IEnumerable<CHART_ITEM> AndDescriptionNOTInList(IEnumerable<CHART_ITEM> query, string desc_list)
        {
            if (String.IsNullOrEmpty(desc_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(desc_list);
            //return query.Where(e => arr.Any(x => x.Contains(e.DESCRIPTION))); // "like" match
            return query.Where(e => !e.DESCRIPTION.ContainsAny(arr)); // "like" match
        }

        // Look for a result that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultInList(IEnumerable<CHART_ITEM> query, string result_list)
        {
            bool use_start_with = false;

            if (String.IsNullOrEmpty(result_list)) return query;

            if (result_list.Left(2) == STARTS_WITH)
            {
                use_start_with = true;
                result_list = result_list.Substring(2);
            }

            var arr = SplitOnCommaAndPrepareElements(result_list);
            //            return query.Where(e => arr.Any(x => x.Contains(e.RESULT))); // "like" match
            if (use_start_with)
                return query.Where(e => arr.Any(item => e.RESULT.StartsWith(item)));
            else
                return query.Where(e => e.RESULT.ContainsAny(arr)); // "like" match
                                                                    //query = query.Where(e => (meds_mr2026.Any(item => e.DESCRIPTION.ToUpper().StartsWith(item))
                                                                    //return query.Where(e => arr.Any(item => e.RESULT.ToLower().Contains(item.ToLower())));
        }

        // Look for a result that does not contain one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultNotInList(IEnumerable<CHART_ITEM> query, string result_list)
        {
            if (String.IsNullOrEmpty(result_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(result_list);
            //            return query.Where(e => !arr.Any(x => x.Contains(e.RESULT))); // "like" match
            return query.Where(e => !e.RESULT.ContainsAny(arr)); // "like" match
        }

        private string DescribeSearchDepth(SearchDepth search_depth)
        {
            string result = "";
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;

            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    //result = "in pull range"
                    result = "";                         //be quiet since this is default
                    break;
                case SearchDepth.SearchSinceArrival:
                    result = "since arrival to unit";
                    break;
                case SearchDepth.SearchSinceAdmission:
                    result = "since admission";
                    break;
                case SearchDepth.SearchPullPlus:
                    result = "in LOS range plus 4 hours after";
                    break;
                case SearchDepth.SearchSince24Hrs:
                    result = "since 24 hours ago";
                    break;
                case SearchDepth.SearchSince16Hrs:
                    result = "since 16 hours ago";
                    break;
                case SearchDepth.SearchSince13Hrs:
                    result = "since 13 hours ago";
                    break;
                case SearchDepth.SearchSince9Hrs:
                    result = "since 9 hours ago";
                    break;
                case SearchDepth.SearchSince4Hrs:
                    result = "since 4 hours ago";
                    break;
            }

            return result;
        }

        // Describe a query that has already been run
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Make a simple query and describe the results
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            var arr = query.ToArray();
            return Describe(arr, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private string LookingFor(string so_far, string what, string op, string value)
        {
            string result;
            if (String.IsNullOrEmpty(value)) return so_far;

            if (so_far == "")
                result = "look for " + what;
            else
                result = so_far + " and " + what;

            if (value.Left(2) == EXACT_MATCH_PREFIX)
                result += "=" + value.Substring(2) + "";
            else if (value.Left(2) == CODE_LIKE_PREFIX)
                result += " contains " + "" + value.Substring(2) + "";
            else if (ValueIsAList(value))
                result += " in " + value + "";
            else
                result += op + "" + value + "";

            return result;
        }
        private string Describe(CHART_ITEM[] arr, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string result = "";
            //result = LookingFor(result, "cat", "=", cat);
            result = LookingFor(result, "code", "=", code_list);
            //result = LookingFor(result, "desc", " contains ", desc_list.Left(20));
            //result = LookingFor(result, "field", "=", field);
            //result = LookingFor(result, "result", " contains ", result_list.Left(20));

            if (arr.Count() == 0) {
                result += "; not found " + DescribeSearchDepth(search_depth);
            } else {
                var e = arr[0];
                result = "FOUND: " + result;
                // We might have searched for a pattern or word list in several fields - show what was found
                //if (e.CATEGORY != null && e.CATEGORY != "") result += " cat=" + e.CATEGORY + "";
                if (e.CODE != null && e.CODE != "") result += " code=" + e.CODE + "";
                if (e.DESCRIPTION != null && e.DESCRIPTION != "") result += " desc=" + e.DESCRIPTION + "";
                //if (e.FIELD_NAME != null && e.FIELD_NAME != "") result += " field=" + e.FIELD_NAME + "";
                if (e.RESULT != null && e.RESULT != "") result += " result=" + e.RESULT + "";
                // Are there more results?  Just say how many; we aren't going to list them.
                if (arr.Count() == 2) {
                    result += " (1 more result)";
                } else if (arr.Count() > 2) {
                    result += " (" + (arr.Count() - 1) + " more results)";
                }
            }

            return result;
        }


        //Set an indicator for this reason (low level)
        private void SetInd(int inum, string reason)
        {
            if (inum <= 0)
            {
                Program.VerboseAudit(reason); // dont set indicator, just output reason
                return;
            }
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Set Ind #" + inum + ": " + reason);          //already set - repeat for verbose only
            } else {
                _inds[inum].is_checked = true;
                Program.Audit("Set Ind #" + inum + ": " + reason);
            }
        }

        //Clear an indicator for this reason (low level)
        private void ClrInd(int inum, string reason)
        {
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Clr Ind #" + inum + ": " + reason);          //already clear - repeat verbose only
            } else {
                _inds[inum].is_checked = false;
                Program.Audit("Clr Ind #" + inum + ": " + reason);
            }
        }

        // Count how many items meet the conditions
        // All items are listed
        private int CountItems(string cat, string desc_list, string code_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountItems(cat, desc_list, code_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountItems(string cat, string desc_list, string code_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)

        {
            bool first = true;
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            if (exclude_periph_iv)
                query = AndDescriptionNOTInList(query, "peripheral iv");
            int count = query.Count();
            found_what = "";
            if (count > 0 && trace)
            {
                foreach (var item in query)
                {
                    if (first)
                    {
                        // always return what was found
                        //            found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                        found_what = "Found desc=" + item.DESCRIPTION + ";result=" + item.RESULT + ";code=" + item.CODE + ";evdt=" + item.EVENT_DATETIME+ ";items found="+count;
                        // echo the result?
                        Program.VerboseAudit(found_what);
                        first = false;
                    }
                    
                }
            }

            return count;
        }


        // Count how many items meet the conditions
        // This has the option to list every entry or give a total
        private int CountResultInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            int count = 0;
            found_what = "";
            string s = "";

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            if (exclude_periph_iv)
                query = AndDescriptionNOTInList(query, "peripheral iv");
            var arr = SplitOnCommaAndPrepareElements(result_list);
            //Walker Schlundt
            //Set Ind #9: found '' in cat='' code='301870' field='' result='oriented x4' 

            foreach (var item in query) {
                // Figure out which of the search words were found
                for (int i = 0; i <= arr.GetUpperBound(0); i++)
                {
                    if (item.RESULT.Contains(arr[i]))
                    {
                        count++;
                        s = arr[i];
                        found_what = "Found desc:" + item.DESCRIPTION + ";result=" + item.RESULT +";code=" + item.CODE;
                        //if (trace) Program.VerboseAudit(found_what);
                        Program.VerboseAudit(found_what);
                    }
                }
                if (count_mode != CountMode.CountAll) break;        //stop counting

            }

            //if (count > 0) {
            //    //We already printed what was found; maybe add how many?
            //    if (trace && count > 0) Program.VerboseAudit("found " + count + " total");
            //} else {
            //    // Describe what was *not* found
            //    //found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
            //    //if (trace) Program.VerboseAudit(found_what);
            //}

            return count;
        }

        // Count how many items meet the conditions
        // This has the option to list every entry or give a total
        private int CountResultInListEXCEPTList(string cat, string code_list, string desc_list, string field, string result_list, string negresult_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            int count = 0;
            found_what = "";

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            query = AndResultNotInList(query, negresult_list);

            var arr = SplitOnCommaAndPrepareElements(result_list);

            foreach (var item in query)
            {
                // Figure out which of the search words were found
                var s = item.RESULT.ContainsWhich(arr);
                found_what = "found " + s + "' in cat=" + item.CATEGORY + "' code=" + item.CODE + "' field=" + item.FIELD_NAME + "' result=" + item.RESULT + "";
                if (trace) Program.VerboseAudit(found_what);
                count++;
                if (count_mode != CountMode.CountAll) break;        //stop counting
            }

            if (count > 0)
            {
                //We already printed what was found; maybe add how many?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            }
            else
            {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        // Count how many items contain ALL results in list
        private int CountResultInListAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            int count = 0;
            found_what = "";

            var arr = SplitOnCommaAndPrepareElements(result_list);

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            Program.VerboseAudit("arr ub=" + arr.GetUpperBound(0));
            for (int i = 0; i <= arr.GetUpperBound(0); i++)
            {
                if (arr[i].StartsWith(NOT_PREFIX))
                {
                    //query = query.Where(e => !e.RESULT.Contains(arr[i].Substring(2)));
                    query = AndResultNotInList(query, arr[i]);
                }
                else
                {
                    Program.VerboseAudit(i + ":" + arr[i]);
                    //query = query.Where(e => e.RESULT.Contains(arr[i]));
                    query = AndResultInList(query, arr[i]);
                }
            }
            //            Program.VerboseAudit("out of for loop");

            count = query.Count();
            //          Program.VerboseAudit("query count = " +count);

            if (count > 0)
            {
                found_what = "found item with all results in " + result_list + "' without those prefixed by !!";
                if (trace) Program.VerboseAudit(found_what);
            }
            else
            {
                // Describe what was *not* found
                //found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                //if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            if (ValueIsAList(result_list))
            {
                return CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what);
            } else {
                return CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what);
            }
        }

        // Is there an item with this result?
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContains(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            if (ValueIsAList(result_list))
            {
                return (CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            } else {
                return (CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
            }
        }

        // Result contains all in list
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            if (ValueIsAList(result_list))
            {
                return (CountResultInListAll(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            }
            else
            {
                found_what = "Not valid for non-list results.";
                return false;
            }
        }
        // Count how many items meet the conditions, with none of the results in result_list.
        // This has the option to say how many were found rather than list all.
        private int CountResultNotInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            int count = 0, rec_count = 0;
            bool found_one;

            found_what = "";
            var arr = SplitOnCommaAndPrepareElements(result_list);         // also deals with CHAR_COMMA

            //Do not filter by result here; we need all results
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            foreach (var item in query) {
                rec_count++;
                //Look for each search word in the result
                found_one = false;

                foreach (string s in arr) {
                    //                    if (String.Equals(item.RESULT, s)) {
                    if (item.RESULT.Contains(s))
                    {
                        found_what = "found " + s + ";result=" + item.RESULT + " -- exclude this";
                        Program.VerboseAudit(found_what);
                        found_one = true;
                        break;
                    }
                }

                if (!found_one) {
                    //none of the words were found - good!
                    found_what = Describe(cat, code_list, desc_list, field, "", search_depth) + " -- does not contain " + result_list + "";
                    Program.VerboseAudit(found_what);
                    count++;
                    if (count_mode != CountMode.CountAll) break;
                }
            }

            if (count > 0) {
                //We already printed what was found; maybe show the total?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else if (rec_count > 0) {
                //We already printed what we ignored
            } else {
                //Print what did not exist
                found_what = Describe(cat, code_list, desc_list, field, "", search_depth);      //not found
                Program.VerboseAudit(found_what);
            }

            return count;
        }

        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, string found_what)
        {
            // Search in the pull range with trace on
            return CountResultDoesNotContain(cat, code_list, desc_list, field, result_list, out found_what, SearchDepth.SearchDefault, true);
        }
        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, out string found_what, SearchDepth search_depth, bool trace)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            return CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0;
        }

        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            return (CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
        }

        // Set the indicator if the conditions are met
        //
        private bool SetIndIfResultContains(int inum, string cat, string desc_list, string code_list, string field, string result_list)
        {
            SearchDepth s = SearchDepth.SearchDefault;
            //if (inum == 5) s = SearchDepth.SearchSince13Hrs;
            //if (inum == 6) s = SearchDepth.SearchSince13Hrs;
            //if (inum == 7) s = SearchDepth.SearchSince13Hrs;
            //if (inum == 8) s = SearchDepth.SearchSince16Hrs;
            //if (inum == 9) s = SearchDepth.SearchSince16Hrs;
            //if (inum == 10) s = SearchDepth.SearchSince13Hrs;
            //if (inum == 11) s = SearchDepth.SearchSince9Hrs;
            //if (inum == 12) s = SearchDepth.SearchSince16Hrs;
            //if (inum == 13) s = SearchDepth.SearchSince16Hrs;
            //if (inum == 14) s = SearchDepth.SearchSince24Hrs;
            //if (inum == 19) s = SearchDepth.SearchSince9Hrs;
            //if (inum == 20) s = SearchDepth.SearchSince9Hrs;
            //if (inum == 21) s = SearchDepth.SearchSince16Hrs;
            //if (inum == 22) s = SearchDepth.SearchSince13Hrs;
            //if (inum == 23) s = SearchDepth.SearchSince9Hrs;
            //if (inum == 24) s = SearchDepth.SearchSince9Hrs;
            return SetIndIfResultContains(inum, cat, desc_list, code_list, field, result_list, s);

        }
        private bool SetIndIfResultContains(int inum, string cat, string desc_list, string code_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            bool b = false;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return true;

            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what)) {
                SetInd(inum, found_what);                          //echo here - found
                b = true;
            } else {
                //   Program.VerboseAudit(found_what);                  //and here - not found
            }
            return b;
        }

        private void SetIndIfResultBetween(int inum, string cat, string code_list, string desc_list, string field, int loval, int hival)
        {
            SetIndIfResultBetween(inum, cat, code_list, desc_list, field, loval, hival, SearchDepth.SearchDefault);
        }

        private void SetIndIfResultBetween(int inum, string cat, string code_list, string desc_list, string field, int loval, int hival, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            int count = 0;
            string found_what = "";

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");
            foreach (var item in query)
            {
                Program.VerboseAudit("ResBetween: code=" + item.CODE + " result=" + item.RESULT);
                if (item.RESULT.IsNumeric())
                {
                    Program.VerboseAudit("  result is numeric");
                    if (item.RESULT.Val() >= loval && item.RESULT.Val() <= hival)
                    {
                        count++;
                        found_what = "found code=" + item.CODE + " with result=" + item.RESULT;
                        Program.VerboseAudit(found_what);
                    }
                }
            }

            if (count > 0)
            {
                SetInd(inum, found_what);
            }
        }


        // Special contains all
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SearchDepth s = SearchDepth.SearchDefault;
            //if (inum == 8) s = SearchDepth.SearchSince24Hrs;
            SetIndIfResultContainsAll(inum, cat, code_list, desc_list, field, result_list, s);
        }
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return;

            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                          //echo here - found
            }
            else
            {
                //Program.VerboseAudit(found_what);                  //and here - not found
            }
        }
        // Set the indicator if the result does not contain any of the words in result_list
        //
        private void SetIndIfResultDoesNotContain(int inum, string cat, string desc_list, string code_list, string field, string result_list)
        {
            SetIndIfResultDoesNotContain(inum, cat, desc_list, code_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultDoesNotContain(int inum, string cat, string desc_list, string code_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return;

            //Turn trace off for ResultDoesNotContain() and echo what was set below with SetInd
            if (ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                           //echo here - not found
            } else {
                //Program.VerboseAudit(found_what);                   //and here - found
            }
        }

        // Clear the indicator if the result contains one of the words in the result_list
        //
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            //avoid more queries if the indicator is already clear
            if (!_inds[inum].is_checked) return;

            //Turn trace off for ResultContains() and echo what was set below with SetInd
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what)) {
                ClrInd(inum, found_what);                           //echo here - found
            } else {
                Program.VerboseAudit(found_what);                   //and here - not found
            }
        }


        //These slightly smaller functions are meant for places where you aren't really looking for a chart result
        private bool Exists(string desc_list, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists("", "", desc_list, "", result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool Exists(string desc_list, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists("", "", desc_list, "", result_list, search_depth, true, out found_what);
        }
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            return (CountResultContains(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
        }

        // Variations with optional parameters (can switch to optional parameters in VS 2010)
        //
        private void SetIndIfFound(int inum, string cat)
        {
            SetIndIfResultContains(inum, cat, "", "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list)
        {
            SetIndIfResultContains(inum, cat, code_list, "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private void ClrIndIfFound(int inum, string cat)
        {
            ClrIndIfResultContains(inum, cat, "", "", "", "", SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Get the max/total value from a result (usually in the middle of the text)
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetIntValue(get_mode, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            int value;
            int result = 0;
            bool found_one = false;

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);

            //Look for a number in the result

            foreach (var item in query) {
                // RESULT might be null so convert to empty string so Split won't blow up
                var arr = PFSDBUtility.DBToString(item.RESULT).Split(' ');
                foreach (string s in arr) {
                    //Look for 1st character numeric; IsNumeric("60min") will fail so look at 1st char only
                    if (s.Left(1).IsNumeric()) {
                        value = (int)s.Val();                           //Use Val; ToInteger will error on "60min"
                        switch (get_mode) {
                            case GetValueMode.GetMax:
                                result = Math.Max(value, result);       //max
                                break;
                            case GetValueMode.GetTotal:
                                result += value;                        //total
                                break;
                            case GetValueMode.GetLast:
                                result = value;                         //last
                                break;
                        }

                        // print what we are searching for (the first time)
                        if (!found_one) {
                            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
                        }
                        found_one = true;
                        // print each value found
                        Program.VerboseAudit("  found numeric value " + result);
                        //Keep going in case there are more
                    }
                }
            }

            if (!found_one) {
                //show what was not found
                Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
            }

            return result;
        }

        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetMaxValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            return GetIntValue(GetValueMode.GetMax, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetTotalValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            return GetIntValue(GetValueMode.GetTotal, cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Get a result; returns true if found with return_result
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result)
        {
            return GetResult(cat, code_list, desc_list, field, out return_result, SearchDepth.SearchDefault);
        }
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0) {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
            } else {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }

        private bool GetLatestResult(string code_list, out string return_result, SearchDepth search_depth)
        {
            return GetLatestResult("", code_list, "", "", out return_result, search_depth);
        }
        private bool GetLatestResult(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");
            query = query.OrderByDescending(e => e.EVENT_DATETIME);

            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT) + " charted at " + query.First().EVENT_DATETIME;
            }
            else
            {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }

        private bool GetResultForInspection(string cat, string code_list, string desc_list, string field, string result, out string return_result, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result);

            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
            }
            else
            {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }

        private void GetResultforOther(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "Other");
            return_result = "";
            foreach (var item in query)
            {
                if (!String.Equals(item.RESULT, "Other"))
                {
                    return_result = item.RESULT;
                    //Program.VerboseAudit(found_what);
                    break;
                }

            }
            return;
        }

        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, string res, out string return_result, out DateTime return_evdt)
        {
            return GetResultAndEVDT(cat, code_list, desc_list, field, res, out return_result, out return_evdt, SearchDepth.SearchDefault);
        }
        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, string res, out string return_result, out DateTime return_evdt, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, res);

            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
                return_evdt = PFSDBUtility.DBToDateTime(query.First().EVENT_DATETIME);
            }
            else
            {
                return_result = "";
                return_evdt = DateTime.MinValue;
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }
        private bool GetEVDT(string cat, string code_list, string desc_list, string res, string field, int comparison, DateTime compevdt, out DateTime return_evdt)
        {
            return GetEVDT(cat, code_list, desc_list, field, res, comparison, compevdt, out return_evdt, SearchDepth.SearchDefault);
        }
        private bool GetEVDT(string cat, string code_list, string desc_list, string field, string res, int comparison, DateTime compevdt, out DateTime return_evdt, SearchDepth search_depth)
        {
            bool sort_ascending = false;
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, res);
            if (compevdt != DateTime.MinValue)
            {
                if (comparison == 0) // equal
                    query = query.Where(e => e.EVENT_DATETIME == compevdt);
                else if (comparison == 1) // GTE
                    query = query.Where(e => e.EVENT_DATETIME >= compevdt);
                else if (comparison == 2) // GT
                    query = query.Where(e => e.EVENT_DATETIME > compevdt);
                else if (comparison == 22) // Least GT
                {
                    query = query.Where(e => e.EVENT_DATETIME > compevdt);
                    sort_ascending = true;
                }
                else if (comparison == 3) // LT
                    query = query.Where(e => e.EVENT_DATETIME < compevdt);
            }
            if (sort_ascending)
                query = query.OrderBy(e => e.EVENT_DATETIME);
            else
                query = query.OrderByDescending(e => e.EVENT_DATETIME);
            Program.VerboseAudit("getevdt count=" + query.Count());
            if (query.Count() > 0)
            {
                return_evdt = PFSDBUtility.DBToDateTime(query.First().EVENT_DATETIME);
            }
            else
            {
                return_evdt = DateTime.MinValue;
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, res, search_depth));
            return (return_evdt != DateTime.MinValue);
        }

        private int SetIndIfCodeBtwn(int ind, int locode, int hicode, SearchDepth search_depth)

        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = query.Where(e => e.CODE.ToLower().StartsWith("edu"));
            query = query.Where(e => e.CODE.Length >= 12);
            query = query.Where(e => e.CODE.Substring(3, 9).Val() >= locode);
            query = query.Where(e => e.CODE.Substring(3, 9).Val() <= hicode);
            int count = query.Count();
            // always return what was found
            string found_what = "There were " + count + " items found with code between " + locode + " and " + hicode;
            if (count >= 8) SetInd(ind, found_what);
            // echo the result?
            //               if (trace) Program.VerboseAudit(found_what);

            return count;
        }

        private DateTime LatestEVDT(string code_list, string desc_list, string res, SearchDepth search_depth)
        {
            DateTime latestdt = DateTime.MinValue;

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, "", code_list, desc_list, "", res);
            query = query.Where(e => e.ORDER_STATUS != "X");
            query = query.OrderByDescending(e => e.EVENT_DATETIME);
            int ct = query.Count();
            Program.VerboseAudit("query ct for " + code_list + "=" + ct);

            if (ct > 0)
            {
                latestdt = PFSDBUtility.DBToDateTime(query.First().EVENT_DATETIME);
            }
            else
            {
                latestdt = DateTime.MinValue;
            }
            return latestdt;
        }

        //======================================================================================
        // Mapping starts here
        //======================================================================================

        private void Check_1_2_3_4()
        {
            Program.VerboseAudit("Default ADL Search Scope = " + _pat.pull_finish.AddHours(-24) + " to " + _pat.pull_finish);


            Program.VerboseAudit("---------------");
            Program.VerboseAudit("1. ADL Self");
            Program.VerboseAudit("2. ADL Assist");
            Program.VerboseAudit("3. ADL Extended");
            Program.VerboseAudit("4. ADL Complete");
            Program.VerboseAudit("---------------");

        

            if (_pat.age < 4.0)
                SetInd(4, "Age=" + _pat.age);

            //if adl self with no documentation, then look back to previous shift for documentation.

            SearchDepth s = SearchDepth.SearchDefault;
            DoADLs(s);
            if (!_inds[1].is_checked && !_inds[2].is_checked && !_inds[3].is_checked && !_inds[4].is_checked)
            {
                SetInd(1, "No documentation found.  Changing look back to 2 shifts ago...");
                s = SearchDepth.SearchSince16Hrs;
                DoADLs(s);
                if (!_inds[1].is_checked && !_inds[2].is_checked && !_inds[3].is_checked && !_inds[4].is_checked)
                {
                    SetInd(1, "No documentation found.  Defaulting to Self.");
                }

            }
        }

        private void DoADLs(SearchDepth s)
        {
            string reslist = "";
            string codelist = "";
            bool feed1 = false;
            bool feed2 = false;
            bool feed4 = false;

            reslist = "none";
            feed1 = Exists("", "", "AS diet feeding assistance", "", reslist,s);

            reslist = "tray set-up, cup";
            feed2 = Exists("", "", "AS diet feeding assistance", "", reslist, s);
            reslist = "assisted with feeding, sippy cup";
            feed2 = Exists("", "", "AS diet feeding assistance", "", reslist, s);

            reslist = "total feed, bottle feed, breastfed, expressed breast milk";
            feed4 |= Exists("", "", "AS diet feeding assistance", "", reslist, s);
            reslist = "TPN, tube feeding";
            feed4 |= Exists("", "", "AS diet nutrition type", "", reslist, s);
            reslist = "";
            feed4 |= Exists("", "", "AS Feeding Assessment NBNI MG", "", reslist, s);
            feed4 |= Exists("", "", "As Breastfeeding Session Infant MG", "", reslist, s);
            feed4 |= Exists("", "", "As nonbreastfeeding Session Infant MG", "", reslist, s);
            feed4 |= Exists("", "", "INVG enteral feed safety", "", reslist, s);

            bool hyg1 = false;
            bool hyg2 = false;
            bool hyg4 = false;

            reslist = "shower";
            hyg1 |= Exists("", "", "INV skin hygiene", "", reslist, s);
            reslist = "bath partial";
            hyg2 |= Exists("", "", "INV skin hygiene", "", reslist, s);
            reslist = "back care";
            hyg2 |= Exists("", "", "INV skin hygiene", "", reslist, s);
            reslist = "bath complete";
            hyg4 |= Exists("", "", "INV skin hygiene", "", reslist, s);
            reslist = "incontinence care";
            hyg4 |= Exists("", "", "INV skin hygiene", "", reslist, s);

            reslist = "";
            hyg4 |= Exists("", "", "INV bath Infant", "", reslist, s);
            hyg4 |= Exists("", "", "INV Mouth care infant", "", reslist, s);
            hyg4 |= Exists("", "", "INV Perineal care nbni", "", reslist, s);

            reslist = "dental appliance cleaned";
            hyg2 |= Exists("", "", "INV oral care", "", reslist, s);
            reslist = "swabbed with antiseptic solution";
            hyg4 |= Exists("", "", "INV oral care", "", reslist, s);

            reslist = "independent";
            hyg1 |= Exists("", "", "INV hygiene assistance", "", reslist, s);
            reslist = "per care provider x1";
            hyg2 |= Exists("", "", "INV hygiene assistance", "", reslist, s);

            bool mob1 = false;
            bool mob2 = false;
            bool mob3 = false;
            bool mob4 = false;

            reslist = "up ad lib, activity adjusted per tolerance, activity encouraged, ambulated in hall, ambulated in room, ambulated to bathroom, sitting edge of bed,  up in chair, up to bedisde commode,  up in lounge/playroom, patient refuses activity";
            mob1 |= Exists("", "", "INV activity type", "", reslist, s);
            reslist = "activity adjusted per tolerance, activity encouraged, ambulated in hall, ambulated in room, ambulated to bathroom, sitting edge of bed, standing at bedisde, stepped/marched in place,  up in chair, up to bedisde commode, up in lounge/playroom";
            mob2 |= Exists("", "", "INV activity type", "", reslist, s);
            reslist = "activity adjusted per tolerance, ambulated in hall, ambulated in room, ambulated to bathroom, standing at bedisde, stepped/marched in place, up in chair, up to bedisde commode, up in stretcher chair";
            mob2 |= Exists("", "", "INV activity type", "", reslist, s);
            reslist = "bedrest, up in chair, up in stretcher chair, ";
            mob2 |= Exists("", "", "INV activity type", "", reslist, s);

            reslist = "independent";
            mob1 |= Exists("", "", "INV assist level", "", reslist, s);
            reslist = "assistance 1 person,  assistance stand by";
            mob2 |= Exists("", "", "INV assist level", "", reslist, s);
            reslist = "assistance 2 people";
            mob3 |= Exists("", "", "INV assist level", "", reslist, s);
            reslist = "assistance 3 or more people, lift team assistance";
            mob4 |= Exists("", "", "INV assist level", "", reslist, s);

            codelist = "INV dev care env mod infant,INV dev care posit aid infant";
            codelist += ",INV dev care posit head infant,INV dev care posit ext infant";
            codelist += ",INV dev care posit bdy infant,INV dev care prom infant";
            codelist += ",INV dev care arom infant,INV dev care stble cons infant,INV dev care atten intr infant";
            mob4 |= Exists("", "", codelist, "", "", s);

            bool toi1 = false;
            bool toi2 = false;
            bool toi4 = false;

            reslist = "up to toilet";
            toi1 |= Exists("", "", "INV elimination assist", "", reslist, s);
            reslist = "up to bedside commode, drainage bag emptied into patient specific container, bed pad changed, bedpan provided";
            toi2 |= Exists("", "", "INV elimination assist", "", reslist, s);
            reslist = "incontinence pad changed";
            toi4 |= Exists("", "", "INV elimination assist", "", reslist, s);

            reslist = "perineum cleansed, sitz bath given";
            toi2 |= Exists("", "", "INV perineal care", "", reslist, s);
            reslist = "absorbant pad changed, catheter care provided, diaper changed, perineum cleansed, protective cream applied, protective ointment applied, sitz bath given, skin sealant/barrier applied";
            toi4 |= Exists("", "", "INV perineal care", "", reslist, s);

            codelist = "AS external catheter size,AS colostomy type,AS fecal collector application";
            codelist += ",AS ileostomy type,AS rectal type,AS urethral cath type";
            toi4 |= Exists("", "", codelist, "", "", s);


            //if (feed4 && hyg4 && mob4 && toi4)
            //    SetInd(4, "All 4 ADL categories found at complete level.");
            int highadl = 0;
            if ((feed4 ? 1 : 0) + (hyg4 ? 1 : 0) + (mob4 ? 1 : 0) + (toi4 ? 1 : 0) >= 2)
            {
                SetInd(4, "At least 2 ADL categories found at the Complete level.");
                highadl = 4;
            }
            else if ((feed4 ? 1 : 0) + (hyg4 ? 1 : 0) + (mob4 ? 1 : 0) + (toi4 ? 1 : 0) >= 1)
            {
                SetInd(3, "1 of 4 ADL categories found at the Complete level warrants Extended.");
                highadl = 3;
            }
            else if (mob3)
            {
                SetInd(3, "Mobility with 2 assist qualifies for Extended.");
                highadl = 3;
            }

            if (highadl < 2)
            {
                if ((feed2 || feed4 ? 1 : 0) + (hyg2 || hyg4 ? 1 : 0) + (mob2 || mob4 ? 1 : 0) + (toi2 || toi4 ? 1 : 0) >= 1)
                {
                    SetInd(2, "At least 1 ADL category found at the Partial level.");
                    highadl = 2;
                }
            }
            if (highadl < 1)
            {
                if (feed1 || hyg1 || mob1 || toi1)
                    SetInd(1, "At least 1 ADL category found at the Independent level.");
            }

        }

        private void UpdatePtChartArrays(string orderid, string ordstatus, string newres)
        {
            foreach (var item in _chart_items_since24hrs)
            {
                if (item.ORDER_ID == orderid)
                {
                    if (ordstatus != "")
                        item.ORDER_STATUS = ordstatus;
                    if (newres != "")
                        item.RESULT = newres;
                }
            }
        }

        private void UpdatePtChartArraysCode(string code, string ordstatus,DateTime dcdt)
        {
            foreach (var item in _chart_items_since_admission)
            {
                if (item.CODE == code && item.EVENT_DATETIME <= dcdt)
                {
                    item.ORDER_STATUS = ordstatus;
                }
            }
        }



        private void Check_5()
        {
            string reslist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("5. ADL Rehab");
            Program.VerboseAudit("---------------");

            exclude_periop_data = false;
            reslist = "nonnutritive sucking";
            SetIndIfResultContains(5, "", "AS feed readiness cues", "", "", reslist);


        }


        private void Check_6_7()
        {
            string reslist;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("6. ADL 2-3 Caregivers");
            Program.VerboseAudit("7. ADL 4 or more Caregivers");
            Program.VerboseAudit("---------------");

            reslist = "assistance 2 people";
            SetIndIfResultContains(6, "", "", "INV assist level", "", reslist);
            reslist = "assistance 3 or more people";
            SetIndIfResultContains(7, "", "", "INV assist level", "", reslist);
        }

        private void Check_8()
        {
            string reslist;
            string found_what;
            string return_result;
            DateTime return_evdt;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("8. Communication");
            Program.VerboseAudit("---------------");

            reslist = "no, unable to assess";
            SetIndIfResultContains(8, "", "", "snch pref lang yn 3", "", reslist,SearchDepth.SearchSinceAdmission);

            reslist = "blind, visually impaired, deaf, hearing impaired";
            SetIndIfResultContains(8, "", "", "snch special needs ms", "", reslist);
            reslist = "right hearing aid, left hearing aid, glasses,";
            SetIndIfResultContains(8, "", "", "snch retained items mss", "", reslist);
            reslist = "endotracheal tube, garbled, hoarse, illogical, incoherent, pace/rate variance, slurred, speaking valve, trached, unable to speak";
            SetIndIfResultContains(8, "", "", "AS speech", "", reslist);
            reslist = "intubated, extubated, reintubated, self extubated, tracheostomy";
            SetIndIfResultContains(8, "", "", "AS airway status", "", reslist);
            reslist = "+4,+3,+2,+1,-1,-2";
            SetIndIfResultContains(8, "", "", "AS SC rass scale", "", reslist);
            //reslist = "none at this time";
            //SetIndIfResultContains(8, "", "", "ED person taught method", "", reslist);

            //add cyracom SinceAdmission
            reslist = "cyracom";
            SetIndIfResultContains(8, "", "", "MSSN LANG Method SS,SNCH Language Meth", "", reslist,SearchDepth.SearchSinceAdmission);
            
            reslist = "Engl";
            SetIndIfResultDoesNotContain(8, "", "", "MSSN LANG Method SS", "", reslist);//every A&I and obrecov
            SetIndIfResultDoesNotContain(8, "", "", "MSSN LANG List SS", "", reslist);

            reslist = "developmental level, literacy, cognitive limitations";
            SetIndIfResultContains(8, "", "", "eda ability learn", "", reslist);

        }



        private void Check_9()
        {
            string reslist;
            bool s2, s3, s4;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("9. Cognitive Support");
            Program.VerboseAudit("---------------");

            reslist = "x4";
            SetIndIfResultDoesNotContain(9, "", "", "AS orientation", "", reslist);
            reslist = "";
            SetIndIfResultContains(9, "", "", "INVG reality orientation", "", reslist);

        }


        private void Check_10_11()
        {
            string reslist;
            bool is_peds = false;
            //int ct;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("10. Behavior/Emotional Management");
            Program.VerboseAudit("11. Behavior/Emotional Mgmt - q 1 Hour");
            Program.VerboseAudit("---------------");


            reslist = "agitated, angry, anxious, excitable, sexually inappropriate, tearful, uncooperative";
            SetIndIfResultContains(10, "", "", "AS mood behavior", "", reslist);
            reslist = "code gray";
            SetIndIfResultContains(10, "", "", "snch_eas event code", "", reslist);
            reslist = "restless";
            SetIndIfResultContains(10, "", "", "AS SC rass scale", "", reslist);
            reslist = "afraid/fearful, agitated, angry, anxious, apprehensive, overstimulated/overactive, overwhelmed, tearful/crying, uncooperative";
            SetIndIfResultContains(10, "", "", "AS emotional state observe", "", reslist);

            reslist = "code gray,code grey";
            SetIndIfResultContains(11, "", "", "event note", "", reslist);

            reslist = "combative";
            SetIndIfResultContains(11, "", "", "AS emotional state observe", "", reslist);

            reslist = "combative (aggressive), hostile, hyperactive (agitated, impulsive), restless, threatening";
            SetIndIfResultContains(11, "", "", "AS mood behavior", "", reslist);
            //reslist = "";
            //SetIndIfResultContains(11, "", "", "snch_eas event code", "", reslist);
            reslist = "combative,very agitated,agitated";
            SetIndIfResultContains(11, "", "", "AS SC rass scale", "", reslist);
            reslist = "attention-seeking behavior, crying continuous/inconsolable, panic, repeated requests, restless";
            SetIndIfResultContains(11, "", "", "AS emotional state observe", "", reslist);

        }

        private int CountLTZero()
        {
            int ct = 0;

            return ct;
        }
        private int CountGTZero(out int ct12, out int ct34)
        {
            int ct = 0;
            ct12 = 0;
            ct34 = 0;


            ct = ct12 + ct34;
            return ct;
        }

        private bool IsQ4Freq()
        {
            string codelist;
            bool ret = false;
            List<gBucket> buckets;

            SetBucketSize(60);
            buckets = new List<gBucket>();
            codelist = "304239398";
            AddBuckets(buckets, "", codelist, "", "", "Yes");
            ret = AnalyzeBuckets(buckets, 15, 60, "indicator 11 attestation", false);

            return (ret);

        }
        private bool IsQ1Freq()
        {
            bool ret = false;


            return ret;
        }

        // (this isn't really a Q1h count -- it is just a count) Make this a q1hr count
        //private int ReturnQ1HrCount(string code_list, string result_list)
        //{
        //    var buckets = new List<gBucket>();
        //    SetBucketSize(60);
        //    AddBuckets(buckets, "", code_list, "", "", result_list);
        //    return CountBuckets(buckets);
        //}

        //private bool IsQ1Hour(int count)
        //{
        //    return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q1H);
        //}
        //private bool IsQ2Hour(int count)
        //{
        //    return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q2H);
        //}

        // Use this if counting safety buckets
        //private void CheckSafety(int count, string desc)
        //{
        //    if (_inds[13].is_checked) return;             //skip if highest already checked
        //    if (count == 0) return;
        //    switch (FreqForCount(_pat.los_hours, count))
        //    {
        //    case Frequencies.Q30M:
        //        SetInd(13, desc + " q30min");
        //        break;
        //    case Frequencies.Q1H:
        //    case Frequencies.Q2H:
        //        SetInd(12, desc + " q2h");
        //        break;
        //    default:
        //        Program.VerboseAudit(desc + ": " + count + " in " + Math.Round(_pat.los_hours) + " hours is not enough");
        //        break;
        //    }
        //}

        private void FindLatest(string code, string reslist, out string res, out DateTime evdt)
        {
            res = "???";
            evdt = DateTime.MinValue;

            var query = StartNewQuery(SearchDepth.SearchSince16Hrs);
            query = AndItemFilter(query, "", code, "", "", reslist);
            CHART_ITEM ch = query.OrderByDescending(e => e.EVENT_DATETIME).FirstOrDefault();
            if (ch == null) return;
            res = ch.RESULT;
            evdt = ch.EVENT_DATETIME;
            Program.VerboseAudit("Latest result and time:" + ch.RESULT + ch.EVENT_DATETIME.ToString());

        }

        private void Check_12_13()
        {
            string reslist;
            bool is_peds = false;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("12. Safety Management - q 2 Hours");
            Program.VerboseAudit("13. Safety Management - q 30 Minutes");
            Program.VerboseAudit("---------------");

            reslist = "";
            SetIndIfResultContains(12, "", "", "SNCH_restraint type", "", reslist);
            reslist = "";
            SetIndIfResultContains(12, "", "", "SNCH_restraint type Level II", "", reslist);
            reslist = "";
            SetIndIfResultContains(12, "", "", "AS seizure interventions", "", reslist);
            reslist = "assistive device/personal items within reach, elopement precautions initiated, fall prevention program maintained, family at bedside, muscle strengthening facilitated, nonskid shoes/slippers when out of bed, supervised activity, toileting scheduled, Fall Video Watched/Reviewed, Fall TIPS Prevention Plan, Fall TIPS education patient/family";
            SetIndIfResultContains(12, "", "", "INV safety fall reduction", "", reslist);
            reslist = "bed alarm set, chair alarm set, family to remain at bedside, security transponder on";
            SetIndIfResultContains(12, "", "", "INV elopement precautions", "", reslist);


            reslist = "";
            SetIndIfResultContains(13, "", "", "AS airway status", "", reslist);
            SetIndIfResultContains(13, "", "", "INV iNO therapy started DT", "", reslist);
            reslist = "Bipap, CPAP";
            SetIndIfResultContains(13, "", "", "EVS o2 device 7B8C", "", reslist);

            if (_pat.age < 18)
            {
                //for Age < 18 only
                reslist = "";
                SetIndIfResultContains(13, "", "", "iv piv site ssx", "", reslist);
                SetIndIfResultContains(13, "", "", "AS iv piv site ssx", "", reslist);
                SetIndIfResultContains(13, "", "", "AS iv site ssx", "", reslist);
                SetIndIfResultContains(13, "", "", "AS iv umbilical a devicesite ssx", "", reslist);
                SetIndIfResultContains(13, "", "", "AS iv umbilical v devicesite ssx", "", reslist);
            }

            reslist = "remote continuous visual monitoring";
            SetIndIfResultContains(13, "", "", "INV safety fall reduction", "", reslist);
            reslist = "safety attendant at bedside, continuous visual monitoring initiated, continuous visual monitoring education provided to patient/family, ongoing need for continuous visual monitoring";
            SetIndIfResultContains(13, "", "", "INV elopement precautions", "", reslist);
        }

        private bool IsValidDate(string yyyymmdd)
        { //expecting yyyymmdd
            Program.VerboseAudit("IsValidDate inpt: " + yyyymmdd);
            bool ret = true;
            if (yyyymmdd.Length < 8)
                return false;
            if (yyyymmdd.Length > 8)
                yyyymmdd = yyyymmdd.Substring(0, 8);
            ret = (yyyymmdd.Substring(0, 4).Val() > 2000
                && yyyymmdd.Substring(4, 2).Val() >= 1
                && yyyymmdd.Substring(4, 2).Val() <= 12
                && yyyymmdd.Substring(6, 2).Val() >= 1
                && yyyymmdd.Substring(6, 2).Val() <= 31);
            return ret;
        }

        private void Check_14()
        {
            string desc_found = "", code;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("14. Isolation");
            Program.VerboseAudit("---------------");

            string[] orderdesc_str = { "Contact Isolation",
                                     "Contact Plus Isolation",
                                     "Droplet Isolation",
                                     "Special Droplet and Contact Isolation",
                                     "Airborne and Contact Isolation",
                                     "Airborne Isolation" };

            foreach (string s in orderdesc_str)
            {
                if (OrderInProgress(s, out desc_found))
                    SetInd(14, "Order in effect: " + desc_found);
            }

        }

        private bool OrderInProgress(string desc, out string found_what)
        {
            bool ret = false;
            desc = desc.ToLower();
            found_what = "";
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = query.Where(e => e.DESCRIPTION.ToLower() == desc
                                  && e.EVENT_DATETIME < loc_out
                                  && e.ORDER_CONTROL.ToLower() == "nw"
                                  && e.ORDER_STATUS.ToLower() == "ip");
            query = query.OrderByDescending(e => e.EVENT_DATETIME);

            int count = query.Count();
            Program.VerboseAudit("order in progress: count=" + count + " since:"+loc_out);

            foreach (var itemA in query)
            {
                var query2 = StartNewQuery(SearchDepth.SearchSinceAdmission);
                query2 = query2.Where(e => e.DESCRIPTION.ToLower() == desc);
                query2 = query2.Where(e => e.ORDER_ID == itemA.ORDER_ID);
                query2 = query2.Where(e => e.ORDER_CONTROL.ToLower() == "ca");
                int ct2 = query2.Count();

                if (ct2 > 0)
                {
                    Program.VerboseAudit("order in progress: order_id=" + itemA.ORDER_ID + " _pat.pull_start=" + _pat.pull_start + " ct2="+ct2);
                    //                  Program.VerboseAudit("order in progress: q2ct=" + ct2);
                    foreach (var x in query2)
                    {
                        Program.VerboseAudit("x.event_datetime=" + x.EVENT_DATETIME);
                        if (x.EVENT_DATETIME >= _pat.pull_start)
                        {
                            ret = true;
                            found_what += "Order ID=" + itemA.ORDER_ID + ": " + itemA.DESCRIPTION + "; range start-end:" + itemA.EVENT_DATETIME.ToString() + " - " + x.EVENT_DATETIME.ToString();
                        }
                        else
                        {
                            DisableOrder(x.ORDER_ID);
                        }
                    }
                }
                else
                {
                    ret = true;
                    found_what = "Order ID=" + itemA.ORDER_ID + ": " + itemA.DESCRIPTION + "; result=" + itemA.RESULT + "; starting:" + itemA.EVENT_DATETIME.ToString();
                    //Program.VerboseAudit("AUDIT ORDER:" + found_what);
                }
            }


            return ret;
        }

        private void DisableOrder(string ordid)
        {
            //update ORDER_CONTROL = 'XNW' for _pat.encounter_id and ordid and NW
            var db = PFSDBUtility.NewSqlConnection();
            string q = "UPDATE chart_item set ORDER_CONTROL='XN' where encounter_id=" + _pat.encounter_id + " and order_id='" + ordid + "' and order_control='nw'";
            SqlCommand cmd = new SqlCommand(q, db);
            cmd.ExecuteNonQuery();
            db.Close();
        }

        private void DisableChartItem(string code, DateTime evdt, bool use_proc_start)
        {
            //update ORDER_STATUS = 'X' for _pat.encounter_id and ordid
            var db = PFSDBUtility.NewSqlConnection();
            //string q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and code='" + code + "' and (result='initiated' or result='continued') and event_datetime<=" + PFSDBUtility.SQLDateTime(evdt);
            string q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and code='" + code + "' and event_datetime<=" + PFSDBUtility.SQLDateTime(evdt);
            if (use_proc_start)
                q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and code='" + code + "' and procedure_start is not null and procedure_start<=" + PFSDBUtility.SQLDateTime(evdt);
            SqlCommand cmd = new SqlCommand(q, db);
            cmd.ExecuteNonQuery();
            db.Close();

        }
        private void DisableChartItem(string code, DateTime evdt)
        {
            DisableChartItem(code, evdt, false);
        }

        //private void CheckAssessment(int count, string desc)
        //{
        //    //if (_inds[18].is_checked) return;          //skip if highest already checked
        //    if (count == 0) return;                    //skip if none

            //    // This should work the same as the original code:
            //    switch (FreqForCount(_pat.los_hours, count))
            //    {
            //        case Frequencies.Q30M:
            //            SetInd(18, desc);
            //            break;
            //        case Frequencies.Q1H:
            //            SetInd(17, desc);
            //            break;
            //        case Frequencies.Q2H:
            //            SetInd(16, desc);
            //            break;
            //        case Frequencies.Q4H:
            //            SetInd(15, desc);
            //            break;
            //        default:
            //            break;
            //    }

            //}

        private void ShowBuckets(List<gBucket> buckets)
        {
            string s = "";
            foreach (var e in buckets)
            {
                s = "(" + e.bucket + "" + e.code + "" + e.evdt.ToString() + ")";
                Program.VerboseAudit("Assessmt item:=" + s);
            }
        }


        private void Check_15_16_17_18()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("15. Assessment q4h");
            Program.VerboseAudit("16. Assessment q2h");
            Program.VerboseAudit("17. Assessment q1h");
            Program.VerboseAudit("18. Assessment q30min");
            Program.VerboseAudit("---------------");


            SetIndIfResultContains(17, "", "AS Sputum amount", "", "", "Large,Copious");
            SetIndIfResultContains(16, "", "AS Sputum amount", "", "", "Moderate");
            SetIndIfResultContains(15, "", "AS Sputum amount", "", "", "Small");

            SetBucketSize(60);

            SetBucketSize(60);
            AnalyzeAssessments(15, 60);

            SetBucketSize(30);
            AnalyzeAssessments(18, 30);

            SetBucketSize(60);

        }

        private int GetAssessInd(DateTime loc_out_time, out DateTime classdt)
        {
            int ind = 0;
            classdt = DateTime.MinValue;
            //get assessment indicator at location out time = loc_out_time
            var db = PFSDBUtility.NewPfsDataContext();        // look for the unit default admission profile
            var query = from ce in db.CLASSIFICATION_EVENTs
                        from ia in db.INDICATOR_ANSWERs
                        where (ce.CLASSIFICATION_EVENT_ID == ia.CLASSIFICATION_EVENT_ID)
                        && (ce.ENCOUNTER_ID == _pat.encounter_id)
                        && (ce.EFFECTIVE_DATETIME_IN <= loc_out_time)
                        && (ia.INDICATOR_NUMBER >= 15 && ia.INDICATOR_NUMBER <= 18)
                        orderby ce.EFFECTIVE_DATETIME_IN descending
                        select new
                        {
                            ia.INDICATOR_NUMBER,
                            ce.CLASSIFICATION_DATETIME
                        };
            if (query.Count() > 0)
            {
                ind = query.First().INDICATOR_NUMBER;
                classdt = query.First().CLASSIFICATION_DATETIME;
            }
            return ind;
        }


        private void AnalyzeAssessments(int ind, int bucket_size)
        {
            string desclist;
            string reslist;
            List<gBucket> buckets;
            string freqstr = "";


            if (bucket_size == 60) freqstr = "====Q4/Q2/Q1 HR EVALUATION================";
            if (bucket_size == 30) freqstr = "====Q30 MIN EVALUATION====================";
            Program.VerboseAudit(freqstr + " bucket size=" + bucket_size + "  _bucket size=" + _bucket_size);


            //        Vitals:
            desclist = "EVS bp arterial systolic NU 89F8,EVS bp arterial diastolic NU 1CD2,";
            desclist += "CVS bp noninvasive systolic mmhg NU,CVS bp noninvasive diastolic mmhg NU,";
            desclist += "EVS orthostat bp systolic lying NU 398B,EVS orthostat bp diastolic lying NU 3211,";
            desclist += "EVS orthostat bp systolic sitting NU 8577,EVS orthostat bp diastolic sitting NU 34CC,";
            desclist += "EVS orthostat bp systolic stand NU BE32,EVS orthostat bp diastolic stand NU A902,";
            desclist += "CVS hr heart rate beats per min NU,CVS resp rate breaths per min NU,";
            desclist += "SNCH EVS ETCO2 NU,CVS temp fahrenheit CAL,";
            desclist += "CVS temp celsius CAL,CVS spo2 percent NU,";
            desclist += "EVS cvp mmhg NU 662C,EVS cvp cm h2o NU 3720,";
            desclist += "EVS pap systolic NU 89BD,EVS pap diastolic NU A7A3,";
            desclist += "EVS pap mean CAL  56F5,EVS pap wedge NU D40D,";
            desclist += "EVS icp NU C327,EVS icp monitor cpp read ACAL";

            string assessgrouplabel = "Vitals";
            buckets = new List<gBucket>();
            AddBuckets(buckets, "", "", desclist, "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);


            //Cardiovascular:
            desclist = "CVS pacer type,AS cardiac wdl,EVS iabp ratio CF87,EVS iabp assisted systole NU BCC7,";
            desclist += "EVS iabp augmentation NU 41DA,EVS iabp assisted diastole NU B35D,SNCH EVS Challenge SS,";
            desclist += "SNCH EVS Delta NU,EVSC chest pain scale 496C,AS chest pain location,AS pvs neuro vas wdl,";
            desclist += "AS radial pulse Lt,AS radial pulse Rt,AS dp pulse Lt,AS dp pulse Rt,";
            desclist += "AS post tib pulse Lt,AS post tib pulse Rt,AS edema trace,AS edema mild,";
            desclist += "AS edema moderate,AS edema severe,AS SC vte score CAL,";
            desclist += "AS cardiac nonstim wdl infant,AS cardiac nonstim monitor rhythm infant,AS cardiac stim wdl nicu,";
            desclist += "AS cardiac stim monitor rhythm nicu,AS heart sounds infant,AS cardiac murmur loc nicu,";
            desclist += "AS cardiac murmur intense nicu,AS cardiac pmi infant,AS cardiac precord assess infant";

            assessgrouplabel = "Cardiovascular";
            buckets = new List<gBucket>();
            AddBuckets(buckets, "", "", desclist, "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            //Pulmonary:
            desclist = "EVS o2 device 7B8C,CVS o2 method infant,AS respiratory wdl,AS airway status,";
            desclist += "AS incentive spirometer predict level NU,INV incentive spirometer adm,AS chest tube location,";
            desclist += "INV chest physiotherapy type,INV suction,SNCH_AS vent mode crit care,";
            desclist += "AS resp nonstim wdl nicu,AS resp excurs nonstim,AS resp infant nonstim,AS resp pattern nicu nonstim,";
            desclist += "AS Sputum amount";

            assessgrouplabel = "Pulmonary";
            buckets = new List<gBucket>();
            AddBuckets(buckets, "", "", desclist, "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            //Neuro:
            desclist = "AS pupils perrla yn,AS SC glasgow total CAL,AS motor response,CVS train of four,";
            desclist += "AS bispectral index NU,AS seizure activity,AS seizure presentation,AS evd level cm above NU,";
            desclist += "AS evd drainage,AS evd insert site,AS icp monitor transduc level,AS icp monitor read NU,";
            desclist += "AS icp monitor insert site,AS SC nih ss total CAL,SNCH_AS_SC_mrs,CASC EDP nih ss total CAL,";
            desclist += "Mod nih ss total CAL,MSSN Cardinal Result SS,SNCH FOUR Score Cal,MSSN Sheath Assess MS,";
            desclist += "MSSN Thrombolytic yn,MSSN Thrombolytic Bleeding yn,AS SC ciwa score CAL,";
            desclist += "AS reflexes infant,AS reflexes absent nbni,AS SC finn nas score CAL";

                        assessgrouplabel = "Neuro";
            buckets = new List<gBucket>();
            AddBuckets(buckets, "", "", desclist, "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            //Gi / GU Fluid Mgt:
            assessgrouplabel = "Gi/GU Fluid Mgt";
            buckets = new List<gBucket>();
            desclist = "AS gi wdl,CVS circumf abd inches CAL,CVS circumf abd cm CAL,AS gi intubation type,";
            desclist += "AS colostomy type,AS drain type,AS enterostomy tube,AS fecal collector application,";
            desclist += "AS ileostomy type,AS stoma appear,AS rectal type,AS gu wdl,AS urethral cath type,";
            desclist += "AS ext cath drainage method,AS cath drainage method,AS nephrostomy location,AS peritoneal type";
            AddBuckets(buckets, "", "", desclist, "");
            desclist = "EIO urine bladder volume per us NU 9A6B,EIO out urine cath indwell urethral NU 27E4,";
            desclist += "EIO out urine cath intermit urethral NU 4BF1,EIO out urine condom catheter NU E6F5,";
            desclist += "EIO out urine voided NU B0FA,EIO urine diaper count NU 61EF,EIO out urine diaper weight NU 7C70,";
            desclist += "EIO out urine nephrostomy NU D64D,EIO out urine suprapubic NU E0A8,EIO out urine ureteral cath NU 6498";
            AddBuckets(buckets, "", "", desclist, "");
            desclist = "EIO out urine penis pouch NU FF4E,EIO out urine urostomy NU 2C3E,EIO out urine or NU 9599,";
            desclist += "EIO out urine void not save count NU 4452,EIO out urine incontinence count NU 2665,SNCH_EIO out constant bladder irrigation,";
            desclist += "MSSN CRRT A Non PFlex Hourly Intake NU,MSSN CRRT B Non PFlex Hourly Output NU,EIO out gi blakemore NU F9C5,";
            desclist += "EIO out gi cholecystostomy NU 3683,EIO out gi emesis NU 1FCC,EIO out gi emesis count NU DDAC";
            AddBuckets(buckets, "", "", desclist, "");
            desclist = "EIO out gi gastrostomy NU 9B61,EIO out gi ileostomy NU 5EDC,EIO out gi jejunostomy NU 0DB4,";
            desclist += "EIO out gi levine NU C29F,EIO out gi miller abbott NU 3901,EIO out gi minnesota NU 5EAC,";
            desclist += "EIO out gi nasoduodenal NU 9F2A,EIO out gi nasogastric NU EDFA,EIO out gi orogastric tube NU E520,";
            desclist += "EIO out gi other gastric NU C0F8,EIO out gi other intestinal NU E88D,EIO out gi rectal tube NU C975";
            AddBuckets(buckets, "", "", desclist, "");
            desclist = "EIO out gi salem sump NU 94E8,EIO out gi stool NU 939C,EIO out gi stool count NU 439E,EIO out drain blake NU 1F37,";
            desclist += "EIO out drain chest tube NU 7493,EIO out drain hemovac NU F1F5,EIO out drain jackson pratt NU 38A0,EIO out drain mediastinal NU 7E58,";
            desclist += "EIO out drain ng tube NU 0F41,EIO out drain og tube NU 58FD,EIO out drain penrose NU 29C2,EIO out amount penrose DDCB,";
            desclist += "EIO out drain pericardial NU 6EBC,EIO out drain t tube NU 0A23,EIO out drain ventriculostomy NU 183A,EIO out drain wounds NU E12D";
            AddBuckets(buckets, "", "", desclist, "");
            desclist = "EIO out dialysis hemodialysis NU 052B,EIO out dialysis peritoneal NU,EIO out ebl estimated blood loss NU ADBE,EIO out ebl clot size 7656,";
            desclist += "EIO out ebl ob pad count NU 2665,EIO out ebl lab draws NU 106E,EIO out ebl ob bleed clots in ml NU 1492,EIO out tf tube feed residual NU AFA8,";
            desclist += "EIO in oral fluid NU E8A8,EIO in oral med fluid NU E800,EIO in oral breast milk NU 7C1D,EIO in bld cryoprecipate NU 0261,";
            desclist += "EIO in bld factor viii NU D4AF,EIO in bld factor ix NU 80A1,SNCH eio in bld Frozen Plasma,SNCH eio in bld Platelets";
            AddBuckets(buckets, "", "", desclist, "");
            desclist = "SNCH eio in bld Red Blood Cells,SNCH eio in bld Rh Immune Globulin,SNCH eio in bld Other Product,SNCH eio in col Albumin 5,";
            desclist += "SNCH eio in col Albumin 5,SNCH eio in col Albumin 25,EIO in pn fat emulsion 10 percent NU D640,EIO in pn fat emulsion 20 percent NU 6C67,";
            desclist += "EIO in ivpb d5w E11E,EIO in ivpb lr C716,EIO in ivpb nacl 0.9 BB8E,EIO in pn ppn NU 78ED,";
            desclist += "EIO in pn tpn NU 97CD,EIO in pn pediatric tpn NU 351D,EIO in pn fat emulsion 10 percent NU D640,EIO in pn fat emulsion 20 percent NU 6C67";
            AddBuckets(buckets, "", "", desclist, "");
            desclist = "SNCH eio in cry d2.5w 500 mL,SNCH eio in cry d5w 500mL,EIO in cry d5w 1000ml NU E6CB,EIO in cry d5w and alcohol 5 percent 1000ml NU CD00,";
            desclist += "EIO in cry d5w plus electrolyte mb 1000ml NU A8F2,EIO in cry d5w and electrolyte 48 1000ml NU 08FA,EIO in cry d5w plus 10meq kcl 1000ml NU 60BB,";
            desclist += "EIO in cry d5w plus 20meq kcl 1000ml NU 05EE,EIO in cry d5w plus 30meq kcl 1000ml NU 1FF0,EIO in cry d5w plus 40meq kcl 1000ml NU 8D0C,";
            desclist += "EIO in cry d5w plus 20meq kpo4 1000ml NU 9544,EIO in cry d5w plus 50eq nahco3 1000ml NU D3E9,SNCH eio in cry d5w nacl 0.2 500 mL";
            AddBuckets(buckets, "", "", desclist, "");
            desclist = "EIO in cry d5 and nacl 0.2 plus 10meq kcl 1000ml NU 57AA,EIO in cry d5 and nacl 0.2 plus 20meq kcl 1000ml NU C087,EIO in cry d5 and nacl 0.2 plus 30meq kcl 1000ml NU D52E,";
            desclist += "EIO in cry d5 and nacl 0.2 plus 40meq kcl 1000ml NU ED27,EIO in cry d5 and nacl 0.2 plus 20meq kpo4 1000ml NU 5842,EIO in cry d5 and nacl 0.2 plus 50meq nahco3 1000ml NU 41E2,";
            desclist += "SNCH eio in cry d5w nacl 0.33 % 500mL,SNCH eio in cry d5w nacl 0.33 % 1000mL,SNCH eio in cry d5w nacl 0.45 % 500mL,";
            desclist += "EIO in cry d5 and nacl 0.45 1000ml NU 156E,SNCH eio in cry d5w nacl 0.45 % 500mL + 10mEqKCL,SNCH eio in cry d5w nacl 0.45 % 500mL + 20mEqKCL";
            AddBuckets(buckets, "", "", desclist, "");
            desclist = "EIO in cry d5 and nacl 0.45 plus 10meq kcl 1000ml NU 665A,EIO in cry d5 and nacl 0.45 plus 20meq kcl 1000ml NU 4C64,EIO in cry d5 and nacl 0.45 plus 30meq kcl 1000ml NU A64D,";
            desclist += "EIO in cry d5 and nacl 0.45 plus 40meq kcl 1000ml NU BE37,EIO in cry d5 and nacl 0.45 plus 20meq kpo4 1000ml NU 8457,EIO in cry d5 and nacl 0.45 plus 50meq nahco3 1000ml NU B356,";
            desclist += "SNCH eio in cry d5w nacl 0.9 % 500mL,SNCH eio in cry d5w nacl 0.9 % 500mL + 10mEqKCL,SNCH eio in cry d5w nacl 0.9 % 500mL + 20mEqKCL,";
            desclist += "EIO in cry d5 and nacl 0.9 1000ml NU 4241,EIO in cry d5 and nacl 0.9 plus 10meq kcl 1000ml NU 5AAB,EIO in cry d5 and nacl 0.9 plus 20meq kcl 1000ml NU EAB5";
            AddBuckets(buckets, "", "", desclist, "");
            desclist = "EIO in cry d5 and nacl 0.9 plus 30meq kcl 1000ml NU 6202,EIO in cry d5 and nacl 0.9 plus 40meq kcl 1000ml NU 67DF,EIO in cry d5 and nacl 0.9 plus 20meq kpo4 1000ml NU 3E93,";
            desclist += "SNCH eio in cry d5w lr 500mL,SNCH eio in cry d5w lr 500mL + 10mEq KCL,SNCH eio in cry d5w lr 500mL + 20mEq KCL,";
            desclist += "EIO in cry d5 and lr 1000ml NU 0B89,EIO in cry d5 and lr plus 10meq kcl 1000ml NU 44AC,EIO in cry d5 and lr plus 20meq kcl 1000ml NU CCE2,";
            desclist += "EIO in cry d5 and lr plus 30meq kcl 1000ml NU 44AC,EIO in cry d5 and lr plus 40 meq kcl 1000ml NU 666E,EIO in cry d5 and lr plus 20meq kpo4 1000ml NU 53AC";
            AddBuckets(buckets, "", "", desclist, "");
            desclist = "EIO in cry d5 and lr plus 50meq nahco3 1000ml NU 5AEE,SNCH eio in cry d7.5w 500 mL,SNCH eio in cry d10w 500 mL,";
            desclist += "EIO in cry d10w 1000ml NU 7C1E,EIO in cry d10w and amino acids 10 percent 1000ml NU 7F3D,EIO in cry d10w plus 20meq kcl 1000ml NU 2968,";
            desclist += "EIO in cry d10w plus 20meq kpo4 1000ml NU 03C8,EIO in cry d10w plus 50meq nahco3 1000ml NU 2699,EIO in cry d10 and nacl 0.2 plus 20meq kcl 1000ml NU EBC7,";
            desclist += "EIO in cry d10 and nacl 0.45 plus 20meq kcl 1000ml NU 4BE1,EIO in cry d10 and nacl 0.45 plus 20meq kpo4 1000ml NU D341,EIO in cry d10 and nacl 0.45 plus 50meq nahco3 1000ml NU 8553";
            AddBuckets(buckets, "", "", desclist, "");
            desclist = "SNCH eio in cry d12.5w 500 mL,EIO in cry d12.5w 1000ml NU 6F4A,EIO in cry d12.5 and nacl 0.45 1000ml NU 8E62,";
            desclist += "SNCH eio in cry d15w 500 mL,EIO in cry d15w 1000ml NU B082,EIO in cry d15w and amino acids 10 percent 1000ml NU 0AA2,";
            desclist += "SNCH eio in cry d17.5w 500 mL,SNCH eio in cry d20w 500 mL,EIO in cry d20w 1000ml NU E78A,";
            desclist += "EIO in cry d20w and amino acids 10 percent 1000ml NU 6177,EIO in cry d20w plus 20meq kcl 1000ml NU 8C7B,EIO in cry d20w plus 20meq kpo4 1000ml NU E0C9";
            AddBuckets(buckets, "", "", desclist, "");
            desclist = "EIO in cry d20w plus 50meq nahco3 1000ml NU BC8B,EIO in cry d20 and nacl 0.2 1000ml NU 4070,EIO in cry d20 and nacl 0.45 1000ml NU 05AD,";
            desclist += "EIO in cry d25w 1000ml NU 3D08,EIO in cry d25w and amino acids 10 percent 1000ml NU 37F3,SNCH eio in cry lr 500mL,";
            desclist += "SNCH eio in cry lr 500mL + 10mEq KCl,SNCH eio in cry lr 500mL + 20mEq KCl,EIO in cry lr 1000ml NU 1421,";
            desclist += "EIO in cry lr plus 10meq kcl 1000ml NU 109A,EIO in cry lr plus 20meq kcl 1000ml NU 6F71,EIO in cry lr plus 30meq kcl 1000ml NU 7120";
            AddBuckets(buckets, "", "", desclist, "");
            desclist = "EIO in cry lr plus 40meq kcl 1000ml NU 2F81,EIO in cry lr plus 20meq kpo4 1000ml NU 755D,EIO in cry lr plus 50meq nahco3 1000ml NU EBE4,";
            desclist += "EIO in cry mannitol 5 percent NU A325,EIO in cry mannitol 10 percent NU ED94,EIO in cry mannitol 15 percent NU AE09,";
            desclist += "EIO in cry mannitol 20 percent NU B680,EIO in cry mannitol 25 percent NU 2913,SNCH eio in cry nacl 1.5 % 500 mL,";
            desclist += "SNCH eio in cry nacl 0.45 500mL,snch eio in cry nacl0.45 500ml + 10meq kcl,EIO in cry nacl 0.45 1000ml NU DDF4";
            AddBuckets(buckets, "", "", desclist, "");
            desclist = "EIO in cry nacl 0.45 plus 10meq kcl 1000ml NU 5691,EIO in cry nacl 0.45 plus 20meq kcl 1000ml NU C3DC,EIO in cry nacl 0.45 plus 30meq kcl 1000ml 371B,";
            desclist += "EIO in cry nacl 0.45 plus 40meq kcl 1000ml NU 47A2,EIO in cry nacl 0.45 plus 20meq kpo4 1000ml NU 3384,EIO in cry nacl 0.45 plus 50meq nahco3 1000ml NU 149F,";
            desclist += "SNCH eio in nacl 0.9 500mL,snch eio in cry nacl0.9 500ml + 10meq kcl,snch eio in cry nacl0.9 500ml + 20meq kcl,";
            desclist += "EIO in cry nacl 0.9 1000ml NU C5B3,EIO in cry nacl 0.9 plus 10meq kcl 1000ml NU EA64,SNCH eio NS0.9 100 + MVI + Thiame + FolicAcid";
            AddBuckets(buckets, "", "", desclist, "");
            desclist = "EIO in cry nacl 0.9 plus 20meq kcl 1000ml NU A1E3,EIO in cry nacl 0.9 plus 40meq kcl 1000ml NU 6B04,EIO in cry nacl 0.9 plus 20meq kpo4 1000ml NU 8DB5,";
            desclist += "EIO in cry nacl 0.9 plus 50meq nahco3 1000ml NU 5C16,SNCH eio in cry nacl 3 % 500 mL,EIO in cry nacl 3 percent 1000ml NU CE2C,";
            desclist += "SNCH eio in cry nacl 5 % 500 mL,EIO in cry nacl 5 percent 1000ml NU 3FF2,EIO in cry na bicarbonate NU A36A,";
            desclist += "EIO in cry na acetate NU 2EF9,EIO in cry na hydroxide NU CEF4,SNCH eio in cry Non Formulary IV Fluid,EIO in drip abciximab NU DB3D";
            AddBuckets(buckets, "", "", desclist, "");
            desclist = "EIO in drip acyclovir NU,EIO in drip alprostadil NU 309F,SNCH EIO in drip glucagon NU,EIO in drip alteplase NU DCE7,";
            desclist += "EIO in drip amino 3 % +Glycerin3 % +Lytes NU,EIO in drip amino 5.4 % NU,EIO in drip amino 8.5 % NU,EIO in drip aminocaproic acid NU 2B12,";
            desclist += "EIO in drip aminophylline NU 567A,EIO in drip amiodarone NU 267D,EIO in drip amphotericin B NU,EIO in drip amphotericin B Lipid Complex NU,";
            desclist += "EIO in drip argatroban NU 4ADC,SNCH EIO in drip Bivalirudin NU,EIO in drip bumetanide NU 3CD2,EIO in drip cefoxitin NU";
            AddBuckets(buckets, "", "", desclist, "");
            desclist = "EIO in drip cisatracurium NU 805F,EIO in drip Clevidipine NU SNCH,EIO in drip clindamycin NU,EIO in drip dexmedetomidine NU CE32,";
            desclist += "EIO in drip diltiazem NU EEC4,EIO in drip dobutamineacls NU F16E,EIO in drip dopamine NU A8DB,EIO in drip dopamineacls NU 3DC0,";
            desclist += "EIO in drip epinephrine NU 5A92,EIO in drip eptifibatide NU F8CF,EIO in drip esmolol NU 5114,EIO in drip fentanyl NU E4E8,";
            desclist += "EIO in drip furosemide NU 9AA6,SNCH_EIO in drp Heparin NU,EIO in drip hydromorphone NU F712,SNCH EIO in drip Remifentani NU";
            AddBuckets(buckets, "", "", desclist, "");
            desclist = "SNCH EIO in drip Rocuronium NU,EIO in drip indomethacin NU,SNCH_EIO in drp Insulin NU,EIO in drip isoproteranol NU B1CD,";
            desclist += "SNCH EIO in drip Ketamine NU,EIO in drip labetalol NU 25FE,EIO in drip lidocaine NU D1E8,EIO in drip lorazepam NU AB8F,";
            desclist += "EIO in drip magnesium sulfate NU 4AD6,SNCH EIO in drip Magnesium Sulfate Loading Dose,EIO in drip midazolam NU AB9C,EIO in drip milrinone NU 0C8E,";
            desclist += "EIO in drip morphine NU D08E,EIO in drip naloxone NU A160,EIO in drip nesiritide NU 7A5A,EIO in drip nicardipine NU A2AB";
            AddBuckets(buckets, "", "", desclist, "");
            desclist = "EIO in drip nitroglycerine NU 4531,EIO in drip nitroprusside NU A0D6,EIO in drip norepinephrine NU 6214,EIO in drip octreotide NU 11AD,";
            desclist += "SNCH_EIO in drp Oxytocin NU,EIO in drip pancuronium NU F91B,SNCH EIO in drip pantoprazole NU,EIO in drip phenylephrine NU 42ED,";
            desclist += "EIO in drip procainamide NU 2ED2,EIO in drip propofol NU 7CC2,EIO in drip propranolol NU,EIO in drip sufentanil NU EF29,";
            desclist += "EIO in drip terbutaline NU FF31,EIO in drip thiopental NU D0A4,SNCH_EIO in drp Vasopressin NU,EIO in drip vecuronium NU 805F";
            AddBuckets(buckets, "", "", desclist, "");
            desclist = "EIO Order Based Drip,EIO in tf brst milk NU D783,EIO in tf free water bolus NU 7994,SNCH eio tf formula tube feeding,";
            desclist += "EIO in tf glucose 5 percent water NU 4469,EIO in enteral tube flush NU,EIO in tf Glucerna NU,EIO in tf Jevity NU,";
            desclist += "EIO in tf Nepro NU,EIO in tf Osmolyte NU,EIO in tf Pulmocare NU,EIO in tf Suplena NU,";
            desclist += "EIO in tf Two Cal NU,EIO in tf Vital NU";
            AddBuckets(buckets, "", "", desclist, "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);


            //Wound:
            desclist = "AS skin wdl,AS SC braden score CAL,AS press ulcer location,AS incision body location,";
            desclist += "INV npwt,AS wound body location,AS rash body location,AS body location,AS burn body location";
            assessgrouplabel = "Wound";
            buckets = new List<gBucket>();
            AddBuckets(buckets, "", "", desclist, "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            //Medication:
            desclist = "snch bc titr start event dt,AS pain presence,AS pain rest NU,AS pain activity NU,";
            desclist += "AS pain word rest,AS pain word activity,AS pain presence faces,AS pain faces rest,";
            desclist += "AS pain faces activity,AS pain presence rflacc,AS SC rflacc rest score CAL,";
            desclist += "AS pain presence painad,AS SC painad score CAL,AS pain preferred scale infant,";
            desclist += "AS SC cries score CAL,AS SC nips score CAL,AS SC pipp score CAL,AS SC npass pain score CAL,";
            desclist += "AS SC npass sed score CAL,EVS pain infusion type 2957,SNCH EVS pca mg med D489,SNCH EVS pca mcg med D489,";
            desclist += "AS drain type,AS SC cpot score CAL,AS SC painad score CAL,AS nerve block site assessment,";
            desclist += "AS SC rass scale,Opioid POSS Scale SR";
            assessgrouplabel = "Medication";
            buckets = new List<gBucket>();
            AddBuckets(buckets, "", "", desclist, "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

        }

        private bool AnalyzeBuckets(List<gBucket> buckets, int ind, int bucketsize, string group)
        {
            return AnalyzeBuckets(buckets, ind, bucketsize, group, true);

        }

        private bool AnalyzeBuckets(List<gBucket> buckets, int ind, int bucketsize, string group, bool set_ind)
        {
            DateTime dt = DateTime.MinValue;
            DateTime firstdt = DateTime.MinValue;

            bool all_ok = OLDAnalyzeBuckets(buckets, ind, bucketsize, group, set_ind);
            return all_ok;


        }
        private bool DoShortLOSAssessEval(List<gBucket> buckets, int ind, int bucketsize, string group, bool set_ind)
        {
            int ct = 0;
            DateTime starttm, endtm;
            DateTime prevtm = DateTime.MinValue;
            int loshrs = (int)(Math.Round(_pat.los_hours));
            var arr = buckets.ToArray();
            bool set18 = false;
            Program.VerboseAudit("----Entering Short LOS Analysis for group: " + group + " LOS=" + loshrs + "  bucketsizew=" + bucketsize);

            for (int i = 0; (i < arr.GetUpperBound(0)); i++)
            {
                prevtm = DateTime.MinValue;
                starttm = arr[i].evdt;
                endtm = starttm.AddHours(loshrs);
                ct = 0;
                for (int j = i; (j < arr.GetUpperBound(0)); j++)
                {
                    if (prevtm < arr[j].evdt && arr[j].evdt >= starttm && arr[j].evdt <= endtm)
                    {
                        ct++;
                        prevtm = arr[j].evdt;
                    }
                }
                if (ct >= 9 / (4.0/loshrs))
                {
                    SetInd(18, ct.ToString() + " or more items in LOS=" + loshrs + " hours.");
                    set18 = true;
                    i = 999;
                }
            }
            Program.VerboseAudit("----Exiting Short LOS Analysis for group: " + group + " LOS=" + loshrs + "  bucketsizew=" + bucketsize);

            return set18;
        }


//Proposed change to remove consecutive factor; use straight scale with Short LOS exception:  
//	                        LOS=12 hrs      LOS = 6 hrs     LOS = 4 hrs     LOS = 2 hrs     Short LOS< 5 hrs
//q30 75% of 30-min buckets	3/4 x 24  = 18	3/4 x 12  = 9	3/4 x 8  = 6	3/4 x 4  = 3	5 or more VS
//q1 66% of 60-min buckets	2/3 x 12  = 8	2/3 x 6  = 4	2/3 x 4  = 2.67=>3	2/3 x 2  = 1.33=>1	3 VS + examine 30min 
//q2 50 % of 60-min buckets	.5 x 12  = 6	.5 x 6  = 3	.5 x 4  = 2	.5 x 2  = 1	2 VS
//q4 33% of 60-min buckets	1/3 x 12  = 4 2	1/3 x 6  = 2 1	1/3 x 4  = 1.33=>1	1/3 x 2  = .67=>1	1 VS
        private bool OLDAnalyzeBuckets(List<gBucket> buckets, int ind, int bucketsize, string group, bool set_ind)
        {
            DateTime dt = DateTime.MinValue;
            int bnum = 0;
            int numbucket = -99;
            int lobucket = 99;
            int hibucket = 0;
            int numitems = 0;
            int numconsec = 0;
            int greatestnumconsec = 0;
            bool all_ok = false;
            List<gBucket> dtlist = new List<gBucket>();
            Program.VerboseAudit("----Begin Assessment Group = " + group + " ---- bucketsizew=" + bucketsize + " start time of first bucket=" + _pat.pull_start);
            //if (ind==18 && Math.Round(_pat.los_hours) <= 4.0)
            //{
            //    all_ok = DoShortLOSAssessEval(buckets,ind,bucketsize,group,set_ind);
            //    Program.VerboseAudit("---- End Assessment Group = " + group + " ----");
            //    return all_ok;
            //}

            //Program.VerboseAudit("buckets count=" + buckets.Count());
            var b = buckets.OrderBy(e => e.evdt).ToList();

            //numitems = buckets.Count();
            foreach (var item in b)
            {
                if (numbucket < item.bucket)
                {
                    numbucket = item.bucket;
                    numitems++;
                    Program.VerboseAudit(item.bucket + ") " + item.evdt.ToString() + " := " + item.code);
                }
                //if (numbucket < item.bucket)
                //{
                //    //add dt to ary
                //    bnum++;
                //    dtlist.Add(item);
                //    if (numbucket == -99 || item.bucket - numbucket == 1)
                //    {
                //        numconsec++;
                //        if (greatestnumconsec < numconsec)
                //            greatestnumconsec = numconsec;
                //    }
                //    else
                //    {
                //        numconsec = 1;
                //    }
                //    numbucket = item.bucket;
                //    if (hibucket < item.bucket) hibucket = item.bucket;
                //    if (lobucket > item.bucket) lobucket = item.bucket;
                //    Program.VerboseAudit(item.bucket + ")." + item.evdt.ToString() + " := " + item.code);// + "  [distinct times only]");
                //}
            }

            //if (bnum <= 1)
            //{
            //    Program.VerboseAudit("---- End Assessment Group = " + group + " ----");
            //    return false;
            //}

            //Program.VerboseAudit("numitems=" + numitems);
            //Program.VerboseAudit("bucket count=" + bnum);
            //int half_los = (int)(_pat.los_hours * (30.0 / bucketsize));//for q30 this will be los_hours.  for q60 this will be .5 * los_hours
            //Program.VerboseAudit("half-LOS bucket count=" + half_los + " at " + bucketsize + "mins each.");
            //Program.VerboseAudit("greatest consec=" + greatestnumconsec);

            int num_buckets_in_los = (int)(_pat.los_hours * (60.0 / bucketsize));//for q30 this will be 2xlos_hours.  for q60 this will be los_hours
            Program.VerboseAudit("total bucket count in LOS=" + num_buckets_in_los);
            Program.VerboseAudit("num buckets filled=" + numitems);
            //Program.VerboseAudit("half-LOS bucket count=" + half_los + " at " + bucketsize + "mins each.");
            //Program.VerboseAudit("greatest consec=" + greatestnumconsec);

            //double bucketratio = (hibucket-lobucket) / (1.0 * (bnum-1));
            //q30 75% of 30-min buckets	3/4 x 24  = 18	3/4 x 12  = 9	3/4 x 8  = 6	3/4 x 4  = 3	5 or more VS
            //q1 66% of 60-min buckets	2/3 x 12  = 8	2/3 x 6  = 4	2/3 x 4  = 2.67=>3	2/3 x 2  = 1.33=>1	3 VS + examine 30min 
            //q2 50 % of 60-min buckets	.5 x 12  = 6	.5 x 6  = 3	.5 x 4  = 2	.5 x 2  = 1	2 VS
            //q4 33% of 60-min buckets	1/3 x 12  = 4 2	1/3 x 6  = 2 1	1/3 x 4  = 1.33=>1	1/3 x 2  = .67=>1	1 VS
            int q30need = (int)Math.Round(0.75 * 2 * _pat.los_hours);
            int q1need = (int)Math.Round(0.667 * _pat.los_hours);
            int q2need = (int)Math.Round(0.5 * _pat.los_hours);
            int q4need = 1;
            if (ind < 18)
            {
                if (_pat.los_hours > 5)
                {
                    if (_pat.los_hours < 8)
                    {
                        q4need = 1;
                        q2need = 2; //Jan27 2021
                    }
                    else
                    {
                        q4need = 2;
                        q2need = 4; // Feb3 2021 4;//Jan27 2021
                    }

                    if (numitems >= q1need)
                    {
                        SetInd(17 * Convert.ToInt32(set_ind), "Qualifies for q1 because numcharted=" + numitems + " is >=" + q1need + " LOS=" + Math.Round(_pat.los_hours, 2) + " x .667=" + q1need);
                        all_ok = (ind <= 17);
                    }
                    else if (numitems >= q2need)
                    {
                        //Jan27 2021 SetInd(16 * Convert.ToInt32(set_ind), "Qualifies for q2 because numcharted=" + numitems + " is >=" + q2need + " LOS=" + Math.Round(_pat.los_hours, 2) + " x .5=" + q2need);
                        SetInd(16 * Convert.ToInt32(set_ind), "Qualifies for q2 because numcharted=" + numitems + " is >=" + q2need + " for LOS=" + Math.Round(_pat.los_hours, 2) + "  >=8hrs needs 4 else 2");
                        all_ok = (ind <= 16);
                    }
                    else if (numitems >= q4need)
                    {
                        SetInd(15 * Convert.ToInt32(set_ind), "Qualifies for q4 because numcharted=" + numitems + " is >=" + q4need + " LOS=" + Math.Round(_pat.los_hours, 2) + "  >=8hrs needs 2 else 1");
                        all_ok = (ind <= 15);
                    }
                }
                else //short los
                {
                    q1need = 99;
                    q2need = 99;
                    q4need = 99;

                    int floor_los_hrs = (int)Math.Floor(_pat.los_hours);
                    if (floor_los_hrs >= 1)
                    {
                        q1need = floor_los_hrs;
                        if (floor_los_hrs / 2 >= 1)
                            q2need = floor_los_hrs / 2;
                    }
                    else if (_pat.los_hours > 0)
                    {
                        q2need = 1;
                    }
                    if (numitems >= q1need)
                    {
                        SetInd(17 * Convert.ToInt32(set_ind), "ShortLOS Qualifies for q1 because numcharted=" + numitems + " is >=" + q1need + " LOS=" + Math.Round(_pat.los_hours, 2) + " needs " + q1need);
                        all_ok = (ind <= 17);
                    }
                    else if (numitems >= q2need)
                    {
                        SetInd(16 * Convert.ToInt32(set_ind), "ShortLOS Qualifies for q2 because numcharted=" + numitems + " is >=" + q2need + " LOS=" + Math.Round(_pat.los_hours, 2) + " needs " + q2need);
                        all_ok = (ind <= 16);
                    }
                    else if (numitems >= q4need)
                    {
                        SetInd(15 * Convert.ToInt32(set_ind), "ShortLOS Qualifies for q4 because numcharted=" + numitems + " is >=" + q4need + " LOS=" + Math.Round(_pat.los_hours, 2) + " needs " + q4need);
                        all_ok = (ind <= 15);
                    }

                }
//                if (greatestnumconsec >= half_los || bucketratio <= 1.5 && bnum >= half_los-1)
//                {
//                    SetInd(17 * Convert.ToInt32(set_ind), "Qualifies for q1hr because chartings are at least q1hr for duration of half-LOS=" + half_los + " maxconsec=" + greatestnumconsec);
//                    all_ok = (ind <= 17);
//                }
//                //                else if (bucketratio <= 1.25 && bnum >= half_los && half_los >= 4)
//                //else if (bucketratio <= 1.25 && bnum >= half_los && half_los >= 4)
//                //{
//                //    SetInd(17 * Convert.ToInt32(set_ind), "Qualifies for q1hr because charting to bucket ratio is less than 1.25: hibucket-lobucket=" + hibucket +"-" +lobucket+"="+ (hibucket - lobucket) + " divided by num buckets=" + bnum + " equals " + bucketratio);
//                //    all_ok = (ind >= 17);
//                //}
//                //else if (bucketratio <= 2.5 && bnum >= 2)
//                //                else if (bnum >= half_los/2)
//                else if (bucketratio < 3 && bnum >= 3 && 
//                    (
//                    (half_los >= 6 && (hibucket - lobucket) >= half_los-1)
//                    ||
//                    ((half_los >= 4 && half_los <= 5) && (hibucket - lobucket) >= half_los - 1)
//                    ||
//                    ((half_los <= 3) && (hibucket - lobucket) >= half_los)
//                    )
//                    )
//                {
//                    SetInd(16 * Convert.ToInt32(set_ind), "Qualifies for q2hr because charting to bucket ratio is less than 3: hibucket-lobucket=" + hibucket + "-" + lobucket + "=" + (hibucket - lobucket) + " divided by num buckets=" + (bnum-1) + " equals " + bucketratio);
//                    all_ok = (ind <= 16);
//                }
////                else if (bucketratio > 2.5 && bucketratio <= 5 && bnum >= 2)
////                else if (bnum >= half_los/4)
//                else if ((bucketratio <= 6 && bnum >= 2) && //change to <=6 Oct15/2020
//                    (
//                    (half_los >= 6 && (hibucket - lobucket) >= half_los - 2)
//                    ||
//                    ((half_los >= 4 && half_los <= 5) && (hibucket - lobucket) >= half_los - 1)
//                    ||
//                    ((half_los <= 3) && (hibucket - lobucket) >= half_los)
//                    )
//                    )
//                {
//                    SetInd(15 * Convert.ToInt32(set_ind), "Qualifies for q4hr because charting to bucket ratio is <=6: hibucket-lobucket=" + hibucket + "-" + lobucket + "=" + (hibucket - lobucket) + " divided by num buckets=" + (bnum-1) + " equals " + bucketratio);
//                    all_ok = (ind == 15);
//                }
            }
            else // ind==18
            {
                if (_pat.los_hours > 5)
                {
                    if (numitems >= q30need)
                    {
                        SetInd(18 * Convert.ToInt32(set_ind), "Qualifies for q30 because numcharted=" + numitems + " is >=" + q30need + " LOS=" + Math.Round(_pat.los_hours, 2) + " x 2 x .75=" + q30need);
                        all_ok = (ind <= 18);
                    }
                }
                else
                {
                    q30need = 5;
                    if (numitems >= q30need)
                    {
                        SetInd(18 * Convert.ToInt32(set_ind), "ShortLOS Qualifies for q30 because numcharted=" + numitems + " is >=" + q30need + " LOS=" + Math.Round(_pat.los_hours, 2) + " x 2 needs " + q30need);
                        all_ok = (ind <= 18);
                    }
                }
                //if (greatestnumconsec >= half_los)
                //{
                //    SetInd(18 * Convert.ToInt32(set_ind), "Qualifies for q30min because consecutive chartings are at least q30 for duration of half-LOS=" + half_los + " maxconsec=" + greatestnumconsec);
                //    all_ok = (ind <= 18);
                //}
            }
            Program.VerboseAudit("---- End Assessment Group = " + group + " ----");
            return all_ok;
        }


        private void SetBucketSize(int minutes)
        {
            _bucket_size = minutes;
        }

        //Add to the list of bucket numbers (redundant buckets are fine)
        private void AddBuckets(List<gBucket> bucket_list, string cat, string desc, string code_list, string field)
        {
            AddBuckets(bucket_list, cat, code_list, desc, field, "", SearchDepth.SearchDefault);
        }
        private void AddBuckets(List<gBucket> bucket_list, string cat, string desc, string code_list,  string field, string result_list)
        {
            AddBuckets(bucket_list, cat, code_list, desc, field, result_list, SearchDepth.SearchDefault);

        }
        private void AddBuckets(List<gBucket> bucket_list, string cat, string desc, string code_list,  string field, string result_list, SearchDepth search_depth)
        {
            // Assign (virtual) bucket numbers to each assessment.  
            // Every x minutes after that is a new bucket.  Bucket zero = pull_start time.
//            Program.VerboseAudit("----Locating items for group: " + assessgrouplabel + "   bucketsize=" + _bucket_size);

            // get the chart items for the assessments
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc, field, result_list);
      query = query.Where(e => e.EVENT_DATETIME < _pat.pull_finish);
            int ct = query.Count();
            Program.VerboseAudit("ct=" + ct + " of codelist=" + code_list);
            // This step is needed for those who want to count the # within a bucket, like Shands #24.
            // This will have no effect on those that count buckets.
            //var query2 = (from item in query select new { item.EVENT_DATETIME }).Distinct();
            //var query2 = (from item in query select new { item.EVENT_DATETIME, item.CODE });

            //foreach (var x in query2)
            //{
            //    Program.VerboseAudit("patpullstart=" + _pat.pull_start.ToString() + " itemevdt=" + x.EVENT_DATETIME.ToString() + " diff=" + (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, x.EVENT_DATETIME)) + " bucketsize=" + _bucket_size + " bucket=" + (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, x.EVENT_DATETIME) / _bucket_size));
            //}

            // figure out what buckets the events belong to
            //var query3 = from item in query2
            //             select new {
            //                bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size)
            //             };
            //// Add to the list
            //foreach (var item in query3) {
            //    bucket_list.Add(item.bucket);
            //}

            var query3 = from item in query
                         select new
                         {
                             bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                             code = item.CODE,
                             desc = item.DESCRIPTION,
                             evdt = item.EVENT_DATETIME
                         };
            // Add to the list
            int cta = 0;
            foreach (var item in query3)
            {
                Program.VerboseAudit("  item: bucket=" + item.bucket + " c=" + item.code + "desc=" + item.desc + " dt=" + item.evdt);
                var b = new gBucket();
                b.bucket = item.bucket;
                b.code = item.code;
                b.description = item.desc;
                b.evdt = item.evdt;
                b.has_all_deps = true;
                gBucket f = bucket_list.Find(x => x.evdt == item.evdt);
                if (f.evdt != item.evdt)
                {
                    bucket_list.Add(b);
                    cta++;
                }
            }
            Program.VerboseAudit("add ct=" + cta);

        }


        private void AddVitalsBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field, string result_list, SearchDepth search_depth)
        {
            // Assign (virtual) bucket numbers to each assessment.  
            // Every x minutes after that is a new bucket.  Bucket zero = pull_start time.
            Program.VerboseAudit("----Locating Vital Signs: Filling buckets...Bucketsize=" + _bucket_size + " start time of first bucket=" + _pat.pull_start);

            // get the chart items for the assessments
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc, field, result_list);

            // This step is needed for those who want to count the # within a bucket, like Shands #24.
            // This will have no effect on those that count buckets.
            //var query2 = (from item in query select new { item.EVENT_DATETIME }).Distinct();
            //var query2 = (from item in query select new { item.EVENT_DATETIME, item.CODE });

            //foreach (var x in query2)
            //{
            //    Program.VerboseAudit("patpullstart=" + _pat.pull_start.ToString() + " itemevdt=" + x.EVENT_DATETIME.ToString() + " diff=" + (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, x.EVENT_DATETIME)) + " bucketsize=" + _bucket_size + " bucket=" + (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, x.EVENT_DATETIME) / _bucket_size));
            //}

            // figure out what buckets the events belong to
            //var query3 = from item in query2
            //             select new {
            //                bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size)
            //             };
            //// Add to the list
            //foreach (var item in query3) {
            //    bucket_list.Add(item.bucket);
            //}

            var query3 = from item in query
                         select new
                         {
                             bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                             code = item.CODE,
                             evdt = item.EVENT_DATETIME,
                             desc = item.DESCRIPTION
                         };
            var query4 = from item in query3
                         orderby item.bucket,item.code
                         select item;

            // Add to the list
            int currb = -1;
//            int prevb = -1;
            string currcode = "xx";
            DateTime currevdt = DateTime.MinValue;
            int currct = 0;
  //          bool addcurr = false;
            //Data is: 0, "123"
            //         0, "234"  0733
            //         0, "234"  0734
            //         1, "123"
            //         2, "234"
            //         2, "345"
            foreach (var item in query4)
            {
                int maxLength = Math.Min(item.desc.Length, 16);
                string d = item.desc.Substring(0, maxLength);
                Program.VerboseAudit("Vitals item: bucket=" + item.bucket + " dt=" + item.evdt + " desc=" + d + " c=" + item.code);
                if (currb != item.bucket)
                {
                    currb = item.bucket;
                    currcode = item.code;
                    currevdt = item.evdt;
                    currct = 1;
                }
                else
                {
                    if (currcode != item.code)
                    {  //minimum of 2 different VS codes
                        if (currct == 1)
                        { // add the first code first
                            var b = new gBucket();
                            b.bucket = item.bucket;
                            b.code = currcode;
                            b.evdt = currevdt;
                            b.has_all_deps = true;
                            //gBucket f = bucket_list.Find(x => x.evdt == item.evdt);
                            //if (f.evdt != item.evdt) bucket_list.Add(b);
                            bucket_list.Add(b);
                            Program.VerboseAudit("  adding item1: b=" + b.bucket + " c=" + b.code + " dt=" + b.evdt);
                        }
                        currct++;
                        currcode = item.code; //guarantees not to add code again in same bucket
                            
                        var b2 = new gBucket();
                        b2.bucket = item.bucket;
                        b2.code = item.code;
                        b2.evdt = item.evdt;
                        b2.has_all_deps = true;
                        //gBucket f = bucket_list.Find(x => x.evdt == item.evdt);
                        //if (f.evdt != item.evdt) bucket_list.Add(b);
                        bucket_list.Add(b2);
                        Program.VerboseAudit("  adding item2: b=" + b2.bucket + " c=" + b2.code + " dt=" + b2.evdt);
                    }
                }
            }

            // This is just for auditing: Say what we are looking for and how many were found
            if (!String.IsNullOrEmpty(result_list))
            {
                // print each word and if it was found or not
                //int i = CountResultContains(cat, code_list, desc, field, result_list);
            }
            else
            {
                // print how many were found
                //Program.VerboseAudit(Describe(cat, code_list, desc, field, ""));
            }
            Program.VerboseAudit("----End of Locating Vital Signs: Filling buckets...Bucketsize=" + _bucket_size);
        }

        //private void AddMedBuckets(List<gBucket> bucket_list)
        //{
        //    string descript = "";
        //    string drugclass = "";
        //    string rte = "";
        //    string result = "";

        //    bool bres1 = false;
        //    bool bres2 = false;
        //    bool bclass1 = false;
        //    bool bclass2 = false;
        //    bool bclass3 = false;
        //    bool bmed1 = false;

        //    string[] medclass = { "7", "1", "2", "6" };

        //    string[] pamed1417 = { "Zofran", "Ondansetron" };

        //    var query = StartNewQuery(SearchDepth.SearchSince13Hrs);    // add custom time range below
        //    query = query.Where(e => e.CODE.ToLower().StartsWith("med"));
        //    //query = query.Where(e => e.DESCRIPTION.Contains(";;;7;;;") || 
        //    //                         e.DESCRIPTION.Contains(";;;33;;;") || 
        //    //                         e.DESCRIPTION.Contains(";;;1;;;") || 
        //    //                         e.DESCRIPTION.Contains(";;;2;;;") || 
        //    //                         e.DESCRIPTION.Contains(";;;6;;;") || 
        //    query = query.Where(e =>
        //        medclass.Any(item1 => e.DESCRIPTION.Contains(";;;" + item1 + ";;;")) ||
        //       pamed1417.Any(item2 => e.DESCRIPTION.ToUpper().StartsWith(item2.ToUpper())));
        //    query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;given") || e.DESCRIPTION.ToLower().Contains(";;;syringe") || e.DESCRIPTION.ToLower().ContainsAny(meds_rate) && (e.UNIT_ID == _pat.unit_id) || e.DESCRIPTION.ToLower().Contains(";;;new"));
        //    query = query.Where(e => e.EVENT_DATETIME <= PFSUtility.MaxDateTime(_pat.unit_departure, _pat.effective_out));
        //    query = query.OrderBy(e => e.EVENT_DATETIME);
        //    //            Program.VerboseAudit("Num Meds found: " + query.Count());
        //    foreach (var item in query)
        //    {
        //        var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
        //        if (arr.GetUpperBound(0) >= 3)
        //        {
        //            descript = arr[0];
        //            drugclass = arr[1];
        //            rte = arr[3];
        //            result = "";
        //            if (arr.Length == 5) result = arr[4];
        //        }
        //        //Program.VerboseAudit("med desc: " + descript + "//" + "med class: " + drugclass + "//" + "med result: " + result);
        //        //exclude #29, 35, 48
        //        bres1 = false;
        //        bres2 = false;
        //        bclass1 = false;
        //        bclass2 = false;
        //        bclass3 = false;
        //        bmed1 = false;

        //        bres1 = result.ToLower().Contains("given");
        //        bres2 = result.ToLower().Contains("syringe") || result.ToLower().ContainsAny(meds_rate) || result.ToLower().Contains("newbag") || result.ToLower().Contains("new bag");
        //        bclass1 = (drugclass == "1") || (drugclass == "2") || (drugclass == "6");
        //        bclass2 = (drugclass == "7");
        //        bclass3 = false; // (drugclass == "33") && (rte.ToLower() == "sc");
        //        bmed1 = pamed1417.Any(item2 => descript.ToUpper().StartsWith(item2.ToUpper()));
        //        if ((bres1 && (bclass2 || bclass3)) ||
        //            ((bres1 || bres2) && bclass1) ||
        //            bmed1)
        //        {
        //            Program.VerboseAudit("====Med found: bucket count A====");
        //            Program.VerboseAudit("med code: " + item.CODE + " desc: " + item.DESCRIPTION + "  evdt: " + item.EVENT_DATETIME.ToString());
        //            SetInd(15, "Med class 7,1,2,6 or named med");
        //        }
        //    }

        //    // get the chart items for the assessments
        //    query = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
        //    query = query.Where(e => e.CODE.ToLower().StartsWith("med"));
        //    //query = query.Where(e => e.DESCRIPTION.Contains(";;;7;;;") || e.DESCRIPTION.Contains(";;;33;;;") || e.DESCRIPTION.Contains(";;;1;;;") || e.DESCRIPTION.Contains(";;;2;;;") || e.DESCRIPTION.Contains(";;;6;;;"));
        //    query = query.Where(e =>
        //       medclass.Any(item1 => e.DESCRIPTION.Contains(";;;" + item1 + ";;;")) ||
        //       pamed1417.Any(item2 => e.DESCRIPTION.ToUpper().StartsWith(item2.ToUpper())));
        //    query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;given") || e.DESCRIPTION.ToLower().Contains(";;;syringe") || e.DESCRIPTION.ToLower().ContainsAny(meds_rate) && (e.UNIT_ID == _pat.unit_id) || e.DESCRIPTION.ToLower().Contains(";;;new"));
        //    query = query.Where(e => e.EVENT_DATETIME <= PFSUtility.MaxDateTime(_pat.unit_departure, _pat.effective_out));
        //    query = query.OrderBy(e => e.EVENT_DATETIME);
        //    //            Program.VerboseAudit("Num Meds found: " + query.Count());
        //    foreach (var item in query)
        //    {
        //        var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
        //        if (arr.GetUpperBound(0) >= 3)
        //        {
        //            descript = arr[0];
        //            drugclass = arr[1];
        //            rte = arr[3];
        //            result = "";
        //            if (arr.Length == 5) result = arr[4];
        //        }
        //        //Program.VerboseAudit("med desc: " + descript + "//" + "med class: " + drugclass + "//" + "med result: " + result);
        //        //exclude #29, 35, 48
        //        bres1 = false;
        //        bres2 = false;
        //        bclass1 = false;
        //        bclass2 = false;
        //        bclass3 = false;
        //        bmed1 = false;

        //        bres1 = result.ToLower().Contains("given");
        //        bres2 = result.ToLower().Contains("syringe") || result.ToLower().ContainsAny(meds_rate) || result.ToLower().Contains("newbag") || result.ToLower().Contains("new bag");
        //        bclass1 = (drugclass == "1") || (drugclass == "2") || (drugclass == "6");
        //        bclass2 = (drugclass == "7");
        //        bclass3 = false;  //(drugclass == "33") && (rte.ToLower() == "sc");
        //        bmed1 = pamed1417.Any(item2 => descript.ToUpper().StartsWith(item2.ToUpper()));
        //        if ((bres1 && (bclass2 || bclass3)) ||
        //            ((bres1 || bres2) && bclass1) ||
        //            bmed1)
        //        {
        //            Program.VerboseAudit("====Med found: bucket count B====");
        //            Program.VerboseAudit("med code: " + item.CODE + " desc: " + item.DESCRIPTION + "  evdt: " + item.EVENT_DATETIME.ToString());
        //            //var query2 = (from item2 in query select new { item2.EVENT_DATETIME, item2.CODE });
        //            //var query3 = from item3 in query2
        //            //             select new
        //            //             {
        //            //                 bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item3.EVENT_DATETIME) / _bucket_size),
        //            //                 code = item3.CODE,
        //            //                 evdt = item3.EVENT_DATETIME
        //            //             };
        //            //foreach (var itemx in query3)
        //            //{
        //            //    var b = new gBucket();
        //            //    b.bucket = itemx.bucket;
        //            //    b.code = itemx.code;
        //            //    b.evdt = itemx.evdt;
        //            //    bucket_list.Add(b);
        //            //}
        //            var b = new gBucket();
        //            b.bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size);
        //            b.code = item.CODE;
        //            b.evdt = item.EVENT_DATETIME;
        //            b.has_all_deps = true;
        //            gBucket f = bucket_list.Find(x => x.evdt == item.EVENT_DATETIME);
        //            if (f.evdt != item.EVENT_DATETIME) bucket_list.Add(b);
        //        }
        //    }
        //}

        //private void AddMedBucketsDrugClass33(List<gBucket> bucket_list)
        //{
        //    string descript = "";
        //    string drugclass = "";
        //    string rte = "";
        //    string result = "";

        //    bool bres1 = false;
        //    bool bres2 = false;
        //    bool bclass1 = false;
        //    bool bclass2 = false;
        //    bool bclass3 = false;
        //    bool bmed1 = false;

        //    var query = StartNewQuery(SearchDepth.SearchSince13Hrs);    // add custom time range below
        //    query = query.Where(e => e.CODE.ToLower().StartsWith("med"));
        //    //query = query.Where(e => e.DESCRIPTION.Contains(";;;7;;;") || 
        //    //                         e.DESCRIPTION.Contains(";;;33;;;") || 
        //    //                         e.DESCRIPTION.Contains(";;;1;;;") || 
        //    //                         e.DESCRIPTION.Contains(";;;2;;;") || 
        //    //                         e.DESCRIPTION.Contains(";;;6;;;") || 
        //    query = query.Where(e => e.DESCRIPTION.Contains(";;;33;;;"));
        //    query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;given") || e.DESCRIPTION.ToLower().Contains(";;;syringe") || e.DESCRIPTION.ToLower().ContainsAny(meds_rate) && (e.UNIT_ID == _pat.unit_id) || e.DESCRIPTION.ToLower().Contains(";;;new"));
        //    query = query.Where(e => e.EVENT_DATETIME <= PFSUtility.MaxDateTime(_pat.unit_departure, _pat.effective_out));
        //    query = query.OrderBy(e => e.EVENT_DATETIME);
        //    //            Program.VerboseAudit("Num Meds found: " + query.Count());
        //    foreach (var item in query)
        //    {
        //        var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
        //        if (arr.GetUpperBound(0) >= 3)
        //        {
        //            descript = arr[0];
        //            drugclass = arr[1];
        //            rte = arr[3];
        //            result = "";
        //            if (arr.Length == 5) result = arr[4];
        //        }
        //        //Program.VerboseAudit("med desc: " + descript + "//" + "med class: " + drugclass + "//" + "med result: " + result);
        //        //exclude #29, 35, 48
        //        bres1 = false;
        //        bres2 = false;
        //        bclass1 = false;
        //        bclass2 = false;
        //        bclass3 = false;
        //        bmed1 = false;

        //        bres1 = result.ToLower().Contains("given");
        //        bclass3 = (drugclass == "33") && (rte.ToLower() == "sc");
        //        if (bres1 && bclass3)
        //        {
        //            Program.VerboseAudit("====Med found: Drug class 33 with Route=SC ==direct trigger for q4==");
        //            Program.VerboseAudit("med code: " + item.CODE + " desc: " + item.DESCRIPTION + "  evdt: " + item.EVENT_DATETIME.ToString());
        //            SetInd(15, "Med class 33");
        //        }
        //    }

        //    // get the chart items for the assessments
        //    query = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
        //    query = query.Where(e => e.CODE.ToLower().StartsWith("med"));
        //    //query = query.Where(e => e.DESCRIPTION.Contains(";;;7;;;") || e.DESCRIPTION.Contains(";;;33;;;") || e.DESCRIPTION.Contains(";;;1;;;") || e.DESCRIPTION.Contains(";;;2;;;") || e.DESCRIPTION.Contains(";;;6;;;"));
        //    query = query.Where(e => e.DESCRIPTION.Contains(";;;33;;;"));
        //    query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;given") || e.DESCRIPTION.ToLower().Contains(";;;syringe") || e.DESCRIPTION.ToLower().ContainsAny(meds_rate) && (e.UNIT_ID == _pat.unit_id) || e.DESCRIPTION.ToLower().Contains(";;;new"));
        //    query = query.Where(e => e.EVENT_DATETIME <= PFSUtility.MaxDateTime(_pat.unit_departure, _pat.effective_out));
        //    query = query.OrderBy(e => e.EVENT_DATETIME);
        //    //            Program.VerboseAudit("Num Meds found: " + query.Count());
        //    foreach (var item in query)
        //    {
        //        var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
        //        if (arr.GetUpperBound(0) >= 3)
        //        {
        //            descript = arr[0];
        //            drugclass = arr[1];
        //            rte = arr[3];
        //            result = "";
        //            if (arr.Length == 5) result = arr[4];
        //        }
        //        //Program.VerboseAudit("med desc: " + descript + "//" + "med class: " + drugclass + "//" + "med result: " + result);
        //        //exclude #29, 35, 48
        //        bres1 = false;
        //        bres2 = false;
        //        bclass1 = false;
        //        bclass2 = false;
        //        bclass3 = false;
        //        bmed1 = false;

        //        bres1 = result.ToLower().Contains("given");
        //        bclass3 = (drugclass == "33") && (rte.ToLower() == "sc");
        //        if (bres1 && bclass3)
        //        {
        //            Program.VerboseAudit("====Med found: Drug class 33 with Route=SC ==adding to freq eval==");
        //            Program.VerboseAudit("med code: " + item.CODE + " desc: " + item.DESCRIPTION + "  evdt: " + item.EVENT_DATETIME.ToString());
        //            //var query2 = (from item2 in query select new { item2.EVENT_DATETIME, item2.CODE });
        //            //var query3 = from item3 in query2
        //            //             select new
        //            //             {
        //            //                 bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item3.EVENT_DATETIME) / _bucket_size),
        //            //                 code = item3.CODE,
        //            //                 evdt = item3.EVENT_DATETIME
        //            //             };
        //            //foreach (var itemx in query3)
        //            //{
        //            //    var b = new gBucket();
        //            //    b.bucket = itemx.bucket;
        //            //    b.code = itemx.code;
        //            //    b.evdt = itemx.evdt;
        //            //    bucket_list.Add(b);
        //            //}
        //            var b = new gBucket();
        //            b.bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size);
        //            b.code = item.CODE;
        //            b.evdt = item.EVENT_DATETIME;
        //            b.has_all_deps = true;
        //            gBucket f = bucket_list.Find(x => x.evdt == item.EVENT_DATETIME);
        //            if (f.evdt != item.EVENT_DATETIME) bucket_list.Add(b);
        //        }
        //    }
        //}


        private int CountBuckets(List<gBucket> bucket_list)       // bucket list: (ha ha)
        {
            //int result = bucket_list.Distinct().Count();
            //if (result > 0) Program.VerboseAudit(result + " unique");
            //return result;
            int x = -99;
            int result = 0;
            //int result = bucket_list.Distinct().Count();
            var query = from b in bucket_list
                        orderby b.bucket ascending
                        select b;
            foreach (var b in query)
            {
                if (x != b.bucket)
                {
                    result++;
                    x = b.bucket;
                }
            }
            if (result > 0) Program.VerboseAudit(result + " unique");
            return result;
        }
        private void Check_19()
        {
            string reslist;
            string codelist;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("19. Vascular Access Site Mgt q1 Hour");
            Program.VerboseAudit("---------------");

            bool highalertmed = false;
            if (Exists("", "MEDHA,MEDIVHA", "", "", "", SearchDepth.SearchDefault))
                highalertmed = true;

            if (_pat.age <= 6.0 || _inds[11].is_checked || highalertmed)
            {
            reslist = "";
            SetIndIfResultContains(19, "", "", "INV iv cvl location", "", reslist);
            SetIndIfResultContains(19, "", "", "INV iv cvl location2", "", reslist);
            SetIndIfResultContains(19, "", "", "AS iv umbilical art device wdl infant", "", reslist);
            SetIndIfResultContains(19, "", "", "AS iv umbilical device wdl", "", reslist);
            SetIndIfResultContains(19, "", "", "AS iv intraosseous device wdl", "", reslist);
            SetIndIfResultContains(19, "", "", "AS iv piv device wdl", "", reslist);
            SetIndIfResultContains(19, "", "", "AS iv piv2 device wdl", "", reslist);
            SetIndIfResultContains(19, "", "", "AS iv piv3 device wdl", "", reslist);
            SetIndIfResultContains(19, "", "", "AS iv picc device wdl", "", reslist);
            SetIndIfResultContains(19, "", "", "AS iv picc device2 wdl", "", reslist);
            SetIndIfResultContains(19, "", "", "AS iv art line device wdl", "", reslist);
            SetIndIfResultContains(19, "", "", "AS iv art line device2 wdl", "", reslist);
            SetIndIfResultContains(19, "", "", "AS iv midline cath device wdl", "", reslist);
            SetIndIfResultContains(19, "", "", "AS iv midline cath device2 wdl", "", reslist);
            //reslist = "need age requirement";
            SetIndIfResultContains(19, "", "", "AS iv port device wdl", "", reslist);
            SetIndIfResultContains(19, "", "", "AS iv umbilical art device wdl infant", "", reslist);
            SetIndIfResultContains(19, "", "", "AS iv umbilical device wdl", "", reslist);
            }

        }


        private void CheckRouteCath()
        {
            int ct = 0;
            //OBX|1|DT|MED9003 ^ ALTEPLASE 50 MG INTRAVENOUS SOLUTION;;;20;;;0.5;;;cath; ; ; RateVerify | RateVerify | 20190802000000 |||||| F ||| 20190802000000
            //OBX|1|DT|MED9003 ^ HEPARIN  MG INTRAVENOUS SOLUTION;;;20;;;0.5;;;cath; ; ; RateVerify | RateVerify | 20190802000000 |||||| F ||| 20190802000000
            var query = StartNewQuery(SearchDepth.SearchSince9Hrs);    // add custom time range below
            query = query.Where(e => e.CODE.StartsWith("med"));
            query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;cath"));
            query = query.Where(e => e.DESCRIPTION.ToUpper().Contains("ALTEPLASE") ||
                                     e.DESCRIPTION.ToUpper().Contains("HEPARIN"));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            Program.VerboseAudit("Med ALTEPLASE or HEPARIN with cath found: " + query.Count());
            foreach (var item in query)
            {
                Program.VerboseAudit("====Med found: ALTEPLASE or HEPARIN====");
                Program.VerboseAudit("med code: " + item.CODE + " desc: " + item.DESCRIPTION + "  evdt: " + item.EVENT_DATETIME.ToString());

                var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                if (arr.GetUpperBound(0) >= 3)
                {
                    string rte = arr[3];
                    if (rte.ToLower() == "cath") ct++;
                }
            }
            if (ct > 0)
                SetInd(19, "Found ALTEPLASE or HEPARIN with route cath.");
        }

        private void Check_20()
        {
            string reslist;
            DateTime evdt;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("20. Medication Activity >= 20 minutes");
            Program.VerboseAudit("---------------");

            //CUSTOM MAPPING NOTE FOR INDICATOR 20:  
            //1) Trigger if there are 8 meds or more over a 30 min period. 
            //2) Trigger if there is 1 high risk med labels as high alert medication.  
            //3.Trigger if 3 or more medication administrations with the route contain " 
            //Intravenous" Can be same or different medications. 
            //4.Trigger for med route nebulizer treatments age less than or equal to 6 yrs. 
            //5.Trigger if med administered via these routes: Jejunostomy Tube, 
            //NasoGastric Tube, Orogastric, PEG Tube 
            //6.Any medication in NICU

            int ct = CountMedsIn30min();
            if (ct >= 8) SetInd(20, "Count of Meds within 30 mins=" + ct);

            //if (Exists("","MEDHA,MEDIVHA","","",""))
            string found_what = "";
            if (HighAlertMedExists(out found_what))
                SetInd(20, found_what);

            if (CountIntravenous() >= 3)
                SetInd(20, "Found 3 or more IV Meds");

            if (_pat.age < 7)
                if (NebulizerMedExists(out found_what))
                    SetInd(20, found_what);

            if (TubeMedExists(out found_what))
                SetInd(20, found_what);

            if (IsNICU())
                if (Exists("", "MED", "", "", ""))
                    SetInd(20, "Med found while pt is in NICU.");

            //OTHER mappings
            //Transfuse orders   TRIGGER FOR 1 TIME if not yet CA
            string desclist = "Transfuse 1 unit of Red Blood Cells,Transfuse 1 unit of Platelets,Transfuse 1 unit of Frozen Plasma,Transfuse 1 unit of Cryoprecipitate,Transfuse 1 unit of Special Blood Product,Transfuse 1 unit of Albumin";
            SetIndIfResultContains(20, "", "XS", desclist, "", "");

        }

        private bool IsNICU()
        {
            switch (_pat.unit_name)
            {
                case "NICU":
                    return true;
                default:
                    return false;
            }
        }

        private void CheckMedGiven(int ind, string desc)
        {

            string[] transplantmed_actions = { "given", "newbag", "new bag", "rateverify", "rate verify", "rate change", "ratechange", "restarted", "started/down" };

            for (int i = 0; i <= transplantmed_actions.GetUpperBound(0); i++)
            {
                transplantmed_actions[i] = ";;;" + transplantmed_actions[i];
            }

            int ct = 0;
            var query = StartNewQuery(SearchDepth.SearchSince9Hrs);
            query = AndItemFilter(query, "", "MED", desc, "", "");
            query = query.Where(e => e.DESCRIPTION.ToLower().ContainsAny(transplantmed_actions));
            ct = query.Count();
            if (ct > 0) SetInd(ind, "Found Med=" + desc);

        }

        private int CountMedsIn30min()
        {
            int ct = 0;
            DateTime evdt = DateTime.MinValue;
            DateTime endevdt = DateTime.MinValue;

            //  CALCIUM CHLORIDE 100 MG / ML(10 %) INTRAVENOUS SYRINGE; ; ; 29; ; ; 1; ; ; IV; ; ; Given
            //  SODIUM BICARBONATE 8.4 % (1 MEQ / ML) INJECTION(WRAPPER); ; ; 29; ; ; 50; ; ; osse; ; ; Given
            var query = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
            query = query.Where(e => e.CODE.ToUpper().StartsWith("MED"));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            if (query.Count() <  8) return 0;
            //Program.VerboseAudit("Num Meds found: " + query.Count());
            foreach (var item in query)
            {
                Program.VerboseAudit("Med=" + item.EVENT_DATETIME + ": " + item.DESCRIPTION);
                if (item.EVENT_DATETIME > evdt)
                {
                    evdt = item.EVENT_DATETIME;
                    endevdt = evdt.AddMinutes(30);
                    if (endevdt > loc_out) endevdt = loc_out;
                    var query2 = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
                    query2 = query2.Where(e => e.CODE.ToUpper().StartsWith("MED"));
                    query2 = query2.Where(e => e.EVENT_DATETIME >= evdt && e.EVENT_DATETIME <= endevdt);
                    if (query2.Count() > ct)
                        ct = query2.Count();
                }
            }

            Program.VerboseAudit("Greatest count of Meds in a 30 minute period=" + ct);
            return ct;
        }
        private bool HighAlertMedExists(out string found_what)
        {
            int ct = 0;
            int count = 0;
            DateTime evdt = DateTime.MinValue;
            DateTime endevdt = DateTime.MinValue;
            bool ret = true;
            found_what = "";

            var query = StartNewQuery(SearchDepth.SearchDefault); 
            query = query.Where(e => e.CODE.ToUpper().StartsWith("MED"));
            query = query.Where(e => e.CODE.ToUpper().Contains("HA"));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            if (query.Count() == 0) return false;

            found_what = "Found High Alert Med at " + query.First().EVENT_DATETIME + 
                " desc=" + query.First().DESCRIPTION;

            return ret;
        }
        private bool NebulizerMedExists(out string found_what)
        {
            int ct = 0;
            int count = 0;
            DateTime evdt = DateTime.MinValue;
            DateTime endevdt = DateTime.MinValue;
            bool ret = true;
            found_what = "";

            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = query.Where(e => e.CODE.ToUpper().StartsWith("MED"));
            query = query.Where(e => e.CODE.ToUpper().Contains("NEB"));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            if (query.Count() == 0) return false;

            found_what = "Found Nebulizer Med at " + query.First().EVENT_DATETIME +
                " desc=" + query.First().DESCRIPTION;

            return ret;
        }
        private bool TubeMedExists(out string found_what)
        {
            int ct = 0;
            int count = 0;
            DateTime evdt = DateTime.MinValue;
            DateTime endevdt = DateTime.MinValue;
            bool ret = true;
            found_what = "";

            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = query.Where(e => e.CODE.ToUpper().StartsWith("MED"));
            query = query.Where(e => e.CODE.ToUpper().Contains("TUBE"));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            if (query.Count() == 0) return false;

            found_what = "Found Tube Med at " + query.First().EVENT_DATETIME +
                " desc=" + query.First().DESCRIPTION;

            return ret;
        }

        private int CountIntravenous()
        {
            int ct = 0;
            int count = 0;
            DateTime evdt = DateTime.MinValue;
            DateTime endevdt = DateTime.MinValue;

            var query = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
            query = query.Where(e => e.CODE.ToUpper().StartsWith("MED"));
            query = query.Where(e => e.CODE.ToUpper().Contains("IV"));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            ct = query.Count();
            if (ct == 0) return 0;
            //Program.VerboseAudit("Num Meds found: " + query.Count());

            foreach (var item in query)
            {
                Program.VerboseAudit("IV Med=" + item.EVENT_DATETIME + ": " + item.DESCRIPTION);
            }

            return ct;
        }


        private void Check_21_22()
        {
            string reslist;
            bool st1 = false;
            string piv;
            string codelist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("21. Wound/Injury Mgmt");
            Program.VerboseAudit("22. Wound/Injury Mgmt >= 30 Minutes");
            Program.VerboseAudit("---------------");

            reslist = "foam" + CHAR_COMMA + " black,foam" + CHAR_COMMA + " silver,foam" + CHAR_COMMA + " white,gauze" + CHAR_COMMA + " antimicrobial,gauze" + CHAR_COMMA + " nonadherent,transparent dressing,unable to visualize";
            SetIndIfResultContains(22, "", "", "INV npwt dressing", "", reslist);
            reslist = "vacuum off";
            SetIndIfResultContains(21, "", "", EXACT_MATCH_PREFIX+"INV npwt", "", reslist);
            reslist = "continuous therapy, intermittent therapy";
            SetIndIfResultContains(22, "", "", EXACT_MATCH_PREFIX + "INV npwt", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "", "SNCH AS Press Ulcer Local MSSS", "", reslist);

            //reslist = "any 1 location";
            SetIndIfResultContains(21, "", "", "AS press ulcer location", "", reslist);
            SetIndIfResultContains(21, "", "", "AS incision body location", "", reslist);
            SetIndIfResultContains(21, "", "", "AS wound body location", "", reslist);
            SetIndIfResultContains(21, "", "", "AS burn body location", "", reslist);

            //3 or more for q30
            int ct = 0;
            reslist = "occipital region, forehead, ear, cheek, nares, shoulder, elbow, wrist, ischial tuberosity, hip, meatus, knee, malleolus, heel, thoracic spine, sacral spine, coccyx";
            ct += CountUniqueWounds("AS press ulcer location", reslist);
            reslist = "occipital region, head, temporal region, parietal region, scalp, face,";
            reslist += "forehead, orbital region, eyebrow, eye, ear, cheek, nose, nostril, jaw, lip,";
            reslist += "gum, palate, tongue, chin, neck, throat, cervical spine, torso, chest, clavicle, ";
            reslist += "sternal, mid axillary, breast, nipple, abdomen, epigastrium, quadrant, umbilical area, ";
            reslist += "ischial tuberosity, hip, greater trochanter, groin, pubis, perineum, penis, scrotum, ";
            reslist += "labia, vagina, urethral meatus, perirectal, anus, shoulder, axilla, arm, ";
            reslist += "elbow, antecubital, wrist, hand, palm, thumb, finger, thigh, leg, knee, ";
            reslist += "popliteal, calf, foot, ankle, malleolus, heel, plantar, toe, residual limb, ";
            reslist += "back, thoracic spine, scapula, lumbar spine, sacral spine, coccyx, gluteal, ";
            reslist += "flank";
            ct += CountUniqueWounds("AS incision body location", reslist);
            ct += CountUniqueWounds("AS wound body location", reslist);
            ct += CountUniqueWounds("AS burn body location", reslist);
            if (ct >= 3)
                SetInd(22, "Count of unique wound locations=" + ct);


            reslist = "x";
            SetIndIfResultContains(21, "", "", "AS iv art line device remove", "", reslist);
            reslist = "x";
            SetIndIfResultContains(21, "", "", "AS iv art line device remove2", "", reslist);
            reslist = "x";
            SetIndIfResultContains(21, "", "", "AS iv cvl device remove indicate", "", reslist);
            reslist = "x";
            SetIndIfResultContains(21, "", "", "AS iv cvl device remove indicate2", "", reslist);
            reslist = "x";
            SetIndIfResultContains(21, "", "", "snch AS iv cvl device remove indicate3", "", reslist);
            reslist = "x";
            SetIndIfResultContains(21, "", "", "snch AS iv cvl device remove indicate4", "", reslist);

            SetIndIfResultContains(21, "", "", "INV umb cord care infant", "", reslist);
            SetIndIfResultContains(21, "", "", "AS umb cord wdl infant", "", reslist);
            SetIndIfResultBetween(21, "", "", "AS SC nscs score CAL", "", 0, 5);
            SetIndIfResultBetween(22, "", "", "AS SC nscs score CAL", "", 6, 9);
            SetIndIfResultContains(21, "", "", "INV phototx source", "", reslist);
            SetIndIfResultContains(21, "", "", "INV phototx care", "", reslist);
            SetIndIfResultContains(21, "", "", "INVG pressure reduction tech infant", "", reslist);


        }

        private int CountUniqueWounds(string desc, string reslist)
        {
            int ct = 0;
            string res = "";
            var arr = SplitOnCommaAndPrepareElements(reslist);

            var query = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
            query = query.Where(e => e.DESCRIPTION.ToLower() == desc.ToLower());
            query = query.Where(e => e.RESULT.ContainsAny(arr));
            query = query.OrderBy(e => e.RESULT);
            foreach (var item in query)
            {
                if (res != item.RESULT)
                {
                    ct++;
                    Program.VerboseAudit("Wound location found: " + item.RESULT);
                    res = item.RESULT;
                }
            }
            return ct;
        }


        private void CheckDrainLDA()
        {
            string codelist = "9993040108530,3045001091";
            DateTime dt = DateTime.MinValue;
            var query1 = StartNewQuery(SearchDepth.SearchSince16Hrs);
            query1 = AndItemFilter(query1, "", codelist, "", "", "");
            query1 = query1.OrderBy(e => e.EVENT_DATETIME);
            foreach (var item in query1)
            { // for each start, find the number of records within 1 hr of it.
                if (dt == DateTime.MinValue)
                {
                    int ct = CountDrainLDAIn1Hour(codelist, item.EVENT_DATETIME);
                    if (ct >= 6)
                    {
                        SetInd(22, "Count of Drain LDAs within 1 hour=" + ct + " starting at: " + item.EVENT_DATETIME.ToString());
                        dt = item.EVENT_DATETIME;
                    }
                }
            }
            return;
        }

        private int CountDrainLDAIn1Hour(string codelist, DateTime startdt)
        {
            int ct = 0;
            var query1 = StartNewQuery(SearchDepth.SearchSince16Hrs);
            query1 = AndItemFilter(query1, "", codelist, "", "", "");
            query1 = query1.Where(e => e.EVENT_DATETIME >= startdt);
            query1 = query1.Where(e => e.EVENT_DATETIME <= startdt.AddMinutes(60));
            ct = query1.Count();
            return ct;

        }

        int EducMins(string timecode, string nursecode)
        {
            string res;
            int val = 0;
            if (ResultContains("", nursecode, "", "", "Nursing"))
            {
                if (GetResult("", timecode, "", "", out res))
                {
                    if (res.Left(1).IsNumeric())
                    {
                        val += (int)res.Val();
                    }
                }
            }
            return val;
        }

        private void Check_23()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("23. Healthcare Mgmt Education >= 1 Hour");
            Program.VerboseAudit("---------------");

            //Change this to look at the past 12 hours sum

            int mins = SumEDUmins();  
            if (mins >= 60)
                SetInd(23, "Sum of Education minutes=" + mins);

            // or 3 unique items with code like "EDI " 
            // dropped to 2 items on 3/10/22
            Educitems(2);

        }

        private int SumEDUmins()
        {
            int totmins = 0;
            var query = StartNewQuery(SearchDepth.SearchSince12Hrs);    // add custom time range below
            query = query.Where(e => e.DESCRIPTION.ToLower().StartsWith("SNCH ED minutes spent teaching NU".ToLower()));
            foreach (var item in query)
            {
                Program.VerboseAudit("Education mins=" + item.RESULT + " at " + item.EVENT_DATETIME);
                totmins += (int)item.RESULT.Val();
            }
            return totmins;
        }
        private void Educitems(int num_to_trigger)
        {
            string prevcode = "";
            int ct = 0;
            var query = StartNewQuery(SearchDepth.SearchSince12Hrs);    // add custom time range below
            query = query.Where(e => e.CODE.ToLower().StartsWith("EDI ".ToLower()));
            query = query.OrderBy(e => e.CODE);
            foreach (var item in query)
            {
                if (item.CODE != prevcode)
                {
                    Program.VerboseAudit("Education item=" + item.CODE + " at " + item.EVENT_DATETIME);
                    ct++;
                    prevcode = item.CODE;
                }
            }
            if (ct >= num_to_trigger)
                SetInd(23, "Found at least "+num_to_trigger+ " unique Education items.");
        }


        private void AddEducActivity(DateTime evdt)
        {
            DateTime enddt;
            Program.VerboseAudit("Activity 5: Found at " + evdt.AddHours(-1).ToString());
            if (!QueuedProcOverlaps(5, evdt.AddHours(-1), evdt))
                if (!ProcExistsInDB(5, evdt.AddHours(-1), out enddt))
                {
                    var proc = new proc_data();
                    proc.procedure_number = 5;
                    proc.start = evdt.AddHours(-1);
                    proc.finish = evdt;
                    _procs.Add(proc);
                    Program.Audit("Activity 5: Found at " + evdt);
                }
        }

        private void Check_24()
        {
            string reslist = "";
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("24. 1 to 1 Physiological Interv. >= 2 Hours");
            Program.VerboseAudit("---------------");

            //24. 1 to 1 Physiological Interv. >= 2 Hours  CHANGE TO BE FOR CURRENT 4-HR PERIOD   JUN 7 22
            SearchDepth sd = SearchDepth.SearchSince4Hrs;

            CheckEventNoteNoCodeGray();

            if (_pat.unit_name.ToUpper() == "NICU" || _pat.unit_name.ToUpper() == "NURSERY")
            {
                reslist = "Situation:111";
                SetIndIfResultContains(24, "", "", "event note", "", reslist,sd);
            }

            reslist = "";
            //SetIndIfResultContains(24, "", "", "SNCH_EAS Event Code", "", reslist);
            ////reslist = "Trigger if 'time called' and rapid response 'cancelled'";
            //SetIndIfResultContains(24, "", "", "RR Called Time", "", reslist);
            //reslist = "";
            //SetIndIfResultContains(24, "", "", "RR Rapid Response cancelled:", "", reslist);
            string resstr = "";
            string resstr2 = "";
            DateTime evdt = DateTime.MinValue;
            DateTime evdt2 = DateTime.MinValue;
            if (GetResultAndEVDT("", "", "Rapid Response Record v1", "", "time called", out resstr, out evdt, sd))
            {
                if (!GetResultAndEVDT("", "", "Rapid Response Record v1", "", "RAPID RESPONSE CANCELLATION", out resstr2, out evdt2, sd))
                {
                    SetInd(24, "Rapid Response: " + resstr + " at " + evdt);
                }
            }
            //CVS resp rate breaths per min NU

            //            CUSTOM MAPPING NOTE FOR INDICATOR 24:  
            //1.Trigger - 8 or greater vital signs (B / P HR, Resp) recorded in a 2 hour period
            //"EVS bp arterial systolic NU 89F8
            //EVS bp arterial diastolic NU 1CD2"
            //"CVS bp noninvasive systolic mmhg NU
            //CVS bp noninvasive diastolic mmhg NU"
            //"EVS orthostat bp systolic lying NU 398B
            //EVS orthostat bp diastolic lying NU 3211"
            //"EVS orthostat bp systolic sitting NU 8577
            //EVS orthostat bp diastolic sitting NU 34CC"
            //"EVS orthostat bp systolic stand NU BE32
            //EVS orthostat bp diastolic stand NU A902"


            //select * from chart_item where description = 'CVS hr heart rate beats per min NU'
            //select *from chart_item where description = 'CVS resp rate breaths per min NU'

            var buckets = new List<gBucket>();    // list of bucket numbers (for each match)
            var buckets2 = new List<gBucket>();    // list of bucket numbers (for each match)
            var buckets3 = new List<gBucket>();    // list of bucket numbers (for each match)
            string codelist = "EVS bp arterial systolic,EVS bp arterial diastolic,";
            codelist += "CVS bp noninvasive systolic, CVS bp noninvasive diastolic,";
            codelist += "EVS orthostat bp systolic lying,EVS orthostat bp diastolic lying,";
            codelist += "EVS orthostat bp systolic sitting,EVS orthostat bp diastolic sitting,";
            codelist += "EVS orthostat bp systolic stand,EVS orthostat bp diastolic stand";
            AddBuckets(buckets, "", "",codelist, "", "", sd);
            // Instead of counting the number of buckets to get the frequency,
            // we want to see if any two hours in a row had 8 or more assessments in the buckets.
            // Make a query that returns each bucket number and its item count.
            var query = buckets.GroupBy(x => x.bucket)   // group by the hour (x is an int)
                               .Select(g => new { Hour = g.Key, Count = g.Count() })
                               .OrderBy(e => e.Hour);
            var arr = query.ToArray();

            string codelist2 = "CVS hr heart rate beats per min";
            AddBuckets(buckets2, "", "", codelist2, "", "", sd);
            var query2 = buckets2.GroupBy(x => x.bucket)   // group by the hour (x is an int)
                               .Select(g => new { Hour = g.Key, Count = g.Count() })
                               .OrderBy(e => e.Hour);
            var arr2 = query2.ToArray();

            string codelist3 = "CVS resp rate breaths per min ";
            AddBuckets(buckets3, "", "", codelist3, "", "", sd);
            var query3 = buckets3.GroupBy(x => x.bucket)   // group by the hour (x is an int)
                               .Select(g => new { Hour = g.Key, Count = g.Count() })
                               .OrderBy(e => e.Hour);
            var arr3 = query3.ToArray();

            string s = "";
            foreach (var e in arr)
            {
                s += ",(" + e.Hour + "," + e.Count + ")";
            }
            if (s.Left(1) == ",") s = s.Substring(1);
            Program.VerboseAudit("Hourly counts for BP" + " = " + s);

            s = "";
            foreach (var e in arr2)
            {
                s += ",(" + e.Hour + "," + e.Count + ")";
            }
            if (s.Left(1) == ",") s = s.Substring(1);
            Program.VerboseAudit("Hourly counts for HR" + " = " + s);

            s = "";
            foreach (var e in arr3)
            {
                s += ",(" + e.Hour + "," + e.Count + ")";
            }
            if (s.Left(1) == ",") s = s.Substring(1);
            Program.VerboseAudit("Hourly counts RESP: " + s);

            for (int i = 0; (i < arr.GetUpperBound(0)); i++)
            {
                // Are these two hours in a row? (beware missing hours)
                if (arr[i].Hour == arr[i + 1].Hour-1)
                {
                    // Add these two consecutive hours
                    if (arr[i].Count + arr[i + 1].Count >= 8)
                    {
                        for (int j = 0; (j < arr2.GetUpperBound(0)); j++)
                        {
                            if ((arr2[j].Hour == arr[i].Hour) && (arr2[j].Hour == arr2[j + 1].Hour - 1))
                            {
                                if (arr2[j].Count + arr2[j + 1].Count >= 8)
                                {
                                    for (int k = 0; (k < arr3.GetUpperBound(0)); k++)
                                    {
                                        if ((arr3[k].Hour == arr[i].Hour) && (arr3[k].Hour == arr3[k + 1].Hour-1))
                                        {
                                            if (arr3[k].Count + arr3[k + 1].Count >= 8)
                                            {
                                                SetInd(24, "At least 8 sets of VS found within 2 hours");
                                                return;
                                            }
                                        }
                                    }

                                }
                            }
                        }

                    }
                }
            }




        }

        private bool CheckEventNoteNoCodeGray()
        {
            string[] events = { "rapid response", "medical team", "rrt", "code h" };

            var query = StartNewQuery(SearchDepth.SearchSince4Hrs);
            query = query.Where(e => e.CODE.ToUpper().StartsWith("EVENT NOTE"));
            query = query.Where(e => !e.RESULT.ToLower().Contains("Code Gray".ToLower()));
            query = query.Where(e => !e.RESULT.ToLower().Contains("Code Grey".ToLower()));
            query = query.Where(e => e.RESULT.ToLower().ContainsAny(events));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            if (query.Count() == 0) return false;

            string res = query.First().RESULT;

            SetInd(24, "Event Note: " + res);
            return true;

        }



        private void CheckUserDefined()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("User-Defined indicators");
            Program.VerboseAudit("---------------");

            //reslist = "Patient arrived during downtime,Patient was cared for during downtime,Patient departed during downtime";
            //SetIndIfResultContains(99, "", "9991600100203", "", "", reslist);
            //reslist = "Yes";
            //SetIndIfResultContains(99, "", "9990000006437", "", "", reslist);
            //reslist = "Green,Yellow ,Red ,Black ";
            //SetIndIfResultContains(99, "", "9991600100265", "", "", reslist);
            //reslist = "Downtime";
            //SetIndIfResultContains(99, "", "9990007096330", "", "", reslist);

        }


        //=====================================================================
        // Group like assessments into buckets of time
        // Each bucket counts as one assessment in the frequency count

        //private void AddDependentBuckets(List<gBucket> bucket_list, string codelist1, string reslist1, string codelist2, string reslist2)
        //{
        //    AddDependentBuckets(bucket_list, codelist1, reslist1, codelist2, reslist2, "", "");
        //}

        //private void AddDependentBuckets(List<gBucket> bucket_list, string codelist1, string reslist1, string codelist2, string reslist2, string codelist3,string reslist3)
        //{
        //    bool dep3 = true;
        //    // get the chart items for the assessments
        //    var query1 = StartNewQuery();
        //    query1 = AndItemFilter(query1, "", codelist1, "", "", reslist1);
        //    var query2 = StartNewQuery();
        //    query2 = AndItemFilter(query2, "", codelist2, "", "", reslist2);
        //    if (codelist3.Trim() == "")
        //    {
        //        dep3 = false;
        //        codelist3 = "Hello, this is a phantom code";
        //    }
        //    var query3 = StartNewQuery();
        //    query3 = AndItemFilter(query3, "", codelist3, "", "", reslist3);

        //    // figure out what buckets the events belong to
        //    var query1a = from item in query1
        //                  select new
        //                  {
        //                      bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
        //                      code = item.CODE,
        //                      evdt = item.EVENT_DATETIME
        //                  };
        //    var query2a = from item in query2
        //                  select new
        //                  {
        //                      bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
        //                      code = item.CODE,
        //                      evdt = item.EVENT_DATETIME
        //                  };
        //    var query3a = from item in query3
        //                      select new
        //                      {
        //                          bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
        //                          code = item.CODE,
        //                          evdt = item.EVENT_DATETIME
        //                      };

        //    string s = "BucketList1 for " + codelist1 + ": ";
        //    foreach (var item in query1a)
        //    {
        //        s += item.bucket + "";
        //    }
        //    Program.VerboseAudit(s);

        //    s = "BucketList2 for " + codelist2 + ": ";
        //    foreach (var item in query2a)
        //    {
        //        s += item.bucket + "";
        //    }
        //    if (dep3)
        //    {
        //        s = "BucketList3 for " + codelist3 + ": ";
        //        foreach (var item in query3a)
        //        {
        //            s += item.bucket + "";
        //        }
        //    }
        //    Program.VerboseAudit(s);
        //    // Add to the list IFF items in both lists occur in same bucket
        //    foreach (var item1 in query1a)
        //    {
        //        foreach (var item2 in query2a)
        //        {
        //            if (item1.bucket == item2.bucket)
        //            {
        //                if (dep3)
        //                {
        //                    foreach (var item3 in query3a)
        //                    {
        //                        if (item1.bucket == item3.bucket)
        //                        {
        //                            var b = new gBucket();
        //                            b.bucket = item1.bucket;
        //                            b.code = item1.code;
        //                            b.evdt = item1.evdt;
        //                            bucket_list.Add(b);
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    foreach (var item3 in query2a)
        //                    {
        //                        if (item1.bucket == item2.bucket)
        //                        {
        //                            var b = new gBucket();
        //                            b.bucket = item1.bucket;
        //                            b.code = item1.code;
        //                            b.evdt = item1.evdt;
        //                            bucket_list.Add(b);
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }

        //}

        private void AddSimpleProc(int pnum, DateTime evdt, DateTime enddt)
        {
            if (pnum <= 0) return;

            if (ProcExists(pnum, evdt, enddt))
            {
                Program.Audit("Activity " + pnum + ": already exists");
            }
            else
            {
                //if (ActivityFits(evdt, enddt))
                {
                    ProcOverlapsInDB_PEID(pnum, evdt, enddt,false); // then delete the db
                    var proc = new proc_data();
                    proc.procedure_number = pnum;
                    proc.start = evdt;
                    proc.finish = enddt;
                    _procs.Add(proc);
                    Program.Audit("Activity " + pnum + ": Found between " + evdt + " and " + enddt);
                }
            }
        }

        private bool ActivityFits(DateTime beg, DateTime fin)
        {
            bool ok = false;
            int unit_id = 0;
            string sql = "select el.unit_id from ENCOUNTER_LOCATION as el";
            sql += " where el.ENCOUNTER_ID=" + _pat.encounter_id;
            //sql += " and el.SPECIAL_UNIT_ID is null";
            sql += " and el.EFFECTIVE_DATETIME_IN<=" + beg.ToString() + "";
            sql += " and el.EFFECTIVE_DATETIME_OUT>=" + fin.ToString() + "";

            Program.VerboseAudit(sql);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd = new SqlCommand(sql, db2);
            SqlDataReader dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr2.Read())
            {
                if (dr2["UNIT_ID"] != DBNull.Value)
                    unit_id = PFSDBUtility.DBToInt(dr2["UNIT_ID"]);
            }
            ok = (unit_id > 0);

            if (ok)
            {
                db2.Close();
                return ok;
            }
            //Now check if two adjacent same units contains the activity.
            int unitid1 = 0;
            int unitid2 = 0;
            sql = "select el.unit_id from ENCOUNTER_LOCATION as el";
            sql += " where el.ENCOUNTER_ID=" + _pat.encounter_id;
            sql += " and " + beg.ToString() + "' between el.EFFECTIVE_DATETIME_IN and el.EFFECTIVE_DATETIME_OUT";
            cmd = new SqlCommand(sql, db2);
            dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr2.Read())
            {
                if (dr2["UNIT_ID"] != DBNull.Value)
                    unitid1 = PFSDBUtility.DBToInt(dr2["UNIT_ID"]);
            }
            sql = "select el.unit_id from ENCOUNTER_LOCATION as el";
            sql += " where el.ENCOUNTER_ID=" + _pat.encounter_id;
            sql += " and " + fin.ToString() + "' between el.EFFECTIVE_DATETIME_IN and el.EFFECTIVE_DATETIME_OUT";
            cmd = new SqlCommand(sql, db2);
            dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr2.Read())
            {
                if (dr2["UNIT_ID"] != DBNull.Value)
                    unitid2 = PFSDBUtility.DBToInt(dr2["UNIT_ID"]);
            }
            ok = (unitid1 > 0 && unitid1 == unitid2);
            //db2.Close();
            return ok;


        }

        //6	2019-09-18 15:00:00.000	2019-09-18 19:00:00.000
        //6	2019-09-19 07:00:00.000	2019-09-19 11:00:00.000

        private bool ActivityDuringClassType6(DateTime beg, DateTime fin, out DateTime classout)
        {
            bool ok = false;
            int pt = 0;
            classout = DateTime.MinValue;
            string sql = "select ce.PATIENT_TYPE,ce.effective_datetime_in,ce.effective_datetime_out from CLASSIFICATION_EVENT as ce";
            sql += " where ce.ENCOUNTER_ID=" + _pat.encounter_id;
            sql += " and ce.PATIENT_TYPE=6";
            sql += " and (" + beg.ToString() + "' >=ce.EFFECTIVE_DATETIME_IN and " + beg.ToString() + "' <ce.EFFECTIVE_DATETIME_OUT";
            sql += " or " + beg.ToString() + "' <ce.EFFECTIVE_DATETIME_IN and " + fin.ToString() + "' >ce.EFFECTIVE_DATETIME_OUT";
            sql += " or " + fin.ToString() + "' >ce.EFFECTIVE_DATETIME_IN and " + fin.ToString() + "' <=ce.EFFECTIVE_DATETIME_OUT)";
            Program.VerboseAudit(sql);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd = new SqlCommand(sql, db2);
            SqlDataReader dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr2.Read())
            {
                if (dr2["PATIENT_TYPE"] != DBNull.Value)
                {
                    pt = PFSDBUtility.DBToInt(dr2["PATIENT_TYPE"]);
                    classout = PFSDBUtility.DBToDateTime(dr2["EFFECTIVE_DATETIME_OUT"]);
                }
            }
            ok = (pt == 6);
            db2.Close();
            return ok;
        }

        private void AtLeastOneADL()
        {
            Program.Audit("---------------");

            if (!(_inds[2].is_checked || _inds[3].is_checked || _inds[4].is_checked)) {
                // Make "#2 ADL - Assist" the default.  (90% of patients)
                SetInd(1, "Defaulting to ADL Self due to lack of documentation.");
            }
        }

        private void HighestIndicatorInEachGroupWins()
        {
            int i, g;
            bool highest_is_on;
            string ind_list;

            Program.Audit("Select highest indicator in each group");

            g = 0;
            highest_is_on = false;
            // Go from bottom up - highest (lowest) indicator in each group wins
            for (i = MAX_INDS; (i >= 1); i--) {
                if (_inds[i].radio_group > 0) {
                    if (_inds[i].radio_group != g) {
                        //this is a new group
                        g = _inds[i].radio_group;
                        highest_is_on = _inds[i].is_checked;
                    } else {
                        //same group
                        if (highest_is_on) {
                            _inds[i].is_checked = false;             //uncheck a lower number
                        } else {
                            highest_is_on = _inds[i].is_checked;     //save this one
                        }
                    }
                }
            } // next i

            //Echo the indicators for an Audit (no classification will be saved)
            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++) {
                if (_inds[i].is_checked) ind_list += "," + i;
            } // next i

            Program.Audit("Final list = " + ind_list.Substring(1));
            Program.Audit("---------------");
        }


        private void CheckProcs()
        {
            CheckProc_1();
            CheckProc_2();
            CheckProc_3();
            CheckProc_4();
            CheckProc_5();
            CheckProc_6();
            CheckProc_7();
            CheckProc_8();
            CheckProc_9();
            CheckProc_10();
            CheckProc_11();

        }

        //private void DoProc(int pnum, string code)
        //{
        //    double mins = 0;
        //    string found_what;
        //    DateTime evdt;
        //    DateTime enddt = DateTime.MinValue;

        //    if (GetResultAndEVDT("", code, "", "", out found_what, out evdt))
        //    {
        //        //mins = 60.0 * found_what.ToDouble();
        //        if (found_what.Contains("180")) mins = 180;
        //        else if (found_what.Contains("120")) mins = 120;
        //        else if (found_what.Contains("90")) mins = 90;
        //        else if (found_what.Contains("60")) mins = 60;
        //        enddt = evdt.AddMinutes(mins);

        //        if (ProcExistsInDB(pnum, evdt, enddt))
        //        {
        //            Program.Audit("Activity " + pnum+ ": already exists");
        //        }
        //        else
        //        {
        //            if (!QueuedProcOverlaps(pnum, evdt, enddt))
        //            {
        //                var proc = new proc_data();
        //                proc.procedure_number = pnum;
        //                proc.start = evdt;
        //                proc.finish = enddt;
        //                _procs.Add(proc);
        //                Program.Audit("Activity " + pnum + ": Found " + code + " between " + evdt + " and " + enddt);
        //            }
        //        }

        //    }

        //}

        private bool ProcOverlapsInDB_PEID(int pnum, DateTime startdt, DateTime enddt, bool del_existing)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            //            LoadPatientProceduresIfNeeded();
            bool overlap_exists = false;
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && ((proc.PROCEDURE_DATETIME >= startdt) && (proc.PROCEDURE_DATETIME < enddt)
                                ||
                                (proc.DEPARTURE_DATETIME > startdt) && (proc.DEPARTURE_DATETIME <= enddt)
                                ||
                                (proc.PROCEDURE_DATETIME < startdt) && (proc.DEPARTURE_DATETIME > enddt)
                                )
                            //&& ( ! (proc.PROCEDURE_DATETIME == startdt) && (proc.DEPARTURE_DATETIME == enddt))
                            && (proc.CLASSIFIED_BY_ID < 0)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new { proc.PROCEDURE_EVENT_ID, proc.PROCEDURE_DATETIME, proc.DEPARTURE_DATETIME };
            overlap_exists = (query.Count() > 0);
            if (del_existing)
            {
                foreach (var a in query)
                {
                    Program.VerboseAudit("Will Delete act: procdt=" + a.PROCEDURE_DATETIME.ToString() + "  depdt=" + a.DEPARTURE_DATETIME.ToString());
                    Program.VerboseAudit("because it overlays startdt=" + startdt.ToString() + "  enddt=" + enddt.ToString());
                    DeleteActivity(a.PROCEDURE_EVENT_ID);
                }
            }
            //            peid = 0;
            return (overlap_exists);
        }
        private void DeleteActivity(int peid)
        {
            //            delete PROCEDURE_ANSWER where PROCEDURE_EVENT_ID=7211351
            //delete RPT_PROC_BY_DAY where PROCEDURE_EVENT_ID=7211351
            //delete PROCEDURE_EVENT where PROCEDURE_EVENT_ID=7211351
            if (peid == 0) return;

            Program.VerboseAudit("db ProcAnsw Deleting peid=" + peid);
            var db = PFSDBUtility.NewPfsDataContext();
            var query = from ia in db.PROCEDURE_ANSWERs
                        where (ia.PROCEDURE_EVENT_ID == peid)
                        select ia;
            if (query.Count() > 0)
            {
                var items = query.ToList();
                foreach (var item in items)
                    db.PROCEDURE_ANSWERs.DeleteOnSubmit(item);
                db.SubmitChanges();
            }

            Program.VerboseAudit("db RptProc Deleting peid=" + peid);
            var db2 = PFSDBUtility.NewPfsDataContext();
            var query2 = from r in db2.RPT_PROC_BY_DAYs
                         where (r.PROCEDURE_EVENT_ID == peid)
                         select r;
            if (query2.Count() > 0)
            {
                var items2 = query2.ToList();
                foreach (var item2 in items2)
                    db2.RPT_PROC_BY_DAYs.DeleteOnSubmit(item2);
                db2.SubmitChanges();
            }

            Program.VerboseAudit("db ProcEvent Deleting peid=" + peid);
            var db3 = PFSDBUtility.NewPfsDataContext();
            var query3 = from ce in db3.PROCEDURE_EVENTs
                         where (ce.PROCEDURE_EVENT_ID == peid)
                         select ce;
            if (query3.Count() > 0)
            {
                var items3 = query3.ToList();
                foreach (var item3 in items3)
                    db3.PROCEDURE_EVENTs.DeleteOnSubmit(item3);
                db3.SubmitChanges();
            }
        }



        private bool QueuedProcOverlaps(int pnum, DateTime startdt, DateTime enddt)
        {
            bool overlap = false;

            Program.VerboseAudit("Activity " + pnum + ": Check for dup at " + startdt.ToString() + " - " + enddt.ToString());
            proc_data[] pary = _procs.ToArray();
            for (int i = 0; i <= pary.GetUpperBound(0); i++)
            {
                if (pary[i].procedure_number == pnum)
                {
                    //overlap if   p.start between stardt and enddt
                    //             p.finish between startdt and enddt
                    //             startdt >= p.start and enddt <= p.finish
                    if ((pary[i].start >= startdt) && (pary[i].start < enddt) ||
                        (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
                        (startdt >= pary[i].start) && (enddt <= pary[i].finish))
                    {
                        overlap = true;
                    }

                    if ((pary[i].start >= startdt) && (pary[i].start < enddt) && (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
                        (pary[i].start < startdt) && (pary[i].finish > startdt) && (pary[i].finish < enddt)
                        )
                    {
                        // the proc in the list is completely contained in startdt/endt pair
                        // change this proc to be the startdt/enddt
                        // overlap is still true, but the list item will be updated with the encompassing times
                        //Program.Audit("Procedure " + pnum + ": " + startdt + " - " + enddt + " will trump " + pary[i].start + " - " + pary[i].finish);
                        pary[i].start = startdt;
                        pary[i].finish = enddt;
                    }
                }
            }
            _procs = pary.ToList();
            Program.VerboseAudit("Activity " + pnum + ": Check for dup returns " + overlap);
            return overlap;
        }

        private bool ProcExistsInDB(int pnum, DateTime startdt, out DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            Program.VerboseAudit("ProcExistsInDB: pnum=" + pnum + " startdt=" + startdt.ToString());
            int ct = 0;
            enddt = DateTime.MinValue;
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            //&& (proc.PROCEDURE_DATETIME <= startdt)
                            && (proc.DEPARTURE_DATETIME > startdt)
                            && (((pnum > 2) && (ans.PROCEDURE_NUMBER == pnum)) || ((pnum <= 2) && (ans.PROCEDURE_NUMBER <= 2)))
                        orderby proc.DEPARTURE_DATETIME descending
                        select new { proc.DEPARTURE_DATETIME };
            ct = query.Count();
            //Program.VerboseAudit("ProcExistsInDB: ct=" + ct);
            //if (ct > 0)
            //{
            //    foreach (var x in query)
            //    {
            //        Program.VerboseAudit("ProcExistsInDB: x=" + x.DEPARTURE_DATETIME);
            //    }
            //}
            if (ct > 0)
                enddt = (DateTime)query.First().DEPARTURE_DATETIME;
            //else //see if there is a saved proc that ended before this startdt
            //{
            //    query = from proc in _procedure_events
            //            from ans in proc.PROCEDURE_ANSWERs
            //            where (proc.ENCOUNTER_ID == _pat.encounter_id)
            //                //&& (proc.PROCEDURE_DATETIME <= startdt)
            //                && (proc.DEPARTURE_DATETIME > startdt.AddHours(-4))
            //                && (ans.PROCEDURE_NUMBER == pnum)
            //            orderby proc.DEPARTURE_DATETIME descending
            //            select new { proc.DEPARTURE_DATETIME };
            //    ct = query.Count();
            //    if (ct > 0)
            //        enddt = (DateTime)query.First().DEPARTURE_DATETIME;
            //}
            Program.VerboseAudit("ProcExistsInDB: pnum=" + pnum + " returns " + ct);
            return ct > 0;
        }
        private void ProcessProc(int pnum, int groupnum, string desc)
        {

        }
        private void NEWProcessProc(int pnum, string desc, string res, string termphrase)
        {
            string DSC1 = "NURS Med/Surg Activities";
            string DSC2 = "NURS Med/Surg Activites Freq";
            string DSC3 = "NURS Med/Surg Activities Start";

            string DSC4 = "NURSI Hour Activities";
            string DSC5 = "NURSI Hour Activities Freq";
            string DSC6 = "NURSI Hour Activities Start";

            string desc1="x", desc2 = "x", desc3 = "x";


            var arr = SplitOnCommaAndPrepareElements(res);
            //return query.Where(e => arr.Any(x => x.Contains(e.DESCRIPTION))); // "like" match

            DateTime evdt = DateTime.MinValue;
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = query.Where(e => e.DESCRIPTION.ToLower().StartsWith(desc.ToLower()));
            query = query.Where(e => e.RESULT.ToLower().ContainsAny(arr));
            query = query.Where(e => e.ORDER_CONTROL != "X");
            query = query.OrderByDescending(e => e.EVENT_DATETIME);
            Program.VerboseAudit("qct: " + query.Count());
            if (query.Count() == 0) return;

            DateTime start1 = query.First().EVENT_DATETIME; //this is the latest time


            bool found_dur = false;
            bool found_st = false;
            bool done = false;
            string dur = "";
            int stimecolpos;

            string stime; //14:00
            int durint;
            DateTime tempdate;
            string stimehr;
            string stimemin;
            DateTime date_of_event;
            DateTime dt_of_event;
            DateTime sdt;
            DateTime event_dt_plus1hr;

            foreach (var q in query)
            {
                if (evdt < q.EVENT_DATETIME && !done)
                {
                    evdt = q.EVENT_DATETIME;
                    var query2 = StartNewQuery(SearchDepth.SearchDefault);
                    query2 = query2.Where(e => e.DESCRIPTION.ToLower().StartsWith(desc.ToLower()));
                    query2 = query2.Where(e => e.EVENT_DATETIME >= evdt);
                    query2 = query2.Where(e => e.RESULT.ToLower().StartsWith(termphrase.ToLower()));
                    query2 = query2.OrderByDescending(e => e.EVENT_DATETIME);
                    Program.VerboseAudit("q2ct: " + query2.Count());
                    if (query2.Count() >= 0)
                    {
                        DateTime fin1 = query2.First().EVENT_DATETIME; 
                        found_dur = true;
                        done = true;
                        int dur2 =(int)PFSUtility.DateDiffInMinutes(start1, fin1);
                        Program.VerboseAudit("q2 duration: " + dur2);
                        if (dur2 >= 60)
                        {
                            AddSimpleProc(1, start1, fin1);
                            Program.VerboseAudit("AddSimpleProc: 1" + "  sdt=" + start1 + " fin=" + fin1);
                        }
                        else
                            Program.VerboseAudit("Will not Add Activity due to short duration minutes=" + dur2);

                        var db = PFSDBUtility.NewSqlConnection();
                        string qstr = "UPDATE chart_item set ORDER_CONTROL='X' where encounter_id=" + _pat.encounter_id + " and description like '" + desc + "%' and event_datetime between '" + start1.ToString() + "' and '" + fin1.ToString() + "'";
                        SqlCommand cmd = new SqlCommand(qstr, db);
                        cmd.ExecuteNonQuery();
                        db.Close();

                    }

                }
            }

            return;

            if (!found_dur)
            {
                Program.VerboseAudit("For activity: " + desc2 + " there was no frequency found.");
                return;
            }

            done = false;
            var query3 = StartNewQuery(SearchDepth.SearchDefault);
            query3 = query3.Where(e => e.DESCRIPTION.ToLower().StartsWith(desc3.ToLower()));
            query3 = query3.Where(e => e.EVENT_DATETIME == evdt);
            Program.VerboseAudit("q3ct: " + query3.Count());
            if (query3.Count() == 0)
            {
                Program.VerboseAudit("For activity: " + desc2 + " there was no start time found.");
                return;
            }

            found_st = true;
            done = true;
            stime = query3.First().RESULT;
            Program.VerboseAudit("q3 stime: " + stime);

//'determine the date for stime that is before rs(0)  say rs(0) = 2/1 01:00  stime=23:00
//' associate stime with rs(0) date.  compare this dt with rs(0)  
// if positive then ok because 2/1 23:00 is After 01:00
//' else associate stime with rs(0)date-1.  1/31 23:00


            stimecolpos = stime.IndexOf(":");
            stimehr = stime.Substring(0, stimecolpos);
            Program.VerboseAudit("q3 stimeHR: " + stimehr);
            if (stimehr.Length == 1) stimehr = "0" + stimehr;
            stimemin = stime.Substring(stimecolpos + 1);
            Program.VerboseAudit("q3 stimeMN: " + stimemin);

            event_dt_plus1hr = evdt.AddHours(1);
            Program.VerboseAudit("event_dt_plus1hr: " + event_dt_plus1hr);
            date_of_event = event_dt_plus1hr.Date;//Format$(event_dt_plus1hr, "yyyymmdd")  ' example 20150122 1400
            Program.VerboseAudit("date_of_event: " + date_of_event);
            dt_of_event = event_dt_plus1hr;// ' add an hour: 201501221500


            sdt = date_of_event.AddHours(stimehr.Val()).AddMinutes(stimemin.Val());
            Program.VerboseAudit("sdt: " + sdt);
            if (dt_of_event < sdt)  //'decrease date_of_event by 1 day
            {
                tempdate = event_dt_plus1hr;
                tempdate = tempdate.AddDays(-1);
                date_of_event = tempdate.Date;  //Format$(tempdate, "yyyymmdd")
                sdt = date_of_event.AddHours(stimehr.Val()).AddMinutes(stimemin.Val());
            }

            tempdate = sdt;

            if (found_dur && found_st)
            {
                //  durint = CInt(dur)
                //numprocs = numprocs + 1;
                //procs(numprocs).pnum = pnum
                //procs(numprocs).start = tempdate
                //procs(numprocs).finish = DateAdd("h", durint, tempdate)
                AddSimpleProc(pnum, sdt, sdt.AddMinutes(dur.Val()));
                Program.VerboseAudit("AddSimpleProc: " + pnum + "  sdt="+sdt + " dur="+dur);
            }


        }

        private void CheckProc_1()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A1. 1-1 safety observation by RN");
            Program.VerboseAudit("---------------");

            string desc = "SNCH_restraint type Level II";
            string res = "4-Point and Constant Observation";
            string terminate_phrase = "restraint removed";
            Program.VerboseAudit("Looking for " + desc);
            NEWProcessProc(1, desc, res, terminate_phrase);

            desc = "SNCH_Restraint Action Codes";
            res = "initial application,continued restraint,release and reapply";
            Program.VerboseAudit("Looking for " + desc);
            NEWProcessProc(1, desc,res,terminate_phrase);

        }

        private void CheckProc_2()
        {
            DateTime evdt=DateTime.MinValue, enddt = DateTime.MinValue;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A2. 1-1 safety observation by non-RN");
            Program.VerboseAudit("---------------");

            //string code = "INV elopement precautions";  // every 12 hours
            //string res = "safety attendant at bedside";
            string code = "SNCH_lethality_observation";  // every 12 hours
            string res = "initiated,maintained";
            DateTime latestevdt = LatestEVDT(code, "", res, SearchDepth.SearchDefault);

            if (latestevdt == DateTime.MinValue) return;

            //trigger for 4 hours
            DateTime dt7am = latestevdt.Date.AddHours(7);
            DateTime dt7PM = latestevdt.Date.AddHours(19);

            // 4/8 8pm
            //    4/8 7am
            //    4/8 7pm
            //
            //
            if (dt7am > latestevdt)
            {
                enddt = dt7am;
                evdt = dt7am.AddHours(-12);
            }
            else if (dt7am <= latestevdt && latestevdt < dt7PM)
            {
                evdt = dt7am;
                enddt = dt7PM;
            }
            else if (dt7PM <= latestevdt)
            {
                evdt = dt7PM;
                enddt = dt7PM.AddHours(12);
            }

            AddSimpleProc(2, evdt, enddt);
            DisableChartItem(code, latestevdt);
    }


    private void AddSitter(string code, DateTime initdt, DateTime evdt)
        {
            string desc = "TC: Direct Observer";
            string res = "Initiated";
            DateTime timestmp = DateTime.Now;
            short seq = 0;
            int unitid = _pat.unit_id;

            using (var db = PFSDBUtility.NewSqlConnection())
            {
                string q = "INSERT INTO chart_item (encounter_id,event_datetime,code,description,result,timestamp,sequence,unit_id,procedure_start)";
                q += " select @encid, @evdt, @code, @desc, @res, @ts,@seq,@unit,@procstart where not exists";
                q += " (select encounter_id,code,event_datetime from chart_item where encounter_id=" + _pat.encounter_id.ToString() + " and code='" + code + "' and event_datetime=" + PFSDBUtility.SQLDateTime(evdt) + ")";
                //string q = "INSERT INTO chart_item (encounter_id,event_datetime,code,description,timestamp,sequence,unit_id,order_id) values (@encid, @evdt, @code, @desc, @ts,@seq,@unit,@oid)";
                SqlCommand cmd = new SqlCommand(q, db);
                cmd.Parameters.AddWithValue("@encid", _pat.encounter_id);
                cmd.Parameters.AddWithValue("@evdt", evdt);
                cmd.Parameters.AddWithValue("@code", code);
                cmd.Parameters.AddWithValue("@desc", desc);
                cmd.Parameters.AddWithValue("@res", res);
                cmd.Parameters.AddWithValue("@ts", timestmp);
                cmd.Parameters.AddWithValue("@seq", seq);
                cmd.Parameters.AddWithValue("@unit", unitid);
                cmd.Parameters.AddWithValue("@procstart", initdt);
                cmd.ExecuteNonQuery();
                db.Close();
            } //using db

        }

        private int GetSitterType(DateTime evdt)
        { // get the sitter type RN or non-RN from the charting that should exist
            //at the same time as the initiation of the observer 1540100298
            int pnum = 0;
            string return_result = "";
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);    // add custom time range below
            query = query.Where(e => e.CODE == "1540100298");
            query = query.Where(e => e.EVENT_DATETIME == evdt);
            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
                if (return_result.Trim().ToUpper().StartsWith("RN")) pnum = 1;
                if (return_result.Trim().ToUpper().StartsWith("NON-RN")) pnum = 2;
            }
            if (pnum == 0)
            {
                Program.VerboseAudit("Sitter type RN or non-RN not found. Defaulting to non_RN.");
                pnum = 2;
            }
            return pnum;
        }

        private void CheckActivity(int actnum,string actcode, string actst, string actlencode)
        {
            int actlen;
            string actsthhmm = "0000";
        
            var query = StartNewQuery(SearchDepth.SearchSince24Hrs);    // add custom time range below
            query = query.Where(e => e.CODE == actcode);
            query = query.Where(e => e.ORDER_STATUS == null || e.ORDER_STATUS == ""); // need to update order_status=x
            query = query.OrderBy(e => e.EVENT_DATETIME);
            Program.VerboseAudit("for code=" + actcode + " count=" + query.Count());
            foreach (var item in query)
            {
                if (item.RESULT.ToLower() == "yes")
                { //09500000
                    var q2 = StartNewQuery(SearchDepth.SearchSince24Hrs);    // add custom time range below
                    q2 = q2.Where(e => e.CODE == actst);
                    q2 = q2.Where(e => e.EVENT_DATETIME == item.EVENT_DATETIME);
                    Program.VerboseAudit("for code=" + actst + " count=" + q2.Count());
                    if (q2.Count() == 0) return;

                    actsthhmm = q2.First().RESULT;
                    Program.VerboseAudit("actsthhmm=" + actsthhmm);
                    if (actsthhmm.Length >= 4)
                        actsthhmm = actsthhmm.Substring(0, 4);
                    Program.VerboseAudit("actsthhmm=" + actsthhmm);

                    var q3 = StartNewQuery(SearchDepth.SearchSince24Hrs);    // add custom time range below
                    q3 = q3.Where(e => e.CODE == actlencode);
                    q3 = q3.Where(e => e.EVENT_DATETIME == item.EVENT_DATETIME);
                    Program.VerboseAudit("for code=" + actlencode + " count=" + q3.Count());
                    if (q3.Count() == 0) return;

                    actlen = (int)q3.First().RESULT.Val();
                    DateTime stdt = DateTime.MinValue;
                    if (actlen >= 60)
                        {
                        TimeSpan start = new TimeSpan(0, 0, 0); //0 o'clock
                        TimeSpan end = new TimeSpan(6, 0, 0); // 6 o'clock
                        TimeSpan item_ts = item.EVENT_DATETIME.TimeOfDay;
                        TimeSpan actst_ts = new TimeSpan((int)actsthhmm.Substring(0, 2).Val(), (int)actsthhmm.Substring(2, 2).Val(), 0);
                        stdt = item.EVENT_DATETIME.Date + actst_ts;
                        if ((item_ts >= start) && (item_ts <= end))
                        {
                            if (actst_ts > item_ts) // then it belongs to prev cal day.
                                stdt = item.EVENT_DATETIME.Date.AddDays(-1) + actst_ts;
                        }
//                      DateTime stdt = item.EVENT_DATETIME.Date.AddHours(actsthhmm.Substring(0, 2).Val()).AddMinutes(actsthhmm.Substring(2, 2).Val());
                        Program.VerboseAudit("stdt=" + stdt);
                        AddSimpleProc(actnum, stdt, stdt.AddMinutes(actlen));
                        NullifyActivity(actcode, item.EVENT_DATETIME);
                        }
                }
            }

        }

        private void NullifyActivity(string actcode, DateTime evdt)
        {
            var db = PFSDBUtility.NewSqlConnection();
            string q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and event_datetime=" + PFSDBUtility.SQLDateTime(evdt) + " and code='" + actcode + "'";
            SqlCommand cmd = new SqlCommand(q, db);
            cmd.ExecuteNonQuery();
            db.Close();
        }

        private DateTime NextFinish(DateTime startdt)
        {
            DateTime dt = DateTime.MinValue;

            int b = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, startdt) / (4 * 60));
            dt = _pat.pull_start.AddMinutes((b + 1) * 240);
            Program.VerboseAudit("NextFinish: startdt=" + startdt.ToString() + " b=" + b + " dt=" + dt.ToString());
            return dt;
        }


        private bool ProcExists(int pnum, DateTime startdt, DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && (proc.PROCEDURE_DATETIME == startdt)
                            && (proc.DEPARTURE_DATETIME == enddt)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new { proc.PROCEDURE_EVENT_ID };
            return (query.Count() > 0);
        }


        private void CheckProc_3()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A3. Off unit accompanied by RN");
            Program.VerboseAudit("---------------");

            string res = "Off unit accompanied by RN";
            ProcessProc(3, 1, res);
            ProcessProc(3, 2, res);

        }
        private void CheckProc_4()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A4. Off unit accompanied by Non-RN");
            Program.VerboseAudit("---------------");

            string res = "Off unit accompanied by non-RN";
            ProcessProc(4, 1, res);
            ProcessProc(4, 2, res);

        }

        private void CheckProc_5()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A5. Patient/family education by RN");
            Program.VerboseAudit("---------------");

            string res = "Patient/family education by RN";
            string desc = "SNCH ED minutes spent teaching NU";
            DateTime evdt = DateTime.MinValue;
            var query = StartNewQuery(SearchDepth.SearchSince9Hrs);
            query = query.Where(e => e.DESCRIPTION.ToLower().StartsWith(desc.ToLower()));
            query = query.Where(e => e.ORDER_CONTROL != "X");
            query = query.OrderBy(e => e.EVENT_DATETIME);
            if (query.Count() == 0) return;
            evdt = query.First().EVENT_DATETIME;

            int ssum = 0;
            Program.VerboseAudit("qct: " + query.Count() + " ssum="+ssum);
            foreach (var e in query)
            {
                ssum = (int)e.RESULT.Val();
                Program.VerboseAudit("educ mins=" + e.RESULT);
                if (ssum >= 60)
                    AddSimpleProc(5, evdt, evdt.AddMinutes(ssum));
            }

        }

    private void CheckProc_6()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A6. Extensive wound management by RN");
            Program.VerboseAudit("---------------");

            string res = "Extensive wound management by RN";
            ProcessProc(6, 1, res);
            ProcessProc(6, 2, res);

        }

        private void CheckProc_7()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A7. Extensive wound management by non-RN");
            Program.VerboseAudit("---------------");

            string res = "Extensive wound management by non-RN";
            ProcessProc(7, 1, res);
            ProcessProc(7, 2, res);

        }

        private void CheckProc_8()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A8. Coordination of care by RN");
            Program.VerboseAudit("---------------");

            string res = "Discharge coordination";
            ProcessProc(8, 1, res);
            ProcessProc(8, 2, res);

            res = "Outside facility transfer coordination";
            ProcessProc(8, 1, res);
            ProcessProc(8, 2, res);

            res = "Interdisciplinary care conference/coordination";
            ProcessProc(8, 1, res);
            ProcessProc(8, 2, res);

        }

        private void CheckProc_9()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A9. 1:1 RN at bedside");
            Program.VerboseAudit("---------------");

            //Mappings for Activity 9:    Jul 2022
            //a.Code = AS peritoneal type with result = CAPD, CCPD
            //b.Code = CVC Insertion Procedure Note
            //Result like · Catheter Type: central venous catheter
            //c.Code = AS iabp insertion site first time found, trigger activity 9 and never again.

            //"1:1 by RN at the bedside";

            string code = "AS peritoneal type";
            string res = "CAPD,CCPD";
            DateTime latestevdt = LatestEVDT(code, "", res, SearchDepth.SearchDefault);
            if (latestevdt > DateTime.MinValue)
            {
                AddSimpleProc(9, latestevdt, latestevdt.AddHours(1));
                DisableChartItem(code, latestevdt);
            }


            code = "CVC Insertion Procedure Note";
            res = "central venous catheter";
            latestevdt = LatestEVDT(code, "", res, SearchDepth.SearchDefault);
            if (latestevdt > DateTime.MinValue)
            {
                AddSimpleProc(9, latestevdt, latestevdt.AddHours(1));
                DisableChartItem(code, latestevdt);
            }
            code = "AS iabp insertion site";
            res = "";
            latestevdt = LatestEVDT(code, "", res, SearchDepth.SearchDefault);
            if (latestevdt > DateTime.MinValue)
            {
                AddSimpleProc(9, latestevdt, latestevdt.AddHours(1));
                DisableChartItem(code, latestevdt);
            }

        }

        private void CheckProc_10()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A10. 1:1 non-RN at bedside");
            Program.VerboseAudit("---------------");

            string res = "1:1 by non-RN at the bedside";
            ProcessProc(10, 1, res);
            ProcessProc(10, 2, res);

        }

        private void CheckProc_11()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A11. 2:1 by RN at bedside");
            Program.VerboseAudit("---------------");

            string res = "2:1 by RN at the bedside";
            ProcessProc(11, 1, res);
            ProcessProc(11, 2, res);

        }

        private void OutputClassAndDNC(bool do_class)
        {
            int i;
            if (numclassdnc == 0 && do_class)
                OutputClass(DateTime.MinValue, DateTime.MinValue);
            else
            {
                for (i = 1; i <= numclassdnc; i++)
                {
                    if (aryclassdnc[i].retain)
                        OutputDNC(aryclassdnc[i].startdt, aryclassdnc[i].enddt);
                    else // do_class will never be false here
                        OutputClass(aryclassdnc[i].startdt, aryclassdnc[i].enddt);
                }
            }
        }

        //        private const string DATETIME_FORMAT = "yyyyMMddHHmm";              // ISO Date/Time w/o seconds
        private const string DATETIME_FORMAT = "yyyyMMddHHmm";              // ISO Date/Time w/o seconds

        private void OutputClass(DateTime dncstdt,DateTime dncendt)
        {
            string outstr, ind_list, desc, str_pull_dt, str_in_dt, str_out_dt = "";
            int i, tc_event_id;
            bool repeat_output = false;
            string repeat_time = "";
            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         212 242 304
            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1
            //1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901 3456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
            //1       |10 5            |                |                |        |10023121779         |ALEXANDER                       |GLORIA                          |                                |0508    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289530  |           |201403030400                                                                      |NNYNNNNYYNNYNNNYNNNYYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
            //1       |10 5            |                |                |        |10024544908         |ARMSTRONG                       |AARON                           |                                |0535    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289531  |           |201403030400                                                                      |NYNNYNNNNNNYNNNYNNNNYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
            //|0547    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289532  |           |201403030400                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN
            //|0547    |A   |201403030700|                |201403030700    |18  |C|    |3040|1440|18289532  |           |201403030700                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//            1RO | RO MB5BG / 6E |                |                |        | 2000180316769 | PARISIEN | GREYSON | LAWRENCE | RMB5514 | P | 20190319030000 |                               | 20 | C |    | 5399 | 480 | 62040560 |           | 20190319030000 | 20190319070000 | NNNYNNNNNNNNYNNNYNYNYNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN

            if (Program.g_is_test)
                tc_event_id = 9999;
            else
                tc_event_id = PFSDBUtility.NextGID();                         //get a unique id for this class
                                                                              //            str_pull_dt = _pat.pull_finish.ToString(DATETIME_FORMAT);
                                                                              //str_pull_dt = _pat.pull_finish.ToString(DATETIME_FORMAT);
                                                                              //if (_pat.unit_departure != null && _pat.unit_departure != DateTime.MinValue)
            if (dncstdt == DateTime.MinValue)
            {
                //                if (_pat.effective_out == Program.g_pull_finish && _pat.unit_departure == DateTime.MinValue)
                Program.VerboseAudit("Loc arrtime=" + loc_arrtime + " gpull_start_save=" + Program.g_pull_start_save);
                if (loc_arrtime >= Program.g_pull_start_save.AddHours(4))
                {
                    //str_in_dt = Program.g_effdt.ToString(DATETIME_FORMAT);
                    str_in_dt = loc_arrtime.ToString(DATETIME_FORMAT);
                    str_out_dt = loc_out.ToString(DATETIME_FORMAT);

                    if (loc_arrtime <= Program.g_effdt)
                    {
                        repeat_output = true;//also make the desired class time
                        repeat_time = Program.g_effdt.ToString(DATETIME_FORMAT);
                    }
                }
                else
                {
                    str_in_dt = Program.g_effdt.ToString(DATETIME_FORMAT);
                    str_out_dt = "";// _pat.effective_out.ToString(DATETIME_FORMAT);
                    str_out_dt = loc_out.ToString(DATETIME_FORMAT);
                }
            }
            else //dnc
            {
                str_in_dt = dncstdt.ToString(DATETIME_FORMAT);
                str_out_dt = dncendt.ToString(DATETIME_FORMAT);
            }
            str_out_dt = "";//leave open-ended for night ASSIGNMENT MODULE 4/14/21

            //outstr = _pat.facilty_code.FixedWidth(8);       //(facility code)
            outstr = "".FixedWidth(8);                       //(facility code)
            outstr += "|" + _pat.unit_name.FixedWidth(16);
            outstr += "|" + "".FixedWidth(16);                               //(unit code)
            outstr += "|" + txarea.FixedWidth(16);                               //(area code)
            outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
            outstr += "|" + _pat.acct.FixedWidth(20);
            outstr += "|" + _pat.last_name.FixedWidth(32);
            outstr += "|" + _pat.first_name.FixedWidth(32);
            outstr += "|" + _pat.middle_name.FixedWidth(32);
            outstr += "|" + _pat.room.FixedWidth(8);
            outstr += "|" + _pat.bed.FixedWidth(4);
            outstr += "|" + str_in_dt;        //CLASS dt
            outstr += "|" + "".FixedWidth(14);                               //(login)
            outstr = outstr.FixedWidth(232);
            outstr += "|" + _pat.effective_out.ToString(DATETIME_FORMAT);//str_out_dt;        //TC Data End Point
            outstr = outstr.FixedWidth(249);
            outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
            outstr += "|" + "C".FixedWidth(1);                               //record type = class
            outstr += "|" + "".FixedWidth(4);                                //(stage)
            outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
            outstr += "|" + _pat.range.ToString().FixedWidth(4);             //TC pull range
            outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
            outstr += "|";
            outstr = outstr.FixedWidth(294);
            outstr += "|" + str_in_dt;        //IN
            outstr = outstr.FixedWidth(346);
            //if (_pat.unit_departure != null && _pat.unit_departure != DateTime.MinValue)
                //                outstr += "|" + _pat.unit_departure.ToString(DATETIME_FORMAT);        //OUT
                outstr += "|" + str_out_dt;        //OUT
            outstr = outstr.FixedWidth(377);
            outstr += "|";

            //if (use_default)
            //{ //make all is_checked = false and then mark defaults
            //    Program.VerboseAudit("Patient Will receive default indicators " + _pat.default_inds_str);
            //    for (i = 1; (i <= MAX_INDS); i++)
            //    {
            //        _inds[i].is_checked = false;
            //    }
            //    foreach (var ind in _pat.default_inds)
            //    {
            //        if (ind <= _inds.GetUpperBound(0))
            //        {
            //            _inds[ind].is_checked = true;
            //        }
            //    }
            //}

            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++) {
                if (_inds[i].is_checked) {
                    outstr += "Y";
                    ind_list += "" + i;
                } else {
                    outstr += "N";
                }
            } // next i
            ind_list = ind_list.Substring(1);                           //strip leading comma
                                                                        //                                                                                                   1                                                                                                   2                                                                                                   3
                                                                        //         1         2         3         4         5         6         7         8         9         0         1         2         3         4         5         6         7         8         9         0         1         2         3         4         5         6         7         8         9         0         1         2         3         4         5         6         7         8         9         0
                                                                        //1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
                                                                        //1       |DO6D            |                |                |        |2000192224892       |BEHNAM                          |KENDRA                          |LEE                             |RDO6311 |P   |20180717110000|                               |20  |C|    |5399|480 |56103278  |           |20180717110000                                     |                              |YNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
            string str5am;
            Program.outfile.WriteLine(outstr);      //1900yesterd output to transparent.txt
            if (repeat_output) // with repeat_time
            {
                string repeat_line = outstr.Substring(0, 203) + repeat_time + outstr.Substring(215, 80) + repeat_time + " ".Repeat(71) + outstr.Substring(378, 120);
                Program.outfile.WriteLine(repeat_line);
            }
            //if (Program.g_make5am)
            //{
            //    //if (str_out_dt.Substring(8, 4) == "0500") //create the 7am at 3am
            //    {
            //        string strdttm = Program.g_pull_finish.ToString(DATETIME_FORMAT);
            //        strdttm = strdttm.Substring(0, 8) + "0500";
            //        //str5am = outstr.Substring(0, 203) + Program.g_pull_finish.ToString(DATETIME_FORMAT) + outstr.Substring(217, 78) + Program.g_pull_finish.ToString(DATETIME_FORMAT) + " ".Repeat(69) + outstr.Substring(378, 120);
            //        str5am = outstr.Substring(0, 203) + strdttm + outstr.Substring(215, 80) + strdttm + " ".Repeat(71) + outstr.Substring(378, 120);
            //        Program.outfile2.WriteLine(str5am);
            //    }
            //}

            Program.Audit("");
            desc = "Classified: " + ind_list;
            if (Program.g_is_test) {
                Program.Audit(desc);
            } else {
                //Save the selected indicators plus both Program.Audits in the event log; link with tc_event_id
                PFSEventLog.AddTransparentMappingEventLogEntry(
                    desc, Program.gLogUnitID, Program.gLogEncounterID,
                    tc_event_id, Program.gLogMapperVersion,
                    Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
            }
        }

        private void OutputClassAndDNCTimes()
        {
            int i;
            DateTime dt1 = Program.g_effdt;
            numclassdnc = 0;
            DateTime lastend = Program.g_effdt;
            // retain has nothing to do with keeping the record. it only means "is DNC".

            for (i = 1; i <= numloa; i++)
            {
                //Why are these historicals stubbed off?? let it go through 10/09/20
                //if (aryloa[i].startdt < dt1) 
                //{
                //    Program.VerboseAudit("historical aryloa i:" + i + " aryloa[].startdt=" + aryloa[i].startdt + " aryloa[].enddt=" + aryloa[i].enddt + " dt1="+dt1);
                //}
                //else
                {
                    if (numclassdnc == 0)
                    {
                        if (aryloa[i].startdt > dt1)
                        {
                            numclassdnc++;
                            aryclassdnc[numclassdnc].startdt = dt1;
                            aryclassdnc[numclassdnc].enddt = aryloa[i].startdt;
                            aryclassdnc[numclassdnc].retain = false; //false=Class
                        }
                        numclassdnc++;
                        aryclassdnc[numclassdnc].startdt = aryloa[i].startdt;
                        aryclassdnc[numclassdnc].enddt = aryloa[i].enddt;
                        aryclassdnc[numclassdnc].retain = true; //true=DNC
                    }
                    else
                    {
                        numclassdnc++;
                        aryclassdnc[numclassdnc].startdt = aryloa[i - 1].enddt;
                        aryclassdnc[numclassdnc].enddt = aryloa[i].startdt;
                        aryclassdnc[numclassdnc].retain = false; //false=Class

                        numclassdnc++;
                        aryclassdnc[numclassdnc].startdt = aryloa[i].startdt;
                        aryclassdnc[numclassdnc].enddt = aryloa[i].enddt;
                        aryclassdnc[numclassdnc].retain = true; //true=DNC
                    }
                    lastend = aryloa[i].enddt;
                }
            } //for
            if (numclassdnc > 0 && lastend != dt1.AddHours(12)) 
                // then the loa ended before this draw period end
            {
                numclassdnc++;
                aryclassdnc[numclassdnc].startdt = lastend; // aryloa[i - 1].enddt;
                aryclassdnc[numclassdnc].enddt = dt1.AddHours(12);
                aryclassdnc[numclassdnc].retain = false; //false=Class
            }
            for (i=1;i<=numclassdnc;i++)
            {
                string classdnctype="";
                if (aryclassdnc[i].retain)
                    classdnctype = "DNC";
                else
                    classdnctype = "Class";
                Program.VerboseAudit(classdnctype + ": " + aryclassdnc[i].startdt + "-" + aryclassdnc[i].enddt);
            }
        }

        private void OutputDNC(DateTime dncstdt, DateTime dncendt)
        {
            string outstr, ind_list, desc, str_pull_dt, str_in_dt, str_out_dt = "";
            int i, tc_event_id;

            if (Program.g_is_test)
                tc_event_id = 9999;
            else
                tc_event_id = PFSDBUtility.NextGID();                         //get a unique id for this class

            //str_in_dt = aryloa[i].startdt.ToString(DATETIME_FORMAT);
            //str_out_dt = aryloa[i].enddt.ToString(DATETIME_FORMAT);
            str_in_dt = dncstdt.ToString(DATETIME_FORMAT);
            str_out_dt = dncendt.ToString(DATETIME_FORMAT);

            //outstr = _pat.facilty_code.FixedWidth(8);                       //(facility code)
            outstr = "".FixedWidth(8);                       //(facility code)
            outstr += "|" + _pat.unit_name.FixedWidth(16);
            outstr += "|" + "".FixedWidth(16);                               //(unit code)
            outstr += "|" + "".FixedWidth(16);                               //(area code)
            outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
            outstr += "|" + _pat.acct.FixedWidth(20);
            outstr += "|" + _pat.last_name.FixedWidth(32);
            outstr += "|" + _pat.first_name.FixedWidth(32);
            outstr += "|" + _pat.middle_name.FixedWidth(32);
            outstr += "|" + _pat.room.FixedWidth(8);
            outstr += "|" + _pat.bed.FixedWidth(4);
            outstr += "|" + str_in_dt;        //CLASS dt
            outstr += "|" + "".FixedWidth(14);                               //(login)
            outstr = outstr.FixedWidth(232);
            outstr += "|" + str_out_dt;  // TC END POINT_pat.effective_out.ToString(DATETIME_FORMAT);//TC Data End Point
            outstr = outstr.FixedWidth(249);
            outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
            outstr += "|" + "D".FixedWidth(1);                               //record type = class
            outstr += "|" + "".FixedWidth(4);                                //(stage)
            outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
            outstr += "|" + "".FixedWidth(4);             //TC pull range
            outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
            outstr += "|";
            outstr = outstr.FixedWidth(294);
            outstr += "|" + str_in_dt;        //IN
            outstr = outstr.FixedWidth(346);
            outstr += "|" + str_out_dt;        //OUT
            outstr = outstr.FixedWidth(377);
            outstr += "|NNNNY";

            Program.outfile.WriteLine(outstr);      //1900yesterd output to transparent.txt
            Program.Audit("");
            desc = "Classified DoNotClassify: NNNNY";
                //Save the selected indicators plus both Program.Audits in the event log; link with tc_event_id
                PFSEventLog.AddTransparentMappingEventLogEntry(
                    desc, Program.gLogUnitID, Program.gLogEncounterID,
                    tc_event_id, Program.gLogMapperVersion,
                    Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());

        }


        private int DeterminePtypeOfIndicators()
        {
            int i;
            var pscore = 0.0;
            int pt_type = 6;
            var indlist = new List<int>();
            string indstr = "";

            Program.VerboseAudit("Determine patient type of triggered indicators");

            for (i = 1; (i <= MAX_INDS); i++)
            {
                if (_inds[i].is_checked)
                {
                    indlist.Add(i);
                    indstr += i.ToString() + ",";
                    pscore += _inds[i].weight;
                }
            }

            Program.VerboseAudit("indicators=" + indstr + " score=" + pscore.ToString());

            var db = PFSDBUtility.NewPfsDataContext();
            var query_ptype = from ptype in db.PATIENT_TYPEs
                              where (ptype.METHODOLOGY_ID == _pat.meth_id)
                              orderby ptype.PATIENT_TYPE1
                              select new
                              {
                                  ptype.PATIENT_TYPE1,
                                  ptype.POINTS_HIVAL
                              };

            foreach (var ptypes in query_ptype)
            {
                //                Program.DebugTrace("type,hival=" + ptypes.PATIENT_TYPE1.ToString() + "" + ptypes.POINTS_HIVAL.ToString(), "");
                if (pscore <= ptypes.POINTS_HIVAL)
                {
                    if (pt_type > ptypes.PATIENT_TYPE1)
                    {
                        pt_type = ptypes.PATIENT_TYPE1;
                    }
                }
            }
            Program.VerboseAudit("patient type=" + pt_type.ToString());

            return pt_type;

        }


        //private bool ExistDefaultInPast16hrs()
        //{
        //    DateTime cdt1 = DateTime.MinValue;
        //    DateTime cdt2 = DateTime.MinValue;
        //    int cnt_all = 0;
        //    int cnt_def = 0;
        //    //get max class date of last non-default class = 1
        //    //get max class date of last default = 2
        //    // if 1 >= 2 then false
        //    // else
        //    //   if 2 > 1 and date is <= 16 hrs ago then true
        //    //   else false
        //    var db = PFSDBUtility.NewPfsDataContext();
        //    //var query = from ce in db.CLASSIFICATION_EVENTs
        //    //            where (ce.ENCOUNTER_ID == _pat.encounter_id)
        //    //            && (ce.CLASSIFIED_BY_ID != -2)
        //    //            && (System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(ce.CLASSIFICATION_DATETIME, Program.g_pull_start) <= 960)
        //    //            select ce;
        //    //cnt_all = query.Count();
        //    //if (cnt_all > 0)
        //    //{
        //    //    cdt1 = PFSUtility.DBToDateTime(query.Max(x => x.CLASSIFICATION_DATETIME));
        //    //    Program.VerboseAudit("Last regular classification was at:"+cdt1.ToString());
        //    //}
        //    //else {
        //    //    Program.VerboseAudit("No regular classifications within the past 16 hours");
        //    //}

        //    var query = from ce in db.CLASSIFICATION_EVENTs
        //                where (ce.ENCOUNTER_ID == _pat.encounter_id)
        //                && (ce.CLASSIFIED_BY_ID == -2)
        //                && (System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(ce.CLASSIFICATION_DATETIME, Program.g_pull_finish) <= 960)
        //                select ce;
        //    cnt_def = query.Count();

        //    if (cnt_def > 0)
        //    {
        //        cdt2 = PFSDBUtility.DBToDateTime(query.Max(x => x.CLASSIFICATION_DATETIME));
        //        Program.VerboseAudit("Last default classification was at:" + cdt2.ToString());
        //    }
        //    else
        //    {
        //        Program.VerboseAudit("No default classifications within the past 16 hours");
        //    }
        //    return (cnt_def > 0);

        //}

        private void OutputProcs()
        {
            int i;
            string outstr, proc_list, desc;
            int tc_event_id;

            foreach (var proc in _procs)
            {
                if (Program.g_is_test)
                    tc_event_id = 9999;
                else
                    tc_event_id = PFSDBUtility.NextGID();                         //get a unique id for this proc

                outstr = "".FixedWidth(8);                       //(facility code)
                outstr += "|" + _pat.unit_name.FixedWidth(16);
                outstr += "|" + "".FixedWidth(16);                               //(unit code)
                outstr += "|" + txarea.FixedWidth(16);                               //(area code)
                outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
                outstr += "|" + _pat.acct.FixedWidth(20);
                outstr += "|" + _pat.last_name.FixedWidth(32);
                outstr += "|" + _pat.first_name.FixedWidth(32);
                outstr += "|" + _pat.middle_name.FixedWidth(32);
                outstr += "|" + _pat.room.FixedWidth(8);
                outstr += "|" + _pat.bed.FixedWidth(4);
                outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //204 proc dt
                outstr += "|" + "".FixedWidth(14);                               //(login)
                outstr = outstr.FixedWidth(249);
                outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
                outstr += "|" + "P".FixedWidth(1);                               //record type = class
                outstr += "|" + "".FixedWidth(4);                                //(stage)
                outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
                outstr += "|" + _pat.range.ToString().FixedWidth(4);             //TC pull range
                outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
                outstr += "|";
                outstr = outstr.FixedWidth(294);
                outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //296 procdt in
                outstr = outstr.FixedWidth(346);
                //if (_pat.unit_departure != null && _pat.unit_departure != DateTime.MinValue)
                //                outstr += "|" + _pat.unit_departure.ToString(DATETIME_FORMAT);        //OUT
                outstr += "|" + proc.finish.ToString(DATETIME_FORMAT);          //348 procdt out
                outstr = outstr.FixedWidth(377);
                outstr += "|";

                proc_list = "";
                for (i = 1; (i < MAX_PROCS); i++)
                {
                    if (proc.procedure_number == i)
                    {
                        outstr += "Y";
                        proc_list += "" + i;
                    }
                    else
                    {
                        outstr += "N";
                    }
                } // next i
                proc_list = proc_list.Substring(1);                             //strip leading comma

                Program.outfile.WriteLine(outstr);                              //output to transparent.txt

                desc = "Activities: " + proc_list;
                if (Program.g_is_test)
                {
                    Program.Audit(desc);
                }
                else
                {
                    //Save the selected procedures plus both Program.Audits in the event log; link with tc_event_id
                    //(this assumes the procedure Program.Audit is mixed in with the class Program.Audit)
                    PFSEventLog.AddTransparentMappingEventLogEntry(desc, Program.gLogUnitID, Program.gLogEncounterID,
                        tc_event_id, Program.gLogMapperVersion,
                        Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
                }
            } // next proc
        }

    }
}


