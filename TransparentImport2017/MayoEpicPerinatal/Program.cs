﻿//
//   OBX|||ADT10^PLACE IN ED OBSERVATION STATUS^C4|||||||||||20180110134541-0600
//  Above is signal for transition to ED inpatient
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;                // for SqlConnection
using System.IO;                            // for Directory, Path, Stream
using System.Windows.Forms;                 // for Application (also add reference)
using PfsShared;                            // add a reference to Shared2 project
//
// Transparent Mapping main program.
// NOTE: Each unit must set the "use transparent" flag.
//
// sample: -efftime=0700 -effdate=yesterday -pulltime=0700 -pulldate=today -range=1440
//
// This is designed to be run as a background job, so never stop for user input -- that means no error MsgBox.
// All communication is done through log files and the event log; it is OK to print to the console.
//
// In order to work with the AcuityPlus Patient Selection and Transparent import:
//
//   * The program name must be TransparentMapping.exe, placed in AcuityPlus\bin
//   * The audit file is called TransparentAudit.log, placed in AcuityPlus\log
//   * The output file is Transparent.txt, placed in AcuityPlus\load_me
//   * Events are saved in the database event log
//
namespace TransparentMapping
{
    public class LocationData
    {
        public DateTime time_in;
        public DateTime time_out;
        public int unit_id;
        public int meth_id;
        public int range;
        public bool toremove;
        public string txarea;
        public string uname;
    }

    public class ClassificationData
    {
        public DateTime time_in;
        public DateTime time_out;
        public int stage;
        public string short_name;
        public int unit_id;
        public string uname;
        public bool toremove;
    }

    public class PatientInfo
    {
        public int      encounter_id;
        public string   last_name;
        public string   first_name;
        public string   middle_name;
        public string   acct;
        public double   age;                    // age (in years) at admission
        public double gestational_age;
        public DateTime dob;
        public string   room;
        public string   bed;
        public DateTime unit_arrival;
        public DateTime unit_departure;
        public DateTime effective;              // patient specific (may be > g_effdt)
        public DateTime pull_start;             // patient specific (may be > g_pull_start)
        public DateTime pull_finish;            // patient specific (may be < g_pull_finish)
        public int      range;                  // patient specific (may be < g_range) - minutes
        public double   los_hours;              // hours during pull
        public int      TC_source_id;           // TCP port (chart source)
        // unit info
        public string   facilty_code;           // class import facility code
        public string   unit_name;
        public int      unit_id;
        public bool     is_ED;
        public bool     is_ICU;
        public int      meth_id;
        public int      stage;
        public DateTime classtime;
        public string adt_unit_name;
        public int num_locs;
        public LocationData[] locary = new LocationData[20];
        public int num_class;
        public ClassificationData[] classary = new ClassificationData[10];
        public bool readyforEDclass;
        public int classified_methid;
        public DateTime lastclass_start;
        public DateTime eff_departuredt;
        public DateTime procev1_dt;
        //public List<int> default_inds;          // unit admission profile indicators for recent arrivals
        //public string default_inds_str;         // "1,6,8"
        public void InitLocArray()
        {
            for (int i = 0; i < locary.Length; ++i)
                locary[i] = new LocationData();
        }
        public void InitClassArray()
        {
            for (int i = 0; i < classary.Length; ++i)
                classary[i] = new ClassificationData();
        }
    }
    //public struct med_data
    //{
    //    public string code;
    //    public string descript;
    //    public string drugclass;
    //    public DateTime evdt;
    //}
    //public class EDchild
    //{
    //    public int parent_unitid;
    //    public int meth_id;
    //    public int child_unitid;
    //    public string child_name;
    //}


    public static class Program
    {

        const string    MAPPER_VERSION = "1.00";
        const string    TRANSP_FILENAME = "Transparent.txt";    // THE output file (for class import)
        const string    TRANSP_AUDIT_LOG = "TransparentAudit.log";  // Legacy debug/audit log
        const int       CHART_ITEM_LIFE = 730;                   // days in the chart_item table
        const int       DEFAULT_RANGE = 1440;                   // 24 hrs in minutes; override with -range
        const int MAX_LOCS = 40;

        public static bool g_abort;                         // stop!
        public static bool g_debug;                         // output to console?
        public static bool g_log;                           // output to log file? (legacy feature)
        public static bool g_no_output;                     // audit file only?
        public static bool g_is_test;                       // output to file w/dummy ids and no event log
        public static bool g_no_delete;                     // keep old chart items?
        public static bool g_stop_on_error;
        public static bool g_import_when_done;

        public static DateTime      g_effdt;                // effective datetime

        public static StreamWriter  outfile;                // Transparent.txt
        public static StreamWriter  logfile;                // TransparentLog.txt

        public static int           gLogUnitID;
        public static int           gLogEncounterID;
        public static string        gLogSourceText;
        public static string        gLogMapperVersion;

        public static StringBuilder gBriefAudit;            // the complete brief audit
        public static StringBuilder gVerboseAudit;          // the complete verbose audit

        public static DateTime      g_pull_start;           // global pull start  (not limited by patient values)
        public static DateTime      g_pull_finish;          // global pull finish
        public static int           g_range;                // global range
        public static DateTime      g_pull_finish_q4;

        private static string   _this_acct;                 // acct filter
        private static string   _this_unit_name;            // unit name filter
        private static int      _this_unit_id;              // unit id filter
        private static int      _lookback= 0;                // number of days to look back

        static string           _import_path;
        static string           _log_path;
        public static PatientInfo _pat;

        // Chart_Item data being processed
        private static int _encounter_id;          // (int is 32 bit so this is OK)
        private static int _sequence;              // sequence# for dummy codes
        private static DateTime _start_dt = DateTime.Now.AddHours(-24);
        private static DateTime _finish_dt = DateTime.Now;
        private static string _treatment_area;
        private static ENCOUNTER_LOCATION[] _locations;    // for unit_id lookup
        private static string _category;
        private static string _fndcode;
        private static string _descript;
        private static string _group;
        private static string _result;
        private static string _perfdate;
        private static int seq = 0; // DateTime.Now.Hour * 10;
        private static int _delcidays = 0;
        //public static List<EDchild> EDchildren;


        static void Main(string[] args)
        {
            try
            {
                InitGlobals();

                ParseCommandLine(args);
                SetMapperVersion();

                gLogSourceText = String.Join(" ", args);           // add command line to log
                LogInfo("Begin mapping and translation", PFSEventLog.EventLogCategory.EVENT_CATEGORY_STARTUP_SHUTDOWN);
                gLogSourceText = "";

                OpenOutputFiles();
                ProcessPatients();

                DeleteOldChartItems();

                LogInfo("Mapping complete", PFSEventLog.EventLogCategory.EVENT_CATEGORY_STARTUP_SHUTDOWN);

                //MaybeRunImport();
                CloseOutputFiles();
            }
            catch (Exception e)
            {
                Console.WriteLine("Unexpected Error: {0}", e.Message);
                Console.WriteLine("{0}", e.StackTrace);
                LogUnexpectedError(e.Message, e.StackTrace);
            }

            if (g_debug)
            {
                Console.WriteLine(Environment.NewLine);
                Console.Write("Press any key...");
                Console.ReadKey();
            }
        }

        static void InitGlobals()
        {
            gLogSourceText = "";
            gBriefAudit = new StringBuilder();
            gVerboseAudit = new StringBuilder();
            _pat = new PatientInfo();
            _pat.InitLocArray();
            _pat.InitClassArray();
        }

        static void ParseCommandLine(string[] args)
        {
            string nowdate, nowtime;
            string value;
            string effdate, efftime;
            string pulldate, pulltime;

            var nowdt = DateTime.Now;
            nowdate = nowdt.ToString("yyyyMMdd");
            nowtime = nowdt.ToString("HHmm");
            effdate = "";
            efftime = "";
            pulldate = "";
            pulltime = "";

            _import_path = PFSUtility.DefaultImportPath();          // ...\load_me
            _log_path = PFSUtility.DefaultLogPath();                // ...\log

            foreach (var arg in args)
            {
                var arr = arg.Split('=');
                value = (arr.GetUpperBound(0) > 0) ? arr[1] : "";

                switch (arr[0])
                {
                    case "-acct":                               // process this patient only
                        _this_acct = value;
                        break;
                        
                    case "-debug":                              // output to console and pause at end
                        g_debug = true;
                        break;

                    case "-effdate":                            // effective date: yyyymmdd
                        if (value == "yesterday")
                            effdate = DateTime.Now.AddDays(-1).ToString("yyyyMMdd");
                        else
                            effdate = value.Left(8);
                        break;
                    
                    case "-efftime":                            // effective time: HHMM or HH
                        efftime = value.Left(4);
                        efftime = efftime.PadRight(4,'0');
                        break;
                    
                    case "-import":                             // run transparent import when done
                        g_import_when_done = true;
                        break;

                    case "-log":                                // create TransparentAudit.log
                        g_log = true;                           // (verbose audit)
                        break;

                    case "-path":                               // override default import path
                        _import_path = value;
                        break;

                    case "-logpath":                               // override default import path
                        _log_path = value;
                        break;
                    case "-n":                                  // no output to transparent.txt 
                        g_no_output = true;                     // (audit only)
                        break;

                    case "-nodelete":                           // do not delete old chart items
                        g_no_delete = true;
                        break;

                    case "-pulldate":                           // pull date: yyyymmdd
                        if (value == "today")
                            pulldate = DateTime.Now.ToString("yyyyMMdd");
                        else
                            pulldate = value.Left(8);
                        break;
                    
                    case "-pulltime":                           // pull time: HHMM or HH
                        pulltime = value.Left(4);
                        pulltime = pulltime.PadRight(4,'0');
                        break;

                    case "-range":                              // range in minutes; default = 1440 = 24hr
                        g_range = value.ToInteger();
                        break;
                        
                    case "-stoponerror":
                        g_stop_on_error = true;
                        break;

                    case "-test":
                        g_is_test = true;
                        break;

                    case "-unit":                               // process this unit only
                        _this_unit_name = value;
                        break;
                    case "-unit_id":                            // process this unit only
                        _this_unit_id = value.ToInteger();
                        break;
                    case "-lookback":
                        _lookback = Math.Abs(value.ToInteger());
                        break;

                    case "-delci":
                        _delcidays = value.ToInteger();
                        break;
                        
                    default:
                        Console.WriteLine("unexpected argument: {0}", arg);
                        break;
                }
            }
            //pulltime = "0700";
            //efftime = "0700";
            if (String.IsNullOrEmpty(effdate))
                effdate = nowdate;
            if (String.IsNullOrEmpty(efftime))
                efftime = nowtime;

            // Note: pulldate defaults to effdate, not nowdate
            if (String.IsNullOrEmpty(pulldate))
                pulldate = effdate;
            if (String.IsNullOrEmpty(pulltime))
                pulltime = "0700";

            if (g_range == 0)
                g_range = DEFAULT_RANGE;

            DebugTrace("Import path=", _import_path);
            DebugTrace("Log path=", _log_path);
            DebugTrace("effdate={0}", effdate);
            DebugTrace("efftime={0}", efftime);
            DebugTrace("pulldate={0}", pulldate);
            DebugTrace("pulltime={0}", pulltime);
            DebugTrace("range={0}", g_range);

            // class IN time
            g_effdt = PFSUtility.ISOToDateTime(effdate + efftime);
            // range for chart item queries
            g_pull_finish_q4 = PFSUtility.ISOToDateTime(pulldate + pulltime);

            g_pull_finish = PFSUtility.ISOToDateTime(pulldate + "0700");

            //Make adjustment if this is before 7am: 10/16 00:50 (=actual time) vs 10/16 07:00
            if (g_pull_finish > g_pull_finish_q4)
                g_pull_finish = g_pull_finish.AddDays(-1); //adjust it to 10/15 07:00
            //No adjustment time is after 7am: 10/16 08:50 (=actual time) vs 10/16 07:00 = OK

            g_pull_start = g_pull_finish.AddMinutes(-g_range);

        }

        static void OpenOutputFiles()
        {
            // Append to existing file
            outfile = new StreamWriter(Path.Combine(_import_path, TRANSP_FILENAME), true);
            
            if (g_log) {
                // re-write new file
                logfile = new StreamWriter(Path.Combine(_log_path, DateTime.Now.ToString("yyyyMMddHHmmss") + TRANSP_AUDIT_LOG));
            }
        }

        static void CloseOutputFiles()
        {
            outfile.Close();
            
            if (logfile != null) {
                logfile.Close();
                logfile = null;
            }
        }

        static void SetMapperVersion()
        {
            gLogMapperVersion = MAPPER_VERSION;
        }


//UNITS TO CONSIDER FOR TC
//parent units:
//select* from unit where is_ed='y' and is_active = 'y' and parent_unit_id is null and USE_TRANSPARENT_CLASSIFICATION = 'y'

//select* from unit where PARENT_UNIT_ID=57012483 

//select* from unit_param where unit_id in (57013039,57013040) and getdate() between EFFECTIVE_DATETIME and EXPIRATION_DATETIME

//select* from METHODOLOGY 24=visit 25=inpt

        static void ProcessPatients()
        {
            const int LOOKBACK_DAYS = 10;                   // 24 hrs in minutes; override with -range
            string sql, audit_header;
            int count, prev_unit_id, prev_enc_id = 0;
            DateTime effdtin;
            DateTime effdtout;
            //parent_id = 2817
            //'Prod env:
            //vunit_id = 43729784
            //iunit_id = 43730689
            // Make a list of all patients with chart items during the pull period.
            //
            // Include only units that have the "use transparent" flag set.
            // Limit to one patient if -acct is given.  Limit to one unit with -unit.
            // Left join encounter_location - the patient may be discharged at pull time.
            // Look for the latest patient arrival to the unit before the pull time; ignore transfers within the unit.
            // Look for a patient departure (if any) before the pull time; ignore transfers within the unit.
            sql =
             "select p.last_name,p.first_name,e.acct_number,e.age_at_admission,el.encounter_id \n"
            + ",u.unit_id,u.name,f.CLASSIFICATION_FACILITY_CODE,el.ADT_UNIT_NAME,el.DATETIME_IN,el.DATETIME_OUT as UNIT_DEPARTURE,el.effective_datetime_out,el.room,el.bed,ce.methodology_id \n"
            + ",p.gestational_age,ce.stage,ce.datetime_in as classtime,e.effective_departure,p.dob,procev.procedure_datetime \n"
            + "from encounter_location as el with (nolock)\n"
            + "inner join unit as u on (el.UNIT_ID = u.UNIT_ID) \n"
            + "inner join unit_param as up on (u.UNIT_ID = up.UNIT_ID) \n"
            + "inner join facility as f on (f.FACILITY_ID = u.FACILITY_ID) \n"
            + "inner join ENCOUNTER as e with (nolock) on (el.ENCOUNTER_ID = e.ENCOUNTER_ID)\n"
            + "inner join PERSON as p on (e.person_id = p.PERSON_ID) \n"
            + "left join  CLASSIFICATION_EVENT as ce on (ce.ENCOUNTER_ID = e.ENCOUNTER_ID) and (ce.methodology_id=27 or ce.methodology_id=19)\n"
            + "left join (select pe.encounter_id, pe.procedure_datetime from procedure_event as pe\n"
            + " inner join PROCEDURE_ANSWER as pa on (pe.procedure_event_id = pa.procedure_event_id)\n"
            + " where pa.PROCEDURE_NUMBER=1) AS PROCEV ON (PROCEV.encounter_id = e.encounter_id)\n"
            //            + "where f.classification_facility_code = '1ro' and u.is_active = 'y' \n"
            + "where u.is_active = 'y' \n"
            + "and (up.methodology_id=27 or up.methodology_id=19)\n"
            + "and getdate() between up.effective_datetime and up.expiration_datetime";
//            + "and (el.working_unit_id in (4049742, 19351)) \n";
//            + "AND el.DATETIME_IN >=" + PFSDBUtility.SQLDateTime(g_pull_finish.AddDays(-LOOKBACK_DAYS)) + " \n"
//            + "and (ce.METHODOLOGY_ID is null or ce.METHODOLOGY_ID = 27) \n";
            //19351   1   RO EI30/ 32  Obstetrics   THIS IS POST PARTUM
            //4049742 1   RO EI33 Family Birth Center
            //+ "(el.datetime_out is null or el.datetime_in>=dateadd(d,-3," + PFSDBUtility.SQLDateTime(g_pull_start) + ")) and (ce.METHODOLOGY_ID is null)\n";
            if (String.IsNullOrEmpty(_this_acct))
            {
                if (_lookback > 0)
                    sql += " and (el.datetime_in>=dateadd(d,-" + _lookback.ToString() + "," + PFSDBUtility.SQLDateTime(g_pull_finish_q4) + "))\n";
                else
                {
                    sql += " and ((el.datetime_out is null and el.datetime_in<=dateadd(hour,-4," + PFSDBUtility.SQLDateTime(g_pull_finish_q4) + "))\n";
                    sql += "    or (el.datetime_out is not null and el.datetime_out>=dateadd(d,-1," + PFSDBUtility.SQLDateTime(g_pull_start) + ")))\n";
                }
            }

            if (!String.IsNullOrEmpty(_this_acct)) {
                sql += " AND E.ACCT_NUMBER=" + PFSDBUtility.SQLString(_this_acct) + "\n";
            }
            if (!String.IsNullOrEmpty(_this_unit_name)) {
                sql += " AND U.NAME=" + PFSDBUtility.SQLString(_this_unit_name) + "\n";
            }
            if (_this_unit_id > 0) {
                sql += " AND UNIT.UNIT_ID=" + _this_unit_id + "\n";
            }

            //sql += " ORDER BY E.ACCT_NUMBER, QUERY_TYPE, MAXDATETIME_OUT desc, MAXDATETIME_IN desc, U.NAME, P.LAST_NAME, P.FIRST_NAME\n";
            sql += " ORDER BY E.ACCT_NUMBER,classtime desc,CE.STAGE desc,U.NAME,P.LAST_NAME,P.FIRST_NAME\n";

            VerboseAudit(sql);
            var db = PFSDBUtility.NewSqlConnection(120);
            var cmd = new SqlCommand(sql, db);
            SqlDataReader dr = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);

        //
        // Process all patients
        //
        count = 0;
            prev_unit_id = 0;
            var default_inds = new List<int>();
            var default_inds_str = "";
            int _queryType = 0;

            while (dr.Read())
            {

                // Package the patient info
                // NOTE: ''dr["FIRST_NAME"] as string'' looks great but it will save nulls.
                //       use ''PFSDBUtility.DBToString(dr["FIRST_NAME"])'' to convert to empty strings.
                var pat = new PatientInfo();
                //_queryType = PFSDBUtility.DBToInt(dr["QUERY_TYPE"]);
                pat.encounter_id = PFSDBUtility.DBToInt(dr["ENCOUNTER_ID"]);
                if (pat.encounter_id != prev_enc_id)
                {
                    VerboseAudit("Query Type =" + _queryType);
                    prev_enc_id = pat.encounter_id;
                    count++;
                    Console.Write("\rProcessing patient {0}", count);
                    pat.InitLocArray();
                    pat.InitClassArray();
                   pat.adt_unit_name = PFSDBUtility.DBToString(dr["ADT_UNIT_NAME"]);
                     pat.unit_id = PFSDBUtility.DBToInt(dr["UNIT_ID"]);
                     pat.encounter_id = PFSDBUtility.DBToInt(dr["ENCOUNTER_ID"]);
                     pat.meth_id = 27;// PFSDBUtility.DBToInt(dr["METHODOLOGY_ID"]);
                     pat.acct = PFSDBUtility.DBToString(dr["ACCT_NUMBER"]);
                     pat.last_name = PFSDBUtility.DBToString(dr["LAST_NAME"]);
                     pat.first_name = PFSDBUtility.DBToString(dr["FIRST_NAME"]);
                     pat.middle_name = "";// PFSDBUtility.DBToString(dr["MIDDLE_NAME"]);
                     pat.facilty_code = PFSDBUtility.DBToString(dr["CLASSIFICATION_FACILITY_CODE"]);
                     pat.unit_name = PFSDBUtility.DBToString(dr["NAME"]);
                     pat.room = PFSDBUtility.DBToString(dr["ROOM"]);
                     pat.bed = PFSDBUtility.DBToString(dr["BED"]);
                     pat.age = PFSDBUtility.DBToDouble(dr["AGE_AT_ADMISSION"]);
                    pat.gestational_age = PFSDBUtility.DBToDouble(dr["GESTATIONAL_AGE"]);
                    pat.dob = PFSDBUtility.DBToDateTime(dr["DOB"]);
                    pat.classified_methid=PFSDBUtility.DBToInt(dr["METHODOLOGY_ID"]);
                    pat.stage = PFSDBUtility.DBToInt(dr["STAGE"]);
                    pat.classtime = PFSDBUtility.DBToDateTime(dr["CLASSTIME"]);
                    pat.eff_departuredt= PFSDBUtility.DBToDateTime(dr["EFFECTIVE_DEPARTURE"]);
                    pat.procev1_dt = PFSDBUtility.DBToDateTime(dr["PROCEDURE_DATETIME"]);
                    VerboseAudit("pat.eff_departuredt=" + pat.eff_departuredt);
                    //        pat.unit_arrival = PFSDBUtility.DBToDateTime(dr["MINDATETIME_IN"]);
                    VerboseAudit("pat.classified_methid=" + pat.classified_methid);
                    //pat.unit_departure = PFSDBUtility.DBToDateTime(dr["UNIT_DEPARTURE"]);
                    //    pat.unit_departure = PFSDBUtility.DBToDateTime(dr["MAXDATETIME_OUT"]);
                    //                VerboseAudit("pat.unit_departure=MAXDATETIME_OUT=" + pat.unit_departure);
                    // pat.effective = PFSUtility.MaxDateTime(g_effdt, pat.unit_arrival);
                    //pat.pull_start = PFSUtility.MaxDateTime(g_pull_start, pat.unit_arrival);
                    //pat.pull_start = pat.unit_arrival;
                    //pat.pull_finish = PFSUtility.MinDateTime(g_pull_finish, pat.unit_departure);
                    //pat.pull_finish = pat.unit_departure;
                    //               if (pat.pull_finish < pat.pull_start)
                    //               {
                    //                   pat.pull_finish = g_pull_finish;
                    //               }
                    //             pat.range = (int)PFSUtility.DateDiffInMinutes(pat.unit_arrival, pat.unit_departure);
                    //             pat.los_hours = pat.range / 60.0;
                    //pat.TC_source_id = PFSUtility.DBToInt(dr["TC_SOURCE_ID"]);        //TCP port
                    //pat.is_ED = PFSUtility.DBToBool(dr["IS_ED"]);
                    //pat.is_ICU = PFSUtility.DBToBool(dr["IS_ICU"]);

                    // Get the list of default indicators on admission (if any)
                    //if (pat.unit_id != prev_unit_id) {
                    //    default_inds = GetUnitDefaultIndicators(pat.unit_id, g_pull_start, out default_inds_str);
                    //    prev_unit_id = pat.unit_id;
                    //}
                    //pat.default_inds = default_inds;
                    //pat.default_inds_str = default_inds_str;

                    // Get ready for event log entries for this patient
                    gLogUnitID = pat.unit_id;
                    gLogEncounterID = pat.encounter_id;
                    gLogSourceText = "";

                    //// Reset both audit strings and make headers for both
                    gBriefAudit = new StringBuilder();
                    gVerboseAudit = new StringBuilder();
                    audit_header =
                        new String('=', 80) + Environment.NewLine +
                        "TRANSPARENT MAPPING AUDIT FILE                    Run Time=" + DateTime.Now + Environment.NewLine +
                        "Version:   " + gLogMapperVersion + Environment.NewLine +
                        "Pull:      " + g_pull_finish_q4 + Environment.NewLine +
                        "Effective: " + g_effdt + Environment.NewLine +
                        "Range:     " + g_range + " (" + Math.Round(g_range / 60.0, 1) + " hrs) starting " + g_pull_start;

                    //// This will add to both brief and verbose audits
                    Audit(audit_header);
                    Audit(new String('=', 80));
                    Audit("Patient " + pat.last_name + ", " + pat.first_name + " " + pat.middle_name +
                        " in " + pat.unit_name + " " + pat.room + " " + pat.bed +
                        " acct=" + pat.acct + " age=" + Math.Round(pat.age, 2) + " stage="+pat.stage+" class time="+pat.classtime);
                    _pat = pat;

                    //DateTime evdt = pat.effective.AddHours(24);// DateTime.MinValue;// GetEDObsStart();
                    bool readyforPericlass = GetLocationHistory(out effdtin, out effdtout); //sets ready for ed class
                    if (!readyforPericlass) VerboseAudit("Patient not ready for classification: Not yet 24 hours LOS.");
                    if (readyforPericlass)
                        {
                        GetClassHistory();
                        Audit(pat.acct.FixedWidth(16) + "/" + pat.last_name.FixedWidth(16) + "/" + pat.first_name.FixedWidth(16) + "/" + pat.unit_name.FixedWidth(16) + "/" + pat.meth_id);
                            //Process this patient
                            //Catch any unexpected errors and continue with next patient.
                            try
                            {
                                // Run the appropriate mapping for this unit and date
                                //
                                //GetPreArrivalItems();

                                switch (pat.meth_id)
                                {
                                    case 27:
                                        var peri = new PeriClass();
                                    //if (evdt != DateTime.MinValue && _queryType != 9)
                                    //{
                                    //pat.unit_departure = evdt;
                                    pat.pull_start = effdtin;
                                        pat.pull_finish = effdtout; // pat.unit_departure;
                                    pat.unit_arrival = pat.pull_start;
//dont do this because need to see out time>today in order to adjust in staging     if (pat.pull_finish > DateTime.Now) pat.pull_finish = DateTime.Now;
                                    pat.unit_departure = pat.pull_finish;
                                    pat.range = (int)PFSUtility.DateDiffInMinutes(pat.unit_arrival, pat.unit_departure);
                                    pat.los_hours = pat.range / 60.0;
                                    Audit("Here from " + pat.pull_start + " to " + pat.pull_finish +
                                        "  (LOS=" + Math.Round(pat.los_hours, 2) + ")");
                                    Audit("pat.unit_departure = " + pat.unit_departure.ToString());
                                    //Audit("REVISED LOS based on Observation Start Time...");
                                    //Audit("Here from " + pat.pull_start + " to " + pat.pull_finish +
                                    //    "  (LOS=" + Math.Round(pat.los_hours, 2) + ")");
                                    _pat = pat;
                                    //}
                                    //if (_queryType == 9)
                                    //{
                                    //     pat.unit_arrival = GetMREDArrival(pat.unit_departure);
                                    //    VerboseAudit("pat.unit_departure=" + pat.unit_departure + " ==> pat.unit_arrival=" + pat.unit_arrival);
                                    //    pat.effective = PFSUtility.MaxDateTime(g_effdt, pat.unit_arrival);
                                    //    pat.pull_start = pat.unit_arrival;
                                    //    VerboseAudit("Query Type 99999");
                                    //    pat.range = (int)PFSUtility.DateDiffInMinutes(pat.unit_arrival, pat.unit_departure);
                                    //    pat.los_hours = pat.range / 60.0;
                                    //    Audit("REVISED LOS due to Second ED Stay...");
                                    //    Audit("Here from " + pat.pull_start + " to " + pat.pull_finish +
                                    //        "  (LOS=" + Math.Round(pat.los_hours, 2) + ")");
                                    //    _pat = pat;
                                    //}
                                    if (pat.unit_arrival != DateTime.MinValue)
                                    {
                                        peri.ProcessPatient(pat);
                                        if ((_pat.stage == 6 || _pat.stage == 9)
                                            && (_pat.lastclass_start < g_effdt.AddDays(-1)))
                                        {
                                            ////pat.pull_start = pat.pull_finish.AddHours(-24);
                                            //pat.classified_methid = 27;
                                            //pat.stage = _pat.stage;//crucial to set in ProcessPat.
                                            //VerboseAudit("Reiterating for stage=:" + pat.stage + " pat.pull_finish=" + pat.pull_finish + " for " + g_effdt);
                                            //peri.ProcessPatient(pat);
                                        }
                                    }
                                        //if (evdt != DateTime.MinValue && _queryType != 9) // only process EDInpt if Obs time != mindt
                                        //{
                                        //    pat.meth_id = 25;
                                        //    pat.unit_arrival = evdt;
                                        //    pat.unit_departure = PFSDBUtility.DBToDateTime(dr["MAXDATETIME_OUT"]);
                                        //    pat.effective = PFSUtility.MaxDateTime(g_effdt, pat.unit_arrival);
                                        //    pat.pull_start = pat.unit_arrival;
                                        //    pat.pull_finish = pat.unit_departure;
                                        //    if (pat.pull_finish < pat.pull_start)
                                        //    {
                                        //        pat.pull_finish = g_pull_finish;
                                        //    }
                                        //    pat.range = (int)PFSUtility.DateDiffInMinutes(pat.unit_arrival, pat.unit_departure);
                                        //    pat.los_hours = pat.range / 60.0;
                                        //    gBriefAudit = new StringBuilder();
                                        //    gVerboseAudit = new StringBuilder();
                                        //    audit_header =
                                        //        new String('=', 80) + Environment.NewLine +
                                        //        "TRANSPARENT MAPPING AUDIT FILE                    Run Time=" + DateTime.Now + Environment.NewLine +
                                        //        "Version:   " + gLogMapperVersion + Environment.NewLine +
                                        //        "Pull:      " + g_pull_finish + Environment.NewLine +
                                        //        "Effective: " + g_effdt + Environment.NewLine +
                                        //        "Range:     " + g_range + " (" + Math.Round(g_range / 60.0, 1) + " hrs) starting " + g_pull_start;

                                        //    // This will add to both brief and verbose audits
                                        //    Audit(audit_header);
                                        //    Audit(new String('=', 80));
                                        //    Audit("Patient " + pat.last_name + ", " + pat.first_name + " " + pat.middle_name +
                                        //        " in " + pat.unit_name + " " + pat.room + " " + pat.bed +
                                        //        " acct=" + pat.acct + " age=" + Math.Round(pat.age, 2));
                                        //    Audit("Here from " + pat.pull_start + " to " + pat.pull_finish +
                                        //        "  (LOS=" + Math.Round(pat.los_hours, 2) + ")");
                                        //    _pat = pat;
                                        //    ei.ProcessPatient(pat);
                                        //}
                                        break;
                                }

                                gLogUnitID = 0;
                                gLogEncounterID = 0;
                            }
                            catch (Exception e)
                            {
                                LogUnexpectedError(e.Message, e.StackTrace);
                                // Stop or keep going?
                                if (Program.g_stop_on_error) g_abort = true;
                            }
                    } //ready for Peri class
                }  //encid
                if (g_abort) break;
            } //while

            Console.WriteLine();
            dr.Close();
            
            if (count < 1) {
                Audit("");
                if(!String.IsNullOrEmpty(_this_acct)) {
                    LogWarning("The selected patient has no chart items in the given time range");
                } else {
                    LogWarning("No chart items found to process - have the unit(s) been enabled for transparent classification?");
                }
            }   
        }

        static bool GetLocationHistory(out DateTime indt, out DateTime outdt)
        {
            string sql;
            int prev_unit_id=-99;
            DateTime firstindt = DateTime.MinValue;
            DateTime lastoutdt = DateTime.MinValue;
            bool readyforPericlass = false;
            bool first = true;
            bool all_fbc_locs_have_dtout = true;
            bool pp_loc_is_ok = true;
            int pplocnum = 0;
            //    'Gets all the EDParent locations and puts them into an array in the pat record.
            sql = "select u.unit_id,u.name as NAME,el.effective_datetime_in as EFFECTIVE_DATETIME_IN,el.effective_datetime_out,el.room from encounter_location as el";
            sql += " inner join encounter as e on (e.encounter_id=el.encounter_id)";
            sql += " inner join unit as u on (el.unit_id=u.unit_id)";
            sql += " inner join unit_param as up on (u.unit_id=up.unit_id)";
            sql += " where e.acct_number=" + PFSDBUtility.SQLString(_pat.acct);
            //sql += " and (u.unit_id=19351 or u.unit_id=4049742)";
//            sql += " and (u.name='RO EI33' or u.name='RO EI30/32')";
            //sql += " and u.use_transparent_classification='y' and u.is_active='y'";
            sql += " and u.is_active='y'";
            sql += " and (up.methodology_id=27 or up.methodology_id=19)";
            sql += " and el.effective_datetime_in<='" + g_pull_finish_q4 + "'";
            sql += " and getdate() between up.effective_datetime and up.expiration_datetime";
            sql += " order by el.effective_datetime_in";
            VerboseAudit("getloc:" + sql);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd2 = new SqlCommand(sql, db2);
            SqlDataReader dr2 = cmd2.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            _pat.num_locs = 0;
            while (dr2.Read())
            {
                if (_pat.num_locs > MAX_LOCS)
                    VerboseAudit("GetLocationHistory num locations exceeds " + MAX_LOCS);
                else
                    _pat.num_locs++;
                _pat.locary[_pat.num_locs].time_in = PFSDBUtility.DBToDateTime(dr2["EFFECTIVE_DATETIME_IN"]);
                _pat.locary[_pat.num_locs].time_out = PFSDBUtility.DBToDateTime(dr2["EFFECTIVE_DATETIME_OUT"]);
                _pat.locary[_pat.num_locs].unit_id = PFSDBUtility.DBToInt(dr2["UNIT_ID"]);
                _pat.locary[_pat.num_locs].uname = PFSDBUtility.DBToString(dr2["NAME"]);
                _pat.locary[_pat.num_locs].toremove = true;
                if (_pat.num_locs > 1)
                {
                    if (_pat.locary[_pat.num_locs].unit_id == prev_unit_id 
                        && _pat.locary[_pat.num_locs-1].time_out == _pat.locary[_pat.num_locs].time_in)
                    {
                        _pat.num_locs--;
                        _pat.locary[_pat.num_locs].time_out = PFSDBUtility.DBToDateTime(dr2["EFFECTIVE_DATETIME_OUT"]);
                    }
                    else
                        prev_unit_id = _pat.locary[_pat.num_locs].unit_id;
                }
                if (first)
                {
                    first = false;
                    firstindt = _pat.locary[_pat.num_locs].time_in;
                    prev_unit_id = _pat.locary[_pat.num_locs].unit_id;
                }
                //19351   1   RO EI30/ 32  Obstetrics   THIS IS POST PARTUM
                //4049742 1   RO EI33 Family Birth Center 
                bool unitispp = (_pat.locary[_pat.num_locs].unit_id == 19351);
                if (unitispp) pplocnum++;
                VerboseAudit("_pat.locary["+_pat.num_locs+"].unit_id=" + _pat.locary[_pat.num_locs].unit_id + " timein="+ _pat.locary[_pat.num_locs].time_in + " timeout="+ _pat.locary[_pat.num_locs].time_out);
                if (!unitispp)
                {
                    if (_pat.num_locs == 1) //look at the earliest location
                    {
                        all_fbc_locs_have_dtout &= (_pat.locary[_pat.num_locs].time_in.AddHours(24) <= g_pull_finish_q4);
                        VerboseAudit("_pat.num_locs=1 _pat.locary[_pat.num_locs].time_in.AddHours(24)=" + _pat.locary[_pat.num_locs].time_in.AddHours(24) + " g_pull_finish_q4=" + g_pull_finish_q4);
                        //all_fbc_locs_have_dtout &= (_pat.locary[_pat.num_locs].time_out < _pat.locary[_pat.num_locs].time_in.AddYears(40));
                    }
                }
                else if (_pat.locary[_pat.num_locs].time_out == _pat.locary[_pat.num_locs].time_in.AddYears(40))
                    if (pplocnum == 1)
                    {
                        pp_loc_is_ok &= (_pat.locary[_pat.num_locs].time_in.AddHours(24) <= g_pull_finish_q4);
                        VerboseAudit("pplocnum=1 _pat.locary[_pat.num_locs].time_in.AddHours(24)=" + _pat.locary[_pat.num_locs].time_in.AddHours(24) + " g_pull_finish_q4=" + g_pull_finish_q4);
                    }
                lastoutdt = _pat.locary[_pat.num_locs].time_out;
            } //dr read
            dr2.Close();
            indt = firstindt;
            if (lastoutdt > g_pull_finish_q4.AddDays(1)) //DateTime.Now.AddDays(1))
                lastoutdt = g_pull_finish_q4;
            outdt = lastoutdt;
            if ((pplocnum >= 1) && pp_loc_is_ok)
                readyforPericlass = true;
            else if (all_fbc_locs_have_dtout)
                readyforPericlass = true;
            //readyforPericlass = (all_fbc_locs_have_dtout && (pplocnum <= 0)  || (pplocnum >= 1) && pp_loc_is_ok);
            VerboseAudit("all_fbc_locs_have_dtout=" + (all_fbc_locs_have_dtout?"true":"false") + " pplocnum=" + pplocnum + " pp_loc_is_ok=" + (pp_loc_is_ok?"true":"false"));
            return readyforPericlass;
        }

        static bool GetClassHistory()
        {
            int prev_stage = 0;
            int curr_stage = 0;
            string sql;
            // For the purpose of seeing where there were non-perinatal units.
            sql = "select u.unit_id,u.name,ce.stage,s.short_name,ce.effective_datetime_in,ce.effective_datetime_out from classification_event as ce";
            sql += " inner join unit as u on (ce.unit_id=u.unit_id)";
            sql += " inner join stage as s on (s.stage=ce.stage) and (s.METHODOLOGY_ID=ce.METHODOLOGY_ID)";
            sql += " where ce.encounter_id=" + _pat.encounter_id;
            sql += " and u.is_active='y'";
            sql += " and (ce.methodology_id=27 or ce.methodology_id=19)";
            sql += " order by ce.effective_datetime_in";
            VerboseAudit("getclass:" + sql);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd2 = new SqlCommand(sql, db2);
            SqlDataReader dr2 = cmd2.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            _pat.num_class = 0;
            int num_class_printed = 0;
            while (dr2.Read())
            {
                curr_stage = PFSDBUtility.DBToInt(dr2["STAGE"]);
                if (_pat.num_class > 10)
                    VerboseAudit("GetClassificationHistory num locations exceeds " + 10);
                else
                    _pat.num_class++;
                if (prev_stage == curr_stage)
                {
                    _pat.num_class--;
                    _pat.classary[_pat.num_class].time_out = PFSDBUtility.DBToDateTime(dr2["EFFECTIVE_DATETIME_OUT"]);
                }
                else
                {
                    _pat.classary[_pat.num_class].time_in = PFSDBUtility.DBToDateTime(dr2["EFFECTIVE_DATETIME_IN"]);
                    _pat.classary[_pat.num_class].time_out = PFSDBUtility.DBToDateTime(dr2["EFFECTIVE_DATETIME_OUT"]);
                    _pat.classary[_pat.num_class].stage = curr_stage;
                    _pat.classary[_pat.num_class].short_name = PFSDBUtility.DBToString(dr2["SHORT_NAME"]);
                    _pat.classary[_pat.num_class].unit_id = PFSDBUtility.DBToInt(dr2["UNIT_ID"]);
                    _pat.classary[_pat.num_class].uname = PFSDBUtility.DBToString(dr2["NAME"]);
                    _pat.classary[_pat.num_class].toremove = false;
                    prev_stage = curr_stage;
                }
                if (_pat.num_class != num_class_printed)
                {
                    VerboseAudit("_pat.classary[" + _pat.num_class + "].stage=" + _pat.classary[_pat.num_class].short_name + " timein=" + _pat.locary[_pat.num_class].time_in + " timeout=" + _pat.locary[_pat.num_class].time_out);
                    num_class_printed = _pat.num_class;
                }

            } //dr read
            dr2.Close();
            return true;
        }
        //static DateTime GetEDObsStart()
        //{
        //    string sql;
        //    DateTime evdt = DateTime.MinValue;

        //    sql = "select max(ci.event_datetime) as EFFECTIVE_DATETIME";
        //    sql += " from chart_item as ci";
        //    sql += " inner join encounter as e on (e.encounter_id=ci.encounter_id)";
        //    sql += " where e.acct_number=" + PFSDBUtility.SQLString(_pat.acct);
        //    sql += " and ci.code=" + PFSDBUtility.SQLString("ADT10");
        //    var db2 = PFSDBUtility.NewSqlConnection();
        //    var cmd2 = new SqlCommand(sql, db2);
        //    SqlDataReader dr2 = cmd2.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
        //    while (dr2.Read())
        //    {
        //        evdt = PFSDBUtility.DBToDateTime(dr2["EFFECTIVE_DATETIME"]);
        //    } //dr read
        //    dr2.Close();
        //    VerboseAudit("ED Obs Start Time=" + evdt.ToString());
        //    return evdt;
        //}

        //static DateTime GetMREDArrival(DateTime depdt)
        //{
        //    string sql;
        //    DateTime dt = DateTime.MinValue;
        //    DateTime arrdt = DateTime.MinValue;
        //    int uid = 0;
        //    bool done = false;

        //    sql = "select el.EFFECTIVE_DATETIME_IN,el.unit_id";
        //    sql += " from ENCOUNTER_LOCATION as el";
        //    sql += " inner join encounter as e on (e.encounter_id=el.encounter_id)";
        //    sql += " inner join unit as u on (el.unit_id=u.unit_id)";
        //    sql += " where e.acct_number=" + PFSDBUtility.SQLString(_pat.acct);
        //    sql += " and el.EFFECTIVE_DATETIME_OUT <= " + PFSDBUtility.SQLDateTime(depdt);
        //    sql += " and u.is_ed='y' and u.use_transparent_classification='y' and u.is_active='y'";
        //    sql += " order by el.EFFECTIVE_DATETIME_IN desc";
        //    VerboseAudit(sql);
        //    var db2 = PFSDBUtility.NewSqlConnection();
        //    var cmd2 = new SqlCommand(sql, db2);
        //    SqlDataReader dr2 = cmd2.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
        //    while (dr2.Read() && !done)
        //    {
        //        uid = PFSDBUtility.DBToInt(dr2["UNIT_ID"]);
        //        dt = PFSDBUtility.DBToDateTime(dr2["EFFECTIVE_DATETIME_IN"]);
        //        //if (uid != 2817)
        //        //{
        //        //    done = true;
        //        //    arrdt = dt;
        //        //}
        //        done = true;
        //        arrdt = dt;
        //    } //dr read
        //    dr2.Close();
        //    VerboseAudit("Most Recent ED Arrival=" + arrdt.ToString());
        //    return arrdt;
        //}


        //    static bool GetPreArrivalItems()
        //    {
        //        string sql;
        //        string room;
        //        string dt;
        //        bool readyforEDclass = true;
        //        int car;

        //        //  Gets and saves all Chart Items that were sent pre-arrival before the A04.
        //        //  These are in the form of rejected R01 messages (rejected because pt not yet registered).
        //        //  Up to 2 hours before A04.

        //sql = "select timestamp,";
        //sql += " case when CHARINDEX('OBX|1', source_text) > 0 then";
        //sql += " ltrim(rtrim(substring(replace(";
        //sql += " cast(substring(source_text, CHARINDEX('OBX|1', source_text), 200) as nvarchar(max)), '|',";
        //sql += " replicate(cast(' ' as nvarchar(max)), 1000)), 3001, 100))) else null end as OBX1,";

        //sql += " case when CHARINDEX('OBX|1', source_text) > 0 then";
        //sql += " ltrim(rtrim(substring(replace(";
        //sql += " cast(substring(source_text, CHARINDEX('OBX|1', source_text), 200) as nvarchar(max)), '|',";
        //sql += " replicate(cast(' ' as nvarchar(max)), 1000)), 5001, 1000))) else null end as OBX1_5,";

        ////        sql = sql & "ltrim(rtrim(substring(replace("
        ////sql = sql & "cast(substring(source_text, CHARINDEX('PV1|', source_text),200) as nvarchar(max)),'|',"
        ////sql = sql & "replicate(cast(' ' as nvarchar(max)),1000)),44001, 1000))) as PV144"


        //        sql += " case when CHARINDEX('OBX|2', source_text) > 0 then";
        //sql += " ltrim(rtrim(substring(replace(";
        //sql += " cast(substring(source_text, CHARINDEX('OBX|2', source_text), 200) as nvarchar(max)), '|',";
        //sql += " replicate(cast(' ' as nvarchar(max)), 1000)), 3001, 100))) else null end as OBX2,";

        //        sql += " case when CHARINDEX('OBX|2', source_text) > 0 then";
        //        sql += " ltrim(rtrim(substring(replace(";
        //        sql += " cast(substring(source_text, CHARINDEX('OBX|2', source_text), 200) as nvarchar(max)), '|',";
        //        sql += " replicate(cast(' ' as nvarchar(max)), 1000)), 5001, 1000))) else null end as OBX2_5";

        //        sql += " from EVENT_LOG where TIMESTAMP between dateadd(hour,-2," + PFSDBUtility.SQLDateTime(_pat.unit_arrival) + ") and " + PFSDBUtility.SQLDateTime(_pat.unit_arrival);
        //sql += " and (description like 'R01%' or description like 'O01%') and description like '%" + _pat.acct + "%'";
        //sql += " and event_source = 1 and event_type = 4 and event_category = 4";
        //        Program.VerboseAudit(sql);
        //        var db2 = PFSDBUtility.NewSqlConnection();
        //        var cmd2 = new SqlCommand(sql, db2);
        //        SqlDataReader dr2 = cmd2.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
        //        var pfs = PFSDBUtility.NewPfsDataContext();
        //        while (dr2.Read())
        //        {
        //            _descript = "";
        //            if (dr2["OBX1"] != null)
        //            {
        //                // ci.timestamp = PFSDBUtility.DBToDateTime(dr2["TIMESTAMP"]);
        //                // ci.event_datetime=ci.timestamp;
        //                _fndcode = PFSDBUtility.DBToString(dr2["OBX1"]);
        //                car = _fndcode.IndexOf('^', 0);
        //                if (car > 0)
        //                {
        //                    _fndcode = _fndcode.Left(car);
        //                    _result = PFSDBUtility.DBToString(dr2["OBX1_5"]);
        //                    _descript = _fndcode.Substring(car);
        //                    SaveChartItem(pfs);
        //                }
        //                // ci.sequence=seq++;
        //            }
        //            if (dr2["OBX2"] != null)
        //            {
        //                // ci.timestamp = PFSDBUtility.DBToDateTime(dr2["TIMESTAMP"]);
        //                // ci.event_datetime=ci.timestamp;
        //                _fndcode = PFSDBUtility.DBToString(dr2["OBX2"]);
        //                car = _fndcode.IndexOf('^', 0);
        //                if (car > 0)
        //                {
        //                    _fndcode = _fndcode.Left(car);
        //                    _result = PFSDBUtility.DBToString(dr2["OBX2_5"]);
        //                    _descript = _fndcode.Substring(car);
        //                    SaveChartItem(pfs);
        //                }
        //                // ci.sequence=seq++;
        //            }
        //        } //dr read
        //        dr2.Close();
        //        return readyforEDclass;
        //    }

        //    static void SaveChartItem(PfsDataContext pfs)
        //    {
        //        //DateTime pdate = PFSUtility.ISOToDateTime(_perfdate);
        //        int ct = 0;
        //        string sql = "select count(*) from chart_item as ci";
        //        sql += " inner join encounter as e on (e.encounter_id=ci.encounter_id)";
        //        sql += " where e.acct_number=" + PFSDBUtility.SQLString(_pat.acct);
        //        sql += " and ci.code=" + PFSDBUtility.SQLString(_fndcode);
        //        //Program.VerboseAudit(sql);
        //        var db2 = PFSDBUtility.NewSqlConnection();
        //        var cmd2 = new SqlCommand(sql, db2);
        //        SqlDataReader dr2 = cmd2.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
        //        while (dr2.Read())
        //        {
        //            ct = dr2.GetInt32(0);
        //        } //dr read
        //        dr2.Close();
        //        if (ct > 0) return;

        //        var ci = new CHART_ITEM();
        //        //string format = "yyyyMMddHHmm";
        //        //if (_perfdate.Length == 12)
        //        //    ci.EVENT_DATETIME = DateTime.ParseExact(_perfdate, format, System.Globalization.CultureInfo.InvariantCulture);
        //        //else
        //        ci.EVENT_DATETIME = _pat.unit_arrival;
        //        ci.ENCOUNTER_ID = _pat.encounter_id;
        //        ci.UNIT_ID = _pat.unit_id;
        //        ci.CODE = _fndcode;
        //        Program.VerboseAudit("adding code=" + _fndcode);
        //        //ci.CATEGORY = _category;
        //        ci.DESCRIPTION = _descript;
        //        //ci.FIELD_NAME = _group;
        //        if (_result == null) _result = "";
        //        ci.RESULT = _result;
        //        ci.TIMESTAMP = _pat.unit_arrival;
        //        ci.SEQUENCE = (short)seq++;
        //        //ci.SOURCE_TEXT = _sourcefn;

        //        pfs.CHART_ITEMs.InsertOnSubmit(ci);
        //        SubmitOverwrite(pfs);

        //    }

        static void SubmitOverwrite(PfsDataContext pfs)
        {
            bool moreToSubmit = true;
            do
            {
                try
                {
                    pfs.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
                    moreToSubmit = false;
                }
                catch (System.Data.Linq.ChangeConflictException)
                {
                    foreach (System.Data.Linq.ObjectChangeConflict occ in pfs.ChangeConflicts)
                    {
                        // All database values overwrite current values with 
                        //values from database
                        occ.Resolve(System.Data.Linq.RefreshMode.OverwriteCurrentValues);
                    }
                }
            }
            while (moreToSubmit);

        }

        //static List<int> GetUnitDefaultIndicators(int unit_id, DateTime pull_dt, out string default_inds_str) 
        //{
        //    var result = new List<int>();                   // make an empty list
        //    default_inds_str = "<none>";
        //    var db = PFSDBUtility.NewPfsDataContext();        // look for the unit default admission profile
        //    var query = from param in db.UNIT_PARAMs
        //                from profile in param.PATIENT_PROFILEs
        //                where (param.UNIT_ID == unit_id)
        //                && (pull_dt >= param.EFFECTIVE_DATETIME) && (pull_dt < param.EXPIRATION_DATETIME)
        //                && (profile.PROFILE_NUMBER == param.DEFAULT_ADMISSION_PROFILE)
        //                select new {
        //                    profile.INDICATORS              // comma-separated indicator list
        //                };
        //    foreach (var inds in query) {
        //        default_inds_str = inds.INDICATORS;
        //        string s = inds.INDICATORS;
        //        if (!String.IsNullOrEmpty(s)) {
        //            var arr = s.Split(',');
        //            foreach (var t in arr) {
        //                if (t.IsNumeric()) {                // add an indicator number to the list
        //                    result.Add(t.ToInteger());
        //                }
        //            }
        //        }
        //    }
        //    return result;
        //}


        //UNITS TO CONSIDER FOR TC
        //parent units:
        //select* from unit where is_ed='y' and is_active = 'y' and parent_unit_id is null and USE_TRANSPARENT_CLASSIFICATION = 'y'

        //select* from unit where PARENT_UNIT_ID=57012483 

        //select* from unit_param where unit_id in (57013039,57013040) and getdate() between EFFECTIVE_DATETIME and EXPIRATION_DATETIME

        //select* from METHODOLOGY 24=visit 25=inpt

//        static List<EDchild> GetEDChildren()
//        {
//            string sql;
////            EDchild childdata = new EDchild();

//            var childlist = new List<EDchild>();                   // make an empty list

//            sql = "select u1.unit_id as parentid,up.methodology_id,u2.unit_id as childid,u2.name as childname from unit as u1";
//            sql += " inner join unit as u2 on (u1.unit_id = u2.PARENT_UNIT_ID)";
//            sql += " inner join unit_param as up on (u2.unit_id = up.UNIT_ID)";
//            sql += " where u1.is_ed = 'y' and u1.is_active = 'y' and u1.parent_unit_id is null and u1.USE_TRANSPARENT_CLASSIFICATION = 'y'";
//            sql += " and u2.is_active = 'y' and u2.USE_TRANSPARENT_CLASSIFICATION = 'y'";
//            sql += " and getdate() between up.EFFECTIVE_DATETIME and up.EXPIRATION_DATETIME";
//            var db2 = PFSDBUtility.NewSqlConnection();
//            var cmd2 = new SqlCommand(sql, db2);
//            SqlDataReader dr2 = cmd2.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
//            while (dr2.Read())
//            {
//                EDchild childdata = new EDchild();
//                childdata.parent_unitid = PFSDBUtility.DBToInt(dr2["parentid"]);
//                childdata.meth_id = PFSDBUtility.DBToInt(dr2["METHODOLOGY_ID"]);
//                childdata.child_unitid = PFSDBUtility.DBToInt(dr2["childid"]);
//                childdata.child_name = PFSDBUtility.DBToString(dr2["childname"]);
//                //VerboseAudit(childdata.child_name);
//                childlist.Add(childdata);
//            } //dr read
//            dr2.Close();
//            //VerboseAudit("ED Obs Start Time=" + evdt.ToString());
//            return childlist;
//        }
//        static void ShowEDChildren()
//        {
//            foreach (var item in EDchildren)
//            {
//                VerboseAudit(item.parent_unitid + " / " + item.meth_id + " / " + item.child_unitid + " / " + item.child_name);
//            }
//        }

      

        static void DeleteOldChartItems()
        {
            string sql;
            DateTime dt;

            if (_delcidays == 0) return;

            var db = PFSDBUtility.NewSqlConnection(120);

            dt = g_effdt.AddDays(-_delcidays);
            sql = "DELETE CHART_ITEM WHERE EVENT_DATETIME <"+ PFSDBUtility.SQLDateTime(dt);

            var cmd = new SqlCommand(sql, db);
            SqlDataReader dr = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);

            LogInfo("Delete chart_item sql="+sql, PFSEventLog.EventLogCategory.EVENT_CATEGORY_STARTUP_SHUTDOWN);

        }

        static void MaybeRunImport()
        {
            if (! g_import_when_done) return;
            
            string path = Path.GetDirectoryName(Application.ExecutablePath);
            if (path.Right(3) != "bin") {                       // running in visual studio?
                path = @"C:\qmdev\AcuityPlus\main\bin";
            }
            if (Directory.Exists(path))
            {
                LogInfo("Running transparent import...");
                // Import will add its own log entries; no need to add any here
                int rc = PFSUtility.ExecuteAndWait(Path.Combine(path, PFSGlobal.TRANSPARENT_IMPORT_EXE_NAME));
                LogInfo("Done; result=" + rc);
            }
            else
            {
                LogWarning("Can't find " + path);
            }
        }



        //=====================================================================
        // Audits and log files
        //=====================================================================
        // Print to console if debug is set
        public static void DebugTrace(string format, params object[] values)
        {
            if (g_debug)
            {
                Console.Write(DateTime.Now.ToString() + " ");
                Console.WriteLine(format, values);
            }
        }

        public static void DebugPause()
        {
            if (g_debug)
            {
                Console.Write("Press any key...");
                Console.ReadKey();
                Console.WriteLine("");
            }
        }

        // Save in both audit files
        static public void Audit(string s)
        {
            DebugTrace(s);
            if (logfile != null) logfile.WriteLine(s);
            gBriefAudit.AppendLine(s);                  // Add to both audit reports
            gVerboseAudit.AppendLine(s);
        }

        // Save in verbose audit only
        static public void VerboseAudit(string s)
        {
            DebugTrace(s);
            if (logfile != null) logfile.WriteLine(s);
            gVerboseAudit.AppendLine(s);                // Add to verbose audit only
        }

        public static void AddLogEntry(PFSEventLog.EventLogType type, string msg, PFSEventLog.EventLogCategory category)
        {
            Audit(msg);

            PFSEventLog.AddEventLogEntry(
                PFSEventLog.EventLogSource.EVENT_SOURCE_TRANSPARENT_MAPPING,
                type, category, msg, gLogSourceText, 0, gLogUnitID, gLogEncounterID);
        }

        public static void LogInfo(string msg, PFSEventLog.EventLogCategory category)
        {
            AddLogEntry(PFSEventLog.EventLogType.EVENT_TYPE_INFO, msg, category);
        }
        public static void LogInfo(string msg)
        {
            LogInfo(msg, PFSEventLog.EventLogCategory.EVENT_CATEGORY_PROCESSED);
        }

        public static void LogWarning(string msg, PFSEventLog.EventLogCategory category)
        {
            AddLogEntry(PFSEventLog.EventLogType.EVENT_TYPE_WARNING, msg, category);
        }
        public static void LogWarning(string msg)
        {
            LogWarning(msg, PFSEventLog.EventLogCategory.EVENT_CATEGORY_VALIDATION);
        }

        public static void LogError(string msg, PFSEventLog.EventLogCategory category)
        {
            AddLogEntry(PFSEventLog.EventLogType.EVENT_TYPE_ERROR, msg, category);
        }
        public static void LogError(string msg)
        {
            LogError(msg, PFSEventLog.EventLogCategory.EVENT_CATEGORY_VALIDATION);
        }
        public static void LogUnexpectedError(string msg, string stack_trace)
        {
            // Add the message and stack trace to event log; message only goes to screen
            gLogSourceText = stack_trace;
            AddLogEntry(PFSEventLog.EventLogType.EVENT_TYPE_ERROR, msg, PFSEventLog.EventLogCategory.EVENT_CATEGORY_UNEXPECTED);
            gLogSourceText = "";
            
            // Add the stack trace to screen and log files
            Audit(msg);
            Audit(stack_trace);
        }

    }
}
