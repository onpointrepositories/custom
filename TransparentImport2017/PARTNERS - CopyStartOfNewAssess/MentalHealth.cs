﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;                     // for LINQ; add a reference
using System.Data.SqlClient;                // for SqlConnection
using System.IO;                            // for Directory, Path, Stream
using PfsShared;                            // add a reference to Shared2 project

// ============================================================================
// Mental Health transparent mapping -- GOES HERE --
// PARTNERS EPIC
// ============================================================================
// This processes one patient.  Only one database query is used.  :)
//
// All search functions use exact match for category, description and field name.
// Codes and results can be a single word or a list of words.
// Codes default to exact match; prefix with CODE_CODE_LIKE_PREFIX if desired (single word only)
// Results default to like matching; prefix with RESULT_EXACT_MATCH_PREFIX if desired (single word only)
//
// All searches are case insensitive.
//
//
namespace TransparentMapping
{
    class MentalHealth
    {
        private const int MAX_INDS = 120;
        private const int MAX_PROCS = 20;

        private const string CODE_LIKE_PREFIX = "%!";       // use to make codes %like%
        private const string EXACT_MATCH_PREFIX = "&!";     // use to make desc/result exact match
        private const string CHAR_COMMA = "||";             // use to insert literal comma in word list
        private const string NOT_PREFIX = "!!";             // use to insert literal comma in word list
        private const string AVOID_NEGATIVE = "!;";
        private const string EXACT_SQL_PREFIX = "!SQL!";     // use to give sql constraint as given
        private const string STARTS_WITH = "[!";     // use to give sql constraint as given

        private struct indicator_data
        {
            public bool is_checked;
            public int radio_group;
        }

        private struct proc_data
        {
            public int procedure_number;
            public DateTime start;
            public DateTime finish;
        }

        // These are database CHART_ITEMs for this patient
        private CHART_ITEM[] _chart_items_since_admission;
        //private CHART_ITEM[] _chart_items_since_unit_arrival;
        //private CHART_ITEM[] _chart_items_during_pull_period;
        //private CHART_ITEM[] _chart_items_pull_period_plus;
        private CHART_ITEM[] _chart_items_since24hrs;
        //private CHART_ITEM[] _chart_items_since13hrs;
        //private CHART_ITEM[] _chart_items_since25hrs;
        //private CHART_ITEM[] _chart_items_since9hrs;
        // These are database PROCEDURE_EVENTs for this patient
        private PROCEDURE_EVENT[] _procedure_events;

        private indicator_data[] _inds;
        private List<proc_data> _procs;
        //private List<proc_data> _outcomes;

        private const int DEFAULT_BUCKET_SIZE = 20;       //min

        private PatientInfo _pat;
        private int _bucket_size = DEFAULT_BUCKET_SIZE;
        private bool is_default;
        private string txarea;
        private bool g_toi4 = false;
        private bool g_gitube = false;
        private DateTime loc_in;
        private DateTime loc_out;

        private bool exclude_periop_data = false;
        private LOAtypePrecision[] ary_hemodial = new LOAtypePrecision[5];
        private int numhemodial = 0;

        private enum SearchDepth
        {
            SearchDefault,
            SearchPullRange,            //search within the current pull     -- default
            SearchSinceArrival,         //search since arrival to the unit
            SearchSinceAdmission,        //search everything since admission to the hospital
            SearchPullPlus,
            SearchSince25Hrs,
            SearchSince24Hrs,
            SearchSince16Hrs,
            SearchSince13Hrs,
            SearchSince9Hrs
        }

        private enum CountMode
        {
            CountAll,
            CountFirst                  //stop after one is found
        }

        private enum GetValueMode
        {
            GetTotal,
            GetMax,
            GetLast
        }

        enum Frequencies
        {
            QNONE,
            Q4H,
            Q2H,
            Q1H,
            Q30M
        }

        private struct fmapRow
        {
            public double los_high;                       //the LOS being testing
            public int[] freq;                           //the count required for each Q value
        }
        private List<fmapRow> _freq_map;                    //1,2,4,8,12,24 hours

        private struct gBucket
        {
            public int bucket;
            public string code;
            public DateTime evdt;
            public bool using_waiver;
            public bool has_all_deps;
            public int num_addl_items;
        }
        private struct gGap
        {
            public int gap;
            public DateTime evdt1;
            public DateTime evdt2;
        }
        private struct MedChartItem
        {
            public string code;
            public string orderid;
            public DateTime evdt;
            public bool valid;
        }


    //
    // This is the main entry point
    //
    public void ProcessPatient(PatientInfo pat)
        {
            _pat = pat;
            bool use_default = false;
            bool no_chart_items_in_24hrs = false;

            InitIndicators(); // sets is_default
            InitProcs();
            if (!is_default)
            {
                LoadFreqTable();
                no_chart_items_in_24hrs = (LoadPatientChart() == 0);
                if (no_chart_items_in_24hrs)
                    Program.Audit("No chart items received in past 24 hrs.");
                {
                    Check_1_2_3();
                    Check_4();
                    Check_5_6();
                    Check_7_8_9();
                    Check_10_11_12_13_14();
                    Check_15();
                    Check_16_17();
                    Check_18_19_20();
                    Check_21();
                    Check_22();
                    Check_23();
                    Check_24();
                    //CheckUserDefined();
                    AtLeastOneADL();
                }
            }

            //if (!no_chart_items_in_24hrs)
            {

                HighestIndicatorInEachGroupWins();

                if (!is_default)
                {
                    if (!Program.g_noactivities) CheckProcs();
                }

                if (Program.g_no_output) return;

                OutputClass(use_default);
                if (!Program.g_noactivities) OutputProcs();
            }
        }


        private void InitIndicators()
        {
            // Make a new (empty) indicator array
            _inds = new indicator_data[MAX_INDS + 1];           // This 1 based so add one
            is_default = false;
            txarea = "";

            // get indicator radio groups from the database
            // ** (This database access can be replaced once we have a C# methodolgy cache)
            var db = PFSDBUtility.NewPfsDataContext();
            var query = from idef in db.INDICATOR_DEFINITIONs
                        where (idef.METHODOLOGY_ID == _pat.meth_id)
                        select idef;
            foreach (var idef in query)
            {
                if (idef.INDICATOR_NUMBER <= _inds.GetUpperBound(0))
                {
                    // (convert nulls to zero)
                    _inds[idef.INDICATOR_NUMBER].radio_group = PFSDBUtility.DBToInt(idef.RADIO_GROUP);
                }
            }
        }

        private void InitProcs()
        {
            _procs = new List<proc_data>();
            //_outcomes = new List<proc_data>();
        }

        private fmapRow LoadFreqTableRow(double los_high, string values)
        {
            fmapRow fmrow;

            fmrow.los_high = los_high;
            fmrow.freq = new int[Enum.GetNames(typeof(Frequencies)).Length];

            var arr = values.Split(',');
            for (int i = 0; i <= fmrow.freq.GetUpperBound(0); i++)
            {
                fmrow.freq[i] = arr[i].ToInteger();
            }
            return fmrow;
        }

        // These are the number of times you need to do something during a certain time period (LOS)
        // in order for it to be considered one of the frequencies listed.
        private void LoadFreqTable()
        {
            _freq_map = new List<fmapRow>();
            //                              LOS,  None Q4h Q2h Q1h Q30m
            _freq_map.Add(LoadFreqTableRow(1, "    0,  0,  0,  1,  2"));
            _freq_map.Add(LoadFreqTableRow(2, "    0,  0,  1,  2,  4"));
            _freq_map.Add(LoadFreqTableRow(4, "    0,  1,  2,  4,  8"));
            _freq_map.Add(LoadFreqTableRow(6, "    0,  2,  3,  6,  9"));
            _freq_map.Add(LoadFreqTableRow(8, "    0,  2,  4,  8, 12"));
            _freq_map.Add(LoadFreqTableRow(12, "   0,  3,  6, 12, 18"));
            _freq_map.Add(LoadFreqTableRow(16, "   0,  4,  8, 16, 24"));
            _freq_map.Add(LoadFreqTableRow(24, "   0,  6, 12, 24, 36"));
            //New freq table 2/5/14
            //q4	q2	q1	q30     q30
            //            Non-ICU	ICU & SD
            // 4	8	15	29	    36
            // 3	5	9	17	    24
            // 2	4	7	13	    19
            // 2	3	5	10	    13

            //to read this table: if you have at least 6 asessmenets in a 12-hour period, you get Q1h
            //(LOS=12, column Q1h has a count of 6)
        }

        private Frequencies FreqForCount(double los_hours, int count)
        {
            foreach (var fmrow in _freq_map)
            {
                if (los_hours <= fmrow.los_high)
                {
                    // Option: pro-rate the count upward if the LOS is less than the row we are interested in.
                    //         This will bump the count to what it might have been at the full LOS.
                    // Note: truncate the result; rounding inflates the value too much.
                    int prorated_count = (int)((fmrow.los_high / los_hours) * count);

                    // foreach goes low to high; go from high to low instead
                    for (int j = (int)Frequencies.Q30M; (j > (int)Frequencies.QNONE); j--)
                    { //search right to left
                        if (prorated_count >= fmrow.freq[j])
                        {
                            return (Frequencies)j;
                        }
                    } // next j
                }
            }

            return Frequencies.QNONE;
        }

        //
        // Load patient chart from the database (one DB access)
        // Load everything since admission
        //
        //private int LoadPatientChart()
        //{
        //    int ct_in_25hrs = 0;
        //    int ctperiop = 0;
        //    // Get the entire patient chart (all units and dates for this patient)
        //    var db = PFSDBUtility.NewPfsDataContext();
        //    var query = from item in db.CHART_ITEMs
        //                where (item.ENCOUNTER_ID == _pat.encounter_id)
        //                where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-25))
        //                select item;
        //    // Save the result
        //    ct_in_25hrs = query.Count();
        //    Program.VerboseAudit("Since 25 hrs count=" + ct_in_25hrs);
        //    _chart_items_since25hrs = query.ToArray();

        //    // Convert all results to lower case for case insensitive comparisons
        //    foreach (var item in _chart_items_since25hrs)
        //    {
        //        if (item.CATEGORY != null) item.CATEGORY = item.CATEGORY.ToLower();
        //        if (item.CODE != null) item.CODE = item.CODE.ToLower();
        //        if (item.DESCRIPTION != null) item.DESCRIPTION = item.DESCRIPTION.ToLower();
        //        if (item.FIELD_NAME != null) item.FIELD_NAME = item.FIELD_NAME.ToLower();
        //        if (item.RESULT != null) item.RESULT = item.RESULT.ToLower();
        //        //mark items during periop
        //        foreach (var perioploc in Program.patperioplist)
        //        {
        //            if (item.EVENT_DATETIME >= perioploc.in_time && item.EVENT_DATETIME <= perioploc.out_time)
        //            {
        //                item.UNIT_ID = -6;
        //                ctperiop++;
        //            }
        //        }

        //    }

        //    Program.VerboseAudit("Since 25 hrs count of periop/temp items=" + ctperiop);
        //    // Prepare more versions of the chart

        //    //var query2 = from item in _chart_items_since25hrs
        //    //             where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-13)) && (item.EVENT_DATETIME <= _pat.pull_finish)
        //    //             select item;
        //    //Program.VerboseAudit("Since 13 hrs count=" + query2.Count());
        //    //_chart_items_since13hrs = query2.ToArray();

        //    //query2 = from item in _chart_items_since25hrs
        //    //             //                     where (item.EVENT_DATETIME >= _pat.pull_start) && (item.EVENT_DATETIME <= _pat.pull_finish)
        //    //         where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-8)) && (item.EVENT_DATETIME <= _pat.pull_finish)
        //    //         select item;
        //    //_chart_items_during_pull_period = query2.ToArray();

        //    return ct_in_25hrs;
        //}

        private int LoadPatientChart()
        {
            // Hemodialysis   Started Completed  no more than 4 hours
            // 3040100125

            //            Placement of UVC
            //3042394421 date
            //3042394422 time
            //900700 date
            //900701 time

            //Removal of UVC
            //3042394424
            //3042394425
            //900702
            //900703
            foreach (var p in Program.patloclist)
            {
                if (p.loc_idx == _pat.loc_idx)
                {
                    loc_in = p.in_time;
                    loc_out = p.out_time;
                }
            }
            _pat.los_hours = PFSUtility.DateDiffInMinutes(loc_in, loc_out) / 60.0;
            Program.VerboseAudit("LoadChart los=" + _pat.los_hours);
            //Program.VerboseAudit("LoadChart unit=" + p.unit_name + " locidx=" + p.loc_idx + " in=" + p.in_time + " out=" + p.out_time);

            int ct_in_24hrs = 0;
            int ctperiop = 0;
            // Get the entire patient chart (all units and dates for this patient)
            var dba = PFSDBUtility.NewPfsDataContext();
            var queryall = from item in dba.CHART_ITEMs
                           where (item.ENCOUNTER_ID == _pat.encounter_id)
                           orderby item.EVENT_DATETIME
                           select item;
            //Get the ECT Aldrete score times
            var queryECT = (from e in queryall
                            where (e.CODE == "304239656")
                            select e.EVENT_DATETIME).Distinct().ToList();
            foreach (var ect in queryECT)
            {
                Program.VerboseAudit("ECT time=" + ect);
            }
            //Exclude the ECT times
            var querya = from g in queryall
                         where (!queryECT.Contains(g.EVENT_DATETIME)
                                || _pat.short_name == "MEE")
                         select g;
            // Save the result
            _chart_items_since_admission = querya.ToArray();

            var query = from item in _chart_items_since_admission
                        where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-24)
                               && (item.EVENT_DATETIME <= _pat.pull_finish))
                        select item;
            // Exclude hemodialysis items between Started and Completed (max=Started+4hrs)
            // Find Started/Completed pairs:
            bool starta = false;
            DateTime starta_dt = DateTime.MinValue;
            foreach (var c in query)
            {
                if (c.CODE == "3040100125")
                {
                    if (c.RESULT.ToLower().StartsWith("start"))
                    {
                        if (!starta)
                        {
                            starta = true;
                            starta_dt = c.EVENT_DATETIME;
                        }
                        else // then found a second start after the initial start
                        {
                            numhemodial++;
                            ary_hemodial[numhemodial].startdt = starta_dt;
                            if (c.EVENT_DATETIME >= starta_dt.AddHours(4))
                                ary_hemodial[numhemodial].enddt = starta_dt.AddHours(4);
                            else
                                ary_hemodial[numhemodial].enddt = c.EVENT_DATETIME;
                            starta_dt = c.EVENT_DATETIME;
                            Program.VerboseAudit("Ignoring HD items between: " + ary_hemodial[numhemodial].startdt + " => " + ary_hemodial[numhemodial].enddt + " [3]");
                        }
                    }
                    else if (c.RESULT.ToLower().StartsWith("complete"))
                    {
                        if (!starta) //then complete without a start: go back 4 hours.
                        {
                            numhemodial++;
                            ary_hemodial[numhemodial].startdt = c.EVENT_DATETIME.AddHours(-4);
                            ary_hemodial[numhemodial].enddt = c.EVENT_DATETIME;
                            Program.VerboseAudit("Ignoring HD items between: " + ary_hemodial[numhemodial].startdt + " => " + ary_hemodial[numhemodial].enddt + " [2]");
                        }
                        else
                        {
                            numhemodial++;
                            ary_hemodial[numhemodial].startdt = starta_dt;
                            ary_hemodial[numhemodial].enddt = c.EVENT_DATETIME;
                            Program.VerboseAudit("Ignoring HD items between: " + ary_hemodial[numhemodial].startdt + " => " + ary_hemodial[numhemodial].enddt + " [1]");
                        }
                        starta = false;
                    }
                }
            }
            if (starta)
            {
                numhemodial++;
                ary_hemodial[numhemodial].startdt = starta_dt;
                ary_hemodial[numhemodial].enddt = starta_dt.AddHours(4);
                Program.VerboseAudit("Ignoring HD items between: " + ary_hemodial[numhemodial].startdt + " => " + ary_hemodial[numhemodial].enddt + " [4]");
            }

            // Save the result
            ct_in_24hrs = query.Count();
            _chart_items_since24hrs = query.ToArray();

            // Convert all results to lower case for case insensitive comparisons
            foreach (var item in _chart_items_since24hrs)
            {
                item.SOURCE_TEXT = null;
                if (item.CATEGORY != null) item.CATEGORY = item.CATEGORY.ToLower();
                if (item.CODE != null) item.CODE = item.CODE.ToLower();
                if (item.DESCRIPTION != null) item.DESCRIPTION = item.DESCRIPTION.ToLower();
                if (item.FIELD_NAME != null) item.FIELD_NAME = item.FIELD_NAME.ToLower();
                if (item.RESULT != null) item.RESULT = item.RESULT.ToLower();
                for (int i = 1; i <= numhemodial; i++)
                {
                    if (item.EVENT_DATETIME >= ary_hemodial[i].startdt
                        && item.EVENT_DATETIME <= ary_hemodial[i].enddt)
                    {
                        item.UNIT_ID = -6;
                        ctperiop++;
                    }
                }
            }

            Program.VerboseAudit("Since 24 hrs count items=" + ct_in_24hrs);
            Program.VerboseAudit("Since 24 hrs count of HD items=" + ctperiop);

            return ct_in_24hrs;
        }

        //private int LoadPatientChart()
        //{
        //    int ct_in_24hrs = 0;
        //    int ctperiop = 0;

        //    foreach (var p in Program.patloclist)
        //    {
        //        if (p.loc_idx == _pat.loc_idx)
        //        {
        //            loc_in = p.in_time;
        //            loc_out = p.out_time;
        //        }
        //    }
        //    _pat.los_hours = PFSUtility.DateDiffInMinutes(loc_in, loc_out) / 60.0;
        //    Program.VerboseAudit("LoadChart los=" + _pat.los_hours);



        //    // Get the entire patient chart (all units and dates for this patient)
        //    var dba = PFSDBUtility.NewPfsDataContext();
        //    var querya = from item in dba.CHART_ITEMs
        //                 where (item.ENCOUNTER_ID == _pat.encounter_id)
        //                 //where (item.CODE == "304239375"
        //                 //|| item.CODE == "304239487"
        //                 //|| item.CODE == "1540100298")
        //                 select item;
        //    // Save the result
        //    _chart_items_since_admission = querya.ToArray();

        //    var db = PFSDBUtility.NewPfsDataContext();
        //    var query = from item in db.CHART_ITEMs
        //                where (item.ENCOUNTER_ID == _pat.encounter_id)
        //                where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-24))
        //                where (item.EVENT_DATETIME <= _pat.pull_finish)
        //                select item;
        //    // Save the result
        //    ct_in_24hrs = query.Count();
        //    _chart_items_since24hrs = query.ToArray();

        //    // Convert all results to lower case for case insensitive comparisons
        //    foreach (var item in _chart_items_since24hrs)
        //    {
        //        item.SOURCE_TEXT = null;
        //        if (item.CATEGORY != null) item.CATEGORY = item.CATEGORY.ToLower();
        //        if (item.CODE != null) item.CODE = item.CODE.ToLower();
        //        if (item.DESCRIPTION != null) item.DESCRIPTION = item.DESCRIPTION.ToLower();
        //        if (item.FIELD_NAME != null) item.FIELD_NAME = item.FIELD_NAME.ToLower();
        //        if (item.RESULT != null) item.RESULT = item.RESULT.ToLower();
        //        foreach (var perioploc in Program.patperioplist)
        //        {
        //            if (item.EVENT_DATETIME >= perioploc.in_time && item.EVENT_DATETIME <= perioploc.out_time)
        //            {
        //                item.UNIT_ID = -6;
        //                ctperiop++;
        //            }
        //        }
        //    }

        //    Program.VerboseAudit("Since 24 hrs count items=" + ct_in_24hrs);
        //    Program.VerboseAudit("Since 24 hrs count of periop/temp items=" + ctperiop);


        //    return ct_in_24hrs;
        //}


        // Delay looking for patient procedures until they are wanted
        private void LoadPatientProceduresIfNeeded()
        {
            if (_procedure_events != null) return;

            var db = PFSDBUtility.NewPfsDataContext();
            var query = from proc in db.PROCEDURE_EVENTs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                        where (proc.PROCEDURE_DATETIME >= _pat.pull_finish.AddHours(-24))
                        select proc;
            _procedure_events = query.ToArray();
        }



        // Started a new chart item query (of a certain depth)
        // Default is the chart during the pull range.  Options for unit arrival and admission to hospital.
        private IEnumerable<CHART_ITEM> StartNewQuery()
        {
            return StartNewQuery(SearchDepth.SearchDefault);
        }
        private IEnumerable<CHART_ITEM> StartNewQuery(SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;

            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= loc_in && (item.EVENT_DATETIME <= loc_out)) select item);
                    else
                        return (from item in _chart_items_since24hrs where (item.EVENT_DATETIME >= loc_in && item.EVENT_DATETIME <= loc_out) select item);
                case SearchDepth.SearchSinceAdmission:
                    return (from item in _chart_items_since_admission select item);
                //case SearchDepth.SearchPullPlus:
                //    return (from item in _chart_items_pull_period_plus select item);
                case SearchDepth.SearchSince24Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                    else
                        return (from item in _chart_items_since24hrs where (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince16Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-16) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since24hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-16) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince13Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-13) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since24hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-13) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                //case SearchDepth.SearchSince12Hrs:
                //    if (exclude_periop_data)
                //        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-12) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                //    else
                //        return (from item in _chart_items_since24hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-12) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince9Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-9) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since24hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-9) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                //case SearchDepth.SearchSince4Hrs:
                //    if (exclude_periop_data)
                //        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME > _pat.pull_finish.AddHours(-4) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                //    else
                //        return (from item in _chart_items_since24hrs where item.EVENT_DATETIME > _pat.pull_finish.AddHours(-4) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
            }
            return null;
        }

        // Is this search term a word or a list of words?
        private bool ValueIsAList(string s)
        {
            return (s != null) && s.Contains(',');                //is this a comma-separated list?
        }

        // Break apart a wordlist and prepare its members
        private string[] SplitOnCommaAndPrepareElements(string s)
        {
            // If a word list has a space after each comma, we will get leading blanks in each term
            var arr = s.Split(',');
            for (int i = 0; i <= arr.GetUpperBound(0); i++)
            {
                arr[i] = arr[i].Trim();                         // get rid of leading blanks
                arr[i] = arr[i].Replace(CHAR_COMMA, ",");       // insert a literal comma into search word
                arr[i] = arr[i].ToLower();                      // convert for case-insensitive comparisons
            }
            return arr;
        }

        // Add a basic chart item filter to a LINQ query
        //
        // cat = exact match
        // code_list = exact match; optional "like" match
        // desc_list = "like" match; optional exact match
        // field = exact match
        // result_list = "like" match; optional exact match
        //
        // These are comma-separated lists.  
        // Search for a literal comma by using CHAR_COMMA in the string.
        private IEnumerable<CHART_ITEM> AndItemFilter(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list)
        {
            // Converted all values to lower case for case-insensitive comparisons.
            if (!String.IsNullOrEmpty(cat))
            {
                cat = cat.ToLower();
                query = query.Where(e => e.CATEGORY == cat);
            }

            if (!String.IsNullOrEmpty(code_list))
            {
                code_list = code_list.ToLower();
                if (code_list.Left(2) == CODE_LIKE_PREFIX)
                {            // override for "like" match?
                    query = query.Where(e => e.CODE.Contains(code_list.Substring(2)));
                }
                else if (code_list.Left(2) == EXACT_MATCH_PREFIX)
                {
                    query = query.Where(e => e.CODE == code_list.Substring(2));
                }
                else if (ValueIsAList(code_list))
                {
                    query = AndCodeInList(query, code_list);            // find one of the words
                }
                else
                {
                    query = query.Where(e => e.CODE.Contains(code_list));      // find this word
                }
            }

            if (!String.IsNullOrEmpty(desc_list))
            {
                desc_list = desc_list.ToLower();
                if (desc_list.Left(2) == EXACT_MATCH_PREFIX)
                {          // exact match override?
                    query = query.Where(e => e.DESCRIPTION == desc_list.Substring(2));
                }
                else if (desc_list.Left(2) == NOT_PREFIX)
                {
                    // query = query.Where(e => !e.DESCRIPTION.Contains(desc_list.Substring(2)));
                    query = AndDescriptionNOTInList(query, desc_list.Substring(2));
                }
                else if (ValueIsAList(desc_list))
                {
                    query = AndDescriptionInList(query, desc_list);
                }
                else
                {
                    desc_list = desc_list.Replace(CHAR_COMMA, ",");     // look for literal comma requests
                    query = query.Where(e => e.DESCRIPTION.Contains(desc_list));
                }
            }

            if (!String.IsNullOrEmpty(field))
            {
                field = field.ToLower();
                query = query.Where(e => e.FIELD_NAME == field);
            }

            if (!String.IsNullOrEmpty(result_list))
            {
                query = query.Where(e => e.RESULT != null);
                result_list = result_list.ToLower();                        // make all results case insensitve
                if (result_list.Left(2) == EXACT_MATCH_PREFIX)
                {     // exact match override?
                    query = query.Where(e => e.RESULT == result_list.Substring(2));
                }
                else if (result_list.Left(2) == AVOID_NEGATIVE)
                {
                    query = query.Where(e => (!e.RESULT.Contains("no " + result_list.Substring(2))) && ((e.RESULT == result_list.Substring(2)) || (e.RESULT.Contains(";" + result_list.Substring(2)))));
                }
                else if (ValueIsAList(result_list))
                {
                    query = AndResultInList(query, result_list);
                }
                else if (result_list.Left(2) == STARTS_WITH)
                {
                    query = query.Where(e => e.RESULT.StartsWith(result_list.Substring(2)));
                }
                else
                {
                    result_list = result_list.Replace(CHAR_COMMA, ",");     // look for literal comma requests
                    query = query.Where(e => e.RESULT.Contains(result_list));
                }
            }
            return query;
        }

        // Look for a code that is one of the words in the list.
        private IEnumerable<CHART_ITEM> AndCodeInList(IEnumerable<CHART_ITEM> query, string code_list)
        {
            if (String.IsNullOrEmpty(code_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(code_list);
            //            return query.Where(e => arr.Any(x => x.Contains(e.CODE))); // "like" match
            return query.Where(e => e.CODE.ContainsAny(arr)); // "like" match
        }

        // Look for a description that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndDescriptionInList(IEnumerable<CHART_ITEM> query, string desc_list)
        {
            if (String.IsNullOrEmpty(desc_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(desc_list);
            //return query.Where(e => arr.Any(x => x.Contains(e.DESCRIPTION))); // "like" match
            return query.Where(e => e.DESCRIPTION.ContainsAny(arr)); // "like" match
        }

        private IEnumerable<CHART_ITEM> AndDescriptionNOTInList(IEnumerable<CHART_ITEM> query, string desc_list)
        {
            if (String.IsNullOrEmpty(desc_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(desc_list);
            //return query.Where(e => arr.Any(x => x.Contains(e.DESCRIPTION))); // "like" match
            return query.Where(e => !e.DESCRIPTION.ContainsAny(arr)); // "like" match
        }


        // Look for a result that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultInList(IEnumerable<CHART_ITEM> query, string result_list)
        {
            bool use_start_with = false;

            if (String.IsNullOrEmpty(result_list)) return query;

            if (result_list.Left(2) == STARTS_WITH)
            {
                use_start_with = true;
                result_list = result_list.Substring(2);
            }

            var arr = SplitOnCommaAndPrepareElements(result_list);
            //            return query.Where(e => arr.Any(x => x.Contains(e.RESULT))); // "like" match
            if (use_start_with)
                return query.Where(e => arr.Any(item => e.RESULT.StartsWith(item)));
            else
                return query.Where(e => e.RESULT.ContainsAny(arr)); // "like" match

        }

        // Look for a result that does not contain one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultNotInList(IEnumerable<CHART_ITEM> query, string result_list)
        {
            if (String.IsNullOrEmpty(result_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(result_list);
            //            return query.Where(e => !arr.Any(x => x.Contains(e.RESULT))); // "like" match
            return query.Where(e => !e.RESULT.ContainsAny(arr)); // "like" match
        }

        private string DescribeSearchDepth(SearchDepth search_depth)
        {
            string result = "";

            switch (search_depth)
            {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    //result = "in pull range"
                    result = "";                         //be quiet since this is default
                    break;
                case SearchDepth.SearchSinceArrival:
                    result = "since arrival to unit";
                    break;
                case SearchDepth.SearchSinceAdmission:
                    result = "since admission";
                    break;
                case SearchDepth.SearchPullPlus:
                    result = "in LOS range plus 4 hours after";
                    break;
            }

            return result;
        }

        // Describe a query that has already been run
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Make a simple query and describe the results
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            var arr = query.ToArray();
            return Describe(arr, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private string LookingFor(string so_far, string what, string op, string value)
        {
            string result;
            if (String.IsNullOrEmpty(value)) return so_far;

            if (so_far == "")
                result = "looking for " + what;
            else
                result = so_far + " and " + what;

            if (value.Left(2) == EXACT_MATCH_PREFIX)
                result += "='" + value.Substring(2) + "'";
            else if (value.Left(2) == CODE_LIKE_PREFIX)
                result += " contains " + "'" + value.Substring(2) + "'";
            else if (ValueIsAList(value))
                result += " in '" + value + "'";
            else
                result += op + "'" + value + "'";

            return result;
        }
        private string Describe(CHART_ITEM[] arr, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string result = "";
            result = LookingFor(result, "code", "=", code_list);
            result = LookingFor(result, "desc", " contains ", desc_list);
            result = LookingFor(result, "result", " contains ", result_list);

            if (arr.Count() == 0)
            {
                result += "; not found " + DescribeSearchDepth(search_depth);
            }
            else
            {
                var e = arr[0];
                result = "FOUND: " + result + "; found";
                // We might have searched for a pattern or word list in several fields - show what was found
                if (e.CODE != null) result += " code='" + e.CODE + "'";
                if (e.DESCRIPTION != null) result += " desc='" + e.DESCRIPTION + "'";
                if (e.RESULT != null) result += " result='" + e.RESULT + "'";
                // Are there more results?  Just say how many; we aren't going to list them.
                if (arr.Count() == 2)
                {
                    result += " (1 more result)";
                }
                else if (arr.Count() > 2)
                {
                    result += " (" + (arr.Count() - 1) + " more results)";
                }
            }

            return result;
        }


        //Set an indicator for this reason (low level)
        private void SetInd(int inum, string reason)
        {
            if (_inds[inum].is_checked)
            {
                Program.VerboseAudit("Set Ind #" + inum + ": " + reason);          //already set - repeat for verbose only
            }
            else
            {
                _inds[inum].is_checked = true;
                Program.Audit("Set Ind #" + inum + ": " + reason);
            }
        }

        //Clear an indicator for this reason (low level)
        private void ClrInd(int inum, string reason)
        {
            if (_inds[inum].is_checked)
            {
                Program.VerboseAudit("Clr Ind #" + inum + ": " + reason);          //already clear - repeat verbose only
            }
            else
            {
                _inds[inum].is_checked = false;
                Program.Audit("Clr Ind #" + inum + ": " + reason);
            }
        }

        // Count how many items meet the conditions
        // All items are listed
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountItems(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            bool first = true;
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            //if (exclude_periph_iv)
            //    query = AndDescriptionNOTInList(query, "peripheral iv");
            int count = query.Count();
            found_what = "";
            //Program.VerboseAudit("countitems: result_list="+result_list + " code=" + code_list);
            if (count > 0 && trace)
            {
                foreach (var item in query)
                {
                    if (first)
                    {
                        // always return what was found
                        //            found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                        found_what = "Found desc=" + item.DESCRIPTION + ";result=" + item.RESULT + ";code=" + item.CODE + ";items found=" + count;
                        // echo the result?
                        Program.VerboseAudit(found_what);
                        first = false;
                    }

                }
            }
            return count;
        }


        // Count how many items meet the conditions
        // This has the option to list every entry or give a total
        private int CountResultInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what = "";
            string s = "";

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            //if (exclude_periph_iv)
            //    query = AndDescriptionNOTInList(query, "peripheral iv");
            var arr = SplitOnCommaAndPrepareElements(result_list);

            foreach (var item in query)
            {
                // Figure out which of the search words were found
                for (int i = 0; i <= arr.GetUpperBound(0); i++)
                {
                    if (item.RESULT.Contains(arr[i]))
                    {
                        count++;
                        s = arr[i];
                        found_what = "Found desc:" + item.DESCRIPTION + ";result=" + item.RESULT + ";code=" + item.CODE;
                        //if (trace) Program.VerboseAudit(found_what);
                        Program.VerboseAudit(found_what);
                    }
                }
                if (count_mode != CountMode.CountAll) break;        //stop counting

            }

            return count;
        }

        // Count how many items meet the conditions
        // This has the option to list every entry or give a total
        private int CountResultInListEXCEPTList(string cat, string code_list, string desc_list, string field, string result_list, string negresult_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what = "";

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            query = AndResultNotInList(query, negresult_list);

            var arr = SplitOnCommaAndPrepareElements(result_list);

            foreach (var item in query)
            {
                // Figure out which of the search words were found
                var s = item.RESULT.ContainsWhich(arr);
                found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' code='" + item.CODE + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "'";
                if (trace) Program.VerboseAudit(found_what);
                count++;
                if (count_mode != CountMode.CountAll) break;        //stop counting
            }

            if (count > 0)
            {
                //We already printed what was found; maybe add how many?
                if (trace && count > 0) Program.VerboseAudit("found " + count + " total");
            }
            else
            {
                // Describe what was *not* found
                //found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                //if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        // Count how many items contain ALL results in list
        private int CountResultInListAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what = "";

            var arr = SplitOnCommaAndPrepareElements(result_list);

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            for (int i = 0; i <= arr.GetUpperBound(0); i++)
            {
                if (arr[i].StartsWith(NOT_PREFIX))
                {
                    query = query.Where(e => !e.RESULT.Contains(arr[i].Substring(2)));
                }
                else
                {
                    query = query.Where(e => e.RESULT.Contains(arr[i]));
                }
            }

            count = query.Count();

            if (count > 0)
            {
                found_what = "found item with all results in '" + result_list + "' without those prefixed by !!";
                if (trace) Program.VerboseAudit(found_what);
            }
            else
            {
                // Describe what was *not* found
                //found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                //if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (ValueIsAList(result_list))
            {
                return CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what);
            }
            else
            {
                return CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what);
            }
        }

        // Is there an item with this result?
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContains(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (ValueIsAList(result_list))
            {
                return (CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            }
            else
            {
                return (CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
            }
        }

        // Result contains all in list
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (ValueIsAList(result_list))
            {
                return (CountResultInListAll(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            }
            else
            {
                found_what = "Not valid for non-list results.";
                return false;
            }
        }
        // Count how many items meet the conditions, with none of the results in result_list.
        // This has the option to say how many were found rather than list all.
        private int CountResultNotInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0, rec_count = 0;
            bool found_one;

            found_what = "";
            var arr = SplitOnCommaAndPrepareElements(result_list);         // also deals with CHAR_COMMA

            //Do not filter by result here; we need all results
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            foreach (var item in query)
            {
                rec_count++;
                //Look for each search word in the result
                found_one = false;

                foreach (string s in arr)
                {
                    if (String.Equals(item.RESULT, s))
                    {
                        found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "' -- ignore this";
                        Program.VerboseAudit(found_what);
                        found_one = true;
                        break;
                    }
                }

                if (!found_one)
                {
                    //none of the words were found - good!
                    found_what = Describe(cat, code_list, desc_list, field, "", search_depth) + " -- does not contain '" + result_list + "'";
                    Program.VerboseAudit(found_what);
                    count++;
                    if (count_mode != CountMode.CountAll) break;
                }
            }

            if (count > 0)
            {
                //We already printed what was found; maybe show the total?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            }
            else if (rec_count > 0)
            {
                //We already printed what we ignored
            }
            else
            {
                //Print what did not exist
                found_what = Describe(cat, code_list, desc_list, field, "", search_depth);      //not found
                Program.VerboseAudit(found_what);
            }

            return count;
        }

        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, string found_what)
        {
            // Search in the pull range with trace on
            return CountResultDoesNotContain(cat, code_list, desc_list, field, result_list, out found_what, SearchDepth.SearchDefault, true);
        }
        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, out string found_what, SearchDepth search_depth, bool trace)
        {
            return CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0;
        }

        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            return (CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0);
        }

        // Set the indicator if the conditions are met
        //
        private bool SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SearchDepth s = SearchDepth.SearchDefault;
            //if (inum <= 3) s = SearchDepth.SearchSince25Hrs;
            //if (inum == 4) s = SearchDepth.SearchSince13Hrs;
            //if (inum == 5 || inum == 6) s = SearchDepth.SearchSince25Hrs;
            //if (inum >= 7 && inum <= 9) s = SearchDepth.SearchSince25Hrs;
            //if (inum >= 10 && inum <= 14) s = SearchDepth.SearchSince13Hrs;
            //if (inum == 15) s = SearchDepth.SearchSince25Hrs;
            //if (inum >= 16 && inum <= 20) s = SearchDepth.SearchSince9Hrs;
            //if (inum == 21) s = SearchDepth.SearchSince25Hrs;
            //if (inum == 22) s = SearchDepth.SearchSince25Hrs;
            //if (inum == 23) s = SearchDepth.SearchSince9Hrs;
            //if (inum == 24) s = SearchDepth.SearchSince9Hrs;
            return SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, s);
        }
        private bool SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            bool b = false;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return true;

            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what))
            {
                SetInd(inum, found_what);                          //echo here - found
                b = true;
            }
            else
            {
                //Program.VerboseAudit(found_what);                  //and here - not found
            }
            return b;
        }

        private void SetIndIfResultBetween(int inum, string cat, string code_list, string desc_list, string field, int loval, int hival)
        {
            SetIndIfResultBetween(inum, cat, code_list, desc_list, field, loval, hival, SearchDepth.SearchDefault);
        }

        private void SetIndIfResultBetween(int inum, string cat, string code_list, string desc_list, string field, int loval, int hival, SearchDepth search_depth)
        {
            int count = 0;
            string found_what = "";

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");
            foreach (var item in query)
            {
                if (item.RESULT.IsNumeric())
                {
                    if (item.RESULT.Val() >= loval && item.RESULT.Val() <= hival)
                    {
                        count++;
                        found_what = "found code=" + item.CODE + " with result=" + item.RESULT;
                        Program.VerboseAudit(found_what);
                    }
                }
            }

            if (count > 0)
            {
                SetInd(inum, found_what);
            }
        }


        // Special contains all
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContainsAll(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return;

            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                          //echo here - found
            }
            else
            {
                //Program.VerboseAudit(found_what);                  //and here - not found
            }
        }
        // Set the indicator if the result does not contain any of the words in result_list
        //
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultDoesNotContain(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return;

            //Turn trace off for ResultDoesNotContain() and echo what was set below with SetInd
            if (ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                           //echo here - not found
            }
            else
            {
                //Program.VerboseAudit(found_what);                   //and here - found
            }
        }

        // Clear the indicator if the result contains one of the words in the result_list
        //
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already clear
            if (!_inds[inum].is_checked) return;

            //Turn trace off for ResultContains() and echo what was set below with SetInd
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                ClrInd(inum, found_what);                           //echo here - found
            }
            else
            {
                Program.VerboseAudit(found_what);                   //and here - not found
            }
        }


        //These slightly smaller functions are meant for places where you aren't really looking for a chart result
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }

        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            return (CountResultContains(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
        }

        // Variations with optional parameters (can switch to optional parameters in VS 2010)
        //
        private void SetIndIfFound(int inum, string cat)
        {
            SetIndIfResultContains(inum, cat, "", "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list)
        {
            SetIndIfResultContains(inum, cat, code_list, "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private void ClrIndIfFound(int inum, string cat)
        {
            ClrIndIfResultContains(inum, cat, "", "", "", "", SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }


        // Get the max/total value from a result (usually in the middle of the text)
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetIntValue(get_mode, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            int value;
            int result = 0;
            bool found_one = false;

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);

            //Look for a number in the result

            foreach (var item in query)
            {
                // RESULT might be null so convert to empty string so Split won't blow up
                var arr = PFSDBUtility.DBToString(item.RESULT).Split(' ');
                foreach (string s in arr)
                {
                    //Look for 1st character numeric; IsNumeric("60min") will fail so look at 1st char only
                    if (s.Left(1).IsNumeric())
                    {
                        value = (int)s.Val();                           //Use Val; ToInteger will error on "60min"
                        switch (get_mode)
                        {
                            case GetValueMode.GetMax:
                                result = Math.Max(value, result);       //max
                                break;
                            case GetValueMode.GetTotal:
                                result += value;                        //total
                                break;
                            case GetValueMode.GetLast:
                                result = value;                         //last
                                break;
                        }

                        // print what we are searching for (the first time)
                        if (!found_one)
                        {
                            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
                        }
                        found_one = true;
                        // print each value found
                        Program.VerboseAudit("  found numeric value " + result);
                        //Keep going in case there are more
                    }
                }
            }

            if (!found_one)
            {
                //show what was not found
                Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
            }

            return result;
        }

        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetMaxValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return GetIntValue(GetValueMode.GetMax, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetTotalValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return GetIntValue(GetValueMode.GetTotal, cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Get a result; returns true if found with return_result
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result)
        {
            return GetResult(cat, code_list, desc_list, field, out return_result, SearchDepth.SearchDefault);
        }
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
            }
            else
            {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }
        private bool GetResultForInspection(string cat, string code_list, string desc_list, string field, string result, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result);

            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
            }
            else
            {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }

        private void GetResultforOther(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "Other");
            return_result = "";
            foreach (var item in query)
            {
                if (!String.Equals(item.RESULT, "Other"))
                {
                    return_result = item.RESULT;
                    //Program.VerboseAudit(found_what);
                    break;
                }

            }
            return;
        }

        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, out string return_result, out DateTime return_evdt)
        {
            return GetResultAndEVDT(cat, code_list, desc_list, field, out return_result, out return_evdt, SearchDepth.SearchDefault);
        }
        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, out string return_result, out DateTime return_evdt, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
                return_evdt = PFSDBUtility.DBToDateTime(query.First().EVENT_DATETIME);
            }
            else
            {
                return_result = "";
                return_evdt = DateTime.MinValue;
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }

        private bool GetEVDT(string cat, string code_list, string desc_list, string res, string field, int comparison, DateTime compevdt, out DateTime return_evdt)
        {
            return GetEVDT(cat, code_list, desc_list, field, res, comparison, compevdt, out return_evdt, SearchDepth.SearchDefault);
        }
        private bool GetEVDT(string cat, string code_list, string desc_list, string field, string res, int comparison, DateTime compevdt, out DateTime return_evdt, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, res);
            if (compevdt != DateTime.MinValue)
            {
                if (comparison == 0) // equal
                    query = query.Where(e => e.EVENT_DATETIME == compevdt);
                else if (comparison == 1) // GTE
                    query = query.Where(e => e.EVENT_DATETIME >= compevdt);
                else if (comparison == 2) // GT
                    query = query.Where(e => e.EVENT_DATETIME > compevdt);
                else if (comparison == 3) // LT
                    query = query.Where(e => e.EVENT_DATETIME < compevdt);
            }
            query = query.OrderByDescending(e => e.EVENT_DATETIME);
            Program.VerboseAudit("getevdt count=" + query.Count());
            if (query.Count() > 0)
            {
                return_evdt = PFSDBUtility.DBToDateTime(query.First().EVENT_DATETIME);
            }
            else
            {
                return_evdt = DateTime.MinValue;
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, res, search_depth));
            return (return_evdt != DateTime.MinValue);
        }

        //======================================================================================
        // Mapping starts here
        //======================================================================================

        private void Check_1_2_3()
        {
            Program.VerboseAudit("Default ADL Search Scope = " + _pat.pull_finish.AddHours(-24) + " to " + _pat.pull_finish);
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 1. ADL Self");
            Program.VerboseAudit("MH 2. ADL Assist");
            Program.VerboseAudit("MH 3. ADL Complete/2+");
            Program.VerboseAudit("---------------");

            exclude_periop_data = true;

            if (_pat.age < 6.0) SetInd(3, "Age < 6 years");
            //if (_pat.age >= 4.0 && _pat.age < 7.0) SetInd(2, "Age 4-6 years");

            string reslist = "";

            bool npo = false;
            string desc_found = "";


            //if (OrderInProgress("226916", out desc_found))
            //{
            //    npo = true;
            //    Program.VerboseAudit("NPO in progress: ADL Feeding component set to dependent/complete.");
            //}

            //desc_found = "";
            //GetNTEValues("40517");//Saves NTE value
            //if (OrderInProgress("40517", out desc_found))
            //{
            //    npo = true;
            //    //Program.VerboseAudit("NPO in progress: ADL Feeding component set to dependent/complete.");
            //    Program.VerboseAudit("order desc found=" + desc_found);
            //    if (desc_found.ToUpper().Contains("NPO"))
            //        npo = true;
            //}


            bool feed1 = false, feed2 = false, feed3 = npo;
            bool hyg1 = false, hyg2 = false, hyg3 = false;
            bool mob1 = false, mob2 = false, mob3 = false;
            bool toi1 = false, toi2 = false, toi3 = false;
            bool hygCOMPL = false, mobCOMPL = false, toiCOMPL = false;

            Program.VerboseAudit("FEEDING: Independent charting search...");
            reslist = "Independent/Self,Family/SO assist,Set up";
            feed1 |= Exists("", "304239251", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Independent";
            feed1 |= Exists("", "304239260", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Able to feed self,Needs set up";
            feed1 |= Exists("", "304239261", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Independent,Modified independent,With set up";
            feed1 |= Exists("", "304239249", "", "", reslist, SearchDepth.SearchSince24Hrs);
            if (feed1)
                Program.VerboseAudit("FEEDING: Independent charting found.");

            Program.VerboseAudit("FEEDING: Partial charting search...");
            reslist = "Partial assist,Needs encouragement";
            feed2 |= Exists("", "304239251", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Needs assistance";
            feed2 |= Exists("", "304239260", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Needs assist";
            feed2 |= Exists("", "304239261", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Contact guard,Minimum assistance,Moderate assistance";
            feed2 |= Exists("", "304239249", "", "", reslist, SearchDepth.SearchSince24Hrs);
            if (feed2)
                Program.VerboseAudit("FEEDING: Partial charting found.");

            Program.VerboseAudit("FEEDING: Complete charting search...");
            reslist = "Total assist";
            feed3 |= Exists("", "304239251", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = EXACT_MATCH_PREFIX+ "Dependent";
            feed3 |= Exists("", "304239260", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Total assist";
            feed3 |= Exists("", "304239261", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Bolus,Continuous,Intermittent";
            feed3 |= Exists("", "304239266", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Maximum assistance,Dependent";
            feed3 |= Exists("", "304239249", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Yes";
            feed3 |= Exists("", "304239267", "", "", reslist, SearchDepth.SearchSince24Hrs);
            if (feed3)
                Program.VerboseAudit("FEEDING: Complete charting found.");


            Program.VerboseAudit("HYGIENE: Independent charting search...");
            reslist = "Independent";
            hyg1 |= Exists("", "304239268", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Independent";
            hyg1 |= Exists("", "304239269", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Independent";
            hyg1 |= Exists("", "304239270", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Independent,With set up";
            hyg1 |= Exists("", "304239247", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Independent,With set up";
            hyg1 |= Exists("", "304239248", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Independent,With set up";
            hyg1 |= Exists("", "304239250", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Independent,With set up";
            hyg1 |= Exists("", "304239306", "", "", reslist, SearchDepth.SearchSince24Hrs);
            if (hyg1)
                Program.VerboseAudit("HYGIENE: Independent charting found.");

            Program.VerboseAudit("HYGIENE: Partial charting search...");
            reslist = "Setup/Supervision,Minimum Assist/Patient Performed 75, Moderate Assist/Patient Performed 50";
            hyg2 |= Exists("", "304239268", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Needs assistance";
            hyg2 |= Exists("", "304239269", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Needs assistance";
            hyg2 |= Exists("", "304239270", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Contact guard,Minimum assistance,Moderate assistance";
            hyg2 |= Exists("", "304239247", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Contact guard,Minimum assistance,Moderate assistance";
            hyg2 |= Exists("", "304239248", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Contact guard,Minimum assistance,Moderate assistance";
            hyg2 |= Exists("", "304239250", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Supervision,Close supervision,Contact guard,Minimum assistance,Moderate assistance";
            hyg2 |= Exists("", "304239306", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "1";
            hyg2 |= Exists("", "304239344", "", "", reslist, SearchDepth.SearchSince24Hrs);
            if (hyg2)
                Program.VerboseAudit("HYGIENE: Partial charting found.");

            Program.VerboseAudit("HYGIENE: Complete charting search...");
            reslist = "Maximum Assist/Patient Performed 25,Total Assistance/ Patient Performed <25";
            hyg3 |= Exists("", "304239268", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = EXACT_MATCH_PREFIX + "Dependent";
            hyg3 |= Exists("", "304239269", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = EXACT_MATCH_PREFIX + "Dependent";
            hyg3 |= Exists("", "304239270", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Maximum assistance";
            hyg3 |= Exists("", "304239247", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = EXACT_MATCH_PREFIX+"Dependent";
            hyg3 |= Exists("", "304239247", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Maximum assistance";
            hyg3 |= Exists("", "304239248", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = EXACT_MATCH_PREFIX + "Dependent";
            hyg3 |= Exists("", "304239248", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Maximum assistance";
            hyg3 |= Exists("", "304239250", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = EXACT_MATCH_PREFIX + "Dependent";
            hyg3 |= Exists("", "304239250", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Maximum assistance";
            hyg3 |= Exists("", "304239306", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = EXACT_MATCH_PREFIX + "Dependent";
            hyg3 |= Exists("", "304239306", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "2,3,4";
            hygCOMPL = Exists("", "304239344", "", "", reslist, SearchDepth.SearchSince24Hrs);
            if (hyg3 || hygCOMPL)
                Program.VerboseAudit("HYGIENE: Complete charting found.");


            Program.VerboseAudit("MOBILITY: Independent charting search...");
            reslist = "Independent,with set up";
            mob1 |= Exists("", "304239023", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Independent,stand by";
            mob1 |= Exists("", "304239024", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Independent,stand by";
            mob1 |= Exists("", "304239025", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Independent,stand by";
            mob1 |= Exists("", "304239026", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Independent,with set up";
            mob1 |= Exists("", "304239305", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Independent";
            mob1 |= Exists("", "304239282", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Independent";
            mob1 |= Exists("", "304239283", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Independent";
            mob1 |= Exists("", "304239027", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Independent,Uses assistive device,Standby assistance";
            mob1 |= Exists("", "304239028", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Independent,With set up";
            mob1 |= Exists("", "304239029", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Independent,With set up";
            mob1 |= Exists("", "304239030", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Independent,With set up";
            mob1 |= Exists("", "304239031", "", "", reslist, SearchDepth.SearchSince24Hrs);
            if (mob1)
                Program.VerboseAudit("MOBILITY: Independent charting found.");

            Program.VerboseAudit("MOBILITY: Partial charting search...");
            reslist = "Supervision,Close supervision,Contact guard,Handheld assist,Minimum assist,Moderate assist";
            mob2 |= Exists("", "304239023", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Minimal,Moderate";
            mob2 |= Exists("", "304239024", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Minimal,Moderate";
            mob2 |= Exists("", "304239025", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Minimal,Moderate";
            mob2 |= Exists("", "304239026", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Supervision,Close supervision,Contact guard,Minimum assistance,Moderate assistance";
            mob2 |= Exists("", "304239305", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Setup/Supervision,Minimum Assist/Patient Performed 75,Moderate Assist/Patient Performed 50-74";
            mob2 |= Exists("", "304239282", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Needs assistance";
            mob2 |= Exists("", "304239283", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Needs assistance";
            mob2 |= Exists("", "304239027", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "1 Person";
            mob2 |= Exists("", "304239028", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Supervision,Close supervision,Contact guard,Minimum assistance,Moderate assistance";
            mob2 |= Exists("", "304239029", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Supervision,Close supervision,Contact guard,Minimum assistance,Moderate assistance";
            mob2 |= Exists("", "304239030", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Supervision,Close supervision,Contact guard,Minimum assistance,Moderate assistance";
            mob2 |= Exists("", "304239031", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Partial";
            mob2 |= Exists("", "304239033", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "1";
            mob2 |= Exists("", "304239061", "", "", reslist, SearchDepth.SearchSince24Hrs);
            if (mob2)
                Program.VerboseAudit("MOBILITY: Partial charting found.");

            Program.VerboseAudit("MOBILITY: Complete charting search...");
            reslist = "Maximum assist";
            mob3 |= Exists("", "304239023", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = EXACT_MATCH_PREFIX + "Dependent";
            mob3 |= Exists("", "304239023", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Maximal,Total";
            mob3 |= Exists("", "304239024", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Maximal,Total";
            mob3 |= Exists("", "304239025", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Maximal,Total";
            mob3 |= Exists("", "304239026", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Maximum assistance";
            mob3 |= Exists("", "304239305", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = EXACT_MATCH_PREFIX + "Dependent";
            mob3 |= Exists("", "304239305", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Maximum Assist/Patient Perf,Total Assistance/Patient Perf";
            mob3 |= Exists("", "304239282", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = EXACT_MATCH_PREFIX + "Dependent";
            mob3 |= Exists("", "304239283", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = EXACT_MATCH_PREFIX + "Dependent";
            mob3 |= Exists("", "304239027", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "2 Persons,3 Persons,Unable to ambulate";
            mob3 |= Exists("", "304239028", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Maximum assistance";
            mob3 |= Exists("", "304239029", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = EXACT_MATCH_PREFIX + "Dependent";
            mob3 |= Exists("", "304239029", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Maximum assistance";
            mob3 |= Exists("", "304239030", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = EXACT_MATCH_PREFIX + "Dependent";
            mob3 |= Exists("", "304239030", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Maximum assistance";
            mob3 |= Exists("", "304239031", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = EXACT_MATCH_PREFIX + "Dependent";
            mob3 |= Exists("", "304239031", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Total,Non-weight bearing";
            mob3 |= Exists("", "304239033", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "2,3,4";
            mobCOMPL = Exists("", "304239061", "", "", reslist, SearchDepth.SearchSince24Hrs);
            if (mob3 || mobCOMPL)
                Program.VerboseAudit("MOBILITY: Complete charting found.");

            Program.VerboseAudit("TOILETING: Independent charting search...");
            reslist = "Independent";
            toi1 |= Exists("", "304239307", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Independent,Modified independent,With set up";
            toi1 |= Exists("", "304239324", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Independent,Modified independent,With set up";
            toi1 |= Exists("", "304239322", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Independent,Modified independent,With set up";
            toi1 |= Exists("", "304239323", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Independent";
            toi1 |= Exists("", "304239003,3042311241", "", "", reslist, SearchDepth.SearchSince24Hrs);
            if (toi1)
                Program.VerboseAudit("TOILETING: Independent charting found.");

            Program.VerboseAudit("TOILETING: Partial charting search...");
            reslist = "Setup/Supervision,Minimum Assist/Patient Perf,Moderate Assist/Patient Perf";
            toi2 |= Exists("", "304239307", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Supervision,Close supervision,Contact guard,Minimum assistance,Moderate assistance";
            toi2 |= Exists("", "304239324", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Supervision,Close supervision,Contact guard,Minimum assistance,Moderate assistance";
            toi2 |= Exists("", "304239322", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Supervision,Close supervision,Contact guard,Minimum assistance,Moderate assistance";
            toi2 |= Exists("", "304239323", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "1";
            toi2 |= Exists("", "3042301011", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "50,75,Setup/Supervision";
            toi2 |= Exists("", "304239003,3042311241", "", "", reslist, SearchDepth.SearchSince24Hrs);
        reslist = "Incontinent";
        toi2 |= Exists("", "304239318", "", "", reslist, SearchDepth.SearchSince24Hrs);
        reslist = "Yes";
        toi2 |= Exists("", "304239319", "", "", reslist, SearchDepth.SearchSince24Hrs);
        reslist = "Incontinence"; // Incontinent daily,Always incontinent";
        toi2 |= Exists("", "304239032", "", "", reslist, SearchDepth.SearchSince24Hrs);
        reslist = "Incontinence";
        toi2 |= Exists("", "304239320", "", "", reslist, SearchDepth.SearchSince24Hrs);
        reslist = "Occasionally incontinent,Frequently incontinent,Always incontinent";
        toi2 |= Exists("", "304239321", "", "", reslist, SearchDepth.SearchSince24Hrs);


            if (toi2)
                Program.VerboseAudit("TOILETING: Partial charting found.");

            Program.VerboseAudit("TOILETING: Complete charting search...");
            reslist = "Maximum Assist/Patient Perf,Total Assistance/Patient Perf";
            toi3 |= Exists("", "304239307", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Maximum assistance";
            toi3 |= Exists("", "304239324", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = EXACT_MATCH_PREFIX + "Dependent";
            toi3 |= Exists("", "304239324", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Maximum assistance";
            toi3 |= Exists("", "304239322", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = EXACT_MATCH_PREFIX + "Dependent";
            toi3 |= Exists("", "304239322", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Maximum assistance";
            toi3 |= Exists("", "304239323", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = EXACT_MATCH_PREFIX + "Dependent";
            toi3 |= Exists("", "304239323", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Incontinence"; // Incontinent daily,Always incontinent";
            toi3 |= Exists("", "304239032", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Incontinence";
            toi3 |= Exists("", "304239320", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Frequently incontinent,Always incontinent";
            toi3 |= Exists("", "304239321", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "External catheter,Indwelling catheter,Nephrostomy tube,Suprapubic catheter,Urostomy";
            toi3 |= Exists("", "304239034", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "25";
            toi3 |= Exists("", "304239003,3042311241", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "";
            toi3 |= Exists("", "304239218", "", "", reslist, SearchDepth.SearchSince24Hrs);

    reslist = "Incontinent";
    bool toi3_18 = (CountItems("", "304239318", "", "", reslist) >= 2);
    reslist = "Yes";
    bool toi3_19 = (CountItems("", "304239319", "", "", reslist) >= 2);
    reslist = "Incontinence"; // Incontinent daily,Always incontinent";
    bool toi3_32 = (CountItems("", "304239032", "", "", reslist) >= 2);
    reslist = "Incontinence";
    bool toi3_20 = (CountItems("", "304239320", "", "", reslist) >= 2);
    reslist = "Occasionally incontinent,Frequently incontinent,Always incontinent";
    bool toi3_21 = (CountItems("", "304239321", "", "", reslist) >= 2);
    bool toi3_incont = false;
    if (toi3_18 || toi3_19 || toi3_20 || toi3_21 || toi3_32)
    {
        toi3_incont = true;
        toi3 = true;
        Program.VerboseAudit("COMPLETE TOILETING: due to 2 or more incontinence in 12 hours.");
    }


            reslist = "2,3,4";
            toiCOMPL |= Exists("", "3042301011", "", "", reslist, SearchDepth.SearchSince24Hrs);
            if (toi3 || toiCOMPL)
                Program.VerboseAudit("TOILETING: Complete charting found.");

            if (!toi3 && !toiCOMPL)
            {
                CheckLDA("Urinary", "3042394421", "3042394422", "3042394424", "3042394425");
                if (g_toi4)
                {
                    toi3 = true;
                    Program.VerboseAudit("Urinary Catheter => Total Toileting");
                }

                
            }


            //if (feed2) Program.VerboseAudit("Partial Feeding present."); else Program.VerboseAudit("Partial Feeding Not present.");
            //if (hyg2) Program.VerboseAudit("Partial Hygiene present."); else Program.VerboseAudit("Partial Hygiene Not present.");
            //if (mob2) Program.VerboseAudit("Partial Mobility present."); else Program.VerboseAudit("Partial Mobility Not present.");
            //if (toi2) Program.VerboseAudit("Partial Toileting present."); else Program.VerboseAudit("Partial Toileting Not present.");

            string yesorno = "";
            if (feed2) yesorno = "YES"; else yesorno = "NO";
            Program.VerboseAudit("Partial Feeding present = " + yesorno);
            if (hyg2) yesorno = "YES"; else yesorno = "NO";
            Program.VerboseAudit("Partial Hygiene present = " + yesorno);
            if (mob2) yesorno = "YES"; else yesorno = "NO";
            Program.VerboseAudit("Partial Mobility present = " + yesorno);
            if (toi2) yesorno = "YES"; else yesorno = "NO";
            Program.VerboseAudit("Partial Toileting present = " + yesorno);


            if (feed3) yesorno = "YES"; else yesorno = "NO";
            Program.VerboseAudit("Total Feeding present = " + yesorno);
            if (hyg3) yesorno = "YES"; else yesorno = "NO";
            Program.VerboseAudit("Total Hygiene present = " + yesorno);
            if (mob3) yesorno = "YES"; else yesorno = "NO";
            Program.VerboseAudit("Total Mobility present = " + yesorno);
            if (toi3) yesorno = "YES"; else yesorno = "NO";
            Program.VerboseAudit("Total Toileting present = " + yesorno);

            bool feedx = (feed2 || feed3);
            bool hygx = (hyg2 || hyg3);
            bool mobx = (mob2 || mob3);
            bool toix = (toi2 || toi3);
            if (hygCOMPL || mobCOMPL || toiCOMPL)
                SetInd(3, "2+ staff for at least one ADL category.");
            if ((feedx ? 1 : 0) + (hygx ? 1 : 0) + (mobx ? 1 : 0) + (toix ? 1 : 0) == 4
                    && (feed3 || hyg3 || mob3 || toi3))
                SetInd(3, "All 4 ADL categories found at partial level or higher PLUS At least 1 at complete.");
            else if ((feedx ? 1 : 0) + (hygx ? 1 : 0) + (mobx ? 1 : 0) + (toix ? 1 : 0) > 0)
                SetInd(2, "At least 1 ADL category found at the partial or complete level.");
            if (feed1 || hyg1 || mob1 || toi1)
                SetInd(1, "At least 1 ADL category found at the independent level.");


            string s = "41343";
            desc_found = "";
            if (OrderInProgress(s, out desc_found))
                SetInd(3, "Order in effect: " + desc_found);

            //s = "40517";
            //if (OrderInProgress(s, out desc_found))
            //    SetInd(2, "ADL Review - Order in effect: " + desc_found);
            //s = "226916";
            //if (OrderInProgress(s, out desc_found))
            //    SetInd(2, "ADL Review - Order in effect: " + desc_found);

            //string codelist = "30423944255,30423944244,3023944211,30423944222";
            //SetIndIfResultContains(2, "", codelist, "", "", "");


            if (!_inds[1].is_checked && !_inds[2].is_checked && !_inds[3].is_checked)
            {
                SetInd(1, "No documentation found.  Defaulting to ADL Self");
            }

        }

        private void CheckLDA(string LDA_type, string startdt_id, string starttm_id, string enddt_id, string endtm_id)
        {
            DateTime uvc_start;
            DateTime uvc_end;
            string linenum;
            string placecodeline;
            string removecodedateline;
            string removecodetimeline;
            LDA_type = LDA_type.ToUpper();
            //Placement of UVC
            //3042394421 date
            //3042394422 time
            //900700 date
            //900701 time

            //Removal of UVC
            //3042394424
            //3042394425
            //900702
            //900703
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);    // add custom time range below
            query = query.Where(e => e.CODE.StartsWith(startdt_id));
            if (LDA_type == "URINARY")
                query = query.Where(e => e.DESCRIPTION.ToUpper().Contains(LDA_type) || e.DESCRIPTION.ToUpper().Contains("FECAL"));
            else if (LDA_type == "GI TUBE")
                query = query.Where(e => e.DESCRIPTION.ToUpper().Contains(LDA_type) || e.DESCRIPTION.ToUpper().Contains("GJ TUBE"));
            else
                query = query.Where(e => e.DESCRIPTION.ToUpper().Contains(LDA_type));
            query = query.Where(e => e.ORDER_STATUS == null || e.ORDER_STATUS == "");
            query = query.OrderBy(e => e.EVENT_DATETIME);
            if (query.Count() == 0) return;
            foreach (var item in query)
            { //we now have the date code for this line
                // get the line number for this LDA
                linenum = item.CODE.Substring(item.CODE.IndexOf("&") + 1);
                Program.VerboseAudit("code=" + item.CODE + "  linenum of " + LDA_type + "=" + linenum);
                // assemble the parts for the time code
                placecodeline = starttm_id + "&" + linenum;
                Program.VerboseAudit("placecodeline=" + placecodeline);
                var q2 = StartNewQuery(SearchDepth.SearchSinceAdmission);
                q2 = q2.Where(e => e.CODE == placecodeline);
                q2 = q2.Where(e => e.EVENT_DATETIME == item.EVENT_DATETIME);
                Program.VerboseAudit("placecodeline q2 code=" + placecodeline + " evdt=" + item.EVENT_DATETIME);
                Program.VerboseAudit("placecodeline q2 count=" + q2.Count());
                if (q2.Count() == 0)
                    uvc_start = PFSUtility.ISOToDateTime(item.RESULT + "0000");
                else
                {
                    foreach (var i2 in q2)
                    {
                        Program.VerboseAudit("Date " + item.RESULT + " Time=" + i2.RESULT.Substring(0, 4));
                        uvc_start = PFSUtility.ISOToDateTime(item.RESULT + i2.RESULT.Substring(0, 4));
                        Program.VerboseAudit("LDA " + i2.DESCRIPTION + " start=" + uvc_start.ToString());
                        removecodedateline = enddt_id + "&" + linenum;
                        var qremove = StartNewQuery(SearchDepth.SearchSinceAdmission);
                        qremove = qremove.Where(e => e.CODE == removecodedateline);
                        if (qremove.Count() == 0)
                        {
                            //if (LDA_type == "UVC")
                            //{
                            //    SetInd(12, "LDA " + i2.DESCRIPTION + " placed: " + uvc_start.ToString());
                            //    SetInd(19, "LDA " + i2.DESCRIPTION + " placed: " + uvc_start.ToString());
                            //}
                            if (LDA_type == "Urinary".ToUpper())
                            {
                                g_toi4 = true;
                            }
                            if (LDA_type == "GI Tube".ToUpper())
                            {
                                g_gitube = true;
                            }
                        }
                        else
                        {
                            foreach (var r1 in qremove)
                            {
                                removecodetimeline = endtm_id + "&" + linenum;
                                var qremove2 = StartNewQuery(SearchDepth.SearchSinceAdmission);
                                qremove2 = qremove2.Where(e => e.CODE == removecodetimeline);
                                if (qremove2.Count() == 0)
                                {
                                    uvc_end = PFSUtility.ISOToDateTime(r1.RESULT + "0000");
                                    if (_pat.pull_start >= uvc_end)
                                        DisableLDA(item.CODE);
                                    else if (_pat.pull_start >= uvc_start)
                                    {
                                        //if (LDA_type == "UVC")
                                        //{
                                        //    SetInd(12, "LDA " + r1.DESCRIPTION + " placed: " + uvc_start.ToString());
                                        //    SetInd(19, "LDA " + r1.DESCRIPTION + " placed: " + uvc_start.ToString());
                                        //}
                                        if (LDA_type == "Urinary".ToUpper())
                                        {
                                            g_toi4 = true;
                                        }
                                        if (LDA_type == "GI Tube".ToUpper())
                                        {
                                            g_gitube = true;
                                        }
                                    }
                                }
                                else
                                {
                                    foreach (var r2 in qremove2)
                                    {
                                        uvc_end = PFSUtility.ISOToDateTime(r1.RESULT + r2.RESULT.Substring(0, 4));
                                        Program.VerboseAudit("LDA " + LDA_type + " removal=" + uvc_end.ToString());
                                        if (_pat.pull_start >= uvc_end)
                                            DisableLDA(item.CODE);
                                        else if (_pat.pull_start >= uvc_start)
                                        {
                                            //if (LDA_type == "UVC")
                                            //{
                                            //    SetInd(12, "LDA " + r2.DESCRIPTION + " placed: " + uvc_start.ToString());
                                            //    SetInd(19, "LDA " + r2.DESCRIPTION + " placed: " + uvc_start.ToString());
                                            //}
                                            if (LDA_type == "Urinary".ToUpper())
                                            {
                                                g_toi4 = true;
                                            }
                                            if (LDA_type == "GI Tube".ToUpper())
                                            {
                                                g_gitube = true;
                                            }
                                        }

                                    }
                                }

                            }
                        }
                    }
                }
            }
            return;
        }

        private void DisableLDA(string exact_code)
        {
            var db = PFSDBUtility.NewSqlConnection();
            string q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and code='" + exact_code + "'";
            SqlCommand cmd = new SqlCommand(q, db);
            cmd.ExecuteNonQuery();
            db.Close();

        }

        private bool GetNWOrder(string CAordcode, DateTime CAts, DateTime CAevdt)
        { //Get NW order that wasn't saved because there was a previous CA order at same time.
            string sql;
            bool ret = false;
            //string room;
            //string dt;
            //bool readyforEDclass = true;
            //int car;
            //DateTime minlocdt = DateTime.MaxValue;

            string CAevdtISO = PFSUtility.DateTimeToISODateTime(CAevdt);

            sql = "select timestamp,";
            sql += " case when CHARINDEX('ORC|NW', source_text) > 0 then";
            sql += " substring(source_text, CHARINDEX('ORC|', source_text), 32) else null end as ORC,";

            sql += " case when CHARINDEX('OBR|', source_text) > 0 and CHARINDEX('|" + CAevdtISO + "', source_text) > 0 then";
            sql += " substring(source_text, CHARINDEX('OBR|', source_text), 200) else null end as OBR";

            sql += " from EVENT_LOG where TIMESTAMP between dateadd(minute,-10," + PFSDBUtility.SQLDateTime(CAts) + ") and dateadd(minute, 10," + PFSDBUtility.SQLDateTime(CAts) + ")";
            sql += " and (description like 'O01%') and description like '%" + _pat.acct + "%'";
            sql += " and event_source = 1 and event_type = 1 and event_category = 4";//NW item was not rejected
            Program.VerboseAudit(sql);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd2 = new SqlCommand(sql, db2);
            SqlDataReader dr2 = cmd2.ExecuteReader(System.Data.CommandBehavior.CloseConnection);

            string orcstr, obrstr, timestr;
            DateTime evdt = DateTime.MinValue;
            int seq = 0;
            while (dr2.Read())
            {
                orcstr = PFSDBUtility.DBToString(dr2["ORC"]);
                obrstr = PFSDBUtility.DBToString(dr2["OBR"]);
                if (orcstr.Trim() != "" && orcstr != null)
                {
                    //ORC|NW|669769268^EPC||3290947318
                    //OBR|1|669769268^EPC||40517^DIET^APEAP^^DIET||202004180752|202004180800

                    Program.VerboseAudit("Searching for unsaved order code: " + CAordcode + " at time " + CAevdt);
                    var arrorc = Program.SplitOnPipeAndPrepareElements(orcstr);
                    var arrobr = Program.SplitOnPipeAndPrepareElements(obrstr);
                    string ordid = "";
                    string ordctrl = "";
                    string ordcode = "";
                    string orddesc = "";

                    if (arrorc.GetUpperBound(0) >= 3 && arrobr.GetUpperBound(0) >= 4)
                    {
                        Program.VerboseAudit("Unsaved NW order ORC: " + orcstr);
                        Program.VerboseAudit("Unsaved NW order OBR: " + obrstr);

                        ordctrl = arrorc[1];
                        Program.VerboseAudit("ordctrl: " + ordctrl);
                        ordid = arrorc[2];
                        //VerboseAudit("ordid: " + ordid);
                        int ordidpos = ordid.IndexOf("^");
                        if (ordidpos > 0) ordid = ordid.Substring(0, ordidpos);
                        //VerboseAudit("ordidfinal: " + ordid);

                        ordcode = arrobr[4];
                        //VerboseAudit("ordcode: " + ordcode);
                        int ordcodepos = ordcode.IndexOf("^");
                        if (ordcodepos > 0)
                        {
                            orddesc = ordcode.Substring(ordcodepos + 1);
                            ordcode = ordcode.Substring(0, ordcodepos);
                        }
                        //VerboseAudit("ordcodefinal: " + ordcode);

                        if (ordcode.Trim() == CAordcode.Trim())
                        {
                            ret = true;
                            evdt = CAevdt.AddMinutes(-1);
                            Program.VerboseAudit("Adding: orderid=" + ordid + " ctrl=" + ordctrl + " code=" + ordcode + " evdt=" + evdt + " desc=" + orddesc);
                            if (arrorc.GetUpperBound(0) >= 2)
                            {
                                using (var db = PFSDBUtility.NewSqlConnection())
                                {
                                    seq++;
                                    //evdt = PFSUtility.ISOToDateTime(timestr);
                                    Program.VerboseAudit("Evdt=" + evdt.ToString());
                                    string q = "INSERT INTO chart_item (encounter_id,event_datetime,code,description,timestamp,sequence,unit_id,order_id,order_control)";
                                    q += " select @encid, @evdt, @code, @desc, @ts, @seq, @unit, @oid, @octrl";
                                    q += " where not exists (select encounter_id,code,event_datetime,unit_id,sequence from chart_item";
                                    q += " where encounter_id=" + _pat.encounter_id.ToString() + " and code='" + ordcode + "' and event_datetime='" + evdt.ToString() + "' and unit_id=-1 and sequence=" + seq + ")";
                                    //string q = "INSERT INTO chart_item (encounter_id,event_datetime,code,description,timestamp,sequence,unit_id,order_id) values (@encid, @evdt, @code, @desc, @ts,@seq,@unit,@oid)";
                                    SqlCommand cmd = new SqlCommand(q, db);
                                    cmd.Parameters.AddWithValue("@encid", _pat.encounter_id);
                                    cmd.Parameters.AddWithValue("@evdt", evdt);
                                    cmd.Parameters.AddWithValue("@code", ordcode);
                                    cmd.Parameters.AddWithValue("@desc", orddesc);
                                    cmd.Parameters.AddWithValue("@ts", DateTime.Now);
                                    cmd.Parameters.AddWithValue("@seq", seq);
                                    cmd.Parameters.AddWithValue("@unit", -1);
                                    cmd.Parameters.AddWithValue("@oid", ordid);
                                    cmd.Parameters.AddWithValue("@octrl", ordctrl);
                                    cmd.ExecuteNonQuery();
                                }

                            }
                        }// if codes match: add chart item 1 minute earlier
                    } // if arrays upper bounds are large enough
                } // if the orc and obr strings are not null
            } //dr read
            //dr2.Close();
            db2.Close();
            return ret;
        }


        //private bool OrderInProgress(string code, out string found_what)
        //{
        //    bool ret = false;
        //    found_what = "";
        //    //look for latest code with NW and order_status != 'x'
        //    var db = PFSDBUtility.NewPfsDataContext();
        //    var query = from item in db.CHART_ITEMs
        //                where (item.ENCOUNTER_ID == _pat.encounter_id)
        //                where (item.CODE == code)
        //                where (item.EVENT_DATETIME < DateTime.Now)
        //                where (item.ORDER_CONTROL.ToUpper() == "NW")
        //                where (item.ORDER_STATUS == null || item.ORDER_STATUS == "")
        //                orderby item.EVENT_DATETIME descending
        //                select item;
        //    foreach (var itemA in query)
        //    {
        //        //Program.VerboseAudit("Order ID=" + itemA.ORDER_ID + ": " + itemA.DESCRIPTION + " result=" + itemA.RESULT + " range:" + itemA.EVENT_DATETIME.ToString());
        //        var db2 = PFSDBUtility.NewPfsDataContext();
        //        var query2 = from item2 in db2.CHART_ITEMs
        //                     where (item2.CODE == code)
        //                     where (item2.ORDER_ID == itemA.ORDER_ID)
        //                     where (item2.EVENT_DATETIME >= itemA.EVENT_DATETIME)
        //                     where (item2.ORDER_CONTROL.ToUpper() == "CA")
        //                     select item2;
        //        int ct = query2.Count();
        //        if (ct > 0)
        //        {
        //            foreach (var x in query2)
        //            {
        //                if (x.EVENT_DATETIME >= _pat.pull_start)
        //                {
        //                    ret = true;
        //                    found_what = "Order ID=" + itemA.ORDER_ID + ": " + itemA.DESCRIPTION + " result=" + itemA.RESULT + " range:" + itemA.EVENT_DATETIME.ToString() + " - " + x.EVENT_DATETIME.ToString();
        //                }
        //                else
        //                {
        //                    DisableOrder(x.ORDER_ID);
        //                }
        //            }
        //        }
        //        else
        //        {
        //            ret = true;
        //            found_what += "Order ID=" + itemA.ORDER_ID + ": " + itemA.DESCRIPTION + " result=" + itemA.RESULT + " range:" + itemA.EVENT_DATETIME.ToString() + " - open";
        //        }

        //    }
        //    return ret;
        //}

        private bool OrderInProgress(string code, out string found_what)
        {
            bool ret = false;
            found_what = "";
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = query.Where(e => e.CODE.ToUpper() == code
                                  && e.EVENT_DATETIME < loc_out
                                  && e.ORDER_CONTROL.ToLower() == "nw"
                                  && (e.ORDER_STATUS == "" || e.ORDER_STATUS == null));
            query = query.OrderByDescending(e => e.EVENT_DATETIME);

            int count = query.Count();
            //Program.VerboseAudit("count order in progress=" + count);

            foreach (var itemA in query)
            {
                var query2 = StartNewQuery(SearchDepth.SearchSinceAdmission);
                query2 = query2.Where(e => e.CODE.ToUpper() == code);
                query2 = query2.Where(e => e.ORDER_ID == itemA.ORDER_ID);
                query2 = query2.Where(e => e.ORDER_CONTROL.ToLower() == "ca");
                int ct2 = query2.Count();

                if (ct2 > 0)
                {
                    Program.VerboseAudit("order in progress: order_id=" + itemA.ORDER_ID + " _pat.pull_start=" + _pat.pull_start + " ct2=" + ct2);
                    //                  Program.VerboseAudit("order in progress: q2ct=" + ct2);
                    foreach (var x in query2)
                    {
                        Program.VerboseAudit("x.event_datetime=" + x.EVENT_DATETIME);
                        if (x.EVENT_DATETIME >= _pat.pull_start)
                        {
                            ret = true;
                            found_what = "Order ID=" + itemA.ORDER_ID + ": " + itemA.DESCRIPTION + "; result=" + itemA.RESULT + "; range:" + PFSUtility.DateTimeToISODateTime(itemA.EVENT_DATETIME) + " - " + PFSUtility.DateTimeToISODateTime(x.EVENT_DATETIME);
                            Program.VerboseAudit("found_what=" + found_what);
                        }
                        else
                        {
                            DisableOrder(x.ORDER_ID);
                        }
                        ret |= GetNWOrder(code, x.TIMESTAMP, x.EVENT_DATETIME);
                    }
                }
                else
                {
                    ret = true;
                    found_what = "Order ID=" + itemA.ORDER_ID + ": " + itemA.DESCRIPTION + "; result=" + itemA.RESULT + "; starting:" + PFSUtility.DateTimeToISODateTime(itemA.EVENT_DATETIME);
                    //Program.VerboseAudit("AUDIT ORDER:" + found_what);
                }
            }


            return ret;
        }

        private void DisableOrder(string ordid)
        {
            //update ORDER_STATUS = 'X' for _pat.encounter_id and ordid
            var db = PFSDBUtility.NewSqlConnection();
            string q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and order_id='" + ordid + "'";
            SqlCommand cmd = new SqlCommand(q, db);
            cmd.ExecuteNonQuery();
            db.Close();
        }

        private bool RestraintOrderInProgress(string code, out string found_what)
        {
            bool ret = false;
            found_what = "";
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = query.Where(e => e.CODE.ToUpper() == code
                                  && e.EVENT_DATETIME < loc_out
                                  && e.ORDER_CONTROL.ToLower() == "nw"
                                  && (e.ORDER_STATUS == "" || e.ORDER_STATUS == null));
            query = query.OrderByDescending(e => e.EVENT_DATETIME);

            int count = query.Count();
            //Program.VerboseAudit("count order in progress=" + count);

            foreach (var itemA in query)
            {
                var query2 = StartNewQuery(SearchDepth.SearchSinceAdmission);
                query2 = query2.Where(e => e.CODE.ToUpper() == code);
                query2 = query2.Where(e => e.ORDER_ID == itemA.ORDER_ID);
                query2 = query2.Where(e => e.ORDER_CONTROL.ToLower() == "ca");
                int ct2 = query2.Count();

                if (ct2 > 0)
                {
                    Program.VerboseAudit("order in progress: order_id=" + itemA.ORDER_ID + " _pat.pull_start=" + _pat.pull_start + " ct2=" + ct2);
                    //                  Program.VerboseAudit("order in progress: q2ct=" + ct2);
                    foreach (var x in query2)
                    {
                        Program.VerboseAudit("x.event_datetime=" + x.EVENT_DATETIME);
                        if (x.EVENT_DATETIME >= _pat.pull_start)
                        {
                            ret = true;
                            found_what = "Order ID=" + itemA.ORDER_ID + ": " + itemA.DESCRIPTION + "; result=" + itemA.RESULT + "; range:" + itemA.EVENT_DATETIME.ToString() + " - " + x.EVENT_DATETIME.ToString();
                        }
                        else
                        {
                            DisableOrder(x.ORDER_ID);
                        }
                        ret |= GetNWOrder(code, x.TIMESTAMP, x.EVENT_DATETIME);
                    }
                }
                else
                {
                    if (itemA.EVENT_DATETIME.AddHours(2) > _pat.pull_start)
                    {
                        ret = true;
                        found_what = "Order ID=" + itemA.ORDER_ID + ": " + itemA.DESCRIPTION + "; result=" + itemA.RESULT + "; starting:" + itemA.EVENT_DATETIME.ToString() + "; Set to Auto-Expire at:" + itemA.EVENT_DATETIME.AddHours(2);
                    }
                    else
                        DisableOrder(itemA.ORDER_ID);
                    //Program.VerboseAudit("AUDIT ORDER:" + found_what);
                }
            }


            return ret;
        }

        //private bool Check_b79()
        //        {
        //            bool b = false;

        //            var query = StartNewQuery(SearchDepth.SearchSince13Hrs);
        //            query = query.Where(e => e.CODE.StartsWith("MED"));
        //            query = query.Where(e => e.DESCRIPTION.ToUpper().Contains("PEG3350 100 GRAM-SOD SUL") && e.DESCRIPTION.ToLower().Contains(";;;given"));
        //            b = (query.Count() > 0);
        //            return b;
        //        }

        private void Check_4()
        {
            string reslist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 4. ADL Supervision");
            Program.VerboseAudit("---------------");
            exclude_periop_data = true;

            if (_pat.age < 6.0) SetInd(4, "Age < 6 years");

            reslist = "Setup/Supervision";
            SetIndIfResultContains(4, "", "304239268", "", "", reslist);

            reslist = "Present";
            SetIndIfResultContains(4, "", "304239410", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(4, "", "304239411", "", "", reslist);
            reslist = "Oral Motor Exercises,Swallowing Exercises,Swallowing Treatment,Oral Motor/Feeding";
            SetIndIfResultContains(4, "", "304239035", "", "", reslist);
            reslist = "yes";
            SetIndIfResultContains(4, "", "304239036", "", "", reslist);
            reslist = "Supervised";
            SetIndIfResultContains(4, "", "30423501630", "", "", reslist);
            reslist = "One to one supervision,Close supervision,Distance supervision";
            SetIndIfResultContains(4, "", "304239261", "", "", reslist);
            reslist = "Supervision,Close Supervision";
            SetIndIfResultContains(4, "", "304239247", "", "", reslist);
            reslist = "Supervision,Close Supervision";
            SetIndIfResultContains(4, "", "304239248", "", "", reslist);
            reslist = "Supervision,Close Supervision";
            SetIndIfResultContains(4, "", "304239249", "", "", reslist);
            reslist = "Supervision,Close Supervision";
            SetIndIfResultContains(4, "", "304239250", "", "", reslist);
            reslist = "yes";
            SetIndIfResultContains(4, "", "3042309348", "", "", reslist);
            reslist = "Oral Motor Exercises,Swallowing Exercises,Swallowing Treatment,Oral Motor/Feeding";
            SetIndIfResultContains(4, "", "3042309349", "", "", reslist);


            reslist = "Setup/Supervision";
            SetIndIfResultContains(4, "", "304239282", "", "", reslist);
            SetIndIfResultContains(4, "", "304239307", "", "", reslist);

            reslist = "Supervision,Close Supervision";
            SetIndIfResultContains(4, "", "304239029", "", "", reslist);
            SetIndIfResultContains(4, "", "304239324", "", "", reslist);
            SetIndIfResultContains(4, "", "304239322", "", "", reslist);
            SetIndIfResultContains(4, "", "304239306", "", "", reslist);
            SetIndIfResultContains(4, "", "304239323", "", "", reslist);
            SetIndIfResultContains(4, "", "304239030", "", "", reslist);
            SetIndIfResultContains(4, "", "304239031", "", "", reslist);

            reslist = "Setup/Supervision";
            SetIndIfResultContains(4, "", "304239003", "", "", reslist);
            SetIndIfResultContains(4, "", "3042311241", "", "", reslist);

            string s = "";
            string desc_found = "";
            //if (OrderInProgress(s, out desc_found))
            //    SetInd(4, "Order in effect: " + desc_found);
            s = "38825";
            desc_found = "";
            if (OrderInProgress(s, out desc_found))
                SetInd(4, "Order in effect: " + desc_found);

            //desc_found = "";
            //GetNTEValues("40517");//Saves NTE value
            //if (OrderInProgress("40517", out desc_found))
            //{
            //    //npo = true;
            //    //Program.VerboseAudit("NPO in progress: ADL Feeding component set to dependent/complete.");
            //    //Program.VerboseAudit("order desc found=" + desc_found);
            //    if (desc_found.ToUpper().Contains("TYPE->DYSPHAGIA"))
            //    {
            //        SetInd(4, "Order in effect: " + desc_found);
            //    }
            //    else if (!desc_found.ToUpper().Contains("TYPE->NPO"))
            //    {
            //        if (desc_found.ToUpper().Contains("SWALLOW SAFETY"))
            //            SetInd(4, "Order in effect: " + desc_found);
            //    }
            //}

        }


        private void Check_5_6()
        {
            string reslist;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 5. Cognitive Support");
            Program.VerboseAudit("MH 6. Cognitive Support q1H");
            Program.VerboseAudit("---------------");
            exclude_periop_data = true;

            reslist = "Coma,Pharmaceutically paralyzed,Sedated,Unresponsive";
            if (Exists("", "304239339", "", "", reslist))
            {
                Program.VerboseAudit("Patient has no LOC.");
                return;
            }

            reslist = "Confused";
            SetIndIfResultContains(5, "", "304239382", "", "", reslist);
            reslist = "1=cannot do serial additions or is uncertain about date,2=disoriented for data by no more than 2 calendar days,3=disoriented for date by more than 2 calendar days,4=disoriented for place or person";
            SetIndIfResultContains(5, "", "304239384", "", "", reslist);
            reslist = "Forgetful";
            SetIndIfResultContains(5, "", "304239385", "", "", reslist);
            reslist = "Agitated/restless,Verbally abusive,Tearful,Hallucination,Delusional,Confused";
            SetIndIfResultContains(5, "", "304239380", "", "", reslist);
            reslist = "Agitated/restless,Verbally abusive,Tearful,Hallucination,Delusional,Confused";
            SetIndIfResultContains(5, "", "304239381", "", "", reslist);
            reslist = "Agitated,Combative,Confused,Uncooperative";
            SetIndIfResultContains(5, "", "304239388", "", "", reslist);
            reslist = "1=present";
            SetIndIfResultContains(5, "", "304239037", "", "", reslist);
            reslist = "1=present";
            SetIndIfResultContains(5, "", "304239038", "", "", reslist);
            reslist = "Recent memory,Remote memory,Other";
            SetIndIfResultContains(5, "", "304239039", "", "", reslist);
            reslist = "Impaired,Impulsive,Unable to follow commands,Other,Requires frequent redirection,Unable to redirect";
            SetIndIfResultContains(5, "", "304239383", "", "", reslist);
            //reslist = "Score  <13";
            SetIndIfResultBetween(5, "", "304239040", "", "", 0,12);
            reslist = "Yes";
            SetIndIfResultContains(6, "", "304239041", "", "", reslist);

            //reslist = "Clang associations,Disorganized,Distractibility,Echolalia";
            //reslist += ",Flight of ideas,Incoherent,Linear,Loose associations,Neologisms";
            //reslist += ",Over-inclusiveness,Perseverations,Racing thoughts,Thought blocking";
            reslist = "Disorganized,Distractibility,Flight of ideas,Loose associations,Perseverations";
            SetIndIfResultContains(5, "", "3042390016", "", "", reslist);

            SetIndIfResultContains(5, "", "3042390004", "", "", AVOID_NEGATIVE + "Delusions");
            SetIndIfResultContains(5, "", "3042390004", "", "", AVOID_NEGATIVE + "Hallucinations");
            SetIndIfResultContains(5, "", "3042390004", "", "", AVOID_NEGATIVE + "Violent or destructive thoughts");

            SetIndIfResultContains(5, "", "3042390005,3042390006", "", "", "");
        }
        //private bool AllOriented(string code1)
        //{
        //    int ct = 0;

        //    var query = StartNewQuery(SearchDepth.SearchSince24Hrs);    // add custom time range below
        //    query = query.Where(e => e.CODE.StartsWith(code1));
        //    query = query.Where(e => (e.RESULT.ToLower().StartsWith("oriented to person") || e.RESULT.ToLower().Contains(";oriented to person"))
        //    && (e.RESULT.ToLower().StartsWith("oriented to place") || e.RESULT.ToLower().Contains(";oriented to place"))
        //    && (e.RESULT.ToLower().StartsWith("oriented to time") || e.RESULT.ToLower().Contains(";oriented to time")));
        //    ct = query.Count();
        //    if (ct > 0)
        //        Program.VerboseAudit("All 3 Orientation found: " + query.Count());
        //    return (ct > 0);
        //}

        private void Check_7_8_9()
        {
            string reslist;
            string[] mhunits = { "BLK 11","West 5 BehavHlt","NSM Ad Epstein 5","NSM Ad Epstein 4","NSM Sr Epstein 3",
            "NWH 3EU3","2 SOUTH" ,"NSM Ch Epstein 2"};

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 7. Safety Management q30m");
            Program.VerboseAudit("MH 8. Safety Management q15m");
            Program.VerboseAudit("MH 9. Safety Management q5m");
            Program.VerboseAudit("---------------");
            exclude_periop_data = false;

            foreach (var n in mhunits)
            {
                if (_pat.unit_name.ToUpper() == n.ToUpper())
                {
                    SetInd(8, "Patient in behavioral unit: " + _pat.unit_name);
                }
            }

            
            //if (_pat.unit_name.ToUpper() == "NSM Ch Epstein 2".ToUpper())
            //{
            //    SetInd(9, "Patient in child behavioral unit: " + _pat.unit_name);
            //}

            string s = "";
            string desc_found = "";
            //if (OrderInProgress(s, out desc_found))
            //    SetInd(9, "Order in effect: " + desc_found);
            s = "38945";
            if (OrderInProgress(s, out desc_found))
                SetInd(9, "Order in effect: " + desc_found);
            s = "RES1";
            desc_found = "Adolescent Violent/Self-Destructive Behavior Restraint (9-17)";
            if (OrderInProgress(s, out desc_found))
                SetInd(9, "Order in effect: " + desc_found);

            //string codelist = "304239489,304239490,3040103562";
            //SetIndIfResultContains(9, "", codelist, "", "", "Yes");

            s = "3040000025401";
            desc_found = "";
            GetNTEValues(s);//Saves NTE value
            if (OrderInProgress(s, out desc_found))
            {
                Program.VerboseAudit("order desc found=" + desc_found);
                if (desc_found.ToUpper().Contains("Q30M"))
                    SetInd(7, "Order in effect: " + desc_found);
                else if (desc_found.ToUpper().Contains("Q15M"))
                    SetInd(8, "Order in effect: " + desc_found);
                else if (desc_found.ToUpper().Contains("Q5M") || desc_found.ToUpper().Contains(":1")) //could be 1:1 or 2:1
                    SetInd(9, "Order in effect: " + desc_found);
            }


        }

        private void GetNTEValues(string itemcode)
        {
            //look for c_i code 3040000025401 with order_ctrl=nw.  
            //if description contains ;;; then process order as usual.
            //else we need to get the NTE value.  if desc not contain ;;; then look for the event_log item that
            //had this message using code and timestamp. 
            //look at the NTE and update the c_i RESULT to the NTE values 
            //and for DESC concat ";;;<NTE value>" onto desc.
            //now this c_i has the value and ;;; to avoid re-saving it.
            var db = PFSDBUtility.NewPfsDataContext();
            var query = from item in db.CHART_ITEMs
                        where (item.ENCOUNTER_ID == _pat.encounter_id)
                        where (item.CODE == itemcode)
                        where (item.EVENT_DATETIME < loc_out)
                        where (item.ORDER_CONTROL.ToUpper() == "NW")
                        where (item.ORDER_STATUS == null || item.ORDER_STATUS == "")
                        where (item.RESULT == null || item.RESULT == "")
                        orderby item.EVENT_DATETIME descending
                        select item;
            foreach (var item in query)
            {
                //NTE|1||Observation Checks->Q15M | NTE | 2 || Unit Level->Restrict to Unit| NTE | 3 || Supervision->None |
                Program.VerboseAudit("Found safety orderid:" + item.ORDER_ID + " at ts:" + item.TIMESTAMP);
                AddNTEValues(itemcode, item.TIMESTAMP, item.ORDER_ID);
            }
        }

        //private void AddNTEValues(string code, DateTime ts, string ordid)
        //{
        //    string sql,res = "";
        //    string nte_str = "";

        //    sql = "select ";
        //    sql += " substring(source_text, CHARINDEX('NTE|',source_text), 400) as NTE";
        //    sql += " from EVENT_LOG where encounter_id=" + _pat.encounter_id;
        //    sql += " and TIMESTAMP between " + PFSDBUtility.SQLDateTime(ts) + " and " + PFSDBUtility.SQLDateTime(ts.AddSeconds(1));
        //    sql += " and (description like 'O01%') and description like '%" + _pat.acct + "%'";
        //    sql += " and CHARINDEX('|" + code + "',source_text) > 0 and CHARINDEX('NTE|',source_text) > 0";
        //    sql += " and CHARINDEX('|" + ordid + "',source_text) > 0";
        //    //Program.VerboseAudit(sql);
        //    var db2 = PFSDBUtility.NewSqlConnection();
        //    var cmd2 = new SqlCommand(sql, db2);
        //    SqlDataReader dr2 = cmd2.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
        //    if (dr2.HasRows)
        //    {
        //        //Program.VerboseAudit("Has rows");
        //        while (dr2.Read())
        //        {
        //            res = "";
        //            nte_str = PFSDBUtility.DBToString(dr2["NTE"]);
        //            //Program.VerboseAudit("NTE string=" + nte_str);
        //            // NTE string= NTE|1||Observation Checks->Q15M|NTE|2||Unit Level->Off unit with staff|NTE|3||Supervision->Sharps|
        //            int nteidx = 0;
        //            while (nte_str.Contains("NTE|"))
        //            { //iterate across the nte string until all NTEs have been found
        //                string s = nte_str.Substring(nteidx);
        //                var arr = s.Split('|');
        //                for (int i = 0; i <= arr.GetUpperBound(0); i++)
        //                {
        //                    arr[i] = arr[i].Trim();                         // get rid of leading blanks
        //                }
        //                if (arr.GetUpperBound(0) >= 3)
        //                {
        //                    res = res + arr[3] + ";";
        //                    //Program.VerboseAudit("res=" + res);
        //                }
        //                nte_str = nte_str.Substring(1);//advance the pointer to find the next NTE
        //                nteidx = nte_str.IndexOf("NTE|");//the next NTE sets the nteidx value
        //                if (nteidx > 0)
        //                {
        //                    nte_str = nte_str.Substring(nteidx);
        //                    nteidx = 0;
        //                }
        //            }
        //            var db = PFSDBUtility.NewSqlConnection();
        //            string q;
        //            if (res.Trim() == "")
        //            {
        //                //Program.VerboseAudit("No result value to add; update order status X");
        //                q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and order_id='" + ordid + "'";
        //                UpdatePtChartArrays(ordid, "X", "");
        //            }
        //            else
        //            {
        //                //Program.VerboseAudit("NTE result value to add: " + res);
        //                q = "UPDATE chart_item set RESULT='" + res + "' where encounter_id=" + _pat.encounter_id + " and order_id='" + ordid + "'";
        //                UpdatePtChartArrays(ordid, "", res);
        //            }
        //            SqlCommand cmd = new SqlCommand(q, db);
        //            cmd.ExecuteNonQuery();
        //            db.Close();
        //        }
        //    }
        //    else
        //    {
        //        var db = PFSDBUtility.NewSqlConnection();
        //        string q;
        //        //Program.VerboseAudit("No such event log found; update order status X");
        //        q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and order_id='" + ordid + "'";
        //        UpdatePtChartArrays(ordid, "X", "");
        //        SqlCommand cmd = new SqlCommand(q, db);
        //        cmd.ExecuteNonQuery();
        //        db.Close();
        //    }
        //    dr2.Close();
        //    db2.Close();
        //}

        private void AddNTEValues(string code, DateTime ts, string ordid)
        {
            string sql, res = "";
            string nte_str = "";
            const char quote = '\'';

            sql = "select ";
            sql += " substring(source_text, CHARINDEX('NTE|',source_text), 400) as NTE";
            sql += " from EVENT_LOG where 1=1"; //encounter_id=" + _pat.encounter_id;
            sql += " and TIMESTAMP between " + PFSDBUtility.SQLDateTime(ts.AddDays(-1)) + " and " + PFSDBUtility.SQLDateTime(ts.AddMinutes(30));
            sql += " and (description like 'O01%') and description like '%" + _pat.acct + "%'";
            sql += " and CHARINDEX('|" + code + "',source_text) > 0 and CHARINDEX('NTE|',source_text) > 0";
            sql += " and CHARINDEX('|" + ordid + "',source_text) > 0";
            Program.VerboseAudit(sql);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd2 = new SqlCommand(sql, db2);
            SqlDataReader dr2 = cmd2.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            if (dr2.HasRows)
            {
                //Program.VerboseAudit("Has rows");
                while (dr2.Read())
                {
                    res = "";
                    nte_str = PFSDBUtility.DBToString(dr2["NTE"]);
                    Program.VerboseAudit("Aidx of apostr=" + nte_str.IndexOf(quote));
                    nte_str = nte_str.Replace(quote, ' ');
                    Program.VerboseAudit("Bidx of apostr=" + nte_str.IndexOf(quote));
                    //Program.VerboseAudit("NTE string=" + nte_str);
                    // NTE string= NTE|1||Observation Checks->Q15M|NTE|2||Unit Level->Off unit with staff|NTE|3||Supervision->Sharps|
                    int nteidx = 0;
                    while (nte_str.Contains("NTE|"))
                    { //iterate across the nte string until all NTEs have been found
                        Program.VerboseAudit("1nte_str=" + nte_str);
                        string s = nte_str.Substring(nteidx);
                        var arr = s.Split('|');
                        for (int i = 0; i <= arr.GetUpperBound(0); i++)
                        {
                            arr[i] = arr[i].Trim();                         // get rid of leading blanks
                        }
                        if (arr.GetUpperBound(0) >= 3)
                        {
                            res = res + arr[3] + ";";
                            //Program.VerboseAudit("res=" + res);
                        }
                        nte_str = nte_str.Substring(1);//advance the pointer to find the next NTE
                        Program.VerboseAudit("2nte_str=" + nte_str);
                        nteidx = nte_str.IndexOf("NTE|");//the next NTE sets the nteidx value
                        if (nteidx > 0)
                        {
                            nte_str = nte_str.Substring(nteidx);
                            Program.VerboseAudit("3nte_str=" + nte_str);
                            nteidx = 0;
                        }
                    }
                    var db = PFSDBUtility.NewSqlConnection();
                    string q;
                    if (res.Trim() == "")
                    {
                        //Program.VerboseAudit("No result value to add; update order status X");
                        q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and order_id='" + ordid + "'";
                        UpdatePtChartArrays(ordid, "X", "");
                    }
                    else
                    {
                        //Program.VerboseAudit("NTE result value to add: " + res);
                        q = "UPDATE chart_item set RESULT='" + res + "' where encounter_id=" + _pat.encounter_id + " and order_id='" + ordid + "'";
                        UpdatePtChartArrays(ordid, "", res);
                    }
                    SqlCommand cmd = new SqlCommand(q, db);
                    cmd.ExecuteNonQuery();
                    db.Close();
                }
            }
            else
            {
                var db = PFSDBUtility.NewSqlConnection();
                string q;
                //Program.VerboseAudit("No such event log found; update order status X");
                q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and order_id='" + ordid + "'";
                UpdatePtChartArrays(ordid, "X", "");
                SqlCommand cmd = new SqlCommand(q, db);
                cmd.ExecuteNonQuery();
                db.Close();
            }
            dr2.Close();
            db2.Close();
        }


        private void UpdatePtChartArrays(string orderid, string ordstatus, string newres)
        {
            foreach (var item in _chart_items_since24hrs)
            {
                if (item.ORDER_ID == orderid)
                {
                    if (ordstatus != "")
                        item.ORDER_STATUS = ordstatus;
                    if (newres != "")
                        item.RESULT = newres;
                }
            }
        }


        //private int CheckSafety()
        //{
        //    int ind = 0, ct = 0;
        //    string[] safety_list = { "q30", "q15", "q5", "Line of sight", "Continuous observation by RN with patient", "Continuous observation by non-RN staff with patient", "Continuous observation by two staff with patient" };

        //    var query = StartNewQuery(SearchDepth.SearchSince24Hrs);
        //    query = query.Where(e => e.CODE.StartsWith("9993040009234"));
        //    query = query.Where(e => safety_list.Any(item => e.RESULT.ToLower().StartsWith(item.ToLower())));
        //    query = query.OrderByDescending(e => e.EVENT_DATETIME);
        //    ct = query.Count();
        //    if (ct > 0)
        //    {
        //        string res = query.First().RESULT;
        //        ind = 9;
        //        if (res.ToLower().StartsWith("q30")) ind = 7;
        //        if (res.ToLower().StartsWith("q15")) ind = 8;
        //        SetInd(ind, "Latest result is = " + res);
        //    }
        //    return ind;
        //}


        private void Check_10_11_12_13_14()
        {
            string reslist;
            //int ct;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 10. Behavior/Emotional Management");
            Program.VerboseAudit("MH 11. Behavior/Emotional Mgmt-q4H");
            Program.VerboseAudit("MH 12. Behavior/Emotional Mgmt-q2H");
            Program.VerboseAudit("MH 13. Behavior/Emotional Mgmt-q1H");
            Program.VerboseAudit("MH 14. Behavior/Emotional Mgmt-q30m");
            Program.VerboseAudit("---------------");
            exclude_periop_data = false;

            reslist = "Agitated,Anxious,Angry,Aggressive physically,Aggressive verbally,Combative,Distracted,Flat affect,Impulsive,Inappropriate,Labile,Sad,Non-compliant,Not interactive,Tearful,Uncooperative,Withdrawn,Disinhibited,Fearful,Restless,Other";
            SetIndIfResultContains(10, "", "3042393971", "", "", reslist);

            reslist = "Anxious,Affect inconsistent with mood,Aggressive physically,Aggressive verbally,Flat affect,Non-compliant,Non-supportive,Tearful,Uncooperative,Other";
            SetIndIfResultContains(10, "", "304239399", "", "", reslist);
            reslist = "Anxious,Affect inconsistent with mood,Aggressive physically,Aggressive verbally,Flat affect,Non-compliant,Non-supportive,Tearful,Uncooperative,Other";
            SetIndIfResultContains(10, "", "304239400", "", "", reslist);
            reslist = "Anger,Anxiety,Denial,Depression,Fearful,Non-verbal,Sadness";
            SetIndIfResultContains(10, "", "304239401", "", "", reslist);
            reslist = "Anger,Anxiety,Denial,Depression,Fearful,Non-verbal,Sadness";
            SetIndIfResultContains(10, "", "304239402", "", "", reslist);

            reslist = "Agitation,Anxiety,Combative,Forgetful,Irritable,Wandering";
            if (Exists("", "304239385", "", "", reslist))
            {
                reslist = "";
                SetIndIfResultContains(10, "", "304239403", "", "", reslist);
            }

            reslist = "+4=Combative - violent, immediate danger to self,+3=Very agitated - pulls or removes tubes, aggressive,+2=Agitated - Frequent non-purposeful movement, fights ventilator,+1=Restless - Anxious but movements not aggressive vigorous";
            SetIndIfResultContains(10, "", "304239326", "", "", reslist);
            reslist = "4=moderately fidgety and restless,5,6,7=paces back and forth during most of the interview, or constantly thrashes about";
            SetIndIfResultContains(10, "", "304239404", "", "", reslist);
            reslist = "Agitated/restless,Verbally abusive,Tearful,Hallucination,Delusional";
            SetIndIfResultContains(10, "", "304239380", "", "", reslist);
            reslist = "Agitated/restless,Verbally abusive,Tearful,Hallucination,Delusional";
            SetIndIfResultContains(10, "", "304239381", "", "", reslist);
            //reslist ="";
            //SetIndIfResultContains(10, "", "304239405", "", "", reslist);
            reslist = "Agitated,Combative,Uncooperative";
            SetIndIfResultContains(10, "", "304239388", "", "", reslist);
            reslist = "Self,Physical,Verbal,Other";
            SetIndIfResultContains(10, "", "304239406", "", "", reslist);
            reslist = "1=Patient reports increasing irritability or anxiousness,2=Patient obviously irritable anxious,4=Patient so irritable or anxious that participation in the assessment is difficult";
            SetIndIfResultContains(10, "", "304239407", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(10, "", "304239409", "", "", reslist);
            reslist = "Present";
            SetIndIfResultContains(10, "", "304239410", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(10, "", "304239411", "", "", reslist);
            reslist = "Impulsive";
            SetIndIfResultContains(10, "", "304239412", "", "", reslist);
            reslist = "1=present";
            SetIndIfResultContains(10, "", "304239413", "", "", reslist);
            reslist = "1=present";
            SetIndIfResultContains(10, "", "304239414", "", "", reslist);
            reslist = "Generalized,Panic attack,Chest pain,Compulsive,Counting,Excessive sweating,Feelings of doom,Obsessions,Palpitations,Ritualistic behavior,Shortness of breath,Social phobias,Unexplained fears,Other";
            SetIndIfResultContains(10, "", "3042390001", "", "", reslist);
            reslist = "Appetite change,Change in energy level,Crying,Decreased libido,Feelings of helplessness,Feelings of hopelessness,Feelings of worthlessness,Impaired concentration,Increased irritability,Isolative,Loss of interest,Psychomotor retardation,Sleep disturbance,Other";
            SetIndIfResultContains(10, "", "3042390002", "", "", reslist);
            reslist = "Flight of ideas,Grandiosity,Hypersexuality,Increased energy,Increased spending,Labile,Less need to sleep,Poor judgment,Pressured speech,Psychomotor agitation,Rapid cycling,Other";
            SetIndIfResultContains(10, "", "3042390003", "", "", reslist);
            reslist = "Other";
            SetIndIfResultContains(10, "", "3042390004", "", "", reslist);

            SetIndIfResultContains(10, "", "3042390004", "", "", AVOID_NEGATIVE + "Delusions");
            SetIndIfResultContains(10, "", "3042390004", "", "", AVOID_NEGATIVE + "Hallucinations");
            SetIndIfResultContains(10, "", "3042390004", "", "", AVOID_NEGATIVE + "Violent or destructive thoughts");
            SetIndIfResultContains(10, "", "3042390004", "", "", AVOID_NEGATIVE + "Homicidality");
            SetIndIfResultContains(10, "", "3042390004", "", "", AVOID_NEGATIVE + "Suicidality");



            reslist ="";
            SetIndIfResultContains(10, "", "3042390005", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(10, "", "3042390006", "", "", reslist);
            reslist = "Impaired,Other";
            SetIndIfResultContains(10, "", "3042390007", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(10, "", "3042390008", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(10, "", "3042390009", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(10, "", "3042390010", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(10, "", "3042390011", "", "", reslist);
            reslist = "Did not interact,Disorganized,Disruptive,Participated with support,Refused,Other";
            SetIndIfResultContains(10, "", "3042390012", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(10, "", "3042390013", "", "", reslist);
            reslist = "Angry,Blunted/flat,Constricted,Incongruent,Wide,Other";
            SetIndIfResultContains(10, "", "3042390014", "", "", reslist);
            reslist = "Agitated,Catatonic,Compulsive,Withdrawn,Echopraxic,Exit seeking,Guarded,Hostile,Hypersexual,Impulsive,Motor perseveration,Preoccupied,Psychomotor retardation,Psychomotor Agitation,Ritualistic,Tearful,Other";
            SetIndIfResultContains(10, "", "3042390015", "", "", reslist);
            reslist = "Circumstantial/tangential,Clang associations,Disorganized,Distractibility,Echolalia,Flight of ideas,Incoherent,Linear,Loose associations,Neologisms,Over-inclusiveness,Perseverations,Racing thoughts,Thought blocking,Other";
            SetIndIfResultContains(10, "", "3042390016", "", "", reslist);
            reslist = "Attended with prompts,Came Late,Left early,Self-initiated,Other";
            SetIndIfResultContains(10, "", "3042390017", "", "", reslist);
            reslist = "Attended with prompts,Came Late,Left early,Self-initiated,Other";
            SetIndIfResultContains(10, "", "3042390018", "", "", reslist);
            reslist = "Attended with prompts,Came Late,Left early,Self-initiated,Other";
            SetIndIfResultContains(10, "", "3042390019", "", "", reslist);

            if (_inds[10].is_checked)
            {
                reslist = "q4 hour";
                SetIndIfResultContains(11, "", "3042390020", "", "", reslist);
                reslist = "q2 hour";
                SetIndIfResultContains(12, "", "3042390020", "", "", reslist);
                reslist = "q1 hour";
                SetIndIfResultContains(13, "", "3042390020", "", "", reslist);
                reslist = "q30 minutes";
                SetIndIfResultContains(14, "", "3042390020", "", "", reslist);
            }

            if (!_inds[14].is_checked)
            {
                string s = "304000050101";
                string desc_found = "";
                if (RestraintOrderInProgress(s, out desc_found))
                    SetInd(14, "Restraint Order in effect: " + desc_found);

                s = "30400000250";
                desc_found = "";
                if (RestraintOrderInProgress(s, out desc_found))
                    SetInd(14, "Restraint Order in effect: " + desc_found);

                s = "30400000206";
                desc_found = "";
                if (RestraintOrderInProgress(s, out desc_found))
                    SetInd(14, "Restraint Order in effect: " + desc_found);

            }
        }

        private int GetMaxBehavInd(DateTime start, DateTime fin, out DateTime classdt)
        {
            int ind = 0;
            classdt = DateTime.MinValue;
            //get assessment indicator at location out time = loc_out_time
            var db = PFSDBUtility.NewPfsDataContext();        // look for the unit default admission profile
            var query = from ce in db.CLASSIFICATION_EVENTs
                        from ia in db.INDICATOR_ANSWERs
                        where (ce.CLASSIFICATION_EVENT_ID == ia.CLASSIFICATION_EVENT_ID)
                        && (ce.ENCOUNTER_ID == _pat.encounter_id)
                        && (ce.CLASSIFICATION_DATETIME >= start)
                        && (ce.CLASSIFICATION_DATETIME < fin)
                        && (ia.INDICATOR_NUMBER >= 10 && ia.INDICATOR_NUMBER <= 14)
                        select new
                        {
                            ia.INDICATOR_NUMBER,
                            ce.CLASSIFICATION_DATETIME
                        };
            if (query.Count() > 0)
            {
                //ind = query.First().INDICATOR_NUMBER;
                //classdt = query.First().CLASSIFICATION_DATETIME;
                foreach (var c in query)
                {
                    if (ind < c.INDICATOR_NUMBER)
                    {
                        ind = c.INDICATOR_NUMBER;
                        classdt = c.CLASSIFICATION_DATETIME;
                    }
                }
            }
            return ind;
        }

        // (this isn't really a Q1h count -- it is just a count) Make this a q1hr count
        //private int ReturnQ1HrCount(string code_list, string result_list)
        //{
        //    var buckets = new List<gBucket>();
        //    SetBucketSize(60);
        //    AddBuckets(buckets, "", code_list, "", "", result_list);
        //    return CountBuckets(buckets);
        //}

        //private bool IsQ1Hour(int count)
        //{
        //    return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q1H);
        //}
        //private bool IsQ2Hour(int count)
        //{
        //    return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q2H);
        //}

        private void Check_15()
        {
            string reslist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 15. Behavior Prevention Plan");
            Program.VerboseAudit("---------------");
            exclude_periop_data = false;
            
            //reslist = "";
            //SetIndIfResultContains(15, "", "304239242", "", "", reslist);
            //reslist = "Date restraint episode started";
            //SetIndIfResultContains(15, "", "304239243", "", "", reslist);
            //reslist ="";
            //SetIndIfResultContains(15, "", "304239244", "", "", reslist);
            //reslist = "Goals and revisions updated,Patient and staff have signed and dated the safety plan,Patient given a copy of their safety plan,Patient involved in his/her safety plan,Patient verbalizes willingness to work with staff,Other";
            //SetIndIfResultContains(15, "", "304239245", "", "", reslist);
            reslist = "Yes";
            SetIndIfResultContains(15, "", "304239246", "", "", reslist);

        }

        private void Check_16_17()
        {
            string reslist;
            string codelist;
            bool found22 = false;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 16. Medication Management q4H"); //3-5 Psymed
            Program.VerboseAudit("MH 17. Medication Management q2H"); //6 or more Psymed
            Program.VerboseAudit("---------------");
            exclude_periop_data = true;

            reslist = "";
            SetIndIfResultContains(16, "", EXACT_MATCH_PREFIX + "58670", "", "", reslist);
            SetIndIfResultContains(16, "", EXACT_MATCH_PREFIX + "58677", "", "", reslist);
            SetIndIfResultContains(16, "", EXACT_MATCH_PREFIX + "58669", "", "", reslist);
            SetIndIfResultContains(16, "", EXACT_MATCH_PREFIX + "58667", "", "", reslist);
            SetIndIfResultContains(16, "", EXACT_MATCH_PREFIX + "58666", "", "", reslist);
            SetIndIfResultContains(16, "", EXACT_MATCH_PREFIX + "66651", "", "", reslist);
            SetIndIfResultContains(16, "", EXACT_MATCH_PREFIX + "66653", "", "", reslist);
            SetIndIfResultContains(16, "", EXACT_MATCH_PREFIX + "60001", "", "", reslist);
            SetIndIfResultContains(16, "", EXACT_MATCH_PREFIX + "58672", "", "", reslist);

            //            "Via Psychotropic Meds extract file:

            //Trigger Indicator #16 ""Medication Management Q4"" 
            //if  two or more ""pyschotropic"" medications are charted in 4 hours."
            //"Via Psychotropic Meds extract file:

            //Trigger Indicator #16 ""Medication Management Q2"" 
            //if  two or more ""pyschotropic"" medications are charted in 2 or less  hours."

            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = query.Where(e => e.CODE.ToUpper().StartsWith("MED"));
            query = query.Where(e => e.ORDER_ID != ""); //has to be a PSYMED order
            query = query.OrderBy(e => e.EVENT_DATETIME);
            int medct = query.Count();
            //if (_pat.los_hours >= 10)
            //{
            //    if (medct >= 6)
            //        SetInd(17, "Med count is 6 or more = " + medct + " in 12 hours.");
            //    else if (medct >= 3)
            //        SetInd(16, "Med count is 3-5 = " + medct + " in 12 hours.");
            //}

            var queryb = (from item in query
                          select new { code = item.CODE, desc = item.DESCRIPTION, orderid = item.ORDER_ID, evdt = item.EVENT_DATETIME }
                           ).Distinct();
            if (queryb.Count() == 0) return;

            MedChartItem[] medarr = new MedChartItem[queryb.Count()+1];
            int counter = 0;
            string s = "";
            foreach (var med in queryb)
            {
                var q2 = StartNewQuery(SearchDepth.SearchSinceAdmission);
                q2 = q2.Where(e => e.CODE.ToUpper().StartsWith("PSYMED"));
                q2 = q2.Where(e => e.ORDER_ID == med.orderid);
                if (q2.Count() > 0)
                {
                    counter++;
                    medarr[counter].code = med.code;
                    medarr[counter].orderid = med.orderid;
                    medarr[counter].evdt = med.evdt;
                    Program.VerboseAudit("Psy Med: " + med.evdt + " desc="+med.desc + " under orderid="+med.orderid);
                }
            }
            if (_pat.los_hours >= 8)  //use 8 hours as minimum los
            {
                if (counter >= 6)
                {
                    SetInd(17, "6 or more PsyMeds in " + _pat.los_hours + " hours. Count=" + counter);
                }
                else if (counter >= 2)
                {
                    SetInd(16, "2-5 PsyMeds in " + _pat.los_hours + " hours. Count=" + counter);
                }
                else if (counter > 0)
                {
                    Program.VerboseAudit("PsyMeds count of " + counter + " does not qualify for q4.");
                }

            }

            //if (counter < 0) return;

            //int ct = 0;
            //DateTime starttm, endtm;
            //for (int i = 0; (i <= counter); i++)
            //{
            //    starttm = medarr[i].evdt;
            //    endtm = starttm.AddHours(2);
            //    ct = 0;
            //    for (int j = i; (j <= counter); j++)
            //    {
            //        if (medarr[j].evdt >= starttm && medarr[j].evdt <= endtm)
            //        {
            //            ct++;
            //        }
            //    }
            //}

            //if (found22) return;

            //if (_inds[16].is_checked) return;

            //ct = 0;
            //for (int i = 0; (i <= counter); i++)
            //{
            //    starttm = medarr[i].evdt;
            //    endtm = starttm.AddHours(4);
            //    ct = 0;
            //    for (int j = i; (j <= counter); j++)
            //    {
            //        if (medarr[j].evdt >= starttm && medarr[j].evdt <= endtm)
            //        {
            //            ct++;
            //        }
            //    }
            //}


            //List<gBucket> buckets;
            //buckets = new List<gBucket>();
            //codelist = "304239221,304239222,304239223,304239224,304239225,304239226,304239227";
            //codelist += ",304239228,304239229,304239230,304239231,304239232,304239233,304239234";
            //codelist += ",304239235,304239236,304239237,304239238";
            //AddBuckets(buckets, "", codelist, "", "", "", SearchDepth.SearchSince9Hrs);
            //codelist = "9990000344170,3040007191,304000344160,30400012412,304000344150,9991070011101";
            //AddBuckets(buckets, "", codelist, "", "", "", SearchDepth.SearchSince9Hrs);
            //reslist = "No untoward effects noted,Use of reversal agent,Hypoxemia < 90";
            //reslist += ",Hypotension of bradycardia requiring intervention";
            //reslist += ",Respiratory failure requiring intervention";
            //reslist += ",Cardiac arrest or death";
            //reslist += ",Sedation recovery time > 60";
            //reslist += ",Unplanned admission or higher level of care";
            //reslist += ",Respiratory distress";
            //reslist += ",Unanticipated need for anesthesia involvement";
            //reslist += ",Inability to complete procedure";
            //reslist += ",No responsible adult for discharge escort,Other";
            //AddBuckets(buckets, "", "9991600100259", "", "", reslist, SearchDepth.SearchSince9Hrs);
            //AddBuckets(buckets, "", "DIPS,ORD06", "", "", "", SearchDepth.SearchSince9Hrs);

            //if (CheckMeds(buckets))
            //    SetInd(16, "Med class 7,33,1,2,6 or named med");

            //AnalyzeBuckets(buckets, 17, 150, "Medication Management q2H");
            //AnalyzeBuckets(buckets, 16, 300, "Medication Management q4H");

            //buckets = new List<gBucket>();
            //int count = CheckPsychoTherapeutics(buckets);
            //if (count >= 2)
            //{
            //    SetInd(16, "Found " + count + " meds of one drugclass (37,38,44)");
            //    AnalyzeBuckets(buckets, 17, 150, "drugclass(37,38,44) Meds q2H");
            //    //AnalyzeBuckets(buckets, 16, 300, "drugclass(37,38,44) Meds q4H");
            //}
            Program.VerboseAudit("Checking for IV or IM PsyMeds...");
            var query2 = StartNewQuery(SearchDepth.SearchDefault);
            query2 = query2.Where(e => e.CODE.ToUpper().StartsWith("MED"));
            query2 = query2.Where(e => e.DESCRIPTION.ToLower().Contains(";;intrav") || e.DESCRIPTION.ToLower().Contains(";;intram"));
            query2 = query2.Where(e => e.ORDER_ID != ""); //has to be a PSYMED order
            query2 = query2.OrderBy(e => e.EVENT_DATETIME);
            int medct2 = query2.Count();

            var queryb2 = (from item in query2
                          select new { code = item.CODE, desc = item.DESCRIPTION, orderid = item.ORDER_ID, evdt = item.EVENT_DATETIME }
                           ).Distinct();
            if (queryb2.Count() == 0)
            {
                Program.VerboseAudit("...none found.");
                return;
            }

            MedChartItem[] medarr2 = new MedChartItem[queryb2.Count() + 1];
            int counter2 = 0;
            foreach (var med in queryb2)
            {
                var q3 = StartNewQuery(SearchDepth.SearchSinceAdmission);
                q3 = q3.Where(e => e.CODE.ToUpper().StartsWith("PSYMED"));
                q3 = q3.Where(e => e.ORDER_ID == med.orderid);
                if (q3.Count() > 0)
                {
                    counter2++;
                    medarr[counter].code = med.code;
                    medarr[counter].orderid = med.orderid;
                    medarr[counter].evdt = med.evdt;
                    Program.VerboseAudit("Psy Med: " + med.evdt + " desc=" + med.desc + " under orderid=" + med.orderid);
                }
            }
            if (counter2 > 0)
                SetInd(17, "At least one IV or IM PsyMed. Count=" + counter2);
            else
                Program.VerboseAudit("...none found.");
        }

        //private int CheckPsychoTherapeutics(List<gBucket> bucket_list)
        //{
        //    string descript = "";
        //    string drugclass = "";
        //    string rte = "";
        //    string result = "";
        //    string[] meds_rate = { "newbag", "new bag", "rateverify", "rate verify", "restarted", "ratechange", "rate change", "started" };
        //    string[] medclass = { "37", "38", "44" };
        //    bool found = false;
        //    int ct = 0;
        //    int dclass1=0, dclass2=0, dclass3 = 0;

        //    var query = StartNewQuery(SearchDepth.SearchSince9Hrs);    // add custom time range below
        //    query = query.Where(e => e.CODE.ToLower().StartsWith("med"));
        //    query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;given"));
        //    query = query.Where(e => e.EVENT_DATETIME <= PFSUtility.MaxDateTime(_pat.unit_departure, _pat.effective_out));
        //    query = query.OrderBy(e => e.EVENT_DATETIME);
        //    foreach (var item in query)
        //    {
        //        var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
        //        if (arr.GetUpperBound(0) >= 3)
        //        {
        //            descript = arr[0];
        //            drugclass = arr[1];
        //            rte = arr[3];
        //            result = "";
        //            if (arr.Length == 5) result = arr[4];
        //            //Program.VerboseAudit("med desc: " + descript + "//" + "med class: " + drugclass + "//" + "med result: " + result);
        //            //exclude #29, 35, 48
        //            bool bres1 = result.ToLower().Contains("given");
        //            if (drugclass == "37") dclass1++;
        //            if (drugclass == "38") dclass2++;
        //            if (drugclass == "44") dclass3++;
        //            if ((medclass.Contains(drugclass)) && bres1)
        //            {
        //                Program.VerboseAudit("drugclass(37,38,44) med code: " + item.CODE + " desc: " + item.DESCRIPTION + "  evdt: " + item.EVENT_DATETIME.ToString());
        //                ct++;
        //                var b = new gBucket();
        //                b.bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size);
        //                b.code = item.CODE;
        //                b.evdt = item.EVENT_DATETIME;
        //                b.has_all_deps = true;
        //                bucket_list.Add(b);
        //            }
        //        }
        //    }
        //    return Math.Max(dclass1,Math.Max(dclass2,dclass3));

        //}

        //        private bool CheckMeds(List<gBucket> bucket_list)
        //        {
        //            string descript = "";
        //            string drugclass = "";
        //            string rte = "";
        //            string result = "";
        //            string[] meds_rate = { "newbag", "new bag", "rateverify", "rate verify", "restarted", "ratechange", "rate change", "started" };
        //            string[] medclass = { "7", "33", "1", "2", "6" };
        //            bool found = false;

        //            string[] pamed1417 = { "Zofran","Ondansetron","Compazine","Prochlorperazine",
        //"Phenergan","Promethazine","Reglan","Metoclopramide","Marinol","Dronabinol","Ativan",
        //"Lorazepam","Imodium","Loperamide","Pepto-Bismol","Kaopectate","Bismuth subsalicylate",
        //"Lomotil","Atropine/diphenoxylate","Tincure of opium","Lactulose"};


        //            var query = StartNewQuery(SearchDepth.SearchSince9Hrs);    // add custom time range below
        //            query = query.Where(e => e.CODE.ToLower().StartsWith("med"));
        //            // query = query.Where(e => e.DESCRIPTION.Contains(";;;7;;;") || e.DESCRIPTION.Contains(";;;33;;;") || e.DESCRIPTION.Contains(";;;1;;;") || e.DESCRIPTION.Contains(";;;2;;;") || e.DESCRIPTION.Contains(";;;6;;;"));
        //            query = query.Where(e =>
        //                medclass.Any(item1 => e.DESCRIPTION.Contains(";;;" + item1 + ";;;")) ||
        //               pamed1417.Any(item2 => e.DESCRIPTION.ToUpper().StartsWith(item2.ToUpper())));
        //            query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;given") || e.DESCRIPTION.ToLower().Contains(";;;syringe") || e.DESCRIPTION.ToLower().ContainsAny(meds_rate) && (e.UNIT_ID == _pat.unit_id) || e.DESCRIPTION.ToLower().Contains(";;;new"));
        //            query = query.Where(e => e.EVENT_DATETIME <= PFSUtility.MaxDateTime(_pat.unit_departure, _pat.effective_out));
        //            query = query.OrderBy(e => e.EVENT_DATETIME);
        //            //            Program.VerboseAudit("Num Meds found: " + query.Count());
        //            foreach (var item in query)
        //            {
        //                var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
        //                if (arr.GetUpperBound(0) >= 3)
        //                {
        //                    descript = arr[0];
        //                    drugclass = arr[1];
        //                    rte = arr[3];
        //                    result = "";
        //                    if (arr.Length == 5) result = arr[4];
        //                }
        //                //Program.VerboseAudit("med desc: " + descript + "//" + "med class: " + drugclass + "//" + "med result: " + result);
        //                //exclude #29, 35, 48
        //                bool bres1 = false;
        //                bool bres2 = false;
        //                bool bclass1 = false;
        //                bool bclass2 = false;
        //                bool bclass3 = false;
        //                bool bmed1 = false;
        //                bres1 = result.ToLower().Contains("given");
        //                bres2 = result.ToLower().Contains("syringe") || result.ToLower().ContainsAny(meds_rate) || result.ToLower().Contains("newbag") || result.ToLower().Contains("new bag");
        //                bclass1 = (drugclass == "1") || (drugclass == "2") || (drugclass == "6");
        //                bclass2 = (drugclass == "7");
        //                bclass3 = (drugclass == "33") && (rte.ToLower() == "sc");
        //                bmed1 = pamed1417.Any(item2 => descript.ToUpper().StartsWith(item2.ToUpper()));
        //                if ((bres1 && (bclass2 || bclass3)) ||
        //                    ((bres1 || bres2) && bclass1) ||
        //                    bmed1)
        //                {
        //                    Program.VerboseAudit("====Med found====");
        //                    Program.VerboseAudit("med code: " + item.CODE + " desc: " + item.DESCRIPTION + "  evdt: " + item.EVENT_DATETIME.ToString());
        //                    found = true;
        //                    //SetInd(16, "Med class 7,33,1,2,6 or med name");
        //                    var b = new gBucket();
        //                    b.bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size);
        //                    b.code = item.CODE;
        //                    b.evdt = item.EVENT_DATETIME;
        //                    b.has_all_deps = true;
        //                    bucket_list.Add(b);
        //                }
        //            }
        //            return found;

        //        }


        private void CheckAssessment(int count, string desc)
        {
            //if (_inds[18].is_checked) return;          //skip if highest already checked
            if (count == 0) return;                    //skip if none

            // This should work the same as the original code:
            switch (FreqForCount(_pat.los_hours, count))
            {
                case Frequencies.Q30M:
                    SetInd(18, desc);
                    break;
                case Frequencies.Q1H:
                    SetInd(17, desc);
                    break;
                case Frequencies.Q2H:
                    SetInd(16, desc);
                    break;
                case Frequencies.Q4H:
                    SetInd(15, desc);
                    break;
                default:
                    break;
            }

        }

        private void ShowBuckets(List<gBucket> buckets)
        {
            string s = "";
            foreach (var e in buckets)
            {
                s = "(" + e.bucket + "," + e.code + "," + e.evdt.ToString() + ")";
                Program.VerboseAudit("Assessmt item:=" + s);
            }
        }

        private void Check_18_19_20()
        {
            string reslist = "";
            string codelist = "";
            List<gBucket> buckets;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 18. Physiological Assessment q4H");
            Program.VerboseAudit("MH 19. Physiological Assessment q2H");
            Program.VerboseAudit("MH 20. Physiological Assessment q1H");
            Program.VerboseAudit("---------------");

            exclude_periop_data = true;

            SetBucketSize(60);
            string freqstr = "====Q4/Q2/Q1 HR EVALUATION================";
            Program.VerboseAudit(freqstr + " bucket size=60mins");

            buckets = new List<gBucket>();
            codelist = "304239239,304239240,304239241,304239566,304239567,304239568,304239569";
            codelist += ",304239570,304239571,304239572,304239573,304239574,304239575,304239576";
            codelist += ",304239577,304239578,304239579,304239580,304239581,304239582,304239583";
            codelist += ",304239584,304239585";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, 18, 60, "Cardio", true);

            buckets = new List<gBucket>();
            codelist = "304239221,304239222,304239223,304239224,304239225,304239226,304239227";
            codelist += ",304239228,304239229,304239230,304239231,304239232,304239233,304239234";
            codelist += ",304239235,304239236,304239237,304239238,304239339,304239503,304239504";
            codelist += ",304239505,304239506,304239507,304239508,304239382,304239551,304239552";
            codelist += ",304239553,304239554,304239555,304239556,304239557,304239387,304239559";
            codelist += ",304239560,304239561,304239385,304239562,304239326,304239655";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, 18, 60, "Neuro", true);

            buckets = new List<gBucket>();
            codelist = "304239492,304239509,304239586,304239587,304239588,304239589";
            codelist += ",304239590,304239591,304239592";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, 18, 60, "Pulmonary", true);

            //Vitals
            buckets = new List<gBucket>();
            codelist = "304239497,304239498,304239461,304239462,304239593,304239594,304239595,304239596,304239597";
            codelist += ",304239598,304239599,304239600,304239601,304239602,304239603,304239604,304239605,304239606";
            codelist += ",304239607,304239608,304239609,304239610,304239611,304239612,304239613,304239614,304239615";
            codelist += ",304239616,304239617,304239618,304239619,304239620,304239621,304239622,304239623,304239624";
            codelist += ",304239625,304239626,304239627,304239628,304239629,304239631,304239632,304239633";
            codelist += ",304239634,304239635,304239636,304239637,304239638,304239639,304239640,304239641,304239642";
            codelist += ",304239643,304239644,304239645,304239646,304239647,304239648";
            codelist += ",3042396285,3042396286,3042396287,30423344220";
            AddVitalsBuckets(buckets, "", codelist, "", "", "", SearchDepth.SearchDefault);
            AnalyzeBuckets(buckets, 18, 60, "Vitals", true);

            //buckets = new List<gBucket>();
            //codelist = "304239598,304239599,304239600,304239602,304239603";
            //AddBuckets(buckets, "", codelist, "", "");
            //AnalyzeBuckets(buckets, 18, 60, "VS", true);
        }

        private void AddVitalsBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field, string result_list, SearchDepth search_depth)
        {
            // Assign (virtual) bucket numbers to each assessment.  
            // Every x minutes after that is a new bucket.  Bucket zero = pull_start time.
            Program.VerboseAudit("----Locating Vital Signs: Filling buckets...Bucketsize=" + _bucket_size);

            // get the chart items for the assessments
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc, field, result_list);

            var query3 = from item in query
                         select new
                         {
                             bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                             code = item.CODE,
                             evdt = item.EVENT_DATETIME
                         };
            var query4 = from item in query3
                         orderby item.bucket, item.code
                         select item;

            // Add to the list
            int currb = -1;
            //            int prevb = -1;
            string currcode = "xx";
            DateTime currevdt = DateTime.MinValue;
            int currct = 0;
            //          bool addcurr = false;
            //Data is: 0, "123"
            //         0, "234"  0733
            //         0, "234"  0734
            //         1, "123"
            //         2, "234"
            //         2, "345"
            foreach (var item in query4)
            {
                //Program.VerboseAudit("q4 item: b=" + item.bucket + " c=" + item.code + " dt=" + item.evdt);
                if (currb != item.bucket)
                {
                    currb = item.bucket;
                    currcode = item.code;
                    currevdt = item.evdt;
                    currct = 1;
                }
                else
                {
                    if (currcode != item.code)
                    {  //minimum of 2 different VS codes
                        if (currct == 1)
                        { // add the first code first
                            var b = new gBucket();
                            b.bucket = item.bucket;
                            b.code = currcode;
                            b.evdt = currevdt;
                            b.has_all_deps = true;
                            //gBucket f = bucket_list.Find(x => x.evdt == item.evdt);
                            //if (f.evdt != item.evdt) bucket_list.Add(b);
                            bucket_list.Add(b);
                            Program.VerboseAudit("  adding item1: b=" + b.bucket + " c=" + b.code + " dt=" + b.evdt);
                        }
                        currct++;
                        currcode = item.code; //guarantees not to add code again in same bucket

                        var b2 = new gBucket();
                        b2.bucket = item.bucket;
                        b2.code = item.code;
                        b2.evdt = item.evdt;
                        b2.has_all_deps = true;
                        //gBucket f = bucket_list.Find(x => x.evdt == item.evdt);
                        //if (f.evdt != item.evdt) bucket_list.Add(b);
                        bucket_list.Add(b2);
                        Program.VerboseAudit("  adding item2: b=" + b2.bucket + " c=" + b2.code + " dt=" + b2.evdt);
                    }
                }
            }

            // This is just for auditing: Say what we are looking for and how many were found
            if (!String.IsNullOrEmpty(result_list))
            {
            }
            else
            {
            }
            Program.VerboseAudit("----End of Locating Vital Signs: Filling buckets...Bucketsize=" + _bucket_size);
        }

        //private void AnalyzeAssessments(int ind, int bucket_size)
        //{
        //    string codelist;
        //    string reslist;
        //    List<gBucket> buckets;

        //    SetBucketSize(bucket_size);

        //    buckets = new List<gBucket>();
        //    codelist = "304234175,304234176,304234177,304238371,304238372,304238373,304238374,304238375";
        //    codelist += ",304238376,304238377,304238378,304238379,304238380,304238381,304238382,304238383";
        //    codelist += ",304238384,304238385,304238386,304238387,304238388,304238389,304238390,304238485";
        //    AddBuckets(buckets, "", codelist, "", "");
        //    NEWAnalyzeBuckets(buckets, ind, bucket_size, "Cardiovascular", true);

        //    buckets = new List<gBucket>();
        //    codelist = "304234157,304234158,304234159,304234160,304234161,304234162,304234163,304234164";
        //    codelist += ",304234165,304234166,304234167,304234168,304234169,304234170,304234171,304234172";
        //    codelist += ",304234173,304234174,304238101,304238288,304238289,304238291,304238292,304238293";
        //    codelist += ",304238294,304238144,304238354,304238355,304238356,304238357,304238360,304238361";
        //    codelist += ",304238362,304238149,304238364,304238365,304238366,304238147,304238367,304238088";
        //    codelist += ",304238481";
        //    AddBuckets(buckets, "", codelist, "", "", "");
        //    NEWAnalyzeBuckets(buckets, ind, bucket_size, "Neuro", true);

        //    buckets = new List<gBucket>();
        //    codelist = "304238254,304238295,304238391,304238392,304238393,304238394,304238395,304238396";
        //    codelist += ",304238397";
        //    AddBuckets(buckets, "", codelist, "", "", "");
        //    NEWAnalyzeBuckets(buckets, ind, bucket_size, "Pulmonary", true);

        //    buckets = new List<gBucket>();
        //    codelist = "304238406,304238407,304238408,304238410,304238411";
        //    AddBuckets(buckets, "", codelist, "", "", "");
        //    NEWAnalyzeBuckets(buckets, ind, bucket_size, "Vitals",true);



        //}

        //private void AddDependentBuckets(List<gBucket> bucket_list, string codelist1, string reslist1, string codelist2, string reslist2)
        //{
        //    AddDependentBuckets(bucket_list, codelist1, reslist1, codelist2, reslist2, "Phantom", "");
        //}
        //private void AddDependentBuckets(List<gBucket> bucket_list, string codelist1, string reslist1, string codelist2, string reslist2, string codelist3, string reslist3)
        //{
        //    AddDependentBuckets(bucket_list, codelist1, reslist1, codelist2, reslist2, codelist3, reslist3, "Phantom", "");
        //}

        //private void AddDependentBuckets(List<gBucket> bucket_list, string codelist1, string reslist1, string codelist2, string reslist2, string codelist3, string reslist3, string codelist4, string reslist4)
        //{
        //    bool dep3 = true;
        //    bool dep4 = true;
        //    // get the chart items for the assessments
        //    var query1 = StartNewQuery(SearchDepth.SearchDefault);
        //    query1 = AndItemFilter(query1, "", codelist1, "", "", reslist1);
        //    //Program.VerboseAudit("query1:" + query1.Count() + " bucketsize:" + _bucket_size);
        //    //foreach (var x in query1)
        //    //{
        //    //    var s1 = "(" + x.CODE + "," + x.EVENT_DATETIME.ToString() + ")";
        //    //    Program.VerboseAudit("query1a item:=" + s1);
        //    //}

        //    var query1b = (from item in query1
        //                       //                        select new { bnum = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size) }).Distinct();
        //                   select new { evdt = item.EVENT_DATETIME }
        //                   ).Distinct();
        //    //Program.VerboseAudit("query1b:" + query1b.Count());

        //    var query2 = StartNewQuery(SearchDepth.SearchDefault);
        //    query2 = AndItemFilter(query2, "", codelist2, "", "", reslist2);
        //    //Program.VerboseAudit("query2:" + query2.Count());
        //    //foreach (var x in query2)
        //    //{
        //    //    var s1 = "(" + x.CODE + "," + x.EVENT_DATETIME.ToString() + ")";
        //    //    Program.VerboseAudit("query2a item:=" + s1);
        //    //}
        //    var query2b = (from item in query2
        //                       //select new { bnum = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size) }).Distinct();
        //                   select new { evdt = item.EVENT_DATETIME }
        //                   ).Distinct();
        //    //Program.VerboseAudit("query2b:" + query2b.Count());

        //    if (codelist3.Trim() == "Phantom")
        //    {
        //        dep3 = false;
        //        codelist3 = "Hello this is a phantom code";
        //    }
        //    var query3 = StartNewQuery(SearchDepth.SearchDefault);
        //    query3 = AndItemFilter(query3, "", codelist3, "", "", reslist3);
        //    //Program.VerboseAudit("query3:" + query3.Count());
        //    //foreach (var x in query3)
        //    //{
        //    //    var s1 = "(" + x.CODE + "," + x.EVENT_DATETIME.ToString() + ")";
        //    //    Program.VerboseAudit("query3a item:=" + s1);
        //    //}
        //    var query3b = (from item in query3
        //                       //                           select new { bnum = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size) }).Distinct();
        //                   select new { evdt = item.EVENT_DATETIME }
        //                   ).Distinct();
        //    //Program.VerboseAudit("query3b:" + query3b.Count());


        //    if (codelist4.Trim() == "Phantom")
        //    {
        //        dep4 = false;
        //        codelist4 = "Hello this is a phantom code";
        //    }
        //    var query4 = StartNewQuery(SearchDepth.SearchDefault);
        //    query4 = AndItemFilter(query4, "", codelist4, "", "", reslist4);
        //    //Program.VerboseAudit("query4:" + query4.Count());
        //    var query4b = (from item in query4
        //                       //                           select new { bnum = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size) }).Distinct();
        //                   select new { evdt = item.EVENT_DATETIME }
        //                   ).Distinct();
        //    //Program.VerboseAudit("query4b:" + query4b.Count());

        //    // figure out what buckets the events belong to
        //    var query1a = from item1x in query1b
        //                  select new
        //                  {
        //                      bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item1x.evdt) / _bucket_size), //  bucket = item1x.bnum,
        //                      code = codelist1,
        //                      evdt = item1x.evdt // _pat.pull_start.AddMinutes(item1x.bnum * _bucket_size)
        //                  };
        //    //foreach (var x in query1a)
        //    //{
        //    //    var s = "(" + x.bucket + "," + x.code + "," + x.evdt.ToString() + ")";
        //    //    Program.VerboseAudit("query1a item:=" + s);
        //    //}
        //    var query2a = from item2x in query2b
        //                  select new
        //                  {
        //                      bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item2x.evdt) / _bucket_size), //bucket = item2x.bnum,
        //                      code = codelist2,
        //                      evdt = item2x.evdt // _pat.pull_start.AddMinutes(item2x.bnum * _bucket_size)
        //                  };
        //    //foreach (var x in query2a)
        //    //{
        //    //    var s = "(" + x.bucket + "," + x.code + "," + x.evdt.ToString() + ")";
        //    //    Program.VerboseAudit("query2a item:=" + s);
        //    //}
        //    var query3a = from item3x in query3b
        //                  select new
        //                  {
        //                      bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item3x.evdt) / _bucket_size),//item3x.bnum,
        //                      code = codelist3,
        //                      evdt = item3x.evdt //_pat.pull_start.AddMinutes(item3x.bnum * _bucket_size)
        //                  };
        //    //foreach (var x in query3a)
        //    //{
        //    //    var s = "(" + x.bucket + "," + x.code + "," + x.evdt.ToString() + ")";
        //    //    Program.VerboseAudit("query3a item:=" + s);
        //    //}
        //    var query4a = from item4x in query4b
        //                  select new
        //                  {
        //                      bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item4x.evdt) / _bucket_size), // item4x.bnum,
        //                      code = codelist4,
        //                      evdt = item4x.evdt //_pat.pull_start.AddMinutes(item4x.bnum * _bucket_size)
        //                  };

        //    int i = 0;
        //    // Add to the list IFF items in both lists occur in same bucket
        //    foreach (var item1 in query1a)
        //    {
        //        foreach (var item2 in query2a)
        //        {
        //            if (item1.bucket == item2.bucket)
        //            {
        //                if (dep3)
        //                {
        //                    foreach (var item3 in query3a)
        //                    {
        //                        if (item1.bucket == item3.bucket)
        //                        {
        //                            if (dep4)
        //                            {
        //                                foreach (var item4b in query4a)
        //                                {
        //                                    if (item1.bucket == item4b.bucket)
        //                                    {
        //                                        var b = new gBucket();
        //                                        b.bucket = item1.bucket;
        //                                        b.code = item1.code;
        //                                        b.evdt = item1.evdt;
        //                                        //b.has_all_deps = (item1.evdt == item2.evdt && item1.evdt == item3.evdt && item1.evdt == item4b.evdt);
        //                                        b.has_all_deps = (item1.evdt.AddMinutes(-15) <= item2.evdt && item1.evdt.AddMinutes(15) >= item2.evdt &&
        //                                                          item1.evdt.AddMinutes(-15) <= item3.evdt && item1.evdt.AddMinutes(15) >= item3.evdt &&
        //                                                          item1.evdt.AddMinutes(-15) <= item4b.evdt && item1.evdt.AddMinutes(15) >= item4b.evdt);
        //                                        if (b.has_all_deps)
        //                                        {
        //                                            b.num_addl_items = 3;
        //                                            gBucket f = bucket_list.Find(x => x.evdt == item1.evdt);
        //                                            if (f.evdt != item1.evdt) bucket_list.Add(b);
        //                                        }
        //                                        //Program.VerboseAudit("4b:"+i++.ToString());
        //                                    }
        //                                }
        //                            }
        //                            else
        //                            {
        //                                if (item1.bucket == item3.bucket)
        //                                {
        //                                    var b = new gBucket();
        //                                    b.bucket = item1.bucket;
        //                                    b.code = item1.code;
        //                                    b.evdt = item1.evdt;
        //                                    //b.has_all_deps = (item1.evdt == item2.evdt && item1.evdt == item3.evdt);
        //                                    b.has_all_deps = (item1.evdt.AddMinutes(-15) <= item2.evdt && item1.evdt.AddMinutes(15) >= item2.evdt &&
        //                                                      item1.evdt.AddMinutes(-15) <= item3.evdt && item1.evdt.AddMinutes(15) >= item3.evdt);
        //                                    if (b.has_all_deps)
        //                                    {
        //                                        b.num_addl_items = 2;
        //                                        gBucket f = bucket_list.Find(x => x.evdt == item1.evdt);
        //                                        if (f.evdt != item1.evdt) bucket_list.Add(b);
        //                                    }
        //                                    //Program.VerboseAudit("3b:" + i++.ToString());
        //                                }

        //                            }
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    if (item1.bucket == item2.bucket)
        //                    {
        //                        var b = new gBucket();
        //                        b.bucket = item1.bucket;
        //                        b.code = item1.code;
        //                        b.evdt = item1.evdt;
        //                        //b.has_all_deps = (item1.evdt == item2.evdt);
        //                        b.has_all_deps = (item1.evdt.AddMinutes(-15) <= item2.evdt && item1.evdt.AddMinutes(15) >= item2.evdt);
        //                        if (b.has_all_deps)
        //                        {
        //                            b.num_addl_items = 1;
        //                            Program.VerboseAudit("  hasall=" + b.has_all_deps);
        //                            gBucket f = bucket_list.Find(x => x.evdt == item1.evdt);
        //                            if (f.evdt != item1.evdt) bucket_list.Add(b);
        //                        }
        //                        //Program.VerboseAudit("2b:" + i++.ToString());
        //                    }
        //                }
        //            }
        //        }
        //    }

        //}



        private bool AnalyzeBuckets(List<gBucket> buckets, int ind, int bucketsize, string group)
        {
            return AnalyzeBuckets(buckets, ind, bucketsize, group, true);
        }

        private bool AnalyzeBuckets(List<gBucket> buckets, int ind, int bucketsize, string group, bool set_ind)
        {
            DateTime dt = DateTime.MinValue;
            int bnum = 0;
            List<gBucket> dtlist = new List<gBucket>();

            bool all_ok = OLDAnalyzeBuckets(buckets, ind, bucketsize, group, set_ind);
            return all_ok;

            //bool all_ok = NEWAnalyzeBuckets(buckets, ind, bucketsize, group, set_ind);
            //return all_ok;

        }

        private bool OLDAnalyzeBuckets(List<gBucket> buckets, int ind, int bucketsize, string group, bool set_ind)
        {
            DateTime dt = DateTime.MinValue;
            int bnum = 0;
            int numbucket = -99;
            int lobucket = 99;
            int hibucket = 0;
            int numitems = 0;
            int numconsec = 0;
            int greatestnumconsec = 0;
            bool all_ok = false;
            List<gBucket> dtlist = new List<gBucket>();
            Program.VerboseAudit("----Begin Assessment Group = " + group + " ---- bucketsizew=" + bucketsize);

            //Program.VerboseAudit("buckets count=" + buckets.Count());
            var b = buckets.OrderBy(e => e.evdt).ToList();

            //numitems = buckets.Count();
            foreach (var item in b)
            {
                if (dt < item.evdt)
                {
                    dt = item.evdt;
                    numitems++;
                    Program.VerboseAudit(item.bucket + ") " + item.evdt.ToString() + " := " + item.code);
                }
                if (numbucket < item.bucket)
                {
                    //add dt to ary
                    bnum++;
                    dtlist.Add(item);
                    if (numbucket == -99 || item.bucket - numbucket == 1)
                    {
                        numconsec++;
                        if (greatestnumconsec < numconsec)
                            greatestnumconsec = numconsec;
                    }
                    else
                    {
                        numconsec = 1;
                    }
                    numbucket = item.bucket;
                    if (hibucket < item.bucket) hibucket = item.bucket;
                    if (lobucket > item.bucket) lobucket = item.bucket;
                    Program.VerboseAudit(item.bucket + ")." + item.evdt.ToString() + " := " + item.code);// + "  [distinct times only]");
                }
            }

            if (bnum <= 1)
            {
                Program.VerboseAudit("---- End Assessment Group = " + group + " ----");
                return false;
            }

            Program.VerboseAudit("numitems=" + numitems);
            Program.VerboseAudit("bucket count=" + bnum);
            int half_los = (int)(_pat.los_hours * (30.0 / bucketsize));//for q30 this will be los_hours.  for q60 this will be .5 * los_hours
            Program.VerboseAudit("half-LOS bucket count=" + half_los + " at " + bucketsize + "mins each.");
            Program.VerboseAudit("greatest consec=" + greatestnumconsec);

            double bucketratio = (hibucket - lobucket) / (1.0 * (bnum - 1));
            if (greatestnumconsec >= half_los || bucketratio <= 1.5 && bnum >= half_los - 1)
            {
                SetInd(20 * Convert.ToInt32(set_ind), "Qualifies for q1hr because chartings are at least q1hr for duration of half-LOS=" + half_los + " maxconsec=" + greatestnumconsec);
                all_ok = true;
            }
            else if (bucketratio < 3 && bnum >= 3 &&
                (
                (half_los >= 6 && (hibucket - lobucket) >= half_los - 1)
                ||
                ((half_los >= 4 && half_los <= 5) && (hibucket - lobucket) >= half_los - 1)
                ||
                ((half_los <= 3) && (hibucket - lobucket) >= half_los)
                )
                )
            {
                SetInd(19 * Convert.ToInt32(set_ind), "Qualifies for q2hr because charting to bucket ratio is less than 3: hibucket-lobucket=" + hibucket + "-" + lobucket + "=" + (hibucket - lobucket) + " divided by num buckets=" + (bnum - 1) + " equals " + bucketratio);
                all_ok = (ind <= 19);
            }
            else if ((bucketratio <= 6 && bnum >= 2) &&  //oct19'20
                (
                (half_los >= 6 && (hibucket - lobucket) >= half_los - 2)
                ||
                ((half_los >= 4 && half_los <= 5) && (hibucket - lobucket) >= half_los - 1)
                ||
                ((half_los <= 3) && (hibucket - lobucket) >= half_los)
                )
                )
            {
                SetInd(18 * Convert.ToInt32(set_ind), "Qualifies for q4hr because charting to bucket ratio is <= 6: hibucket-lobucket=" + hibucket + "-" + lobucket + "=" + (hibucket - lobucket) + " divided by num buckets=" + (bnum - 1) + " equals " + bucketratio);
                all_ok = (ind == 18);
            }
            Program.VerboseAudit("---- End Assessment Group = " + group + " ----");

            return all_ok;
        }


        //Proposed change to remove consecutive factor; use straight scale with Short LOS exception:  
        //	                        LOS=12 hrs      LOS = 6 hrs     LOS = 4 hrs     LOS = 2 hrs     Short LOS< 5 hrs
        //q30 75% of 30-min buckets	3/4 x 24  = 18	3/4 x 12  = 9	3/4 x 8  = 6	3/4 x 4  = 3	5 or more VS
        //q1 66% of 60-min buckets	2/3 x 12  = 8	2/3 x 6  = 4	2/3 x 4  = 2.67=>3	2/3 x 2  = 1.33=>1	3 VS + examine 30min 
        //q2 50 % of 60-min buckets	.5 x 12  = 6	.5 x 6  = 3	.5 x 4  = 2	.5 x 2  = 1	2 VS
        //q4 33% of 60-min buckets	1/3 x 12  = 4 2	1/3 x 6  = 2 1	1/3 x 4  = 1.33=>1	1/3 x 2  = .67=>1	1 VS
        //private bool OLDAnalyzeBuckets(List<gBucket> buckets, int ind, int bucketsize, string group, bool set_ind)
        //{
        //    DateTime dt = DateTime.MinValue;
        //    int bnum = 0;
        //    int numbucket = -99;
        //    int lobucket = 99;
        //    int hibucket = 0;
        //    int numitems = 0;
        //    int numconsec = 0;
        //    int greatestnumconsec = 0;
        //    bool all_ok = false;
        //    List<gBucket> dtlist = new List<gBucket>();
        //    Program.VerboseAudit("----Begin Assessment Group = " + group + " ---- bucketsizew=" + bucketsize);
        //    //if (ind==18 && Math.Round(_pat.los_hours) <= 4.0)
        //    //{
        //    //    all_ok = DoShortLOSAssessEval(buckets,ind,bucketsize,group,set_ind);
        //    //    Program.VerboseAudit("---- End Assessment Group = " + group + " ----");
        //    //    return all_ok;
        //    //}

        //    //Program.VerboseAudit("buckets count=" + buckets.Count());
        //    var b = buckets.OrderBy(e => e.evdt).ToList();

        //    //numitems = buckets.Count();
        //    foreach (var item in b)
        //    {
        //        if (numbucket < item.bucket)
        //        {
        //            numbucket = item.bucket;
        //            numitems++;
        //            Program.VerboseAudit(item.bucket + ") " + item.evdt.ToString() + " := " + item.code);
        //        }
        //        //if (numbucket < item.bucket)
        //        //{
        //        //    //add dt to ary
        //        //    bnum++;
        //        //    dtlist.Add(item);
        //        //    if (numbucket == -99 || item.bucket - numbucket == 1)
        //        //    {
        //        //        numconsec++;
        //        //        if (greatestnumconsec < numconsec)
        //        //            greatestnumconsec = numconsec;
        //        //    }
        //        //    else
        //        //    {
        //        //        numconsec = 1;
        //        //    }
        //        //    numbucket = item.bucket;
        //        //    if (hibucket < item.bucket) hibucket = item.bucket;
        //        //    if (lobucket > item.bucket) lobucket = item.bucket;
        //        //    Program.VerboseAudit(item.bucket + ")." + item.evdt.ToString() + " := " + item.code);// + "  [distinct times only]");
        //        //}
        //    }

        //    //if (bnum <= 1)
        //    //{
        //    //    Program.VerboseAudit("---- End Assessment Group = " + group + " ----");
        //    //    return false;
        //    //}

        //    //Program.VerboseAudit("numitems=" + numitems);
        //    //Program.VerboseAudit("bucket count=" + bnum);
        //    //int half_los = (int)(_pat.los_hours * (30.0 / bucketsize));//for q30 this will be los_hours.  for q60 this will be .5 * los_hours
        //    //Program.VerboseAudit("half-LOS bucket count=" + half_los + " at " + bucketsize + "mins each.");
        //    //Program.VerboseAudit("greatest consec=" + greatestnumconsec);

        //    int num_buckets_in_los = (int)(_pat.los_hours * (60.0 / bucketsize));//for q30 this will be 2xlos_hours.  for q60 this will be los_hours
        //    Program.VerboseAudit("total bucket count in LOS=" + num_buckets_in_los);
        //    Program.VerboseAudit("num buckets filled=" + numitems);
        //    //Program.VerboseAudit("half-LOS bucket count=" + half_los + " at " + bucketsize + "mins each.");
        //    //Program.VerboseAudit("greatest consec=" + greatestnumconsec);

        //    //double bucketratio = (hibucket-lobucket) / (1.0 * (bnum-1));
        //    //q30 75% of 30-min buckets	3/4 x 24  = 18	3/4 x 12  = 9	3/4 x 8  = 6	3/4 x 4  = 3	5 or more VS
        //    //q1 66% of 60-min buckets	2/3 x 12  = 8	2/3 x 6  = 4	2/3 x 4  = 2.67=>3	2/3 x 2  = 1.33=>1	3 VS + examine 30min 
        //    //q2 50 % of 60-min buckets	.5 x 12  = 6	.5 x 6  = 3	.5 x 4  = 2	.5 x 2  = 1	2 VS
        //    //q4 33% of 60-min buckets	1/3 x 12  = 4 2	1/3 x 6  = 2 1	1/3 x 4  = 1.33=>1	1/3 x 2  = .67=>1	1 VS
        //    //int q30need = (int)Math.Round(0.75 * 2 * _pat.los_hours);
        //    int q1need = (int)Math.Round(0.667 * _pat.los_hours);
        //    int q2need = (int)Math.Round(0.5 * _pat.los_hours);
        //    int q4need = 1;
        //        if (_pat.los_hours > 5)
        //        {
        //            if (_pat.los_hours < 8)
        //                q4need = 1;
        //            else
        //                q4need = 2;

        //            if (numitems >= q1need)
        //            {
        //                SetInd(20 * Convert.ToInt32(set_ind), "Qualifies for q1 because numcharted=" + numitems + " is >=" + q1need + " LOS=" + Math.Round(_pat.los_hours, 2) + " x .667=" + q1need);
        //                all_ok = (ind <= 20);
        //            }
        //            else if (numitems >= q2need)
        //            {
        //                SetInd(19 * Convert.ToInt32(set_ind), "Qualifies for q2 because numcharted=" + numitems + " is >=" + q2need + " LOS=" + Math.Round(_pat.los_hours, 2) + " x .5=" + q2need);
        //                all_ok = (ind <= 19);
        //            }
        //            else if (numitems >= q4need)
        //            {
        //                SetInd(18 * Convert.ToInt32(set_ind), "Qualifies for q4 because numcharted=" + numitems + " is >=" + q4need + " LOS=" + Math.Round(_pat.los_hours, 2) + "  >= 8 needs 2 else 1");
        //                all_ok = (ind <= 18);
        //            }
        //        }
        //        else //short los
        //        {
        //            q1need = 3;
        //            q2need = 2;
        //            q4need = 1;

        //            if (numitems >= q1need)
        //            {
        //                SetInd(20 * Convert.ToInt32(set_ind), "ShortLOS Qualifies for q1 because numcharted=" + numitems + " is >=" + q1need + " LOS=" + Math.Round(_pat.los_hours, 2) + " needs just 3");
        //                all_ok = (ind <= 20);
        //            }
        //            else if (numitems >= q2need)
        //            {
        //                SetInd(19 * Convert.ToInt32(set_ind), "ShortLOS Qualifies for q2 because numcharted=" + numitems + " is >=" + q2need + " LOS=" + Math.Round(_pat.los_hours, 2) + " needs just 2");
        //                all_ok = (ind <= 19);
        //            }
        //            else if (numitems >= q4need)
        //            {
        //                SetInd(18 * Convert.ToInt32(set_ind), "ShortLOS Qualifies for q4 because numcharted=" + numitems + " is >=" + q4need + " LOS=" + Math.Round(_pat.los_hours, 2) + " needs just 1");
        //                all_ok = (ind <= 18);
        //            }

        //        }
        //    Program.VerboseAudit("---- End Assessment Group = " + group + " ----");
        //    return all_ok;
        //}


        //private bool NEWAnalyzeBuckets(List<gBucket> buckets, int ind, int bucketsize, string group, bool set_ind)
        //{
        //    DateTime dt = DateTime.MinValue;
        //    DateTime firstdt = DateTime.MinValue;
        //    bool specialq30 = false;

        //    int bnum = 0;
        //    int numbucket = -99;
        //    int numitems = 0;
        //    int numallitems = 0;
        //    List<gGap> gaplist = new List<gGap>();
        //    Program.VerboseAudit("----GAP Begin Assessment Group = " + group + " ---- bucketsizew=" + bucketsize);

        //    //Program.VerboseAudit("buckets count=" + buckets.Count());
        //    var b = buckets.OrderBy(e => e.evdt).ToList();
        //    //numitems = buckets.Count();

        //    DateTime lastq4dt = DateTime.MinValue;
        //    int numq4 = 1;

        //    foreach (var item in b)
        //    {
        //        Program.VerboseAudit(item.bucket + ") " + item.evdt.ToString() + " := " + item.code + " has all deps=" + item.has_all_deps);
        //        if (item.has_all_deps)
        //            if (numitems == 0)
        //            {
        //                dt = item.evdt;
        //                firstdt = dt;
        //                lastq4dt = dt;
        //            }
        //            else
        //            {
        //                if (lastq4dt.AddMinutes(30) >= dt)
        //                {
        //                    numq4++;
        //                    lastq4dt = dt;
        //                }
        //                var g = new gGap();
        //                g.gap = (int)(PFSUtility.DateDiffInMinutes(dt, item.evdt));
        //                g.evdt1 = dt;
        //                g.evdt2 = item.evdt;
        //                gaplist.Add(g);
        //                Program.VerboseAudit("addgap dt=" + dt.ToString() + " evdt=" + item.evdt.ToString() + " gap="+g.gap);
        //                dt = item.evdt;
        //            }
        //        numitems++;
        //        numallitems = numallitems + 1 + item.num_addl_items;
        //    }
        //    Program.VerboseAudit("numitems=" + numitems + " numq4=" + numq4);
        //    //if (numitems <= 2)
        //    //{
        //    //    if (numq4 >= 2)
        //    //    {
        //    //        if (set_ind && ind >= 18 && ind <= 20)
        //    //        {
        //    //            SetInd(18, "q4 based on number >=2 of items charted = " + numq4);
        //    //            return true;
        //    //        }
        //    //    }
        //    //    //if (numitems == 2 && ind >= 16 && ind <= 17)
        //    //    //{
        //    //    //    gGap[] sgapary = gaplist.ToArray();
        //    //    //    int k = 0;
        //    //    //    for (k = 0; k <= sgapary.GetUpperBound(0); k++)
        //    //    //    {
        //    //    //        Program.VerboseAudit("k=" + k + " gap=" + sgapary[k].gap);
        //    //    //    }
        //    //    //    if (sgapary[0].gap >= 150)
        //    //    //        SetInd(16, "Two item gap =" + sgapary[0].gap);
        //    //    //    else
        //    //    //        SetInd(17, "Two item gap =" + sgapary[0].gap);
        //    //    //}
        //    //    return false;
        //    //}

        //    int i, j, setind = 0;
        //    int numgaps = 0;
        //    int gapsum = 0;
        //    double gapavg = 999.0, lo_gapavg = 999.0, altlo_gapavg = 999.0;
        //    bool lastgap = false;
        //    bool shortstop = false;
        //    int timegap = 0;
        //    int lastj;
        //    DateTime lo_evdt1 = DateTime.MinValue, lo_evdt2 = DateTime.MinValue;
        //    DateTime altlo_evdt1 = DateTime.MinValue, altlo_evdt2 = DateTime.MinValue;
        //    gGap[] gapary = gaplist.ToArray();
        //    string s = "";
        //    for (i = 0; i <= gapary.GetUpperBound(0); i++) s = s + "," + gapary[i].gap;
        //    if (numitems == 2 && numq4 >= 2) // numitems will always be > 2
        //    {
        //        numgaps = 1;
        //        gapsum = gapary[0].gap;
        //        gapavg = gapsum;
        //        lo_gapavg = gapavg;
        //        lo_evdt1 = gapary[0].evdt1;
        //        lo_evdt2 = gapary[gapary.GetUpperBound(0)].evdt2;
        //    }
        //    else
        //    {

        //        //numitems = 4
        //        //i = 0 j = 0[i].evdt1 = 9 / 4 / 2019 5:15:00 AM[j].evdt2 = 9 / 4 / 2019 9:32:00 AM

        //        for (i = 0; i <= gapary.GetUpperBound(0) && !shortstop; i++)
        //        {
        //            numgaps = 0;
        //            gapsum = 0;
        //            gapavg = 999.0;
        //            lastgap = false;
        //            for (j = i; j <= gapary.GetUpperBound(0) && !lastgap; j++)
        //            {
        //                Program.VerboseAudit("i=" + i + " j=" + j + " [i].evdt1=" + gapary[i].evdt1 + " [j].evdt2=" + gapary[j].evdt2);
        //                timegap = (int)(PFSUtility.DateDiffInMinutes(gapary[i].evdt1, gapary[j].evdt2));
        //                if (timegap <= _pat.los_hours * 30 + 30)
        //                {
        //                    numgaps++;
        //                    gapsum += gapary[j].gap;
        //                    Program.VerboseAudit("numgaps=" + numgaps + " [j].gap=" + gapary[j].gap + " gapsum=" + gapsum);
        //                }
        //                else
        //                {
        //                    if (j == i)
        //                    {
        //                        lastj = i;
        //                        numgaps = 1;
        //                        gapsum = gapary[j].gap;
        //                    }
        //                    else
        //                        lastj = j - 1;
        //                    //Program.VerboseAudit("Last Gap: i=" + i + " j=" + j + " [i].evdt1=" + gapary[i].evdt1 + " [lastj].evdt2=" + gapary[lastj].evdt2);
        //                    lastgap = true;
        //                    gapavg = 1.0 * gapsum / numgaps;
        //                    Program.VerboseAudit("gapavg=" + gapavg + " numgaps=" + numgaps + " gapsum=" + gapsum);
        //                    timegap = (int)(PFSUtility.DateDiffInMinutes(gapary[i].evdt1, gapary[lastj].evdt2));
        //                    //                            if (gapavg < lo_gapavg && timegap >= .75 * _pat.los_hours * 30)
        //                    if (gapavg < lo_gapavg && timegap >= _pat.los_hours * 30)
        //                    {
        //                        lo_gapavg = gapavg;
        //                        Program.VerboseAudit("Low gapavg=" + lo_gapavg + " btwn " + gapary[i].evdt1 + " and " + gapary[lastj].evdt2);
        //                        lo_evdt1 = gapary[i].evdt1;
        //                        lo_evdt2 = gapary[lastj].evdt2;
        //                    }
        //                    numgaps = 0;
        //                    gapsum = 0;
        //                }

        //            }
        //            if (!lastgap)
        //            {
        //                shortstop = true;
        //                j--; // take the j index back by 1 because it incremented at the end.
        //                //Program.VerboseAudit(".Last Gap: i=" + i + " j=" + j + " [i].evdt1=" + gapary[i].evdt1 + " [j].evdt2=" + gapary[j].evdt2);
        //                gapavg = 1.0 * gapsum / numgaps;
        //                //Program.VerboseAudit(".gapavg=" + gapavg + " numgaps=" + numgaps + " gapsum=" + gapsum);
        //                timegap = (int)(PFSUtility.DateDiffInMinutes(gapary[i].evdt1, gapary[gapary.GetUpperBound(0)].evdt2));
        //                Program.VerboseAudit(".timegap=" + timegap + " .75halfLOS=" + .75 * _pat.los_hours * 30);
        //                if (gapavg < lo_gapavg)
        //                {
        //                    //                            if (timegap >= .75 * _pat.los_hours * 30)
        //                    if (timegap >= _pat.los_hours * 30)
        //                    {
        //                        lo_gapavg = gapavg;
        //                        //Program.VerboseAudit(".Low gapavg=" + lo_gapavg + " btwn " + gapary[i].evdt1 + " and " + gapary[gapary.GetUpperBound(0)].evdt2);
        //                        lo_evdt1 = gapary[i].evdt1;
        //                        lo_evdt2 = gapary[gapary.GetUpperBound(0)].evdt2;
        //                    }
        //                    else
        //                    {
        //                        altlo_gapavg = gapavg;
        //                        //Program.VerboseAudit(".AltLow gapavg=" + lo_gapavg + " btwn " + gapary[i].evdt1 + " and " + gapary[gapary.GetUpperBound(0)].evdt2);
        //                        altlo_evdt1 = gapary[i].evdt1;
        //                        altlo_evdt2 = gapary[gapary.GetUpperBound(0)].evdt2;
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    if (numitems >= 2) Program.VerboseAudit("Final Low gapavg=" + lo_gapavg + " btwn " + lo_evdt1.ToString() + " and " + lo_evdt2.ToString() + " " + (_pat.los_hours <= 4 ? 0 : 1) + "LOS=" + _pat.los_hours + " assmts:" + (_inds[15].is_checked ? 1 : 0) + (_inds[16].is_checked ? 1 : 0) + (_inds[17].is_checked ? 1 : 0) + (_inds[18].is_checked ? 1 : 0) + " gap[]: =" + s);
        //    //Program.VerboseAudit("Alt   Low gapavg=" + altlo_gapavg + " btwn " + altlo_evdt1.ToString() + " and " + altlo_evdt2.ToString());
        //    //q4: 3 hr + gap average  in 6 - 8 hrs
        //    //  q2:   1 hr 30 - 2 hr 59m avg gap  in 4 hrs
        //    //  q1:   46 min - 1 hr 29 min avg gap in 4 hrs
        //    //  q30:  45 min or less avg gap in 4 hrs
        //    //            if (lo_gapavg <= 45)
        //    int sample_breadth = (int)(PFSUtility.DateDiffInMinutes(lo_evdt1, lo_evdt2));
        //    //            if (_pat.los_hours >= 4 && sample_breadth > 0.75 * _pat.los_hours * 30 && numitems >= 2)
        //    if (_pat.los_hours >= 4 && sample_breadth >= _pat.los_hours * 30 && numitems >= 2)
        //    {
        //        //if (lo_gapavg <= 80) setind = 18;  //q60
        //        //else if (lo_gapavg <= 180) setind = 19; //q120
        //        //else if (lo_gapavg <= 999) setind = 20;  //q240
        //        //if (lo_gapavg <= 45) specialq30 = true;       //q30
        //        if (lo_gapavg <= 70) setind = 18;  //q60
        //        else if (lo_gapavg <= 130) setind = 19; //q120
        //        else if (lo_gapavg <= 250) setind = 20;  //q240
        //        //if (lo_gapavg <= 45) specialq30 = true;       //q30

        //    }
        //    else
        //    {
        //        if (numitems > 2 && sample_breadth >= _pat.los_hours * 30)
        //        {
        //            if (lo_gapavg <= 60) setind = 20;  //q60
        //            else if (lo_gapavg <= 120) setind = 19; //q120
        //            else if (lo_gapavg <= 240) setind = 18;  //q240
        //            //if (lo_gapavg <= 30) specialq30 = true;       //q30
        //        }
        //        else if (numitems == 2 && sample_breadth >= 30)
        //        {
        //            if (sample_breadth >= _pat.los_hours * 30 && lo_gapavg <= 240) setind = 18;
        //            //else setind = 18;  //q240
        //        }
        //        //else if (numitems >= 2)
        //        //    setind = 20;
        //    }
        //    int imins = 0;
        //    bool all_ok = false;
        //    if (setind == 20) imins = 60;
        //    else if (setind == 19) imins = 120;
        //    else if (setind == 18) imins = 240;
        //    if (ind <= 17)
        //    {
        //        setind = 0;
        //        if ((bucketsize == 40 && specialq30)
        //            || (bucketsize == 80 && imins <= 60)
        //            || (bucketsize == 150 && imins <= 120)
        //            || (bucketsize == 300 && imins <= 240))
        //            setind = ind;
        //    }
        //    if (setind > 0)
        //    {
        //        if (set_ind)
        //            SetInd(setind, "Qualifies for q" + imins + "mins for gap avg=" + lo_gapavg + "[" + (sample_breadth < 4 ? "<" : "") + "in " + sample_breadth + " mins] btwn " + lo_evdt1.ToString() + " and " + lo_evdt2.ToString());
        //        else //for when AnalyzeBuckets is asked NOT to set the indicator...
        //            Program.VerboseAudit(group + "Qualifies for q" + imins + "mins for gap avg=" + lo_gapavg + "[" + (sample_breadth < 4 ? "<" : "") + "in " + sample_breadth + " mins] btwn " + lo_evdt1.ToString() + " and " + lo_evdt2.ToString());
        //    }
        //    if (ind >= 18 && ind <= 20 && setind >= 18) all_ok = true;
        //    if (ind <= 17 && setind == ind) all_ok = true;

        //    Program.VerboseAudit("---- GAP End Assessment Group = " + group + " ----");
        //    return all_ok;

        //}


        private void SetBucketSize(int minutes)
        {
            _bucket_size = minutes;
        }

        //Add to the list of bucket numbers (redundant buckets are fine)
        private void AddBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field)
        {
            AddBuckets(bucket_list, cat, code_list, desc, field, "", SearchDepth.SearchDefault);
        }
        private void AddBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field, string result_list)
        {
            AddBuckets(bucket_list, cat, code_list, desc, field, result_list, SearchDepth.SearchDefault);

        }

        private void AddBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field, string result_list, SearchDepth search_depth)
        {
            // Assign (virtual) bucket numbers to each assessment.  
            // Every x minutes after that is a new bucket.  Bucket zero = pull_start time.
            //            Program.VerboseAudit("----Locating items for group: " + assessgrouplabel + "   bucketsize=" + _bucket_size);

            // get the chart items for the assessments
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc, field, result_list);
            int ct = query.Count();
            Program.VerboseAudit("ct=" + ct + " of codelist=" + code_list);

            var query3 = from item in query
                         select new
                         {
                             bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                             code = item.CODE,
                             evdt = item.EVENT_DATETIME
                         };
            // Add to the list
            int cta = 0;
            foreach (var item in query3)
            {
                var b = new gBucket();
                b.bucket = item.bucket;
                b.code = item.code;
                b.evdt = item.evdt;
                b.has_all_deps = true;
                gBucket f = bucket_list.Find(x => x.evdt == item.evdt);
                if (f.evdt != item.evdt)
                {
                    bucket_list.Add(b);
                    cta++;
                }
            }
            Program.VerboseAudit("add ct=" + cta);

            // This is just for auditing: Say what we are looking for and how many were found
            if (!String.IsNullOrEmpty(result_list))
            {
            }
            else
            {
            }

        }


        //private int CountBuckets(List<gBucket> bucket_list)       // bucket list: (ha ha)
        //{
        //    //int result = bucket_list.Distinct().Count();
        //    //if (result > 0) Program.VerboseAudit(result + " unique");
        //    //return result;
        //    int x = -99;
        //    int result = 0;
        //    //int result = bucket_list.Distinct().Count();
        //    var query = from b in bucket_list
        //                orderby b.bucket ascending
        //                select b;
        //    foreach (var b in query)
        //    {
        //        if (x != b.bucket)
        //        {
        //            result++;
        //            x = b.bucket;
        //        }
        //    }
        //    if (result > 0) Program.VerboseAudit(result + " unique");
        //    return result;
        //}

        private void Check_21()
        {
            string reslist;
            string codelist;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 21. Fluid Management");
            Program.VerboseAudit("---------------");
            exclude_periop_data = false;

            reslist ="";
            SetIndIfResultContains(21, "", "304239218", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239219", "", "", reslist);
            reslist = "Provided,Fluid restriction,IV Fluids";
            SetIndIfResultContains(21, "", "304239220", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239510", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239511", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239512", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239513", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239514", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239515", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239516", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239517", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239518", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239519", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239520", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239521", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239522", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239523", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239524", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239525", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239526", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239527", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239528", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239529", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239530", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239531", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239532", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239533", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239534", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239535", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239536", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239537", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239538", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239539", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239540", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239541", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239542", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239543", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239544", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239545", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239546", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239547", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239548", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239549", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(21, "", "304239550", "", "", reslist);
            
            SetIndIfResultContains(21, "", "3042319546", "", "", reslist);

            SetIndIfResultContains(21, "", "304230100004", "", "", reslist);
            SetIndIfResultContains(21, "", "304237070009", "", "", reslist);
            SetIndIfResultContains(21, "", "3042340101332", "", "", reslist);
            SetIndIfResultContains(21, "", "304234370012", "", "", reslist);
            SetIndIfResultContains(21, "", "3042335586", "", "", reslist);
            SetIndIfResultContains(21, "", "304237071002", "", "", reslist);

        }

        private void Check_22()
        {
            string reslist;
            bool st1 = false;
            string piv;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 22. Wound/Injury Mgmt");
            Program.VerboseAudit("---------------");

            exclude_periop_data = false;

            reslist = "Separation,Other";
            SetIndIfResultContains(22, "", "304239005", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239006", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239007", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239008", "", "", reslist);
            reslist = "Copious,Large,Moderate,Small,Scant,None,Other";
            SetIndIfResultContains(22, "", "304239009", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239010", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239011", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239012", "", "", reslist);
            reslist = "Adhesive strips,Staples,Surgical skin adhesive,Sutures,Retention sutures,Staples Removed,Sutures removed,Other";
            SetIndIfResultContains(22, "", "304239013", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239014", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239015", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239016", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239017", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239018", "", "", reslist);
            reslist = "New drainage,Old drainage,Other";
            SetIndIfResultContains(22, "", "304239019", "", "", reslist);
            reslist = "Yes";
SetIndIfResultContains(22, "", "304239020", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239021", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239073", "", "", reslist);
            reslist = "Bright Red,Brown,Clear,Clots,Malodorous,Normal show,Pink,Other";
            SetIndIfResultContains(22, "", "304239074", "", "", reslist);
            reslist = "Heavy,Moderate,Scant,Spotting,Pad 25% Saturated,Pad 50% Saturated,Pad 75% Saturated,Pad 100% Saturated";
            SetIndIfResultContains(22, "", "304239075", "", "", reslist);
            //reslist = "Wound cleanser,Granulex spray";
            //SetIndIfResultContains(22, "", "304239076", "", "", reslist);
            reslist = "Applied";
            SetIndIfResultContains(22, "", "304239077", "", "", reslist);
            reslist = "Applied";
            SetIndIfResultContains(22, "", "304239078", "", "", reslist);
            reslist = "Bleeding,Draining,Extravasated,Hematoma,Leaking,Infiltrated,Phlebitis";
            SetIndIfResultContains(22, "", "304239662", "", "", reslist);
            reslist = "Antimicrobial patch,Gauze,Occlusive,Pressure dressing,Medicinal with wrap,Non-Occlusive device,Non-applicable/No drainage,Sterile,Other";
            SetIndIfResultContains(22, "", "304239079", "", "", reslist);
            reslist = "Trauma/injury,Swelling";
            SetIndIfResultContains(22, "", "304239080", "", "", reslist);
            reslist = "Boggy,Breakdown,Bulging,Firm,Sunken,Pulsating,Helmet use,Other";
            SetIndIfResultContains(22, "", "304239081", "", "", reslist);
            reslist = "Foreign body,Trauma/Injury,Chemical Exposure,Drainage,Edema";
            SetIndIfResultContains(22, "", "304239351", "", "", reslist);
            reslist = "Foreign body,Trauma/Injury,Chemical Exposure,Drainage,Edema";
            SetIndIfResultContains(22, "", "304239352", "", "", reslist);
            reslist = "Abrasions,External trauma,Lacerations,Foreign body,Trauma/injury";
            SetIndIfResultContains(22, "", "304239353", "", "", reslist);
            reslist = "Abrasions,External trauma,Lacerations,Foreign body,Trauma/injury";
            SetIndIfResultContains(22, "", "304239354", "", "", reslist);
            reslist = "Abrasion,Epistaxis,Foreign body,Laceration,Trauma/Injury";
            SetIndIfResultContains(22, "", "304239082", "", "", reslist);
            reslist = "Laceration,Lesion";
            SetIndIfResultContains(22, "", "304239083", "", "", reslist);
            reslist = "Bleeding,Lesions";
            SetIndIfResultContains(22, "", "304239084", "", "", reslist);
            reslist = "Blistered,Cracked,Lacerated,Trauma/injury";
            SetIndIfResultContains(22, "", "304239085", "", "", reslist);
            reslist = "Bleeding,Blistered,Lesions,Mucositis/Stomatitis,Ulcerations";
            SetIndIfResultContains(22, "", "304239086", "", "", reslist);
            reslist = "Grade 1 Asymptomatic or mild symptoms, intervention not indicated,Grade 2 Moderate pain, not interfering with oral intake, modified diet indicated,Grade 3 Severe pain, interfering with oral intake,Grade 4 Life-threatening consequences, urgent intervention indicated,Grade 5 Death";
            SetIndIfResultContains(22, "", "304239087", "", "", reslist);
            reslist = "Abscess,Trauma/Injury";
            SetIndIfResultContains(22, "", "304239088", "", "", reslist);
            reslist = "Trauma/injury,Swollen";
SetIndIfResultContains(22, "", "304239089", "", "", reslist);
            reslist = "Bloody,Blood Tinged,Pink tinged";
            SetIndIfResultContains(22, "", "304239090", "", "", reslist);
            reslist = "Bloody,Black flecked,Pink tinged";
            SetIndIfResultContains(22, "", "304239091", "", "", reslist);
            reslist = "Blanchable Erythema,Bleeding,Blistered,Drainage,Edema/Swelling,Erythema,Hematoma,Macerate,Oozing secretions,Subcutaneous emphysema,Sutures present,Swelling";
SetIndIfResultContains(22, "", "304239092", "", "", reslist);
            reslist = "Cleansed,Dressing applied";
            SetIndIfResultContains(22, "", "304239093", "", "", reslist);
            reslist = "Abrasion,Blister,Burn,Cracking,Excoriation,Laceration,Rash,Tear";
            SetIndIfResultContains(22, "", "304239094", "", "", reslist);
            reslist = "Lesion,Weeping";
            SetIndIfResultContains(22, "", "304239095", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239096", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239097", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239098", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239099", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239100", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239101", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239102", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239103", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239104", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239105", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239106", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239107", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239108", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239109", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239110", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239111", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239112", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239113", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239114", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239115", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239116", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239117", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239118", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239119", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239120", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239121", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239122", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239123", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239124", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239125", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239126", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239127", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239128", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239129", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239130", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239131", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239132", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239133", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239134", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239135", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239136", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239137", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239138", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239139", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239140", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239141", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239142", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239143", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239144", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239145", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239146", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(22, "", "304239147", "", "", reslist);
            reslist = "Black,Coffee ground,Hematesmesis,Red";
            SetIndIfResultContains(22, "", "304239148", "", "", reslist);
            reslist = "Bleeding,Excoriated,Macerated,Blistered,Drainage at site,Erythema";
SetIndIfResultContains(22, "", "304239149", "", "", reslist);
            reslist = "Alginate,Dry dressing,Foam,Hydrocolloid,Moist to dry,Moist to moist,Moisture barrier,Non adherent,Papain-urea debridement,Pressure dressing,Protective barrier,Split gauze,Special type,Triad hydro,Vacuum based,Water based,Wound gel,Xeroform";
            SetIndIfResultContains(22, "", "304239150", "", "", reslist);
//            reslist = "Bile,Bloody,Bright red,Brown,Clear,Clots,Coffee ground,Dark red,Fecal,Green,Pink tinged,Tan,Yellow,Undigested contents,Other";
            reslist = "Bloody,Bright red,Brown,Clots,Coffee ground,Dark red,Fecal,Pink tinged,Other";
            SetIndIfResultContains(22, "", "304239151", "", "", reslist);
            reslist = "Bloody";
            SetIndIfResultContains(22, "", "304239152", "", "", reslist);
            reslist = "Black,Red,Red streaks,Melena";
            SetIndIfResultContains(22, "", "304239153", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239154", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239155", "", "", reslist);
            reslist = "Pouch system: changed,Irrigation";
            SetIndIfResultContains(22, "", "304239156", "", "", reslist);
            reslist = "Blood clots,Red flecks";
            SetIndIfResultContains(22, "", "304239157", "", "", reslist);
            reslist = "Injury/trauma,Lesion,Swelling";
            SetIndIfResultContains(22, "", "304239158", "", "", reslist);
            reslist = "Injury/trauma,Lesion,Swelling";
            SetIndIfResultContains(22, "", "304239159", "", "", reslist);
            reslist = "Injury/trauma,Lesion,Swelling";
            SetIndIfResultContains(22, "", "304239160", "", "", reslist);
            reslist = "Injury/trauma,Lesion,Swelling";
            SetIndIfResultContains(22, "", "304239161", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239162", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239163", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239164", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239165", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239166", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239167", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239168", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239169", "", "", reslist);
            reslist = "Clean,Dry,Intact,Bleeding,Black,Brown,Dark edges,Dusky,Edematous,Excoriated,Fragile,Hyper-granulation tissue,Light purple,Malodorous,Pale,Pink,Red,Tan,White,Yellow,Other";
            SetIndIfResultContains(22, "", "304239170", "", "", reslist);
            reslist = "Clean,Dry,Intact skin,Black,Bleeding,Boggy,Brown,Calloused,Crepitus,Dark edges,Denuded,Edema,Ecchymosis,Excoriated,Erythema: blanchable,Erythema: non-blanchable,Fragile,Hematoma,Hyperpigmented,Induration,Maceration,Pale,Pink,Purple,Red,Tan,White,Yellow,Other";
            SetIndIfResultContains(22, "", "304239171", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239172", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239173", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239174", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239175", "", "", reslist);
            reslist = "Autograft taken,Bleeding,Dusky,Edematous,Epithelialized,Erythema: blanching,Erythema: non-blanching,Eschar,Excoriated,Fragile,Fully granulated,Hematoma,Hypergranulation tissue,Malodorous,Necrotic,Partial granulation,Peeling,Pink,Skin buds/Capillary buds,Sloughing,White,Yellow,Other";
            SetIndIfResultContains(22, "", "304239176", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239177", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239178", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239179", "", "", reslist);
            reslist ="unable to assess";
            SetIndIfResultDoesNotContain(22, "", "304239180", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239181", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239182", "", "", reslist);
            reslist = "Clean, dry, intact,Blistered,Bleeding,Clean,Dry,Ecchymotic,Erythema,Extravasated,Intact,Macerate,Oozing,Other";
            SetIndIfResultContains(22, "", "304239665", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239183", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239563", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239667", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239565", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239184", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239185", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239186", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239187", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239188", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239534", "", "", reslist);
            reslist = "Blanchable Erythema,Blistered,Bleeding,Cool to touch,Crepitus";
            reslist += ",Draining,Ecchymotic,Edema,Swelling,Erythema,Extravasated,Hematoma";
            reslist += ",Hyperpigmentation,Hypopigmentation,Induration,Infiltrated,Leaking";
            reslist += ",Macerate,Moist,Painful to touch,Phlebitis,Pink,Tender to touch";
            reslist += ",Warm to touch";
            SetIndIfResultContains(22, "", "304239660", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239189", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239190", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239191", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239192", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239663", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239666", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239661", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239664", "", "", reslist);
            reslist = "Abrasion,Blister,Desquamation,Ecchymosis,Forceps Mark,Gelatinous,Laceration,Rash,Rash, Diaper,Vacuum Mark";
            SetIndIfResultContains(22, "", "304239193", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239194", "", "", reslist);
            reslist = "Trauma/injury,Drainage,Redness";
            SetIndIfResultContains(22, "", "304239195", "", "", reslist);
            reslist = "Trauma/injury,Drainage,Redness";
            SetIndIfResultContains(22, "", "304239196", "", "", reslist);
            reslist = "Dry,Moist,Clamp off,Clamp on,2 cord vessels,3 cord vessels,Periumbilical Erythema,Drainage,Gelatinous,Malodorous,Other";
            SetIndIfResultContains(22, "", "304239197", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239198", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239199", "", "", reslist);
            //reslist = "Bloody,Coffee ground,Red";
            //SetIndIfResultContains(22, "", "304239200", "", "", reslist);
            reslist = "Blood tinged";
            SetIndIfResultContains(22, "", "304239201", "", "", reslist);
            reslist = "Not intact,White patches";
            SetIndIfResultContains(22, "", "304239202", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239203", "", "", reslist);
            reslist = "Trauma/injury,Foreign Body";
            SetIndIfResultContains(22, "", "304239204", "", "", reslist);
            reslist = "Trauma/injury,Bleeding,Lesions";
            SetIndIfResultContains(22, "", "304239205", "", "", reslist);
            reslist = "Yes";
            SetIndIfResultContains(22, "", "304239206", "", "", reslist);
            reslist = "Plugged duct,Red";
            SetIndIfResultContains(22, "", "304239207", "", "", reslist);
            reslist = "Plugged duct,Red";
            SetIndIfResultContains(22, "", "304239208", "", "", reslist);
            reslist = "Bleeding,Blisters,Cracked";
            SetIndIfResultContains(22, "", "304239209", "", "", reslist);
            reslist = "Bleeding,Blisters,Cracked";
            SetIndIfResultContains(22, "", "304239210", "", "", reslist);
            reslist = "Alba,Rubra,Serosa";
            SetIndIfResultContains(22, "", "304239211", "", "", reslist);
            reslist ="";
            SetIndIfResultContains(22, "", "304239212", "", "", reslist);
            reslist = "Yes";
            SetIndIfResultContains(22, "", "304239214", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(22, "", "3041308801", "", "", reslist);

            SetIndIfResultContains(22, "", "3048716070", "", "", "");
            reslist = "Adhesive bandage,Biopatch,Dry,Non adherent,Pressure,Special Tape,Sterile gauze,TSM,Other";
            SetIndIfResultContains(22, "", "3048700787", "", "", reslist);


        }


        int EducMins(string timecode, string nursecode)
        {
            string res;
            int val = 0;
            if (ResultContains("", nursecode, "", "", "Nursing"))
            {
                if (GetResult("", timecode, "", "", out res))
                {
                    if (res.Left(1).IsNumeric())
                    {
                        val += (int)res.Val();
                    }
                }
            }
            return val;
        }

        private void Check_23()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 23. Patient/Family Education >= 1 Hour by RN");
            Program.VerboseAudit("---------------");
            exclude_periop_data = true;
            exclude_periop_data = false;
            string[] educodes = { "3040019666", "3040019679" };
            SetIndIfCodeBtwn(23, 304230700, 304235657, SearchDepth.SearchDefault);


        }
        private int SetIndIfCodeBtwn(int ind, int locode, int hicode, SearchDepth search_depth)

        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = query.Where(e => e.CODE.ToLower().StartsWith("edu"));
            query = query.Where(e => e.CODE.Substring(3, 9).Val() >= locode);
            query = query.Where(e => e.CODE.Substring(3, 9).Val() <= hicode);
            int count = query.Count();
            // always return what was found
            string found_what = "There were " + count + " items found with code between " + locode + " and " + hicode;
            if (count >= 8) SetInd(ind, found_what);
            // echo the result?
            //               if (trace) Program.VerboseAudit(found_what);

            return count;
        }

        private void CheckEDUtab(string educode)
        {
            int ub, i;
            string[] desc = new string[3];
            DateTime evdt;
            string cd1, cd2, res1, res2, topic1, topic2;

            //OBX | 2 | ST | EDU3040004179METHOD ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC STROKE EVERY MINUTE MATTERS MC6074| 1 | E |||||| F ||| 20170612113300
            //OBX | 4 | ST | EDU3040004179RESPONSE ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC STROKE EVERY MINUTE MATTERS MC6074| 1 | TB |||||| F ||| 20170612113300
            //OBX | 6 | ST | EDU3040004179METHOD ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC YOU CAN IMPROVE YOUR HEALTH MC2066 - 09 | 1 | E |||||| F ||| 20170612113300
            //OBX | 8 | ST | EDU3040004179RESPONSE ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC YOU CAN IMPROVE YOUR HEALTH MC2066 - 09 | 1 | TB |||||| F ||| 20170612113300
            //OBX | 10 | ST | EDU3040004179METHOD ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC HIGH BLOOD PRESSURE (HYPERTENSION)MC5056 | 1 | E |||||| F ||| 20170612113300
            //OBX | 12 | ST | EDU3040004179RESPONSE ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC HIGH BLOOD PRESSURE (HYPERTENSION)MC5056 | 1 | TB |||||| F ||| 20170612113300
            var query1 = StartNewQuery(SearchDepth.SearchSince9Hrs);
            query1 = AndItemFilter(query1, "", "EDU" + educode + "METHOD", "", "", "E,D,I");
            query1 = query1.Where(e => e.EVENT_DATETIME <= PFSUtility.MaxDateTime(_pat.unit_departure, _pat.effective_out));
            foreach (var item1 in query1)
            {
                evdt = item1.EVENT_DATETIME;
                res1 = item1.RESULT;
                cd1 = item1.CODE;
                var arr = item1.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                ub = arr.GetUpperBound(0);
                for (i = 0; (i <= Math.Min(ub, 2)); i++)
                    desc[i] = arr[i];
                if (ub >= 2)
                    topic1 = desc[2].Trim();
                else
                    topic1 = "";
                var query2 = StartNewQuery(SearchDepth.SearchSince9Hrs);
                query2 = AndItemFilter(query2, "", "EDU" + educode + "RESPONSE", "", "", "IP,TB,NR");
                query2 = query2.Where(e => e.EVENT_DATETIME == evdt);
                foreach (var item2 in query2)
                {
                    res2 = item2.RESULT;
                    cd2 = item2.CODE;
                    var arr2 = item2.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                    ub = arr.GetUpperBound(0);
                    for (i = 0; (i <= Math.Min(ub, 2)); i++)
                        desc[i] = arr[i];
                    if (ub >= 2)
                        topic2 = desc[2].Trim();
                    else
                        topic2 = "";
                    if (topic1 == topic2)
                    {
                        SetInd(23, "Found EDU" + educode + ": " + desc[0] + "^" + topic1 + " at:" + evdt.ToString() + " METHOD=" + res1 + " RESPONSE=" + res2);
                    }
                }

            }

        }

        private void Check_24()
        {
            string reslist = "";
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 24. Coordination of Care >= 1 Hour by RN");
            Program.VerboseAudit("---------------");
            exclude_periop_data = true;

            reslist = "Hospital interpreter,Telephonic services,Video services,Family with waiver,Other";
            SetIndIfResultContains(24, "", "304239350", "", "", reslist);
            SetIndIfResultDoesNotContain(24, "", "304239375,304239376", "", "", "English,unable to respond");

            if (_inds[24].is_checked) return;

            CheckLanguage();

            if (_inds[24].is_checked) return;

            string s = "277";
            string desc_found = "";
            if (OrderInProgress(s, out desc_found))
                SetInd(24, "Order in effect: " + desc_found);


        }

        private void CheckLanguage()
        {
            //            "Via ADT Intterface PID-15:
            //Trigger Indicator #8 Communication if this field's value is anything other than: (blank), eng, english, unk, dc, or unv."
            //"Via ADT Intterface in PID-15:
            //Trigger Indicator #8 Communication if this field's value is anything other than: (blank), Eng, UNK, dc, or  unv."

            int sep_idx = _pat.lang_lvc.IndexOf(";;");
            //Program.VerboseAudit("Language=" + _pat.lang_lvc + " sep_idx="+sep_idx);
            if (sep_idx <= 0) return;
            string pref_lang = _pat.lang_lvc.Substring(0, sep_idx);
            string writ_lang = _pat.lang_lvc.Substring(sep_idx + 2, _pat.lang_lvc.Length - sep_idx - 2);
            bool pref_ok = (pref_lang.Trim() == "" || pref_lang.Trim().ToUpper().StartsWith("ENG")
                || pref_lang.Trim().ToUpper().StartsWith("DC")
                || pref_lang.Trim().ToUpper().StartsWith("UNK")
                || pref_lang.Trim().ToUpper().StartsWith("UNV"));
            bool writ_ok = (writ_lang.Trim() == "" || writ_lang.Trim().ToUpper().StartsWith("ENG")
                || writ_lang.Trim().ToUpper().StartsWith("DC")
                || writ_lang.Trim().ToUpper().StartsWith("UNK")
                || writ_lang.Trim().ToUpper().StartsWith("UNV"));
            if (!(pref_ok && writ_ok))
                SetInd(24, "Preferred language=" + pref_lang + " Written=" + writ_lang);
        }

        private void Check_Activities()
        {

            Program.VerboseAudit("---------------");
            Program.VerboseAudit(" Activities");
            Program.VerboseAudit("---------------");


        }

        private void AtLeastOneADL()
        {
            Program.Audit("---------------");

            if (!(_inds[2].is_checked || _inds[3].is_checked))
            {
                SetInd(1, "Defaulting to ADL Self.");
            }
        }

        private void HighestIndicatorInEachGroupWins()
        {
            int i, g;
            bool highest_is_on;
            string ind_list;

            Program.Audit("Select highest indicator in each group");

            g = 0;
            highest_is_on = false;
            // Go from bottom up - highest (lowest) indicator in each group wins
            for (i = MAX_INDS; (i >= 1); i--)
            {
                if (_inds[i].radio_group > 0)
                {
                    if (_inds[i].radio_group != g)
                    {
                        //this is a new group
                        g = _inds[i].radio_group;
                        highest_is_on = _inds[i].is_checked;
                    }
                    else
                    {
                        //same group
                        if (highest_is_on)
                        {
                            _inds[i].is_checked = false;             //uncheck a lower number
                        }
                        else
                        {
                            highest_is_on = _inds[i].is_checked;     //save this one
                        }
                    }
                }
            } // next i

            //Echo the indicators for an Audit (no classification will be saved)
            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++)
            {
                if (_inds[i].is_checked) ind_list += "," + i;
            } // next i

            Program.Audit("Final list = " + ind_list.Substring(1));
            Program.Audit("---------------");
        }


        private void CheckProcs()
        {
            CheckProc_2();
            CheckProc_3();
            CheckProc_4();
            CheckProc_5();
            CheckProc_6();
            CheckProc_7();

        }

        private void CheckActivity(int actnum, string actcode, string actst, string actlencode)
        {
            int actlen;
            string actsthhmm = "0000";

            var query = StartNewQuery(SearchDepth.SearchSince24Hrs);    // add custom time range below
            query = query.Where(e => e.CODE == actcode);
            query = query.Where(e => e.ORDER_STATUS == null || e.ORDER_STATUS == ""); // need to update order_status=x
            query = query.OrderBy(e => e.EVENT_DATETIME);
            Program.VerboseAudit("for code=" + actcode + " count=" + query.Count());
            foreach (var item in query)
            {
                if (item.RESULT.ToLower() == "yes")
                { //09500000
                    var q2 = StartNewQuery(SearchDepth.SearchSince24Hrs);    // add custom time range below
                    q2 = q2.Where(e => e.CODE == actst);
                    q2 = q2.Where(e => e.EVENT_DATETIME == item.EVENT_DATETIME);
                    Program.VerboseAudit("for code=" + actst + " count=" + q2.Count());
                    if (q2.Count() == 0) return;

                    actsthhmm = q2.First().RESULT;
                    Program.VerboseAudit("actsthhmm=" + actsthhmm);
                    if (actsthhmm.Length >= 4)
                        actsthhmm = actsthhmm.Substring(0, 4);
                    Program.VerboseAudit("actsthhmm=" + actsthhmm);

                    var q3 = StartNewQuery(SearchDepth.SearchSince24Hrs);    // add custom time range below
                    q3 = q3.Where(e => e.CODE == actlencode);
                    q3 = q3.Where(e => e.EVENT_DATETIME == item.EVENT_DATETIME);
                    Program.VerboseAudit("for code=" + actlencode + " count=" + q3.Count());
                    if (q3.Count() == 0) return;

                    actlen = (int)q3.First().RESULT.Val();
                    DateTime stdt = DateTime.MinValue;
                    if (actlen >= 60)
                    {
                        TimeSpan start = new TimeSpan(0, 0, 0); //0 o'clock
                        TimeSpan end = new TimeSpan(6, 0, 0); // 6 o'clock
                        TimeSpan item_ts = item.EVENT_DATETIME.TimeOfDay;
                        TimeSpan actst_ts = new TimeSpan((int)actsthhmm.Substring(0, 2).Val(), (int)actsthhmm.Substring(2, 2).Val(), 0);
                        stdt = item.EVENT_DATETIME.Date + actst_ts;
                        if ((item_ts >= start) && (item_ts <= end))
                        {
                            if (actst_ts > item_ts) // then it belongs to prev cal day.
                                stdt = item.EVENT_DATETIME.Date.AddDays(-1) + actst_ts;
                        }
                        //                      DateTime stdt = item.EVENT_DATETIME.Date.AddHours(actsthhmm.Substring(0, 2).Val()).AddMinutes(actsthhmm.Substring(2, 2).Val());
                        Program.VerboseAudit("stdt=" + stdt);
                        AddSimpleProc(actnum, stdt, stdt.AddMinutes(actlen));
                        NullifyActivity(actcode, item.EVENT_DATETIME);
                    }
                }
            }

        }

        private void NullifyActivity(string actcode, DateTime evdt)
        {
            var db = PFSDBUtility.NewSqlConnection();
            string q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and event_datetime=" + PFSDBUtility.SQLDateTime(evdt) + " and code='" + actcode + "'";
            SqlCommand cmd = new SqlCommand(q, db);
            cmd.ExecuteNonQuery();
            db.Close();
        }

        //====================================================
        //====================================================

        private void CheckProc_2()
        {
            if (_pat.short_name.ToUpper() != "MGH") return;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MHA1. 1-1 safety observation by RN");
            Program.VerboseAudit("MHA2. 1-1 safety observation by non-RN");
            Program.VerboseAudit("---------------");

            //54610   2020 - 01 - 29 11:00:00.000 Initiated   304239487
            //54610   2020 - 01 - 30 10:00:00.000 Continued   304239487
            //54610   2020 - 01 - 30 22:00:00.000 Discontinued   304239487
            //            304239487

            //            "Initiated
            //Continued
            //Discontinued"
            bool stop = false;
            DateTime dcdt = DateTime.MinValue;
            bool found_dc = false;
            bool first = true;
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);    // add custom time range below
            query = query.Where(e => e.CODE == "304239487");
            query = query.Where(e => e.ORDER_STATUS == null || e.ORDER_STATUS == ""); // need to update order_status=x
            query = query.Where(e => e.RESULT.ToLower() == "initiated" || e.RESULT.ToLower() == "continued");
            query = query.OrderBy(e => e.EVENT_DATETIME);
            Program.VerboseAudit("qct: " + query.Count());
            if (query.Count() > 0)
            {
                foreach (var item in query)
                {
                    if (item.EVENT_DATETIME > dcdt) //skip any intermediate starts
                    {
                        if (item.PROCEDURE_START == null)
                            item.PROCEDURE_START = DateTime.MaxValue;
                        Program.VerboseAudit("item:" + item.EVENT_DATETIME + " original start=" + item.PROCEDURE_START);
                        found_dc = MakeActivityIfFindDiscontinue((DateTime)item.PROCEDURE_START, item.EVENT_DATETIME, out dcdt);
                        Program.VerboseAudit("Sitter: " + item.RESULT + " " + item.EVENT_DATETIME.ToString());
                        first = false;
                        if (found_dc)
                        {
                            Program.VerboseAudit("founddc: true");
                            UpdatePtChartArraysCode("304239487", "X", dcdt);
                        }
                    }
                }
            }
        }

        private bool MakeActivityIfFindDiscontinue(DateTime origstart, DateTime initdt, out DateTime dcdt)
        {
            int pnum = 0;
            bool found_dc = false;
            dcdt = DateTime.MinValue;
            bool first = true;
            DateTime save_initdt = initdt;

            Program.VerboseAudit("q origstart=" + origstart + " initdt=" + initdt + " g_pull_finist=" + Program.g_pull_finish);
            if (origstart < initdt) initdt = origstart;
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);    // add custom time range below
            query = query.Where(e => e.CODE == "304239487");
            query = query.Where(e => e.ORDER_STATUS == null || e.ORDER_STATUS == ""); // need to update order_status=x
            query = query.Where(e => e.RESULT.ToLower() == "discontinued");
            query = query.Where(e => e.EVENT_DATETIME >= initdt);
            query = query.OrderBy(e => e.EVENT_DATETIME);
            Program.VerboseAudit("qcount=" + query.Count());
            if (query.Count() > 0)
            {
                found_dc = true;
                foreach (var item in query)
                {
                    if (first)
                    {
                        first = false;
                        if (item.EVENT_DATETIME >= initdt) //valid discontinued exists
                        {
                            dcdt = item.EVENT_DATETIME;
                            if (origstart < Program.g_pull_finish.AddHours(-12)
                                && origstart >= Program.g_pull_finish.AddHours(-24))
                            {
                                //make for origstart to item.evdt
                                Program.VerboseAudit("2A: dc time=" + item.EVENT_DATETIME);
                                pnum = GetSitterType(origstart);
                                AddSimpleProc(pnum, origstart, item.EVENT_DATETIME);
                                DisableSitter(item.CODE, origstart, true);
                            }
                            else if (item.EVENT_DATETIME <= Program.g_pull_finish.AddHours(-12))
                            { // discontinued in past; neutralize this pair.
                                Program.VerboseAudit("1: dc time=" + item.EVENT_DATETIME);
                            }
                            else if (initdt < Program.g_pull_finish.AddHours(-12))
                            {
                                //make for pull start to item.evdt
                                Program.VerboseAudit("2: dc time=" + item.EVENT_DATETIME);
                                pnum = GetSitterType(initdt);
                                AddSimpleProc(pnum, Program.g_pull_finish.AddHours(-12), item.EVENT_DATETIME);
                            }
                            else if (item.EVENT_DATETIME <= Program.g_pull_finish)
                            {
                                Program.VerboseAudit("3: dc time=" + item.EVENT_DATETIME);
                                //make for initdt to item.evdt
                                pnum = GetSitterType(initdt);
                                AddSimpleProc(pnum, initdt, item.EVENT_DATETIME);
                            }
                            else
                            {
                                Program.VerboseAudit("4: dc time=" + item.EVENT_DATETIME);
                                //make for initdt to _pat.pull finish
                                pnum = GetSitterType(initdt);
                                AddSimpleProc(pnum, initdt, Program.g_pull_finish);
                            }
                            DisableSitter(item.CODE, item.EVENT_DATETIME);
                            DisableSitter(item.CODE, item.EVENT_DATETIME, true);
                        }
                        Program.VerboseAudit("Sitter: " + item.RESULT + " " + item.EVENT_DATETIME.ToString());
                    }
                }
            }
            else if (initdt <= Program.g_pull_finish.AddHours(-12)) //no discontinued
            {
                //make for pull start to pull finish
                Program.VerboseAudit("5: dc time=" + Program.g_pull_finish);
                dcdt = Program.g_pull_finish;
                pnum = GetSitterType(initdt);
                AddSimpleProc(pnum, Program.g_pull_finish.AddHours(-12), Program.g_pull_finish);
            }
            else if (initdt < Program.g_pull_finish) // if initdt==g_pull_finish then dont create
            {
                Program.VerboseAudit("6: dc time=" + Program.g_pull_finish);
                dcdt = Program.g_pull_finish;
                found_dc = true;
                pnum = GetSitterType(initdt);
                AddSimpleProc(pnum, initdt, Program.g_pull_finish);
                DisableSitter("304239487", initdt);
                AddSitter("304239487", initdt, Program.g_pull_finish);
            }
            return found_dc;
        }

        private void AddSitter(string code, DateTime initdt, DateTime evdt)
        {
            string desc = "TC: Direct Observer";
            string res = "Initiated";
            DateTime timestmp = DateTime.Now;
            short seq = 0;
            int unitid = _pat.unit_id;

            using (var db = PFSDBUtility.NewSqlConnection())
            {
                string q = "INSERT INTO chart_item (encounter_id,event_datetime,code,description,result,timestamp,sequence,unit_id,procedure_start)";
                q += " select @encid, @evdt, @code, @desc, @res, @ts,@seq,@unit,@procstart where not exists";
                q += " (select encounter_id,code,event_datetime from chart_item where encounter_id=" + _pat.encounter_id.ToString() + " and code='" + code + "' and event_datetime=" + PFSDBUtility.SQLDateTime(evdt) + ")";
                //string q = "INSERT INTO chart_item (encounter_id,event_datetime,code,description,timestamp,sequence,unit_id,order_id) values (@encid, @evdt, @code, @desc, @ts,@seq,@unit,@oid)";
                SqlCommand cmd = new SqlCommand(q, db);
                cmd.Parameters.AddWithValue("@encid", _pat.encounter_id);
                cmd.Parameters.AddWithValue("@evdt", evdt);
                cmd.Parameters.AddWithValue("@code", code);
                cmd.Parameters.AddWithValue("@desc", desc);
                cmd.Parameters.AddWithValue("@res", res);
                cmd.Parameters.AddWithValue("@ts", timestmp);
                cmd.Parameters.AddWithValue("@seq", seq);
                cmd.Parameters.AddWithValue("@unit", unitid);
                cmd.Parameters.AddWithValue("@procstart", initdt);
                cmd.ExecuteNonQuery();
                db.Close();
            } //using db

        }

        private int GetSitterType(DateTime evdt)
        { // get the sitter type RN or non-RN from the charting that should exist
            //at the same time as the initiation of the observer 1540100298
            int pnum = 0;
            string return_result = "";
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);    // add custom time range below
            query = query.Where(e => e.CODE == "1540100298");
            query = query.Where(e => e.EVENT_DATETIME == evdt);
            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
                if (return_result.Trim().ToUpper().StartsWith("RN")) pnum = 1;
                if (return_result.Trim().ToUpper().StartsWith("NON-RN")) pnum = 2;
            }
            if (pnum == 0)
            {
                Program.VerboseAudit("Sitter type RN or non-RN not found. Defaulting to non_RN.");
                pnum = 2;
            }
            return pnum;
        }


        //====================================================
        //====================================================

        //private void CheckProc_1_2()
        //{
        //    Program.VerboseAudit("---------------");
        //    Program.VerboseAudit("MHA1. 1-1 safety observation by RN");
        //    Program.VerboseAudit("MHA2. 1-1 safety observation by non-RN");
        //    Program.VerboseAudit("---------------");
        //    //            304239487
        //    //1540100298
        //    bool stop = false;
        //    DateTime dcdt = DateTime.MinValue;
        //    while (!stop)
        //    {
        //        bool found_dc = false;
        //        bool first = true;
        //        var query = StartNewQuery(SearchDepth.SearchSinceAdmission);    // add custom time range below
        //        query = query.Where(e => e.CODE == "304239487");
        //        query = query.Where(e => e.ORDER_STATUS == null || e.ORDER_STATUS == ""); // need to update order_status=x
        //        query = query.Where(e => e.RESULT.ToLower() == "initiated" || e.RESULT.ToLower() == "continued");
        //        query = query.OrderBy(e => e.EVENT_DATETIME);
        //        if (query.Count() > 0)
        //        {
        //            foreach (var item in query)
        //            {
        //                if (first)
        //                {
        //                    found_dc = MakeActivityIfFindDiscontinue(item.EVENT_DATETIME);
        //                    dcdt = item.EVENT_DATETIME;
        //                    Program.VerboseAudit("Sitter: " + item.RESULT + " " + item.EVENT_DATETIME.ToString());
        //                    first = false;
        //                }
        //            }
        //        }
        //        if (found_dc)
        //        {
        //            UpdatePtChartArraysCode("304239487", "X", dcdt);
        //            stop = false;
        //        }
        //        else
        //            stop = true;
        //    }

        //}

        private void DisableSitter(string code, DateTime evdt, bool use_proc_start)
        {
            //update ORDER_STATUS = 'X' for _pat.encounter_id and ordid
            var db = PFSDBUtility.NewSqlConnection();
            //string q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and code='" + code + "' and (result='initiated' or result='continued') and event_datetime<=" + PFSDBUtility.SQLDateTime(evdt);
            string q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and code='" + code + "' and event_datetime<=" + PFSDBUtility.SQLDateTime(evdt);
            if (use_proc_start)
                q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and code='" + code + "' and procedure_start is not null and procedure_start<=" + PFSDBUtility.SQLDateTime(evdt);
            SqlCommand cmd = new SqlCommand(q, db);
            cmd.ExecuteNonQuery();
            db.Close();

        }
        private void DisableSitter(string code, DateTime evdt)
        {
            DisableSitter(code, evdt, false);
        }

        private void UpdatePtChartArraysCode(string code, string ordstatus, DateTime dcdt)
        {
            foreach (var item in _chart_items_since_admission)
            {
                if (item.CODE == code && item.EVENT_DATETIME <= dcdt)
                {
                    item.ORDER_STATUS = ordstatus;
                }
            }
        }

        private bool ProcExistsInDB(int pnum, DateTime startdt, out DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            Program.VerboseAudit("ProcExistsInDB: pnum=" + pnum + " startdt=" + startdt.ToString());
            int ct = 0;
            enddt = DateTime.MinValue;
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            //&& (proc.PROCEDURE_DATETIME <= startdt)
                            && (proc.DEPARTURE_DATETIME > startdt)
                            && (((pnum > 2) && (ans.PROCEDURE_NUMBER == pnum)) || ((pnum <= 2) && (ans.PROCEDURE_NUMBER <= 2)))
                        orderby proc.DEPARTURE_DATETIME descending
                        select new { proc.DEPARTURE_DATETIME };
            ct = query.Count();
            if (ct > 0)
                enddt = (DateTime)query.First().DEPARTURE_DATETIME;
            Program.VerboseAudit("ProcExistsInDB: pnum=" + pnum + " returns " + ct);
            return ct > 0;
        }
        private void CheckProc_3()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MHA3. Off unit accompanied by RN");
            Program.VerboseAudit("---------------");

            int actnum = 3;
            string actcode = "304239042";
            string actlencode = "304239043";
            string actst = "304237002101";
            CheckActivity(actnum, actcode, actst, actlencode);

        }


        private void CheckProc_4()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MHA4. Off unit accompanied by non-RN");
            Program.VerboseAudit("---------------");

        int actnum = 4;
        string actcode = "304239044";
        string actlencode = "304239045";
        string actst = "304237002111";
        CheckActivity(actnum, actcode, actst, actlencode);

        }

        private void AddSimpleProc(int pnum, DateTime evdt, DateTime enddt)
        {
            if (pnum <= 0) return;

            if (ProcExists(pnum, evdt, enddt))
            {
                Program.Audit("Activity " + pnum + ": already exists");
            }
            else
            {
                //if (ActivityFits(evdt, enddt))
                //{
                    ProcOverlapsInDB_PEID(pnum, evdt, enddt); // then delete the db
                    var proc = new proc_data();
                    proc.procedure_number = pnum;
                    proc.start = evdt;
                    proc.finish = enddt;
                    _procs.Add(proc);
                    Program.Audit("Activity " + pnum + ": Found between " + evdt + " and " + enddt);
                //}
            }
        }
        private bool ActivityFits(DateTime beg, DateTime fin)
        {
            bool ok = false;
            int unit_id = 0;
            string sql = "select el.unit_id from ENCOUNTER_LOCATION as el";
            sql += " where el.ENCOUNTER_ID=" + _pat.encounter_id;
            sql += " and el.SPECIAL_UNIT_ID is null";
            sql += " and el.EFFECTIVE_DATETIME_IN<='" + beg.ToString() + "'";
            sql += " and el.EFFECTIVE_DATETIME_OUT>='" + fin.ToString() + "'";

            Program.VerboseAudit(sql);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd = new SqlCommand(sql, db2);
            SqlDataReader dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr2.Read())
            {
                if (dr2["UNIT_ID"] != DBNull.Value)
                    unit_id = PFSDBUtility.DBToInt(dr2["UNIT_ID"]);
            }
            ok = (unit_id > 0);
            db2.Close();
            return ok;
        }
        private bool ProcOverlapsInDB_PEID(int pnum, DateTime startdt, DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            //            LoadPatientProceduresIfNeeded();
            bool overlap_exists = false;
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && ((proc.PROCEDURE_DATETIME >= startdt) && (proc.PROCEDURE_DATETIME < enddt)
                                ||
                                (proc.DEPARTURE_DATETIME > startdt) && (proc.DEPARTURE_DATETIME <= enddt)
                                ||
                                (proc.PROCEDURE_DATETIME < startdt) && (proc.DEPARTURE_DATETIME > enddt)
                                )
                            //&& ( ! (proc.PROCEDURE_DATETIME == startdt) && (proc.DEPARTURE_DATETIME == enddt))
                            && (proc.CLASSIFIED_BY_ID < 0)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new { proc.PROCEDURE_EVENT_ID, proc.PROCEDURE_DATETIME, proc.DEPARTURE_DATETIME };
            overlap_exists = (query.Count() > 0);
            foreach (var a in query)
            {
                Program.VerboseAudit("Will Delete act: procdt=" + a.PROCEDURE_DATETIME.ToString() + "  depdt=" + a.DEPARTURE_DATETIME.ToString());
                Program.VerboseAudit("because it overlays startdt=" + startdt.ToString() + "  enddt=" + enddt.ToString());
                DeleteActivity(a.PROCEDURE_EVENT_ID);
            }
            //            peid = 0;
            return (overlap_exists);
        }
        private void DeleteActivity(int peid)
        {
            //            delete PROCEDURE_ANSWER where PROCEDURE_EVENT_ID=7211351
            //delete RPT_PROC_BY_DAY where PROCEDURE_EVENT_ID=7211351
            //delete PROCEDURE_EVENT where PROCEDURE_EVENT_ID=7211351
            if (peid == 0) return;

            Program.VerboseAudit("db ProcAnsw Deleting peid=" + peid);
            var db = PFSDBUtility.NewPfsDataContext();
            var query = from ia in db.PROCEDURE_ANSWERs
                        where (ia.PROCEDURE_EVENT_ID == peid)
                        select ia;
            if (query.Count() > 0)
            {
                var items = query.ToList();
                foreach (var item in items)
                    db.PROCEDURE_ANSWERs.DeleteOnSubmit(item);
                db.SubmitChanges();
            }

            Program.VerboseAudit("db RptProc Deleting peid=" + peid);
            var db2 = PFSDBUtility.NewPfsDataContext();
            var query2 = from r in db2.RPT_PROC_BY_DAYs
                         where (r.PROCEDURE_EVENT_ID == peid)
                         select r;
            if (query2.Count() > 0)
            {
                var items2 = query2.ToList();
                foreach (var item2 in items2)
                    db2.RPT_PROC_BY_DAYs.DeleteOnSubmit(item2);
                db2.SubmitChanges();
            }

            Program.VerboseAudit("db ProcEvent Deleting peid=" + peid);
            var db3 = PFSDBUtility.NewPfsDataContext();
            var query3 = from ce in db3.PROCEDURE_EVENTs
                         where (ce.PROCEDURE_EVENT_ID == peid)
                         select ce;
            if (query3.Count() > 0)
            {
                var items3 = query3.ToList();
                foreach (var item3 in items3)
                    db3.PROCEDURE_EVENTs.DeleteOnSubmit(item3);
                db3.SubmitChanges();
            }
        }

        private bool QueuedProcOverlaps(int pnum, DateTime startdt, DateTime enddt)
        {
            bool overlap = false;

            proc_data[] pary = _procs.ToArray();
            for (int i = 0; i <= pary.GetUpperBound(0); i++)
            {
                if (pary[i].procedure_number == pnum)
                {
                    //overlap if   p.start between stardt and enddt
                    //             p.finish between startdt and enddt
                    //             startdt >= p.start and enddt <= p.finish
                    if ((pary[i].start >= startdt) && (pary[i].start < enddt) ||
                        (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
                        (startdt >= pary[i].start) && (enddt <= pary[i].finish))
                    {
                        overlap = true;
                    }

                    if ((pary[i].start >= startdt) && (pary[i].start < enddt) && (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
                        (pary[i].start < startdt) && (pary[i].finish > startdt) && (pary[i].finish < enddt)
                        )
                    {
                        // the proc in the list is completely contained in startdt/endt pair
                        // change this proc to be the startdt/enddt
                        // overlap is still true, but the list item will be updated with the encompassing times
                        //Program.Audit("Procedure " + pnum + ": " + startdt + " - " + enddt + " will trump " + pary[i].start + " - " + pary[i].finish);
                        pary[i].start = startdt;
                        pary[i].finish = enddt;
                    }
                }
            }
            _procs = pary.ToList();
            return overlap;
        }


        private bool ProcExists(int pnum, DateTime startdt, DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && (proc.PROCEDURE_DATETIME == startdt)
                            && (proc.DEPARTURE_DATETIME == enddt)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new { proc.PROCEDURE_EVENT_ID };
            return (query.Count() > 0);
        }
        private void CheckProc_5()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MHA5. 1:1 Continuous Observation >1 Hour by RN");
            Program.VerboseAudit("---------------");

            int actnum = 5;
            string actcode = "304239046";
            string actlencode = "304239047";
            string actst = "304237002121";
            CheckActivity(actnum, actcode, actst, actlencode);

        }

        private void CheckProc_6()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MHA6. 1:1 Continuous Observation >1 Hour by non-RN");
            Program.VerboseAudit("---------------");
            int actnum = 6;
            string actcode = "304239048";
            string actlencode = "304239049";
            string actst = "304237002131";
            CheckActivity(actnum, actcode, actst, actlencode);
        }

        private void CheckProc_7()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MHA7. 2:1 Continuous Observation >1 Hour by RN");
            Program.VerboseAudit("---------------");

            int actnum = 7;
            string actcode = "304239215";
            string actlencode = "304239217";
            string actst = "304237002141";
            CheckActivity(actnum, actcode, actst, actlencode);

        }


        private const string DATETIME_FORMAT = "yyyyMMddHHmm";              // ISO Date/Time w/o seconds

        private void OutputClass(bool use_default)
        {
            string outstr, ind_list, desc, str_pull_dt, str_in_dt, str_out_dt = "";
            int i, tc_event_id;

            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         212 242 304
            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1
            //1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901 3456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
            //1       |10 5            |                |                |        |10023121779         |ALEXANDER                       |GLORIA                          |                                |0508    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289530  |           |201403030400                                                                      |NNYNNNNYYNNYNNNYNNNYYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
            //1       |10 5            |                |                |        |10024544908         |ARMSTRONG                       |AARON                           |                                |0535    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289531  |           |201403030400                                                                      |NYNNYNNNNNNYNNNYNNNNYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
            //|0547    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289532  |           |201403030400                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN
            //|0547    |A   |201403030700|                |201403030700    |18  |C|    |3040|1440|18289532  |           |201403030700                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN
            //            1RO | RO MB5BG / 6E |                |                |        | 2000180316769 | PARISIEN | GREYSON | LAWRENCE | RMB5514 | P | 20190319030000 |                               | 20 | C |    | 5399 | 480 | 62040560 |           | 20190319030000 | 20190319070000 | NNNYNNNNNNNNYNNNYNYNYNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN

            if (Program.g_is_test)
                tc_event_id = 9999;
            else
                tc_event_id = PFSDBUtility.NextGID();                         //get a unique id for this class
                                                                              //            str_pull_dt = _pat.pull_finish.ToString(DATETIME_FORMAT);
                                                                              //str_pull_dt = _pat.pull_finish.ToString(DATETIME_FORMAT);
                                                                              //if (_pat.unit_departure != null && _pat.unit_departure != DateTime.MinValue)
            //if (_pat.effective_out == Program.g_pull_finish && _pat.unit_departure == DateTime.MinValue)
            //{
            //    //str_in_dt = Program.g_effdt.ToString(DATETIME_FORMAT);
            //    str_in_dt = loc_in.ToString(DATETIME_FORMAT);
            //    str_out_dt = "";
            //}
            //else
            //{
            //    //str_in_dt = Program.g_effdt.ToString(DATETIME_FORMAT);
            //    str_in_dt = loc_in.ToString(DATETIME_FORMAT);
            //    str_out_dt = "";// _pat.effective_out.ToString(DATETIME_FORMAT);
            //}
            if (loc_in != Program.g_pull_start_save)
            {
                //str_in_dt = Program.g_effdt.ToString(DATETIME_FORMAT);
                str_in_dt = loc_in.ToString(DATETIME_FORMAT);
                str_out_dt = "";
                str_out_dt = loc_out.ToString(DATETIME_FORMAT);
            }
            else
            {
                str_in_dt = Program.g_effdt.ToString(DATETIME_FORMAT);
                str_out_dt = "";// _pat.effective_out.ToString(DATETIME_FORMAT);
                str_out_dt = loc_out.ToString(DATETIME_FORMAT);
            }
            if (!Program.g_output_outtime) str_out_dt = "";//leave open-ended 5/3/21

            outstr = "".FixedWidth(8);                       //(facility code)
            outstr += "|" + _pat.unit_name.FixedWidth(16);
            outstr += "|" + "".FixedWidth(16);                               //(unit code)
            outstr += "|" + txarea.FixedWidth(16);                               //(area code)
            outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
            outstr += "|" + _pat.acct.FixedWidth(20);
            outstr += "|" + _pat.last_name.FixedWidth(32);
            outstr += "|" + _pat.first_name.FixedWidth(32);
            outstr += "|" + _pat.middle_name.FixedWidth(32);
            outstr += "|" + _pat.room.FixedWidth(8);
            outstr += "|" + _pat.bed.FixedWidth(4);
            outstr += "|" + str_in_dt;        //CLASS dt
            outstr += "|" + "".FixedWidth(14);                               //(login)
            outstr = outstr.FixedWidth(232);
            outstr += "|" + _pat.effective_out.ToString(DATETIME_FORMAT);//str_out_dt;        //TC Data End Point
            outstr = outstr.FixedWidth(249);
            outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
            outstr += "|" + "C".FixedWidth(1);                               //record type = class
            outstr += "|" + "".FixedWidth(4);                                //(stage)
            outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
            outstr += "|" + _pat.range.ToString().FixedWidth(4);             //TC pull range
            outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
            outstr += "|";
            outstr = outstr.FixedWidth(294);
            outstr += "|" + str_in_dt;        //IN
            outstr = outstr.FixedWidth(346);
            //if (_pat.unit_departure != null && _pat.unit_departure != DateTime.MinValue)
            //                outstr += "|" + _pat.unit_departure.ToString(DATETIME_FORMAT);        //OUT
            outstr += "|" + str_out_dt;        //OUT
            outstr = outstr.FixedWidth(377);
            outstr += "|";

            //if (use_default)
            //{ //make all is_checked = false and then mark defaults
            //    Program.VerboseAudit("Patient Will receive default indicators " + _pat.default_inds_str);
            //    for (i = 1; (i <= MAX_INDS); i++)
            //    {
            //        _inds[i].is_checked = false;
            //    }
            //    foreach (var ind in _pat.default_inds)
            //    {
            //        if (ind <= _inds.GetUpperBound(0))
            //        {
            //            _inds[ind].is_checked = true;
            //        }
            //    }
            //}

            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++)
            {
                if (_inds[i].is_checked)
                {
                    outstr += "Y";
                    ind_list += "" + i;
                }
                else
                {
                    outstr += "N";
                }
            } // next i
            ind_list = ind_list.Substring(1);                           //strip leading comma
                                                                        //                                                                                                   1                                                                                                   2                                                                                                   3
                                                                        //         1         2         3         4         5         6         7         8         9         0         1         2         3         4         5         6         7         8         9         0         1         2         3         4         5         6         7         8         9         0         1         2         3         4         5         6         7         8         9         0
                                                                        //1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
                                                                        //1       |DO6D            |                |                |        |2000192224892       |BEHNAM                          |KENDRA                          |LEE                             |RDO6311 |P   |20180717110000|                               |20  |C|    |5399|480 |56103278  |           |20180717110000                                     |                              |YNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN


            string str5am;
            Program.outfile.WriteLine(outstr);      //1900yesterd output to transparent.txt
            if (Program.g_make5am)
            {
                //if (str_out_dt.Substring(8, 4) == "0500") //create the 7am at 3am
                {
                    string strdttm = Program.g_pull_finish.ToString(DATETIME_FORMAT);
                    strdttm = strdttm.Substring(0, 8) + "0500";
                    //str5am = outstr.Substring(0, 203) + Program.g_pull_finish.ToString(DATETIME_FORMAT) + outstr.Substring(217, 78) + Program.g_pull_finish.ToString(DATETIME_FORMAT) + " ".Repeat(69) + outstr.Substring(378, 120);
                    str5am = outstr.Substring(0, 203) + strdttm + outstr.Substring(215, 80) + strdttm + " ".Repeat(71) + outstr.Substring(378, 120);
                    Program.outfile2.WriteLine(str5am);
                }
            }

            Program.Audit("");
            desc = "Classified: " + ind_list;
            if (Program.g_is_test)
            {
                Program.Audit(desc);
            }
            else
            {
                //Save the selected indicators plus both Program.Audits in the event log; link with tc_event_id
                PFSEventLog.AddTransparentMappingEventLogEntry(
                    desc, Program.gLogUnitID, Program.gLogEncounterID,
                    tc_event_id, Program.gLogMapperVersion,
                    Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
            }
        }

        private void OutputProcs()
        {
            int i;
            string outstr, proc_list, desc;
            int tc_event_id;

            foreach (var proc in _procs)
            {
                if (Program.g_is_test)
                    tc_event_id = 9999;
                else
                    tc_event_id = PFSDBUtility.NextGID();                         //get a unique id for this proc

                outstr = "".FixedWidth(8);                       //(facility code)
                outstr += "|" + _pat.unit_name.FixedWidth(16);
                outstr += "|" + "".FixedWidth(16);                               //(unit code)
                outstr += "|" + txarea.FixedWidth(16);                               //(area code)
                outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
                outstr += "|" + _pat.acct.FixedWidth(20);
                outstr += "|" + _pat.last_name.FixedWidth(32);
                outstr += "|" + _pat.first_name.FixedWidth(32);
                outstr += "|" + _pat.middle_name.FixedWidth(32);
                outstr += "|" + _pat.room.FixedWidth(8);
                outstr += "|" + _pat.bed.FixedWidth(4);
                outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //204 proc dt
                outstr += "|" + "".FixedWidth(14);                               //(login)
                outstr = outstr.FixedWidth(249);
                outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
                outstr += "|" + "P".FixedWidth(1);                               //record type = class
                outstr += "|" + "".FixedWidth(4);                                //(stage)
                outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
                outstr += "|" + _pat.range.ToString().FixedWidth(4);             //TC pull range
                outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
                outstr += "|";
                outstr = outstr.FixedWidth(294);
                outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //296 procdt in
                outstr = outstr.FixedWidth(346);
                //if (_pat.unit_departure != null && _pat.unit_departure != DateTime.MinValue)
                //                outstr += "|" + _pat.unit_departure.ToString(DATETIME_FORMAT);        //OUT
                outstr += "|" + proc.finish.ToString(DATETIME_FORMAT);          //348 procdt out
                outstr = outstr.FixedWidth(377);
                outstr += "|";

                proc_list = "";
                for (i = 1; (i < MAX_PROCS); i++)
                {
                    if (proc.procedure_number == i)
                    {
                        outstr += "Y";
                        proc_list += "," + i;
                    }
                    else
                    {
                        outstr += "N";
                    }
                } // next i
                proc_list = proc_list.Substring(1);                             //strip leading comma

                Program.outfile.WriteLine(outstr);                              //output to transparent.txt

                desc = "Activities: " + proc_list;
                if (Program.g_is_test)
                {
                    Program.Audit(desc);
                }
                else
                {
                    //Save the selected procedures plus both Program.Audits in the event log; link with tc_event_id
                    //(this assumes the procedure Program.Audit is mixed in with the class Program.Audit)
                    PFSEventLog.AddTransparentMappingEventLogEntry(desc, Program.gLogUnitID, Program.gLogEncounterID,
                        tc_event_id, Program.gLogMapperVersion,
                        Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
                }
            } // next proc
        } // OutputProcs

    } //class MentalHealth
}
