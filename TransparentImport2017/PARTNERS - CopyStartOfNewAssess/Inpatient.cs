﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;                     // for LINQ; add a reference
using System.Data.SqlClient;                // for SqlConnection
using System.IO;                            // for Directory, Path, Stream
using PfsShared;                            // add a reference to Shared2 project

// ============================================================================
// Inpatient transparent mapping -- GOES HERE --
// PARTNERS EPIC
// ============================================================================
// This processes one patient.  Only one database query is used.  :)
//
// All search functions use exact match for category, description and field name.
// Codes and results can be a single word or a list of words.
// Codes default to exact match; prefix with CODE_CODE_LIKE_PREFIX if desired (single word only)
// Results default to like matching; prefix with RESULT_EXACT_MATCH_PREFIX if desired (single word only)
//
// All searches are case insensitive.
//
//
namespace TransparentMapping
{
    class Inpatient
    {
        private const int MAX_INDS = 120;
        private const int MAX_PROCS = 20;

        private const string CODE_LIKE_PREFIX = "%!";       // use to make codes %like%
        private const string EXACT_MATCH_PREFIX = "&!";     // use to make desc/result exact match
        private const string CHAR_COMMA = "||";             // use to insert literal comma in word list
        private const string NOT_PREFIX = "!!";             // use to insert literal comma in word list
        private const string AVOID_NEGATIVE = "!;";
        private const string EXACT_SQL_PREFIX = "!SQL!";     // use to give sql constraint as given
        private const string STARTS_WITH = "[!";     // use to give sql constraint as given

        private struct indicator_data {
            public bool is_checked;
            public int radio_group;
            public int weight;
        }

        private struct proc_data {
            public int procedure_number;
            public DateTime start;
            public DateTime finish;
        }

        // These are database CHART_ITEMs for this patient
        private CHART_ITEM[] _chart_items_since_admission;
        //private CHART_ITEM[] _chart_items_since_unit_arrival;
        //private CHART_ITEM[] _chart_items_during_pull_period;
        //private CHART_ITEM[] _chart_items_pull_period_plus;
        //private CHART_ITEM[] _chart_items_since24hrs;
        //private CHART_ITEM[] _chart_items_since13hrs;
        private CHART_ITEM[] _chart_items_since24hrs;
        //private CHART_ITEM[] _chart_items_since9hrs;
        // These are database PROCEDURE_EVENTs for this patient
        private PROCEDURE_EVENT[] _procedure_events;

        private indicator_data[] _inds;
        private List<proc_data> _procs;
        private List<proc_data> _outcomes;

        private const int DEFAULT_BUCKET_SIZE = 60;       //min

        private PatientInfo _pat;
        private int _bucket_size = DEFAULT_BUCKET_SIZE;
        private bool is_default;
        private string txarea;
        private bool periop_found_inpast13hrs = false;

        private bool exclude_periop_data = false;
        private bool exclude_periph_iv = false;
        private bool coma = false;
        private bool g_toi4 = false;
        private bool g_gitube_attempted = false;
        private bool g_gitube = false;
        private DateTime loc_in;
        private DateTime loc_out;
        private DateTime loc_arrtime;
        int rassct12 = 0;
        int rassct34 = 0;
        int rassctLTzero = 0;
        private LOAtypePrecision[] aryloa = new LOAtypePrecision[10];
        private int numloa=0;
        private LOAtypePrecision[] ary_hemodial = new LOAtypePrecision[5];
        private int numhemodial = 0;
        private LOAtypePrecision[] aryclassdnc = new LOAtypePrecision[10];
        private int numclassdnc = 0;
        private bool g_lda6yo = false;

        private string assessgrouplabel = "";

        private enum SearchDepth {
            SearchDefault,
            SearchPullRange,            //search within the current pull     -- default
            SearchSinceArrival,         //search since arrival to the unit
            SearchSinceAdmission,        //search everything since admission to the hospital
            SearchPullPlus,
            SearchSince24Hrs,
            SearchSince13Hrs,
            SearchSince12Hrs,
            SearchSince16Hrs,
            SearchSince9Hrs,
            SearchSince4Hrs
        }

        private enum CountMode {
            CountAll,
            CountFirst                  //stop after one is found
        }

        private enum GetValueMode {
            GetTotal,
            GetMax,
            GetLast
        }

        enum Frequencies {
            QNONE,
            Q4H,
            Q2H,
            Q1H,
            Q30M
        }

        private struct fmapRow {
            public double los_high;                       //the LOS being testing
            public int[] freq;                           //the count required for each Q value
        }
        private List<fmapRow> _freq_map;                    //1,2,4,8,12,24 hours

        private struct gBucket
        {
            public int bucket;
            public string code;
            public DateTime evdt;
            public bool using_waiver;
            public bool has_all_deps; //has all dependents
            public int num_addl_items;
            public string description;
        }
        private struct gGap
        {
            public int gap;
            public DateTime evdt1;
            public DateTime evdt2;
        }

        private struct MedChartItem
        {
            public string code;
            public string orderid;
            public DateTime evdt;
            public bool valid;
        }

        static string[] cat_ary = { "Cardiovascular", "GI/GU",
            "I/O", 
            "Neuro",
            "OB Assessment",
            "Pain",
            "Pulmonary",
            "Technology",
            "Vitals",
            "Wound Site"};
        bool[,] ar1 = new bool[cat_ary.GetUpperBound(0)+1, 4];

        //private string[] meds_rate = { "newbag", "new bag", "rateverify", "rate verify", "restarted", "ratechange", "rate change", "started", "continued" };
        //private struct med2026and5077
        //{
        //    public string name;
        //    public bool found;
        //}
        //
        // This is the main entry point
        //
        public void ProcessPatient(PatientInfo pat)
        {
            _pat = pat;
            bool use_default = false;
            bool no_chart_items_in_24hrs = false;
            bool loa_exists = false;
            bool do_class = true;

            InitIndicators(); // sets is_default
            InitProcs();
            if (!is_default)
            {
                LoadFreqTable();
                //CheckIfPeriopInPast13hrs();
                no_chart_items_in_24hrs = (LoadPatientChart() == 0);
                Program.VerboseAudit("New query default scope = " + loc_in + " to " + loc_out);
                if (_pat.short_name.ToUpper().StartsWith("S"))
                {   // for Spaulding only; do_loa = !noactivities
                    loa_exists = SetLOARanges(!Program.g_noactivities);
                    Program.Audit("loa_exists=" + (loa_exists?"yes":"no"));
                    if (loa_exists && Program.g_noactivities) do_class = false;
                    if (!loa_exists && Program.g_noactivities) do_class = true;
                    Program.Audit("do_class=" + (do_class ? "yes" : "no"));
                    //RemoveChartingsDuringLOA();//is this really necc? pt wont be there
                }
                if (no_chart_items_in_24hrs)
                    Program.Audit("No chart items received in past 24 hrs.");
                if (!Program.g_onlydnc && do_class)
                {
                    Check_1_2_3_4();
                    Check_5();
                    Check_6_7();
                    Check_8();
                    Check_9();
                    Check_10_11();
                    Check_12_13();
                    Check_14();
                    //Program.sttimer();
                    Check_15_16_17_18();
                    //Program._t3 = Program.entimer("t3=", Program._t3);
                    Check_19();
                    Check_20();
                    Check_21_22();
                    Check_23();
                    Check_24();
                    //CheckUserDefined();
                    AtLeastOneADL();
                }
            }

            //if (!no_chart_items_in_24hrs)
            {
                if (!Program.g_onlydnc && do_class)
                {
                    HighestIndicatorInEachGroupWins();

                    if (!is_default)
                    {
                        _pat.ptype = DeterminePtypeOfIndicators();
                        //Program.sttimer();
                        if (!Program.g_noactivities) CheckProcs();
                        //Program._t4 = Program.entimer("t4=", Program._t4);
                        //CheckOutcomes();
                        //if (Program.g_do_OW) CheckOtherWorkload();
                    }

                    if (Program.g_no_output) return;
                    //if (_pat.default_ptype > DeterminePtypeOfIndicators())
                    //{ // if the default pt type is higher than the pt type of this class
                    //  // then if there is a default classification in the past 16 hrs
                    //  // then make these indicators the default indicators.
                    //    use_default = ExistDefaultInPast16hrs();
                    //    if (use_default)
                    //    {
                    //        Program.VerboseAudit("Default indicators will be used");
                    //    }
                    //}
                    OutputClassAndDNCTimes();
                    OutputClassAndDNC(do_class);
                    if (!Program.g_noactivities) OutputProcs();
                }
                else
                {
                    //if (!Program.g_noactivities) OutputDNC();
                }
                //OutputOutcomes();
            }
        }

        private void CheckIfPeriopInPast13hrs()
        {
            foreach (var perioploc in Program.patperioplist)
            {
                periop_found_inpast13hrs = periop_found_inpast13hrs
                    || (perioploc.in_time >= _pat.pull_finish.AddHours(-13))
                    || (perioploc.out_time >= _pat.pull_finish.AddHours(-13));
            }
        }

        private void InitIndicators()
        {
            // Make a new (empty) indicator array
            _inds = new indicator_data[MAX_INDS + 1];           // This 1 based so add one
            is_default = false;
            txarea = "";
            //if (_pat.los_hours <= 4.0)
            //{
            //    is_default = true;
            //    Program.VerboseAudit("Patient was here 4 hrs or less. Will receive default indicators " + _pat.default_inds_str);
            //    foreach (var ind in _pat.default_inds)
            //    {
            //        if (ind <= _inds.GetUpperBound(0))
            //        {
            //            _inds[ind].is_checked = true;
            //        }
            //    }
            //}
            Program.VerboseAudit("initializing assessments array");
            for (int i = 0; (i <= 9); i++)
                for (int j = 0; (j <= 3); j++)
                    ar1[i, j] = false;
            Program.VerboseAudit("completed assessments array init");


            // get indicator radio groups from the database
            // ** (This database access can be replaced once we have a C# methodolgy cache)
            var db = PFSDBUtility.NewPfsDataContext();
            var query = from idef in db.INDICATOR_DEFINITIONs
                        where (idef.METHODOLOGY_ID == _pat.meth_id)
                        select idef;
            foreach (var idef in query) {
                if (idef.INDICATOR_NUMBER <= _inds.GetUpperBound(0)) {
                    // (convert nulls to zero)
                    _inds[idef.INDICATOR_NUMBER].radio_group = PFSDBUtility.DBToInt(idef.RADIO_GROUP);
                    _inds[idef.INDICATOR_NUMBER].weight = PFSDBUtility.DBToInt(idef.WEIGHT);
                }
            }
        }

        private void InitProcs()
        {
            _procs = new List<proc_data>();
            _outcomes = new List<proc_data>();
        }

        private fmapRow LoadFreqTableRow(double los_high, string values)
        {
            fmapRow fmrow;

            fmrow.los_high = los_high;
            fmrow.freq = new int[Enum.GetNames(typeof(Frequencies)).Length];

            var arr = values.Split(',');
            for (int i = 0; i <= fmrow.freq.GetUpperBound(0); i++)
            {
                fmrow.freq[i] = arr[i].ToInteger();
            }
            return fmrow;
        }

        // These are the number of times you need to do something during a certain time period (LOS)
        // in order for it to be considered one of the frequencies listed.
        private void LoadFreqTable()
        {
            _freq_map = new List<fmapRow>();
            //                              LOS,  None Q4h Q2h Q1h Q30m
            _freq_map.Add(LoadFreqTableRow(1, "    0,  0,  0,  1,  2"));
            _freq_map.Add(LoadFreqTableRow(2, "    0,  0,  1,  2,  4"));
            _freq_map.Add(LoadFreqTableRow(4, "    0,  1,  2,  4,  8"));
            _freq_map.Add(LoadFreqTableRow(6, "    0,  2,  3,  6,  9"));
            _freq_map.Add(LoadFreqTableRow(8, "    0,  2,  4,  8, 12"));
            _freq_map.Add(LoadFreqTableRow(12, "   0,  3,  6, 12, 18"));
            _freq_map.Add(LoadFreqTableRow(16, "   0,  4,  8, 16, 24"));
            _freq_map.Add(LoadFreqTableRow(24, "   0,  6, 12, 24, 36"));
            //New freq table 2/5/14
            //q4	q2	q1	q30     q30
            //            Non-ICU	ICU & SD
            // 4	8	15	29	    36
            // 3	5	9	17	    24
            // 2	4	7	13	    19
            // 2	3	5	10	    13

            //to read this table: if you have at least 6 asessmenets in a 12-hour period, you get Q1h
            //(LOS=12, column Q1h has a count of 6)
        }

        //private Frequencies FreqForCount(double los_hours, int count)
        //{
        //    foreach (var fmrow in _freq_map) {
        //        if (los_hours <= fmrow.los_high) {
        //            // Option: pro-rate the count upward if the LOS is less than the row we are interested in.
        //            //         This will bump the count to what it might have been at the full LOS.
        //            // Note: truncate the result; rounding inflates the value too much.
        //            int prorated_count = (int)((fmrow.los_high / los_hours) * count);

        //            // foreach goes low to high; go from high to low instead
        //            for (int j = (int)Frequencies.Q30M; (j > (int)Frequencies.QNONE); j--) { //search right to left
        //                if (prorated_count >= fmrow.freq[j]) {
        //                    return (Frequencies)j;
        //                }
        //            } // next j
        //        }
        //    }

        //    return Frequencies.QNONE;
        //}

        //
        // Load patient chart from the database (one DB access)
        // Load everything since admission
        //
        private int LoadPatientChart()
        {
            // Hemodialysis   Started Completed  no more than 4 hours
            // 3040100125

            //            Placement of UVC
            //3042394421 date
            //3042394422 time
            //900700 date
            //900701 time

            //Removal of UVC
            //3042394424
            //3042394425
            //900702
            //900703
            foreach (var p in Program.patloclist)
            {
                if (p.loc_idx == _pat.loc_idx)
                {
                    loc_in = p.in_time;
                    loc_out = p.out_time;
                    loc_arrtime = p.arr_time;
                }
            }
            _pat.los_hours = PFSUtility.DateDiffInMinutes(loc_in, loc_out) / 60.0;
            Program.VerboseAudit("LoadChart los=" + _pat.los_hours);
            //Program.VerboseAudit("LoadChart unit=" + p.unit_name + " locidx=" + p.loc_idx + " in=" + p.in_time + " out=" + p.out_time);

            int ct_in_24hrs = 0;
            int ctperiop = 0;
            // Get the entire patient chart (all units and dates for this patient)
            var dba = PFSDBUtility.NewPfsDataContext();
            var queryall = from item in dba.CHART_ITEMs
                           where (item.ENCOUNTER_ID == _pat.encounter_id)
                           where (item.EVENT_DATETIME <= loc_out)
                         orderby item.EVENT_DATETIME
                         select item;
            //Get the ECT Aldrete score times
            var queryECT = (from e in queryall
                           where (e.CODE == "304239656")
                           select e.EVENT_DATETIME).Distinct().ToList();
            foreach (var ect in queryECT)
            {                
                Program.VerboseAudit("ECT time=" + ect);
            }
            //Exclude the ECT times
            var querya = from g in queryall
                         where (!queryECT.Contains(g.EVENT_DATETIME) 
                                || _pat.short_name == "MEE")
                         select g;
            // Save the result
            _chart_items_since_admission = querya.ToArray();
            Program.VerboseAudit("Count since adm=" + querya.Count());
            

            // var db = PFSDBUtility.NewPfsDataContext();
            //var query = from item in db.CHART_ITEMs
            //            where (item.ENCOUNTER_ID == _pat.encounter_id)
            //            where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-24))
            //            where (item.EVENT_DATETIME <= _pat.pull_finish)
            //            //where (item.EVENT_DATETIME >= loc_in && item.EVENT_DATETIME <= loc_out)
            //            select item;
            var query = from item in _chart_items_since_admission
                        where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-24) 
                               && (item.EVENT_DATETIME <= _pat.pull_finish))
                        select item;
            // Exclude hemodialysis items between Started and Completed (max=Started+4hrs)
            // Find Started/Completed pairs:
            bool starta = false;
            DateTime starta_dt = DateTime.MinValue;
            foreach (var c in query)
            {
                if (c.CODE == "3040100125")
                {
                    if (c.RESULT.ToLower().StartsWith("start"))
                    {
                        if (!starta)
                        {
                            starta = true;
                            starta_dt = c.EVENT_DATETIME;
                        }
                        else // then found a second start after the initial start
                        {
                            numhemodial++;
                            ary_hemodial[numhemodial].startdt = starta_dt;
                            if (c.EVENT_DATETIME >= starta_dt.AddHours(4))
                                ary_hemodial[numhemodial].enddt = starta_dt.AddHours(4);
                            else
                                ary_hemodial[numhemodial].enddt = c.EVENT_DATETIME;
                            starta_dt = c.EVENT_DATETIME;
                            Program.VerboseAudit("Ignoring HD items between: " + ary_hemodial[numhemodial].startdt + " => " + ary_hemodial[numhemodial].enddt + " [3]");
                        }
                    }
                    else if (c.RESULT.ToLower().StartsWith("complete"))
                    {
                        if (!starta) //then complete without a start: go back 4 hours.
                        {
                            numhemodial++;
                            ary_hemodial[numhemodial].startdt = c.EVENT_DATETIME.AddHours(-4);
                            ary_hemodial[numhemodial].enddt = c.EVENT_DATETIME;
                            Program.VerboseAudit("Ignoring HD items between: " + ary_hemodial[numhemodial].startdt + " => " + ary_hemodial[numhemodial].enddt + " [2]");
                        }
                        else
                        {
                            numhemodial++;
                            ary_hemodial[numhemodial].startdt = starta_dt;
                            ary_hemodial[numhemodial].enddt = c.EVENT_DATETIME;
                            Program.VerboseAudit("Ignoring HD items between: " + ary_hemodial[numhemodial].startdt + " => " + ary_hemodial[numhemodial].enddt + " [1]");
                        }
                        starta = false;
                    }
                }
            }
            if (starta)
            {
                numhemodial++;
                ary_hemodial[numhemodial].startdt = starta_dt;
                ary_hemodial[numhemodial].enddt = starta_dt.AddHours(4);
                Program.VerboseAudit("Ignoring HD items between: " + ary_hemodial[numhemodial].startdt + " => " + ary_hemodial[numhemodial].enddt + " [4]");
            }

            // Save the result
            ct_in_24hrs = query.Count();
            _chart_items_since24hrs = query.ToArray();

            // Convert all results to lower case for case insensitive comparisons
            foreach (var item in _chart_items_since24hrs) {
                item.SOURCE_TEXT = null;
                if (item.CATEGORY != null) item.CATEGORY = item.CATEGORY.ToLower();
                if (item.CODE != null) item.CODE = item.CODE.ToLower();
                if (item.DESCRIPTION != null) item.DESCRIPTION = item.DESCRIPTION.ToLower();
                if (item.FIELD_NAME != null) item.FIELD_NAME = item.FIELD_NAME.ToLower();
                if (item.RESULT != null) item.RESULT = item.RESULT.ToLower();
                for (int i=1; i<=numhemodial; i++)
                {
                    if (item.EVENT_DATETIME >= ary_hemodial[i].startdt 
                        && item.EVENT_DATETIME <= ary_hemodial[i].enddt)
                    {
                        item.UNIT_ID = -6;
                        ctperiop++;
                    }
                }
                //foreach (var perioploc in Program.patperioplist)
                //{
                //    if (item.EVENT_DATETIME >= perioploc.in_time && item.EVENT_DATETIME <= perioploc.out_time)
                //    {
                //        item.UNIT_ID = -6;
                //        ctperiop++;
                //    }
                //}
            }

            Program.VerboseAudit("Since 24 hrs count items=" + ct_in_24hrs);
            Program.VerboseAudit("Since 24 hrs count of HD items=" + ctperiop);

            return ct_in_24hrs;
        }

        // Delay looking for patient procedures until they are wanted
        private void LoadPatientProceduresIfNeeded()
        {
            if (_procedure_events != null) return;

            var db = PFSDBUtility.NewPfsDataContext();
            var query = from proc in db.PROCEDURE_EVENTs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                        where (proc.PROCEDURE_DATETIME >= _pat.pull_finish.AddHours(-24))
                        select proc;
            _procedure_events = query.ToArray();
        }



        // Started a new chart item query (of a certain depth)
        // Default is the chart during the pull range.  Options for unit arrival and admission to hospital.
        private IEnumerable<CHART_ITEM> StartNewQuery()
        {
            return StartNewQuery(SearchDepth.SearchDefault);
        }
        private IEnumerable<CHART_ITEM> StartNewQuery(SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;

            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    if (exclude_periop_data)
                        //return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_start && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= loc_in && (item.EVENT_DATETIME <= loc_out)) select item);
                        //return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-12)) select item);
                    else
                        //return (from item in _chart_items_since24hrs where (item.EVENT_DATETIME >= _pat.pull_start && item.EVENT_DATETIME <= _pat.pull_finish) select item);
                        return (from item in _chart_items_since24hrs where (item.EVENT_DATETIME >= loc_in && item.EVENT_DATETIME <= loc_out) select item);
                        //return (from item in _chart_items_since24hrs where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-12)) select item);
                //if (exclude_periop_data)
                //    return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-8) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                //else
                //    return (from item in _chart_items_since24hrs where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-8) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                //    return (from item in _chart_items_since_unit_arrival select item);
                case SearchDepth.SearchSinceAdmission:
                    return (from item in _chart_items_since_admission select item);
                //case SearchDepth.SearchPullPlus:
                //    return (from item in _chart_items_pull_period_plus select item);
                case SearchDepth.SearchSince24Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                    else
                        return (from item in _chart_items_since24hrs where (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince16Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-16) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since24hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-16) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince13Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-13) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since24hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-13) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince12Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-12) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since24hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-12) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince9Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-9) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since24hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-9) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince4Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME > _pat.pull_finish.AddHours(-4) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since24hrs where item.EVENT_DATETIME > _pat.pull_finish.AddHours(-4) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
            }
            return null;
        }

        // Is this search term a word or a list of words?
        private bool ValueIsAList(string s)
        {
            return (s != null) && s.Contains(',');                //is this a comma-separated list?
        }

        // Break apart a wordlist and prepare its members
        private string[] SplitOnCommaAndPrepareElements(string s)
        {
            // If a word list has a space after each comma, we will get leading blanks in each term
            var arr = s.Split(',');
            for (int i = 0; i <= arr.GetUpperBound(0); i++) {
                arr[i] = arr[i].Trim();                         // get rid of leading blanks
                arr[i] = arr[i].Replace(CHAR_COMMA, "");       // insert a literal comma into search word
                arr[i] = arr[i].ToLower();                      // convert for case-insensitive comparisons
            }
            return arr;
        }

        // Add a basic chart item filter to a LINQ query
        //
        // cat = exact match
        // code_list = exact match; optional "like" match
        // desc_list = "like" match; optional exact match
        // field = exact match
        // result_list = "like" match; optional exact match
        //
        // These are comma-separated lists.  
        // Search for a literal comma by using CHAR_COMMA in the string.
        private IEnumerable<CHART_ITEM> AndItemFilter(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list)
        {
            // Converted all values to lower case for case-insensitive comparisons.
            if (!String.IsNullOrEmpty(cat))
            {
                cat = cat.ToLower();
                query = query.Where(e => e.CATEGORY.ToLower() == cat);
            }

            if (!String.IsNullOrEmpty(code_list))
            {
                code_list = code_list.ToLower();
                if (code_list.Left(2) == CODE_LIKE_PREFIX)
                {            // override for "like" match?
                    query = query.Where(e => e.CODE.Contains(code_list.Substring(2)));
                }
                else if (code_list.Left(2) == EXACT_MATCH_PREFIX)
                {
                    query = query.Where(e => e.CODE == code_list.Substring(2));
                }
                else if (ValueIsAList(code_list))
                {
                    query = AndCodeInList(query, code_list);            // find one of the words
                }
                else
                {
                    query = query.Where(e => e.CODE.StartsWith(code_list));      // find this word
                }
            }

            if (!String.IsNullOrEmpty(desc_list))
            {
                desc_list = desc_list.ToLower();
                if (desc_list.Left(2) == EXACT_MATCH_PREFIX)
                {          // exact match override?
                    query = query.Where(e => e.DESCRIPTION == desc_list.Substring(2));
                }
                else if (desc_list.Left(2) == NOT_PREFIX)
                {
                    // query = query.Where(e => !e.DESCRIPTION.Contains(desc_list.Substring(2)));
                    query = AndDescriptionNOTInList(query, desc_list.Substring(2));
                }
                else if (ValueIsAList(desc_list))
                {
                    query = AndDescriptionInList(query, desc_list);
                }
                else
                {
                    desc_list = desc_list.Replace(CHAR_COMMA, "");     // look for literal comma requests
                    query = query.Where(e => e.DESCRIPTION.Contains(desc_list));
                }
            }

            if (!String.IsNullOrEmpty(field))
            {
                field = field.ToLower();
                query = query.Where(e => e.FIELD_NAME == field);
            }

            if (!String.IsNullOrEmpty(result_list))
            {
                query = query.Where(e => e.RESULT != null);
                result_list = result_list.ToLower();                        // make all results case insensitve
                if (result_list.Left(2) == EXACT_MATCH_PREFIX)
                {     // exact match override?
                    query = query.Where(e => e.RESULT == result_list.Substring(2));
                }
                else if (result_list.Left(2) == AVOID_NEGATIVE)
                {
                    query = query.Where(e => (!e.RESULT.Contains("no " + result_list.Substring(2))) && ((e.RESULT == result_list.Substring(2)) || (e.RESULT.Contains(";" + result_list.Substring(2)))));
                }
                else if (ValueIsAList(result_list))
                {
                    query = AndResultInList(query, result_list);
                }
                else if (result_list.Left(2) == STARTS_WITH)
                {
                    query = query.Where(e => e.RESULT.StartsWith(result_list.Substring(2)));
                }
                else
                {
                    result_list = result_list.Replace(CHAR_COMMA, "");     // look for literal comma requests
                    query = query.Where(e => e.RESULT.Contains(result_list));
                }
            }
            return query;
        }

        // Look for a code that is one of the words in the list.
        private IEnumerable<CHART_ITEM> AndCodeInList(IEnumerable<CHART_ITEM> query, string code_list)
        {
            if (String.IsNullOrEmpty(code_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(code_list);
            //            return query.Where(e => arr.Any(x => x.Contains(e.CODE))); // "like" match
            return query.Where(e => e.CODE.ContainsAny(arr)); // "like" match
        }

        // Look for a description that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndDescriptionInList(IEnumerable<CHART_ITEM> query, string desc_list)
        {
            if (String.IsNullOrEmpty(desc_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(desc_list);
            //return query.Where(e => arr.Any(x => x.Contains(e.DESCRIPTION))); // "like" match
            return query.Where(e => e.DESCRIPTION.ContainsAny(arr)); // "like" match
        }
        private IEnumerable<CHART_ITEM> AndDescriptionNOTInList(IEnumerable<CHART_ITEM> query, string desc_list)
        {
            if (String.IsNullOrEmpty(desc_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(desc_list);
            //return query.Where(e => arr.Any(x => x.Contains(e.DESCRIPTION))); // "like" match
            return query.Where(e => !e.DESCRIPTION.ContainsAny(arr)); // "like" match
        }

        // Look for a result that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultInList(IEnumerable<CHART_ITEM> query, string result_list)
        {
            bool use_start_with = false;

            if (String.IsNullOrEmpty(result_list)) return query;

            if (result_list.Left(2) == STARTS_WITH)
            {
                use_start_with = true;
                result_list = result_list.Substring(2);
            }

            var arr = SplitOnCommaAndPrepareElements(result_list);
            //            return query.Where(e => arr.Any(x => x.Contains(e.RESULT))); // "like" match
            if (use_start_with)
                return query.Where(e => arr.Any(item => e.RESULT.StartsWith(item)));
            else
                return query.Where(e => e.RESULT.ContainsAny(arr)); // "like" match
                                                                    //query = query.Where(e => (meds_mr2026.Any(item => e.DESCRIPTION.ToUpper().StartsWith(item))
                                                                    //return query.Where(e => arr.Any(item => e.RESULT.ToLower().Contains(item.ToLower())));
        }

        // Look for a result that does not contain one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultNotInList(IEnumerable<CHART_ITEM> query, string result_list)
        {
            if (String.IsNullOrEmpty(result_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(result_list);
            //            return query.Where(e => !arr.Any(x => x.Contains(e.RESULT))); // "like" match
            return query.Where(e => !e.RESULT.ContainsAny(arr)); // "like" match
        }

        private string DescribeSearchDepth(SearchDepth search_depth)
        {
            string result = "";
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;

            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    //result = "in pull range"
                    result = "";                         //be quiet since this is default
                    break;
                case SearchDepth.SearchSinceArrival:
                    result = "since arrival to unit";
                    break;
                case SearchDepth.SearchSinceAdmission:
                    result = "since admission";
                    break;
                case SearchDepth.SearchPullPlus:
                    result = "in LOS range plus 4 hours after";
                    break;
                case SearchDepth.SearchSince24Hrs:
                    result = "since 24 hours ago";
                    break;
                case SearchDepth.SearchSince16Hrs:
                    result = "since 16 hours ago";
                    break;
                case SearchDepth.SearchSince13Hrs:
                    result = "since 13 hours ago";
                    break;
                case SearchDepth.SearchSince9Hrs:
                    result = "since 9 hours ago";
                    break;
                case SearchDepth.SearchSince4Hrs:
                    result = "since 4 hours ago";
                    break;
            }

            return result;
        }

        // Describe a query that has already been run
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Make a simple query and describe the results
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            var arr = query.ToArray();
            return Describe(arr, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private string LookingFor(string so_far, string what, string op, string value)
        {
            string result;
            if (String.IsNullOrEmpty(value)) return so_far;

            if (so_far == "")
                result = "look for " + what;
            else
                result = so_far + " and " + what;

            if (value.Left(2) == EXACT_MATCH_PREFIX)
                result += "=" + value.Substring(2) + "";
            else if (value.Left(2) == CODE_LIKE_PREFIX)
                result += " contains " + "" + value.Substring(2) + "";
            else if (ValueIsAList(value))
                result += " in " + value + "";
            else
                result += op + "" + value + "";

            return result;
        }
        private string Describe(CHART_ITEM[] arr, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string result = "";
            //result = LookingFor(result, "cat", "=", cat);
            result = LookingFor(result, "code", "=", code_list);
            //result = LookingFor(result, "desc", " contains ", desc_list.Left(20));
            //result = LookingFor(result, "field", "=", field);
            //result = LookingFor(result, "result", " contains ", result_list.Left(20));

            if (arr.Count() == 0) {
                result += "; not found " + DescribeSearchDepth(search_depth);
            } else {
                var e = arr[0];
                result = "FOUND: " + result;
                // We might have searched for a pattern or word list in several fields - show what was found
                //if (e.CATEGORY != null && e.CATEGORY != "") result += " cat=" + e.CATEGORY + "";
                if (e.CODE != null && e.CODE != "") result += " code=" + e.CODE + "";
                if (e.DESCRIPTION != null && e.DESCRIPTION != "") result += " desc=" + e.DESCRIPTION + "";
                //if (e.FIELD_NAME != null && e.FIELD_NAME != "") result += " field=" + e.FIELD_NAME + "";
                if (e.RESULT != null && e.RESULT != "") result += " result=" + e.RESULT + "";
                // Are there more results?  Just say how many; we aren't going to list them.
                if (arr.Count() == 2) {
                    result += " (1 more result)";
                } else if (arr.Count() > 2) {
                    result += " (" + (arr.Count() - 1) + " more results)";
                }
            }

            return result;
        }


        //Set an indicator for this reason (low level)
        private void SetInd(int inum, string reason)
        {
            if (inum <= 0)
            {
                Program.VerboseAudit(reason); // dont set indicator, just output reason
                return;
            }
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Set Ind #" + inum + ": " + reason);          //already set - repeat for verbose only
            } else {
                _inds[inum].is_checked = true;
                Program.Audit("Set Ind #" + inum + ": " + reason);
            }
        }

        //Clear an indicator for this reason (low level)
        private void ClrInd(int inum, string reason)
        {
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Clr Ind #" + inum + ": " + reason);          //already clear - repeat verbose only
            } else {
                _inds[inum].is_checked = false;
                Program.Audit("Clr Ind #" + inum + ": " + reason);
            }
        }

        // Count how many items meet the conditions
        // All items are listed
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountItems(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)

        {
            bool first = true;
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            if (exclude_periph_iv)
                query = AndDescriptionNOTInList(query, "peripheral iv");
            int count = query.Count();
            found_what = "";
            if (count > 0 && trace)
            {
                foreach (var item in query)
                {
                    if (first)
                    {
                        // always return what was found
                        //            found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                        found_what = "Found desc=" + item.DESCRIPTION + ";result=" + item.RESULT + ";code=" + item.CODE + ";evdt=" + item.EVENT_DATETIME+ ";items found="+count;
                        // echo the result?
                        Program.VerboseAudit(found_what);
                        first = false;
                    }
                    
                }
            }

            return count;
        }


        // Count how many items meet the conditions
        // This has the option to list every entry or give a total
        private int CountResultInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            int count = 0;
            found_what = "";
            string s = "";

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            if (exclude_periph_iv)
                query = AndDescriptionNOTInList(query, "peripheral iv");
            var arr = SplitOnCommaAndPrepareElements(result_list);
            //Walker Schlundt
            //Set Ind #9: found '' in cat='' code='301870' field='' result='oriented x4' 

            foreach (var item in query) {
                // Figure out which of the search words were found
                for (int i = 0; i <= arr.GetUpperBound(0); i++)
                {
                    if (item.RESULT.Contains(arr[i]))
                    {
                        count++;
                        s = arr[i];
                        found_what = "Found desc:" + item.DESCRIPTION + ";result=" + item.RESULT +";code=" + item.CODE;
                        //if (trace) Program.VerboseAudit(found_what);
                        Program.VerboseAudit(found_what);
                    }
                }
                if (count_mode != CountMode.CountAll) break;        //stop counting

            }

            //if (count > 0) {
            //    //We already printed what was found; maybe add how many?
            //    if (trace && count > 0) Program.VerboseAudit("found " + count + " total");
            //} else {
            //    // Describe what was *not* found
            //    //found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
            //    //if (trace) Program.VerboseAudit(found_what);
            //}

            return count;
        }

        // Count how many items meet the conditions
        // This has the option to list every entry or give a total
        private int CountResultInListEXCEPTList(string cat, string code_list, string desc_list, string field, string result_list, string negresult_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            int count = 0;
            found_what = "";

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            query = AndResultNotInList(query, negresult_list);

            var arr = SplitOnCommaAndPrepareElements(result_list);

            foreach (var item in query)
            {
                // Figure out which of the search words were found
                var s = item.RESULT.ContainsWhich(arr);
                found_what = "found " + s + "' in cat=" + item.CATEGORY + "' code=" + item.CODE + "' field=" + item.FIELD_NAME + "' result=" + item.RESULT + "";
                if (trace) Program.VerboseAudit(found_what);
                count++;
                if (count_mode != CountMode.CountAll) break;        //stop counting
            }

            if (count > 0)
            {
                //We already printed what was found; maybe add how many?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            }
            else
            {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        // Count how many items contain ALL results in list
        private int CountResultInListAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            int count = 0;
            found_what = "";

            var arr = SplitOnCommaAndPrepareElements(result_list);

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            Program.VerboseAudit("arr ub=" + arr.GetUpperBound(0));
            for (int i = 0; i <= arr.GetUpperBound(0); i++)
            {
                if (arr[i].StartsWith(NOT_PREFIX))
                {
                    //query = query.Where(e => !e.RESULT.Contains(arr[i].Substring(2)));
                    query = AndResultNotInList(query, arr[i]);
                }
                else
                {
                    Program.VerboseAudit(i + ":" + arr[i]);
                    //query = query.Where(e => e.RESULT.Contains(arr[i]));
                    query = AndResultInList(query, arr[i]);
                }
            }
            //            Program.VerboseAudit("out of for loop");

            count = query.Count();
            //          Program.VerboseAudit("query count = " +count);

            if (count > 0)
            {
                found_what = "found item with all results in " + result_list + "' without those prefixed by !!";
                if (trace) Program.VerboseAudit(found_what);
            }
            else
            {
                // Describe what was *not* found
                //found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                //if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            if (ValueIsAList(result_list))
            {
                return CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what);
            } else {
                return CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what);
            }
        }

        // Is there an item with this result?
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContains(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            if (ValueIsAList(result_list))
            {
                return (CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            } else {
                return (CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
            }
        }

        // Result contains all in list
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            if (ValueIsAList(result_list))
            {
                return (CountResultInListAll(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            }
            else
            {
                found_what = "Not valid for non-list results.";
                return false;
            }
        }
        // Count how many items meet the conditions, with none of the results in result_list.
        // This has the option to say how many were found rather than list all.
        private int CountResultNotInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            int count = 0, rec_count = 0;
            bool found_one;

            found_what = "";
            var arr = SplitOnCommaAndPrepareElements(result_list);         // also deals with CHAR_COMMA

            //Do not filter by result here; we need all results
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            foreach (var item in query) {
                rec_count++;
                //Look for each search word in the result
                found_one = false;

                foreach (string s in arr) {
                    //                    if (String.Equals(item.RESULT, s)) {
                    if (item.RESULT.Contains(s))
                    {
                        found_what = "found " + s + ";result=" + item.RESULT + " -- exclude this";
                        Program.VerboseAudit(found_what);
                        found_one = true;
                        break;
                    }
                }

                if (!found_one) {
                    //none of the words were found - good!
                    found_what = Describe(cat, code_list, desc_list, field, "", search_depth) + " -- does not contain " + result_list + "";
                    Program.VerboseAudit(found_what);
                    count++;
                    if (count_mode != CountMode.CountAll) break;
                }
            }

            if (count > 0) {
                //We already printed what was found; maybe show the total?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else if (rec_count > 0) {
                //We already printed what we ignored
            } else {
                //Print what did not exist
                found_what = Describe(cat, code_list, desc_list, field, "", search_depth);      //not found
                Program.VerboseAudit(found_what);
            }

            return count;
        }

        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, string found_what)
        {
            // Search in the pull range with trace on
            return CountResultDoesNotContain(cat, code_list, desc_list, field, result_list, out found_what, SearchDepth.SearchDefault, true);
        }
        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, out string found_what, SearchDepth search_depth, bool trace)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            return CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0;
        }

        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            return (CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
        }

        // Set the indicator if the conditions are met
        //
        private bool SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SearchDepth s = SearchDepth.SearchDefault;
            //if (inum == 5) s = SearchDepth.SearchSince13Hrs;
            //if (inum == 6) s = SearchDepth.SearchSince13Hrs;
            //if (inum == 7) s = SearchDepth.SearchSince13Hrs;
            //if (inum == 8) s = SearchDepth.SearchSince16Hrs;
            //if (inum == 9) s = SearchDepth.SearchSince16Hrs;
            //if (inum == 10) s = SearchDepth.SearchSince13Hrs;
            //if (inum == 11) s = SearchDepth.SearchSince9Hrs;
            //if (inum == 12) s = SearchDepth.SearchSince16Hrs;
            //if (inum == 13) s = SearchDepth.SearchSince16Hrs;
            //if (inum == 14) s = SearchDepth.SearchSince24Hrs;
            //if (inum == 19) s = SearchDepth.SearchSince9Hrs;
            //if (inum == 20) s = SearchDepth.SearchSince9Hrs;
            //if (inum == 21) s = SearchDepth.SearchSince16Hrs;
            //if (inum == 22) s = SearchDepth.SearchSince13Hrs;
            //if (inum == 23) s = SearchDepth.SearchSince9Hrs;
            //if (inum == 24) s = SearchDepth.SearchSince9Hrs;
            return SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, s);

        }
        private bool SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            bool b = false;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return true;

            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what)) {
                SetInd(inum, found_what);                          //echo here - found
                b = true;
            } else {
                //   Program.VerboseAudit(found_what);                  //and here - not found
            }
            return b;
        }

        private void SetIndIfResultBetween(int inum, string cat, string code_list, string desc_list, string field, int loval, int hival)
        {
            SetIndIfResultBetween(inum, cat, code_list, desc_list, field, loval, hival, SearchDepth.SearchDefault);
        }

        private void SetIndIfResultBetween(int inum, string cat, string code_list, string desc_list, string field, int loval, int hival, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            int count = 0;
            string found_what = "";

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");
            foreach (var item in query)
            {
                Program.VerboseAudit("ResBetween: code=" + item.CODE + " result=" + item.RESULT);
                if (item.RESULT.IsNumeric())
                {
                    Program.VerboseAudit("  result is numeric");
                    if (item.RESULT.Val() >= loval && item.RESULT.Val() <= hival)
                    {
                        count++;
                        found_what = "found code=" + item.CODE + " with result=" + item.RESULT;
                        Program.VerboseAudit(found_what);
                    }
                }
            }

            if (count > 0)
            {
                SetInd(inum, found_what);
            }
        }


        // Special contains all
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SearchDepth s = SearchDepth.SearchDefault;
            //if (inum == 8) s = SearchDepth.SearchSince24Hrs;
            SetIndIfResultContainsAll(inum, cat, code_list, desc_list, field, result_list, s);
        }
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return;

            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                          //echo here - found
            }
            else
            {
                //Program.VerboseAudit(found_what);                  //and here - not found
            }
        }
        // Set the indicator if the result does not contain any of the words in result_list
        //
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultDoesNotContain(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return;

            //Turn trace off for ResultDoesNotContain() and echo what was set below with SetInd
            if (ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                           //echo here - not found
            } else {
                //Program.VerboseAudit(found_what);                   //and here - found
            }
        }

        // Clear the indicator if the result contains one of the words in the result_list
        //
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            //avoid more queries if the indicator is already clear
            if (!_inds[inum].is_checked) return;

            //Turn trace off for ResultContains() and echo what was set below with SetInd
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what)) {
                ClrInd(inum, found_what);                           //echo here - found
            } else {
                Program.VerboseAudit(found_what);                   //and here - not found
            }
        }


        //These slightly smaller functions are meant for places where you aren't really looking for a chart result
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            return (CountResultContains(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
        }

        // Variations with optional parameters (can switch to optional parameters in VS 2010)
        //
        private void SetIndIfFound(int inum, string cat)
        {
            SetIndIfResultContains(inum, cat, "", "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list)
        {
            SetIndIfResultContains(inum, cat, code_list, "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private void ClrIndIfFound(int inum, string cat)
        {
            ClrIndIfResultContains(inum, cat, "", "", "", "", SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Get the max/total value from a result (usually in the middle of the text)
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetIntValue(get_mode, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            int value;
            int result = 0;
            bool found_one = false;

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);

            //Look for a number in the result

            foreach (var item in query) {
                // RESULT might be null so convert to empty string so Split won't blow up
                var arr = PFSDBUtility.DBToString(item.RESULT).Split(' ');
                foreach (string s in arr) {
                    //Look for 1st character numeric; IsNumeric("60min") will fail so look at 1st char only
                    if (s.Left(1).IsNumeric()) {
                        value = (int)s.Val();                           //Use Val; ToInteger will error on "60min"
                        switch (get_mode) {
                            case GetValueMode.GetMax:
                                result = Math.Max(value, result);       //max
                                break;
                            case GetValueMode.GetTotal:
                                result += value;                        //total
                                break;
                            case GetValueMode.GetLast:
                                result = value;                         //last
                                break;
                        }

                        // print what we are searching for (the first time)
                        if (!found_one) {
                            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
                        }
                        found_one = true;
                        // print each value found
                        Program.VerboseAudit("  found numeric value " + result);
                        //Keep going in case there are more
                    }
                }
            }

            if (!found_one) {
                //show what was not found
                Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
            }

            return result;
        }

        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetMaxValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            return GetIntValue(GetValueMode.GetMax, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetTotalValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            return GetIntValue(GetValueMode.GetTotal, cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Get a result; returns true if found with return_result
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result)
        {
            return GetResult(cat, code_list, desc_list, field, out return_result, SearchDepth.SearchDefault);
        }
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0) {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
            } else {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }

        private bool GetLatestResult(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");
            query = query.OrderByDescending(e => e.EVENT_DATETIME);

            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT) + " charted at " + query.First().EVENT_DATETIME;
            }
            else
            {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }

        private bool GetResultForInspection(string cat, string code_list, string desc_list, string field, string result, out string return_result, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result);

            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
            }
            else
            {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }

        private void GetResultforOther(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "Other");
            return_result = "";
            foreach (var item in query)
            {
                if (!String.Equals(item.RESULT, "Other"))
                {
                    return_result = item.RESULT;
                    //Program.VerboseAudit(found_what);
                    break;
                }

            }
            return;
        }

        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, string res, out string return_result, out DateTime return_evdt)
        {
            return GetResultAndEVDT(cat, code_list, desc_list, field, res, out return_result, out return_evdt, SearchDepth.SearchDefault);
        }
        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, string res, out string return_result, out DateTime return_evdt, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, res);

            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
                return_evdt = PFSDBUtility.DBToDateTime(query.First().EVENT_DATETIME);
            }
            else
            {
                return_result = "";
                return_evdt = DateTime.MinValue;
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }
        private bool GetEVDT(string cat, string code_list, string desc_list, string res, string field, int comparison, DateTime compevdt, out DateTime return_evdt)
        {
            return GetEVDT(cat, code_list, desc_list, field, res, comparison, compevdt, out return_evdt, SearchDepth.SearchDefault);
        }
        private bool GetEVDT(string cat, string code_list, string desc_list, string field, string res, int comparison, DateTime compevdt, out DateTime return_evdt, SearchDepth search_depth)
        {
            bool sort_ascending = false;
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, res);
            if (compevdt != DateTime.MinValue)
            {
                if (comparison == 0) // equal
                    query = query.Where(e => e.EVENT_DATETIME == compevdt);
                else if (comparison == 1) // GTE
                    query = query.Where(e => e.EVENT_DATETIME >= compevdt);
                else if (comparison == 2) // GT
                    query = query.Where(e => e.EVENT_DATETIME > compevdt);
                else if (comparison == 22) // Least GT
                {
                    query = query.Where(e => e.EVENT_DATETIME > compevdt);
                    sort_ascending = true;
                }
                else if (comparison == 3) // LT
                    query = query.Where(e => e.EVENT_DATETIME < compevdt);
            }
            if (sort_ascending)
                query = query.OrderBy(e => e.EVENT_DATETIME);
            else
                query = query.OrderByDescending(e => e.EVENT_DATETIME);
            Program.VerboseAudit("getevdt count=" + query.Count());
            if (query.Count() > 0)
            {
                return_evdt = PFSDBUtility.DBToDateTime(query.First().EVENT_DATETIME);
            }
            else
            {
                return_evdt = DateTime.MinValue;
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, res, search_depth));
            return (return_evdt != DateTime.MinValue);
        }

        private int SetIndIfCodeBtwn(int ind, int locode, int hicode, SearchDepth search_depth)

        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = query.Where(e => e.CODE.ToLower().StartsWith("edu"));
            query = query.Where(e => e.CODE.Length >= 12);
            query = query.Where(e => e.CODE.Substring(3, 9).Val() >= locode);
            query = query.Where(e => e.CODE.Substring(3, 9).Val() <= hicode);
            int count = query.Count();
            // always return what was found
            string found_what = "There were " + count + " items found with code between " + locode + " and " + hicode;
            if (count >= 8) SetInd(ind, found_what);
            // echo the result?
            //               if (trace) Program.VerboseAudit(found_what);

            return count;
        }

        private DateTime LatestEVDT(string code_list, string desc_list, string res, SearchDepth search_depth)
        {
            DateTime latestdt = DateTime.MinValue;

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, "", code_list, desc_list, "", res);
            query = query.OrderByDescending(e => e.EVENT_DATETIME);
            int ct = query.Count();

            if (ct > 0)
            {
                latestdt = PFSDBUtility.DBToDateTime(query.First().EVENT_DATETIME);
            }
            else
            {
                latestdt = DateTime.MinValue;
            }
            return latestdt;
        }

        //======================================================================================
        // Mapping starts here
        //======================================================================================

        private void Check_1_2_3_4()
        {
            Program.VerboseAudit("Default ADL Search Scope = " + _pat.pull_finish.AddHours(-24) + " to " + _pat.pull_finish);
            

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("1. ADL Self");
            Program.VerboseAudit("2. ADL Assist");
            Program.VerboseAudit("3. ADL Extended");
            Program.VerboseAudit("4. ADL Complete");
            Program.VerboseAudit("---------------");

 //These two items should only be searched for in the current location.  Mar 3 2022.
 //Test with Charlene patient for sedation level found in icu -- feb 21,22 LIDDLE 3428486562
            string reslist = "Coma,Pharmaceutically paralyzed,Sedated,Unresponsive";
            coma = SetIndIfResultContains(4, "", "304239339", "", "", reslist, SearchDepth.SearchDefault);
            reslist = "-2,-3,-4,-5";
            SetIndIfResultContains(4, "", "304239326", "", "", reslist, SearchDepth.SearchDefault);
            // 4. COMPLETE CARE

            //            "ADL #1-4- Look back:
            //            - 13 Hr Mobility
            //-25 hr Hygiene
            //-13 Hr Toileting
            //-25 Hr Feeding
            //-13 Hrs - All Other rows"
            exclude_periop_data = false;

            if (_pat.age < 4.0)
                SetInd(4, "Age=" + _pat.age);
            //else if (_pat.age < 7.0)
            //    SetInd(2, "Age=" + _pat.age);
            //if (_inds[4].is_checked) return;

            reslist = "";
            //NPO Orders
            //bool npo = Exists("", EXACT_MATCH_PREFIX + "40517", "", "", reslist);
            //npo |= Exists("", EXACT_MATCH_PREFIX + "226916", "", "", reslist);
            bool npo = false;

            //if (OrderInProgress("226916", out desc_found))
            //{
            //    npo = true;
            //    Program.VerboseAudit("NPO in progress: ADL Feeding component set to dependent/complete.");
            //}


            //Dressing only for Spaulding
            //
            //reslist = "Independent,Modified independent,With set up";
            //SetIndIfResultContains(1, "", "30423800", "", "", reslist);
            //reslist = "Independent,Modified independent,With set up";
            //SetIndIfResultContains(1, "", "304239252", "", "", reslist);
            //reslist = "Independent";
            //SetIndIfResultContains(1, "", "304239253", "", "", reslist);
            //reslist = "Independent,Modified Independent";
            //SetIndIfResultContains(1, "", "304239254", "", "", reslist);
            //reslist = "Independent,Modified Independent";
            //SetIndIfResultContains(1, "", "304239255", "", "", reslist);
            //reslist = "0=Independent";
            //SetIndIfResultContains(1, "", "304239256", "", "", reslist);
            //reslist = "0=No setup or physical help";
            //SetIndIfResultContains(1, "", "304239257", "", "", reslist);
            //reslist = "06=Independent,05=Setup or clean-up assistance";
            //SetIndIfResultContains(1, "", "304239258", "", "", reslist);
            //reslist = "06=Independent,05=Setup or clean-up assistance";
            //SetIndIfResultContains(1, "", "304239259", "", "", reslist);

            Program.VerboseAudit("Feeding: Independent charting search...");
            reslist = "Independent";
            bool feed1 = Exists("", "304239260", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Able to feed self,Able to feed self with adaptive devices,Needs set up";
            feed1 |= Exists("", "304239261", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Independent,Modified Independent,Set-up/Supervision";
            feed1 |= Exists("", "304239262", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "06,05,04";
            feed1 |= Exists("", "304239263", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "0,1";
            feed1 |= Exists("", "304239264", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "0,1";
            feed1 |= Exists("", "304239265", "", "", reslist, SearchDepth.SearchSince24Hrs);
            //reslist = "";
            //feed1 |= Exists("", "304239266", "", "", reslist, SearchDepth.SearchSince24Hrs);
            //reslist = "";
            //feed1 |= Exists("", "304239267", "", "", reslist, SearchDepth.SearchSince24Hrs);

            DateTime feed1_latest, evdt1;
            feed1_latest = LatestEVDT("304239260", "", "Independent", SearchDepth.SearchSince24Hrs);
            evdt1 = LatestEVDT("304239261", "", "Able to feed self,Able to feed self with adaptive devices,Needs set up", SearchDepth.SearchSince24Hrs);
            feed1_latest = (evdt1 > feed1_latest ? evdt1 : feed1_latest);
            evdt1 = LatestEVDT("304239262", "", "Independent,Modified Independent,Set-up/Supervision", SearchDepth.SearchSince24Hrs);
            feed1_latest = (evdt1 > feed1_latest ? evdt1 : feed1_latest);
            evdt1 = LatestEVDT("304239263", "", "06,05,04", SearchDepth.SearchSince24Hrs);
            feed1_latest = (evdt1 > feed1_latest ? evdt1 : feed1_latest);
            evdt1 = LatestEVDT("304239264,304239265", "", "0,1", SearchDepth.SearchSince24Hrs);
            feed1_latest = (evdt1 > feed1_latest ? evdt1 : feed1_latest);
            //evdt1 = LatestEVDT("304239266,304239267", "", "", SearchDepth.SearchSince24Hrs);
            //feed1_latest = (evdt1 > feed1_latest ? evdt1 : feed1_latest);

            if (feed1)
                Program.VerboseAudit("Feeding: Independent charting found.");

            Program.VerboseAudit("Feeding: Partial charting search...");
            reslist = "Needs assistance";
            bool feed2 = Exists("", "304239260", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "One to one supervision,Close supervision,Distance supervision,Needs assist";
            feed2 |= Exists("", "304239261", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "75,50";
            feed2 |= Exists("", "304239262", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "03";
            feed2 |= Exists("", "304239263", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "2,3";
            feed2 |= Exists("", "304239264", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "2";
            feed2 |= Exists("", "304239265", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Bolus,Continuous,Intermittent";
            feed2 |= Exists("", "304239266", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Yes";
            feed2 |= Exists("", "304239267", "", "", reslist, SearchDepth.SearchSince24Hrs);

            DateTime feed2_latest;
            feed2_latest = LatestEVDT("304239260", "", "Needs assistance", SearchDepth.SearchSince24Hrs);
            evdt1 = LatestEVDT("304239261", "", "One to one supervision,Close supervision,Distance supervision,Needs assist", SearchDepth.SearchSince24Hrs);
            feed2_latest = (evdt1 > feed2_latest ? evdt1 : feed2_latest);

     DateTime feed2_75_dt = LatestEVDT("304239262", "", "75", SearchDepth.SearchSince24Hrs);
     bool feed2_75_is_latest = false;

            evdt1 = LatestEVDT("304239262", "", "50", SearchDepth.SearchSince24Hrs);
            feed2_latest = (evdt1 > feed2_latest ? evdt1 : feed2_latest);
            evdt1 = LatestEVDT("304239263", "", "03", SearchDepth.SearchSince24Hrs);
            feed2_latest = (evdt1 > feed2_latest ? evdt1 : feed2_latest);
            evdt1 = LatestEVDT("304239264,304239265", "", "2", SearchDepth.SearchSince24Hrs);
            feed2_latest = (evdt1 > feed2_latest ? evdt1 : feed2_latest);
            evdt1 = LatestEVDT("304239265", "", "3", SearchDepth.SearchSince24Hrs);
            feed2_latest = (evdt1 > feed2_latest ? evdt1 : feed2_latest);
            evdt1 = LatestEVDT("304239266", "", "Bolus,Continuous,Intermittent", SearchDepth.SearchSince24Hrs);
            feed2_latest = (evdt1 > feed2_latest ? evdt1 : feed2_latest);
            evdt1 = LatestEVDT("304239267", "", "Yes", SearchDepth.SearchSince24Hrs);
            feed2_latest = (evdt1 > feed2_latest ? evdt1 : feed2_latest);

            if (feed2_75_dt > feed2_latest)
            {
                feed2_latest = feed2_75_dt;
                feed2_75_is_latest = true;
            }

            if (feed2)
                Program.VerboseAudit("Feeding: Partial charting found.");

            Program.VerboseAudit("Feeding: Complete charting search...");
            bool feed4 = npo;
            reslist = EXACT_MATCH_PREFIX + "Dependent";
            feed4 |= Exists("", "304239260", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Total assist";
            feed4 |= Exists("", "304239261", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Maximum Assist/Perf 25-49,Total Assist/Perf <";
            feed4 |= Exists("", "304239262", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "02,01";
            feed4 |= Exists("", "304239263", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "4";
            feed4 |= Exists("", "304239264", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Bolus,Continuous,Intermittent";
            feed4 |= Exists("", "304239266", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Yes";
            feed4 |= Exists("", "304239267", "", "", reslist, SearchDepth.SearchSince24Hrs);

            DateTime feed4_latest;
            feed4_latest = LatestEVDT("304239260", "", EXACT_MATCH_PREFIX + "Dependent", SearchDepth.SearchSince24Hrs);
            evdt1 = LatestEVDT("304239261", "", "Total assist", SearchDepth.SearchSince24Hrs);
            feed4_latest = (evdt1 > feed4_latest ? evdt1 : feed4_latest);
            evdt1 = LatestEVDT("304239262", "", "Maximum Assist/Perf 25-49,Total Assist/Perf <", SearchDepth.SearchSince24Hrs);
            feed4_latest = (evdt1 > feed4_latest ? evdt1 : feed4_latest);
            evdt1 = LatestEVDT("304239263", "", "02,01", SearchDepth.SearchSince24Hrs);
            feed4_latest = (evdt1 > feed4_latest ? evdt1 : feed4_latest);
            evdt1 = LatestEVDT("304239264", "", "4", SearchDepth.SearchSince24Hrs);
            feed4_latest = (evdt1 > feed4_latest ? evdt1 : feed4_latest);
            evdt1 = LatestEVDT("304239265", "", "3", SearchDepth.SearchSince24Hrs);
            feed4_latest = (evdt1 > feed4_latest ? evdt1 : feed4_latest);
            evdt1 = LatestEVDT("304239266", "", "Bolus,Continuous,Intermittent", SearchDepth.SearchSince24Hrs);
            feed4_latest = (evdt1 > feed4_latest ? evdt1 : feed4_latest);
            evdt1 = LatestEVDT("304239267", "", "Yes", SearchDepth.SearchSince24Hrs);
            feed4_latest = (evdt1 > feed4_latest ? evdt1 : feed4_latest);

            if (feed4)
                Program.VerboseAudit("Feeding: Complete charting found.");

            if (feed4_latest > DateTime.MinValue && feed4_latest >= feed2_latest && feed4_latest >= feed1_latest)
            {
                Program.VerboseAudit("LATEST FEEDING: Complete level is latest charting.");
                feed1 = false;
                feed2 = false;
                feed4 = true;
            }
            else if (feed2_latest > DateTime.MinValue && feed2_latest >= feed1_latest && feed2_latest >= feed4_latest)
            {
                    Program.VerboseAudit("LATEST FEEDING: Partial level is latest charting.");
                    feed1 = false;
                    feed2 = true;
                    feed4 = false;
            }
            else if (feed1_latest > DateTime.MinValue && feed1_latest >= feed2_latest && feed1_latest >= feed4_latest)
            {
                Program.VerboseAudit("LATEST FEEDING: Self level is latest charting.");
                feed1 = true;
                feed2 = false;
                feed4 = false;
            }

            Program.VerboseAudit("HYGIENE: Independent charting search...");
            reslist = "Independent,Modified Independent";
            bool hyg1 = Exists("", "304239268", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Independent";
            hyg1 |= Exists("", "304239269", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Independent";
            hyg1 |= Exists("", "304239270", "", "", reslist, SearchDepth.SearchSince24Hrs);
            //reslist = "Independent,Modified Independent,Set-up";
            reslist = "Independent,Setup,Supervision";
            hyg1 |= Exists("", "304239271", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Independent,Modified Independent,Set-up";
            hyg1 |= Exists("", "304239272", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "06,05,04";
            hyg1 |= Exists("", "304239273", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "06,05,04";
            hyg1 |= Exists("", "304239274", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "0,1";
            hyg1 |= Exists("", "304239275", "", "", reslist, SearchDepth.SearchSince24Hrs);
            hyg1 |= Exists("", "304239276", "", "", reslist, SearchDepth.SearchSince24Hrs);
            hyg1 |= Exists("", "304239277", "", "", reslist, SearchDepth.SearchSince24Hrs);
            hyg1 |= Exists("", "304239278", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "3=Independent";
            hyg1 |= Exists("", "304239279", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "06,05,04";
            hyg1 |= Exists("", "304239280", "", "", reslist, SearchDepth.SearchSince24Hrs);
            //reslist = "Independent";
            //hyg1 |= Exists("", "304239254,304239255", "", "", reslist);

            DateTime hyg1_latest;
            hyg1_latest = LatestEVDT("304239268,304239269,304239270", "", "Independent", SearchDepth.SearchSince24Hrs);
            evdt1 = LatestEVDT("304239271", "", "Independent,Setup,Supervision", SearchDepth.SearchSince24Hrs);
            hyg1_latest = (evdt1 > hyg1_latest ? evdt1 : hyg1_latest);
            evdt1 = LatestEVDT("304239272", "", "Independent,Set-up", SearchDepth.SearchSince24Hrs);
            hyg1_latest = (evdt1 > hyg1_latest ? evdt1 : hyg1_latest);
            evdt1 = LatestEVDT("304239273,304239274,304239280", "", "06,05,04", SearchDepth.SearchSince24Hrs);
            hyg1_latest = (evdt1 > hyg1_latest ? evdt1 : hyg1_latest);
            evdt1 = LatestEVDT("304239275,304239276,304239277,304239278", "", "0,1", SearchDepth.SearchSince24Hrs);
            hyg1_latest = (evdt1 > hyg1_latest ? evdt1 : hyg1_latest);
            evdt1 = LatestEVDT("304239279", "", "3=Independent", SearchDepth.SearchSince24Hrs);
            hyg1_latest = (evdt1 > hyg1_latest ? evdt1 : hyg1_latest);

            if (hyg1)
                Program.VerboseAudit("HYGIENE: Independent charting found.");

            Program.VerboseAudit("HYGIENE: Partial charting search...");
            reslist = "Setup/Supervision,Minimum Assist/Patient Performed 75,Moderate Assist/Patient Performed 50-74";
            bool hyg2 = Exists("", "304239268", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Needs assistance";
            hyg2 |= Exists("", "304239269", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Needs assistance";
            hyg2 |= Exists("", "304239270", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Partial/moderate assistance"; //,75,50";
            hyg2 |= Exists("", "304239271", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "75,50";
            hyg2 |= Exists("", "304239272", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "03";
            hyg2 |= Exists("", "304239273", "", "", reslist, SearchDepth.SearchSince24Hrs);
            hyg2 |= Exists("", "304239274", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "2,3";
            hyg2 |= Exists("", "304239275", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "2";
            hyg2 |= Exists("", "304239276", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "2,3";
            hyg2 |= Exists("", "304239277", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "2";
            hyg2 |= Exists("", "304239278", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "2=Needed some help";
            hyg2 |= Exists("", "304239279", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "03";
            hyg2 |= Exists("", "304239280", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Maximum Assist/Patient Performed 25,Total Assistance/Patient Performed < 25";
            hyg2 |= Exists("", "304239254,304239255", "", "", reslist, SearchDepth.SearchSince24Hrs);
            //reslist = "Set-up/Supervision,Minimum Assist/Patient Performed 75,Moderate Assist/Patient Performed 50";
            //hyg2 |= Exists("", "304239254,304239255", "", "", reslist);
            reslist = "1 staff";
            hyg2 |= Exists("", "304239344", "", "", reslist, SearchDepth.SearchSince24Hrs);

            DateTime hyg2_latest;
            hyg2_latest = LatestEVDT("304239268", "", "Moderate Assist/Patient Performed 50-74", SearchDepth.SearchSince24Hrs);
  DateTime hyg2_75_dt = LatestEVDT("304239268", "", "Minimum Assist/Patient Performed 75", SearchDepth.SearchSince24Hrs);
  DateTime hyg2ss_dt = LatestEVDT("304239268", "", "Setup/Supervision", SearchDepth.SearchSince24Hrs);
  bool hyg2ss_is_latest = false;
            evdt1 = LatestEVDT("304239269,304239270", "", "Needs assistance", SearchDepth.SearchSince24Hrs);
            hyg2_latest = (evdt1 > hyg2_latest ? evdt1 : hyg2_latest);

            hyg2_latest = (evdt1 > hyg2_latest ? evdt1 : hyg2_latest);
            evdt1 = LatestEVDT("304239271", "", "Partial/moderate assistance", SearchDepth.SearchSince24Hrs);
            hyg2_latest = (evdt1 > hyg2_latest ? evdt1 : hyg2_latest);
DateTime hyg2_75b_dt = LatestEVDT("304239272", "", "75", SearchDepth.SearchSince24Hrs);
            evdt1 = LatestEVDT("304239272", "", "50", SearchDepth.SearchSince24Hrs);
            hyg2_latest = (evdt1 > hyg2_latest ? evdt1 : hyg2_latest);
            evdt1 = LatestEVDT("304239273,304239274,304239280", "", "03", SearchDepth.SearchSince24Hrs);
            hyg2_latest = (evdt1 > hyg2_latest ? evdt1 : hyg2_latest);
            evdt1 = LatestEVDT("304239275,304239276,304239277,304239278", "", "2", SearchDepth.SearchSince24Hrs);
            hyg2_latest = (evdt1 > hyg2_latest ? evdt1 : hyg2_latest);
            evdt1 = LatestEVDT("304239275,304239277", "", "3", SearchDepth.SearchSince24Hrs);
            hyg2_latest = (evdt1 > hyg2_latest ? evdt1 : hyg2_latest);
            evdt1 = LatestEVDT("304239279", "", "2=Needed some help", SearchDepth.SearchSince24Hrs);
            hyg2_latest = (evdt1 > hyg2_latest ? evdt1 : hyg2_latest);
            evdt1 = LatestEVDT("304239254,304239255", "", "Maximum Assist/Patient Performed 25,Total Assistance/Patient Performed < 25", SearchDepth.SearchSince24Hrs);
            hyg2_latest = (evdt1 > hyg2_latest ? evdt1 : hyg2_latest);
            evdt1 = LatestEVDT("304239344", "", "1 staff", SearchDepth.SearchSince24Hrs);
            hyg2_latest = (evdt1 > hyg2_latest ? evdt1 : hyg2_latest);

            if (hyg2ss_dt > hyg2_latest || hyg2_75_dt > hyg2_latest || hyg2_75b_dt > hyg2_latest)
            {
                if (hyg2ss_dt > hyg2_latest) hyg2_latest = hyg2ss_dt;
                if (hyg2_75_dt > hyg2_latest) hyg2_latest = hyg2_75_dt;
                if (hyg2_75b_dt > hyg2_latest) hyg2_latest = hyg2_75b_dt;
                hyg2ss_is_latest = true;
            }

            if (hyg2)
                Program.VerboseAudit("HYGIENE: Partial charting found.");

            Program.VerboseAudit("HYGIENE: Complete charting search...");
            reslist = "25";
            bool hyg4 = Exists("", "304239268", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = EXACT_MATCH_PREFIX + "Dependent";
            hyg4 |= Exists("", "304239269", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = EXACT_MATCH_PREFIX + "Dependent";
            hyg4 |= Exists("", "304239270", "", "", reslist, SearchDepth.SearchSince24Hrs);
            //reslist = "25";
            reslist = "Substantial/maximal assistance";
            hyg4 |= Exists("", "304239271", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = EXACT_MATCH_PREFIX + "Dependent";
            hyg4 |= Exists("", "304239271", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "25";
            hyg4 |= Exists("", "304239272", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "02,01";
            hyg4 |= Exists("", "304239273", "", "", reslist, SearchDepth.SearchSince24Hrs);
            hyg4 |= Exists("", "304239274", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "4";
            hyg4 |= Exists("", "304239275", "", "", reslist, SearchDepth.SearchSince24Hrs);
            hyg4 |= Exists("", "304239277", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "1=Dependent";
            hyg4 |= Exists("", "304239279", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "02,01";
            hyg4 |= Exists("", "304239280", "", "", reslist, SearchDepth.SearchSince24Hrs);

            DateTime hyg4_latest;
            hyg4_latest = LatestEVDT("304239268,304239272", "", "25", SearchDepth.SearchSince24Hrs);
            evdt1 = LatestEVDT("304239269", "", EXACT_MATCH_PREFIX + "Dependent", SearchDepth.SearchSince24Hrs);
            hyg4_latest = (evdt1 > hyg4_latest ? evdt1 : hyg4_latest);
            evdt1 = LatestEVDT("304239270", "", EXACT_MATCH_PREFIX + "Dependent", SearchDepth.SearchSince24Hrs);
            hyg4_latest = (evdt1 > hyg4_latest ? evdt1 : hyg4_latest);
            evdt1 = LatestEVDT("304239271", "", "Substantial/maximal assistance", SearchDepth.SearchSince24Hrs);
            hyg4_latest = (evdt1 > hyg4_latest ? evdt1 : hyg4_latest);
            evdt1 = LatestEVDT("304239273,304239274,304239280", "", "02,01", SearchDepth.SearchSince24Hrs);
            hyg4_latest = (evdt1 > hyg4_latest ? evdt1 : hyg4_latest);
            evdt1 = LatestEVDT("304239275,304239277", "", "4", SearchDepth.SearchSince24Hrs);
            hyg4_latest = (evdt1 > hyg4_latest ? evdt1 : hyg4_latest);
            evdt1 = LatestEVDT("304239279", "", "1=Dependent", SearchDepth.SearchSince24Hrs);
            hyg4_latest = (evdt1 > hyg4_latest ? evdt1 : hyg4_latest);

            if (hyg4)
                Program.VerboseAudit("HYGIENE: Complete charting found.");

            if (hyg4_latest > DateTime.MinValue && hyg4_latest >= hyg2_latest && hyg4_latest >= hyg1_latest)
            {
                Program.VerboseAudit("LATEST HYGIENE: Complete level is latest charting.");
                hyg1 = false;
                hyg2 = false;
                hyg4 = true;
            }
            else if (hyg2_latest > DateTime.MinValue && hyg2_latest >= hyg1_latest && hyg2_latest >= hyg4_latest)
            {
                    Program.VerboseAudit("LATEST HYGIENE: Partial level is latest charting.");
                    hyg1 = false;
                    hyg2 = true;
                    hyg4 = false;
            }
            else if (hyg1_latest > DateTime.MinValue && hyg1_latest >= hyg4_latest && hyg1_latest >= hyg2_latest)
            {
                Program.VerboseAudit("LATEST HYGIENE: Self level is latest charting.");
                hyg1 = true;
                hyg2 = false;
                hyg4 = false;
            }


            Program.VerboseAudit("MOBILITY: Independent charting search...");
            reslist = "Patient independent";
            bool mob1 = Exists("", "304239281", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Independent,Modified Independent";
            mob1 |= Exists("", "304239282", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Independent";
            mob1 |= Exists("", "304239283", "", "", reslist, SearchDepth.SearchSince24Hrs);
            //reslist = "Independent,Modified Independent,Supervision";
            reslist = "Independent,Setup,Supervision";
            mob1 |= Exists("", "304239284", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "06,05,04";
            mob1 |= Exists("", "304239285", "", "", reslist, SearchDepth.SearchSince24Hrs);
            mob1 |= Exists("", "304239286", "", "", reslist, SearchDepth.SearchSince24Hrs);
            mob1 |= Exists("", "304239287", "", "", reslist, SearchDepth.SearchSince24Hrs);
            mob1 |= Exists("", "304239288", "", "", reslist, SearchDepth.SearchSince24Hrs);
            mob1 |= Exists("", "304239289", "", "", reslist, SearchDepth.SearchSince24Hrs);
            mob1 |= Exists("", "304239290", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Independent,Modified independent,With set up";
            mob1 |= Exists("", "304239291", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Independent,Modified independent,With set up";
            mob1 |= Exists("", "304239292", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "0,1";
            mob1 |= Exists("", "304239293", "", "", reslist, SearchDepth.SearchSince24Hrs);
            mob1 |= Exists("", "304239294", "", "", reslist, SearchDepth.SearchSince24Hrs);
            mob1 |= Exists("", "304239295", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "0=No setup or physical help,1=Setup help only";
            mob1 |= Exists("", "304239296", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "0,1";
            mob1 |= Exists("", "304239297", "", "", reslist, SearchDepth.SearchSince24Hrs);
            mob1 |= Exists("", "304239298", "", "", reslist, SearchDepth.SearchSince24Hrs);
            mob1 |= Exists("", "304239299", "", "", reslist, SearchDepth.SearchSince24Hrs);
            mob1 |= Exists("", "304239300", "", "", reslist, SearchDepth.SearchSince24Hrs);
            mob1 |= Exists("", "304239301", "", "", reslist, SearchDepth.SearchSince24Hrs);
            mob1 |= Exists("", "304239302", "", "", reslist, SearchDepth.SearchSince24Hrs);
            mob1 |= Exists("", "304239303", "", "", reslist, SearchDepth.SearchSince24Hrs);
            mob1 |= Exists("", "304239304", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Independent,Modified independent,With set up";
            mob1 |= Exists("", "304239305", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Independent,Modified independent,With set up,Supervision,Contact guard";
            mob1 |= Exists("", "304239306", "", "", reslist, SearchDepth.SearchSince24Hrs);

            DateTime mob1_latest;
            mob1_latest = LatestEVDT("304239281", "", "Patient independent", SearchDepth.SearchSince24Hrs);
            evdt1 = LatestEVDT("304239282,304239283", "", "Independent", SearchDepth.SearchSince24Hrs);
            mob1_latest = (evdt1 > mob1_latest ? evdt1 : mob1_latest);
            evdt1 = LatestEVDT("304239284", "", "Independent,Setup,Supervision", SearchDepth.SearchSince24Hrs);
            mob1_latest = (evdt1 > mob1_latest ? evdt1 : mob1_latest);
            evdt1 = LatestEVDT("304239285,304239286,304239287,304239288,304239289,304239290", "", "06,05,04", SearchDepth.SearchSince24Hrs);
            mob1_latest = (evdt1 > mob1_latest ? evdt1 : mob1_latest);
            evdt1 = LatestEVDT("304239291,304239292", "", "Independent,With set up", SearchDepth.SearchSince24Hrs);
            mob1_latest = (evdt1 > mob1_latest ? evdt1 : mob1_latest);
            evdt1 = LatestEVDT("304239296", "", "0=No setup or physical help,1=Setup help only", SearchDepth.SearchSince24Hrs);
            mob1_latest = (evdt1 > mob1_latest ? evdt1 : mob1_latest);
            evdt1 = LatestEVDT("304239293,304239294,304239295,304239297,304239298,304239299,304239300,304239301,304239302,304239303,304239304", "", "0,1", SearchDepth.SearchSince24Hrs);
            mob1_latest = (evdt1 > mob1_latest ? evdt1 : mob1_latest);
            evdt1 = LatestEVDT("304239305,304239306", "", "Independent,With set up", SearchDepth.SearchSince24Hrs);
            mob1_latest = (evdt1 > mob1_latest ? evdt1 : mob1_latest);

            if (mob1)
                Program.VerboseAudit("MOBILITY: Independent charting found.");

            Program.VerboseAudit("MOBILITY: Partial charting search...");
            reslist = "Stand by assist,One person assist,Two person assist";
            bool mob2 = Exists("", "304239281", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Setup/Supervision,Minimum Assist/Patient Performed 75,Moderate Assist/Patient Performed 50";
            mob2 |= Exists("", "304239282", "", "", reslist, SearchDepth.SearchSince24Hrs);

            reslist = "Needs assistance";
            mob2 |= Exists("", "304239283", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Partial/moderate assistance"; // 75,50";
            mob2 |= Exists("", "304239284", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "03";
            mob2 |= Exists("", "304239285", "", "", reslist, SearchDepth.SearchSince24Hrs);
            mob2 |= Exists("", "304239286", "", "", reslist, SearchDepth.SearchSince24Hrs);
            mob2 |= Exists("", "304239287", "", "", reslist, SearchDepth.SearchSince24Hrs);
            mob2 |= Exists("", "304239288", "", "", reslist, SearchDepth.SearchSince24Hrs);
            mob2 |= Exists("", "304239289", "", "", reslist, SearchDepth.SearchSince24Hrs);
            mob2 |= Exists("", "304239290", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Supervision,Close supervision,Contact guard,Minimum assist,Moderate assist";
            mob2 |= Exists("", "304239291", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Supervision,Close supervision,Contact guard,Minimum assist,Moderate assist";
            mob2 |= Exists("", "304239292", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "2,3";
            mob2 |= Exists("", "304239293", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "2";
            mob2 |= Exists("", "304239294", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "2,3";
            mob2 |= Exists("", "304239295", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "2";
            mob2 |= Exists("", "304239296", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "2,3";
            mob2 |= Exists("", "304239297", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "2";
            mob2 |= Exists("", "304239298", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "2,3";
            mob2 |= Exists("", "304239299", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "2";
            mob2 |= Exists("", "304239300", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "2,3";
            mob2 |= Exists("", "304239301", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "2";
            mob2 |= Exists("", "304239302", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "2,3";
            mob2 |= Exists("", "304239303", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "2";
            mob2 |= Exists("", "304239304", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Supervision,Close supervision,Contact guard,Minimum assistance,Moderate assistance";
            mob2 |= Exists("", "304239305", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Minimum assistance,Moderate assistance";
            mob2 |= Exists("", "304239306", "", "", reslist, SearchDepth.SearchSince24Hrs);
            mob2 |= Exists("", "304239349,304239061,3042370041", "", "", "1", SearchDepth.SearchSince24Hrs);

            DateTime mob2ss_dt = LatestEVDT("304239282", "", "Setup/Supervision", SearchDepth.SearchSince24Hrs);
            bool mob2ss_is_latest = false;
            DateTime mob2_latest;
            mob2_latest = LatestEVDT("304239281", "", "Stand by assist,One person assist,Two person assist", SearchDepth.SearchSince24Hrs);
DateTime mob2_75_dt = LatestEVDT("304239282", "", "Minimum Assist/Patient Performed 75", SearchDepth.SearchSince24Hrs);
            evdt1 = LatestEVDT("304239282", "", "Moderate Assist/Patient Performed 50", SearchDepth.SearchSince24Hrs);
            mob2_latest = (evdt1 > mob2_latest ? evdt1 : mob2_latest);
            evdt1 = LatestEVDT("304239283", "", "Needs assistance", SearchDepth.SearchSince24Hrs);
            mob2_latest = (evdt1 > mob2_latest ? evdt1 : mob2_latest);
            evdt1 = LatestEVDT("304239284", "", "Partial/moderate assistance", SearchDepth.SearchSince24Hrs);
            mob2_latest = (evdt1 > mob2_latest ? evdt1 : mob2_latest);
            evdt1 = LatestEVDT("304239285,304239286,304239287,304239288,304239289,304239290", "", "03", SearchDepth.SearchSince24Hrs);
            mob2_latest = (evdt1 > mob2_latest ? evdt1 : mob2_latest);
            evdt1 = LatestEVDT("304239291,304239292", "", "Supervision,Close supervision,Contact guard,Minimum assist,Moderate assist", SearchDepth.SearchSince24Hrs);
            mob2_latest = (evdt1 > mob2_latest ? evdt1 : mob2_latest);
            evdt1 = LatestEVDT("304239293,304239294,304239295,304239296,304239297,304239298,304239299,304239300,304239301,304239302,304239303,304239304", "", "2", SearchDepth.SearchSince24Hrs);
            mob2_latest = (evdt1 > mob2_latest ? evdt1 : mob2_latest);
            evdt1 = LatestEVDT("304239293,304239295,304239297,304239299,304239301,304239303", "", "3", SearchDepth.SearchSince24Hrs);
            mob2_latest = (evdt1 > mob2_latest ? evdt1 : mob2_latest);
            evdt1 = LatestEVDT("304239305", "", "Supervision,Close supervision,Contact guard,Minimum assistance,Moderate assistance", SearchDepth.SearchSince24Hrs);
            mob2_latest = (evdt1 > mob2_latest ? evdt1 : mob2_latest);
            evdt1 = LatestEVDT("304239306", "", "Minimum assistance,Moderate assistance", SearchDepth.SearchSince24Hrs);
            mob2_latest = (evdt1 > mob2_latest ? evdt1 : mob2_latest);
            evdt1 = LatestEVDT("304239349,304239061,3042370041", "", "1", SearchDepth.SearchSince24Hrs);
            mob2_latest = (evdt1 > mob2_latest ? evdt1 : mob2_latest);

            if (mob2ss_dt > mob2_latest || mob2_75_dt > mob2_latest)
            {
                if (mob2ss_dt > mob2_latest) mob2_latest = mob2ss_dt;
                if (mob2_75_dt > mob2_latest) mob2_latest = mob2_75_dt;
                mob2ss_is_latest = true;
            }

            if (mob2)
                Program.VerboseAudit("MOBILITY: Partial charting found.");

            Program.VerboseAudit("MOBILITY: Complete charting search...");
            reslist = "Complete assist";
            bool mob4 = Exists("", "304239281", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Maximum Assist/Patient Performed 25,Total Assistance/Patient Performed <";
            mob4 |= Exists("", "304239282", "", "", reslist, SearchDepth.SearchSince24Hrs);

            reslist = EXACT_MATCH_PREFIX + "Dependent";
            mob4 |= Exists("", "304239283", "", "", reslist, SearchDepth.SearchSince24Hrs);
            //            reslist = "25";
            reslist = "Substantial/maximal assistance";
            mob4 |= Exists("", "304239284", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = EXACT_MATCH_PREFIX + "Dependent";
            mob4 |= Exists("", "304239284", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "02,01";
            mob4 |= Exists("", "304239285", "", "", reslist, SearchDepth.SearchSince24Hrs);
            mob4 |= Exists("", "304239286", "", "", reslist, SearchDepth.SearchSince24Hrs);
            mob4 |= Exists("", "304239287", "", "", reslist, SearchDepth.SearchSince24Hrs);
            mob4 |= Exists("", "304239288", "", "", reslist, SearchDepth.SearchSince24Hrs);
            mob4 |= Exists("", "304239289", "", "", reslist, SearchDepth.SearchSince24Hrs);
            mob4 |= Exists("", "304239290", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Maximum assist";
            mob4 |= Exists("", "304239291", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = EXACT_MATCH_PREFIX + "Dependent";
            mob4 |= Exists("", "304239291", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Maximum assist";
            mob4 |= Exists("", "304239292", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = EXACT_MATCH_PREFIX + "Dependent";
            mob4 |= Exists("", "304239292", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "4";
            mob4 |= Exists("", "304239293", "", "", reslist, SearchDepth.SearchSince24Hrs);
            mob4 |= Exists("", "304239295", "", "", reslist, SearchDepth.SearchSince24Hrs);
            mob4 |= Exists("", "304239297", "", "", reslist, SearchDepth.SearchSince24Hrs);
            mob4 |= Exists("", "304239299", "", "", reslist, SearchDepth.SearchSince24Hrs);
            mob4 |= Exists("", "304239301", "", "", reslist, SearchDepth.SearchSince24Hrs);
            mob4 |= Exists("", "304239303", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Maximum assistance";
            mob4 |= Exists("", "304239305", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = EXACT_MATCH_PREFIX + "Dependent";
            mob4 |= Exists("", "304239305", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Maximum assistance";
            mob4 |= Exists("", "304239306", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = EXACT_MATCH_PREFIX + "Dependent";
            mob4 |= Exists("", "304239306", "", "", reslist, SearchDepth.SearchSince24Hrs);

            DateTime mob4_latest;
            mob4_latest = LatestEVDT("304239281", "", "Complete assist", SearchDepth.SearchSince24Hrs);
            evdt1 = LatestEVDT("304239282", "", "Maximum Assist/Patient Performed 25,Total Assistance/Patient Performed <", SearchDepth.SearchSince24Hrs);
            mob4_latest = (evdt1 > mob4_latest ? evdt1 : mob4_latest);
            evdt1 = LatestEVDT("304239283", "", EXACT_MATCH_PREFIX + "Dependent", SearchDepth.SearchSince24Hrs);
            mob4_latest = (evdt1 > mob4_latest ? evdt1 : mob4_latest);
            evdt1 = LatestEVDT("304239284", "", "Substantial/maximal assistance", SearchDepth.SearchSince24Hrs);
            mob4_latest = (evdt1 > mob4_latest ? evdt1 : mob4_latest);
            evdt1 = LatestEVDT("304239285,304239286,304239287,304239288,304239289,304239290", "", "02,01", SearchDepth.SearchSince24Hrs);
            mob4_latest = (evdt1 > mob4_latest ? evdt1 : mob4_latest);
            evdt1 = LatestEVDT("304239291", "", "Maximum assist", SearchDepth.SearchSince24Hrs);
            mob4_latest = (evdt1 > mob4_latest ? evdt1 : mob4_latest);
            evdt1 = LatestEVDT("304239291", "", EXACT_MATCH_PREFIX + "Dependent", SearchDepth.SearchSince24Hrs);
            mob4_latest = (evdt1 > mob4_latest ? evdt1 : mob4_latest);
            evdt1 = LatestEVDT("304239292", "", "Maximum assist", SearchDepth.SearchSince24Hrs);
            mob4_latest = (evdt1 > mob4_latest ? evdt1 : mob4_latest);
            evdt1 = LatestEVDT("304239292", "", EXACT_MATCH_PREFIX + "Dependent", SearchDepth.SearchSince24Hrs);
            mob4_latest = (evdt1 > mob4_latest ? evdt1 : mob4_latest);
            evdt1 = LatestEVDT("304239293,304239295,304239297,304239299,304239301,304239303", "", "4", SearchDepth.SearchSince24Hrs);
            mob4_latest = (evdt1 > mob4_latest ? evdt1 : mob4_latest);
            evdt1 = LatestEVDT("304239305", "", "Maximum assistance", SearchDepth.SearchSince24Hrs);
            mob4_latest = (evdt1 > mob4_latest ? evdt1 : mob4_latest);
            evdt1 = LatestEVDT("304239305", "", EXACT_MATCH_PREFIX + "Dependent", SearchDepth.SearchSince24Hrs);
            mob4_latest = (evdt1 > mob4_latest ? evdt1 : mob4_latest);
            evdt1 = LatestEVDT("304239306", "", "Maximum assistance", SearchDepth.SearchSince24Hrs);
            mob4_latest = (evdt1 > mob4_latest ? evdt1 : mob4_latest);
            evdt1 = LatestEVDT("304239306", "", EXACT_MATCH_PREFIX + "Dependent", SearchDepth.SearchSince24Hrs);
            mob4_latest = (evdt1 > mob4_latest ? evdt1 : mob4_latest);

            if (mob4)
                Program.VerboseAudit("MOBILITY: Complete charting found.");

            if (mob4_latest > DateTime.MinValue && mob4_latest >= mob2_latest && mob4_latest >= mob1_latest)
            {
                Program.VerboseAudit("LATEST MOBILITY: Complete level is latest charting.");
                mob1 = false;
                mob2 = false;
                mob4 = true;
            }
            else if (mob2_latest > DateTime.MinValue && mob2_latest >= mob1_latest && mob2_latest >= mob4_latest)
            {
                Program.VerboseAudit("LATEST MOBILITY: Partial level is latest charting.");
                mob1 = false;
                mob2 = true;
                mob4 = false;
            }
            else if (mob1_latest > DateTime.MinValue && mob1_latest >= mob4_latest && mob1_latest >= mob2_latest)
            {
                Program.VerboseAudit("LATEST MOBILITY: Self level is latest charting.");
                mob1 = true;
                mob2 = false;
                mob4 = false;
            }

            Program.VerboseAudit("TOILETING: Independent charting search...");
            reslist = "Independent,Modified Independent";
            bool toi1 = Exists("", "304239307", "", "", reslist, SearchDepth.SearchSince24Hrs);
            //reslist = "Independent,Modified Independent,Supervision";
            reslist = "Independent,Setup,Supervision";
            toi1 |= Exists("", "304239308", "", "", reslist, SearchDepth.SearchSince24Hrs);
            toi1 |= Exists("", "304239309", "", "", reslist, SearchDepth.SearchSince24Hrs);
            toi1 |= Exists("", "304239310", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "06,05,04";
            toi1 |= Exists("", "304239311", "", "", reslist, SearchDepth.SearchSince24Hrs);
            //reslist = "Independent,Modified Independent,Supervision";
            reslist = "Independent,Setup,Supervision";
            toi1 |= Exists("", "304239312", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Patient/Resident toileted self";
            toi1 |= Exists("", "304239313", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "0,1";
            toi1 |= Exists("", "304239314", "", "", reslist, SearchDepth.SearchSince24Hrs);
            toi1 |= Exists("", "304239315", "", "", reslist, SearchDepth.SearchSince24Hrs);
            //reslist = "";
            //toi1 |= Exists("", "30423099316", "", "", reslist);
            //reslist = "";
            //toi1 |= Exists("", "304237064", "", "", reslist);
            //reslist = "";
            //toi1 |= Exists("", "304239318", "", "", reslist);
            //reslist = "";
            //toi1 |= Exists("", "304239319", "", "", reslist);
            //reslist = "";
            //toi1 |= Exists("", "304239320", "", "", reslist);
            reslist = "Always continent";
            toi1 |= Exists("", "304239321", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Independent,Modified independent,With set up,Supervision,Contact guard";
            toi1 |= Exists("", "304239322", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Independent,Modified independent,With set up";
            toi1 |= Exists("", "304239323", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Independent,Modified independent,With set up";
            toi1 |= Exists("", "304239324", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Independent,Modified Independent";
            toi1 |= Exists("", "304239003", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Independent";
            toi1 |= Exists("", "304239308,304239309,304239310,3042311241", "", "", reslist, SearchDepth.SearchSince24Hrs);

            DateTime toi1_latest;
            toi1_latest = LatestEVDT("304239307", "", "Independent", SearchDepth.SearchSince24Hrs);
            evdt1 = LatestEVDT("304239308,304239309,304239310", "", "Independent,Setup,Supervision", SearchDepth.SearchSince24Hrs);
            toi1_latest = (evdt1 > toi1_latest ? evdt1 : toi1_latest);
            evdt1 = LatestEVDT("304239311", "", "06,05,04", SearchDepth.SearchSince24Hrs);
            toi1_latest = (evdt1 > toi1_latest ? evdt1 : toi1_latest);
            evdt1 = LatestEVDT("304239312", "", "Independent,Setup,Supervision", SearchDepth.SearchSince24Hrs);
            toi1_latest = (evdt1 > toi1_latest ? evdt1 : toi1_latest);
            evdt1 = LatestEVDT("304239313", "", "Patient/Resident toileted self", SearchDepth.SearchSince24Hrs);
            toi1_latest = (evdt1 > toi1_latest ? evdt1 : toi1_latest);
            evdt1 = LatestEVDT("304239314,304239315", "", "0,1", SearchDepth.SearchSince24Hrs);
            toi1_latest = (evdt1 > toi1_latest ? evdt1 : toi1_latest);
            evdt1 = LatestEVDT("304239321", "", "Always continent", SearchDepth.SearchSince24Hrs);
            toi1_latest = (evdt1 > toi1_latest ? evdt1 : toi1_latest);
            evdt1 = LatestEVDT("304239322", "", "Independent,Modified independent,With set up,Supervision,Contact guard", SearchDepth.SearchSince24Hrs);
            toi1_latest = (evdt1 > toi1_latest ? evdt1 : toi1_latest);
            evdt1 = LatestEVDT("304239323,304239324", "", "Independent,Modified independent,With set up", SearchDepth.SearchSince24Hrs);
            toi1_latest = (evdt1 > toi1_latest ? evdt1 : toi1_latest);
            evdt1 = LatestEVDT("304239003", "", "Independent,Modified Independent", SearchDepth.SearchSince24Hrs);
            toi1_latest = (evdt1 > toi1_latest ? evdt1 : toi1_latest);
            evdt1 = LatestEVDT("304239308,304239309,304239310,3042311241", "", "Independent", SearchDepth.SearchSince24Hrs);
            toi1_latest = (evdt1 > toi1_latest ? evdt1 : toi1_latest);

            if (toi1)
                Program.VerboseAudit("TOILETING: Independent charting found.");

            Program.VerboseAudit("TOILETING: Partial charting search...");
            reslist = "Setup/Supervision,Minimum Assist/Patient Perf,Moderate Assist/Patient Perf";
            bool toi2 = Exists("", "304239307", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Partial/moderate assistance"; // 75,50";
            toi2 |= Exists("", "304239308", "", "", reslist, SearchDepth.SearchSince24Hrs);
            toi2 |= Exists("", "304239309", "", "", reslist, SearchDepth.SearchSince24Hrs);
            toi2 |= Exists("", "304239310", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "03";
            toi2 |= Exists("", "304239311", "", "", reslist, SearchDepth.SearchSince24Hrs);
            //reslist = "Minimum,Moderate";
            reslist = "Partial/moderate assistance"; // 75,50";
            toi2 |= Exists("", "304239312", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Toileted by staff,Bed pan/ commode,Incontinent brief used";
            toi2 |= Exists("", "304239313", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "2,3";
            toi2 |= Exists("", "304239314", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "2";
            toi2 |= Exists("", "304239315", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "";
            toi2 |= Exists("", "30423099316", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "";
            toi2 |= Exists("", "304237064", "", "", reslist, SearchDepth.SearchSince24Hrs);

            

        reslist = "Incontinent";
        toi2 |= Exists("", "304239318", "", "", reslist, SearchDepth.SearchSince24Hrs);
        reslist = "Yes";
        toi2 |= Exists("", "304239319", "", "", reslist, SearchDepth.SearchSince24Hrs);
        reslist = "Incontinence"; // Incontinent daily,Always incontinent";
        toi2 |= Exists("", "304239032", "", "", reslist, SearchDepth.SearchSince24Hrs);
        reslist = "Incontinence";
        toi2 |= Exists("", "304239320", "", "", reslist, SearchDepth.SearchSince24Hrs);
        reslist = "Occasionally incontinent,Frequently incontinent,Always incontinent";
        toi2 |= Exists("", "304239321", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Minimum,Moderate";
            toi2 |= Exists("", "304239322", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Supervision,Close supervsion,Contact guard,Minimum assistance,Moderate assistance";
            toi2 |= Exists("", "304239323", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Supervision,Close supervsion,Contact guard,Minimum assistance,Moderate assistance";
            toi2 |= Exists("", "304239324", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Setup/Supervision,Minimum Assist/Patient Performed 75,Moderate Assist/Patient Performed 50";
            toi2 |= Exists("", "304239003", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Incontinence";
            toi2 |= Exists("", "304239032", "", "", reslist, SearchDepth.SearchSince24Hrs);
            //reslist = "50,75";
            //toi2 |= Exists("", "304239308,304239309,304239310", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "1 staff";
            toi2 |= Exists("", "3042301011", "", "", reslist, SearchDepth.SearchSince24Hrs);

            reslist = "setup,50,75";
            toi2 |= Exists("", "3042311241", "", "", reslist, SearchDepth.SearchSince24Hrs);

            DateTime toi2ss_dt = LatestEVDT("304239307,304239003", "", "Setup/Supervision", SearchDepth.SearchSince24Hrs);
            bool toi2ss_is_latest = false;

            DateTime toi2_latest;
            toi2_latest = LatestEVDT("304239307", "", "Minimum Assist/Patient Perf,Moderate Assist/Patient Perf", SearchDepth.SearchSince24Hrs);
            evdt1 = LatestEVDT("304239308,304239309,304239310", "", "Partial/moderate assistance", SearchDepth.SearchSince24Hrs);
            toi2_latest = (evdt1 > toi2_latest ? evdt1 : toi2_latest);
            evdt1 = LatestEVDT("304239311", "", "03", SearchDepth.SearchSince24Hrs);
            toi2_latest = (evdt1 > toi2_latest ? evdt1 : toi2_latest);
            evdt1 = LatestEVDT("304239312", "", "Partial/moderate assistance", SearchDepth.SearchSince24Hrs);
            toi2_latest = (evdt1 > toi2_latest ? evdt1 : toi2_latest);
            evdt1 = LatestEVDT("304239313", "", "Toileted by staff,Bed pan/ commode,Incontinent brief used", SearchDepth.SearchSince24Hrs);
            toi2_latest = (evdt1 > toi2_latest ? evdt1 : toi2_latest);
            evdt1 = LatestEVDT("304239314", "", "2,3", SearchDepth.SearchSince24Hrs);
            toi2_latest = (evdt1 > toi2_latest ? evdt1 : toi2_latest);
            evdt1 = LatestEVDT("304239315", "", "2", SearchDepth.SearchSince24Hrs);
            toi2_latest = (evdt1 > toi2_latest ? evdt1 : toi2_latest);
            evdt1 = LatestEVDT("30423099316,304237064", "", "", SearchDepth.SearchSince24Hrs);
            toi2_latest = (evdt1 > toi2_latest ? evdt1 : toi2_latest);

        //evdt1 = LatestEVDT("304239318", "", "Incontinent", SearchDepth.SearchSince24Hrs);
        //toi2_latest = (evdt1 > toi2_latest ? evdt1 : toi2_latest);
        //evdt1 = LatestEVDT("304239319", "", "Yes", SearchDepth.SearchSince24Hrs);
        //toi2_latest = (evdt1 > toi2_latest ? evdt1 : toi2_latest);
        //evdt1 = LatestEVDT("304239320", "", "Incontinence", SearchDepth.SearchSince24Hrs);
        //toi2_latest = (evdt1 > toi2_latest ? evdt1 : toi2_latest);
        //evdt1 = LatestEVDT("304239321", "", "Occasionally incontinent,Frequently incontinent,Always incontinent", SearchDepth.SearchSince24Hrs);
        //toi2_latest = (evdt1 > toi2_latest ? evdt1 : toi2_latest);
        //evdt1 = LatestEVDT("304239032", "", "Incontinence", SearchDepth.SearchSince24Hrs);
        //toi2_latest = (evdt1 > toi2_latest ? evdt1 : toi2_latest);


            evdt1 = LatestEVDT("304239322", "", "Minimum,Moderate", SearchDepth.SearchSince24Hrs);
            toi2_latest = (evdt1 > toi2_latest ? evdt1 : toi2_latest);
            reslist = "Supervision,Close superv,Contact guard,Minimum assistance,Moderate assistance";
            evdt1 = LatestEVDT("304239323,304239324", "", reslist, SearchDepth.SearchSince24Hrs);
            toi2_latest = (evdt1 > toi2_latest ? evdt1 : toi2_latest);
 DateTime toi2_75_dt= LatestEVDT("304239003", "", "Minimum Assist/Patient Performed 75", SearchDepth.SearchSince24Hrs);
            evdt1 = LatestEVDT("304239003", "", "Moderate Assist/Patient Performed 50", SearchDepth.SearchSince24Hrs);
            toi2_latest = (evdt1 > toi2_latest ? evdt1 : toi2_latest);


            reslist = "1 staff";
            evdt1 = LatestEVDT("3042301011", "", reslist, SearchDepth.SearchSince24Hrs);
            toi2_latest = (evdt1 > toi2_latest ? evdt1 : toi2_latest);

 DateTime toi2_75b_dt = LatestEVDT("3042311241", "", "75", SearchDepth.SearchSince24Hrs);

            reslist = "setup,50";
            evdt1 = LatestEVDT("3042311241", "", reslist, SearchDepth.SearchSince24Hrs);
            toi2_latest = (evdt1 > toi2_latest ? evdt1 : toi2_latest);

            if (toi2ss_dt > toi2_latest || toi2_75_dt > toi2_latest || toi2_75b_dt > toi2_latest)
            {
                if (toi2ss_dt > toi2_latest) toi2_latest = toi2ss_dt;
                if (toi2_75_dt > toi2_latest) toi2_latest = toi2_75_dt;
                if (toi2_75b_dt > toi2_latest) toi2_latest = toi2_75b_dt;
                toi2ss_is_latest = true;
            }

            if (toi2)
                Program.VerboseAudit("TOILETING: Partial charting found.");

            Program.VerboseAudit("TOILETING: Complete charting search...");
            bool toi4 = false;
            reslist = "Maximum Assist/Patient Performed 25-49,Total Assistance/Patient Performed <";
            toi4 |= Exists("", "304239307", "", "", reslist, SearchDepth.SearchSince24Hrs);
            //reslist = "Maximum,Total";
            reslist = "Substantial/maximal assistance";
            toi4 |= Exists("", "304239308", "", "", reslist, SearchDepth.SearchSince24Hrs);
            toi4 |= Exists("", "304239309", "", "", reslist, SearchDepth.SearchSince24Hrs);
            toi4 |= Exists("", "304239310", "", "", reslist, SearchDepth.SearchSince24Hrs);
            toi4 |= Exists("", "304239312", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = EXACT_MATCH_PREFIX + "Dependent";
            toi4 |= Exists("", "304239308", "", "", reslist, SearchDepth.SearchSince24Hrs);
            toi4 |= Exists("", "304239309", "", "", reslist, SearchDepth.SearchSince24Hrs);
            toi4 |= Exists("", "304239310", "", "", reslist, SearchDepth.SearchSince24Hrs);
            toi4 |= Exists("", "304239312", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "02,01";
            toi4 |= Exists("", "304239311", "", "", reslist, SearchDepth.SearchSince24Hrs);
            //reslist = "Maximum,Total";
            reslist = "4";
            toi4 |= Exists("", "304239314", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "";
            toi4 |= Exists("", "30423099316", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "";
            toi4 |= Exists("", "304237064", "", "", reslist, SearchDepth.SearchSince24Hrs);
            //reslist = "Incontinent";
            //bool toi4_18 = Exists("", "304239318", "", "", reslist, SearchDepth.SearchSince24Hrs);
            //reslist = "Yes";
            //bool toi4_19 = Exists("", "304239319", "", "", reslist, SearchDepth.SearchSince24Hrs);
            //reslist = "Incontinence"; // Incontinent daily,Always incontinent";
            //bool toi4_32 = Exists("", "304239032", "", "", reslist, SearchDepth.SearchSince24Hrs);
            //reslist = "Incontinence";
            //bool toi4_20 = Exists("", "304239320", "", "", reslist, SearchDepth.SearchSince24Hrs);
            //reslist = "Occasionally incontinent,Frequently incontinent,Always incontinent";
            //bool toi4_21 = Exists("", "304239321", "", "", reslist, SearchDepth.SearchSince24Hrs);
            //bool toi4_incont = false;
            //if ((toi4_18 ? 1 : 0) + (toi4_19 ? 1 : 0) + (toi4_20 ? 1 : 0) + (toi4_21 ? 1 : 0) + (toi4_32 ? 1 : 0) >= 4)
            //{
            //    toi4_incont = true;
            //    toi4 = true;
            //}

            reslist = "Incontinent";
            bool toi4_18 = (CountItems("", "304239318", "", "", reslist) >= 2);
            reslist = "Yes";
            bool toi4_19 = (CountItems("", "304239319", "", "", reslist) >= 2);
            reslist = "Incontinence"; // Incontinent daily,Always incontinent";
            bool toi4_32 = (CountItems("", "304239032", "", "", reslist) >= 2);

            //string foundwhat;
            //int ct2 = CountItems("", "304239320", "", "", reslist,SearchDepth.SearchDefault,true,out foundwhat);
            //Program.VerboseAudit("incont20 count=" + ct2 + " foundwhat="+foundwhat);
            reslist = "Incontinence";
            bool toi4_20 = (CountItems("", "304239320", "", "", reslist) >= 2);

            //reslist = "Occasionally incontinent,Frequently incontinent,Always incontinent";
            reslist = "Incontinent";
            bool toi4_21 = (CountItems("", "304239321", "", "", reslist) >= 2);
            bool toi4_incont = false;
            if (toi4_18 || toi4_19 || toi4_20 || toi4_21 || toi4_32)
            {
                toi4_incont = true;
                toi4 = true;
            }

            reslist = "Maximum";
            toi4 |= Exists("", "304239322", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = EXACT_MATCH_PREFIX + "Dependent";
            toi4 |= Exists("", "304239322", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Maximum assistance";
            toi4 |= Exists("", "304239323", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = EXACT_MATCH_PREFIX + "Dependent";
            toi4 |= Exists("", "304239323", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Maximum assistance";
            toi4 |= Exists("", "304239324", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = EXACT_MATCH_PREFIX + "Dependent";
            toi4 |= Exists("", "304239324", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "Maximum Assist/Patient Performed 25,Total Assistance/Patient Performed <";
            toi4 |= Exists("", "304239003", "", "", reslist, SearchDepth.SearchSince24Hrs);
            reslist = "maximum,total";
            //toi4 |= Exists("", "304239308,304239309,304239310", "", "", reslist, SearchDepth.SearchSince24Hrs);
            toi4 |= Exists("", "3042311241", "", "", reslist, SearchDepth.SearchSince24Hrs);

            DateTime toi4_latest;
            toi4_latest = LatestEVDT("304239307", "", "Maximum Assist/Patient Performed 25-49,Total Assistance/Patient Performed <", SearchDepth.SearchSince24Hrs);
            evdt1 = LatestEVDT("304239308,304239309,304239310,304239312", "", "Substantial/maximal assistance", SearchDepth.SearchSince24Hrs);
            toi4_latest = (evdt1 > toi4_latest ? evdt1 : toi4_latest);
            reslist = EXACT_MATCH_PREFIX + "Dependent";
            evdt1 = LatestEVDT("304239308,304239309,304239310,304239312", "", reslist, SearchDepth.SearchSince24Hrs);
            toi4_latest = (evdt1 > toi4_latest ? evdt1 : toi4_latest);
            evdt1 = LatestEVDT("304239311", "", "02,01", SearchDepth.SearchSince24Hrs);
            toi4_latest = (evdt1 > toi4_latest ? evdt1 : toi4_latest);
            evdt1 = LatestEVDT("304239314", "", "4", SearchDepth.SearchSince24Hrs);
            toi4_latest = (evdt1 > toi4_latest ? evdt1 : toi4_latest);
            evdt1 = LatestEVDT("30423099316,304237064", "", "", SearchDepth.SearchSince24Hrs);
            toi4_latest = (evdt1 > toi4_latest ? evdt1 : toi4_latest);
    //DateTime toi4_evdt18 = LatestEVDT("304239318", "", "Incontinent", SearchDepth.SearchSince24Hrs);
    //DateTime toi4_evdt19 = LatestEVDT("304239319", "", "Yes", SearchDepth.SearchSince24Hrs);
    //DateTime toi4_evdt32 = LatestEVDT("304239032", "", "Incontinence", SearchDepth.SearchSince24Hrs);
    //DateTime toi4_evdt20 = LatestEVDT("304239320", "", "Incontinence", SearchDepth.SearchSince24Hrs);
    //DateTime toi4_evdt21 = LatestEVDT("304239321", "", "Occasionally incontinent,Frequently incontinent,Always incontinent", SearchDepth.SearchSince24Hrs);
    //DateTime toi4_incontdt = (toi4_evdt18 > toi4_evdt19 ? toi4_evdt18 : toi4_evdt19);
    //toi4_incontdt = (toi4_incontdt > toi4_evdt32 ? toi4_incontdt : toi4_evdt32);
    //toi4_incontdt = (toi4_incontdt > toi4_evdt20 ? toi4_incontdt : toi4_evdt20);
    //toi4_incontdt = (toi4_incontdt > toi4_evdt21 ? toi4_incontdt : toi4_evdt21);
            evdt1 = LatestEVDT("304239322", "", "Maximum", SearchDepth.SearchSince24Hrs);
            toi4_latest = (evdt1 > toi4_latest ? evdt1 : toi4_latest);
            reslist = EXACT_MATCH_PREFIX + "Dependent";
            evdt1 = LatestEVDT("304239322", "", reslist, SearchDepth.SearchSince24Hrs);
            toi4_latest = (evdt1 > toi4_latest ? evdt1 : toi4_latest);
            evdt1 = LatestEVDT("304239323,304239324", "", "Maximum assistance", SearchDepth.SearchSince24Hrs);
            toi4_latest = (evdt1 > toi4_latest ? evdt1 : toi4_latest);
            reslist = EXACT_MATCH_PREFIX + "Dependent";
            evdt1 = LatestEVDT("304239323", "", reslist, SearchDepth.SearchSince24Hrs);
            toi4_latest = (evdt1 > toi4_latest ? evdt1 : toi4_latest);
            evdt1 = LatestEVDT("304239324", "", reslist, SearchDepth.SearchSince24Hrs);
            toi4_latest = (evdt1 > toi4_latest ? evdt1 : toi4_latest);
            reslist = "Maximum Assist/Patient Performed 25,Total Assistance/Patient Performed <";
            evdt1 = LatestEVDT("304239003", "", reslist, SearchDepth.SearchSince24Hrs);
            toi4_latest = (evdt1 > toi4_latest ? evdt1 : toi4_latest);
            reslist = "maximum,total";
            evdt1 = LatestEVDT("3042311241", "", reslist, SearchDepth.SearchSince24Hrs);
            toi4_latest = (evdt1 > toi4_latest ? evdt1 : toi4_latest);

            //if (toi4_incontdt > toi4_latest && toi4_incont)
            //{
            //    toi4_latest = toi4_incontdt;
            //    Program.VerboseAudit("TOILETING Incontinence is the latest charting AND at least 2 incontinence items are charted within 12 hours.");
            //}
            if (toi4_incont)
            {
                Program.VerboseAudit("COMPLETE TOILETING: at least 2 incontinence items were charted within 12 hours...WILL TRIGGER EXTENDED minimally.");
            }

            if (toi4)
                Program.VerboseAudit("TOILETING: Complete charting found.");

            if (toi4_incont || (toi4_latest > DateTime.MinValue && toi4_latest >= toi1_latest && toi4_latest >= toi2_latest))
            {
                if (!toi4_incont)
                    Program.VerboseAudit("LATEST TOILETING: Complete level is latest charting.");
                toi1 = false;
                toi2 = false;
                toi4 = true;
            }
            else if (toi2_latest > DateTime.MinValue && toi2_latest >= toi1_latest && toi2_latest >= toi4_latest)
            {
                Program.VerboseAudit("LATEST TOILETING: Partial level is latest charting.");
                toi1 = false;
                toi2 = true;
                toi4 = false;
            }
            else if (toi1_latest > DateTime.MinValue && toi1_latest > toi2_latest && toi1_latest > toi4_latest)
            {
                Program.VerboseAudit("LATEST TOILETING: Self level is latest charting.");
                toi1 = true;
                toi2 = false;
                toi4 = false;
            }


            if (!toi4 || (_pat.age < 6.0))
            {
                CheckLDA("Urinary", "3042394421", "3042394422", "3042394424", "3042394425");
                CheckLDA("Urinary","900700", "900701", "900702", "900703");

                if (g_toi4)
                {
                    toi4 = true;
                    Program.VerboseAudit("Urinary Catheter => Total Toileting");
                }

            }

            //if (feed2) Program.VerboseAudit("Partial Feeding present."); else Program.VerboseAudit("Partial Feeding Not present.");
            //if (hyg2) Program.VerboseAudit("Partial Hygiene present."); else Program.VerboseAudit("Partial Hygiene Not present.");
            //if (mob2) Program.VerboseAudit("Partial Mobility present."); else Program.VerboseAudit("Partial Mobility Not present.");
            //if (toi2) Program.VerboseAudit("Partial Toileting present."); else Program.VerboseAudit("Partial Toileting Not present.");
            //if (feed4) Program.VerboseAudit("Total Feeding present."); else Program.VerboseAudit("Total Feeding Not present.");
            //if (hyg4) Program.VerboseAudit("Total Hygiene present."); else Program.VerboseAudit("Total Hygiene Not present.");
            //if (mob4) Program.VerboseAudit("Total Mobility present."); else Program.VerboseAudit("Total Mobility Not present.");
            //if (toi4) Program.VerboseAudit("Total Toileting present."); else Program.VerboseAudit("Total Toileting Not present.");

            string yesorno = "";
            if (feed2) yesorno = "YES"; else yesorno = "NO";
            Program.VerboseAudit("Partial Feeding present = " + yesorno);
            if (hyg2) yesorno = "YES"; else yesorno = "NO";
            Program.VerboseAudit("Partial Hygiene present = " + yesorno);
            if (mob2) yesorno = "YES"; else yesorno = "NO";
            Program.VerboseAudit("Partial Mobility present = " + yesorno);
            if (toi2) yesorno = "YES"; else yesorno = "NO";
            Program.VerboseAudit("Partial Toileting present = " + yesorno);


            if (feed4) yesorno = "YES"; else yesorno = "NO";
            Program.VerboseAudit("Total Feeding present = " + yesorno);
            if (hyg4) yesorno = "YES"; else yesorno = "NO";
            Program.VerboseAudit("Total Hygiene present = " + yesorno);
            if (mob4) yesorno = "YES"; else yesorno = "NO";
            Program.VerboseAudit("Total Mobility present = " + yesorno);
            if (toi4) yesorno = "YES"; else yesorno = "NO";
            Program.VerboseAudit("Total Toileting present = " + yesorno);

            bool feedx = (feed2 || feed4);
            bool hygx = (hyg2 || hyg4);
            bool mobx = (mob2 || mob4);
            bool toix = (toi2 || toi4);
            //if ((feedx ? 1 : 0) + (hygx ? 1 : 0) + (mobx ? 1 : 0) + (toix ? 1 : 0) == 4
            //        && (feed4 || hyg4 || mob4 || toi4))
            //    SetInd(4, "All 4 ADL categories found at partial level or higher PLUS At least 1 at complete.");
            if (feed2 && feed2_75_is_latest) Program.VerboseAudit("Feeding: Moderate 75% was latest result.");
            if (hyg2 && hyg2ss_is_latest) Program.VerboseAudit("Hygiene: Setup/Supervision was latest result.");
            if (mob2 && mob2ss_is_latest) Program.VerboseAudit("Mobility: Setup/Supervision was latest result.");
            if (toi2 && toi2ss_is_latest) Program.VerboseAudit("Toileting: Setup/Supervision was latest result.");

            if (feed4 && hyg4 && mob4 && toi4)
                SetInd(4, "All 4 ADL categories found at complete level.");
            if (toi4_incont) //Feb 3 2022
                SetInd(3, "Incont x2");
            if ((feed2 && feed2_75_is_latest ? 1 : 0) + (hyg2 && hyg2ss_is_latest ? 1 : 0) + (mob2 && mob2ss_is_latest ? 1 : 0) + (toi2 && toi2ss_is_latest ? 1 : 0) >= 2)
            {
                SetInd(2, "At least 2 ADL categories found at the Setup/Supervision or Moderate 75% level.");
            }
            else if (((feedx ? 1 : 0) + (hygx ? 1 : 0) + (mobx ? 1 : 0) + (toix ? 1 : 0) >= 3) && !hyg2ss_is_latest)
            {
                SetInd(3, "At least 3 ADL categories found at the partial or complete level -AND- at most one category charted as Setup/Supervision or Moderate 75% -AND- Hygiene is not Min/SS -OR- Incontx2");
            }
            else if ((feedx ? 1 : 0) + (hygx ? 1 : 0) + (mobx ? 1 : 0) + (toix ? 1 : 0) > 0)
            {
                SetInd(2, "At least 1 ADL category found at the partial or complete level.");
            }
            if (feed1 || hyg1 || mob1 || toi1)
                SetInd(1, "At least 1 ADL category found at the independent level.");

            if (!_inds[1].is_checked && !_inds[2].is_checked && !_inds[3].is_checked && !_inds[4].is_checked)
            {
                SetInd(1, "No documentation found.  Defaulting to ADL Self");
            }

        }
        private void GetNTEValues(string itemcode)
        {
            //look for c_i code 3040000025401 with order_ctrl=nw.  
            //if description contains ;;; then process order as usual.
            //else we need to get the NTE value.  if desc not contain ;;; then look for the event_log item that
            //had this message using code and timestamp. 
            //look at the NTE and update the c_i RESULT to the NTE values 
            //and for DESC concat ";;;<NTE value>" onto desc.
            //now this c_i has the value and ;;; to avoid re-saving it.
            var db = PFSDBUtility.NewPfsDataContext();
            var query = from item in db.CHART_ITEMs
                        where (item.ENCOUNTER_ID == _pat.encounter_id)
                        where (item.CODE == itemcode)
                        where (item.EVENT_DATETIME < loc_out)
                        where (item.ORDER_CONTROL.ToUpper() == "NW")
                        where (item.ORDER_STATUS == null || item.ORDER_STATUS == "")
                        where (item.RESULT == null || item.RESULT == "")
                        orderby item.EVENT_DATETIME descending
                        select item;
            foreach (var item in query)
            {
                //NTE|1||Observation Checks->Q15M | NTE | 2 || Unit Level->Restrict to Unit| NTE | 3 || Supervision->None |
                Program.VerboseAudit("Found safety orderid:" + item.ORDER_ID + " at ts:" + item.TIMESTAMP);
                AddNTEValues(itemcode, item.TIMESTAMP, item.ORDER_ID);
            }
        }

        private void AddNTEValues(string code, DateTime ts, string ordid)
        {
            string sql, res = "";
            string nte_str = "";
            const char quote = '\'';

            sql = "select ";
            sql += " substring(source_text, CHARINDEX('NTE|',source_text), 400) as NTE";
            sql += " from EVENT_LOG where 1=1"; //encounter_id=" + _pat.encounter_id;
            sql += " and TIMESTAMP between " + PFSDBUtility.SQLDateTime(ts.AddDays(-1)) + " and " + PFSDBUtility.SQLDateTime(ts.AddMinutes(30));
            sql += " and (description like 'O01%') and description like '%" + _pat.acct + "%'";
            sql += " and CHARINDEX('|" + code + "',source_text) > 0 and CHARINDEX('NTE|',source_text) > 0";
            sql += " and CHARINDEX('|" + ordid + "',source_text) > 0";
            Program.VerboseAudit(sql);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd2 = new SqlCommand(sql, db2);
            SqlDataReader dr2 = cmd2.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            if (dr2.HasRows)
            {
                //Program.VerboseAudit("Has rows");
                while (dr2.Read())
                {
                    res = "";
                    nte_str = PFSDBUtility.DBToString(dr2["NTE"]);
                    Program.VerboseAudit("Aidx of apostr=" + nte_str.IndexOf(quote));
                    nte_str = nte_str.Replace(quote, ' ');
                    Program.VerboseAudit("Bidx of apostr=" + nte_str.IndexOf(quote));
                    //Program.VerboseAudit("NTE string=" + nte_str);
                    // NTE string= NTE|1||Observation Checks->Q15M|NTE|2||Unit Level->Off unit with staff|NTE|3||Supervision->Sharps|
                    int nteidx = 0;
                    while (nte_str.Contains("NTE|"))
                    { //iterate across the nte string until all NTEs have been found
                        Program.VerboseAudit("1nte_str=" + nte_str);
                        string s = nte_str.Substring(nteidx);
                        var arr = s.Split('|');
                        for (int i = 0; i <= arr.GetUpperBound(0); i++)
                        {
                            arr[i] = arr[i].Trim();                         // get rid of leading blanks
                        }
                        if (arr.GetUpperBound(0) >= 3)
                        {
                            res = res + arr[3] + ";";
                            //Program.VerboseAudit("res=" + res);
                        }
                        nte_str = nte_str.Substring(1);//advance the pointer to find the next NTE
                        Program.VerboseAudit("2nte_str=" + nte_str);
                        nteidx = nte_str.IndexOf("NTE|");//the next NTE sets the nteidx value
                        if (nteidx > 0)
                        {
                            nte_str = nte_str.Substring(nteidx);
                            Program.VerboseAudit("3nte_str=" + nte_str);
                            nteidx = 0;
                        }
                    }
                    var db = PFSDBUtility.NewSqlConnection();
                    string q;
                    if (res.Trim() == "")
                    {
                        //Program.VerboseAudit("No result value to add; update order status X");
                        q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and order_id='" + ordid + "'";
                        UpdatePtChartArrays(ordid, "X", "");
                    }
                    else
                    {
                        //Program.VerboseAudit("NTE result value to add: " + res);
                        q = "UPDATE chart_item set RESULT='" + res + "' where encounter_id=" + _pat.encounter_id + " and order_id='" + ordid + "'";
                        UpdatePtChartArrays(ordid, "", res);
                    }
                    SqlCommand cmd = new SqlCommand(q, db);
                    cmd.ExecuteNonQuery();
                    db.Close();
                }
            }
            else
            {
                var db = PFSDBUtility.NewSqlConnection();
                string q;
                //Program.VerboseAudit("No such event log found; update order status X");
                q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and order_id='" + ordid + "'";
                UpdatePtChartArrays(ordid, "X", "");
                SqlCommand cmd = new SqlCommand(q, db);
                cmd.ExecuteNonQuery();
                db.Close();
            }
            dr2.Close();
            db2.Close();
        }
        private void UpdatePtChartArrays(string orderid, string ordstatus, string newres)
        {
            foreach (var item in _chart_items_since24hrs)
            {
                if (item.ORDER_ID == orderid)
                {
                    if (ordstatus != "")
                        item.ORDER_STATUS = ordstatus;
                    if (newres != "")
                        item.RESULT = newres;
                }
            }
        }

        private void UpdatePtChartArraysCode(string code, string ordstatus,DateTime dcdt)
        {
            foreach (var item in _chart_items_since_admission)
            {
                if (item.CODE == code && item.EVENT_DATETIME <= dcdt)
                {
                    item.ORDER_STATUS = ordstatus;
                }
            }
        }


        //private bool Check_b79()
        //{
        //    bool b = false;

        //    var query = StartNewQuery(SearchDepth.SearchSince13Hrs);
        //    query = query.Where(e => e.CODE.StartsWith("MED"));
        //    query = query.Where(e => e.DESCRIPTION.ToUpper().Contains("PEG3350 100 GRAM-SOD SUL") && e.DESCRIPTION.ToLower().Contains(";;;given"));
        //    b = (query.Count() > 0);
        //    return b;
        //}

        //private void CheckQ2FreqForADLExt(string maprow, string code1, string res1, string code2, string res2)
        //{
        //    if (_inds[3].is_checked || _inds[4].is_checked) return;
        //    var buckets = new List<gBucket>();
        //    if (code2 != "")
        //        AddDependentBuckets(buckets, code1, res1, code2, res2);
        //    else
        //        AddBuckets(buckets, "", code1, "", "", res1);
        //    if (AnalyzeBuckets(buckets, 3, 120, maprow, false))
        //        SetInd(3, maprow);
        //}



        private void Check_5()
        {
            string reslist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("5. ADL Rehab");
            Program.VerboseAudit("---------------");

            exclude_periop_data = false;

            if (_pat.short_name == "SRH" || _pat.short_name == "SCC")
                SetInd(5, "Selecting indicator based on facility=" + _pat.short_name);

            reslist = "2,3,4,5"; //pos or neg
            if (Exists("", "304239326", "", "", reslist))
            {
                Program.VerboseAudit("RASS score magnitude excludes indicator.");
                return;
            }

            if (coma)
            {
                Program.VerboseAudit("Patient has no LOC.");
                return;
            }

            reslist = "Ultra slow flow,Very slow flow,Slow flow,Cross cut,Special needs feeder,Other";
            SetIndIfResultContains(5, "", "3042309339", "", "", reslist);
            reslist = "Multiple swallows,Tongue thrust,Uncoordinated suck/swallow/breathe,With encouragement";
            SetIndIfResultContains(5, "", "3042309340", "", "", reslist);
            reslist = "Asymmetric,Excessive,Uncoordinated,Gag hyperactive";
            SetIndIfResultContains(5, "", "3042309341", "", "", reslist);
            reslist = "Inplace,Started,Restarted,Other";
            SetIndIfResultContains(5, "", "3042309342", "", "", reslist);
            reslist = "Inplace,Started,Restarted,Other";
            SetIndIfResultContains(5, "", "3042309343", "", "", reslist);
            reslist = "Inplace,Started,Restarted,Other";
            SetIndIfResultContains(5, "", "3042309344", "", "", reslist);
            reslist = "Inplace,Started,Discontinued,Other";
            SetIndIfResultContains(5, "", "3042309345", "", "", reslist);
            //reslist = "Bottle";
            //SetIndIfResultContains(5, "", "3042309346", "", "", reslist);
            reslist = "Bradycardia,Color change,Desaturations,Gagging,Needs pacing,Uncoordinated suck,Tachycardia,Tachypnea";
            SetIndIfResultContains(5, "", "3042309347", "", "", reslist);
            reslist = "yes";
            SetIndIfResultContains(5, "", "3042309348", "", "", reslist);
            reslist = "Oral Motor Exercises,Swallowing Exercises,Swallowing Treatment,Oral Motor/Feeding";
            SetIndIfResultContains(5, "", "3042309349", "", "", reslist);
            reslist = "Cleft palate,Cleft lip";
            SetIndIfResultContains(5, "", "3042309350", "", "", reslist);
            reslist = "Ineffective,Uncoordinated suck/swallow/breathe,Weak";
            SetIndIfResultContains(5, "", "304239325", "", "", reslist);

            reslist = "2=Nipples with a strong coordinated SSB but fatigues with progression.,3=Difficulty coordinating SSB despite consistent suck.,4=Nipples with a weak/inconsistent SSB. Little to no rhythm";
            SetIndIfResultContains(5, "", "304239327", "", "", reslist);
            reslist = "A=Modified Sidelying: Position infant in inclined sidelying position with head in midline to assist with bolus management.,B=External Pacing: Tip bottle downward/break seal at breast to remove or decrease the flow of liquid to facilitate SSB pattern.,C=Specialty Nipple: Use nipple other than standard for specific purpose i.e. nipple shield., slow-flow, Haberman,D=Cheek Support: Provide gentle unilateral support to improve intra oral pressure.,E=Frequent Burping: Burp infant based on behavioral cues not on time or volume completed.,F=Chin Support: Provide gentle forward pressure on mandible to ensure effective latch/tongue stripping if small chin or wide jaw excursion.";
            SetIndIfResultContains(5, "", "304239328", "", "", reslist);
            reslist = "Yes";
            SetIndIfResultContains(5, "", "304239329", "", "", reslist);
            reslist = "Yes";
            SetIndIfResultContains(5, "", "304239330", "", "", reslist);
            reslist = "Yes";
            SetIndIfResultContains(5, "", "304239331", "", "", reslist);
            reslist = "Yes";
            SetIndIfResultContains(5, "", "304239332", "", "", reslist);
            reslist = "Passive ROM/ Active ROM,Transfer,Dressing/Grooming,Ostomy Teaching,Eating /Swallowing,Communication,Bed mobility,Bladder Retraining,Bowel Retraining,Ambulation,Splint Application,Prosthetic Care & Application,Orthotics Care & Application";
            SetIndIfResultContains(5, "", "304239333", "", "", reslist);
            reslist = "Yes";
            SetIndIfResultContains(5, "", "304239334", "", "", reslist);
            reslist = "Yes";
            SetIndIfResultContains(5, "", "304239335", "", "", reslist);
            reslist = "Yes";
            SetIndIfResultContains(5, "", "304239336", "", "", reslist);
            reslist = "Yes";
            SetIndIfResultContains(5, "", "304239337", "", "", reslist);
            reslist = "Yes";
            SetIndIfResultContains(5, "", "304239338", "", "", reslist);
            reslist = "Yes";
            SetIndIfResultContains(5, "", "304239004", "", "", reslist);
            reslist = "Yes";
            SetIndIfResultContains(5, "", "304230094052", "", "", reslist);
            SetIndIfResultContains(5, "", "3048704214", "", "", "");
            //Orders
            //NUR373,PRE6,134294,35408
            //Program.VerboseAudit("Checking Orders....");
            string desc_found = "";
            string code = "41343"; //"NUR373";
            if (OrderInProgress(code, out desc_found))
                SetInd(5, "Order in effect: " + desc_found);

            //code = "35406"; // "PRE6";
            //if (OrderInProgress(code, out desc_found))
            //    SetInd(5, "Order in effect: " + desc_found);

            //code = "134294";
            //if (OrderInProgress(code, out desc_found))
            //    SetInd(5, "Order in effect: " + desc_found);

            //code = "35408";
            //if (OrderInProgress(code, out desc_found))
            //    SetInd(5, "Order in effect: " + desc_found);

            //            “Dysphagia Diet”, trigger for ADL Rehab indicator #5
            //“Diet Regular” and a response to Swallow Safety Instructions(any value) completed, then trigger for ADL Rehab Indicator #5
            desc_found = "";
            GetNTEValues("40517");//Saves NTE value
            if (OrderInProgress("40517", out desc_found))
            {
                //npo = true;
                //Program.VerboseAudit("NPO in progress: ADL Feeding component set to dependent/complete.");
                //Program.VerboseAudit("order desc found=" + desc_found);
                if (desc_found.ToUpper().Contains("TYPE->DYSPHAGIA"))
                {
                    SetInd(5, "Order in effect: " + desc_found);
                }
                else if (!desc_found.ToUpper().Contains("TYPE->NPO"))
                {
                    if (desc_found.ToUpper().Contains("SWALLOW SAFETY"))
                        SetInd(5, "Order in effect: " + desc_found);
                }
            }



            //reslist = "";
            //SetIndIfResultContains(5, "", EXACT_MATCH_PREFIX+ "41343", "", "", reslist);
            //SetIndIfResultContains(5, "", EXACT_MATCH_PREFIX + "35406", "", "", reslist);
            //SetIndIfResultContains(5, "", EXACT_MATCH_PREFIX + "134294", "", "", reslist);
            //SetIndIfResultContains(5, "", EXACT_MATCH_PREFIX + "35408", "", "", reslist);

        }


        private void Check_6_7()
        {
            string reslist;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("6. ADL 2-3 Caregivers");
            Program.VerboseAudit("7. ADL 4 or more Caregivers");
            Program.VerboseAudit("---------------");

            SearchDepth search_depth = SearchDepth.SearchPullRange; // 2/10/21

            exclude_periop_data = false;

            //if (coma) return;
            if (_inds[1].is_checked && !_inds[2].is_checked && !_inds[3].is_checked && !_inds[4].is_checked)
            {
                Program.VerboseAudit("Suppressing 2-3,4+ caregivers due to ADL Self.");
                return;
            }

            reslist = "Ceiling/mechanical lift,slide board";
            SetIndIfResultContains(6, "", "304239340", "", "", reslist,search_depth);
            reslist = "";
            //SetIndIfResultContains(6, "", "304239341", "", "", reslist);
            SetIndIfResultContains(6, "", "304239605", "", "", "", search_depth);
            reslist = "";
            SetIndIfResultContains(6, "", "304239342", "", "", reslist, search_depth);
            reslist = "";
            SetIndIfResultContains(6, "", "304239343", "", "", reslist, search_depth);
            reslist = "2-3 person assist";
            SetIndIfResultContains(6, "", "304239284", "", "", reslist, search_depth);
            reslist = "2-3 Staff";
            SetIndIfResultContains(6, "", "304239344", "", "", reslist, search_depth);
            reslist = "Two person assist";
            SetIndIfResultContains(6, "", "304239281", "", "", reslist, search_depth);
            reslist = "3=Two or more person physical assist";
            SetIndIfResultContains(6, "", "304239276", "", "", reslist, search_depth);
            reslist = "2,3";
            SetIndIfResultContains(6, "", "304239345", "", "", reslist, search_depth);
            reslist = "2,3";
            SetIndIfResultContains(6, "", "304239346", "", "", reslist, search_depth);
            reslist = "2,3";
            SetIndIfResultContains(6, "", "304239347", "", "", reslist, search_depth);
            reslist = "2,3";
            SetIndIfResultContains(6, "", "304239348", "", "", reslist, search_depth);
            reslist = "3=Two or more person physical assist";
            SetIndIfResultContains(6, "", "304239294", "", "", reslist, search_depth);
            reslist = "3=Two or more person physical assist";
            SetIndIfResultContains(6, "", "304239296", "", "", reslist, search_depth);
            reslist = "3=Two or more person physical assist";
            SetIndIfResultContains(6, "", "304239298", "", "", reslist, search_depth);
            reslist = "3=Two or more person physical assist";
            SetIndIfResultContains(6, "", "304239300", "", "", reslist, search_depth);
            reslist = "3=Two or more person physical assist";
            SetIndIfResultContains(6, "", "304239302", "", "", reslist, search_depth);
            reslist = "3=Two or more person physical assist";
            SetIndIfResultContains(6, "", "304239304", "", "", reslist, search_depth);
            reslist = "3=Two or more person physical assist";
            SetIndIfResultContains(6, "", "304239257", "", "", reslist, search_depth);
            reslist = "3=Two or more person physical assist";
            SetIndIfResultContains(6, "", "304239265", "", "", reslist, search_depth);
            reslist = "3=Two or more person physical assist";
            SetIndIfResultContains(6, "", "304239315", "", "", reslist, search_depth);
            reslist = "3=Two or more person physical assist";
            SetIndIfResultContains(6, "", "304239278", "", "", reslist, search_depth);
            reslist = "Two (2) or more persons physical assist";
            SetIndIfResultContains(6, "", "304239349", "", "", reslist, search_depth);
            reslist = "2-3 Staff";
            SetIndIfResultContains(6, "", "304239061", "", "", reslist, search_depth);
            reslist = "2-3 Staff";
            SetIndIfResultContains(6, "", "304239061", "", "", reslist, search_depth);
            reslist = "2-3 Staff";
            SetIndIfResultContains(6, "", "3042370041", "", "", reslist, search_depth);
            reslist = "2,3";
            SetIndIfResultContains(6, "", "3042301011", "", "", reslist, search_depth);
            

            reslist = "4";
            SetIndIfResultContains(7, "", "304239344", "", "", reslist, search_depth);
            reslist = "4";
            SetIndIfResultContains(7, "", "304239345", "", "", reslist, search_depth);
            reslist = "4";
            SetIndIfResultContains(7, "", "304239346", "", "", reslist, search_depth);
            reslist = "4";
            SetIndIfResultContains(7, "", "304239347", "", "", reslist, search_depth);
            reslist = "4";
            SetIndIfResultContains(7, "", "304239348", "", "", reslist, search_depth);
            reslist = "4";
            SetIndIfResultContains(7, "", "304239061", "", "", reslist, search_depth);
            reslist = "4";
            SetIndIfResultContains(7, "", "3042370041", "", "", reslist, search_depth);
            reslist = "4";
            SetIndIfResultContains(7, "", "3042301011", "", "", reslist, search_depth);

            //Orders
            reslist = "";
            SetIndIfResultContains(7, "", EXACT_MATCH_PREFIX + "134345", "", "", reslist, search_depth);

        }

        private void Check_8()
        {
            string reslist;
            string found_what;
            string return_result;
            DateTime return_evdt;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("8. Communication");
            Program.VerboseAudit("---------------");

            bool lang_trig = CheckLanguage(); // PERSON.LANGUAGE_LVC will be xxx;;yyy  example: ENG;;RUS where xxx is preferred and yyy is written.

            if (GetResultAndEVDT("", "304239326", "", "", "-3,-4,-5", out return_result, out return_evdt))
            {
                if (!lang_trig)
                    Program.VerboseAudit("From this point forward, Communication triggers will be ignored due to RASS score of " + return_result + " at " + return_evdt.ToString());
                return;
            }

            exclude_periop_data = true;

            reslist = ""; //any value
            SetIndIfResultContains(8, "", "304239350", "", "", reslist);

            reslist = "Coma,Pharmaceutically paralyzed,Unresponsive";
            string res = "";
            //bool comm_coma = Exists("", "304239339", "", "", reslist, SearchDepth.SearchSince24Hrs);
            bool comm_coma = false;
            GetLatestResult("","304239339", "", "", out res,SearchDepth.SearchDefault);
            //if (res != "" && reslist.Contains(res))
            if (res != "" && (res.ToLower().Contains("coma") || res.ToLower().Contains("pharmaceutically paralyzed") || res.ToLower().Contains("unresponsive")))
                comm_coma = true;

            if (comm_coma)   //removed 5/7/20  reinstated 9/24/20  sedated does not apply for comm. 5/12/21
            {
                Program.VerboseAudit("Patient has no LOC.");
                return;
            }

            reslist = "Mouth,Jaw";
            if (Exists("", "30423396060", "", "", reslist))
            {
                reslist = "Surgical Site";
                SetIndIfResultContains(8, "", "3040200505", "", "", reslist);
            }

            reslist = "Impaired vision,Anopthalmia,Blurred,Blind,Cloudy,Double Vision,Prosthesis,Tunnel Vision";
            SetIndIfResultContains(8, "", "304239351", "", "", reslist);
            reslist = "Impaired vision,Anopthalmia,Blurred,Blind,Cloudy,Double Vision,Prosthesis,Tunnel Vision";
            SetIndIfResultContains(8, "", "304239352", "", "", reslist);
            reslist = "Impaired hearing,Deaf";
            SetIndIfResultContains(8, "", "304239353", "", "", reslist);
            reslist = "Impaired hearing,Deaf";
            SetIndIfResultContains(8, "", "304239354", "", "", reslist);
            reslist = "No Voice,Trach,Aphonic,Speaking valve,Voice Amplifier,Muffled";
            SetIndIfResultContains(8, "", "304239355", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(8, "", "304239356", "", "", reslist);
            reslist = "Artificial airway,Expressive Aphasia,Receptive Aphasia,Delayed,Garbled,Incomprehensible,No Speech,Receptive Aphasia,Slurred";
            SetIndIfResultContains(8, "", "304239357", "", "", reslist);
            reslist = "Impaired,Neglect left,Neglect right";
            SetIndIfResultContains(8, "", "304239358", "", "", reslist);
            reslist = "Impaired";
            SetIndIfResultContains(8, "", "304239359", "", "", reslist);
            //reslist = "No";
            //SetIndIfResultContains(8, "", "304239360", "", "", reslist);
            reslist = "Impaired";
            SetIndIfResultContains(8, "", "304239361", "", "", reslist);
            reslist = "Apparent hearing loss,Known hearing loss,Impacts communication";
            SetIndIfResultContains(8, "", "304239362", "", "", reslist);
            reslist = "Augmentative Device,Nonverbal - Gestures,Nonverbal - Sign Language,Nonverbal -  Written";
            SetIndIfResultContains(8, "", "304239363", "", "", reslist);
            //== testandprod            
            reslist = "Aphasia,Auditory processing deficit,Developmental Language Disorder,Nonverbal Learning Disorder";
            SetIndIfResultContains(8, "", "304239364", "", "", reslist);
            reslist = "Impaired,nonverbal";
            SetIndIfResultContains(8, "", "304239365", "", "", reslist);
            reslist = "Mouthing words,Gesturing,Writing,Augmentative alternative communication,Other";
            SetIndIfResultContains(8, "", "304239366", "", "", reslist);
            reslist = "Impaired";
            SetIndIfResultContains(8, "", "304239367", "", "", reslist);
            reslist = "Difficulty expressing needs,Difficulty understanding verbal information,No speech output,Inaccurate yes/no responses,Requires additional verbal cuing/selection to express message,Requires visual cuing to comprehend message,Requires simple and/or repeated sentences,Understandable with impaired speech,Uses incorrect or misplaced words";
            SetIndIfResultContains(8, "", "304239368", "", "", reslist);
            reslist = "Impaired";
            SetIndIfResultContains(8, "", "304239369", "", "", reslist);
            reslist = "Impaired";
            SetIndIfResultContains(8, "", "304239370", "", "", reslist);
            reslist = "Impaired";
            SetIndIfResultContains(8, "", "304239371", "", "", reslist);
            reslist = "Alternative communication,Difficulty understanding verbal information,Difficulty expressing needs,Distorted misarticulated speech,No Speech Output";
            SetIndIfResultContains(8, "", "304239372", "", "", reslist);
            reslist = "Communication board,Electronic device,Handwritten words,Pictures,Pictures with text,Sign language,Social stories,Typed words,Other";
            SetIndIfResultContains(8, "", "304239373", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(8, "", "304239374", "", "", reslist);
            reslist = "Spanish,Arabic,Cantonese,French,French Creole,Khmer,Mandarin,Portuguese,Russian,Somali,American Sign Language,Other";
            SetIndIfResultContains(8, "", "304239375", "", "", reslist, SearchDepth.SearchSinceAdmission);
            reslist = "Spanish,Arabic,Cantonese,French,French Creole,Khmer,Mandarin,Portuguese,Russian,Somali,Other";
            SetIndIfResultContains(8, "", "304239376", "", "", reslist);
            reslist = "Impaired";
            SetIndIfResultContains(8, "", "304239377", "", "", reslist);
            reslist = "Cueing needed,Redirection required,Uses inappropriate words";
            SetIndIfResultContains(8, "", "304239378", "", "", reslist);
//            reslist = "Unable to express,Needs assistance";
            //reslist = "Needs assistance";
            //SetIndIfResultContains(8, "", "304239379", "", "", reslist);



            reslist = "Impaired vision,Anopthalmia,Blurred,Blind,Cloudy,Double Vision,Prosthesis,Tunnel Vision";
            SetIndIfResultContains(8, "", "304238113", "", "", reslist);
            reslist = "Impaired vision,Anopthalmia,Blurred,Blind,Cloudy,Double Vision,Prosthesis,Tunnel Vision";
            SetIndIfResultContains(8, "", "304238114", "", "", reslist);
            reslist = "Impaired hearing,Deaf";
            SetIndIfResultContains(8, "", "304238115", "", "", reslist);
            reslist = "Impaired hearing,Deaf";
            SetIndIfResultContains(8, "", "304238116", "", "", reslist);
            reslist = "No Voice,Trach,Aphonic,Speaking valve,Voice Amplifier,Muffled";
            SetIndIfResultContains(8, "", "304238117", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(8, "", "304238118", "", "", reslist);
            reslist = "Artificial airway,Expressive Aphasia,Receptive Aphasia,Delayed,Garbled,Incomprehensible,No Speech,Receptive Aphasia,Slurred";
            SetIndIfResultContains(8, "", "304238119", "", "", reslist);
            reslist = "Impaired,Neglect left,Neglect right";
            SetIndIfResultContains(8, "", "304238120", "", "", reslist);
            reslist = "Impaired";
            SetIndIfResultContains(8, "", "304238121", "", "", reslist);
            reslist = "No";
            SetIndIfResultContains(8, "", "304238122", "", "", reslist);
            reslist = "Impaired";
            SetIndIfResultContains(8, "", "304238123", "", "", reslist);
            reslist = "Apparent hearing loss,Known hearing loss,Impacts communication";
            SetIndIfResultContains(8, "", "304238124", "", "", reslist);
            reslist = "Augmentative Device,Nonverbal - Gestures,Nonverbal - Sign Language,Nonverbal -  Written";
            SetIndIfResultContains(8, "", "304238125", "", "", reslist);
            reslist = "Non-fluent Aphasia,Fluent Aphasia,Broca,Wernicke,Anomic Aphasia,Global Aphasia,Conduction Aphasia,Transcortical Motor Aphasia,Transcortical Sensory Aphasia,Mixed Transcortical Aphasia,Auditory processing deficit,Developmental Language Disorder,Nonverbal Learning Disorder";
            SetIndIfResultContains(8, "", "304238126", "", "", reslist);
            reslist = "Impaired";
            SetIndIfResultContains(8, "", "304238127", "", "", reslist);
            reslist = "Mouthing words,Gesturing,Writing,Augmentative alternative communication,Other";
            SetIndIfResultContains(8, "", "304238128", "", "", reslist);
            reslist = "Impaired";
            SetIndIfResultContains(8, "", "304238129", "", "", reslist);
            reslist = "Difficulty expressing needs,Difficulty understanding verbal information,No speech output,Inaccurate yes/no responses,Requires additional verbal cuing/selection to express message,Requires visual cuing to comprehend message,Requires simple and/or repeated sentences,Understandable with impaired speech,Uses incorrect or misplaced words";
            SetIndIfResultContains(8, "", "304238130", "", "", reslist);
            reslist = "Impaired";
            SetIndIfResultContains(8, "", "304238131", "", "", reslist);
            reslist = "Impaired";
            SetIndIfResultContains(8, "", "304238132", "", "", reslist);
            reslist = "Impaired";
            SetIndIfResultContains(8, "", "304238133", "", "", reslist);
            reslist = "Alternative communication,Difficulty understanding verbal information,Difficulty expressing needs,Distorted misarticulated speech,No Speech Output";
            SetIndIfResultContains(8, "", "304238134", "", "", reslist);
            reslist = "Communication board,Electronic device,Handwritten words,Pictures,Pictures with text,Sign language,Social stories,Typed words,Other";
            SetIndIfResultContains(8, "", "304238135", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(8, "", "304238136", "", "", reslist);
            reslist = "Spanish,Arabic,Cantonese,French,French Creole,Khmer,Mandarin,Portuguese,Russian,Somali,American Sign Language,Other";
            SetIndIfResultContains(8, "", "304238137", "", "", reslist);
            reslist = "Spanish,Arabic,Cantonese,French,French Creole,Khmer,Mandarin,Portuguese,Russian,Somali,Other";
            SetIndIfResultContains(8, "", "304238138", "", "", reslist);
            reslist = "Impaired";
            SetIndIfResultContains(8, "", "304238139", "", "", reslist);
            reslist = "Cueing needed,Redirection required,Uses inappropriate words";
            SetIndIfResultContains(8, "", "304238140", "", "", reslist);
            reslist = "Unable to express,Needs assistance";
            SetIndIfResultContains(8, "", "304238141", "", "", reslist);

            reslist = "Impaired,Central field,Right field cut,Left field cut";
            reslist += ",Upper field cut,Lower field cut,Double vision";
            SetIndIfResultContains(8, "", "3041314601,3041314501", "", "", reslist);

            string desc_found = "";
            if (OrderInProgress("134750", out desc_found))
                SetInd(8, "Order in effect: " + desc_found);
            
        }

        private bool CheckLanguage()
        {
            //            "Via ADT Intterface PID-15:
            //Trigger Indicator #8 Communication if this field's value is anything other than: (blank), eng, english, unk, dc, or unv."
            //"Via ADT Intterface in PID-15:
            //Trigger Indicator #8 Communication if this field's value is anything other than: (blank), Eng, UNK, dc, or  unv."
            bool triggered = false;
            int sep_idx = _pat.lang_lvc.IndexOf(";;");
            //Program.VerboseAudit("Language=" + _pat.lang_lvc + " sep_idx="+sep_idx);
            if (sep_idx <= 0) return triggered;
            string pref_lang = _pat.lang_lvc.Substring(0, sep_idx);
            string writ_lang = _pat.lang_lvc.Substring(sep_idx + 2, _pat.lang_lvc.Length - sep_idx - 2);
            int interp_idx = _pat.lang_lvc.LastIndexOf(";;");
            string interp_str = _pat.lang_lvc.Substring(interp_idx+2);
            // ex:  KOR;;KOR;;INTERP 
            bool pref_ok = (pref_lang.Trim() == "" || pref_lang.Trim().ToUpper().StartsWith("ENG")
                || pref_lang.Trim().ToUpper().StartsWith("DC")
                || pref_lang.Trim().ToUpper().StartsWith("UNK")
                || pref_lang.Trim().ToUpper().StartsWith("UNV"));
            bool writ_ok = (writ_lang.Trim() == "" || writ_lang.Trim().ToUpper().StartsWith("ENG")
                || writ_lang.Trim().ToUpper().StartsWith("DC")
                || writ_lang.Trim().ToUpper().StartsWith("UNK")
                || writ_lang.Trim().ToUpper().StartsWith("UNV"));
            bool interp_need = (interp_str.StartsWith("INTERP"));
            if (!(pref_ok && writ_ok))
            {
                SetInd(8, "Preferred language=" + pref_lang + " Written=" + writ_lang);
                triggered = true;
            }
            if (interp_need)
            {
                SetInd(8, "Interpreter needed=" + _pat.lang_lvc);
                triggered = true;
            }

            return triggered;
        }

        //private void CheckInterpreterItems()
        //{  //look back = 36 hrs
        //    string code;
        //    string descript;
        //    DateTime evdt;
        //    string res;
        //    bool done = false;

        //    string sql = "select ci.code,ci.description,ci.event_datetime,ci.result from chart_item as ci";
        //    sql += " where ci.encounter_id=" + _pat.encounter_id;
        //    sql += " and ((ci.code like '%9991733444441%' and (ci.result like '%in person%' or ci.result like '%ipad%' or ci.result like '%phone%' or ci.result like '%other%'))";
        //    sql += " or (ci.code like '%9993040109069%' and (ci.result like '%Admission%' or ci.result like '%Assessment%' or ci.result like '%consent%' or ci.result like '%discharge instructions%' or ci.result like '%education%' or ci.result like '%plan of care%' or ci.result like '%other%'))";
        //    sql += " or (ci.code like '%9993040109071%' and (ci.result like '%Hospital/clinic approved on site interpreter%' or ci.result like '%Telephone interpreter%' or ci.result like '%Video remote interpreter%' or ci.result like '%other%'))";
        //    sql += " or (ci.code like '%9993040108551%')";
        //    sql += " or (ci.code like '%9990007070581%' and (ci.result like '%Braille%' or ci.result like '%Communication board%' or ci.result like '%hearing aid%' or ci.result like '%interpreter%' or ci.result like '%sign language%'))";
        //    sql += " or (ci.code like '%9993040108693%' and (ci.result like '%yes%')))";
        //    sql += " and ci.event_datetime >=" + _pat.pull_finish.AddHours(-36).ToString() + "";
        //    sql += " order by ci.event_datetime desc";
        //    Program.VerboseAudit(sql);
        //    var db2 = PFSDBUtility.NewSqlConnection();
        //    var cmd = new SqlCommand(sql, db2);
        //    SqlDataReader dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
        //    while (dr2.Read() && !done)
        //    {
        //        code = PFSDBUtility.DBToString(dr2["CODE"]);
        //        descript = PFSDBUtility.DBToString(dr2["DESCRIPTION"]);
        //        evdt = PFSDBUtility.DBToDateTime(dr2["EVENT_DATETIME"]);
        //        res = PFSDBUtility.DBToString(dr2["RESULT"]);
        //        SetInd(8, "Found interpreter within 36 hrs: " + code + "/" + res + "/" + evdt.ToString());
        //        done = true;
        //    }

        //}

        //private bool AllOriented()
        //{
        //    int ct = 0;

        //    var query = StartNewQuery(SearchDepth.SearchSince13Hrs);    // add custom time range below
        //    query = query.Where(e => e.CODE.StartsWith("9990000301870"));
        //    query = query.Where(e => (e.RESULT.ToLower().StartsWith("oriented to person") || e.RESULT.ToLower().Contains(";oriented to person"))
        //    && (e.RESULT.ToLower().StartsWith("oriented to place") || e.RESULT.ToLower().Contains(";oriented to place"))
        //    && (e.RESULT.ToLower().StartsWith("oriented to time") || e.RESULT.ToLower().Contains(";oriented to time")));
        //    ct = query.Count();
        //    if (ct > 0)
        //        Program.VerboseAudit("All 3 Orientation found: " + query.Count());
        //    return (ct > 0);
        //}


        private void Check_9()
        {
            string reslist;
            bool s2, s3, s4;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("9. Cognitive Support");
            Program.VerboseAudit("---------------");

            exclude_periop_data = true;

            if (_pat.unit_name.ToUpper() == "SRH 7")
                SetInd(9, "Patient is in unit SRH 7.");
                
            reslist = "Hallucination,Delusional,Confused";
            SetIndIfResultContains(9, "", "304239380", "", "", reslist);
            reslist = "Hallucination,Delusional,Confused";
            SetIndIfResultContains(9, "", "304239381", "", "", reslist);
            reslist = "Confused";
            SetIndIfResultContains(9, "", "304239382", "", "", reslist);
            reslist = "Requires frequent redirection,Unable to redirect";
            SetIndIfResultContains(9, "", "304239383", "", "", reslist);
            reslist = "1=cannot do serial additions or is uncertain about date,2=disoriented for data by no more than 2 calendar days,3=disoriented for date by more than 2 calendar days,4=disoriented for place or person";
            SetIndIfResultContains(9, "", "304239384", "", "", reslist);
            reslist = "Forgetful";
            SetIndIfResultContains(9, "", "304239385", "", "", reslist);
            reslist = "Positive";
            SetIndIfResultContains(9, "", "304239386", "", "", reslist);
            reslist = "Positive";
            SetIndIfResultContains(9, "", "304239387", "", "", reslist);
            reslist = "Confused";
            SetIndIfResultContains(9, "", "304239388", "", "", reslist);
            reslist = "Yes";
            SetIndIfResultContains(9, "", "304239389", "", "", reslist);
            reslist = "Alzheimer,Delusions,Dementia,Hallucinations,Schizophrenia,Other";
            SetIndIfResultContains(9, "", "304239390", "", "", reslist);
            reslist = "Yes";
            SetIndIfResultContains(9, "", "304239391", "", "", reslist);
            reslist = "2=Needed some help,1=Dependent";
            SetIndIfResultContains(9, "", "304239392", "", "", reslist);
            reslist = "2=Confused";
            SetIndIfResultContains(9, "", "304239393", "", "", reslist);
            reslist = "1=Yes";
            SetIndIfResultContains(9, "", "304239394", "", "", reslist);

        }


        private void Check_10_11()
        {
            string reslist;
            bool is_peds = false;
            //int ct;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("10. Behavior/Emotional Management");
            Program.VerboseAudit("11. Behavior/Emotional Mgmt - q 1 Hour");
            Program.VerboseAudit("---------------");
            
            exclude_periop_data = false;
            is_peds = (_pat.age <= 14.0);

            bool behq1 = false;
            reslist = "Anxious,Affect inconsistent with mood,Aggressive physically,Aggressive verbally,Flat affect,Non-compliant,Non-supportive,Tearful,Uncooperative";
            behq1 |= SetIndIfResultContains(10, "", "304239399", "", "", reslist);
            reslist = "Anxious,Affect inconsistent with mood,Aggressive physically,Aggressive verbally,Flat affect,Non-compliant,Non-supportive,Tearful,Uncooperative";
            behq1 |= SetIndIfResultContains(10, "", "304239400", "", "", reslist);

            string found_what;
            rassctLTzero = CountResultInList("", "304239326", "","","-3,-4,-5",SearchDepth.SearchDefault,CountMode.CountAll,true,out found_what);
            rassct12 = CountResultInList("", "304239326", "", "", "+1,+2", SearchDepth.SearchDefault, CountMode.CountAll, true, out found_what);
            rassct34 = CountResultInList("", "304239326", "", "", "+3,+4", SearchDepth.SearchDefault, CountMode.CountAll, true, out found_what);
            int ctGTzero = rassct12 + rassct34;

            //reslist = "-3,-4,-5";
            //            if (Exists("", "304239326", "", "", reslist))
            if (rassctLTzero > ctGTzero)
            {
                Program.VerboseAudit("Excluding Behavior due to majority of RASS scores in (-3,-4,-5). Count of scores (-3,-4,-5)=" + rassctLTzero + "; Count of scores (1-4)=" + ctGTzero);
                return;
            }

            reslist = "Agitated,Difficult to console,Irritable,Restless";
            if (Exists("", "304010007801", "", "", reslist))
            {
                reslist = "";
                bool b1 = SetIndIfResultContains(10, "", "3040100082", "", "", reslist);
                if (b1)
                    if (IsQ4Freq()) //q1 attestation q4
                        SetInd(11, "Infant Behavior found PLUS q1 Attestation at least q4 hrs.");
            }

            reslist = "Agitated,Anxious,Angry,Aggressive physically,Aggressive verbally,Combative,Distracted,Flat affect,Impulsive,Inappropriate,Labile,Sad,Non-compliant,Tearful,Uncooperative,Withdrawn,Disinhibited,Fearful,Restless";
            behq1 |= SetIndIfResultContains(10, "", "3042393971", "", "", reslist);
            if (behq1)
            {
                if (IsQ4Freq()) //needs to be q4
                    SetInd(11, "Behavior found PLUS q1 Attestation at least q4 hrs.");
            }

            reslist = "Anger,Anxiety,Denial,Depression,Fearful,Non-verbal,Sadness";
            SetIndIfResultContains(10, "", "304239401", "", "", reslist);
            reslist = "Anger,Anxiety,Denial,Depression,Fearful,Non-verbal,Sadness";
            SetIndIfResultContains(10, "", "304239402", "", "", reslist);

            reslist = "Agitation,Anxiety,Combative,Irritable,Wandering";
            SetIndIfResultContains(10, "", "304239385", "", "", reslist);
            //reslist = "";
            //SetIndIfResultContains(10, "", "304239403", "", "", reslist);

            //reslist = "+1,+2";
            //SetIndIfResultContains(10, "", "304239326", "", "", reslist);
            //reslist = "+3,+4";
            //SetIndIfResultContains(11, "", "304239326", "", "", reslist);
            if (ctGTzero >= rassctLTzero)
            {
                if (rassct12 > 0 && rassct12 > rassct34)
                {
                    SetInd(10, "Count of RASS Scores (+1,+2)=" + rassct12 + " is more than (+3,+4)=" + rassct34);
                }
                if (rassct34 > 0 && rassct34 >= rassct12)
                {
                    SetInd(11, "Count of RASS Scores (+3,+4)=" + rassct34 + " is more than (+1,+2)=" + rassct12);
                }
            }

            reslist = "4,5";
            SetIndIfResultContains(10, "", "304239404", "", "", reslist);
            reslist = "6,7";
            SetIndIfResultContains(11, "", "304239404", "", "", reslist);

            reslist = "Agitated/restless,Verbally abusive,Tearful,Hallucination,Delusional";
            SetIndIfResultContains(11, "", "304239380", "", "", reslist);

            reslist = "Agitated/restless,Verbally abusive,Tearful,Hallucination,Delusional";
            SetIndIfResultContains(10, "", "304239381", "", "", reslist);

            reslist = "";
            //SetIndIfResultContains(10, "", "304239405", "", "", reslist);
            reslist = "Agitated,Combative,Uncooperative";
            SetIndIfResultContains(10, "", "304239388", "", "", reslist);
            reslist = "Self,Physical,Verbal,Other";
            SetIndIfResultContains(10, "", "304239406", "", "", reslist);
            reslist = "1=Patient reports increasing irritability or anxiousness,2=Patient obviously irritable anxious,4=Patient so irritable or anxious that participation in the assessment is difficult";
            SetIndIfResultContains(10, "", "304239407", "", "", reslist);
            //reslist = "Newborn separated from Mom,Not involved in Newborn care";
            //SetIndIfResultContains(10, "", "304239408", "", "", reslist);
            reslist = "Anxious,nervous,Depressed,Irritable,Fearful,Lack of energy or fatigued";
            SetIndIfResultContains(10, "", "304239409", "", "", reslist);
            reslist = "Present";
            SetIndIfResultContains(10, "", "304239410", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(10, "", "304239411", "", "", reslist);
            reslist = "Impaired,Impulsive";
            SetIndIfResultContains(10, "", "304239412", "", "", reslist);
            reslist = "1=present";
            SetIndIfResultContains(10, "", "304239413", "", "", reslist);
            reslist = "1=present";
            SetIndIfResultContains(10, "", "304239414", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(10, "", "304239415", "", "", reslist);
            reslist = "Swearing,Threatening,Screaming at staff,Screaming at other patient/resident,Yelling out,Yells at staff or others,Racial slurs,Other";
            SetIndIfResultContains(10, "", "304239416", "", "", reslist);
            reslist = "Hitting,Kicking,Punching,Striking out,Slapping,Throwing objects,Physical abusive to self/others,Biting,Pinching,Scratches,Sexual abusive,Other";
            SetIndIfResultContains(10, "", "304239417", "", "", reslist);
            reslist = "Exhibits sexual behavior or disrobes in public,Performs self-abusive acts,Smears feces,Spitting,Steals other patient's food,Throws food,Other";
            SetIndIfResultContains(10, "", "304239418", "", "", reslist);
            reslist = "Alzheimer’s,Anxiety disorder,Delusions,Dementia,Depression,Hallucinations,Schizophrenia,Other";
            SetIndIfResultContains(10, "", "304239419", "", "", reslist);
            reslist = "Angry,Ambivalent,Depressed,Despair,Fearful,Irritable,Labile,Sad,Tearful,Uncooperative,Withdrawn";
            SetIndIfResultContains(10, "", "304239420", "", "", reslist);
            reslist = "Agitated/restless,Anxious,Combative,Compulsive,Delusional,Destructive to self/others,Hallucination/paranoia,Non-compliant,Physically abusive,Resistance to care,Rammaging others belonging,Socially inappropriate,Verbally abusive,Wandering behavior,Other";
            SetIndIfResultContains(10, "", "304239421", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(11, "", "304239422", "", "", reslist);
            reslist = "Irritable,Jittery,Shrill cry";
            SetIndIfResultContains(11, "", "304239423", "", "", reslist);

            
            SetIndIfResultContains(10, "", "30423104120007", "", "", "Yes");
            SetIndIfResultContains(10, "", "104120008", "", "", "Yes");
            SetIndIfResultContains(10, "", "30423104120009", "", "", "Yes");

            SetIndIfResultContains(10, "", "30423104120010", "", "", "1,2");
            SetIndIfResultContains(11, "", "30423104120010", "", "", "3");
            SetIndIfResultContains(10, "", "304239452", "", "", "3,4");
            
        }

        private int CountLTZero()
        {
            int ct = 0;

            return ct;
        }
        private int CountGTZero(out int ct12, out int ct34)
        {
            int ct = 0;
            ct12 = 0;
            ct34 = 0;


            ct = ct12 + ct34;
            return ct;
        }

        private bool IsQ4Freq()
        {
            string codelist;
            bool ret = false;
            List<gBucket> buckets;

            SetBucketSize(60);
            buckets = new List<gBucket>();
            codelist = "304239398";
            AddBuckets(buckets, "", codelist, "", "", "Yes");
            ret = AnalyzeBuckets(buckets, 15, 60, "indicator 11 attestation", false);

            return (ret);

        }
        private bool IsQ1Freq()
        {
            bool ret = false;


            return ret;
        }

        // (this isn't really a Q1h count -- it is just a count) Make this a q1hr count
        //private int ReturnQ1HrCount(string code_list, string result_list)
        //{
        //    var buckets = new List<gBucket>();
        //    SetBucketSize(60);
        //    AddBuckets(buckets, "", code_list, "", "", result_list);
        //    return CountBuckets(buckets);
        //}

        //private bool IsQ1Hour(int count)
        //{
        //    return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q1H);
        //}
        //private bool IsQ2Hour(int count)
        //{
        //    return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q2H);
        //}

        // Use this if counting safety buckets
        //private void CheckSafety(int count, string desc)
        //{
        //    if (_inds[13].is_checked) return;             //skip if highest already checked
        //    if (count == 0) return;
        //    switch (FreqForCount(_pat.los_hours, count))
        //    {
        //    case Frequencies.Q30M:
        //        SetInd(13, desc + " q30min");
        //        break;
        //    case Frequencies.Q1H:
        //    case Frequencies.Q2H:
        //        SetInd(12, desc + " q2h");
        //        break;
        //    default:
        //        Program.VerboseAudit(desc + ": " + count + " in " + Math.Round(_pat.los_hours) + " hours is not enough");
        //        break;
        //    }
        //}

        private void FindLatest(string code, string reslist, out string res, out DateTime evdt)
        {
            res = "???";
            evdt = DateTime.MinValue;

            var query = StartNewQuery(SearchDepth.SearchSince16Hrs);
            query = AndItemFilter(query, "", code, "", "", reslist);
            CHART_ITEM ch = query.OrderByDescending(e => e.EVENT_DATETIME).FirstOrDefault();
            if (ch == null) return;
            res = ch.RESULT;
            evdt = ch.EVENT_DATETIME;
            Program.VerboseAudit("Latest result and time:" + ch.RESULT + ch.EVENT_DATETIME.ToString());

        }

        private void Check_12_13()
        {
            string reslist;
            bool is_peds = false;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("12. Safety Management - q 2 Hours");
            Program.VerboseAudit("13. Safety Management - q 30 Minutes");
            Program.VerboseAudit("---------------");

            if (g_lda6yo)
                SetInd(12, "FOUND Urinary catheter LDA for age < 6.");

            exclude_periop_data = false;
            is_peds = (_pat.age < 15.0);

            //if (_pat.age >= 7.0 && coma)
            //{
            //    Program.VerboseAudit("Patient has no LOC and age>6; suppressing indicators 12,13.");
            //    return;
            //}

            //reslist = "Positive";
            //if (Exists("", "304239395", "", "", reslist))
            //{
            //    reslist = "Every 1 hour,Every 2 hours";
            //    SetIndIfResultContains(12, "", "304239396", "", "", reslist);
            //    reslist = "Every 15 minutes";
            //    SetIndIfResultContains(13, "", "304239396", "", "", reslist);
            //}
            if (_pat.age < 6.0)
            {
                SetIndIfResultContains(13, "", "304239605", "", "", "");
                //reslist = "Blanchable Erythema,Blistered,Bleeding,Cool to touch,Crepitus";
                //reslist += ",Draining,Ecchymotic,Edema,Swelling,Erythema,Extravasated,Hematoma";
                //reslist += ",Hyperpigmentation,Hypopigmentation,Induration,Infiltrated,Leaking";
                //reslist += ",Macerate,Moist,Painful to touch,Phlebitis,Pink,Tender to touch";
                //reslist += ",Warm to touch";
                reslist = "";
                SetIndIfResultContains(12, "", "304239660", "", "", reslist);
                SetIndIfResultContains(12, "", "30423301550", "", "", "");
                SetIndIfResultContains(13, "", "304239189", "", "", "");
            }
            //else
            //{
            //    SetIndIfResultContains(12, "", "304239605", "", "", "");
            //}
            if (_pat.age < 4.0)
            {
                reslist = "";
                SetIndIfResultContains(12, "", "304239397&", "", "", reslist);
            }
            //The following 2 moved to Behavior due to RASS Score....
            //reslist = "+1,+2";
            //SetIndIfResultContains(12, "", "304239326", "", "", reslist);
            //reslist = "+4,+3";
            //SetIndIfResultContains(13, "", "304239326", "", "", reslist);
            if (rassct12 > 0)
            {
                if (rassct34 >= rassct12)
                {
                    if (rassctLTzero <= rassct34)
                        SetInd(13, "Majority of RASS scores are 3 or 4 count=" + rassct34 + " count of 1 or 2=" + rassct12+ " count of negative=" + rassctLTzero);
                    else
                        Program.VerboseAudit("Majority of RASS scores are negative count=" + rassctLTzero +  " count of 1 or 2=" + rassct12 + " count of 3 or 4=" + rassct34);
                }
                else if (rassctLTzero <= rassct12)
                {
                    SetInd(12, "Majority of RASS scores are 1 or 2 count=" + rassct12 + " count of 3 or 4 count=" + rassct34 + " count of negative=" + rassctLTzero);
                }
                else if (rassctLTzero > rassct12)
                {
                    Program.VerboseAudit("Majority of RASS scores are negative count=" + rassctLTzero + " count of 1 or 2=" + rassct12 + " count of 3 or 4=" + rassct34);
                }
            }
            else if (rassct34 > 0)
            {
                if (rassctLTzero <= rassct34)
                    SetInd(13, "Majority of RASS scores are 3 or 4 count=" + rassct34 + " count of 1 or 2=" + rassct12 + " count of negative=" + rassctLTzero);
            }




            reslist = "score > equal to 45";
            if (GetLatestResult("", "304239424", "", "", out reslist, SearchDepth.SearchSince24Hrs))
            {
                double v = reslist.Val();
                Program.VerboseAudit("Morse score=" + reslist + " val="+ v);
                    if (v >= 45)
                    {
                        Program.VerboseAudit("Morse score>=45 val=" + v);
                        reslist = "Bed alarm on,Chair alarm on,Frequent checks,Keep close to nurse station,Safety belt used for transfers,Tab alarm on,Security Alarm Bracelet";
                        SetIndIfResultContains(12, "", "304239465", "", "", reslist);
                        reslist = "Every 1 hour,Every 2 hours";
                        SetIndIfResultContains(12, "", "304239396", "", "", reslist);
                        reslist = "Every 15 minutes";
                        SetIndIfResultContains(13, "", "304239396", "", "", reslist);
                    }
            }
            else
                Program.VerboseAudit("Morse score returned false");

            reslist = "";
            SetIndIfResultContains(12, "", "304239425", "", "", reslist);
            reslist = "Start,Continued";
            SetIndIfResultContains(12, "", "304239426", "", "", reslist);
            reslist = "Start,Continued";
            SetIndIfResultContains(12, "", "304239427", "", "", reslist);
            reslist = "Start,Continued";
            SetIndIfResultContains(12, "", "304239428", "", "", reslist);
            reslist = "Start,Continued";
            SetIndIfResultContains(12, "", "304239429", "", "", reslist);
            reslist = "Start,Continued";
            SetIndIfResultContains(12, "", "304239430", "", "", reslist);
            reslist = "Start,Continued";
            SetIndIfResultContains(12, "", "304239431", "", "", reslist);
            reslist = "Start,Continued";
            SetIndIfResultContains(12, "", "304239432", "", "", reslist);
            reslist = "Start,Continued";
            SetIndIfResultContains(12, "", "304239433", "", "", reslist);
            reslist = "Start,Continued";
            SetIndIfResultContains(12, "", "304239434", "", "", reslist);
            reslist = "Start,Continued";
            SetIndIfResultContains(12, "", "304239435", "", "", reslist);
            reslist = "Start,Continued";
            SetIndIfResultContains(12, "", "304239436", "", "", reslist);
            reslist = "Start,Continued";
            SetIndIfResultContains(12, "", "304239437", "", "", reslist);
            reslist = "Start,Continued";
            SetIndIfResultContains(12, "", "304239438", "", "", reslist);
            reslist = "Start,Continued";
            SetIndIfResultContains(12, "", "304239439", "", "", reslist);
            reslist = "Start,Continued";
            SetIndIfResultContains(12, "", "304239440", "", "", reslist);
            reslist = "Start,Continued";
            SetIndIfResultContains(12, "", "304239441", "", "", reslist);
            reslist = "Start,Continued";
            SetIndIfResultContains(12, "", "304239442", "", "", reslist);
            reslist = "Start,Continued";
            SetIndIfResultContains(12, "", "304239443", "", "", reslist);
            reslist = "Yes";
            SetIndIfResultContains(12, "", "304239444", "", "", reslist);
            reslist = "Yes";
            SetIndIfResultContains(12, "", "304239445", "", "", reslist);

            //reslist = "Q 1 hour,Q 2 hour";
            //SetIndIfResultContains(12, "", "304239447", "", "", reslist);
            reslist = "Yes";
            SetIndIfResultContains(12, "", "304239448", "", "", reslist);
            reslist = "Heightened surveillance,Peek a boo mitts";
            SetIndIfResultContains(12, "", "304239449", "", "", reslist);
            // = "Score of 2 or greater";
            SetIndIfResultBetween(12, "", "304239450", "", "", 2,999);
            reslist = "";
            SetIndIfResultContains(12, "", "304239451", "", "", reslist);
            reslist = "3=Present to moderate degree";
            SetIndIfResultContains(12, "", "304239452", "", "", reslist);
            reslist = "3=Present to moderate degree";
            SetIndIfResultContains(12, "", "304239453", "", "", reslist);
            reslist = "3=Present to moderate degree";
            SetIndIfResultContains(12, "", "304239454", "", "", reslist);
            reslist = "3=Present to moderate degree";
            SetIndIfResultContains(12, "", "304239455", "", "", reslist);
            reslist = "3=Present to moderate degree";
            SetIndIfResultContains(12, "", "304239456", "", "", reslist);
            reslist = "3=Present to moderate degree";
            SetIndIfResultContains(12, "", "304239457", "", "", reslist);
            reslist = "3=Present to moderate degree";
            SetIndIfResultContains(12, "", "304239458", "", "", reslist);
            reslist = "3=Present to moderate degree";
            SetIndIfResultContains(12, "", "304239459", "", "", reslist);
            reslist = "3=Present to moderate degree";
            SetIndIfResultContains(12, "", "304239460", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(13, "", "304239461", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(13, "", "304239462", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(13, "", "304239463", "", "", reslist);
            if (_pat.age < 6.0)
            {
                reslist = "";
                SetIndIfResultContains(12, "", "304239464", "", "", reslist);
            }
            reslist = "Gait belt,Lap belt with crotch belt,Lap belt without crotch belt,Other";
            SetIndIfResultContains(12, "", "304239466", "", "", reslist);
            reslist = "Yes";
            SetIndIfResultContains(12, "", "304239467", "", "", reslist);

            //            Placement of UVC
            //3042394421
            //3042394422
            //900700
            //900701

            //Removal of UVC
            //3042394424
            //3042394425
            //900702
            //900703
            CheckLDA("UVC","3042394421", "3042394422", "3042394424", "3042394425");
            CheckLDA("UVC", "900700", "900701", "900702", "900703");


            //reslist = "Every 15 minutes";
            //SetIndIfResultContains(13, "", "304239396", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(13, "", "304239446", "", "", reslist);
            reslist = "Agitated,Asleep,Awake,Combative,Confused,Cooperative,Uncooperative";
            SetIndIfResultContains(13, "", "304239388", "", "", reslist);
            //reslist = "Q 15 mins,Q 30 mins,1:1";
            //SetIndIfResultContains(13, "", "304239447", "", "", reslist);
            reslist = "4=Present to an extreme degree";
            SetIndIfResultContains(13, "", "304239452", "", "", reslist);
            reslist = "4=Present to an extreme degree";
            SetIndIfResultContains(13, "", "304239453", "", "", reslist);
            reslist = "4=Present to an extreme degree";
            SetIndIfResultContains(13, "", "304239454", "", "", reslist);
            reslist = "4=Present to an extreme degree";
            SetIndIfResultContains(13, "", "304239455", "", "", reslist);
            reslist = "4=Present to an extreme degree";
            SetIndIfResultContains(13, "", "304239456", "", "", reslist);
            reslist = "4=Present to an extreme degree";
            SetIndIfResultContains(13, "", "304239457", "", "", reslist);
            reslist = "4=Present to an extreme degree";
            SetIndIfResultContains(13, "", "304239458", "", "", reslist);
            reslist = "4=Present to an extreme degree";
            SetIndIfResultContains(13, "", "304239459", "", "", reslist);
            reslist = "4=Present to an extreme degree";
            SetIndIfResultContains(13, "", "304239460", "", "", reslist);
            reslist = "Obtunded,Eyes do not open to stimulus,Non-responsive to stimulation,Pharmacologically paralyzed";
            SetIndIfResultContains(13, "", "304239468", "", "", reslist);
            reslist = "Start,Continued";
            SetIndIfResultContains(13, "", "304239469", "", "", reslist);
            reslist = "Start,Continued";
            SetIndIfResultContains(13, "", "304239470", "", "", reslist);
            reslist = "Start,Continued";
            SetIndIfResultContains(13, "", "304239471", "", "", reslist);
            reslist = "Start,Continued";
            SetIndIfResultContains(13, "", "304239472", "", "", reslist);
            reslist = "Start,Continued";
            SetIndIfResultContains(13, "", "304239473", "", "", reslist);
            reslist = "Start,Continued";
            SetIndIfResultContains(13, "", "304239474", "", "", reslist);
            reslist = "Start,Continued";
            SetIndIfResultContains(13, "", "304239475", "", "", reslist);
            reslist = "Start,Continued";
            SetIndIfResultContains(13, "", "304239476", "", "", reslist);
            reslist = "Start,Continued";
            SetIndIfResultContains(13, "", "304239477", "", "", reslist);
            reslist = "Start,Continued";
            SetIndIfResultContains(13, "", "304239478", "", "", reslist);
            reslist = "Start,Continued";
            SetIndIfResultContains(13, "", "304239479", "", "", reslist);
            reslist = "Start,Continued";
            SetIndIfResultContains(13, "", "304239480", "", "", reslist);
            reslist = "Start,Continued";
            SetIndIfResultContains(13, "", "304239481", "", "", reslist);
            reslist = "Start,Continued";
            SetIndIfResultContains(13, "", "304239482", "", "", reslist);
            reslist = "Start,Continued";
            SetIndIfResultContains(13, "", "304239483", "", "", reslist);
            reslist = "Start,Continued";
            SetIndIfResultContains(13, "", "304239484", "", "", reslist);
            reslist = "Yes";
            SetIndIfResultContains(13, "", "304239485", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(13, "", "304239486", "", "", reslist);
            reslist = "Initiated,Continued";
            SetIndIfResultContains(13, "", "304239487", "", "", reslist);
            reslist = "Initiated,Continued";
            SetIndIfResultContains(13, "", "304239488", "", "", reslist);
            reslist = "Self,Physical,Verbal,Other";
            SetIndIfResultContains(13, "", "304239406", "", "", reslist);
            reslist = "Yes";
            SetIndIfResultContains(13, "", "304239489", "", "", reslist);
            reslist = "Yes";
            SetIndIfResultContains(13, "", "304239490", "", "", reslist);
            SetIndIfResultContains(13, "", "3042399073", "", "", "");
            SetIndIfResultContains(13, "", "3048700072", "", "", "");

            if (_pat.age < 6.0)
            { //chg 6/22/21
                SetIndIfResultContains(13, "", "3048716069", "", "", "");
                SetIndIfResultContains(13, "", "3048716135", "", "", "");
            }

            //Orders
            //41157,134349,35404,35398
            string desc_found = "";
            string code = "41157";
            if (OrderInProgress(code, out desc_found))
                SetInd(12, "Order in effect: " + desc_found);
            code = "134349";
            if (OrderInProgress(code, out desc_found))
                SetInd(12, "Order in effect: " + desc_found);
            code = "35404";
            if (OrderInProgress(code, out desc_found))
                SetInd(12, "Order in effect: " + desc_found);
            code = "35398";
            if (OrderInProgress(code, out desc_found))
                SetInd(12, "Order in effect: " + desc_found);
            // q30 min orders
            code = "134345";
            if (OrderInProgress(code, out desc_found))
                SetInd(13, "Order in effect: " + desc_found);
            code = "35400";
            if (OrderInProgress(code, out desc_found))
                SetInd(13, "Order in effect: " + desc_found);

        }

        private void CheckLDA(string LDA_type, string startdt_id, string starttm_id, string enddt_id, string endtm_id)
        {
            DateTime uvc_start = DateTime.MinValue;
            DateTime uvc_end;
            string linenum;
            string placecodeline;
            string removecodedateline;
            string removecodetimeline;
            LDA_type = LDA_type.ToUpper();
    //            MSH |^ ~\&| EPIC | PHS | AcuityPlus || 202006160041 || ORU ^ R01 | 22537760 | P | 2.6 EVN | R01 | 20200616004135 ||| 13003 ^ PELLETIER ^ EMILY ^ A.R.N.^^^^^ PHS ^^^^^ MGHMC PID | 1 || 6600113 ^^^ MGH ^ MRN || MENARD ^ STEVEN ^ R || 19590607 | M | MENARD ^ STEVE ^^|| 106 PINERIDGE DRIVE^^ WESTFIELD ^ MA ^ 01085 | HAMPDEN | (413)627 - 6791 || ENG | MARRIED / CU | NO PREFERENC | 3308814463 OBR | 1 || 8357686120200601000000 |||| 202006010000
    //OBX | 1 | DT | 3042394421 & 2714 ^ Placement Date - Urinary Catheter Temperature probe 16 Fr.^ ACUITYFDC || 20200616 |||||| F ||| 202006160041 || 13003 ^ PELLETIER ^ EMILY ^ A.R.N.^ OBX | 2 | TM | 3042394422 & 2714 ^ Placement Time - Urinary Catheter Temperature probe 16 Fr.^ ACUITYFDC || 00400000 |||||| F ||| 202006160041 || 13003 ^ PELLETIER ^ EMILY ^ A.R.N.^ OBX | 3 | ST | 304239316 & 2714 ^ Catheter Type - Urinary Catheter Temperature probe 16 Fr.^ ACUITYFDC || Temperature probe |||||| F ||| 202006160041 || 13003 ^ PELLETIER ^ EMILY ^ A.R.N.^
// Rejected  2020 - 06 - 16 12:32:54.167 source=1  type=4  category=4   R01 - Unsolicited Observation: UPGTTTWOTHREEFIVEFIVE, EPICMGH: Acct = 2000951172, MRN = 8627158, Type = 'O'
    //Placement of UVC
    //3042394421 date
    //3042394422 time
    //900700 date
    //900701 time

            //Removal of UVC
            //3042394424
            //3042394425
            //900702
            //900703
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);    // add custom time range below
            query = query.Where(e => e.CODE.StartsWith(startdt_id));
            if (LDA_type == "URINARY")
                query = query.Where(e => e.DESCRIPTION.ToUpper().Contains(LDA_type) || e.DESCRIPTION.ToUpper().Contains("FECAL"));
            else if (LDA_type == "GI TUBE")
                query = query.Where(e => e.DESCRIPTION.ToUpper().Contains(LDA_type) || e.DESCRIPTION.ToUpper().Contains("GJ TUBE"));
            else
                query = query.Where(e => e.DESCRIPTION.ToUpper().Contains(LDA_type));
            query = query.Where(e => e.ORDER_STATUS == null || e.ORDER_STATUS == "");
            query = query.OrderBy(e => e.EVENT_DATETIME);
            if (query.Count() == 0) return;
            bool  b_find_uvc_start = false;
            foreach (var item in query)
            { //we now have the date code for this line
              // get the line number for this LDA
                b_find_uvc_start = false;
                linenum = item.CODE.Substring(item.CODE.IndexOf("&")+1);
                Program.VerboseAudit("code=" + item.CODE + "  linenum of " + LDA_type + "=" + linenum);
                // assemble the parts for the time code
                placecodeline = starttm_id + "&" + linenum;
                Program.VerboseAudit("placecodeline for time=" + placecodeline);
                var q2 = StartNewQuery(SearchDepth.SearchSinceAdmission);
                q2 = q2.Where(e => e.CODE == placecodeline);
                q2 = q2.Where(e => e.EVENT_DATETIME == item.EVENT_DATETIME);
                //Program.VerboseAudit("placecodeline q2 code=" + placecodeline + " evdt="+item.EVENT_DATETIME);
                Program.VerboseAudit("placecodeline q2 count=" + q2.Count());
                if (q2.Count() == 0)
                {
                    Program.VerboseAudit("placement time not present. using midnight.");
                    if (IsValidDate(item.RESULT))
                        uvc_start = PFSUtility.ISOToDateTime(item.RESULT + "0000");
                    else
                    {
                        Program.VerboseAudit("Date is invalid:" + item.RESULT + " attempting to use eventdt...");
                        uvc_start = item.EVENT_DATETIME.Date;
                        Program.VerboseAudit("Date to be used:" + uvc_start);
                    }
                    Program.VerboseAudit(".LDA " + item.DESCRIPTION + " start=" + uvc_start.ToString());
                    removecodedateline = enddt_id + "&" + linenum;
                    var qremove = StartNewQuery(SearchDepth.SearchSinceAdmission);
                    qremove = qremove.Where(e => e.CODE == removecodedateline);
                    if (qremove.Count() == 0)
                    {
                        if (LDA_type == "UVC")
                        {
                            SetInd(13, "LDA " + item.DESCRIPTION + " placed: " + uvc_start.ToString());
                            SetInd(19, "LDA " + item.DESCRIPTION + " placed: " + uvc_start.ToString());
                        }
                        if (LDA_type == "Urinary".ToUpper())
                        {
                            g_toi4 = true;
                            if (_pat.age < 6.0)
                            {
                                SetInd(12, "LDA for age<6" + item.DESCRIPTION + " placed: " + uvc_start.ToString());
                                g_lda6yo = true;
                            }
                        }
                        if (LDA_type == "GI Tube".ToUpper())
                        {
                            g_gitube = true;
                        }
                    }
                    else
                    {
                        foreach (var r1 in qremove)
                        {
                            removecodetimeline = endtm_id + "&" + linenum;
                            var qremove2 = StartNewQuery(SearchDepth.SearchSinceAdmission);
                            qremove2 = qremove2.Where(e => e.CODE == removecodetimeline);
                            if (qremove2.Count() == 0)
                            {
                                if (IsValidDate(item.RESULT))
                                    uvc_end = PFSUtility.ISOToDateTime(r1.RESULT + "0000");
                                else
                                {
                                    Program.VerboseAudit("Date is invalid:" + r1.RESULT + " attempting to use eventdt.....");
                                    uvc_end = r1.EVENT_DATETIME.Date;
                                    Program.VerboseAudit("Date to be used:" + uvc_end);
                                }
                                if (_pat.pull_start >= uvc_end)
                                    DisableLDA(item.CODE);
                                else if (_pat.pull_start >= uvc_start)
                                {
                                    if (LDA_type == "UVC")
                                    {
                                        SetInd(13, "LDA-" + r1.DESCRIPTION + " placed: " + uvc_start.ToString());
                                        SetInd(19, "LDA-" + r1.DESCRIPTION + " placed: " + uvc_start.ToString());
                                    }
                                    if (LDA_type == "Urinary".ToUpper())
                                    {
                                        g_toi4 = true;
                                        if (_pat.age < 6.0)
                                        {
                                            SetInd(12, "LDA for age<6" + item.DESCRIPTION + " placed: " + uvc_start.ToString());
                                            g_lda6yo = true;
                                        }
                                    }
                                    if (LDA_type == "GI Tube".ToUpper())
                                    {
                                        g_gitube = true;
                                    }
                                }
                            }
                            else //there is an REMOVE TIME
                            {
                                foreach (var r2 in qremove2)
                                {
                                    if (IsValidDate(r1.RESULT))
                                        uvc_end = PFSUtility.ISOToDateTime(r1.RESULT + r2.RESULT.Substring(0, 4));
                                    else
                                    {
                                        Program.VerboseAudit("Date is invalid:" + r1.RESULT + " attempting to use eventdt.......");
                                        uvc_end = r1.EVENT_DATETIME.Date.AddHours(r2.RESULT.Substring(0, 2).Val()).AddMinutes(r2.RESULT.Substring(2, 2).Val());
                                        Program.VerboseAudit("Date to be used:" + uvc_end);
                                    }
                                    Program.VerboseAudit("LDA " + LDA_type + " removal=" + uvc_end.ToString());
                                    if (_pat.pull_start >= uvc_end)
                                        DisableLDA(item.CODE);
                                    else if (_pat.pull_start >= uvc_start)
                                    {
                                        if (LDA_type == "UVC")
                                        {
                                            SetInd(13, "LDA=" + r2.DESCRIPTION + " placed: " + uvc_start.ToString());
                                            SetInd(19, "LDA=" + r2.DESCRIPTION + " placed: " + uvc_start.ToString());
                                        }
                                        if (LDA_type == "Urinary".ToUpper())
                                        {
                                            g_toi4 = true;
                                            if (_pat.age < 6.0)
                                            {
                                                SetInd(12, "LDA for age<6" + item.DESCRIPTION + " placed: " + uvc_start.ToString());
                                                g_lda6yo = true;
                                            }

                                        }
                                        if (LDA_type == "GI Tube".ToUpper())
                                        {
                                            g_gitube = true;
                                        }
                                    }

                                }
                            }

                        }
                    } //else qremove count>0
            }
                else
                    b_find_uvc_start = true;//need to find uvc_start
                foreach (var i2 in q2)
                    {
                        if (b_find_uvc_start)
                        {
                        Program.VerboseAudit("Date " + item.RESULT + " Time=" + i2.RESULT.Substring(0, 4));
                        if (IsValidDate(item.RESULT))
                            uvc_start = PFSUtility.ISOToDateTime(item.RESULT + i2.RESULT.Substring(0, 4));
                        else
                        {
                            Program.VerboseAudit("Date is invalid:" + item.RESULT + " attempting to use eventdt.........");
                            uvc_start = item.EVENT_DATETIME.Date.AddHours(i2.RESULT.Substring(0, 2).Val()).AddMinutes(i2.RESULT.Substring(2, 2).Val());
                            Program.VerboseAudit("Date to be used:" + uvc_start);
                        }
                    }
                    Program.VerboseAudit("LDA "+i2.DESCRIPTION + " start=" + uvc_start.ToString());
                        removecodedateline = enddt_id + "&" + linenum;
                        var qremove = StartNewQuery(SearchDepth.SearchSinceAdmission);
                        qremove = qremove.Where(e => e.CODE == removecodedateline);
                        if (qremove.Count() == 0)
                        {
                            if (LDA_type == "UVC")
                            {
                                SetInd(13, "LDA " + i2.DESCRIPTION + " placed: " + uvc_start.ToString());
                                SetInd(19, "LDA " + i2.DESCRIPTION + " placed: " + uvc_start.ToString());
                            }
                            if (LDA_type == "Urinary".ToUpper())
                            {
                                g_toi4 = true;
                            if (_pat.age < 6.0)
                            {
                                SetInd(12, "LDA for age<6" + item.DESCRIPTION + " placed: " + uvc_start.ToString());
                                g_lda6yo = true;
                            }

                        }
                        if (LDA_type == "GI Tube".ToUpper())
                            {
                                g_gitube = true;
                            }
                        }
                        else
                        {
                        Program.VerboseAudit("c LDA removal date found: " + i2.DESCRIPTION + " start=" + uvc_start.ToString() + " remove date/line=" + removecodedateline);
                        foreach (var r1 in qremove)
                            {
                                removecodetimeline = endtm_id + "&" + linenum;
                                var qremove2 = StartNewQuery(SearchDepth.SearchSinceAdmission);
                                qremove2 = qremove2.Where(e => e.CODE == removecodetimeline);
                                if (qremove2.Count() == 0)
                                {
                                if (IsValidDate(r1.RESULT))
                                    uvc_end = PFSUtility.ISOToDateTime(r1.RESULT + "0000");
                                else
                                {
                                    Program.VerboseAudit("Date is invalid:" + r1.RESULT + " attempting to use eventdt.............");
                                    uvc_end = r1.EVENT_DATETIME.Date;
                                    Program.VerboseAudit("Date to be used:" + uvc_end);
                                }
                                if (_pat.pull_start >= uvc_end)
                                        DisableLDA(item.CODE);
                                    else if (_pat.pull_start < uvc_end)
                                    {
                                        if (LDA_type == "UVC")
                                        {
                                            SetInd(13, "LDA-" + r1.DESCRIPTION + " placed: " + uvc_start.ToString());
                                            SetInd(19, "LDA-" + r1.DESCRIPTION + " placed: " + uvc_start.ToString());
                                        }
                                        if (LDA_type == "Urinary".ToUpper())
                                        {
                                            g_toi4 = true;
                                        if (_pat.age < 6.0)
                                        {
                                            SetInd(12, "LDA for age<6" + item.DESCRIPTION + " placed: " + uvc_start.ToString());
                                            g_lda6yo = true;
                                        }

                                    }
                                    if (LDA_type == "GI Tube".ToUpper())
                                        {
                                            g_gitube = true;
                                        }
                                        if (_pat.pull_finish <= uvc_end)
                                            DisableLDA(item.CODE);
                                }
                            }
                                else //there is an REMOVE TIME
                                {
                                Program.VerboseAudit("d LDA removal time found: " + i2.DESCRIPTION + " start=" + uvc_start.ToString() + " remove time/line=" + removecodetimeline);
                                foreach (var r2 in qremove2)
                                    {
                                    Program.VerboseAudit("r2.RESULT: " + r2.RESULT + " and r1.RESULT="+r1.RESULT);
                                    if (IsValidDate(r1.RESULT))
                                        uvc_end = PFSUtility.ISOToDateTime(r1.RESULT + r2.RESULT.Substring(0,4));
                                    else
                                    {
                                        Program.VerboseAudit("Date is invalid:" + r1.RESULT + " attempting to use eventdt...................");
                                        uvc_end = r1.EVENT_DATETIME.Date;
                                        Program.VerboseAudit("Date to be used:" + uvc_end);
                                        uvc_end = r1.EVENT_DATETIME.Date.AddHours(r2.RESULT.Substring(0, 2).Val()).AddMinutes(r2.RESULT.Substring(2, 2).Val());
                                    }
                                    Program.VerboseAudit("LDA " + LDA_type + " removal=" + uvc_end.ToString());
                                        if (_pat.pull_start >= uvc_end)
                                            DisableLDA(item.CODE);
                                        else if (_pat.pull_start < uvc_end)
                                        {
                                            if (LDA_type == "UVC")
                                            {
                                                SetInd(13, "LDA=" + r2.DESCRIPTION + " placed: " + uvc_start.ToString());
                                                SetInd(19, "LDA=" + r2.DESCRIPTION + " placed: " + uvc_start.ToString());
                                            }
                                            if (LDA_type == "Urinary".ToUpper())
                                            {
                                                g_toi4 = true;
                                            if (_pat.age < 6.0)
                                            {
                                                SetInd(12, "LDA for age<6" + item.DESCRIPTION + " placed: " + uvc_start.ToString());
                                                g_lda6yo = true;
                                            }

                                        }
                                        if (LDA_type == "GI Tube".ToUpper())
                                            {
                                                g_gitube = true;
                                            }
                                            if (_pat.pull_finish <= uvc_end)
                                                DisableLDA(item.CODE);
                                    }

                                }
                                }

                            }
                        } //else qremove count>0
                    } //foreach i2 in q2
            }
            return;
        }
        private bool IsValidDate(string yyyymmdd)
        { //expecting yyyymmdd
            Program.VerboseAudit("IsValidDate inpt: " + yyyymmdd);
            bool ret = true;
            if (yyyymmdd.Length < 8)
                return false;
            if (yyyymmdd.Length > 8)
                yyyymmdd = yyyymmdd.Substring(0, 8);
            ret = (yyyymmdd.Substring(0, 4).Val() > 2000
                && yyyymmdd.Substring(4, 2).Val() >= 1
                && yyyymmdd.Substring(4, 2).Val() <= 12
                && yyyymmdd.Substring(6, 2).Val() >= 1
                && yyyymmdd.Substring(6, 2).Val() <= 31);
            return ret;
        }

        private void DisableLDA(string exact_code)
        {
            var db = PFSDBUtility.NewSqlConnection();
            string q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and code='" + exact_code + "'";
            SqlCommand cmd = new SqlCommand(q, db);
            cmd.ExecuteNonQuery();
            db.Close();

        }

        private void Check_14()
        {
            string desc_found = "", code;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("14. Isolation");
            Program.VerboseAudit("---------------");

            exclude_periop_data = false;
            string[] orderid_str = { "1","557", "134048","553","512285","3040000025001",
                "304000082801","304000083901","304000093101","7848","567512","577954"};

            foreach (string s in orderid_str)
            {
                if (OrderInProgress(s, out desc_found))
                    SetInd(14, "Order in effect: " + desc_found);
            }

        }

        //        private bool OLDOrderInProgress(string code, out string found_what)
        //        {
        //            bool ret = false;
        //            found_what = "";
        //            //look for latest code with NW and order_status != 'x'
        //            var db = PFSDBUtility.NewPfsDataContext();
        //            var query = from item in db.CHART_ITEMs
        //                        where (item.ENCOUNTER_ID == _pat.encounter_id)
        //                        where (item.CODE.ToUpper() == code)
        //                        where (item.EVENT_DATETIME < DateTime.Now)
        //                        where (item.ORDER_CONTROL.ToUpper() == "NW")
        //                        where (item.ORDER_STATUS == null || item.ORDER_STATUS == "")
        //                        orderby item.EVENT_DATETIME descending
        //                        select item;
        //            int ct = query.Count();
        ////            if (ct > 0) Program.VerboseAudit("order in progress: qct=" + ct);
        //            foreach (var itemA in query)
        //            {
        //                Program.VerboseAudit("order in progress: order_id=" + itemA.ORDER_ID);
        //                var db2 = PFSDBUtility.NewPfsDataContext();
        //                var query2 = from item2 in db2.CHART_ITEMs
        //                             where (item2.CODE.ToUpper() == code)
        //                             where (item2.ORDER_ID == itemA.ORDER_ID)
        //                             //where (item2.EVENT_DATETIME >= itemA.EVENT_DATETIME)
        //                             where (item2.ORDER_CONTROL.ToUpper() == "CA")
        //                             select item2;
        //                int ct2 = query2.Count();
        //                if (ct2 > 0)
        //                {
        //  //                  Program.VerboseAudit("order in progress: q2ct=" + ct2);
        //                    foreach (var x in query2)
        //                    {
        //                        if (x.EVENT_DATETIME >= _pat.pull_start)
        //                        {
        //                            ret = true;
        //                            found_what = "Order ID=" + itemA.ORDER_ID + ": " + itemA.DESCRIPTION + "; result=" + itemA.RESULT + "; range:" + itemA.EVENT_DATETIME.ToString() + " - " + x.EVENT_DATETIME.ToString();
        //                        }
        //                        else
        //                        {
        //                            DisableOrder(x.ORDER_ID);
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    ret = true;
        //                    found_what = "Order ID=" + itemA.ORDER_ID + ": " + itemA.DESCRIPTION + "; result=" + itemA.RESULT + "; starting:" + itemA.EVENT_DATETIME.ToString();
        //                    //Program.VerboseAudit("AUDIT ORDER:" + found_what);
        //                }
        //            }

        //            return ret;
        //        }

        private bool GetNWOrder(string CAordcode, DateTime CAts, DateTime CAevdt)
        { //Get NW order that wasn't saved because there was a previous CA order at same time.
            string sql;
            bool ret = false;
            //string room;
            //string dt;
            //bool readyforEDclass = true;
            //int car;
            //DateTime minlocdt = DateTime.MaxValue;

            string CAevdtISO = PFSUtility.DateTimeToISODateTime(CAevdt);

            sql = "select timestamp,";
            sql += " case when CHARINDEX('ORC|NW', source_text) > 0 then";
            sql += " substring(source_text, CHARINDEX('ORC|', source_text), 32) else null end as ORC,";

            sql += " case when CHARINDEX('OBR|', source_text) > 0 and CHARINDEX('|" + CAevdtISO + "', source_text) > 0 then";
            sql += " substring(source_text, CHARINDEX('OBR|', source_text), 200) else null end as OBR";

            sql += " from EVENT_LOG where TIMESTAMP between dateadd(minute,-10," + PFSDBUtility.SQLDateTime(CAts) + ") and dateadd(minute, 10," + PFSDBUtility.SQLDateTime(CAts) + ")";
            sql += " and (description like 'O01%') and description like '%" + _pat.acct + "%'";
            sql += " and event_source = 1 and event_type = 1 and event_category = 4";//NW item was not rejected
            Program.VerboseAudit(sql);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd2 = new SqlCommand(sql, db2);
            SqlDataReader dr2 = cmd2.ExecuteReader(System.Data.CommandBehavior.CloseConnection);

            string orcstr, obrstr, timestr;
            DateTime evdt=DateTime.MinValue;
            int seq = 0;
            while (dr2.Read())
            {
                orcstr = PFSDBUtility.DBToString(dr2["ORC"]);
                obrstr = PFSDBUtility.DBToString(dr2["OBR"]);
                if (orcstr.Trim() != "" && orcstr != null)
                {
                    //ORC|NW|669769268^EPC||3290947318
                    //OBR|1|669769268^EPC||40517^DIET^APEAP^^DIET||202004180752|202004180800

                    Program.VerboseAudit("Searching for unsaved order code: " + CAordcode + " at time " + CAevdt);
                    var arrorc = Program.SplitOnPipeAndPrepareElements(orcstr);
                    var arrobr = Program.SplitOnPipeAndPrepareElements(obrstr);
                    string ordid = "";
                    string ordctrl = "";
                    string ordcode = "";
                    string orddesc = "";

                    if (arrorc.GetUpperBound(0) >= 3 && arrobr.GetUpperBound(0) >= 4)
                    {
                        Program.VerboseAudit("Unsaved NW order ORC: " + orcstr);
                        Program.VerboseAudit("Unsaved NW order OBR: " + obrstr);

                        ordctrl = arrorc[1];
                        Program.VerboseAudit("ordctrl: " + ordctrl);
                        ordid = arrorc[2];
                        //VerboseAudit("ordid: " + ordid);
                        int ordidpos = ordid.IndexOf("^");
                        if (ordidpos > 0) ordid = ordid.Substring(0, ordidpos);
                        //VerboseAudit("ordidfinal: " + ordid);

                        ordcode = arrobr[4];
                        //VerboseAudit("ordcode: " + ordcode);
                        int ordcodepos = ordcode.IndexOf("^");
                        if (ordcodepos > 0)
                        {
                            orddesc = ordcode.Substring(ordcodepos + 1);
                            ordcode = ordcode.Substring(0, ordcodepos);
                        }
                        //VerboseAudit("ordcodefinal: " + ordcode);

                        if (ordcode.Trim() == CAordcode.Trim())
                        {
                            ret = true;
                            evdt = CAevdt.AddMinutes(-1);
                            Program.VerboseAudit("Adding: orderid=" + ordid + " ctrl=" + ordctrl + " code=" + ordcode + " evdt=" + evdt + " desc=" + orddesc);
                            if (arrorc.GetUpperBound(0) >= 2)
                            {
                                using (var db = PFSDBUtility.NewSqlConnection())
                                {
                                    seq++;
                                    //evdt = PFSUtility.ISOToDateTime(timestr);
                                    Program.VerboseAudit("Evdt=" + evdt.ToString());
                                    string q = "INSERT INTO chart_item (encounter_id,event_datetime,code,description,timestamp,sequence,unit_id,order_id,order_control)";
                                    q += " select @encid, @evdt, @code, @desc, @ts, @seq, @unit, @oid, @octrl";
                                    q += " where not exists (select encounter_id,code,event_datetime,unit_id,sequence from chart_item";
                                    q += " where encounter_id=" + _pat.encounter_id.ToString() + " and code='" + ordcode + "' and event_datetime='" + evdt.ToString() + "' and unit_id=-1 and sequence=" + seq + ")";
                                    //string q = "INSERT INTO chart_item (encounter_id,event_datetime,code,description,timestamp,sequence,unit_id,order_id) values (@encid, @evdt, @code, @desc, @ts,@seq,@unit,@oid)";
                                    SqlCommand cmd = new SqlCommand(q, db);
                                    cmd.Parameters.AddWithValue("@encid", _pat.encounter_id);
                                    cmd.Parameters.AddWithValue("@evdt", evdt);
                                    cmd.Parameters.AddWithValue("@code", ordcode);
                                    cmd.Parameters.AddWithValue("@desc", orddesc);
                                    cmd.Parameters.AddWithValue("@ts", DateTime.Now);
                                    cmd.Parameters.AddWithValue("@seq", seq);
                                    cmd.Parameters.AddWithValue("@unit", -1);
                                    cmd.Parameters.AddWithValue("@oid", ordid);
                                    cmd.Parameters.AddWithValue("@octrl", ordctrl);
                                    cmd.ExecuteNonQuery();
                                }

                            }
                        }// if codes match: add chart item 1 minute earlier
                    } // if arrays upper bounds are large enough
                } // if the orc and obr strings are not null
            } //dr read
            //dr2.Close();
            db2.Close();
            return ret;
        }


        private bool OrderInProgress(string code, out string found_what)
        {
            bool ret = false;
            found_what = "";
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = query.Where(e => e.CODE.ToUpper() == code
                                  && e.EVENT_DATETIME < loc_out
                                  && e.ORDER_CONTROL.ToLower() == "nw"
                                  && (e.ORDER_STATUS == "" || e.ORDER_STATUS == null));
            query = query.OrderByDescending(e => e.EVENT_DATETIME);

            //query = query.Where(e => e.CODE.ToUpper() == code);
            //query = query.Where(e => e.EVENT_DATETIME < DateTime.Now);
            //query = query.Where(e => e.ORDER_CONTROL.ToUpper() == "NW");
            //query = query.Where(e => e.ORDER_STATUS == "");
            //query = query.OrderByDescending(e => e.EVENT_DATETIME);

            int count = query.Count();
            //Program.VerboseAudit("count order in progress=" + count);

            foreach (var itemA in query)
            {
                var query2 = StartNewQuery(SearchDepth.SearchSinceAdmission);
                query2 = query2.Where(e => e.CODE.ToUpper() == code);
                query2 = query2.Where(e => e.ORDER_ID == itemA.ORDER_ID);
                query2 = query2.Where(e => e.ORDER_CONTROL.ToLower() == "ca");
                int ct2 = query2.Count();

                if (ct2 > 0)
                {
                    Program.VerboseAudit("order in progress: order_id=" + itemA.ORDER_ID + " _pat.pull_start=" + _pat.pull_start + " ct2="+ct2);
                    //                  Program.VerboseAudit("order in progress: q2ct=" + ct2);
                    foreach (var x in query2)
                    {
                        Program.VerboseAudit("x.event_datetime=" + x.EVENT_DATETIME);
                        if (x.EVENT_DATETIME >= _pat.pull_start)
                        {
                            ret = true;
                            found_what = "Order ID=" + itemA.ORDER_ID + ": " + itemA.DESCRIPTION + "; result=" + itemA.RESULT + "; range:" + itemA.EVENT_DATETIME.ToString() + " - " + x.EVENT_DATETIME.ToString();
                        }
                        else
                        {
                            DisableOrder(x.ORDER_ID);
                        }
                        ret |= GetNWOrder(code, x.TIMESTAMP, x.EVENT_DATETIME);
                    }
                }
                else
                {
                    ret = true;
                    found_what = "Order ID=" + itemA.ORDER_ID + ": " + itemA.DESCRIPTION + "; result=" + itemA.RESULT + "; starting:" + itemA.EVENT_DATETIME.ToString();
                    //Program.VerboseAudit("AUDIT ORDER:" + found_what);
                }
            }


            return ret;
        }

        private void DisableOrder(string ordid)
        {
            //update ORDER_STATUS = 'X' for _pat.encounter_id and ordid
            var db = PFSDBUtility.NewSqlConnection();
            string q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and order_id='" + ordid + "'";
            SqlCommand cmd = new SqlCommand(q, db);
            cmd.ExecuteNonQuery();
            db.Close();

        }

        private void DisableSitter(string code, DateTime evdt, bool use_proc_start)
        {
            //update ORDER_STATUS = 'X' for _pat.encounter_id and ordid
            var db = PFSDBUtility.NewSqlConnection();
            //string q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and code='" + code + "' and (result='initiated' or result='continued') and event_datetime<=" + PFSDBUtility.SQLDateTime(evdt);
            string q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and code='" + code + "' and event_datetime<=" + PFSDBUtility.SQLDateTime(evdt);
            if (use_proc_start)
                q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and code='" + code + "' and procedure_start is not null and procedure_start<=" + PFSDBUtility.SQLDateTime(evdt);
            SqlCommand cmd = new SqlCommand(q, db);
            cmd.ExecuteNonQuery();
            db.Close();

        }
        private void DisableSitter(string code, DateTime evdt)
        {
            DisableSitter(code, evdt, false);
        }

        //private void CheckAssessment(int count, string desc)
        //{
        //    //if (_inds[18].is_checked) return;          //skip if highest already checked
        //    if (count == 0) return;                    //skip if none

            //    // This should work the same as the original code:
            //    switch (FreqForCount(_pat.los_hours, count))
            //    {
            //        case Frequencies.Q30M:
            //            SetInd(18, desc);
            //            break;
            //        case Frequencies.Q1H:
            //            SetInd(17, desc);
            //            break;
            //        case Frequencies.Q2H:
            //            SetInd(16, desc);
            //            break;
            //        case Frequencies.Q4H:
            //            SetInd(15, desc);
            //            break;
            //        default:
            //            break;
            //    }

            //}

        private void ShowBuckets(List<gBucket> buckets)
        {
            string s = "";
            foreach (var e in buckets)
            {
                s = "(" + e.bucket + "" + e.code + "" + e.evdt.ToString() + ")";
                Program.VerboseAudit("Assessmt item:=" + s);
            }
        }


        private void Check_15_16_17_18()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("15. Assessment q4h");
            Program.VerboseAudit("16. Assessment q2h");
            Program.VerboseAudit("17. Assessment q1h");
            Program.VerboseAudit("18. Assessment q30min");
            Program.VerboseAudit("---------------");

            //For the assessments orders SpO2 or Telemetry see orders tab,
            // the asslevel can only go up to q2 with only spo2 charted
            // Would need other VS to get to asslevel q1 or higher.
            SetBucketSize(60);

            exclude_periop_data = true;

            Program.sttimer();

            string desc_found;
            string code = "41047";
            if (OrderInProgress(code, out desc_found))
            {
                Program._t3 = Program.entimer("t3order in assess15-18=", Program._t3);
                List<gBucket> buckets;
                Program.sttimer();
                buckets = new List<gBucket>();
                string codelist = "304239598,304239599";
                AddBuckets(buckets, "", codelist, "", "");
                if (AnalyzeBuckets(buckets, 16, 60, "Check Heart Rate freq", false))
                    SetInd(16, "ECG Monitoring Order in effect with HeartRate at least q2: " + desc_found);
                Program._t6 = Program.entimer("t6 in assess15-18=", Program._t6);
            }

            code = "3040000070001";
            if (OrderInProgress(code, out desc_found))
            {
                List<gBucket> buckets;
                buckets = new List<gBucket>();
                string codelist = "304239603";
                AddBuckets(buckets, "", codelist, "", "");
                if (AnalyzeBuckets(buckets, 16, 60, "Check PulseOximetry freq", false))
                    SetInd(16, "O2 Sat Order in effect with PulseOximetry at least q2: " + desc_found);
            }

            //CountAssessments(18, 40);
            //// if (_inds[18].is_checked) return;
            //CountAssessments(17, 80);
            ////if (_inds[17].is_checked) return;
            //CountAssessments(16, 150);
            ////if (_inds[16].is_checked) return;

            SetBucketSize(60);
            Program.sttimer();
            CountAssessments(15,60);
            Program._t4 = Program.entimer("t4 assess15=", Program._t4);

            SetBucketSize(30);
            Program.sttimer();
            CountAssessments(18, 30);
            Program._t5 = Program.entimer("t5 assess18=", Program._t5);

            SetBucketSize(60);

        }

        private int GetAssessInd(DateTime loc_out_time, out DateTime classdt)
        {
            int ind = 0;
            classdt = DateTime.MinValue;
            //get assessment indicator at location out time = loc_out_time
            var db = PFSDBUtility.NewPfsDataContext();        // look for the unit default admission profile
            var query = from ce in db.CLASSIFICATION_EVENTs
                        from ia in db.INDICATOR_ANSWERs
                        where (ce.CLASSIFICATION_EVENT_ID == ia.CLASSIFICATION_EVENT_ID)
                        && (ce.ENCOUNTER_ID == _pat.encounter_id)
                        && (ce.EFFECTIVE_DATETIME_IN <= loc_out_time)
                        && (ia.INDICATOR_NUMBER >= 15 && ia.INDICATOR_NUMBER <= 18)
                        orderby ce.EFFECTIVE_DATETIME_IN descending
                        select new
                        {
                            ia.INDICATOR_NUMBER,
                            ce.CLASSIFICATION_DATETIME
                        };
            if (query.Count() > 0)
            {
                ind = query.First().INDICATOR_NUMBER;
                classdt = query.First().CLASSIFICATION_DATETIME;
            }
            return ind;
        }


        private void CountAssessments(int ind, int bucket_size)
        {

            SetBucketSize(bucket_size);

            if (ind >= 15 && ind <= 17) AnalyzeAssessments15_17(ind, bucket_size);
            if (ind == 18) AnalyzeAssessments15_17(ind, bucket_size);

        }

        private void AnalyzeAssessments15_17(int ind, int bucket_size)
        {
            string codelist;
            string reslist;
            List<gBucket> buckets;
            string freqstr = "";

            //Example:  ar1[Array.IndexOf(cat_ary,"Neuro"), 1] = true;
            int indnumset = 0;

            if (bucket_size == 60) freqstr = "====Q4/Q2/Q1 HR EVALUATION================";
            if (bucket_size == 30) freqstr = "====Q30 MIN EVALUATION====================";
            Program.VerboseAudit(freqstr + " bucket size=" + bucket_size + "  _bucket size=" + _bucket_size);

            //Cardiovascular group
            assessgrouplabel = "Cardiovascular";
            buckets = new List<gBucket>();
            codelist = "304239566,304239567,304239568,304239569,304239570,304239571,304239572,304239573";
            codelist += ",304239574,304239575,304239576,304239577,304239578,304239579,304239580,304239581,304239582";
            codelist += ",304239583,304239584,304239585,304239649,304239650,304239651,304239652,304239653,304239654";
            codelist += ",304239659,30423200088,30423200089,30423200898,30423200899,30423200900,30423200901,30423200902";
            codelist += ",30423302820";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, "Cardiovascular", true, out indnumset);
            if (indnumset >=15 && indnumset <=18)
                ar1[Array.IndexOf(cat_ary, assessgrouplabel), indnumset - 15] = true;

            //GI/GU group
            assessgrouplabel = "GI/GU";
            buckets = new List<gBucket>();
            codelist = "3042319544,3042319545,3042319546";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, "GI/GU", true, out indnumset);
            if (indnumset >= 15 && indnumset <= 18)
                ar1[Array.IndexOf(cat_ary, assessgrouplabel), indnumset - 15] = true;

            // I/O group
            assessgrouplabel = "I/O";
            buckets = new List<gBucket>();
            codelist = "304239510,304239511,304239512,304239513,304239514,304239515,304239516,304239517,304239518";
            codelist += ",304239519,304239520,304239521,304239522,304239523,304239524,304239525,304239526,304239527,304239528,304239529";
            codelist += ",304239530,304239531,304239532,304239533,304239534,304239535,304239536,304239537,304239538,304239539,304239540";
            codelist += ",304239541,304239542,304239543,304239544,304239545,304239546,304239547,304239548,304239549,304239550";
            codelist += ",3042373790,3042373800,3042373810,304239218";
            AddBuckets(buckets, "", codelist, "", "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, "I/O", true, out indnumset);
            if (indnumset >= 15 && indnumset <= 18)
                ar1[Array.IndexOf(cat_ary, assessgrouplabel), indnumset - 15] = true;


            //Neuro group
            assessgrouplabel = "Neuro";
            buckets = new List<gBucket>();
            codelist = "304239493,304239494,304239495,304239496,304239502,304239339,304239503,304239504,304239505";
            codelist += ",304239506,304239507,304239508,304239382,304239551,304239552,304239553,304239554,304239555";
            codelist += ",304239556,304239557,304239558,304239386,304239387,304239559,304239560,304239561,304239385";
            codelist += ",304239562,304239564,304239326,304239655";
            //304239656 = Aldrete score
            AddBuckets(buckets, "", codelist, "", "", "");
            //304237096770 & 127           Line Care-Neuro External Ventricular Drain Right Parietal region
            AddBuckets(buckets, "", "304237096770", "Line Care-Neuro External", "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, "Neuro", true, out indnumset);
            Program.VerboseAudit("AnalyzeBuckets returned=" + indnumset);
            if (indnumset >= 15 && indnumset <= 18)
            {
                Program.VerboseAudit("ar1 element=" + Array.IndexOf(cat_ary, assessgrouplabel) + "," + (indnumset-15));
                ar1[Array.IndexOf(cat_ary, assessgrouplabel), indnumset - 15] = true;
            }

            //OB Assess group
            assessgrouplabel = "OB Assessment";
            buckets = new List<gBucket>();
            codelist = "3042319525,3042319526,3042319527,3042319528,3042319529,3042319530,3042319531,3042319532,3042319533";
            codelist += ",3042319534,3042319535,3042319536,3042319537,3042319538,3042319539,3042319540,3042319541";
            AddBuckets(buckets, "", codelist, "", "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, "OB Assessment", true, out indnumset);
            if (indnumset >= 15 && indnumset <= 18)
                ar1[Array.IndexOf(cat_ary, assessgrouplabel), indnumset - 15] = true;

            //Pain group
            assessgrouplabel = "Pain";
            buckets = new List<gBucket>();
            codelist = "3042319524,3042319548,3042319549,3042319550,3042319551,3042319552,3042319553,3042319554";
            codelist += ",3042319555,3042319556,3042319557,3042319558,3042319559,3042319560,3042319561,3042319562,3042319563";
            codelist += ",3042319564";
            AddBuckets(buckets, "", codelist, "", "", "");
            codelist = "3042319547";
            AddBuckets(buckets, "", codelist, "", "", "1,2,3,4,5,6,7,8,9,10");
            AnalyzeBuckets(buckets, ind, bucket_size, "Pain", true, out indnumset);
            if (indnumset >= 15 && indnumset <= 18)
                ar1[Array.IndexOf(cat_ary, assessgrouplabel), indnumset - 15] = true;

            //Pulmonary
            assessgrouplabel = "Pulmonary";
            buckets = new List<gBucket>();
            codelist = "304239491,304239492,304239499,304239509,304239586,304239587,304239588";
            codelist += ",304239589,304239590,304239591,304239592,304239657";
            AddBuckets(buckets, "", codelist, "", "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, "Pulmonary", true, out indnumset);
            if (indnumset >= 15 && indnumset <= 18)
                ar1[Array.IndexOf(cat_ary, assessgrouplabel), indnumset - 15] = true;

            //Tech
            assessgrouplabel = "Technology";
            buckets = new List<gBucket>();
            codelist = "3042319505,3042319506,3042319507,3042319508,3042319509,3042319511,3042319513";
            codelist += ",3042319515,304239342,3042319542,3042319543,304239563";
            AddBuckets(buckets, "", codelist, "", "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, "Technology", true, out indnumset);
            if (indnumset >= 15 && indnumset <= 18)
                ar1[Array.IndexOf(cat_ary, assessgrouplabel), indnumset - 15] = true;

            //Vitals
            assessgrouplabel = "Vitals";
            buckets = new List<gBucket>();
            codelist = "304239497,304239498,304239461,304239462,304239593,304239594,304239595,304239596,304239597";
            codelist += ",304239598,304239599,304239600,304239601,304239602,304239603,304239604,304239605,304239606";
            codelist += ",304239607,304239608,304239609,304239610,304239611,304239612,304239613,304239614,304239615";
            codelist += ",304239616,304239617,304239618,304239619,304239620,304239621,304239622,304239623,304239624";
            codelist += ",304239625,304239626,304239627,304239628,304239629,304239631,304239632,304239633";
            codelist += ",304239634,304239635,304239636,304239637,304239638,304239639,304239640,304239641,304239642";
            codelist += ",304239643,304239644,304239645,304239646,304239647,304239648";
            codelist += ",3042396285,3042396286,3042396287,30423344220";
            AddVitalsBuckets(buckets, "", codelist, "", "", "", SearchDepth.SearchDefault);
            AnalyzeBuckets(buckets, ind, bucket_size, "Vitals", true, out indnumset);
            Program.VerboseAudit("AnalyzeBuckets returned=" + indnumset);
            if (indnumset >= 15 && indnumset <= 18)
            {
                Program.VerboseAudit("ar1 element=" + Array.IndexOf(cat_ary, assessgrouplabel) + "," + (indnumset - 15));
                ar1[Array.IndexOf(cat_ary, assessgrouplabel), indnumset - 15] = true;
            }

            //Wound Site
            assessgrouplabel = "Wound Site";
            buckets = new List<gBucket>();
            codelist = "304239500,304239501,3042319501,3042319502,3042319503,3042319504,3042319510,3042319512,3042319514";
            codelist += ",3042319516,3042319517,3042319518,3042319519,3042319520,3042319521,3042319523,304239565";
            codelist += ",30423600305,30423600306";
            AddBuckets(buckets, "", codelist, "", "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, "Wound Site", true, out indnumset);
            if (indnumset >= 15 && indnumset <= 18)
                ar1[Array.IndexOf(cat_ary, assessgrouplabel), indnumset - 15] = true;

        }

        private bool AnalyzeBuckets(List<gBucket> buckets, int ind, int bucketsize, string group)
        {
            return AnalyzeBuckets(buckets, ind, bucketsize, group, true);
        }
        private bool AnalyzeBuckets(List<gBucket> buckets, int ind, int bucketsize, string group, bool set_ind)
        {
            int indnumset;
            return AnalyzeBuckets(buckets, ind, bucketsize, group, set_ind, out indnumset);
        }

        private bool AnalyzeBuckets(List<gBucket> buckets, int ind, int bucketsize, string group, bool set_ind, out int indnumset)
        {
            DateTime dt = DateTime.MinValue;
            DateTime firstdt = DateTime.MinValue;

            bool all_ok = OLDAnalyzeBuckets(buckets, ind, bucketsize, group, set_ind, out indnumset);
            Program.VerboseAudit("---- After IndNumSet= " + indnumset + " group=" + group + " ----");
            return all_ok;


//            int bnum = 0;
//            int numbucket = -99;
//            int numitems = 0;
//            int numallitems = 0;
//            List<gGap> gaplist = new List<gGap>();
//            Program.VerboseAudit("----GAP Begin Assessment Group = " + group + " ---- bucketsizew=" + bucketsize);

//            //Program.VerboseAudit("buckets count=" + buckets.Count());
//            var b = buckets.OrderBy(e => e.evdt).ToList();
//            //numitems = buckets.Count();

//            DateTime lastq4dt = DateTime.MinValue;
//            int numq4 = 1;

//            foreach (var item in b)
//            {
//                Program.VerboseAudit(item.bucket + ") " + item.evdt.ToString() + " := " + item.code + " has all deps=" + item.has_all_deps);
//                if (item.has_all_deps)
//                    if (numitems == 0)
//                    {
//                        dt = item.evdt;
//                        firstdt = dt;
//                        lastq4dt = dt;
//                    }
//                    else
//                    {
//                        if (lastq4dt.AddMinutes(30) >= dt)
//                        {
//                            numq4++;
//                            lastq4dt = dt;
//                        }
//                        var g = new gGap();
//                        g.gap = (int)(PFSUtility.DateDiffInMinutes(dt, item.evdt));
//                        g.evdt1 = dt;
//                        g.evdt2 = item.evdt;
//                        gaplist.Add(g);
//                        dt = item.evdt;
//                    }
//                numitems++;
//                numallitems = numallitems + 1 + item.num_addl_items;
//            }
//            //Program.VerboseAudit("numitems=" + numitems + " numq4=" + numq4);
//            //if (numitems <= 2)
//            //{
//            //    if (numq4 >= 2)
//            //    {
//            //        if (set_ind) //&& ind >= 15 && ind <= 17)
//            //        {
//            //            SetInd(15, "q4 based on number >=2 of items charted = " + numq4);
//            //            return true;
//            //        }
//            //    }
//            //    return false;
//            //}

//            int i, j, setind = 0;
//            int numgaps = 0;
//            int gapsum = 0;
//            double gapavg = 999.0, lo_gapavg = 999.0, altlo_gapavg = 999.0;
//            bool lastgap = false;
//            bool shortstop = false;
//            int timegap = 0;
//            int lastj;
//            DateTime lo_evdt1 = DateTime.MinValue, lo_evdt2 = DateTime.MinValue;
//            DateTime altlo_evdt1 = DateTime.MinValue, altlo_evdt2 = DateTime.MinValue;
//            gGap[] gapary = gaplist.ToArray();
//            string s = "";
//            for (i = 0; i <= gapary.GetUpperBound(0); i++) s = s + "" + gapary[i].gap;
//            if (numitems == 2 && numq4 >= 2) // numitems will always be > 2
//            {
//                numgaps = 1;
//                gapsum = gapary[0].gap;
//                gapavg = gapsum;
//                lo_gapavg = gapavg;
//                lo_evdt1 = gapary[0].evdt1;
//                lo_evdt2 = gapary[gapary.GetUpperBound(0)].evdt2;
//            }
//            else
//            {

//                //numitems = 4
//                //i = 0 j = 0[i].evdt1 = 9 / 4 / 2019 5:15:00 AM[j].evdt2 = 9 / 4 / 2019 9:32:00 AM

//                for (i = 0; i <= gapary.GetUpperBound(0) && !shortstop; i++)
//                {
//                    numgaps = 0;
//                    gapsum = 0;
//                    gapavg = 999.0;
//                    lastgap = false;
//                    for (j = i; j <= gapary.GetUpperBound(0) && !lastgap; j++)
//                    {
//                        //Program.VerboseAudit("i=" + i + " j=" + j + " [i].evdt1=" + gapary[i].evdt1 + " [j].evdt2=" + gapary[j].evdt2);
//                        timegap = (int)(PFSUtility.DateDiffInMinutes(gapary[i].evdt1, gapary[j].evdt2));
//                        if (timegap <= _pat.los_hours * 30 + 30)
//                        {
//                            numgaps++;
//                            gapsum += gapary[j].gap;
//                            Program.VerboseAudit("numgaps=" + numgaps + " [j].gap=" + gapary[j].gap + " gapsum=" + gapsum);
//                        }
//                        else
//                        {
//                            if (j == i)
//                            {
//                                lastj = i;
//                                numgaps = 1;
//                                gapsum = gapary[j].gap;
//                            }
//                            else
//                                lastj = j - 1;
//                            //Program.VerboseAudit("Last Gap: i=" + i + " j=" + j + " [i].evdt1=" + gapary[i].evdt1 + " [lastj].evdt2=" + gapary[lastj].evdt2);
//                            lastgap = true;
//                            gapavg = 1.0 * gapsum / numgaps;
//                            Program.VerboseAudit("gapavg=" + gapavg + " numgaps=" + numgaps + " gapsum=" + gapsum);
//                            timegap = (int)(PFSUtility.DateDiffInMinutes(gapary[i].evdt1, gapary[lastj].evdt2));
//                            //                            if (gapavg < lo_gapavg && timegap >= .75 * _pat.los_hours * 30)
//                            if (gapavg < lo_gapavg && timegap >= _pat.los_hours * 30)
//                            {
//                                lo_gapavg = gapavg;
//                                //Program.VerboseAudit("Low gapavg=" + lo_gapavg + " btwn " + gapary[i].evdt1 + " and " + gapary[lastj].evdt2);
//                                lo_evdt1 = gapary[i].evdt1;
//                                lo_evdt2 = gapary[lastj].evdt2;
//                            }
//                            numgaps = 0;
//                            gapsum = 0;
//                        }

//                    }
//                    if (!lastgap)
//                    {
//                        shortstop = true;
//                        j--; // take the j index back by 1 because it incremented at the end.
//                        //Program.VerboseAudit(".Last Gap: i=" + i + " j=" + j + " [i].evdt1=" + gapary[i].evdt1 + " [j].evdt2=" + gapary[j].evdt2);
//                        gapavg = 1.0 * gapsum / numgaps;
//                        //Program.VerboseAudit(".gapavg=" + gapavg + " numgaps=" + numgaps + " gapsum=" + gapsum);
//                        timegap = (int)(PFSUtility.DateDiffInMinutes(gapary[i].evdt1, gapary[gapary.GetUpperBound(0)].evdt2));
//                        //Program.VerboseAudit(".timegap=" + timegap + " .75halfLOS=" + .75 * _pat.los_hours * 30);
//                        if (gapavg < lo_gapavg)
//                        {
//                            if (timegap >= _pat.los_hours * 30)
////                          if (timegap >= .75 * _pat.los_hours * 30)
//                            {
//                                lo_gapavg = gapavg;
//                                //Program.VerboseAudit(".Low gapavg=" + lo_gapavg + " btwn " + gapary[i].evdt1 + " and " + gapary[gapary.GetUpperBound(0)].evdt2);
//                                lo_evdt1 = gapary[i].evdt1;
//                                lo_evdt2 = gapary[gapary.GetUpperBound(0)].evdt2;
//                            }
//                            else
//                            {
//                                altlo_gapavg = gapavg;
//                                //Program.VerboseAudit(".AltLow gapavg=" + lo_gapavg + " btwn " + gapary[i].evdt1 + " and " + gapary[gapary.GetUpperBound(0)].evdt2);
//                                altlo_evdt1 = gapary[i].evdt1;
//                                altlo_evdt2 = gapary[gapary.GetUpperBound(0)].evdt2;
//                            }
//                        }
//                    }
//                }
//            }
//            if (numitems >= 2) Program.VerboseAudit("Final Low gapavg=" + lo_gapavg + " btwn " + lo_evdt1.ToString() + " and " + lo_evdt2.ToString() + " " + (_pat.los_hours <= 4 ? 0 : 1) + "LOS=" + _pat.los_hours + " assmts:" + (_inds[15].is_checked ? 1 : 0) + (_inds[16].is_checked ? 1 : 0) + (_inds[17].is_checked ? 1 : 0) + (_inds[18].is_checked ? 1 : 0) + " gap[]: =" + s);
//            //Program.VerboseAudit("Alt   Low gapavg=" + altlo_gapavg + " btwn " + altlo_evdt1.ToString() + " and " + altlo_evdt2.ToString());
//            //q4: 3 hr + gap average  in 6 - 8 hrs
//            //  q2:   1 hr 30 - 2 hr 59m avg gap  in 4 hrs
//            //  q1:   46 min - 1 hr 29 min avg gap in 4 hrs
//            //  q30:  45 min or less avg gap in 4 hrs
//            //            if (lo_gapavg <= 45)
//            int sample_breadth = (int)(PFSUtility.DateDiffInMinutes(lo_evdt1, lo_evdt2));
////            if (_pat.los_hours >= 4 && sample_breadth > 0.75 * _pat.los_hours * 30 && numitems >= 2)
//            if (_pat.los_hours >= 4 && sample_breadth >= _pat.los_hours * 30 && numitems >= 2)
//            {
//                //if (lo_gapavg <= 45) setind = 18;       //q30
//                //else if (lo_gapavg <= 80) setind = 17;  //q60
//                //else if (lo_gapavg <= 180) setind = 16; //q120
//                //else if (lo_gapavg <= 999) setind = 15;  //q240
//                if (lo_gapavg <= 40) setind = 18;       //q30
//                else if (lo_gapavg <= 70) setind = 17;  //q60
//                else if (lo_gapavg <= 130) setind = 16; //q120
//                else if (lo_gapavg <= 250) setind = 15;  //q240

//            }
//            else
//            {
////                if (numitems > 2 && sample_breadth >= 0.75 * _pat.los_hours * 30)
//                if (numitems > 2 && sample_breadth >= _pat.los_hours * 30)
//                {
//                    if (lo_gapavg <= 30) setind = 18;  //q60
//                    else if (lo_gapavg <= 60) setind = 17;  //q60
//                    else if (lo_gapavg <= 120) setind = 16; //q120
//                    else if (lo_gapavg <= 240) setind = 15;  //q240
//                }
//                else if (numitems == 2 && sample_breadth >= 30)
//                {
//                    //if (sample_breadth >= 0.5 * _pat.los_hours * 30) setind = 16; //q120
//                    //else setind = 15;  //q240
//                    if (sample_breadth >= _pat.los_hours * 30 && lo_gapavg <= 240) setind = 15;
//                    //else setind = 15;  //q240
//                }
//                //else if (numitems >= 2)
//                //    setind = 15;
//            }
//            int imins = 0;
//             all_ok = false;
//            if (setind == 18) imins = 30;
//            else if (setind == 17) imins = 60;
//            else if (setind == 16) imins = 120;
//            else if (setind == 15) imins = 240;
//            if (ind == 11)
//            {
//                if (setind >= 15)
//                    setind = 11;
//                else
//                    setind = 0;
//            }
//            //if (ind >= 15 && ind <= 17)
//            //{
//            //    if (setind == 18) setind = 17;
//            //}
//            if (setind > 0)
//            {
//                if (set_ind)
//                    SetInd(setind, "Qualifies for q" + imins + "mins for gap avg=" + lo_gapavg + "[" + (sample_breadth < 4 ? "<" : "") + "in " + sample_breadth + " mins] btwn " + lo_evdt1.ToString() + " and " + lo_evdt2.ToString());
//                else
//                    Program.VerboseAudit(group + "Qualifies for q" + imins + "mins for gap avg=" + lo_gapavg + "[" + (sample_breadth < 4 ? "<" : "") + "in " + sample_breadth + " mins] btwn " + lo_evdt1.ToString() + " and " + lo_evdt2.ToString());
//            }
//            if (ind == 18 && setind == 18) all_ok = true;
//            if (ind >= 15 && ind <= 17 && setind >= 15) all_ok = true;
//            if (ind == 11 && setind == 11) all_ok = true;
//            if (ind == 3 && setind >= 16) all_ok = true;
//            if (ind == 19 && setind >= 17) SetInd(19, "Site Assessmt q1hr+ and age=" + _pat.age);
//            //if (ind == 19 && setind >= 16 && _pat.age <= 6.0) SetInd(19, "Site Assessmt q2hr+ and age=" + _pat.age);
//            Program.VerboseAudit("---- GAP End Assessment Group = " + group + " ----");
//            return all_ok;

        }
        private bool DoShortLOSAssessEval(List<gBucket> buckets, int ind, int bucketsize, string group, bool set_ind)
        {
            int ct = 0;
            DateTime starttm, endtm;
            DateTime prevtm = DateTime.MinValue;
            int loshrs = (int)(Math.Round(_pat.los_hours));
            var arr = buckets.ToArray();
            bool set18 = false;
            Program.VerboseAudit("----Entering Short LOS Analysis for group: " + group + " LOS=" + loshrs + "  bucketsizew=" + bucketsize);

            for (int i = 0; (i < arr.GetUpperBound(0)); i++)
            {
                prevtm = DateTime.MinValue;
                starttm = arr[i].evdt;
                endtm = starttm.AddHours(loshrs);
                ct = 0;
                for (int j = i; (j < arr.GetUpperBound(0)); j++)
                {
                    if (prevtm < arr[j].evdt && arr[j].evdt >= starttm && arr[j].evdt <= endtm)
                    {
                        ct++;
                        prevtm = arr[j].evdt;
                    }
                }
                if (ct >= 9 / (4.0/loshrs))
                {
                    SetInd(18, ct.ToString() + " or more items in LOS=" + loshrs + " hours.");
                    set18 = true;
                    i = 999;
                }
            }
            Program.VerboseAudit("----Exiting Short LOS Analysis for group: " + group + " LOS=" + loshrs + "  bucketsizew=" + bucketsize);

            return set18;
        }


//Proposed change to remove consecutive factor; use straight scale with Short LOS exception:  
//	                        LOS=12 hrs      LOS = 6 hrs     LOS = 4 hrs     LOS = 2 hrs     Short LOS< 5 hrs
//q30 75% of 30-min buckets	3/4 x 24  = 18	3/4 x 12  = 9	3/4 x 8  = 6	3/4 x 4  = 3	5 or more VS
//q1 66% of 60-min buckets	2/3 x 12  = 8	2/3 x 6  = 4	2/3 x 4  = 2.67=>3	2/3 x 2  = 1.33=>1	3 VS + examine 30min 
//q2 50 % of 60-min buckets	.5 x 12  = 6	.5 x 6  = 3	.5 x 4  = 2	.5 x 2  = 1	2 VS
//q4 33% of 60-min buckets	1/3 x 12  = 4 2	1/3 x 6  = 2 1	1/3 x 4  = 1.33=>1	1/3 x 2  = .67=>1	1 VS
        private bool OLDAnalyzeBuckets(List<gBucket> buckets, int ind, int bucketsize, string group, bool set_ind, out int indnumset)
        {
            DateTime dt = DateTime.MinValue;
            int bnum = 0;
            int numbucket = -99;
            int lobucket = 99;
            int hibucket = 0;
            int numitems = 0;
            int numconsec = 0;
            int greatestnumconsec = 0;
            bool all_ok = false;
            indnumset = 0;
            List<gBucket> dtlist = new List<gBucket>();
            Program.VerboseAudit("----Begin Assessment Group = " + group + " ---- bucketsizew=" + bucketsize + " start time of first bucket=" + _pat.pull_start);
            //if (ind==18 && Math.Round(_pat.los_hours) <= 4.0)
            //{
            //    all_ok = DoShortLOSAssessEval(buckets,ind,bucketsize,group,set_ind);
            //    Program.VerboseAudit("---- End Assessment Group = " + group + " ----");
            //    return all_ok;
            //}

            //Program.VerboseAudit("buckets count=" + buckets.Count());
            var b = buckets.OrderBy(e => e.evdt).ToList();

            //numitems = buckets.Count();
            foreach (var item in b)
            {
                if (numbucket < item.bucket)
                {
                    numbucket = item.bucket;
                    numitems++;
                    Program.VerboseAudit(item.bucket + ") " + item.evdt.ToString() + " := " + item.code);
                }
                //if (numbucket < item.bucket)
                //{
                //    //add dt to ary
                //    bnum++;
                //    dtlist.Add(item);
                //    if (numbucket == -99 || item.bucket - numbucket == 1)
                //    {
                //        numconsec++;
                //        if (greatestnumconsec < numconsec)
                //            greatestnumconsec = numconsec;
                //    }
                //    else
                //    {
                //        numconsec = 1;
                //    }
                //    numbucket = item.bucket;
                //    if (hibucket < item.bucket) hibucket = item.bucket;
                //    if (lobucket > item.bucket) lobucket = item.bucket;
                //    Program.VerboseAudit(item.bucket + ")." + item.evdt.ToString() + " := " + item.code);// + "  [distinct times only]");
                //}
            }

            //if (bnum <= 1)
            //{
            //    Program.VerboseAudit("---- End Assessment Group = " + group + " ----");
            //    return false;
            //}

            //Program.VerboseAudit("numitems=" + numitems);
            //Program.VerboseAudit("bucket count=" + bnum);
            //int half_los = (int)(_pat.los_hours * (30.0 / bucketsize));//for q30 this will be los_hours.  for q60 this will be .5 * los_hours
            //Program.VerboseAudit("half-LOS bucket count=" + half_los + " at " + bucketsize + "mins each.");
            //Program.VerboseAudit("greatest consec=" + greatestnumconsec);

            int num_buckets_in_los = (int)(_pat.los_hours * (60.0 / bucketsize));//for q30 this will be 2xlos_hours.  for q60 this will be los_hours
            Program.VerboseAudit("total bucket count in LOS=" + num_buckets_in_los);
            Program.VerboseAudit("num buckets filled=" + numitems);
            //Program.VerboseAudit("half-LOS bucket count=" + half_los + " at " + bucketsize + "mins each.");
            //Program.VerboseAudit("greatest consec=" + greatestnumconsec);

            //double bucketratio = (hibucket-lobucket) / (1.0 * (bnum-1));
            //q30 75% of 30-min buckets	3/4 x 24  = 18	3/4 x 12  = 9	3/4 x 8  = 6	3/4 x 4  = 3	5 or more VS
            //q1 66% of 60-min buckets	2/3 x 12  = 8	2/3 x 6  = 4	2/3 x 4  = 2.67=>3	2/3 x 2  = 1.33=>1	3 VS + examine 30min 
            //q2 50 % of 60-min buckets	.5 x 12  = 6	.5 x 6  = 3	.5 x 4  = 2	.5 x 2  = 1	2 VS
            //q4 33% of 60-min buckets	1/3 x 12  = 4 2	1/3 x 6  = 2 1	1/3 x 4  = 1.33=>1	1/3 x 2  = .67=>1	1 VS
            int q30need = (int)Math.Round(0.75 * 2 * _pat.los_hours);
            int q1need = (int)Math.Round(0.667 * _pat.los_hours);
            int q2need = (int)Math.Round(0.5 * _pat.los_hours);
            int q4need = 1;
            if (ind < 18)
            {
                if (_pat.los_hours > 5)
                {
                    if (_pat.los_hours < 8)
                    {
                        q4need = 1;
                        q2need = 2; //Jan27 2021
                    }
                    else
                    {
                        q4need = 2;
                        q2need = 5; // Feb3 2021 4;//Jan27 2021
                    }

                    if (numitems >= q1need)
                    {
                        SetInd(17 * Convert.ToInt32(set_ind), "Qualifies for q1 because numcharted=" + numitems + " is >=" + q1need + " LOS=" + Math.Round(_pat.los_hours, 2) + " x .667=" + q1need);
                        indnumset = 17;
                        all_ok = (ind <= 17);
                    }
                    else if (numitems >= q2need)
                    {
                        //Jan27 2021 SetInd(16 * Convert.ToInt32(set_ind), "Qualifies for q2 because numcharted=" + numitems + " is >=" + q2need + " LOS=" + Math.Round(_pat.los_hours, 2) + " x .5=" + q2need);
                        SetInd(16 * Convert.ToInt32(set_ind), "Qualifies for q2 because numcharted=" + numitems + " is >=" + q2need + " for LOS=" + Math.Round(_pat.los_hours, 2) + "  >=8hrs needs 5 else 2");
                        indnumset = 16;
                        all_ok = (ind <= 16);
                    }
                    else if (numitems >= q4need)
                    {
                        SetInd(15 * Convert.ToInt32(set_ind), "Qualifies for q4 because numcharted=" + numitems + " is >=" + q4need + " LOS=" + Math.Round(_pat.los_hours, 2) + "  >=8hrs needs 2 else 1");
                        indnumset = 15;
                        all_ok = (ind <= 15);
                    }
                }
                else //short los
                {
                    q1need = 3;
                    q2need = 2;
                    q4need = 1;

                    if (numitems >= q1need)
                    {
                        SetInd(17 * Convert.ToInt32(set_ind), "ShortLOS Qualifies for q1 because numcharted=" + numitems + " is >=" + q1need + " LOS=" + Math.Round(_pat.los_hours, 2) + " needs just 3");
                        indnumset = 17;
                        all_ok = (ind <= 17);
                    }
                    else if (numitems >= q2need)
                    {
                        SetInd(16 * Convert.ToInt32(set_ind), "ShortLOS Qualifies for q2 because numcharted=" + numitems + " is >=" + q2need + " LOS=" + Math.Round(_pat.los_hours, 2) + " needs just 2");
                        indnumset = 16;
                        all_ok = (ind <= 16);
                    }
                    else if (numitems >= q4need)
                    {
                        SetInd(15 * Convert.ToInt32(set_ind), "ShortLOS Qualifies for q4 because numcharted=" + numitems + " is >=" + q4need + " LOS=" + Math.Round(_pat.los_hours, 2) + " needs just 1");
                        indnumset = 15;
                        all_ok = (ind <= 15);
                    }

                }
//                if (greatestnumconsec >= half_los || bucketratio <= 1.5 && bnum >= half_los-1)
//                {
//                    SetInd(17 * Convert.ToInt32(set_ind), "Qualifies for q1hr because chartings are at least q1hr for duration of half-LOS=" + half_los + " maxconsec=" + greatestnumconsec);
//                    all_ok = (ind <= 17);
//                }
//                //                else if (bucketratio <= 1.25 && bnum >= half_los && half_los >= 4)
//                //else if (bucketratio <= 1.25 && bnum >= half_los && half_los >= 4)
//                //{
//                //    SetInd(17 * Convert.ToInt32(set_ind), "Qualifies for q1hr because charting to bucket ratio is less than 1.25: hibucket-lobucket=" + hibucket +"-" +lobucket+"="+ (hibucket - lobucket) + " divided by num buckets=" + bnum + " equals " + bucketratio);
//                //    all_ok = (ind >= 17);
//                //}
//                //else if (bucketratio <= 2.5 && bnum >= 2)
//                //                else if (bnum >= half_los/2)
//                else if (bucketratio < 3 && bnum >= 3 && 
//                    (
//                    (half_los >= 6 && (hibucket - lobucket) >= half_los-1)
//                    ||
//                    ((half_los >= 4 && half_los <= 5) && (hibucket - lobucket) >= half_los - 1)
//                    ||
//                    ((half_los <= 3) && (hibucket - lobucket) >= half_los)
//                    )
//                    )
//                {
//                    SetInd(16 * Convert.ToInt32(set_ind), "Qualifies for q2hr because charting to bucket ratio is less than 3: hibucket-lobucket=" + hibucket + "-" + lobucket + "=" + (hibucket - lobucket) + " divided by num buckets=" + (bnum-1) + " equals " + bucketratio);
//                    all_ok = (ind <= 16);
//                }
////                else if (bucketratio > 2.5 && bucketratio <= 5 && bnum >= 2)
////                else if (bnum >= half_los/4)
//                else if ((bucketratio <= 6 && bnum >= 2) && //change to <=6 Oct15/2020
//                    (
//                    (half_los >= 6 && (hibucket - lobucket) >= half_los - 2)
//                    ||
//                    ((half_los >= 4 && half_los <= 5) && (hibucket - lobucket) >= half_los - 1)
//                    ||
//                    ((half_los <= 3) && (hibucket - lobucket) >= half_los)
//                    )
//                    )
//                {
//                    SetInd(15 * Convert.ToInt32(set_ind), "Qualifies for q4hr because charting to bucket ratio is <=6: hibucket-lobucket=" + hibucket + "-" + lobucket + "=" + (hibucket - lobucket) + " divided by num buckets=" + (bnum-1) + " equals " + bucketratio);
//                    all_ok = (ind == 15);
//                }
            }
            else // ind==18
            {
                if (_pat.los_hours > 5)
                {
                    if (numitems >= q30need)
                    {
                        SetInd(18 * Convert.ToInt32(set_ind), "Qualifies for q30 because numcharted=" + numitems + " is >=" + q30need + " LOS=" + Math.Round(_pat.los_hours, 2) + " x 2 x .75=" + q30need);
                        indnumset = 18;
                        all_ok = (ind <= 18);
                    }
                }
                else
                {
                    q30need = 5;
                    if (numitems >= q30need)
                    {
                        SetInd(18 * Convert.ToInt32(set_ind), "ShortLOS Qualifies for q30 because numcharted=" + numitems + " is >=" + q30need + " LOS=" + Math.Round(_pat.los_hours, 2) + " x 2 needs " + q30need);
                        indnumset = 18;
                        all_ok = (ind <= 18);
                    }
                }
                //if (greatestnumconsec >= half_los)
                //{
                //    SetInd(18 * Convert.ToInt32(set_ind), "Qualifies for q30min because consecutive chartings are at least q30 for duration of half-LOS=" + half_los + " maxconsec=" + greatestnumconsec);
                //    all_ok = (ind <= 18);
                //}
            }
            Program.VerboseAudit("---- End Assessment Group = " + group + " ----");
            if (Program.g_catstudy) Program.VerboseAudit("---- USER-DEFINED ind=" + indnumset + "  group=" + group + " ----");
            return all_ok;
        }


        private void SetBucketSize(int minutes)
        {
            _bucket_size = minutes;
        }

        //Add to the list of bucket numbers (redundant buckets are fine)
        private void AddBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field)
        {
            AddBuckets(bucket_list, cat, code_list, desc, field, "", SearchDepth.SearchDefault);
        }
        private void AddBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field, string result_list)
        {
            AddBuckets(bucket_list, cat, code_list, desc, field, result_list, SearchDepth.SearchDefault);

        }
        private void AddBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field, string result_list, SearchDepth search_depth)
        {
            // Assign (virtual) bucket numbers to each assessment.  
            // Every x minutes after that is a new bucket.  Bucket zero = pull_start time.
//            Program.VerboseAudit("----Locating items for group: " + assessgrouplabel + "   bucketsize=" + _bucket_size);

            // get the chart items for the assessments
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc, field, result_list);
            int ct = query.Count();
            Program.VerboseAudit("ct=" + ct + " of codelist=" + code_list);
            // This step is needed for those who want to count the # within a bucket, like Shands #24.
            // This will have no effect on those that count buckets.
            //var query2 = (from item in query select new { item.EVENT_DATETIME }).Distinct();
            //var query2 = (from item in query select new { item.EVENT_DATETIME, item.CODE });

            //foreach (var x in query2)
            //{
            //    Program.VerboseAudit("patpullstart=" + _pat.pull_start.ToString() + " itemevdt=" + x.EVENT_DATETIME.ToString() + " diff=" + (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, x.EVENT_DATETIME)) + " bucketsize=" + _bucket_size + " bucket=" + (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, x.EVENT_DATETIME) / _bucket_size));
            //}

            // figure out what buckets the events belong to
            //var query3 = from item in query2
            //             select new {
            //                bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size)
            //             };
            //// Add to the list
            //foreach (var item in query3) {
            //    bucket_list.Add(item.bucket);
            //}

            var query3 = from item in query
                         select new
                         {
                             bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                             code = item.CODE,
                             evdt = item.EVENT_DATETIME,
                             desc= item.DESCRIPTION
                         };
            // Add to the list
            int cta = 0;
            foreach (var item in query3)
            {
                Program.VerboseAudit("  item: bucket=" + item.bucket + " c=" + item.code + " dt=" + item.evdt + " desc="+item.desc);
                var b = new gBucket();
                b.bucket = item.bucket;
                b.code = item.code;
                b.evdt = item.evdt;
                b.has_all_deps = true;
                gBucket f = bucket_list.Find(x => x.evdt == item.evdt);
                if (f.evdt != item.evdt)
                {
                    bucket_list.Add(b);
                    cta++;
                }
            }
            Program.VerboseAudit("add ct=" + cta);

            // This is just for auditing: Say what we are looking for and how many were found
            if (!String.IsNullOrEmpty(result_list))
            {
                // print each word and if it was found or not
                //int i = CountResultContains(cat, code_list, desc, field, result_list);
            }
            else
            {
                // print how many were found
                //Program.VerboseAudit(Describe(cat, code_list, desc, field, ""));
            }

        }


        private void AddVitalsBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field, string result_list, SearchDepth search_depth)
        {
            // Assign (virtual) bucket numbers to each assessment.  
            // Every x minutes after that is a new bucket.  Bucket zero = pull_start time.
            Program.VerboseAudit("----Locating Vital Signs: Filling buckets...Bucketsize=" + _bucket_size + " start time of first bucket=" + _pat.pull_start);

            // get the chart items for the assessments
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc, field, result_list);

            // This step is needed for those who want to count the # within a bucket, like Shands #24.
            // This will have no effect on those that count buckets.
            //var query2 = (from item in query select new { item.EVENT_DATETIME }).Distinct();
            //var query2 = (from item in query select new { item.EVENT_DATETIME, item.CODE });

            //foreach (var x in query2)
            //{
            //    Program.VerboseAudit("patpullstart=" + _pat.pull_start.ToString() + " itemevdt=" + x.EVENT_DATETIME.ToString() + " diff=" + (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, x.EVENT_DATETIME)) + " bucketsize=" + _bucket_size + " bucket=" + (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, x.EVENT_DATETIME) / _bucket_size));
            //}

            // figure out what buckets the events belong to
            //var query3 = from item in query2
            //             select new {
            //                bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size)
            //             };
            //// Add to the list
            //foreach (var item in query3) {
            //    bucket_list.Add(item.bucket);
            //}

            var query3 = from item in query
                         select new
                         {
                             bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                             code = item.CODE,
                             evdt = item.EVENT_DATETIME,
                             desc = item.DESCRIPTION
                         };
            var query4 = from item in query3
                         orderby item.bucket,item.code
                         select item;

            // Add to the list
            int currb = -1;
//            int prevb = -1;
            string currcode = "xx";
            DateTime currevdt = DateTime.MinValue;
            int currct = 0;
  //          bool addcurr = false;
            //Data is: 0, "123"
            //         0, "234"  0733
            //         0, "234"  0734
            //         1, "123"
            //         2, "234"
            //         2, "345"
            foreach (var item in query4)
            {
                int maxLength = Math.Min(item.desc.Length, 16);
                string d = item.desc.Substring(0, maxLength);
                Program.VerboseAudit("Vitals item: bucket=" + item.bucket + " dt=" + item.evdt + " desc=" + d + " c=" + item.code);
                if (currb != item.bucket)
                {
                    currb = item.bucket;
                    currcode = item.code;
                    currevdt = item.evdt;
                    currct = 1;
                }
                else
                {
                    if (currcode != item.code)
                    {  //minimum of 2 different VS codes
                        if (currct == 1)
                        { // add the first code first
                            var b = new gBucket();
                            b.bucket = item.bucket;
                            b.code = currcode;
                            b.evdt = currevdt;
                            b.has_all_deps = true;
                            //gBucket f = bucket_list.Find(x => x.evdt == item.evdt);
                            //if (f.evdt != item.evdt) bucket_list.Add(b);
                            bucket_list.Add(b);
                            Program.VerboseAudit("  adding item1: b=" + b.bucket + " c=" + b.code + " dt=" + b.evdt);
                        }
                        currct++;
                        currcode = item.code; //guarantees not to add code again in same bucket
                            
                        var b2 = new gBucket();
                        b2.bucket = item.bucket;
                        b2.code = item.code;
                        b2.evdt = item.evdt;
                        b2.has_all_deps = true;
                        //gBucket f = bucket_list.Find(x => x.evdt == item.evdt);
                        //if (f.evdt != item.evdt) bucket_list.Add(b);
                        bucket_list.Add(b2);
                        Program.VerboseAudit("  adding item2: b=" + b2.bucket + " c=" + b2.code + " dt=" + b2.evdt);
                    }
                }
            }

            // This is just for auditing: Say what we are looking for and how many were found
            if (!String.IsNullOrEmpty(result_list))
            {
                // print each word and if it was found or not
                //int i = CountResultContains(cat, code_list, desc, field, result_list);
            }
            else
            {
                // print how many were found
                //Program.VerboseAudit(Describe(cat, code_list, desc, field, ""));
            }
            Program.VerboseAudit("----End of Locating Vital Signs: Filling buckets...Bucketsize=" + _bucket_size);
        }

        //private void AddMedBuckets(List<gBucket> bucket_list)
        //{
        //    string descript = "";
        //    string drugclass = "";
        //    string rte = "";
        //    string result = "";

        //    bool bres1 = false;
        //    bool bres2 = false;
        //    bool bclass1 = false;
        //    bool bclass2 = false;
        //    bool bclass3 = false;
        //    bool bmed1 = false;

        //    string[] medclass = { "7", "1", "2", "6" };

        //    string[] pamed1417 = { "Zofran", "Ondansetron" };

        //    var query = StartNewQuery(SearchDepth.SearchSince13Hrs);    // add custom time range below
        //    query = query.Where(e => e.CODE.ToLower().StartsWith("med"));
        //    //query = query.Where(e => e.DESCRIPTION.Contains(";;;7;;;") || 
        //    //                         e.DESCRIPTION.Contains(";;;33;;;") || 
        //    //                         e.DESCRIPTION.Contains(";;;1;;;") || 
        //    //                         e.DESCRIPTION.Contains(";;;2;;;") || 
        //    //                         e.DESCRIPTION.Contains(";;;6;;;") || 
        //    query = query.Where(e =>
        //        medclass.Any(item1 => e.DESCRIPTION.Contains(";;;" + item1 + ";;;")) ||
        //       pamed1417.Any(item2 => e.DESCRIPTION.ToUpper().StartsWith(item2.ToUpper())));
        //    query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;given") || e.DESCRIPTION.ToLower().Contains(";;;syringe") || e.DESCRIPTION.ToLower().ContainsAny(meds_rate) && (e.UNIT_ID == _pat.unit_id) || e.DESCRIPTION.ToLower().Contains(";;;new"));
        //    query = query.Where(e => e.EVENT_DATETIME <= PFSUtility.MaxDateTime(_pat.unit_departure, _pat.effective_out));
        //    query = query.OrderBy(e => e.EVENT_DATETIME);
        //    //            Program.VerboseAudit("Num Meds found: " + query.Count());
        //    foreach (var item in query)
        //    {
        //        var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
        //        if (arr.GetUpperBound(0) >= 3)
        //        {
        //            descript = arr[0];
        //            drugclass = arr[1];
        //            rte = arr[3];
        //            result = "";
        //            if (arr.Length == 5) result = arr[4];
        //        }
        //        //Program.VerboseAudit("med desc: " + descript + "//" + "med class: " + drugclass + "//" + "med result: " + result);
        //        //exclude #29, 35, 48
        //        bres1 = false;
        //        bres2 = false;
        //        bclass1 = false;
        //        bclass2 = false;
        //        bclass3 = false;
        //        bmed1 = false;

        //        bres1 = result.ToLower().Contains("given");
        //        bres2 = result.ToLower().Contains("syringe") || result.ToLower().ContainsAny(meds_rate) || result.ToLower().Contains("newbag") || result.ToLower().Contains("new bag");
        //        bclass1 = (drugclass == "1") || (drugclass == "2") || (drugclass == "6");
        //        bclass2 = (drugclass == "7");
        //        bclass3 = false; // (drugclass == "33") && (rte.ToLower() == "sc");
        //        bmed1 = pamed1417.Any(item2 => descript.ToUpper().StartsWith(item2.ToUpper()));
        //        if ((bres1 && (bclass2 || bclass3)) ||
        //            ((bres1 || bres2) && bclass1) ||
        //            bmed1)
        //        {
        //            Program.VerboseAudit("====Med found: bucket count A====");
        //            Program.VerboseAudit("med code: " + item.CODE + " desc: " + item.DESCRIPTION + "  evdt: " + item.EVENT_DATETIME.ToString());
        //            SetInd(15, "Med class 7,1,2,6 or named med");
        //        }
        //    }

        //    // get the chart items for the assessments
        //    query = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
        //    query = query.Where(e => e.CODE.ToLower().StartsWith("med"));
        //    //query = query.Where(e => e.DESCRIPTION.Contains(";;;7;;;") || e.DESCRIPTION.Contains(";;;33;;;") || e.DESCRIPTION.Contains(";;;1;;;") || e.DESCRIPTION.Contains(";;;2;;;") || e.DESCRIPTION.Contains(";;;6;;;"));
        //    query = query.Where(e =>
        //       medclass.Any(item1 => e.DESCRIPTION.Contains(";;;" + item1 + ";;;")) ||
        //       pamed1417.Any(item2 => e.DESCRIPTION.ToUpper().StartsWith(item2.ToUpper())));
        //    query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;given") || e.DESCRIPTION.ToLower().Contains(";;;syringe") || e.DESCRIPTION.ToLower().ContainsAny(meds_rate) && (e.UNIT_ID == _pat.unit_id) || e.DESCRIPTION.ToLower().Contains(";;;new"));
        //    query = query.Where(e => e.EVENT_DATETIME <= PFSUtility.MaxDateTime(_pat.unit_departure, _pat.effective_out));
        //    query = query.OrderBy(e => e.EVENT_DATETIME);
        //    //            Program.VerboseAudit("Num Meds found: " + query.Count());
        //    foreach (var item in query)
        //    {
        //        var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
        //        if (arr.GetUpperBound(0) >= 3)
        //        {
        //            descript = arr[0];
        //            drugclass = arr[1];
        //            rte = arr[3];
        //            result = "";
        //            if (arr.Length == 5) result = arr[4];
        //        }
        //        //Program.VerboseAudit("med desc: " + descript + "//" + "med class: " + drugclass + "//" + "med result: " + result);
        //        //exclude #29, 35, 48
        //        bres1 = false;
        //        bres2 = false;
        //        bclass1 = false;
        //        bclass2 = false;
        //        bclass3 = false;
        //        bmed1 = false;

        //        bres1 = result.ToLower().Contains("given");
        //        bres2 = result.ToLower().Contains("syringe") || result.ToLower().ContainsAny(meds_rate) || result.ToLower().Contains("newbag") || result.ToLower().Contains("new bag");
        //        bclass1 = (drugclass == "1") || (drugclass == "2") || (drugclass == "6");
        //        bclass2 = (drugclass == "7");
        //        bclass3 = false;  //(drugclass == "33") && (rte.ToLower() == "sc");
        //        bmed1 = pamed1417.Any(item2 => descript.ToUpper().StartsWith(item2.ToUpper()));
        //        if ((bres1 && (bclass2 || bclass3)) ||
        //            ((bres1 || bres2) && bclass1) ||
        //            bmed1)
        //        {
        //            Program.VerboseAudit("====Med found: bucket count B====");
        //            Program.VerboseAudit("med code: " + item.CODE + " desc: " + item.DESCRIPTION + "  evdt: " + item.EVENT_DATETIME.ToString());
        //            //var query2 = (from item2 in query select new { item2.EVENT_DATETIME, item2.CODE });
        //            //var query3 = from item3 in query2
        //            //             select new
        //            //             {
        //            //                 bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item3.EVENT_DATETIME) / _bucket_size),
        //            //                 code = item3.CODE,
        //            //                 evdt = item3.EVENT_DATETIME
        //            //             };
        //            //foreach (var itemx in query3)
        //            //{
        //            //    var b = new gBucket();
        //            //    b.bucket = itemx.bucket;
        //            //    b.code = itemx.code;
        //            //    b.evdt = itemx.evdt;
        //            //    bucket_list.Add(b);
        //            //}
        //            var b = new gBucket();
        //            b.bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size);
        //            b.code = item.CODE;
        //            b.evdt = item.EVENT_DATETIME;
        //            b.has_all_deps = true;
        //            gBucket f = bucket_list.Find(x => x.evdt == item.EVENT_DATETIME);
        //            if (f.evdt != item.EVENT_DATETIME) bucket_list.Add(b);
        //        }
        //    }
        //}

        //private void AddMedBucketsDrugClass33(List<gBucket> bucket_list)
        //{
        //    string descript = "";
        //    string drugclass = "";
        //    string rte = "";
        //    string result = "";

        //    bool bres1 = false;
        //    bool bres2 = false;
        //    bool bclass1 = false;
        //    bool bclass2 = false;
        //    bool bclass3 = false;
        //    bool bmed1 = false;

        //    var query = StartNewQuery(SearchDepth.SearchSince13Hrs);    // add custom time range below
        //    query = query.Where(e => e.CODE.ToLower().StartsWith("med"));
        //    //query = query.Where(e => e.DESCRIPTION.Contains(";;;7;;;") || 
        //    //                         e.DESCRIPTION.Contains(";;;33;;;") || 
        //    //                         e.DESCRIPTION.Contains(";;;1;;;") || 
        //    //                         e.DESCRIPTION.Contains(";;;2;;;") || 
        //    //                         e.DESCRIPTION.Contains(";;;6;;;") || 
        //    query = query.Where(e => e.DESCRIPTION.Contains(";;;33;;;"));
        //    query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;given") || e.DESCRIPTION.ToLower().Contains(";;;syringe") || e.DESCRIPTION.ToLower().ContainsAny(meds_rate) && (e.UNIT_ID == _pat.unit_id) || e.DESCRIPTION.ToLower().Contains(";;;new"));
        //    query = query.Where(e => e.EVENT_DATETIME <= PFSUtility.MaxDateTime(_pat.unit_departure, _pat.effective_out));
        //    query = query.OrderBy(e => e.EVENT_DATETIME);
        //    //            Program.VerboseAudit("Num Meds found: " + query.Count());
        //    foreach (var item in query)
        //    {
        //        var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
        //        if (arr.GetUpperBound(0) >= 3)
        //        {
        //            descript = arr[0];
        //            drugclass = arr[1];
        //            rte = arr[3];
        //            result = "";
        //            if (arr.Length == 5) result = arr[4];
        //        }
        //        //Program.VerboseAudit("med desc: " + descript + "//" + "med class: " + drugclass + "//" + "med result: " + result);
        //        //exclude #29, 35, 48
        //        bres1 = false;
        //        bres2 = false;
        //        bclass1 = false;
        //        bclass2 = false;
        //        bclass3 = false;
        //        bmed1 = false;

        //        bres1 = result.ToLower().Contains("given");
        //        bclass3 = (drugclass == "33") && (rte.ToLower() == "sc");
        //        if (bres1 && bclass3)
        //        {
        //            Program.VerboseAudit("====Med found: Drug class 33 with Route=SC ==direct trigger for q4==");
        //            Program.VerboseAudit("med code: " + item.CODE + " desc: " + item.DESCRIPTION + "  evdt: " + item.EVENT_DATETIME.ToString());
        //            SetInd(15, "Med class 33");
        //        }
        //    }

        //    // get the chart items for the assessments
        //    query = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
        //    query = query.Where(e => e.CODE.ToLower().StartsWith("med"));
        //    //query = query.Where(e => e.DESCRIPTION.Contains(";;;7;;;") || e.DESCRIPTION.Contains(";;;33;;;") || e.DESCRIPTION.Contains(";;;1;;;") || e.DESCRIPTION.Contains(";;;2;;;") || e.DESCRIPTION.Contains(";;;6;;;"));
        //    query = query.Where(e => e.DESCRIPTION.Contains(";;;33;;;"));
        //    query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;given") || e.DESCRIPTION.ToLower().Contains(";;;syringe") || e.DESCRIPTION.ToLower().ContainsAny(meds_rate) && (e.UNIT_ID == _pat.unit_id) || e.DESCRIPTION.ToLower().Contains(";;;new"));
        //    query = query.Where(e => e.EVENT_DATETIME <= PFSUtility.MaxDateTime(_pat.unit_departure, _pat.effective_out));
        //    query = query.OrderBy(e => e.EVENT_DATETIME);
        //    //            Program.VerboseAudit("Num Meds found: " + query.Count());
        //    foreach (var item in query)
        //    {
        //        var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
        //        if (arr.GetUpperBound(0) >= 3)
        //        {
        //            descript = arr[0];
        //            drugclass = arr[1];
        //            rte = arr[3];
        //            result = "";
        //            if (arr.Length == 5) result = arr[4];
        //        }
        //        //Program.VerboseAudit("med desc: " + descript + "//" + "med class: " + drugclass + "//" + "med result: " + result);
        //        //exclude #29, 35, 48
        //        bres1 = false;
        //        bres2 = false;
        //        bclass1 = false;
        //        bclass2 = false;
        //        bclass3 = false;
        //        bmed1 = false;

        //        bres1 = result.ToLower().Contains("given");
        //        bclass3 = (drugclass == "33") && (rte.ToLower() == "sc");
        //        if (bres1 && bclass3)
        //        {
        //            Program.VerboseAudit("====Med found: Drug class 33 with Route=SC ==adding to freq eval==");
        //            Program.VerboseAudit("med code: " + item.CODE + " desc: " + item.DESCRIPTION + "  evdt: " + item.EVENT_DATETIME.ToString());
        //            //var query2 = (from item2 in query select new { item2.EVENT_DATETIME, item2.CODE });
        //            //var query3 = from item3 in query2
        //            //             select new
        //            //             {
        //            //                 bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item3.EVENT_DATETIME) / _bucket_size),
        //            //                 code = item3.CODE,
        //            //                 evdt = item3.EVENT_DATETIME
        //            //             };
        //            //foreach (var itemx in query3)
        //            //{
        //            //    var b = new gBucket();
        //            //    b.bucket = itemx.bucket;
        //            //    b.code = itemx.code;
        //            //    b.evdt = itemx.evdt;
        //            //    bucket_list.Add(b);
        //            //}
        //            var b = new gBucket();
        //            b.bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size);
        //            b.code = item.CODE;
        //            b.evdt = item.EVENT_DATETIME;
        //            b.has_all_deps = true;
        //            gBucket f = bucket_list.Find(x => x.evdt == item.EVENT_DATETIME);
        //            if (f.evdt != item.EVENT_DATETIME) bucket_list.Add(b);
        //        }
        //    }
        //}


        private int CountBuckets(List<gBucket> bucket_list)       // bucket list: (ha ha)
        {
            //int result = bucket_list.Distinct().Count();
            //if (result > 0) Program.VerboseAudit(result + " unique");
            //return result;
            int x = -99;
            int result = 0;
            //int result = bucket_list.Distinct().Count();
            var query = from b in bucket_list
                        orderby b.bucket ascending
                        select b;
            foreach (var b in query)
            {
                if (x != b.bucket)
                {
                    result++;
                    x = b.bucket;
                }
            }
            if (result > 0) Program.VerboseAudit(result + " unique");
            return result;
        }
        private void Check_19()
        {
            string reslist;
            string codelist;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("19. Vascular Access Site Mgt q1 Hour");
            Program.VerboseAudit("---------------");

            SetBucketSize(60);
            exclude_periop_data = true;

            //add freq constraint q2 for <=6   q1 for >6

            SetIndIfResultContains(19, "", "304239461", "", "", "");//pressures imply q30 safety
            SetIndIfResultContains(19, "", "304239462", "", "", "");
            SetIndIfResultContains(19, "", "304239463", "", "", "");

            List<gBucket> buckets;
            buckets = new List<gBucket>();
            codelist = "304239661,304239662,304239663"; //,304239664";
            codelist += ",304239666,304239500,304239501,3041333101";
            AddBuckets(buckets, "", codelist, "", "");
            //reslist = "Blanchable Erythema,Blistered,Bleeding,Cool to touch,Crepitus";
            //reslist += ",Draining,Ecchymotic,Edema,Swelling,Erythema,Extravasated,Hematoma";
            //reslist += ",Hyperpigmentation,Hypopigmentation,Induration,Infiltrated,Leaking";
            //reslist += ",Macerate,Moist,Painful to touch,Phlebitis,Pink,Tender to touch";
            //reslist += ",Warm to touch";
            reslist = "";
            AddBuckets(buckets, "", "304239660", "", "", reslist);
            AddBuckets(buckets, "", "304237096770", "Line Care-Neuro External", "", "");
            bool ret = AnalyzeBuckets(buckets, 17, 60, "Vascular Access", false);
            if (ret)
                SetInd(19, "Vascular Access at q1 or more frequent.");

            if (_inds[19].is_checked) return;

            //"Via High Risk Meds extract file:

            //Trigger Indicator #19 ""Vascular Access Site Management q1 hour"" (2 parts):

            //1) Trigger indicator if List #1 medications administered through any route.

            //2) Trigger indicator only if List #2 medications are administered through route of peripheral IV.
            //"
            //LIST #1 - This medication list to trigger indicator if medication administered through any route:

            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = query.Where(e => e.CODE.ToUpper().StartsWith("MED"));
            query = query.Where(e => e.ORDER_ID != "");
            query = query.OrderBy(e => e.EVENT_DATETIME);
            var queryb = (from item in query
                          select new { code = item.CODE, desc = item.DESCRIPTION, orderid = item.ORDER_ID, evdt = item.EVENT_DATETIME }
                           ).Distinct();
            int ct = queryb.Count();

            if (ct > 0)
            {
                MedChartItem[] arr = new MedChartItem[queryb.Count()];
                int counter = 0;
                string s = "";
                foreach (var med in queryb)
                { //check med orders
                    var q2 = StartNewQuery(SearchDepth.SearchSinceAdmission);
                    q2 = q2.Where(e => e.CODE.ToUpper().StartsWith("HIACUMED"));
                    q2 = q2.Where(e => e.FIELD_NAME == "1"); //List 1 med
                    q2 = q2.Where(e => e.ORDER_ID == med.orderid);
                    if (q2.Count() > 0)
                    {
                        counter++;
                        arr[counter - 1].code = med.code;
                        arr[counter - 1].orderid = med.orderid;
                        arr[counter - 1].evdt = med.evdt;
                        SetInd(19, "High Acuity Med List 1:" + med.desc + "//" + med.evdt + " OrderID=" + med.orderid);
                    }
                }

                //SetIndIfResultContains(20, "", "HIACUMED", "", "", "");
            }


            //xupdfor MAR            bool found = false;
            //var query = StartNewQuery(SearchDepth.SearchDefault);
            //query = query.Where(e => e.CODE.StartsWith("hiacumed"));
            //query = query.Where(e => e.FIELD_NAME == "1");
            //int ct = query.Count();
            //foreach (var item in query)
            //{
            //    if (!found)
            //    {
            //        SetInd(19, "High Acuity Med List 1:" + item.DESCRIPTION + "  Total found="+ct);
            //        found = true;
            //    }
            //}


            // DONT Use List2 Meds at all.  No support for PERIPHERAL IV
            //found = false;
            //var query2 = StartNewQuery(SearchDepth.SearchDefault);
            //query2 = query2.Where(e => e.CODE.StartsWith("hiacumed"));
            //query2 = query2.Where(e => e.FIELD_NAME == "2");
            //ct = query2.Count();
            //foreach (var item in query2)
            //{
            //    if (!found)
            //    {
            //        Program.VerboseAudit("High Acuity Med in LIST2:" + item.DESCRIPTION + "  Total found=" + ct);
            //        found = true;
            //    }
            //}

        }

        private void CheckRouteCath()
        {
            int ct = 0;
            //OBX|1|DT|MED9003 ^ ALTEPLASE 50 MG INTRAVENOUS SOLUTION;;;20;;;0.5;;;cath; ; ; RateVerify | RateVerify | 20190802000000 |||||| F ||| 20190802000000
            //OBX|1|DT|MED9003 ^ HEPARIN  MG INTRAVENOUS SOLUTION;;;20;;;0.5;;;cath; ; ; RateVerify | RateVerify | 20190802000000 |||||| F ||| 20190802000000
            var query = StartNewQuery(SearchDepth.SearchSince9Hrs);    // add custom time range below
            query = query.Where(e => e.CODE.StartsWith("med"));
            query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;cath"));
            query = query.Where(e => e.DESCRIPTION.ToUpper().Contains("ALTEPLASE") ||
                                     e.DESCRIPTION.ToUpper().Contains("HEPARIN"));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            Program.VerboseAudit("Med ALTEPLASE or HEPARIN with cath found: " + query.Count());
            foreach (var item in query)
            {
                Program.VerboseAudit("====Med found: ALTEPLASE or HEPARIN====");
                Program.VerboseAudit("med code: " + item.CODE + " desc: " + item.DESCRIPTION + "  evdt: " + item.EVENT_DATETIME.ToString());

                var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                if (arr.GetUpperBound(0) >= 3)
                {
                    string rte = arr[3];
                    if (rte.ToLower() == "cath") ct++;
                }
            }
            if (ct > 0)
                SetInd(19, "Found ALTEPLASE or HEPARIN with route cath.");
        }

        private void Check_20()
        {
            string reslist;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("20. Medication Activity >= 20 minutes");
            Program.VerboseAudit("---------------");

            // IMPORTANT:  Meds are pre-filtered by the sending interface to ONLY include the positive MAR actions of
            //  Given, NewBag, RateVerify, etc. We only receive these MAR actions.

            string[] MEE_meds = {"apraclonidine","bacitracin","balanced salt solution",
"ciprofloxacin-dexamethasone","dexmedetomidine","ephedrine","fentanyl","lidocaine",
"midazolam","mineral oil","morphine","oxymetazoline","phenylephrine","propofol",
"rocuronium","remifentanil" };

            exclude_periop_data = true; 
            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = query.Where(e => e.CODE.ToUpper().StartsWith("MED"));
            if (_pat.short_name.ToUpper() == "MEE")
                query = query.Where(e => !e.DESCRIPTION.ToLower().ContainsAny(MEE_meds));
            query = query.Where(e => e.ORDER_ID != "");
            query = query.OrderBy(e => e.EVENT_DATETIME);
            var queryb = (from item in query
                          select new { code = item.CODE, desc = item.DESCRIPTION, orderid = item.ORDER_ID, evdt = item.EVENT_DATETIME }
                           ).Distinct();
            int ct = queryb.Count();

            foreach (var med in queryb)
            { //check med orders
                Program.VerboseAudit("*20*desc="+med.desc);
            }

            if (ct > 0)
            {
                MedChartItem[] arr = new MedChartItem[queryb.Count()];
                int counter = 0;
                string s = "";
                foreach (var med in queryb)
                { //check med orders
                    var q2 = StartNewQuery(SearchDepth.SearchSinceAdmission);
                    q2 = q2.Where(e => e.CODE.ToUpper().StartsWith("HIACUMED"));
                    q2 = q2.Where(e => e.ORDER_ID == med.orderid);
                    if (q2.Count() > 0)
                    {
                        counter++;
                        arr[counter - 1].code = med.code;
                        arr[counter - 1].orderid = med.orderid;
                        arr[counter - 1].evdt = med.evdt;
                        Program.VerboseAudit("Hi Acu med desc: " + med.desc + "//" + med.evdt);
                    }
                }
                //          if (s.Left(1) == ",") s = s.Substring(1);
                if (counter > 0) SetInd(20, "Found High Acuity Med.");
                //SetIndIfResultContains(20, "", "HIACUMED", "", "", "");
            }

            if (_pat.age < 6.0)
            {
                bool found_nebu = false;
                foreach (var med in queryb)
                { //check med orders
                    if (!found_nebu)
                        if (med.desc.ToLower().Contains(";;nebulization"))
                        {
                            SetInd(20, "Nebulization found for patient age=" + _pat.age + " med="+med.desc + " at="+med.evdt);
                            found_nebu = true;
                        }
                }
            }

            ct = CountMedsIn30min(MEE_meds);
            if (ct >= 8) SetInd(20, "Med count >=8 in 30 mins=" + ct);

            reslist = "";
            //SetIndIfResultContains(20, "", EXACT_MATCH_PREFIX+ "8048", "", "", reslist);
            SetIndIfResultContains(20, "", EXACT_MATCH_PREFIX + "58670", "", "", reslist);
            //SetIndIfResultContains(20, "", EXACT_MATCH_PREFIX + "58665", "", "", reslist);
            SetIndIfResultContains(20, "", EXACT_MATCH_PREFIX + "58677", "", "", reslist);
            //SetIndIfResultContains(20, "", EXACT_MATCH_PREFIX + "8052", "", "", reslist);
            SetIndIfResultContains(20, "", EXACT_MATCH_PREFIX + "58669", "", "", reslist);
            //SetIndIfResultContains(20, "", EXACT_MATCH_PREFIX + "8056", "", "", reslist);
            SetIndIfResultContains(20, "", EXACT_MATCH_PREFIX + "58667", "", "", reslist);
            //SetIndIfResultContains(20, "", EXACT_MATCH_PREFIX + "1376", "", "", reslist);
            SetIndIfResultContains(20, "", EXACT_MATCH_PREFIX + "58666", "", "", reslist);
            //SetIndIfResultContains(20, "", EXACT_MATCH_PREFIX + "66637", "", "", reslist);
            SetIndIfResultContains(20, "", EXACT_MATCH_PREFIX + "66651", "", "", reslist);
            //SetIndIfResultContains(20, "", EXACT_MATCH_PREFIX + "66639", "", "", reslist);
            SetIndIfResultContains(20, "", EXACT_MATCH_PREFIX + "66653", "", "", reslist);
            //SetIndIfResultContains(20, "", EXACT_MATCH_PREFIX + "60000", "", "", reslist);
            SetIndIfResultContains(20, "", EXACT_MATCH_PREFIX + "60001", "", "", reslist);
            //SetIndIfResultContains(20, "", EXACT_MATCH_PREFIX + "89347", "", "", reslist);
            //SetIndIfResultContains(20, "", EXACT_MATCH_PREFIX + "387212", "", "", reslist);
            SetIndIfResultContains(20, "", EXACT_MATCH_PREFIX + "58672", "", "", reslist);
            SetIndIfResultContains(20, "", "3041900035", "", "", reslist);
            SetIndIfResultContains(20, "", "3041900183", "", "", reslist);
            reslist = "Crushed,applesauce,ice cream,pudding,puree,One at a time with supervision";
            SetIndIfResultContains(20, "", "3042376734", "", "", reslist);

            //string desclist = "Bolus,Chemo,Continued,Given,Independent,Inserted,NewBag,Patch,RateChange,Started";
            Program.VerboseAudit("Searching for Meds with specified routes in combination with GTube....");
            if (Exists("", "MED", "Mouth/Throat,Oral,Gastrostomy,Nasogastric,Jejunostomy,Orogastric,Nasal-Jejunal,Per Gavage", "", ""))
            {
                if (Exists("", "304239397&", "", "", "", SearchDepth.SearchSince24Hrs))
                    SetInd(20, "GI Tube Status charted within 24 hrs with at least one MED.");
                else
                {
                    CheckLDA("GI Tube".ToUpper(), "900700", "900701", "900702", "900703");
                    if (g_gitube)
                        SetInd(20, "GI/GJ Tube placed with at least one MED.");
                }
            }

            ct = CountIntravenous(MEE_meds);
            if (ct >= 3) SetInd(20, "Intravenous Med count in 8 hours=" + ct);
        }

        private void CheckEyeDrops()
        {
            //OBX|1|DT|MEDNOEICUID ^ AMPHOTERICIN B 1.5 MG / ML(0.15 %) OPHTHALMIC SOLUTION (CNR);;;13;;;1;;;left eye;;;Given | Given | 20190731145800 |||||| F ||| 20190731145800
            //Include when 5 results of Given when the drug route found in OBX 3 after the third; ; ; is one of the options under code
            //Both eyes,Left eye,Right eye,Opht
            string[] eyeroute = { "Both eyes", "Left eye", "Right eye", "Opht" };

            int ct = 0;
            var query = StartNewQuery(SearchDepth.SearchSince9Hrs);    // add custom time range below
            query = query.Where(e => e.CODE.StartsWith("med"));
            query = query.Where(e => eyeroute.Any(x =>
                e.DESCRIPTION.ToUpper().Contains(";;;" + x.ToUpper() + ";;;GIVEN")));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            Program.VerboseAudit("Meds found with EYE route: " + query.Count());
            if (query.Count() >= 5)
            {
                foreach (var item in query)
                {
                    Program.VerboseAudit("med code: " + item.CODE + " desc: " + item.DESCRIPTION + "  evdt: " + item.EVENT_DATETIME.ToString());
                    ct++;
                }
            }
            if (ct >= 5)
                SetInd(20, "Found Eye Drops med count=" + ct);
        }

        private void CheckMedGiven(int ind, string desc)
        {

            string[] transplantmed_actions = { "given", "newbag", "new bag", "rateverify", "rate verify", "rate change", "ratechange", "restarted", "started/down" };

            for (int i = 0; i <= transplantmed_actions.GetUpperBound(0); i++)
            {
                transplantmed_actions[i] = ";;;" + transplantmed_actions[i];
            }

            int ct = 0;
            var query = StartNewQuery(SearchDepth.SearchSince9Hrs);
            query = AndItemFilter(query, "", "MED", desc, "", "");
            query = query.Where(e => e.DESCRIPTION.ToLower().ContainsAny(transplantmed_actions));
            ct = query.Count();
            if (ct > 0) SetInd(ind, "Found Med=" + desc);

        }

        private int CountMedsIn30min(string[] MEE_meds)
        {
            string descript = "";
            string drugclass = "";
            string result = "";
            int ct = 0;
            DateTime evdt = DateTime.MinValue;
            DateTime endevdt = DateTime.MinValue;

            //  CALCIUM CHLORIDE 100 MG / ML(10 %) INTRAVENOUS SYRINGE; ; ; 29; ; ; 1; ; ; IV; ; ; Given
            //  SODIUM BICARBONATE 8.4 % (1 MEQ / ML) INJECTION(WRAPPER); ; ; 29; ; ; 50; ; ; osse; ; ; Given
            var query = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
            query = query.Where(e => e.CODE.ToUpper().StartsWith("MED"));
            if (_pat.short_name.ToUpper() == "MEE")
                query = query.Where(e => !e.DESCRIPTION.ToLower().ContainsAny(MEE_meds));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            //Program.VerboseAudit("Num Meds found: " + query.Count());
            foreach (var item in query)
            {
                Program.VerboseAudit("Med=" + item.EVENT_DATETIME + ": " + item.DESCRIPTION);
                if (item.EVENT_DATETIME > evdt)
                {
                    evdt = item.EVENT_DATETIME;
                    endevdt = evdt.AddMinutes(30);
                    if (endevdt > loc_out) endevdt = loc_out;
                    var query2 = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
                    query2 = query2.Where(e => e.CODE.ToUpper().StartsWith("MED"));
                    if (_pat.short_name.ToUpper() == "MEE")
                        query2 = query2.Where(e => !e.DESCRIPTION.ToLower().ContainsAny(MEE_meds));
                    query2 = query2.Where(e => e.EVENT_DATETIME >= evdt && e.EVENT_DATETIME <= endevdt);
                    if (query2.Count() > ct)
                        ct = query2.Count();
                }
            }

            Program.VerboseAudit("Greatest count of unique Admin times in a 30 minute period=" + ct);
            return ct;
        }
        private int CountIntravenous(string[] MEE_meds)
        {
            int ct = 0;
            int count = 0;
            DateTime evdt = DateTime.MinValue;
            DateTime endevdt = DateTime.MinValue;

            var query = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
            query = query.Where(e => e.CODE.ToUpper().StartsWith("MED"));
            query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;intravenous"));
            if (_pat.short_name.ToUpper() == "MEE")
                query = query.Where(e => !e.DESCRIPTION.ToLower().ContainsAny(MEE_meds));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            //Program.VerboseAudit("Num Meds found: " + query.Count());
            foreach (var item in query)
            {
                Program.VerboseAudit("IV Med=" + item.EVENT_DATETIME + ": " + item.DESCRIPTION);
                if (item.EVENT_DATETIME > evdt)
                {
                    evdt = item.EVENT_DATETIME;
                    endevdt = evdt.AddHours(8);
                    if (endevdt > loc_out) endevdt = loc_out;
                    var query2 = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
                    query2 = query2.Where(e => e.CODE.ToUpper().StartsWith("MED"));
                    query2 = query2.Where(e => e.DESCRIPTION.ToLower().Contains(";;intravenous"));
                    if (_pat.short_name.ToUpper() == "MEE")
                        query2 = query2.Where(e => !e.DESCRIPTION.ToLower().ContainsAny(MEE_meds));
                    query2 = query2.Where(e => e.EVENT_DATETIME >= evdt && e.EVENT_DATETIME <= endevdt);
                    count = query2.Count();
                    if (count > ct)
                        ct = count;
                }
            }

            Program.VerboseAudit("Greatest count of IV Admin times in an 8-hour period=" + ct);
            return ct;
        }


        private void Check_21_22()
        {
            string reslist;
            bool st1 = false;
            string piv;
            string codelist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("21. Wound/Injury Mgmt");
            Program.VerboseAudit("22. Wound/Injury Mgmt >= 30 Minutes");
            Program.VerboseAudit("---------------");

            exclude_periop_data = false;

            //description not contains 'peripheral iv'
            exclude_periph_iv = true;

            reslist = "";
            SetIndIfResultContains(21, "", "304239073", "", "", reslist);
            reslist = "Bright Red,Brown,Clear,Clots,Malodorous,Normal show,Pink,Other";
            SetIndIfResultContains(21, "", "304239074", "", "", reslist);
            reslist = "Heavy,Moderate,Scant,Spotting,Pad 25% Saturated,Pad 50% Saturated,Pad 75% Saturated,Pad 100% Saturated";
            SetIndIfResultContains(21, "", "304239075", "", "", reslist);
            //reslist = "Wound cleanser,Granulex spray";
            //SetIndIfResultContains(21, "", "304239076", "", "", reslist);
            reslist = "Applied";
            SetIndIfResultContains(21, "", "304239077", "", "", reslist);
            reslist = "Applied";
            SetIndIfResultContains(21, "", "304239078", "", "", reslist);
            reslist = "Bleeding,Draining,Extravasated,Hematoma,Leaking,Infiltrated,Phlebitis";
            SetIndIfResultContains(21, "", "304239662", "", "", reslist);
            reslist = "Antimicrobial patch,Gauze,Occlusive,Pressure dressing,Medicinal with wrap,Non-Occlusive device,Non-applicable/No drainage,Sterile,Other";
            SetIndIfResultContains(21, "", "304239079", "", "", reslist);
            reslist = AVOID_NEGATIVE + "Trauma/injury";
            SetIndIfResultContains(21, "", "304239080", "", "", reslist);
            reslist = AVOID_NEGATIVE + "Swelling";
            SetIndIfResultContains(21, "", "304239080", "", "", reslist);
            reslist = "Boggy,Breakdown,Bulging,Firm,Sunken,Pulsating,Helmet use,Other";
            SetIndIfResultContains(21, "", "304239081", "", "", reslist);
            reslist = "Foreign body,Trauma/Injury,Chemical Exposure,Drainage,Edema";
            SetIndIfResultContains(21, "", "304239351", "", "", reslist);
            reslist = "Foreign body,Trauma/Injury,Chemical Exposure,Drainage,Edema";
            SetIndIfResultContains(21, "", "304239352", "", "", reslist);
            reslist = "Abrasions,External trauma,Lacerations,Foreign body,Trauma/injury";
            SetIndIfResultContains(21, "", "304239353", "", "", reslist);
            reslist = "Abrasions,External trauma,Lacerations,Foreign body,Trauma/injury";
            SetIndIfResultContains(21, "", "304239354", "", "", reslist);
            reslist = "Abrasion,Epistaxis,Foreign body,Laceration,Trauma/Injury";
            SetIndIfResultContains(21, "", "304239082", "", "", reslist);
            reslist = "Laceration,Lesion";
            SetIndIfResultContains(21, "", "304239083", "", "", reslist);
            reslist = "Bleeding,Lesions";
            SetIndIfResultContains(21, "", "304239084", "", "", reslist);
            reslist = "Blistered,Cracked,Lacerated,Trauma/injury";
            SetIndIfResultContains(21, "", "304239085", "", "", reslist);
            reslist = "Bleeding,Blistered,Lesions,Mucositis/Stomatitis,Ulcerations";
            SetIndIfResultContains(21, "", "304239086", "", "", reslist);
            reslist = "Grade 1 Asymptomatic or mild symptoms, intervention not indicated,Grade 2 Moderate pain, not interfering with oral intake, modified diet indicated,Grade 3 Severe pain, interfering with oral intake,Grade 4 Life-threatening consequences, urgent intervention indicated,Grade 5 Death";
            SetIndIfResultContains(21, "", "304239087", "", "", reslist);
            reslist = "Abscess,Trauma/Injury";
            SetIndIfResultContains(21, "", "304239088", "", "", reslist);
            reslist = "Trauma/injury,Swollen";
            SetIndIfResultContains(21, "", "304239089", "", "", reslist);
            reslist = "Bloody,Blood Tinged,Pink tinged";
            SetIndIfResultContains(21, "", "304239090", "", "", reslist);
            reslist = "Bloody,Black flecked,Pink tinged";
            SetIndIfResultContains(21, "", "304239091", "", "", reslist);
            reslist = "Blanchable Erythema,Bleeding,Blistered,Drainage,Edema/Swelling,Erythema,Hematoma,Macerate,Oozing secretions,Subcutaneous emphysema,Sutures present,Swelling";
            SetIndIfResultContains(21, "", "304239092", "", "", reslist);
            reslist = "Cleansed,Dressing applied";
            SetIndIfResultContains(21, "", "304239093", "", "", reslist);
            reslist = "Abrasion,Blister,Burn,Cracking,Excoriation,Laceration,Rash,Tear";
            SetIndIfResultContains(21, "", "304239094", "", "", reslist);
            reslist = "Lesion,Weeping";
            SetIndIfResultContains(21, "", "304239095", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239096", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239097", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239098", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239099", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239100", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239101", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239102", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239103", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239104", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239105", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239106", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239107", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239108", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239109", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239110", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239111", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239112", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239113", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239114", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239115", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239116", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239117", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239118", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239119", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239120", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239121", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239122", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239123", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239124", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239125", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239126", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239127", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239128", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239129", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239130", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239131", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239132", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239133", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239134", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239135", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239136", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239137", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239138", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239139", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239140", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239141", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239142", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239143", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239144", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239145", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239146", "", "", reslist);
            reslist = "Injury/trauma,Surgical site";
            SetIndIfResultContains(21, "", "304239147", "", "", reslist);
            reslist = "Black,Coffee ground,Hematesmesis,Red";
            SetIndIfResultContains(21, "", "304239148", "", "", reslist);
            reslist = "Bleeding,Excoriated,Macerated,Blistered,Drainage at site,Erythema";
            SetIndIfResultContains(21, "", "304239149", "", "", reslist);
            reslist = "Alginate,Dry dressing,Foam,Hydrocolloid,Moist to dry,Moist to moist,Moisture barrier,Non adherent,Papain-urea debridement,Pressure dressing,Protective barrier,Split gauze,Special type,Triad hydro,Vacuum based,Water based,Wound gel,Xeroform";
            SetIndIfResultContains(21, "", "304239150", "", "", reslist);
            //reslist = "Bile,Bloody,Bright red,Brown,Clear,Clots,Coffee ground,Dark red,Fecal,Green,Pink tinged,Tan,Yellow,Undigested contents,Other";
            reslist = "Bloody,Bright red,Brown,Clots,Coffee ground,Dark red,Fecal,Pink tinged,Other";
            SetIndIfResultContains(21, "", "304239151", "", "", reslist);
            reslist = "Bloody";
            SetIndIfResultContains(21, "", "304239152", "", "", reslist);
            reslist = "Black,Red,Red streaks,Melena";
            SetIndIfResultContains(21, "", "304239153", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239154", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239155", "", "", reslist);
            reslist = "Pouch system: changed,Irrigation";
            SetIndIfResultContains(21, "", "304239156", "", "", reslist);
            reslist = "Blood clots,Red flecks";
            SetIndIfResultContains(21, "", "304239157", "", "", reslist);
            reslist = "Injury/trauma,Lesion,Swelling";
            SetIndIfResultContains(21, "", "304239158", "", "", reslist);
            reslist = "Injury/trauma,Lesion,Swelling";
            SetIndIfResultContains(21, "", "304239159", "", "", reslist);
            reslist = "Injury/trauma,Lesion,Swelling";
            SetIndIfResultContains(21, "", "304239160", "", "", reslist);
            reslist = "Injury/trauma,Lesion,Swelling";
            SetIndIfResultContains(21, "", "304239161", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239162", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239163", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239164", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239165", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "30423600305", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239167", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239168", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239169", "", "", reslist);
            reslist = "Clean,Dry,Intact,Bleeding,Black,Brown,Dark edges,Dusky,Edematous,Excoriated,Fragile,Hyper-granulation tissue,Light purple,Malodorous,Pale,Pink,Red,Tan,White,Yellow,Other";
            SetIndIfResultContains(21, "", "304239170", "", "", reslist);
            reslist = "Clean,Dry,Intact skin,Black,Bleeding,Boggy,Brown,Calloused,Crepitus,Dark edges,Denuded,Edema,Ecchymosis,Excoriated,Erythema: blanchable,Erythema: non-blanchable,Fragile,Hematoma,Hyperpigmented,Induration,Maceration,Pale,Pink,Purple,Red,Tan,White,Yellow,Other";
            SetIndIfResultContains(21, "", "304239171", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239172", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239173", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239174", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239175", "", "", reslist);
            reslist = "Autograft taken,Bleeding,Dusky,Edematous,Epithelialized,Erythema: blanching,Erythema: non-blanching,Eschar,Excoriated,Fragile,Fully granulated,Hematoma,Hypergranulation tissue,Malodorous,Necrotic,Partial granulation,Peeling,Pink,Skin buds/Capillary buds,Sloughing,White,Yellow,Other";
            SetIndIfResultContains(21, "", "304239176", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239177", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239178", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239179", "", "", reslist);
            reslist = "unable to assess";
            SetIndIfResultDoesNotContain(21, "", "304239180", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239181", "", "", reslist);
            reslist = "New drainage,Changed,Reinforced";
            SetIndIfResultContains(21, "", "304239182", "", "", reslist);

            reslist = "Blistered,Bleeding,Ecchymotic,Erythema,Extravasated,Macerate,Oozing,Other";
            SetIndIfResultContains(21, "", "304239665", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239183", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239563", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239667", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239565", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239184", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239185", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239186", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239187", "", "", reslist);
            reslist = "Bleeding,Blisters,Cool to touch,Drainage,Ecchymotic";
            reslist += ",Edema/Swelling,Erythema,Excoriated,Hematoma,Hyperpigmentation";
            reslist += ",Hypopigmentation,Induration,Macerated,Moist,Painful to touch";
            reslist += ",Pink,Tender,Warm to touch,Other";
            SetIndIfResultContains(21, "", "304239188", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239534", "", "", reslist);
            reslist = "Blanchable Erythema,Blistered,Bleeding,Cool to touch,Crepitus";
            reslist += ",Draining,Ecchymotic,Edema,Swelling,Erythema,Extravasated,Hematoma";
            reslist += ",Hyperpigmentation,Hypopigmentation,Induration,Infiltrated,Leaking";
            reslist += ",Macerate,Moist,Painful to touch,Phlebitis,Pink,Tender to touch";
            reslist += ",Warm to touch";
            SetIndIfResultContains(21, "", "304239660", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239189", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239190", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239191", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239192", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239663", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239666", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239661", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239664", "", "", reslist);
            reslist = "Abrasion,Blister,Desquamation,Ecchymosis,Forceps Mark,Gelatinous,Laceration,Rash,Rash, Diaper,Vacuum Mark";
            SetIndIfResultContains(21, "", "304239193", "", "", reslist);
            reslist = "Red,Excoriation,Plaque,Pustules,Papules,Bleeding";
            SetIndIfResultContains(21, "", "304239194", "", "", reslist);
            reslist = "Trauma/injury,Drainage,Redness";
            SetIndIfResultContains(21, "", "304239195", "", "", reslist);
            reslist = "Trauma/injury,Drainage,Redness";
            SetIndIfResultContains(21, "", "304239196", "", "", reslist);
            double agedays = PFSUtility.DateDiffInDays(_pat.dob, loc_out);
            if (agedays < 8)
                {
                Program.VerboseAudit("age in days=" + agedays);
                reslist = "Dry,Moist,Clamp off,Clamp on,2 cord vessels,3 cord vessels,Periumbilical Erythema,Drainage,Gelatinous,Malodorous,Other";
                    SetIndIfResultContains(21, "", "304239197", "", "", reslist);
                }
            reslist = "";
            SetIndIfResultContains(21, "", "304239198", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239199", "", "", reslist);
            //reslist = "Bloody,Coffee ground,Red";
            //SetIndIfResultContains(21, "", "304239200", "", "", reslist);
            reslist = "Blood tinged";
            SetIndIfResultContains(21, "", "304239201", "", "", reslist);
            reslist = "Not intact,White patches";
            SetIndIfResultContains(21, "", "304239202", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239203", "", "", reslist);
            reslist = "Trauma/injury,Foreign Body";
            SetIndIfResultContains(21, "", "304239204", "", "", reslist);
            reslist = "Trauma/injury,Bleeding,Lesions";
            SetIndIfResultContains(21, "", "304239205", "", "", reslist);
            reslist = "Yes";
            SetIndIfResultContains(21, "", "304239206", "", "", reslist);
            reslist = "Plugged duct,Red";
            SetIndIfResultContains(21, "", "304239207", "", "", reslist);
            reslist = "Plugged duct,Red";
            SetIndIfResultContains(21, "", "304239208", "", "", reslist);
            reslist = "Bleeding,Blisters,Cracked";
            SetIndIfResultContains(21, "", "304239209", "", "", reslist);
            reslist = "Bleeding,Blisters,Cracked";
            SetIndIfResultContains(21, "", "304239210", "", "", reslist);
            reslist = "Alba,Rubra,Serosa";
            SetIndIfResultContains(21, "", "304239211", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239212", "", "", reslist);
            reslist = "Yes";
            SetIndIfResultContains(21, "", "304239213", "", "", reslist);
            reslist = "Yes";
            SetIndIfResultContains(21, "", "304239214", "", "", reslist);
            reslist = "Separation,Other";
            SetIndIfResultContains(21, "", "304239005", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239006", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239007", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239008", "", "", reslist);
            reslist = "Copious,Large,Moderate,Small,Scant,None,Other";
            SetIndIfResultContains(21, "", "304239009", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239010", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239011", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239012", "", "", reslist);
            reslist = "Adhesive strips,Staples,Surgical skin adhesive,Sutures,Retention sutures,Staples Removed,Sutures removed,Other";
            SetIndIfResultContains(21, "", "304239013", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239014", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239015", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239016", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239017", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239018", "", "", reslist);
            reslist = "New drainage,old drainage,other";
            SetIndIfResultContains(21, "", "304239019", "", "", reslist);
            reslist = "Yes";
            SetIndIfResultContains(21, "", "304239020", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "304239021", "", "", reslist);
            SetIndIfResultContains(21, "", "3043700180", "", "", "");
            reslist = "Abdominal binder,ABductor pillow,Ace wrap,Brace,Collar,Helmet";
            reslist += ",Immobilizer,Ortho boot,Scrotal support,Sling,Splint,Sand bag";
            reslist += ",Traction,Other";
            SetIndIfResultContains(21, "", "30423101312", "", "", reslist);
            reslist = "Cervical,Aspen,Vista adjustable,Miami,Philly,Soft,Other";
            SetIndIfResultContains(21, "", "30423103830", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "3041308801", "", "", reslist);

            codelist = "3042342674,3042343086,304231301226,30423200518,304230200519,304230200541";
            codelist += ",304230200635,30423200636,30423200637,30423200929,30423200930";
            codelist += ",30423200931,30423200932,30423200933,30423200934";
            SetIndIfResultContains(21, "", codelist, "", "", reslist);
            reslist = "Sanguineous";
            SetIndIfResultContains(21, "", "304230370070", "", "", reslist);
            SetIndIfResultContains(21, "", "30423304140", "", "", "Red");

            SetIndIfResultContains(21, "", "30423142010,304237085590,3048700795", "", "", "");
            reslist = "Adhesive bandage,Biopatch,Dry,Non adherent,Pressure,Special Tape,Sterile gauze,TSM,Other";
            SetIndIfResultContains(21, "", "3048700787", "", "", reslist);
            SetIndIfResultContains(21, "", "3048716070", "", "", "");

            reslist = "Sand bag,External Compression Device";
            SetIndIfResultContains(22, "", "304234003", "", "", reslist);
            reslist = "Dressing,1 piece,2 piece,Diapering,Other";
            SetIndIfResultContains(22, "", EXACT_MATCH_PREFIX+"304239001", "", "", reslist);
            reslist = "Exposed: adipose,Exposed: bone,Exposed: hardware,Exposed: muscle,Exposed: tendon";
            SetIndIfResultContains(22, "", "304239176", "", "", reslist);
            reslist = "Sand bag,External Compression Device";
            SetIndIfResultContains(22, "", "304239002", "", "", reslist);
            if (_inds[21].is_checked)
            {
                reslist = "Yes";
                SetIndIfResultContains(22, "", "304239022", "", "", reslist);
            }

            exclude_periph_iv = false;

        }

        private void CheckDrainLDA()
        {
            string codelist = "9993040108530,3045001091";
            DateTime dt = DateTime.MinValue;
            var query1 = StartNewQuery(SearchDepth.SearchSince16Hrs);
            query1 = AndItemFilter(query1, "", codelist, "", "", "");
            query1 = query1.OrderBy(e => e.EVENT_DATETIME);
            foreach (var item in query1)
            { // for each start, find the number of records within 1 hr of it.
                if (dt == DateTime.MinValue)
                {
                    int ct = CountDrainLDAIn1Hour(codelist, item.EVENT_DATETIME);
                    if (ct >= 6)
                    {
                        SetInd(22, "Count of Drain LDAs within 1 hour=" + ct + " starting at: " + item.EVENT_DATETIME.ToString());
                        dt = item.EVENT_DATETIME;
                    }
                }
            }
            return;
        }

        private int CountDrainLDAIn1Hour(string codelist, DateTime startdt)
        {
            int ct = 0;
            var query1 = StartNewQuery(SearchDepth.SearchSince16Hrs);
            query1 = AndItemFilter(query1, "", codelist, "", "", "");
            query1 = query1.Where(e => e.EVENT_DATETIME >= startdt);
            query1 = query1.Where(e => e.EVENT_DATETIME <= startdt.AddMinutes(60));
            ct = query1.Count();
            return ct;

        }
        //private void CheckExtensiveWound()
        //{
        //    DateTime evdt = DateTime.MinValue;
        //    DateTime st_time = DateTime.MinValue;
        //    DateTime en_time = DateTime.MinValue;
        //    var query = StartNewQuery(SearchDepth.SearchDefault);
        //    query = AndCodeInList(query, "1375032519");
        //    query = AndResultInList(query, "extensive wound");
        //    query = query.OrderBy(e => e.EVENT_DATETIME);
        //    if (query.Count() == 0) return;
        //    foreach (var ci in query)
        //    {
        //        if (!_inds[22].is_checked)
        //        {
        //            evdt = ci.EVENT_DATETIME;
        //            st_time = GetResultTime("1375032547", evdt, 2);
        //            en_time = GetResultTime("1375032561", evdt, 2);
        //            if ((st_time != DateTime.MinValue) &&
        //                (en_time != DateTime.MinValue) && (st_time <= en_time))
        //            {
        //                if (st_time.AddMinutes(30) <= en_time)
        //                    SetInd(22, "Extensive Wound Management>=30min start:" + st_time.ToString() + " end:" + en_time.ToString());
        //            }
        //        }
        //    }
        //}

        // Use this if you can total the education time
        //private void CheckEducation(int total)
        //{
        //    if (_inds[23].is_checked) return;             //skip if already checked

        //    if (total >= 60) {
        //        SetInd(23, "education >= 60 min");
        //    }
        //}

        int EducMins(string timecode, string nursecode)
        {
            string res;
            int val = 0;
            if (ResultContains("", nursecode, "", "", "Nursing"))
            {
                if (GetResult("", timecode, "", "", out res))
                {
                    if (res.Left(1).IsNumeric())
                    {
                        val += (int)res.Val();
                    }
                }
            }
            return val;
        }

        private void Check_23()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("23. Healthcare Mgmt Education >= 1 Hour");
            Program.VerboseAudit("---------------");

            exclude_periop_data = false;
            string[] educodes = { "3040019666", "3040019679" };

            // this routine looks for a count of 8 items.
            //SetIndIfCodeBtwn(23, 304230700, 304235631, SearchDepth.SearchDefault);
            SetIndIfCodeBtwn(23, 304230700, 304235657, SearchDepth.SearchDefault); //5/18/21 (4/29/18 additions in Epic)

        }
        private void CheckEDUtab(string educode)
        {
            int ub, i;
            string[] desc = new string[3];
            DateTime evdt;
            string cd1, cd2, res1, res2, topic1, topic2;

            //OBX | 2 | ST | EDU3040004179METHOD ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC STROKE EVERY MINUTE MATTERS MC6074| 1 | E |||||| F ||| 20170612113300
            //OBX | 4 | ST | EDU3040004179RESPONSE ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC STROKE EVERY MINUTE MATTERS MC6074| 1 | TB |||||| F ||| 20170612113300
            //OBX | 6 | ST | EDU3040004179METHOD ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC YOU CAN IMPROVE YOUR HEALTH MC2066 - 09 | 1 | E |||||| F ||| 20170612113300
            //OBX | 8 | ST | EDU3040004179RESPONSE ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC YOU CAN IMPROVE YOUR HEALTH MC2066 - 09 | 1 | TB |||||| F ||| 20170612113300
            //OBX | 10 | ST | EDU3040004179METHOD ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC HIGH BLOOD PRESSURE (HYPERTENSION)MC5056 | 1 | E |||||| F ||| 20170612113300
            //OBX | 12 | ST | EDU3040004179RESPONSE ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC HIGH BLOOD PRESSURE (HYPERTENSION)MC5056 | 1 | TB |||||| F ||| 20170612113300
            var query1 = StartNewQuery(SearchDepth.SearchSince9Hrs);
            query1 = AndItemFilter(query1, "", "EDU" + educode + "METHOD", "", "", "E,D,I");
            query1 = query1.Where(e => e.EVENT_DATETIME <= PFSUtility.MaxDateTime(_pat.unit_departure, _pat.effective_out));
            foreach (var item1 in query1)
            {
                evdt = item1.EVENT_DATETIME;
                res1 = item1.RESULT;
                cd1 = item1.CODE;
                Program.VerboseAudit("EDU: res=" + res1 + " code=" + cd1 + " evdt=" + evdt.ToString());

                var arr = item1.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                ub = arr.GetUpperBound(0);
                for (i = 0; (i <= Math.Min(ub, 2)); i++)
                    desc[i] = arr[i];
                if (ub >= 2)
                    topic1 = desc[2].Trim();
                else
                    topic1 = "";
                var query2 = StartNewQuery(SearchDepth.SearchSince9Hrs);
                query2 = AndItemFilter(query2, "", "EDU" + educode + "RESPONSE", "", "", "IP,TB,NR");
                query2 = query2.Where(e => e.EVENT_DATETIME == evdt);
                foreach (var item2 in query2)
                {
                    res2 = item2.RESULT;
                    cd2 = item2.CODE;
                    var arr2 = item2.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                    ub = arr.GetUpperBound(0);
                    for (i = 0; (i <= Math.Min(ub, 2)); i++)
                        desc[i] = arr[i];
                    if (ub >= 2)
                        topic2 = desc[2].Trim();
                    else
                        topic2 = "";
                    if (topic1 == topic2)
                    {
                        SetInd(23, "Found EDU" + educode + ": " + desc[0] + "^" + topic1 + " at:" + evdt.ToString() + " METHOD=" + res1 + " RESPONSE=" + res2);
                        if (res1.ToUpper().Contains("I"))
                        {
                            AddEducActivity(evdt);
                        }
                    }
                }

            }

        }
        private void AddEducActivity(DateTime evdt)
        {
            DateTime enddt;
            Program.VerboseAudit("Activity 5: Found at " + evdt.AddHours(-1).ToString());
            if (!QueuedProcOverlaps(5, evdt.AddHours(-1), evdt))
                if (!ProcExistsInDB(5, evdt.AddHours(-1), out enddt))
                {
                    var proc = new proc_data();
                    proc.procedure_number = 5;
                    proc.start = evdt.AddHours(-1);
                    proc.finish = evdt;
                    _procs.Add(proc);
                    Program.Audit("Activity 5: Found at " + evdt);
                }
        }

        private void Check_24()
        {
            string reslist = "";
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("24. 1 to 1 Physiological Interv. >= 2 Hours");
            Program.VerboseAudit("---------------");

            if (_pat.short_name.ToUpper() == "MEE" && _pat.unit_name == "10 PEDI")
            {
                Program.VerboseAudit("Suppressed for unit 10 PEDI in facility MEE.");
                return;//suppress for MEE facility
            }

            exclude_periop_data = true;

            if (Exists("", "1020100031", "", "", reslist))
            {
                Program.VerboseAudit("Found Seat Tested; suppressing indicator 24.");
                return;
            }

            //8 of these in 2 hrs 
            string codelist = "304239497,304239498,304239461,304239462,304239593,304239594,304239595,304239596,304239597";
            codelist += ",304239598,304239599,304239600,304239601,304239602,304239603,304239604,304239605,304239606";
            codelist += ",304239607,304239608,304239609,304239610,304239611,304239612,304239613,304239614,304239615";
            codelist += ",304239616,304239617,304239618,304239619,304239620,304239621,304239622,304239623,304239624";
            codelist += ",304239625,304239626,304239627,304239628,304239629,304239631,304239632,304239633";
            codelist += ",304239634,304239635,304239636,304239637,304239638,304239639,304239640,304239641,304239642";
            codelist += ",304239643,304239644,304239645,304239646,304239647,304239648";
            codelist += ",3042396285,3042396286,3042396287";
            var arr = SplitOnCommaAndPrepareElements(codelist);

            var query = StartNewQuery(SearchDepth.SearchSince12Hrs);
            query = query.Where(e => e.EVENT_DATETIME >= loc_in);
            query = query.Where(e => arr.Any(item => e.CODE.StartsWith(item)));
            query = query.OrderBy(e => e.EVENT_DATETIME);

            DateTime evdt = DateTime.MinValue;
            List<gBucket> buckets = new List<gBucket>();
            int ct = 0;
            foreach (var item in query)
            {
                Program.VerboseAudit("VS item: " + item.EVENT_DATETIME + " descript=" + item.DESCRIPTION);
                if (evdt < item.EVENT_DATETIME)
                {
                    ct = query.Where(e => e.EVENT_DATETIME == item.EVENT_DATETIME).Count();
                    evdt = item.EVENT_DATETIME;
                    if (ct >= 2)
                    {
                        Program.VerboseAudit("2VS : " + item.EVENT_DATETIME);
                        var b2 = new gBucket();
                        b2.code = item.CODE;
                        b2.evdt = item.EVENT_DATETIME;
                        buckets.Add(b2);
                    }
                }
            }

            //each bucket in buckets now represents a time where at least 2 VS items exist.
            var queryb = (from item in buckets
                          select new { evdt = item.evdt }
                           ).Distinct().OrderBy(e => e.evdt).ToArray();

            string s = "";
            foreach (var e in queryb)
            {
                s += ",(" + e.evdt + ")";
            }
            if (s.Left(1) == ",") s = s.Substring(1);
            Program.VerboseAudit("Times where two VS exist: " + s);

            ct = 0;
            DateTime starttm, endtm;
            for (int i = 0; (i < queryb.GetUpperBound(0)); i++)
            {
                starttm = queryb[i].evdt;
                endtm = starttm.AddHours(2);
                ct = 0;
                for (int j = i; (j < queryb.GetUpperBound(0)); j++)
                {
                    if (queryb[j].evdt >= starttm && queryb[j].evdt <= endtm)
                    {
                        ct++;
                    }
                }
                if (ct >= 9)
                {
                    SetInd(24, "9 or more VS in 2 hours, with at least two VS per time.");
                    i = 999;
                }
            }

            //ct = 0;
            //DateTime starttm, endtm;
            //for (int i = 0; (i < queryb.GetUpperBound(0)); i++) //in the list of times...
            //{
            //    starttm = queryb[i].evdt;
            //    endtm = starttm.AddHours(2);
            //    ct = 0;
            //    foreach (var item in query)
            //    {
            //        if (item.EVENT_DATETIME >= starttm && item.EVENT_DATETIME <= endtm)
            //        {
            //            ct++;
            //            Program.VerboseAudit(ct + ": " + item.EVENT_DATETIME + "=" + item.CODE);
            //        }
            //    }
            //    //for (int j = 0; (j < queryc.GetUpperBound(0)); j++)
            //    //{
            //    //    if (queryc[j].evdt >= starttm && queryc[j].evdt <= endtm)
            //    //    {
            //    //        ct++;
            //    //        Program.VerboseAudit(ct + ": " + queryc[j].evdt + "=" + queryc[j].code);
            //    //    }
            //    //}
            //    if (ct >= 9)
            //    {
            //        SetInd(24, "9 or more VS in 2 hours, with at least two VS per time.");
            //        i = 999;
            //    }
            //}

        }

        private void CheckNorepi()
        {
            string str_rate = "";
            double dbl_rate = 0.0;
            //            Select #24 when >0.1

            //OBX | 1 | DT | MED128033 ^ NOREPINEPHRINE BITARTRATE 4 MG / 250 ML(16 MCG / ML) IN DEXTROSE 5 % IV; ; ; 18; ; ; 0.15; ; ; IV; ; ; NewBag
            //OBX | 1 | DT | MED128033 ^ NOREPINEPHRINE BITARTRATE 4 MG / 250 ML(16 MCG / ML) IN DEXTROSE 5 % IV; ; ; 18; ; ; 0.33; ; ; IV; ; ; NewBag
            //OBX | 1 | DT | MED128033 ^ NOREPINEPHRINE BITARTRATE 4 MG / 250 ML(16 MCG / ML) IN DEXTROSE 5 % IV; ; ; 18; ; ; 0.331; ; ; IV; ; ; RateVerify

            //                       Do not select #24 if value <0.1

            //OBX | 1 | DT | MED128033 ^ NOREPINEPHRINE BITARTRATE 4 MG / 250 ML(16 MCG / ML) IN DEXTROSE 5 % IV; ; ; 18; ; ; 0.01; ; ; IV; ; ; NewBag
            //OBX | 1 | DT | MED128033 ^ NOREPINEPHRINE BITARTRATE 4 MG / 250 ML(16 MCG / ML) IN DEXTROSE 5 % IV; ; ; 18; ; ; 0.07; ; ; IV; ; ; RateChange
            //OBX | 1 | DT | MED128033 ^ NOREPINEPHRINE BITARTRATE 4 MG / 250 ML(16 MCG / ML) IN DEXTROSE 5 % IV; ; ; 18; ; ; 0.08; ; ; IV; ; ; RateChange
            var query = StartNewQuery(SearchDepth.SearchSince9Hrs);    // add custom time range below
            query = query.Where(e => e.CODE.StartsWith("med"));
            query = query.Where(e => e.UNIT_ID == _pat.unit_id);
            query = query.Where(e => e.DESCRIPTION.ToUpper().StartsWith("NOREPINEPHRINE"));
            query = query.Where(e => e.DESCRIPTION.Contains(";;;iv;;;"));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            Program.VerboseAudit("Norepinephrine found: " + query.Count());
            bool found = false;
            foreach (var item in query)
            {
                if (!found)
                {
                    Program.VerboseAudit("====Med found: Norepinephrine====");
                    Program.VerboseAudit("med code: " + item.CODE + " desc: " + item.DESCRIPTION + "  evdt: " + item.EVENT_DATETIME.ToString());

                    var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                    if (arr.GetUpperBound(0) >= 3)
                    {
                        str_rate = arr[2];
                        dbl_rate = str_rate.ToDouble();
                        if (dbl_rate >= 0.1)
                        {
                            SetInd(24, "Norepinephrine rate=" + dbl_rate.ToString());
                            found = true;
                        }
                    }
                }
            }
            return;
        }

        private void CheckUserDefined()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("User-Defined indicators");
            Program.VerboseAudit("---------------");

            //reslist = "Patient arrived during downtime,Patient was cared for during downtime,Patient departed during downtime";
            //SetIndIfResultContains(99, "", "9991600100203", "", "", reslist);
            //reslist = "Yes";
            //SetIndIfResultContains(99, "", "9990000006437", "", "", reslist);
            //reslist = "Green,Yellow ,Red ,Black ";
            //SetIndIfResultContains(99, "", "9991600100265", "", "", reslist);
            //reslist = "Downtime";
            //SetIndIfResultContains(99, "", "9990007096330", "", "", reslist);


        }

        //private bool IsICU()
        //{
        //    switch (_pat.unit_name)
        //    {
        //        case "ICU":
        //        case "3BR":
        //        case "CTIC":
        //        case "NIC":
        //        case "PCU":
        //        case "TICU":
        //        case "CCB":
        //        case "RICU":
        //        case "CCUS":
        //            return true;
        //        default:
        //            return false;
        //    }
        //}

        //private bool IsTele()
        //{
        //    switch (_pat.unit_name)
        //    {
        //        case "4EST":
        //        case "2NW":
        //        case "3AE":
        //        case "5MEH":
        //        case "5NW":
        //        case "6MEH":
        //        case "6NW":
        //        case "7MEH":
        //        case "B4W":
        //        case "B5S":
        //        case "B5W":
        //        case "2NOB":
        //        case "CPLX":
        //        case "TELMS":
        //        case "MAN4":
        //            return true;
        //        default:
        //            return false;
        //    }
        //}



        //=====================================================================
        // Group like assessments into buckets of time
        // Each bucket counts as one assessment in the frequency count

        //private void AddDependentBuckets(List<gBucket> bucket_list, string codelist1, string reslist1, string codelist2, string reslist2)
        //{
        //    AddDependentBuckets(bucket_list, codelist1, reslist1, codelist2, reslist2, "", "");
        //}

        //private void AddDependentBuckets(List<gBucket> bucket_list, string codelist1, string reslist1, string codelist2, string reslist2, string codelist3,string reslist3)
        //{
        //    bool dep3 = true;
        //    // get the chart items for the assessments
        //    var query1 = StartNewQuery();
        //    query1 = AndItemFilter(query1, "", codelist1, "", "", reslist1);
        //    var query2 = StartNewQuery();
        //    query2 = AndItemFilter(query2, "", codelist2, "", "", reslist2);
        //    if (codelist3.Trim() == "")
        //    {
        //        dep3 = false;
        //        codelist3 = "Hello, this is a phantom code";
        //    }
        //    var query3 = StartNewQuery();
        //    query3 = AndItemFilter(query3, "", codelist3, "", "", reslist3);

        //    // figure out what buckets the events belong to
        //    var query1a = from item in query1
        //                  select new
        //                  {
        //                      bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
        //                      code = item.CODE,
        //                      evdt = item.EVENT_DATETIME
        //                  };
        //    var query2a = from item in query2
        //                  select new
        //                  {
        //                      bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
        //                      code = item.CODE,
        //                      evdt = item.EVENT_DATETIME
        //                  };
        //    var query3a = from item in query3
        //                      select new
        //                      {
        //                          bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
        //                          code = item.CODE,
        //                          evdt = item.EVENT_DATETIME
        //                      };

        //    string s = "BucketList1 for " + codelist1 + ": ";
        //    foreach (var item in query1a)
        //    {
        //        s += item.bucket + "";
        //    }
        //    Program.VerboseAudit(s);

        //    s = "BucketList2 for " + codelist2 + ": ";
        //    foreach (var item in query2a)
        //    {
        //        s += item.bucket + "";
        //    }
        //    if (dep3)
        //    {
        //        s = "BucketList3 for " + codelist3 + ": ";
        //        foreach (var item in query3a)
        //        {
        //            s += item.bucket + "";
        //        }
        //    }
        //    Program.VerboseAudit(s);
        //    // Add to the list IFF items in both lists occur in same bucket
        //    foreach (var item1 in query1a)
        //    {
        //        foreach (var item2 in query2a)
        //        {
        //            if (item1.bucket == item2.bucket)
        //            {
        //                if (dep3)
        //                {
        //                    foreach (var item3 in query3a)
        //                    {
        //                        if (item1.bucket == item3.bucket)
        //                        {
        //                            var b = new gBucket();
        //                            b.bucket = item1.bucket;
        //                            b.code = item1.code;
        //                            b.evdt = item1.evdt;
        //                            bucket_list.Add(b);
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    foreach (var item3 in query2a)
        //                    {
        //                        if (item1.bucket == item2.bucket)
        //                        {
        //                            var b = new gBucket();
        //                            b.bucket = item1.bucket;
        //                            b.code = item1.code;
        //                            b.evdt = item1.evdt;
        //                            bucket_list.Add(b);
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }

        //}

        private void AddSimpleProc(int pnum, DateTime evdt, DateTime enddt)
        {
            if (pnum <= 0) return;

            if (ProcExists(pnum, evdt, enddt))
            {
                Program.Audit("Activity " + pnum + ": already exists");
            }
            else
            {
                //if (ActivityFits(evdt, enddt))
                {
                    ProcOverlapsInDB_PEID(pnum, evdt, enddt); // then delete the db
                    var proc = new proc_data();
                    proc.procedure_number = pnum;
                    proc.start = evdt;
                    proc.finish = enddt;
                    _procs.Add(proc);
                    Program.Audit("Activity " + pnum + ": Found between " + evdt + " and " + enddt);
                }
            }
        }

        private bool ActivityFits(DateTime beg, DateTime fin)
        {
            bool ok = false;
            int unit_id = 0;
            string sql = "select el.unit_id from ENCOUNTER_LOCATION as el";
            sql += " where el.ENCOUNTER_ID=" + _pat.encounter_id;
            //sql += " and el.SPECIAL_UNIT_ID is null";
            sql += " and el.EFFECTIVE_DATETIME_IN<=" + beg.ToString() + "";
            sql += " and el.EFFECTIVE_DATETIME_OUT>=" + fin.ToString() + "";

            Program.VerboseAudit(sql);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd = new SqlCommand(sql, db2);
            SqlDataReader dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr2.Read())
            {
                if (dr2["UNIT_ID"] != DBNull.Value)
                    unit_id = PFSDBUtility.DBToInt(dr2["UNIT_ID"]);
            }
            ok = (unit_id > 0);

            if (ok)
            {
                db2.Close();
                return ok;
            }
            //Now check if two adjacent same units contains the activity.
            int unitid1 = 0;
            int unitid2 = 0;
            sql = "select el.unit_id from ENCOUNTER_LOCATION as el";
            sql += " where el.ENCOUNTER_ID=" + _pat.encounter_id;
            sql += " and " + beg.ToString() + "' between el.EFFECTIVE_DATETIME_IN and el.EFFECTIVE_DATETIME_OUT";
            cmd = new SqlCommand(sql, db2);
            dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr2.Read())
            {
                if (dr2["UNIT_ID"] != DBNull.Value)
                    unitid1 = PFSDBUtility.DBToInt(dr2["UNIT_ID"]);
            }
            sql = "select el.unit_id from ENCOUNTER_LOCATION as el";
            sql += " where el.ENCOUNTER_ID=" + _pat.encounter_id;
            sql += " and " + fin.ToString() + "' between el.EFFECTIVE_DATETIME_IN and el.EFFECTIVE_DATETIME_OUT";
            cmd = new SqlCommand(sql, db2);
            dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr2.Read())
            {
                if (dr2["UNIT_ID"] != DBNull.Value)
                    unitid2 = PFSDBUtility.DBToInt(dr2["UNIT_ID"]);
            }
            ok = (unitid1 > 0 && unitid1 == unitid2);
            //db2.Close();
            return ok;


        }

        //6	2019-09-18 15:00:00.000	2019-09-18 19:00:00.000
        //6	2019-09-19 07:00:00.000	2019-09-19 11:00:00.000

        private bool ActivityDuringClassType6(DateTime beg, DateTime fin, out DateTime classout)
        {
            bool ok = false;
            int pt = 0;
            classout = DateTime.MinValue;
            string sql = "select ce.PATIENT_TYPE,ce.effective_datetime_in,ce.effective_datetime_out from CLASSIFICATION_EVENT as ce";
            sql += " where ce.ENCOUNTER_ID=" + _pat.encounter_id;
            sql += " and ce.PATIENT_TYPE=6";
            sql += " and (" + beg.ToString() + "' >=ce.EFFECTIVE_DATETIME_IN and " + beg.ToString() + "' <ce.EFFECTIVE_DATETIME_OUT";
            sql += " or " + beg.ToString() + "' <ce.EFFECTIVE_DATETIME_IN and " + fin.ToString() + "' >ce.EFFECTIVE_DATETIME_OUT";
            sql += " or " + fin.ToString() + "' >ce.EFFECTIVE_DATETIME_IN and " + fin.ToString() + "' <=ce.EFFECTIVE_DATETIME_OUT)";
            Program.VerboseAudit(sql);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd = new SqlCommand(sql, db2);
            SqlDataReader dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr2.Read())
            {
                if (dr2["PATIENT_TYPE"] != DBNull.Value)
                {
                    pt = PFSDBUtility.DBToInt(dr2["PATIENT_TYPE"]);
                    classout = PFSDBUtility.DBToDateTime(dr2["EFFECTIVE_DATETIME_OUT"]);
                }
            }
            ok = (pt == 6);
            db2.Close();
            return ok;
        }

        private void AtLeastOneADL()
        {
            Program.Audit("---------------");

            if (!(_inds[2].is_checked || _inds[3].is_checked || _inds[4].is_checked)) {
                // Make "#2 ADL - Assist" the default.  (90% of patients)
                SetInd(1, "Defaulting to ADL Self due to lack of documentation.");
            }
        }

        private void HighestIndicatorInEachGroupWins()
        {
            int i, g;
            bool highest_is_on;
            string ind_list;

            Program.Audit("Select highest indicator in each group");

            g = 0;
            highest_is_on = false;
            // Go from bottom up - highest (lowest) indicator in each group wins
            for (i = MAX_INDS; (i >= 1); i--) {
                if (_inds[i].radio_group > 0) {
                    if (_inds[i].radio_group != g) {
                        //this is a new group
                        g = _inds[i].radio_group;
                        highest_is_on = _inds[i].is_checked;
                    } else {
                        //same group
                        if (highest_is_on) {
                            _inds[i].is_checked = false;             //uncheck a lower number
                        } else {
                            highest_is_on = _inds[i].is_checked;     //save this one
                        }
                    }
                }
            } // next i

            //Echo the indicators for an Audit (no classification will be saved)
            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++) {
                if (_inds[i].is_checked) ind_list += "," + i;
            } // next i

            Program.Audit("Final list = " + ind_list.Substring(1));
            Program.Audit("---------------");
        }


        private void CheckProcs()
        {
            //CheckProc_1();
            CheckProc_2();  //MGH Sitters
            CheckProc_3();
            CheckProc_4();
            CheckProc_5();
            CheckProc_6();
            CheckProc_7();
            CheckProc_8();
            CheckProc_9();
            CheckProc_10();
            CheckProc_11();

        }

        //private void DoProc(int pnum, string code)
        //{
        //    double mins = 0;
        //    string found_what;
        //    DateTime evdt;
        //    DateTime enddt = DateTime.MinValue;

        //    if (GetResultAndEVDT("", code, "", "", out found_what, out evdt))
        //    {
        //        //mins = 60.0 * found_what.ToDouble();
        //        if (found_what.Contains("180")) mins = 180;
        //        else if (found_what.Contains("120")) mins = 120;
        //        else if (found_what.Contains("90")) mins = 90;
        //        else if (found_what.Contains("60")) mins = 60;
        //        enddt = evdt.AddMinutes(mins);

        //        if (ProcExistsInDB(pnum, evdt, enddt))
        //        {
        //            Program.Audit("Activity " + pnum+ ": already exists");
        //        }
        //        else
        //        {
        //            if (!QueuedProcOverlaps(pnum, evdt, enddt))
        //            {
        //                var proc = new proc_data();
        //                proc.procedure_number = pnum;
        //                proc.start = evdt;
        //                proc.finish = enddt;
        //                _procs.Add(proc);
        //                Program.Audit("Activity " + pnum + ": Found " + code + " between " + evdt + " and " + enddt);
        //            }
        //        }

        //    }

        //}

        private bool ProcOverlapsInDB_PEID(int pnum, DateTime startdt, DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            //            LoadPatientProceduresIfNeeded();
            bool overlap_exists = false;
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && ((proc.PROCEDURE_DATETIME >= startdt) && (proc.PROCEDURE_DATETIME < enddt)
                                ||
                                (proc.DEPARTURE_DATETIME > startdt) && (proc.DEPARTURE_DATETIME <= enddt)
                                ||
                                (proc.PROCEDURE_DATETIME < startdt) && (proc.DEPARTURE_DATETIME > enddt)
                                )
                            //&& ( ! (proc.PROCEDURE_DATETIME == startdt) && (proc.DEPARTURE_DATETIME == enddt))
                            && (proc.CLASSIFIED_BY_ID < 0)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new { proc.PROCEDURE_EVENT_ID, proc.PROCEDURE_DATETIME, proc.DEPARTURE_DATETIME };
            overlap_exists = (query.Count() > 0);
            foreach (var a in query)
            {
                Program.VerboseAudit("Will Delete act: procdt=" + a.PROCEDURE_DATETIME.ToString() + "  depdt=" + a.DEPARTURE_DATETIME.ToString());
                Program.VerboseAudit("because it overlays startdt=" + startdt.ToString() + "  enddt=" + enddt.ToString());
                DeleteActivity(a.PROCEDURE_EVENT_ID);
            }
            //            peid = 0;
            return (overlap_exists);
        }
        private void DeleteActivity(int peid)
        {
            //            delete PROCEDURE_ANSWER where PROCEDURE_EVENT_ID=7211351
            //delete RPT_PROC_BY_DAY where PROCEDURE_EVENT_ID=7211351
            //delete PROCEDURE_EVENT where PROCEDURE_EVENT_ID=7211351
            if (peid == 0) return;

            Program.VerboseAudit("db ProcAnsw Deleting peid=" + peid);
            var db = PFSDBUtility.NewPfsDataContext();
            var query = from ia in db.PROCEDURE_ANSWERs
                        where (ia.PROCEDURE_EVENT_ID == peid)
                        select ia;
            if (query.Count() > 0)
            {
                var items = query.ToList();
                foreach (var item in items)
                    db.PROCEDURE_ANSWERs.DeleteOnSubmit(item);
                db.SubmitChanges();
            }

            Program.VerboseAudit("db RptProc Deleting peid=" + peid);
            var db2 = PFSDBUtility.NewPfsDataContext();
            var query2 = from r in db2.RPT_PROC_BY_DAYs
                         where (r.PROCEDURE_EVENT_ID == peid)
                         select r;
            if (query2.Count() > 0)
            {
                var items2 = query2.ToList();
                foreach (var item2 in items2)
                    db2.RPT_PROC_BY_DAYs.DeleteOnSubmit(item2);
                db2.SubmitChanges();
            }

            Program.VerboseAudit("db ProcEvent Deleting peid=" + peid);
            var db3 = PFSDBUtility.NewPfsDataContext();
            var query3 = from ce in db3.PROCEDURE_EVENTs
                         where (ce.PROCEDURE_EVENT_ID == peid)
                         select ce;
            if (query3.Count() > 0)
            {
                var items3 = query3.ToList();
                foreach (var item3 in items3)
                    db3.PROCEDURE_EVENTs.DeleteOnSubmit(item3);
                db3.SubmitChanges();
            }
        }



        private bool QueuedProcOverlaps(int pnum, DateTime startdt, DateTime enddt)
        {
            bool overlap = false;

            Program.VerboseAudit("Activity " + pnum + ": Check for dup at " + startdt.ToString() + " - " + enddt.ToString());
            proc_data[] pary = _procs.ToArray();
            for (int i = 0; i <= pary.GetUpperBound(0); i++)
            {
                if (pary[i].procedure_number == pnum)
                {
                    //overlap if   p.start between stardt and enddt
                    //             p.finish between startdt and enddt
                    //             startdt >= p.start and enddt <= p.finish
                    if ((pary[i].start >= startdt) && (pary[i].start < enddt) ||
                        (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
                        (startdt >= pary[i].start) && (enddt <= pary[i].finish))
                    {
                        overlap = true;
                    }

                    if ((pary[i].start >= startdt) && (pary[i].start < enddt) && (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
                        (pary[i].start < startdt) && (pary[i].finish > startdt) && (pary[i].finish < enddt)
                        )
                    {
                        // the proc in the list is completely contained in startdt/endt pair
                        // change this proc to be the startdt/enddt
                        // overlap is still true, but the list item will be updated with the encompassing times
                        //Program.Audit("Procedure " + pnum + ": " + startdt + " - " + enddt + " will trump " + pary[i].start + " - " + pary[i].finish);
                        pary[i].start = startdt;
                        pary[i].finish = enddt;
                    }
                }
            }
            _procs = pary.ToList();
            Program.VerboseAudit("Activity " + pnum + ": Check for dup returns " + overlap);
            return overlap;
        }

        private bool ProcExistsInDB(int pnum, DateTime startdt, out DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            Program.VerboseAudit("ProcExistsInDB: pnum=" + pnum + " startdt=" + startdt.ToString());
            int ct = 0;
            enddt = DateTime.MinValue;
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            //&& (proc.PROCEDURE_DATETIME <= startdt)
                            && (proc.DEPARTURE_DATETIME > startdt)
                            && (((pnum > 2) && (ans.PROCEDURE_NUMBER == pnum)) || ((pnum <= 2) && (ans.PROCEDURE_NUMBER <= 2)))
                        orderby proc.DEPARTURE_DATETIME descending
                        select new { proc.DEPARTURE_DATETIME };
            ct = query.Count();
            //Program.VerboseAudit("ProcExistsInDB: ct=" + ct);
            //if (ct > 0)
            //{
            //    foreach (var x in query)
            //    {
            //        Program.VerboseAudit("ProcExistsInDB: x=" + x.DEPARTURE_DATETIME);
            //    }
            //}
            if (ct > 0)
                enddt = (DateTime)query.First().DEPARTURE_DATETIME;
            //else //see if there is a saved proc that ended before this startdt
            //{
            //    query = from proc in _procedure_events
            //            from ans in proc.PROCEDURE_ANSWERs
            //            where (proc.ENCOUNTER_ID == _pat.encounter_id)
            //                //&& (proc.PROCEDURE_DATETIME <= startdt)
            //                && (proc.DEPARTURE_DATETIME > startdt.AddHours(-4))
            //                && (ans.PROCEDURE_NUMBER == pnum)
            //            orderby proc.DEPARTURE_DATETIME descending
            //            select new { proc.DEPARTURE_DATETIME };
            //    ct = query.Count();
            //    if (ct > 0)
            //        enddt = (DateTime)query.First().DEPARTURE_DATETIME;
            //}
            Program.VerboseAudit("ProcExistsInDB: pnum=" + pnum + " returns " + ct);
            return ct > 0;
        }

        private void CheckProc_2()
        {
            if (_pat.short_name.ToUpper() != "MGH" && 
                _pat.short_name.ToUpper() != "WDH") return;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A2. 1-1 safety observation by non-RN");
            Program.VerboseAudit("---------------");

            //54610   2020 - 01 - 29 11:00:00.000 Initiated   304239487
            //54610   2020 - 01 - 30 10:00:00.000 Continued   304239487
            //54610   2020 - 01 - 30 22:00:00.000 Discontinued   304239487
            //            304239487

            //            "Initiated
            //Continued
            //Discontinued"
            bool stop = false;
            DateTime dcdt = DateTime.MinValue;
            bool found_dc = false;
            bool first = true;
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);    // add custom time range below
            query = query.Where(e => e.CODE == "304239487");
            query = query.Where(e => e.ORDER_STATUS == null || e.ORDER_STATUS == ""); // need to update order_status=x
            query = query.Where(e => e.RESULT.ToLower() == "initiated" || e.RESULT.ToLower() == "continued");
            query = query.OrderBy(e => e.EVENT_DATETIME);
            Program.VerboseAudit("qct: " + query.Count());
            if (query.Count() > 0)
            {
                foreach (var item in query)
                {
                    if (item.EVENT_DATETIME > dcdt) //skip any intermediate starts
                    {
                        if (item.PROCEDURE_START == null)
                            item.PROCEDURE_START = DateTime.MaxValue;
                        Program.VerboseAudit("item:" + item.EVENT_DATETIME + " original start="+item.PROCEDURE_START);
                        found_dc = MakeActivityIfFindDiscontinue((DateTime)item.PROCEDURE_START, item.EVENT_DATETIME, out dcdt);
                        Program.VerboseAudit("Sitter: " + item.RESULT + " " + item.EVENT_DATETIME.ToString());
                        first = false;
                        if (found_dc)
                        {
                            Program.VerboseAudit("founddc: true");
                            UpdatePtChartArraysCode("304239487", "X", dcdt);
                        }
                    }
                }
            }
        }

        private bool MakeActivityIfFindDiscontinue(DateTime origstart,DateTime initdt,out DateTime dcdt)
        {
            int pnum = 0;
            bool found_dc = false;
            dcdt = DateTime.MinValue;
            bool first = true;
            DateTime save_initdt = initdt;

            Program.VerboseAudit("q origstart=" + origstart+ " initdt=" + initdt + " g_pull_finist=" + Program.g_pull_finish);
            if (origstart < initdt) initdt = origstart;
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);    // add custom time range below
            query = query.Where(e => e.CODE == "304239487");
            query = query.Where(e => e.ORDER_STATUS == null || e.ORDER_STATUS == ""); // need to update order_status=x
            query = query.Where(e => e.RESULT.ToLower() == "discontinued");
            query = query.Where(e => e.EVENT_DATETIME >= initdt);
            query = query.OrderBy(e => e.EVENT_DATETIME);
            Program.VerboseAudit("qcount=" + query.Count());
            if (query.Count() > 0)
            {
                found_dc = true;
                foreach (var item in query)
                {
                    if (first)
                    {
                        first = false;
                        if (item.EVENT_DATETIME >= initdt) //valid discontinued exists
                        {
                            dcdt = item.EVENT_DATETIME;
                            if (origstart < Program.g_pull_finish.AddHours(-12)
                                && origstart >= Program.g_pull_finish.AddHours(-24))
                            {
                                //make for origstart to item.evdt
                                Program.VerboseAudit("2A: dc time=" + item.EVENT_DATETIME);
                                pnum = GetSitterType(origstart);
                                AddSimpleProc(pnum, origstart, item.EVENT_DATETIME);
                                DisableSitter(item.CODE, origstart, true);
                            }
                            else if (item.EVENT_DATETIME <= Program.g_pull_finish.AddHours(-12))
                            { // discontinued in past; neutralize this pair.
                                Program.VerboseAudit("1: dc time=" + item.EVENT_DATETIME);
                            }
                            else if (initdt < Program.g_pull_finish.AddHours(-12))
                            {
                                //make for pull start to item.evdt
                                Program.VerboseAudit("2: dc time=" + item.EVENT_DATETIME);
                                pnum = GetSitterType(initdt);
                                AddSimpleProc(pnum, Program.g_pull_finish.AddHours(-12), item.EVENT_DATETIME);
                            }
                            else if (item.EVENT_DATETIME <= Program.g_pull_finish)
                            {
                                Program.VerboseAudit("3: dc time=" + item.EVENT_DATETIME);
                                //make for initdt to item.evdt
                                pnum = GetSitterType(initdt);
                                AddSimpleProc(pnum, initdt, item.EVENT_DATETIME);
                            }
                            else
                            {
                                Program.VerboseAudit("4: dc time=" + item.EVENT_DATETIME);
                                //make for initdt to _pat.pull finish
                                pnum = GetSitterType(initdt);
                                AddSimpleProc(pnum, initdt, Program.g_pull_finish);
                            }
                            DisableSitter(item.CODE, item.EVENT_DATETIME);
                            DisableSitter(item.CODE, item.EVENT_DATETIME, true);
                        }
                        Program.VerboseAudit("Sitter: " + item.RESULT + " " + item.EVENT_DATETIME.ToString());
                    }
                }
            }
            else if (initdt <= Program.g_pull_finish.AddHours(-12)) //no discontinued
            {
                //make for pull start to pull finish
                Program.VerboseAudit("5: dc time=" + Program.g_pull_finish);
                dcdt = Program.g_pull_finish;
                pnum = GetSitterType(initdt);
                AddSimpleProc(pnum, Program.g_pull_finish.AddHours(-12), Program.g_pull_finish);
            }
            else if (initdt < Program.g_pull_finish) // if initdt==g_pull_finish then dont create
            {
                Program.VerboseAudit("6: dc time=" + Program.g_pull_finish);
                dcdt = Program.g_pull_finish;
                found_dc = true;
                pnum = GetSitterType(initdt);
                AddSimpleProc(pnum, initdt, Program.g_pull_finish);
                DisableSitter("304239487", initdt);
                AddSitter("304239487", initdt, Program.g_pull_finish);
            }
            return found_dc;
        }

        private void AddSitter(string code, DateTime initdt, DateTime evdt)
        {
            string desc = "TC: Direct Observer";
            string res = "Initiated";
            DateTime timestmp = DateTime.Now;
            short seq = 0;
            int unitid = _pat.unit_id;

            using (var db = PFSDBUtility.NewSqlConnection())
            {
                string q = "INSERT INTO chart_item (encounter_id,event_datetime,code,description,result,timestamp,sequence,unit_id,procedure_start)";
                q += " select @encid, @evdt, @code, @desc, @res, @ts,@seq,@unit,@procstart where not exists";
                q += " (select encounter_id,code,event_datetime from chart_item where encounter_id=" + _pat.encounter_id.ToString() + " and code='" + code + "' and event_datetime=" + PFSDBUtility.SQLDateTime(evdt) + ")";
                //string q = "INSERT INTO chart_item (encounter_id,event_datetime,code,description,timestamp,sequence,unit_id,order_id) values (@encid, @evdt, @code, @desc, @ts,@seq,@unit,@oid)";
                SqlCommand cmd = new SqlCommand(q, db);
                cmd.Parameters.AddWithValue("@encid", _pat.encounter_id);
                cmd.Parameters.AddWithValue("@evdt", evdt);
                cmd.Parameters.AddWithValue("@code", code);
                cmd.Parameters.AddWithValue("@desc", desc);
                cmd.Parameters.AddWithValue("@res", res);
                cmd.Parameters.AddWithValue("@ts", timestmp);
                cmd.Parameters.AddWithValue("@seq", seq);
                cmd.Parameters.AddWithValue("@unit", unitid);
                cmd.Parameters.AddWithValue("@procstart", initdt);
                cmd.ExecuteNonQuery();
                db.Close();
            } //using db

        }

        private int GetSitterType(DateTime evdt)
        { // get the sitter type RN or non-RN from the charting that should exist
            //at the same time as the initiation of the observer 1540100298
            int pnum = 0;
            string return_result = "";
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);    // add custom time range below
            query = query.Where(e => e.CODE == "1540100298");
            query = query.Where(e => e.EVENT_DATETIME == evdt);
            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
                if (return_result.Trim().ToUpper().StartsWith("RN")) pnum = 1;
                if (return_result.Trim().ToUpper().StartsWith("NON-RN")) pnum = 2;
            }
            if (pnum == 0)
            {
                Program.VerboseAudit("Sitter type RN or non-RN not found. Defaulting to non_RN.");
                pnum = 2;
            }
            return pnum;
        }

        private void CheckActivity(int actnum,string actcode, string actst, string actlencode)
        {
            int actlen;
            string actsthhmm = "0000";
        
            var query = StartNewQuery(SearchDepth.SearchSince24Hrs);    // add custom time range below
            query = query.Where(e => e.CODE == actcode);
            query = query.Where(e => e.ORDER_STATUS == null || e.ORDER_STATUS == ""); // need to update order_status=x
            query = query.OrderBy(e => e.EVENT_DATETIME);
            Program.VerboseAudit("for code=" + actcode + " count=" + query.Count());
            foreach (var item in query)
            {
                if (item.RESULT.ToLower() == "yes")
                { //09500000
                    var q2 = StartNewQuery(SearchDepth.SearchSince24Hrs);    // add custom time range below
                    q2 = q2.Where(e => e.CODE == actst);
                    q2 = q2.Where(e => e.EVENT_DATETIME == item.EVENT_DATETIME);
                    Program.VerboseAudit("for code=" + actst + " count=" + q2.Count());
                    if (q2.Count() == 0) return;

                    actsthhmm = q2.First().RESULT;
                    Program.VerboseAudit("actsthhmm=" + actsthhmm);
                    if (actsthhmm.Length >= 4)
                        actsthhmm = actsthhmm.Substring(0, 4);
                    Program.VerboseAudit("actsthhmm=" + actsthhmm);

                    var q3 = StartNewQuery(SearchDepth.SearchSince24Hrs);    // add custom time range below
                    q3 = q3.Where(e => e.CODE == actlencode);
                    q3 = q3.Where(e => e.EVENT_DATETIME == item.EVENT_DATETIME);
                    Program.VerboseAudit("for code=" + actlencode + " count=" + q3.Count());
                    if (q3.Count() == 0) return;

                    actlen = (int)q3.First().RESULT.Val();
                    DateTime stdt = DateTime.MinValue;
                    double hrsdiff = 0;
                    if (actlen >= 60)
                        {
                        DateTime foundtime = item.EVENT_DATETIME.Date.AddHours((int)actsthhmm.Substring(0, 2).Val()).AddMinutes((int)actsthhmm.Substring(2, 2).Val());
                        hrsdiff = PFSUtility.DateDiffInHours(item.EVENT_DATETIME, foundtime);
                        // What I'm going to do is simply choose the nearest date regardless if it's Before or After the charted time.
                        // Examples:  
                        // case 1: They chart at 4/1 3:00am with a time of 0100. Make it 4/1 01:00
                        // case 1: They chart at 4/1 3:00am with a time of 0600. Make it 4/1 06:00
                        // case 2: They chart at 3/31 23:00PM with a time of 0300. MAKE IT 4/1 03:00(next calendar date)
                        // case 3: They chart at 4/1 2:00am with a time of 2200. MAKE IT 3/31 22:00(prev calendar date)
                        if (Math.Abs(hrsdiff) <= 12)  //case 1
                            stdt = foundtime;
                        else if (hrsdiff < 0) //case 2
                            //then this hhmm must belong to tomorrow's date
                            stdt = foundtime.AddDays(1);
                        else if (hrsdiff > 0) //case 3
                            //then this hhmm must belong to yesterday's date
                            stdt = foundtime.AddDays(-1);

                        //TimeSpan start = new TimeSpan(0, 0, 0); //0 o'clock
                        //TimeSpan end = new TimeSpan(6, 0, 0); // 6 o'clock
                        //TimeSpan item_ts = item.EVENT_DATETIME.TimeOfDay;
                        //TimeSpan actst_ts = new TimeSpan((int)actsthhmm.Substring(0, 2).Val(), (int)actsthhmm.Substring(2, 2).Val(), 0);
                        //stdt = item.EVENT_DATETIME.Date + actst_ts;
                        //if ((item_ts >= start) && (item_ts <= end))
                        //{
                        //    if (actst_ts > item_ts) // then it belongs to prev cal day.
                        //        stdt = item.EVENT_DATETIME.Date.AddDays(-1) + actst_ts;
                        //}
                        //                      DateTime stdt = item.EVENT_DATETIME.Date.AddHours(actsthhmm.Substring(0, 2).Val()).AddMinutes(actsthhmm.Substring(2, 2).Val());
                        Program.VerboseAudit("stdt=" + stdt);
                        AddSimpleProc(actnum, stdt, stdt.AddMinutes(actlen));
                        NullifyActivity(actcode, item.EVENT_DATETIME);
                        }
                }
            }

        }

        private void NullifyActivity(string actcode, DateTime evdt)
        {
            var db = PFSDBUtility.NewSqlConnection();
            string q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and event_datetime=" + PFSDBUtility.SQLDateTime(evdt) + " and code='" + actcode + "'";
            SqlCommand cmd = new SqlCommand(q, db);
            cmd.ExecuteNonQuery();
            db.Close();
        }

        private DateTime NextFinish(DateTime startdt)
        {
            DateTime dt = DateTime.MinValue;

            int b = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, startdt) / (4 * 60));
            dt = _pat.pull_start.AddMinutes((b + 1) * 240);
            Program.VerboseAudit("NextFinish: startdt=" + startdt.ToString() + " b=" + b + " dt=" + dt.ToString());
            return dt;
        }


        private bool ProcExists(int pnum, DateTime startdt, DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && (proc.PROCEDURE_DATETIME == startdt)
                            && (proc.DEPARTURE_DATETIME == enddt)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new { proc.PROCEDURE_EVENT_ID };
            return (query.Count() > 0);
        }


        private void CheckProc_3()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A3. Off unit accompanied by RN");
            Program.VerboseAudit("---------------");
//            ('304237002146','304239063','304239064',
//'304237002151','304239065','304239066',
//'304237002157','304239067','304239216',
//'304237002162','304239050','304239051',
//'304237002167','304239052','304239053',
//'304237002172','304239054','304239055',
//'304237002177','304239056','304239057',
//'304237002181','304239058','304239059',
//'304237002191')
            int actnum = 3;
            string actcode = "304239062";
            string actst = "304237002146";
            string actlencode = "304239063";
            CheckActivity(actnum, actcode, actst, actlencode);

        }
        private void CheckProc_4()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A4. Off unit accompanied by Non-RN");
            Program.VerboseAudit("---------------");

            int actnum = 4;
            string actcode = "304239064";
            string actlencode = "304239065";
            string actst = "304237002151";
            CheckActivity(actnum, actcode, actst, actlencode);

        }

        private void CheckProc_5()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A5. Patient/family education by RN");
            Program.VerboseAudit("---------------");

            int actnum = 5;
            string actcode = "304239066";
            string actlencode = "304239067";
            string actst = "304237002157";
            CheckActivity(actnum, actcode, actst, actlencode);


        }

        private void CheckProc_6()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A6. Extensive wound management by RN");
            Program.VerboseAudit("---------------");

            int actnum = 6;
            string actcode = "304239216";
            string actlencode = "304239050";
            string actst = "304237002162";
            CheckActivity(actnum, actcode, actst, actlencode);

        }

        private void CheckProc_7()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A7. Extensive wound management by non-RN");
            Program.VerboseAudit("---------------");

            //304239051
            //304239057,304239058
            //304239059,304239060
            int actnum = 7;
            string actcode = "304239051";
            string actlencode = "304239052";
            string actst = "304237002167";
            CheckActivity(actnum, actcode, actst, actlencode);

        }

        private void CheckProc_8()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A8. Coordination of care by RN");
            Program.VerboseAudit("---------------");
            //304239057,304239058
            //304239059,304239060
            int actnum = 8;
            string actcode = "304239053";
            string actlencode = "304239054";
            string actst = "304237002172";
            CheckActivity(actnum, actcode, actst, actlencode);
        }

        private void CheckProc_9()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A9. 1:1 RN at bedside");
            Program.VerboseAudit("---------------");

            //304239057,304239058
            //304239059,304239060
            int actnum = 9;
            string actcode = "304239055";
            string actlencode = "304239056";
            string actst = "304237002177";
            CheckActivity(actnum, actcode, actst, actlencode);
        }

        private void CheckProc_10()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A10. 1:1 non-RN at bedside");
            Program.VerboseAudit("---------------");
            //304239057,304239058
            //304239059,304239060
            int actnum = 10;
            string actcode = "304239057";
            string actlencode = "304239058";
            string actst = "304237002181";
            CheckActivity(actnum, actcode, actst, actlencode);
        }

        private void CheckProc_11()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A11. 2:1 by RN at bedside");
            Program.VerboseAudit("---------------");
            //304239059,304239060
            int actnum = 11;
            string actcode = "304239059";
            string actlencode = "304239060";
            string actst = "304237002191";
            CheckActivity(actnum, actcode, actst, actlencode);

        }

        private void CheckOutcomes()
        {
            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = AndItemFilter(query, "", "A_MHPostFalltime", "", "", "");
            foreach (var ch in query)
            {
                var outc = new proc_data();
                outc.procedure_number = 1;
                outc.start = ch.EVENT_DATETIME;
                _outcomes.Add(outc);
                Program.Audit("Outcomes 1: Found A_MHPostFalltime at " + outc.start);
            }
        }

        //private void CheckOtherWorkload()
        //{
        //    Program.VerboseAudit("---------------");
        //    Program.VerboseAudit("Other Activity: ECMO");
        //    Program.VerboseAudit("---------------");
        //    //9993040001093
        //    var query = StartNewQuery(SearchDepth.SearchDefault);
        //    query = AndItemFilter(query, "", "9993040101000", "", "", "");
        //    query = query.Where(e => e.UNIT_ID > 0);
        //    query = query.OrderBy(e => e.EVENT_DATETIME);
        //    bool added = false;
        //    foreach (var ch in query)
        //    {
        //        if (!added && ch.EVENT_DATETIME >= _pat.pull_finish.AddHours(-4))
        //        {
        //            added = true;
        //            Program.Audit("Adding ECMO Other Activity; charted at: " + ch.EVENT_DATETIME.ToString());
        //            AddOtherWorkload(ch.UNIT_ID, _pat.pull_finish.AddHours(-4));
        //        }
        //    }
        //}
        //private void AddOtherWorkload(int unit_id, DateTime startdt)
        //{
        //    var cn = PFSDBUtility.NewSqlConnection();
        //    string sql, columns = "", values = "";
        //    double thpwi = 5.0;
        //    int shflen = 8;
        //    DateTime starttm, finishtm;
        //    DateTime start, finish;
        //    int wkld_mins = 0, assess=0, interv = 0, consult = 0;

        //    columns = "OTHER_WORKLOAD_ID";
        //    columns += ",UNIT_ID";
        //    columns += ",SHIFT_DEFINITION_ID";
        //    columns += ",WORKLOAD_TYPE";
        //    columns += ",START_DATETIME";
        //    columns += ",FINISH_DATETIME";
        //    columns += ",METHODOLOGY_ID";
        //    columns += ",METHODOLOGY_WORKLOAD";
        //    columns += ",THPWI";
        //    columns += ",REC_HOURS_PER_HOUR";
        //    columns += ",CREATED_BY_ID";
        //    columns += ",CREATED_DATETIME";
        //    columns += ",LOS_HOURS";
        //    columns += ",COUNT20";
        //    columns += ",TOTAL_WORKLOAD_HOURS";
        //    columns += ",ASSESSMENT_HOURS,INTERVENTION_HOURS,CONSULTATION_HOURS";
        //    columns += ",REPORT_DATE";
        //    columns += ",TIMESTAMP";
        //    columns += ",TOTAL_VOLUME";
        //    columns += ",UNIT_PARAM_ID";

        //    long ow_id = PFSDBUtility.NextGID();

        //    DateTime rpt_date = startdt.Date;
        //    if (startdt.Hour < _pat.sod.Hour)
        //        rpt_date = rpt_date.AddDays(-1);

        //    long shfdefid = GetShiftDefID(_pat.upid, rpt_date, startdt, out shflen, out wkld_mins, out starttm, out finishtm,out thpwi,out assess,out interv,out consult);
        //    double methwkld = wkld_mins / 60.0 / thpwi;
        //    double rechrphr = wkld_mins / 60.0 / shflen;
        //    double wkld_hrs = wkld_mins / 60.0;
        //    start = rpt_date.Date + starttm.TimeOfDay;
        //    finish = start.AddHours(shflen);

        //    values = ow_id.ToString();
        //    values += "" + unit_id.ToString();
        //    values += "" + shfdefid.ToString();
        //    values += ",2";
        //    values += "" + PFSDBUtility.SQLDateTime(start);
        //    values += "" + PFSDBUtility.SQLDateTime(finish);
        //    values += ",20";
        //    values += "" + methwkld.ToString();
        //    values += "" + thpwi.ToString();
        //    values += "" + rechrphr.ToString();
        //    values += ",-3";
        //    values += "" + PFSDBUtility.SQLDateTime(DateTime.Now);
        //    values += "" + shflen.ToString();
        //    values += ",1"; //count20
        //    values += "" + wkld_hrs.ToString();
        //    values += "" + (wkld_hrs * assess / 100.0).ToString();
        //    values += "" + (wkld_hrs * interv / 100.0).ToString();
        //    values += "" + (wkld_hrs * consult / 100.0).ToString();
        //    values += "" + PFSDBUtility.SQLDateTime(rpt_date);
        //    values += "" + PFSDBUtility.SQLDateTime(DateTime.Now);
        //    values += ",1";
        //    values += "" + _pat.upid.ToString();

        //    sql = "INSERT INTO OTHER_WORKLOAD (" + columns + ") VALUES (" + values + ")";
        //    // Do not throw an error because this is the place where errors get recorded so it gets recursive.
        //    // Either try again or exit quietly instead.
        //    Program.VerboseAudit(sql);
        //    while (true)
        //    {
        //        try
        //        {
        //            var db = PFSDBUtility.NewSqlConnection();
        //            var cmd = new SqlCommand(sql, db);
        //            cmd.ExecuteNonQuery();
        //        }
        //        catch (Exception e)
        //        {
        //            string msg = e.Message.ToLower();
        //            if (PFSDBUtility.IsFatalDatabaseError(msg))
        //            {
        //                return;                         // give up
        //            }
        //        }
        //        break;
        //    }
        //}

        //private int GetShiftDefID(int upid, DateTime rptdt,DateTime startdt, out int shflen, out int wkld_mins, out DateTime starttm, out DateTime finishtm, out double thpwi, out int assess, out int interv, out int consult)
        //{
        //    int shfid=0;
        //    string d="";

        //    shflen = 0;
        //    starttm = DateTime.MinValue;
        //    finishtm = DateTime.MinValue;
        //    thpwi = 5.0;
        //    assess = 0;
        //    interv = 0;
        //    consult = 0;
        //    wkld_mins = 0;

        //    DayOfWeek dow = rptdt.DayOfWeek;
        //    switch (dow)
        //    {
        //        case DayOfWeek.Sunday:
        //            d = "Sunday";
        //            break;
        //        case DayOfWeek.Monday:
        //            d = "Monday";
        //            break;
        //        case DayOfWeek.Tuesday:
        //            d = "Tuesday";
        //            break;
        //        case DayOfWeek.Wednesday:
        //            d = "Wednesday";
        //            break;
        //        case DayOfWeek.Thursday:
        //            d = "Thursday";
        //            break;
        //        case DayOfWeek.Friday:
        //            d = "Friday";
        //            break;
        //        case DayOfWeek.Saturday:
        //            d = "Saturday";
        //            break;
        //    }
        //    d += "_ID";
        //    string sql = "select shift_definition_id,paid_hours,workload_minutes,start_time,finish_time,thpwi,assessment_pct,intervention_pct,consultation_pct from SHIFT_DEFINITION as sd";
        //    sql += " inner join DAY_DEFINITION as dd on(dd.DAY_DEFINITION_ID = sd.DAY_DEFINITION_ID)";
        //    sql += " inner join UNIT_PARAM as up on(dd.UNIT_PARAM_ID = dd.UNIT_PARAM_ID)";
        //    sql += " inner join OTHER_WORKLOAD_DEFINITION as owd on(up.UNIT_PARAM_ID = owd.UNIT_PARAM_ID)";
        //    sql += " where sd.is_primary_shift='Y' and up.UNIT_PARAM_ID=" + upid.ToString();
        //    sql += " and owd.COLUMN_NUMBER=20";
        //    sql += " and up." + d + "= dd.DAY_DEFINITION_ID";
        //    sql += " and (" + startdt.Hour + " between datepart(hour,sd.start_time) and datepart(hour,sd.finish_time)";
        //    sql += " or (datepart(hour,sd.start_time) > datepart(hour,sd.finish_time)";
        //    sql += " and (" + startdt.Hour + ">=datepart(hour,sd.start_time) or " + startdt.Hour + "<datepart(hour,sd.finish_time))))";

        //    //(7 between datepart(hour, sd.start_time) and datepart(hour, sd.finish_time) or
        //    //(datepart(hour, sd.start_time) > datepart(hour, sd.finish_time) and
        //    // (7 >= datepart(hour, sd.start_time) or 7 < datepart(hour, sd.finish_time))))


        //    Program.VerboseAudit(sql);
        //    var db2 = PFSDBUtility.NewSqlConnection();
        //    var cmd = new SqlCommand(sql, db2);
        //    SqlDataReader dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
        //    while (dr2.Read())
        //    {
        //        shfid = PFSDBUtility.DBToInt(dr2["SHIFT_DEFINITION_ID"]);
        //        shflen = PFSDBUtility.DBToInt(dr2["PAID_HOURS"]);
        //        wkld_mins = PFSDBUtility.DBToInt(dr2["WORKLOAD_MINUTES"]);
        //        starttm = PFSDBUtility.DBToDateTime(dr2["START_TIME"]);
        //        finishtm = PFSDBUtility.DBToDateTime(dr2["FINISH_TIME"]);
        //        thpwi = PFSDBUtility.DBToDouble(dr2["THPWI"]);
        //        assess = PFSDBUtility.DBToInt(dr2["ASSESSMENT_PCT"]);
        //        interv = PFSDBUtility.DBToInt(dr2["INTERVENTION_PCT"]);
        //        consult = PFSDBUtility.DBToInt(dr2["CONSULTATION_PCT"]);
        //    }
        //    db2.Close();
        //    return shfid;
        //}

        private bool TC_Event_IDExists(int evid)
        {
            bool idexists = false;
            string sql = "select tc_event_id from EVENT_LOG";
            sql += " where timestamp>dateadd(mi,-5,getdate()) and TC_EVENT_ID=" + evid;
            Program.VerboseAudit(sql);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd = new SqlCommand(sql, db2);
            SqlDataReader dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr2.Read())
            {
                if (dr2["TC_EVENT_ID"] != DBNull.Value)
                    idexists = true;
            }
            db2.Close();
            return idexists;

        }

        private void OutputClassAndDNC(bool do_class)
        {
            int i;
            if (numclassdnc == 0 && do_class)
                OutputClass(DateTime.MinValue, DateTime.MinValue);
            else
            {
                for (i = 1; i <= numclassdnc; i++)
                {
                    if (aryclassdnc[i].retain)
                        OutputDNC(aryclassdnc[i].startdt, aryclassdnc[i].enddt);
                    else // do_class will never be false here
                        OutputClass(aryclassdnc[i].startdt, aryclassdnc[i].enddt);
                }
            }
        }

        //        private const string DATETIME_FORMAT = "yyyyMMddHHmm";              // ISO Date/Time w/o seconds
        private const string DATETIME_FORMAT = "yyyyMMddHHmm";              // ISO Date/Time w/o seconds

        private void OutputClass(DateTime dncstdt,DateTime dncendt)
        {
            string outstr, ind_list, desc, str_pull_dt, str_in_dt, str_out_dt = "";
            int i, tc_event_id;
            bool repeat_output = false;
            string repeat_time = "";

            Program.VerboseAudit("cat_ary ub=" + cat_ary.GetUpperBound(0));
            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         212 242 304
            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1
            //1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901 3456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
            //1       |10 5            |                |                |        |10023121779         |ALEXANDER                       |GLORIA                          |                                |0508    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289530  |           |201403030400                                                                      |NNYNNNNYYNNYNNNYNNNYYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
            //1       |10 5            |                |                |        |10024544908         |ARMSTRONG                       |AARON                           |                                |0535    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289531  |           |201403030400                                                                      |NYNNYNNNNNNYNNNYNNNNYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
            //|0547    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289532  |           |201403030400                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN
            //|0547    |A   |201403030700|                |201403030700    |18  |C|    |3040|1440|18289532  |           |201403030700                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//            1RO | RO MB5BG / 6E |                |                |        | 2000180316769 | PARISIEN | GREYSON | LAWRENCE | RMB5514 | P | 20190319030000 |                               | 20 | C |    | 5399 | 480 | 62040560 |           | 20190319030000 | 20190319070000 | NNNYNNNNNNNNYNNNYNYNYNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN

            if (Program.g_is_test)
                tc_event_id = 9999;
            else
                tc_event_id = PFSDBUtility.NextGID();                         //get a unique id for this class
                                                                              //            str_pull_dt = _pat.pull_finish.ToString(DATETIME_FORMAT);
                                                                              //str_pull_dt = _pat.pull_finish.ToString(DATETIME_FORMAT);
                                                                              //if (_pat.unit_departure != null && _pat.unit_departure != DateTime.MinValue)
            if (dncstdt == DateTime.MinValue)
            {
                //                if (_pat.effective_out == Program.g_pull_finish && _pat.unit_departure == DateTime.MinValue)
                Program.VerboseAudit("Loc arrtime=" + loc_arrtime + " gpull_start_save=" + Program.g_pull_start_save);
                if (loc_arrtime >= Program.g_pull_start_save)
                {
                    //str_in_dt = Program.g_effdt.ToString(DATETIME_FORMAT);
                    str_in_dt = loc_arrtime.ToString(DATETIME_FORMAT);
                    str_out_dt = loc_out.ToString(DATETIME_FORMAT);

                    if (loc_arrtime <= loc_in) //Program.g_effdt)
                    {
                        repeat_output = true;//also make the desired class time
                        repeat_time = loc_in.ToString(DATETIME_FORMAT); // Program.g_effdt.ToString(DATETIME_FORMAT);
                    }
                }
                else
                {
                    str_in_dt = loc_in.ToString(DATETIME_FORMAT); // Program.g_effdt.ToString(DATETIME_FORMAT);
                    str_out_dt = "";// _pat.effective_out.ToString(DATETIME_FORMAT);
                    str_out_dt = loc_out.ToString(DATETIME_FORMAT);
                }
            }
            else //dnc
            {
                str_in_dt = dncstdt.ToString(DATETIME_FORMAT);
                str_out_dt = dncendt.ToString(DATETIME_FORMAT);
            }
            if (!Program.g_output_outtime) str_out_dt = "";//leave open-ended for night ASSIGNMENT MODULE 4/14/21

            //outstr = _pat.facilty_code.FixedWidth(8);       //(facility code)
            outstr = "".FixedWidth(8);                       //(facility code)
            outstr += "|" + _pat.unit_name.FixedWidth(16);
            outstr += "|" + "".FixedWidth(16);                               //(unit code)
            outstr += "|" + txarea.FixedWidth(16);                               //(area code)
            outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
            outstr += "|" + _pat.acct.FixedWidth(20);
            outstr += "|" + _pat.last_name.FixedWidth(32);
            outstr += "|" + _pat.first_name.FixedWidth(32);
            outstr += "|" + _pat.middle_name.FixedWidth(32);
            outstr += "|" + _pat.room.FixedWidth(8);
            outstr += "|" + _pat.bed.FixedWidth(4);
            outstr += "|" + str_in_dt;        //CLASS dt
            outstr += "|" + "".FixedWidth(14);                               //(login)
            outstr = outstr.FixedWidth(232);
            outstr += "|" + _pat.effective_out.ToString(DATETIME_FORMAT);//str_out_dt;        //TC Data End Point
            outstr = outstr.FixedWidth(249);
            outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
            outstr += "|" + "C".FixedWidth(1);                               //record type = class
            outstr += "|" + "".FixedWidth(4);                                //(stage)
            outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
            outstr += "|" + _pat.range.ToString().FixedWidth(4);             //TC pull range
            outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
            outstr += "|";
            outstr = outstr.FixedWidth(294);
            outstr += "|" + str_in_dt;        //IN
            outstr = outstr.FixedWidth(346);
            //if (_pat.unit_departure != null && _pat.unit_departure != DateTime.MinValue)
                //                outstr += "|" + _pat.unit_departure.ToString(DATETIME_FORMAT);        //OUT
                outstr += "|" + str_out_dt;        //OUT
            outstr = outstr.FixedWidth(377);
            outstr += "|";

            //if (use_default)
            //{ //make all is_checked = false and then mark defaults
            //    Program.VerboseAudit("Patient Will receive default indicators " + _pat.default_inds_str);
            //    for (i = 1; (i <= MAX_INDS); i++)
            //    {
            //        _inds[i].is_checked = false;
            //    }
            //    foreach (var ind in _pat.default_inds)
            //    {
            //        if (ind <= _inds.GetUpperBound(0))
            //        {
            //            _inds[ind].is_checked = true;
            //        }
            //    }
            //}

            ind_list = "";
            if (!Program.g_catstudy)
            {
                for (i = 1; (i <= MAX_INDS); i++)
                {
                    if (_inds[i].is_checked)
                    {
                        outstr += "Y";
                        ind_list += "" + i;
                    }
                    else
                    {
                        outstr += "N";
                    }
                } // next i
                ind_list = ind_list.Substring(1);                           //strip leading comma
            }
            else //ASSESSMENTS CATEGORY STUDY
            {
                ind_list = "";
                for (i = 1; (i <= 39); i++)
                {
                    if (_inds[i].is_checked)
                    {
                        outstr += "Y";
                        ind_list += "," + i;
                    }
                    else
                    {
                        outstr += "N";
                    }
                } // next i
                for (i = 0; (i <= 9); i++)
                    for (int j = 0; (j <= 3); j++)
                    {
                        if (ar1[i, j])
                        {
                            outstr += "Y";
                            ind_list += "," + 40 + 4 * i + j;
                        }
                        else
                        {
                            outstr += "N";
                        }
                    }
                ind_list = ind_list.Substring(1);                           //strip leading comma
            }                                                          //                                                                                                   1                                                                                                   2                                                                                                   3
                                                                        //         1         2         3         4         5         6         7         8         9         0         1         2         3         4         5         6         7         8         9         0         1         2         3         4         5         6         7         8         9         0         1         2         3         4         5         6         7         8         9         0
                                                                        //1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
                                                                        //1       |DO6D            |                |                |        |2000192224892       |BEHNAM                          |KENDRA                          |LEE                             |RDO6311 |P   |20180717110000|                               |20  |C|    |5399|480 |56103278  |           |20180717110000                                     |                              |YNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
            string str5am;
            Program.outfile.WriteLine(outstr);      //1900yesterd output to transparent.txt
            if (repeat_output) // with repeat_time
            {
                string repeat_line = outstr.Substring(0, 203) + repeat_time + outstr.Substring(215, 80) + repeat_time + " ".Repeat(71) + outstr.Substring(378, 120);
                Program.outfile.WriteLine(repeat_line);
            }
            //if (Program.g_make5am)
            //{
            //    //if (str_out_dt.Substring(8, 4) == "0500") //create the 7am at 3am
            //    {
            //        string strdttm = Program.g_pull_finish.ToString(DATETIME_FORMAT);
            //        strdttm = strdttm.Substring(0, 8) + "0500";
            //        //str5am = outstr.Substring(0, 203) + Program.g_pull_finish.ToString(DATETIME_FORMAT) + outstr.Substring(217, 78) + Program.g_pull_finish.ToString(DATETIME_FORMAT) + " ".Repeat(69) + outstr.Substring(378, 120);
            //        str5am = outstr.Substring(0, 203) + strdttm + outstr.Substring(215, 80) + strdttm + " ".Repeat(71) + outstr.Substring(378, 120);
            //        Program.outfile2.WriteLine(str5am);
            //    }
            //}

            Program.Audit("");
            desc = "Classified: " + ind_list;
            if (Program.g_is_test) {
                Program.Audit(desc);
            } else {
                //Save the selected indicators plus both Program.Audits in the event log; link with tc_event_id
                bool evlog_saved = false;
                int save_attempts = 0;
                while (!evlog_saved && (save_attempts < 3))
                {
                    PFSEventLog.AddTransparentMappingEventLogEntry(
                    desc, Program.gLogUnitID, Program.gLogEncounterID,
                    tc_event_id, Program.gLogMapperVersion,
                    Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());

                    save_attempts++;
                    //seek for event_id
                    // if !exist then wait/sleep 1000milli sec and retry
                    evlog_saved = TC_Event_IDExists(tc_event_id);
                    if (!evlog_saved) PFSUtility.Sleep(500); // wait half a second
                }
            }
        }

        private void OutputClassAndDNCTimes()
        {
            int i;
            DateTime dt1 = Program.g_effdt;
            numclassdnc = 0;
            DateTime lastend = Program.g_effdt;
            // retain has nothing to do with keeping the record. it only means "is DNC".

            for (i = 1; i <= numloa; i++)
            {
                //Why are these historicals stubbed off?? let it go through 10/09/20
                //if (aryloa[i].startdt < dt1) 
                //{
                //    Program.VerboseAudit("historical aryloa i:" + i + " aryloa[].startdt=" + aryloa[i].startdt + " aryloa[].enddt=" + aryloa[i].enddt + " dt1="+dt1);
                //}
                //else
                {
                    if (numclassdnc == 0)
                    {
                        if (aryloa[i].startdt > dt1)
                        {
                            numclassdnc++;
                            aryclassdnc[numclassdnc].startdt = dt1;
                            aryclassdnc[numclassdnc].enddt = aryloa[i].startdt;
                            aryclassdnc[numclassdnc].retain = false; //false=Class
                        }
                        numclassdnc++;
                        aryclassdnc[numclassdnc].startdt = aryloa[i].startdt;
                        aryclassdnc[numclassdnc].enddt = aryloa[i].enddt;
                        aryclassdnc[numclassdnc].retain = true; //true=DNC
                    }
                    else
                    {
                        numclassdnc++;
                        aryclassdnc[numclassdnc].startdt = aryloa[i - 1].enddt;
                        aryclassdnc[numclassdnc].enddt = aryloa[i].startdt;
                        aryclassdnc[numclassdnc].retain = false; //false=Class

                        numclassdnc++;
                        aryclassdnc[numclassdnc].startdt = aryloa[i].startdt;
                        aryclassdnc[numclassdnc].enddt = aryloa[i].enddt;
                        aryclassdnc[numclassdnc].retain = true; //true=DNC
                    }
                    lastend = aryloa[i].enddt;
                }
            } //for
            if (numclassdnc > 0 && lastend != dt1.AddHours(12)) 
                // then the loa ended before this draw period end
            {
                numclassdnc++;
                aryclassdnc[numclassdnc].startdt = lastend; // aryloa[i - 1].enddt;
                aryclassdnc[numclassdnc].enddt = dt1.AddHours(12);
                aryclassdnc[numclassdnc].retain = false; //false=Class
            }
            for (i=1;i<=numclassdnc;i++)
            {
                string classdnctype="";
                if (aryclassdnc[i].retain)
                    classdnctype = "DNC";
                else
                    classdnctype = "Class";
                Program.VerboseAudit(classdnctype + ": " + aryclassdnc[i].startdt + "-" + aryclassdnc[i].enddt);
            }
        }

        private void OutputDNC(DateTime dncstdt, DateTime dncendt)
        {
            string outstr, ind_list, desc, str_pull_dt, str_in_dt, str_out_dt = "";
            int i, tc_event_id;

            if (Program.g_is_test)
                tc_event_id = 9999;
            else
                tc_event_id = PFSDBUtility.NextGID();                         //get a unique id for this class

            //str_in_dt = aryloa[i].startdt.ToString(DATETIME_FORMAT);
            //str_out_dt = aryloa[i].enddt.ToString(DATETIME_FORMAT);
            str_in_dt = dncstdt.ToString(DATETIME_FORMAT);
            str_out_dt = dncendt.ToString(DATETIME_FORMAT);

            //outstr = _pat.facilty_code.FixedWidth(8);                       //(facility code)
            outstr = "".FixedWidth(8);                       //(facility code)
            outstr += "|" + _pat.unit_name.FixedWidth(16);
            outstr += "|" + "".FixedWidth(16);                               //(unit code)
            outstr += "|" + "".FixedWidth(16);                               //(area code)
            outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
            outstr += "|" + _pat.acct.FixedWidth(20);
            outstr += "|" + _pat.last_name.FixedWidth(32);
            outstr += "|" + _pat.first_name.FixedWidth(32);
            outstr += "|" + _pat.middle_name.FixedWidth(32);
            outstr += "|" + _pat.room.FixedWidth(8);
            outstr += "|" + _pat.bed.FixedWidth(4);
            outstr += "|" + str_in_dt;        //CLASS dt
            outstr += "|" + "".FixedWidth(14);                               //(login)
            outstr = outstr.FixedWidth(232);
            outstr += "|" + str_out_dt;  // TC END POINT_pat.effective_out.ToString(DATETIME_FORMAT);//TC Data End Point
            outstr = outstr.FixedWidth(249);
            outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
            outstr += "|" + "D".FixedWidth(1);                               //record type = class
            outstr += "|" + "".FixedWidth(4);                                //(stage)
            outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
            outstr += "|" + "".FixedWidth(4);             //TC pull range
            outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
            outstr += "|";
            outstr = outstr.FixedWidth(294);
            outstr += "|" + str_in_dt;        //IN
            outstr = outstr.FixedWidth(346);
            outstr += "|" + str_out_dt;        //OUT
            outstr = outstr.FixedWidth(377);
            outstr += "|NNNNY";

            Program.outfile.WriteLine(outstr);      //1900yesterd output to transparent.txt
            Program.Audit("");
            desc = "Classified DoNotClassify: NNNNY";
                //Save the selected indicators plus both Program.Audits in the event log; link with tc_event_id
                PFSEventLog.AddTransparentMappingEventLogEntry(
                    desc, Program.gLogUnitID, Program.gLogEncounterID,
                    tc_event_id, Program.gLogMapperVersion,
                    Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());

        }


        private int DeterminePtypeOfIndicators()
        {
            int i;
            var pscore = 0.0;
            int pt_type = 6;
            var indlist = new List<int>();
            string indstr = "";

            Program.VerboseAudit("Determine patient type of triggered indicators");

            for (i = 1; (i <= MAX_INDS); i++)
            {
                if (_inds[i].is_checked)
                {
                    indlist.Add(i);
                    indstr += i.ToString() + ",";
                    pscore += _inds[i].weight;
                }
            }

            Program.VerboseAudit("indicators=" + indstr + " score=" + pscore.ToString());

            var db = PFSDBUtility.NewPfsDataContext();
            var query_ptype = from ptype in db.PATIENT_TYPEs
                              where (ptype.METHODOLOGY_ID == _pat.meth_id)
                              orderby ptype.PATIENT_TYPE1
                              select new
                              {
                                  ptype.PATIENT_TYPE1,
                                  ptype.POINTS_HIVAL
                              };

            foreach (var ptypes in query_ptype)
            {
                //                Program.DebugTrace("type,hival=" + ptypes.PATIENT_TYPE1.ToString() + "" + ptypes.POINTS_HIVAL.ToString(), "");
                if (pscore <= ptypes.POINTS_HIVAL)
                {
                    if (pt_type > ptypes.PATIENT_TYPE1)
                    {
                        pt_type = ptypes.PATIENT_TYPE1;
                    }
                }
            }
            Program.VerboseAudit("patient type=" + pt_type.ToString());

            return pt_type;

        }


        //private bool ExistDefaultInPast16hrs()
        //{
        //    DateTime cdt1 = DateTime.MinValue;
        //    DateTime cdt2 = DateTime.MinValue;
        //    int cnt_all = 0;
        //    int cnt_def = 0;
        //    //get max class date of last non-default class = 1
        //    //get max class date of last default = 2
        //    // if 1 >= 2 then false
        //    // else
        //    //   if 2 > 1 and date is <= 16 hrs ago then true
        //    //   else false
        //    var db = PFSDBUtility.NewPfsDataContext();
        //    //var query = from ce in db.CLASSIFICATION_EVENTs
        //    //            where (ce.ENCOUNTER_ID == _pat.encounter_id)
        //    //            && (ce.CLASSIFIED_BY_ID != -2)
        //    //            && (System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(ce.CLASSIFICATION_DATETIME, Program.g_pull_start) <= 960)
        //    //            select ce;
        //    //cnt_all = query.Count();
        //    //if (cnt_all > 0)
        //    //{
        //    //    cdt1 = PFSUtility.DBToDateTime(query.Max(x => x.CLASSIFICATION_DATETIME));
        //    //    Program.VerboseAudit("Last regular classification was at:"+cdt1.ToString());
        //    //}
        //    //else {
        //    //    Program.VerboseAudit("No regular classifications within the past 16 hours");
        //    //}

        //    var query = from ce in db.CLASSIFICATION_EVENTs
        //                where (ce.ENCOUNTER_ID == _pat.encounter_id)
        //                && (ce.CLASSIFIED_BY_ID == -2)
        //                && (System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(ce.CLASSIFICATION_DATETIME, Program.g_pull_finish) <= 960)
        //                select ce;
        //    cnt_def = query.Count();

        //    if (cnt_def > 0)
        //    {
        //        cdt2 = PFSDBUtility.DBToDateTime(query.Max(x => x.CLASSIFICATION_DATETIME));
        //        Program.VerboseAudit("Last default classification was at:" + cdt2.ToString());
        //    }
        //    else
        //    {
        //        Program.VerboseAudit("No default classifications within the past 16 hours");
        //    }
        //    return (cnt_def > 0);

        //}

        //=====================================================================
        // LOA DNC coding
        //=====================================================================

        private bool SetLOARanges(bool do_loa)  //return if currently LOA
        {
            int numstartloa = 0, numendloa, i, j;
            string sql, sql2;
            DateTime startdt=DateTime.MinValue, endt;

            //        public struct LOAtypePrecision
            //{
            //    public DateTime startdt;
            //    public DateTime enddt;
            //    public DateTime timestamp;
            //    public bool cancel;
            //    public bool retain;
            //}
            Program.VerboseAudit("Set LOA ranges");


            //'there shouldnt be 2 starts in a row
            //'-- choose the later timestamp and cancel the earlier one
            //'ONLY check for starts less than the finish time--dont want to do future dncs.

            numstartloa = 0;
            //sql = "select code,event_datetime,timestamp from chart_item where encounter_id=" + pat.encounter_id;
            //sql += " and order_status<>'X' and event_datetime<" + PFSDBUtility.SQLDateTime(g_pull_finish);
            //sql += " order by timestamp";

            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = query.Where(e => e.CODE.ToLower().StartsWith("@start"));
            query = query.Where(e => e.CATEGORY.ToLower() == "loa");
            query = query.Where(e => e.ORDER_STATUS == null || e.ORDER_STATUS == ""); // need to update order_status=x
            query = query.Where(e => e.EVENT_DATETIME <= _pat.pull_finish);
            query = query.OrderBy(e => e.EVENT_DATETIME);
            int ct = query.Count();
            Program.VerboseAudit("ct=" + ct + " pull_finish=" + _pat.pull_finish);
            if (ct == 0)
            {
                return false;
            }
            if (!do_loa) // ct > 0
            {
                return true; //there is a loa but this is not a draw for DNCs.
            }
            Program.VerboseAudit("qloacount=" + ct);

            bool atstart = false;
            bool foundend = false;
            var query2 = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query2 = query2.Where(e => e.CODE.StartsWith("@"));
            query2 = query2.Where(e => e.CATEGORY.ToLower() == "loa");
            query2 = query2.Where(e => e.ORDER_STATUS == null || e.ORDER_STATUS == ""); // need to update order_status=x
            query2 = query2.Where(e => e.EVENT_DATETIME <= _pat.pull_finish);
            query2 = query2.OrderBy(e => e.EVENT_DATETIME);
            ct = query2.Count();
            Program.VerboseAudit("q2loacount=" + ct);
            foreach (var item in query2)
            {
                Program.VerboseAudit(item.CODE + ":" + item.EVENT_DATETIME + " at:" + item.TIMESTAMP);
                if (item.CODE.ToUpper() == "@START_LOA")
                {
                    if (atstart) // then found 2 starts in a row -- KEEP the earliest start as the LOA start
                        Program.VerboseAudit("found 2nd start:" + item.EVENT_DATETIME);
                    else
                    {
                        startdt = item.EVENT_DATETIME;
                        atstart = true;
                        foundend = false;
                    }
                }
                else if (item.CODE.ToUpper() == "@CANCEL_LOA" || item.CODE.ToUpper() == "@FINISH_LOA")
                {
                    foundend = true;
                    if (!atstart) // then no preceding start
                        Program.VerboseAudit("ignoring stop at:" + item.EVENT_DATETIME);
                    else
                    {
                        if (item.EVENT_DATETIME == startdt)
                        {
                            DisableLOA(startdt,startdt);
                        }
                        else if (item.EVENT_DATETIME > startdt)
                        {
                            numloa++;
                            aryloa[numloa].startdt = startdt;
                            aryloa[numloa].enddt = item.EVENT_DATETIME;
                            DisableLOA(startdt,item.EVENT_DATETIME);
                        }
                    }
                    atstart = false;

                }
            } //foreach
            if (atstart && !foundend) // then finish still not entered
            {
                numloa++;
                aryloa[numloa].startdt = startdt;
                aryloa[numloa].enddt = _pat.pull_finish;//artificial finish at pull_finish
                AddStartLOA(_pat.pull_finish);//create artificial start at pull_finish for the next draw
                DisableLOA(startdt, startdt);//disable this start
            }
            return true;
        }

        private void DisableLOA(DateTime startevdt, DateTime endevdt)
        {
            var db = PFSDBUtility.NewSqlConnection();
            string q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and code like '@%' and category='loa' and event_datetime>=" + PFSDBUtility.SQLDateTime(startevdt) + " and event_datetime<=" + PFSDBUtility.SQLDateTime(endevdt);
            SqlCommand cmd = new SqlCommand(q, db);
            cmd.ExecuteNonQuery();
            db.Close();
            Program.VerboseAudit("disableLOA:" + q);
        }
        private void AddStartLOA(DateTime startevdt)
        {
            string code = "@START_LOA";
            string cat = "LOA";
            string desc = "Added by TC";
            DateTime timestmp = DateTime.Now;
            short seq = 0;
            int unitid = _pat.unit_id;

            using (var db = PFSDBUtility.NewSqlConnection())
            {
                string q = "INSERT INTO chart_item (encounter_id,event_datetime,code,category,description,timestamp,sequence,unit_id)";
                q += " select @encid, @evdt, @code, @cat, @desc, @ts,@seq,@unit where not exists";
                q += " (select encounter_id,code,event_datetime from chart_item where encounter_id=" + _pat.encounter_id.ToString() + " and code='" + code + "' and event_datetime=" + PFSDBUtility.SQLDateTime(startevdt)+ ")";
                //string q = "INSERT INTO chart_item (encounter_id,event_datetime,code,description,timestamp,sequence,unit_id,order_id) values (@encid, @evdt, @code, @desc, @ts,@seq,@unit,@oid)";
                SqlCommand cmd = new SqlCommand(q, db);
                cmd.Parameters.AddWithValue("@encid", _pat.encounter_id);
                cmd.Parameters.AddWithValue("@evdt", startevdt);
                cmd.Parameters.AddWithValue("@code", code);
                cmd.Parameters.AddWithValue("@cat", cat);
                cmd.Parameters.AddWithValue("@desc", desc);
                cmd.Parameters.AddWithValue("@ts", timestmp);
                cmd.Parameters.AddWithValue("@seq", seq);
                cmd.Parameters.AddWithValue("@unit", unitid);
                cmd.ExecuteNonQuery();
                db.Close();
            } //using db

        }


        private void OutputProcs()
        {
            int i;
            string outstr, proc_list, desc;
            int tc_event_id;

            foreach (var proc in _procs)
            {
                if (Program.g_is_test)
                    tc_event_id = 9999;
                else
                    tc_event_id = PFSDBUtility.NextGID();                         //get a unique id for this proc

                outstr = "".FixedWidth(8);                       //(facility code)
                outstr += "|" + _pat.unit_name.FixedWidth(16);
                outstr += "|" + "".FixedWidth(16);                               //(unit code)
                outstr += "|" + txarea.FixedWidth(16);                               //(area code)
                outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
                outstr += "|" + _pat.acct.FixedWidth(20);
                outstr += "|" + _pat.last_name.FixedWidth(32);
                outstr += "|" + _pat.first_name.FixedWidth(32);
                outstr += "|" + _pat.middle_name.FixedWidth(32);
                outstr += "|" + _pat.room.FixedWidth(8);
                outstr += "|" + _pat.bed.FixedWidth(4);
                outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //204 proc dt
                outstr += "|" + "".FixedWidth(14);                               //(login)
                outstr = outstr.FixedWidth(249);
                outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
                outstr += "|" + "P".FixedWidth(1);                               //record type = class
                outstr += "|" + "".FixedWidth(4);                                //(stage)
                outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
                outstr += "|" + _pat.range.ToString().FixedWidth(4);             //TC pull range
                outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
                outstr += "|";
                outstr = outstr.FixedWidth(294);
                outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //296 procdt in
                outstr = outstr.FixedWidth(346);
                //if (_pat.unit_departure != null && _pat.unit_departure != DateTime.MinValue)
                //                outstr += "|" + _pat.unit_departure.ToString(DATETIME_FORMAT);        //OUT
                outstr += "|" + proc.finish.ToString(DATETIME_FORMAT);          //348 procdt out
                outstr = outstr.FixedWidth(377);
                outstr += "|";

                proc_list = "";
                for (i = 1; (i < MAX_PROCS); i++)
                {
                    if (proc.procedure_number == i)
                    {
                        outstr += "Y";
                        proc_list += "" + i;
                    }
                    else
                    {
                        outstr += "N";
                    }
                } // next i
                proc_list = proc_list.Substring(1);                             //strip leading comma

                Program.outfile.WriteLine(outstr);                              //output to transparent.txt

                desc = "Activities: " + proc_list;
                if (Program.g_is_test)
                {
                    Program.Audit(desc);
                }
                else
                {
                    //Save the selected procedures plus both Program.Audits in the event log; link with tc_event_id
                    //(this assumes the procedure Program.Audit is mixed in with the class Program.Audit)
                    PFSEventLog.AddTransparentMappingEventLogEntry(desc, Program.gLogUnitID, Program.gLogEncounterID,
                        tc_event_id, Program.gLogMapperVersion,
                        Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
                }
            } // next proc
        }

    }
}


