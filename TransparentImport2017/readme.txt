Things to change to upgrade an older C# project to this:

1. make sure csproj, sln are read/write;  ..\shared2\
2. open sln with VS 2015
3. change PFSUtility to PFSDBUtility
4. change project to use .NET 4.5
