﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;                     // for LINQ; add a reference
using System.Data.SqlClient;                // for SqlConnection
using System.IO;                            // for Directory, Path, Stream
using PfsShared;                            // add a reference to Shared2 project

// ============================================================================
// Inpatient transparent mapping -- GOES HERE --
// NIH Allscripts
// ============================================================================
// This processes one patient.  Only one database query is used.  :)
//
// All search functions use exact match for category, description and field name.
// Codes and results can be a single word or a list of words.
// Codes default to exact match; prefix with CODE_CODE_LIKE_PREFIX if desired (single word only)
// Results default to like matching; prefix with RESULT_EXACT_MATCH_PREFIX if desired (single word only)
//
// All searches are case insensitive.
//
//
namespace TransparentMapping
{
    class Inpatient
    {
        private const int MAX_INDS = 120;
        private const int MAX_PROCS = 20;

        private const string CODE_LIKE_PREFIX = "%!";       // use to make codes %like%
        private const string EXACT_MATCH_PREFIX = "&!";     // use to make desc/result exact match
        private const string CHAR_COMMA = "||";             // use to insert literal comma in word list
        private const string NOT_PREFIX = "!!";             // use to insert literal comma in word list
        private const string AVOID_NEGATIVE = "!;";
        private const string EXACT_SQL_PREFIX = "!SQL!";     // use to give sql constraint as given
        private const string STARTS_WITH = "[!";     // use to give sql constraint as given

        private struct indicator_data {
            public bool is_checked;
            public int radio_group;
            public int weight;
        }

        private struct proc_data {
            public int procedure_number;
            public DateTime start;
            public DateTime finish;
        }

        // These are database CHART_ITEMs for this patient
        private CHART_ITEM[] _chart_items_since_admission;
        //private CHART_ITEM[] _chart_items_since_unit_arrival;
        //private CHART_ITEM[] _chart_items_during_pull_period;
        //private CHART_ITEM[] _chart_items_pull_period_plus;
        //private CHART_ITEM[] _chart_items_since24hrs;
        //private CHART_ITEM[] _chart_items_since13hrs;
        private CHART_ITEM[] _chart_items_since24hrs;
        //private CHART_ITEM[] _chart_items_since9hrs;
        // These are database PROCEDURE_EVENTs for this patient
        private PROCEDURE_EVENT[] _procedure_events;

        private indicator_data[] _inds;
        private List<proc_data> _procs;
        private List<proc_data> _outcomes;

        private const int DEFAULT_BUCKET_SIZE = 60;       //min

        private PatientInfo _pat;
        private int _bucket_size = DEFAULT_BUCKET_SIZE;
        private bool is_default;
        private string txarea;
        private bool periop_found_inpast13hrs = false;

        private bool exclude_periop_data = false;
        private bool exclude_periph_iv = false;
        private bool coma = false;
        private bool g_toi4 = false;
        private bool g_gitube_attempted = false;
        private bool g_gitube = false;
        private DateTime loc_in;
        private DateTime loc_out;
        private DateTime loc_arrtime;
        int rassct12 = 0;
        int rassct34 = 0;
        int rassctLTzero = 0;
        private LOAtypePrecision[] aryloa = new LOAtypePrecision[10];
        private int numloa=0;
        private LOAtypePrecision[] ary_hemodial = new LOAtypePrecision[5];
        private int numhemodial = 0;
        private LOAtypePrecision[] aryclassdnc = new LOAtypePrecision[10];
        private int numclassdnc = 0;
        private bool g_lda6yo = false;
        private bool g_iabp = false;

        private string assessgrouplabel = "";

        private enum SearchDepth {
            SearchDefault,
            SearchPullRange,            //search within the current pull     -- default
            SearchSinceArrival,         //search since arrival to the unit
            SearchSinceAdmission,        //search everything since admission to the hospital
            SearchPullPlus,
            SearchSince24Hrs,
            SearchSince13Hrs,
            SearchSince12Hrs,
            SearchSince16Hrs,
            SearchSince9Hrs,
            SearchSince4Hrs
        }

        private enum CountMode {
            CountAll,
            CountFirst                  //stop after one is found
        }

        private enum GetValueMode {
            GetTotal,
            GetMax,
            GetLast
        }

        enum Frequencies {
            QNONE,
            Q4H,
            Q2H,
            Q1H,
            Q30M
        }

        private struct fmapRow {
            public double los_high;                       //the LOS being testing
            public int[] freq;                           //the count required for each Q value
        }
        private List<fmapRow> _freq_map;                    //1,2,4,8,12,24 hours

        private struct gBucket
        {
            public int bucket;
            public string code;
            public DateTime evdt;
            public bool using_waiver;
            public bool has_all_deps; //has all dependents
            public int num_addl_items;
            public string description;
        }
        private struct gGap
        {
            public int gap;
            public DateTime evdt1;
            public DateTime evdt2;
        }

        private struct MedChartItem
        {
            public string code;
            public string orderid;
            public DateTime evdt;
            public bool valid;
        }
        //private string[] meds_rate = { "newbag", "new bag", "rateverify", "rate verify", "restarted", "ratechange", "rate change", "started", "continued" };
        //private struct med2026and5077
        //{
        //    public string name;
        //    public bool found;
        //}
        //
        // This is the main entry point
        //
        public void ProcessPatient(PatientInfo pat)
        {
            _pat = pat;
            bool use_default = false;
            bool no_chart_items_in_24hrs = false;
            bool loa_exists = false;
            bool do_class = true;

            InitIndicators(); // sets is_default
            InitProcs();
            if (!is_default)
            {
                LoadFreqTable();
                //CheckIfPeriopInPast13hrs();
                no_chart_items_in_24hrs = (LoadPatientChart() == 0);
                Program.VerboseAudit("New query default scope = " + loc_in + " to " + loc_out);
                if (no_chart_items_in_24hrs)
                    Program.Audit("No chart items received in past 24 hrs.");
                if (!Program.g_onlydnc && do_class && !Program.g_only_do_procs)
                {
                    Check_1_2_3_4();
                    Check_5();
                    Check_6_7();
                    Check_8();
                    Check_9();
                    Check_10_11();
                    Check_12_13();
                    Check_14();
                    Check_15_16_17_18();
                    Check_19();
                    Check_20();
                    Check_21_22();
                    Check_23();
                    Check_24();
                    //CheckUserDefined();
                    AtLeastOneADL();
                }
            }

            //if (!no_chart_items_in_24hrs)
            {
                if (!Program.g_onlydnc && do_class)
                {
                    if (!Program.g_only_do_procs)
                        HighestIndicatorInEachGroupWins();

                    if (!is_default)
                    {
                        if (!Program.g_only_do_procs)
                            _pat.ptype = DeterminePtypeOfIndicators();
                        //Program.sttimer();
                        if (!Program.g_noactivities) CheckProcs();
                        //Program._t4 = Program.entimer("t4=", Program._t4);
                        //CheckOutcomes();
                        //if (Program.g_do_OW) CheckOtherWorkload();
                    }

                    if (Program.g_no_output) return;
                    //if (_pat.default_ptype > DeterminePtypeOfIndicators())
                    //{ // if the default pt type is higher than the pt type of this class
                    //  // then if there is a default classification in the past 16 hrs
                    //  // then make these indicators the default indicators.
                    //    use_default = ExistDefaultInPast16hrs();
                    //    if (use_default)
                    //    {
                    //        Program.VerboseAudit("Default indicators will be used");
                    //    }
                    //}

                    if (!Program.g_only_do_procs)
                    {
                        OutputClassAndDNCTimes();
                        OutputClassAndDNC(do_class);
                    }
                    if (!Program.g_noactivities) OutputProcs();
                }
                else
                {
                    //if (!Program.g_noactivities) OutputDNC();
                }
                //OutputOutcomes();
            }
        }

        private void CheckIfPeriopInPast13hrs()
        {
            foreach (var perioploc in Program.patperioplist)
            {
                periop_found_inpast13hrs = periop_found_inpast13hrs
                    || (perioploc.in_time >= _pat.pull_finish.AddHours(-13))
                    || (perioploc.out_time >= _pat.pull_finish.AddHours(-13));
            }
        }

        private void InitIndicators()
        {
            // Make a new (empty) indicator array
            _inds = new indicator_data[MAX_INDS + 1];           // This 1 based so add one
            is_default = false;
            txarea = "";
            //if (_pat.los_hours <= 4.0)
            //{
            //    is_default = true;
            //    Program.VerboseAudit("Patient was here 4 hrs or less. Will receive default indicators " + _pat.default_inds_str);
            //    foreach (var ind in _pat.default_inds)
            //    {
            //        if (ind <= _inds.GetUpperBound(0))
            //        {
            //            _inds[ind].is_checked = true;
            //        }
            //    }
            //}


            // get indicator radio groups from the database
            // ** (This database access can be replaced once we have a C# methodolgy cache)
            var db = PFSDBUtility.NewPfsDataContext();
            var query = from idef in db.INDICATOR_DEFINITIONs
                        where (idef.METHODOLOGY_ID == _pat.meth_id)
                        select idef;
            foreach (var idef in query) {
                if (idef.INDICATOR_NUMBER <= _inds.GetUpperBound(0)) {
                    // (convert nulls to zero)
                    _inds[idef.INDICATOR_NUMBER].radio_group = PFSDBUtility.DBToInt(idef.RADIO_GROUP);
                    _inds[idef.INDICATOR_NUMBER].weight = PFSDBUtility.DBToInt(idef.WEIGHT);
                }
            }
        }

        private void InitProcs()
        {
            _procs = new List<proc_data>();
            _outcomes = new List<proc_data>();
        }

        private fmapRow LoadFreqTableRow(double los_high, string values)
        {
            fmapRow fmrow;

            fmrow.los_high = los_high;
            fmrow.freq = new int[Enum.GetNames(typeof(Frequencies)).Length];

            var arr = values.Split(',');
            for (int i = 0; i <= fmrow.freq.GetUpperBound(0); i++)
            {
                fmrow.freq[i] = arr[i].ToInteger();
            }
            return fmrow;
        }

        // These are the number of times you need to do something during a certain time period (LOS)
        // in order for it to be considered one of the frequencies listed.
        private void LoadFreqTable()
        {
            _freq_map = new List<fmapRow>();
            //                              LOS,  None Q4h Q2h Q1h Q30m
            _freq_map.Add(LoadFreqTableRow(1, "    0,  0,  0,  1,  2"));
            _freq_map.Add(LoadFreqTableRow(2, "    0,  0,  1,  2,  4"));
            _freq_map.Add(LoadFreqTableRow(4, "    0,  1,  2,  4,  8"));
            _freq_map.Add(LoadFreqTableRow(6, "    0,  2,  3,  6,  9"));
            _freq_map.Add(LoadFreqTableRow(8, "    0,  2,  4,  8, 12"));
            _freq_map.Add(LoadFreqTableRow(12, "   0,  3,  6, 12, 18"));
            _freq_map.Add(LoadFreqTableRow(16, "   0,  4,  8, 16, 24"));
            _freq_map.Add(LoadFreqTableRow(24, "   0,  6, 12, 24, 36"));
            //New freq table 2/5/14
            //q4	q2	q1	q30     q30
            //            Non-ICU	ICU & SD
            // 4	8	15	29	    36
            // 3	5	9	17	    24
            // 2	4	7	13	    19
            // 2	3	5	10	    13

            //to read this table: if you have at least 6 asessmenets in a 12-hour period, you get Q1h
            //(LOS=12, column Q1h has a count of 6)
        }

        private int LoadPatientChart()
        {
            foreach (var p in Program.patloclist)
            {
                if (p.loc_idx == _pat.loc_idx)
                {
                    loc_in = p.in_time;
                    loc_out = p.out_time;
                    loc_arrtime = p.arr_time;
                }
            }
            _pat.los_hours = PFSUtility.DateDiffInMinutes(loc_in, loc_out) / 60.0;
            Program.VerboseAudit("LoadChart los=" + _pat.los_hours);
            //Program.VerboseAudit("LoadChart unit=" + p.unit_name + " locidx=" + p.loc_idx + " in=" + p.in_time + " out=" + p.out_time);

            int ct_in_24hrs = 0;
            int ctperiop = 0;
            // Get the entire patient chart (all units and dates for this patient)
            var dba = PFSDBUtility.NewPfsDataContext();
            var queryall = from item in dba.CHART_ITEMs
                           where (item.ENCOUNTER_ID == _pat.encounter_id)
                           where (item.EVENT_DATETIME <= loc_out)
                         orderby item.EVENT_DATETIME
                         select item;
            var querya = from g in queryall
                         select g;
            // Save the result
            _chart_items_since_admission = querya.ToArray();
            Program.VerboseAudit("Count since adm=" + querya.Count());
            

            var query = from item in _chart_items_since_admission
                        where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-24) 
                               && (item.EVENT_DATETIME <= _pat.pull_finish))
                        select item;
            // Save the result
            ct_in_24hrs = query.Count();
            _chart_items_since24hrs = query.ToArray();

            // Convert all results to lower case for case insensitive comparisons
            foreach (var item in _chart_items_since24hrs) {
                item.SOURCE_TEXT = null;
                if (item.CATEGORY != null) item.CATEGORY = item.CATEGORY.ToLower();
                if (item.CODE != null) item.CODE = item.CODE.ToLower();
                if (item.DESCRIPTION != null) item.DESCRIPTION = item.DESCRIPTION.ToLower();
                if (item.FIELD_NAME != null) item.FIELD_NAME = item.FIELD_NAME.ToLower();
                if (item.RESULT != null) item.RESULT = item.RESULT.ToLower();
            }

            Program.VerboseAudit("Since 24 hrs count items=" + ct_in_24hrs);
            Program.VerboseAudit("Since 24 hrs count of HD items=" + ctperiop);

            return ct_in_24hrs;
        }

        // Delay looking for patient procedures until they are wanted
        private void LoadPatientProceduresIfNeeded()
        {
            if (_procedure_events != null) return;

            var db = PFSDBUtility.NewPfsDataContext();
            var query = from proc in db.PROCEDURE_EVENTs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                        where (proc.PROCEDURE_DATETIME >= _pat.pull_finish.AddHours(-24))
                        select proc;
            _procedure_events = query.ToArray();
        }



        // Started a new chart item query (of a certain depth)
        // Default is the chart during the pull range.  Options for unit arrival and admission to hospital.
        private IEnumerable<CHART_ITEM> StartNewQuery()
        {
            return StartNewQuery(SearchDepth.SearchDefault);
        }
        private IEnumerable<CHART_ITEM> StartNewQuery(SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;

            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    if (exclude_periop_data)
                        //return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_start && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= loc_in && (item.EVENT_DATETIME <= loc_out)) select item);
                        //return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-12)) select item);
                    else
                        //return (from item in _chart_items_since24hrs where (item.EVENT_DATETIME >= _pat.pull_start && item.EVENT_DATETIME <= _pat.pull_finish) select item);
                        return (from item in _chart_items_since24hrs where (item.EVENT_DATETIME >= loc_in && item.EVENT_DATETIME <= loc_out) select item);
                        //return (from item in _chart_items_since24hrs where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-12)) select item);
                //if (exclude_periop_data)
                //    return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-8) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                //else
                //    return (from item in _chart_items_since24hrs where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-8) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                //    return (from item in _chart_items_since_unit_arrival select item);
                case SearchDepth.SearchSinceAdmission:
                    return (from item in _chart_items_since_admission select item);
                //case SearchDepth.SearchPullPlus:
                //    return (from item in _chart_items_pull_period_plus select item);
                case SearchDepth.SearchSince24Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                    else
                        return (from item in _chart_items_since24hrs where (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince16Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-16) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since24hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-16) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince13Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-13) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since24hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-13) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince12Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-12) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since24hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-12) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince9Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-9) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since24hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-9) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince4Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME > _pat.pull_finish.AddHours(-4) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since24hrs where item.EVENT_DATETIME > _pat.pull_finish.AddHours(-4) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
            }
            return null;
        }

        // Is this search term a word or a list of words?
        private bool ValueIsAList(string s)
        {
            return (s != null) && s.Contains(',');                //is this a comma-separated list?
        }

        // Break apart a wordlist and prepare its members
        private string[] SplitOnCommaAndPrepareElements(string s)
        {
            // If a word list has a space after each comma, we will get leading blanks in each term
            var arr = s.Split(',');
            for (int i = 0; i <= arr.GetUpperBound(0); i++) {
                arr[i] = arr[i].Trim();                         // get rid of leading blanks
                arr[i] = arr[i].Replace(CHAR_COMMA, "");       // insert a literal comma into search word
                arr[i] = arr[i].ToLower();                      // convert for case-insensitive comparisons
            }
            return arr;
        }

        // Add a basic chart item filter to a LINQ query
        //
        // cat = exact match
        // code_list = exact match; optional "like" match
        // desc_list = "like" match; optional exact match
        // field = exact match
        // result_list = "like" match; optional exact match
        //
        // These are comma-separated lists.  
        // Search for a literal comma by using CHAR_COMMA in the string.
        private IEnumerable<CHART_ITEM> AndItemFilter(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list)
        {
Program.VerboseAudit("AndItemFilter:code=" + code_list + " desc=" + desc_list);
            // Converted all values to lower case for case-insensitive comparisons.
            if (!String.IsNullOrEmpty(cat))
            {
                cat = cat.ToLower();
                query = query.Where(e => e.CATEGORY.ToLower() == cat);
            }

            if (!String.IsNullOrEmpty(code_list))
            {
                code_list = code_list.ToLower();
                if (code_list.Left(2) == CODE_LIKE_PREFIX)
                {            // override for "like" match?
                    query = query.Where(e => e.CODE.Contains(code_list.Substring(2)));
                }
                else if (code_list.Left(2) == EXACT_MATCH_PREFIX)
                {
                    query = query.Where(e => e.CODE == code_list.Substring(2));
                }
                else if (ValueIsAList(code_list))
                {
                    query = AndCodeInList(query, code_list);            // find one of the words
                }
                else
                {
                    query = query.Where(e => e.CODE.StartsWith(code_list));      // find this word
                }
            }

            if (!String.IsNullOrEmpty(desc_list))
            {
                desc_list = desc_list.ToLower();
                if (desc_list.Left(2) == EXACT_MATCH_PREFIX)
                {          // exact match override?
                    query = query.Where(e => e.DESCRIPTION == desc_list.Substring(2));
                }
                else if (desc_list.Left(2) == NOT_PREFIX)
                {
                    // query = query.Where(e => !e.DESCRIPTION.Contains(desc_list.Substring(2)));
                    query = AndDescriptionNOTInList(query, desc_list.Substring(2));
                }
                else if (ValueIsAList(desc_list))
                {
                    query = AndDescriptionInList(query, desc_list);
                }
                else
                {
                    desc_list = desc_list.Replace(CHAR_COMMA, "");     // look for literal comma requests
                    query = query.Where(e => e.DESCRIPTION.Contains(desc_list));
                }
            }

            if (!String.IsNullOrEmpty(field))
            {
                field = field.ToLower();
                query = query.Where(e => e.FIELD_NAME == field);
            }

            if (!String.IsNullOrEmpty(result_list))
            {
                query = query.Where(e => e.RESULT != null);
                result_list = result_list.ToLower();                        // make all results case insensitve
                if (result_list.Left(2) == EXACT_MATCH_PREFIX)
                {     // exact match override?
                    query = query.Where(e => e.RESULT == result_list.Substring(2));
                }
                else if (result_list.Left(2) == AVOID_NEGATIVE)
                {
                    query = query.Where(e => (!e.RESULT.Contains("no " + result_list.Substring(2))) && ((e.RESULT == result_list.Substring(2)) || (e.RESULT.Contains(";" + result_list.Substring(2)))));
                }
                else if (ValueIsAList(result_list))
                {
                    query = AndResultInList(query, result_list);
                }
                else if (result_list.Left(2) == STARTS_WITH)
                {
                    query = query.Where(e => e.RESULT.StartsWith(result_list.Substring(2)));
                }
                else
                {
                    result_list = result_list.Replace(CHAR_COMMA, "");     // look for literal comma requests
                    query = query.Where(e => e.RESULT.Contains(result_list));
                }
            }
            return query;
        }

        // Look for a code that is one of the words in the list.
        private IEnumerable<CHART_ITEM> AndCodeInList(IEnumerable<CHART_ITEM> query, string code_list)
        {
            if (String.IsNullOrEmpty(code_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(code_list);
            //            return query.Where(e => arr.Any(x => x.Contains(e.CODE))); // "like" match
            return query.Where(e => e.CODE.ContainsAny(arr)); // "like" match
        }

        // Look for a description that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndDescriptionInList(IEnumerable<CHART_ITEM> query, string desc_list)
        {
            if (String.IsNullOrEmpty(desc_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(desc_list);
            //return query.Where(e => arr.Any(x => x.Contains(e.DESCRIPTION))); // "like" match
            return query.Where(e => e.DESCRIPTION.ContainsAny(arr)); // "like" match
        }
        private IEnumerable<CHART_ITEM> AndDescriptionNOTInList(IEnumerable<CHART_ITEM> query, string desc_list)
        {
            if (String.IsNullOrEmpty(desc_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(desc_list);
            //return query.Where(e => arr.Any(x => x.Contains(e.DESCRIPTION))); // "like" match
            return query.Where(e => !e.DESCRIPTION.ContainsAny(arr)); // "like" match
        }

        // Look for a result that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultInList(IEnumerable<CHART_ITEM> query, string result_list)
        {
            bool use_start_with = false;

            if (String.IsNullOrEmpty(result_list)) return query;

            if (result_list.Left(2) == STARTS_WITH)
            {
                use_start_with = true;
                result_list = result_list.Substring(2);
            }

            var arr = SplitOnCommaAndPrepareElements(result_list);
            //            return query.Where(e => arr.Any(x => x.Contains(e.RESULT))); // "like" match
            if (use_start_with)
                return query.Where(e => arr.Any(item => e.RESULT.StartsWith(item)));
            else
                return query.Where(e => e.RESULT.ContainsAny(arr)); // "like" match
                                                                    //query = query.Where(e => (meds_mr2026.Any(item => e.DESCRIPTION.ToUpper().StartsWith(item))
                                                                    //return query.Where(e => arr.Any(item => e.RESULT.ToLower().Contains(item.ToLower())));
        }

        // Look for a result that does not contain one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultNotInList(IEnumerable<CHART_ITEM> query, string result_list)
        {
            if (String.IsNullOrEmpty(result_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(result_list);
            //            return query.Where(e => !arr.Any(x => x.Contains(e.RESULT))); // "like" match
            return query.Where(e => !e.RESULT.ContainsAny(arr)); // "like" match
        }

        private string DescribeSearchDepth(SearchDepth search_depth)
        {
            string result = "";
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;

            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    //result = "in pull range"
                    result = "";                         //be quiet since this is default
                    break;
                case SearchDepth.SearchSinceArrival:
                    result = "since arrival to unit";
                    break;
                case SearchDepth.SearchSinceAdmission:
                    result = "since admission";
                    break;
                case SearchDepth.SearchPullPlus:
                    result = "in LOS range plus 4 hours after";
                    break;
                case SearchDepth.SearchSince24Hrs:
                    result = "since 24 hours ago";
                    break;
                case SearchDepth.SearchSince16Hrs:
                    result = "since 16 hours ago";
                    break;
                case SearchDepth.SearchSince13Hrs:
                    result = "since 13 hours ago";
                    break;
                case SearchDepth.SearchSince9Hrs:
                    result = "since 9 hours ago";
                    break;
                case SearchDepth.SearchSince4Hrs:
                    result = "since 4 hours ago";
                    break;
            }

            return result;
        }

        // Describe a query that has already been run
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Make a simple query and describe the results
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            var arr = query.ToArray();
            return Describe(arr, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private string LookingFor(string so_far, string what, string op, string value)
        {
            string result;
            if (String.IsNullOrEmpty(value)) return so_far;

            if (so_far == "")
                result = "look for " + what;
            else
                result = so_far + " and " + what;

            if (value.Left(2) == EXACT_MATCH_PREFIX)
                result += "=" + value.Substring(2) + "";
            else if (value.Left(2) == CODE_LIKE_PREFIX)
                result += " contains " + "" + value.Substring(2) + "";
            else if (ValueIsAList(value))
                result += " in " + value + "";
            else
                result += op + "" + value + "";

            return result;
        }
        private string Describe(CHART_ITEM[] arr, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string result = "";
            //result = LookingFor(result, "cat", "=", cat);
            result = LookingFor(result, "code", "=", code_list);
            //result = LookingFor(result, "desc", " contains ", desc_list.Left(20));
            //result = LookingFor(result, "field", "=", field);
            //result = LookingFor(result, "result", " contains ", result_list.Left(20));

            if (arr.Count() == 0) {
                result += "; not found " + DescribeSearchDepth(search_depth);
            } else {
                var e = arr[0];
                result = "FOUND: " + result;
                // We might have searched for a pattern or word list in several fields - show what was found
                //if (e.CATEGORY != null && e.CATEGORY != "") result += " cat=" + e.CATEGORY + "";
                if (e.CODE != null && e.CODE != "") result += " code=" + e.CODE + "";
                if (e.DESCRIPTION != null && e.DESCRIPTION != "") result += " desc=" + e.DESCRIPTION + "";
                //if (e.FIELD_NAME != null && e.FIELD_NAME != "") result += " field=" + e.FIELD_NAME + "";
                if (e.RESULT != null && e.RESULT != "") result += " result=" + e.RESULT + "";
                // Are there more results?  Just say how many; we aren't going to list them.
                if (arr.Count() == 2) {
                    result += " (1 more result)";
                } else if (arr.Count() > 2) {
                    result += " (" + (arr.Count() - 1) + " more results)";
                }
            }

            return result;
        }


        //Set an indicator for this reason (low level)
        private void SetInd(int inum, string reason)
        {
            if (inum <= 0)
            {
                Program.VerboseAudit(reason); // dont set indicator, just output reason
                return;
            }
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Set Ind #" + inum + ": " + reason);          //already set - repeat for verbose only
            } else {
                _inds[inum].is_checked = true;
                Program.Audit("Set Ind #" + inum + ": " + reason);
            }
        }

        //Clear an indicator for this reason (low level)
        private void ClrInd(int inum, string reason)
        {
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Clr Ind #" + inum + ": " + reason);          //already clear - repeat verbose only
            } else {
                _inds[inum].is_checked = false;
                Program.Audit("Clr Ind #" + inum + ": " + reason);
            }
        }

        // Count how many items meet the conditions
        // All items are listed
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountItems(cat,  code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountItems(string cat,  string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)

        {
            bool first = true;
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            if (exclude_periph_iv)
                query = AndDescriptionNOTInList(query, "peripheral iv");
            int count = query.Count();
Program.VerboseAudit("CountItems:count=" + count);
            found_what = "";
if (count > 0 ) //&& trace)
            {
                foreach (var item in query)
                {
                    if (first)
                    {
                        // always return what was found
                        //            found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                        found_what = "Found desc=" + item.DESCRIPTION + ";result=" + item.RESULT + ";code=" + item.CODE + ";evdt=" + item.EVENT_DATETIME+ ";items found="+count;
                        // echo the result?
                        Program.VerboseAudit(found_what);
                        first = false;
                    }
                    
                }
            }

            return count;
        }


        // Count how many items meet the conditions
        // This has the option to list every entry or give a total
        private int CountResultInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            int count = 0;
            found_what = "";
            string s = "";

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            if (exclude_periph_iv)
                query = AndDescriptionNOTInList(query, "peripheral iv");
            var arr = SplitOnCommaAndPrepareElements(result_list);
            //Walker Schlundt
            //Set Ind #9: found '' in cat='' code='301870' field='' result='oriented x4' 

            foreach (var item in query) {
                // Figure out which of the search words were found
                for (int i = 0; i <= arr.GetUpperBound(0); i++)
                {
                    if (item.RESULT.Contains(arr[i]))
                    {
                        count++;
                        s = arr[i];
                        found_what = "Found desc:" + item.DESCRIPTION + ";result=" + item.RESULT +";code=" + item.CODE;
                        //if (trace) Program.VerboseAudit(found_what);
                        Program.VerboseAudit(found_what);
                    }
                }
                if (count_mode != CountMode.CountAll) break;        //stop counting

            }

            //if (count > 0) {
            //    //We already printed what was found; maybe add how many?
            //    if (trace && count > 0) Program.VerboseAudit("found " + count + " total");
            //} else {
            //    // Describe what was *not* found
            //    //found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
            //    //if (trace) Program.VerboseAudit(found_what);
            //}

            return count;
        }

        // Count how many items meet the conditions
        // This has the option to list every entry or give a total
        private int CountResultInListEXCEPTList(string cat, string code_list, string desc_list, string field, string result_list, string negresult_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            int count = 0;
            found_what = "";

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            query = AndResultNotInList(query, negresult_list);

            var arr = SplitOnCommaAndPrepareElements(result_list);

            foreach (var item in query)
            {
                // Figure out which of the search words were found
                var s = item.RESULT.ContainsWhich(arr);
                found_what = "found " + s + "' in cat=" + item.CATEGORY + "' code=" + item.CODE + "' field=" + item.FIELD_NAME + "' result=" + item.RESULT + "";
                if (trace) Program.VerboseAudit(found_what);
                count++;
                if (count_mode != CountMode.CountAll) break;        //stop counting
            }

            if (count > 0)
            {
                //We already printed what was found; maybe add how many?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            }
            else
            {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        // Count how many items contain ALL results in list
        private int CountResultInListAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            int count = 0;
            found_what = "";

            var arr = SplitOnCommaAndPrepareElements(result_list);

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            Program.VerboseAudit("arr ub=" + arr.GetUpperBound(0));
            for (int i = 0; i <= arr.GetUpperBound(0); i++)
            {
                if (arr[i].StartsWith(NOT_PREFIX))
                {
                    //query = query.Where(e => !e.RESULT.Contains(arr[i].Substring(2)));
                    query = AndResultNotInList(query, arr[i]);
                }
                else
                {
                    Program.VerboseAudit(i + ":" + arr[i]);
                    //query = query.Where(e => e.RESULT.Contains(arr[i]));
                    query = AndResultInList(query, arr[i]);
                }
            }
            //            Program.VerboseAudit("out of for loop");

            count = query.Count();
            //          Program.VerboseAudit("query count = " +count);

            if (count > 0)
            {
                found_what = "found item with all results in " + result_list + "' without those prefixed by !!";
                if (trace) Program.VerboseAudit(found_what);
            }
            else
            {
                // Describe what was *not* found
                //found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                //if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            if (ValueIsAList(result_list))
            {
                return CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what);
            } else {
                return CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what);
            }
        }

        // Is there an item with this result?
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContains(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            if (ValueIsAList(result_list))
            {
                return (CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            } else {
                return (CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
            }
        }

        // Result contains all in list
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            if (ValueIsAList(result_list))
            {
                return (CountResultInListAll(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            }
            else
            {
                found_what = "Not valid for non-list results.";
                return false;
            }
        }
        // Count how many items meet the conditions, with none of the results in result_list.
        // This has the option to say how many were found rather than list all.
        private int CountResultNotInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            int count = 0, rec_count = 0;
            bool found_one;

            found_what = "";
            var arr = SplitOnCommaAndPrepareElements(result_list);         // also deals with CHAR_COMMA

            //Do not filter by result here; we need all results
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            foreach (var item in query) {
                rec_count++;
                //Look for each search word in the result
                found_one = false;

                foreach (string s in arr) {
                    //                    if (String.Equals(item.RESULT, s)) {
                    if (item.RESULT.Contains(s))
                    {
                        found_what = "found " + s + ";result=" + item.RESULT + " -- exclude this";
                        Program.VerboseAudit(found_what);
                        found_one = true;
                        break;
                    }
                }

                if (!found_one) {
                    //none of the words were found - good!
                    found_what = Describe(cat, code_list, desc_list, field, "", search_depth) + " -- does not contain " + result_list + "";
                    Program.VerboseAudit(found_what);
                    count++;
                    if (count_mode != CountMode.CountAll) break;
                }
            }

            if (count > 0) {
                //We already printed what was found; maybe show the total?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else if (rec_count > 0) {
                //We already printed what we ignored
            } else {
                //Print what did not exist
                found_what = Describe(cat, code_list, desc_list, field, "", search_depth);      //not found
                Program.VerboseAudit(found_what);
            }

            return count;
        }

        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, string found_what)
        {
            // Search in the pull range with trace on
            return CountResultDoesNotContain(cat, code_list, desc_list, field, result_list, out found_what, SearchDepth.SearchDefault, true);
        }
        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, out string found_what, SearchDepth search_depth, bool trace)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            return CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0;
        }

        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            return (CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
        }

        // Set the indicator if the conditions are met
        //
        private bool SetIndIfResultContains(int inum, string cat, string desc_list, string code_list, string field, string result_list)
        {
            SearchDepth s = SearchDepth.SearchDefault;
            //if (inum == 5) s = SearchDepth.SearchSince13Hrs;
            //if (inum == 6) s = SearchDepth.SearchSince13Hrs;
            //if (inum == 7) s = SearchDepth.SearchSince13Hrs;
            //if (inum == 8) s = SearchDepth.SearchSince16Hrs;
            //if (inum == 9) s = SearchDepth.SearchSince16Hrs;
            //if (inum == 10) s = SearchDepth.SearchSince13Hrs;
            //if (inum == 11) s = SearchDepth.SearchSince9Hrs;
            //if (inum == 12) s = SearchDepth.SearchSince16Hrs;
            //if (inum == 13) s = SearchDepth.SearchSince16Hrs;
            //if (inum == 14) s = SearchDepth.SearchSince24Hrs;
            //if (inum == 19) s = SearchDepth.SearchSince9Hrs;
            //if (inum == 20) s = SearchDepth.SearchSince9Hrs;
            //if (inum == 21) s = SearchDepth.SearchSince16Hrs;
            //if (inum == 22) s = SearchDepth.SearchSince13Hrs;
            //if (inum == 23) s = SearchDepth.SearchSince9Hrs;
            //if (inum == 24) s = SearchDepth.SearchSince9Hrs;
            return SetIndIfResultContains(inum, cat, desc_list, code_list, field, result_list, s);

        }
        private bool SetIndIfResultContains(int inum, string cat, string desc_list, string code_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            bool b = false;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return true;

            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what)) {
                SetInd(inum, found_what);                          //echo here - found
                b = true;
            } else {
                //   Program.VerboseAudit(found_what);                  //and here - not found
            }
            return b;
        }

        private void SetIndIfResultBetween(int inum, string cat, string code_list, string desc_list, string field, int loval, int hival)
        {
            SetIndIfResultBetween(inum, cat, code_list, desc_list, field, loval, hival, SearchDepth.SearchDefault);
        }

        private void SetIndIfResultBetween(int inum, string cat, string code_list, string desc_list, string field, int loval, int hival, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            int count = 0;
            string found_what = "";

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");
            foreach (var item in query)
            {
                Program.VerboseAudit("ResBetween: code=" + item.CODE + " result=" + item.RESULT);
                if (item.RESULT.IsNumeric())
                {
                    Program.VerboseAudit("  result is numeric");
                    if (item.RESULT.Val() >= loval && item.RESULT.Val() <= hival)
                    {
                        count++;
                        found_what = "found code=" + item.CODE + " with result=" + item.RESULT;
                        Program.VerboseAudit(found_what);
                    }
                }
            }

            if (count > 0)
            {
                SetInd(inum, found_what);
            }
        }


        // Special contains all
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SearchDepth s = SearchDepth.SearchDefault;
            //if (inum == 8) s = SearchDepth.SearchSince24Hrs;
            SetIndIfResultContainsAll(inum, cat, code_list, desc_list, field, result_list, s);
        }
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return;

            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                          //echo here - found
            }
            else
            {
                //Program.VerboseAudit(found_what);                  //and here - not found
            }
        }
        // Set the indicator if the result does not contain any of the words in result_list
        //
        private void SetIndIfResultDoesNotContain(int inum, string cat, string desc_list, string code_list, string field, string result_list)
        {
            SetIndIfResultDoesNotContain(inum, cat, desc_list, code_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultDoesNotContain(int inum, string cat, string desc_list, string code_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return;

            //Turn trace off for ResultDoesNotContain() and echo what was set below with SetInd
            if (ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                           //echo here - not found
            } else {
                //Program.VerboseAudit(found_what);                   //and here - found
            }
        }

        // Clear the indicator if the result contains one of the words in the result_list
        //
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            //avoid more queries if the indicator is already clear
            if (!_inds[inum].is_checked) return;

            //Turn trace off for ResultContains() and echo what was set below with SetInd
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what)) {
                ClrInd(inum, found_what);                           //echo here - found
            } else {
                Program.VerboseAudit(found_what);                   //and here - not found
            }
        }


        //These slightly smaller functions are meant for places where you aren't really looking for a chart result
        private bool Exists(string desc_list, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists("", "", desc_list, "", result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool Exists(string desc_list, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists("", "", desc_list, "", result_list, search_depth, true, out found_what);
        }
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            return (CountResultContains(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
        }

        // Variations with optional parameters (can switch to optional parameters in VS 2010)
        //
        private void SetIndIfFound(int inum, string cat)
        {
            SetIndIfResultContains(inum, cat, "", "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list)
        {
            SetIndIfResultContains(inum, cat, code_list, "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private void ClrIndIfFound(int inum, string cat)
        {
            ClrIndIfResultContains(inum, cat, "", "", "", "", SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Get the max/total value from a result (usually in the middle of the text)
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetIntValue(get_mode, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            int value;
            int result = 0;
            bool found_one = false;

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);

            //Look for a number in the result

            foreach (var item in query) {
                // RESULT might be null so convert to empty string so Split won't blow up
                var arr = PFSDBUtility.DBToString(item.RESULT).Split(' ');
                foreach (string s in arr) {
                    //Look for 1st character numeric; IsNumeric("60min") will fail so look at 1st char only
                    if (s.Left(1).IsNumeric()) {
                        value = (int)s.Val();                           //Use Val; ToInteger will error on "60min"
                        switch (get_mode) {
                            case GetValueMode.GetMax:
                                result = Math.Max(value, result);       //max
                                break;
                            case GetValueMode.GetTotal:
                                result += value;                        //total
                                break;
                            case GetValueMode.GetLast:
                                result = value;                         //last
                                break;
                        }

                        // print what we are searching for (the first time)
                        if (!found_one) {
                            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
                        }
                        found_one = true;
                        // print each value found
                        Program.VerboseAudit("  found numeric value " + result);
                        //Keep going in case there are more
                    }
                }
            }

            if (!found_one) {
                //show what was not found
                Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
            }

            return result;
        }

        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetMaxValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            return GetIntValue(GetValueMode.GetMax, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetTotalValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            return GetIntValue(GetValueMode.GetTotal, cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Get a result; returns true if found with return_result
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result)
        {
            return GetResult(cat, code_list, desc_list, field, out return_result, SearchDepth.SearchDefault);
        }
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0) {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
            } else {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }

        private bool GetLatestResult(string code_list, out string return_result, SearchDepth search_depth)
        {
            return GetLatestResult("", code_list, "", "", out return_result, search_depth);
        }
        private bool GetLatestResult(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");
            query = query.OrderByDescending(e => e.EVENT_DATETIME);

            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT) + " charted at " + query.First().EVENT_DATETIME;
            }
            else
            {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }

        private bool GetResultForInspection(string cat, string code_list, string desc_list, string field, string result, out string return_result, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result);

            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
            }
            else
            {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }

        private void GetResultforOther(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "Other");
            return_result = "";
            foreach (var item in query)
            {
                if (!String.Equals(item.RESULT, "Other"))
                {
                    return_result = item.RESULT;
                    //Program.VerboseAudit(found_what);
                    break;
                }

            }
            return;
        }

        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, string res, out string return_result, out DateTime return_evdt)
        {
            return GetResultAndEVDT(cat, code_list, desc_list, field, res, out return_result, out return_evdt, SearchDepth.SearchDefault);
        }
        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, string res, out string return_result, out DateTime return_evdt, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, res);

            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
                return_evdt = PFSDBUtility.DBToDateTime(query.First().EVENT_DATETIME);
            }
            else
            {
                return_result = "";
                return_evdt = DateTime.MinValue;
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }
        private bool GetEVDT(string cat, string code_list, string desc_list, string res, string field, int comparison, DateTime compevdt, out DateTime return_evdt)
        {
            return GetEVDT(cat, code_list, desc_list, field, res, comparison, compevdt, out return_evdt, SearchDepth.SearchDefault);
        }
        private bool GetEVDT(string cat, string code_list, string desc_list, string field, string res, int comparison, DateTime compevdt, out DateTime return_evdt, SearchDepth search_depth)
        {
            bool sort_ascending = false;
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, res);
            if (compevdt != DateTime.MinValue)
            {
                if (comparison == 0) // equal
                    query = query.Where(e => e.EVENT_DATETIME == compevdt);
                else if (comparison == 1) // GTE
                    query = query.Where(e => e.EVENT_DATETIME >= compevdt);
                else if (comparison == 2) // GT
                    query = query.Where(e => e.EVENT_DATETIME > compevdt);
                else if (comparison == 22) // Least GT
                {
                    query = query.Where(e => e.EVENT_DATETIME > compevdt);
                    sort_ascending = true;
                }
                else if (comparison == 3) // LT
                    query = query.Where(e => e.EVENT_DATETIME < compevdt);
            }
            if (sort_ascending)
                query = query.OrderBy(e => e.EVENT_DATETIME);
            else
                query = query.OrderByDescending(e => e.EVENT_DATETIME);
            Program.VerboseAudit("getevdt count=" + query.Count());
            if (query.Count() > 0)
            {
                return_evdt = PFSDBUtility.DBToDateTime(query.First().EVENT_DATETIME);
            }
            else
            {
                return_evdt = DateTime.MinValue;
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, res, search_depth));
            return (return_evdt != DateTime.MinValue);
        }

        private int SetIndIfCodeBtwn(int ind, int locode, int hicode, SearchDepth search_depth)

        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = query.Where(e => e.CODE.ToLower().StartsWith("edu"));
            query = query.Where(e => e.CODE.Length >= 12);
            query = query.Where(e => e.CODE.Substring(3, 9).Val() >= locode);
            query = query.Where(e => e.CODE.Substring(3, 9).Val() <= hicode);
            int count = query.Count();
            // always return what was found
            string found_what = "There were " + count + " items found with code between " + locode + " and " + hicode;
            if (count >= 8) SetInd(ind, found_what);
            // echo the result?
            //               if (trace) Program.VerboseAudit(found_what);

            return count;
        }

        private DateTime LatestEVDT(string code_list, string desc_list, string res, SearchDepth search_depth)
        {
            DateTime latestdt = DateTime.MinValue;

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, "", code_list, desc_list, "", res);
            query = query.OrderByDescending(e => e.EVENT_DATETIME);
            int ct = query.Count();

            if (ct > 0)
            {
                latestdt = PFSDBUtility.DBToDateTime(query.First().EVENT_DATETIME);
            }
            else
            {
                latestdt = DateTime.MinValue;
            }
            return latestdt;
        }

        //======================================================================================
        // Mapping starts here
        //======================================================================================

        private void Check_1_2_3_4()
        {
            Program.VerboseAudit("Default ADL Search Scope = " + _pat.pull_finish.AddHours(-24) + " to " + _pat.pull_finish);
            

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("1. ADL Self");
            Program.VerboseAudit("2. ADL Assist");
            Program.VerboseAudit("3. ADL Extended");
            Program.VerboseAudit("4. ADL Complete");
            Program.VerboseAudit("---------------");

            string desc_found = "";
            if (OrderInProgress("NSG_IABP Settings", out desc_found))
            {
                SetInd(4, desc_found);
                g_iabp = true;
            }
            string reslist = "";

            if (_pat.age < 4.0)
                SetInd(4, "Age=" + _pat.age);

            reslist="None";
            SetIndIfResultContains(1, "", "NURS Assistance Levels", "", "", reslist); 
            reslist="Self";
            SetIndIfResultContains(1,"", "NURS Activity Assistance","","", reslist);
            reslist="Self";
            SetIndIfResultContains(1,"", "NURS Skin Hygiene Performed","","", reslist);
            reslist="self";
            SetIndIfResultContains(1,"", "NURS Diet Feeding Assistance","","", reslist);
            reslist="Performed By Patient self-care";
            SetIndIfResultContains(1,"", "NURSI TX Beck","","",reslist);
            reslist="Performed By Patient self-care";
            reslist="Eats Independently";
            SetIndIfResultContains(1,"", "NURS Food/Fluid Feeding Patterns","","", reslist);

            reslist="Patient performs independent toileting";
            SetIndIfResultContains(1,"", "NURS Toileting","","", reslist);
            reslist="Self";
            SetIndIfResultContains(1,"", "NURSI Diet Feeding Assistance","","", reslist);
            reslist="Self";
            SetIndIfResultContains(1,"", "NURSI TX Skin Characteristics","","", reslist);
            reslist="Self";
            SetIndIfResultContains(1,"", "NURSI TX Activity Assistance","","", reslist);


            reslist = "";
            //NPO Orders
            //bool npo = Exists("", EXACT_MATCH_PREFIX + "40517", "", "", reslist);
            //npo |= Exists("", EXACT_MATCH_PREFIX + "226916", "", "", reslist);
            bool npo = false;


            reslist = "Partial";
            SetIndIfResultContains(2, "", "NURS Assistance Levels", "", "", reslist);
            reslist = "Unsteady, Ataxic, Scissor, Spastic, Staggering, Wadding, Drags right foot while walking, Drags left foot while walking, Leans forward while walking, Leans backward while walking; High stepping walk (foot drop)";
            SetIndIfResultContains(2, "", "NURS Ambulation Mobility ", "", "", reslist);
            reslist = "Partial";
            SetIndIfResultContains(2, "", "NURS Ambulation Mobility ", "", "", reslist);
            reslist = "Cane, Gait Belt, Walker";
            SetIndIfResultContains(2, "", "NURS Ambulation Mobility ", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(2, "", "NURS GU Stoma Type", "", "", reslist);
            reslist = "Partial";
            SetIndIfResultContains(2, "", "NURS Activity Assistance", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(2, "", "NURS Urine Drain Type", "", "", reslist);
            reslist = "Assisted to Bathroom, Assisted to Commode";
            SetIndIfResultContains(2, "", "NURS Bowel Interventions", "", "", reslist);
            reslist = "Partial";
            SetIndIfResultContains(2, "", "NURS Skin Hygiene Performed", "", "", reslist);
            reslist = "With 1 staff, With 2 Staff, With 3 staff, With 4 or more staff";
            SetIndIfResultContains(2, "", "NURS Skin Hygiene Performed", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(2, "", "NURS_EN TF Delivery Method", "", "", reslist);
            reslist = "Partial";
            SetIndIfResultContains(2, "", "NURS Diet Feeding Assistance", "", "", reslist);
            reslist = "Performed by RN/PCT";
            SetIndIfResultContains(2, "", "NURSI TX Beck", "", "", reslist);
            reslist = "Partial";
            SetIndIfResultContains(2, "", "NURS Skin Hygiene Performed", "", "", reslist);
            reslist = "Needs Assisstance";
            SetIndIfResultContains(2, "", "NURS Food/Fluid Feeding Patterns", "", "", reslist);
            reslist = "Needs partial assistance when toileting";
            SetIndIfResultContains(2, "", "NURS Toileting", "", "", reslist);
            reslist = "Partial";
            SetIndIfResultContains(2, "", "NURSI Diet Feeding Assistance", "", "", reslist);
            reslist = "tube feeding";
            SetIndIfResultContains(2, "", "NURSI Diet Type", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(2, "", "NURSI GI Stoma Location", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(2, "", "NURSI GU Stoma Type", "", "", reslist);
            reslist = "Partial";
            SetIndIfResultContains(2, "", "NURSI TX Hygiene Care", "", "", reslist);
            reslist = "Partial";
            SetIndIfResultContains(2, "", "NURSI TX Activity Assistance", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(2, "", "NURSI Diet Interventions", "", "", reslist);
            reslist = "Ambulated, Ambulated in room, Ambulated to bathroom, Sat on edge of bed, Stood at bedside, Up ad lib, Up in cardiac chair, Up in chair, Up to bedside commode";
            SetIndIfResultContains(2, "", "NURSI TX Activity Type", "", "", reslist);

            bool act3 = false;
            bool diet3 = false;
            bool toi3 = false;
            bool hyg3 = false;
            reslist = "with 1 staff, with 2 staff,  with 3 staff, with 4 or more staff ";
            act3 |= Exists("NURS Activity Assistance", reslist);
            reslist = "Family/Caregiver , Partial";
            act3 |= Exists("NURS Activity Assistance", reslist);
            reslist = "Unsteady, Ataxic, Scissor, Spastic, Staggering, Wadding, Drags right foot while walking, Drags left foot while walking, Leans forward while walking, Leans backward while walking, High stepping walk (foot drop)";
            act3 |= Exists("NURS Ambulation Mobility", reslist);
            reslist = "Partial";
            act3 |= Exists("NURSI TX Activity Assistance", reslist);

            reslist = "Partial";
            diet3 |= Exists("NURS Diet Feeding Assistance", reslist);

            reslist = "Assisted to Bathroom, Assisted to Commode";
            toi3 |= Exists("NURS Bowel Interventions", reslist);
            reslist = "Bedpan used";
            toi3 |= Exists("NURSI Bowel Interventions", reslist);
            reslist = "bedside commode/toileting assist ";
            toi3 |= Exists("NURSI Falls/Safety Interventions (Equipment)", reslist);

            reslist = "By patient with staff supervision, ";
            hyg3 |= Exists("NURS Skin Hygiene Performed", reslist);
            reslist = "With 1 staff, With 2 staff, With 3 staff, With 4 or more staff";
            hyg3 |= Exists("NURS Skin Hygiene Performed", reslist);
            reslist = "Performed by RN/PCT";
            hyg3 |= Exists("NURSI TX Beck", reslist);
            reslist = "Partial";
            hyg3 |= Exists("NURSI TX Hygiene Care", reslist);

            int adl3count = (act3 ? 1 : 0) + (diet3 ? 1 : 0) + (toi3 ? 1 : 0) + (hyg3 ? 1 : 0);
            if (adl3count >= 3)
            {
                SetInd(3, "At least 3 ADL Extended categories:  Activity=" + act3 + ";Feeding=" + diet3 + ";Toileting=" + toi3 + ";Hygiene=" + hyg3);
            }
            else if (adl3count >= 1)
            {
                SetInd(2, "At least 1 ADL Extended categories:  Activity=" + act3 + ";Feeding=" + diet3 + ";Toileting=" + toi3 + ";Hygiene=" + hyg3);
            }

            reslist = "Transvenous";
            SetIndIfResultContains(3, "", "NURSI Pacemaker Type", "", "", reslist);

            int numdrains = 0;
            reslist = "";
            numdrains += CountItems("", "NURSI Chest Tube Type", "", "", reslist);
            reslist = "";
            numdrains += CountItems("", "NURSI TX I Drain Type of Drain/Suction", "", "", reslist);
            if (numdrains >= 3)
                SetInd(3, "Drain count is >=3: " + numdrains);

            reslist = "With 3 staff, With 4 or more staff";
            SetIndIfResultContains(4, "", "NURSI TX Position/Device", "", "", reslist);
            SetIndIfResultContains(4, "", "NSG_IABP Settings", "", "", "");

            reslist = "Neuromuscular Blockade Train of Four";
            SetIndIfResultContains(4, "", "NURSI NMB Train of Four", "", "", reslist);
            reslist = "Continuous drain, Intermittent drain";
            SetIndIfResultContains(4, "", "NURSI Lumbar Status", "", "", reslist);
            reslist = "Continuous drain, Intermittent drain";
            SetIndIfResultContains(4, "", "NURSI Ventric Status", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(4, "", "NURSI ICP", "", "", reslist);
            reslist = "+4,+3,+2,+1,-1,-2,-3,-4,-5";
            SetIndIfResultContains(4, "", "NURSI RASS", "", "", reslist);
            reslist = "Aerosol blow-by, Aerosol face mask, Aerosol face tent, Aerosol trach mask, Aerosol T-piece, BiFlow cannula, CPAP/BiPAP (Ad Non-invasive Ventilation parameter), Non-rebreather mask, Ventilator, Vapo Therm nasal cannula, Vapo Therm trach mask";
            SetIndIfResultContains(4, "", "NURSI O2 Delivery Mode", "", "", reslist);

            int count = 0;
            bool act4 = false;
            reslist = "Complete";
            if (Exists("NURS Activity Type", reslist)) act4=true;
            reslist = "Full";
            if (Exists("NURS Assistance Levels", reslist)) act4 = true;
            reslist = "Paraplegic,Quadriplegic";
            if (Exists("NURSI Motor Neuro-motor Mobility", reslist)) act4 = true;
            reslist = "complete";
            if (Exists("NURSI TX Activity Assistance", reslist)) act4 = true;
            if (act4)
            {
                count++;
                Program.VerboseAudit("Activity=complete item found.");
            }

            reslist = "";
            SetIndIfResultContains(4, "", "NURS Disoriented To", "", "", reslist);
            reslist = "Lethargic, Obtunded, Stuporous, Sedated, Comatose, Drowsy";
            SetIndIfResultContains(4, "", "NURSI Level of Consciousness", "", "", reslist);

            bool toil4 = false;
            reslist = "Incontinent of stool";
            if (Exists("NURS Bowel Observations", reslist)) toil4 = true;
            reslist = "Incontinent";
            if (Exists("NURS Urinary Appearance", reslist)) toil4 = true;
            reslist = "Bedpan used, Diaper/pad changed";
            if (Exists("NURS Bowel Interventions", reslist)) toil4 = true;
            reslist = "Total dependence with toileting";
            if (Exists("NURS Toileting", reslist)) toil4 = true;
            reslist = "";
            if (Exists("NURSI Bowel Observations", reslist)) toil4 = true;
            reslist = "Incontinent  ";
            if (Exists("NURSI Urine Observations", reslist)) toil4 = true;
            reslist = "";
            if (Exists("NURSI Fecal Containment Type", reslist)) toil4 = true;
            reslist = "Incontinence ";
            if (Exists("NURS FR Elimination", reslist)) toil4 = true;
            if (toil4)
            {
                count++;
                Program.VerboseAudit("Toileting=complete item found.");
            }

            bool feed4 = false;
            reslist = "Total";
            if (Exists("NURS Diet Feeding Assistance", reslist)) feed4 = true;
            reslist = "Bottle, Family,Total";
            if (Exists("NURSI Diet Feeding Assistance", reslist)) feed4 = true;
            reslist = "Must Be Fed";
            if (Exists("NURS Food/Fluid Feeding Patterns", reslist)) feed4 = true;
            if (feed4)
            {
                count++;
                Program.VerboseAudit("Feeding=complete item found.");
            }

            bool bath4 = false;
            reslist = "Bedbath, Tub bath-patient room, tub bath-1nw, tub bath-3nw, tube bath-5nw";
            if (Exists("NURS Skin Hygiene Performed", reslist)) bath4 = true;
            reslist = "Complete";
            if (Exists("NURS Skin Hygiene Performed", reslist)) bath4 = true;
            reslist = "complete";
            if (Exists("NURSI TX Skin Characteristics", reslist)) bath4 = true;
            if (bath4)
            {
                count++;
                Program.VerboseAudit("Bathing=complete item found.");
            }

            reslist = "Crib";
            SetIndIfResultContains(4, "", "NURSI TX ICU Bed Type", "", "", reslist);
            reslist = "Child attended at all times, Crib rails up or Hand contact when crib rails are down";
            SetIndIfResultContains(4, "", "NURS_FPH Pediatric Standard Precautions", "", "", reslist);

            int adl4count = (act4 ? 1 : 0) + (toil4 ? 1 : 0) + (feed4 ? 1 : 0) + (bath4 ? 1 : 0);
            if (adl4count >= 2)
            {
                SetInd(4, "At least 2 Complete items found among: activity, toileting, feeding, bathing. count=" + adl4count);
            }
            else if (adl4count >= 1)
            {
                SetInd(2, "At least 1 Complete items found among: activity, toileting, feeding, bathing.count=" + adl4count);
            }

            double v;
            reslist = "Total Score 5 and below";
            //if (GetLatestResult("NURS Glasgow Eye Opening", out reslist, SearchDepth.SearchDefault))
            //{
            //    v = reslist.Substring(0, 1).Val();
            //    Program.VerboseAudit("Glasgow val=" + v);
            //    if (v <= 5)
            //        SetInd(4, "NURS Glasgow Eye Opening=" + reslist + " val=" + v);
            //}
            //if (GetLatestResult("NURS Glasgow Best Verbal Response", out reslist, SearchDepth.SearchDefault))
            //{
            //    v = reslist.Substring(0, 1).Val();
            //    Program.VerboseAudit("Glasgow val=" + v);
            //    if (v <= 5)
            //        SetInd(4, "NURS Glasgow Best Verbal Response=" + reslist + " val=" + v);
            //}
            //if (GetLatestResult("NURS Glasgow Best Motor Response", out reslist, SearchDepth.SearchDefault))
            //{
            //    v = reslist.Substring(0, 1).Val();
            //    Program.VerboseAudit("Glasgow val=" + v);
            //    if (v <= 5)
            //        SetInd(4, "NURS Glasgow Best Motor Response=" + reslist + " val=" + v);
            //}
            if (GetLatestResult("NURS Glasgow Coma Scale Total Score", out reslist, SearchDepth.SearchDefault))
            {
                v = reslist.Substring(0, 1).Val();
                Program.VerboseAudit("Glasgow val=" + v);
                if (v <= 5)
                    SetInd(4, "NURS Glasgow Coma Scale Total Score=" + reslist + " val=" + v);
            }

            reslist = "With 4 or more staff";
            SetIndIfResultContains(4, "", "NURSI TX Position/Device", "", "", reslist);
            reslist = "Bedrest";
            SetIndIfResultContains(4, "", "NURSI TX Activity Type", "", "", reslist);

            if (!_inds[1].is_checked && !_inds[2].is_checked && !_inds[3].is_checked && !_inds[4].is_checked)
            {
                SetInd(1, "No documentation found.  Defaulting to ADL Self");
            }

        }

        private void UpdatePtChartArrays(string orderid, string ordstatus, string newres)
        {
            foreach (var item in _chart_items_since24hrs)
            {
                if (item.ORDER_ID == orderid)
                {
                    if (ordstatus != "")
                        item.ORDER_STATUS = ordstatus;
                    if (newres != "")
                        item.RESULT = newres;
                }
            }
        }

        private void UpdatePtChartArraysCode(string code, string ordstatus,DateTime dcdt)
        {
            foreach (var item in _chart_items_since_admission)
            {
                if (item.CODE == code && item.EVENT_DATETIME <= dcdt)
                {
                    item.ORDER_STATUS = ordstatus;
                }
            }
        }



        private void Check_5()
        {
            string reslist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("5. ADL Rehab");
            Program.VerboseAudit("---------------");

            exclude_periop_data = false;


            reslist = "Unable to cough";
            SetIndIfResultContains(5, "", "NURS Cough Freq/Description", "", "", reslist);
            reslist = "Gag reflex absent ";
            SetIndIfResultContains(5, "", "NURS Upper GI Observations      ", "", "", reslist);
            reslist = "Dizziness, Fatigue, Pain, Shortness of Breath, Weakness";
            SetIndIfResultContains(5, "", "NURS Ambulation Tolerance", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(5, "", "NURS Ambulation Mobility ", "", "", reslist);
            reslist = "Partial, Unable";
            SetIndIfResultContains(5, "", "NURS Ambulation Mobility ", "", "", reslist);
            reslist = "Unsteady, Ataxic, Scissor, Spastic, Staggering, Wadding, Drags right foot while walking, Drags left foot while walking, Leans forward while walking, Leans backward while walking, High stepping walk (foot drop)";
            SetIndIfResultContains(5, "", "NURS Ambulation Mobility ", "", "", reslist);
            reslist = "RN assist with swallowing precautions";
            SetIndIfResultContains(5, "", "NURS Diet Interventions", "", "", reslist);
            reslist = "Bed frame with trapeze";
            SetIndIfResultContains(5, "", "NURS Position/Device/Caregiver", "", "", reslist);
            reslist = "Boots,air,foam,multi-podus,Foot cradle";
            SetIndIfResultContains(5, "", "NURS Skin Pressure Ulcer Prevent", "", "", reslist);
            reslist = "Active range of motion";
            SetIndIfResultDoesNotContain(5, "", "NURS ROM/Exercise Performed", "", "", reslist);
            reslist = "Subject seen today for services indicated below";
            SetIndIfResultContains(5, "", "RMD_Brief Service Note", "", "", reslist);
            reslist = "Continue as per pulmonary rehabilitation plan of care";
            SetIndIfResultContains(5, "", "RMDPD Plan", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(5, "", "NURS Respiratory Tracheostomy Tube", "", "", reslist);
            reslist = "Occupational Therapy, Physiatry, Physical therapy";
            SetIndIfResultContains(5, "", "REHAB Encounter Service", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(5, "", "NURS Respiratory Quality of Voice", "", "", reslist);
            reslist = "Yes";
            SetIndIfResultContains(5, "", "NURS FAST Screen Swallowing", "", "", reslist);
            reslist = "RN assist with swallowing precautions";
            SetIndIfResultContains(5, "", "NURSI Diet Interventions", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(5, "", "NURSI Musculoskeletal Observations", "", "", reslist);
            reslist = "Gag Reflex Absent, Difficulty Swallowing, Cough Absent";
            SetIndIfResultContains(5, "", "NURSI CN Glossopharyngeal/Vagus", "", "", reslist);
            reslist = "Boots, multi-podus";
            SetIndIfResultContains(5, "", "NURSI TX Pressure Ulcer Prevention", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(5, "", "NURSI Tracheostomy Tube Type", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(5, "", "NURSI TX Exercise Performed", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(5, "", "NURSI Trach Speaking Valve", "", "", reslist);
        }


        private void Check_6_7()
        {
            string reslist;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("6. ADL 2-3 Caregivers");
            Program.VerboseAudit("7. ADL 4 or more Caregivers");
            Program.VerboseAudit("---------------");

            reslist = "With 2 staff,With 3 staff";
            SetIndIfResultContains(6, "", "NURS Position/Device/Caregiver", "", "", reslist);
            SetIndIfResultContains(6, "", "NURS Skin Hygiene Performed", "", "", reslist);
            SetIndIfResultContains(6, "", "NURS Activity Assistance", "", "", reslist);
            SetIndIfResultContains(6, "", "NURSI TX Position/Device", "", "", reslist);
            SetIndIfResultContains(6, "", "NURSI TX Hygiene Care", "", "", reslist);
            SetIndIfResultContains(6, "", "NURSI TX Activity Assistance", "", "", reslist);

            reslist = "With 4 or more staff";
            SetIndIfResultContains(7, "", "NURS Position/Device/Caregiver", "", "", reslist);
            SetIndIfResultContains(7, "", "NURS Skin Hygiene Performed", "", "", reslist);
            SetIndIfResultContains(7, "", "NURS Activity Assistance", "", "", reslist);
            SetIndIfResultContains(7, "", "NURSI TX Position/Device", "", "", reslist);
            SetIndIfResultContains(7, "", "NURSI TX Hygiene Care", "", "", reslist);
            SetIndIfResultContains(7, "", "NURSI TX Assist/Caregivers", "", "", reslist);

        }

        private void Check_8()
        {
            string reslist;
            string found_what;
            string return_result;
            DateTime return_evdt;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("8. Communication");
            Program.VerboseAudit("---------------");


            reslist = "";
            SetIndIfResultContains(8, "", "NURS Neuro Speech Deficits/Abnor", "", "", reslist);
            reslist = "Communication";
            SetIndIfResultContains(8, "", "NURS Physical Safety Risks Due To", "", "", reslist);
            reslist = "Uncompensated visual/auditory impairment";
            SetIndIfResultContains(8, "", "NURS FR Mobility", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(8, "", "NURS Respiratory Tracheostomy Tube", "", "", reslist);
            reslist = "Abnormal rate of speech, Expressive aphasia, Receptive aphasia, Incomprehensible, Slurred";
            SetIndIfResultContains(8, "", "NURS Speech", "", "", reslist);
            reslist = "Inappropriate for age, Aphasia, Artificial airway, Disease process, Hard of hearing/Deaf, Language barriers, Oral deformity, Speech impairment";
            SetIndIfResultContains(8, "", "NURS Barrier Communication Assesment", "", "", reslist);
            reslist = "Spanish, Greek, Vietnamese, Korean, Creole, Arabic, Mandarin, Italian, German, Hebrew, French, American Sign Language, Other";
            SetIndIfResultContains(8, "", "NURS Primary Language", "", "", reslist);
            reslist = "Non-English speaking, Illiteracy, Vision Impairment, Hearing Impairment, Dyslexia, Aphasia";
            SetIndIfResultContains(8, "", "NURS Teach/Learning Barriers Pat", "", "", reslist);
            reslist = "yes";
            SetIndIfResultContains(8, "", "NURS FAT Mobility 2", "", "", reslist);
            reslist = "Yes";
            SetIndIfResultContains(8, "", "NURS FAST Screen Speech", "", "", reslist);
            reslist = "Translation phone";
            SetIndIfResultContains(8, "", "NURSI Falls/Safety Interventions", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(8, "", "NURSI Tracheostomy Tube Type", "", "", reslist);
            reslist = "Abnormal rate of speech, Expressive aphasia, Receptive Aphasia, Incomprehensible, Slurred";
            SetIndIfResultContains(8, "", "NURSI Speech", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(8, "", "CC_Resp Airway Type", "", "", reslist);
            reslist = "Artificial Airway, Aphasia, Language Barriers, Speech Impairment, Oral Deformity, Mute, Hard of Hearing/Deaf, Disease process";
            SetIndIfResultContains(8, "", "NURSI Barrier Assessment", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(8, "", "NURSI NIV O2 Mode", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(8, "", "NURS_FPH Visual/Hearing Impairment", "", "", reslist);
            reslist = "face mask";
            SetIndIfResultContains(8, "", "NURSI NIV Mask Type", "", "", reslist);

        }



        private void Check_9()
        {
            string reslist;
            bool s2, s3, s4;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("9. Cognitive Support");
            Program.VerboseAudit("---------------");

            reslist = "";
            SetIndIfResultContains(9, "", "NURS Disoriented To", "", "", reslist);
            reslist = "Cognitive Ability";
            SetIndIfResultContains(9, "", "NURS Physical Safety Risks Due To", "", "", reslist);
            reslist = "Lethargic, Obtunded, Stuporous, Semi-comatose, Comatose, Drowsy, Sedated";
            SetIndIfResultContains(9, "", "NURS Level of Consciousness ", "", "", reslist);
            reslist = "4-Confused, 3-Inappropriate words, 2-Incomprehensible sounds";
            SetIndIfResultContains(9, "", "NURS Glasgow Best Verbal Response", "", "", reslist);
            reslist = "Impaired, Absent";
            SetIndIfResultContains(9, "", "NURS Neuro Memory - Short Term", "", "", reslist);
            SetIndIfResultContains(9, "", "NURS Neuro Memory - Long Term", "", "", reslist);
            SetIndIfResultContains(9, "", "NURS Neuro Memory - Immediate Recall", "", "", reslist);
            SetIndIfResultContains(9, "", "NURS Cognitive  Similarities", "", "", reslist);
            SetIndIfResultContains(9, "", "NURS Cognitive Judgment", "", "", reslist);
            SetIndIfResultContains(9, "", "NURS Cognitive Insight", "", "", reslist);
            SetIndIfResultContains(9, "", "NURS Cognitive General Knowledge", "", "", reslist);
            SetIndIfResultContains(9, "", "NURS Cognitive Decision Making", "", "", reslist);
            SetIndIfResultContains(9, "", "NURS Cognitive Calculations", "", "", reslist);
            SetIndIfResultContains(9, "", "NURS Cognitive Atten./Concentrat", "", "", reslist);
            SetIndIfResultContains(9, "", "NURS Cognitive Abstract Reasonin", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(9, "", "NURS_FPH Cognitive Impairment/Di", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(9, "", "NURS Psych Perceptual Disturbances", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(9, "", "NURS Psych Perceptual Disturbances", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(9, "", "NURS Psych thought Content Disorder", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(9, "", "NURS Psych thought Content Disorder", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(9, "", "NURS_FPH Cognitive Impairment/Disorientation", "", "", reslist);
            reslist = "Confused, Hallucinating, Immediate recall loss, Short term memory loss, Long term memory loss, Unable to assess";
            SetIndIfResultContains(9, "", "NURSI Mentation", "", "", reslist);

        }


        private void Check_10_11()
        {
            string reslist;
            bool is_peds = false;
            //int ct;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("10. Behavior/Emotional Management");
            Program.VerboseAudit("11. Behavior/Emotional Mgmt - q 1 Hour");
            Program.VerboseAudit("---------------");

            bool q248 = false;
            bool q1 = false;

            reslist = "8 hour,4 hour,2 hour";
            q248 = (Exists("", "NURS Psych Support Frequency2", "", "", reslist)
                ||
            Exists("", "NURSI Psych Support Frequency", "", "", reslist));

            reslist = "1 hour";
            q1 = (Exists("", "NURS Psych Support Frequency2", "", "", reslist)
                ||
            Exists("", "NURSI Psych Support Frequency", "", "", reslist));

            int ind=1;
            if (q248) ind = 10;
            if (q1) ind = 11;

            reslist = "Appropriate to situation, Calm, Cooperative, Congruent with thought process";
            SetIndIfResultDoesNotContain(ind, "", "NURS Behavior", "", "", reslist);
            reslist = "None";
            SetIndIfResultDoesNotContain(ind, "", "NURS Psych Overt Behavior", "", "", reslist);
            reslist = "Appropriate for age, Calm/relaxed, Happy";
            SetIndIfResultDoesNotContain(ind, "", "NURS Psych Patient Appears", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(ind, "", "NURS Psych Action Taken", "", "", reslist);
            //reslist = "Every 8 hours, Every 4 hours, Every 2 hours";
            //SetIndIfResultContains(10, "", "NURS Psych Support Frequency2", "", "", reslist);
            reslist = "4,3,2,1";
            SetIndIfResultContains(10, "", "NURSI RASS", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(ind, "", "NURSI Psych Action Taken", "", "", reslist);
            //reslist = "Every 8 hours, Every 4 hours, Every 2 hours";
            //SetIndIfResultContains(10, "", "NURSI Psych Support Frequency", "", "", reslist);
            reslist = "Angry,Anxious,Clinging to parent,Crying/tearful,Combative,Depressed,Difficult to engage, Restless, Talkative, Uncooperative, Withdrawn";
            SetIndIfResultContains(ind, "", "NURSI Patient Appears", "", "", reslist);
            reslist = "Inappropriate to situation, Agitated, Angry, Anxious, Combative, Euphoric, Frantic sucking of hands, Irritable, Flat affect, Labile affect, Paranoid, Picking at times, Restless, Sad, Uncooperative, Withdrawn";
            SetIndIfResultContains(10, "", "NURSI Behavior", "", "", reslist);

            //if (_inds[10].is_checked)
            //{
            //    reslist = "Every 1 hour";
            //    SetIndIfResultContains(11, "", "NURS Psych Support Frequency2", "", "", reslist);
            //    SetIndIfResultContains(11, "", "NURSI Psych Support Frequency", "", "", reslist);
            //}
            //reslist = "Any except for Appropriate to situation, Calm, Cooperative, Congruent with thought process ";
            //SetIndIfResultContains(11, "", "NURS Behavior", "", "", reslist);
            //reslist = "Any";
            //SetIndIfResultContains(11, "", "NURS Psych Overt Behavior & Motor Acts", "", "", reslist);
            //reslist = "ANY";
            //SetIndIfResultContains(11, "", "NURS Psych Observation Status", "", "", reslist);
            //reslist = "Any";
            //SetIndIfResultContains(11, "", "NURS Psych Action Taken", "", "", reslist);
            //reslist = "Any except for Appropriate for age (PEDS), Calm/relaxed, and Happy";
            //SetIndIfResultContains(11, "", "NURS Psych Patient Appears", "", "", reslist);
            //reslist = "Angry, Anxious, Clinging to parent, Crying/tearful, Combative,Cooperative, Depressed, Difficult to engage, Restless, Talkative, Uncooperative and Withdrawn";
            //SetIndIfResultContains(11, "", "NURSI Patient Appears", "", "", reslist);
            //reslist = "Any";
            //SetIndIfResultContains(11, "", "NURSI Psych Action Taken", "", "", reslist);

        }

        private int CountLTZero()
        {
            int ct = 0;

            return ct;
        }
        private int CountGTZero(out int ct12, out int ct34)
        {
            int ct = 0;
            ct12 = 0;
            ct34 = 0;


            ct = ct12 + ct34;
            return ct;
        }

        private bool IsQ4Freq()
        {
            string codelist;
            bool ret = false;
            List<gBucket> buckets;

            SetBucketSize(60);
            buckets = new List<gBucket>();
            codelist = "304239398";
            AddBuckets(buckets, "", codelist, "", "", "Yes");
            ret = AnalyzeBuckets(buckets, 15, 60, "indicator 11 attestation", false);

            return (ret);

        }
        private bool IsQ1Freq()
        {
            bool ret = false;


            return ret;
        }

        // (this isn't really a Q1h count -- it is just a count) Make this a q1hr count
        //private int ReturnQ1HrCount(string code_list, string result_list)
        //{
        //    var buckets = new List<gBucket>();
        //    SetBucketSize(60);
        //    AddBuckets(buckets, "", code_list, "", "", result_list);
        //    return CountBuckets(buckets);
        //}

        //private bool IsQ1Hour(int count)
        //{
        //    return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q1H);
        //}
        //private bool IsQ2Hour(int count)
        //{
        //    return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q2H);
        //}

        // Use this if counting safety buckets
        //private void CheckSafety(int count, string desc)
        //{
        //    if (_inds[13].is_checked) return;             //skip if highest already checked
        //    if (count == 0) return;
        //    switch (FreqForCount(_pat.los_hours, count))
        //    {
        //    case Frequencies.Q30M:
        //        SetInd(13, desc + " q30min");
        //        break;
        //    case Frequencies.Q1H:
        //    case Frequencies.Q2H:
        //        SetInd(12, desc + " q2h");
        //        break;
        //    default:
        //        Program.VerboseAudit(desc + ": " + count + " in " + Math.Round(_pat.los_hours) + " hours is not enough");
        //        break;
        //    }
        //}

        private void FindLatest(string code, string reslist, out string res, out DateTime evdt)
        {
            res = "???";
            evdt = DateTime.MinValue;

            var query = StartNewQuery(SearchDepth.SearchSince16Hrs);
            query = AndItemFilter(query, "", code, "", "", reslist);
            CHART_ITEM ch = query.OrderByDescending(e => e.EVENT_DATETIME).FirstOrDefault();
            if (ch == null) return;
            res = ch.RESULT;
            evdt = ch.EVENT_DATETIME;
            Program.VerboseAudit("Latest result and time:" + ch.RESULT + ch.EVENT_DATETIME.ToString());

        }

        private void Check_12_13()
        {
            string reslist;
            bool is_peds = false;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("12. Safety Management - q 2 Hours");
            Program.VerboseAudit("13. Safety Management - q 30 Minutes");
            Program.VerboseAudit("---------------");

            //NURS Physical Safety Observation Status (which gives freq) needs to be used for q30
            bool q1 = false;
            bool q15 = false;
            reslist = "Hourly";
            q1 = Exists("", "NURS Psych Observation Status", "", "", reslist);
            reslist = "30 min,15 min";
            q15 = Exists("", "NURS Psych Observation Status", "", "", reslist);
            int ind = 1;
            if (q1) ind = 12;
            if (q15) ind = 13;

            reslist = "Staff observation of patient behavior";
            SetIndIfResultContains(12, "", "NURS Psych Reliability", "", "", reslist);
            reslist = "Impaired, Does not undestand the likely outcome of personal behavior, Unable to predict appropriate outcome to imaginary situation";
            SetIndIfResultContains(12, "", "NURS Psych Judgement", "", "", reslist);
            reslist = "Yes";
            SetIndIfResultContains(12, "", "NURS Psych Elopement Risk", "", "", reslist);
            reslist = "Rounded on patient every 60 minutes";
            SetIndIfResultContains(12, "", "NURS_FPH Standard Precautions", "", "", reslist);

            //for Exists, use DESC field due to 33 char code
            reslist = "";
            if (Exists( "", "", "NURS Physical Safety Risks Due To", "", reslist))
                SetIndIfResultContains(12, "", "NURS Physical Safety Observational", "", "", reslist);

            reslist = "Side rails up,observed every 30 minutes";
            SetIndIfResultDoesNotContain(12, "", "NURS_FPH Seizures", "", "", reslist);

            reslist = "";
            if (Exists("", "NURS_FPH Communication of Falls", "", "", reslist))
            {
                SetIndIfResultContains(12, "", "NURS FR Risk Level", "", "", "High Risk");
                reslist = "Confused, Hallucinating, Immediate recall loss, Short term memory loss, Long term memory loss";
                SetIndIfResultContains(12, "", "NURSI Mentation", "", "", reslist);
                reslist = "Agitated, angry, anxious, combative, paranoid, picking at items, uncooperative, restless";
                SetIndIfResultContains(12, "", "NURSI Behavior", "", "", reslist);
                reslist = "Medical devices that tether patient";
                SetIndIfResultContains(12, "", "NURS FR Patient Care Equipment", "", "", reslist);
                reslist = "Yes";
                SetIndIfResultContains(12, "", "NURS FR Bones", "", "", reslist);
                reslist = "Yes";
                SetIndIfResultContains(12, "", "NURS FR Complications Risk", "", "", reslist);
                reslist = "Yes";
                SetIndIfResultContains(12, "", "NURS FR Bleeding Risk", "", "", reslist);
            }

            reslist = "Safety Evaluated and Maintained";
            SetIndIfResultContains(12, "", "NURSI Seizure Interventions", "", "", reslist);
            reslist = "bed alarms on, siderails up x 4, room near nurses station ";
            SetIndIfResultContains(12, "", "NURS_FPH Cognitive Impairment/Disorientation", "", "", reslist);

            reslist = "Patient kept under continuous eye contact";
            SetIndIfResultContains(13, "", "NURS Physical Safety Observational", "", "", reslist);
            if (q15)
            {
                reslist = "30 min,15 min";
                SetIndIfResultContains(13, "", "NURS Physical Safety Observation Status", "", "", reslist);
                reslist = "";
                SetIndIfResultContains(13, "", "NURS Physical Safety Risks Due To", "", "", reslist);
            }
            //reslist = "Every 15 minutes, Every 30 minutes, Every 5 minutes, Eye contact, arm's length, Eye contact, constant";
            //SetIndIfResultContains(13, "", "NURS Psych Observation Status", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(13, "", "NURS EEG Electrode Types", "", "", reslist);
            reslist = "side rails up,observed every 60 minutes";
            SetIndIfResultDoesNotContain(13, "", "NURS_FPH Seizures", "", "", reslist);
            reslist = "";
            if (Exists("", "NURS Restraint Types", "", "", reslist))
            {
                reslist = "Done";
                SetIndIfResultContains(13, "", "NURS Restraint Eye Contact", "", "", reslist);
                reslist = "Done";
                SetIndIfResultContains(13, "", "NURS Restraint Q15 Minute", "", "", reslist);
            }
            reslist = "+4,+3,+2,+1,-1,-2,-3,-4,-5";
            SetIndIfResultContains(13, "", "NURSI RASS", "", "", reslist);
            reslist = "Sitter Present";
            SetIndIfResultContains(13, "", "NURS_FPH Cognitive Impairment/Disorientation", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(13, "", "NURS_FPH Pediatric Standard Prec", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(13, "", "NURSI EEG Electrode Types", "", "", reslist);
            reslist = "SCUF, CVVH, CVVHDF";
            SetIndIfResultContains(13, "", "NURSI CRRT Mode", "", "", reslist);
            reslist = "endotracheal";
            SetIndIfResultContains(13, "", "CC_Resp Airway Type", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(13, "", "NURSI ICP", "", "", reslist);
            reslist = "Continuous drain, Intermittent drain";
            SetIndIfResultContains(13, "", "NURSI Ventric Status", "", "", reslist);
            if (g_iabp)
                SetInd(13, "IABP order in effect.");
        }

        private bool IsValidDate(string yyyymmdd)
        { //expecting yyyymmdd
            Program.VerboseAudit("IsValidDate inpt: " + yyyymmdd);
            bool ret = true;
            if (yyyymmdd.Length < 8)
                return false;
            if (yyyymmdd.Length > 8)
                yyyymmdd = yyyymmdd.Substring(0, 8);
            ret = (yyyymmdd.Substring(0, 4).Val() > 2000
                && yyyymmdd.Substring(4, 2).Val() >= 1
                && yyyymmdd.Substring(4, 2).Val() <= 12
                && yyyymmdd.Substring(6, 2).Val() >= 1
                && yyyymmdd.Substring(6, 2).Val() <= 31);
            return ret;
        }

        private void Check_14()
        {
            string desc_found = "", code;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("14. Isolation");
            Program.VerboseAudit("---------------");

            string[] orderid_str = { "MACPO70012","MACPO70013", "MACPO70014","MACPO70015",
                "MACPO70016","MACPO70017","MACPO70018"};

            foreach (string s in orderid_str)
            {
                if (OrderInProgress(s, out desc_found))
                    SetInd(14, "Order in effect: " + desc_found);
            }

        }


        private bool GetNWOrder(string CAordcode, DateTime CAts, DateTime CAevdt)
        { //Get NW order that wasn't saved because there was a previous CA order at same time.
            string sql;
            bool ret = false;
            //string room;
            //string dt;
            //bool readyforEDclass = true;
            //int car;
            //DateTime minlocdt = DateTime.MaxValue;

            string CAevdtISO = PFSUtility.DateTimeToISODateTime(CAevdt);

            sql = "select timestamp,";
            sql += " case when CHARINDEX('ORC|NW', source_text) > 0 then";
            sql += " substring(source_text, CHARINDEX('ORC|', source_text), 32) else null end as ORC,";

            sql += " case when CHARINDEX('OBR|', source_text) > 0 and CHARINDEX('|" + CAevdtISO + "', source_text) > 0 then";
            sql += " substring(source_text, CHARINDEX('OBR|', source_text), 200) else null end as OBR";

            sql += " from EVENT_LOG where TIMESTAMP between dateadd(minute,-10," + PFSDBUtility.SQLDateTime(CAts) + ") and dateadd(minute, 10," + PFSDBUtility.SQLDateTime(CAts) + ")";
            sql += " and (description like 'O01%') and description like '%" + _pat.acct + "%'";
            sql += " and event_source = 1 and event_type = 1 and event_category = 4";//NW item was not rejected
            Program.VerboseAudit(sql);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd2 = new SqlCommand(sql, db2);
            SqlDataReader dr2 = cmd2.ExecuteReader(System.Data.CommandBehavior.CloseConnection);

            string orcstr, obrstr, timestr;
            DateTime evdt=DateTime.MinValue;
            int seq = 0;
            while (dr2.Read())
            {
                orcstr = PFSDBUtility.DBToString(dr2["ORC"]);
                obrstr = PFSDBUtility.DBToString(dr2["OBR"]);
                if (orcstr.Trim() != "" && orcstr != null)
                {
                    //ORC|NW|669769268^EPC||3290947318
                    //OBR|1|669769268^EPC||40517^DIET^APEAP^^DIET||202004180752|202004180800

                    Program.VerboseAudit("Searching for unsaved order code: " + CAordcode + " at time " + CAevdt);
                    var arrorc = Program.SplitOnPipeAndPrepareElements(orcstr);
                    var arrobr = Program.SplitOnPipeAndPrepareElements(obrstr);
                    string ordid = "";
                    string ordctrl = "";
                    string ordcode = "";
                    string orddesc = "";

                    if (arrorc.GetUpperBound(0) >= 3 && arrobr.GetUpperBound(0) >= 4)
                    {
                        Program.VerboseAudit("Unsaved NW order ORC: " + orcstr);
                        Program.VerboseAudit("Unsaved NW order OBR: " + obrstr);

                        ordctrl = arrorc[1];
                        Program.VerboseAudit("ordctrl: " + ordctrl);
                        ordid = arrorc[2];
                        //VerboseAudit("ordid: " + ordid);
                        int ordidpos = ordid.IndexOf("^");
                        if (ordidpos > 0) ordid = ordid.Substring(0, ordidpos);
                        //VerboseAudit("ordidfinal: " + ordid);

                        ordcode = arrobr[4];
                        //VerboseAudit("ordcode: " + ordcode);
                        int ordcodepos = ordcode.IndexOf("^");
                        if (ordcodepos > 0)
                        {
                            orddesc = ordcode.Substring(ordcodepos + 1);
                            ordcode = ordcode.Substring(0, ordcodepos);
                        }
                        //VerboseAudit("ordcodefinal: " + ordcode);

                        if (ordcode.Trim() == CAordcode.Trim())
                        {
                            ret = true;
                            evdt = CAevdt.AddMinutes(-1);
                            Program.VerboseAudit("Adding: orderid=" + ordid + " ctrl=" + ordctrl + " code=" + ordcode + " evdt=" + evdt + " desc=" + orddesc);
                            if (arrorc.GetUpperBound(0) >= 2)
                            {
                                using (var db = PFSDBUtility.NewSqlConnection())
                                {
                                    seq++;
                                    //evdt = PFSUtility.ISOToDateTime(timestr);
                                    Program.VerboseAudit("Evdt=" + evdt.ToString());
                                    string q = "INSERT INTO chart_item (encounter_id,event_datetime,code,description,timestamp,sequence,unit_id,order_id,order_control)";
                                    q += " select @encid, @evdt, @code, @desc, @ts, @seq, @unit, @oid, @octrl";
                                    q += " where not exists (select encounter_id,code,event_datetime,unit_id,sequence from chart_item";
                                    q += " where encounter_id=" + _pat.encounter_id.ToString() + " and code='" + ordcode + "' and event_datetime='" + evdt.ToString() + "' and unit_id=-1 and sequence=" + seq + ")";
                                    //string q = "INSERT INTO chart_item (encounter_id,event_datetime,code,description,timestamp,sequence,unit_id,order_id) values (@encid, @evdt, @code, @desc, @ts,@seq,@unit,@oid)";
                                    SqlCommand cmd = new SqlCommand(q, db);
                                    cmd.Parameters.AddWithValue("@encid", _pat.encounter_id);
                                    cmd.Parameters.AddWithValue("@evdt", evdt);
                                    cmd.Parameters.AddWithValue("@code", ordcode);
                                    cmd.Parameters.AddWithValue("@desc", orddesc);
                                    cmd.Parameters.AddWithValue("@ts", DateTime.Now);
                                    cmd.Parameters.AddWithValue("@seq", seq);
                                    cmd.Parameters.AddWithValue("@unit", -1);
                                    cmd.Parameters.AddWithValue("@oid", ordid);
                                    cmd.Parameters.AddWithValue("@octrl", ordctrl);
                                    cmd.ExecuteNonQuery();
                                }

                            }
                        }// if codes match: add chart item 1 minute earlier
                    } // if arrays upper bounds are large enough
                } // if the orc and obr strings are not null
            } //dr read
            //dr2.Close();
            db2.Close();
            return ret;
        }


        private bool OrderInProgress(string code, out string found_what)
        {
            bool ret = false;
            found_what = "";
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = query.Where(e => e.CODE.ToUpper() == code.ToUpper()
                                  && e.EVENT_DATETIME < loc_out
                                  && e.ORDER_CONTROL.ToLower() == "nw"
                                  && (e.ORDER_STATUS == "" || !e.ORDER_STATUS.StartsWith("X") || e.ORDER_STATUS == null));
            query = query.OrderByDescending(e => e.EVENT_DATETIME);

            int count = query.Count();
            Program.VerboseAudit("count of orderNW records=" + count);

            foreach (var itemA in query)
            {
                var query2 = StartNewQuery(SearchDepth.SearchSinceAdmission);
                query2 = query2.Where(e => e.CODE.ToUpper() == code.ToUpper());
                query2 = query2.Where(e => e.ORDER_ID == itemA.ORDER_ID);
                query2 = query2.Where(e => e.ORDER_CONTROL.ToLower() == "ca" || e.ORDER_CONTROL.ToLower() == "cm");
                int ct2 = query2.Count();

                if (ct2 > 0)
                {
                    Program.VerboseAudit("order in progress: order_id=" + itemA.ORDER_ID + " _pat.pull_start=" + _pat.pull_start + " ct2="+ct2);
                    //                  Program.VerboseAudit("order in progress: q2ct=" + ct2);
                    foreach (var x in query2)
                    {
                        Program.VerboseAudit("x.event_datetime=" + x.EVENT_DATETIME);
                        if (x.EVENT_DATETIME >= _pat.pull_start)
                        {
                            ret = true;
                            found_what = "Order ID=" + itemA.ORDER_ID + ": " + itemA.DESCRIPTION + "; result=" + itemA.RESULT + "; range:" + itemA.EVENT_DATETIME.ToString() + " - " + x.EVENT_DATETIME.ToString();
                        }
                        else
                        {
                            DisableOrder(x.ORDER_ID);
                        }
                        //ret |= GetNWOrder(code, x.TIMESTAMP, x.EVENT_DATETIME);
                    }
                }
                else
                {
                    ret = true;
                    found_what = "Order ID=" + itemA.ORDER_ID + ": " + itemA.DESCRIPTION + "; result=" + itemA.RESULT + "; starting:" + itemA.EVENT_DATETIME.ToString();
                    //Program.VerboseAudit("AUDIT ORDER:" + found_what);
                }
            }


            return ret;
        }

        private void DisableOrder(string ordid)
        {
            //update ORDER_STATUS = 'X' for _pat.encounter_id and ordid
            var db = PFSDBUtility.NewSqlConnection();
            string q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and order_id='" + ordid + "'";
            SqlCommand cmd = new SqlCommand(q, db);
            cmd.ExecuteNonQuery();
            db.Close();

        }

        private void DisableSitter(string code, DateTime evdt, bool use_proc_start)
        {
            //update ORDER_STATUS = 'X' for _pat.encounter_id and ordid
            var db = PFSDBUtility.NewSqlConnection();
            //string q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and code='" + code + "' and (result='initiated' or result='continued') and event_datetime<=" + PFSDBUtility.SQLDateTime(evdt);
            string q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and code='" + code + "' and event_datetime<=" + PFSDBUtility.SQLDateTime(evdt);
            if (use_proc_start)
                q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and code='" + code + "' and procedure_start is not null and procedure_start<=" + PFSDBUtility.SQLDateTime(evdt);
            SqlCommand cmd = new SqlCommand(q, db);
            cmd.ExecuteNonQuery();
            db.Close();

        }
        private void DisableSitter(string code, DateTime evdt)
        {
            DisableSitter(code, evdt, false);
        }

        //private void CheckAssessment(int count, string desc)
        //{
        //    //if (_inds[18].is_checked) return;          //skip if highest already checked
        //    if (count == 0) return;                    //skip if none

            //    // This should work the same as the original code:
            //    switch (FreqForCount(_pat.los_hours, count))
            //    {
            //        case Frequencies.Q30M:
            //            SetInd(18, desc);
            //            break;
            //        case Frequencies.Q1H:
            //            SetInd(17, desc);
            //            break;
            //        case Frequencies.Q2H:
            //            SetInd(16, desc);
            //            break;
            //        case Frequencies.Q4H:
            //            SetInd(15, desc);
            //            break;
            //        default:
            //            break;
            //    }

            //}

        private void ShowBuckets(List<gBucket> buckets)
        {
            string s = "";
            foreach (var e in buckets)
            {
                s = "(" + e.bucket + "" + e.code + "" + e.evdt.ToString() + ")";
                Program.VerboseAudit("Assessmt item:=" + s);
            }
        }


        private void Check_15_16_17_18()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("15. Assessment q4h");
            Program.VerboseAudit("16. Assessment q2h");
            Program.VerboseAudit("17. Assessment q1h");
            Program.VerboseAudit("18. Assessment q30min");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(18, "", "NURSI CRRTStatus", "", "", "started,in progress");
            SetIndIfResultContains(16, "", "NURS_PROC Omya LOC Pre", "", "", "Alert, Lethargic, Stuporous, Semi-comatose, Drowsy, unable to assess");
            //For the assessments orders SpO2 or Telemetry see orders tab,
            // the asslevel can only go up to q2 with only spo2 charted
            // Would need other VS to get to asslevel q1 or higher.
            SetBucketSize(60);

            SetBucketSize(60);
            AnalyzeAssessments(15, 60);

            SetBucketSize(30);
            AnalyzeAssessments(18, 30);

            SetBucketSize(60);

        }

        private int GetAssessInd(DateTime loc_out_time, out DateTime classdt)
        {
            int ind = 0;
            classdt = DateTime.MinValue;
            //get assessment indicator at location out time = loc_out_time
            var db = PFSDBUtility.NewPfsDataContext();        // look for the unit default admission profile
            var query = from ce in db.CLASSIFICATION_EVENTs
                        from ia in db.INDICATOR_ANSWERs
                        where (ce.CLASSIFICATION_EVENT_ID == ia.CLASSIFICATION_EVENT_ID)
                        && (ce.ENCOUNTER_ID == _pat.encounter_id)
                        && (ce.EFFECTIVE_DATETIME_IN <= loc_out_time)
                        && (ia.INDICATOR_NUMBER >= 15 && ia.INDICATOR_NUMBER <= 18)
                        orderby ce.EFFECTIVE_DATETIME_IN descending
                        select new
                        {
                            ia.INDICATOR_NUMBER,
                            ce.CLASSIFICATION_DATETIME
                        };
            if (query.Count() > 0)
            {
                ind = query.First().INDICATOR_NUMBER;
                classdt = query.First().CLASSIFICATION_DATETIME;
            }
            return ind;
        }


        private void AnalyzeAssessments(int ind, int bucket_size)
        {
            string codelist;
            string reslist;
            List<gBucket> buckets;
            string freqstr = "";


            if (bucket_size == 60) freqstr = "====Q4/Q2/Q1 HR EVALUATION================";
            if (bucket_size == 30) freqstr = "====Q30 MIN EVALUATION====================";
            Program.VerboseAudit(freqstr + " bucket size=" + bucket_size + "  _bucket size=" + _bucket_size);

            assessgrouplabel = "BP";
            buckets = new List<gBucket>();
            codelist="NURS Respiratory Rate,NURS Post Procedure Blood Pressure Diastolic,NURS Post Procedure Blood Pressure Systolic,NURS Post Procedure Respiratory Rate,NURSI NIBP Systolic,NURSI NIBP Diastolic,NURSI ABP Systolic,NURSI ABP Diastolic,NURS Blood Pressure Systolic,NURS Blood Pressure Diastolic,NURS Post Procedure Heart Rate / Pulse,NURS Post Procedure Blood Pressure Diastolic,NURS Post Procedure Blood Pressure Systolic";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            assessgrouplabel = "Cardiac monitoring";
            buckets = new List<gBucket>();
            codelist = "NURSI Heart Rhythm,NURS Heart Rhythm";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            assessgrouplabel = "I/O";
            buckets = new List<gBucket>();
            codelist = "io_intake,io_output";
            AddBuckets(buckets, "", codelist, "", "", "");
            codelist = "NURSI CRRT";
            AddBuckets(buckets, "", codelist, "", "", "started, in progress");
            codelist = "NURSI Lumbar Status";
            AddBuckets(buckets, "", codelist, "", "", "continuous, intermittent");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            assessgrouplabel = "Gastrointestinal";
            buckets = new List<gBucket>();
            codelist = "NURSI BBGM Result,NURS Food/Fluid Food Intake Obs";
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            assessgrouplabel = "Genitourinary";
            buckets = new List<gBucket>();
            codelist = "NURS Urine Specific Gravity Reading,NURS Pregnancy Test Results,NURS Urinary Post Void Residu Measure By,NURS CBI Color of Drainage,NURSI Bladder Mean,NURSI CBI Color of Drainage";
            AddBuckets(buckets, "", codelist, "", "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            assessgrouplabel = "Glucose";
            buckets = new List<gBucket>();
            codelist = "NURS BGM Glucose H/H Interventions,NURS Insulin Pump Site Condition,NURS Insulin Pump Interventions,NURS Post Procedure Temperature,NURSI BBGM Result,NURS BGM Glucose Reading";
            AddBuckets(buckets, "", codelist, "", "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            assessgrouplabel = "Heart rate/rhythm";
            buckets = new List<gBucket>();
            codelist = "NURS Radial Artery,NURSI Heart Rate/Pulse,NURS Heart Rate/Pulse,NURS Heart Rhythm,NURSI Heart Rhythm,NURSI Pacemaker Type,NURS Blood Pressure Systolic,NURS Blood Pressure Diastolic";
            AddBuckets(buckets, "", codelist, "", "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            assessgrouplabel = "Neuro";
            buckets = new List<gBucket>();
            codelist = "NURS Oriented To,NURS Disoriented To,NURS Respiratory Depth,NURS Food/Fluid Food Intake Obs, NURSI Level of Consciousness, NURSI Oriented To,NURSI ICP,NURS Level of Consciousness,NURS FS Level of Consciousness,NURSI RASS,NURS EEG Electrode Types,NURSI EEG Electrode Types";
            AddBuckets(buckets, "", codelist, "", "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            assessgrouplabel = "Pulmonary";
            buckets = new List<gBucket>();
            codelist = "NURS Circulation Cardiac Monitor,NURS Post Procedure Heart Rate/Pulse,NURS Respiratory Depth,NURS Respiratory Oxygen Delivery Mode,NURS Respiratory Oxygen Saturation,NURS Respiratory Rate,NURS Respiratory Rhythm at Rest,NURS Sleep Respirations,NURS Suction Route,NURS Urinary Post Void Residu Measure By,NURSI O2 Delivery Mode,NURSI O2 Saturation,NURSI Resp Depth/Rhythm,NURSI Respiratory Rate";
            AddBuckets(buckets, "", codelist, "", "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            assessgrouplabel = "Skin/Temp";
            buckets = new List<gBucket>();
            codelist = "NURS Braden Risk Assessment,NURS Skin Characteristics,NURS Temp Reg Device Status,NURS Temperature,NURS Temp Reg Device Status,NURS Temp Reg Device Status,NURSI Temperature,NURSI TX Temp Reg Device Status,NURS Post Procedure Temperature";
            AddBuckets(buckets, "", codelist, "", "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            assessgrouplabel = "Symptom/Pain Mgt";
            buckets = new List<gBucket>();
            codelist = "NURS Symptom Assessment Reason,NURS Symptom Assessment Reason For Intervention,NURS CIWA Total Score,NURS Pain CNVI At Rest Total,NURS Pain Presence of Pain,NURS Pain Cries Total,NURS Pain FLACC Total,Nurs Pain Comfort Total";
            AddBuckets(buckets, "", codelist, "", "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            assessgrouplabel = "Telemetry";
            buckets = new List<gBucket>();
            codelist = "NURS Circulation Monitor Status,NURS Circulation Cardiac Monitor Interventions,NURS Heart Rhythm,NURS Capillary Refill/Nailbeds,NURS Circulation Cardiac Monitor";
            AddBuckets(buckets, "", codelist, "", "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            assessgrouplabel = "Urine";
            buckets = new List<gBucket>();
            codelist = "NURS Urine Specific Gravity Reading,NURS Urine Hemastix Results";
            AddBuckets(buckets, "", codelist, "", "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            assessgrouplabel = "Vascular";
            buckets = new List<gBucket>();
            codelist = "NURS Capillary Refill,NURS Circulation Capillary Refill,NURS Circulation Pulse Assessment,NURS Post Procedure Heart Rate/Pulse";
            codelist += ",NURS Radial Artery,NURS Dorsalis Pedis Artery,NURS Posterior Tibial,NURS Femora Artery,NURS Brachial Artery,NURS Carotid Artery,NURS Popliteal Artery,NURS Temporal Artery";
            codelist += ",NURS Skin Turgor,NURS Skin Color,NURS Skin Temperature";
            AddBuckets(buckets, "", codelist, "", "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            assessgrouplabel = "Wound";
            buckets = new List<gBucket>();
            codelist = "NURS Procedure Site Observations";
            AddBuckets(buckets, "", codelist, "", "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

        }

        private bool AnalyzeBuckets(List<gBucket> buckets, int ind, int bucketsize, string group)
        {
            return AnalyzeBuckets(buckets, ind, bucketsize, group, true);

        }

        private bool AnalyzeBuckets(List<gBucket> buckets, int ind, int bucketsize, string group, bool set_ind)
        {
            DateTime dt = DateTime.MinValue;
            DateTime firstdt = DateTime.MinValue;

            bool all_ok = OLDAnalyzeBuckets(buckets, ind, bucketsize, group, set_ind);
            return all_ok;


//            int bnum = 0;
//            int numbucket = -99;
//            int numitems = 0;
//            int numallitems = 0;
//            List<gGap> gaplist = new List<gGap>();
//            Program.VerboseAudit("----GAP Begin Assessment Group = " + group + " ---- bucketsizew=" + bucketsize);

//            //Program.VerboseAudit("buckets count=" + buckets.Count());
//            var b = buckets.OrderBy(e => e.evdt).ToList();
//            //numitems = buckets.Count();

//            DateTime lastq4dt = DateTime.MinValue;
//            int numq4 = 1;

//            foreach (var item in b)
//            {
//                Program.VerboseAudit(item.bucket + ") " + item.evdt.ToString() + " := " + item.code + " has all deps=" + item.has_all_deps);
//                if (item.has_all_deps)
//                    if (numitems == 0)
//                    {
//                        dt = item.evdt;
//                        firstdt = dt;
//                        lastq4dt = dt;
//                    }
//                    else
//                    {
//                        if (lastq4dt.AddMinutes(30) >= dt)
//                        {
//                            numq4++;
//                            lastq4dt = dt;
//                        }
//                        var g = new gGap();
//                        g.gap = (int)(PFSUtility.DateDiffInMinutes(dt, item.evdt));
//                        g.evdt1 = dt;
//                        g.evdt2 = item.evdt;
//                        gaplist.Add(g);
//                        dt = item.evdt;
//                    }
//                numitems++;
//                numallitems = numallitems + 1 + item.num_addl_items;
//            }
//            //Program.VerboseAudit("numitems=" + numitems + " numq4=" + numq4);
//            //if (numitems <= 2)
//            //{
//            //    if (numq4 >= 2)
//            //    {
//            //        if (set_ind) //&& ind >= 15 && ind <= 17)
//            //        {
//            //            SetInd(15, "q4 based on number >=2 of items charted = " + numq4);
//            //            return true;
//            //        }
//            //    }
//            //    return false;
//            //}

//            int i, j, setind = 0;
//            int numgaps = 0;
//            int gapsum = 0;
//            double gapavg = 999.0, lo_gapavg = 999.0, altlo_gapavg = 999.0;
//            bool lastgap = false;
//            bool shortstop = false;
//            int timegap = 0;
//            int lastj;
//            DateTime lo_evdt1 = DateTime.MinValue, lo_evdt2 = DateTime.MinValue;
//            DateTime altlo_evdt1 = DateTime.MinValue, altlo_evdt2 = DateTime.MinValue;
//            gGap[] gapary = gaplist.ToArray();
//            string s = "";
//            for (i = 0; i <= gapary.GetUpperBound(0); i++) s = s + "" + gapary[i].gap;
//            if (numitems == 2 && numq4 >= 2) // numitems will always be > 2
//            {
//                numgaps = 1;
//                gapsum = gapary[0].gap;
//                gapavg = gapsum;
//                lo_gapavg = gapavg;
//                lo_evdt1 = gapary[0].evdt1;
//                lo_evdt2 = gapary[gapary.GetUpperBound(0)].evdt2;
//            }
//            else
//            {

//                //numitems = 4
//                //i = 0 j = 0[i].evdt1 = 9 / 4 / 2019 5:15:00 AM[j].evdt2 = 9 / 4 / 2019 9:32:00 AM

//                for (i = 0; i <= gapary.GetUpperBound(0) && !shortstop; i++)
//                {
//                    numgaps = 0;
//                    gapsum = 0;
//                    gapavg = 999.0;
//                    lastgap = false;
//                    for (j = i; j <= gapary.GetUpperBound(0) && !lastgap; j++)
//                    {
//                        //Program.VerboseAudit("i=" + i + " j=" + j + " [i].evdt1=" + gapary[i].evdt1 + " [j].evdt2=" + gapary[j].evdt2);
//                        timegap = (int)(PFSUtility.DateDiffInMinutes(gapary[i].evdt1, gapary[j].evdt2));
//                        if (timegap <= _pat.los_hours * 30 + 30)
//                        {
//                            numgaps++;
//                            gapsum += gapary[j].gap;
//                            Program.VerboseAudit("numgaps=" + numgaps + " [j].gap=" + gapary[j].gap + " gapsum=" + gapsum);
//                        }
//                        else
//                        {
//                            if (j == i)
//                            {
//                                lastj = i;
//                                numgaps = 1;
//                                gapsum = gapary[j].gap;
//                            }
//                            else
//                                lastj = j - 1;
//                            //Program.VerboseAudit("Last Gap: i=" + i + " j=" + j + " [i].evdt1=" + gapary[i].evdt1 + " [lastj].evdt2=" + gapary[lastj].evdt2);
//                            lastgap = true;
//                            gapavg = 1.0 * gapsum / numgaps;
//                            Program.VerboseAudit("gapavg=" + gapavg + " numgaps=" + numgaps + " gapsum=" + gapsum);
//                            timegap = (int)(PFSUtility.DateDiffInMinutes(gapary[i].evdt1, gapary[lastj].evdt2));
//                            //                            if (gapavg < lo_gapavg && timegap >= .75 * _pat.los_hours * 30)
//                            if (gapavg < lo_gapavg && timegap >= _pat.los_hours * 30)
//                            {
//                                lo_gapavg = gapavg;
//                                //Program.VerboseAudit("Low gapavg=" + lo_gapavg + " btwn " + gapary[i].evdt1 + " and " + gapary[lastj].evdt2);
//                                lo_evdt1 = gapary[i].evdt1;
//                                lo_evdt2 = gapary[lastj].evdt2;
//                            }
//                            numgaps = 0;
//                            gapsum = 0;
//                        }

//                    }
//                    if (!lastgap)
//                    {
//                        shortstop = true;
//                        j--; // take the j index back by 1 because it incremented at the end.
//                        //Program.VerboseAudit(".Last Gap: i=" + i + " j=" + j + " [i].evdt1=" + gapary[i].evdt1 + " [j].evdt2=" + gapary[j].evdt2);
//                        gapavg = 1.0 * gapsum / numgaps;
//                        //Program.VerboseAudit(".gapavg=" + gapavg + " numgaps=" + numgaps + " gapsum=" + gapsum);
//                        timegap = (int)(PFSUtility.DateDiffInMinutes(gapary[i].evdt1, gapary[gapary.GetUpperBound(0)].evdt2));
//                        //Program.VerboseAudit(".timegap=" + timegap + " .75halfLOS=" + .75 * _pat.los_hours * 30);
//                        if (gapavg < lo_gapavg)
//                        {
//                            if (timegap >= _pat.los_hours * 30)
////                          if (timegap >= .75 * _pat.los_hours * 30)
//                            {
//                                lo_gapavg = gapavg;
//                                //Program.VerboseAudit(".Low gapavg=" + lo_gapavg + " btwn " + gapary[i].evdt1 + " and " + gapary[gapary.GetUpperBound(0)].evdt2);
//                                lo_evdt1 = gapary[i].evdt1;
//                                lo_evdt2 = gapary[gapary.GetUpperBound(0)].evdt2;
//                            }
//                            else
//                            {
//                                altlo_gapavg = gapavg;
//                                //Program.VerboseAudit(".AltLow gapavg=" + lo_gapavg + " btwn " + gapary[i].evdt1 + " and " + gapary[gapary.GetUpperBound(0)].evdt2);
//                                altlo_evdt1 = gapary[i].evdt1;
//                                altlo_evdt2 = gapary[gapary.GetUpperBound(0)].evdt2;
//                            }
//                        }
//                    }
//                }
//            }
//            if (numitems >= 2) Program.VerboseAudit("Final Low gapavg=" + lo_gapavg + " btwn " + lo_evdt1.ToString() + " and " + lo_evdt2.ToString() + " " + (_pat.los_hours <= 4 ? 0 : 1) + "LOS=" + _pat.los_hours + " assmts:" + (_inds[15].is_checked ? 1 : 0) + (_inds[16].is_checked ? 1 : 0) + (_inds[17].is_checked ? 1 : 0) + (_inds[18].is_checked ? 1 : 0) + " gap[]: =" + s);
//            //Program.VerboseAudit("Alt   Low gapavg=" + altlo_gapavg + " btwn " + altlo_evdt1.ToString() + " and " + altlo_evdt2.ToString());
//            //q4: 3 hr + gap average  in 6 - 8 hrs
//            //  q2:   1 hr 30 - 2 hr 59m avg gap  in 4 hrs
//            //  q1:   46 min - 1 hr 29 min avg gap in 4 hrs
//            //  q30:  45 min or less avg gap in 4 hrs
//            //            if (lo_gapavg <= 45)
//            int sample_breadth = (int)(PFSUtility.DateDiffInMinutes(lo_evdt1, lo_evdt2));
////            if (_pat.los_hours >= 4 && sample_breadth > 0.75 * _pat.los_hours * 30 && numitems >= 2)
//            if (_pat.los_hours >= 4 && sample_breadth >= _pat.los_hours * 30 && numitems >= 2)
//            {
//                //if (lo_gapavg <= 45) setind = 18;       //q30
//                //else if (lo_gapavg <= 80) setind = 17;  //q60
//                //else if (lo_gapavg <= 180) setind = 16; //q120
//                //else if (lo_gapavg <= 999) setind = 15;  //q240
//                if (lo_gapavg <= 40) setind = 18;       //q30
//                else if (lo_gapavg <= 70) setind = 17;  //q60
//                else if (lo_gapavg <= 130) setind = 16; //q120
//                else if (lo_gapavg <= 250) setind = 15;  //q240

//            }
//            else
//            {
////                if (numitems > 2 && sample_breadth >= 0.75 * _pat.los_hours * 30)
//                if (numitems > 2 && sample_breadth >= _pat.los_hours * 30)
//                {
//                    if (lo_gapavg <= 30) setind = 18;  //q60
//                    else if (lo_gapavg <= 60) setind = 17;  //q60
//                    else if (lo_gapavg <= 120) setind = 16; //q120
//                    else if (lo_gapavg <= 240) setind = 15;  //q240
//                }
//                else if (numitems == 2 && sample_breadth >= 30)
//                {
//                    //if (sample_breadth >= 0.5 * _pat.los_hours * 30) setind = 16; //q120
//                    //else setind = 15;  //q240
//                    if (sample_breadth >= _pat.los_hours * 30 && lo_gapavg <= 240) setind = 15;
//                    //else setind = 15;  //q240
//                }
//                //else if (numitems >= 2)
//                //    setind = 15;
//            }
//            int imins = 0;
//             all_ok = false;
//            if (setind == 18) imins = 30;
//            else if (setind == 17) imins = 60;
//            else if (setind == 16) imins = 120;
//            else if (setind == 15) imins = 240;
//            if (ind == 11)
//            {
//                if (setind >= 15)
//                    setind = 11;
//                else
//                    setind = 0;
//            }
//            //if (ind >= 15 && ind <= 17)
//            //{
//            //    if (setind == 18) setind = 17;
//            //}
//            if (setind > 0)
//            {
//                if (set_ind)
//                    SetInd(setind, "Qualifies for q" + imins + "mins for gap avg=" + lo_gapavg + "[" + (sample_breadth < 4 ? "<" : "") + "in " + sample_breadth + " mins] btwn " + lo_evdt1.ToString() + " and " + lo_evdt2.ToString());
//                else
//                    Program.VerboseAudit(group + "Qualifies for q" + imins + "mins for gap avg=" + lo_gapavg + "[" + (sample_breadth < 4 ? "<" : "") + "in " + sample_breadth + " mins] btwn " + lo_evdt1.ToString() + " and " + lo_evdt2.ToString());
//            }
//            if (ind == 18 && setind == 18) all_ok = true;
//            if (ind >= 15 && ind <= 17 && setind >= 15) all_ok = true;
//            if (ind == 11 && setind == 11) all_ok = true;
//            if (ind == 3 && setind >= 16) all_ok = true;
//            if (ind == 19 && setind >= 17) SetInd(19, "Site Assessmt q1hr+ and age=" + _pat.age);
//            //if (ind == 19 && setind >= 16 && _pat.age <= 6.0) SetInd(19, "Site Assessmt q2hr+ and age=" + _pat.age);
//            Program.VerboseAudit("---- GAP End Assessment Group = " + group + " ----");
//            return all_ok;

        }
        private bool DoShortLOSAssessEval(List<gBucket> buckets, int ind, int bucketsize, string group, bool set_ind)
        {
            int ct = 0;
            DateTime starttm, endtm;
            DateTime prevtm = DateTime.MinValue;
            int loshrs = (int)(Math.Round(_pat.los_hours));
            var arr = buckets.ToArray();
            bool set18 = false;
            Program.VerboseAudit("----Entering Short LOS Analysis for group: " + group + " LOS=" + loshrs + "  bucketsizew=" + bucketsize);

            for (int i = 0; (i < arr.GetUpperBound(0)); i++)
            {
                prevtm = DateTime.MinValue;
                starttm = arr[i].evdt;
                endtm = starttm.AddHours(loshrs);
                ct = 0;
                for (int j = i; (j < arr.GetUpperBound(0)); j++)
                {
                    if (prevtm < arr[j].evdt && arr[j].evdt >= starttm && arr[j].evdt <= endtm)
                    {
                        ct++;
                        prevtm = arr[j].evdt;
                    }
                }
                if (ct >= 9 / (4.0/loshrs))
                {
                    SetInd(18, ct.ToString() + " or more items in LOS=" + loshrs + " hours.");
                    set18 = true;
                    i = 999;
                }
            }
            Program.VerboseAudit("----Exiting Short LOS Analysis for group: " + group + " LOS=" + loshrs + "  bucketsizew=" + bucketsize);

            return set18;
        }


//Proposed change to remove consecutive factor; use straight scale with Short LOS exception:  
//	                        LOS=12 hrs      LOS = 6 hrs     LOS = 4 hrs     LOS = 2 hrs     Short LOS< 5 hrs
//q30 75% of 30-min buckets	3/4 x 24  = 18	3/4 x 12  = 9	3/4 x 8  = 6	3/4 x 4  = 3	5 or more VS
//q1 66% of 60-min buckets	2/3 x 12  = 8	2/3 x 6  = 4	2/3 x 4  = 2.67=>3	2/3 x 2  = 1.33=>1	3 VS + examine 30min 
//q2 50 % of 60-min buckets	.5 x 12  = 6	.5 x 6  = 3	.5 x 4  = 2	.5 x 2  = 1	2 VS
//q4 33% of 60-min buckets	1/3 x 12  = 4 2	1/3 x 6  = 2 1	1/3 x 4  = 1.33=>1	1/3 x 2  = .67=>1	1 VS
        private bool OLDAnalyzeBuckets(List<gBucket> buckets, int ind, int bucketsize, string group, bool set_ind)
        {
            DateTime dt = DateTime.MinValue;
            int bnum = 0;
            int numbucket = -99;
            int lobucket = 99;
            int hibucket = 0;
            int numitems = 0;
            int numconsec = 0;
            int greatestnumconsec = 0;
            bool all_ok = false;
            List<gBucket> dtlist = new List<gBucket>();
            Program.VerboseAudit("----Begin Assessment Group = " + group + " ---- bucketsizew=" + bucketsize + " start time of first bucket=" + _pat.pull_start);
            //if (ind==18 && Math.Round(_pat.los_hours) <= 4.0)
            //{
            //    all_ok = DoShortLOSAssessEval(buckets,ind,bucketsize,group,set_ind);
            //    Program.VerboseAudit("---- End Assessment Group = " + group + " ----");
            //    return all_ok;
            //}

            //Program.VerboseAudit("buckets count=" + buckets.Count());
            var b = buckets.OrderBy(e => e.evdt).ToList();

            //numitems = buckets.Count();
            foreach (var item in b)
            {
                if (numbucket < item.bucket)
                {
                    numbucket = item.bucket;
                    numitems++;
                    Program.VerboseAudit(item.bucket + ") " + item.evdt.ToString() + " := " + item.code);
                }
                //if (numbucket < item.bucket)
                //{
                //    //add dt to ary
                //    bnum++;
                //    dtlist.Add(item);
                //    if (numbucket == -99 || item.bucket - numbucket == 1)
                //    {
                //        numconsec++;
                //        if (greatestnumconsec < numconsec)
                //            greatestnumconsec = numconsec;
                //    }
                //    else
                //    {
                //        numconsec = 1;
                //    }
                //    numbucket = item.bucket;
                //    if (hibucket < item.bucket) hibucket = item.bucket;
                //    if (lobucket > item.bucket) lobucket = item.bucket;
                //    Program.VerboseAudit(item.bucket + ")." + item.evdt.ToString() + " := " + item.code);// + "  [distinct times only]");
                //}
            }

            //if (bnum <= 1)
            //{
            //    Program.VerboseAudit("---- End Assessment Group = " + group + " ----");
            //    return false;
            //}

            //Program.VerboseAudit("numitems=" + numitems);
            //Program.VerboseAudit("bucket count=" + bnum);
            //int half_los = (int)(_pat.los_hours * (30.0 / bucketsize));//for q30 this will be los_hours.  for q60 this will be .5 * los_hours
            //Program.VerboseAudit("half-LOS bucket count=" + half_los + " at " + bucketsize + "mins each.");
            //Program.VerboseAudit("greatest consec=" + greatestnumconsec);

            int num_buckets_in_los = (int)(_pat.los_hours * (60.0 / bucketsize));//for q30 this will be 2xlos_hours.  for q60 this will be los_hours
            Program.VerboseAudit("total bucket count in LOS=" + num_buckets_in_los);
            Program.VerboseAudit("num buckets filled=" + numitems);
            //Program.VerboseAudit("half-LOS bucket count=" + half_los + " at " + bucketsize + "mins each.");
            //Program.VerboseAudit("greatest consec=" + greatestnumconsec);

            //double bucketratio = (hibucket-lobucket) / (1.0 * (bnum-1));
            //q30 75% of 30-min buckets	3/4 x 24  = 18	3/4 x 12  = 9	3/4 x 8  = 6	3/4 x 4  = 3	5 or more VS
            //q1 66% of 60-min buckets	2/3 x 12  = 8	2/3 x 6  = 4	2/3 x 4  = 2.67=>3	2/3 x 2  = 1.33=>1	3 VS + examine 30min 
            //q2 50 % of 60-min buckets	.5 x 12  = 6	.5 x 6  = 3	.5 x 4  = 2	.5 x 2  = 1	2 VS
            //q4 33% of 60-min buckets	1/3 x 12  = 4 2	1/3 x 6  = 2 1	1/3 x 4  = 1.33=>1	1/3 x 2  = .67=>1	1 VS
            int q30need = (int)Math.Round(0.75 * 2 * _pat.los_hours);
            int q1need = (int)Math.Round(0.667 * _pat.los_hours);
            int q2need = (int)Math.Round(0.5 * _pat.los_hours);
            int q4need = 1;
            if (ind < 18)
            {
                if (_pat.los_hours > 5)
                {
                    if (_pat.los_hours < 8)
                    {
                        q4need = 1;
                        q2need = 2; //Jan27 2021
                    }
                    else
                    {
                        q4need = 2;
                        q2need = 4; // Feb3 2021 4;//Jan27 2021
                    }

                    if (numitems >= q1need)
                    {
                        SetInd(17 * Convert.ToInt32(set_ind), "Qualifies for q1 because numcharted=" + numitems + " is >=" + q1need + " LOS=" + Math.Round(_pat.los_hours, 2) + " x .667=" + q1need);
                        all_ok = (ind <= 17);
                    }
                    else if (numitems >= q2need)
                    {
                        //Jan27 2021 SetInd(16 * Convert.ToInt32(set_ind), "Qualifies for q2 because numcharted=" + numitems + " is >=" + q2need + " LOS=" + Math.Round(_pat.los_hours, 2) + " x .5=" + q2need);
                        SetInd(16 * Convert.ToInt32(set_ind), "Qualifies for q2 because numcharted=" + numitems + " is >=" + q2need + " for LOS=" + Math.Round(_pat.los_hours, 2) + "  >=8hrs needs 5 else 2");
                        all_ok = (ind <= 16);
                    }
                    else if (numitems >= q4need)
                    {
                        SetInd(15 * Convert.ToInt32(set_ind), "Qualifies for q4 because numcharted=" + numitems + " is >=" + q4need + " LOS=" + Math.Round(_pat.los_hours, 2) + "  >=8hrs needs 2 else 1");
                        all_ok = (ind <= 15);
                    }
                }
                else //short los
                {
                    q1need = 3;
                    q2need = 2;
                    q4need = 1;

                    if (numitems >= q1need)
                    {
                        SetInd(17 * Convert.ToInt32(set_ind), "ShortLOS Qualifies for q1 because numcharted=" + numitems + " is >=" + q1need + " LOS=" + Math.Round(_pat.los_hours, 2) + " needs just 3");
                        all_ok = (ind <= 17);
                    }
                    else if (numitems >= q2need)
                    {
                        SetInd(16 * Convert.ToInt32(set_ind), "ShortLOS Qualifies for q2 because numcharted=" + numitems + " is >=" + q2need + " LOS=" + Math.Round(_pat.los_hours, 2) + " needs just 2");
                        all_ok = (ind <= 16);
                    }
                    else if (numitems >= q4need)
                    {
                        SetInd(15 * Convert.ToInt32(set_ind), "ShortLOS Qualifies for q4 because numcharted=" + numitems + " is >=" + q4need + " LOS=" + Math.Round(_pat.los_hours, 2) + " needs just 1");
                        all_ok = (ind <= 15);
                    }

                }
//                if (greatestnumconsec >= half_los || bucketratio <= 1.5 && bnum >= half_los-1)
//                {
//                    SetInd(17 * Convert.ToInt32(set_ind), "Qualifies for q1hr because chartings are at least q1hr for duration of half-LOS=" + half_los + " maxconsec=" + greatestnumconsec);
//                    all_ok = (ind <= 17);
//                }
//                //                else if (bucketratio <= 1.25 && bnum >= half_los && half_los >= 4)
//                //else if (bucketratio <= 1.25 && bnum >= half_los && half_los >= 4)
//                //{
//                //    SetInd(17 * Convert.ToInt32(set_ind), "Qualifies for q1hr because charting to bucket ratio is less than 1.25: hibucket-lobucket=" + hibucket +"-" +lobucket+"="+ (hibucket - lobucket) + " divided by num buckets=" + bnum + " equals " + bucketratio);
//                //    all_ok = (ind >= 17);
//                //}
//                //else if (bucketratio <= 2.5 && bnum >= 2)
//                //                else if (bnum >= half_los/2)
//                else if (bucketratio < 3 && bnum >= 3 && 
//                    (
//                    (half_los >= 6 && (hibucket - lobucket) >= half_los-1)
//                    ||
//                    ((half_los >= 4 && half_los <= 5) && (hibucket - lobucket) >= half_los - 1)
//                    ||
//                    ((half_los <= 3) && (hibucket - lobucket) >= half_los)
//                    )
//                    )
//                {
//                    SetInd(16 * Convert.ToInt32(set_ind), "Qualifies for q2hr because charting to bucket ratio is less than 3: hibucket-lobucket=" + hibucket + "-" + lobucket + "=" + (hibucket - lobucket) + " divided by num buckets=" + (bnum-1) + " equals " + bucketratio);
//                    all_ok = (ind <= 16);
//                }
////                else if (bucketratio > 2.5 && bucketratio <= 5 && bnum >= 2)
////                else if (bnum >= half_los/4)
//                else if ((bucketratio <= 6 && bnum >= 2) && //change to <=6 Oct15/2020
//                    (
//                    (half_los >= 6 && (hibucket - lobucket) >= half_los - 2)
//                    ||
//                    ((half_los >= 4 && half_los <= 5) && (hibucket - lobucket) >= half_los - 1)
//                    ||
//                    ((half_los <= 3) && (hibucket - lobucket) >= half_los)
//                    )
//                    )
//                {
//                    SetInd(15 * Convert.ToInt32(set_ind), "Qualifies for q4hr because charting to bucket ratio is <=6: hibucket-lobucket=" + hibucket + "-" + lobucket + "=" + (hibucket - lobucket) + " divided by num buckets=" + (bnum-1) + " equals " + bucketratio);
//                    all_ok = (ind == 15);
//                }
            }
            else // ind==18
            {
                if (_pat.los_hours > 5)
                {
                    if (numitems >= q30need)
                    {
                        SetInd(18 * Convert.ToInt32(set_ind), "Qualifies for q30 because numcharted=" + numitems + " is >=" + q30need + " LOS=" + Math.Round(_pat.los_hours, 2) + " x 2 x .75=" + q30need);
                        all_ok = (ind <= 18);
                    }
                }
                else
                {
                    q30need = 5;
                    if (numitems >= q30need)
                    {
                        SetInd(18 * Convert.ToInt32(set_ind), "ShortLOS Qualifies for q30 because numcharted=" + numitems + " is >=" + q30need + " LOS=" + Math.Round(_pat.los_hours, 2) + " x 2 needs " + q30need);
                        all_ok = (ind <= 18);
                    }
                }
                //if (greatestnumconsec >= half_los)
                //{
                //    SetInd(18 * Convert.ToInt32(set_ind), "Qualifies for q30min because consecutive chartings are at least q30 for duration of half-LOS=" + half_los + " maxconsec=" + greatestnumconsec);
                //    all_ok = (ind <= 18);
                //}
            }
            Program.VerboseAudit("---- End Assessment Group = " + group + " ----");
            return all_ok;
        }


        private void SetBucketSize(int minutes)
        {
            _bucket_size = minutes;
        }

        //Add to the list of bucket numbers (redundant buckets are fine)
        private void AddBuckets(List<gBucket> bucket_list, string cat, string desc, string code_list, string field)
        {
            AddBuckets(bucket_list, cat, code_list, desc, field, "", SearchDepth.SearchDefault);
        }
        private void AddBuckets(List<gBucket> bucket_list, string cat, string desc, string code_list,  string field, string result_list)
        {
            AddBuckets(bucket_list, cat, code_list, desc, field, result_list, SearchDepth.SearchDefault);

        }
        private void AddBuckets(List<gBucket> bucket_list, string cat, string desc, string code_list,  string field, string result_list, SearchDepth search_depth)
        {
            // Assign (virtual) bucket numbers to each assessment.  
            // Every x minutes after that is a new bucket.  Bucket zero = pull_start time.
//            Program.VerboseAudit("----Locating items for group: " + assessgrouplabel + "   bucketsize=" + _bucket_size);

            // get the chart items for the assessments
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc, field, result_list);
            int ct = query.Count();
            Program.VerboseAudit("ct=" + ct + " of codelist=" + code_list);
            // This step is needed for those who want to count the # within a bucket, like Shands #24.
            // This will have no effect on those that count buckets.
            //var query2 = (from item in query select new { item.EVENT_DATETIME }).Distinct();
            //var query2 = (from item in query select new { item.EVENT_DATETIME, item.CODE });

            //foreach (var x in query2)
            //{
            //    Program.VerboseAudit("patpullstart=" + _pat.pull_start.ToString() + " itemevdt=" + x.EVENT_DATETIME.ToString() + " diff=" + (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, x.EVENT_DATETIME)) + " bucketsize=" + _bucket_size + " bucket=" + (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, x.EVENT_DATETIME) / _bucket_size));
            //}

            // figure out what buckets the events belong to
            //var query3 = from item in query2
            //             select new {
            //                bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size)
            //             };
            //// Add to the list
            //foreach (var item in query3) {
            //    bucket_list.Add(item.bucket);
            //}

            var query3 = from item in query
                         select new
                         {
                             bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                             code = item.CODE,
                             desc = item.DESCRIPTION,
                             evdt = item.EVENT_DATETIME
                         };
            // Add to the list
            int cta = 0;
            foreach (var item in query3)
            {
                Program.VerboseAudit("  item: bucket=" + item.bucket + " c=" + item.code + "desc=" + item.desc + " dt=" + item.evdt);
                var b = new gBucket();
                b.bucket = item.bucket;
                b.code = item.code;
                b.description = item.desc;
                b.evdt = item.evdt;
                b.has_all_deps = true;
                gBucket f = bucket_list.Find(x => x.evdt == item.evdt);
                if (f.evdt != item.evdt)
                {
                    bucket_list.Add(b);
                    cta++;
                }
            }
            Program.VerboseAudit("add ct=" + cta);

        }


        private void AddVitalsBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field, string result_list, SearchDepth search_depth)
        {
            // Assign (virtual) bucket numbers to each assessment.  
            // Every x minutes after that is a new bucket.  Bucket zero = pull_start time.
            Program.VerboseAudit("----Locating Vital Signs: Filling buckets...Bucketsize=" + _bucket_size + " start time of first bucket=" + _pat.pull_start);

            // get the chart items for the assessments
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc, field, result_list);

            // This step is needed for those who want to count the # within a bucket, like Shands #24.
            // This will have no effect on those that count buckets.
            //var query2 = (from item in query select new { item.EVENT_DATETIME }).Distinct();
            //var query2 = (from item in query select new { item.EVENT_DATETIME, item.CODE });

            //foreach (var x in query2)
            //{
            //    Program.VerboseAudit("patpullstart=" + _pat.pull_start.ToString() + " itemevdt=" + x.EVENT_DATETIME.ToString() + " diff=" + (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, x.EVENT_DATETIME)) + " bucketsize=" + _bucket_size + " bucket=" + (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, x.EVENT_DATETIME) / _bucket_size));
            //}

            // figure out what buckets the events belong to
            //var query3 = from item in query2
            //             select new {
            //                bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size)
            //             };
            //// Add to the list
            //foreach (var item in query3) {
            //    bucket_list.Add(item.bucket);
            //}

            var query3 = from item in query
                         select new
                         {
                             bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                             code = item.CODE,
                             evdt = item.EVENT_DATETIME,
                             desc = item.DESCRIPTION
                         };
            var query4 = from item in query3
                         orderby item.bucket,item.code
                         select item;

            // Add to the list
            int currb = -1;
//            int prevb = -1;
            string currcode = "xx";
            DateTime currevdt = DateTime.MinValue;
            int currct = 0;
  //          bool addcurr = false;
            //Data is: 0, "123"
            //         0, "234"  0733
            //         0, "234"  0734
            //         1, "123"
            //         2, "234"
            //         2, "345"
            foreach (var item in query4)
            {
                int maxLength = Math.Min(item.desc.Length, 16);
                string d = item.desc.Substring(0, maxLength);
                Program.VerboseAudit("Vitals item: bucket=" + item.bucket + " dt=" + item.evdt + " desc=" + d + " c=" + item.code);
                if (currb != item.bucket)
                {
                    currb = item.bucket;
                    currcode = item.code;
                    currevdt = item.evdt;
                    currct = 1;
                }
                else
                {
                    if (currcode != item.code)
                    {  //minimum of 2 different VS codes
                        if (currct == 1)
                        { // add the first code first
                            var b = new gBucket();
                            b.bucket = item.bucket;
                            b.code = currcode;
                            b.evdt = currevdt;
                            b.has_all_deps = true;
                            //gBucket f = bucket_list.Find(x => x.evdt == item.evdt);
                            //if (f.evdt != item.evdt) bucket_list.Add(b);
                            bucket_list.Add(b);
                            Program.VerboseAudit("  adding item1: b=" + b.bucket + " c=" + b.code + " dt=" + b.evdt);
                        }
                        currct++;
                        currcode = item.code; //guarantees not to add code again in same bucket
                            
                        var b2 = new gBucket();
                        b2.bucket = item.bucket;
                        b2.code = item.code;
                        b2.evdt = item.evdt;
                        b2.has_all_deps = true;
                        //gBucket f = bucket_list.Find(x => x.evdt == item.evdt);
                        //if (f.evdt != item.evdt) bucket_list.Add(b);
                        bucket_list.Add(b2);
                        Program.VerboseAudit("  adding item2: b=" + b2.bucket + " c=" + b2.code + " dt=" + b2.evdt);
                    }
                }
            }

            // This is just for auditing: Say what we are looking for and how many were found
            if (!String.IsNullOrEmpty(result_list))
            {
                // print each word and if it was found or not
                //int i = CountResultContains(cat, code_list, desc, field, result_list);
            }
            else
            {
                // print how many were found
                //Program.VerboseAudit(Describe(cat, code_list, desc, field, ""));
            }
            Program.VerboseAudit("----End of Locating Vital Signs: Filling buckets...Bucketsize=" + _bucket_size);
        }

        //private void AddMedBuckets(List<gBucket> bucket_list)
        //{
        //    string descript = "";
        //    string drugclass = "";
        //    string rte = "";
        //    string result = "";

        //    bool bres1 = false;
        //    bool bres2 = false;
        //    bool bclass1 = false;
        //    bool bclass2 = false;
        //    bool bclass3 = false;
        //    bool bmed1 = false;

        //    string[] medclass = { "7", "1", "2", "6" };

        //    string[] pamed1417 = { "Zofran", "Ondansetron" };

        //    var query = StartNewQuery(SearchDepth.SearchSince13Hrs);    // add custom time range below
        //    query = query.Where(e => e.CODE.ToLower().StartsWith("med"));
        //    //query = query.Where(e => e.DESCRIPTION.Contains(";;;7;;;") || 
        //    //                         e.DESCRIPTION.Contains(";;;33;;;") || 
        //    //                         e.DESCRIPTION.Contains(";;;1;;;") || 
        //    //                         e.DESCRIPTION.Contains(";;;2;;;") || 
        //    //                         e.DESCRIPTION.Contains(";;;6;;;") || 
        //    query = query.Where(e =>
        //        medclass.Any(item1 => e.DESCRIPTION.Contains(";;;" + item1 + ";;;")) ||
        //       pamed1417.Any(item2 => e.DESCRIPTION.ToUpper().StartsWith(item2.ToUpper())));
        //    query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;given") || e.DESCRIPTION.ToLower().Contains(";;;syringe") || e.DESCRIPTION.ToLower().ContainsAny(meds_rate) && (e.UNIT_ID == _pat.unit_id) || e.DESCRIPTION.ToLower().Contains(";;;new"));
        //    query = query.Where(e => e.EVENT_DATETIME <= PFSUtility.MaxDateTime(_pat.unit_departure, _pat.effective_out));
        //    query = query.OrderBy(e => e.EVENT_DATETIME);
        //    //            Program.VerboseAudit("Num Meds found: " + query.Count());
        //    foreach (var item in query)
        //    {
        //        var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
        //        if (arr.GetUpperBound(0) >= 3)
        //        {
        //            descript = arr[0];
        //            drugclass = arr[1];
        //            rte = arr[3];
        //            result = "";
        //            if (arr.Length == 5) result = arr[4];
        //        }
        //        //Program.VerboseAudit("med desc: " + descript + "//" + "med class: " + drugclass + "//" + "med result: " + result);
        //        //exclude #29, 35, 48
        //        bres1 = false;
        //        bres2 = false;
        //        bclass1 = false;
        //        bclass2 = false;
        //        bclass3 = false;
        //        bmed1 = false;

        //        bres1 = result.ToLower().Contains("given");
        //        bres2 = result.ToLower().Contains("syringe") || result.ToLower().ContainsAny(meds_rate) || result.ToLower().Contains("newbag") || result.ToLower().Contains("new bag");
        //        bclass1 = (drugclass == "1") || (drugclass == "2") || (drugclass == "6");
        //        bclass2 = (drugclass == "7");
        //        bclass3 = false; // (drugclass == "33") && (rte.ToLower() == "sc");
        //        bmed1 = pamed1417.Any(item2 => descript.ToUpper().StartsWith(item2.ToUpper()));
        //        if ((bres1 && (bclass2 || bclass3)) ||
        //            ((bres1 || bres2) && bclass1) ||
        //            bmed1)
        //        {
        //            Program.VerboseAudit("====Med found: bucket count A====");
        //            Program.VerboseAudit("med code: " + item.CODE + " desc: " + item.DESCRIPTION + "  evdt: " + item.EVENT_DATETIME.ToString());
        //            SetInd(15, "Med class 7,1,2,6 or named med");
        //        }
        //    }

        //    // get the chart items for the assessments
        //    query = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
        //    query = query.Where(e => e.CODE.ToLower().StartsWith("med"));
        //    //query = query.Where(e => e.DESCRIPTION.Contains(";;;7;;;") || e.DESCRIPTION.Contains(";;;33;;;") || e.DESCRIPTION.Contains(";;;1;;;") || e.DESCRIPTION.Contains(";;;2;;;") || e.DESCRIPTION.Contains(";;;6;;;"));
        //    query = query.Where(e =>
        //       medclass.Any(item1 => e.DESCRIPTION.Contains(";;;" + item1 + ";;;")) ||
        //       pamed1417.Any(item2 => e.DESCRIPTION.ToUpper().StartsWith(item2.ToUpper())));
        //    query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;given") || e.DESCRIPTION.ToLower().Contains(";;;syringe") || e.DESCRIPTION.ToLower().ContainsAny(meds_rate) && (e.UNIT_ID == _pat.unit_id) || e.DESCRIPTION.ToLower().Contains(";;;new"));
        //    query = query.Where(e => e.EVENT_DATETIME <= PFSUtility.MaxDateTime(_pat.unit_departure, _pat.effective_out));
        //    query = query.OrderBy(e => e.EVENT_DATETIME);
        //    //            Program.VerboseAudit("Num Meds found: " + query.Count());
        //    foreach (var item in query)
        //    {
        //        var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
        //        if (arr.GetUpperBound(0) >= 3)
        //        {
        //            descript = arr[0];
        //            drugclass = arr[1];
        //            rte = arr[3];
        //            result = "";
        //            if (arr.Length == 5) result = arr[4];
        //        }
        //        //Program.VerboseAudit("med desc: " + descript + "//" + "med class: " + drugclass + "//" + "med result: " + result);
        //        //exclude #29, 35, 48
        //        bres1 = false;
        //        bres2 = false;
        //        bclass1 = false;
        //        bclass2 = false;
        //        bclass3 = false;
        //        bmed1 = false;

        //        bres1 = result.ToLower().Contains("given");
        //        bres2 = result.ToLower().Contains("syringe") || result.ToLower().ContainsAny(meds_rate) || result.ToLower().Contains("newbag") || result.ToLower().Contains("new bag");
        //        bclass1 = (drugclass == "1") || (drugclass == "2") || (drugclass == "6");
        //        bclass2 = (drugclass == "7");
        //        bclass3 = false;  //(drugclass == "33") && (rte.ToLower() == "sc");
        //        bmed1 = pamed1417.Any(item2 => descript.ToUpper().StartsWith(item2.ToUpper()));
        //        if ((bres1 && (bclass2 || bclass3)) ||
        //            ((bres1 || bres2) && bclass1) ||
        //            bmed1)
        //        {
        //            Program.VerboseAudit("====Med found: bucket count B====");
        //            Program.VerboseAudit("med code: " + item.CODE + " desc: " + item.DESCRIPTION + "  evdt: " + item.EVENT_DATETIME.ToString());
        //            //var query2 = (from item2 in query select new { item2.EVENT_DATETIME, item2.CODE });
        //            //var query3 = from item3 in query2
        //            //             select new
        //            //             {
        //            //                 bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item3.EVENT_DATETIME) / _bucket_size),
        //            //                 code = item3.CODE,
        //            //                 evdt = item3.EVENT_DATETIME
        //            //             };
        //            //foreach (var itemx in query3)
        //            //{
        //            //    var b = new gBucket();
        //            //    b.bucket = itemx.bucket;
        //            //    b.code = itemx.code;
        //            //    b.evdt = itemx.evdt;
        //            //    bucket_list.Add(b);
        //            //}
        //            var b = new gBucket();
        //            b.bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size);
        //            b.code = item.CODE;
        //            b.evdt = item.EVENT_DATETIME;
        //            b.has_all_deps = true;
        //            gBucket f = bucket_list.Find(x => x.evdt == item.EVENT_DATETIME);
        //            if (f.evdt != item.EVENT_DATETIME) bucket_list.Add(b);
        //        }
        //    }
        //}

        //private void AddMedBucketsDrugClass33(List<gBucket> bucket_list)
        //{
        //    string descript = "";
        //    string drugclass = "";
        //    string rte = "";
        //    string result = "";

        //    bool bres1 = false;
        //    bool bres2 = false;
        //    bool bclass1 = false;
        //    bool bclass2 = false;
        //    bool bclass3 = false;
        //    bool bmed1 = false;

        //    var query = StartNewQuery(SearchDepth.SearchSince13Hrs);    // add custom time range below
        //    query = query.Where(e => e.CODE.ToLower().StartsWith("med"));
        //    //query = query.Where(e => e.DESCRIPTION.Contains(";;;7;;;") || 
        //    //                         e.DESCRIPTION.Contains(";;;33;;;") || 
        //    //                         e.DESCRIPTION.Contains(";;;1;;;") || 
        //    //                         e.DESCRIPTION.Contains(";;;2;;;") || 
        //    //                         e.DESCRIPTION.Contains(";;;6;;;") || 
        //    query = query.Where(e => e.DESCRIPTION.Contains(";;;33;;;"));
        //    query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;given") || e.DESCRIPTION.ToLower().Contains(";;;syringe") || e.DESCRIPTION.ToLower().ContainsAny(meds_rate) && (e.UNIT_ID == _pat.unit_id) || e.DESCRIPTION.ToLower().Contains(";;;new"));
        //    query = query.Where(e => e.EVENT_DATETIME <= PFSUtility.MaxDateTime(_pat.unit_departure, _pat.effective_out));
        //    query = query.OrderBy(e => e.EVENT_DATETIME);
        //    //            Program.VerboseAudit("Num Meds found: " + query.Count());
        //    foreach (var item in query)
        //    {
        //        var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
        //        if (arr.GetUpperBound(0) >= 3)
        //        {
        //            descript = arr[0];
        //            drugclass = arr[1];
        //            rte = arr[3];
        //            result = "";
        //            if (arr.Length == 5) result = arr[4];
        //        }
        //        //Program.VerboseAudit("med desc: " + descript + "//" + "med class: " + drugclass + "//" + "med result: " + result);
        //        //exclude #29, 35, 48
        //        bres1 = false;
        //        bres2 = false;
        //        bclass1 = false;
        //        bclass2 = false;
        //        bclass3 = false;
        //        bmed1 = false;

        //        bres1 = result.ToLower().Contains("given");
        //        bclass3 = (drugclass == "33") && (rte.ToLower() == "sc");
        //        if (bres1 && bclass3)
        //        {
        //            Program.VerboseAudit("====Med found: Drug class 33 with Route=SC ==direct trigger for q4==");
        //            Program.VerboseAudit("med code: " + item.CODE + " desc: " + item.DESCRIPTION + "  evdt: " + item.EVENT_DATETIME.ToString());
        //            SetInd(15, "Med class 33");
        //        }
        //    }

        //    // get the chart items for the assessments
        //    query = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
        //    query = query.Where(e => e.CODE.ToLower().StartsWith("med"));
        //    //query = query.Where(e => e.DESCRIPTION.Contains(";;;7;;;") || e.DESCRIPTION.Contains(";;;33;;;") || e.DESCRIPTION.Contains(";;;1;;;") || e.DESCRIPTION.Contains(";;;2;;;") || e.DESCRIPTION.Contains(";;;6;;;"));
        //    query = query.Where(e => e.DESCRIPTION.Contains(";;;33;;;"));
        //    query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;given") || e.DESCRIPTION.ToLower().Contains(";;;syringe") || e.DESCRIPTION.ToLower().ContainsAny(meds_rate) && (e.UNIT_ID == _pat.unit_id) || e.DESCRIPTION.ToLower().Contains(";;;new"));
        //    query = query.Where(e => e.EVENT_DATETIME <= PFSUtility.MaxDateTime(_pat.unit_departure, _pat.effective_out));
        //    query = query.OrderBy(e => e.EVENT_DATETIME);
        //    //            Program.VerboseAudit("Num Meds found: " + query.Count());
        //    foreach (var item in query)
        //    {
        //        var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
        //        if (arr.GetUpperBound(0) >= 3)
        //        {
        //            descript = arr[0];
        //            drugclass = arr[1];
        //            rte = arr[3];
        //            result = "";
        //            if (arr.Length == 5) result = arr[4];
        //        }
        //        //Program.VerboseAudit("med desc: " + descript + "//" + "med class: " + drugclass + "//" + "med result: " + result);
        //        //exclude #29, 35, 48
        //        bres1 = false;
        //        bres2 = false;
        //        bclass1 = false;
        //        bclass2 = false;
        //        bclass3 = false;
        //        bmed1 = false;

        //        bres1 = result.ToLower().Contains("given");
        //        bclass3 = (drugclass == "33") && (rte.ToLower() == "sc");
        //        if (bres1 && bclass3)
        //        {
        //            Program.VerboseAudit("====Med found: Drug class 33 with Route=SC ==adding to freq eval==");
        //            Program.VerboseAudit("med code: " + item.CODE + " desc: " + item.DESCRIPTION + "  evdt: " + item.EVENT_DATETIME.ToString());
        //            //var query2 = (from item2 in query select new { item2.EVENT_DATETIME, item2.CODE });
        //            //var query3 = from item3 in query2
        //            //             select new
        //            //             {
        //            //                 bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item3.EVENT_DATETIME) / _bucket_size),
        //            //                 code = item3.CODE,
        //            //                 evdt = item3.EVENT_DATETIME
        //            //             };
        //            //foreach (var itemx in query3)
        //            //{
        //            //    var b = new gBucket();
        //            //    b.bucket = itemx.bucket;
        //            //    b.code = itemx.code;
        //            //    b.evdt = itemx.evdt;
        //            //    bucket_list.Add(b);
        //            //}
        //            var b = new gBucket();
        //            b.bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size);
        //            b.code = item.CODE;
        //            b.evdt = item.EVENT_DATETIME;
        //            b.has_all_deps = true;
        //            gBucket f = bucket_list.Find(x => x.evdt == item.EVENT_DATETIME);
        //            if (f.evdt != item.EVENT_DATETIME) bucket_list.Add(b);
        //        }
        //    }
        //}


        private int CountBuckets(List<gBucket> bucket_list)       // bucket list: (ha ha)
        {
            //int result = bucket_list.Distinct().Count();
            //if (result > 0) Program.VerboseAudit(result + " unique");
            //return result;
            int x = -99;
            int result = 0;
            //int result = bucket_list.Distinct().Count();
            var query = from b in bucket_list
                        orderby b.bucket ascending
                        select b;
            foreach (var b in query)
            {
                if (x != b.bucket)
                {
                    result++;
                    x = b.bucket;
                }
            }
            if (result > 0) Program.VerboseAudit(result + " unique");
            return result;
        }
        private void Check_19()
        {
            string reslist;
            string codelist;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("19. Vascular Access Site Mgt q1 Hour");
            Program.VerboseAudit("---------------");

            SetBucketSize(60);
            exclude_periop_data = true;


            reslist = "every 1 hour";
            SetIndIfResultContains(19, "", "NURS Chemo/Bio Site Check", "", "", reslist);
            reslist = "Arterial, cordis, PA catheter, A Sheath, V Sheath";
            SetIndIfResultContains(19, "", "NURS VADCD Catheter Type", "", "", reslist);
            reslist = "every hour";
            SetIndIfResultContains(19, "", "NURS VADP Site Check,NURS VADPIV Site Check,NURS VADC Site Check,NURS VADCT Site Check", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(19, "", "NURSI PAP Systolic", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(19, "", "NURSI ABP Systolic                                         NURSI ABP Diastolic", "", "", reslist);
            reslist = "SCUF, CVVH, CVVHDF";
            SetIndIfResultContains(19, "", "NURSI CRRT Mode", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(19, "", "NURSI Site Access", "", "", reslist);
        }


        private void CheckRouteCath()
        {
            int ct = 0;
            //OBX|1|DT|MED9003 ^ ALTEPLASE 50 MG INTRAVENOUS SOLUTION;;;20;;;0.5;;;cath; ; ; RateVerify | RateVerify | 20190802000000 |||||| F ||| 20190802000000
            //OBX|1|DT|MED9003 ^ HEPARIN  MG INTRAVENOUS SOLUTION;;;20;;;0.5;;;cath; ; ; RateVerify | RateVerify | 20190802000000 |||||| F ||| 20190802000000
            var query = StartNewQuery(SearchDepth.SearchSince9Hrs);    // add custom time range below
            query = query.Where(e => e.CODE.StartsWith("med"));
            query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;cath"));
            query = query.Where(e => e.DESCRIPTION.ToUpper().Contains("ALTEPLASE") ||
                                     e.DESCRIPTION.ToUpper().Contains("HEPARIN"));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            Program.VerboseAudit("Med ALTEPLASE or HEPARIN with cath found: " + query.Count());
            foreach (var item in query)
            {
                Program.VerboseAudit("====Med found: ALTEPLASE or HEPARIN====");
                Program.VerboseAudit("med code: " + item.CODE + " desc: " + item.DESCRIPTION + "  evdt: " + item.EVENT_DATETIME.ToString());

                var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                if (arr.GetUpperBound(0) >= 3)
                {
                    string rte = arr[3];
                    if (rte.ToLower() == "cath") ct++;
                }
            }
            if (ct > 0)
                SetInd(19, "Found ALTEPLASE or HEPARIN with route cath.");
        }

        private void Check_20()
        {
            string reslist;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("20. Medication Activity >= 20 minutes");
            Program.VerboseAudit("---------------");

            //Look back 24 hours.
            SearchDepth s = SearchDepth.SearchDefault; // change to default from look back of 24 hrs.

            reslist = "Pre transfusion";
            SetIndIfResultContains(20, "", "NURS Vital Sign Reason", "", "", reslist, s);
            reslist = "Pre transfusion, +15 minutes of transfusion, During trasnfusion, Post transfusion, Transfusion";
            SetIndIfResultContains(20, "", "NURS Vital Sign Reason", "", "", reslist, s);
            reslist = "Medications given prior to feeding";
            SetIndIfResultContains(20, "", "NURS Diet Interventions", "", "", reslist, s);
            reslist = "";
            SetIndIfResultContains(20, "", "NURS Chemo Pre-Administration Prep", "", "", reslist, s);
            reslist = "Continuous analgesic medication started, Continuous analgesic infusion increased, Continuous analgesic infusion decreased ";
            SetIndIfResultContains(20, "", "DTM Cell Blood", "", "", reslist, s);
            reslist = "";
            SetIndIfResultContains(20, "", "DTM Blood Component", "", "", reslist, s);
            reslist = "";
            SetIndIfResultContains(20, "", "DTM Cell Therapy Product", "", "", reslist, s);
            reslist = "";
            SetIndIfResultContains(20, "", "NURS Cell Products", "", "", reslist, s);
            reslist = "";
            SetIndIfResultContains(20, "", "NURS_PROC DI type of Medicatin Infused", "", "", reslist, s);
            reslist = "Albumin 5, Albumin 25";
            SetIndIfResultContains(20, "", "io_intake_colloid_albumin_25; io_intake_colloid_albumin_5", "", "", reslist, s);
            reslist = "Pre transfusion, +15 minutes of transfusion, Post transfusion";
            SetIndIfResultContains(20, "", "NURSI Vital Sign Reason", "", "", reslist, s);
            reslist = "4,3,2,1";
            SetIndIfResultContains(20, "", "NURSI RASS", "", "", reslist, s);
            reslist = "";
            SetIndIfResultContains(20, "", "io_intake_drip_ped", "", "", reslist, s);
            reslist = "";
            SetIndIfResultContains(20, "", "io_intake_drip_adlt", "", "", reslist, s);
            reslist = "";
            SetIndIfResultContains(20, "", "DTM Transfusion Reaction Symptoms", "", "", reslist, s);
            reslist = "Started, Primed with heparin,reprimed with saline,Primed with saline only";
            SetIndIfResultContains(20, "", "NURSI CRRTStatus", "", "", reslist, s);

        }


        private void CheckMedGiven(int ind, string desc)
        {

            string[] transplantmed_actions = { "given", "newbag", "new bag", "rateverify", "rate verify", "rate change", "ratechange", "restarted", "started/down" };

            for (int i = 0; i <= transplantmed_actions.GetUpperBound(0); i++)
            {
                transplantmed_actions[i] = ";;;" + transplantmed_actions[i];
            }

            int ct = 0;
            var query = StartNewQuery(SearchDepth.SearchSince9Hrs);
            query = AndItemFilter(query, "", "MED", desc, "", "");
            query = query.Where(e => e.DESCRIPTION.ToLower().ContainsAny(transplantmed_actions));
            ct = query.Count();
            if (ct > 0) SetInd(ind, "Found Med=" + desc);

        }

        private int CountMedsIn30min()
        {
            string descript = "";
            string drugclass = "";
            string result = "";
            int ct = 0;
            DateTime evdt = DateTime.MinValue;
            DateTime endevdt = DateTime.MinValue;

            //  CALCIUM CHLORIDE 100 MG / ML(10 %) INTRAVENOUS SYRINGE; ; ; 29; ; ; 1; ; ; IV; ; ; Given
            //  SODIUM BICARBONATE 8.4 % (1 MEQ / ML) INJECTION(WRAPPER); ; ; 29; ; ; 50; ; ; osse; ; ; Given
            var query = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
            query = query.Where(e => e.CODE.ToUpper().StartsWith("MED"));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            //Program.VerboseAudit("Num Meds found: " + query.Count());
            foreach (var item in query)
            {
                Program.VerboseAudit("Med=" + item.EVENT_DATETIME + ": " + item.DESCRIPTION);
                if (item.EVENT_DATETIME > evdt)
                {
                    evdt = item.EVENT_DATETIME;
                    endevdt = evdt.AddMinutes(30);
                    if (endevdt > loc_out) endevdt = loc_out;
                    var query2 = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
                    query2 = query2.Where(e => e.CODE.ToUpper().StartsWith("MED"));
                    query2 = query2.Where(e => e.EVENT_DATETIME >= evdt && e.EVENT_DATETIME <= endevdt);
                    if (query2.Count() > ct)
                        ct = query2.Count();
                }
            }

            Program.VerboseAudit("Greatest count of unique Admin times in a 30 minute period=" + ct);
            return ct;
        }
        private int CountIntravenous()
        {
            int ct = 0;
            int count = 0;
            DateTime evdt = DateTime.MinValue;
            DateTime endevdt = DateTime.MinValue;

            var query = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
            query = query.Where(e => e.CODE.ToUpper().StartsWith("MED"));
            query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;intravenous"));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            //Program.VerboseAudit("Num Meds found: " + query.Count());
            foreach (var item in query)
            {
                Program.VerboseAudit("IV Med=" + item.EVENT_DATETIME + ": " + item.DESCRIPTION);
                if (item.EVENT_DATETIME > evdt)
                {
                    evdt = item.EVENT_DATETIME;
                    endevdt = evdt.AddHours(8);
                    if (endevdt > loc_out) endevdt = loc_out;
                    var query2 = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
                    query2 = query2.Where(e => e.CODE.ToUpper().StartsWith("MED"));
                    query2 = query2.Where(e => e.DESCRIPTION.ToLower().Contains(";;intravenous"));
                    query2 = query2.Where(e => e.EVENT_DATETIME >= evdt && e.EVENT_DATETIME <= endevdt);
                    count = query2.Count();
                    if (count > ct)
                        ct = count;
                }
            }

            Program.VerboseAudit("Greatest count of IV Admin times in an 8-hour period=" + ct);
            return ct;
        }


        private void Check_21_22()
        {
            string reslist;
            bool st1 = false;
            string piv;
            string codelist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("21. Wound/Injury Mgmt");
            Program.VerboseAudit("22. Wound/Injury Mgmt >= 30 Minutes");
            Program.VerboseAudit("---------------");


            reslist = "";
            SetIndIfResultContains(21, "", "NURS Incision", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURS Incision Drainage", "", "", reslist);
            reslist = "Foam, Gauze, Hydrogel, Hydrocolloid, Non-stick gauze, Transparent film, Wet to moist, Silicone, Alginate";
            SetIndIfResultContains(21, "", "NURS Incision", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURS Incision Drain Type", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURS Incision Drain", "", "", reslist);
            reslist = "Malodorous";
            SetIndIfResultContains(21, "", "NURS Incision Drain Drainage", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURS Wound", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURS Wound Drainage", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURS Pressure Ulcer", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURS Pressure Ulcer Drainage", "", "", reslist);
            reslist = "wound(s) present";
            SetIndIfResultContains(21, "", "NURS Skin Pressure Ulcer Eval", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURS Skin Pressure Ulcer Location", "", "", reslist);
            reslist = "Dressing Change Completed";
            SetIndIfResultContains(21, "", "NURS VADCT Site Care", "", "", reslist);
            reslist = "Dressing Change Completed";
            SetIndIfResultContains(21, "", "NURS VADCNT Site Care", "", "", reslist);
            reslist = "Dressing Change Completed";
            SetIndIfResultContains(21, "", "NURS VADCP Site Care", "", "", reslist);
            reslist = "Dressing Change Completed";
            SetIndIfResultContains(21, "", "NURS VADCD Site Care", "", "", reslist);
            reslist = "Dressing Change Completed";
            SetIndIfResultContains(21, "", "NURS VADC Site Care", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURS Pain CAP Insertion Site", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURS GU Stoma Type", "", "", reslist);
            reslist = "Frank blood, tarry stool";
            SetIndIfResultContains(21, "", "NURS Stool Color/Consist/Odor", "", "", reslist);
            reslist = "Active bleeding";
            SetIndIfResultContains(21, "", "NURS Circulation Bleeding Obs Status", "", "", reslist);
            reslist = "fissured";
            SetIndIfResultContains(21, "", "NURS Skin Characteristics", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURS Respiratory Tracheostomy Tube Type", "", "", reslist);
            reslist = "Dressing changed, Stoma cleaned, Inner cannula changed, Inner cannula cleaned, Trach ties changed";
            SetIndIfResultContains(21, "", "NURS Tracheostomy Care", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURS Respiratory Tracheostomy Tube Type", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURS PD Cath Exit Site Appearance", "", "", reslist);
            reslist = "Nephrostomy tube, Suprapubic catheter, Ureteral stent";
            SetIndIfResultContains(21, "", "NURS Urine Drain Type", "", "", reslist);
            reslist = "Colostomy, Ileostomy, Mucous fistula";
            SetIndIfResultContains(21, "", "", "", "", reslist);
            reslist = "G-tube, PEG, J-tube, G-J tube";
            SetIndIfResultContains(21, "", "NURS GI Tube Type", "", "", reslist);
            reslist = "Lip hydration, Oral hydration, Mouth Care";
            SetIndIfResultContains(21, "", "NURS Oral Care", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI CBI Color of Drainage", "", "", reslist);
            reslist = "15mins";
            SetIndIfResultContains(21, "", "NURS_WOCN Time Spent", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "io_output_dr_lumbar", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(22, "", "NURS Chest Tube Type", "", "", reslist);
            reslist = "Bladder irrigation";
            SetIndIfResultContains(21, "", "io_output_net_bladder_irrigation", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX I Location", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX I Sensation", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX I Appearance", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX I Length", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX I Width", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX I Depth", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX I Dressing", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX I Closure", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX I Drainage", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX I Care", "", "", reslist);
            reslist = "Foam, Gauze, Hydrogel, Hydrocolloid, Non-stick gauze, Transparent film, Wet to dry, Wet to moist, Silicone, Alginate";
            SetIndIfResultContains(21, "", "NURSI TX I Drsg Type", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX I Neg Pressure Wound Therapy Setting", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX I Drain Type of Drain/Suction", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX I Drain Intervention", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX I Drain Odor", "", "", reslist);
            reslist = "free text";
            SetIndIfResultContains(21, "", "NURSI TX P Location", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX P Sensation", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX P Wound Bed", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX P Length", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX P Width", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX P Depth", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX P Ulcer Staging/Description", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX P Peri-wound", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX P Dressing", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX P Drainage", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX P Care", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX P Drsg Type", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX P Neg Pressure Wound Therapy Setting", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX W Location", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX W Sensation", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX W Bed", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX W Length", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX W Widtth", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX W Depth", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX W Peri-wound", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX W Dressing", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX W Closure", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX W Drainage", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX W Care", "", "", reslist);
            reslist = "Foam, Gauze, Hydrogel, Hydrocolloid, Non-stick gauze, Transparent film, Wet to dry, Wet to moist, Silicone, Alginate";
            SetIndIfResultContains(21, "", "NURSI TX W Drsg Type", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX W Neg Pressure Wound Therapy Setting", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX W Drain Type of Drain/Suction", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX W Drain Intervention", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX W Drain Odor", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI CBI Color of Drainage", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI GU Stoma Type", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI GU Stoma Care", "", "", reslist);
            reslist = "Dressing changed, site cleansed";
            SetIndIfResultContains(21, "", "NURSI Urine Drain Interventiosns", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI GI Stoma Type", "", "", reslist);
            reslist = "G-tube, PEG, J-tube, G-J tube";
            SetIndIfResultContains(21, "", "NURSI GI Tube Type", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI Tracheostomy Tube Type", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI Tracheostomy Care", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX I Care", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX W Drain Type of Drain/Suction", "", "", reslist);
            reslist = "rash, cracked, fissured, excoriated, wet desquamation, dry desquamation";
            SetIndIfResultContains(21, "", "NURSI TX Skin Characteristics", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX Drain Intervention", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX I Drain Type of Drain/Suction", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI Site Access", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI TX P Care", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI Chest Tube Dressing", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI Epidural Dressing", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI Epidural Dressing", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", " NURSI Tracheostomy Care", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "NURSI Lumbar Dressing                                        NURSI Ventric Dressing                                           NURSI EEG Electrode Types                                ", "", "", reslist);
            reslist = "gross blood, tarry stool";
            SetIndIfResultContains(21, "", "NURSI Bowel Stool Color/Consistency", "", "", reslist);

            reslist = "Greater than 30 minutes";
            SetIndIfResultContains(22, "", "NURS Wound Care Total", "", "", reslist);

            reslist = "";
            if (Exists("", "NURS VADCNT Catheter Type", "", "", reslist))
                SetIndIfResultContains(22, "", "NURS VADCNT Catheter Removal Date", "", "", reslist);
            if (Exists("", "NURS VADCD Catheter Type", "", "", reslist))
                SetIndIfResultContains(22, "", "NURS VADCD Catheter Removal Date", "", "", reslist);
            if (Exists("", "NURS VADC Catheter Type", "", "", reslist))
                SetIndIfResultContains(22, "", "NURS VADC Catheter Removal Date", "", "", reslist);

            reslist = "15 mins";
            SetIndIfResultDoesNotContain(22, "", "NURS_WOCN Time Spent", "", "", reslist);
            reslist = "Wound Irrigation";
            SetIndIfResultContains(22, "", "io_output_net_wound_irrigation", "", "", reslist);
            reslist = "Greater than 30 minutes";
            SetIndIfResultContains(22, "", "NURSI Wound Care Total", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(22, "", "NURSI Site Pressure", "", "", reslist);

        }

        private void CheckDrainLDA()
        {
            string codelist = "9993040108530,3045001091";
            DateTime dt = DateTime.MinValue;
            var query1 = StartNewQuery(SearchDepth.SearchSince16Hrs);
            query1 = AndItemFilter(query1, "", codelist, "", "", "");
            query1 = query1.OrderBy(e => e.EVENT_DATETIME);
            foreach (var item in query1)
            { // for each start, find the number of records within 1 hr of it.
                if (dt == DateTime.MinValue)
                {
                    int ct = CountDrainLDAIn1Hour(codelist, item.EVENT_DATETIME);
                    if (ct >= 6)
                    {
                        SetInd(22, "Count of Drain LDAs within 1 hour=" + ct + " starting at: " + item.EVENT_DATETIME.ToString());
                        dt = item.EVENT_DATETIME;
                    }
                }
            }
            return;
        }

        private int CountDrainLDAIn1Hour(string codelist, DateTime startdt)
        {
            int ct = 0;
            var query1 = StartNewQuery(SearchDepth.SearchSince16Hrs);
            query1 = AndItemFilter(query1, "", codelist, "", "", "");
            query1 = query1.Where(e => e.EVENT_DATETIME >= startdt);
            query1 = query1.Where(e => e.EVENT_DATETIME <= startdt.AddMinutes(60));
            ct = query1.Count();
            return ct;

        }

        int EducMins(string timecode, string nursecode)
        {
            string res;
            int val = 0;
            if (ResultContains("", nursecode, "", "", "Nursing"))
            {
                if (GetResult("", timecode, "", "", out res))
                {
                    if (res.Left(1).IsNumeric())
                    {
                        val += (int)res.Val();
                    }
                }
            }
            return val;
        }

        private void Check_23()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("23. Healthcare Mgmt Education >= 1 Hour");
            Program.VerboseAudit("---------------");

            string reslist = "Greater than 1 hour";
            SetIndIfResultContains(23, "", "NIH Patient Ed Instruction Duration", "", "", reslist);
        }
        private void AddEducActivity(DateTime evdt)
        {
            DateTime enddt;
            Program.VerboseAudit("Activity 5: Found at " + evdt.AddHours(-1).ToString());
            if (!QueuedProcOverlaps(5, evdt.AddHours(-1), evdt))
                if (!ProcExistsInDB(5, evdt.AddHours(-1), out enddt))
                {
                    var proc = new proc_data();
                    proc.procedure_number = 5;
                    proc.start = evdt.AddHours(-1);
                    proc.finish = evdt;
                    _procs.Add(proc);
                    Program.Audit("Activity 5: Found at " + evdt);
                }
        }

        private void Check_24()
        {
            string reslist = "";
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("24. 1 to 1 Physiological Interv. >= 2 Hours");
            Program.VerboseAudit("---------------");


            reslist = "";
            SetIndIfResultContains(24, "", "DTM Blood Component", "", "", reslist);
            reslist = "Yes";
            SetIndIfResultContains(24, "", "NURS_AC Intervention", "", "", reslist);

            string desc_found = "";
            if (g_iabp)
                SetInd(24, "IABP order in effect");
            if (OrderInProgress("NSG_IabpMode", out desc_found))
                SetInd(24, desc_found);

        }


        private void CheckUserDefined()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("User-Defined indicators");
            Program.VerboseAudit("---------------");

            //reslist = "Patient arrived during downtime,Patient was cared for during downtime,Patient departed during downtime";
            //SetIndIfResultContains(99, "", "9991600100203", "", "", reslist);
            //reslist = "Yes";
            //SetIndIfResultContains(99, "", "9990000006437", "", "", reslist);
            //reslist = "Green,Yellow ,Red ,Black ";
            //SetIndIfResultContains(99, "", "9991600100265", "", "", reslist);
            //reslist = "Downtime";
            //SetIndIfResultContains(99, "", "9990007096330", "", "", reslist);

        }


        //=====================================================================
        // Group like assessments into buckets of time
        // Each bucket counts as one assessment in the frequency count

        //private void AddDependentBuckets(List<gBucket> bucket_list, string codelist1, string reslist1, string codelist2, string reslist2)
        //{
        //    AddDependentBuckets(bucket_list, codelist1, reslist1, codelist2, reslist2, "", "");
        //}

        //private void AddDependentBuckets(List<gBucket> bucket_list, string codelist1, string reslist1, string codelist2, string reslist2, string codelist3,string reslist3)
        //{
        //    bool dep3 = true;
        //    // get the chart items for the assessments
        //    var query1 = StartNewQuery();
        //    query1 = AndItemFilter(query1, "", codelist1, "", "", reslist1);
        //    var query2 = StartNewQuery();
        //    query2 = AndItemFilter(query2, "", codelist2, "", "", reslist2);
        //    if (codelist3.Trim() == "")
        //    {
        //        dep3 = false;
        //        codelist3 = "Hello, this is a phantom code";
        //    }
        //    var query3 = StartNewQuery();
        //    query3 = AndItemFilter(query3, "", codelist3, "", "", reslist3);

        //    // figure out what buckets the events belong to
        //    var query1a = from item in query1
        //                  select new
        //                  {
        //                      bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
        //                      code = item.CODE,
        //                      evdt = item.EVENT_DATETIME
        //                  };
        //    var query2a = from item in query2
        //                  select new
        //                  {
        //                      bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
        //                      code = item.CODE,
        //                      evdt = item.EVENT_DATETIME
        //                  };
        //    var query3a = from item in query3
        //                      select new
        //                      {
        //                          bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
        //                          code = item.CODE,
        //                          evdt = item.EVENT_DATETIME
        //                      };

        //    string s = "BucketList1 for " + codelist1 + ": ";
        //    foreach (var item in query1a)
        //    {
        //        s += item.bucket + "";
        //    }
        //    Program.VerboseAudit(s);

        //    s = "BucketList2 for " + codelist2 + ": ";
        //    foreach (var item in query2a)
        //    {
        //        s += item.bucket + "";
        //    }
        //    if (dep3)
        //    {
        //        s = "BucketList3 for " + codelist3 + ": ";
        //        foreach (var item in query3a)
        //        {
        //            s += item.bucket + "";
        //        }
        //    }
        //    Program.VerboseAudit(s);
        //    // Add to the list IFF items in both lists occur in same bucket
        //    foreach (var item1 in query1a)
        //    {
        //        foreach (var item2 in query2a)
        //        {
        //            if (item1.bucket == item2.bucket)
        //            {
        //                if (dep3)
        //                {
        //                    foreach (var item3 in query3a)
        //                    {
        //                        if (item1.bucket == item3.bucket)
        //                        {
        //                            var b = new gBucket();
        //                            b.bucket = item1.bucket;
        //                            b.code = item1.code;
        //                            b.evdt = item1.evdt;
        //                            bucket_list.Add(b);
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    foreach (var item3 in query2a)
        //                    {
        //                        if (item1.bucket == item2.bucket)
        //                        {
        //                            var b = new gBucket();
        //                            b.bucket = item1.bucket;
        //                            b.code = item1.code;
        //                            b.evdt = item1.evdt;
        //                            bucket_list.Add(b);
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }

        //}

        private void AddSimpleProc(int pnum, DateTime evdt, DateTime enddt)
        {
            if (pnum <= 0) return;

            if (ProcExists(pnum, evdt, enddt))
            {
                Program.Audit("Activity " + pnum + ": already exists");
            }
            else
            {
                //if (ActivityFits(evdt, enddt))
                {
                    ProcOverlapsInDB_PEID(pnum, evdt, enddt); // then delete the db
                    var proc = new proc_data();
                    proc.procedure_number = pnum;
                    proc.start = evdt;
                    proc.finish = enddt;
                    _procs.Add(proc);
                    Program.Audit("Activity " + pnum + ": Found between " + evdt + " and " + enddt);
                }
            }
        }

        private bool ActivityFits(DateTime beg, DateTime fin)
        {
            bool ok = false;
            int unit_id = 0;
            string sql = "select el.unit_id from ENCOUNTER_LOCATION as el";
            sql += " where el.ENCOUNTER_ID=" + _pat.encounter_id;
            //sql += " and el.SPECIAL_UNIT_ID is null";
            sql += " and el.EFFECTIVE_DATETIME_IN<=" + beg.ToString() + "";
            sql += " and el.EFFECTIVE_DATETIME_OUT>=" + fin.ToString() + "";

            Program.VerboseAudit(sql);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd = new SqlCommand(sql, db2);
            SqlDataReader dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr2.Read())
            {
                if (dr2["UNIT_ID"] != DBNull.Value)
                    unit_id = PFSDBUtility.DBToInt(dr2["UNIT_ID"]);
            }
            ok = (unit_id > 0);

            if (ok)
            {
                db2.Close();
                return ok;
            }
            //Now check if two adjacent same units contains the activity.
            int unitid1 = 0;
            int unitid2 = 0;
            sql = "select el.unit_id from ENCOUNTER_LOCATION as el";
            sql += " where el.ENCOUNTER_ID=" + _pat.encounter_id;
            sql += " and " + beg.ToString() + "' between el.EFFECTIVE_DATETIME_IN and el.EFFECTIVE_DATETIME_OUT";
            cmd = new SqlCommand(sql, db2);
            dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr2.Read())
            {
                if (dr2["UNIT_ID"] != DBNull.Value)
                    unitid1 = PFSDBUtility.DBToInt(dr2["UNIT_ID"]);
            }
            sql = "select el.unit_id from ENCOUNTER_LOCATION as el";
            sql += " where el.ENCOUNTER_ID=" + _pat.encounter_id;
            sql += " and " + fin.ToString() + "' between el.EFFECTIVE_DATETIME_IN and el.EFFECTIVE_DATETIME_OUT";
            cmd = new SqlCommand(sql, db2);
            dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr2.Read())
            {
                if (dr2["UNIT_ID"] != DBNull.Value)
                    unitid2 = PFSDBUtility.DBToInt(dr2["UNIT_ID"]);
            }
            ok = (unitid1 > 0 && unitid1 == unitid2);
            //db2.Close();
            return ok;


        }

        //6	2019-09-18 15:00:00.000	2019-09-18 19:00:00.000
        //6	2019-09-19 07:00:00.000	2019-09-19 11:00:00.000

        private bool ActivityDuringClassType6(DateTime beg, DateTime fin, out DateTime classout)
        {
            bool ok = false;
            int pt = 0;
            classout = DateTime.MinValue;
            string sql = "select ce.PATIENT_TYPE,ce.effective_datetime_in,ce.effective_datetime_out from CLASSIFICATION_EVENT as ce";
            sql += " where ce.ENCOUNTER_ID=" + _pat.encounter_id;
            sql += " and ce.PATIENT_TYPE=6";
            sql += " and (" + beg.ToString() + "' >=ce.EFFECTIVE_DATETIME_IN and " + beg.ToString() + "' <ce.EFFECTIVE_DATETIME_OUT";
            sql += " or " + beg.ToString() + "' <ce.EFFECTIVE_DATETIME_IN and " + fin.ToString() + "' >ce.EFFECTIVE_DATETIME_OUT";
            sql += " or " + fin.ToString() + "' >ce.EFFECTIVE_DATETIME_IN and " + fin.ToString() + "' <=ce.EFFECTIVE_DATETIME_OUT)";
            Program.VerboseAudit(sql);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd = new SqlCommand(sql, db2);
            SqlDataReader dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr2.Read())
            {
                if (dr2["PATIENT_TYPE"] != DBNull.Value)
                {
                    pt = PFSDBUtility.DBToInt(dr2["PATIENT_TYPE"]);
                    classout = PFSDBUtility.DBToDateTime(dr2["EFFECTIVE_DATETIME_OUT"]);
                }
            }
            ok = (pt == 6);
            db2.Close();
            return ok;
        }

        private void AtLeastOneADL()
        {
            Program.Audit("---------------");

            if (!(_inds[2].is_checked || _inds[3].is_checked || _inds[4].is_checked)) {
                // Make "#2 ADL - Assist" the default.  (90% of patients)
                SetInd(1, "Defaulting to ADL Self due to lack of documentation.");
            }
        }

        private void HighestIndicatorInEachGroupWins()
        {
            int i, g;
            bool highest_is_on;
            string ind_list;

            Program.Audit("Select highest indicator in each group");

            g = 0;
            highest_is_on = false;
            // Go from bottom up - highest (lowest) indicator in each group wins
            for (i = MAX_INDS; (i >= 1); i--) {
                if (_inds[i].radio_group > 0) {
                    if (_inds[i].radio_group != g) {
                        //this is a new group
                        g = _inds[i].radio_group;
                        highest_is_on = _inds[i].is_checked;
                    } else {
                        //same group
                        if (highest_is_on) {
                            _inds[i].is_checked = false;             //uncheck a lower number
                        } else {
                            highest_is_on = _inds[i].is_checked;     //save this one
                        }
                    }
                }
            } // next i

            //Echo the indicators for an Audit (no classification will be saved)
            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++) {
                if (_inds[i].is_checked) ind_list += "," + i;
            } // next i

            Program.Audit("Final list = " + ind_list.Substring(1));
            Program.Audit("---------------");
        }


        private void CheckProcs()
        {
            CheckProc_1();
            CheckProc_2();
            CheckProc_3();
            CheckProc_4();
            CheckProc_5();
            CheckProc_6();
            CheckProc_7();
            CheckProc_8();
            CheckProc_9();
            CheckProc_10();
            CheckProc_11();

        }

        //private void DoProc(int pnum, string code)
        //{
        //    double mins = 0;
        //    string found_what;
        //    DateTime evdt;
        //    DateTime enddt = DateTime.MinValue;

        //    if (GetResultAndEVDT("", code, "", "", out found_what, out evdt))
        //    {
        //        //mins = 60.0 * found_what.ToDouble();
        //        if (found_what.Contains("180")) mins = 180;
        //        else if (found_what.Contains("120")) mins = 120;
        //        else if (found_what.Contains("90")) mins = 90;
        //        else if (found_what.Contains("60")) mins = 60;
        //        enddt = evdt.AddMinutes(mins);

        //        if (ProcExistsInDB(pnum, evdt, enddt))
        //        {
        //            Program.Audit("Activity " + pnum+ ": already exists");
        //        }
        //        else
        //        {
        //            if (!QueuedProcOverlaps(pnum, evdt, enddt))
        //            {
        //                var proc = new proc_data();
        //                proc.procedure_number = pnum;
        //                proc.start = evdt;
        //                proc.finish = enddt;
        //                _procs.Add(proc);
        //                Program.Audit("Activity " + pnum + ": Found " + code + " between " + evdt + " and " + enddt);
        //            }
        //        }

        //    }

        //}

        private bool ProcOverlapsInDB_PEID(int pnum, DateTime startdt, DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            //            LoadPatientProceduresIfNeeded();
            bool overlap_exists = false;
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && ((proc.PROCEDURE_DATETIME >= startdt) && (proc.PROCEDURE_DATETIME < enddt)
                                ||
                                (proc.DEPARTURE_DATETIME > startdt) && (proc.DEPARTURE_DATETIME <= enddt)
                                ||
                                (proc.PROCEDURE_DATETIME < startdt) && (proc.DEPARTURE_DATETIME > enddt)
                                )
                            //&& ( ! (proc.PROCEDURE_DATETIME == startdt) && (proc.DEPARTURE_DATETIME == enddt))
                            && (proc.CLASSIFIED_BY_ID < 0)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new { proc.PROCEDURE_EVENT_ID, proc.PROCEDURE_DATETIME, proc.DEPARTURE_DATETIME };
            overlap_exists = (query.Count() > 0);
            foreach (var a in query)
            {
                Program.VerboseAudit("Will Delete act: procdt=" + a.PROCEDURE_DATETIME.ToString() + "  depdt=" + a.DEPARTURE_DATETIME.ToString());
                Program.VerboseAudit("because it overlays startdt=" + startdt.ToString() + "  enddt=" + enddt.ToString());
                DeleteActivity(a.PROCEDURE_EVENT_ID);
            }
            //            peid = 0;
            return (overlap_exists);
        }
        private void DeleteActivity(int peid)
        {
            //            delete PROCEDURE_ANSWER where PROCEDURE_EVENT_ID=7211351
            //delete RPT_PROC_BY_DAY where PROCEDURE_EVENT_ID=7211351
            //delete PROCEDURE_EVENT where PROCEDURE_EVENT_ID=7211351
            if (peid == 0) return;

            Program.VerboseAudit("db ProcAnsw Deleting peid=" + peid);
            var db = PFSDBUtility.NewPfsDataContext();
            var query = from ia in db.PROCEDURE_ANSWERs
                        where (ia.PROCEDURE_EVENT_ID == peid)
                        select ia;
            if (query.Count() > 0)
            {
                var items = query.ToList();
                foreach (var item in items)
                    db.PROCEDURE_ANSWERs.DeleteOnSubmit(item);
                db.SubmitChanges();
            }

            Program.VerboseAudit("db RptProc Deleting peid=" + peid);
            var db2 = PFSDBUtility.NewPfsDataContext();
            var query2 = from r in db2.RPT_PROC_BY_DAYs
                         where (r.PROCEDURE_EVENT_ID == peid)
                         select r;
            if (query2.Count() > 0)
            {
                var items2 = query2.ToList();
                foreach (var item2 in items2)
                    db2.RPT_PROC_BY_DAYs.DeleteOnSubmit(item2);
                db2.SubmitChanges();
            }

            Program.VerboseAudit("db ProcEvent Deleting peid=" + peid);
            var db3 = PFSDBUtility.NewPfsDataContext();
            var query3 = from ce in db3.PROCEDURE_EVENTs
                         where (ce.PROCEDURE_EVENT_ID == peid)
                         select ce;
            if (query3.Count() > 0)
            {
                var items3 = query3.ToList();
                foreach (var item3 in items3)
                    db3.PROCEDURE_EVENTs.DeleteOnSubmit(item3);
                db3.SubmitChanges();
            }
        }



        private bool QueuedProcOverlaps(int pnum, DateTime startdt, DateTime enddt)
        {
            bool overlap = false;

            Program.VerboseAudit("Activity " + pnum + ": Check for dup at " + startdt.ToString() + " - " + enddt.ToString());
            proc_data[] pary = _procs.ToArray();
            for (int i = 0; i <= pary.GetUpperBound(0); i++)
            {
                if (pary[i].procedure_number == pnum)
                {
                    //overlap if   p.start between stardt and enddt
                    //             p.finish between startdt and enddt
                    //             startdt >= p.start and enddt <= p.finish
                    if ((pary[i].start >= startdt) && (pary[i].start < enddt) ||
                        (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
                        (startdt >= pary[i].start) && (enddt <= pary[i].finish))
                    {
                        overlap = true;
                    }

                    if ((pary[i].start >= startdt) && (pary[i].start < enddt) && (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
                        (pary[i].start < startdt) && (pary[i].finish > startdt) && (pary[i].finish < enddt)
                        )
                    {
                        // the proc in the list is completely contained in startdt/endt pair
                        // change this proc to be the startdt/enddt
                        // overlap is still true, but the list item will be updated with the encompassing times
                        //Program.Audit("Procedure " + pnum + ": " + startdt + " - " + enddt + " will trump " + pary[i].start + " - " + pary[i].finish);
                        pary[i].start = startdt;
                        pary[i].finish = enddt;
                    }
                }
            }
            _procs = pary.ToList();
            Program.VerboseAudit("Activity " + pnum + ": Check for dup returns " + overlap);
            return overlap;
        }

        private bool ProcExistsInDB(int pnum, DateTime startdt, out DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            Program.VerboseAudit("ProcExistsInDB: pnum=" + pnum + " startdt=" + startdt.ToString());
            int ct = 0;
            enddt = DateTime.MinValue;
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            //&& (proc.PROCEDURE_DATETIME <= startdt)
                            && (proc.DEPARTURE_DATETIME > startdt)
                            && (((pnum > 2) && (ans.PROCEDURE_NUMBER == pnum)) || ((pnum <= 2) && (ans.PROCEDURE_NUMBER <= 2)))
                        orderby proc.DEPARTURE_DATETIME descending
                        select new { proc.DEPARTURE_DATETIME };
            ct = query.Count();
            //Program.VerboseAudit("ProcExistsInDB: ct=" + ct);
            //if (ct > 0)
            //{
            //    foreach (var x in query)
            //    {
            //        Program.VerboseAudit("ProcExistsInDB: x=" + x.DEPARTURE_DATETIME);
            //    }
            //}
            if (ct > 0)
                enddt = (DateTime)query.First().DEPARTURE_DATETIME;
            //else //see if there is a saved proc that ended before this startdt
            //{
            //    query = from proc in _procedure_events
            //            from ans in proc.PROCEDURE_ANSWERs
            //            where (proc.ENCOUNTER_ID == _pat.encounter_id)
            //                //&& (proc.PROCEDURE_DATETIME <= startdt)
            //                && (proc.DEPARTURE_DATETIME > startdt.AddHours(-4))
            //                && (ans.PROCEDURE_NUMBER == pnum)
            //            orderby proc.DEPARTURE_DATETIME descending
            //            select new { proc.DEPARTURE_DATETIME };
            //    ct = query.Count();
            //    if (ct > 0)
            //        enddt = (DateTime)query.First().DEPARTURE_DATETIME;
            //}
            Program.VerboseAudit("ProcExistsInDB: pnum=" + pnum + " returns " + ct);
            return ct > 0;
        }

        private void ProcessProc(int pnum, int groupnum, string res)
        {
            string DSC1 = "NURS Med/Surg Activities";      // this anchors the evdt for all DSC1-DSC3
            string DSC2 = "NURS Med/Surg Activites Freq";  // result contains the duration
            string DSC3 = "NURS Med/Surg Activities Start";// result contains the start time hh:mm

            string DSC4 = "NURSI Hour Activities";
            string DSC5 = "NURSI Hour Activities Freq";
            string DSC6 = "NURSI Hour Activities Start";

            string desc1, desc2, desc3;

            if (groupnum == 1)
            {
                desc1 = DSC1; desc2 = DSC2; desc3 = DSC3;
            }
            else if (groupnum == 2)
            {
                desc1 = DSC4; desc2 = DSC5; desc3 = DSC6;
            }
            else
                return;

            DateTime evdt = DateTime.MinValue;
            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = query.Where(e => e.DESCRIPTION.ToLower().StartsWith(desc1.ToLower()));
            query = query.Where(e => e.RESULT.ToLower().StartsWith(res.ToLower()));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            Program.VerboseAudit("qct: " + query.Count());
            if (query.Count() == 0) return;

            bool found_dur = false;
            bool found_st = false;
            bool done = false;
            string dur = "";
            int stimecolpos;

            string stime; //14:00
            int durint;
            DateTime tempdate;
            string stimehr;
            string stimemin;
            DateTime date_of_event;
            DateTime dt_of_event;
            DateTime sdt;
            DateTime event_dt_plus1hr;
            string res2 = "";
            foreach (var q in query)
            {
                if (evdt < q.EVENT_DATETIME && !done)
                {
                    evdt = q.EVENT_DATETIME;
                    var query2 = StartNewQuery(SearchDepth.SearchDefault);
                    query2 = query2.Where(e => e.DESCRIPTION.ToLower().StartsWith(desc2.ToLower()));
                    query2 = query2.Where(e => e.EVENT_DATETIME == evdt);
                    Program.VerboseAudit("q2ct: " + query2.Count());
                    if (query2.Count() > 0)
                    {
                        found_dur = true;
                        done = true;
                        res2 = query2.First().RESULT;
                        stimecolpos = res2.IndexOf(" ");
                        dur = res2.Substring(0, stimecolpos);
                        Program.VerboseAudit("q2 duration=" + dur+ " from result="+res2);
                    }

                }
            }

            if (!found_dur)
            {
                Program.VerboseAudit("For activity: " + desc2 + " there was no frequency found.");
                return;
            }

            done = false;
            var query3 = StartNewQuery(SearchDepth.SearchDefault);
            query3 = query3.Where(e => e.DESCRIPTION.ToLower().StartsWith(desc3.ToLower()));
            query3 = query3.Where(e => e.EVENT_DATETIME == evdt);
            Program.VerboseAudit("q3ct: " + query3.Count());
            if (query3.Count() == 0)
            {
                Program.VerboseAudit("For activity: " + desc2 + " there was no start time found.");
                return;
            }

            found_st = true;
            done = true;
            stime = query3.First().RESULT;
            Program.VerboseAudit("q3 stime: " + stime);

//'determine the date for stime that is before rs(0)  say rs(0) = 2/1 01:00  stime=23:00
//' associate stime with rs(0) date.  compare this dt with rs(0)  
// if positive then ok because 2/1 23:00 is After 01:00
//' else associate stime with rs(0)date-1.  1/31 23:00


            stimecolpos = stime.IndexOf(":");
            stimehr = stime.Substring(0, stimecolpos);
            Program.VerboseAudit("q3 stimeHR: " + stimehr);
            if (stimehr.Length == 1) stimehr = "0" + stimehr;
            stimemin = stime.Substring(stimecolpos + 1);
            Program.VerboseAudit("q3 stimeMN: " + stimemin);

            event_dt_plus1hr = evdt.AddHours(1);
            Program.VerboseAudit("event_dt_plus1hr: " + event_dt_plus1hr);
            date_of_event = event_dt_plus1hr.Date;//Format$(event_dt_plus1hr, "yyyymmdd")  ' example 20150122 1400
            Program.VerboseAudit("date_of_event: " + date_of_event);
            dt_of_event = event_dt_plus1hr;// ' add an hour: 201501221500


            sdt = date_of_event.AddHours(stimehr.Val()).AddMinutes(stimemin.Val());
            Program.VerboseAudit("sdt: " + sdt);
            if (dt_of_event < sdt)  //'decrease date_of_event by 1 day
            {
                tempdate = event_dt_plus1hr;
                tempdate = tempdate.AddDays(-1);
                date_of_event = tempdate.Date;  //Format$(tempdate, "yyyymmdd")
                sdt = date_of_event.AddHours(stimehr.Val()).AddMinutes(stimemin.Val());
            }

            tempdate = sdt;

            if (found_dur && found_st)
            {
                //  durint = CInt(dur)
                //numprocs = numprocs + 1;
                //procs(numprocs).pnum = pnum
                //procs(numprocs).start = tempdate
                //procs(numprocs).finish = DateAdd("h", durint, tempdate)
                AddSimpleProc(pnum, sdt, sdt.AddHours(dur.Val()));
                Program.VerboseAudit("AddSimpleProc: " + pnum + "  sdt="+sdt + " dur="+dur);
            }


        }

        private void CheckProc_1()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A1. 1-1 safety observation by RN");
            Program.VerboseAudit("---------------");

            string res = "1:1 Safety observation by RN";
            ProcessProc(1, 1, res);
            ProcessProc(1, 2, res);
        }

        private void CheckProc_2()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A2. 1-1 safety observation by non-RN");
            Program.VerboseAudit("---------------");

            string res = "1:1 Safety observation by non-RN";
            ProcessProc(2, 1, res);
            ProcessProc(2, 2, res);
        }


        private void AddSitter(string code, DateTime initdt, DateTime evdt)
        {
            string desc = "TC: Direct Observer";
            string res = "Initiated";
            DateTime timestmp = DateTime.Now;
            short seq = 0;
            int unitid = _pat.unit_id;

            using (var db = PFSDBUtility.NewSqlConnection())
            {
                string q = "INSERT INTO chart_item (encounter_id,event_datetime,code,description,result,timestamp,sequence,unit_id,procedure_start)";
                q += " select @encid, @evdt, @code, @desc, @res, @ts,@seq,@unit,@procstart where not exists";
                q += " (select encounter_id,code,event_datetime from chart_item where encounter_id=" + _pat.encounter_id.ToString() + " and code='" + code + "' and event_datetime=" + PFSDBUtility.SQLDateTime(evdt) + ")";
                //string q = "INSERT INTO chart_item (encounter_id,event_datetime,code,description,timestamp,sequence,unit_id,order_id) values (@encid, @evdt, @code, @desc, @ts,@seq,@unit,@oid)";
                SqlCommand cmd = new SqlCommand(q, db);
                cmd.Parameters.AddWithValue("@encid", _pat.encounter_id);
                cmd.Parameters.AddWithValue("@evdt", evdt);
                cmd.Parameters.AddWithValue("@code", code);
                cmd.Parameters.AddWithValue("@desc", desc);
                cmd.Parameters.AddWithValue("@res", res);
                cmd.Parameters.AddWithValue("@ts", timestmp);
                cmd.Parameters.AddWithValue("@seq", seq);
                cmd.Parameters.AddWithValue("@unit", unitid);
                cmd.Parameters.AddWithValue("@procstart", initdt);
                cmd.ExecuteNonQuery();
                db.Close();
            } //using db

        }

        private int GetSitterType(DateTime evdt)
        { // get the sitter type RN or non-RN from the charting that should exist
            //at the same time as the initiation of the observer 1540100298
            int pnum = 0;
            string return_result = "";
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);    // add custom time range below
            query = query.Where(e => e.CODE == "1540100298");
            query = query.Where(e => e.EVENT_DATETIME == evdt);
            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
                if (return_result.Trim().ToUpper().StartsWith("RN")) pnum = 1;
                if (return_result.Trim().ToUpper().StartsWith("NON-RN")) pnum = 2;
            }
            if (pnum == 0)
            {
                Program.VerboseAudit("Sitter type RN or non-RN not found. Defaulting to non_RN.");
                pnum = 2;
            }
            return pnum;
        }

        private void CheckActivity(int actnum,string actcode, string actst, string actlencode)
        {
            int actlen;
            string actsthhmm = "0000";
        
            var query = StartNewQuery(SearchDepth.SearchSince24Hrs);    // add custom time range below
            query = query.Where(e => e.CODE == actcode);
            query = query.Where(e => e.ORDER_STATUS == null || e.ORDER_STATUS == ""); // need to update order_status=x
            query = query.OrderBy(e => e.EVENT_DATETIME);
            Program.VerboseAudit("for code=" + actcode + " count=" + query.Count());
            foreach (var item in query)
            {
                if (item.RESULT.ToLower() == "yes")
                { //09500000
                    var q2 = StartNewQuery(SearchDepth.SearchSince24Hrs);    // add custom time range below
                    q2 = q2.Where(e => e.CODE == actst);
                    q2 = q2.Where(e => e.EVENT_DATETIME == item.EVENT_DATETIME);
                    Program.VerboseAudit("for code=" + actst + " count=" + q2.Count());
                    if (q2.Count() == 0) return;

                    actsthhmm = q2.First().RESULT;
                    Program.VerboseAudit("actsthhmm=" + actsthhmm);
                    if (actsthhmm.Length >= 4)
                        actsthhmm = actsthhmm.Substring(0, 4);
                    Program.VerboseAudit("actsthhmm=" + actsthhmm);

                    var q3 = StartNewQuery(SearchDepth.SearchSince24Hrs);    // add custom time range below
                    q3 = q3.Where(e => e.CODE == actlencode);
                    q3 = q3.Where(e => e.EVENT_DATETIME == item.EVENT_DATETIME);
                    Program.VerboseAudit("for code=" + actlencode + " count=" + q3.Count());
                    if (q3.Count() == 0) return;

                    actlen = (int)q3.First().RESULT.Val();
                    DateTime stdt = DateTime.MinValue;
                    if (actlen >= 60)
                        {
                        TimeSpan start = new TimeSpan(0, 0, 0); //0 o'clock
                        TimeSpan end = new TimeSpan(6, 0, 0); // 6 o'clock
                        TimeSpan item_ts = item.EVENT_DATETIME.TimeOfDay;
                        TimeSpan actst_ts = new TimeSpan((int)actsthhmm.Substring(0, 2).Val(), (int)actsthhmm.Substring(2, 2).Val(), 0);
                        stdt = item.EVENT_DATETIME.Date + actst_ts;
                        if ((item_ts >= start) && (item_ts <= end))
                        {
                            if (actst_ts > item_ts) // then it belongs to prev cal day.
                                stdt = item.EVENT_DATETIME.Date.AddDays(-1) + actst_ts;
                        }
//                      DateTime stdt = item.EVENT_DATETIME.Date.AddHours(actsthhmm.Substring(0, 2).Val()).AddMinutes(actsthhmm.Substring(2, 2).Val());
                        Program.VerboseAudit("stdt=" + stdt);
                        AddSimpleProc(actnum, stdt, stdt.AddMinutes(actlen));
                        NullifyActivity(actcode, item.EVENT_DATETIME);
                        }
                }
            }

        }

        private void NullifyActivity(string actcode, DateTime evdt)
        {
            var db = PFSDBUtility.NewSqlConnection();
            string q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and event_datetime=" + PFSDBUtility.SQLDateTime(evdt) + " and code='" + actcode + "'";
            SqlCommand cmd = new SqlCommand(q, db);
            cmd.ExecuteNonQuery();
            db.Close();
        }

        private DateTime NextFinish(DateTime startdt)
        {
            DateTime dt = DateTime.MinValue;

            int b = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, startdt) / (4 * 60));
            dt = _pat.pull_start.AddMinutes((b + 1) * 240);
            Program.VerboseAudit("NextFinish: startdt=" + startdt.ToString() + " b=" + b + " dt=" + dt.ToString());
            return dt;
        }


        private bool ProcExists(int pnum, DateTime startdt, DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && (proc.PROCEDURE_DATETIME == startdt)
                            && (proc.DEPARTURE_DATETIME == enddt)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new { proc.PROCEDURE_EVENT_ID };
            return (query.Count() > 0);
        }


        private void CheckProc_3()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A3. Off unit accompanied by RN");
            Program.VerboseAudit("---------------");

            string res = "Off unit accompanied by RN";
            ProcessProc(3, 1, res);
            ProcessProc(3, 2, res);

        }
        private void CheckProc_4()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A4. Off unit accompanied by Non-RN");
            Program.VerboseAudit("---------------");

            string res = "Off unit accompanied by non-RN";
            ProcessProc(4, 1, res);
            ProcessProc(4, 2, res);
            
        }

        private void CheckProc_5()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A5. Patient/family education by RN");
            Program.VerboseAudit("---------------");

            string res = "Patient/family education by RN";
            ProcessProc(5, 1, res);
            ProcessProc(5, 2, res);

        }

        private void CheckProc_6()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A6. Extensive wound management by RN");
            Program.VerboseAudit("---------------");

            string res = "Extensive wound management by RN";
            ProcessProc(6, 1, res);
            ProcessProc(6, 2, res);

        }

        private void CheckProc_7()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A7. Extensive wound management by non-RN");
            Program.VerboseAudit("---------------");

            string res = "Extensive wound management by non-RN";
            ProcessProc(7, 1, res);
            ProcessProc(7, 2, res);

        }

        private void CheckProc_8()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A8. Coordination of care by RN");
            Program.VerboseAudit("---------------");

            string res = "Discharge coordination";
            ProcessProc(8, 1, res);
            ProcessProc(8, 2, res);

            res = "Outside facility transfer coordination";
            ProcessProc(8, 1, res);
            ProcessProc(8, 2, res);

            res = "Interdisciplinary care conference/coordination";
            ProcessProc(8, 1, res);
            ProcessProc(8, 2, res);

        }

        private void CheckProc_9()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A9. 1:1 RN at bedside");
            Program.VerboseAudit("---------------");

            string res = "1:1 by RN at the bedside";
            ProcessProc(9, 1, res);
            ProcessProc(9, 2, res);

        }

        private void CheckProc_10()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A10. 1:1 non-RN at bedside");
            Program.VerboseAudit("---------------");

            string res = "1:1 by non-RN at the bedside";
            ProcessProc(10, 1, res);
            ProcessProc(10, 2, res);

        }

        private void CheckProc_11()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A11. 2:1 by RN at bedside");
            Program.VerboseAudit("---------------");

            string res = "2:1 by RN at the bedside";
            ProcessProc(11, 1, res);
            ProcessProc(11, 2, res);

        }

        private void OutputClassAndDNC(bool do_class)
        {
            int i;
            if (numclassdnc == 0 && do_class)
                OutputClass(DateTime.MinValue, DateTime.MinValue);
            else
            {
                for (i = 1; i <= numclassdnc; i++)
                {
                    if (aryclassdnc[i].retain)
                        OutputDNC(aryclassdnc[i].startdt, aryclassdnc[i].enddt);
                    else // do_class will never be false here
                        OutputClass(aryclassdnc[i].startdt, aryclassdnc[i].enddt);
                }
            }
        }

        //        private const string DATETIME_FORMAT = "yyyyMMddHHmm";              // ISO Date/Time w/o seconds
        private const string DATETIME_FORMAT = "yyyyMMddHHmm";              // ISO Date/Time w/o seconds

        private void OutputClass(DateTime dncstdt,DateTime dncendt)
        {
            string outstr, ind_list, desc, str_pull_dt, str_in_dt, str_out_dt = "";
            int i, tc_event_id;
            bool repeat_output = false;
            string repeat_time = "";
            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         212 242 304
            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1
            //1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901 3456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
            //1       |10 5            |                |                |        |10023121779         |ALEXANDER                       |GLORIA                          |                                |0508    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289530  |           |201403030400                                                                      |NNYNNNNYYNNYNNNYNNNYYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
            //1       |10 5            |                |                |        |10024544908         |ARMSTRONG                       |AARON                           |                                |0535    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289531  |           |201403030400                                                                      |NYNNYNNNNNNYNNNYNNNNYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
            //|0547    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289532  |           |201403030400                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN
            //|0547    |A   |201403030700|                |201403030700    |18  |C|    |3040|1440|18289532  |           |201403030700                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//            1RO | RO MB5BG / 6E |                |                |        | 2000180316769 | PARISIEN | GREYSON | LAWRENCE | RMB5514 | P | 20190319030000 |                               | 20 | C |    | 5399 | 480 | 62040560 |           | 20190319030000 | 20190319070000 | NNNYNNNNNNNNYNNNYNYNYNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN

            if (Program.g_is_test)
                tc_event_id = 9999;
            else
                tc_event_id = PFSDBUtility.NextGID();                         //get a unique id for this class
                                                                              //            str_pull_dt = _pat.pull_finish.ToString(DATETIME_FORMAT);
                                                                              //str_pull_dt = _pat.pull_finish.ToString(DATETIME_FORMAT);
                                                                              //if (_pat.unit_departure != null && _pat.unit_departure != DateTime.MinValue)
            if (dncstdt == DateTime.MinValue)
            {
                //                if (_pat.effective_out == Program.g_pull_finish && _pat.unit_departure == DateTime.MinValue)
                Program.VerboseAudit("Loc arrtime=" + loc_arrtime + " gpull_start_save=" + Program.g_pull_start_save);
                if (loc_arrtime >= Program.g_pull_start_save)
                {
                    //str_in_dt = Program.g_effdt.ToString(DATETIME_FORMAT);
                    str_in_dt = loc_arrtime.ToString(DATETIME_FORMAT);
                    str_out_dt = loc_out.ToString(DATETIME_FORMAT);

                    if (loc_arrtime <= Program.g_effdt)
                    {
                        repeat_output = true;//also make the desired class time
                        repeat_time = Program.g_effdt.ToString(DATETIME_FORMAT);
                    }
                }
                else
                {
                    str_in_dt = Program.g_effdt.ToString(DATETIME_FORMAT);
                    str_out_dt = "";// _pat.effective_out.ToString(DATETIME_FORMAT);
                    str_out_dt = loc_out.ToString(DATETIME_FORMAT);
                }
            }
            else //dnc
            {
                str_in_dt = dncstdt.ToString(DATETIME_FORMAT);
                str_out_dt = dncendt.ToString(DATETIME_FORMAT);
            }
            str_out_dt = "";//leave open-ended for night ASSIGNMENT MODULE 4/14/21

            //outstr = _pat.facilty_code.FixedWidth(8);       //(facility code)
            outstr = "".FixedWidth(8);                       //(facility code)
            outstr += "|" + _pat.unit_name.FixedWidth(16);
            outstr += "|" + "".FixedWidth(16);                               //(unit code)
            outstr += "|" + txarea.FixedWidth(16);                               //(area code)
            outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
            outstr += "|" + _pat.acct.FixedWidth(20);
            outstr += "|" + _pat.last_name.FixedWidth(32);
            outstr += "|" + _pat.first_name.FixedWidth(32);
            outstr += "|" + _pat.middle_name.FixedWidth(32);
            outstr += "|" + _pat.room.FixedWidth(8);
            outstr += "|" + _pat.bed.FixedWidth(4);
            outstr += "|" + str_in_dt;        //CLASS dt
            outstr += "|" + "".FixedWidth(14);                               //(login)
            outstr = outstr.FixedWidth(232);
            outstr += "|" + _pat.effective_out.ToString(DATETIME_FORMAT);//str_out_dt;        //TC Data End Point
            outstr = outstr.FixedWidth(249);
            outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
            outstr += "|" + "C".FixedWidth(1);                               //record type = class
            outstr += "|" + "".FixedWidth(4);                                //(stage)
            outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
            outstr += "|" + _pat.range.ToString().FixedWidth(4);             //TC pull range
            outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
            outstr += "|";
            outstr = outstr.FixedWidth(294);
            outstr += "|" + str_in_dt;        //IN
            outstr = outstr.FixedWidth(346);
            //if (_pat.unit_departure != null && _pat.unit_departure != DateTime.MinValue)
                //                outstr += "|" + _pat.unit_departure.ToString(DATETIME_FORMAT);        //OUT
                outstr += "|" + str_out_dt;        //OUT
            outstr = outstr.FixedWidth(377);
            outstr += "|";

            //if (use_default)
            //{ //make all is_checked = false and then mark defaults
            //    Program.VerboseAudit("Patient Will receive default indicators " + _pat.default_inds_str);
            //    for (i = 1; (i <= MAX_INDS); i++)
            //    {
            //        _inds[i].is_checked = false;
            //    }
            //    foreach (var ind in _pat.default_inds)
            //    {
            //        if (ind <= _inds.GetUpperBound(0))
            //        {
            //            _inds[ind].is_checked = true;
            //        }
            //    }
            //}

            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++) {
                if (_inds[i].is_checked) {
                    outstr += "Y";
                    ind_list += "" + i;
                } else {
                    outstr += "N";
                }
            } // next i
            ind_list = ind_list.Substring(1);                           //strip leading comma
                                                                        //                                                                                                   1                                                                                                   2                                                                                                   3
                                                                        //         1         2         3         4         5         6         7         8         9         0         1         2         3         4         5         6         7         8         9         0         1         2         3         4         5         6         7         8         9         0         1         2         3         4         5         6         7         8         9         0
                                                                        //1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
                                                                        //1       |DO6D            |                |                |        |2000192224892       |BEHNAM                          |KENDRA                          |LEE                             |RDO6311 |P   |20180717110000|                               |20  |C|    |5399|480 |56103278  |           |20180717110000                                     |                              |YNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
            string str5am;
            Program.outfile.WriteLine(outstr);      //1900yesterd output to transparent.txt
            if (repeat_output) // with repeat_time
            {
                string repeat_line = outstr.Substring(0, 203) + repeat_time + outstr.Substring(215, 80) + repeat_time + " ".Repeat(71) + outstr.Substring(378, 120);
                Program.outfile.WriteLine(repeat_line);
            }
            //if (Program.g_make5am)
            //{
            //    //if (str_out_dt.Substring(8, 4) == "0500") //create the 7am at 3am
            //    {
            //        string strdttm = Program.g_pull_finish.ToString(DATETIME_FORMAT);
            //        strdttm = strdttm.Substring(0, 8) + "0500";
            //        //str5am = outstr.Substring(0, 203) + Program.g_pull_finish.ToString(DATETIME_FORMAT) + outstr.Substring(217, 78) + Program.g_pull_finish.ToString(DATETIME_FORMAT) + " ".Repeat(69) + outstr.Substring(378, 120);
            //        str5am = outstr.Substring(0, 203) + strdttm + outstr.Substring(215, 80) + strdttm + " ".Repeat(71) + outstr.Substring(378, 120);
            //        Program.outfile2.WriteLine(str5am);
            //    }
            //}

            Program.Audit("");
            desc = "Classified: " + ind_list;
            if (Program.g_is_test) {
                Program.Audit(desc);
            } else {
                //Save the selected indicators plus both Program.Audits in the event log; link with tc_event_id
                PFSEventLog.AddTransparentMappingEventLogEntry(
                    desc, Program.gLogUnitID, Program.gLogEncounterID,
                    tc_event_id, Program.gLogMapperVersion,
                    Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
            }
        }

        private void OutputClassAndDNCTimes()
        {
            int i;
            DateTime dt1 = Program.g_effdt;
            numclassdnc = 0;
            DateTime lastend = Program.g_effdt;
            // retain has nothing to do with keeping the record. it only means "is DNC".

            for (i = 1; i <= numloa; i++)
            {
                //Why are these historicals stubbed off?? let it go through 10/09/20
                //if (aryloa[i].startdt < dt1) 
                //{
                //    Program.VerboseAudit("historical aryloa i:" + i + " aryloa[].startdt=" + aryloa[i].startdt + " aryloa[].enddt=" + aryloa[i].enddt + " dt1="+dt1);
                //}
                //else
                {
                    if (numclassdnc == 0)
                    {
                        if (aryloa[i].startdt > dt1)
                        {
                            numclassdnc++;
                            aryclassdnc[numclassdnc].startdt = dt1;
                            aryclassdnc[numclassdnc].enddt = aryloa[i].startdt;
                            aryclassdnc[numclassdnc].retain = false; //false=Class
                        }
                        numclassdnc++;
                        aryclassdnc[numclassdnc].startdt = aryloa[i].startdt;
                        aryclassdnc[numclassdnc].enddt = aryloa[i].enddt;
                        aryclassdnc[numclassdnc].retain = true; //true=DNC
                    }
                    else
                    {
                        numclassdnc++;
                        aryclassdnc[numclassdnc].startdt = aryloa[i - 1].enddt;
                        aryclassdnc[numclassdnc].enddt = aryloa[i].startdt;
                        aryclassdnc[numclassdnc].retain = false; //false=Class

                        numclassdnc++;
                        aryclassdnc[numclassdnc].startdt = aryloa[i].startdt;
                        aryclassdnc[numclassdnc].enddt = aryloa[i].enddt;
                        aryclassdnc[numclassdnc].retain = true; //true=DNC
                    }
                    lastend = aryloa[i].enddt;
                }
            } //for
            if (numclassdnc > 0 && lastend != dt1.AddHours(12)) 
                // then the loa ended before this draw period end
            {
                numclassdnc++;
                aryclassdnc[numclassdnc].startdt = lastend; // aryloa[i - 1].enddt;
                aryclassdnc[numclassdnc].enddt = dt1.AddHours(12);
                aryclassdnc[numclassdnc].retain = false; //false=Class
            }
            for (i=1;i<=numclassdnc;i++)
            {
                string classdnctype="";
                if (aryclassdnc[i].retain)
                    classdnctype = "DNC";
                else
                    classdnctype = "Class";
                Program.VerboseAudit(classdnctype + ": " + aryclassdnc[i].startdt + "-" + aryclassdnc[i].enddt);
            }
        }

        private void OutputDNC(DateTime dncstdt, DateTime dncendt)
        {
            string outstr, ind_list, desc, str_pull_dt, str_in_dt, str_out_dt = "";
            int i, tc_event_id;

            if (Program.g_is_test)
                tc_event_id = 9999;
            else
                tc_event_id = PFSDBUtility.NextGID();                         //get a unique id for this class

            //str_in_dt = aryloa[i].startdt.ToString(DATETIME_FORMAT);
            //str_out_dt = aryloa[i].enddt.ToString(DATETIME_FORMAT);
            str_in_dt = dncstdt.ToString(DATETIME_FORMAT);
            str_out_dt = dncendt.ToString(DATETIME_FORMAT);

            //outstr = _pat.facilty_code.FixedWidth(8);                       //(facility code)
            outstr = "".FixedWidth(8);                       //(facility code)
            outstr += "|" + _pat.unit_name.FixedWidth(16);
            outstr += "|" + "".FixedWidth(16);                               //(unit code)
            outstr += "|" + "".FixedWidth(16);                               //(area code)
            outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
            outstr += "|" + _pat.acct.FixedWidth(20);
            outstr += "|" + _pat.last_name.FixedWidth(32);
            outstr += "|" + _pat.first_name.FixedWidth(32);
            outstr += "|" + _pat.middle_name.FixedWidth(32);
            outstr += "|" + _pat.room.FixedWidth(8);
            outstr += "|" + _pat.bed.FixedWidth(4);
            outstr += "|" + str_in_dt;        //CLASS dt
            outstr += "|" + "".FixedWidth(14);                               //(login)
            outstr = outstr.FixedWidth(232);
            outstr += "|" + str_out_dt;  // TC END POINT_pat.effective_out.ToString(DATETIME_FORMAT);//TC Data End Point
            outstr = outstr.FixedWidth(249);
            outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
            outstr += "|" + "D".FixedWidth(1);                               //record type = class
            outstr += "|" + "".FixedWidth(4);                                //(stage)
            outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
            outstr += "|" + "".FixedWidth(4);             //TC pull range
            outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
            outstr += "|";
            outstr = outstr.FixedWidth(294);
            outstr += "|" + str_in_dt;        //IN
            outstr = outstr.FixedWidth(346);
            outstr += "|" + str_out_dt;        //OUT
            outstr = outstr.FixedWidth(377);
            outstr += "|NNNNY";

            Program.outfile.WriteLine(outstr);      //1900yesterd output to transparent.txt
            Program.Audit("");
            desc = "Classified DoNotClassify: NNNNY";
                //Save the selected indicators plus both Program.Audits in the event log; link with tc_event_id
                PFSEventLog.AddTransparentMappingEventLogEntry(
                    desc, Program.gLogUnitID, Program.gLogEncounterID,
                    tc_event_id, Program.gLogMapperVersion,
                    Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());

        }


        private int DeterminePtypeOfIndicators()
        {
            int i;
            var pscore = 0.0;
            int pt_type = 6;
            var indlist = new List<int>();
            string indstr = "";

            Program.VerboseAudit("Determine patient type of triggered indicators");

            for (i = 1; (i <= MAX_INDS); i++)
            {
                if (_inds[i].is_checked)
                {
                    indlist.Add(i);
                    indstr += i.ToString() + ",";
                    pscore += _inds[i].weight;
                }
            }

            Program.VerboseAudit("indicators=" + indstr + " score=" + pscore.ToString());

            var db = PFSDBUtility.NewPfsDataContext();
            var query_ptype = from ptype in db.PATIENT_TYPEs
                              where (ptype.METHODOLOGY_ID == _pat.meth_id)
                              orderby ptype.PATIENT_TYPE1
                              select new
                              {
                                  ptype.PATIENT_TYPE1,
                                  ptype.POINTS_HIVAL
                              };

            foreach (var ptypes in query_ptype)
            {
                //                Program.DebugTrace("type,hival=" + ptypes.PATIENT_TYPE1.ToString() + "" + ptypes.POINTS_HIVAL.ToString(), "");
                if (pscore <= ptypes.POINTS_HIVAL)
                {
                    if (pt_type > ptypes.PATIENT_TYPE1)
                    {
                        pt_type = ptypes.PATIENT_TYPE1;
                    }
                }
            }
            Program.VerboseAudit("patient type=" + pt_type.ToString());

            return pt_type;

        }


        //private bool ExistDefaultInPast16hrs()
        //{
        //    DateTime cdt1 = DateTime.MinValue;
        //    DateTime cdt2 = DateTime.MinValue;
        //    int cnt_all = 0;
        //    int cnt_def = 0;
        //    //get max class date of last non-default class = 1
        //    //get max class date of last default = 2
        //    // if 1 >= 2 then false
        //    // else
        //    //   if 2 > 1 and date is <= 16 hrs ago then true
        //    //   else false
        //    var db = PFSDBUtility.NewPfsDataContext();
        //    //var query = from ce in db.CLASSIFICATION_EVENTs
        //    //            where (ce.ENCOUNTER_ID == _pat.encounter_id)
        //    //            && (ce.CLASSIFIED_BY_ID != -2)
        //    //            && (System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(ce.CLASSIFICATION_DATETIME, Program.g_pull_start) <= 960)
        //    //            select ce;
        //    //cnt_all = query.Count();
        //    //if (cnt_all > 0)
        //    //{
        //    //    cdt1 = PFSUtility.DBToDateTime(query.Max(x => x.CLASSIFICATION_DATETIME));
        //    //    Program.VerboseAudit("Last regular classification was at:"+cdt1.ToString());
        //    //}
        //    //else {
        //    //    Program.VerboseAudit("No regular classifications within the past 16 hours");
        //    //}

        //    var query = from ce in db.CLASSIFICATION_EVENTs
        //                where (ce.ENCOUNTER_ID == _pat.encounter_id)
        //                && (ce.CLASSIFIED_BY_ID == -2)
        //                && (System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(ce.CLASSIFICATION_DATETIME, Program.g_pull_finish) <= 960)
        //                select ce;
        //    cnt_def = query.Count();

        //    if (cnt_def > 0)
        //    {
        //        cdt2 = PFSDBUtility.DBToDateTime(query.Max(x => x.CLASSIFICATION_DATETIME));
        //        Program.VerboseAudit("Last default classification was at:" + cdt2.ToString());
        //    }
        //    else
        //    {
        //        Program.VerboseAudit("No default classifications within the past 16 hours");
        //    }
        //    return (cnt_def > 0);

        //}

        private void OutputProcs()
        {
            int i;
            string outstr, proc_list, desc;
            int tc_event_id;

            foreach (var proc in _procs)
            {
                if (Program.g_is_test)
                    tc_event_id = 9999;
                else
                    tc_event_id = PFSDBUtility.NextGID();                         //get a unique id for this proc

                outstr = "".FixedWidth(8);                       //(facility code)
                outstr += "|" + _pat.unit_name.FixedWidth(16);
                outstr += "|" + "".FixedWidth(16);                               //(unit code)
                outstr += "|" + txarea.FixedWidth(16);                               //(area code)
                outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
                outstr += "|" + _pat.acct.FixedWidth(20);
                outstr += "|" + _pat.last_name.FixedWidth(32);
                outstr += "|" + _pat.first_name.FixedWidth(32);
                outstr += "|" + _pat.middle_name.FixedWidth(32);
                outstr += "|" + _pat.room.FixedWidth(8);
                outstr += "|" + _pat.bed.FixedWidth(4);
                outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //204 proc dt
                outstr += "|" + "".FixedWidth(14);                               //(login)
                outstr = outstr.FixedWidth(249);
                outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
                outstr += "|" + "P".FixedWidth(1);                               //record type = class
                outstr += "|" + "".FixedWidth(4);                                //(stage)
                outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
                outstr += "|" + _pat.range.ToString().FixedWidth(4);             //TC pull range
                outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
                outstr += "|";
                outstr = outstr.FixedWidth(294);
                outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //296 procdt in
                outstr = outstr.FixedWidth(346);
                //if (_pat.unit_departure != null && _pat.unit_departure != DateTime.MinValue)
                //                outstr += "|" + _pat.unit_departure.ToString(DATETIME_FORMAT);        //OUT
                outstr += "|" + proc.finish.ToString(DATETIME_FORMAT);          //348 procdt out
                outstr = outstr.FixedWidth(377);
                outstr += "|";

                proc_list = "";
                for (i = 1; (i < MAX_PROCS); i++)
                {
                    if (proc.procedure_number == i)
                    {
                        outstr += "Y";
                        proc_list += "" + i;
                    }
                    else
                    {
                        outstr += "N";
                    }
                } // next i
                proc_list = proc_list.Substring(1);                             //strip leading comma

                Program.outfile.WriteLine(outstr);                              //output to transparent.txt

                desc = "Activities: " + proc_list;
                if (Program.g_is_test)
                {
                    Program.Audit(desc);
                }
                else
                {
                    //Save the selected procedures plus both Program.Audits in the event log; link with tc_event_id
                    //(this assumes the procedure Program.Audit is mixed in with the class Program.Audit)
                    PFSEventLog.AddTransparentMappingEventLogEntry(desc, Program.gLogUnitID, Program.gLogEncounterID,
                        tc_event_id, Program.gLogMapperVersion,
                        Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
                }
            } // next proc
        }

    }
}


