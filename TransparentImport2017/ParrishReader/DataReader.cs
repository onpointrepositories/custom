﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;                // for SqlConnection
using System.IO;                            // for Path
using PfsShared;
//
// Data Reader for Transparent Classification
// This loads a text file into the chart_item table.
// Patients must already exist.
//
namespace DataReader
{
    class DataReader
    {      
        // Info about the patient being processed
        const string DATETIME_FORMAT = "yyyyMMddHHmm";              // ISO Date/Time w/o seconds
        const string DATE_FORMAT = "yyyyMMdd";              // ISO Date/Time w/o seconds
        string _acct_number;
        //string      _unit_name;
        int         _encounter_id = 0; 
        int         _unit_id = 0;
        DateTime    _start = DateTime.Now;
        DateTime    _finish = DateTime.Now;
        DateTime    _dietstart = DateTime.Now;

        // Process one text file.
        // Returns true if the file has been processed and it is OK to rename.
        //
        public bool process(string pathname)
        {
            StreamReader infile;
            string line;
            string prev_acct_num="Starting account num";
            //int line_num = 0;
            string filename = Path.GetFileName(pathname);
            bool is_diets_file = (pathname.ToLower().Contains("diets"));
            bool skip_pt = false;

            short sequence = 0;
            string dtstr = "";
            string code = "";
            string cat = "";
            string desc = "";
            string res="";
            string fld = "";

            var pfs = PFSDBUtility.NewPfsDataContext();

            try
            {
                infile = new StreamReader(pathname);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error opening file {0}", pathname);
                Console.WriteLine(e.Message);
                Program.LogValidationError(e.Message, pathname);
                return false;
            }

            DeleteExistingBorderItems();
//A00030910723	WILLIAMS,TEST	3N		06/29/15 13:53	2000002	Assess IV/invasive line status	Intervention Filed
//InterventionsAM
//Account Number^Patient Name^Unit^Activity Date^Intervention^Name^Documentation Result
//A00032382285^BRATCHER,PAMELA G^3MSW^07/12/15 18:27^0500030^Intake and Output^Intervention Filed
// 0 = acct  3= dt  4=code  5=desc  6=res


//ProblemsAM
//Account Number^Name^Location^Documentation Date/Time^Mnemonic^Name^Status^Start Date/Time
//A00032372096^SMITH,DARROCH H^EMA^07/13/15 08:53^MENTAL^Mental Status - Impaired^Active Problem^06/12/15 18:50
//A00032392227^CAMPBELL,MITCHIL H^2ICU^07/13/15 08:53^SINI^Social Interaction - Impaired^Active Problem^06/12/15 18:50
// 0 = acct  3=dt  4=code 5=desc  6=res


//QueriesAM
//A00032495608^WOODS,CHRISTEEN^4S^2015-07-09 21:00:00.000^GI.BP^Bowel Pattern^{Incontinent}
//A00032495608^WOODS,CHRISTEEN^4S^2015-07-09 19:16:00.000^VS.RESP^Respiratory Rate^20
// 0 =acct 3=dt  4=code 5=desc 6=res       
            int i = 0;
            while ((line = infile.ReadLine()) != null) {
                Program.DebugTrace("beforeLine");
                line.Replace("{", " ");
                line.Replace("}", " ");
                Program.DebugTrace(++i + " : " + line);
                var rec_items = line.Split('^'); // split each line on carat
                Program.DebugTrace("ub=" + rec_items.GetUpperBound(0));
                _acct_number = rec_items[0].Trim();
                if (_acct_number != prev_acct_num)
                { //acct
                    if (prev_acct_num != "Starting account num")
                    {
                        Program.DebugTrace("beforesubmit1");
                        SubmitPatientChartItems(pfs);
                    }
                    sequence = 0;
                    skip_pt = false;
                    prev_acct_num = _acct_number;
                    var encounter_lookup =
                        from enc in pfs.ENCOUNTERs
                        where enc.ACCT_NUMBER == _acct_number
                        select new {
                            enc.ENCOUNTER_ID
                        };
                    if (encounter_lookup.Count() == 0) {
                        Program.DebugTrace("beforeacctnotfound");
                        Program.LogValidationError("Account number not found: " + _acct_number, filename);
                        skip_pt = true;
                        _encounter_id = 0;
                    }

                    if (!skip_pt)
                    { //Unit
                        var enc_found = encounter_lookup.First();
                        _encounter_id = enc_found.ENCOUNTER_ID;
                        Program.DebugTrace("beforeencid");
                        Program.DebugTrace("Encounter ID = {0}", _encounter_id);
                        // Search for unit id
                            // Then look for the unit name
                            var encloc_lookup =
                                from encloc in pfs.ENCOUNTER_LOCATIONs
                                where encloc.ENCOUNTER_ID == _encounter_id
                                orderby encloc.EFFECTIVE_DATETIME_OUT descending
                                select new
                                {
                                    encloc.WORKING_UNIT_ID
                                };
                            if (encloc_lookup.Count() == 0)
                            {
                                //Program.LogValidationError("Unit not found: " + _unit_name, filename);
                                skip_pt = true;
                                _unit_id = 0;
                                //return false;
                            }
                            if (!skip_pt)
                            {
                                var encloc_found = encloc_lookup.First();
                                _unit_id = (int)encloc_found.WORKING_UNIT_ID;
                            }
                    } //Unit
                } //acct
                if (!skip_pt)
                {
                    Program.DebugTrace("ub = {0}", rec_items.GetUpperBound(0));
                    //Program.DebugTrace("desc = {0}", desc);
                    Program.DebugTrace("times = {0}", rec_items[1]);
//A00030910723	WILLIAMS,TEST	3N		06/29/15 13:53	2000002	Assess IV/invasive line status	Intervention Filed

//Account Number^Name^Location^Treatment Area^Start^Diet^Name
//A00033196171^SPROLES,TED^2ICU^^^^
//A00033159211^MEYER,PAUL E^2ICU^^01/06/16 07:56^TUBE^Tube Feedings
//A00033191529^MORGAN,RAYMOND W^2ICU^^01/13/16 18:21^LF^Low Cholesterol/Low Fat/2 gm Sodium
//A00033180613^GILSTRAP,BEVERLY A^2ICU^^01/13/16 00:01^NPO^Nothing Per Oral
                    if (is_diets_file)
                    {
                        dtstr = ""; // date in file is start, but we just want current time
                        code = rec_items[5].Trim();
                        if (code == "") code = "NPO";
                        cat = filename; 
                        desc = rec_items[6].Trim();
                    }
                    else
                    {
                        dtstr = rec_items[3].Trim();
                        code = rec_items[4].Trim();
                        cat = filename; 
                        desc = rec_items[5].Trim();
                        res="";
                        if (rec_items.GetUpperBound(0) > 5) 
                            res = rec_items[6].Trim();
                        fld = "";
                        if (rec_items.GetUpperBound(0) > 6)
                            fld = rec_items[7].Trim();
                    }


                    // String to DateTime
                    DateTime perf_time = DateTime.MinValue;
                    Program.DebugTrace("parsed date: " + dtstr);
                    if (dtstr == "")
                        perf_time = _start;
                    else
                    {
                        if (dtstr.Contains("-"))
                        {
                            Program.DebugTrace("format will be yyyy-MM-dd HH:mm");
                            perf_time = DateTime.ParseExact(dtstr.Substring(0, 16), "yyyy-MM-dd HH:mm", null);
                        }
                        else
                            perf_time = DateTime.ParseExact(dtstr, "MM/dd/yy HH:mm", null);
                    }

                    // get latest location for unit id
                    var ci = NewChartItem(code, sequence, cat, desc, fld, res, perf_time);
                    pfs.CHART_ITEMs.InsertOnSubmit(ci);

                    if (sequence == 0)
                    {
                        _start = perf_time;
                        _finish = perf_time;
                    }
                    else
                    {
                        // note: items may be in random order
                        if (perf_time < _start) _start = perf_time;
                        if (perf_time > _finish) _finish = perf_time;
                    }
                    sequence++;
                }

            } //while

            Program.DebugTrace("beforeend");
            infile.Close();
            SubmitPatientChartItems(pfs);

            return true;
        }

        private void DeleteExistingBorderItems()
        {
            //
            // Delete existing MEDFILE chart items for this encounter bewteen the timestamps
            //
            string str_dt = _start.ToString(DATETIME_FORMAT); // yyyymmddhhnn
            string str_0700dt = _start.ToString(DATE_FORMAT) + "0700"; // yyyymmdd
            string str_1900dt = _start.ToString(DATE_FORMAT) + "1900"; // yyyymmdd
            string str_ev_dt;
            string diet_dt;
            DateTime ev_dt;
            Program.DebugTrace(str_dt);
            Program.DebugTrace(str_1900dt);
            int result = str_dt.CompareTo(str_1900dt);
            if (result > 0)
            {
                Program.DebugTrace("compare is greater");
                //then current time is greater than 1900 = delete todays 0700
                str_ev_dt = str_0700dt.Substring(4, 2) + "/" + str_0700dt.Substring(6, 2) + "/" + str_0700dt.Substring(0, 4) + " 07:00";
            }
            else
            {
                Program.DebugTrace("compare is Less");
                //then current time is less than 1900 = delete yesterday 1900
                ev_dt = _start.AddDays(-1);
                str_dt = ev_dt.ToString(DATE_FORMAT); // yyyymmdd
                str_1900dt = str_dt + "1900"; // yyyymmddhhnn
                str_ev_dt = str_1900dt.Substring(4, 2) + "/" + str_1900dt.Substring(6, 2) + "/" + str_1900dt.Substring(0, 4) + " 19:00";
            }

            diet_dt = str_ev_dt; // 12/31/2016 19:00
            _dietstart = PFSUtility.ISOToDateTime(diet_dt.Substring(6, 4) + diet_dt.Substring(0, 2) + diet_dt.Substring(3, 2) + diet_dt.Substring(11, 2) + diet_dt.Substring(14, 2));

            Program.DebugTrace("Delete existing...");
            using (var cn = PFSDBUtility.NewSqlConnection())
            {
                string sql = "delete from chart_item where " +
                    " category like '%.txt'" +
                    " and event_datetime = '" + str_ev_dt + "'";
                Program.DebugTrace(sql);
                var cmd = new SqlCommand(sql, cn);
                cmd.ExecuteNonQuery();
            }

        }

        private void SubmitPatientChartItems(PfsDataContext pfs)
        {
            if (_encounter_id == 0) return;
            if (_unit_id == 0) return;


            // Now save the new chart items
            pfs.SubmitChanges();
            Console.WriteLine("Processed acct {0}", _acct_number);
        }

        private CHART_ITEM NewChartItem(string code, short sequence, string cat, string desc, string fld, string res, DateTime perf_time)
        {
            var result = new CHART_ITEM();

            result.EVENT_DATETIME = perf_time;
            result.ENCOUNTER_ID = _encounter_id;
            result.UNIT_ID = _unit_id;
            result.SEQUENCE = sequence;                     // needed in case of duplicate codes
            result.CODE = code;
            result.DESCRIPTION = desc;
            result.CATEGORY = cat;
            result.RESULT = res;
            result.FIELD_NAME = fld;
            // We should reach out and get the server time, but this should be running on the server anyway
            result.TIMESTAMP = DateTime.Now;                        // ** should use server time
            
            return result;
        }

    }
}
