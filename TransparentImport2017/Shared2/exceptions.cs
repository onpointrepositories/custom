﻿using System;
//
// Make an exception that we can raise that won't be confused with a system exception.
// Our PFSUtility.MessageBoxError() can detect this and make a more friendly message.
//
namespace PfsShared
{
    public class PfsValidationException: Exception
    {
        public PfsValidationException()
        {
        }
        public PfsValidationException(string message) : base(message)
        {
        }
        public PfsValidationException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}
