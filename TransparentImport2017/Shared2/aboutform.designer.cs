﻿namespace PfsShared
{
    partial class AboutForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AboutForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.lblProdVersion = new System.Windows.Forms.Label();
            this.lblModule = new System.Windows.Forms.Label();
            this.lblBuildNum = new System.Windows.Forms.Label();
            this.lblPatchLevel = new System.Windows.Forms.Label();
            this.lblServer = new System.Windows.Forms.Label();
            this.lblCopyRight = new System.Windows.Forms.Label();
            this.pnlAboutForm = new System.Windows.Forms.Panel();
            this.bttTechSupport = new System.Windows.Forms.Button();
            this.bttOK = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.pnlAboutForm.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel1.Controls.Add(this.picLogo);
            this.panel1.Controls.Add(this.lblProdVersion);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(9, 9);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(536, 88);
            this.panel1.TabIndex = 0;
            // 
            // picLogo
            // 
            this.picLogo.Image = global::PfsShared.Properties.Resources.Harris_HC_screen;
            this.picLogo.Location = new System.Drawing.Point(12, 12);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(202, 73);
            this.picLogo.TabIndex = 1;
            this.picLogo.TabStop = false;
            // 
            // lblProdVersion
            // 
            this.lblProdVersion.AutoSize = true;
            this.lblProdVersion.Font = new System.Drawing.Font("Arial", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblProdVersion.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(79)))), ((int)(((byte)(140)))), ((int)(((byte)(152)))));
            this.lblProdVersion.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.lblProdVersion.Location = new System.Drawing.Point(220, 27);
            this.lblProdVersion.Name = "lblProdVersion";
            this.lblProdVersion.Size = new System.Drawing.Size(215, 32);
            this.lblProdVersion.TabIndex = 0;
            this.lblProdVersion.Text = "AcuityPlus™ 0.0";
            this.lblProdVersion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblModule
            // 
            this.lblModule.AutoSize = true;
            this.lblModule.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblModule.Location = new System.Drawing.Point(34, 114);
            this.lblModule.Name = "lblModule";
            this.lblModule.Size = new System.Drawing.Size(48, 13);
            this.lblModule.TabIndex = 1;
            this.lblModule.Text = "Module";
            // 
            // lblBuildNum
            // 
            this.lblBuildNum.AutoSize = true;
            this.lblBuildNum.Location = new System.Drawing.Point(34, 137);
            this.lblBuildNum.Name = "lblBuildNum";
            this.lblBuildNum.Size = new System.Drawing.Size(70, 13);
            this.lblBuildNum.TabIndex = 2;
            this.lblBuildNum.Text = "Build Number";
            // 
            // lblPatchLevel
            // 
            this.lblPatchLevel.AutoSize = true;
            this.lblPatchLevel.Location = new System.Drawing.Point(34, 164);
            this.lblPatchLevel.Name = "lblPatchLevel";
            this.lblPatchLevel.Size = new System.Drawing.Size(64, 13);
            this.lblPatchLevel.TabIndex = 3;
            this.lblPatchLevel.Text = "Patch Level";
            // 
            // lblServer
            // 
            this.lblServer.AutoSize = true;
            this.lblServer.Location = new System.Drawing.Point(34, 191);
            this.lblServer.Name = "lblServer";
            this.lblServer.Size = new System.Drawing.Size(71, 13);
            this.lblServer.TabIndex = 4;
            this.lblServer.Text = "Connected to";
            // 
            // lblCopyRight
            // 
            this.lblCopyRight.AutoSize = true;
            this.lblCopyRight.Location = new System.Drawing.Point(34, 217);
            this.lblCopyRight.Name = "lblCopyRight";
            this.lblCopyRight.Size = new System.Drawing.Size(51, 13);
            this.lblCopyRight.TabIndex = 5;
            this.lblCopyRight.Text = "Copyright";
            // 
            // pnlAboutForm
            // 
            this.pnlAboutForm.BackColor = System.Drawing.SystemColors.ControlLight;
            this.pnlAboutForm.Controls.Add(this.bttTechSupport);
            this.pnlAboutForm.Controls.Add(this.bttOK);
            this.pnlAboutForm.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlAboutForm.Location = new System.Drawing.Point(9, 247);
            this.pnlAboutForm.Name = "pnlAboutForm";
            this.pnlAboutForm.Size = new System.Drawing.Size(536, 44);
            this.pnlAboutForm.TabIndex = 6;
            // 
            // bttTechSupport
            // 
            this.bttTechSupport.Location = new System.Drawing.Point(276, 6);
            this.bttTechSupport.Name = "bttTechSupport";
            this.bttTechSupport.Size = new System.Drawing.Size(100, 31);
            this.bttTechSupport.TabIndex = 2;
            this.bttTechSupport.Text = "Support";
            this.bttTechSupport.UseVisualStyleBackColor = true;
            this.bttTechSupport.Click += new System.EventHandler(this.bttTechSupport_Click);
            // 
            // bttOK
            // 
            this.bttOK.Location = new System.Drawing.Point(161, 6);
            this.bttOK.Name = "bttOK";
            this.bttOK.Size = new System.Drawing.Size(100, 31);
            this.bttOK.TabIndex = 0;
            this.bttOK.Text = "OK";
            this.bttOK.UseVisualStyleBackColor = true;
            this.bttOK.Click += new System.EventHandler(this.bttOK_Click);
            // 
            // AboutForm
            // 
            this.AcceptButton = this.bttOK;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(554, 300);
            this.Controls.Add(this.pnlAboutForm);
            this.Controls.Add(this.lblCopyRight);
            this.Controls.Add(this.lblServer);
            this.Controls.Add(this.lblPatchLevel);
            this.Controls.Add(this.lblBuildNum);
            this.Controls.Add(this.lblModule);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AboutForm";
            this.Padding = new System.Windows.Forms.Padding(9);
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "About";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.pnlAboutForm.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblProdVersion;
        private System.Windows.Forms.Label lblModule;
        private System.Windows.Forms.Label lblBuildNum;
        private System.Windows.Forms.Label lblPatchLevel;
        private System.Windows.Forms.Label lblServer;
        private System.Windows.Forms.Label lblCopyRight;
        private System.Windows.Forms.Panel pnlAboutForm;
        private System.Windows.Forms.Button bttTechSupport;
        private System.Windows.Forms.Button bttOK;
        private System.Windows.Forms.PictureBox picLogo;

    }
}
