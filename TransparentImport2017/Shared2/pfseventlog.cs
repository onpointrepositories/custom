﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;        // for SQL access

namespace PfsShared
{
    public static class PFSEventLog
    {
        //
        // PFSEventLog: Add to the event log
        //
        // IF YOU ADD TO THESE LISTS, BE SURE TO ADD TO THE DATABASE (LOOKUP_VALUE) AS WELL.
        // DO NOT CHANGE ANY EXISTING NUMBERS!
        //
        const int MAX_EVENT_LIFE_IN_DAYS = 45;

        const string UNCLOSED_QUOTE_WARNING = "(unclosed quote)";

        public enum EventLogSource                  //Lookup list #66
        {
            EVENT_SOURCE_NONE = 0,
            EVENT_SOURCE_HL7 = 1,
            EVENT_SOURCE_BATCH_IMPORT = 2,          //class,midnight,staffing from text files
            EVENT_SOURCE_BATCH_EXPORT = 3,
            EVENT_SOURCE_REPORT_CALCULATIONS = 4,   //from AutoReport.exe and audit report
            EVENT_SOURCE_TRANSPARENT_IMPORT = 5,    //batch import
            EVENT_SOURCE_TRANSPARENT_MAPPING = 6,   //mapping only (generates an import file)
            EVENT_SOURCE_DATA_CONSISTENCY = 7,
            EVENT_SOURCE_DBUPGRADE = 8
        }

        public enum EventLogType                    //Lookup list #67
        {
            EVENT_TYPE_NONE = 0,
            EVENT_TYPE_INFO = 1,                    //generic message
            EVENT_TYPE_WARNING = 2,
            EVENT_TYPE_ERROR = 3,
            EVENT_TYPE_REJECT = 4
        }

        public enum EventLogCategory                //Lookup list #68
        {
            EVENT_CATEGORY_NONE = 0,                //generic category
            EVENT_CATEGORY_STARTUP_SHUTDOWN = 1,    //startup or shutdown message
            EVENT_CATEGORY_VALIDATION = 2,          //a validation error
            EVENT_CATEGORY_UNEXPECTED = 3,          //an unexpected (internal) error
            EVENT_CATEGORY_PROCESSED = 4            //a processed message
        }

        private static DateTime _lastEventDate;
        private static bool _doNotRemoveOldEvents = false;
   
        public static bool DoNotRemoveOldEvents
        {
            get { return _doNotRemoveOldEvents;  }
            set { _doNotRemoveOldEvents = value; }
        }

        //
        // Add an item to the event log
        //
        public static void DoAddEventLogEntry(
            EventLogSource eventSource,
            EventLogType eventType,
            EventLogCategory eventCategory,
            string description,                     // can be null
            string sourceText,                      // can be null
            int TCPPort,
            int unitID,
            int encounterID,
            int eventID,
            string mapper_version,
            string briefAudit,
            string verboseAudit
        )
        {
            // Don't save empty events
            if (string.IsNullOrEmpty(description)) return;

            string sql, src, columns, values;

            //Source text often has a trailing cr/lf - strip it here.
            //Convert null SourceText to null string or StripCrLf and Left$ will blow up.
            if (string.IsNullOrEmpty(sourceText))
                src = "";
            else
                src = sourceText.StripCrLf();

            // Start with the basic columns
            columns =
                "TIMESTAMP, EVENT_SOURCE, EVENT_TYPE, EVENT_CATEGORY" +
                ",DESCRIPTION, SOURCE_TEXT, TCP_PORT, UNIT_ID, ENCOUNTER_ID";
            values =
                "GETDATE()" +
                "," + (int)eventSource + 
                "," + (int)eventType + 
                "," + (int)eventCategory + 
                "," + PFSDBUtility.SQLString(description.Left(1024)) +
                "," + PFSDBUtility.SQLString(src) +                           // no limit [39118]
                "," + PFSDBUtility.SQLZeroToNull(TCPPort) +
                "," + PFSDBUtility.SQLZeroToNull(unitID) +
                "," + PFSDBUtility.SQLZeroToNull(encounterID);

            // Add these columns only if values are given
            // (These were added in 8.3.1 and this is trying to be version independent)
            if (eventID != 0) 
            {
                columns += ",TC_EVENT_ID, MAPPER_VERSION, BRIEF_AUDIT, VERBOSE_AUDIT";
                values +=
                    "," + eventID +
                    "," + PFSDBUtility.SQLString(mapper_version.Left(32)) +
                    "," + PFSDBUtility.SQLString(briefAudit) +
                    "," + PFSDBUtility.SQLString(verboseAudit);
            }

            sql = "INSERT INTO EVENT_LOG (" + columns + ") VALUES (" + values + ")";
            //Console.WriteLine(sql);

            // Do not throw an error because this is the place where errors get recorded so it gets recursive.
            // Either try again or exit quietly instead.
            while (true)
            {
                try
                {
                    var db = PFSDBUtility.NewSqlConnection();
                    var cmd = new SqlCommand(sql, db);
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    string msg = ex.Message.ToLower();

                    if (PFSDBUtility.IsFatalDatabaseError(msg))
                    {
                        return;                         // give up
                    }   
                    if (msg.Contains("duplicate key"))
                    {
                        //Try again with a new timestamp (.1 sec delay is fine)
                        PFSUtility.Sleep(100);
                        continue;
                    }
                    if (PFSDBUtility.IsRetryDatabaseError(msg))
                    {
                        //Try again if there is a timeout.  A search or delete may have locked the table? [7431]
                        PFSUtility.Sleep(100);
                        continue;
                    }
                    if (msg.Contains("unit_id"))
                    {
                        //We were given an invalid unit_id - try again without it
                        AddEventLogEntry (eventSource, eventType, eventCategory, description, src, TCPPort, 0, encounterID);
                        break;
                    }
                    if (msg.Contains("encounter_id"))
                    {
                        //We were given an invalid encounter_id - try again without it
                        AddEventLogEntry (eventSource, eventType, eventCategory, description, src, TCPPort, unitID, 0);
                        break;
                    }
                    if (msg.Contains("unclosed quotation"))
                    {
                        //Either description or source got an unclosed quote error; most likely the source
                        if (sourceText != UNCLOSED_QUOTE_WARNING)
                        {
                            //Try again without the source string; replace it with a warning
                            AddEventLogEntry (eventSource, eventType, eventCategory,
                                description, UNCLOSED_QUOTE_WARNING, TCPPort, unitID, encounterID);
                            break;
                        }
                        if (description != UNCLOSED_QUOTE_WARNING)
                        {
                            //Try again without the description (should never get here)
                            AddEventLogEntry (eventSource, eventType, eventCategory,
                                UNCLOSED_QUOTE_WARNING, UNCLOSED_QUOTE_WARNING, TCPPort, unitID, encounterID);
                        }
                        break;
                    }
                    // unexpected error
                    // Don't put up a message box because that would halt the interface
                    Console.WriteLine("AddEventLogEntry unexpected error: {0}", msg);
                    // Hope this doesn't cause an infinite loop...
                    PFSUtility.Sleep(500);
                    //throw new PfsValidationException("AddEventLogEntry unexpected error: " + ex.Message);
                    return;
                }

                // No error?  Done!
                break;
            }

            //Once a day remove old events
            //This is for the HL7 interface which runs continuously without startup/shutdown.
            if (_lastEventDate.Date != DateTime.Today)
            {
                try
                {
                    RemoveOldEvents();
                    _lastEventDate = DateTime.Today;
                }
                catch (Exception ex)
                {
                    //Don't put up a message box because that would halt the interface
                    Console.WriteLine("RemoveOldEvents unexpected error: {0}", ex.Message);
                }
            }

        }

        //
        // Generic (original) Add Log Entry
        //
        public static void AddEventLogEntry(
            EventLogSource eventSource,
            EventLogType eventType,
            EventLogCategory eventCategory,
            string description,                     // can be null
            string sourceText,                      // can be null
            int TCPPort,
            int unitID,
            int encounterID
        )
        {
            DoAddEventLogEntry(eventSource, eventType, eventCategory,
                description, sourceText, TCPPort, unitID, encounterID,
                0, "", "", "");
        }

        //
        // Transparent classification specific entry
        //
        public static void AddTransparentMappingEventLogEntry(
            string description,
            int unitID,
            int encounterID,
            int eventID,
            string mapperVersion,
            string briefAudit,
            string verboseAudit
        )
        {
            DoAddEventLogEntry (
                EventLogSource.EVENT_SOURCE_TRANSPARENT_MAPPING,
                EventLogType.EVENT_TYPE_INFO,
                EventLogCategory.EVENT_CATEGORY_PROCESSED,
                description, "", 0, unitID, encounterID,
                eventID, mapperVersion, briefAudit, verboseAudit);
        }




        //
        // Batch interfaces should call this during startup or shutdown.
        // HL7 calls this (indirectly) each midnight.
        //
        public static void RemoveOldEvents()
        {
            if (_doNotRemoveOldEvents) return;

            using (var cn = PFSDBUtility.NewSqlConnection())
            {
                DateTime dt = DateTime.Today.AddDays(-MAX_EVENT_LIFE_IN_DAYS);
                string sql = "DELETE FROM EVENT_LOG WHERE TIMESTAMP < " + PFSDBUtility.SQLDate(dt);
                //Console.WriteLine(sql);
                var cmd = new SqlCommand(sql, cn);
                cmd.CommandTimeout = 20 * 60;               // increase from 30 sec to 20 min
                cmd.ExecuteNonQuery();
            }
        }

    }
}
