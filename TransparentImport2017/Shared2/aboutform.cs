﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Windows.Forms;
using Microsoft.Win32;
using System.IO;
using System.Diagnostics;
using Utility = PfsShared.PFSUtility;
using DBUtility = PfsShared.PFSDBUtility;

namespace PfsShared
{
    partial class AboutForm : Form
    {
        public AboutForm()
        {
            DateTime dt;
            string exe_path = Assembly.GetEntryAssembly().Location;

            InitializeComponent();
            Utility.SetStdTheme(this);

            var acopy = (AssemblyCopyrightAttribute)Attribute.GetCustomAttribute(Assembly.GetEntryAssembly(), typeof(AssemblyCopyrightAttribute));
            // Note: the AssemblyVersionAttribute returns null; get file version instead.
            var afv = (AssemblyFileVersionAttribute)Attribute.GetCustomAttribute(Assembly.GetEntryAssembly(), typeof(AssemblyFileVersionAttribute));
            Version vrs = new Version(Application.ProductVersion);

            this.Text = "About";
            lblProdVersion.Text = Utility.AppProduct() + "™ " + Utility.AppVersion();

            lblModule.Text = Utility.AppTitle();
            lblPatchLevel.Text = Utility.AppPatchLevel();
            dt = File.GetLastWriteTime(exe_path);                       // modifed
            lblBuildNum.Text = String.Format("{0} {1}.{2}.{3} build {4}  {5}", 
                Path.GetFileName(exe_path), 
                vrs.Major, vrs.Minor, vrs.Build, 
                PfsShared.PFSGlobal.BUILD_NUMBER,
                dt);
            lblServer.Text = String.Format("Server={0}, Database={1}", DBUtility.ServerName(), DBUtility.DatabaseName());
            this.lblCopyRight.Text = acopy.Copyright;
        }

        private void bttOK_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void bttTechSupport_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Phone: 877.823.7263\n",
                            this.CompanyName + " Technical Support",
                            MessageBoxButtons.OK);
        }
    }
}
