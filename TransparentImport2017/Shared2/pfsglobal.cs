using System;
using System.Collections.Generic;
using System.Text;

namespace PfsShared
{
    public class PFSGlobal
    {
        public const double DATABASE_VERSION = 8.703;       //The current database version
        public const int BUILD_NUMBER = 6;                  //Remember to update DBUpgrade.cs

        // Special Unit IDs
        public const int UNIT_ID_UNKNOWN = -1;              // unit is not defined on our system
        public const int UNIT_ID_IN_TRANSIT = -2;           // patient is between units but still under care
        public const int UNIT_ID_ON_LEAVE = -3;             // patient is away for the weekend (long-term care)
        public const int UNIT_ID_OFF_SITE = -4;             // patient is no longer under care (ambulatory)
        public const int UNIT_ID_TEMPORARY = -5;            // patient is in OR, X-ray or hallway.

        // Special User IDs
        public const int USER_ID_UNKNOWN = -1;              // user is not defined on our system
        public const int USER_ID_DEFAULT = -2;              // default classification
        public const int USER_ID_IMPORT = -3;               // class import
        public const int USER_ID_SCANNER = -4;              // scanned classification
        public const int USER_ID_HL7 = -5;                  // HL7 interface

        // Special methodology ids
        public const int METH_ID_DO_NOT_CLASSIFY = -1;

        // Methodology ids
        public const int METH_ID_MEDSURG_VI = 1;
        public const int METH_ID_MENTAL_HEALTH_VI = 2;
        public const int METH_ID_EMERGE = 3;                // visit only
        public const int METH_ID_PERINATAL = 4;
        public const int METH_ID_AMBULATORY = 5;
        public const int METH_ID_DIALYSIS = 6;
        public const int METH_ID_PFSWM_INPATIENT = 7;
        public const int METH_ID_PFSWM_MENTAL_HEALTH = 8;
        public const int METH_ID_PFSWM_PERINATAL = 9;
        public const int METH_ID_ED_TRIAGE3 = 10;           // 3 types
        public const int METH_ID_ED_VISIT = 11;
        public const int METH_ID_ED_INPATIENT = 12;
        public const int METH_ID_ED_TRIAGE4 = 13;           // 4 types
        public const int METH_ID_ED_TRIAGE5 = 14;           // 5 types
        public const int METH_ID_INPATIENT = 15;
        public const int METH_ID_APLUS_AMBULATORY = 16;
        public const int METH_ID_APLUS_MENTAL_HEALTH = 17;
        public const int METH_ID_APLUS_INPATIENT2 = 18;
        public const int METH_ID_APLUS_PERINATAL1 = 19;
        public const int METH_ID_APLUS_INPATIENT2_1 = 20;
        public const int METH_ID_ED2_TRIAGE3 = 21;
        public const int METH_ID_ED2_TRIAGE4 = 22;
        public const int METH_ID_ED2_TRIAGE5 = 23;
        public const int METH_ID_ED2_VISIT = 24;
        public const int METH_ID_ED2_INPATIENT = 25;

        // These methodology ids form emerge sub-groups
        public const string METH_IDS_ED_TRIAGE = "10,13,14, 21,22,23";
        public const string METH_IDS_ED_VISIT = "3,11,24";
        public const string METH_IDS_ED_INPATIENT = "12,25";

        public const string METH_GROUP_INPATIENT = "I";
        public const string METH_GROUP_EMERGENCY = "E";
        public const string METH_GROUP_PERINATAL = "P";
        public const string METH_GROUP_AMBULATORY = "A";
        public const string METH_GROUP_CUSTOM = "C";

        // "Other" workload types
        public const int WORKLOAD_TYPE_TELEPHONE = 1;
        public const int WORKLOAD_TYPE_OTHER = 2;
        public const int MAX_OTHER_WORKLOAD_COLUMNS = 20;

        // Misc constants
        public const int CLASSIFICATION_MAX_LOS_DAYS = 7;
        public const int PROCEDURE_MAX_LOS_DAYS = 2;

        // Programs we might want to start
        //public const string BIN_PATH = @"..\bin\";
        //public const string ATSTAFF_STAFFING_EXPORT_EXE = "CustomStaffExport.exe";
        //public const string BATCH_INPUT_EXE_NAME = "BatchInput.exe";
        //public const string CLASSIFICATION_EXPORT_EXE_NAME = "ClassExport.exe";
        //public const string DATABOOK_EXTRACT_EXE_NAME = "DBExtract.exe";
        //public const string FINANCE_EXPORT_EXE_NAME = "FinanceExport.exe";
        //public const string FORECASTER_EXE_NAME = "StaffingForecaster.exe";
        //public const string OUTCOMES_REPORT_BUILDER_EXE_NAME = "OutcomesReportBuilder.exe";
        //public const string OUTCOMES_REPORT_GENERATOR_EXE_NAME = "OutcomesReportGenerator.exe";
        //public const string REPORTS_EXE_NAME = "Reports.exe";
        //public const string SCANTRON_EXE_NAME = "ScanClient.exe";
        //public const string STAFFING_EXPORT_EXE_NAME = "StaffingExport.exe";
        //public const string SYSMGR_EXE_NAME = "SysManager.exe";
        public const string TRANSPARENT_IMPORT_EXE_NAME = "TransparentImport.exe";

    }
}
