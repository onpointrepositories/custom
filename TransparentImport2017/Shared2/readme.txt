pfs.dbml:

Do not delete and recreate pfs.dbml - all the added relationships will be lost.
For example, none of the views will be related to their parent tables.

You can delete and recreate a table in the diagram if others do not depend on it.

-----
The table you are looking for can be hard to find because there is no "go to" command.
Make sure the Properties pane is open and select the table from the pulldown -- that table
will then be centered on the screen!

