//#define NET35                               // comment out if targeting higher than .NET 3.5

using System;
using System.Runtime.InteropServices;       // for Reg
using System.Text;                          // for Reg
using Microsoft.Win32;                      // for ODBC & registry
using System.Windows.Forms;                 // for MessageBox (also add reference)
using System.IO;                            // for Directory
using System.Diagnostics;                   // for Process
using System.Reflection;                    // for DataTable
using System.Collections.Generic;
using System.Drawing;
using System.ComponentModel;                // for Win32Exception
// Add a reference to System.Data.Linq

namespace PfsShared
{
    // Generic utility functions
    // These static functions can be called any time without creating an object

    public static class PFSUtility
    {

        /// <summary>
        /// AppCaption()
        /// </summary>
        /// <param name="title"></param>
        /// <returns>Caption name of the calling assembly</returns>
        public static string AppCaption(string title = "")
        {
            string result = AppCompany() + " " + AppProduct() + " ";

            if (!string.IsNullOrEmpty(title))
                result += title;
            else
                result += AppTitle();

            result += " " + AppVersion();

            return result;
        }

        public static string AppCompany()
        {
            var aca = (AssemblyCompanyAttribute)Attribute.GetCustomAttribute(Assembly.GetEntryAssembly(), typeof(AssemblyCompanyAttribute));
            return aca.Company;
        }

        public static string AppProduct()
        {
            var ata = (AssemblyProductAttribute)Attribute.GetCustomAttribute(Assembly.GetEntryAssembly(), typeof(AssemblyProductAttribute));
            return ata.Product;
        }

        public static string AppTitle()
        {
            var ata = (AssemblyTitleAttribute)Attribute.GetCustomAttribute(Assembly.GetEntryAssembly(), typeof(AssemblyTitleAttribute));
            return ata.Title;
        }

        // Format the application version for use in window titles and report footers
        public static string AppVersion()
        {
            var afv = (AssemblyFileVersionAttribute)Attribute.GetCustomAttribute(Assembly.GetEntryAssembly(), typeof(AssemblyFileVersionAttribute));
            return FormatAppVersion(afv.Version);
        }

        private static string FormatAppVersion(string version)
        {
            string result = version;
            result = result.Substring(0, 5);                // 8.0.0    - strip the build number
            result = StripTrailingDotZero(result);          // 8.0      - strip the patch number (if zero)
            return result;
        }

        public static string AppPatchLevel(string app_title = "")
        {
            var aca = (AssemblyCompanyAttribute)Attribute.GetCustomAttribute(Assembly.GetEntryAssembly(), typeof(AssemblyCompanyAttribute));
            var apa = (AssemblyProductAttribute)Attribute.GetCustomAttribute(Assembly.GetEntryAssembly(), typeof(AssemblyProductAttribute));

            int major = 0, minor = 0, revision = 0, patch = 0;
            string result = aca.Company + " ";

            if (string.IsNullOrEmpty(app_title))
                result += apa.Product;
            else if (app_title == "~")
                result += apa.Product + " " + AppTitle();
            else
                result += apa.Product + " " + app_title;

            result += " ";

            GetRegistryAppPatchLevel(ref major, ref minor, ref revision, ref patch);

            Version appVersion = new Version(Application.ProductVersion);

            if (appVersion.Major == major && appVersion.Minor == minor)
            {
                result += major + "." + minor;
                if (patch > 0)
                    result += "." + revision + "." + patch;
                else if (revision > 0)
                    result += "." + revision;
            }
            else
                result += appVersion.Major + "." + appVersion.Minor + "." + appVersion.Build;

            return result;
        }

        public static void GetRegistryAppPatchLevel(ref int major, ref int minor, ref int revision, ref int patch)
        {
            try
            {
                // This is always in the 32-bit registry
                string version = GetRegKey32(@"HKEY_LOCAL_MACHINE\SOFTWARE\Quadramed\AcuityPlus", "Version", "");

                if (!string.IsNullOrEmpty(version))
                {
                    string[] arr = version.Split('.');

                    int.TryParse(arr[0], out major);
                    int.TryParse((arr.GetUpperBound(0) >= 1 ? arr[1] : "0"), out minor);
                    int.TryParse((arr.GetUpperBound(0) >= 2 ? arr[2] : "0"), out revision);
                    int.TryParse((arr.GetUpperBound(0) >= 3 ? arr[3] : "0"), out patch);
                }
                else
                {
                    Version vrs = new Version(Application.ProductVersion);
                    major = vrs.Major;
                    minor = vrs.Minor;
                    revision = vrs.Build;
                    patch = 0;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        //=====================================================================
        // File system
        //=====================================================================
        public static string DefaultPathToWritableFolder(string subfolder)
        {
            // Windows 7 and later has a write-enabled C:\Users\Public folder.
            // Program Files cannot be written into unless we are running as admin.
            if (Directory.Exists(@"C:\Users\Public"))
            {
                if (!Directory.Exists(@"C:\Users\Public\AcuityPlus"))
                    Directory.CreateDirectory(@"C:\Users\Public\AcuityPlus");

                if (!Directory.Exists(@"C:\Users\Public\AcuityPlus\" + subfolder))
                    Directory.CreateDirectory(@"C:\Users\Public\AcuityPlus\" + subfolder);

                return @"C:\Users\Public\AcuityPlus\" + subfolder;
            }
            else
            {
                return Directory.GetCurrentDirectory() + @"\..\" + subfolder;
            }
        }

        public static string DefaultImportPath()
        {
            return DefaultPathToWritableFolder("load_me");
        }

        public static string DefaultExportPath()
        {
            return DefaultPathToWritableFolder("export");
        }

        public static string DefaultLogPath()
        {
            return DefaultPathToWritableFolder("log");
        }

        public static string DefaultArchivePath()
        {
            return DefaultPathToWritableFolder("archive");
        }

        //=====================================================================
        // Conversions
        //=====================================================================

        /// <summary>
        /// Convert YYYYMMDDHHMMSS (ISO format) to DateTime
        /// </summary>
        public static DateTime ISOToDateTime(string s)
        {
            int year = 1900, month = 1, day = 1, hour = 0, minute = 0, second = 0;

            if (s.Length >= 8)
            {
                //Assume we have YYYYMMDD
                year = Convert.ToInt32(s.Substring(0, 4));
                month = Convert.ToInt32(s.Substring(4, 2));
                day = Convert.ToInt32(s.Substring(6, 2));

                if (s.Length >= 10)
                {
                    hour = Convert.ToInt32(s.Substring(8, 2));
                }
                if (s.Length >= 12)
                {
                    minute = Convert.ToInt32(s.Substring(10, 2));
                }
                if (s.Length >= 14)
                {
                    second = Convert.ToInt32(s.Substring(12, 2));
                }
            }
            try
            {
                return new DateTime(year, month, day, hour, minute, second);
            }
            catch (ArgumentException)
            {
                throw new ArgumentException("Invalid date/time: " + s);
            }
        }

        /// <summary>
        /// Convert YYYYMMDD (ISO format) to DateTime (at midnight)
        /// </summary>
        public static DateTime ISODateToDateTime(string s)
        {
            return ISOTimeToDateTime(s);
        }

        /// <summary>
        /// Convert HHMMSS to DateTime (on 1/1/1900)
        /// </summary>
        public static DateTime ISOTimeToDateTime(string s)
        {
            int year = 1900, month = 1, day = 1, hour = 0, minute = 0, second = 0;

            if (s.Length >= 2)
                hour = Convert.ToInt32(s.Substring(0, 2));
            if (s.Length >= 4)
                minute = Convert.ToInt32(s.Substring(2, 2));
            if (s.Length >= 6)
                second = Convert.ToInt32(s.Substring(4, 2));
            return new DateTime(year, month, day, hour, minute, second);
        }

        public static DateTime CombineDateAndTime(DateTime d, DateTime t)
        {
            return new DateTime(d.Year, d.Month, d.Day, t.Hour, t.Minute, t.Second);
        }

        /// <summary>
        /// Convert DateTime to ISO DateTime YYYYMMDDHHMMSS
        /// </summary>
        public static string DateTimeToISODateTime(DateTime d)
        {
            return d.ToString("yyyyMMddHHmmss");
        }
        /// <summary>
        /// Convert DateTime to ISO Date YYYYMMDD
        /// </summary>
        public static string DateTimeToISODate(DateTime d)
        {
            return d.ToString("yyyyMMdd");
        }
        /// <summary>
        /// Convert DateTime to ISO Time HHMMSS
        /// </summary>
        public static string DateTimeToISOTime(DateTime d)
        {
            return d.ToString("HHmmss");
        }


        public static double DateDiffInYears(DateTime? d1, DateTime? d2)
        {
            if ((d1 == null) || (d2 == null)) return -1;
            TimeSpan ts = ((DateTime)d2 - (DateTime)d1);
            return (ts.TotalDays / 365.24219);
        }
        public static double DateDiffInDays(DateTime? d1, DateTime? d2)
        {
            if ((d1 == null) || (d2 == null)) return -1;
            TimeSpan ts = ((DateTime)d2 - (DateTime)d1);
            return ts.TotalDays;
        }
        public static double DateDiffInHours(DateTime? d1, DateTime? d2)
        {
            if ((d1 == null) || (d2 == null)) return -1;
            TimeSpan ts = ((DateTime)d2 - (DateTime)d1);
            return ts.TotalHours;
        }
        public static double DateDiffInMinutes(DateTime? d1, DateTime? d2)
        {
            if ((d1 == null) || (d2 == null)) return -1;
            TimeSpan ts = ((DateTime)d2 - (DateTime)d1);
            return ts.TotalMinutes;
        }

        /// <summary>
        /// Select the smaller of the two datetimes.  Null counts as zero.
        /// </summary>
        public static DateTime MinDateTime(DateTime? d1, DateTime? d2)
        {
            if (d1 == null)
                return d2 ?? DateTime.MinValue;
            if (d2 == null)
                return d1 ?? DateTime.MinValue;
            // Neither is null
            if (((DateTime)d1).CompareTo((DateTime)d2) <= 0)
                return (DateTime)d1;
            else
                return (DateTime)d2;
        }

        /// <summary>
        /// Select the larger of the two datetimes.  Null counts as zero.
        /// </summary>
        public static DateTime MaxDateTime(DateTime? d1, DateTime? d2)
        {
            if (d1 == null)
                return d2 ?? DateTime.MinValue;
            if (d2 == null)
                return d1 ?? DateTime.MinValue;
            // Neither is null
            if (((DateTime)d1).CompareTo((DateTime)d2) >= 0)
                return (DateTime)d1;
            else
                return (DateTime)d2;
        }

        /// <summary>
        /// Is this a date (or is it null)?  Used to help converted VB code
        /// </summary>
        public static bool IsDate(DateTime? dt)
        {
            return (dt != null);
        }

        // ** DateOnly?
        // ** TimeOnly?



        //=====================================================================
        // Math
        //=====================================================================
        public static double DivideWithoutError(double num, double dem)
        {
            if (dem != 0)
                return num / dem;
            else
                return 0;
        }

        //=====================================================================
        // Misc
        //=====================================================================
        public static void Sleep(int ms)
        {
            System.Threading.Thread.Sleep(ms);
        }

        private static string StripTrailingDotZero(string s)
        {
            string result = s;
            if (s.EndsWith(".0"))
                result = s.Substring(0, s.Length - 2);
            return result;
        }

        //=====================================================================
        // Strings
        //=====================================================================
        // (see Extensions.cs)

        //=====================================================================
        // Exception handling
        //=====================================================================

        public static void MessageBoxError(System.Exception ex)
        {
            CursorPopAll();                     // get rid of hourglass
            MessageBoxError(ex, "");
        }

        // Describe an exception
        public static void MessageBoxError(System.Exception ex, string more)
        {
            string title = "Error", msg, short_stack_trace = "";
            string[] tmp;

            if (ex is PfsValidationException) {
                title = "Validation Error";
                short_stack_trace = "";
            }
            else
            {
                title = "Unexpected Error";
                // The full stack trace can get crazy with all the system calls.
                // Make a short stack trace that just includes our stuff.
                if (ex.StackTrace != null)
                {
                    tmp = ex.StackTrace.Split('\n');
                    short_stack_trace = "";
                    foreach (string s in tmp)
                    {
                        // look for our stuff
                        // ** How do we keep up to date with all our namespaces?
                        // ** The best bet is to always start the namespace with Pfs.
                        if (s.ToLower().Contains("pfs") || s.Contains("Reports2"))
                        {
                            short_stack_trace += s.Substring(0, s.IndexOf('(')).TrimStart() + "()" + "\n\t";      // add to short trace
                        }
                    }
                }
            }

            msg = ex.Message;
            if (ex.InnerException != null)
            {
                //If there is an inner message, the outer is usually worthless
                msg += "  " + ex.InnerException.Message;     // detail message
            }

            if (short_stack_trace.Length > 0)
            {
                msg += "\n\nError occurred: \n\t" + short_stack_trace;
            }


            if (more.Length > 0)
            {
                msg += "\nError while " + more;
            }

            MessageBox.Show(msg, title, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        //=====================================================================
        // GUI functions
        //=====================================================================
        static int _cursor_depth = 0;

        public static void CursorPushHourglass()
        {
            if (_cursor_depth == 0)
                Cursor.Current = Cursors.WaitCursor;
            _cursor_depth++;
        }

        public static void CursorPop()
        {
            if (_cursor_depth > 0)
            {
                _cursor_depth--;
                if (_cursor_depth == 0)
                    Cursor.Current = Cursors.Default;
            }
        }

        public static void CursorPopAll()
        {
            _cursor_depth = 0;
            Cursor.Current = Cursors.Default;
        }

        //=====================================================================
        // Processes
        //=====================================================================

        /// <summary>
        /// Execute a program and wait for it to finish.
        /// Returns the result code.
        /// </summary>
        /// <returns>int result_code</returns>
        public static int ExecuteAndWait(string path)
        {
            var proc = new Process();

            proc.StartInfo.FileName = path;
            proc.EnableRaisingEvents = true;
            proc.Start();
            proc.WaitForExit();

            return proc.ExitCode;
        }

        /// <summary>
        /// Execute a program and wait for it to finish.
        /// Returns the result code. 
        /// </summary>
        /// <param name="exePath"></param>
        /// <param name="cmd"></param>
        /// <returns>exit code</returns>
        public static int ExecuteAndWait(string exePath, string cmd, string logPath)
        {
            int exitCode = 0;

            Process process = new Process();
            process.StartInfo.FileName = exePath;
            process.StartInfo.Arguments = cmd;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            //process.StartInfo.RedirectStandardError = true;
            process.StartInfo.CreateNoWindow = false;
            process.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
            process.EnableRaisingEvents = true;
            process.Start();

            string output = process.StandardOutput.ReadToEnd();
            process.WaitForExit();

            exitCode = process.ExitCode;
            process.Close();

            if (!string.IsNullOrEmpty(logPath))
            {
                using (StreamWriter writer = new StreamWriter(logPath))
                {
                    writer.Write(output);
                }
            }
            return exitCode;

        }

        // Is this a 32 or 64-bit process?
        public static int CurrentProcessBitSize()
        {
            return 8 * IntPtr.Size;                 // IntPtr is a platform-specific structure
        }

        //==============================================================================
        // File System
        //==============================================================================
        public static bool IsFileInPath(string fileName, ref string foundHere)
        {
            foundHere = "";
            string[] results = Environment.GetEnvironmentVariable("PATH").Split(';');
            foreach (string path in results)
            {
                if (File.Exists(path + "\\" + fileName))
                {
                    foundHere = path;
                    return true;
                }
            }

            return false;
        }


        /// <summary>
        /// GetParentFolders(): check if the start up program runs under VS debugger or Stand Alone application
        /// </summary>
        /// <returns></returns>
        public static string GetParentFolders()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory;

            if (path.EndsWith(@"\Debug\") ||
                path.EndsWith(@"\Release\"))
                path = Path.GetFullPath(Path.Combine(path, @"..\..\..\"));
            else
                path = Path.GetFullPath(Path.Combine(path, @"..\"));

            return path;
        }

        public static string ExpandPath(string relativePath)
        {
            string result = "";
            if (relativePath.StartsWith(@".\"))             // path begin with .\
                result = AppDomain.CurrentDomain.BaseDirectory + relativePath.Substring(2);
            else if (relativePath.StartsWith(@"..\"))       // path begin with ..\
                result = GetParentFolders() + relativePath.Substring(3);
            else if (relativePath.Substring(1, 2) == @":\") // path begins with X:\
                result = relativePath;
            else if (relativePath.StartsWith(@"\\"))        // path begins with \\<server>
                result = relativePath;
            else if (relativePath.StartsWith(@"\"))         // path begins with \
            {
                if (relativePath.Substring(1, 1) == ":")
                    result = AppDomain.CurrentDomain.BaseDirectory.Substring(0, 2) + relativePath;
                else
                    result = relativePath;
            }
            return result;
        }

        public static string AddTrailingBackslash(string s)
        {
            string result = s;
            if (!s.EndsWith("\\"))
                return s += "\\";
            return result;
        }

        public static string FormatErrorMsg(string method, string err)
        {
            return string.Format("[{0}] Error: {1}", method, err);
        }


        //=====================================================================
        // Look and Feel
        //=====================================================================
        
        public static Icon HarrisIcon ()
        {
            // Use an imbedded resource (no external files); Add with project properties, Resources
            // Do not copy the original files to the output directory
     
            //return PfsShared.Properties.Resources.Harris;
            //return PfsShared.Properties.Resources.Harris_white;
            return PfsShared.Properties.Resources.Harris_button;
        }

        /// <summary>
        /// SetStdTheme()
        /// Set all the backcolor of the controls of window form to White
        /// </summary>
        /// <param name="form"></param>
        public static void SetStdTheme(Form form, bool loadHarrisIcon = false)
        {
            try
            {
                if (loadHarrisIcon)
                {
                    form.Icon = HarrisIcon();
                }

                if (UseFlatWhiteTheme())
                {
                    form.BackColor = System.Drawing.Color.White;

                    foreach (Control ctrl in form.Controls)
                    {
                        SetControlBackColor(ctrl);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static void SetControlBackColor(Control control)
        {

            if (!(control is StatusStrip) &&
                !(control is MenuStrip) &&
                !(control is ContextMenuStrip) &&
                !(control is ToolStrip) &&
                !(control is ToolStripContainer) &&
                !(control is Button)
            )
                control.BackColor = System.Drawing.Color.White;

        }

        /// <summary>
        /// UseFlatWhiteTheme()
        /// Detect OS Version
        /// </summary>
        /// <returns>
        /// Return true if it's greater or equal to 6.2 (Window 8 and beyond)
        /// Return false otherwise
        /// </returns>
        private static bool UseFlatWhiteTheme()
        {
            double versionNo;
            string versionFormat = Environment.OSVersion.Version.Major.ToString() + "." + Environment.OSVersion.Version.Minor.ToString();

            return ((double.TryParse(versionFormat, out versionNo) && versionNo >= 6.2));
        }


        //================================================================================================
        // Registry
        // Get 64-bit registry values from 32-bit processes.
        // This is much easier in .NET 4.5 but .NET 3.5 needs to make Windows API calls.
        //================================================================================================
#if (NET35)
        private static class RegHive
        {
            public static UIntPtr HKEY_LOCAL_MACHINE = new UIntPtr(0x80000002u);
            public static UIntPtr HKEY_CURRENT_USER = new UIntPtr(0x80000001u);
        }

        private enum RegSAM
        {
            QueryValue = 0x0001,
            SetValue = 0x0002,
            CreateSubKey = 0x0004,
            EnumerateSubKeys = 0x0008,
            Notify = 0x0010,
            CreateLink = 0x0020,
            WOW64_32Key = 0x0200,
            WOW64_64Key = 0x0100,
            WOW64_Res = 0x0300,
            Read = 0x00020019,
            Write = 0x00020006,
            Execute = 0x00020019,
            AllAccess = 0x000f003f
        }

        [DllImport("Advapi32.dll")]
        static extern int RegOpenKeyEx(
            UIntPtr     hKey,
            string      lpSubKey,
            uint        ulOptions,
            int         samDesired,
            out int     phkResult);

        [DllImport("Advapi32.dll")]
        static extern int RegCreateKeyEx(
            UIntPtr     hKey,
            string      lpSubKey,
            int         lpReserved,             // 0
            uint        lpClass,                // optional
            uint        ulOptions,
            int         samDesired,
            uint        lpSecurityAtttributes,  // optional
            out int     phkResult,
            uint        lpDisposition);         // optional

        [DllImport("Advapi32.dll")]
        static extern uint RegCloseKey(int hKey);

        [DllImport("advapi32.dll", EntryPoint = "RegQueryValueEx")]
        public static extern int RegQueryValueEx(
            int         hKey,
            string      lpValueName,
            int         lpReserved,
            ref uint    lpType,
            StringBuilder buf,
            ref uint    lpcbData);

        //[DllImport("advapi32.dll", EntryPoint = "RegSetValueEx")]
        //public static extern int RegSetValueEx(
        //    int         hKey,
        //    string      lpValueName,
        //    int         lpReserved,
        //    ref uint    lpType,
        //    byte[]      lpData,
        //    ref uint    lpcbData);

        
        // The .NET Registry.GetValue() function wants HKEY_LOCAL_MACHINE as part of the key name so we're doing the same here.
        // The Windows function, however, wants a constant for HKLM and it wants the key to start at "Software", etc.
        // This will split the keyName into hive and subkey.
        private static void SplitKeyIntoHiveAndSubkey(string keyName, out UIntPtr hiveKey, out string subkeyName)
        {
            const string HKLM = "HKEY_LOCAL_MACHINE";
            const string HKCU = "HKEY_CURRENT_USER";

            if (keyName.Left(HKLM.Length) == HKLM)
            {
                hiveKey = RegHive.HKEY_LOCAL_MACHINE;
                subkeyName = keyName.Remove(0, HKLM.Length + 1);
            }
            else if (keyName.Left(HKCU.Length) == HKCU)
            {
                hiveKey = RegHive.HKEY_CURRENT_USER;
                subkeyName = keyName.Remove(0, HKCU.Length + 1);
            }
            else
            {
                throw new ArgumentException("Only " + HKLM + " and " + HKCU + " are supported at this time");
            }
        }

        /// <summary>
        /// GetRegKey()
        /// Get a value from the registry (native access 32/64 bit)
        /// </summary>
        public static string GetRegKey(string keyName, string propertyName, string defaultValue)
        {
            return GetRegKey6432(keyName, 0, propertyName, defaultValue);
        }

        /// <summary>
        /// GetRegKey32()
        /// Get a value from the 32-bit registry (even if you are a 64-bit process)
        /// Do not include Wow6432Node in the path.
        /// </summary>
        public static string GetRegKey32(string keyName, string propertyName, string defaultValue)
        {
            return GetRegKey6432(keyName, RegSAM.WOW64_32Key, propertyName, defaultValue);
        }

        /// <summary>
        /// GetRegKey64()
        /// Get a value from the 64-bit registry (even if you are a 32-bit process)
        /// </summary>
        public static string GetRegKey64(string keyName, string propertyName, string defaultValue)
        {
            return GetRegKey6432(keyName, RegSAM.WOW64_64Key, propertyName, defaultValue);
        }

        // Get a value from the registry (do not create a new key)
        // Return the default string if the key does not exist; >> if the default value is null, raise an exception.
        // NOTE: These are Windows API calls.  This can be done in .NET but not until version 4.0 (with RegistryKey and RegistryView)
        // We are currently using .NET 3.5 so the .NET solution is not available.
        private static string GetRegKey6432(string keyName, RegSAM in32or64key, string propertyName, string defaultValue)
        {
            int hkey = 0;
            string result = null, subkey;
            UIntPtr hiveKey;

            try
            {
                SplitKeyIntoHiveAndSubkey(keyName, out hiveKey, out subkey);

                // Open the registry folder
                int lResult = RegOpenKeyEx(hiveKey, subkey, 0, (int)RegSAM.QueryValue | (int)in32or64key, out hkey);
                if (lResult == 0)
                {
                    uint lpType = 0;
                    var buf = new StringBuilder(8192);
                    uint lpcbData = (uint)buf.MaxCapacity;
                    // Because this is a Windows API call, it will not raise an exception; must check lResult
                    lResult = RegQueryValueEx(hkey, propertyName, 0, ref lpType, buf, ref lpcbData);
                    // lpcbData now contains the value length
                    if (lResult == 0)
                    {
                        result = buf.ToString();                        // convert ASCII-Z string to unicode
                    }
                    else if (defaultValue == null)
                    {
                        // raise a validation error instead of an exception in order to hide the stack trace
                        string errMsg = new Win32Exception(lResult).Message;
                        throw new PfsValidationException("Could not get " + keyName + "\\" + propertyName + "; error " + lResult + " - " + errMsg);
                    }
                }
                else
                {
                    if (defaultValue == null)
                    {
                        string errMsg = new Win32Exception(lResult).Message;
                        throw new PfsValidationException("Could not find " + keyName + "; error " + lResult + " - " + errMsg);
                    }
                }
            }
            finally
            {
                if (hkey != 0) RegCloseKey(hkey);                       // success?  Close
            }

            if (string.IsNullOrEmpty(result)) result = defaultValue;
            return result;
        }


        ///// <summary>
        ///// SetRegKey64()
        ///// Set a value in the 64-bit registry (even if you are a 32-bit process)
        ///// </summary>
        //public static void SetRegKey64(string keyName, string propertyName, string value)
        //{
        //    SetRegKey6432(keyName, RegSAM.WOW64_64Key, propertyName, value);
        //}

        ///// <summary>
        ///// GetRegKey32()
        ///// Get a value from the 32-bit registry (even if you are a 64-bit process)
        ///// Note: Do not include Wow6432Node in the path.
        ///// </summary>
        //public static void SetRegKey32(string keyName, string propertyName, string value)
        //{
        //    SetRegKey6432(keyName, RegSAM.WOW64_32Key, propertyName, value);
        //}

        //private static void SetRegKey6432(string keyName, RegSAM in32or64key, string propertyName, string value)
        //{
        //    int hkey = 0;
        //    string subkey;
        //    UIntPtr hiveKey;

        //    try
        //    {
        //        GetRegHive(keyName, out hiveKey, out subkey);

        //        int lResult = RegCreateKeyEx(hiveKey, subkey, 0, 0, 0, (int)RegSAM.CreateSubKey | (int)RegSAM.SetValue | (int)in32or64key, 0, out hkey, 0);
        //        if (lResult != 0) throw new ArgumentException(string.Format(@"Can't create {0}\{1}; error {2}", keyName, subkey, lResult));
        //        uint lpType = 0;
        //        byte[] buf = Encoding.UTF8.GetBytes(value);                         // convert Unicode to ASCII
        //        uint lpcbData = (uint)value.Length;
        //        lResult = RegSetValueEx(hkey, propertyName, 0, ref lpType, buf, ref lpcbData);
        //        if (lResult == 998) throw new ArgumentException("Access Denied.  Are you running as administrator?");
        //        if (lResult != 0) throw new ArgumentException(string.Format(@"Can't set {0}\{1}; error {2}", keyName, subkey, lResult));
        //    }
        //    finally
        //    {
        //        if (hkey != 0) RegCloseKey(hkey);
        //    }
        //}
#else
        // The .NET Registry.GetValue() function wants HKEY_LOCAL_MACHINE as part of the key name so we're doing the same here.
        // The OpenBaseKey function, however, wants a constant for HKLM and it wants the key to start at "Software", etc.
        // This will split the keyName into hive and subkey.
        private static void SplitKeyIntoHiveAndSubkey(string keyName, out RegistryHive hive, out string subkeyName)
        {
            const string HKLM = "HKEY_LOCAL_MACHINE";
            const string HKCU = "HKEY_CURRENT_USER";

            if (keyName.Left(HKLM.Length) == HKLM)
            {
                hive = RegistryHive.LocalMachine;
                subkeyName = keyName.Remove(0, HKLM.Length + 1);
            }
            else if (keyName.Left(HKCU.Length) == HKCU)
            {
                hive = RegistryHive.CurrentUser;
                subkeyName = keyName.Remove(0, HKCU.Length + 1);
            }
            else
            {
                throw new ArgumentException("Only " + HKLM + " and " + HKCU + " are supported at this time");
            }
        }

        /// <summary>
        /// GetRegKey()
        /// Get a value from the registry (native access 32/64 bit)
        /// </summary>
        public static string GetRegKey(string keyName, string propertyName, string defaultValue)
        {
            return Registry.GetValue(keyName, propertyName, defaultValue).ToString();
        }

        /// <summary>
        /// GetRegKey32()
        /// Get a value from the 32-bit registry (even if you are a 64-bit process)
        /// Do not include Wow6432Node in the path.
        /// </summary>
        public static string GetRegKey32(string keyName, string propertyName, string defaultValue)
        {
            return GetRegKey6432(keyName, RegistryView.Registry32, propertyName, defaultValue);
        }

        /// <summary>
        /// GetRegKey64()
        /// Get a value from the 64-bit registry (even if you are a 32-bit process)
        /// </summary>
        public static string GetRegKey64(string keyName, string propertyName, string defaultValue)
        {
            return GetRegKey6432(keyName, RegistryView.Registry64, propertyName, defaultValue);
        }

        // Get a value from the registry (do not create a new key)
        // Return the default string if the key does not exist;     >> if the default value is null, raise an exception.
        private static string GetRegKey6432(string keyName, RegistryView regView, string propertyName, string defaultValue)
        {
            object obj = null;
            string result = null, subkeyName = "";
            RegistryHive hiveKey;

            SplitKeyIntoHiveAndSubkey(keyName, out hiveKey, out subkeyName);

            using (RegistryKey baseKey = RegistryKey.OpenBaseKey(hiveKey, regView))
            {
                if (baseKey != null)
                {
                    using (RegistryKey localKey = baseKey.OpenSubKey(subkeyName))
                    {
                        if (localKey != null)
                        {
                            obj = localKey.GetValue(propertyName);

                            if (obj != null)
                                result = obj.ToString();
                        }
                    }
                }
            }

            if ((result == null) && (defaultValue != null))
                result = defaultValue;

            if (result == null)
                throw new PfsValidationException("Could not find " + keyName + "\\" + propertyName);
            else
                return result;
        }

#endif

        //=====================================================================
        // Misc Registry
        //=====================================================================

        /// <summary>
        /// Is this a reserved value name in the AcuityPlus registry?
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static bool IsReservedRegistryName(string s)
        {
            switch (s)
            {
                case "Database":
                case "Directory":
                case "Group":
                case "Server":
                case "Version":
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Is this a server name in the AcuityPlus registry?
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static bool IsAServerNameInRegistry(string s)
        {
            return !IsReservedRegistryName(s);
        }

        /// <summary>
        /// Returns an open registry key pointing to the AcuityPlus folder
        /// </summary>
        /// <param name="writable"></param>
        /// <returns></returns>
        public static RegistryKey AcuityPlusRegistryKey(bool writable = false)
        {
            const string keyname = @"Software\Quadramed\AcuityPlus";
            var result = Registry.LocalMachine.OpenSubKey(keyname, writable);
            if (result != null) return result;

            // Create the missing folder
            return Registry.LocalMachine.CreateSubKey(keyname);
        }

        //=====================================================================
        // Dialog boxes
        //=====================================================================

        /// <summary>
        /// Show an "About" box with information from the calling assembly
        /// </summary>
        public static void ShowAboutBox()
        {
            var frm = new PfsShared.AboutForm();
            frm.ShowDialog();
        }

        /// <summary>
        /// A simple form similar to the VB InputBox.
        /// The main difference is this returns a DialogResult -- i.e. you can cancel.
        /// </summary>
        /// <param name="promptText"></param>
        /// <param name="title"></param>
        /// <param name="value"></param>
        /// <param name="default_value"></param>
        /// <returns></returns>
        public static DialogResult InputBox(string promptText, string caption,  out string value, string default_value = "")
        {
            Form form = new Form();
            Label label = new Label();
            TextBox textBox = new TextBox();
            Button buttonOk = new Button();
            Button buttonCancel = new Button();

            form.Text = caption;
            label.Text = promptText;
            textBox.Text = default_value;

            buttonOk.Text = "OK";
            buttonCancel.Text = "Cancel";
            buttonOk.DialogResult = DialogResult.OK;
            buttonCancel.DialogResult = DialogResult.Cancel;

            label.SetBounds(9, 20, 372, 13);
            textBox.SetBounds(12, 36, 372, 20);
            buttonOk.SetBounds(228, 72, 75, 23);
            buttonCancel.SetBounds(309, 72, 75, 23);

            label.AutoSize = true;
            textBox.Anchor = textBox.Anchor | AnchorStyles.Right;
            buttonOk.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;
            buttonCancel.Anchor = AnchorStyles.Bottom | AnchorStyles.Right;

            form.ClientSize = new Size(396, 107);
            form.Controls.AddRange(new Control[] { label, textBox, buttonOk, buttonCancel });
            form.ClientSize = new Size(Math.Max(300, label.Right + 10), form.ClientSize.Height);
            form.FormBorderStyle = FormBorderStyle.FixedDialog;
            form.StartPosition = FormStartPosition.CenterScreen;
            form.MinimizeBox = false;
            form.MaximizeBox = false;
            form.AcceptButton = buttonOk;
            form.CancelButton = buttonCancel;

            DialogResult dialogResult = form.ShowDialog();
            value = textBox.Text;
            return dialogResult;
        }


    }
}
