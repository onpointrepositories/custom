﻿using System;
using System.Data.SqlClient;
using System.Diagnostics;                   // for debug
using System.Data;
using System.Net;
//
// Database utilities
//
namespace PfsShared
{
    public class PfsConnectionInfo
    {
        public enum AuthenticationMode
        {
            SQLServerAuthentication = 0,
            WindowsAuthentication = 1
        }
        public string server;
        public string dbname;
        public AuthenticationMode authentication;
        public string user, password;

        public PfsConnectionInfo() { }
        public PfsConnectionInfo(PfsConnectionInfo other)       // Copy
        {
            server = other.server;
            dbname = other.dbname;
            authentication = other.authentication;
            user = other.user;
            password = other.password;
        }
        public PfsConnectionInfo Clone()                        // Clone
        {
            return new PfsConnectionInfo(this);
        }
        public bool Equals(PfsConnectionInfo other)             // compare values
        {
            return (other.server == server) && (other.dbname == dbname) && (other.user == user) && (other.password == password) && (other.authentication == authentication);
        }
    }

    public static class PFSDBUtility
    {  
        //=====================================================================
        // Database Connections
        //=====================================================================

        private static void EnableTLS12()
        {
            // .NET 4.5 does not enable TLS 1.2 by default.  We can add it here:
            System.Net.ServicePointManager.SecurityProtocol |= (SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12);
        }

        /// <summary>
        /// NewSqlConnection()
        /// Open a connection to the SQL Server database.
        /// Our default timeout is 25 seconds (up from 15 in .NET)
        /// This will retry once if the attempt times out.  The timeout will be increased by 1.5.
        /// </summary>
        public static System.Data.SqlClient.SqlConnection NewSqlConnection(int timeout = 25, int retry_count = 0)
        {
            try
            {
                EnableTLS12();
                SqlConnection cn = new SqlConnection(MakeSqlConnectionString(GetSqlConnectionInfo(), timeout));
                cn.Open();
                return cn;
            }
            catch (Exception ex)
            {
                // Sometimes we get an "access denied" message for a timeout so check for both
                if (ex.Message.Contains("timeout") || ex.Message.Contains("access"))
                {
                    Debug.WriteLine("timeout={0}, retry={1}", timeout, retry_count);
                    if (retry_count < 1)
                    {
                        return NewSqlConnection(3*timeout/2, retry_count + 1);
                    }
                }
                throw;
            }
        }

        /// <summary>
        /// NewOleDbConnection()
        /// Open a connection to an Access database
        /// </summary>
        public static System.Data.OleDb.OleDbConnection NewAccessDbConnection(string db_name)
        {
            var cn = new System.Data.OleDb.OleDbConnection( MakeAccessConnectionString(db_name));
            cn.Open();
            return cn;
        }

        /// <summary>
        /// Returns an open context for use with LINQ commands.
        /// </summary>
        public static PfsDataContext NewPfsDataContext()
        {
            EnableTLS12();
            return new PfsDataContext( MakeSqlConnectionString( GetSqlConnectionInfo()));
        }


        //=====================================================================
        // Database Connection support
        //=====================================================================

        public static PfsConnectionInfo GetSqlConnectionInfo()
        {
            string server, dbname;

            // Find out what server and database to use.
            string key = @"HKEY_LOCAL_MACHINE\SOFTWARE\Quadramed\AcuityPlus";
            //                                   key name       default value
            server = PFSUtility.GetRegKey64(key, "Server",      null);          // no default; error if not found
            dbname = PFSUtility.GetRegKey64(key, "Database",    "pfs");         // default to "pfs"

            // This looks for a server specific login and decrypts it, or it returns the default login
            var result = PFSEncryption.GetServerLoginInfo(server, dbname);      // get user and password (or default)

            return result;
        }

        /// <summary>
        /// MakeSqlConnectionString()
        /// Use to open the SQL Server database
        /// </summary>
        public static string MakeSqlConnectionString(PfsConnectionInfo info, int timeout)
        {
            string result;

            if (String.IsNullOrEmpty(info.server))
                throw new MissingFieldException("Missing server name");
            if (info.dbname == "master")                    // wrong! - cannot be master
                info.dbname = "pfs";

            result =
                "Data Source="       + info.server +
                "; Initial Catalog=" + info.dbname;
            if (info.authentication == PfsConnectionInfo.AuthenticationMode.WindowsAuthentication)
            {
                result += "; Integrated Security=true";
            }
            else
            {
                result +=
                    "; User Id="  + info.user +
                    "; Password=" + info.password;
            }
            result +=
                "; Connection Timeout=" + timeout +         // default is 15 seconds
                "; Asynchronous Processing=true" +          // For DBUpgrade and DBExtract
                "; MultipleActiveResultSets=true";
            return result;
        }

        public static string MakeSqlConnectionString(PfsConnectionInfo info)
        {
            return MakeSqlConnectionString(info, 20);
        }

        /// <summary>
        /// MakeOleDbConnectionString()
        /// Use to open an Access 2007 database (.accdb)
        /// </summary>
        public static string MakeAccessConnectionString(string db_name)
        {
            return @"Provider=Microsoft.ACE.OLEDB.12.0; Data Source=" + db_name;
        }

        /// <summary>
        /// MakeSqlConnectionStringForAccessSql()
        /// Imbed a SQL Server connection in an Access SQL command.
        /// </summary>
        public static string MakeSqlConnectionStringForAccessSql()
        {
            var info = GetSqlConnectionInfo();
            string result;

            // ODBC default SQL Server driver
            //result = "[ODBC; DRIVER=SQL Server";                      // TLS 1.0: (SQLSRV32.DLL)
            result = "[ODBC; DRIVER=ODBC Driver 17 for SQL Server";     // TLS 1.2 (MSODBCSQL17.DLL)
            result += "; SERVER=" + info.server + "; DATABASE=" + info.dbname;
            if (info.authentication == PfsConnectionInfo.AuthenticationMode.SQLServerAuthentication)
                result += "; UID=" + info.user + "; PWD=" + info.password;
            else
                result += "; Trusted_Connection=yes";
            result += "]";

            return result;
        }

        // You can retry some database errors, we should just give up with these
        public static bool IsFatalDatabaseError(string errmsg)
        {
            string msg = errmsg.ToLower();
            return msg.Contains("cannot open database") ||
                msg.Contains("communication link") ||       // failure
                msg.Contains("connectionopen") ||
                msg.Contains("connectionread") ||
                msg.Contains("connectionwrite") ||
                msg.Contains("network") ||
                msg.Contains("server does not exist") ||
                msg.Contains("terminated") ||
                msg.Contains("the transaction log");        // is full
        }

        // You should try again if you get one of these errors
        public static bool IsRetryDatabaseError(string errmsg)
        {
            string msg = errmsg.ToLower();
            return msg.Contains("cannot obtain a lock") ||
                msg.Contains("deadlock victim") ||
                msg.Contains("rerun the transaction") ||
                msg.Contains("timeout");
        }

        /// <summary>
        /// Execute a (non-query) command and return the number of rows affected.
        /// This is good for INSERT, UPDATE and DELETE.
        /// </summary>
        /// <returns>returns number of records affected</returns>
        public static int ExecuteSQL(string sql)
        {
            using (var cn = PFSDBUtility.NewSqlConnection())
            {
                var cmd = new SqlCommand(sql, cn);
                return cmd.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// SqlExecuteReader : Fetch a single value from a sql query
        /// </summary>
        /// <param name="sql"> query</param>
        /// <param name="colname">The column retreive</param>
        public static object ExecuteSqlDataReader(string sql, string colname)
        {
            object value = null;
            try
            {
                using (var cn = PFSDBUtility.NewSqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand(sql, cn))
                    {
                        SqlDataReader dr = cmd.ExecuteReader();
                        if (dr.Read()) value = dr[0];
                        dr.Close();
                    }
                }
            }
            catch (Exception e)
            {
                if (IsFatalDatabaseError(e.ToString()))
                    PFSUtility.MessageBoxError(e);
            }

            return value;
        }


        //=====================================================================
        // Help creating SQL commands 
        //=====================================================================

        public static string SQLBoolean(bool v)
        {
            return SQLString(BoolToDB(v).ToString());
        }

        //Make a region-independent date for a SQL command.
        public static string SQLDate(DateTime? dt)
        {
            if (dt == null)
                return "NULL";
            else
                return "'" + ((DateTime)dt).ToString("yyyyMMdd") + "'";
        }

        public static string AccessDate(DateTime? dt)
        {
            if (dt == null)
                return "NULL";
            else
                return "#" + ((DateTime)dt).ToString("MM/dd/yyyy") + "#";
        }


        public static string SQLTime(DateTime? dt)
        {
            if (dt == null)
                return "NULL";
            else
                return "'" + ((DateTime)dt).ToString("HH:mm:ss") + "'";
        }

        public static string SQLDateTime(DateTime? dt)
        {
            if (dt == null)
                return "NULL";
            else
                return "'" + ((DateTime)dt).ToString("yyyy-MM-dd HH:mm:ss") + "'";
        }

        /// <summary>
        // Surround with apostrophies after quoting imbedded apostrophies
        /// </summary>
        public static string SQLString(string s)
        {
            if (s == null)
                return "NULL";
            else
                return "'" + s.Replace("'", "''") + "'";
        }

        public static string SQLZeroToNull(double v)
        {
            return (v == 0 ? "NULL" : v.ToString());
        }

        //=====================================================================
        // Convert values from the database
        //=====================================================================
        /// <summary>
        /// Convert database Y/N to bool (NULL == false)
        /// </summary>
        public static bool DBToBool(Object dbval)
        {
            return (DBToString(dbval) == "Y");
        }

        /// <summary>
        /// Returns a DateTime? (nullable DateTime)
        /// </summary>
        public static DateTime? DBToDateTimeOrNull(Object dbval)
        {
            return (dbval as DateTime?);
        }

        /// <summary>
        /// Converts database NULL to min datetime
        /// </summary>
        public static DateTime DBToDateTime(Object dbval)
        {
            return (dbval as DateTime?) ?? DateTime.MinValue;
        }

        /// <summary>
        /// Converts database NULL to zero
        /// </summary>
        public static double DBToDouble(Object dbval)
        {
            //return (dbval as double? ?? 0);
            double result;
            if ((dbval != null) && double.TryParse(dbval.ToString(), out result))
                return result;
            else
                return 0;
        }

        /// <summary>
        /// Converts database NULL to zero
        /// </summary>
        public static int DBToInt(Object dbval)
        {
            int result;
            //return (dbval as int? ?? 0);                  // this fails on short :(
            if ((dbval != null) && int.TryParse(dbval.ToString(), out result))
                return result;
            else
                return 0;
        }

        /// <summary>
        /// Converts database NULL to zero
        /// </summary>
        public static short DBToShort(Object dbval)
        {
            short result;
            //return (dbval as int? ?? 0);                  // this fails on short :(
            if ((dbval != null) && short.TryParse(dbval.ToString(), out result))
                return result;
            else
                return 0;
        }

        /// <summary>
        /// Converts database NULL to empty string
        /// </summary>
        public static string DBToString(Object dbval)
        {
            //return (dbval as string ?? "");               // this fails with char
            if (dbval != null)
                return dbval.ToString();
            else
                return "";

        }

        //=====================================================================
        // Get values ready for the database
        //=====================================================================
        public static char BoolToDB(bool v)
        {
            return (v ? 'Y' : 'N');
        }

        /// <summary>
        /// Converts empty string to null
        /// </summary>
        public static string StringToDB(string s)
        {
            return (String.IsNullOrEmpty(s) ? null : s);
        }

        public static int? ZeroToNull(int v)
        {
            return (v == 0 ? null : (int?)v);
        }

        //=====================================================================
        // Database Utilities
        //=====================================================================
        
        /// <summary>
        /// Return the server's current date and time - note this is a round-trip call
        /// </summary>
        public static DateTime ServerDateTime()
        {
            var result = DateTime.Now;                              // default to workstation time (just in case)
            using (var db = PFSDBUtility.NewSqlConnection())        // auto dispose & close
            {
                var cmd = new SqlCommand("SELECT GETDATE()", db);
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read()) result = DBToDateTime(dr[0]);
                dr.Close();
            }
            return result;
        }

        /// <summary>
        /// Return the user's local date and time - without using the users clock
        /// </summary>
        public static DateTime LocalDateTime()
        {
            var result = DateTime.Now;                                  // default to workstation time (just in case)

            using (var db = PFSDBUtility.NewSqlConnection())            // auto dispose & close
            {
                var cmd = new SqlCommand("SELECT GETUTCDATE()", db);    // get the server UTC time
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.Read()) result = DBToDateTime(dr[0]);
                dr.Close();

                TimeSpan diff = DateTime.Now - DateTime.UtcNow;         // get the user time zone
                int UTC_Offset = (int)diff.TotalMinutes;
                result = result.AddMinutes(UTC_Offset);                 // adjust to user time
            }

            return result;
        }

        public static string ServerName()
        {
            var info = GetSqlConnectionInfo();
            return info.server;
        }

        public static string DatabaseName()
        {
            var info = GetSqlConnectionInfo();
            return info.dbname;
        }

        /// <summary>
        /// DatabaseVersion(): Get the current database version (or -1)
        /// </summary>
        public static double DatabaseVersion()
        {
            double dbVersion = -1;

            try
           {
                using (var db = PFSDBUtility.NewSqlConnection())
                {
                    using (var cmd = new SqlCommand("SELECT MAX(DATABASE_REVISION) AS DBVERSION FROM SYS_REVISION", db))
                    {
                        SqlDataReader dr = cmd.ExecuteReader();
                        if (dr.Read() && !dr.IsDBNull(0))
                        {
                            dbVersion = Math.Round(Convert.ToDouble(dr[0]), 3);             // Must round (not sure why)
                        }
                        dr.Close();
                    }
                }
           }
           catch (Exception e)
           {
               if (IsFatalDatabaseError(e.ToString()))
                   PFSUtility.MessageBoxError(e);
               dbVersion = -1;
           }

            return dbVersion;
        }

        public static string FormatDbVersion(double value)
        {
            return String.Format("{0:0.000}", value);             // Format version as 0.000 (with trailing zeros)
        }

        //=====================================================================
        // Get Unique IDs from SYS_SEQUENCE
        //=====================================================================
        private const string GID_SEQUENCE_NAME = "GID";
        private const int GID_DEFAULT_BLOCKSIZE = 10;

        private static int gid_blocksize = 0;
        private static int gid_next_id = 0;
        private static int gid_last_id = 0;

        public static void ReserveGIDs(int num_ids)
        {
            gid_blocksize = num_ids;                  // save for next call to NextGID
        }

        /// <summary>
        /// Return the next ID from the database "GID" sequence (Global ID)
        /// </summary>
        public static int NextGID()
        {
            int result;

            // have some ids already been reserved?
            if ((gid_next_id <= gid_last_id) && (gid_next_id != 0))
            {
                // yes, return the next one
                result = gid_next_id;
            }
            else
            {
                //Use the default block size unless ReserveGIDs was called
                if (gid_blocksize < 1) gid_blocksize = GID_DEFAULT_BLOCKSIZE;
                //Get a new block of numbers
                result = AllocateIDs(GID_SEQUENCE_NAME, gid_blocksize);
                //Save the last valid number
                gid_last_id = result + gid_blocksize - 1;
                //gid_blocksize = 0;                    //revert to default for next time
            }

            gid_next_id = result + 1;                   // ready for next time
            return result;
        }

        public static int AllocateIDs(string sequence_name, int num_ids)
        {
            //Allocate a block of IDs from the SYS_SEQUENCE table
            var cn = PFSDBUtility.NewSqlConnection();
            string sql;
            int next_id = 0;

            do
            {
                var tran = cn.BeginTransaction();

                try
                {
                    sql = "SELECT NEXT_VALUE FROM SYS_SEQUENCE WHERE SEQUENCE_NAME=" + PFSDBUtility.SQLString(sequence_name);
                    var cmd = new SqlCommand(sql, cn, tran);
                    var dr = cmd.ExecuteReader();
                    if (dr.Read())
                    {
                        next_id = (dr["NEXT_VALUE"] as int? ?? 0);      // get the next id; convert null to zero
                    }
                    dr.Close();

                    if (next_id == 0)
                    {
                        // This is a new sequence
                        next_id = 1;                                    // always start at one, not zero.
                        var cmd2 = new SqlCommand(sql, cn, tran);
                        cmd2.CommandText = "INSERT INTO SYS_SEQUENCE (SEQUENCE_NAME, NEXT_VALUE) VALUES (@name, @value)";
                        cmd2.Parameters.AddWithValue("@name", sequence_name);
                        cmd2.Parameters.AddWithValue("@value", next_id);
                        cmd2.Transaction = tran;
                        cmd2.ExecuteNonQuery();
                    }
                    else
                    {
                        var cmd2 = new SqlCommand(sql, cn, tran);
                        cmd2.CommandText = "UPDATE SYS_SEQUENCE SET NEXT_VALUE=@value WHERE SEQUENCE_NAME=@name";
                        cmd2.Parameters.AddWithValue("@value", next_id + num_ids);
                        cmd2.Parameters.AddWithValue("@name", sequence_name);
                        cmd2.Transaction = tran;
                        cmd2.ExecuteNonQuery();
                    }
                    tran.Commit();
                }
                catch (DBConcurrencyException e)
                {
                    // Two of us tried to update the same sequence at the same time
                    Debug.WriteLine(e.Message);
                    tran.Rollback();
                    PFSUtility.Sleep(500);
                }
                catch (SqlException e)
                {
                    // We need to stop if this was a fatal database error
                    Debug.WriteLine(e.Message);
                    if (IsFatalDatabaseError(e.Message)) throw new Exception(e.Message, e);
                    break;
                }

            } while (next_id == 0);

            cn.Close();

            return next_id;
        }


    }
}
