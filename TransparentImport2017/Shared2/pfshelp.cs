﻿using System;
using System.IO;
using System.Windows.Forms;

namespace PfsShared
{
    public class PFSHelp
    {
        public enum PFSHelpContext
        {
            HELP_NONE = 0,

            HELP_INDEX = 5,
            HELP_PATIENT_SELECTION = 25,
            HELP_NEONATAL_DATA_ENTRY = 210,
            HELP_FIND_PATIENTS = 30,
            HELP_DELETE_ENCOUNTERS = 290,

            HELP_CLASSIFICATION = 35,
            HELP_EDIT_CLASS = 310,
            HELP_VIEW_CLASS = 320,
            HELP_DO_NOT_CLASSIFY = 330,
            HELP_NEW_ACTIVITY = 340,
            HELP_EDIT_ACTIVITY = 350,

            HELP_CLASS_BY_PROFILE = 125,
            HELP_CLASS_BY_TYPE = 127,
            HELP_MONITOR = 130,

            HELP_OTHER_WORKLOAD = 110,

            HELP_SCHEDULED_STAFFING = 40,
            HELP_ACTUAL_STAFFING = 45,

            HELP_MANUAL_ADT = 95,
            HELP_MANUAL_ADMIT = 120,
            HELP_MANUAL_UPDATE = 260,
            HELP_MANUAL_TRANSFER = 270,
            HELP_MANUAL_DISCHARGE = 280,

            HELP_AUDIT_TRAIL = 85,
            HELP_AUDIT_REPORT_CALCS = 82,
            HELP_EVENT_LOG_VIEWER = 115,
            HELP_EVENT_FILTER = 117,
            HELP_DATA_CONSISTENCY_CHECKS = 200,

            HELP_STAFFING_FORECASTER = 100,
            HELP_DISCHARGE_MANAGEMENT = 87,

            HELP_IRRT_SETUP_TEST = 135,
            HELP_IRRT_ASSIGN_TEST = 140,
            HELP_IRRT_MANAGEMENT_REPORTS = 155,
            HELP_IRRT_START_TEST = 145,
            HELP_IRRT_VIEW_TEST_RESULTS = 150,

            HELP_SYSTEM_MANAGER = 10,
            HELP_SECURITY = 20,
            HELP_SYSTEM_PARAMETERS = 15,
            HELP_DATA_ENTRY = 50,
            HELP_BATCH_COPY_UNIT_PARAMETERS = 300,
            HELP_CHANGE_ANY_PASSWORD = 360,

            HELP_STAFF_ASSIGNMENT = 180,
            HELP_ASSIGNMENT_TEAM_MANAGER = 230,
            HELP_ASSIGNMENT_ACTIVITY_MANAGER = 250,
            HELP_ASSIGNMENT_RN_RATIO_MANAGER = 390,
            HELP_ASSIGNMENT_EDIT_STAFF = 240,
            HELP_ASSIGNMENT_OPTIONS = 220,
            HELP_ASSIGNMENT_ADD_ACTIVITY = 370,
            HELP_ASSIGNMENT_ADD_PINNED_PATIENT = 380,

            HELP_STAFFING_NOTES = 105,
            HELP_CHART_VIEWER = 190,
            HELP_REPORTS = 55,

            HELP_OUTCOMES_DATA_ENTRY = 160,
            HELP_OUTCOMES_CLASSIFICATION = 165,
            HELP_OUTCOMES_REPORT_BUILDER = 167,
            HELP_OUTCOMES_REPORT_GENERATOR = 170,

            HELP_DATABOOK_EXTRACT = 90,
            HELP_CLASS_EXPORT = 70,
            HELP_FINANCE_EXPORT = 60,
            HELP_STAFF_EXPORT = 65
        }

        //include ..\bin in the path so this will work in design mode too
        private String MAIN_HELPFILE = Application.StartupPath+@"\AcuityPlus.chm";   //formerly WinPFS.chm


        public void ShowHelp(Form form, PFSHelpContext pfs_context)
        {

            string helpfile;
            string topic;
            int context;
            helpfile = MAIN_HELPFILE;
            topic = "Introduction.htm";
            context = Convert.ToInt32(pfs_context);


            //To find display topics in a help file, display the page you want
            //and right-button to get Properities.  The topic is imbedded in
            //the address string.  (You can drag select it and copy with ctrl-C.)
            //
            //Context numbers do not appear to visible in the page properties.
            //Context numbers are better because they are permanent where page
            //names may change.
            //In case may have more than one help file at some point

            try
            {
                if (!File.Exists(helpfile))
                    throw new FileNotFoundException();
                else
                {
                    if (context == 0)
                    {
                        Help.ShowHelp(form, helpfile, HelpNavigator.Topic, topic);
                    }
                    else
                    {
                        Help.ShowHelp(form, helpfile, HelpNavigator.TopicId, context.ToString());
                    }
                }
            }
            catch (FileNotFoundException)
            {
                MessageBox.Show("Could not find help file", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            
        }
    }
}
