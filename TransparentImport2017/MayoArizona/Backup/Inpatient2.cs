﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;                     // for LINQ; add a reference
using System.Data.SqlClient;                // for SqlConnection
using System.IO;                            // for Directory, Path, Stream
using PfsShared;                            // add a reference to Shared2 project

// ============================================================================
// Inpatient 2.0 transparent mapping -- GOES HERE --
// Mayo Arizona
// ============================================================================
// This processes one patient.  Only one database query is used.  :)
//
// All search functions use exact match for category, description and field name.
// Codes and results can be a single word or a list of words.
// Codes default to exact match; prefix with CODE_CODE_LIKE_PREFIX if desired (single word only)
// Results default to like matching; prefix with RESULT_EXACT_MATCH_PREFIX if desired (single word only)
//
// All searches are case insensitive.
//
//
namespace TransparentMapping
{
    class Inpatient2
    {
        private const int MAX_INDS = 50;
        private const int MAX_PROCS = 20;

        private const string CODE_LIKE_PREFIX = "%!";       // use to make codes %like%
        private const string EXACT_MATCH_PREFIX = "&!";     // use to make desc/result exact match
        private const string CHAR_COMMA = "||";             // use to insert literal comma in word list
        private const string NOT_PREFIX = "!!";             // use to insert literal comma in word list

        private struct indicator_data {
            public bool     is_checked;     
            public int      radio_group;       
        }

        private struct proc_data {                      
            public int      procedure_number;
            public DateTime start;   
            public DateTime finish;
        }

        // These are database CHART_ITEMs for this patient
        private CHART_ITEM[] _chart_items_since_admission;
        private CHART_ITEM[] _chart_items_since_unit_arrival;
        private CHART_ITEM[] _chart_items_during_pull_period;
        private CHART_ITEM[] _chart_items_24_hours;
        // These are database PROCEDURE_EVENTs for this patient
        private PROCEDURE_EVENT[] _procedure_events;

        private indicator_data[] _inds;
        private List<proc_data> _procs;
        private List<proc_data> _outcomes;

        private const int DEFAULT_BUCKET_SIZE = 20;       //min

        private PatientInfo _pat;
        private bool adl23;
        private bool adl4;
        private bool tubefeed;
        private bool coma;
        private int  _bucket_size = DEFAULT_BUCKET_SIZE;
        private bool is_default;
        private bool no_chart_items = false;
        private bool give_default_inds = false;


        private enum SearchDepth {
            SearchDefault,
            SearchPullRange,            //search within the current pull     -- default
            SearchSinceArrival,         //search since arrival to the unit
            SearchSinceAdmission,        //search everything since admission to the hospital
            Search24Hours
        }

        private enum CountMode {
            CountAll,
            CountFirst                  //stop after one is found
        }

        private enum GetValueMode {
            GetTotal,
            GetMax,
            GetLast
        }

        enum Frequencies {
            QNONE,
            Q4H,
            Q2H,
            Q1H,
            Q30M
        }

        private struct fmapRow {
            public double   los_high;                       //the LOS being testing
            public int[]    freq;                           //the count required for each Q value
        }
        private List<fmapRow> _freq_map;                    //1,2,4,8,12,24 hours
        private struct gBucket
        {
            public int bucket;
            public string code;
            public DateTime evdt;
        }

     
        //
        // This is the main entry point
        //
        public void ProcessPatient(PatientInfo pat)
        {
            _pat = pat;
            no_chart_items = false;
            InitIndicators(); // sets is_default
            InitProcs();
            if (! is_default)
                {
                LoadFreqTable();
                LoadPatientChart();
                Check_1_2_3_4();
                Check_5();
                Check_6_7();
                Check_8();
                Check_9();
                Check_10_11();
                Check_12_13();
                Check_14();
                Check_15_16_17_18();
                Check_19();
                Check_20();
                Check_21_22();
                Check_23();
                Check_24();
            }
            AtLeastOneADL();
            HighestIndicatorInEachGroupWins();

            if (!is_default)
            {
                CheckProcs();
                //CheckOutcomes();
            }
            int ceid = 0;
            int mceid = 0;
            DateTime effin_of3ampm = DateTime.MinValue;
            DateTime effout_of3ampm = DateTime.MinValue;
            if (ThreeAMPMClassExists(out ceid, out effin_of3ampm, out effout_of3ampm))
            // checks to see if there is a 3pm/am class and if it does then delete it
            {
                DeleteClassification(ceid, effin_of3ampm, effout_of3ampm); // modifies the class before it
            }
            else
            {
                // check if discharged and classification=dctime
                // the problem stems from the desire to delete 3am/3pm classifications.
                // if the 3pm goes in and then A03 arrives at 15:30 with dctime=2:45pm,
                // then the 3pm was deleted by the 3AM (! - I would rather the 7pm delete it)
                if (_pat.dctime > DateTime.Now.AddDays(-1))
                {
                    Program.VerboseAudit("Post-dc pat.dctime=" + _pat.dctime.ToString());
                    CleanUpClassifications();
                }
            }
            if (Program.g_no_output) return;
//            if (no_chart_items) return; //but wait -- need to give default if within4
            bool def_in_past4hrs;
            if (def_in_past4hrs = DefaultInPast4hrs())
            {
                if (no_chart_items)
                {
                    Program.VerboseAudit("Patient has no documentation. def ptype=" + _pat.default_ptype);
                    give_default_inds = true;
                }
                else
                {
                    Program.VerboseAudit("default found def ptype=" + _pat.default_ptype);
                    give_default_inds = (_pat.default_ptype > DeterminePtypeOfIndicators());
                }
            }
            if (no_chart_items && !def_in_past4hrs) return;
            OutputClass();
            OutputProcs();
            //OutputOutcomes();
        }

       
        private void InitIndicators()
        {
            // Make a new (empty) indicator array
            _inds = new indicator_data[MAX_INDS + 1];           // This 1 based so add one
            is_default = false;
            //if (_pat.los_hours <= 4.0) {
            //    is_default = true;
            //    Program.VerboseAudit("Patient was here 4 hrs or less. Will receive default indicators " + _pat.default_inds_str);
            //    foreach (var ind in _pat.default_inds) {
            //        if (ind <= _inds.GetUpperBound(0)) {
            //            _inds[ind].is_checked = true;
            //        }
            //    }
            //}

            adl23 = false;
            adl4 = false;
            tubefeed = false;
            coma = false;

            // get indicator radio groups from the database
            // ** (This database access can be replaced once we have a C# methodolgy cache)
            var db = PFSUtility.NewPfsDataContext();
            var query = from idef in db.INDICATOR_DEFINITIONs
                        where (idef.METHODOLOGY_ID == _pat.meth_id)
                        select idef;
            foreach (var idef in query) {
                if (idef.INDICATOR_NUMBER <= _inds.GetUpperBound(0)) {
                    // (convert nulls to zero)
                    _inds[idef.INDICATOR_NUMBER].radio_group = PFSUtility.DBToInt(idef.RADIO_GROUP);
                }
            }
        }

        private void InitProcs()
        {
            _procs = new List<proc_data>();
            _outcomes = new List<proc_data>();
        }

        private fmapRow LoadFreqTableRow(double los_high, string values)
        {
            fmapRow fmrow;
            
            fmrow.los_high = los_high;
            fmrow.freq = new int[5];

            var arr = values.Split(',');
            for (int i = 0; i <= fmrow.freq.GetUpperBound(0) ; i++ )
            {
                fmrow.freq[i] = arr[i].ToInteger();
            }
            return fmrow;
        }

        // These are the number of times you need to do something during a certain time period (LOS)
        // in order for it to be considered one of the frequencies listed.
        private void LoadFreqTable()
        {
            _freq_map = new List<fmapRow>();
            //                              LOS,  None Q4h Q2h Q1h Q30m
            _freq_map.Add(LoadFreqTableRow(1, "    0,  0,  0,  1,  2"));
            _freq_map.Add(LoadFreqTableRow(2, "    0,  0,  1,  2,  3"));
            _freq_map.Add(LoadFreqTableRow(4, "    0,  1,  2,  3,  6"));
            _freq_map.Add(LoadFreqTableRow(6, "    0,  1,  2,  4,  9"));
            _freq_map.Add(LoadFreqTableRow(8, "  0,  1,  2,  6, 11"));
            _freq_map.Add(LoadFreqTableRow(12, " 0,  2,  4,  8, 17"));
            _freq_map.Add(LoadFreqTableRow(16, "   0,  3,  5,  9, 20"));
            _freq_map.Add(LoadFreqTableRow(24, "   0,  4,  8, 15, 29"));
            _freq_map.Add(LoadFreqTableRow(9999, " 0,  4,  8, 15, 29"));
//New freq table 2/5/14
//q4	q2	q1	q30     q30
//            Non-ICU	ICU & SD
// 4	8	15	29	    36
// 3	5	9	17	    24
// 2	4	7	13	    19
// 2	3	5	10	    13

            //to read this table: if you have at least 6 asessmenets in a 12-hour period, you get Q1h
            //(LOS=12, column Q1h has a count of 6)
        }

        private Frequencies FreqForCount(double los_hours, int count) 
        {
            if (count <= 0) return Frequencies.QNONE;
            foreach(var fmrow in _freq_map) {
                if (los_hours <= fmrow.los_high) {
                    // Option: pro-rate the count upward if the LOS is less than the row we are interested in.
                    //         This will bump the count to what it might have been at the full LOS.
                    // Note: truncate the result; rounding inflates the value too much.
                    int prorated_count = (int)((fmrow.los_high / los_hours) * count);

                    // foreach goes low to high; go from high to low instead
                    for (int j = (int)Frequencies.Q30M; (j >= (int)Frequencies.QNONE); j--) { //search right to left
                        if (prorated_count >= fmrow.freq[j]) {
                            return (Frequencies)j;
                        }
                    } // next j
                }
            }
            
            return Frequencies.QNONE;
        }

        //
        // Load patient chart from the database (one DB access)
        // Load everything since admission
        //
        private void LoadPatientChart()
        {
            // Get the entire patient chart (all units and dates for this patient)
            var db = PFSUtility.NewPfsDataContext();
            var query = from item in db.CHART_ITEMs
                        where (item.ENCOUNTER_ID == _pat.encounter_id)
                        select item;
            // Save the result
            _chart_items_since_admission = query.ToArray();
            no_chart_items = (query.Count() == 0);

            // Convert all results to lower case for case insensitive comparisons
            foreach (var item in _chart_items_since_admission) {
                if (item.CATEGORY != null) item.CATEGORY = item.CATEGORY.ToLower();
                if (item.CODE != null) item.CODE = item.CODE.ToLower();
                if (item.DESCRIPTION != null) item.DESCRIPTION = item.DESCRIPTION.ToLower();
                if (item.FIELD_NAME != null) item.FIELD_NAME = item.FIELD_NAME.ToLower();
                if (item.RESULT != null) item.RESULT = item.RESULT.ToLower();
            }

            // Prepare two more versions of the chart
            var query2 = from item in _chart_items_since_admission
                    where(item.EVENT_DATETIME >= _pat.unit_arrival) && (item.EVENT_DATETIME < _pat.pull_finish)
                    select item;
            _chart_items_since_unit_arrival = query2.ToArray();

            query2 = from item in _chart_items_since_admission
                     where (item.EVENT_DATETIME >= _pat.pull_start) && (item.EVENT_DATETIME < _pat.pull_finish)
                     select item;
            _chart_items_during_pull_period = query2.ToArray();

            query2 = from item in _chart_items_since_admission
                     where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-24)) && (item.EVENT_DATETIME < _pat.pull_finish)
                     select item;
            _chart_items_24_hours = query2.ToArray();
        }

        // Delay looking for patient procedures until they are wanted
        private void LoadPatientProceduresIfNeeded()
        {
            if (_procedure_events != null) return;

            var db = PFSUtility.NewPfsDataContext();
            var query = from proc in db.PROCEDURE_EVENTs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                        select proc;
            _procedure_events = query.ToArray();
        }



        // Started a new chart item query (of a certain depth)
        // Default is the chart during the pull range.  Options for unit arrival and admission to hospital.
        private IEnumerable<CHART_ITEM> StartNewQuery()
        {
            return StartNewQuery(SearchDepth.SearchDefault);
        }
        private IEnumerable<CHART_ITEM> StartNewQuery(SearchDepth search_depth)
        {
            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    return (from item in _chart_items_during_pull_period select item);
                case SearchDepth.SearchSinceArrival:
                    return (from item in _chart_items_since_unit_arrival select item);
                case SearchDepth.SearchSinceAdmission:
                    return (from item in _chart_items_since_admission select item);
                case SearchDepth.Search24Hours:
                    return (from item in _chart_items_24_hours select item);
            }
            return null;
        }

        // Is this search term a word or a list of words?
        private bool ValueIsAList(string s) 
        {
            return (s != null) && s.Contains(',');                //is this a comma-separated list?
        }

        // Break apart a wordlist and prepare its members
        private string[] SplitOnCommaAndPrepareElements(string s)
        {
            // If a word list has a space after each comma, we will get leading blanks in each term
            var arr = s.Split(',');
            for (int i = 0; i <= arr.GetUpperBound(0); i++) {
                arr[i] = arr[i].Trim();                         // get rid of leading blanks
                arr[i] = arr[i].Replace(CHAR_COMMA, ",");       // insert a literal comma into search word
                arr[i] = arr[i].ToLower();                      // convert for case-insensitive comparisons
            }
            return arr;
        }

        // Add a basic chart item filter to a LINQ query
        //
        // cat = exact match
        // code_list = exact match; optional "like" match
        // desc_list = "like" match; optional exact match
        // field = exact match
        // result_list = "like" match; optional exact match
        //
        // These are comma-separated lists.  
        // Search for a literal comma by using CHAR_COMMA in the string.
        private IEnumerable<CHART_ITEM> AndItemFilter(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list) 
        {
            // Converted all values to lower case for case-insensitive comparisons.
            if (!String.IsNullOrEmpty(cat)) {
                cat = cat.ToLower();
                query = query.Where(e => e.CATEGORY == cat);
            }

            if (!String.IsNullOrEmpty(code_list)) {
                code_list = code_list.ToLower();
                if (code_list.Left(2) == EXACT_MATCH_PREFIX) {          // exact match override?
                    query = query.Where(e => e.CODE == code_list.Substring(2));
                } else if (code_list.Left(2) == CODE_LIKE_PREFIX)
                {            // override for "like" match?
                    query = query.Where(e => e.CODE.Contains(code_list.Substring(2)));
                } else if (ValueIsAList(code_list)) {
                    query = AndCodeInList(query, code_list);            // find one of the words
                } else {
                    query = query.Where(e => e.CODE.Contains(code_list));      // find this word
                }
            }
            
            if (!String.IsNullOrEmpty(desc_list)) {
                desc_list = desc_list.ToLower();
                if (desc_list.Left(2) == EXACT_MATCH_PREFIX) {          // exact match override?
                    query = query.Where(e => e.DESCRIPTION == desc_list.Substring(2));
                } else if (ValueIsAList(desc_list)) {
                    query = AndDescriptionInList(query, desc_list);
                } else {
                    desc_list = desc_list.Replace(CHAR_COMMA, ",");     // look for literal comma requests
                    query = query.Where(e => e.DESCRIPTION.Contains(desc_list));
                }
            }
            
            if (!String.IsNullOrEmpty(field)) {
                field = field.ToLower();
                query = query.Where(e => e.FIELD_NAME == field);
            }
            
            if (!String.IsNullOrEmpty(result_list)) {
                result_list = result_list.ToLower();                        // make all results case insensitve
                if (result_list.Left(2) == EXACT_MATCH_PREFIX) {     // exact match override?
                    query = query.Where(e => e.RESULT == result_list.Substring(2));
                } else if (ValueIsAList(result_list)) {
                    query = AndResultInList(query, result_list);
                } else {
                    result_list = result_list.Replace(CHAR_COMMA, ",");     // look for literal comma requests
                    query = query.Where(e => e.RESULT.Contains(result_list));
                }
            }
            return query;
        }

        // Look for a code that is one of the words in the list.
        private IEnumerable<CHART_ITEM> AndCodeInList(IEnumerable<CHART_ITEM> query, string code_list) 
        {
            if (String.IsNullOrEmpty(code_list)) return query;
            
            var arr = SplitOnCommaAndPrepareElements(code_list);
            return query.Where(e => e.CODE.ContainsAny(arr));  // use like match. Exact match = arr.Contains(e.CODE));   
        }

        // Look for a description that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndDescriptionInList(IEnumerable<CHART_ITEM> query, string desc_list) 
        {
            if (String.IsNullOrEmpty(desc_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(desc_list);
            return query.Where(e => e.DESCRIPTION.ContainsAny(arr));    // "like" match
        }

        // Look for a result that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultInList(IEnumerable<CHART_ITEM> query, string result_list) 
        {
            if (String.IsNullOrEmpty(result_list)) return query;
            
            var arr = SplitOnCommaAndPrepareElements(result_list);
            return query.Where(e => e.RESULT.ContainsAny(arr));         // "like" match
        }

        // Look for a result that does not contain one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultNotInList(IEnumerable<CHART_ITEM> query, string result_list)
        {
            if (String.IsNullOrEmpty(result_list)) return query;
            
            var arr = SplitOnCommaAndPrepareElements(result_list);
            return query.Where(e => !e.RESULT.ContainsAny(arr));        // "like" match
        }

        private string DescribeSearchDepth(SearchDepth search_depth) 
        {
            string result = "";
            
            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    //result = "in pull range"
                    result = "";                         //be quiet since this is default
                    break;
                case SearchDepth.SearchSinceArrival:
                    result = "since arrival to unit";
                    break;
                case SearchDepth.SearchSinceAdmission:
                    result = "since admission";
                    break;
                case SearchDepth.Search24Hours:
                    result = "in past 24 hours";
                    break;
            }
            
            return result;
        }

        // Describe a query that has already been run
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Make a simple query and describe the results
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            var arr = query.ToArray();
            return Describe(arr, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private string LookingFor(string so_far, string what, string op, string value)
        {
            string result;
            if (String.IsNullOrEmpty(value)) return so_far;

            if (so_far == "")
                result = "looking for " + what;
            else
                result = so_far + " and " + what;

            if (value.Left(2) == EXACT_MATCH_PREFIX)
                result += "='" + value.Substring(2) + "'";
            else if (value.Left(2) == CODE_LIKE_PREFIX)
                result += " contains " + "'" + value.Substring(2) + "'";
            else if (ValueIsAList(value))
                result += " in '" + value + "'";
            else
                result += op + "'" + value + "'";
            
            return result;
        }
        private string Describe(CHART_ITEM[] arr, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string result = "";
            result = LookingFor(result, "cat", "=", cat);
            result = LookingFor(result, "code", "=", code_list);
            result = LookingFor(result, "desc", " contains ", desc_list);
            result = LookingFor(result, "field", "=", field);
            result = LookingFor(result, "result", " contains ", result_list);

            if (arr.Count() == 0) {
                result += "; not found " + DescribeSearchDepth(search_depth);
            } else {
                var e = arr[0];
                result = "FOUND: " + result + "; found";
                // We might have searched for a pattern or word list in several fields - show what was found
                if (e.CATEGORY != null)     result += " cat='" + e.CATEGORY + "'";
                if (e.CODE != null)         result += " code='" + e.CODE + "'";
                if (e.DESCRIPTION != null)  result += " desc='" + e.DESCRIPTION + "'";
                if (e.FIELD_NAME != null)   result += " field='" + e.FIELD_NAME + "'";
                if (e.RESULT != null)       result += " result='" + e.RESULT + "'";
                // Are there more results?  Just say how many; we aren't going to list them.
                if (arr.Count() == 2) {
                    result += " (1 more result)";
                } else if (arr.Count() > 2) {
                    result += " (" + (arr.Count() - 1) + " more results)";
                }
            }
            
            return result;
        }


        //Set an indicator for this reason (low level)
        private void SetInd(int inum, string reason)
        {
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Set Ind #" + inum + ": " + reason);          //already set - repeat for verbose only
            } else {
                _inds[inum].is_checked = true;
                Program.Audit("Set Ind #" + inum + ": " + reason);
            }
        }

        //Clear an indicator for this reason (low level)
        private void ClrInd(int inum, string reason)
        {
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Clr Ind #" + inum + ": " + reason);          //already clear - repeat verbose only
            } else {
                _inds[inum].is_checked = false;
                Program.Audit("Clr Ind #" + inum + ": " + reason);
            }
        }

        // Count how many items meet the conditions
        // All items are listed
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountItems(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            int count = query.Count();

            // always return what was found
            found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
            // echo the result?
            if (trace) Program.VerboseAudit(found_what);
            
            return count;
        }

        // Count how many items meet the conditions
        // This has the option to list every entry or give a total
        private int CountResultInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what="";
 
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);

            var arr = SplitOnCommaAndPrepareElements(result_list);

            foreach (var item in query) {
                // Figure out which of the search words were found
                var s = item.RESULT.ContainsWhich(arr);
                found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' code='" + item.CODE + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "'";
                if (trace) Program.VerboseAudit(found_what);
                count++;
                if (count_mode != CountMode.CountAll) break;        //stop counting
            }
            
            if (count > 0) {
                //We already printed what was found; maybe add how many?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }
            
            return count;
        }

        // Count how many items contain ALL results in list
        private int CountResultInListAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what = "";

            var arr = SplitOnCommaAndPrepareElements(result_list);

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            for (int i = 0; i <= arr.GetUpperBound(0); i++)
            {
                if (arr[i].StartsWith(NOT_PREFIX))
                {
                    query = query.Where(e => !e.RESULT.Contains(arr[i].Substring(2)));
                }
                else
                {
                    query = query.Where(e => e.RESULT.Contains(arr[i]));
                }
            }

            count = query.Count();

            if (count > 0)
            {
                found_what = "found item with all results in '" + result_list + "' without those prefixed by !!";
                if (trace) Program.VerboseAudit(found_what);
            }
            else
            {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (ValueIsAList(result_list)) {
                return CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what);
            } else {
                return CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what);
            }
        }

        // Is there an item with this result?
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list) 
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth) 
        {
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContains(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what) 
        {
            if (ValueIsAList(result_list)) {
                return (CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            } else {
                return (CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
            }
        }

        // Result contains all in list
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (ValueIsAList(result_list))
            {
                return (CountResultInListAll(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            }
            else
            {
                found_what = "Not valid for non-list results.";
                return false;
            }
        }
        // Count how many items meet the conditions, with none of the results in result_list.
        // This has the option to say how many were found rather than list all.
        private int CountResultNotInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what) 
        {
            int count = 0, rec_count = 0;
            bool found_one;

            found_what = "";
            var arr = SplitOnCommaAndPrepareElements(result_list);         // also deals with CHAR_COMMA
            
            //Do not filter by result here; we need all results
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");
            
            foreach(var item in query) {
                rec_count++;
                //Look for each search word in the result
                found_one = false;
                
                foreach (string s in arr) {
                    if (String.Equals(item.RESULT, s)) {
                        found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "' -- ignore this";
                        Program.VerboseAudit(found_what);
                        found_one = true;
                        break;
                    }
                }

                if (! found_one) {
                    //none of the words were found - good!
                    found_what = Describe(cat, code_list, desc_list, field, "", search_depth) + " -- does not contain '" + result_list + "'";
                    Program.VerboseAudit(found_what);
                    count++;
                    if (count_mode != CountMode.CountAll) break;
                }
            }
            
            if (count > 0) {
                //We already printed what was found; maybe show the total?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else if (rec_count > 0) {
                //We already printed what we ignored
            } else {
                //Print what did not exist
                found_what = Describe(cat, code_list, desc_list, field, "", search_depth);      //not found
                Program.VerboseAudit(found_what);
            }
            
            return count;
        }

        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, string found_what) 
        {
            // Search in the pull range with trace on
            return CountResultDoesNotContain(cat, code_list, desc_list, field, result_list, out found_what, SearchDepth.SearchDefault, true);
        }
        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, out string found_what, SearchDepth search_depth, bool trace)
        {
            return CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0;
        }

        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            return (CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0);
        }

        // Set the indicator if the conditions are met
        //
        private bool SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);

        }
        private bool SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            bool b = false;
            //avoid more queries if the indicator is already set
            if (_inds[inum].is_checked) return true;
            
            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what)) {
                SetInd(inum, found_what);                          //echo here - found
                b = true;
            } else {
                Program.VerboseAudit(found_what);                  //and here - not found
            }
            return b;
        }
        // Special contains all
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContainsAll(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already set
            if (_inds[inum].is_checked) return;

            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                          //echo here - found
            }
            else
            {
                Program.VerboseAudit(found_what);                  //and here - not found
            }
        }
        // Set the indicator if the result does not contain any of the words in result_list
        //
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultDoesNotContain(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {   
            string found_what;
            //avoid more queries if the indicator is already set
            if (_inds[inum].is_checked) return;
            
            //Turn trace off for ResultDoesNotContain() and echo what was set below with SetInd
            if (ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                           //echo here - not found
            } else {
                Program.VerboseAudit(found_what);                   //and here - found
            }
        }

        // Clear the indicator if the result contains one of the words in the result_list
        //
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already clear
            if (! _inds[inum].is_checked) return;
            
            //Turn trace off for ResultContains() and echo what was set below with SetInd
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what)) {
                ClrInd(inum, found_what);                           //echo here - found
            } else {
                Program.VerboseAudit(found_what);                   //and here - not found
            }
        }


        //These slightly smaller functions are meant for places where you aren't really looking for a chart result
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            return (CountResultContains(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
        }

        // Variations with optional parameters (can switch to optional parameters in VS 2010)
        //
        private void SetIndIfFound(int inum, string cat)
        {
            SetIndIfResultContains(inum, cat, "", "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list)
        {
            SetIndIfResultContains(inum, cat, code_list, "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private void ClrIndIfFound(int inum, string cat)
        {
            ClrIndIfResultContains(inum, cat, "","","","", SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }


        // Get the max/total value from a result (usually in the middle of the text)
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetIntValue(get_mode, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            int value;
            int result = 0;
            bool found_one = false;

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            
            //Look for a number in the result
            
            foreach(var item in query) {
                // RESULT might be null so convert to empty string so Split won't blow up
                var arr = PFSUtility.DBToString(item.RESULT).Split(' ');
                foreach (string s in arr) {
                    //Look for 1st character numeric; IsNumeric("60min") will fail so look at 1st char only
                    if (s.Left(1).IsNumeric()) {
                        value = (int)s.Val();                           //Use Val; ToInteger will error on "60min"
                        switch (get_mode) {
                            case GetValueMode.GetMax:
                                result = Math.Max(value, result);       //max
                                break;
                            case GetValueMode.GetTotal:
                                result += value;                        //total
                                break;
                            case GetValueMode.GetLast:
                                result = value;                         //last
                                break;
                        }
                        
                        // print what we are searching for (the first time)
                        if (! found_one) {
                            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
                        }
                        found_one = true;
                        // print each value found
                        Program.VerboseAudit("  found numeric value " + result);
                        //Keep going in case there are more
                    }
                } 
            }
            
            if (! found_one) {
                //show what was not found
                Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
            }
            
            return result;
        }

        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetMaxValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return GetIntValue(GetValueMode.GetMax, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetTotalValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return GetIntValue(GetValueMode.GetTotal, cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Get a result; returns true if found with return_result
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result)
        {
            return GetResult(cat, code_list, desc_list, field, out return_result, SearchDepth.SearchDefault);
        }
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0) {
                return_result = PFSUtility.DBToString(query.First().RESULT);
            } else {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }
        private void GetResultforOther(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "Other (comment)");
            return_result="";
            foreach (var item in query)
            {
                if (!String.Equals(item.RESULT, "Other (comment)"))
                {
                    return_result = item.RESULT;
                    //Program.VerboseAudit(found_what);
                    break;
                }

            }
            return;
        }

        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, out string return_result, out DateTime return_evdt)
        {
            return GetResultAndEVDT(cat, code_list, desc_list, field, out return_result, out return_evdt, SearchDepth.SearchDefault);
        }
        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, out string return_result, out DateTime return_evdt, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0)
            {
                return_result = PFSUtility.DBToString(query.First().RESULT);
                return_evdt = PFSUtility.DBToDateTime(query.First().EVENT_DATETIME);
            }
            else
            {
                return_result = "";
                return_evdt = DateTime.MinValue;
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }


        //======================================================================================
        // Mapping starts here
        //======================================================================================

        private void Check_1_2_3_4()
        {
            //bool mob3 = false;
            bool mob4 = false;

            bool feed2 = false;
            bool bath2 = false;
            bool toil2 = false;
            bool mob2 = false;

            //bool feed3 = false;
            //bool feed4 = false;
            bool hyg3 = false;
            bool hyg4 = false;
            string found_what;
            bool adl1 = false;
            bool adl2 = false;
            bool adl3 = false;
            bool adl4 = false;
            bool adl5 = false;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("1. ADL Self");
            Program.VerboseAudit("2. ADL Assist");
            Program.VerboseAudit("3. ADL Extended");
            Program.VerboseAudit("4. ADL Complete");
            Program.VerboseAudit("---------------");
            
            // 4. COMPLETE CARE

            if (_pat.age < 4.0) {
                SetInd(4,"Age <=3 years");
            }
            if (_inds[4].is_checked) return;

            feed2 = SetIndIfResultContains(2, "", "5622708", "", "", "Assisted with Feeding,Aspiration precautions observed");
            feed2 |= SetIndIfResultContains(2, "", "5622708", "", "", EXACT_MATCH_PREFIX+"Fed");
            SetIndIfResultContains(2, "", "316346183", "", "", "");
            SetIndIfResultContains(2, "", "345372935", "", "", "");
            SetIndIfResultContains(2, "", "304208921", "", "", "");
            SetIndIfResultContains(2, "", "406392835", "", "", "");
            SetIndIfResultContains(2, "", "406392847", "", "", "");
            SetIndIfResultContains(2, "", "406392853", "", "", "");
            SetIndIfResultContains(2, "", "117648", "", "", ""); //tube feeding volume
            SetIndIfResultContains(2, "", "406392841", "", "", "");
            SetIndIfResultContains(2, "", "5613413", "", "", "Assist");
            SetIndIfResultContains(2, "", "5613413", "", "", "Complete/Total Care");
            SetIndIfResultContains(2, "", "5613413", "", "", "Moderate Assistance");
            SetIndIfResultContains(2, "", "5613414", "", "", "Assist");
            SetIndIfResultContains(2, "", "5613414", "", "", "Complete/Total Care");
            bath2 = SetIndIfResultContains(2, "", "5613416", "", "", "Assist,Bed bath,Complete/Total Care,Moderate Assistance");
            SetIndIfResultContains(2, "", "5613417", "", "", "Assist");
            SetIndIfResultContains(2, "", "5613417", "", "", "Complete/Total Care");
            toil2 = SetIndIfResultContains(2, "", "5613420", "", "", "Bedpan,Bedside Commode,Foley,Incontinent,Rectal tube,with assistance");
            SetIndIfResultContains(2, "", "209315142", "", "", "");
            SetIndIfResultContains(2, "", "5613422", "", "", "Bedrest");
            SetIndIfResultContains(2, "", "5613422", "", "", "Dangled");
            SetIndIfResultContains(2, "", "5613422", "", "", "Lift assist device");
            mob2 = SetIndIfResultContains(2, "", "5613422,1572632461", "", "", "Mobilize");
            SetIndIfResultContains(2, "", "5613422", "", "", "wheelchair");
            SetIndIfResultContains(2, "", "305006242", "", "", "Yes");
            SetIndIfResultContains(2, "", "305006247", "", "", "Yes");
            SetIndIfResultContains(2, "", "305006257", "", "", "Yes");
            SetIndIfResultContains(2, "", "383992219", "", "", "");
            SetIndIfResultContains(2, "", "305056546", "", "", "rectal tube");

            //int num_adl_items = (feed2 ? 1 : 0) + (bath2 ? 1 : 0) + (toil2 ? 1 : 0) + (mob2 ? 1 : 0);
            //if (num_adl_items >= 4)
            //{
            //    SetInd(3, "Four or more Partial ADL found among Feeding,Bathing,Toileting,Mobility");
            //}

            bool feed3 = false;
            feed3 |= ResultContains("", "5613457", "", "", "Assisted with Feeding,Aspiration precautions observed");
            feed3 |= ResultContains("", "5613457", "", "", EXACT_MATCH_PREFIX+"Fed");
            feed3 |= ResultContains("", "345372935", "", "", "");
            feed3 |= ResultContains("", "406392835", "", "", "");
            feed3 |= ResultContains("", "117648", "", "", ""); //tube feeding volume
            feed3 |= ResultContains("", "5613456", "", "", "NPO");
            feed3 |= ResultContains("", "316346183", "", "", "");
            feed3 |= ResultContains("", "406392841", "", "", "");
            feed3 |= ResultContains("", "406392847", "", "", "");
            feed3 |= ResultContains("", "406392853", "", "", "");

            bool bath3 = false;
            bath3 |= ResultContains("", "5613413", "", "", "Assist,Complete/Total Care,Moderate Assistance");
            bath3 |= ResultContains("", "5613414", "", "", "Assist,Complete/Total Care");
            bath3 |= ResultContains("", "5613416", "", "", "Assist,Bed bath,Complete/Total Care,Moderate Assistance");

            bool toil3 = false;
            toil3 |= ResultContains("", "5613420", "", "", "Bedpan,Bedside Commode,Foley,Incontinent,Rectal tube,with assistance");
            toil3 |= ResultContains("", "5613417", "", "", "Assist,Complete/Total Care");
            toil3 |= ResultContains("", "305056531", "", "", "Ostomy Appliance Emptied");
            toil3 |= ResultContains("", "305056469", "", "", "Ostomy Appliance Emptied");
            toil3 |= ResultContains("", "304208961", "", "", "");
            toil3 |= ResultContains("", "304208966", "", "", "");
            toil3 |= ResultContains("", "209315142", "", "", "");
            toil3 |= ResultContains("", "383992219", "", "", "");

            bool mob3 = false;
            mob3 |= ResultContains("", "1572632461", "", "", "Mobilize with 1,Mobilize with 2,Mobilize with >3,Lift assist device");
            mob3 |= ResultContains("", "5613422", "", "", "Bedrest,Dangled,Mobilize with 1,Mobilize with 2,Mobilize with >3,Lift assist device,wheelchair");

            bool dress3 = false;
            dress3 |= ResultContains("", "1229815929", "", "", "Assist,Complete/Total Care");

            int num3_items = (feed3 ? 1 : 0) + (bath3 ? 1 : 0) + (toil3 ? 1 : 0) + (mob3 ? 1 : 0) + (dress3 ? 1 : 0);
            if (num3_items >= 3)
            {
                SetInd(3, "Three or more Partial(or higher) ADLs found among Feeding,Bathing,Toileting,Mobility,Dressing");
            }

            bool feed4 = false;
            feed4 |= ResultContains("", "5613457", "", "", EXACT_MATCH_PREFIX + "Fed", SearchDepth.Search24Hours);
            feed4 |= ResultContains("", "5613456", "", "", "NPO", SearchDepth.Search24Hours);
            feed4 |= ResultContains("", "117648", "", "", "", SearchDepth.Search24Hours);
            feed4 |= ResultContains("", "316346183", "", "", "", SearchDepth.Search24Hours);
            feed4 |= ResultContains("", "345372935", "", "", "", SearchDepth.Search24Hours);
            feed4 |= ResultContains("", "304208921", "", "", "", SearchDepth.Search24Hours);
            feed4 |= ResultContains("", "406392841", "", "", "", SearchDepth.Search24Hours);
            feed4 |= ResultContains("", "406392835", "", "", "", SearchDepth.Search24Hours);
            feed4 |= ResultContains("", "406392847", "", "", "", SearchDepth.Search24Hours);
            feed4 |= ResultContains("", "406392853", "", "", "", SearchDepth.Search24Hours);
            feed4 |= ResultContains("", "MED", "", "", "parenteral", SearchDepth.Search24Hours);
            feed4 |= ResultContains("", "5613402", "", "", "Mechanical Ventilator");

            bool bath4 = false;
            bath4 |= ResultContains("", "5613416", "", "", "Complete/Total Care", SearchDepth.Search24Hours);

            if (feed4 && bath4) SetInd(4, "Complete bathing and feeding found in past 24 hours");

            //SetIndIfResultContains(4, "", "5613402", "", "", "High Flow Humidified Oxygen");
            //SetIndIfResultContains(4, "", "5613402", "", "", "High Flow Reservoir Mask");
            //SetIndIfResultContains(4, "", "5613402", "", "", "High Flow Venturi Mask");
            //SetIndIfResultContains(4, "", "5613402", "", "", "Mechanical Ventilator");
            //SetIndIfResultContains(4, "", "5613402", "", "", "Non rebreathing Mask");
            //SetIndIfResultContains(4, "", "5613402", "", "", "Bi-Level Posivitive Airway Pressure");

            if (!_inds[2].is_checked && !_inds[3].is_checked && !_inds[4].is_checked)
            {
                SetInd(1, "No ADL documentation found - defaulting to ADL Self");
            }


        }


        private void Check_5()
        {
            string reslist;
            string found_what;
            
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("5. ADL Rehab");
            Program.VerboseAudit("---------------");
            
            if (IsRehab()) SetInd(5, "Unit is RHB");
            SetIndIfResultContains(5, "", "303419250", "", "", "Bowel Retraining");
            SetIndIfResultContains(5, "", "303419210", "", "", "Bladder Retraining");
            if (ResultContains("", "303419044", "", "", "Nursing"))
            {
                SetIndIfResultContains(5, "", "5613457", "", "", "ADL Rehabilitative");
                SetIndIfResultContains(5, "", "5613416", "", "", "ADL Rehabilitative");
                SetIndIfResultContains(5, "", "5613420", "", "", "ADL Rehabilitative");
                SetIndIfResultContains(5, "", "5613422", "", "", "ADL Rehabilitative");
                SetIndIfResultContains(5, "", "1229815929", "", "", "ADL Rehabilitative");
            }
        }


        private void Check_6_7()
        {    
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("6. ADL 2-3 Caregivers");
            Program.VerboseAudit("7. ADL 4 or more Caregivers");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(6, "", "5613422", "", "", "Mobilize with 2-3 person assist");
            SetIndIfResultContains(6, "", "1572632461", "", "", "Mobilize with 2-3 person assist");

            SetIndIfResultContains(7, "", "5613422", "", "", "Mobilize with > 3 person assist");
            SetIndIfResultContains(7, "", "1572632461", "", "", "Mobilize with > 3 person assist");

        }

        private void Check_8()
        {
            string reslist;
            string other_str;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("8. Communication");
            Program.VerboseAudit("---------------");

            bool coma = ResultContains("", "305994957", "", "", "Comatose");
            SetIndIfResultContains(8, "", "", "Interpreter Needed", "", "Yes", SearchDepth.SearchSinceAdmission);
            SetIndIfResultContains(8, "", "", "Interpreter Needed", "", "ATT Line explained to patient", SearchDepth.SearchSinceAdmission);
            SetIndIfResultContains(8, "", "", "Interpreter Needed", "", "Patient/Family refused intepreter", SearchDepth.SearchSinceAdmission);
            SetIndIfResultContains(8, "", "92408462", "", "", "Yes");
            SetIndIfResultContains(8, "", "92408462", "", "", "ATT Line explained to patient", SearchDepth.SearchSinceAdmission);
            SetIndIfResultContains(8, "", "92408462", "", "", "Patient/Family refused intepreter");

            if (coma)
            {
                Program.Audit("Patient is comatose; Communication not applicable except for translation.");
                return;
            }
            SetIndIfResultContains(8, "", "", "Barriers to Learning", "", "Reading",SearchDepth.SearchSinceAdmission);
            SetIndIfResultContains(8, "", "", "Reading Problems", "", "Unable to read", SearchDepth.SearchSinceAdmission);
            SetIndIfResultContains(8, "", "", "Speech Problems", "", "Unable,incomprehensible,speaking valve");
            SetIndIfResultContains(8, "", "", "Hearing Problems", "", "Uncompensated");
            SetIndIfResultContains(8, "", "", "Hearing Problems", "", "deaf", SearchDepth.SearchSinceAdmission);
            SetIndIfResultContains(8, "", "", "Vision Problems", "", "Uncompensated");
            SetIndIfResultContains(8, "", "", "Vision Problems", "", "legally blind", SearchDepth.SearchSinceAdmission);

            SetIndIfResultContains(8, "", "92329080", "", "", "Unable to read");
            SetIndIfResultContains(8, "", "92329083", "", "", "Incomprehensible speech");
            SetIndIfResultContains(8, "", "92329083", "", "", "Unable to Speak");
            SetIndIfResultContains(8, "", "92329083", "", "", "Unable to Assess");
            SetIndIfResultContains(8, "", "92329083", "", "", "Speaking Valve");
            SetIndIfResultContains(8, "", "92408465", "", "", "Hearing Problem Uncompensated");
            SetIndIfResultContains(8, "", "92408463", "", "", "Vision Problem Uncompensated");
            SetIndIfResultContains(8, "", "7812343", "", "", "artificial airway");
            SetIndIfResultContains(8, "", "7812343", "", "", "Difficulty speaking");
            SetIndIfResultContains(8, "", "7812343", "", "", "expressive aphasia");
            SetIndIfResultContains(8, "", "7812343", "", "", "Inappropriate speech");
            SetIndIfResultContains(8, "", "7812343", "", "", "incomprehensible sounds");
            SetIndIfResultContains(8, "", "7812343", "", "", "receptive aphasia");
            SetIndIfResultContains(8, "", "7812343", "", "", "repetitive speech");
            SetIndIfResultContains(8, "", "7812343", "", "", "slurred");
            SetIndIfResultContains(8, "", "5613405", "", "", "Bivona Tube");
            SetIndIfResultContains(8, "", "5613405", "", "", "ET Tube");
            SetIndIfResultContains(8, "", "5613405", "", "", "Lary Tube");
            SetIndIfResultContains(8, "", "5613405", "", "", "Shiley Tube");
            SetIndIfResultContains(8, "", "481831381", "", "", "Mild to moderate aphasia");
            SetIndIfResultContains(8, "", "481831381", "", "", "Severe aphasia");
            SetIndIfResultContains(8, "", "481831381", "", "", "Mute");
            SetIndIfResultContains(8, "", "297618593", "", "", "Mild to moderate dysarthria = 1");
            SetIndIfResultContains(8, "", "297618593", "", "", "Near unintelligible or worse = 2");
            SetIndIfResultContains(8, "", "17076152", "", "", "Hearing Loss");
            SetIndIfResultContains(8, "", "17076154", "", "", "Difficulty Speaking");

            if (ResultContains("", "303419125", "", "", "Active,Not Met"))
            {
                SetIndIfResultContains(8, "", "303419100", "", "", "Expressive communication Impaired,Receptive communication impaired", SearchDepth.SearchSinceAdmission);
            }

            if (ResultContains("", "303419382", "", "", "Active,Not Met"))
            {
                SetIndIfResultContains(8, "", "303419356", "", "", "Aphasia,Dysarthria,Dysphonia,Cognitive Linguistic Impairment", SearchDepth.SearchSinceAdmission);
            }


            SetIndIfResultContains(8, "", "5613218", "", "", "Reading");
            SetIndIfResultContains(8, "", "10512644", "", "", "Expressive communication Impaired,Receptive communication impaired");
            reslist = "Aphasia,Dysarthria,Cognitive Linguistic Impairment,Dysphonia";
            SetIndIfResultContains(8, "", "105124801", "", "", reslist);

        }

        private void Check_9()
        {
            string reslist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("9. Cognitive Support");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(9, "", "7812341", "", "", "Not oriented to time");
            SetIndIfResultContains(9, "", "7812341", "", "", "Not oriented to place");
            SetIndIfResultContains(9, "", "7812341", "", "", "Not oriented to person");
            SetIndIfResultContains(9, "", "7812341", "", "", "Short Term Memory Loss");

            SetIndIfResultContains(9, "", "7812342", "", "", "Confused");
            SetIndIfResultContains(9, "", "92408458", "", "", "Patient is cognitively Impaired");
            SetIndIfResultContains(9, "", "5613218", "", "", "Cognitive");
            SetIndIfResultContains(9, "", "97036799", "", "", "Confused");
            SetIndIfResultContains(9, "", "772835966", "", "", "Positive CAM-ICU");
            SetIndIfResultContains(9, "", "1694018909", "", "", "Positive bCAM");
            SetIndIfResultContains(9, "", "481831074", "", "", "Answers one correctly");
            SetIndIfResultContains(9, "", "481831074", "", "", "Answers none correctly");

            SetIndIfResultContains(9, "", "", "Reason unable to complet", "", "cognitively impaired");
            SetIndIfResultContains(9, "", "", "Barriers to Learning", "", "Cognitive");

            if (ResultContains("", "303419125", "", "", "Active,Not Met"))
                SetIndIfResultContains(9, "", "303419100", "", "", "Cognition,Confusion,Orientation,Impaired memory", SearchDepth.SearchSinceAdmission);

            reslist = "Cognition,Confusion,Orientation,Impaired memory";
            SetIndIfResultContains(9, "", "", "10512644", "", reslist);

        }

        private void Check_10_11()
        {
            string reslist, found_what;
            //int ct;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("10. Behavior/Emotional Management");
            Program.VerboseAudit("11. Behavior/Emotional Mgmt - q 1 Hour");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(10, "", "303419135", "", "", "Nursing");
            SetIndIfResultContains(10, "", "303419483", "", "", "Nursing");
            SetIndIfResultContains(10, "", "303693346", "", "", "");

            //reslist = "Angry,Anxious,Combative,Crying,Depressed,Fearful";
            //reslist += ",Hallucinations,Hopelessness,Hostile,Irritability";
            //reslist += ",Labile,Restless,Sadness,Suicidal Tendencies,Violent,Withdrawn,Worried";
            //SetIndIfResultContains(10, "", "7812626", "", "", reslist);

            reslist = "Angry,Anxious,Crying,Denial,Fearful,Labile,Overwhelmed";
            reslist += ",Stressed,Affect inconsistent with mood,Aggressive physically,Aggressive Verbally";
            SetIndIfResultContains(10, "", "1927671495", "", "", reslist);

            reslist = "Paranoia,Delusions,Hallucinations";
            SetIndIfResultContains(10, "", "1927671163", "", "", reslist);

            reslist = "Repetitive purposeful activity,Argumentative,Restless,Repetitive vocalizations";
            SetIndIfResultContains(10, "", "1927671217", "", "", reslist);

            SetIndIfResultContains(11, "", "316346213","","","Violent/Self Destructive");
            SetIndIfResultContains(11, "", "527480678","","","Yes-Behavioral/Violent Restraint");
            //SetIndIfResultContains(11, "", "305006262", "", "", "Yes");

            SetIndIfResultContains(11, "", "1249837227", "", "", "");

            string codelist = "303693346,18802346,7812625,1228232355";
            int ct = ReturnQ1HrCount(codelist, "");
            if (IsQ1Hour(ct)) SetInd(11, "Item count = " + ct);
   
        }

        // (this isn't really a Q1h count -- it is just a count) Make this a q1hr count
        private int ReturnQ1HrCount(string code_list, string result_list)
        {
            var buckets = new List<gBucket>();
            SetBucketSize(60);
            AddBuckets(buckets, "", code_list, "", "", result_list);
            return CountBuckets(buckets);
        }

        private bool IsQ1Hour(int count)
        {
            return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q1H);
        }
        private bool IsQ2Hour(int count)
        {
            return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q2H);
        }

        // Use this if counting safety buckets
        //private void CheckSafety(int count, string desc)
        //{
        //    if (_inds[13].is_checked) return;             //skip if highest already checked
        //    if (count == 0) return;
        //    switch (FreqForCount(_pat.los_hours, count))
        //    {
        //    case Frequencies.Q30M:
        //        SetInd(13, desc + " q30min");
        //        break;
        //    case Frequencies.Q1H:
        //    case Frequencies.Q2H:
        //        SetInd(12, desc + " q2h");
        //        break;
        //    default:
        //        Program.VerboseAudit(desc + ": " + count + " in " + Math.Round(_pat.los_hours) + " hours is not enough");
        //        break;
        //    }
        //}

        private void Check_12_13()
        {
            bool suppress13=false;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("12. Safety Management - q 2 Hours");
            Program.VerboseAudit("13. Safety Management - q 30 Minutes");
            Program.VerboseAudit("---------------");

            int ct = ReturnQ1HrCount("316346213", "Attempting to Remove");
            if (IsQ2Hour(ct)) SetInd(12, "Item count = " + ct);

            SetIndIfResultContains(12, "", "305006247", "", "", "Yes");
            //SetIndIfResultContains(12, "", "104632002", "", "", "Documentation of restraints q 2 hours");
            SetIndIfResultContains(12, "", "303419513", "", "", "Nursing");

            SetIndIfResultContains(13, "", "5613460","","","Sitter");
            SetIndIfResultContains(13, "", "316346213","","","Violent/Self Destructive");
            SetIndIfResultContains(13, "", "527480678","","","Yes-Behavioral/Violent Restraint");
            SetIndIfResultContains(13, "", "305006262","","","Yes");
            SetIndIfResultContains(13, "", "1375032519","","","sitter at bedside");
            SetIndIfResultContains(13, "", "2198328955","","","initiated,continued");

        }

        private void Check_14()
        {
            string fcode = "";
            string fres = "";
            string fdesc = "";
            string nfcode = "";
            string nfres = "";
            string nfdesc = "";

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("14. Isolation");
            Program.VerboseAudit("---------------");

//use the latest thing: either none, or one of the triggers
            //if (Exists("", "5613462", "", "", "None") || Exists("", "Isolation Type", "", "", "None"))
            //    return;
            //SetIndIfResultContains(14, "", "5613462", "", "", "Airborne");
            //SetIndIfResultContains(14, "", "5613462", "", "", "Contact");
            //SetIndIfResultContains(14, "", "5613462", "", "", "CJD Precautions");
            //SetIndIfResultContains(14, "", "5613462", "", "", "Droplet");
            //SetIndIfResultContains(14, "", "5613462", "", "", "Negative Air Room");
            //SetIndIfResultContains(14, "", "5613462", "", "", "Protective Environment");
            //SetIndIfResultContains(14, "", "Isolation Type", "", "", "Airborne");
            //SetIndIfResultContains(14, "", "Isolation Type", "", "", "Contact");
            //SetIndIfResultContains(14, "", "Isolation Type", "", "", "CJD Precautions");
            //SetIndIfResultContains(14, "", "Isolation Type", "", "", "Droplet");
            //SetIndIfResultContains(14, "", "Isolation Type", "", "", "Negative Air Room");
            //SetIndIfResultContains(14, "", "Isolation Type", "", "", "Protective Environment");

            bool first = true;
            DateTime latest_time = DateTime.MinValue;
            DateTime none_latest_time = DateTime.MinValue;
            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = AndCodeInList(query, "5613462,isolation type");
            query = AndResultInList(query, "none");
            query = query.OrderByDescending(e => e.EVENT_DATETIME);
            if (query.Count() > 0)
            {
                foreach (var ci in query)
                {
                    if (first)
                    {
                        none_latest_time = ci.EVENT_DATETIME;
                        nfcode = ci.CODE;
                        nfres = ci.RESULT;
                        nfdesc = ci.DESCRIPTION;
                        first = false;
                    }
                }
            }

            first = true;
            var query2 = StartNewQuery(SearchDepth.SearchDefault);
            query2 = AndCodeInList(query2, "5613462,isolation type");
            query2 = AndResultInList(query2, "airborne,contact,cjd precautions,droplet,negative air room,protective environment");
            query2 = query2.OrderByDescending(e => e.EVENT_DATETIME);
            if (query2.Count() > 0)
            {
                foreach (var ci in query2)
                {
                    if (first)
                    {
                        latest_time = ci.EVENT_DATETIME;
                        fcode = ci.CODE;
                        fres = ci.RESULT;
                        fdesc = ci.DESCRIPTION;
                        first = false;
                    }
                }
            }
            if (none_latest_time > latest_time)
                Program.VerboseAudit("Isolation:none was found; Code=" + nfcode + " Descript=" + nfdesc + " Result=" + nfres + "  at time=" + none_latest_time.ToString());
            if ((latest_time > DateTime.MinValue) && (latest_time >= none_latest_time)) 
                SetInd(14, "FOUND Code=" +fcode+ " Descript=" + fdesc+ " Result="+ fres +"  at time=" + latest_time.ToString());

        }

        private void CheckAssessment(int count, string desc)
        {
            //if (_inds[18].is_checked) return;          //skip if highest already checked
            if (count == 0) return;                    //skip if none
            
            // This should work the same as the original code:
            switch (FreqForCount(_pat.los_hours, count)) {
                case Frequencies.Q30M:
                    SetInd(18, desc);
                    break;
                case Frequencies.Q1H:
                    SetInd(17, desc);
                    break;
                case Frequencies.Q2H:
                    SetInd(16, desc);
                    break;
                case Frequencies.Q4H:
                    SetInd(15, desc);
                    break;
                default:
                    break;
            }

        }

        private void ShowBuckets(List<gBucket> buckets)
        {
            string s = "";
            foreach (var e in buckets)
            {
                s = "(" + e.bucket + "," + e.code + "," + e.evdt.ToString() + ")";
                Program.VerboseAudit("Assessmt item:=" + s);
            }
        }


        private void Check_15_16_17_18()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("15. Assessment q4h");
            Program.VerboseAudit("16. Assessment q2h");
            Program.VerboseAudit("17. Assessment q1h");
            Program.VerboseAudit("18. Assessment q30min");
            Program.VerboseAudit("---------------");

            CountAssessments(30);               // always do q30 buckets--but use different freq tables for icu vs non-icu.

        }

        private bool IsRehab()
        {
            switch (_pat.unit_name)
            {
                case "RHB":
                    return true;
                default:
                    return false;
            }
        }


        //private bool IsICU()
        //{
        //    switch (_pat.unit_name)
        //    {
        //        case "ICU":
        //        case "3BR":
        //        case "CTIC":
        //        case "NIC":
        //        case "PCU":
        //        case "TICU":
        //        case "CCB":
        //        case "RICU":
        //        case "CCUS":
        //            return true;
        //        default:
        //            return false;
        //    }
        //}

        //private bool IsTele()
        //{
        //    switch (_pat.unit_name)
        //    {
        //        case "4EST":
        //        case "2NW":
        //        case "3AE":
        //        case "5MEH":
        //        case "5NW":
        //        case "6MEH":
        //        case "6NW":
        //        case "7MEH":
        //        case "B4W":
        //        case "B5S":
        //        case "B5W":
        //        case "2NOB":
        //        case "CPLX":
        //        case "TELMS":
        //        case "MAN4":
        //            return true;
        //        default:
        //            return false;
        //    }
        //}


        private void CountAssessments(int bucket_size)
        {
            int ct;
            string codelist;
            string reslist;
            List<gBucket> buckets;

            SetBucketSize(bucket_size);

            //buckets = new List<gBucket>();
            //AddBuckets(buckets, "", "7812497", "", "", "Endotracheal,Nasal,Nasal Tracheal Suction,Oral Suction,Tracheal Suction");
            //codelist = "117646,117590,117589,";
            //codelist += "117531,17343783,305994900,117638,141615,142506593,12499648,";
            //codelist += "117509,117498,117496,117584,305994913,305994918,3059991317,";
            //codelist += "117544,292718281,305995220,130920968,350850553,";
            //codelist += "305056309,305056294,305056394,633685506,305056509,305056484,";
            //codelist += "481751481,481752177,304208931,316381365,305056531,316346132,";
            //codelist += "305056541,117533,117643,304208961,7812336,18802339,305994957,";
            //codelist += "7812341,7812342,7812343,7812365,18802341,7812366,12499648,";
            //codelist += "305995290,7812367,305995300,7812371,7812372,7812373,7812374,7812375,";
            //codelist += "7812376,7812377,7812464,7812465,7812466,7812467,7812468,7812469,";
            //codelist += "7812470,7812493,18802342,7812494,7812495,7812496,";
            //codelist += "7812506,7812522,18802343,7812531,18802344,7812540,18802345,";
            //codelist += "758359245,325564484,302915761,302915771,592033203,592033208,";
            //codelist += "592033213,345372930,412500531,302916056,302916076,758629716,";
            //codelist += "758608948,758642066,758669207,303692843,383992195,303692914,";
            //codelist += "383992207,303693055,383992201,772531865,772522579,303693186,";
            //codelist += "303693191,303693196,303693201,303693206,303693211,303693216,";
            //codelist += "303693221,7812529,305056504,406392859,406392865,303693351,";
            //codelist += "303693371,303693346";
            //AddBuckets(buckets, "", codelist, "", "");
            //AddBuckets(buckets, "", "MED", "", "Heparin,Bivalrudin,Argatroban,Sodium Bicarb");
            //ct = CountBuckets(buckets);
            //CheckAssessment(ct, "All assessments count=" + ct);
            //ShowBuckets(buckets);

            buckets = new List<gBucket>();
            AddDependentBuckets(buckets, "117531,17343783", "117590,117589");
            codelist = "117646,141615,117498,117496,117584,305994913,305994918,3059991317";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Vitals=" + ct);
            ShowBuckets(buckets);

            buckets = new List<gBucket>();

            codelist = "117509,7812365,18802341,7812366,12499648,305995290,7812367,305995300,7812371";
            codelist += ",7812372,7812373,7812374,7812375,7812376,7812377,7812464,7812465,7812466";
            codelist += ",7812467,7812468,7812469,7812470";
            codelist += ",302915631,302915651,302915651,302915796,758359245,2163556287,2163556675,325564459";
            codelist += ",325564484,302915921,383992177,302915696,302915761,302915771,592033173,592033203";
            codelist += ",592033208,592033213,345372885,345372930,412498371,412500531,302916021,302916056";
            codelist += ",302916076,758595380,758629716,758608948,758637775,758642066,758669207,864827398,864830261";
            codelist += ",303693186,303693191,303693196,303693201,303693206,303693211,303693216,303693221";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Cardiac=" + ct);
            ShowBuckets(buckets);


            buckets = new List<gBucket>();
            //codelist = "292718281,305995220";
            //AddBuckets(buckets, "", codelist, "", "");
            codelist = "7812364";
            AddBuckets(buckets, "", codelist, "", "", "Cold,Heat,Massage,Relaxation Techniques,Medication");
            codelist = "Pain Intervention";
            AddBuckets(buckets, "", codelist, "", "", "Cold,Heat,Relaxation Techniques,Medication,Massage");
            AddBuckets(buckets, "", "Pain intensity", "", "");
            //AddBuckets(buckets, "", "", "Medication Not given for pain", "Yes");
            AddBuckets(buckets, "", "Alternative Pain scale used", "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Pain=" + ct);
            ShowBuckets(buckets);
            //if (_inds[18].is_checked) return;

            buckets = new List<gBucket>();
            //codelist = "12499648,117509,7812365,18802341,7812366,12499648,305995290,7812367,";
            //codelist += "305995300,7812371,7812372,7812373,7812374,7812375,7812376,7812377,";
            //codelist += "7812464,7812465,7812466,7812467,7812468,7812469,7812470,303031350,";
            //codelist += "325754541,303031329,532319071,345591779,303693186,303693191,303693196,";
            //codelist += "303693201,303693206,303693211,303693216,303693221";
            //AddBuckets(buckets, "", codelist, "", "");
            AddDependentBuckets(buckets, "305994900", "117638");
            codelist = "7812493,18802342,7812494,7812495,7812496,142506593";
            AddBuckets(buckets, "", codelist, "", "");
            reslist = "Endotracheal,Nasal,Oral Suction,Tracheal Suction";
            AddBuckets(buckets, "", "7812497", "", "", reslist);
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Respiratory=" + ct);
            ShowBuckets(buckets);
            //if (_inds[18].is_checked) return;

            buckets = new List<gBucket>();
            codelist = "117544,7812336,18802339,305994957,7812341,7812342,7812343,303693351,303693371,303693346,481831474";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Neurological=" + ct);
            ShowBuckets(buckets);
            //if (_inds[18].is_checked) return;

            buckets = new List<gBucket>();
            //codelist = "7812540,18802345,304015183,345603431";
            codelist = "7812540,18802345,303692843,383992195,303692914,383992207";
            codelist += ",303693055,383992201,772531865,772522579";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Wound=" + ct);
            ShowBuckets(buckets);
            //if (_inds[18].is_checked) return;

            buckets = new List<gBucket>();
            codelist = "130920968,350850553";
            AddBuckets(buckets, "", codelist, "", "");
            //reslist = "Heparin,Bivalrudin,Argatroban,Sodium Bicarb";
            //AddBuckets(buckets, "", "MED", reslist, "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Medications=" + ct);
            ShowBuckets(buckets);
            //if (_inds[18].is_checked) return;

            buckets = new List<gBucket>();
            codelist = "305056394,633685506,305056509,481751481,481752177,304208931,316381365,";
            codelist += "305056531,316346132,117533,117643,304208961,7812529,305056504,406392859,406392865";
            AddBuckets(buckets, "", codelist, "", "");
            reslist = "APD,Biliary,Blake,CAPD,Drain,Constavac,Davol,G-J Tube,G-Tube,";
            reslist += "Hemovac,JP,J-Tube,PEG tube,Pelvic Drain,Pendrose Drain,";
            reslist += "Pericardial,T-Tube,Other";
            AddBuckets(buckets, "", "305056269", "", "", reslist);
            AddBuckets(buckets, "", "305056309,305056294,305056484,305056541", "", "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Fluid Mgt=" + ct);
            ShowBuckets(buckets);
            //if (_inds[18].is_checked) return;



            buckets = new List<gBucket>();
            codelist = "7812506,7812522,18802343,7812531,18802344";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "GI/GU=" + ct);
            ShowBuckets(buckets);
            //if (_inds[18].is_checked) return;
        }

        //=====================================================================
        // Group like assessments into buckets of time
        // Each bucket counts as one assessment in the frequency count

        private void AddDependentBuckets(List<gBucket> bucket_list, string codelist1, string codelist2)
        {
            // get the chart items for the assessments
            var query1 = StartNewQuery();
            query1 = AndItemFilter(query1, "", codelist1, "", "", "");
            var query2 = StartNewQuery();
            query2 = AndItemFilter(query2, "", codelist2, "", "", "");

            // figure out what buckets the events belong to
            var query1a = from item in query1
                          select new
                          {
                             bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                             code = item.CODE, 
                             evdt = item.EVENT_DATETIME
                          };
            var query2a = from item in query2
                          select new
                          {
                             bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                             code = item.CODE, 
                             evdt = item.EVENT_DATETIME
                          };
            string s = "BucketList1 for " + codelist1 + ": ";
            foreach (var item in query1a)
            {
                s += item.bucket + ",";
            }
            Program.VerboseAudit(s);

            s = "BucketList2 for " + codelist2 + ": ";
            foreach (var item in query2a)
            {
                s += item.bucket + ",";
            }
            Program.VerboseAudit(s);
            // Add to the list IFF items in both lists occur in same bucket
            foreach (var item1 in query1a)
            {
                foreach (var item2 in query2a)
                {
                    if (item1.bucket == item2.bucket)
                    {
                        var b = new gBucket();
                        b.bucket = item1.bucket;
                        b.code = item1.code;
                        b.evdt = item1.evdt;
                        bucket_list.Add(b);
                    }
                }
            }

        }


        private void SetBucketSize(int minutes)
        {
            _bucket_size = minutes;
        }

        //Add to the list of bucket numbers (redundant buckets are fine)
        private void AddBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field)
        {
            AddBuckets(bucket_list, cat, code_list, desc, field, "");
        }
        private void AddBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field, string result_list)
        {
            // Assign (virtual) bucket numbers to each assessment.  
            // Every x minutes after that is a new bucket.  Bucket zero = pull_start time.

            // get the chart items for the assessments
            var query = StartNewQuery();
            query = AndItemFilter(query, cat, code_list, desc, field, result_list);

            // This step is needed for those who want to count the # within a bucket, like Shands #24.
            // This will have no effect on those that count buckets.
            //var query2 = (from item in query select new { item.EVENT_DATETIME }).Distinct();
            var query2 = (from item in query select new { item.EVENT_DATETIME, item.CODE });

            // figure out what buckets the events belong to
            //var query3 = from item in query2
            //             select new {
            //                bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size)
            //             };
            //// Add to the list
            //foreach (var item in query3) {
            //    bucket_list.Add(item.bucket);
            //}

            var query3 = from item in query2
                         select new
                         {
                             bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                             code = item.CODE,
                             evdt = item.EVENT_DATETIME
                         };
            // Add to the list
            foreach (var item in query3)
            {
                var b = new gBucket();
                b.bucket = item.bucket;
                b.code = item.code;
                b.evdt = item.evdt;
                bucket_list.Add(b);
            }

            // This is just for auditing: Say what we are looking for and how many were found
            if (!String.IsNullOrEmpty(result_list)) {
                // print each word and if it was found or not
                int i = CountResultContains(cat, code_list, desc, field, result_list);
            } else {
                // print how many were found
                Program.VerboseAudit(Describe(cat, code_list, desc, field, ""));
            }
        }

        private int CountBuckets(List<gBucket> bucket_list)       // bucket list: (ha ha)
        {
            //int result = bucket_list.Distinct().Count();
            //if (result > 0) Program.VerboseAudit(result + " unique");
            //return result;
            int x = -99;
            int result = 0;
            //int result = bucket_list.Distinct().Count();
            var query = from b in bucket_list
                        orderby b.bucket ascending
                        select b;
            foreach (var b in query)
            {
                if (x != b.bucket)
                {
                    result++;
                    x = b.bucket;
                }
            }
            if (result > 0) Program.VerboseAudit(result + " unique");
            return result;
        }


        private void Check_19()
        {
            string desclist;
            int ct;
            string found_what;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("19. Vascular Access Site Mgt q1 Hour");
            Program.VerboseAudit("---------------");

            string codelist = "303419785,632824241,892363904,892363071,303419710,632821508,303419650,632820530,";
            codelist += "303419836,632822728,303419891,632823709,414141054,632824627";
            ct = ReturnQ1HrCount(codelist, "");
            if (IsQ1Hour(ct)) SetInd(19, "Item count = " + ct);

        }

        private void Check_20()
        {
            string medlist,found_what,found_what2;
            int ct,cttube,ctpo,ctcrush;
            
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("20. Medication Activity >= 20 minutes");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(20, "", "1694018549", "", "", "Yes");
            SetIndIfResultContains(20, "", "112722817", "", "", "");
            SetIndIfResultContains(20, "", "112722820", "", "", "");
            SetIndIfResultContains(20, "", "112722850", "", "", "");
            SetIndIfResultContains(20, "", "303693621", "", "", "");
            //SetIndIfResultContains(20, "", "305520292", "", "", "");
            //SetIndIfResultContains(20, "", "436691737", "", "", "");
            SetIndIfResultContains(20, "", "213677373", "", "", "");

            //Medication - would have to identify every possible identification	
            //MAR documentation of 6 or more Medications with a route of:	
            //Nasoenteric tube, Dtube, Gtube, Jtube, NDtube, NG Tube, NJ Tube, OD Tube, OG Tube, OJ Tube, Peg Tube, or PO/Crush
                //code=med description like tube or description like route=po
            cttube = CountItems( "", "MED", "TUBE", "", "", SearchDepth.SearchDefault, true, out found_what);
            ctcrush = CountItems( "", "MED", "ROUTE=Crush/PO", "", "", SearchDepth.SearchDefault, true, out found_what2);
            if (cttube + ctcrush >= 6) SetInd(20, "Count of Meds with route=tube or po/crush: " + cttube + ctcrush);

            //code=med and description like Peritoneal Dialysate dextrose
            //2 documentations within 8 hour period
            ct = CountItems( "", "MED", "Peritoneal Dialysate dextrose", "", "", SearchDepth.SearchDefault, true, out found_what);
            if (ct >= 2) SetInd(20, "Count of Peritoneal Dialysate dextrose: " + ct);
            
            SetIndIfResultContains(20, "", "112722850", "", "", "");
            SetIndIfResultContains(20, "", "304018158", "", "", "");

            //with the presence of six medication with route of PO PO/Crush route");
            // code=MED and description like route=po
            ctpo = CountItems("", "MED", "ROUTE=PO", "", "", SearchDepth.SearchDefault, true, out found_what2);
            if (ctpo + ctcrush >= 6) SetIndIfResultContains(20, "", "305006257", "", "", "");

        }

        private void Check_21_22()
        {
            string codelist;
            
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("21. Wound/Injury Mgmt");
            Program.VerboseAudit("22. Wound/Injury Mgmt >= 30 Minutes");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(21, "", "303693120", "", "", ""); //from ED change
            SetIndIfResultContains(21, "", "303692879", "", "", ""); //from ED change
            SetIndIfResultContains(21, "", "303692843", "", "", "");
            SetIndIfResultContains(21, "", "304015194", "", "", "");
            SetIndIfResultContains(21, "", "303692914", "", "", "");
            SetIndIfResultContains(21, "", "303692985", "", "", "");
            SetIndIfResultContains(21, "", "303693055", "", "", "");
            SetIndIfResultContains(21, "", "303693120", "", "", "");
            SetIndIfResultContains(21, "", "305056509", "", "", "");
            SetIndIfResultContains(21, "", "305056309", "", "", "");
            SetIndIfResultContains(21, "", "305056424", "", "", "");
            SetIndIfResultContains(21, "", "383992219", "", "", "");
            SetIndIfResultContains(21, "", "117642", "", "", "Bloody");
            SetIndIfResultContains(21, "", "117642", "", "", "Maroon");
            SetIndIfResultContains(21, "", "117642", "", "", "Red");
            SetIndIfResultContains(21, "", "117642", "", "", "Tarry");
            SetIndIfResultContains(21, "", "303419740", "", "", "Dressing Change");
            SetIndIfResultContains(21, "", "303419675", "", "", "Dressing Change");
            SetIndIfResultContains(21, "", "303419861", "", "", "Dressing Change");
            SetIndIfResultContains(21, "", "303419916", "", "", "Dressing Change");
            SetIndIfResultContains(21, "", "7812620", "", "", "");
            SetIndIfResultContains(21, "", "5613405", "", "", "");
            //SetIndIfResultContains(21, "", "305995275", "", "", "Bulla");
            //SetIndIfResultContains(21, "", "305995275", "", "", "Desquamous");
            //SetIndIfResultContains(21, "", "305995275", "", "", "Diffuse");
            //SetIndIfResultContains(21, "", "305995275", "", "", "Moderate");
            //SetIndIfResultContains(21, "", "305995275", "", "", "Raised");
            //SetIndIfResultContains(21, "", "305995275", "", "", "Rash <25");
            //SetIndIfResultContains(21, "", "305995275", "", "", "Rash 20-50");
            SetIndIfResultContains(21, "", "303693441", "", "", "Lacerations,Mouth Trauma");

            SetIndIfResultContains(21, "", "5613437", "", "", "");
            SetIndIfResultContains(21, "", "305056509", "", "", "");
            SetIndIfResultContains(21, "", "864827398", "", "", "");
            SetIndIfResultContains(21, "", "864830261", "", "", "");
            SetIndIfResultContains(21, "", "864827398", "", "", "");
            SetIndIfResultContains(21, "", "302916311", "", "", "Intact,Reinforced,Changed");

            SetIndIfResultContains(22, "", "552900219", "", "", "");
            //SetIndIfResultContains(22, "", "302916311", "", "", "Changed");

            if (!_inds[22].is_checked) CheckExtensiveWound();

        }

        private void CheckExtensiveWound()
        {
            DateTime evdt = DateTime.MinValue;
            DateTime st_time = DateTime.MinValue;
            DateTime en_time = DateTime.MinValue;
            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = AndCodeInList(query, "1375032519");
            query = AndResultInList(query, "extensive wound");
            query = query.OrderBy(e => e.EVENT_DATETIME);
            if (query.Count() == 0) return;
            foreach (var ci in query)
            { 
                if (!_inds[22].is_checked) {
                evdt = ci.EVENT_DATETIME;
                st_time = GetResultTime("1375032547", evdt, 2);
                en_time = GetResultTime("1375032561", evdt, 2);
                if ((st_time != DateTime.MinValue) &&
                    (en_time != DateTime.MinValue) && (st_time <= en_time))
                {
                    if (st_time.AddMinutes(30) <= en_time)
                        SetInd(22, "Extensive Wound Management>=30min start:" + st_time.ToString() + " end:" + en_time.ToString()); 
                }}
            }
        }

        // Use this if you can total the education time
        //private void CheckEducation(int total)
        //{
        //    if (_inds[23].is_checked) return;             //skip if already checked

        //    if (total >= 60) {
        //        SetInd(23, "education >= 60 min");
        //    }
        //}

        int EducMins(string timecode, string nursecode)
        {
            string res;
            int val = 0;
            if (ResultContains("", nursecode, "", "", "Nursing"))
            {
                if (GetResult("", timecode, "", "", out res))
                {
                    if (res.Left(1).IsNumeric())
                    {
                        val += (int)res.Val();
                    }
                }
            }
            return val;
        }

        private void Check_23()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("23. Healthcare Mgmt Education >= 1 Hour");
            Program.VerboseAudit("---------------");
            string res;
            int val = 0;
            int value = 0;

value += EducMins("Angina/MI Length of Instruct","Angina/MI Discipline");
value += EducMins("Anticoagulation Length of Instr","Anticoagulation Discipline Educa");
value += EducMins("Bone Marrow Length of Instruct","Bone Marrow Discipline");
value += EducMins("Cardiac Rehab Len of Instruct","Cardiac Rehab Discipline");
value += EducMins("Central Line Len of Instruct","Central Line Discipline");
value += EducMins("Chemo/Onc Length of Instruct","Chemo/Oncology Discipline");
value += EducMins("Diabetes Length of Instruct","Diabetes Discipline");
value += EducMins("Dialysis Length of Instruct","Dialysis Discipline");
value += EducMins("Generic Med Length of Instruct","General Medical Discipline");
value += EducMins("GenericSurgery Len of Instruct","Surgery Discipline");
value += EducMins("Heart Failure Len of Instruct","Heart Failure Discipline");
value += EducMins("Heart/PTCA Length of Instruct","Heart Cath/PTCA Discipline");
value += EducMins("Infection Ctrl Len of Instruct","Infection Control Discipline");
//value += EducMins("Medication Length of Instruct");
value += EducMins("Open Heart Surgery Len of Instr","Open Heart Surgery Discipline Ed");
value += EducMins("Nutrition Length of Instruct","Nutrition Discipline");
value += EducMins("Ostomy/Stoma Length of Instruct","Ostomy/Stoma Discipline");
value += EducMins("Rad Therapy/TBI Len of Instruct","Radiation Tx/TBI Discipline");
value += EducMins("Respiratory Tx Len of Instruct","Resp Treatments Discipline");
value += EducMins("Stroke Length of Instruct","Stroke Discipline");
value += EducMins("Therapy Srvcs Len of Instruct","Physical Therapy Discipline");
value += EducMins("Tracheostomy Len of Instructi","Tracheostomy Discipline");
value += EducMins("Trans Post-Op/DC Len of Instruct","Trans Post-Op/DC Discipline");
value += EducMins("Urinary Cath Len of Instruct","Urinary Cath Discipline");
value += EducMins("VAD/MCAD Length of Instruct","VAD/MCAD Discipline");
value += EducMins("Wound Length of Instruct","Wound Instruction Discipline");
value += EducMins("End of Life Date/Time of Instruc", "End of Life Discipline");


            if (value >= 60) SetInd(23, "Total combined instruction time=" + value + " mins");

        }

        private void Check_24()
        {
            string s;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("24. 1 to 1 Physiological Interv. >= 2 Hours");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(24, "", "1375032575", "", "", "Yes");
            SetIndIfResultContains(24, "", "1122231967", "", "", "Yes");
        }


        private void AtLeastOneADL()
        {
            Program.Audit("---------------");
            if (!(_inds[1].is_checked || _inds[2].is_checked || _inds[3].is_checked || _inds[4].is_checked)) {
                // Make "#2 ADL - Assist" the default.  (90% of patients)
                SetInd(1, "Defaulting to ADL Self due to lack of documentation.");
            }
        }

        private void HighestIndicatorInEachGroupWins()
        {
            int i, g;
            bool highest_is_on;
            string ind_list;

            Program.Audit("Select highest indicator in each group");
            
            g = 0;
            highest_is_on = false;
            // Go from bottom up - highest (lowest) indicator in each group wins
            for (i = MAX_INDS; (i >= 1); i--) {
                if (_inds[i].radio_group > 0) {
                    if (_inds[i].radio_group != g) {
                        //this is a new group
                        g = _inds[i].radio_group;
                        highest_is_on = _inds[i].is_checked;
                    } else {
                        //same group
                        if (highest_is_on) {
                            _inds[i].is_checked = false;             //uncheck a lower number
                        } else {
                            highest_is_on = _inds[i].is_checked;     //save this one
                        }
                    }
                }
            } // next i
            
            //Echo the indicators for an Audit (no classification will be saved)
            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++) {
                if (_inds[i].is_checked) ind_list += "," + i;
            } // next i

            Program.Audit("Final list = " + ind_list.Substring(1));
            Program.Audit("---------------");
        }


        private void CheckProcs()
        {
            CheckProc_1_2();
            CheckProc_3();
            CheckProc_4();
            CheckProc_5();
            CheckProc_67();
            CheckProc_8();
            CheckProc_91011();

        }

        //private void DoProc(int pnum, string code, string result, string sttime_code, string fntime_code)
        //{
        //    double mins = 0;
        //    string found_res;
        //    DateTime evdt = DateTime.MinValue;
        //    DateTime enddt = DateTime.MinValue;

        //    var query = StartNewQuery(SearchDepth.SearchDefault);
        //    query = AndCodeInList(query, code);
        //    query = AndResultInList(query, result);
        //    query = query.OrderBy(e => e.EVENT_DATETIME);
        //    foreach (var ci in query)
        //    { //pick the earliest one if multiple
        //        if (evdt == DateTime.MinValue) evdt = ci.EVENT_DATETIME;
        //    }

        //    DateTime st_time = GetResultTime(sttime_code, evdt);
        //    DateTime en_time = GetResultTime(fntime_code, evdt);

        //    if (st_time == DateTime.MinValue || en_time == DateTime.MinValue || st_time>=en_time)
        //    {
        //        Program.Audit("Data for Activity " + pnum + " has invalid datetime(s):  start="+st_time.ToString()+"  finish="+en_time.ToString());
        //        return;
        //    }

        //    if (ProcExistsInDB(pnum, st_time, en_time))
        //    {
        //        Program.Audit("Activity " + pnum+ ": already exists");
        //    }
        //    else
        //    {
        //        if (!QueuedProcOverlaps(pnum, st_time, en_time))
        //        {
        //            var proc = new proc_data();
        //            proc.procedure_number = pnum;
        //            proc.start = evdt;
        //            proc.finish = enddt;
        //            _procs.Add(proc);
        //            Program.Audit("Activity " + pnum + ": Found " + code + " between " + evdt + " and " + enddt);
        //        }
        //    }

        //}


        DateTime GetResultTime(string code, DateTime evdt, int offset)
        { //offset is the starting pos of yyyyMMddHHmm in the result string
            string res = "";
            string s;
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = AndCodeInList(query, code);
            query = query.Where(e => (e.EVENT_DATETIME == evdt));
            if (query.Count() == 0)
            {
                Program.VerboseAudit("Time not found for DTA=" + code + " at time="+evdt.ToString());
                return DateTime.MinValue;
            }
            foreach (var ci in query)
            {
                res = ci.RESULT;
            }
            if (res.Length >= 12)
            {
                s = res.Substring(offset, 12);
                Program.Audit("time string=" + s);
            }
            else
                return DateTime.MinValue;
            
            if (s.Length == 12)
            {
                //string format = "yyyyMMddHHmm";
                DateTime d = PFSUtility.ISOToDateTime(s);
                Program.VerboseAudit("time=" + d.ToString());
                return d;
            }
            else
                return DateTime.MinValue;

        }

        private bool QueuedProcOverlaps(int pnum, DateTime startdt, DateTime enddt)
        {
            bool overlap = false;

            proc_data[] pary = _procs.ToArray();
            for (int i = 0; i <= pary.GetUpperBound(0); i++)
            {
                if (pary[i].procedure_number == pnum)
                {
                    //overlap if   p.start between stardt and enddt
                    //             p.finish between startdt and enddt
                    //             startdt >= p.start and enddt <= p.finish
                    if ((pary[i].start >= startdt) && (pary[i].start < enddt) ||
                        (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
                        (startdt >= pary[i].start) && (enddt <= pary[i].finish))
                    {
                        overlap = true;
                    }

                    if ((pary[i].start >= startdt) && (pary[i].start < enddt) && (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
                        (pary[i].start < startdt) && (pary[i].finish > startdt) && (pary[i].finish < enddt)
                        )
                    {
                        // the proc in the list is completely contained in startdt/endt pair
                        // change this proc to be the startdt/enddt
                        // overlap is still true, but the list item will be updated with the encompassing times
                        //Program.Audit("Procedure " + pnum + ": " + startdt + " - " + enddt + " will trump " + pary[i].start + " - " + pary[i].finish);
                        pary[i].start = startdt;
                        pary[i].finish = enddt;
                    }
                }
            }
            _procs = pary.ToList();
            return overlap;
        }

        private void CheckProc_1_2()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A1. 1-1 safety observation by RN");
            Program.VerboseAudit("A2. 1-1 safety observation by non-RN");
            Program.VerboseAudit("---------------");

//1375032519	sitter at bedside
//1375032547	Date/Time field
//1375032561	Date/Time field
//1375032533	RN
            DateTime evdt = DateTime.MaxValue;
            var query = StartNewQuery(SearchDepth.SearchPullRange);
            query = query.Where(e => e.CODE == "1375032519");
            query = query.Where(e => e.RESULT.ToLower().Contains("sitter at bedside"));
            query = query.Where(e => e.ORDER_STATUS != "X");
            if (query.Count() == 0) return;
            foreach (var item in query)
            {
                evdt = item.EVENT_DATETIME;
                DoSitter(evdt);
            }


        }

        //1375032519	8075323	2017-01-12 12:59:00.000	Bedside Procedure/Activity	Sitter at bedisde
        //1375032533	8075323	2017-01-12 12:59:00.000	Staff Involved	Non-RN
        //1375032547	8075323	2017-01-12 12:59:00.000	Procedure/Activity Start Date/Time	0:2017011207300000:0.000000:59:0
        //1375032561	8075323	2017-01-12 12:59:00.000	Procedure/Activity End Date/Time	0:2017011213300000:0.000000:59:0        
        private void DoSitter(DateTime evdt)
        {

            var query = StartNewQuery(SearchDepth.SearchPullRange);
            query = query.Where(e => e.CODE == "1375032533"); //rn or non-rn
            query = query.Where(e => e.EVENT_DATETIME == evdt);
            if (query.Count() == 0)
            {
                CheckProc_2(evdt);  //nonrn by default when staff involved not present
            }
            else
            {
                foreach (var item in query)
                {
                    if (item.RESULT.ToLower().Contains("non-rn"))
                    {
                        CheckProc_2(evdt);  //nonrn
                    }
                    else
                    {
                        CheckProc_1(evdt);  //rn
                    }
                }
            }

        }

        private void CheckProc_1(DateTime evdt)
        {
            DateTime sttime = DateTime.MinValue;
            DateTime entime = DateTime.MinValue;
            int peid = 0;

            sttime = GetResultTime("1375032547", evdt, 2); //start time
            if (sttime > DateTime.MinValue) // then start time exists
            {
                entime = GetResultTime("1375032561", evdt, 2); //end time
                if (entime > sttime)
                {
                    if (ProcExistsInDB(2, sttime, entime, out peid))
                    {
                        Program.Audit("Activity 1: already exists");
                    }
                    else
                    {
                        var proc = new proc_data();
                        proc.procedure_number = 2;
                        proc.start = sttime;
                        proc.finish = entime;
                        _procs.Add(proc);
                        Program.Audit("Activity 1: Found RN Sitter between " + sttime.ToString() + " and " + entime.ToString());
                    }
                }

            }
        }

        private void CheckProc_2(DateTime evdt)
        {
            DateTime sttime = DateTime.MinValue;
            DateTime entime = DateTime.MinValue;
            int peid = 0;

            sttime = GetResultTime("1375032547", evdt, 2); //start time
            if (sttime > DateTime.MinValue) // then start time exists
            {
                entime = GetResultTime("1375032561", evdt, 2); //end time
                if (entime > sttime) //then no end time or endtime less than start
                {
                    if (ProcExistsInDB(2, sttime, entime, out peid))
                    {
                        Program.Audit("Activity 2: already exists");
                    }
                    else
                    {
                        var proc = new proc_data();
                        proc.procedure_number = 2;
                        proc.start = sttime;
                        proc.finish = entime;
                        _procs.Add(proc);
                        Program.Audit("Activity 2: Found Non-RN Sitter between " + sttime.ToString() + " and " + entime.ToString());
                    }
                }

            }

//305006247   Sitter
//305006262   Yes
            //var query = StartNewQuery(SearchDepth.SearchPullRange);
            //query = query.Where(e => (e.CODE == "5613460" && e.RESULT.ToLower().Contains("sitter")) ||
            //                         (e.CODE == "305006262" && e.RESULT == "Yes"));
            //if (query.Count() > 0)
            //{
            //    if (ProcExists(2, _pat.pull_start, _pat.pull_finish))
            //    {
            //        Program.Audit("Activity 2: already exists");
            //    }
            //    else
            //    {
            //        var proc = new proc_data();
            //        proc.procedure_number = 2;
            //        proc.start = _pat.pull_start;
            //        proc.finish = _pat.pull_finish;
            //        _procs.Add(proc);
            //        Program.Audit("Activity 2: Found Non-RN Sitter dta=5613460 - Assigning for entire shift between " + _pat.pull_start.ToString() + " and " + _pat.pull_finish.ToString());
            //    }
            //}
        }


        private bool ProcExistsInDB(int pnum, DateTime startdt, DateTime enddt, out int peid)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && (proc.PROCEDURE_DATETIME < enddt)
                            && (proc.DEPARTURE_DATETIME > startdt)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new {proc.PROCEDURE_EVENT_ID};
            foreach (var a in query)
            {
                DeleteActivity(a.PROCEDURE_EVENT_ID);
            }
            peid = 0;
            return (query.Count() > 0);
        }

        private void CheckProc_3()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A3. Off unit accompanied by RN");
            Program.VerboseAudit("---------------");

            CheckProc_34(3, "Unit Based RN");
        }

        private void CheckProc_4()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A4. Off unit accompanied by non-RN");
            Program.VerboseAudit("---------------");

            CheckProc_34(4, "Unit Based PCA/PCT");
        }

        private void CheckProc_34(int pnum,string caregiver)
        {
            //"Transfer Off Unit Accompanied by"  PCA/PCT
            //  Date/Time Patient Returned to Un    20160809113000
            //  Date/Time Transported Off Unit  20160809103000
            string code = "Transfer Off Unit Accompanied by";
            DateTime evdt = DateTime.MinValue;
            string sttime_code = "Date/Time Transported Off Unit";
            string fntime_code = "Date/Time Patient Returned to Un";
            int peid = 0;

            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = AndCodeInList(query, code);
            query = AndResultInList(query, caregiver);
            query = query.OrderBy(e => e.EVENT_DATETIME);
            if (query.Count() == 0) return;
            foreach (var ci in query)
            { //pick the earliest one if multiple
                if (evdt == DateTime.MinValue) evdt = ci.EVENT_DATETIME;
            }

            DateTime st_time = GetResultTime(sttime_code, evdt, 0);
            DateTime en_time = GetResultTime(fntime_code, evdt, 0);

            if (st_time == DateTime.MinValue || en_time == DateTime.MinValue)
            {
                return;
            }
            if (st_time >= en_time)
            {
                Program.Audit("Data for Activity " + pnum + " has invalid datetime(s):  start=" + st_time.ToString() + "  finish=" + en_time.ToString());
                return;
            }

            if (ProcExistsInDB(pnum, st_time, en_time,out peid))
            {
                Program.Audit("Activity " + pnum + ": already exists");
            }
            else
            {
                if (!QueuedProcOverlaps(pnum, st_time, en_time))
                {
                    var proc = new proc_data();
                    proc.procedure_number = pnum;
                    proc.start = st_time;
                    proc.finish = en_time;
                    _procs.Add(proc);
                    Program.Audit("Activity " + pnum + ": Found " + code + "  start=" + st_time.ToString() + "  end=" + en_time.ToString());
                }
            }


        }

        private void CheckProc_5()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A5. Patient/family education by RN");
            Program.VerboseAudit("---------------");
            //1375032589  educ start time in result
            //1375032603  educ end time in result
            string res="";
            string res2="";
            int pnum = 5;
            string code = "1375032519";
            DateTime evdt = DateTime.MinValue;
            string sttime_code = "1375032589";
            string fntime_code = "1375032603";
            DateTime st_time, en_time;
            int peid = 0;

            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = AndCodeInList(query, sttime_code);
            query = query.OrderBy(e => e.EVENT_DATETIME);
            if (query.Count() == 0) return;
            foreach (var ci in query)
            { //pick the earliest one if multiple
                if (evdt == DateTime.MinValue)
                {
                    evdt = ci.EVENT_DATETIME;
                    res = ci.RESULT;
                }
            }
            st_time = GetResultTime(sttime_code, evdt, 2); //start time
            Program.Audit("Education start time=" + st_time.ToString());

            en_time = GetResultTime(fntime_code, evdt, 2); //finish time
            if (en_time <= st_time) en_time = st_time.AddHours(1);
            Program.Audit("Education finish time=" + en_time.ToString());

            //string format = "yyyyMMddHHmm";
            //st_time = DateTime.ParseExact(res, format, System.Globalization.CultureInfo.InvariantCulture);
            //en_time = DateTime.ParseExact(res2, format, System.Globalization.CultureInfo.InvariantCulture);
            if (ProcExistsInDB(pnum, st_time, en_time,out peid))
            {
                Program.Audit("Activity " + pnum + ": already exists");
            }
            else
            {
                if (!QueuedProcOverlaps(pnum, st_time, en_time))
                {
                    var proc = new proc_data();
                    proc.procedure_number = pnum;
                    proc.start = st_time;
                    proc.finish = en_time;
                    _procs.Add(proc);
                    Program.Audit("Activity " + pnum + ": Found " + code + "  start=" + st_time.ToString() + "  end=" + en_time.ToString());
                }
            }

        }

        private void CheckProc_67()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A6. Extensive wound management by RN");
            Program.VerboseAudit("A7. Extensive wound management by non-RN");
            Program.VerboseAudit("---------------");

//1375032519		Extensive Wound Management
//1375032547	 Start Date/Time	Date/Time field
//1375032561	End Date/Time	Date/Time field
//1375033639		1:1 RN   or Non-RN

//1375032519		Bedside Procedure/Activity
//1375032533		Staff Involved
//1375032547		Procedure/Activity Start Date/Time
//1375032561		Procedure/Activity End Date/Time

            int pnum = 6;
            string code = "1375032519";
            DateTime evdt = DateTime.MinValue;
            string sttime_code = "1375032547";
            string fntime_code = "1375032561";
            int peid = 0;

            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = AndCodeInList(query, code);
            query = AndResultInList(query, "Extensive Wound Management");
            query = query.OrderBy(e => e.EVENT_DATETIME);
            if (query.Count() == 0) return;
            foreach (var ci in query)
            { //pick the earliest one if multiple
                if (evdt == DateTime.MinValue) evdt = ci.EVENT_DATETIME;
            }

//            int cgtype = GetCaregiverType("1375033639", evdt); // 0=none 1=RN  2=nonRN
            int cgtype = GetCaregiverType("1375032533", evdt); // 0=none 1=RN  2=nonRN
            if (cgtype == 0) return;
            if (cgtype == 1) pnum = 6;
            if (cgtype == 2) pnum = 7;

            DateTime st_time = GetResultTime(sttime_code, evdt, 2);
            DateTime en_time = GetResultTime(fntime_code, evdt, 2);

            if (st_time == DateTime.MinValue || en_time == DateTime.MinValue)
            {
                return;
            }
            if (st_time >= en_time)
            {
                Program.Audit("Data for Activity " + pnum + " has invalid datetime(s):  start=" + st_time.ToString() + "  finish=" + en_time.ToString());
                return;
            }

            if (ProcExistsInDB(pnum, st_time, en_time, out peid))
            {
                Program.Audit("Activity " + pnum + ": already exists");
            }
            else
            {
                if (!QueuedProcOverlaps(pnum, st_time, en_time))
                {
                    var proc = new proc_data();
                    proc.procedure_number = pnum;
                    proc.start = st_time;
                    proc.finish = en_time;
                    _procs.Add(proc);
                    Program.Audit("Activity " + pnum + ": Found " + code + "  start=" + st_time.ToString() + "  end=" + en_time.ToString());
                    if ((int)PFSUtility.DateDiffInMinutes(st_time, en_time) >= 30)
                    {
                        SetInd(22, "Bedside wound activity >= 30mins");
                    }
                }
            }


        }

        private int GetCaregiverType(string code, DateTime evdt)
        {
            //1375033639		1:1 RN   or Non-RN
            string r = "";
            int ret = 1;

            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = AndCodeInList(query, code);
            query = query.Where(e => e.EVENT_DATETIME == evdt);
            query = query.OrderBy(e => e.EVENT_DATETIME);
            if (query.Count() == 0) return 0;
            foreach (var ci in query)
            { 
                r = ci.RESULT;
            }
            if (r.Trim() == "Non-RN") ret = 2;
            return ret;

        }

        private void CheckProc_8()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A8. Coordination of care by RN");
            Program.VerboseAudit("---------------");
            
            int pnum = 8;
            string code = "1375032519";
            DateTime evdt = DateTime.MinValue;
            string sttime_code = "1375032547";
            string fntime_code = "1375032561";
            int peid = 0;

            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = AndCodeInList(query, code);
            query = AndResultInList(query, "Coordination of care");
            query = query.OrderBy(e => e.EVENT_DATETIME);
            if (query.Count() == 0) return;
            foreach (var ci in query)
            { //pick the earliest one if multiple
                if (evdt == DateTime.MinValue) evdt = ci.EVENT_DATETIME;
            }

            DateTime st_time = GetResultTime(sttime_code, evdt, 2);
            DateTime en_time = GetResultTime(fntime_code, evdt, 2);

            if (st_time == DateTime.MinValue || en_time == DateTime.MinValue)
            {
                return;
            }
            if (st_time >= en_time)
            {
                Program.Audit("Data for Activity " + pnum + " has invalid datetime(s):  start=" + st_time.ToString() + "  finish=" + en_time.ToString());
                return;
            }

            if (ProcExistsInDB(pnum, st_time, en_time, out peid))
            {
                Program.Audit("Activity " + pnum + ": already exists");
            }
            else
            {
                if (!QueuedProcOverlaps(pnum, st_time, en_time))
                {
                    var proc = new proc_data();
                    proc.procedure_number = pnum;
                    proc.start = st_time;
                    proc.finish = en_time;
                    _procs.Add(proc);
                    Program.Audit("Activity " + pnum + ": Found " + code + "  start=" + st_time.ToString() + "  end=" + en_time.ToString());
                }
            }

        }

        private void CheckProc_91011()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A9 1:1 RN at bedside");
            Program.VerboseAudit("A10 1:1 Non-RN at bedside");
            Program.VerboseAudit("A11 2:1 RN at bedside");
            Program.VerboseAudit("---------------");

            int pnum = 9;
            DateTime evdt = DateTime.MinValue;

            string reslist = "Bronchoscopy,Cardioversion,CBI Hand Irrigation,Code Blue";
            reslist += ",ECMO,Endoscopy,Exercising EMU Patients,Intubation,Stem cell infusion,LVAD Ramp Studies";
            reslist += ",Open chest at Bedside,Palliative Sedation Therapy,Roto-prone";
            reslist += ",TEE,Tube/Line Insertion,Other";
            string code =     "1375032519";
            string activity = "";
            string st_code =  "1375032547";
            string en_code =  "1375032561";
            string stf_code = "1375032533";
            string staff =    "";
            int peid = 0;

            //is there an activity?
            //if so, then get evdt of activity
            //see if there are st_code and en_code at evdt
            //see if there is stf_code at evdt
            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = AndCodeInList(query, code);
            query = AndResultInList(query, reslist);
            query = query.OrderBy(e => e.EVENT_DATETIME);
            if (query.Count() == 0) return;
            foreach (var ci in query)
            { //pick the earliest one if multiple
                if (evdt == DateTime.MinValue) 
                {
                    evdt = ci.EVENT_DATETIME;
                    activity = ci.RESULT;
                }
            }
            Program.Audit("Activity found = " + activity + " at event time=" + evdt.ToString());

            DateTime st_time = GetResultTime(st_code, evdt, 2);
            DateTime en_time = GetResultTime(en_code, evdt, 2);

            if (st_time == DateTime.MinValue || en_time == DateTime.MinValue)
            {
                return;
            }
            
            Program.Audit("Start time=" + st_time.ToString() + "  End time="+en_time.ToString());

            var query2 = StartNewQuery(SearchDepth.SearchDefault);
            query2 = AndCodeInList(query2, stf_code);
            query2 = query2.Where(e => e.EVENT_DATETIME == evdt);
            if (query2.Count() == 0) return;
            foreach (var ci in query)
            { //pick the earliest one if multiple
                if (evdt == DateTime.MinValue)
                {
                    staff = ci.RESULT;
                }
            }
            Program.Audit("Staff involved=" + staff);

            if (staff.Contains("1:1")) pnum = 9;
            if (staff.Contains("1:1") && staff.ToLower().Contains("non-rn")) pnum = 10;
            if (staff.Contains("2:1")) pnum = 11;

            if (ProcExistsInDB(pnum, st_time, en_time,out peid))
            {
                Program.Audit("Activity " + pnum + ": already exists");
            }
            else
            {
                if (!QueuedProcOverlaps(pnum, st_time, en_time))
                {
                    var proc = new proc_data();
                    proc.procedure_number = pnum;
                    proc.start = st_time;
                    proc.finish = en_time;
                    _procs.Add(proc);
                    Program.Audit("Activity " + pnum + ": Found " + code + "  start=" + st_time.ToString() + "  end=" + en_time.ToString());
                }
            }


        }

        private void CheckOutcomes()
        {
            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = AndItemFilter(query, "", "A_MHPostFalltime", "", "", "");
            foreach (var ch in query)
            {
                var outc = new proc_data();
                outc.procedure_number = 1;
                outc.start = ch.EVENT_DATETIME;
                _outcomes.Add(outc);
                Program.Audit("Outcomes 1: Found A_MHPostFalltime at " + outc.start);
            }
        }
        

        private const string DATETIME_FORMAT = "yyyyMMddHHmm";              // ISO Date/Time w/o seconds

        private void OutputClass()
        {
            string outstr, ind_list, desc, str_pull_dt;
            int i, tc_event_id;

            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         212 242 304
            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1
            //1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901 3456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
            //1       |10 5            |                |                |        |10023121779         |ALEXANDER                       |GLORIA                          |                                |0508    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289530  |           |201403030400                                                                      |NNYNNNNYYNNYNNNYNNNYYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//1       |10 5            |                |                |        |10024544908         |ARMSTRONG                       |AARON                           |                                |0535    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289531  |           |201403030400                                                                      |NYNNYNNNNNNYNNNYNNNNYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//|0547    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289532  |           |201403030400                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//|0547    |A   |201403030700|                |201403030700    |18  |C|    |3040|1440|18289532  |           |201403030700                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN

            if (Program.g_is_test)
                tc_event_id = 9999;
            else
                tc_event_id = PFSUtility.NextGID();                         //get a unique id for this class
            str_pull_dt = _pat.pull_finish.ToString(DATETIME_FORMAT);

            outstr = _pat.facilty_code.FixedWidth(8);                       //(facility code)
            outstr += "|" + _pat.unit_name.FixedWidth(16);
            outstr += "|" + "".FixedWidth(16);                               //(unit code)
            outstr += "|" + "".FixedWidth(16);                               //(area code)
            outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
            outstr += "|" + _pat.acct.FixedWidth(20);
            outstr += "|" + _pat.last_name.FixedWidth(32);
            outstr += "|" + _pat.first_name.FixedWidth(32);
            outstr += "|" + _pat.middle_name.FixedWidth(32);
            outstr += "|" + _pat.room.FixedWidth(8);
            outstr += "|" + _pat.bed.FixedWidth(4);
          outstr += "|" + str_pull_dt.FixedWidth(12);                      //class datetime (could change)
            outstr += "|" + "".FixedWidth(16);                               //(login)
          outstr += "|" + str_pull_dt.FixedWidth(16);                      //(employee)/(pull datetime)
            outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
            outstr += "|" + "C".FixedWidth(1);                               //record type = class
            outstr += "|" + "".FixedWidth(4);                                //(stage)
            outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
            outstr += "|" + _pat.range.ToString().FixedWidth(4);             //TC pull range
            outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
            outstr += "|";
            outstr = outstr.FixedWidth(294);
          outstr += "|" + _pat.effective.ToString(DATETIME_FORMAT);        //IN
            outstr = outstr.FixedWidth(377);
            outstr += "|";
            
            ind_list = "";
            if (give_default_inds)
            {
                Program.VerboseAudit("Patient will receive default indicators " + _pat.default_inds_str);
                for (i = 1; (i <= MAX_INDS); i++)
                {
                    _inds[i].is_checked = false;
                }
                foreach (var ind in _pat.default_inds)
                {
                    if (ind <= _inds.GetUpperBound(0))
                    {
                        _inds[ind].is_checked = true;
                    }
                }
            }
            for (i = 1; (i <= MAX_INDS); i++)
            {
                if (_inds[i].is_checked)
                {
                    outstr += "Y";
                    ind_list += "," + i;
                }
                else
                {
                    outstr += "N";
                }
            } // next i
            ind_list = ind_list.Substring(1);                           //strip leading comma
  
            Program.outfile.WriteLine(outstr);                          //output to transparent.txt

            Program.Audit("");
            desc = "Classified: " + ind_list;
            if (Program.g_is_test) {
                Program.Audit(desc);
            } else {
                //Save the selected indicators plus both Program.Audits in the event log; link with tc_event_id
                PFSEventLog.AddTransparentMappingEventLogEntry(
                    desc, Program.gLogUnitID, Program.gLogEncounterID,
                    tc_event_id, Program.gLogMapperVersion,
                    Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
            }
        }

        private void OutputProcs()
        {
            int i;
            string outstr, proc_list, desc;
            int tc_event_id;

            foreach(var proc in _procs) {
                if (Program.g_is_test)
                    tc_event_id = 9999;
                else
                    tc_event_id = PFSUtility.NextGID();                         //get a unique id for this proc

                outstr = _pat.facilty_code.FixedWidth(8);
                outstr += "|" + _pat.unit_name;                                 //10
                outstr = outstr.FixedWidth(68);
                outstr += "|" + _pat.acct.FixedWidth(20);                       //90
                outstr += "|" + _pat.last_name.FixedWidth(32);
                outstr += "|" + _pat.first_name.FixedWidth(32);
                outstr += "|" + _pat.middle_name.FixedWidth(32);
                outstr = outstr.FixedWidth(202);
                outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //204 proc dt
                outstr = outstr.FixedWidth(254);
                outstr += "|P";                                                 //256 procedure type record
                outstr += "|" + "".FixedWidth(4);                               //(stage)
                outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);     //TC source ID
                outstr += "|" + _pat.range.ToString().FixedWidth(4);            //TC pull range
                outstr += "|" + tc_event_id.ToString().FixedWidth(10);          //TC event ID
                outstr += "|";
                outstr = outstr.FixedWidth(294);
                outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //296 procdt in
                outstr = outstr.FixedWidth(346);
                outstr += "|" + proc.finish.ToString(DATETIME_FORMAT);          //348 procdt out
                outstr = outstr.FixedWidth(377);
                outstr += "|";
                
                proc_list = "";
                for (i = 1; (i < MAX_PROCS); i++) {
                    if (proc.procedure_number == i) {
                        outstr += "Y";
                        proc_list += "," + i;
                    } else {
                        outstr += "N";
                    }
                } // next i
                proc_list = proc_list.Substring(1);                             //strip leading comma

                Program.outfile.WriteLine(outstr);                              //output to transparent.txt

                desc = "Procedures: " + proc_list;
                if (Program.g_is_test) {
                    Program.Audit(desc);
                } else {
                    //Save the selected procedures plus both Program.Audits in the event log; link with tc_event_id
                    //(this assumes the procedure Program.Audit is mixed in with the class Program.Audit)
                    PFSEventLog.AddTransparentMappingEventLogEntry(desc, Program.gLogUnitID, Program.gLogEncounterID,
                        tc_event_id, Program.gLogMapperVersion,
                        Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
                }
            } // next proc
        }

        private void OutputOutcomes()
        {
            string outstr;

            foreach (var oc in _outcomes)
            {
                outstr = "".FixedWidth(9) + _pat.unit_name.FixedWidth(16);       // unitname
                outstr = outstr.FixedWidth(60);
                outstr += oc.start.ToString(DATETIME_FORMAT);                 //event_datetime
                outstr = outstr.FixedWidth(73);
                outstr += _pat.acct.FixedWidth(20);                            //acct
                outstr = outstr.FixedWidth(94);
                outstr += oc.procedure_number;                                //outcome indicator
                Program.out2file.WriteLine(outstr);                          //output to outcomesindicator.txt
            }

        }

        private bool ThreeAMPMClassExists(out int ceid, out DateTime effdtin_of3ampm, out DateTime effdtout_of3ampm)
        {
            bool ret=false;
            ceid = 0;
            effdtin_of3ampm = DateTime.MinValue;
            effdtout_of3ampm = DateTime.MinValue;
            Program.VerboseAudit("_pat pull finish=" + _pat.pull_finish.ToString());

            var db = PFSUtility.NewPfsDataContext();
            var query = from ce in db.CLASSIFICATION_EVENTs
                        where (ce.ENCOUNTER_ID == _pat.encounter_id)
                        where (ce.METHODOLOGY_ID == PFSGlobal.METH_ID_APLUS_INPATIENT2)
                        where (ce.CLASSIFIED_BY_ID == -3)
                        where (ce.EFFECTIVE_DATETIME_IN < _pat.pull_finish)
                        orderby (ce.EFFECTIVE_DATETIME_IN) descending
                        select ce;
            if (query.Count() == 0) return false;

            int ct = 1;
            foreach (var ce in query) {
                if (ct == 1)
                {
                    Program.VerboseAudit("ce.EFFECTIVE_DATETIME_IN.Hour % 12=" + ce.EFFECTIVE_DATETIME_IN.Hour % 12);
                    if ((ce.EFFECTIVE_DATETIME_IN.Hour % 12 == 3) && (ce.EFFECTIVE_DATETIME_IN.Minute == 0))
                    {
                        ceid = ce.CLASSIFICATION_EVENT_ID;
                        effdtin_of3ampm = ce.EFFECTIVE_DATETIME_IN;  
                        effdtout_of3ampm = ce.EFFECTIVE_DATETIME_OUT; 
                        ret = true;
                    }
                    ct++;
                }
            }
            return ret;
        }

        private void CleanUpClassifications()
        {
            var db = PFSUtility.NewPfsDataContext();
            var query = from ce in db.CLASSIFICATION_EVENTs
                        where (ce.ENCOUNTER_ID == _pat.encounter_id)
                        where (ce.METHODOLOGY_ID == PFSGlobal.METH_ID_APLUS_INPATIENT2)
                        where (ce.EFFECTIVE_DATETIME_IN >= _pat.dctime)
                        orderby (ce.EFFECTIVE_DATETIME_IN) descending
                        select ce;
            if (query.Count() == 0) return;

            foreach (var item in query)
            {
                Program.VerboseAudit("Post-dc Delete EFFECTIVE_DATETIME_IN=" + item.EFFECTIVE_DATETIME_IN.ToString());
                DelClass(item.CLASSIFICATION_EVENT_ID);
            }

            var db2 = PFSUtility.NewPfsDataContext();
            var query2 = from ce in db2.CLASSIFICATION_EVENTs
                        where (ce.ENCOUNTER_ID == _pat.encounter_id)
                        where (ce.METHODOLOGY_ID == PFSGlobal.METH_ID_APLUS_INPATIENT2)
                        where (ce.EFFECTIVE_DATETIME_IN < _pat.dctime)
                        where (ce.EFFECTIVE_DATETIME_OUT > _pat.dctime)
                         orderby (ce.EFFECTIVE_DATETIME_IN) descending
                        select ce;
            var items2 = query2.ToList();
            foreach (var item2 in items2)
            {
                Program.VerboseAudit("...post-dc cleaning EFFECTIVE_DATETIME_IN/OUT=" + item2.EFFECTIVE_DATETIME_IN.ToString() + " / " + item2.EFFECTIVE_DATETIME_OUT.ToString());
                item2.DATETIME_OUT = _pat.dctime;
                item2.EFFECTIVE_DATETIME_OUT = _pat.dctime;
                item2.LOS_HOURS = (float)PFSUtility.DateDiffInMinutes(item2.EFFECTIVE_DATETIME_IN, item2.EFFECTIVE_DATETIME_OUT) / 60.0f;
                item2.METHODOLOGY_WORKLOAD = (float)(item2.ACUITY * item2.LOS_HOURS) / 24f;
                item2.TIMESTAMP = DateTime.Now;
            }
            db2.SubmitChanges();

        }

        private int GetMonitorCEID(int ceid)
        {
            int ret = 0;
 
            var db = PFSUtility.NewPfsDataContext();
            var query = from ce in db.MONITOR_CLASS_EVENTs
                        where (ce.CLASSIFICATION_EVENT_ID == ceid)
                        select ce;
            if (query.Count() == 0) return 0;

            foreach (var ce in query)
            {
                ret = ce.MONITOR_CLASS_EVENT_ID;
            }
            return ret;
        }

        private void DelClass(int ceid)
        {
            if (ceid == 0) return;

            int mceid = GetMonitorCEID(ceid);

            Program.VerboseAudit("dbA Deleting ceid=" + ceid);
            var dbA = PFSUtility.NewPfsDataContext();
            var queryA = from ins in dbA.INDICATOR_SNAPSHOTs
                         where (ins.CLASSIFICATION_EVENT_ID == ceid)
                         select ins;
            var itemsA = queryA.ToList();
            foreach (var item in itemsA)
                dbA.INDICATOR_SNAPSHOTs.DeleteOnSubmit(item);
            dbA.SubmitChanges();

            Program.VerboseAudit("dbB Deleting ceid=" + ceid);
            var dbB = PFSUtility.NewPfsDataContext();
            var queryB = from cs in dbB.CLASSIFICATION_SNAPSHOTs
                         where (cs.CLASSIFICATION_EVENT_ID == ceid)
                         select cs;
            var itemsB = queryB.ToList();
            foreach (var item in itemsB)
                dbB.CLASSIFICATION_SNAPSHOTs.DeleteOnSubmit(item);
            dbB.SubmitChanges();

            Program.VerboseAudit("dbC Deleting ceid=" + ceid + "  mceid="+mceid);
            var dbC = PFSUtility.NewPfsDataContext();
            var queryC = from mia in dbC.MONITOR_IND_ANSWERs
                         where (mia.MONITOR_CLASS_EVENT_ID == mceid)
                         select mia;
            if (queryC.Count() > 0)
            {
                Program.VerboseAudit("   mia rec ct=" + queryC.Count());
                var itemsC = queryC.ToList();
                foreach (var item in itemsC)
                    dbC.MONITOR_IND_ANSWERs.DeleteOnSubmit(item);
                dbC.SubmitChanges();
            }

            Program.VerboseAudit("dbD Deleting ceid=" + ceid + "  mceid=" + mceid);
            var dbD = PFSUtility.NewPfsDataContext();
            var queryD = from mce in dbD.MONITOR_CLASS_EVENTs
                         where (mce.MONITOR_CLASS_EVENT_ID == mceid)
                         select mce;
            if (queryD.Count() > 0)
            {
                Program.VerboseAudit("   mce rec ct=" + queryD.Count());
                var itemsD = queryD.ToList();
                foreach (var item in itemsD)
                    dbD.MONITOR_CLASS_EVENTs.DeleteOnSubmit(item);
                dbD.SubmitChanges();
            }

            Program.VerboseAudit("db Deleting ceid=" + ceid);
            var db = PFSUtility.NewPfsDataContext();
            var query = from ia in db.INDICATOR_ANSWERs
                        where (ia.CLASSIFICATION_EVENT_ID == ceid)
                        select ia;
            if (query.Count() > 0)
            {
                var items = query.ToList();
                foreach (var item in items)
                    db.INDICATOR_ANSWERs.DeleteOnSubmit(item);
                db.SubmitChanges();
            }

            Program.VerboseAudit("db2 Deleting ceid=" + ceid);
            var db2 = PFSUtility.NewPfsDataContext();
            var query2 = from r in db2.RPT_CLASS_BY_DAYs
                         where (r.CLASSIFICATION_EVENT_ID == ceid)
                         select r;
            if (query2.Count() > 0)
            {
                var items2 = query2.ToList();
                foreach (var item2 in items2)
                    db2.RPT_CLASS_BY_DAYs.DeleteOnSubmit(item2);
                db2.SubmitChanges();
            }

            Program.VerboseAudit("db3 Deleting ceid=" + ceid);
            var db3 = PFSUtility.NewPfsDataContext();
            var query3 = from ce in db3.CLASSIFICATION_EVENTs
                         where (ce.CLASSIFICATION_EVENT_ID == ceid)
                         select ce;
            if (query3.Count() > 0)
            {
                var items3 = query3.ToList();
                foreach (var item3 in items3)
                    db3.CLASSIFICATION_EVENTs.DeleteOnSubmit(item3);
                db3.SubmitChanges();
            }

        }

        private void DeleteClassification(int ceid, DateTime effdtin_of3ampm, DateTime effdtout_of3ampm)
        {
            DateTime newoutdt = DateTime.MaxValue;

            DelClass(ceid);

            //Now update the previous classification, if it exists
              //los_hours = effective out - effective in in seconds / 3600
              //METHODOLOGY_workload = m_Acuity * los_hours / 24#

            Program.VerboseAudit("Updating prevclass...");
            var db4 = PFSUtility.NewPfsDataContext();
            var query4 = from ce in db4.CLASSIFICATION_EVENTs
                         where (ce.ENCOUNTER_ID == _pat.encounter_id)
                         where (ce.EFFECTIVE_DATETIME_OUT == effdtin_of3ampm)
                         select ce;
            if (query4.Count() == 0) return;
            var items4 = query4.ToList();
            foreach (var item4 in items4)
            {
                Program.VerboseAudit("...EFFECTIVE_DATETIME_IN=" + item4.EFFECTIVE_DATETIME_IN.ToString());
                Program.VerboseAudit("...old DATETIME_OUT=" + item4.DATETIME_OUT.ToString());
                if (TimeIs7am(effdtout_of3ampm))
                {
                    item4.DATETIME_OUT = null;
                    Program.VerboseAudit("...new DATETIME_OUT=null");
                }
                else
                {
                    item4.DATETIME_OUT = effdtout_of3ampm; //when transfer btwn 3 and 7
                    Program.VerboseAudit("...new DATETIME_OUT=" + item4.DATETIME_OUT.ToString());
                }

                Program.VerboseAudit("...old EFFECTIVE_DATETIME_OUT=" + item4.EFFECTIVE_DATETIME_OUT.ToString());
                item4.EFFECTIVE_DATETIME_OUT = effdtout_of3ampm;
                Program.VerboseAudit("...new EFFECTIVE_DATETIME_OUT=" + item4.EFFECTIVE_DATETIME_OUT.ToString());

                Program.VerboseAudit("...old LOS_HOURS=" + item4.LOS_HOURS.ToString());
                Program.VerboseAudit("...LOS_HOURS=" + PFSUtility.DateDiffInMinutes(item4.EFFECTIVE_DATETIME_IN,item4.EFFECTIVE_DATETIME_OUT) / 60.0f);
                item4.LOS_HOURS = (float)PFSUtility.DateDiffInMinutes(item4.EFFECTIVE_DATETIME_IN, item4.EFFECTIVE_DATETIME_OUT) / 60.0f;
                Program.VerboseAudit("...new LOS_HOURS=" + item4.LOS_HOURS.ToString());

                Program.VerboseAudit("...old METH_WKLD=" + item4.METHODOLOGY_WORKLOAD.ToString());
                item4.METHODOLOGY_WORKLOAD = (float)(item4.ACUITY * item4.LOS_HOURS) / 24f;
                Program.VerboseAudit("...new METH_WKLD=" + item4.METHODOLOGY_WORKLOAD.ToString());
                item4.TIMESTAMP = DateTime.Now;
            }
            db4.SubmitChanges();

        }

        private void DeleteActivity(int peid)
        {
//            delete PROCEDURE_ANSWER where PROCEDURE_EVENT_ID=7211351
//delete RPT_PROC_BY_DAY where PROCEDURE_EVENT_ID=7211351
//delete PROCEDURE_EVENT where PROCEDURE_EVENT_ID=7211351
            if (peid == 0) return;

            Program.VerboseAudit("dbP1 Deleting peid=" + peid);
            var db = PFSUtility.NewPfsDataContext();
            var query = from ia in db.PROCEDURE_ANSWERs
                        where (ia.PROCEDURE_EVENT_ID == peid)
                        select ia;
            if (query.Count() > 0)
            {
                var items = query.ToList();
                foreach (var item in items)
                    db.PROCEDURE_ANSWERs.DeleteOnSubmit(item);
                db.SubmitChanges();
            }

            Program.VerboseAudit("dbP2 Deleting peid=" + peid);
            var db2 = PFSUtility.NewPfsDataContext();
            var query2 = from r in db2.RPT_PROC_BY_DAYs
                         where (r.PROCEDURE_EVENT_ID == peid)
                         select r;
            if (query2.Count() > 0)
            {
                var items2 = query2.ToList();
                foreach (var item2 in items2)
                    db2.RPT_PROC_BY_DAYs.DeleteOnSubmit(item2);
                db2.SubmitChanges();
            }

            Program.VerboseAudit("dbP3 Deleting peid=" + peid);
            var db3 = PFSUtility.NewPfsDataContext();
            var query3 = from ce in db3.PROCEDURE_EVENTs
                         where (ce.PROCEDURE_EVENT_ID == peid)
                         select ce;
            if (query3.Count() > 0)
            {
                var items3 = query3.ToList();
                foreach (var item3 in items3)
                    db3.PROCEDURE_EVENTs.DeleteOnSubmit(item3);
                db3.SubmitChanges();
            }
        }

        bool TimeIs7am(DateTime t)
        {
            string s = PFSUtility.DateTimeToISOTime(t);
            return (s == "070000");
        }

        private bool DefaultInPast4hrs()
        {
            bool def = false;

            var db = PFSUtility.NewPfsDataContext();

            var query = from ce in db.CLASSIFICATION_EVENTs
                        where (ce.ENCOUNTER_ID == _pat.encounter_id)
                        where (ce.EFFECTIVE_DATETIME_IN >= _pat.pull_finish.AddMinutes(-(4 * 60 + Program._offset_mins)))
                        orderby (ce.EFFECTIVE_DATETIME_IN) 
                        select ce;

            int ct = query.Count();
            Program.VerboseAudit("ce count=" + ct);
            if (ct == 0) return def;
            bool first = true;
            foreach (var byid in query)
            {
                if (first)
                {
                    first = false;
                    def = (byid.CLASSIFIED_BY_ID == -2); // this is a default
                    Program.VerboseAudit("ceid=" + byid.CLASSIFICATION_EVENT_ID + "  byid=" + byid.CLASSIFIED_BY_ID + "  exists="+def.ToString());
                }
            }
            return def;
        }

        private int DeterminePtypeOfIndicators()
        {
            int i;
            var pscore = 0.0;
            int pt_type = 6;
            var indlist = new List<int>();
            string indstr = "";

            Program.VerboseAudit("Determine patient type of triggered indicators");

            for (i = 1; (i <= MAX_INDS); i++)
            {
                if (_inds[i].is_checked)
                {
                    indlist.Add(i);
                    indstr += i.ToString() + ",";
                }
            }

            var db = PFSUtility.NewPfsDataContext();
            var query_ind_def = from ind_def in db.INDICATOR_DEFINITIONs
                                where (ind_def.METHODOLOGY_ID == PFSGlobal.METH_ID_APLUS_INPATIENT2) &&
                                  indlist.Contains(ind_def.INDICATOR_NUMBER)
                                select new
                                {
                                    ind_def.WEIGHT
                                };
            foreach (var wgts in query_ind_def)
            {
                pscore += wgts.WEIGHT;
            }
            Program.VerboseAudit("indicators=" + indstr);
            Program.VerboseAudit("score=" + pscore.ToString());

            var query_ptype = from ptype in db.PATIENT_TYPEs
                              where (ptype.METHODOLOGY_ID == PFSGlobal.METH_ID_APLUS_INPATIENT2)
                              orderby ptype.PATIENT_TYPE1
                              select new
                              {
                                  ptype.PATIENT_TYPE1,
                                  ptype.POINTS_HIVAL
                              };

            foreach (var ptypes in query_ptype)
            {
                //                Program.DebugTrace("type,hival=" + ptypes.PATIENT_TYPE1.ToString() + "," + ptypes.POINTS_HIVAL.ToString(), "");
                if (pscore <= ptypes.POINTS_HIVAL)
                {
                    if (pt_type > ptypes.PATIENT_TYPE1)
                    {
                        pt_type = ptypes.PATIENT_TYPE1;
                    }
                }
            }
            Program.VerboseAudit("patient type=" + pt_type.ToString());

            return pt_type;
        }



    }
}
