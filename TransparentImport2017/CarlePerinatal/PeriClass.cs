﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;                     // for LINQ; add a reference
using System.Data.SqlClient;                // for SqlConnection
using System.IO;                            // for Directory, Path, Stream
using PfsShared;                            // add a reference to Shared2 project

// ============================================================================
// PERINATAL transparent mapping -- GOES HERE --
// Carle      taken from Mayo Roch
// ============================================================================
// This processes one patient.  Only one database query is used.  :)
//
// All search functions use exact match for category, description and field name.
// Codes and results can be a single word or a list of words.
// Codes default to exact match; prefix with CODE_CODE_LIKE_PREFIX if desired (single word only)
// Results default to like matching; prefix with RESULT_EXACT_MATCH_PREFIX if desired (single word only)
//
// All searches are case insensitive.
//
//
namespace TransparentMapping
{
    class PeriClass
    {
        private const int MAX_INDS = 50;
        private const int MAX_STAGES = 10;
        private const int MAX_PROCS = 50;
        private const int MAX_BUCKETS = 2880;

        private const string CODE_LIKE_PREFIX = "%!";       // use to make codes %like%
        private const string EXACT_MATCH_PREFIX = "&!";     // use to make desc/result exact match
        private const string CHAR_COMMA = "||";             // use to insert literal comma in word list
        private const string NOT_PREFIX = "!!";             // use to insert literal comma in word list
        private const string EXACT_SQL_PREFIX = "!SQL!";     // use to give sql constraint as given
        private const string AVOID_NEGATIVE = "!;";
        private const string STARTS_WITH = "[!";     // use to give sql constraint as given

        private struct indicator_data {
            public bool is_checked;
            public int radio_group;
            public int originating_unit;
        }

        private struct staging_data
        {
            public Stages stage;
            public DateTime start;
            public DateTime finish;
            public bool NBN_was_split; //for indicators 10 and 17 in NBN stage after non-peri unit xfer.
            public int unitid;
            public string uname;
            public bool remove;
            public bool has_addl_locs;
            public indicator_data[] _sinds;
        }

        private struct proc_data {
            public int procedure_number;
            public DateTime start;
            public DateTime finish;
            public DateTime recoverydt;
            public int stage;
            public string uname;
            public bool is_surg;
        }

        // These are database CHART_ITEMs for this patient
        private CHART_ITEM[] _chart_items_since_admission;
        private CHART_ITEM[] _chart_items_since_unit_arrival;
        private CHART_ITEM[] _chart_items_during_pull_period;
        private CHART_ITEM[] _chart_items_pull_period_plus;
        // These are database PROCEDURE_EVENTs for this patient
        private PROCEDURE_EVENT[] _procedure_events;

        private indicator_data[] _inds;
        private proc_data xproc;
        private List<proc_data> _procs;
        private List<proc_data> _outcomes;
        private staging_data[] _stages;
        private staging_data[] _stages_addl_loc;
        private staging_data[] _stages_prestage_loc;
        private int num_addl_loc = 0;
        private int num_prestage_loc = 0;

        private const int DEFAULT_BUCKET_SIZE = 20;       //min

        private PatientInfo _pat;
        private int _bucket_size = DEFAULT_BUCKET_SIZE;
        private bool is_default;
        private bool isEDonly = false;
        private int stageidx = -1;
        private bool end_stage_analysis = false;
        private bool output_found_what = true;
        private int ind_stage;
        private bool _use_stage_query = false;
        private Stages _currstage;
        private bool _cesareanbirth = false;
        private DateTime pplocstartdt = DateTime.MinValue;
        private DateTime pplocenddt = DateTime.MinValue;
        private DateTime pt_went_thru_recdt = DateTime.MinValue;
        private bool delivered = false;
        private bool delivcs = false;
        private DateTime delivcs_dt = DateTime.MinValue;
        private int delivcs_ct = 0;
        private DateTime fundaldt = DateTime.MinValue;// or pumping time 4/7/21
        private bool advance_to_NBN = false;
        private DateTime advance_to_NBN_time = DateTime.MinValue;
        private bool _epidural_found_in_EL = false;
        private bool stage_unit_set = false;
        private int _originating_unit;
        private bool shortOPLOS = false;
        private bool circ_done = false;
        private DateTime circ_time = DateTime.MinValue;

        private enum SearchDepth {
            SearchDefault,
            SearchPullRange,            //search within the current pull     -- default
            SearchSinceArrival,         //search since arrival to the unit
            SearchSinceAdmission,        //search everything since admission to the hospital
            SearchPullPlus
        }

        private enum CountMode {
            CountAll,
            CountFirst                  //stop after one is found
        }

        private enum GetValueMode {
            GetTotal,
            GetMax,
            GetLast
        }

        enum Frequencies {
            QNONE,
            //Q4H,
            //Q2H,
            Q1H,
            //Q30M,
            Q15M
        }

        enum Stages
        {
            OP,  //0
            ANT, //1
            EL,  //2
            AL,  //3
            DEL, //4
            REC, //5
            PP,  //6
            IC,  //7
            TC,  //8
            NBN  //9
        };
        //private const string STAGE01_NAME = "ANT";
        //private const string STAGE02_NAME = "EL";
        //private const string STAGE03_NAME = "AL";
        //private const string STAGE04_NAME = "TRAN";
        //private const string STAGE05_NAME = "DEL";
        //private const string STAGE06_NAME = "REC";
        //private const string STAGE07_NAME = "PP";
        //private const string STAGE08_NAME = "IC";
        //private const string STAGE09_NAME = "TC";
        //private const string STAGE10_NAME = "NBN";

        private struct fmapRow {
            public double los_high;                       //the LOS being testing
            public int[] freq;                           //the count required for each Q value
        }
        private List<fmapRow> _freq_map;                    //1,2,4,8,12,24 hours

        private struct gBucket
        {
            public int bucket;
            public string code;
            public DateTime evdt;
            public bool using_waiver;
            public bool has_all_deps; //has all dependents
            public int num_addl_items;
        }
        private struct gGap
        {
            public int gap;
            public DateTime evdt1;
            public DateTime evdt2;
        }
        private string[] meds_rate = { "newbag", "new bag", "rateverify", "rate verify",
            "restarted", "ratechange", "rate change", "started", "continued" , "stopped"};
        private string[] meds_rate_without_stopped = { "newbag", "new bag", "rateverify", "rate verify",
            "restarted", "ratechange", "rate change", "started", "continued"};

        //
        // This is the main entry point
        //
        public void ProcessPatient(PatientInfo pat)
        {
            _pat = pat;
            InitIndicators(); // sets is_default
            InitProcs();
            //isEDonly = OnlyHasED();
            if (!is_default)
            {
                if (_pat.classified_methid == 27) // change the bucket range
                {
                    _pat.pull_start = _pat.classtime;
                }

                LoadPatientChart();
                CheckStaging();  //if nonperi,perinatal then make latter peri NBN.
                //if (!stage_unit_set)
                //{
                //    Program.VerboseAudit("Abort processing of patient: No locations within stage scope.");
                //    return;
                //}
                for (int s = 0; s <= stageidx; s++)
                {
                    ind_stage = s;
                    if (!_stages[s].remove && _stages[s].stage != Stages.OP)
                    {
                        Program.VerboseAudit("==================================================================================================");
                        Program.VerboseAudit("  INDICATORS FOR STAGE: " + Enum.GetName(typeof(Stages), (int)_stages[s].stage) + " start=" + _stages[s].start + " end=" + _stages[s].finish);
                        Program.VerboseAudit("==================================================================================================");

                        //if (!(((Enum.GetName(typeof(Stages), (int)_stages[s].stage)) == "PP")
                        //   && (_stages[s].start.AddHours(24) >= Program.g_pull_finish_q4)))
                        // if (not pp and a day later)
                        //•	The frequency indicators should be within the LOS of each stage. (#6-10; 12-14)
                        //•	The following should apply across the laboring stages starting when 
                        //  first documented(EL, AL, DEL, REC), but separately for each ANTE, PP, TC and NBN
                        // (#1, 2, 3, 4, 11)
                        //•	The rest would apply when first documented for the rest of the stay or they only 
                        //apply to one stage. (Note could start with 15 and change to 
                        //16 at a later stage.)(5, 15 & 16, 17, 18, 19, 20)
                        {
                            Check_1_2(s);
                            Check_3(s);
                            Check_4(s);
                            Check_5(s);
                            Check_678910(s);
                            Check_11(s);
                            Check_121314(s);
                            Check_1516(s);
                            Check_17(s);
                            Check_18(s);
                            Check_19(s);
                            Check_20(s);
                            HighestIndicatorInEachGroupWins();
                        }
                    }
                } // for stages
                CheckProcedures();
            }


            //if (!is_default)
            //{
            //    CheckProcs();
            //    CheckOutcomes();
            //}

            if (Program.g_no_output) return;
            OutputStages();
            //OutputClass();
            OutputProcs();
            //OutputOutcomes();
        }


        private void InitIndicators()
        {

            //; perinatal classification by stage; no indicators required
            //| C | Ant |
            //| C | El | m
            //| C | Al | m
            //| C | Tran |
            //| C | Del | m
            //| C | Rec |
            //| C | PP | m
            //| C | PP | m
            //| C | IC | ba
            //| C | TC | b
            //| C | NBN |


            // Make a new (empty) indicator array
            _inds = new indicator_data[MAX_INDS + 1];           // This 1 based so add one

            _stages = new staging_data[MAX_STAGES + 1];
            _stages_addl_loc = new staging_data[MAX_STAGES + 1];
            _stages_prestage_loc = new staging_data[MAX_STAGES + 1];
            stageidx = -1;

            is_default = false;
            //if (_pat.los_hours <= 4.0) {
            //    is_default = true;
            //    Program.VerboseAudit("Patient was here 4 hrs or less. Will receive default indicators " + _pat.default_inds_str);
            //    foreach (var ind in _pat.default_inds) {
            //        if (ind <= _inds.GetUpperBound(0)) {
            //            _inds[ind].is_checked = true;
            //        }
            //    }
            //}

            // get indicator radio groups from the database
            // ** (This database access can be replaced once we have a C# methodolgy cache)
            var db = PFSDBUtility.NewPfsDataContext();
            var query = from idef in db.INDICATOR_DEFINITIONs
                        where (idef.METHODOLOGY_ID == _pat.meth_id)
                        select idef;
            for (int s = 0; s <= MAX_STAGES; s++)
            {
                _stages[s].uname = "";
                _stages[s]._sinds = new indicator_data[MAX_INDS + 1];  // This 1 based so add one
                _stages_addl_loc[s]._sinds = new indicator_data[MAX_INDS + 1];
                _stages[s].has_addl_locs = false;
                foreach (var idef in query)
                {
                    if (idef.INDICATOR_NUMBER <= _inds.GetUpperBound(0))
                    {
                        // (convert nulls to zero)
                        _stages[s]._sinds[idef.INDICATOR_NUMBER].radio_group = PFSDBUtility.DBToInt(idef.RADIO_GROUP);
                        _stages_addl_loc[s]._sinds[idef.INDICATOR_NUMBER].radio_group = PFSDBUtility.DBToInt(idef.RADIO_GROUP);
                    }
                }
            }
        }

        private void InitProcs()
        {
            _procs = new List<proc_data>();
            _outcomes = new List<proc_data>();
        }

        private int GetALAssessment()
        {
            int AL_assess_ind = 0;
            for (int s = 0; s <= stageidx; s++)
            {
                if (_stages[s].stage == Stages.AL)
                {
                    for (int i = 6; i <= 10; i++)
                    {
                        if (_stages[s]._sinds[i].is_checked)
                        {
                            AL_assess_ind = i;
                        }
                    }
                }
            } //end for
            return AL_assess_ind;
        }

        private int CheckELAssessment()
        {
            //for (int s = 0; s <= stageidx; s++)
            //if (_stages[s].stage == Stages.AL)

            //First get EL stage assessment.
            bool found_EL = false;
            int EL_assess_ind = 0;
            for (int s = 0; s <= stageidx; s++)
            {
                if (_stages[s].stage == Stages.EL)
                {
                    found_EL = true;
                    for (int i = 6; i <= 10; i++)
                    {
                        if (_stages[s]._sinds[i].is_checked)
                        {
                            EL_assess_ind = i;
                        }
                    }
                }
            } //end for
            if (!found_EL) //not found in current TC batch, therefore check in db
            {
                bool found_ELindb = false;
                var db = PFSDBUtility.NewPfsDataContext();
                var query = from ce in db.CLASSIFICATION_EVENTs
                            join ia in db.INDICATOR_ANSWERs on ce.CLASSIFICATION_EVENT_ID equals ia.CLASSIFICATION_EVENT_ID
                            where (ce.ENCOUNTER_ID == _pat.encounter_id 
                                   && ce.STAGE == 2
                                   && ia.INDICATOR_NUMBER >= 6
                                   && ia.INDICATOR_NUMBER <= 10)
                            select new { ceid = ce.CLASSIFICATION_EVENT_ID,
                                         assess_ind = ia.INDICATOR_NUMBER};
                found_ELindb = (query.Count() > 0);
                if (found_ELindb)
                {
                    Program.VerboseAudit("Found EL stage classification in db.");
                    foreach (var item in query)
                    {
                        EL_assess_ind = item.assess_ind; //should only be 1 record.
                        Program.VerboseAudit("Found EL assess indicator=" + EL_assess_ind);
                    }
                }

            } //if !found_EL
            return EL_assess_ind;
        }


        //private fmapRow LoadFreqTableRow(double los_high, string values)
        //{
        //    fmapRow fmrow;

        //    fmrow.los_high = los_high;
        //    fmrow.freq = new int[Enum.GetNames(typeof(Frequencies)).Length];

        //    var arr = values.Split(',');
        //    for (int i = 0; i <= fmrow.freq.GetUpperBound(0); i++)
        //    {
        //        fmrow.freq[i] = arr[i].ToInteger();
        //    }
        //    return fmrow;
        //}

        // These are the number of times you need to do something during a certain time period (LOS)
        // in order for it to be considered one of the frequencies listed.
        //private void LoadFreqTable()
        //{
        //    _freq_map = new List<fmapRow>();
        //    //                              LOS,  None Q4h Q2h Q1h Q15m
        //    //_freq_map.Add(LoadFreqTableRow(1, "    0,  0,  0,  1,  2"));
        //    //_freq_map.Add(LoadFreqTableRow(2, "    0,  0,  1,  1,  4"));
        //    //_freq_map.Add(LoadFreqTableRow(4, "    0,  1,  2,  2,  8"));
        //    //_freq_map.Add(LoadFreqTableRow(6, "    0,  1,  2,  3, 12"));
        //    //_freq_map.Add(LoadFreqTableRow(8, "    0,  1,  2,  4, 16"));
        //    //_freq_map.Add(LoadFreqTableRow(12, "   0,  2,  4,  6, 24"));
        //    //_freq_map.Add(LoadFreqTableRow(16, "   0,  3,  5,  8, 32"));
        //    //_freq_map.Add(LoadFreqTableRow(24, "   0,  4,  8, 12, 48"));
        //    //_freq_map.Add(LoadFreqTableRow(36, "   0,  4,  8, 18, 72"));
        //    //_freq_map.Add(LoadFreqTableRow(48, "   0,  4,  8, 24, 96"));
        //    //New freq table 2/5/14
        //    //q4	q2	q1	q30     q30
        //    //            Non-ICU	ICU & SD
        //    // 4	8	15	29	    36
        //    // 3	5	9	17	    24
        //    // 2	4	7	13	    19
        //    // 2	3	5	10	    13
        //    //                                         q1H q15min
        //    _freq_map.Add(LoadFreqTableRow(1, "   0,  1, 3"));
        //    _freq_map.Add(LoadFreqTableRow(3, "   0,  2, 6"));
        //    _freq_map.Add(LoadFreqTableRow(6, "   0,  3, 12"));
        //    _freq_map.Add(LoadFreqTableRow(12, "   0,  6, 24"));
        //    _freq_map.Add(LoadFreqTableRow(24, "   0,  12, 48"));
        //    _freq_map.Add(LoadFreqTableRow(48, "   0,  24, 96"));
        //    _freq_map.Add(LoadFreqTableRow(72, "   0,  36, 144"));
        //    _freq_map.Add(LoadFreqTableRow(96, "   0,  48, 192"));
        //    _freq_map.Add(LoadFreqTableRow(192, "  0,  96, 384"));
        //    _freq_map.Add(LoadFreqTableRow(384, "  0,  192, 768"));
        //    _freq_map.Add(LoadFreqTableRow(768, "  0,  384, 1536"));

        //    //to read this table: if you have at least 6 asessmenets in a 12-hour period, you get Q1h
        //    //(LOS=12, column Q1h has a count of 6)
        //}

        //---------------
        //ED Visit 11. Physiologic Assessment q1hr
        //ED Visit 12. Physiologic Assessment q15min
        //---------------
        //FOUND: looking for code in '304987655,9990007096285,304987657,3045001025,3045001064,304987666,9990304100017,30454321,9990000006294,9993041120042,3045001041,9990304001499,3045001018,9993040103255'; found cat='' code='304987655' desc='temp' field='' result='36.5' (4 more results)
        //1 unique
        //Index was outside the bounds of the array.
        //Index was outside the bounds of the array.
        //   at TransparentMapping.EDVisit.FreqForCount(Double los_hours, Int32 count)
        //   at TransparentMapping.EDVisit.CheckAssessment(Int32 count, String desc)
        //   at TransparentMapping.EDVisit.CountAssessments(Int32 bucket_size, Int32 ind)
        //   at TransparentMapping.EDVisit.Check_1112()
        //   at TransparentMapping.EDVisit.ProcessPatient(PatientInfo pat)
        //   at TransparentMapping.Program.ProcessPatients()
        //Mapping complete


        //private Frequencies FreqForCount(double los_hours, int count)
        //{
        //    foreach (var fmrow in _freq_map) {
        //        if (los_hours <= fmrow.los_high) {
        //            // Option: pro-rate the count upward if the LOS is less than the row we are interested in.
        //            //         This will bump the count to what it might have been at the full LOS.
        //            // Note: truncate the result; rounding inflates the value too much.
        //            int prorated_count = (int)((fmrow.los_high / los_hours) * count);

        //            // foreach goes low to high; go from high to low instead
        //            for (int j = (int)Frequencies.Q15M; (j > (int)Frequencies.QNONE); j--) { //search right to left
        //                if (prorated_count >= fmrow.freq[j]) {
        //                    return (Frequencies)j;
        //                }
        //            } // next j
        //        }
        //    }

        //    return Frequencies.QNONE;
        //}

        //
        // Load patient chart from the database (one DB access)
        // Load everything since admission
        //
        private void LoadPatientChart()
        {
            Program.VerboseAudit("LoadPtChart range: " + _pat.pull_start + " -to= " + _pat.pull_finish);
            // Get the entire patient chart (all units and dates for this patient)
            var db = PFSDBUtility.NewPfsDataContext();
            var query = from item in db.CHART_ITEMs
                        where (item.ENCOUNTER_ID == _pat.encounter_id)
                        where (item.TIMESTAMP <= Program.g_pull_finish_q4)
                        select item;
            // Save the result
            _chart_items_since_admission = query.ToArray();

            // Convert all results to lower case for case insensitive comparisons
            foreach (var item in _chart_items_since_admission) {
                if (item.CATEGORY != null) item.CATEGORY = item.CATEGORY.ToLower();
                if (item.CODE != null) item.CODE = item.CODE.ToLower();
                if (item.DESCRIPTION != null) item.DESCRIPTION = item.DESCRIPTION.ToLower();
                if (item.FIELD_NAME != null) item.FIELD_NAME = item.FIELD_NAME.ToLower();
                if (item.RESULT != null) item.RESULT = item.RESULT.ToLower();
            }

            // Prepare two more versions of the chart
            var query2 = from item in _chart_items_since_admission
                         where (item.EVENT_DATETIME >= _pat.unit_arrival) && (item.EVENT_DATETIME <= _pat.pull_finish)
                         select item;
            _chart_items_since_unit_arrival = query2.ToArray();

            query2 = from item in _chart_items_since_admission
                     where (item.EVENT_DATETIME >= _pat.pull_start) && (item.EVENT_DATETIME <= _pat.pull_finish)
                     //where (item.EVENT_DATETIME <= _pat.unit_departure.AddMinutes(10))
                     select item;
            _chart_items_during_pull_period = query2.ToArray();
            query2 = from item in _chart_items_since_admission
                     //where (item.EVENT_DATETIME >= _pat.pull_start) && (item.EVENT_DATETIME <= _pat.pull_finish.AddHours(4))
                     where (item.EVENT_DATETIME >= Program.g_pull_finish_q4.AddHours(-24).AddMinutes(-PFSUtility.DateDiffInMinutes(Program.g_pull_finish_q4.Date.AddHours(7), Program.g_pull_finish_q4)) && item.EVENT_DATETIME <= Program.g_pull_finish_q4)
                     select item;
            _chart_items_pull_period_plus = query2.ToArray();
        }

        // Delay looking for patient procedures until they are wanted
        private void LoadPatientProceduresIfNeeded()
        {
            if (_procedure_events != null) return;

            var db = PFSDBUtility.NewPfsDataContext();
            var query = from proc in db.PROCEDURE_EVENTs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                        select proc;
            _procedure_events = query.ToArray();
        }



        // Started a new chart item query (of a certain depth)
        // Default is the chart during the pull range.  Options for unit arrival and admission to hospital.
        private IEnumerable<CHART_ITEM> StartNewQuery()
        {
            if (isEDonly)
                return StartNewQuery(SearchDepth.SearchSinceAdmission);
            else
                return StartNewQuery(SearchDepth.SearchDefault);
        }
        private IEnumerable<CHART_ITEM> StartNewQuery(SearchDepth search_depth)
        {
            int len_stage;
            if (_use_stage_query) return StartNewStageQuery(out len_stage);

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;

            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    return (from item in _chart_items_during_pull_period
                            where item.EVENT_DATETIME <= Program.g_pull_finish_q4
                            select item);
                case SearchDepth.SearchSinceArrival:
                    return (from item in _chart_items_since_unit_arrival
                            where item.EVENT_DATETIME <= Program.g_pull_finish_q4
                            select item);
                case SearchDepth.SearchSinceAdmission:
                    return (from item in _chart_items_since_admission
                            where item.EVENT_DATETIME <= Program.g_pull_finish_q4
                            select item);
                case SearchDepth.SearchPullPlus:
                    return (from item in _chart_items_pull_period_plus
                            select item);
            }
            return null;
        }

        private IEnumerable<CHART_ITEM> StartNewStageQuery(out int len_stage)
        {
            DateTime startdt = _pat.pull_start;
            DateTime findt = _pat.pull_finish;

            for (int i = 0; i <= stageidx; i++)
            {
                if (_stages[i].stage == _currstage)
                {
                    startdt = _stages[i].start;
                    findt = _stages[i].finish;
                    if (_stages[i + 1].stage > _currstage)
                    {
                        findt = _stages[i + 1].start;
                    }
                }
            }
            if (_currstage == Stages.TC) // then make start equal to IC stage start time if IC exists
            {
                for (int i = 0; i <= stageidx; i++)
                {
                    if (_stages[i].stage == Stages.IC)
                    {
                        startdt = _stages[i].start;
                        Program.VerboseAudit("SPECIAL CHANGE OF RANGE FOR TC:  Start time is now IC start time=" + startdt);
                    }
                }
            }

            len_stage = (int)(findt - startdt).TotalMinutes;
            Program.VerboseAudit("StageQuery look for items between: " + startdt + " and " + findt);
            return (from item in _chart_items_since_admission where
                    item.EVENT_DATETIME >= startdt && item.EVENT_DATETIME <= findt select item);
        }

        // Is this search term a word or a list of words?
        private bool ValueIsAList(string s)
        {
            return (s != null) && s.Contains(',');                //is this a comma-separated list?
        }

        // Break apart a wordlist and prepare its members
        private string[] SplitOnCommaAndPrepareElements(string s)
        {
            // If a word list has a space after each comma, we will get leading blanks in each term
            var arr = s.Split(',');
            for (int i = 0; i <= arr.GetUpperBound(0); i++) {
                arr[i] = arr[i].Trim();                         // get rid of leading blanks
                arr[i] = arr[i].Replace(CHAR_COMMA, ",");       // insert a literal comma into search word
                arr[i] = arr[i].ToLower();                      // convert for case-insensitive comparisons
            }
            return arr;
        }

        // Add a basic chart item filter to a LINQ query
        //
        // cat = exact match
        // code_list = exact match; optional "like" match
        // desc_list = "like" match; optional exact match
        // field = exact match
        // result_list = "like" match; optional exact match
        //
        // These are comma-separated lists.  
        // Search for a literal comma by using CHAR_COMMA in the string.
        private IEnumerable<CHART_ITEM> AndItemFilter(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list)
        {
            // Converted all values to lower case for case-insensitive comparisons.
            if (!String.IsNullOrEmpty(cat)) {
                cat = cat.ToLower();
                query = query.Where(e => e.CATEGORY == cat);
            }

            if (!String.IsNullOrEmpty(code_list)) {
                code_list = code_list.ToLower();
                if (code_list.Left(2) == EXACT_MATCH_PREFIX) {          // exact match override?
                    query = query.Where(e => e.CODE == code_list.Substring(2));
                } else if (code_list.Left(2) == CODE_LIKE_PREFIX)
                {            // override for "like" match?
                    query = query.Where(e => e.CODE.Contains(code_list.Substring(2)));
                } else if (ValueIsAList(code_list)) {
                    query = AndCodeInList(query, code_list);            // find one of the words
                } else {
                    query = query.Where(e => e.CODE.Contains(code_list));      // find this word
                }
            }

            if (!String.IsNullOrEmpty(desc_list)) {
                bool special1 = false; // (desc_list == ";ROUTE=IV");
                desc_list = desc_list.ToLower();
                if (desc_list.Left(2) == EXACT_MATCH_PREFIX)
                {          // exact match override?
                    query = query.Where(e => e.DESCRIPTION == desc_list.Substring(2));
                }
                else if (desc_list.Left(2) == NOT_PREFIX)
                {
                    // query = query.Where(e => !e.DESCRIPTION.Contains(desc_list.Substring(2)));
                    query = AndDescriptionNOTInList(query, desc_list.Substring(2));
                }
                else if (ValueIsAList(desc_list))
                {
                    query = AndDescriptionInList(query, desc_list);
                }
                else
                {
                    desc_list = desc_list.Replace(CHAR_COMMA, ",");     // look for literal comma requests
                    query = query.Where(e => e.DESCRIPTION.Contains(desc_list));
                    if (special1)
                    {
                        query = query.Where(e => !e.DESCRIPTION.Contains(";ROUTE=IV SLOW PUSH"));
                        query = query.Where(e => !e.DESCRIPTION.Contains(";ROUTE=IV PUSH"));
                    }

                }
            }

            if (!String.IsNullOrEmpty(field)) {
                field = field.ToLower();
                query = query.Where(e => e.FIELD_NAME == field);
            }

            if (!String.IsNullOrEmpty(result_list)) {
                result_list = result_list.ToLower();                        // make all results case insensitve
                if (result_list.Left(2) == EXACT_MATCH_PREFIX) {     // exact match override?
                    query = query.Where(e => e.RESULT == result_list.Substring(2));
                }
                else if (result_list.Left(2) == AVOID_NEGATIVE)
                {
                    query = query.Where(e => (!e.RESULT.Contains("no " + result_list.Substring(2))) && ((e.RESULT == result_list.Substring(2)) || (e.RESULT.Contains(";" + result_list.Substring(2)))));
                }
                else if (ValueIsAList(result_list)) {
                    query = AndResultInList(query, result_list);
                }
                else if (result_list.Left(2) == STARTS_WITH)
                {
                    query = query.Where(e => e.RESULT.StartsWith(result_list.Substring(2)));
                }
                else
                {
                    result_list = result_list.Replace(CHAR_COMMA, ",");     // look for literal comma requests
                    query = query.Where(e => e.RESULT.Contains(result_list));
                }
            }
            return query;
        }

        // Look for a code that is one of the words in the list.
        private IEnumerable<CHART_ITEM> AndCodeInList(IEnumerable<CHART_ITEM> query, string code_list)
        {
            if (String.IsNullOrEmpty(code_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(code_list);
            return query.Where(e => e.CODE.ContainsAny(arr));  // use like match. Exact match = arr.Contains(e.CODE));   
        }

        // Look for a description that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndDescriptionInList(IEnumerable<CHART_ITEM> query, string desc_list)
        {
            if (String.IsNullOrEmpty(desc_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(desc_list);
            return query.Where(e => e.DESCRIPTION.ContainsAny(arr));    // "like" match
        }

        private IEnumerable<CHART_ITEM> AndDescriptionNOTInList(IEnumerable<CHART_ITEM> query, string desc_list)
        {
            if (String.IsNullOrEmpty(desc_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(desc_list);
            //return query.Where(e => arr.Any(x => x.Contains(e.DESCRIPTION))); // "like" match
            return query.Where(e => !e.DESCRIPTION.ContainsAny(arr)); // "like" match
        }


        // Look for a result that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultInList(IEnumerable<CHART_ITEM> query, string result_list)
        {
            bool use_start_with = false;

            if (String.IsNullOrEmpty(result_list)) return query;

            if (result_list.Left(2) == STARTS_WITH)
            {
                use_start_with = true;
                result_list = result_list.Substring(2);
            }

            var arr = SplitOnCommaAndPrepareElements(result_list);

            if (use_start_with)
                return query.Where(e => arr.Any(item => e.RESULT.StartsWith(item)));
            else
                return query.Where(e => e.RESULT.ContainsAny(arr)); // "like" match
        }

        // Look for a result that does not contain one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultNotInList(IEnumerable<CHART_ITEM> query, string result_list)
        {
            if (String.IsNullOrEmpty(result_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(result_list);
            return query.Where(e => !e.RESULT.ContainsAny(arr));        // "like" match
        }

        private string DescribeSearchDepth(SearchDepth search_depth)
        {
            string result = "";
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;

            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    //result = "in pull range"
                    result = "";                         //be quiet since this is default
                    break;
                case SearchDepth.SearchSinceArrival:
                    result = "since arrival to unit";
                    break;
                case SearchDepth.SearchSinceAdmission:
                    result = "since admission";
                    break;
                case SearchDepth.SearchPullPlus:
                    result = "in LOS range plus 4 hours after";
                    break;
            }

            return result;
        }

        // Describe a query that has already been run
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Make a simple query and describe the results
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            var arr = query.ToArray();
            return Describe(arr, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private string LookingFor(string so_far, string what, string op, string value)
        {
            string result;
            if (String.IsNullOrEmpty(value)) return so_far;

            if (so_far == "")
                result = "looking for " + what;
            else
                result = so_far + " and " + what;

            if (value.Left(2) == EXACT_MATCH_PREFIX)
                result += "='" + value.Substring(2) + "'";
            else if (value.Left(2) == CODE_LIKE_PREFIX)
                result += " contains " + "'" + value.Substring(2) + "'";
            else if (ValueIsAList(value))
                result += " in '" + value + "'";
            else
                result += op + "'" + value + "'";

            return result;
        }
        private string Describe(CHART_ITEM[] arr, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            string result = "";
            result = LookingFor(result, "cat", "=", cat);
            result = LookingFor(result, "code", "=", code_list);
            result = LookingFor(result, "desc", " contains ", desc_list);
            result = LookingFor(result, "field", "=", field);
            result = LookingFor(result, "result", " contains ", result_list);

            if (arr.Count() == 0) {
                result += "; not found " + DescribeSearchDepth(search_depth);
            } else {
                var e = arr[0];
                result = "FOUND: " + result + "; found";
                // We might have searched for a pattern or word list in several fields - show what was found
                if (e.CATEGORY != null) result += " cat='" + e.CATEGORY + "'";
                if (e.CODE != null) result += " code='" + e.CODE + "'";
                if (e.DESCRIPTION != null) result += " desc='" + e.DESCRIPTION + "'";
                if (e.FIELD_NAME != null) result += " field='" + e.FIELD_NAME + "'";
                if (e.RESULT != null) result += " result='" + e.RESULT + "'";
                // Are there more results?  Just say how many; we aren't going to list them.
                if (arr.Count() == 2) {
                    result += " (1 more result)";
                } else if (arr.Count() > 2) {
                    result += " (" + (arr.Count() - 1) + " more results)";
                }
            }

            return result;
        }


        //Set an indicator for this reason (low level)
        private void SetInd(int inum, string reason)
        {
            if (_stages[ind_stage]._sinds[inum].is_checked) {
                Program.VerboseAudit("Set Ind #" + inum + ": " + reason);          //already set - repeat for verbose only
            }
            else {
                _stages[ind_stage]._sinds[inum].is_checked = true;
                Program.Audit("Set Ind #" + inum + ": " + reason);
            }
            _stages[ind_stage]._sinds[inum].originating_unit = _originating_unit;
        }

        //Clear an indicator for this reason (low level)
        private void ClrInd(int inum, string reason)
        {
            if (_stages[ind_stage]._sinds[inum].is_checked) {
                Program.VerboseAudit("Clr Ind #" + inum + ": " + reason);          //already clear - repeat verbose only
            } else {
                _stages[ind_stage]._sinds[inum].is_checked = false;
                Program.Audit("Clr Ind #" + inum + ": " + reason);
            }
        }

        // Count how many items meet the conditions
        // All items are listed
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountItems(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            int count = query.Count();

            // always return what was found
            found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
            // echo the result?
            if (trace && !output_found_what) Program.VerboseAudit(found_what);

            return count;
        }

        // Count how many items meet the conditions
        // This has the option to list every entry or give a total
        private int CountResultInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what = "";

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);

            var arr = SplitOnCommaAndPrepareElements(result_list);

            foreach (var item in query) {
                // Figure out which of the search words were found
                var s = item.RESULT.ContainsWhich(arr);
                found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' code='" + item.CODE + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "'";
                if (trace) Program.VerboseAudit(found_what);
                count++;
                _originating_unit = item.UNIT_ID;
                if (count_mode != CountMode.CountAll) break;        //stop counting
            }

            if (count > 0) {
                //We already printed what was found; maybe add how many?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace && !output_found_what) Program.VerboseAudit(found_what);
            }

            return count;
        }

        // Count how many items meet the conditions
        // This has the option to list every entry or give a total
        private int CountResultInListEXCEPTList(string cat, string code_list, string desc_list, string field, string result_list, string negresult_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what = "";

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            query = AndResultNotInList(query, negresult_list);

            var arr = SplitOnCommaAndPrepareElements(result_list);

            foreach (var item in query)
            {
                // Figure out which of the search words were found
                var s = item.RESULT.ContainsWhich(arr);
                found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' code='" + item.CODE + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "'";
                if (trace) Program.VerboseAudit(found_what);
                count++;
                if (count_mode != CountMode.CountAll) break;        //stop counting
            }

            if (count > 0)
            {
                //We already printed what was found; maybe add how many?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            }
            else
            {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        // Count how many items contain ALL results in list
        private int CountResultInListAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what = "";

            var arr = SplitOnCommaAndPrepareElements(result_list);

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            Program.VerboseAudit("result list arr ub=" + arr.GetUpperBound(0));
            for (int i = 0; i <= arr.GetUpperBound(0); i++)
            {
                Program.VerboseAudit("result list[" + i + "]=" + arr[i]);
                if (arr[i].StartsWith(NOT_PREFIX))
                {
                    query = AndResultNotInList(query, arr[i]);
                    //query = query.Where(e => !e.RESULT.Contains(arr[i].Substring(2)));
                }
                else
                {
                    query = AndResultInList(query, arr[i]);
                    //query = query.Where(e => e.RESULT.Contains(arr[i]));
                }
            }

            count = query.Count();

            if (count > 0)
            {
                found_what = "found item with all results in '" + result_list + "' without those prefixed by !!";
                if (trace) Program.VerboseAudit(found_what);
            }
            else
            {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            if (ValueIsAList(result_list))
            {
                return CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what);
            } else {
                return CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what);
            }
        }

        // Is there an item with this result?
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContains(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            if (ValueIsAList(result_list))
            {
                return (CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            } else {
                return (CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
            }
        }

        // Result contains all in list
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            if (ValueIsAList(result_list))
            {
                return (CountResultInListAll(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            }
            else
            {
                found_what = "Not valid for non-list results.";
                return false;
            }
        }
        // Count how many items meet the conditions, with none of the results in result_list.
        // This has the option to say how many were found rather than list all.
        private int CountResultNotInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0, rec_count = 0;
            bool found_one;

            found_what = "";
            var arr = SplitOnCommaAndPrepareElements(result_list);         // also deals with CHAR_COMMA

            //Do not filter by result here; we need all results
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            foreach (var item in query) {
                rec_count++;
                //Look for each search word in the result
                found_one = false;

                foreach (string s in arr) {
                    if (String.Equals(item.RESULT, s)) {
                        found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "' -- ignore this";
                        Program.VerboseAudit(found_what);
                        found_one = true;
                        break;
                    }
                }

                if (!found_one) {
                    //none of the words were found - good!
                    found_what = Describe(cat, code_list, desc_list, field, "", search_depth) + " -- does not contain '" + result_list + "'";
                    Program.VerboseAudit(found_what);
                    count++;
                    if (count_mode != CountMode.CountAll) break;
                }
            }

            if (count > 0) {
                //We already printed what was found; maybe show the total?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else if (rec_count > 0) {
                //We already printed what we ignored
            } else {
                //Print what did not exist
                found_what = Describe(cat, code_list, desc_list, field, "", search_depth);      //not found
                Program.VerboseAudit(found_what);
            }

            return count;
        }

        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, string found_what)
        {
            // Search in the pull range with trace on
            return CountResultDoesNotContain(cat, code_list, desc_list, field, result_list, out found_what, SearchDepth.SearchDefault, true);
        }
        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, out string found_what, SearchDepth search_depth, bool trace)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            return CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0;
        }

        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            return (CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0);
        }

        // Set the indicator if the conditions are met
        //
        private bool SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);

        }
        private bool SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            bool b = false;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return true;

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what)) {
                SetInd(inum, found_what);                          //echo here - found
                b = true;
            } else {
                Program.VerboseAudit(found_what);                  //and here - not found
            }
            return b;
        }

        private void SetIndIfResultBetween(int inum, string cat, string code_list, string desc_list, string field, double loval, double hival)
        {
            SetIndIfResultBetween(inum, cat, code_list, desc_list, field, loval, hival, SearchDepth.SearchDefault);
        }

        private void SetIndIfResultBetween(int inum, string cat, string code_list, string desc_list, string field, double loval, double hival, SearchDepth search_depth)
        {
            //if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            int count = 0;
            string found_what = "";

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");
            foreach (var item in query)
            {
                //                Program.VerboseAudit("ResBetween: code=" + item.CODE + " result=" + item.RESULT);
                if (item.RESULT.IsNumeric())
                {
                    //Program.VerboseAudit("  result is numeric");
                    if (item.RESULT.Val() >= loval && item.RESULT.Val() <= hival)
                    {
                        count++;
                        found_what = "found code=" + item.CODE + " with result=" + item.RESULT + ": " + item.EVENT_DATETIME;
                        Program.VerboseAudit(found_what);
                    }
                }
            }

            if (count > 0)
            {
                SetInd(inum, found_what);
            }
        }

        // Special contains all
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContainsAll(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return;

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                          //echo here - found
            }
            else
            {
                Program.VerboseAudit(found_what);                  //and here - not found
            }
        }
        // Set the indicator if the result does not contain any of the words in result_list
        //
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultDoesNotContain(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return;

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            //Turn trace off for ResultDoesNotContain() and echo what was set below with SetInd
            if (ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                           //echo here - not found
            } else {
                Program.VerboseAudit(found_what);                   //and here - found
            }
        }

        // Clear the indicator if the result contains one of the words in the result_list
        //
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already clear
            if (!_stages[ind_stage]._sinds[inum].is_checked) return;

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            //Turn trace off for ResultContains() and echo what was set below with SetInd
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what)) {
                ClrInd(inum, found_what);                           //echo here - found
            } else {
                Program.VerboseAudit(found_what);                   //and here - not found
            }
        }

        private bool SetIndIfResultStartsWith(int inum, string cat, string code_list, string desc_list, string field, bool starts_with, string start_char, SearchDepth search_depth)
        {
            int count = 0;
            string found_what = "";
            bool b = false;

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");
            if (starts_with)
                query = query.Where(e => e.RESULT.StartsWith(start_char.ToLower()));
            else
                query = query.Where(e => !e.RESULT.StartsWith(start_char.ToLower()));
            foreach (var item in query)
            {
                count++;
                found_what = "found code=" + item.CODE + " with result=" + item.RESULT;
                b = true;
            }

            if (count > 0)
                SetInd(inum, found_what);
            return b;
        }


        //These slightly smaller functions are meant for places where you aren't really looking for a chart result
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list)
        {
            bool ret;
            string found_what;
            // Search in the pull range with trace on
            ret = Exists(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
            if (ret && output_found_what)
                Program.VerboseAudit(found_what);
            return ret;
        }
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            return (CountResultContains(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
        }

        // Variations with optional parameters (can switch to optional parameters in VS 2010)
        //
        private void SetIndIfFound(int inum, string cat)
        {
            SetIndIfResultContains(inum, cat, "", "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list)
        {
            SetIndIfResultContains(inum, cat, code_list, "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private void ClrIndIfFound(int inum, string cat)
        {
            ClrIndIfResultContains(inum, cat, "", "", "", "", SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }


        // Get the max/total value from a result (usually in the middle of the text)
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetIntValue(get_mode, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            int value;
            int result = 0;
            bool found_one = false;

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);

            //Look for a number in the result

            foreach (var item in query) {
                // RESULT might be null so convert to empty string so Split won't blow up
                var arr = PFSDBUtility.DBToString(item.RESULT).Split(' ');
                foreach (string s in arr) {
                    //Look for 1st character numeric; IsNumeric("60min") will fail so look at 1st char only
                    if (s.Left(1).IsNumeric()) {
                        value = (int)s.Val();                           //Use Val; ToInteger will error on "60min"
                        switch (get_mode) {
                            case GetValueMode.GetMax:
                                result = Math.Max(value, result);       //max
                                break;
                            case GetValueMode.GetTotal:
                                result += value;                        //total
                                break;
                            case GetValueMode.GetLast:
                                result = value;                         //last
                                break;
                        }

                        // print what we are searching for (the first time)
                        if (!found_one) {
                            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
                        }
                        found_one = true;
                        // print each value found
                        Program.VerboseAudit("  found numeric value " + result);
                        //Keep going in case there are more
                    }
                }
            }

            if (!found_one) {
                //show what was not found
                Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
            }

            return result;
        }

        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetMaxValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            return GetIntValue(GetValueMode.GetMax, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetTotalValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            return GetIntValue(GetValueMode.GetTotal, cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Get a result; returns true if found with return_result
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result)
        {
            return GetResult(cat, code_list, desc_list, field, out return_result, SearchDepth.SearchDefault);
        }
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0) {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
            } else {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }

        private void GetResultforOther(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "Other");
            return_result = "";
            foreach (var item in query)
            {
                if (!String.Equals(item.RESULT, "Other"))
                {
                    return_result = item.RESULT;
                    //Program.VerboseAudit(found_what);
                    break;
                }

            }
            return;
        }

        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, out string return_result, out DateTime return_evdt)
        {
            return GetResultAndEVDT(cat, code_list, desc_list, field, out return_result, out return_evdt, SearchDepth.SearchDefault);
        }
        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, out string return_result, out DateTime return_evdt, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
                return_evdt = PFSDBUtility.DBToDateTime(query.First().EVENT_DATETIME);
            }
            else
            {
                return_result = "";
                return_evdt = DateTime.MinValue;
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }

        private bool GetEVDTWhereResultContains(string cat, string code_list, string desc_list, string field, string res_list, out DateTime return_evdt)
        {
            return GetEVDTWhereResultContains(cat, code_list, desc_list, field, res_list, out return_evdt, SearchDepth.SearchDefault);
        }
        private bool GetEVDTWhereResultContains(string cat, string code_list, string desc_list, string field, string res_list, out DateTime return_evdt, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, res_list);

            if (query.Count() > 0)
            {
                return_evdt = PFSDBUtility.DBToDateTime(query.First().EVENT_DATETIME);
            }
            else
            {
                return_evdt = DateTime.MinValue;
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (return_evdt > DateTime.MinValue);
        }



        //======================================================================================
        // Mapping starts here
        //======================================================================================

        private void CheckStaging()
        {
            bool pt_went_thru_rec = false;
            bool pp_shortcut = false;
            bool ante_shortcut = false;
            bool nbn_shortcut = false;
            bool send_back_to_ANTE = false;
            
            if (_pat.age > 2.0)
            {
                pt_went_thru_recdt = PatientWentThruRecovery();//sets fundaldt
                pt_went_thru_rec = (pt_went_thru_recdt > DateTime.MinValue);
                //if (pt_went_thru_rec)
                //    if (DELAfterREC(pt_went_thru_recdt)) pt_went_thru_rec = false;

                ShowLocations();

                if (_pat.classified_methid != 27) // this is the first time being classified
                {
                    if (_pat.procev1_dt > DateTime.MinValue) //activity 1 already exists:bail.
                    {
                        Program.VerboseAudit("OP procedure 1 exists at:" + _pat.procev1_dt + "...ending patient analysis.");
                        return;
                    }
                    CheckStage_OP();
                    if (end_stage_analysis)
                    {
                        AssignStagesToLocations();
                        return;
                    }

                    CheckStage_ANTE(false);
                    CheckStage_EL();
                    CheckStage_AL();
                    CheckStage_DEL();
                    //wait one phase
                    CheckStage_REC();
                    CheckStage_PP(false);
                }
                else if (pt_went_thru_rec && (_pat.stage >= 1 && _pat.stage <= 3))
                {
                    if (RECIsInClassHistory())
                    {
                        Program.VerboseAudit("RECOVERY is in class history indicates that this was fetal surgery.");
                        send_back_to_ANTE = true;
                        stageidx = -1;
                        CheckStage_ANTE(true);
                    }
                    if (!send_back_to_ANTE)
                    {
                        pp_shortcut = false;
                        if (_pat.stage == 1) CheckStage_ANTE(false);
                        if (_pat.stage == 1 && stageidx > -1 || _pat.stage > 1) 
                        {
                            CheckStage_EL();
                            CheckStage_AL();
                            CheckStage_DEL();
                            //wait one phase
                            CheckStage_REC();
                            CheckStage_PP(false);
                        }
                    }
                }
                else if (pt_went_thru_rec && _pat.stage == 4)
                {
                    pp_shortcut = false;
                    CheckStage_REC();
                    CheckStage_PP(false);
                }
                else if (pt_went_thru_rec && (_pat.stage == 5 || _pat.stage == 6))
                {
                    if (_pat.stage == 5) // then examine in-utero possibility
                    {
                        //if in utero start less than recdt, then change stage to 
                        // ANTE and classify there.
                        if (InUteroSurgStart() <= pt_went_thru_recdt)
                        {
                            send_back_to_ANTE = true;
                            stageidx = -1;
                            CheckStage_ANTE(true);
                        }
                    }
                    if (!send_back_to_ANTE)
                    {
                        pp_shortcut = true;
                        if (_pat.stage == 5)
                        {
                            pp_shortcut = false;
                            if (_pat.classtime < Program.g_pull_finish_q4)
                                CheckStage_REC();
                        }

                        CheckStage_PP(pp_shortcut);
                    }
                }
                else if (!pt_went_thru_rec)
                {
                    if (_pat.stage == 1)
                    {
                        //commented out 11/23
                        //CheckStage_OP();
                        //if (end_stage_analysis)
                        //{
                        //    return;
                        //}
                        ante_shortcut = true;
                        CheckStage_ANTE(ante_shortcut);
                        CheckStage_EL();
                        CheckStage_AL();
                    }
                    else if (_pat.stage == 2)
                    {
                        ante_shortcut = true; //added 11/18
                        CheckStage_ANTE(ante_shortcut);//added 11/18
                        CheckStage_EL();
                        CheckStage_AL();
                    }
                    else if (_pat.stage == 3)
                    {
                        CheckStage_AL();
                    }
                }
            } // if age > 2
            else if (_pat.stage == 9)
            {
                nbn_shortcut = true;
                CheckStage_NBN(nbn_shortcut);
            }
            else if (_pat.stage <= 7)
            {
                Program.VerboseAudit("Before ic processing...");
                CheckStage_IC();
                if (!advance_to_NBN)
                {
                    if (end_stage_analysis)
                    {
                        AssignStagesToLocations();
                        return;
                    }

                    Program.VerboseAudit("Before tc processing...");
                    CheckStage_TC();
                    if (end_stage_analysis)
                    {
                        AssignStagesToLocations();
                        return;
                    }
                }
                Program.VerboseAudit("Before nbn processing...");
                CheckStage_NBN(false);
            }
            else if (_pat.stage == 8)
            {
                Program.VerboseAudit("Before tc2 processing...");
                CheckStage_TC();
                if (!advance_to_NBN)
                {
                    if (end_stage_analysis)
                    {
                        AssignStagesToLocations();
                        return;
                    }

                    Program.VerboseAudit("Before nbn2 processing...");
                    CheckStage_NBN(false);
                    if (end_stage_analysis)
                    {
                        AssignStagesToLocations();
                        return;
                    }
                }
                Program.VerboseAudit("Before nbn3 processing...");
                CheckStage_NBN(false);
            }


            AssignStagesToLocations(pp_shortcut || nbn_shortcut);

            if (num_prestage_loc > 0)
            { //shift the stages downward.  we now have _stages[-num_prestage_loc as the latest pre-stage addition]
                Program.VerboseAudit("num_prestage_loc=" + num_prestage_loc + "  stageidx="+stageidx);
                int save_stageidx = stageidx;
                stageidx += num_prestage_loc;
                for (int i = stageidx; i >= 0; i--)
                {
                    if (i >= num_prestage_loc)
                        _stages[i] = _stages[i - save_stageidx];
                    else
                        _stages[i] = _stages_prestage_loc[save_stageidx - i];
                }

            }

            Program.VerboseAudit("CheckStaging final. stageidx=" + stageidx);
            if (stageidx == 0 && _stages[0].uname == "")
            {//then this stage was unassigned a location; do not classify
                stageidx = -1; 
            }
            else  //take care of stages without location matches. 4/4/22.
            {
                for (int i = 0; i <= stageidx; i++)
                {
                    Program.VerboseAudit("Stages: i=" + i + ": " + Enum.GetName(typeof(Stages), (int)_stages[i].stage) + " uname=]" + _stages[i].uname + "[ start=" + _stages[i].start + " finish=" + _stages[i].finish);
                    if (_stages[i].uname.Trim() == "")
                    {
                        Program.VerboseAudit("No Location Found. Removing stage i=" + i + ": " + Enum.GetName(typeof(Stages), (int)_stages[i].stage) + " start=" + _stages[i].start + " finish=" + _stages[i].finish);
                        _stages[i].remove = true;
                    }
                    else
                    {
                        Program.VerboseAudit("FinalStages: i=" + i + ": " + Enum.GetName(typeof(Stages), (int)_stages[i].stage) + " start=" + _stages[i].start + " finish=" + _stages[i].finish);
                        if (i == stageidx) _pat.lastclass_start = _stages[i].start;
                    }
                }
            }


        }

        private DateTime InUteroSurgStart()
        {
            DateTime in_utero_start = DateTime.MaxValue;

            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = query.Where(e => e.CODE.ToUpper().StartsWith("SCH"));
            query = query.Where(e => (e.DESCRIPTION.ToUpper().Contains("Fetal Invasive".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains("In Utero".ToUpper())));
            query = query.Where(e => (e.DESCRIPTION.ToUpper().Contains(";;;Anes Start".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;In Room".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;Proc Start".ToUpper())));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            int dcstartcount = query.Count();
            Program.Audit("InUteroSurgStart records count=" + dcstartcount);

            if (dcstartcount > 0)
            {
                in_utero_start = query.First().EVENT_DATETIME;
            }

            if (in_utero_start == DateTime.MaxValue) return in_utero_start;

            //Check for DEL AFTER SURGERY
            var querycs = StartNewQuery();
            querycs = querycs.Where(e => e.EVENT_DATETIME > in_utero_start);
            querycs = querycs.Where(e => e.CODE == "1028000030" || e.CODE == "9991027100281" || e.CODE == "9991027100282");
            querycs = querycs.OrderBy(e => e.EVENT_DATETIME);
            delivcs_ct = querycs.Count();
            delivcs = (delivcs_ct > 0);
            if (delivcs)
            {
                delivcs_dt = querycs.First().EVENT_DATETIME;
                Program.VerboseAudit("(2)Found Emergency C/S ct=" + delivcs_ct + " at:" + delivcs_dt);
            }

            query = StartNewQuery();
            query = query.Where(e => e.EVENT_DATETIME > in_utero_start);
            query = query.Where(e => (e.CODE == "1028000046" && e.RESULT.Val() == 10)
                                     ||
            (e.CODE == "1028000055" || e.CODE == "1028000056")
                                     ||
        (e.CODE == "9991021600004" || (e.CODE == "9991021628000" && e.RESULT.ToLower().Contains("labor induction"))) //fetal demise
                                    ||
        (e.CODE.ToUpper().StartsWith("SCH") && e.DESCRIPTION.ToUpper().Contains("CESAREAN") && !e.DESCRIPTION.ToLower().Contains("in facility"))
        );
            query = query.Where(e => !e.DESCRIPTION.ToLower().Contains(";;;schedulingstatus"));
            bool deliv = (query.Count() > 0);

            if (delivcs || deliv)
            {
                // in_utero_start occurred before delivery (queries above require > in_utero_start).
                // return as if in-Utero never occurred so we can proceed to delivery.
                in_utero_start = DateTime.MaxValue;
            }

            return in_utero_start;
        }

        private bool RECIsInClassHistory()
        {
            bool found = false;
            DateTime recovery_dt = DateTime.MaxValue;

            foreach (var cls in _pat.classary)
            {
                found = found || cls.stage == 5;
                if (cls.stage == 5)
                {
                    recovery_dt = cls.time_in;
                }
            }

            if (!found) return false;

            //Check for DEL AFTER SURGERY
            var querycs = StartNewQuery();
            querycs = querycs.Where(e => e.EVENT_DATETIME > recovery_dt);
            querycs = querycs.Where(e => e.CODE == "1028000030" || e.CODE == "9991027100281" || e.CODE == "9991027100282");
            querycs = querycs.OrderBy(e => e.EVENT_DATETIME);
            int xdelivcs_ct = querycs.Count();
            bool xdelivcs = (xdelivcs_ct > 0);
            if (xdelivcs)
                Program.VerboseAudit("(2)Found Emergency C/S ct=" + xdelivcs_ct + " at:" + querycs.First().EVENT_DATETIME);

            var query = StartNewQuery();
            query = query.Where(e => e.EVENT_DATETIME > recovery_dt);
            query = query.Where(e => (e.CODE == "1028000046" && e.RESULT.Val() == 10)
                                     ||
            (e.CODE == "1028000055" || e.CODE == "1028000056")
                                     ||
        (e.CODE == "9991021600004" || (e.CODE == "9991021628000" && e.RESULT.ToLower().Contains("labor induction"))) //fetal demise
                                    ||
        (e.CODE.ToUpper().StartsWith("SCH") && e.DESCRIPTION.ToUpper().Contains("CESAREAN") && !e.DESCRIPTION.ToLower().Contains("in facility"))
        );
            query = query.Where(e => !e.DESCRIPTION.ToLower().Contains(";;;schedulingstatus"));
            bool deliv = (query.Count() > 0);

            if (xdelivcs || deliv)
            {
                // in_utero_start occurred before delivery (queries above require > in_utero_start).
                // return as if in-Utero never occurred so we can proceed to delivery.
                recovery_dt = DateTime.MaxValue;
            }

            return (recovery_dt < DateTime.MaxValue);

        }

        private DateTime PatientWentThruRecovery()
        {
            DateTime deldt = DateTime.MaxValue;
            DateTime recdt = DateTime.MaxValue;
            //int delivcs_ct = 0;

            //Check if went thru DeliveryCS
            //var querycs = StartNewQuery(SearchDepth.SearchSinceAdmission); //   SearchPullPlus);
            //querycs = querycs.Where(e => e.CODE == "12052228" || e.CODE == "12049243" || e.CODE == "12025811");
            //querycs = querycs.OrderBy(e => e.EVENT_DATETIME);
            //delivcs_ct = querycs.Count();
            //delivcs = (delivcs_ct > 0);
            //if (delivcs)
            //{
            //    delivcs_dt = querycs.First().EVENT_DATETIME;
            //    Program.VerboseAudit("(1)Found Emergency C/S ct=" + delivcs_ct + " at:" + delivcs_dt);
            //}

            //Check if went thru Delivery
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission); //   SearchPullPlus);
            query = query.Where(e => (e.CODE == "12027447")); // Pushing
            query = query.OrderBy(e => e.EVENT_DATETIME);
            delivered = (query.Count() > 0);

            if (delivcs)
            {
                string found_what = "";
                DateTime evdt = DateTime.MinValue;
                GetResultAndEVDT("", "1028000030", "", "", out found_what, out evdt,SearchDepth.SearchSinceAdmission);
                if (evdt != DateTime.MinValue)
                    deldt = evdt;
                if (deldt == DateTime.MaxValue)
                {
                    GetResultAndEVDT("", "9991027100281", "", "", out found_what, out evdt, SearchDepth.SearchSinceAdmission);
                    if (evdt != DateTime.MinValue)
                        deldt = evdt;
                }
                if (deldt == DateTime.MaxValue)
                {
                    GetResultAndEVDT("", "9991027100282", "", "", out found_what, out evdt, SearchDepth.SearchSinceAdmission);
                    if (evdt != DateTime.MinValue)
                        deldt = evdt;
                }
            }
            //            bool deliveredCS = (deldt != DateTime.MaxValue);

            Program.VerboseAudit("deliv=" + (delivered || delivcs ? "true" : "false"));
            if (delivered && !delivcs)
            {
                deldt = query.First().EVENT_DATETIME;
                Program.VerboseAudit("deldt=" + deldt);
            }

            //Check if went thru Recovery
            var query2 = StartNewQuery(SearchDepth.SearchSinceAdmission); //   SearchPullPlus);
            query2 = query2.Where(e => e.CODE == "12052228" || e.CODE == "12049243" || e.CODE == "12025811");
            query2 = query2.OrderBy(e => e.EVENT_DATETIME);
            //int ct = query2.Count();
            //bool recov = (ct > 0);

            bool recov = false;
            if (FundalCheckFreqIsQ15min())
                recov = true;

            //Program.VerboseAudit("recov ct=" + ct + " recov=" + (recov ? "true" : "false"));
            if (recov)
            {
                recdt = query2.First().EVENT_DATETIME;
                Program.VerboseAudit("recdt=" + recdt);
            }

            if (recdt <= deldt) recdt = GetLatestDeliveryTime();

            if (!recov) // (!delivcs && delivered && !recov)
            {
                DoFundalChecks(); //sets fundaldt if found
                recov = (fundaldt > DateTime.MinValue);
                if (recov) recdt = fundaldt;
            }

            int comp = DateTime.Compare(deldt, recdt); // (deliv || recov);
            if (comp > 0) // then deldt > recdt
            {
                Program.VerboseAudit("return recdt=" + recdt);
                return recdt;
            }
            if (comp < 0) // then deldt < recdt
            {
                Program.VerboseAudit("return deldt=" + deldt);
                return recdt; // deldt;
            }
            if (comp == 0 && deldt != DateTime.MaxValue)
            {
                Program.VerboseAudit("return deldt2=" + deldt);
                return deldt;
            }

            return DateTime.MinValue;
        }

        //private bool DELAfterREC(DateTime recdt)
        //{
        //    var querycs = StartNewQuery();
        //    querycs = querycs.Where(e => e.EVENT_DATETIME > recdt);
        //    querycs = querycs.Where(e => e.CODE == "1028000030" || e.CODE == "9991027100281" || e.CODE == "9991027100282");
        //    querycs = querycs.OrderBy(e => e.EVENT_DATETIME);
        //    int xdelivcs_ct = querycs.Count();
        //    bool xdelivcs = (xdelivcs_ct > 0);
        //    if (xdelivcs)
        //        Program.VerboseAudit("(2)Found Emergency C/S ct=" + xdelivcs_ct + " at:" + querycs.First().EVENT_DATETIME);

        //    var query = StartNewQuery();
        //    query = query.Where(e => e.EVENT_DATETIME > recdt);
        //    query = query.Where(e => (e.CODE == "1028000046" && e.RESULT.Val() == 10)
        //                             ||
        //    (e.CODE == "1028000055" || e.CODE == "1028000056")
        //                             ||
        //(e.CODE == "9991021600004" || (e.CODE == "9991021628000" && e.RESULT.ToLower().Contains("labor induction"))) //fetal demise
        //                            ||
        //(e.CODE.ToUpper().StartsWith("SCH") && e.DESCRIPTION.ToUpper().Contains("CESAREAN") && !e.DESCRIPTION.ToLower().Contains("in facility"))
        //);
        //    query = query.Where(e => !e.DESCRIPTION.ToLower().Contains(";;;schedulingstatus"));
        //    bool deliv = (query.Count() > 0);

        //    return (xdelivcs || deliv);
        //}

        private void DoFundalChecks()
        {
            var query = StartNewQuery();
            query = query.Where(e => e.CODE == "12052228" || e.CODE == "12049243" || e.CODE == "12025811");
            query = query.OrderBy(e => e.EVENT_DATETIME);
            bool fundal = (query.Count() > 0);

            if (fundal)
            {
                fundaldt = query.First().EVENT_DATETIME;
                Program.VerboseAudit("fundal=" + (fundal ? "true" : "false") + " REC time=" + fundaldt);
                return;
            }

            //var query2 = StartNewQuery();
            //query2 = query2.Where(e => e.CODE == "9990000305560");
            //query2 = query2.Where(e => e.RESULT.ToUpper() == "PUMPING");
            //query2 = query2.OrderBy(e => e.EVENT_DATETIME);
            //bool pumping = (query2.Count() > 0);

            //if (pumping)
            //{
            //    fundaldt = query2.First().EVENT_DATETIME;
            //    Program.VerboseAudit("pumping=" + (pumping ? "true" : "false") + " REC time=" + fundaldt);
            //}

        }

        private void ShowLocations()
        {
            //bool pt_went_thru_rec = (pt_went_thru_recdt > DateTime.MinValue);

            Program.VerboseAudit("Locations: pt_went_thru_recdt=" + pt_went_thru_recdt);
            for (int i = 1; i <= _pat.num_locs; i++)
            {
                Program.VerboseAudit(i + ": " + _pat.locary[i].uname + _pat.locary[i].unit_id + " in=" + _pat.locary[i].time_in + " out=" + _pat.locary[i].time_out);
                if ((_pat.locary[i].unit_id == 15393435 || _pat.locary[i].unit_id == 16132738) //&& pt_went_thru_rec 
                    && (_pat.stage <= 5)
                    && pt_went_thru_recdt != DateTime.MinValue
                    && _pat.locary[i].time_in > pt_went_thru_recdt)
                {
                    Program.VerboseAudit("pp loc and timein>pt went thru recdt:" + _pat.locary[i].time_in + ">" + pt_went_thru_recdt);
                    if (pplocstartdt == DateTime.MinValue)
                        pplocstartdt = _pat.locary[i].time_in;
                    if (pplocenddt < _pat.locary[i].time_in)
                        pplocenddt = _pat.locary[i].time_in;
                }
            }

        }

        private void AssignStagesToLocations()
        {
            AssignStagesToLocations(false);
        }

        private void AssignStagesToLocations(bool ppshortcut)
        {
            //19351      RO EI30/32  Obstetrics
            //4049742    RO EI33     Family Birth Center
            Program.VerboseAudit("Entering AssignStagesToLocations...stageidx=" + stageidx);

            for (int k = 1; k <= stageidx; k++)
            { // fill in finish times according to start times of next stage
                _stages[k - 1].finish = _stages[k].start;
                Program.VerboseAudit("..stageidx=" + k + " _stages["+(k-1)+"].finish="+ _stages[k - 1].finish);
            }

            if (ppshortcut) // revived 2/4
            {
                Program.VerboseAudit("shortcut _stages[0].start: " + _stages[0].start);

                bool found = false;
                int locidx=1;
                for (int i = 1; i <= _pat.num_locs; i++)
                {
                    if (!found)
                    {
                        Program.VerboseAudit(i + ": " + _pat.locary[i].uname + _pat.locary[i].unit_id + " in=" + _pat.locary[i].time_in + " out=" + _pat.locary[i].time_out);
                        if (_pat.locary[i].time_in <= _stages[0].start && _pat.locary[i].time_out > _stages[0].start)
                        {
                            found = true;
                            locidx = i;
                        }
                    }
                }
                _stages[0].unitid = _pat.locary[locidx].unit_id;
                _stages[0].uname = _pat.locary[locidx].uname;
                //_stages[0].start = _pat.pull_start;
            }
            else
            {
                for (int i = 0; i <= stageidx; i++)
                {
                    if (i == stageidx) // then last stage
                    {
                        Program.VerboseAudit("i==stageidx="+i);
                        if (_stages[i].start == _stages[i].finish)
                        {
                            Program.VerboseAudit("[i].start==finish=" + _stages[i].start);
                            _stages[i].finish = _stages[i].start.AddHours(24);
                        }
                    }
                    Program.VerboseAudit(i + ":Stages[i].stage=" + Enum.GetName(typeof(Stages), (int)_stages[i].stage) + " start: " + _stages[i].start + " finish=" + _stages[i].finish);
                    int prev_unit_id = -1;
                    bool added_for_loc_change = false;
                    for (int j = 1; j <= _pat.num_locs; j++)
                    {
                        stage_unit_set = false;
                        if (_stages[i].start >= _pat.locary[j].time_in && _stages[i].start < _pat.locary[j].time_out)
                        {
                            //if (i == 0 &&  &&_stages[i].start > _pat.locary[j].time_in)
                            //{// then add a holder between _stages[i].start and _pat.locary[j].time_in
                            //    num_prestage_loc++;
                            //    Program.VerboseAudit("  Addl for pre-stage start time range betw last stage and this stage: " + num_prestage_loc + "=" + Enum.GetName(typeof(Stages), (int)_stages[i].stage) + " loc=" + _pat.locary[j].uname + " start=" + _pat.locary[j].time_in);
                            //    _stages_prestage_loc[num_prestage_loc].stage = (Stages)_pat.stage;
                            //    _stages_prestage_loc[num_prestage_loc].start = _pat.locary[j].time_in;
                            //    _stages_prestage_loc[num_prestage_loc].uname = _pat.locary[j].uname;
                            //    _stages_prestage_loc[num_prestage_loc].unitid = _pat.locary[j].unit_id;
                            //}
                            _stages[i].unitid = _pat.locary[j].unit_id;
                            _stages[i].uname = _pat.locary[j].uname;
                            Program.VerboseAudit("  Setting Stage Unit to Location: " + j + " loc=" + _pat.locary[j].uname + " start=" + _pat.locary[j].time_in + " finish=" + _pat.locary[j].time_out);
                            stage_unit_set = true;
                        }
                        added_for_loc_change = false;
                        Program.VerboseAudit("  Location: " + j + " loc=" + _pat.locary[j].uname + " start=" + _pat.locary[j].time_in + " finish=" + _pat.locary[j].time_out);
                        if (j == 1)
                        {
                            prev_unit_id = _pat.locary[j].unit_id;
                            if (_stages[i].start < _pat.locary[j].time_in)
                            {
                                Program.VerboseAudit("  Force stage start to be first Location start: " + j + "=" + Enum.GetName(typeof(Stages), (int)_stages[i].stage) + " loc=" + _pat.locary[j].uname + " start=" + _pat.locary[j].time_in);
                                _stages[i].start = _pat.locary[j].time_in;
                                _stages[i].unitid = _pat.locary[j].unit_id;
                                _stages[i].uname = _pat.locary[j].uname;
                            }
                        }
                        else
                        {
                            Program.VerboseAudit("Point 1...j=" + j + "  stage_unit_set="+(stage_unit_set?"true":"false") + " [j].unit_id="+ _pat.locary[j].unit_id+"  prev_unit_id="+ prev_unit_id);
                            if (_pat.locary[j].unit_id != prev_unit_id && !stage_unit_set)
                            {
                                Program.VerboseAudit("Point 2...");
                                //Jan12'21 if (_pat.classtime != DateTime.MinValue && _pat.classtime < _pat.locary[j].time_in && _stages[i].start <= _pat.locary[j].time_in)
                                {
                                    if ((_stages[i].start >= _pat.locary[j].time_in && _stages[i].start < _pat.locary[j].time_out)
                                        ||
                                        (_stages[i].finish > _pat.locary[j].time_in && _stages[i].finish <= _pat.locary[j].time_out)
                                        ||
                                        (_stages[i].start < _pat.locary[j].time_in && _stages[i].finish > _pat.locary[j].time_out))
                                    {
                                        _stages[i].has_addl_locs = true;
                                        num_addl_loc++;
                                        Program.VerboseAudit("  Addl Location for loc change in stage: " + num_addl_loc + "=" + Enum.GetName(typeof(Stages), (int)_stages[i].stage) + " loc=" + _pat.locary[j].uname + " start=" + _pat.locary[j].time_in);
                                        _stages_addl_loc[num_addl_loc].stage = _stages[i].stage;
                                        _stages_addl_loc[num_addl_loc].start = _pat.locary[j].time_in;
                                        _stages_addl_loc[num_addl_loc].uname = _pat.locary[j].uname;
                                        _stages_addl_loc[num_addl_loc].unitid = _pat.locary[j].unit_id;

                                        added_for_loc_change = true;
                                    }
                                }
                            }
                            prev_unit_id = _pat.locary[j].unit_id;//always set prev unit id
                        }
                        if (!added_for_loc_change)
                            if (_stages[i].start >= _pat.locary[j].time_in && _stages[i].start < _pat.locary[j].time_out)
                            {
                                Program.VerboseAudit("  LoopA: " + j + "=" + Enum.GetName(typeof(Stages), (int)_stages[i].stage) + " loc=" + _pat.locary[j].uname + " start=" + _pat.locary[j].time_in);
                                //_stages[i].unitid = _pat.locary[j].unit_id;
                                //_stages[i].uname = _pat.locary[j].uname;
                                if ((_stages[i].stage == Stages.NBN || _stages[i].stage == Stages.PP)
                                    && (_stages[i].start >= _pat.locary[j].time_in) //changed on 1/8 looking at Shimek
                                    && (_pat.classtime > DateTime.MinValue && !(_pat.locary[j].time_in <= _pat.classtime && _pat.classtime < _stages[i].start)))
                                { //then make it start at the last location start
                                    DateTime save_stagestart = _stages[i].start;
                                    if (_pat.num_locs >= j)
                                    {
                                        Program.VerboseAudit("  Adjusting time IN for stage: " + Enum.GetName(typeof(Stages), (int)_stages[i].stage) + " set stage start to looploc time in=" + _pat.locary[j].time_in);
                                        _stages[i].start = _pat.locary[j].time_in;//changed to j from _pat.num_locs index
                                        _stages[i].unitid = _pat.locary[j].unit_id;
                                        _stages[i].uname = _pat.locary[j].uname;
                                    }
                                    //_stages[i].has_addl_locs = true;
                                    //num_addl_loc++;
                                    //Program.VerboseAudit("  Addl Location for Adj stage: " + num_addl_loc + "=" + Enum.GetName(typeof(Stages), (int)_stages[i].stage) + " loc=" + _pat.locary[j].uname + " start=" + save_stagestart);
                                    //_stages_addl_loc[num_addl_loc].stage = _stages[i].stage;
                                    //_stages_addl_loc[num_addl_loc].start = save_stagestart;
                                    //_stages_addl_loc[num_addl_loc].uname = _pat.locary[j].uname;
                                    //_stages_addl_loc[num_addl_loc].unitid = _pat.locary[j].unit_id;
                                }
                            }
                            else if (_stages[i].start < _pat.locary[j].time_in && _stages[i].finish > _pat.locary[j].time_in && _stages[i].finish <= _pat.locary[j].time_out) // && i < stageidx)
                            {
                                Program.VerboseAudit("  LoopB: " + j + "=" + Enum.GetName(typeof(Stages), (int)_stages[i].stage) + " loc=" + _pat.locary[j].uname + " start=" + _pat.locary[j].time_in);
                                Program.VerboseAudit("  LoopB for i: " + i + ":_stages[i].start=" + _stages[i].start + "_stages[i].finish=" + _stages[i].finish);
                                //_stages[i].unitid = _pat.locary[j].unit_id;
                                //_stages[i].uname = _pat.locary[j].uname;

                                _stages[i].has_addl_locs = true;
                                num_addl_loc++;
                                Program.VerboseAudit("  Addl Location for stage: " + num_addl_loc + "=" + Enum.GetName(typeof(Stages), (int)_stages[i].stage) + " loc=" + _pat.locary[j].uname + " start=" + _pat.locary[j].time_in);
                                _stages_addl_loc[num_addl_loc].stage = _stages[i].stage;
                                _stages_addl_loc[num_addl_loc].start = _pat.locary[j].time_in;
                                _stages_addl_loc[num_addl_loc].uname = _pat.locary[j].uname;
                                _stages_addl_loc[num_addl_loc].unitid = _pat.locary[j].unit_id;
                            }
                            else if (_stages[i].start < _pat.locary[j].time_in && _stages[i].finish > _pat.locary[j].time_out && i < stageidx)
                            {
                                Program.VerboseAudit("  LoopC: " + j + "=" + Enum.GetName(typeof(Stages), (int)_stages[i].stage) + " loc=" + _pat.locary[j].uname + " start=" + _pat.locary[j].time_in);
                                //_stages[i].unitid = _pat.locary[j].unit_id;
                                //_stages[i].uname = _pat.locary[j].uname;

                                _stages[i].has_addl_locs = true;
                                num_addl_loc++;
                                Program.VerboseAudit("  Addl Location for stage: " + num_addl_loc + "=" + Enum.GetName(typeof(Stages), (int)_stages[i].stage) + " loc=" + _pat.locary[j].uname + " start=" + _pat.locary[j].time_in);
                                _stages_addl_loc[num_addl_loc].stage = _stages[i].stage;
                                _stages_addl_loc[num_addl_loc].start = _pat.locary[j].time_in;
                                _stages_addl_loc[num_addl_loc].uname = _pat.locary[j].uname;
                                _stages_addl_loc[num_addl_loc].unitid = _pat.locary[j].unit_id;
                            }
                    } // for pat locs
                } //for stageidx
                //if the last stage is beyond the last location, then reduce the number of stages.
                if (stageidx > -1)
                    if (_stages[stageidx].start > _pat.locary[_pat.num_locs].time_out)
                        stageidx--;
            }
        }


        private void CheckStage_OP()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("OP Stage");
            Program.VerboseAudit("---------------");

            bool fhr = false;
            bool toco = false;
            bool fhrfoundany = false;
            bool tocofoundany = false;
            bool foundany = false;
            bool active_lab = false;
            //            _stages[++stageidx].stage = Stages.PP;
            Program.VerboseAudit("OP1: _pat.pull_start=" + _pat.pull_start + " finish=" + _pat.pull_finish + " effdep=" + _pat.eff_departuredt);
            if (_pat.eff_departuredt == DateTime.MinValue) return;
            if (_pat.stage == 0 && fundaldt > DateTime.MinValue) return; //go to PP
            if (_pat.stage == 0 && pt_went_thru_recdt > DateTime.MinValue) return;

            if (_pat.pull_start.AddHours(24) >= _pat.pull_finish)
            { //if LOS < 24 hrs and no stage charting, then OP
                shortOPLOS = (PFSUtility.DateDiffInMinutes(_pat.pull_start, _pat.eff_departuredt) <= 24 * 60);
                if (!shortOPLOS)
                {
                    Program.VerboseAudit("OP: start=" + _pat.pull_start + " finish=" + _pat.pull_finish);
                    fhr = CountFHRAssessments(12, 60, out fhrfoundany);
                    toco = CountTOCOAssessments(12, 60, out tocofoundany);
                    Program.VerboseAudit("fhr:" + (fhr ? "true" : "false"));
                    Program.VerboseAudit("toco:" + (toco ? "true" : "false"));
                    active_lab = Exists("", "1020100606", "", "", "Yes");
                    active_lab |= Exists("", "1028000046", "", "", "4,6,7,8,9,lip");
                    active_lab |= Exists("", "1028000046", "", "", EXACT_MATCH_PREFIX + "5"); // to avoid the result ".5-fingertip"
                    if (active_lab) Program.VerboseAudit("ACTIVE LABOR FOUND");
                }
                //bool deliv = Exists("", "1028000046", "", "", "10");
                //deliv |= Exists("", "1028000055,1028000056,1028000030,9991027100281,9991027100282", "", "", "");
                //if (deliv) Program.VerboseAudit("FULL DILATION OR CS DETERMINATION");

                Program.VerboseAudit("OP: fhr="+fhr + " fhrfoundany=" + fhrfoundany + "; toco="+ toco + " tocofoundany=" + tocofoundany);
                foundany = fhrfoundany || tocofoundany;
                if (shortOPLOS || (!fhr && !toco && !foundany && !active_lab && !delivered))
                {
                    if (ProcExists(1, _pat.pull_start, _pat.pull_finish))
                    {
                        end_stage_analysis = true;
                    }
                    else
                    {
                        _stages[++stageidx].stage = Stages.OP;
                        _stages[stageidx].start = _pat.pull_start;
                        _stages[stageidx].finish = _pat.pull_finish;
                        end_stage_analysis = true;
                        Program.VerboseAudit("OP stage in : " + _stages[stageidx].start);
                        Program.VerboseAudit("OP stage out: " + _stages[stageidx].finish);
                        _pat.stage = (int)Stages.OP;
                    }
                }
            }

        }

        private void CheckStage_ANTE(bool ante_shortcut) //ante_shortcut=did NOT go thru RECOVERY
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ANTE Stage");
            Program.VerboseAudit("---------------");

            DateTime min_ante_locstartdt;

            if (_pat.stage == 0) //then this is the first class, use first loc time
            {
                Program.VerboseAudit("ANTE: _pat.stage=" + _pat.stage + " _pat.locary[1].time_in=" + _pat.locary[1].time_in);
                min_ante_locstartdt = _pat.locary[1].time_in.AddHours(24);
                if (min_ante_locstartdt > Program.g_pull_finish_q4)
                {
                    Program.VerboseAudit("ANTE: bailing=too early: min_ante_locstartdt=" + min_ante_locstartdt + " q4="+ Program.g_pull_finish_q4);
                    return;
                }
            }

            //CHECK IF ACTIVE LABOR FOUND.  IF IT IS, THEN NEED TO COMPLETE ANTE=No Waiting.
            bool active_lab;
            var query = StartNewQuery();
            query = query.Where(e => (e.CODE == "1020100606" && e.RESULT.ToLower() == "yes")
                                     ||
               (e.CODE == "1028000046" && ((e.RESULT.Val() >= 4 && e.RESULT.Val() <= 9) || e.RESULT.ToLower().Contains("lip"))));
            active_lab = (query.Count() > 0);


            if (stageidx >= 0)
                Program.VerboseAudit("Entering ANTE stage...stageidx=" + stageidx + "  stage=" + Enum.GetName(typeof(Stages), (int)_stages[stageidx].stage));

            bool adjusted_to_7am = false; // adjustment to 7am (less than 24 hrs).
            bool setANTE = false;
            //            _stages[++stageidx].stage = Stages.PP;
            //Program.VerboseAudit("PP1 stageidx=" + stageidx);
            Program.VerboseAudit("ante_shortcut:" + (ante_shortcut ? "true" : "false"));

            DateTime testtime = _pat.classtime.Date.AddDays(2).AddHours(7);
            DateTime nextclasstime = DateTime.MinValue;
            Program.VerboseAudit("ante: _pat.classtime=" + _pat.classtime + " testtime=" + testtime);
            if (_pat.classtime < testtime)
                nextclasstime = testtime;
            else
                nextclasstime = testtime.AddDays(1);

            Program.VerboseAudit("ante: nextclasstime=" + nextclasstime);
            if (nextclasstime < Program.g_pull_finish_q4.Date.AddHours(7))
                nextclasstime = Program.g_pull_finish_q4;
            Program.VerboseAudit("+ante: nextclasstime=" + nextclasstime);

            bool advance_to_ANTE = false;
            DateTime advance_to_ANTE_time = DateTime.MinValue;

            bool advance_to_ANTE_special = false;
            DateTime advance_to_ANTE_time_special = DateTime.MinValue;
            if (CheckForLocationChangeBtwn(_pat.classtime, nextclasstime, out advance_to_ANTE_time_special))
            {
                advance_to_ANTE_special = true;
            }
            if (!advance_to_ANTE_special)
            {
                DateTime keeptimein;
                int targetidx;
                for (int i = _pat.num_locs; i >= 1; i--)
                {
                    Program.VerboseAudit(i + ":locary[i].uname=" + _pat.locary[i].uname +
                        " in: " + _pat.locary[i].time_in + " out=" + _pat.locary[i].time_out + " _pat.pull_finish=" + _pat.pull_finish);
                    if (nextclasstime >= _pat.locary[i].time_in
                        && nextclasstime < _pat.locary[i].time_out)
                    {
                        keeptimein = _pat.locary[i].time_in;
                        targetidx = i - 1;
                        Program.VerboseAudit("keeptimein=" + keeptimein);
                        advance_to_ANTE = true;
                        advance_to_ANTE_time = keeptimein;
                    }
                }
            }

            if (PFSUtility.DateDiffInDays(_pat.classtime, nextclasstime) < 1)
            {
                Program.VerboseAudit("datediffindays<=1: _pat.classtime=" + _pat.classtime + " nextclasstime=" + nextclasstime);
                nextclasstime = nextclasstime.AddDays(1);
                adjusted_to_7am = true;
            }
            Program.VerboseAudit("ante: adjusted to 7am=" + adjusted_to_7am.ToString());
            Program.VerboseAudit(" last class time=" + _pat.classtime + " next class time=" + nextclasstime + " g_pull_finish=" + Program.g_pull_finish + " g_pull_finish_q4=" + Program.g_pull_finish_q4);

            //The problem here is that I need the correct range of time for the 
            //assessments but I don't know the range until after.
            //The range needs to be set based on last class time = pat.classtime as start.
            // ALSO: avoid checking FHR...:
            bool fhr = false;
            bool fhrfoundany = false;
            bool toco = false;
            bool tocofoundany = false;
            bool foundany = false;
            //            if (!RECIsInClassHistory())
            fhr = CountFHRAssessments(12, 60, out fhrfoundany);
            toco = CountTOCOAssessments(12, 60, out tocofoundany);

            Program.VerboseAudit("ANTE fhr:" + (fhr ? "true" : "false"));
            Program.VerboseAudit("ANTE fhrfoundany:" + (fhrfoundany ? "true" : "false"));
            Program.VerboseAudit("ANTE toco:" + (toco ? "true" : "false"));
            Program.VerboseAudit("ANTE tocofoundany:" + (tocofoundany ? "true" : "false"));
            foundany = fhrfoundany || tocofoundany;
            if ((!(fhr || toco) && foundany) && ante_shortcut || active_lab || delivered || delivcs)
            {
                Program.VerboseAudit("_pat.stage=" + _pat.stage);
                Program.VerboseAudit("nextclasstime=" + nextclasstime + "  g_pull_finish_q4=" + Program.g_pull_finish_q4);
                if (_pat.stage != 1) // then this is the first ANTE class
                {
                    setANTE = true;
                    Program.VerboseAudit("not prev ante shortcut: pull date 7am=" + Program.g_pull_finish);
                    _stages[++stageidx].stage = Stages.ANT;
                    _stages[stageidx].start = _pat.pull_start; //Program.g_pull_finish_q4;
                    _stages[stageidx].finish = _pat.pull_start.AddDays(1); //Program.g_pull_finish_q4.AddDays(1);
                    Program.VerboseAudit("+ANTE stage in : " + _stages[stageidx].start);
                    Program.VerboseAudit("+ANTE stage out: " + _stages[stageidx].finish);
                    //if (_stages[stageidx].finish > Program.g_pull_finish_q4)
                    //{
                    //    //wait 24 hours
                    //    Program.VerboseAudit("Waiting 24 hrs before next ANTE class.");
                    //    stageidx = -1; // prevent class
                    //}
//                    return;
                }
                else if (nextclasstime <= Program.g_pull_finish_q4 || active_lab)
                {
                    setANTE = true;
                    Program.VerboseAudit("ante shortcut: nextclasstime=" + nextclasstime + "   pull date q4=" + Program.g_pull_finish_q4);
                    _stages[++stageidx].stage = Stages.ANT;
                    if (advance_to_ANTE_special)
                    {
                        Program.VerboseAudit("special advance=true");
                        _stages[stageidx].start = advance_to_ANTE_time_special;
                        _stages[stageidx].finish = _pat.pull_finish;
                    }
                    else if (advance_to_ANTE)
                    {
                        Program.VerboseAudit("advance=true");
                        _stages[stageidx].start = nextclasstime.AddDays(-1); // advance_to_NBN_time;
                        _stages[stageidx].finish = _pat.pull_finish;
                    }
                    else if (adjusted_to_7am)
                    {
                        Program.VerboseAudit("adjusted to 7am=true");
                        _stages[stageidx].start = nextclasstime.AddDays(-1); //Mar9 use the prev day as the stage start
                        if (nextclasstime <= Program.g_pull_finish_q4) //_pat.pull_finish)
                            _stages[stageidx].start = nextclasstime;
                        //if (_stages[stageidx].start > _pat.classtime)
                        //    _stages[stageidx].start = _pat.classtime;
                        _stages[stageidx].finish = nextclasstime;
                    }
                    else
                    {
                        Program.VerboseAudit("adjusted to 7am=FALSE");
                        _stages[stageidx].start = nextclasstime.AddDays(-1); // _pat.classtime; // Program.g_pull_finish;
                        ////if (nextclasstime <= _pat.pull_finish)
                        //    if (_stages[stageidx].start > _pat.classtime)
                        //        _stages[stageidx].start = _pat.classtime;
                        _stages[stageidx].finish = Program.g_pull_finish_q4;
                    }
                    Program.VerboseAudit("-ANTE stage in : " + _stages[stageidx].start);
                    Program.VerboseAudit("-ANTE stage out: " + _stages[stageidx].finish);
                    Program._pat.lastclass_start = Program.g_pull_start;
                }
                else
                {
                    Program.VerboseAudit("Waiting 24 hrs since last ANTE class at: " + _pat.classtime);
                    stageidx = -1; // prevent class
                    return;
                }

            } // if anteshortcut

            {
                ////Check for AL
                //var query = StartNewQuery();
                //query = query.Where(e => (e.CODE == "1020100606" && e.RESULT.ToLower() == "yes")
                //                         ||
                //   (e.CODE == "1028000046" && ((e.RESULT.Val() >= 4 && e.RESULT.Val() <= 9) || e.RESULT.ToLower().Contains("lip"))));
                //bool active_lab = (query.Count() > 0);

                //Check for DEL
                var querycs = StartNewQuery();
                querycs = querycs.Where(e => e.CODE == "1028000030" || e.CODE == "9991027100281" || e.CODE == "9991027100282");
                querycs = querycs.OrderBy(e => e.EVENT_DATETIME);
                delivcs_ct = querycs.Count();
                delivcs = (delivcs_ct > 0);
                if (delivcs)
                {
                    delivcs_dt = querycs.First().EVENT_DATETIME;
                    Program.VerboseAudit("(2)Found Emergency C/S ct=" + delivcs_ct + " at:" + delivcs_dt);
                }

                query = StartNewQuery();
                query = query.Where(e => (e.CODE == "1028000046" && e.RESULT.Val() == 10)
                                         ||
                (e.CODE == "1028000055" || e.CODE == "1028000056")
                                         ||
            (e.CODE == "9991021600004" || (e.CODE == "9991021628000" && e.RESULT.ToLower().Contains("labor induction"))) //fetal demise
                                        ||
            (e.CODE.ToUpper().StartsWith("SCH") && e.DESCRIPTION.ToUpper().Contains("CESAREAN") && !e.DESCRIPTION.ToLower().Contains("in facility"))
            );
                query = query.Where(e => !e.DESCRIPTION.ToLower().Contains(";;;schedulingstatus"));
                bool deliv = (query.Count() > 0);

                DateTime deldt = DateTime.MaxValue;
                if (delivcs)
                {
                    string found_what = "";
                    DateTime evdt = DateTime.MinValue;
                    GetResultAndEVDT("", "1028000030", "", "", out found_what, out evdt, SearchDepth.SearchSinceAdmission);
                    if (evdt != DateTime.MinValue)
                        deldt = evdt;
                    if (deldt == DateTime.MaxValue)
                    {
                        GetResultAndEVDT("", "9991027100281", "", "", out found_what, out evdt, SearchDepth.SearchSinceAdmission);
                        if (evdt != DateTime.MinValue)
                            deldt = evdt;
                    }
                    if (deldt == DateTime.MaxValue)
                    {
                        GetResultAndEVDT("", "9991027100282", "", "", out found_what, out evdt, SearchDepth.SearchSinceAdmission);
                        if (evdt != DateTime.MinValue)
                            deldt = evdt;
                    }
                }
                //bool deliveredCS = (deldt != DateTime.MaxValue);

                if (fhr || toco || active_lab || deliv || delivcs)
                {
                    if (fhr || toco) Program.VerboseAudit("EL: FHR or TOCO is q4hr or more frequent.");
                    if (active_lab) Program.VerboseAudit("EL: AL data found.");
                    if (delivcs)
                        Program.VerboseAudit("EL: DELcs data found.");
                    else if (deliv)
                        Program.VerboseAudit("EL: DEL data found.");

                    if (RECIsInClassHistory() && stageidx == -1)
                    {
                        _stages[++stageidx].stage = Stages.ANT;
                    }
                    else
                    {
                        _stages[++stageidx].stage = Stages.EL;
                    }
                    Program.VerboseAudit("1stageidx=" + stageidx + "  Stage " + Enum.GetName(typeof(Stages), (int)_stages[stageidx].stage) + "  start=" + _stages[stageidx].start + " end=" + _stages[stageidx].finish);
                    //if this stageidx start < stageidx-1.start, then forego the prev stage at stageidx-1 and just use the new stage.
                    if (stageidx >= 1 && (_stages[stageidx].start < _stages[stageidx - 1].start))
                    {
                        Program.VerboseAudit("stageidx=" + stageidx + " stageidx-1=" + (stageidx - 1) + "  Stage " + Enum.GetName(typeof(Stages), (int)_stages[stageidx].stage));
                        Program.VerboseAudit("  [stageidx-1]start=" + _stages[stageidx-1].start + " [stageidx-1]end=" + _stages[stageidx-1].finish + " [stageidx]start = " + _stages[stageidx].start + " [stageidx]end = " + _stages[stageidx].finish);
                        _stages[stageidx - 1].stage = _stages[stageidx].stage;
                        //_stages[stageidx - 1].start = _stages[stageidx].start;
                        //_stages[stageidx - 1].finish = _stages[stageidx].finish;
                        stageidx--;
                    }
                    else
                    {
                        _stages[stageidx].start = _pat.pull_start;
                        _stages[stageidx].finish = _pat.pull_finish;
                    }
                    if (_pat.stage >= 5)
                    {
                        _stages[stageidx].start = nextclasstime.AddDays(-1);
                        _stages[stageidx].finish = nextclasstime;
                    }
                    Program.VerboseAudit("2stageidx=" + stageidx + "  Stage " + Enum.GetName(typeof(Stages), (int)_stages[stageidx].stage) + "  start=" + _stages[stageidx].start + " end=" + _stages[stageidx].finish);
                }
                else
                {
                    Program.VerboseAudit("ANTE: FHR or TOCO less frequent than q4hr. setANTE="+ (setANTE ? "true" : "false"));
                    if (!setANTE)
                    {
                        _stages[++stageidx].stage = Stages.ANT;
                        _stages[stageidx].start = _pat.pull_start;
                        //_stages[stageidx].finish = _pat.pull_finish;
                        _stages[stageidx].finish = Program.g_pull_finish_q4;
                    }
                }
                //_stages[stageidx].start = _pat.pull_start;
                //_stages[stageidx].finish = _pat.pull_finish;
                //            Program.VerboseAudit("Stage " + Enum.GetName(typeof(Stages), (int)_stages[stageidx].stage) + "  start=" + _stages[stageidx].start + " end=" + _stages[stageidx].finish);
            }

            if (stageidx >= 0)
                Program.VerboseAudit("Leaving ANTE stage...stageidx=" + stageidx + "  stage="+ Enum.GetName(typeof(Stages), (int)_stages[stageidx].stage));

            if (stageidx >= 0)
            {
                Program.VerboseAudit(Enum.GetName(typeof(Stages), (int)_stages[stageidx].stage) + " stage in : " + _stages[stageidx].start);
                Program.VerboseAudit(Enum.GetName(typeof(Stages), (int)_stages[stageidx].stage) + " stage out: " + _stages[stageidx].finish);

                Program.VerboseAudit("_stages[stageidx].finish=" + _stages[stageidx].finish + "  Program.g_pull_finish_q4=" + Program.g_pull_finish_q4);
                if (!setANTE && _stages[stageidx].finish > Program.g_pull_finish_q4)
                {
                    //wait 24 hours
                    Program.VerboseAudit("Waiting 24 hrs before next ANTE class....");
                    stageidx = -1; // prevent class
                }
            }

        }

        private bool CheckForLocationChangeBtwn(DateTime beg, DateTime fin, out DateTime dtm)
        {
            //This will find the earliest location between the 2 times, if it exists.
            DateTime nextclasstime = DateTime.MinValue;
            dtm = DateTime.MinValue;
            int targetidx=0;
            bool found = false;
            bool found7am = false;

            Program.VerboseAudit("CheckForLocationChangeBtwn...beg="+ beg + " fin="+fin);
            for (int i = _pat.num_locs; i >= 1; i--)
            {
                Program.VerboseAudit(i + ";locary[i].uname=" + _pat.locary[i].uname +
                    " in: " + _pat.locary[i].time_in + " out=" + _pat.locary[i].time_out);
                if (beg.Hour < 7)
                {
                    if (beg.Date.AddHours(7) < _pat.locary[i].time_in)
                        found7am = true;
                }
                else if (beg.Hour >= 7)
                {
                    if (beg.Date.AddDays(1).AddHours(7) < _pat.locary[i].time_in)
                        found7am = true;
                }
                if (!found7am)
                {
                    if (beg < _pat.locary[i].time_in && _pat.locary[i].time_in <= fin)
                    {
                        targetidx = i;
                        dtm = _pat.locary[i].time_in;
                        Program.VerboseAudit("found i=" + i + " dtm=" + _pat.locary[i].time_in);
                        found = true;
                    }
                }
            }

            return found; //(targetidx < _pat.num_locs);
        }
        private bool CheckNextClassTime(DateTime nextclassdt, out DateTime validnextclassdt)
        {
            validnextclassdt = DateTime.MinValue;

            Program.VerboseAudit("CheckNextClassTime: nextclassdt=" + nextclassdt);
            bool found = false;
            for (int i = 1; i <= _pat.num_locs; i++)
            {
                if (!found)
                {
                    if (_pat.locary[i].time_in >= nextclassdt && _pat.locary[i].time_in < nextclassdt)
                    {
                        found = true;
                        validnextclassdt = _pat.locary[i].time_in;
                    }
                }
            }
            return found;
        }

        private void CheckStage_EL()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("EL Stage");
            Program.VerboseAudit("---------------");
            // If patient has LOS < 24 hours and does not have trigger for ANTE, AL or DEL, REC or PP, then enter as Obs and Assessment activity    
            // If patient has LOS > 24 hours and does not have trigger for ANTE, AL or DEL, REC or PP, then enter as Obs and Assessment activity    
            // NOTE: this patient would have min of hourly FHR rows
            //            _stages[++stageidx].stage = Stages.EL;
            bool early_lab;
            DateTime min_locstartdt;

            if (_pat.stage == 0) //then this is the first class, use first loc time
            {
                min_locstartdt = _pat.locary[1].time_in.AddHours(24);
                if (min_locstartdt > Program.g_pull_finish_q4)
                {
                    Program.VerboseAudit("EL: bailing=too early: min_locstartdt=" + min_locstartdt + " q4=" + Program.g_pull_finish_q4);
                    return;
                }
            }

            var query = StartNewQuery();
            query = query.Where(e => (e.CODE == "12026523") && (e.RESULT.ToInteger() < 4));
            early_lab = (query.Count() > 0);

            if (early_lab)
            {
                Program.VerboseAudit("EARLY LABOR FOUND:...");
                DateTime mintime = DateTime.MaxValue;
                foreach (var item in query)
                {
                    Program.VerboseAudit("early labor: " + item.CODE + " " + item.DESCRIPTION + " " + item.RESULT + " " + item.EVENT_DATETIME.ToString());
                    if (item.EVENT_DATETIME < mintime)
                    {
                        mintime = item.EVENT_DATETIME;//find the latest time
                    }
                }
                if (stageidx >= 0)
                {
                    if (_stages[stageidx].start <= mintime && _stages[stageidx].start.AddHours(1) >= mintime)
                    {
                        Program.VerboseAudit("EL will overwrite short " + Enum.GetName(typeof(Stages), (int)_stages[stageidx].stage) + " stage." + "  start=" + _stages[stageidx].start);
                        _stages[stageidx].stage = Stages.EL;
                        _stages[stageidx].finish = _pat.pull_finish;

                    }
                    else if (_stages[stageidx].start.AddHours(1) >= mintime)
                    { // overwrite this stage since it's within 1 hr of AL start.
                        Program.VerboseAudit("EL will overwrite " + Enum.GetName(typeof(Stages), (int)_stages[stageidx].stage) + " stage." + "  start=" + _stages[stageidx].start);
                        _stages[stageidx].stage = Stages.EL;
                        _stages[stageidx].start = mintime;
                        _stages[stageidx].finish = _pat.pull_finish;
                    }
                    else
                    {
                        _stages[++stageidx].stage = Stages.EL;
                        _stages[stageidx].start = mintime;
                        _stages[stageidx].finish = _pat.pull_finish;
                        Program.VerboseAudit("EL stage add." + " EL start=" + _stages[stageidx].start + " end=" + _stages[stageidx].finish);
                    }
                }
                else
                {
                    _stages[++stageidx].stage = Stages.EL;
                    _stages[stageidx].start = mintime;
                    _stages[stageidx].finish = _pat.pull_finish;
                    Program.VerboseAudit("EL stage add." + " EL start=" + _stages[stageidx].start + " end=" + _stages[stageidx].finish);
                }
                Program.VerboseAudit("EL stage in : " + _stages[stageidx].start);
                Program.VerboseAudit("EL stage out: " + _stages[stageidx].finish);
            }
            else
                Program.VerboseAudit("No evidence of EL stage before: " + Program.g_pull_finish_q4);


        }
        private void CheckStage_AL()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("AL Stage");
            Program.VerboseAudit("---------------");
            bool active_lab;
            //bool active_lab = Exists("", "1020100606", "", "", "Yes");
            //active_lab |= Exists("", "1028000046", "", "", "4,5,6,7,8,9,lip");

            DateTime min_locstartdt;

            if (_pat.stage == 0) //then this is the first class, use first loc time
            {
                min_locstartdt = _pat.locary[1].time_in.AddHours(24);
                if (min_locstartdt > Program.g_pull_finish_q4)
                {
                    Program.VerboseAudit("AL: bailing=too early: min_locstartdt=" + min_locstartdt + " q4=" + Program.g_pull_finish_q4);
                    return;
                }
            }


            var query = StartNewQuery();
            query = query.Where(e => (e.CODE == "12026523") && (e.RESULT.ToInteger() >= 4 || e.RESULT.ToLower() == "lip/rim"));
            //query = query.Where(e => (e.CODE == "1020100606" && e.RESULT.ToLower() == "yes")
            //                         ||
            //   (e.CODE == "1028000046" && ((e.RESULT.Val() >= 4 && e.RESULT.Val() <= 9) || e.RESULT.ToLower().Contains("lip"))));
            active_lab = (query.Count() > 0);

            if (active_lab)
            {
                Program.VerboseAudit("ACTIVE LABOR FOUND:...");
                DateTime mintime = DateTime.MaxValue;
                foreach (var item in query)
                {
                    Program.VerboseAudit("active labor: " + item.CODE + " " + item.DESCRIPTION + " " + item.RESULT + " " + item.EVENT_DATETIME.ToString());
                    if (item.EVENT_DATETIME < mintime)
                    {
                        mintime = item.EVENT_DATETIME;//find the latest time
                    }
                }
                if (stageidx >= 0)
                {
                    if (_stages[stageidx].start <= mintime && _stages[stageidx].start.AddHours(1) >= mintime)
                    {
                        Program.VerboseAudit("AL will overwrite short " + Enum.GetName(typeof(Stages), (int)_stages[stageidx].stage) + " stage." + "  start=" + _stages[stageidx].start);
                        _stages[stageidx].stage = Stages.AL;
                        _stages[stageidx].finish = _pat.pull_finish;

                    }
                    else if (_stages[stageidx].start.AddHours(1) >= mintime)
                    { // overwrite this stage since it's within 1 hr of AL start.
                        Program.VerboseAudit("AL will overwrite " + Enum.GetName(typeof(Stages), (int)_stages[stageidx].stage) + " stage." + "  start=" + _stages[stageidx].start);
                        _stages[stageidx].stage = Stages.AL;
                        _stages[stageidx].start = mintime;
                        _stages[stageidx].finish = _pat.pull_finish;
                    }
                    else
                    {
                        _stages[++stageidx].stage = Stages.AL;
                        _stages[stageidx].start = mintime;
                        _stages[stageidx].finish = _pat.pull_finish;
                        Program.VerboseAudit("AL stage add." + " AL start=" + _stages[stageidx].start + " end=" + _stages[stageidx].finish);
                    }
                }
                else
                {
                    _stages[++stageidx].stage = Stages.AL;
                    _stages[stageidx].start = mintime;
                    _stages[stageidx].finish = _pat.pull_finish;
                    Program.VerboseAudit("AL stage add." + " AL start=" + _stages[stageidx].start + " end=" + _stages[stageidx].finish);
                }
                Program.VerboseAudit("AL stage in : " + _stages[stageidx].start);
                Program.VerboseAudit("AL stage out: " + _stages[stageidx].finish);
            }
            else
                Program.VerboseAudit("No evidence of AL stage before: " + Program.g_pull_finish_q4);

        }
        private void CheckStage_DEL()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("DEL Stage");
            Program.VerboseAudit("---------------");
            bool deliv = false;

            DateTime min_locstartdt;

            if (_pat.stage == 0) //then this is the first class, use first loc time
            {
                min_locstartdt = _pat.locary[1].time_in.AddHours(24);
                if (min_locstartdt > Program.g_pull_finish_q4)
                {
                    Program.VerboseAudit("DEL: bailing=too early: min_locstartdt=" + min_locstartdt + " q4=" + Program.g_pull_finish_q4);
                    return;
                }
            }

            //bool deliv = Exists("", "1028000046", "", "", "10");
            //deliv = Exists("", "1028000055,1028000056,1028000030,9991027100281,9991027100282", "", "", "");
            //2020 - 02 - 16 10:43:00.000 1028000030 10430000 Decision Time Unscheduled C/ S
            //2020 - 02 - 16 10:43:00.000 9991027100281 20200216 Decision Date Unscheduled C/ S
            //2020 - 02 - 16 10:43:00.000 9991027100282 5652780180 Decision Date/ Time Instant

            //2020 - 02 - 17 13:40:00.000 1028000056 13400000 Start Pushing Time
            //2020 - 02 - 17 13:40:00.000 1028000055 20200217 Start Pushing Date
            Program.VerboseAudit("_pat.pull_finish=" + _pat.pull_finish);

            var querycs = StartNewQuery();
            querycs = querycs.Where(e => e.CODE == "1028000030" || e.CODE == "9991027100281" || e.CODE == "9991027100282");
            querycs = querycs.OrderBy(e => e.EVENT_DATETIME);
            delivcs_ct = querycs.Count();
            delivcs = (delivcs_ct > 0);
            if (delivcs)
            {
                delivcs_dt = querycs.First().EVENT_DATETIME;
                Program.VerboseAudit("(3)Found Emergency C/S ct=" + delivcs_ct + " at:" + delivcs_dt);
            }

            var query = StartNewQuery();
            query = query.Where(e => (e.CODE == "12027447"));
            //query = query.Where(e => (e.CODE == "1028000046" && e.RESULT.Val() == 10)
            //                            ||
            //(e.CODE == "1028000055" || e.CODE == "1028000056")
            //                            ||
            //(e.CODE == "9991021600004" || (e.CODE == "9991021628000" && e.RESULT.ToLower().Contains("labor induction"))) //fetal demise
            //                            ||
            //(e.CODE.ToUpper().StartsWith("SCH") && e.DESCRIPTION.ToUpper().Contains("CESAREAN") && !e.DESCRIPTION.ToLower().Contains("in facility"))
            //);
            //query = query.Where(e => !e.DESCRIPTION.ToLower().Contains(";;;schedulingstatus"));
            deliv = (query.Count() > 0);

            DateTime deldt = DateTime.MaxValue;
            if (delivcs)
            {
                string found_what = "";
                DateTime evdt = DateTime.MinValue;
                GetResultAndEVDT("", "1028000030", "", "", out found_what, out evdt, SearchDepth.SearchSinceAdmission);
                if (evdt != DateTime.MinValue)
                    deldt = evdt;
                if (deldt == DateTime.MaxValue)
                {
                    GetResultAndEVDT("", "9991027100281", "", "", out found_what, out evdt, SearchDepth.SearchSinceAdmission);
                    if (evdt != DateTime.MinValue)
                        deldt = evdt;
                }
                if (deldt == DateTime.MaxValue)
                {
                    GetResultAndEVDT("", "9991027100282", "", "", out found_what, out evdt, SearchDepth.SearchSinceAdmission);
                    if (evdt != DateTime.MinValue)
                        deldt = evdt;
                }
            }
            //            bool deliveredCS = (deldt != DateTime.MaxValue);

            //1028000030      Decision Time Unscheduled C/ S
            //9991027100281       Decision Date Unscheduled C/ S
            //9991027100282       Decision Date/ Time Instant

            if (deliv || delivcs)
            {
                Program.VerboseAudit("DELIVERY FOUND:...");
                DateTime mintime = DateTime.MaxValue;
                DateTime evdt;
                if (delivcs)
                    mintime = deldt;
                else if (deliv)
                {
                    foreach (var item in query)
                    {
                        Program.VerboseAudit("delivery: " + item.CODE + " " + item.DESCRIPTION + " " + item.RESULT + " " + item.EVENT_DATETIME.ToString());
                        if (item.EVENT_DATETIME < mintime)
                        {
                            mintime = item.EVENT_DATETIME;
                        }
                        //if (item.CODE == "1028000046")
                        //    evdt = item.EVENT_DATETIME;
                        //else
                        //{//5653711860 = 2/27/20 6:10
                        // //    1028000055,1028000056,1028000030,9991027100281,9991027100282
                        //    evdt = ConvertToDateTime(item.RESULT);
                        //}
                        //if (item.EVENT_DATETIME < mintime)
                        //{
                        //    mintime = item.EVENT_DATETIME;
                        //}
                    }
                }

                _stages[++stageidx].stage = Stages.DEL;
                _stages[stageidx].start = mintime;
                _stages[stageidx].finish = pt_went_thru_recdt;//  _pat.pull_finish;


                DateTime max_time = DateTime.MinValue;
                Stages prev_stage = (Stages)0;

                for (int i = stageidx; i >= 0; i--)
                {
                    Program.VerboseAudit("i=" + i + " stage=" + Enum.GetName(typeof(Stages), (int)_stages[i].stage) + " start=" + _stages[i].start.ToString());
                    if (i < stageidx)
                    {
                        if (_stages[i].stage < prev_stage)
                            if (_stages[i].start >= max_time)
                            {
                                Program.VerboseAudit("..removed");
                                _stages[i].remove = true;
                            }
                            else
                            {
                                Program.VerboseAudit("..adjusting stage finish");
                                _stages[i].finish = _stages[i + 1].start;
                            }
                    }
                    if (!_stages[i].remove)
                    {
                        max_time = _stages[i].start;
                        prev_stage = _stages[i].stage;
                    }

                }

                Program.VerboseAudit("DEL stage in : " + _stages[stageidx].start);
                Program.VerboseAudit("DEL stage out: " + _stages[stageidx].finish);

                //Check if this DEL start time falls into a peri location
                bool found = false;
                for (int i = 1; i <= _pat.num_locs; i++)
                {
                    if (!found)
                    {
                        Program.VerboseAudit(i + ": " + _pat.locary[i].uname + _pat.locary[i].unit_id + " in=" + _pat.locary[i].time_in + " out=" + _pat.locary[i].time_out);
                        if (_pat.locary[i].time_in <= _stages[stageidx].start && _pat.locary[i].time_out > _stages[stageidx].start)
                        {
                            found = true;
                            Program.VerboseAudit("*DEL stage falls into location: " + _pat.locary[i].time_in);
                        }
                    }
                }
                if (!found) // then DEL start does not occur in peri unit.  skip it.
                {
                    stageidx--;
                    Program.VerboseAudit("**DEL stage does not fall into any peri location: omitting DEL stage. stageidx=" + stageidx);
                    //but allow to continue on to next REC stage check.
                }


            }

        }

        private DateTime FindDELstart()
        {
            //bool delivcs = false;
            var querycs = StartNewQuery(SearchDepth.SearchSinceAdmission); //   SearchPullPlus);
            querycs = querycs.Where(e => e.CODE == "1028000030" || e.CODE == "9991027100281" || e.CODE == "9991027100282");
            querycs = querycs.OrderBy(e => e.EVENT_DATETIME);
            delivcs_ct = querycs.Count();
            delivcs = (delivcs_ct > 0);
            if (delivcs)
            {
                delivcs_dt = querycs.First().EVENT_DATETIME;
                Program.VerboseAudit("(4)Found Emergency C/S ct=" + delivcs_ct + " at:" + delivcs_dt);
            }


            bool deliv;
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission); //   SearchPullPlus);
            query = query.Where(e => (e.CODE == "12027447"));

            //query = query.Where(e => (e.CODE == "1028000046" && e.RESULT.Val() == 10)
            //                         ||
            //(e.CODE == "1028000055" || e.CODE == "1028000056")
            //                         ||
            //(e.CODE == "9991021600004" || (e.CODE == "9991021628000" && e.RESULT.ToLower().Contains("labor induction"))) //fetal demise
            //                        ||
            //(e.CODE.ToUpper().StartsWith("SCH") && e.DESCRIPTION.ToUpper().Contains("CESAREAN") && !e.DESCRIPTION.ToLower().Contains("in facility"))
            //);
            //query = query.Where(e => !e.DESCRIPTION.ToLower().Contains(";;;schedulingstatus"));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            deliv = (query.Count() > 0);

            DateTime deldt = DateTime.MaxValue;
            if (delivcs)
            {
                string found_what = "";
                DateTime evdt = DateTime.MinValue;
                GetResultAndEVDT("", "1028000030", "", "", out found_what, out evdt, SearchDepth.SearchSinceAdmission);
                Program.VerboseAudit("evdt1=" + evdt);
                if (evdt != DateTime.MinValue)
                    deldt = evdt;
                if (deldt == DateTime.MaxValue)
                {
                    GetResultAndEVDT("", "9991027100281", "", "", out found_what, out evdt, SearchDepth.SearchSinceAdmission);
                    Program.VerboseAudit("evdt2=" + evdt);
                    if (evdt != DateTime.MinValue)
                        deldt = evdt;
                }
                if (deldt == DateTime.MaxValue)
                {
                    GetResultAndEVDT("", "9991027100282", "", "", out found_what, out evdt, SearchDepth.SearchSinceAdmission);
                    Program.VerboseAudit("evdt3=" + evdt);
                    if (evdt != DateTime.MinValue)
                        deldt = evdt;
                }
                Program.VerboseAudit("deldt==" + deldt);
            }
            //            bool deliveredCS = (deldt != DateTime.MaxValue);


            DateTime mintime = DateTime.MaxValue;
            if (deliv || delivcs)
            {
                Program.VerboseAudit("deliv time:...");
                if (delivcs)
                {
                    mintime = deldt;
                    Program.VerboseAudit(".delivcs=true mintime="+deldt);
                }
                else if (deliv)
                {
                    mintime = query.First().EVENT_DATETIME;
                    Program.VerboseAudit("Delivery*: " + query.First().CODE + " " + query.First().DESCRIPTION + " " + query.First().RESULT + " " + query.First().EVENT_DATETIME.ToString());
                    //foreach (var item in query)
                    //{
                    //    Program.VerboseAudit("delivery: " + item.CODE + " " + item.DESCRIPTION + " " + item.RESULT + " " + item.EVENT_DATETIME.ToString());
                    //    if (item.EVENT_DATETIME < mintime)
                    //    {
                    //        mintime = item.EVENT_DATETIME;
                    //    }
                    //}
                }
            }

            if (mintime == DateTime.MaxValue) mintime = DateTime.MinValue;
            return mintime;
        }
        private void CheckStage_REC()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("REC Stage");
            Program.VerboseAudit("---------------");

            DateTime min_locstartdt;

            if (_pat.stage == 0) //then this is the first class, use first loc time
            {
                min_locstartdt = _pat.locary[1].time_in.AddHours(24);
                if (min_locstartdt > Program.g_pull_finish_q4)
                {
                    Program.VerboseAudit("REC: exiting=too early: min_locstartdt=" + min_locstartdt + " q4=" + Program.g_pull_finish_q4);
                    return;
                }
            }

            DateTime mindeltime = DateTime.MinValue;
            DateTime rectime = DateTime.MinValue;

            mindeltime = FindDELstart();

            //            _stages[++stageidx].stage = Stages.REC;
            bool recov = false;
            if (FundalCheckFreqIsQ15min())
            {
                recov = true;
                Program.VerboseAudit("Recovery based on fundal check q15.");
            }


            if (!recov && (fundaldt == DateTime.MinValue || (fundaldt > DateTime.MinValue && !delivered && !delivcs))) return;

            if (recov)
            {
                Program.VerboseAudit("Recovery found:...");

                //if (_stages[stageidx].stage == Stages.DEL)
                //{
                //    mindeltime = _stages[stageidx].start;
                //}
                //mindeltime = FindDELstart();
                Program.VerboseAudit("...FindDELstart: mindeltime=" + mindeltime);
                bool done = false;


                //string codelist = "12052228,12049243,12025811";
                var queryrec = StartNewQuery(SearchDepth.SearchSinceAdmission); //   SearchPullPlus);
                queryrec = queryrec.Where(e => e.CODE == "12052228" || e.CODE == "12049243" || e.CODE == "12025811");
                queryrec = queryrec.OrderBy(e => e.EVENT_DATETIME);


                CHART_ITEM item;
                item = queryrec.First();
                Program.VerboseAudit("Recovery*: " + item.CODE + "/ " + item.DESCRIPTION + "/ " + item.RESULT + "/ " + item.EVENT_DATETIME.ToString());
                if (!done)
                {
                    if (item.EVENT_DATETIME > mindeltime)
                    {
                        rectime = item.EVENT_DATETIME;
                        Program.VerboseAudit("set rectime=" + rectime);
                    }
                    else
                    {
                        rectime = GetLatestDeliveryTime();
                        Program.VerboseAudit("set rectime=latest Delivery time=" + rectime);
                    }
                    done = true;
                }
            }
            else //fundal
            {
                Program.VerboseAudit("Fundal recovery:..." + fundaldt);
                rectime = fundaldt;
            }
            if (rectime < mindeltime)
            {
                rectime = GetLatestDeliveryTime();
                Program.VerboseAudit("set rectime=latest Delivery time; rectime was found to be less than Min Del time=" + rectime);
            }
            Program.VerboseAudit("rectime was set=" + rectime);

            //Program.VerboseAudit("REC1 stageidx=" + stageidx);
            if (pplocstartdt > DateTime.MinValue)
            {
                if (rectime < pplocstartdt)  //adding this requirement because i'm seeing REC at the pplocstart time
                {
                    _stages[++stageidx].stage = Stages.REC;
                    Program.VerboseAudit("stageidx was advanced to=" + stageidx);
                    _stages[stageidx].start = rectime;

                    if (pt_went_thru_recdt > DateTime.MinValue)
                        _stages[stageidx].finish = pplocstartdt;
                    else
                        _stages[stageidx].finish = _pat.pull_finish;
                    //Program.VerboseAudit("REC2 stageidx=" + stageidx);
                    Program.VerboseAudit("REC stage in : " + _stages[stageidx].start);
                    Program.VerboseAudit("REC stage out: " + _stages[stageidx].finish);
                }
                else
                {
                    Program.VerboseAudit("in checkstage_rec REC NOT being staged: rectime=" + rectime + " pplocstartdt=" + pplocstartdt);
                }
            }
            else
            {
                Program.VerboseAudit("REC being staged when pplocstartdt=minval: rectime=" + rectime + " pplocstartdt=" + pplocstartdt);
                _stages[++stageidx].stage = Stages.REC;
                Program.VerboseAudit("stageidx was advanced to=" + stageidx);
                _stages[stageidx].start = rectime;

                if (pt_went_thru_recdt > DateTime.MinValue)
                    _stages[stageidx].finish = pplocstartdt;
                else
                    _stages[stageidx].finish = _pat.pull_finish;
                //Program.VerboseAudit("REC2 stageidx=" + stageidx);
                Program.VerboseAudit("REC stage in : " + _stages[stageidx].start);
                Program.VerboseAudit("REC stage out: " + _stages[stageidx].finish);

            }
        }

        private bool FundalCheckFreqIsQ15min()
        {
            List<gBucket> buckets;
            bool ret = false;
            string codelist;

            SetBucketSize(15);

            ////Fundal check
            buckets = new List<gBucket>();
            codelist = "12052228,12049243,12025811";
            AddBuckets(buckets, "", codelist, "", "");
            bool isq15 = AnalyzeBuckets(buckets, 9, 15, "Fundal check q15:", false);
            Program.VerboseAudit("Fundal check q15min=" + (isq15 ? "true" : "false"));
            return isq15;
        }
        private bool FundalCheckFreqIsQ1HR()
        {
            List<gBucket> buckets;
            bool ret = false;
            string codelist;

            //SetBucketSize(60);
            //////Fundal check
            //buckets = new List<gBucket>();
            //codelist = "12052228,12049243,12025811";
            //AddBuckets(buckets, "", codelist, "", "");
            //bool isq1hr = AnalyzeBuckets(buckets, 7, 60, "Fundal check q1 hr:", false);
            //Program.VerboseAudit("Fundal check q1Hour=" + (isq1hr ? "true" : "false"));
            //return isq1hr;

            DateTime latest_time = DateTime.MinValue;

            //bool delivcs = false;
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = query.Where(e => e.CODE == "12052228" || e.CODE == "12049243" || e.CODE == "12025811");
            query = query.OrderByDescending(e => e.EVENT_DATETIME);
            if (query.Count() > 0)
            {
                latest_time = query.First().EVENT_DATETIME;
                Program.VerboseAudit("Latest Fundal check time=" + latest_time);
            }

            return latest_time <= Program.g_pull_finish_q4.AddHours(-1);

        }

        private DateTime LatestFundalCheck()
        {
            List<gBucket> buckets;
            bool ret = false;
            string codelist;

            DateTime latest_time = DateTime.MinValue;

            //bool delivcs = false;
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = query.Where(e => e.CODE == "12052228" || e.CODE == "12049243" || e.CODE == "12025811");
            query = query.OrderByDescending(e => e.EVENT_DATETIME);
            if (query.Count() > 0)
            {
                latest_time = query.First().EVENT_DATETIME;
                Program.VerboseAudit("Latest Fundal check time=" + latest_time);
            }

            return latest_time;

        }

        private DateTime GetLatestDeliveryTime()
        {
            DateTime latest_deltime = DateTime.MinValue;

            //bool delivcs = false;
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission); //   SearchPullPlus);
            query = query.Where(e => e.CODE == "12027447");

            query = query.OrderByDescending(e => e.EVENT_DATETIME);
            if (query.Count() > 0)
            {
                latest_deltime = query.First().EVENT_DATETIME;
                Program.VerboseAudit("Latest Delivery=" + latest_deltime);
            }

            return latest_deltime;

        }

        private void CheckStage_PP(bool is_shortcut)
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("PP Stage");
            Program.VerboseAudit("---------------");

            DateTime min_locstartdt;

            if (_pat.stage == 0) //then this is the first class, use first loc time
            {
                min_locstartdt = _pat.locary[1].time_in.AddHours(24);
                if (min_locstartdt > Program.g_pull_finish_q4)
                {
                    Program.VerboseAudit("PP: exiting=too early: min_locstartdt=" + min_locstartdt + " q4=" + Program.g_pull_finish_q4);
                    return;
                }
            }

            Program.VerboseAudit("(PP) C/S eval...");
            var querycs = StartNewQuery(SearchDepth.SearchSinceAdmission);
            querycs = querycs.Where(e => (e.CODE == "1028000030" || e.CODE == "9991027100281" || e.CODE == "9991027100282")
                                       ||
                 (e.CODE.ToUpper().StartsWith("SCH") && e.DESCRIPTION.ToUpper().Contains("CESAREAN") && !e.DESCRIPTION.ToLower().Contains("in facility")));
            querycs = querycs.Where(e => !e.DESCRIPTION.ToLower().Contains(";;;schedulingstatus"));
            querycs = querycs.OrderBy(e => e.EVENT_DATETIME);
            delivcs_ct = querycs.Count();
            delivcs = (delivcs_ct > 0);
            Program.VerboseAudit("(PP) C/S ct=" + delivcs_ct);
            foreach (var ci in querycs)
            {
                Program.VerboseAudit("(PP) C/S:" + ci.CODE+ " desc=" + ci.DESCRIPTION+ " evdt="+ci.EVENT_DATETIME);
            }
            if (delivcs)
            {
                delivcs_dt = querycs.First().EVENT_DATETIME;
                Program.VerboseAudit("(PP)Found C/S ct=" + delivcs_ct + " at:" + delivcs_dt);
            }


            if ((stageidx >= 0 && _stages[stageidx].stage == Stages.ANT) || (_pat.stage == 0))
            {
                if (!delivered && !delivcs && fundaldt > DateTime.MinValue && !FHRpresent())
                {
                    //fundal check without delivery goes direct to PP
                    stageidx = -1;
                    _stages[0].stage = Stages.PP;
                    if (_pat.stage==0) //then this is the first class, use first loc time
                    {
                        pplocstartdt = _pat.locary[1].time_in;
                        _stages[0].unitid = _pat.locary[1].unit_id;
                        _stages[0].uname = _pat.locary[1].uname;
                    }
                    else
                    {

                    }
                }
            }
           

            // 2/24/21: Prevent PP when 1028000004,7,10,16.  Put the unbirthed mom into ANTE.
            bool postp = false;
            bool postp_data = false;
            DateTime mintime = DateTime.MaxValue;
            bool adjusted_to_7am = false; // adjustment to 7am (less than 24 hrs).
            //            _stages[++stageidx].stage = Stages.PP;
            //Program.VerboseAudit("PP1 stageidx=" + stageidx);
            DateTime testtime = _pat.classtime.Date.AddHours(7);
            DateTime nextclasstime = DateTime.MinValue;
            Program.VerboseAudit("pp: _pat.classtime=" + _pat.classtime + " testtime=" + testtime);
            if (_pat.classtime < testtime)
                nextclasstime = testtime;
            else
                nextclasstime = testtime.AddDays(1);

            Program.VerboseAudit("pp: nextclasstime=" + nextclasstime);
            DateTime middt;
            if (CheckIfLocChangeBtwnPrevNextClass(_pat.classtime, nextclasstime, out middt))
            {
                nextclasstime = middt;
            }
            else
            {
                if (PFSUtility.DateDiffInDays(_pat.classtime, nextclasstime) <= 1)
                {
                    nextclasstime = nextclasstime.AddDays(1);
                    adjusted_to_7am = true;
                }
            }
            Program.VerboseAudit("pp: adjusted to 7am=" + adjusted_to_7am.ToString());
            Program.VerboseAudit(" last class time=" + _pat.classtime + " next class time=" + nextclasstime + " g_pull_finish=" + Program.g_pull_finish + " g_pull_finish_q4=" + Program.g_pull_finish_q4);

            Program.VerboseAudit(" pplocstartdt raw=" + pplocstartdt);
            if (pplocstartdt == DateTime.MinValue)
            { //then get the REC complete time for this
                SetRECCompleteTime();

                //if (pplocstartdt > _pat.pull_start)
                //    return; //stop processing. wait to see if PP loc comes.
            }

            if (pplocstartdt == DateTime.MinValue) return;

            bool found = false;
            int ppi;
            for (int i = 1; i <= _pat.num_locs; i++)
            {
                if (!found)
                {
                    Program.VerboseAudit(i + ": " + _pat.locary[i].uname + _pat.locary[i].unit_id + " in=" + _pat.locary[i].time_in + " out=" + _pat.locary[i].time_out);
                    if (_pat.locary[i].time_in <= pplocstartdt && _pat.locary[i].time_out > pplocstartdt)
                    {
                        found = true;
                        Program.VerboseAudit("PP falls into location: " + _pat.locary[i].time_in);
                    }
                }
            }
            if (!found)
            {
                for (int i = 1; i <= _pat.num_locs; i++)
                {
                    if (!found)
                    {
                        Program.VerboseAudit(i + ": " + _pat.locary[i].uname + _pat.locary[i].unit_id + " in=" + _pat.locary[i].time_in + " out=" + _pat.locary[i].time_out);
                        if (_pat.locary[i].time_in > pplocstartdt)
                        {
                            found = true;
                            pplocstartdt = _pat.locary[i].time_in;
                            nextclasstime = _pat.locary[i].time_in;
                            adjusted_to_7am = false;
                            Program.VerboseAudit("*PP falls into location: " + _pat.locary[i].time_in);
                        }
                    }
                }

            }


            //newly added Apr5 to avoid too early PP class acct=2000397537604  
            Program.VerboseAudit(" pplocstartdt.date.add1.add7=" + pplocstartdt.Date.AddDays(1).AddHours(7) + " g_pull_finish_q4=" + Program.g_pull_finish_q4);
            if (Program.g_pull_finish_q4 < pplocstartdt.Date.AddDays(1).AddHours(7))
            {
                Program.VerboseAudit("Waiting until 7am for first PP class at: " + pplocstartdt);
                return;
            }

            if (is_shortcut)
            {
                Program.VerboseAudit("is_shortcut=true");
                if (_pat.stage != 6) // then this is the first PP class
                {
                    Program.VerboseAudit("first pp=" + pplocstartdt);
                    _stages[++stageidx].stage = Stages.PP;
                    _stages[stageidx].start = pplocstartdt;
                    _stages[stageidx].finish = pplocstartdt.Date.AddDays(1).AddHours(7);
                    Program.VerboseAudit("+PP stage in : " + _stages[stageidx].start);
                    Program.VerboseAudit("+PP stage out: " + _stages[stageidx].finish);
                    Program._pat.stage = _pat.stage;
                    Program._pat.lastclass_start = pplocstartdt;
                }
                //else if (pplocenddt < Program.g_pull_finish)
                //{
                //    Program.VerboseAudit("pplocenddt TimeOfDay=" + pplocenddt.TimeOfDay.ToString() + " pplocenddt Date=" + pplocenddt.Date.ToString());
                //    if (pplocenddt.TimeOfDay < Program.g_pull_finish.TimeOfDay)
                //        pplocstartdt = pplocenddt.Date.AddDays(-1);
                //    else
                //        pplocstartdt = pplocenddt.Date.AddHours(7);
                //    _stages[++stageidx].stage = Stages.PP;
                //    _stages[stageidx].start = pplocstartdt;
                //    _stages[stageidx].finish = pplocenddt;
                //    Program.VerboseAudit("+PP stage in : " + _stages[stageidx].start);
                //    Program.VerboseAudit("+PP stage out: " + _stages[stageidx].finish);
                //}
                else if (nextclasstime <= Program.g_pull_finish)
                {
                    Program.VerboseAudit("pp shortcut: pull date 7am=" + Program.g_pull_finish);
                    _stages[++stageidx].stage = Stages.PP;
                    if (adjusted_to_7am)
                    {
                        _stages[stageidx].start = nextclasstime.AddDays(-1); //Mar9 use the prev day as the stage start
                        _stages[stageidx].finish = nextclasstime;
                    }
                    else
                    {
                        _stages[stageidx].start = nextclasstime; // _pat.classtime; // Program.g_pull_finish;
                        _stages[stageidx].finish = Program.g_pull_finish;
                    }
                    Program.VerboseAudit("-PP stage in : " + _stages[stageidx].start);
                    Program.VerboseAudit("-PP stage out: " + _stages[stageidx].finish);
                    Program._pat.lastclass_start = Program.g_pull_start;
                }
                else
                {
                    Program.VerboseAudit("Waiting 24 hrs since last PP class at: " + _pat.classtime);
                }
                return;
            } //is shortcut

            if (stageidx > -1)
            {
                if (_stages[stageidx].stage == Stages.REC)
                { //look for location change, end REC there, start PP there
                    if (pplocstartdt != DateTime.MinValue)
                    {
                        Program.VerboseAudit("Postpartum location found: PP start=" + pplocstartdt + " REC start=" + _stages[stageidx].start);
                        postp = true;
                    }
                }
            }

            //if (!postp)
            //{
            //    var query = StartNewQuery();
            //    query = query.Where(e => (e.CODE == "9991025122718" && e.RESULT.ToLower() == "complete"));
            //    postp_data = (query.Count() > 0);
            //}
            if (!postp) postp_data = (pplocstartdt > DateTime.MinValue);

            if (!postp && !postp_data) return;

            if (postp)
            {
                mintime = pplocstartdt;
            }
            else if (postp_data)
            {
                //var query1 = StartNewQuery();
                //query1 = query1.Where(e => (e.CODE == "9991025122718" && e.RESULT.ToLower() == "complete"));
                //query1 = query1.OrderByDescending(e => e.EVENT_DATETIME);
                //int ct1 = query1.Count();
                if (pplocstartdt > DateTime.MinValue)
                {
                    Program.VerboseAudit("REC complete found = PP start established:...");
                    //bool first = true;
                    //foreach (var item in query1)
                    //{
                    //    if (first)
                    //    {
                    //        first = false;
                    //        Program.VerboseAudit("pp: " + item.CODE + " " + item.DESCRIPTION + " " + item.RESULT + " " + item.EVENT_DATETIME.ToString());
                    //        if (item.EVENT_DATETIME < mintime)
                    //        {
                    //            mintime = item.EVENT_DATETIME;
                    //        }
                    //    }
                    //}
                    mintime = pplocstartdt;
                }
            }
            Program.VerboseAudit("classified meth:..." + _pat.classified_methid.ToString());
            _stages[++stageidx].stage = Stages.PP;
            _stages[stageidx].start = mintime;
            _stages[stageidx].finish = _pat.pull_finish;
            //if (_pat.classified_methid != 27)
            //{
            //    _stages[++stageidx].stage = Stages.PP;
            //    _stages[stageidx].start = mintime;
            //    _stages[stageidx].finish = _pat.pull_finish;
            //}
            //else
            //{
            //    _stages[++stageidx].stage = Stages.PP;
            //    _stages[stageidx].start = Program.g_pull_finish;
            //    _stages[stageidx].finish = DateTime.MaxValue;

            //}
            Program.VerboseAudit("PP stage in : " + _stages[stageidx].start);
            Program.VerboseAudit("PP stage out: " + _stages[stageidx].finish);

            Program.VerboseAudit("stageidx=" + stageidx + " start: " + _stages[stageidx].start + " g_pull_finish" + Program.g_pull_finish);
            if (_pat.classified_methid == 0) //then first time PP
            {
                Program.VerboseAudit("set first time _pat.stage=PP");
                _pat.stage = (int)Stages.PP;
            }
            else
            {
                if (_stages[stageidx].start.AddHours(24) >= Program.g_pull_finish_q4)
                {
                    stageidx--;
                    Program.VerboseAudit("stageidx--=" + stageidx);
                }
                else
                {
                    Program.VerboseAudit("set _pat.stage=PP");
                    _pat.stage = (int)Stages.PP;
                }

            }
        }

        private bool CheckIfLocChangeBtwnPrevNextClass(DateTime prevdt, DateTime nextdt, out DateTime middt)
        {
            middt = DateTime.MinValue;

            Program.VerboseAudit("CheckIfLocChange: prevdt=" + prevdt + " nextdt="+nextdt);
            bool found = false;
            for (int i = 1; i <= _pat.num_locs; i++)
            {
                if (!found)
                {
                    Program.VerboseAudit(i + ": " + _pat.locary[i].uname + _pat.locary[i].unit_id + " in=" + _pat.locary[i].time_in + " out=" + _pat.locary[i].time_out);
                    if (_pat.locary[i].time_in > prevdt && _pat.locary[i].time_in <= nextdt)
                    {
                        found = true;
                        middt = _pat.locary[i].time_in;
                    }
                }
            }
            return found;
        }
        private void SetRECCompleteTime()
        {
            DateTime afundaldt;

            Program.VerboseAudit("Finding Recovery Complete Time...");
            var query1 = StartNewQuery();
            query1 = query1.Where(e => (e.CODE == "9991025122718" && e.RESULT.ToLower() == "complete"));
            query1 = query1.OrderByDescending(e => e.EVENT_DATETIME);
            int ct1 = query1.Count();
            if (ct1 > 0)
            {
                pplocstartdt = query1.First().EVENT_DATETIME;
                Program.VerboseAudit("Set Recover Complete Time=PP start dt=" + pplocstartdt);
            }
            else
            {
                var query2 = StartNewQuery();
                query2 = query2.Where(e => (e.CODE == "9991025122718" && e.RESULT.ToLower() == "start"));
                query2 = query2.OrderBy(e => e.EVENT_DATETIME);
                int ct2 = query2.Count();
                if (ct2 > 0)
                {
                    pplocstartdt = query2.First().EVENT_DATETIME.AddHours(3);
                    Program.VerboseAudit("Auto-Complete Recover Time set to 3 hrs+start=" + pplocstartdt);
                }
                else
                {
                    var query = StartNewQuery();
                    query = query.Where(e => e.CODE == "9990000012127" || e.CODE == "9990000012128" || e.CODE == "9990000012129");
                    query = query.OrderBy(e => e.EVENT_DATETIME);
                    bool fundal = (query.Count() > 0);
                    Program.VerboseAudit("SetRECComplete: fundal=" + (fundal ? "true" : "false"));

                    if (fundal)
                    {
                        afundaldt = query.First().EVENT_DATETIME;
                        if (!delivered && !delivcs)
                            pplocstartdt = afundaldt;
                        else
                            pplocstartdt = afundaldt.AddHours(3);
                    }


                }

            }
        }
        private void CheckStage_IC()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("IC Stage");
            Program.VerboseAudit("---------------");

            DateTime min_ic_locstartdt;

            if (_pat.stage == 0) //then this is the first class, use first loc time
            {
                min_ic_locstartdt = _pat.locary[1].time_in.AddHours(24);
                if (min_ic_locstartdt > Program.g_pull_finish_q4)
                {
                    Program.VerboseAudit("IC: bailing=too early: min_ic_locstartdt=" + min_ic_locstartdt + " q4=" + Program.g_pull_finish_q4);
                    return;
                }
            }


            if (_pat.classtime > DateTime.MinValue)
            {
                CheckIfAdvanceToNBN(DateTime.MinValue);
                if (advance_to_NBN) return;
            }

            _stages[++stageidx].stage = Stages.IC;

            string found_what = "";
            bool keep_in_IC = false;
            string reslist = "";
            if (Exists("", "3045001117,9993040000635,3045001128", "", "", reslist, SearchDepth.SearchPullPlus,false, out found_what))
            {
                keep_in_IC = true;
                Program.VerboseAudit("Patient intubated.  Stage will remain in IC until transfer out.");
            }

            Program.VerboseAudit("_pat.pull_start=" + _pat.pull_start + " _pat.pull_finish=" + _pat.pull_finish);
            if (_pat.pull_start.AddHours(1) >= _pat.pull_finish || keep_in_IC)
            {
                _stages[stageidx].start = _pat.pull_start;
                _stages[stageidx].finish = _pat.pull_finish;
                end_stage_analysis = true;
                Program.VerboseAudit("_pat.pull_start.AddHours(1) >= _pat.pull_finish || keep_in_IC=" + keep_in_IC);
            }
            else
            {
                _stages[stageidx].start = _pat.pull_start;
                _stages[stageidx].finish = _pat.pull_start.AddHours(1);
            }
            Program.VerboseAudit("IC stage in : " + _stages[stageidx].start);
            Program.VerboseAudit("IC stage out: " + _stages[stageidx].finish);

            Program.VerboseAudit("checkstage_ic: check if adv to nbn: _pat.pull_start=" + _pat.pull_start + " _pat.pull_finish=" + _pat.pull_finish);
            CheckIfAdvanceToNBN(_pat.pull_start);
            Program.VerboseAudit("advance_to_NBN=" + advance_to_NBN);

        }

        private void CheckIfAdvanceToNBN(DateTime nextclasstime)
        {
            int targetidx = 0;
            DateTime keeptimein = DateTime.MinValue;
            bool last_loc = false;

            DateTime basetime;
            if (nextclasstime < DateTime.MinValue.AddHours(24))
                basetime = nextclasstime;
            else
                basetime = nextclasstime.AddHours(-24);

            Program.VerboseAudit("Check if Advance To NBN...with nextclasstime=" + nextclasstime);
            for (int i = _pat.num_locs; i >= 1; i--)
            {
                Program.VerboseAudit(i + ":locary[i].uname=" + _pat.locary[i].uname +
                    " in: " + _pat.locary[i].time_in + " out=" + _pat.locary[i].time_out + " _pat.pull_finish=" + _pat.pull_finish);
                if (_pat.pull_finish >= _pat.locary[i].time_in
                    && _pat.pull_finish <= _pat.locary[i].time_out)
                {
                    Program.VerboseAudit("i=" + i);
                    keeptimein = _pat.locary[i].time_in;
                    targetidx = i - 1;
                }
                //else if (_pat.pull_finish == _pat.locary[i].time_out)
                //{
                //    Program.VerboseAudit(".i=" + i);
                //    keeptimein = _pat.locary[i].time_in;
                //    targetidx = i;
                //    last_loc = true;
                //}
                if (i == targetidx)
                {
                    Program.VerboseAudit("i==targetidx" + i);
                    if ((keeptimein > _pat.locary[i].time_out || last_loc) && keeptimein >= basetime)
                    {
                        advance_to_NBN = true;
                        advance_to_NBN_time = keeptimein;
                        Program.VerboseAudit("will advance to NBN due to non-perinatal unit...Advance to NBN at time=" + advance_to_NBN_time);
                    }
                }
            }

            Program.VerboseAudit("CheckifAdvance to NBN keeptime=" + keeptimein + " advance_to_NBN_time=" + advance_to_NBN_time);
            //if (!advance_to_NBN && keeptimein > _pat.classtime && nextclasstime > DateTime.MinValue)
            //{
            //    advance_to_NBN = true;
            //    advance_to_NBN_time = keeptimein;
            //    Program.VerboseAudit("will advance to NBN due to non-classed unit...Advance to NBN at time=" + advance_to_NBN_time);
            //}

        }
        private void CheckStage_TC()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("TC Stage");
            Program.VerboseAudit("---------------");
            //            _stages[++stageidx].stage = Stages.TC;
            DateTime min_tc_locstartdt;

            if (_pat.stage == 0) //then this is the first class, use first loc time
            {
                min_tc_locstartdt = _pat.locary[1].time_in.AddHours(24);
                if (min_tc_locstartdt > Program.g_pull_finish_q4)
                {
                    Program.VerboseAudit("TC: bailing=too early: min_tc_locstartdt=" + min_tc_locstartdt + " q4=" + Program.g_pull_finish_q4);
                    return;
                }
            }


            if (_pat.classtime > DateTime.MinValue)
            {
                CheckIfAdvanceToNBN(_pat.classtime);
                if (advance_to_NBN) return;
            }

            _stages[++stageidx].stage = Stages.TC;
            if (_pat.pull_start.AddHours(4) >= _pat.pull_finish)
            {
                _stages[stageidx].start = _pat.pull_start.AddHours(1);
                _stages[stageidx].finish = _pat.pull_finish;
                end_stage_analysis = true;
            }
            else
            {
                _stages[stageidx].start = _pat.pull_start.AddHours(1);
                _stages[stageidx].finish = _pat.pull_start.AddHours(4);
            }
            Program.VerboseAudit("TC stage in : " + _stages[stageidx].start);
            Program.VerboseAudit("TC stage out: " + _stages[stageidx].finish);

        }
        private void CheckStage_NBN(bool is_shortcut)
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("NBN Stage");
            Program.VerboseAudit("---------------");

            DateTime min_nbn_locstartdt;

            if (_pat.stage == 0) //then this is the first class, use first loc time
            {
                min_nbn_locstartdt = _pat.locary[1].time_in.AddHours(24);
                if (min_nbn_locstartdt > Program.g_pull_finish_q4)
                {
                    Program.VerboseAudit("NBN: bailing=too early: min_nbn_locstartdt=" + min_nbn_locstartdt + " q4=" + Program.g_pull_finish_q4);
                    return;
                }
            }

            //Get Circumcision time
            //            private bool circ_done = false;
            //private DateTime circ_time = DateTime.MinValue;
            if (GetEVDTWhereResultContains("", "9990000026972", "", "", "Yes,1", out circ_time,SearchDepth.SearchSinceAdmission))
            {
                Program.VerboseAudit("circ time=" + circ_time);
                circ_done = true;
            }

            DateTime testtime = _pat.classtime.Date.AddHours(7);
            DateTime nextclasstime = DateTime.MinValue;
            bool adjusted_to_7am = false;
            if (_pat.classtime < testtime)
                nextclasstime = testtime;
            else
                nextclasstime = testtime.AddDays(1);
            if (PFSUtility.DateDiffInDays(_pat.classtime, nextclasstime) <= 1)
            {
                nextclasstime = nextclasstime.AddDays(1);
                adjusted_to_7am = true;
            }
            Program.VerboseAudit(" last class time=" + _pat.classtime + " next class time=" + nextclasstime + " g_pull_finish=" + Program.g_pull_finish + " g_pull_finish_q4=" + Program.g_pull_finish_q4);

            CheckIfAdvanceToNBN(nextclasstime);

            if (advance_to_NBN)
            {
                _stages[++stageidx].stage = Stages.NBN;
                _stages[stageidx].start = advance_to_NBN_time;
                //_stages[stageidx].finish = _pat.pull_finish;
                _stages[stageidx].finish = _stages[stageidx].start.Date.AddDays(1).AddHours(7); ;
                _stages[stageidx].NBN_was_split = true;
                Program.VerboseAudit("advNBN stage in : " + _stages[stageidx].start);
                Program.VerboseAudit("advNBN stage out: " + _stages[stageidx].finish);
            }
            else if (is_shortcut) //detected NBN=9 already from ce.stage
            {
                if (nextclasstime == Program.g_pull_finish)
                {
                    Program.VerboseAudit("nbn shortcut: pull date 7am=" + Program.g_pull_finish);
                    _stages[++stageidx].stage = Stages.NBN;
                    if (adjusted_to_7am)
                    {
                        _stages[stageidx].start = nextclasstime.AddDays(-1);//Mar9
                        _stages[stageidx].finish = nextclasstime;
                    }
                    else
                    {
                        _stages[stageidx].start = nextclasstime; // _pat.classtime; // Program.g_pull_finish;
                        //_stages[stageidx].finish = Program.g_pull_finish;
                        _stages[stageidx].finish = _stages[stageidx].start.Date.AddDays(1).AddHours(7); ;
                    }
                    Program.VerboseAudit("+nbn stage in : " + _stages[stageidx].start);
                    Program.VerboseAudit("+nbn stage out: " + _stages[stageidx].finish);
                    Program._pat.lastclass_start = Program.g_pull_start;
                }
                else
                {
                    Program.VerboseAudit("Waiting 24 hrs since last NBN class at: " + _pat.classtime);
                }
            }
            else
            {
                //            _stages[++stageidx].stage = Stages.NBN;
                _stages[++stageidx].stage = Stages.NBN;
                _stages[stageidx].start = _pat.pull_start.AddHours(4);
                //_stages[stageidx].finish = _pat.pull_finish;
                _stages[stageidx].finish = _stages[stageidx].start.Date.AddDays(1).AddHours(7);
                Program.VerboseAudit("NBN stage in : " + _stages[stageidx].start);
                Program.VerboseAudit("NBN stage out: " + _stages[stageidx].finish);
            }
            _pat.stage = 9;//crucial to set this for next iteration
            Program._pat.stage = _pat.stage;
            Program._pat.lastclass_start = pplocstartdt;
            return;
        }

        private void TurnOnStageQuery(int s)
        {
            _use_stage_query = true;
            _currstage = _stages[s].stage;
            Program.VerboseAudit("TurnOnStageQ: " + Enum.GetName(typeof(Stages), _currstage) + " start=" + _stages[s].start + " end=" + _stages[s].finish);

        }
        private int GetCurrStageIdx()
        {
            int idx = 0;
            Program.VerboseAudit("GetCurrStageIdx target stage: " + Enum.GetName(typeof(Stages), _currstage));
            for (int i = 0; i <= stageidx; i++)
            {
                if (_stages[i].stage == _currstage)
                {
                    idx = i;
                    Program.VerboseAudit("GetCurrStageIdx: idx=" + idx + " start=" + _stages[idx].start + " end=" + _stages[idx].finish);
                }
            }
            Program.VerboseAudit("GetCurrStageIdx: Return=" + idx);
            return idx;
        }

        private void Check_1_2(int s)
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P2 1. ADL - Partial Care");
            Program.VerboseAudit("P2 2. ADL - Complete Care");
            Program.VerboseAudit("---------------");

            _use_stage_query = false;
            bool ok = (_stages[s].stage == Stages.ANT ||
                        _stages[s].stage == Stages.EL ||
                        _stages[s].stage == Stages.AL ||
                        _stages[s].stage == Stages.PP);
            if (!ok) return;

            if (_stages[s].stage == Stages.PP) TurnOnStageQuery(s);

            string reslist;
            reslist = "Ambulate in hall,Ambulate in room,Bathroom privileges,Bedpan,Bedrest,Bike,Birthing ball,Chair,Commode,Dangle,Held,In bed,Stand at bedside,Supervised exercise,Tilt table,Turn,Up in chair,Wagon,Wheelchair";
            bool A1 = Exists("", "9990000305560", "", "", reslist);
            reslist = "Lying right side,Lying left side,Turn right,Turn left,Supine,Semi Fowler,Pillow support,Prone,Sitting,Standing,Up in chair,Micro turn left,Micro turn right,Do Not Turn,Unstable to turn";
            bool A3 = Exists("", "9990000400604", "", "", reslist);
            reslist = "Left side,Left tilt,Right side,Right tilt,Semi-fowler,Fowler,Knee chest,Chair,Rocking chair,Supine,Trendelenberg,Squatting,Standing,Kneeling,Walking,Prone";
            bool A4 = Exists("", "1028000027", "", "", reslist);
            if (A1 || A3 || A4)
            {
                reslist = "1 Assist,2 Assist,3 Assist,>4 Assist,4 or more Assist,Stand by Assist,Supervision required";
                SetIndIfResultContains(1, "", "9993040109530", "", "", reslist);
            }
            reslist = "Assist";
            SetIndIfResultContains(1, "", "9991025006475", "", "", reslist);

            reslist = "1 Assist,2 Assist,3 Assist,>4 Assist,Supervised";
            bool A6 = Exists("", "9990007060350", "", "", reslist);
            reslist = "Partial assist,Complete assist";
            bool A8 = Exists("", "9990000305650", "", "", reslist);
            if (A6 || A8)
            {
                reslist = "Chlorhexidine,Bathed,Showered,Bath in a bag,Catheter care,Peri care,Hair washed,Hair dryed/curled,Shaved";
                SetIndIfResultContains(1, "", "9990000342030", "", "", reslist);
            }

            reslist = "Needs assist,Supervised,Total assist ";
            SetIndIfResultContains(1, "", "9990000700380", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(1, "", "3043040100003", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(1, "", "9993040100004", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(1, "", "3043040101422", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(1, "", "9993040101423", "", "", reslist);

            reslist = "";
            bool ADLLDA10 = SetIndIfResultContains(1, "", "3045001094", "", "", reslist);
            reslist = "NG tube,OG tube,G-tube,J-tube,ND tube,OD tube,PEG-tube";
            bool ADLLDA11 = SetIndIfResultContains(1, "", "9991733565652", "", "", reslist);
            bool ADLLDA12 = SetIndIfResultContains(1, "", "9991733569855", "", "", "");
            bool ADLLDA13 = SetIndIfResultContains(1, "", "9993040304520", "", "", "");


            reslist = "Yes,1";
            SetIndIfResultContains(1, "", EXACT_MATCH_PREFIX + "23", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(1, "", "3045001090", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(1, "", "3045001089", "", "", reslist);
            reslist = "Yes,1";
            SetIndIfResultContains(1, "", EXACT_MATCH_PREFIX + "16", "", "", reslist);

            reslist = "";
            bool ADLLDA1 = SetIndIfResultContains(1, "", "3045001088", "", "", reslist);
            bool ADLLDA2 = SetIndIfResultContains(1, "", "9990007085310", "", "", reslist);
            bool ADLLDA3 = SetIndIfResultContains(1, "", "9993040104906", "", "", reslist);
            bool ADLLDA4 = SetIndIfResultContains(1, "", "9990000396150", "", "", reslist);
            bool ADLLDA5 = SetIndIfResultContains(1, "", "9990007085490", "", "", reslist);
            bool ADLLDA6 = SetIndIfResultContains(1, "", "9993040021276", "", "", reslist);


            SetIndIfResultBetween(1, "", "3045001023", "", "", 9, 12, SearchDepth.SearchDefault);
            SetIndIfResultBetween(1, "", "9993040001207", "", "", 9, 12, SearchDepth.SearchDefault);

        }


        private void Check_3(int s)
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P2 3. ADL Developmental");
            Program.VerboseAudit("---------------");
            string reslist;
            _use_stage_query = false;

//  Yes.A score of 0 or 1 on the hold positioning row more than 2x in the NBN classification period. Would only apply to each specific NBN classification period and not carry over to other NBN classifications.
//  I.e.:  it could apply on the first NBN classification from 1900 - 0700, but not on the following classifications for that baby that are from 0700 - 0700.For some babies it may not occur, others only on the first NBN classification, and others could apply to a couple classifications.
//  I would only expect the type to increase if in combination with other indicators.

            bool ok = (_stages[s].stage == Stages.NBN);
            if (!ok) return;
            TurnOnStageQuery(s);

            SetIndIfResultBetween(3, "", "9990000336025", "", "", 0, 6, SearchDepth.SearchDefault);

            string found_what = "";
            int ct=CountResultInList("", "9990000336024", "", "", "0,1", SearchDepth.SearchDefault, CountMode.CountAll, true, out found_what);
            //9990000336024  check if this is Holding
            if (ct >= 2) SetInd(3, "Found Holding count=" + ct + "; " + found_what);

            reslist = "Feeding cup,Medicine dropper,Spoon,Supplemental nursing system (SNS)";
            SetIndIfResultContains(3, "", "9991020100583", "", "", reslist);
            reslist = "Cup feeding,Finger feeding,Spoon feeding,Syringe feeding";
            SetIndIfResultContains(3, "", "9991025073970", "", "", reslist);
            reslist = "Bolus per gravity,Bolus per pump,Continous pump,Not applicable";
            SetIndIfResultContains(3, "", "9990007074010", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(3, "", "3045001094", "", "", reslist);
            reslist = "NG tube,OG tube,G-tube,J-tube,ND tube,OD tube,PEG-tube,Cup feeding,Finger feeding";
            SetIndIfResultContains(3, "", "9991733565652", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(3, "", "9991733569855", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(3, "", "9993040304520", "", "", reslist);

            SetIndIfResultContains(3, "", "999304004008", "", "", "Yes,1");

            reslist = "";
            SetIndIfResultContains(3, "", "9990000344170", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(3, "", "3040007191", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(3, "", "ORD06", "", "", reslist);
            reslist = "Given,Given/Down,Given/Other,Syringe,Rate Verify or RateVerify,Rate Change,RateChange,New Bag,Restarted,Continued,Stopped";
            SetIndIfResultContains(3, "", "MED", ";;;33;;;", "", reslist);

        }
        private bool Check_b79()
        {
            bool b = false;

            var query = StartNewQuery();
            query = query.Where(e => e.CODE.StartsWith("MED"));
            query = query.Where(e => e.DESCRIPTION.ToUpper().Contains("PEG3350 100 GRAM-SOD SUL") && e.DESCRIPTION.ToLower().Contains(";;;given"));
            b = (query.Count() > 0);
            return b;
        }

        private void CheckOrient3()
        {
            SearchDepth search_depth = SearchDepth.SearchDefault;
            bool do_not_set = false;
            //oriented to person, oriented to place, oriented to time
            bool o1 = ResultContains("", "9990000301870", "", "", "oriented to person");
            bool o2 = ResultContains("", "9990000301870", "", "", "oriented to place");
            bool o3 = ResultContains("", "9990000301870", "", "", "oriented to time");
            int oTot = (o1 ? 1 : 0) + (o2 ? 1 : 0) + (o3 ? 1 : 0);
            if ((oTot > 0) && (oTot < 3))
            {
                SetInd(3, "Oriented to at least one but less than three aspects.");
            }
            if (oTot != 3) return; // otherwise see if the 3 times are the same....

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var q1 = StartNewQuery(search_depth);
            q1 = AndItemFilter(q1, "", "9990000301870", "", "", "oriented to person");
            foreach (var item in q1)
            {
                var q2 = StartNewQuery(search_depth);
                q2 = AndItemFilter(q2, "", "9990000301870", "", "", "oriented to place");
                q2 = q2.Where(e2 => e2.EVENT_DATETIME == item.EVENT_DATETIME);
                var ct2 = q2.Count();
                if (ct2 > 0)
                {
                    var q3 = StartNewQuery(search_depth);
                    q3 = AndItemFilter(q3, "", "9990000301870", "", "", "oriented to time");
                    q3 = q3.Where(e3 => e3.EVENT_DATETIME == item.EVENT_DATETIME);
                    var ct3 = q3.Count();
                    if (ct3 > 0)
                    {
                        do_not_set = true;
                    }

                }
            }
            if (!do_not_set) SetInd(3, "Not oriented to all three aspects at the same time.");
        }

        private void Check_4(int s)
        {
            string reslist;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P2 4. Extensive Coordination of Care");
            Program.VerboseAudit("---------------");

            _use_stage_query = false;

            bool ok = (_stages[s].stage == Stages.REC ||
                        _stages[s].stage == Stages.PP ||
                        _stages[s].stage == Stages.IC ||
                        _stages[s].stage == Stages.TC);
            if (!ok) return;

            bool non_labor = (_stages[s].stage == Stages.REC ||
            _stages[s].stage == Stages.PP ||
            _stages[s].stage == Stages.TC);

            if (non_labor) TurnOnStageQuery(s);

            bool a = Exists("", "9993047060440", "", "", "Care conference");
            if (a)
                SetIndIfResultContains(4, "", "9993040004578", "", "", "60,75,90,120,180");

            SetIndIfResultContains(4, "", "9990007096529", "", "", "Transport team");
        }

        private void Check_5(int s)
        {
            string reslist;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P2 5. Communication Support");
            Program.VerboseAudit("---------------");
            _use_stage_query = false;

            bool ok = (_stages[s].stage == Stages.ANT ||
                        _stages[s].stage == Stages.EL ||
                        _stages[s].stage == Stages.AL ||
                        _stages[s].stage == Stages.DEL ||
                        _stages[s].stage == Stages.REC ||
                        _stages[s].stage == Stages.PP ||
                        _stages[s].stage == Stages.NBN);
            if (!ok) return;

            //TurnOnStageQuery(s);


            reslist = "Artificial airway,Attempts to verbalize,Delayed responses,Dysphasia,";
            reslist += "Expressive aphasia,Garbled,Global aphasia,Incomprehensible,Nods/gestures appropriately,";
            reslist += "Non-verbal,Receptive aphasia,Slurred,Uses communication aid(s)";
            SetIndIfResultContains(5, "", "9990000301890", "", "", reslist);


            reslist = "Impaired vision- not corrected,Blind";
            if (Exists("", "9990000002106", "", "", reslist))
                SetIndIfResultContains(5, "", "9990000002107", "", "", reslist);

            reslist = "Impaired hearing- not corrected,Acute hearing loss,Deaf";
            if (Exists("", "9990000002108", "", "", reslist))
                SetIndIfResultContains(5, "", "9990000002109", "", "", reslist);

            reslist = "Difficulty talking,Trach,Hoarse,Muffled,Speaking valve,Voice amplifier";
            SetIndIfResultContains(5, "", "9990000002115", "", "", reslist);

            SetIndIfResultContains(5, "", "9990304000117", "", "", "3");

            SetIndIfResultContains(5, "", "3045001024", "", "", "Inappropriate words,3");
            reslist = "Delayed,Impoverished,Mumbled,Mute,Overproductive,Pressured,Rambling,Rapid,Slurred,Stutter";
            SetIndIfResultContains(5, "", "9993040105630", "", "", reslist);

            SetIndIfResultContains(5, "", "9990007090150", "", "", "1,2,3");
            SetIndIfResultContains(5, "", "9990007090160", "", "", "1,2");
            SetIndIfResultContains(5, "", "9990007090070", "", "", "2,3");



            SetIndIfResultContains(5, "", "9993040001091", "", "", "Blindness");
            SetIndIfResultContains(5, "", "9990000002221", "", "", "Unable to hear");
            reslist = "";
            SetIndIfResultContains(5, "", "9991733444441", "", "", reslist);
            //reslist = "Admission,Assessment,Consent,Discharge instructions,Education,Plan of care,Other";
            SetIndIfResultContains(5, "", "9993040109069", "", "", reslist);
            reslist = "Hospital/clinic approved on site interpreter,Telephone interpreter,Video remote interpreter,Other";
            SetIndIfResultContains(5, "", "9993040109071", "", "", reslist);

            SetIndIfResultContains(5, "", "9993040108551", "", "", "");
            reslist = "Braille,Communication board,Hearing aid,Interpreter,Sign language";
            SetIndIfResultContains(5, "", "9990007070581", "", "", reslist);

            SetIndIfResultContains(5, "", "9993040108693", "", "", "Yes,1");

            //            reslist = "Mask,Tubing,CPAP,BPAP,Patient able to manage equipment on own,Equipment inspected (per site policy),Waiver signed (per site policy),Other";
            //bool F23 = Exists("", "9993040000639", "", "", reslist);
            //if (!F23)
            //{
            //    reslist = "Bilevel,CPAP,Auto-Bilevel,Auto-CPAP,AVAPS,PCV";
            //    SetIndIfResultContains(5, "", "3045001108", "", "", reslist);
            //    reslist = "Full face mask,Performax,Total face mask,Endotracheal,Tracheostomy";
            //    SetIndIfResultContains(5, "", "9993040000637", "", "", reslist);


            SetIndIfResultContains(5, "", "9993040108584", "", "", "Yes,1");
            SetIndIfResultContains(5, "", "9993040108585", "", "", "Yes,1");
            SetIndIfResultContains(5, "", "9990007081420", "", "", "No");
            SetIndIfResultContains(5, "", "9990007085050", "", "", "Deaf");
            SetIndIfResultContains(5, "", "9990007085060", "", "", "Deaf");
            SetIndIfResultContains(5, "", "9991150000153", "", "", "Slow and slurred");

        }


        private void Check_678910(int s)
        {
            int ct;
            string codelist;
            string reslist;
            bool is_peds = false;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P2 6. Assessment q2 hr");
            Program.VerboseAudit("P2 7. Assessment q1 hr");
            Program.VerboseAudit("P2 8. Assessment q30 min");
            Program.VerboseAudit("P2 9. Assessment q15 min");
            Program.VerboseAudit("P2 10. Assessment Continuous");
            Program.VerboseAudit("---------------");

            bool ok = (_stages[s].stage == Stages.ANT ||
                        _stages[s].stage == Stages.EL ||
                        _stages[s].stage == Stages.AL ||
                        _stages[s].stage == Stages.PP ||
                        _stages[s].stage == Stages.NBN ||
                        _stages[s].stage == Stages.TC);
            if (!ok) return;
            //for TC, look back to beginning of IC in the IC-TC connected transition 
            //(not for a lone TC, if that could even occur).
            //SetIndIfResultContains(6, "", "NUR185", "glucose POC,poc glucose,dextrose gel", "", "",SearchDepth.SearchPullPlus);
            DateTime glucose_time = DateTime.MinValue;
            if (GetEVDTWhereResultContains("", "NUR185", "glucose POC,poc glucose,dextrose gel", "", "", out glucose_time, SearchDepth.SearchSinceAdmission))
                if (glucose_time.AddHours(24) >= _stages[s].start)
                    SetInd(6, "Glucose or dextrose at time=" + glucose_time);

            TurnOnStageQuery(s);

            CountAssessments(6, 120);
            CountAssessments(7, 60);
            CountAssessments(8, 30);
            CountAssessments(9, 15);

            bool ok10 = (_stages[s].stage == Stages.NBN ||
                        _stages[s].stage == Stages.TC);
            if (!ok10) return;
            TurnOnStageQuery(s);

            SetIndIfResultDoesNotContain(10, "", "3045001051", "", "", "room air");
            codelist = "9991733888883,9991600100681,3045001119,3045001117,9993040000635";
            SetIndIfResultContains(10, "", codelist, "", "", "");

            bool J10b = Exists("", "9993040109337", "", "", "labored");
            bool J10c = Exists("", "9993040109338", "", "", "");
            if (J10b && J10c)
            {
                SetIndIfResultBetween(10, "", "3045001064", "", "", 0, 24);
                SetIndIfResultBetween(10, "", "3045001064", "", "", 69, 999);
            }

            //            ////
            //            ////Diabetic Med group
            //            ////
            //            buckets = new List<gBucket>();
            //            codelist = "9990000344170,3040007191,304000344160,30400012412,304000344150,ORD06";
            //            AddBuckets(buckets, "", codelist, "", "");
            //            AddMedBucketsDrugClass33(buckets);
            //            codelist = "9993040000345";
            //            AddBuckets(buckets, "", codelist, "", "", "fruit juice,iv/medication");
            //            AnalyzeBuckets(buckets, ind, bucket_size, "Diabetic Mgmt", true);


            //            reslist = "Impulsive,Lack of safety awarenesss,Does not use call light or ask for assistance,Danger to self or others,Danger from others,Exit seeking,Epilepsy monitoring,Grid/SEEG electrodes";
            //            bool G1 = Exists("", "9993040009123", "", "", reslist);
            //            reslist = "Q30 min,Q15 min,Q5 min,Line of sight";
            //            bool G2 = Exists("", "9993040009234", "", "", reslist);
            //            if (G1 && G2) SetInd(6, "Both G1 and G2 present.");
            //            if (is_peds && G2)
            //                SetInd(6, "Peds and G2");

            //            SetIndIfResultContains(6, "", "9990000300101", "", "", "24 hours");
            //            reslist = "4 hours (Age 18 and older),2 hours (Age 9 to 17),1 hour (Age 8 and younger)";
            //            SetIndIfResultContains(6, "", "9990000300001", "", "", reslist);

            //            reslist = "1:1 Nsg care for seclusion for the first hour";
            //reslist += ",Constant Nsg care for restraints for the first hour";
            //reslist += ",Record behavior every 5 minutes";
            //reslist += ",Review strengths/comfort measures - assist pt in reaching goal for discontinuation";
            //reslist += ",Administration of medication - to help client regain previous level of functioning";
            //reslist += ",Respiratory status assessed/documented each check - to ensure adequate air exchange";
            //reslist += ",Restraints checked every 15 minutes - to prevent injury from cuffs constricting normal blood flow or irritation of skin";
            //reslist += ",RN hourly assessment - mental"+CHAR_COMMA+" behavior and respiratory status - to minimize length of the procedure";
            //reslist += ",Offer fluids every 2 hours - to provide elimination opportunities";
            //reslist += ",Offer toileting every 2 hours - to provide elimination opportunities";
            //reslist += ",ROM to joints every 2 hours - to prevent injury from occurring when client is in restraints";
            //reslist += ",Hygiene PRN - to provide comfort and support";
            //reslist += ",Meals and snacks offered - to maintain nutrition (document reason for not providing and meal or snack at regular times)";
            //reslist += ",Other";
            //            SetIndIfResultContains(6, "", "9990007096219", "", "", reslist);

            //            reslist = "Implemented - Seclusion Treatment Policy";
            //reslist += ",Implemented - Behavioral Restraint Policy";
            //reslist += ",Procedure explained to patient";
            //reslist += ",Reason for seclusion/restraints explained";
            //reslist += ",Informed the patient of the goal\\Mattress checked for dangerous items";
            //reslist += ",Mattress checked for dangerous items";
            //reslist += ",check room for lighting"+CHAR_COMMA+" temperature and safety";
            //reslist += ",Dangerous items and jewelry removed";
            //reslist += ",Respiratory status monitored";
            //reslist += ",Pt assisted in achieving goal for d/c";
            //            SetIndIfResultContains(6, "", "9990007096227", "", "", reslist);
            //            SetIndIfResultContains(6, "", "PRE3", "Suicide Precautions", "", "");

            //            reslist = "Impulsive,Lack of safety awarenesss,Does not use call light or ask for assistance,Danger to self or others,Danger from others,Exit seeking,Epilepsy monitoring,Grid/SEEG electrodes";
            //            bool H1 = Exists("", "9993040009123", "", "", reslist);
            //            reslist = "Continuous observation by RN with patient";
            //reslist += ",Continuous observation by non-RN staff with patient";
            //reslist += ",Continuous observation by two staff with patient";
            //            bool H2 = Exists("", "9993040009234", "", "", reslist);
            //            if (H1 && H2) SetInd(7, "Both H1 and H2 present.");
            //            if (is_peds && H2)
            //                SetInd(7, "Peds and H2");
            int ELassess, ALassess;
            if (_stages[s].stage == Stages.AL)
            {
                ALassess = GetALAssessment();
                ELassess = CheckELAssessment();
                if (ELassess > 0 && ALassess < ELassess)
                {
                    Program.VerboseAudit("Make AL assmt equal EL assmt: AL assmt=" + ALassess + "; EL assmt=" + ELassess);
                    _stages[s]._sinds[ELassess].is_checked = true;
                }
            }

        }

        private void CountAssessments(int ind, int bucket_size)
        {
            List<gBucket> buckets;
            bool ret = false;
            string codelist;

            SetBucketSize(bucket_size);

            ////
            ////VS
            ////
            buckets = new List<gBucket>();
            codelist = "304987655,9990007096285";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, "Vital Signs: Temperature", true);

            buckets = new List<gBucket>();
            codelist = "9991733444451";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, "Vital Signs: Temp Intervention", true);

            buckets = new List<gBucket>();
            codelist = "304987657";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, "Vital Signs: VS-6", true);
            buckets = new List<gBucket>();
            codelist = "3045001025";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, "Vital Signs: VS-7", true);
            buckets = new List<gBucket>();
            codelist = "3045001064";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, "Vital Signs: VS-8", true);
            buckets = new List<gBucket>();
            codelist = "304987666";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, "Vital Signs: VS-9", true);
            buckets = new List<gBucket>();
            codelist = "9990304100017";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, "Vital Signs: VS-10", true);
            buckets = new List<gBucket>();
            codelist = "30454321";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, "Vital Signs: VS-11", true);
            buckets = new List<gBucket>();
            codelist = "9990000006294";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, "Vital Signs: VS-12", true);
            buckets = new List<gBucket>();
            codelist = "9993041120042";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, "Vital Signs: VS-13", true);
            buckets = new List<gBucket>();
            codelist = "3045001041";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, "Vital Signs: VS-14", true);
            buckets = new List<gBucket>();
            codelist = "9990304001499";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, "Vital Signs: VS-15", true);
            buckets = new List<gBucket>();
            codelist = "3045001018";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, "Vital Signs: VS-16", true);
            buckets = new List<gBucket>();
            codelist = "9993040103255";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, "Vital Signs: VS-17", true);
            buckets = new List<gBucket>();
            codelist = "9993049900009";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, "Vital Signs: VS-18", true);
            //            "Cooling blanket
            //Esophageal probe
            //Fluid warmer
            //On
            //Off
            //Radiant warmer
            //Warming blanket
            //Other"
            buckets = new List<gBucket>();
            codelist = "3045001051,3045001052,3045001053,9990000302570,9993040109339";
            codelist += ",9993040109337,9993040109338,9990000302580,9990000302390,9990000302400,9990000302410";
            codelist += ",9990000451120,9990000451080,9990000451040,9990000315800,9990000325950,9991733888883";
            codelist += ",9991733666660,9990000344210,9990000316380,9990000344220,9990000344230,9990007096450";
            codelist += ",9990000344250,9990000344280,9990000006808,3041733124512,9991733666661,3045001108";
            codelist += ",9993040000637,9993040000639,9991600100035,9991600100039,3045001117,9993040000635";
            codelist += ",3045001128,9991600100048,9990160238401,9990160238501,9993040104500";
            codelist += ",9990000400613,9993040102736,9993040102752,9993040106219,9993040106220,9993040106221";
            codelist += ",9993040106222,9993040108077,9993040108078,9993040108079";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, "Pulmonary", true);


            buckets = new List<gBucket>();
            codelist = "3045001065,3045001066,3045001067,9993040108650,9993040108651";
            codelist += ",9993040108652,9993040108653,9993040100446,9993040101320,9993040101316,9993040101317";
            codelist += ",9993040001044,9993040108605,9993040108606,9993040108607,9993040001045,9993040108611";
            codelist += ",9993040108612,9993040108613,9993040001048,9993040001049,9990000303200,9990000303210";
            codelist += ",9990000303220,3045001045,9990000303270,9990000303280,9990000303320,9990000303330";
            codelist += ",3045001012,3045001014,9990000303390,9990000303400,3045001013,3045001015,3045001004";
            codelist += ",3045001002,3045001003,3045001001,9991600100613,9991600100614,9991600100147,9990000302900";
            codelist += ",9993045006597,9990000302920,9993045006598,9993040101373,9993040101374,9993040101375";
            codelist += ",9993040101376,3040000000022,99930400000027,3040000000028,9993040000062,99930400000056";
            codelist += ",3040000000050,9993040000044,9993040000035,9993040000045,9993040000049,9993047096403";
            codelist += ",9993040000050,9993040000064,9993040000065,3045001056,3045001055,9991733888889";
            codelist += ",9990000302650,9991733888890,9991733999991,9993040100223,3045001000,9990000303180";
            codelist += ",9990000303240,9990000303300,9990000303370,9990007061360,9993040103299,9993040103300";
            codelist += ",9993040001046,9993040108614,9993040108615,9993040108616,9993040101321,9993040101322";
            codelist += ",9993040101323,9993040101324,99930434002,9993043034001,9993040107859,9993040107862";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, "Cardiovascular", true);

            buckets = new List<gBucket>();
            codelist = "9993040101409,3045001007,3045001039,9993040006316,9990304006317,9993040006318";
            codelist += ",9993040006319,9993040006320,9990000301930,9990000301920,9990000301910,9990000301900";
            codelist += ",9993040101162,9993040101163,9990000002280,9990000002279,3045001017,3045001016";
            codelist += ",9993040101166,9993040101167,9990000301980,9990000301990,9993040102775,9990000301940";
            codelist += ",9990000301950,9993040102776,9990000302000,9990000302010,9993040102777,9990000301960";
            codelist += ",9990000301970,9993040102778,9990000302190,9990000302180,3045001023,9993040001207";
            codelist += ",3045001080,3045001081,3045001079,9993040104675,9990000398011,9993040001216,3045001011";
            codelist += ",9990000450430,9990000450420,9990000450990,9990000450410,9993040103168,9993040103169";
            codelist += ",9993040109102,9990000450470,9990000450480,3045001047,9993040001091,9990000002216";
            codelist += ",9990000002217,9990000002218,9990000002219,9990000002220,9990000002221,9990000002222";
            codelist += ",9990000002223,9990000002224,9993040001057,9990000302300,9990000002230,9990000304510";
            codelist += ",9993040109138,9991070011101,9991600100259,99910701000111,9990000450560,9990000450570";
            codelist += ",9990000450580,9993040108685,9993040108686,9990000450590,9990000450600,9990000450620";
            codelist += ",9993040101196,9993040101198,9993040100079,9991733999990,9993040102780,9991140100006";
            codelist += ",9993040100076,9990000451750,9990000450440,9990000450450,9990000450460,9993040109103";
            codelist += ",9993040109104,9991140100013,9991140100014";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, "Neurological", true);

            buckets = new List<gBucket>();
            codelist = "3045001036,3045001034,3045001038,9990008100010,9990008100020,9993041001004,9993041001003";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, "Technology Mgt", true);

            buckets = new List<gBucket>();
            codelist = "9993040108530,3045001091";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, "Drains", true);

            buckets = new List<gBucket>();
            codelist = "9993040000345,9990000344170,3040007191,304000344160,30400012412,ORD06";
            AddBuckets(buckets, "", codelist, "", "");
            AddMedBucketsDrugClass33(buckets);
            AnalyzeBuckets(buckets, ind, bucket_size, "Diabetic Mgt", true);

            buckets = new List<gBucket>();
            codelist = "9991733000016,9993040100610";
            AddBuckets(buckets, "", codelist, "", "");
            AddMedBuckets(buckets);
            AnalyzeBuckets(buckets, ind, bucket_size, "Pain Withdrawal-Nausea", true);


            buckets = new List<gBucket>();
            codelist = "304000344150,DIPS,ROUI";
            AddBuckets(buckets, "", codelist, "", "");
            AddGeneralMedBuckets(buckets);
            AnalyzeBuckets(buckets, ind, bucket_size, "Medication Response", true);

            buckets = new List<gBucket>();
            bool obmeds = AddOBMedBuckets(buckets);
            if (obmeds) SetInd(7, "OB Meds q1 hour.");
            AnalyzeBuckets(buckets, ind, bucket_size, "OB MEDS", true);

            buckets = new List<gBucket>();
            codelist = "9990007096390,9990000305290,3045001090,3045001089,9990000370180";
            codelist += ",3040011360,9990000006298,9993040108593";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, "Fluid Status/GU/GI", true);

            buckets = new List<gBucket>();
            codelist = "9991025006827,9991025006826,9991025006828,9991025006829,9991025006830";
            codelist += ",9990000012301,9990000012302,9990000012303,9990000012305,9990000012306,9990000012304";
            codelist += ",1028000004,1028000001,9990102521012,1028000002,1028000003,9991020100645,9991020100655";
            codelist += ",1028000007,1028000008,1028000009,9991020100648,9991020100657,1028000010,1028000011";
            codelist += ",1028000012,1028000013,1028000014,1028000015,1028000016,1028000017,1028000018,9991025051116";
            codelist += ",9991025111616,1028000022,1028000023,1028000024,9991020100652,9991020100661,9990000012331";
            codelist += ",9990000012332,1028000040,1028000043,1028000045,1028000046,9991020100568,9991020100569";
            codelist += ",9990000012127,9990000012128,9990000012309,9991025012136,9991025012137,9991025012139";
            codelist += ",9991025012144,9993040102317,9993040102318,9993040102319,9993040102320";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, "OB Assessment", true);



        }
        private void Check_11(int s)
        {
            int ct;
            string codelist;
            string reslist;
            bool is_peds = false;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P2 11. Intensive Therapeutic Support");
            Program.VerboseAudit("---------------");
            _use_stage_query = false;

            bool ok = (_stages[s].stage == Stages.ANT ||
                        _stages[s].stage == Stages.EL ||
                        _stages[s].stage == Stages.AL ||
                        _stages[s].stage == Stages.DEL ||
                        _stages[s].stage == Stages.REC ||
                        _stages[s].stage == Stages.PP ||
                        _stages[s].stage == Stages.NBN ||
                        _stages[s].stage == Stages.TC);
            if (!ok) return;

            bool non_laboring = (_stages[s].stage == Stages.ANT ||
                        _stages[s].stage == Stages.REC ||
                        _stages[s].stage == Stages.PP ||
                        _stages[s].stage == Stages.NBN ||
                        _stages[s].stage == Stages.TC);
            if (non_laboring) TurnOnStageQuery(s);


            // private bool GetEVDTWhereResultContains(string cat, string code_list, string desc_list, string field, string res_list, out DateTime return_evdt)
            DateTime evdt;
            reslist = "States is not coping,Crying,tearfulness,Inability to focus";
            reslist += ",Clawing,biting,Tense,Thrashing in bed,Jitteriness";
            reslist += ",Sweaty,Panicked activity,Tremulous voice";
            evdt = EVDTIsSame("9991025012053", "RN continuous at bedside", "1028000029", reslist, SearchDepth.SearchDefault);
            if (evdt > DateTime.MinValue)
            {
                SetInd(11, "K1 and K2 have same charted time at "+evdt);
            }
            SetIndIfResultContains(11, "", "3041025021710", "", "", "Continuous");
            SetIndIfResultContains(11, "", "9991020100509,9991020100510", "", "", "");

            codelist = "9993040100610";
            SetIndIfResultContains(11, "", codelist, "", "", "");
            SetIndIfResultBetween(11, "", "9991733000016", "", "", 9, 99);
            SetIndIfResultBetween(11, "", "9993040100610", "", "", 9, 99);

            SetIndIfResultContains(11, "", "9991025121827", "", "", "> 60 minutes");

            reslist = "Combative,Compulsive,Crying Inappropriately,Delusional,Demanding";
            reslist += ",Destructive,Disruptive,Exit seeking,Hallucinating,Hostile";
            reslist += ",Oppositional,Resistant to care,Staff seeking";
            SetIndIfResultContains(11, "", "9993040105629", "", "", reslist);

            CheckEDUtab("3040019564");
            CheckEDUtab("3040018802");
            CheckEDUtab("3040019429");
            CheckEDUtab("3040019626");
            CheckEDUtab("3040019773");
            CheckEDUtab("3040020569");
            CheckEDUtab("3040018770");
            CheckEDUtab("3040019597");
            CheckEDUtab("3040019587");
            CheckEDUtab("3040018788");
            CheckEDUtab("3040018690");
            CheckEDUtab("3040019462");
            CheckEDUtab("3040020762");
            CheckEDUtab("3040020565");
            CheckEDUtab("3040020708");
            CheckEDUtab("3040021311");
            CheckEDUtab("3040020757");
            CheckEDUtab("3040019438");
            CheckEDUtab("3040018686");
            CheckEDUtab("3040020288");
            CheckEDUtab("3040020278");
            CheckEDUtab("3040019172");
            CheckEDUtab("3040020314");
            CheckEDUtab("3040019604");
            CheckEDUtab("3040020664");
            CheckEDUtab("3040019143");
            CheckEDUtab("3040018773");
            CheckEDUtab("3040018769");
            CheckEDUtab("3040019385");
            CheckEDUtab("3040018689");
            CheckEDUtab("3040020820");
            CheckEDUtab("3040020559");
            CheckEDUtab("3040020534");
            CheckEDUtab("3040020463");
            CheckEDUtab("3040019415");
            CheckEDUtab("3040020413");
            CheckEDUtab("3040018515");
            CheckEDUtab("3040020310");
            CheckEDUtab("3040020404");
            CheckEDUtab("3040020512");
            CheckEDUtab("3040018603");
            CheckEDUtab("3040020282");
            CheckEDUtab("3040018908");
            CheckEDUtab("3040020466");
            CheckEDUtab("3040020255");
            CheckEDUtab("3040020262");
            CheckEDUtab("3040020297");
            CheckEDUtab("3040019782");
            CheckEDUtab("3040019666");
            CheckEDUtab("3040020328");
            CheckEDUtab("3040018684");
            CheckEDUtab("3040002644");
            CheckEDUtab("3040001000");
            CheckEDUtab("3040019679");
            CheckEDUtab("3040020321");
            CheckEDUtab("3040020477");
            CheckEDUtab("3040018533");
            CheckEDUtab("3040019736");
            CheckEDUtab("3040018932");
            CheckEDUtab("3040019011");
            CheckEDUtab("3040018647");
            CheckEDUtab("3040000907");
            CheckEDUtab("3040019707");
            CheckEDUtab("3040018906");
            CheckEDUtab("3040020694");
            CheckEDUtab("3040019762");
            CheckEDUtab("3040018587");
            CheckEDUtab("3040018530");
            CheckEDUtab("3040020741");
            CheckEDUtab("3040019360");
            CheckEDUtab("3040019721");
            CheckEDUtab("3040019691");
            CheckEDUtab("3040019352");
            CheckEDUtab("3040018525");
            CheckEDUtab("3040018688");
            CheckEDUtab("3040020418");
            CheckEDUtab("3040018687");
            CheckEDUtab("3040018692");
            CheckEDUtab("3040018691");
            CheckEDUtab("3040018613");
            CheckEDUtab("3040018631");
            CheckEDUtab("3040020809");
            CheckEDUtab("3040020801");
            CheckEDUtab("3040018872");
            CheckEDUtab("3040020265");
            CheckEDUtab("3040020550");
            CheckEDUtab("3040018910");
            CheckEDUtab("3040020431");
            CheckEDUtab("3040020730");
            CheckEDUtab("3040020546");
            CheckEDUtab("3040020540");
            CheckEDUtab("3040020991");
            CheckEDUtab("3040020982");
            CheckEDUtab("3040020600");
            CheckEDUtab("3040020615");
            CheckEDUtab("3040020622");
            CheckEDUtab("3040020610");
            CheckEDUtab("3040018556");
            CheckEDUtab("3040020717");
            CheckEDUtab("3040019405");
            CheckEDUtab("3040018772");
            CheckEDUtab("3040020317");
            CheckEDUtab("3040018685");
            CheckEDUtab("3040020554");
            CheckEDUtab("3040019749");
            CheckEDUtab("3040019448");
            CheckEDUtab("3040004179");
            CheckEDUtab("3040020576");
            CheckEDUtab("3040020581");
            CheckEDUtab("3040020744");
            CheckEDUtab("3040019147");
            CheckEDUtab("3040020425");
            CheckEDUtab("3040020437");
            CheckEDUtab("3040019576");
            CheckEDUtab("3040018909");
            CheckEDUtab("3040020324");
            CheckEDUtab("3040019158");
            CheckEDUtab("3040018598");
            CheckEDUtab("3040018771");
            CheckEDUtab("3040020644");
            CheckEDUtab("3040021310");
            CheckEDUtab("3040020635");

            CheckEDUtab("3048000084");
            //CheckEDUtab("1021111110");
            //CheckEDUtab("3048000419");
            CheckEDUtab("3040020431");
            CheckEDUtab("3040021208");

        }

        private DateTime EVDTIsSame(string codestr1, string reslist1, string codestr2, string reslist2, SearchDepth search_depth)
        {
            DateTime evdt = DateTime.MinValue;
            var query1 = StartNewQuery(search_depth);
            query1 = AndItemFilter(query1, "", codestr1, "", "", reslist1);

            var query2 = StartNewQuery(search_depth);
            query2 = AndItemFilter(query2, "", codestr2, "", "", reslist2);

            foreach (var item1 in query1)
            {
                foreach (var item2 in query2)
                {
                    if (item1.EVENT_DATETIME == item2.EVENT_DATETIME)
                        evdt = item1.EVENT_DATETIME;
                }
            }

            return evdt;
        }

        private void CheckEDUtab(string educode)
        {
            int ub, i;
            string[] desc = new string[3];
            DateTime evdt;
            string cd1, cd2, res1, res2, topic1, topic2;

            //OBX | 2 | ST | EDU3040004179METHOD ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC STROKE EVERY MINUTE MATTERS MC6074| 1 | E |||||| F ||| 20170612113300
            //OBX | 4 | ST | EDU3040004179RESPONSE ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC STROKE EVERY MINUTE MATTERS MC6074| 1 | TB |||||| F ||| 20170612113300
            //OBX | 6 | ST | EDU3040004179METHOD ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC YOU CAN IMPROVE YOUR HEALTH MC2066 - 09 | 1 | E |||||| F ||| 20170612113300
            //OBX | 8 | ST | EDU3040004179RESPONSE ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC YOU CAN IMPROVE YOUR HEALTH MC2066 - 09 | 1 | TB |||||| F ||| 20170612113300
            //OBX | 10 | ST | EDU3040004179METHOD ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC HIGH BLOOD PRESSURE (HYPERTENSION)MC5056 | 1 | E |||||| F ||| 20170612113300
            //OBX | 12 | ST | EDU3040004179RESPONSE ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC HIGH BLOOD PRESSURE (HYPERTENSION)MC5056 | 1 | TB |||||| F ||| 20170612113300
            var query1 = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query1 = AndItemFilter(query1, "", "EDU" + educode + "METHOD", "", "", "D,I");
            foreach (var item1 in query1)
            {
                evdt = item1.EVENT_DATETIME;
                res1 = item1.RESULT;
                cd1 = item1.CODE;
                //Program.VerboseAudit("EDU: res=" + res1 + " code=" + cd1 + " evdt=" + evdt.ToString());

                var arr = item1.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                ub = arr.GetUpperBound(0);
                for (i = 0; (i <= Math.Min(ub, 2)); i++)
                    desc[i] = arr[i];
                if (ub >= 2)
                    topic1 = desc[2].Trim();
                else
                    topic1 = "";
                SetInd(11, "Found EDU" + educode + ": " + desc[0] + "^" + topic1 + " at:" + evdt.ToString() + " METHOD=" + res1);
            }

        } // end proc

        //private bool CheckResultFor11RNTime(string cat, string code_list, string desc_list, string field, string result, SearchDepth search_depth)
        //{
        //    bool qualifies = false;
        //    int ct = 0;

        //    if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
        //    var query = StartNewQuery(search_depth);
        //    query = AndItemFilter(query, cat, code_list, desc_list, field, 
        //        "Following behavior safety plan,Multiple Staff for acute behavioral intervention,1:1 RN Time,1:1 Staff Time");
        //    ct = query.Count();
        //    foreach (var item in query)
        //    {
        //        if (!qualifies)
        //        {
        //            qualifies |= (item.RESULT.ToLower().Contains("Following behavior safety plan".ToLower()));
        //            qualifies |= (item.RESULT.ToLower().Contains("Multiple Staff for acute behavioral intervention".ToLower()));
        //        }
        //    }
        //    if (!qualifies && (ct > 0)) // then 1:1 RN are the only results.  need to check their NTE.
        //    {
        //        // Query to look for OBX + NTE
        //        //select patindex('%|9993040000352%NTE|%', SOURCE_TEXT),
        //        //      substring(SOURCE_TEXT, patindex('%|9993040000352%NTE|%', SOURCE_TEXT), 240) from EVENT_LOG
        //        //where source_text like '%9993040000352%NTE|%' and timestamp>'2/12/19'

        //        //OBX|1|ST|9993040000352^Staff Interventions||1:1 Staff Time(# minutes)||||||F|||20190211203400||IDMPROD20979444^BIRCHLER^SARAH^M
        //        //NTE|1||60
        //        DateTime tstamp;
        //        foreach (var item in query)
        //        {
        //            if (!qualifies && 
        //                (item.RESULT.ToLower().Contains("1:1 RN Time".ToLower()) || item.RESULT.ToLower().Contains("1:1 Staff Time".ToLower())))
        //            {
        //                tstamp = PFSDBUtility.DBToDateTime(item.TIMESTAMP);
        //                Program.VerboseAudit("Check NTE segment for 1:1 Time after:" + tstamp);
        //                qualifies = CheckForNTE(tstamp);
        //            }

        //        }

        //    }

        //    return qualifies;
        //}
        //private bool CheckForNTE(DateTime tstamp)
        //{
        //    bool qualifies = false;
        //    int v = 0;
        //    //event_log timestamp is always later than the chartitem timestamp it created.
        //    string sql = "select substring(SOURCE_TEXT, patindex('%|9993040000352%NTE|%', SOURCE_TEXT)-3, 240) from EVENT_LOG";
        //    sql += " where encounter_id=" + _pat.encounter_id.ToString();
        //    sql += " and timestamp>=" + PFSDBUtility.SQLDateTime(tstamp);
        //    sql += " and timestamp<" + PFSDBUtility.SQLDateTime(tstamp.AddMinutes(3));
        //    sql += " and source_text like '%9993040000352%NTE|%'";
        //    var db = PFSDBUtility.NewSqlConnection();
        //    var cmd = new SqlCommand(sql, db);
        //    SqlDataReader dr = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
        //    while (dr.Read())
        //    {
        //        string s = PFSDBUtility.DBToString(dr[0]);
        //        Program.VerboseAudit("String containing NTE segment: " + s);
        //        int posNTE = s.IndexOf("NTE|",0);
        //        int posOBX = s.IndexOf("OBX|",0);
        //        Program.VerboseAudit("posNTE=" + posNTE + "  posOBX=" + posOBX);
        //        if ((posNTE > 0) && (posOBX > 0) && (posNTE < posOBX)) // then this NTE is associated with the item
        //        {
        //            //look for the value in NTE-3
        //            if (s.Length >= 32)
        //            {
        //                s = s.Substring(posNTE, 32);
        //                var arr = s.Split(new string[] { "|" }, StringSplitOptions.None);
        //                if (arr.GetUpperBound(0) >= 3)
        //                {
        //                    if (arr[3].IsNumeric())
        //                    {
        //                        v = (int)arr[3].Val();
        //                    }
        //                    qualifies |= (v >= 30);
        //                }
        //            }
        //        }
        //    }
        //    dr.Close();
        //    return qualifies;
        //}



        //        private void CountFluids()
        //        {
        //            string descript;
        //            string drugclass;
        //            bool newbag = false;
        ////OBX|1|DT|MED9811^DEXTROSE 2.5 % AND 0.45 % SODIUM CHLORIDE INTRAV;;;29;;;50;;;IV;;;NewBag|NewBag|20170609120700||||||F|||20170609120700

        //            var query = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
        //            query = query.Where(e => e.CODE.StartsWith("med"));
        //            query = query.Where(e => e.EVENT_DATETIME <= _pat.unit_departure);
        //            query = query.OrderBy(e => e.EVENT_DATETIME);
        //            foreach (var item in query)
        //            {
        //                var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
        //                Program.VerboseAudit("Count Fluids ub=" + arr.GetUpperBound(0) + " descript="+item.DESCRIPTION);
        //                if (arr.GetUpperBound(0) >= 4)
        //                {
        //                    descript = arr[0];
        //                    drugclass = arr[1];
        //                    newbag = (arr[4].ToLower() == "newbag" || arr[4].ToLower() == "new bag" || arr[4].ToLower().Contains("bolus"));
        //                    if ((drugclass == "29") && (newbag))
        //                    {
        //                        SetInd(10, "Found Fluids in Meds with NewBag:" + item.EVENT_DATETIME.ToString() + "; drugclass=" + drugclass + "; descript=" + descript);
        //                    }
        //                }
        //            }

        //        }

        private void Check_121314(int s)
        {
            string reslist;
            int ct;
            List<int> buckets;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P2 12. Medication Mgt q4 hours");
            Program.VerboseAudit("P2 13. Medication Mgt q2 hours");
            Program.VerboseAudit("P2 14. Medication Mgt q1 hours");
            Program.VerboseAudit("---------------");

            // Obey stage rules but if present in EL, then automatically give to AL 2/3/2021

            bool ok = (_stages[s].stage == Stages.ANT ||
            _stages[s].stage == Stages.EL ||
            _stages[s].stage == Stages.AL ||
            _stages[s].stage == Stages.REC ||
            _stages[s].stage == Stages.PP);
            if (!ok) return;

            TurnOnStageQuery(s);

            if (_stages[s].stage == Stages.AL && _epidural_found_in_EL)
                SetInd(14, "Setting indicator based on finding Epidural/oxytocin in EL.");
            if (_stages[s]._sinds[14].is_checked) return;

            if (CheckEpiduralOxytocinInStage())
            {
                SetInd(14, "Epidural/oxytocin found.");
                if (_stages[s].stage == Stages.EL) _epidural_found_in_EL = true;
            }
            //        Change this to look for the range of time between new and stopped and

            //        see if it infiltrates into AL stage.

            //        var query = StartNewQuery(SearchDepth.SearchDefault);
            //        query = query.Where(e => e.CODE.ToLower().StartsWith("med"));
            //        query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;given") || e.DESCRIPTION.ToLower().Contains(";;;syringe") || e.DESCRIPTION.ToLower().ContainsAny(meds_rate) || e.DESCRIPTION.ToLower().Contains(";;;new"));
            //        query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;epid"));
            //ALSO oxytocin!
            //        query = query.OrderBy(e => e.EVENT_DATETIME);
            //        ct = query.Count();
            //        if (ct > 0) SetInd(14, "Epidural med found. Count=" + ct);
            if (_stages[s]._sinds[14].is_checked) return;

            CountMedAssessments(12, 240);
            CountMedAssessments(13, 120);
            CountMedAssessments(14, 60);
            //CountAssessments(12, 20);

            CheckMedBP();

        }
        private void CheckMedBP()
        {
            int stagemins = 0;
            var query = StartNewStageQuery(out stagemins);   
            query = query.Where(e => e.CODE.ToLower().StartsWith("med"));
            //query = query.Where(e => e.DESCRIPTION.Contains(";;;7;;;") || e.DESCRIPTION.Contains(";;;33;;;") || e.DESCRIPTION.Contains(";;;1;;;") || e.DESCRIPTION.Contains(";;;2;;;") || e.DESCRIPTION.Contains(";;;6;;;"));
            query = query.Where(e => e.DESCRIPTION.Contains(";;;22;;;")
                    || e.DESCRIPTION.Contains(";;;23;;;")
                    || e.DESCRIPTION.Contains(";;;28;;;"));
            //query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;given") || e.DESCRIPTION.ToLower().Contains(";;;syringe") || e.DESCRIPTION.ToLower().ContainsAny(meds_rate) || e.DESCRIPTION.ToLower().Contains(";;;new"));
            query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;given"));
            int ct = query.Count();
            Program.VerboseAudit("Num Med Class 22,23,28 found: " + ct);
            if (ct == 0) return;

            //304987666 BP   if 2 BP then 14 else if BP is q2/q1 then 13/14.
            var qBP = StartNewStageQuery(out stagemins);
            qBP = qBP.Where(e => e.CODE == "304987666" || e.CODE == "9990304100017" || e.CODE == "9993041120042" || e.CODE == "3045001041" || e.CODE == "9993049900009");
            int ctBP = qBP.Count();
            Program.VerboseAudit("Num BP found: " + ctBP);
            if (ctBP >= 2)
            {
                int ind = 0;
                string indreason = "";
                ind = 12;
                indreason = "MedClass 22,23,28 with a min BP count of " + ctBP;
                int numhrs = stagemins / 60;
                if (numhrs >= 4)
                {
                    if (ctBP >= numhrs / 2)
                    {
                        ind = 13;
                        indreason= "MedClass 22,23,28 with BP q2 count of " + ctBP;
                    }
                    if (ctBP >= numhrs)
                    {
                        ind = 14;
                        indreason = "MedClass 22,23,28 with BP q1 count of " + ctBP;
                    }
                }
                SetInd(ind, indreason);

            }

        }
        private bool CheckEpiduralOxytocinInStage()
        {
            bool ret = false;
            DateTime startdt = _pat.pull_start;
            DateTime findt = _pat.pull_finish;

            for (int i = 0; i <= stageidx; i++)
            {
                if (_stages[i].stage == _currstage)
                {
                    startdt = _stages[i].start;
                    findt = _stages[i].finish;
                    if (_stages[i + 1].stage > _currstage)
                    {
                        findt = _stages[i + 1].start;
                    }
                }
            }

            Program.VerboseAudit("Look for epidural/oxytocin between: " + startdt + " and " + findt);

            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = query.Where(e => e.CODE.ToLower().StartsWith("med"));
            query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;given") || e.DESCRIPTION.ToLower().Contains(";;;syringe") || e.DESCRIPTION.ToLower().ContainsAny(meds_rate_without_stopped) || e.DESCRIPTION.ToLower().Contains(";;;new"));
            query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;epid")
                                    || e.DESCRIPTION.ToLower().Contains(";;;oxytocin"));
            query = query.OrderByDescending(e => e.EVENT_DATETIME);
            if (query.Count() <= 0) return ret;

            DateTime begdt = query.First().EVENT_DATETIME;
            DateTime enddt = DateTime.MaxValue;

            var query2 = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query2 = query2.Where(e => e.CODE.ToLower().StartsWith("med"));
            query2 = query2.Where(e => e.EVENT_DATETIME > begdt);
            query2 = query2.Where(e => e.DESCRIPTION.ToLower().Contains(";;;stopped"));
            query2 = query2.Where(e => e.DESCRIPTION.ToLower().Contains(";;;epid")
                                    || e.DESCRIPTION.ToLower().Contains(";;;oxytocin"));
            query2 = query2.OrderBy(e => e.EVENT_DATETIME);
            if (query2.Count() > 0) enddt = query2.First().EVENT_DATETIME;

            Program.VerboseAudit("epidural/oxytocin: " + begdt + " thru " + enddt);

            if ((begdt <= startdt && enddt > startdt) ||
                (begdt > startdt && begdt < findt))
                ret = true;

            return ret;
        }

        private void Check_1516(int s)
        {
            string reslist, codelist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P2 15. Wound Mgmt");
            Program.VerboseAudit("P2 16. Wound Mgmt Extended");
            Program.VerboseAudit("---------------");

            string piv = "Midline Dual Lumen Catheter,Midline Single Lumen Catheter,Peripheral IV";

            _use_stage_query = false;

            bool ok = (_stages[s].stage == Stages.ANT ||
            _stages[s].stage == Stages.PP ||
            _stages[s].stage == Stages.NBN);
            if (ok)
            {
                //for first 7 days, give 15 to NBN....  See change for 2/3/2021
                double ageindays = PFSUtility.DateDiffInDays(_pat.dob, Program.g_pull_finish);
                if (ageindays < 8) SetInd(15, "Auto-trigger for up to 7 days: Age=" + ageindays + " days.");

                if (!Exists("", "9993041000044", "", "", EXACT_MATCH_PREFIX + "Stage I"))
                {
                    reslist = "Stage II,Stage III,Stage IV,Unstageable,Deep tissue injury";
                    SetIndIfResultContains(15, "", "9993041000044", "", "", reslist);
                    codelist = "9990000303750,9993046629812,9993040021267,9990007061270";
                    codelist += ",99930413000000,9990000001493,9990000001494,9990007061240,9993040102846";
                    codelist += ",9993040021269,9993041000077,9993040021270,9990000303910";
                    SetIndIfResultContains(15, "", codelist, "", "", "");

                    reslist = "";
                    SetIndIfResultContains(15, "", "9990000303800", NOT_PREFIX + piv, "", reslist);
                    SetIndIfResultContains(15, "", "9990000303820", NOT_PREFIX + piv, "", reslist);
                }

                codelist = "9993040104036,9993040104037,9993040023765,9993040100564,9991420100006";
                codelist += ",9991420100007,9991420100009,9990000304500,9990000304510,9993040103946";
                codelist += ",9993040103947,9993040103948,9993040103951,9993040103954,9993040103957";
                codelist += ",9990007070177,9990007070178,9990007070179,9993040001021,9993040000088";
                codelist += ",9993040001022,9993040103228,9990007085420,9993040102851,9990007085260";
                codelist += ",9990000016070,9993040102647,9993040102642,9993040102740,9993040102746";
                codelist += ",9993040102649,9993040102644,9993040102648,9993040102660,9993040102643";
                codelist += ",9993040102662,9993040102742,9993040102748,9993040102741,9993040102744";
                codelist += ",9993040102747,9993040102747,9993040102750,9990000396120,9990007061290";
                codelist += ",9990000006333,9993040108525";
                SetIndIfResultContains(15, "", codelist, "", "", "");

                SetIndIfResultContains(15, "", "3045001032", NOT_PREFIX + piv, "", "");
                SetIndIfResultContains(15, "", "9990007096660", NOT_PREFIX + piv, "", "");
                SetIndIfResultContains(15, "", "9990007073550", NOT_PREFIX + piv, "", "");

                codelist = "3045001041,9990000301270,9990007080680,9993040103205,9993040103206";
                codelist += ",9993040103209,9993040000199,9990304840001,9990304840002,9990000370090";
                codelist += ",9993040001044,9993040108605,9993040108606,9993040108607,9993040001046";
                codelist += ",9993040108614,9993040108615,9993040108616,9991180102055,9991180102056";
                SetIndIfResultContains(15, "", codelist, "", "", "");

                SetIndIfResultContains(15, "", "9990007085590", "", "", "Bloody,Bright red,Coffee ground,Dark red");

                codelist = "99930400003205,9993040304870,9993040108530,3045001091,9993040108062";
                codelist += ",9993040108060,9993040108059,9993040108070,9993040108068,3040108067";
                codelist += ",9993040108108,9993040108102,9993040108114,9993040108109,9993040108103";
                codelist += ",9993040108115,9993040108110,9993040108104,9993040108116,9993040108111";
                codelist += ",9993040108105,9993040108117,9993040108112,9993040108106,9993040108118";
                codelist += ",9993040108113,9993040108119,9993040109510,9993040109511";
                codelist += ",9993040108145,9993040108135,9993040108140,9993040108146,9993040108136";
                codelist += ",9993040108141,9993040108147,9993040108137,9993040108142,9993040108148";
                codelist += ",9993040108138,9993040108143,9993040108149,9993040108139,9993040108144,9993040108150";
                SetIndIfResultContains(15, "", codelist, "", "", "");
                SetIndIfResultContains(15, "", EXACT_MATCH_PREFIX + "9993040108", "", "", "");

                codelist = "9990000006298";
                reslist = "Tea,Rusty,Peach,Cherry,Pink,Ketchup";
                SetIndIfResultContains(15, "", codelist, "", "", reslist);

                codelist = "9991020100568,9991020100569";
                SetIndIfResultContains(15, "", codelist, "", "", "");

                codelist = "9991733888867";
                reslist = "Applied,Changed,Marked,Reinforced,Site care,Staples,Sutures";
                SetIndIfResultContains(15, "", codelist, "", "", reslist);


                codelist = "9991733888850,9991140100024";
                SetIndIfResultContains(15, "", codelist, "", "", "");


                codelist = EXACT_MATCH_PREFIX + "18";
                reslist = "Bloody,Clots,Tarry";
                SetIndIfResultContains(15, "", codelist, "", "", reslist);

                codelist = EXACT_MATCH_PREFIX + "17";
                reslist = "Maroon,Red";
                SetIndIfResultContains(15, "", codelist, "", "", reslist);

                codelist = "9990000002114";
                reslist = "Frenulectomy,Lacerated";
                SetIndIfResultContains(15, "", codelist, "", "", reslist);

                SetIndIfResultContains(15, "", "3045001041", "", "", "");
                SetIndIfResultContains(15, "", "9990000306470", "", "", "Laceration,Tear");
                SetIndIfResultContains(15, "", "9991600100476", "", "", "Laceration");
            }

            ok = (_stages[s].stage == Stages.ANT);
            if (ok)
            {

                codelist = "9990102547210";
                reslist = "Yes";
                SetIndIfResultContains(15, "", codelist, "", "", reslist);

                codelist = "9991025006819";
                reslist = "Spotting,Small,Moderate,Large,Brown,Pink,Bright red,Dark Red,Clots present";
                SetIndIfResultContains(15, "", codelist, "", "", reslist);

                codelist = "1028000040";
                reslist = "Spontaneous,AROM,Prolonged,PROM,PPROM";
                SetIndIfResultContains(15, "", codelist, "", "", reslist);

                codelist = "102000043";
                reslist = "Clear,Meconium,Bloody,Purulent";
                SetIndIfResultContains(15, "", codelist, "", "", reslist);

                codelist = "1028000045";
                reslist = "Scant,Small,Moderate,Large";
                SetIndIfResultContains(15, "", codelist, "", "", reslist);

                codelist = "1025081728";
                reslist = "Bloody show,Spotting,Small,Moderate,Large,Brown,Bright red,Dark red,Clots present";
                SetIndIfResultContains(15, "", codelist, "", "", reslist);
            }

            ok = (_stages[s].stage == Stages.ANT) ||
                (_stages[s].stage == Stages.REC) ||
                (_stages[s].stage == Stages.PP) ||
                (_stages[s].stage == Stages.NBN);
            if (ok)
            {
                if (delivcs && (_stages[s].stage == Stages.PP || _stages[s].stage == Stages.REC))
                {
                    SetInd(16, "Cesarean section at:" + delivcs_dt);
                }
                if (circ_done && _stages[s].stage == Stages.NBN)
                {
                    if (circ_time <= _stages[s].finish)
                        SetInd(16, "Circumcision done at time=" + circ_time + "  less than NBN stage end=" + _stages[s].finish);
                }
                reslist = "Done";
                SetIndIfResultContains(16, "", "9991140100003", "", "", reslist);//circ
                reslist = "";
                SetIndIfResultContains(16, "", "9991140100026", "", "", reslist);
                reslist = "";
                SetIndIfResultContains(16, "", "9991140100027", "", "", reslist);
                //reslist = "Yes,1";
                //SetIndIfResultContains(16, "", "9990000026972", "", "", reslist);

                if (!Exists("", "9993041000044", "", "", EXACT_MATCH_PREFIX + "Stage I"))
                {
                    SetIndIfResultContains(16, "", "9990007061190", "", "", "");
                    SetIndIfResultContains(16, "", "9990000304850", NOT_PREFIX + piv, "", "");
                    SetIndIfResultContains(16, "", "9990000303780", NOT_PREFIX + piv, "", "");
                }

                reslist = "Midline,Right,Left";
                SetIndIfResultContains(16, "", "9990000012134", "", "", reslist);
                reslist = "Third degree,Fourth degree";
                SetIndIfResultContains(16, "", "9990000012135", "", "", reslist);
            }
        }

        private void Check_17(int s)
        {
            string reslist, codelist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P2 17. At-Risk Birth");
            Program.VerboseAudit("---------------");

            _use_stage_query = false;

            bool ok = (_stages[s].stage == Stages.IC ||
            _stages[s].stage == Stages.TC);
            if (ok)
            {
                if (_pat.gestational_age > 0 && _pat.gestational_age <= 35) SetInd(17, "Gestational age=" + _pat.gestational_age.ToString());

                SetIndIfResultBetween(17, "", "304987657", "", "", 201, 999, SearchDepth.SearchDefault);
                SetIndIfResultBetween(17, "", "3045001025", "", "", 0, 99, SearchDepth.SearchDefault);
                reslist = EXACT_MATCH_PREFIX + "0";
                SetIndIfResultContains(17, "", "3045001064", "", "", reslist);
                reslist = EXACT_MATCH_PREFIX + "0";
                SetIndIfResultContains(17, "", "9990000012181", "", "", reslist);
                reslist = EXACT_MATCH_PREFIX + "0";
                SetIndIfResultContains(17, "", "9990000012182", "", "", reslist);
                SetIndIfResultBetween(17, "", "9990000012186", "", "", 0, 6, SearchDepth.SearchDefault);

                SetIndIfResultBetween(17, "", "9990000012196", "", "", 0, 6, SearchDepth.SearchDefault);
                SetIndIfResultBetween(17, "", "9990000012606", "", "", 0, 6, SearchDepth.SearchDefault);
                SetIndIfResultBetween(17, "", "9990000012706", "", "", 0, 6, SearchDepth.SearchDefault);
                SetIndIfResultBetween(17, "", "9990000012707", "", "", 0, 6, SearchDepth.SearchDefault);
                reslist = "Blow-by Oxygen,Oxygen,Positive pressure ventilation";
                SetIndIfResultContains(17, "", "9990000344250", "", "", reslist);
                reslist = "Room air";
                SetIndIfResultDoesNotContain(17, "", "3045001051", "", "", reslist);
                reslist = "";
                SetIndIfResultContains(17, "", "9991733888883", "", "", reslist);


                reslist = "";
                SetIndIfResultContains(17, "", "3045001117", "", "", reslist);
                reslist = "";
                SetIndIfResultContains(17, "", "9993040000635", "", "", reslist);
                reslist = "";
                SetIndIfResultContains(17, "", "3045001128", "", "", reslist);

                SetIndIfResultBetween(17, "", "304987663", "", "", 0, 2.5, SearchDepth.SearchDefault);
                SetIndIfResultBetween(17, "", "304987663", "", "", 4.5, 99, SearchDepth.SearchDefault);
                reslist = "";
                SetIndIfResultContains(17, "", "9991600100681,3045001119", "", "", reslist);
            }
        }


        private void Check_18(int s)
        {
            string reslist, codelist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P2 18. High Risk Pregnancy");
            Program.VerboseAudit("---------------");

            _use_stage_query = false;
            SearchDepth d = SearchDepth.SearchSinceAdmission;

            bool ok = (_stages[s].stage == Stages.ANT ||
            _stages[s].stage == Stages.EL ||
            _stages[s].stage == Stages.AL ||
            _stages[s].stage == Stages.DEL);
            if (ok)
            {
                reslist = "Yes,1";
                SetIndIfResultContains(18, "", "999102566445", "", "", reslist,d);
                reslist = "Preterm labor,Abruptio placentae,Placenta Previa";
                SetIndIfResultContains(18, "", "9990000012126", "", "", reslist, d);
                reslist = "PPROM";
                SetIndIfResultContains(18, "", "1028000040", "", "", reslist, d);
                if (_pat.age <= 15 || _pat.age >= 35)
                    SetInd(18, "Age (<15 or >35) =" + _pat.age.ToString());
                SetIndIfResultBetween(18, "", "304987663", "", "", 0, 45, d);
                SetIndIfResultBetween(18, "", "304987663", "", "", 114, 999, d);
                SetIndIfResultContains(18, "", "EDU3040018533", "", "", "", d);
                LookForGestationWks();

                reslist = "Yes,1";
                SetIndIfResultContains(18, "", "9990000012018", "", "", reslist, d);
                reslist = "Yes,1";
                SetIndIfResultContains(18, "", "9991025664455", "", "", reslist, d);
                reslist = "Normal,Bradycardia,Tachycardia,Indeterminate";
                SetIndIfResultContains(18, "", "1028000008", "", "", reslist, d);
                reslist = "Normal,Bradycardia,Tachycardia,Indeterminate";
                SetIndIfResultContains(18, "", "1028000009", "", "", reslist, d);
                reslist = "Normal,Bradycardia,Tachycardia,Indeterminate";
                SetIndIfResultContains(18, "", "9991020100648", "", "", reslist, d);
                reslist = "Normal,Bradycardia,Tachycardia,Indeterminate";
                SetIndIfResultContains(18, "", "9991020100657", "", "", reslist, d);
                reslist = "Minimal,Moderate,Marked,Absent";
                SetIndIfResultContains(18, "", "1028000011", "", "", reslist, d);
                reslist = "Minimal,Moderate,Marked,Absent";
                SetIndIfResultContains(18, "", "1028000012", "", "", reslist, d);
                reslist = "Category I,Category II,Category III";
                SetIndIfResultContains(18, "", "1028000023", "", "", reslist, d);
                reslist = "Category I,Category II,Category III";
                SetIndIfResultContains(18, "", "1028000024", "", "", reslist, d);
                reslist = "Category I,Category II,Category III";
                SetIndIfResultContains(18, "", "9991020100652", "", "", reslist, d);
                reslist = "Category I,Category II,Category III";
                SetIndIfResultContains(18, "", "9991020100661", "", "", reslist, d);

            }
        }

        private void LookForGestationWks()
        {
            // ICD code = Z3A.36
            // <=36 or >=43
            ENCOUNTER_ICD_DX[] _icd_items;

            var db = PFSDBUtility.NewPfsDataContext();
            var query = from item in db.ENCOUNTER_ICD_DXes
                        where (item.ENCOUNTER_ID == _pat.encounter_id)
                        where (item.CODE.ToUpper().StartsWith("Z3A"))
                        select item;
            _icd_items = query.ToArray();

            int pos;
            string wks;
            double gwks = 0;
            // Convert all results to lower case for case insensitive comparisons
            foreach (var item in _icd_items)
            {
                pos = item.CODE.IndexOf(".");
                if (item.CODE.Length >= pos + 1)
                {
                    wks = item.CODE.Substring(pos + 1);
                    if (wks.IsNumeric())
                    {
                        gwks = wks.Val();
                        if (gwks <= 36 || gwks >= 43)
                            SetInd(18, "Gestation weeks=" + wks);
                    }
                }
            }

        }
        private void Check_19(int s)
        {
            string reslist, codelist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P2 19. Multiple Pregnancy");
            Program.VerboseAudit("---------------");

            _use_stage_query = false;
            SearchDepth d = SearchDepth.SearchSinceAdmission;

            bool ok = (_stages[s].stage == Stages.ANT ||
                       _stages[s].stage == Stages.DEL);
            if (ok)
            {
                reslist = "Yes,1";
                SetIndIfResultContains(19, "", "9990000012018", "", "", reslist, d);
                reslist = "Yes,1";
                SetIndIfResultContains(19, "", "9991025664455", "", "", reslist, d);
                reslist = "Normal,Bradycardia,Tachycardia,Indeterminate";
                SetIndIfResultContains(19, "", "1028000008", "", "", reslist, d);
                reslist = "Normal,Bradycardia,Tachycardia,Indeterminate";
                SetIndIfResultContains(19, "", "1028000009", "", "", reslist, d);
                reslist = "Normal,Bradycardia,Tachycardia,Indeterminate";
                SetIndIfResultContains(19, "", "9991020100648", "", "", reslist, d);
                reslist = "Normal,Bradycardia,Tachycardia,Indeterminate";
                SetIndIfResultContains(19, "", "9991020100657", "", "", reslist, d);
                reslist = "Minimal,Moderate,Marked,Absent";
                SetIndIfResultContains(19, "", "1028000011", "", "", reslist, d);
                reslist = "Minimal,Moderate,Marked,Absent";
                SetIndIfResultContains(19, "", "1028000012", "", "", reslist, d);
                reslist = "Category I,Category II,Category III";
                SetIndIfResultContains(19, "", "1028000023", "", "", reslist, d);
                reslist = "Category I,Category II,Category III";
                SetIndIfResultContains(19, "", "1028000024", "", "", reslist, d);
                reslist = "Category I,Category II,Category III";
                SetIndIfResultContains(19, "", "9991020100652", "", "", reslist, d);
                reslist = "Category I,Category II,Category III";
                SetIndIfResultContains(19, "", "9991020100661", "", "", reslist, d);


            }
        }

        private void Check_20(int s)
        {
            string reslist, codelist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P2 20. Cesarean Birth");
            Program.VerboseAudit("---------------");

            _use_stage_query = false;

            bool ok = (_stages[s].stage == Stages.DEL);
            if (ok)
            {
                var query = StartNewQuery();
                query = query.Where(e => (e.CODE == "1028000030" || e.CODE == "9991027100281" || e.CODE == "9991027100282")
                                           ||
                                         (e.CODE.ToUpper().StartsWith("SCH") && e.DESCRIPTION.ToUpper().Contains("CESAREAN")));
                query = query.Where(e => !e.DESCRIPTION.ToLower().Contains(";;;schedulingstatus"));
                _cesareanbirth = (query.Count() > 0);

                if (_cesareanbirth)
                {
                    string found_what = query.First().DESCRIPTION + " at " + query.First().EVENT_DATETIME.ToString();
                    SetInd(20, "Cesarean data found: " + found_what);
                }
            }
        }


        private bool AnalyzeBuckets(List<gBucket> buckets, int ind, int bucketsize, string group)
        {
            return AnalyzeBuckets(buckets, ind, bucketsize, group, true);

        }

        private bool AnalyzeBuckets(List<gBucket> buckets, int ind, int bucketsize, string group, bool set_ind)
        {
            DateTime dt = DateTime.MinValue;
            DateTime firstdt = DateTime.MinValue;

            bool all_ok = OLDAnalyzeBuckets(buckets, ind, bucketsize, group, set_ind);
            return all_ok;

        }

        private bool OLDAnalyzeBuckets(List<gBucket> buckets, int ind, int bucketsize, string group, bool set_ind)
        {
            DateTime dt = DateTime.MinValue;
            int bnum = 0;
            int numbucket = -99;
            int lobucket = 99;
            int hibucket = 0;
            int numitems = 0;
            int numconsec = 0;
            int greatestnumconsec = 0;
            int num_unique_times = 0;
            DateTime prev_time = DateTime.MinValue;
            bool all_ok = false;
            List<gBucket> dtlist = new List<gBucket>();
            Program.VerboseAudit("----Begin Assessment Group = " + group + " ---- bucketsizew=" + bucketsize + " start time of first bucket=" + _pat.pull_start);

            //Program.VerboseAudit("buckets count=" + buckets.Count());
            var b = buckets.OrderBy(e => e.evdt).ToList();

            //numitems = buckets.Count();
            foreach (var item in b)
            {
                if (prev_time != item.evdt)
                {
                    num_unique_times++;
                    prev_time = item.evdt;
                }
                if (numbucket < item.bucket)
                {
                    numbucket = item.bucket;
                    numitems++;
                    Program.VerboseAudit(item.bucket + ") " + item.evdt.ToString() + " := " + item.code);
                }
            }

            int num_buckets_in_los = (int)(_pat.los_hours * (60.0 / bucketsize));//for q30 this will be 2xlos_hours.  for q60 this will be los_hours
            Program.VerboseAudit("total bucket count in LOS=" + num_buckets_in_los);
            Program.VerboseAudit("num buckets filled=" + numitems);
            //Program.VerboseAudit("half-LOS bucket count=" + half_los + " at " + bucketsize + "mins each.");
            //Program.VerboseAudit("greatest consec=" + greatestnumconsec);

            //double bucketratio = (hibucket-lobucket) / (1.0 * (bnum-1));
            //q30 75% of 30-min buckets	3/4 x 24  = 18	3/4 x 12  = 9	3/4 x 8  = 6	3/4 x 4  = 3	5 or more VS
            //q1 66% of 60-min buckets	2/3 x 12  = 8	2/3 x 6  = 4	2/3 x 4  = 2.67=>3	2/3 x 2  = 1.33=>1	3 VS + examine 30min 
            //q2 50 % of 60-min buckets	.5 x 12  = 6	.5 x 6  = 3	.5 x 4  = 2	.5 x 2  = 1	2 VS
            //q4 33% of 60-min buckets	1/3 x 12  = 4 2	1/3 x 6  = 2 1	1/3 x 4  = 1.33=>1	1/3 x 2  = .67=>1	1 VS
            int q1need = (int)Math.Round(0.667 * _pat.los_hours);
            int q2need = (int)Math.Round(0.5 * _pat.los_hours);
            int q4need = 1;
            //           12 = q4
            // 6 = q2    13 = q2
            // 7 = q1    14 = q1
            // 8 = q30 
            // 9 = q15

            if (ind <= 14 && ind >= 12)
            {
                if (_pat.los_hours > 5)
                {
                    if (_pat.los_hours < 8)
                    {
                        q4need = 1;
                        q2need = 2; //Jan27 2021
                    }
                    else
                    {
                        q4need = 2;
                        q2need = 4; // Feb3 2021 4;//Jan27 2021
                    }

                    if (numitems >= q1need)
                    {
                        SetInd(14 * Convert.ToInt32(set_ind), "Qualifies for q1 because numcharted=" + numitems + " is >=" + q1need + " LOS=" + Math.Round(_pat.los_hours, 2) + " x .667=" + q1need);
                        all_ok = (ind <= 14);
                    }
                    else if (numitems >= q2need)
                    {
                        //Jan27 2021 SetInd(16 * Convert.ToInt32(set_ind), "Qualifies for q2 because numcharted=" + numitems + " is >=" + q2need + " LOS=" + Math.Round(_pat.los_hours, 2) + " x .5=" + q2need);
                        SetInd(13 * Convert.ToInt32(set_ind), "Qualifies for q2 because numcharted=" + numitems + " is >=" + q2need + " for LOS=" + Math.Round(_pat.los_hours, 2) + "  >=8hrs needs 5 else 2");
                        all_ok = (ind <= 13);
                    }
                    else if (numitems >= q4need)
                    {
                        SetInd(12 * Convert.ToInt32(set_ind), "Qualifies for q4 because numcharted=" + numitems + " is >=" + q4need + " LOS=" + Math.Round(_pat.los_hours, 2) + "  >=8hrs needs 2 else 1");
                        all_ok = (ind <= 12);
                    }
                }
                else //short los
                {
                    q1need = 3;
                    q2need = 2;
                    q4need = 1;

                    if (numitems >= q1need)
                    {
                        SetInd(14 * Convert.ToInt32(set_ind), "ShortLOS Qualifies for q1 because numcharted=" + numitems + " is >=" + q1need + " LOS=" + Math.Round(_pat.los_hours, 2) + " needs just 3");
                        all_ok = (ind <= 14);
                    }
                    else if (numitems >= q2need)
                    {
                        SetInd(13 * Convert.ToInt32(set_ind), "ShortLOS Qualifies for q2 because numcharted=" + numitems + " is >=" + q2need + " LOS=" + Math.Round(_pat.los_hours, 2) + " needs just 2");
                        all_ok = (ind <= 13);
                    }
                    else if (numitems >= q4need)
                    {
                        SetInd(12 * Convert.ToInt32(set_ind), "ShortLOS Qualifies for q4 because numcharted=" + numitems + " is >=" + q4need + " LOS=" + Math.Round(_pat.los_hours, 2) + " needs just 1");
                        all_ok = (ind <= 12);
                    }

                }
            }
            else if (ind <= 7 && ind >= 6)
            {

                { //6-7
                    if (_pat.los_hours > 5)
                    {
                        if (_pat.los_hours < 8)
                        {
                            q2need = 2; //Jan27 2021
                        }
                        else
                        {
                            q2need = 4; // Feb3 2021 4;//Jan27 2021
                        }

                        if (numitems >= q1need)
                        {
                            SetInd(7 * Convert.ToInt32(set_ind), "Qualifies for q1 because numcharted=" + numitems + " is >=" + q1need + " LOS=" + Math.Round(_pat.los_hours, 2) + " x .667=" + q1need);
                            all_ok = (ind <= 7);
                        }
                        else if (numitems >= q4need)
                        {
                            SetInd(6 * Convert.ToInt32(set_ind), "Qualifies for q2 because numcharted=" + numitems + " is >=" + q2need + " for LOS=" + Math.Round(_pat.los_hours, 2) + "  >=8hrs needs 5 else 2");
                            all_ok = (ind <= 6);
                        }
                    }
                    else //short los
                    {
                        q1need = 3;
                        q2need = 2;
                        q4need = 1;

                        if (numitems >= q1need)
                        {
                            SetInd(7 * Convert.ToInt32(set_ind), "ShortLOS Qualifies for q1 because numcharted=" + numitems + " is >=" + q1need + " LOS=" + Math.Round(_pat.los_hours, 2) + " needs just 3");
                            all_ok = (ind <= 7);
                        }
                        else if (numitems >= q2need)
                        {
                            SetInd(6 * Convert.ToInt32(set_ind), "ShortLOS Qualifies for q2 because numcharted=" + numitems + " is >=" + q2need + " LOS=" + Math.Round(_pat.los_hours, 2) + " needs just 2");
                            all_ok = (ind <= 6);
                        }

                    }

                } // 6-7


            } // if 6-7
            else if (ind <= 9 && ind >= 8)
            {
                int q15need = (int)Math.Round(0.75 * 4 * _pat.los_hours);
                int q30need = (int)Math.Round(0.75 * 2 * _pat.los_hours);
                if (_pat.los_hours > 2)
                {
                    if (numitems >= q15need)
                    {
                        SetInd(9 * Convert.ToInt32(set_ind), "Qualifies for q15 because numcharted=" + numitems + " is >=" + q15need + " LOS=" + Math.Round(_pat.los_hours, 2) + " x 2 x .75=" + q15need);
                        all_ok = (ind <= 9);
                    }
                    else if (numitems >= q30need)
                    {
                        SetInd(8 * Convert.ToInt32(set_ind), "Qualifies for q30 because numcharted=" + numitems + " is >=" + q30need + " LOS=" + Math.Round(_pat.los_hours, 2) + " x 2 x .75=" + q30need);
                        all_ok = (ind <= 8);
                    }
                }
                else  //for los<=2 need 3 items for q30 is 
                {
                    q15need = 6;
                    q30need = 3;
                    if (num_unique_times >= q15need)
                    {
                        SetInd(9 * Convert.ToInt32(set_ind), "ShortLOS Qualifies for q30 because numcharted=" + numitems + " is >=" + q15need + " LOS=" + Math.Round(_pat.los_hours, 2) + " with num_unique_times=" + num_unique_times + " Need:" + q15need);
                        all_ok = (ind <= 9);
                    }
                    else if (num_unique_times >= q30need)
                    {
                        SetInd(8 * Convert.ToInt32(set_ind), "ShortLOS Qualifies for q30 because numcharted=" + numitems + " is >=" + q30need + " LOS=" + Math.Round(_pat.los_hours, 2) + " with num_unique_times=" + num_unique_times + " Need:" + q30need);
                        all_ok = (ind <= 8);
                    }

                }

            }
            Program.VerboseAudit("---- End Assessment Group = " + group + " ----");
            return all_ok;
        }

        private bool FHRpresent()
        {
            string codelist = "1223409";
            bool ret = Exists("", codelist, "", "", "");
            return ret;
        }
        private bool CountFHRAssessments(int ind, int bucket_size, out bool foundany)
        {
            int ct;
            string codelist;
            string reslist;
            List<gBucket> buckets;
            bool ret = false;

            SetBucketSize(bucket_size);

            //FHR group
            buckets = new List<gBucket>();
            //            codelist = "1028000001,9990102521012";
            codelist = "1223409";
            AddBuckets(buckets, "", codelist, "", "");
            ShowBuckets(buckets);
            AnalyzeBuckets(buckets, ind, bucket_size, "FHR", true);
            ret = (_stages[ind_stage]._sinds[12].is_checked || _stages[ind_stage]._sinds[13].is_checked || _stages[ind_stage]._sinds[14].is_checked);
            _stages[ind_stage]._sinds[14].is_checked = false;
            _stages[ind_stage]._sinds[13].is_checked = false;
            _stages[ind_stage]._sinds[12].is_checked = false;
            if (ret)
                foundany = true;
            else
                foundany = Exists("", codelist, "", "", "");
            return ret;
        }
        private bool CountTOCOAssessments(int ind, int bucket_size, out bool foundany)
        {
            int ct;
            string codelist;
            string reslist;
            List<gBucket> buckets;
            bool ret = false;

            SetBucketSize(bucket_size);

            //FHR group
            buckets = new List<gBucket>();
            //            codelist = "1028000001,9990102521012";
            codelist = "122337";
            AddBuckets(buckets, "", codelist, "", "");
            ShowBuckets(buckets);
            AnalyzeBuckets(buckets, ind, bucket_size, "TOCO", true);
            ret = (_stages[ind_stage]._sinds[12].is_checked || _stages[ind_stage]._sinds[13].is_checked || _stages[ind_stage]._sinds[14].is_checked);
            _stages[ind_stage]._sinds[14].is_checked = false;
            _stages[ind_stage]._sinds[13].is_checked = false;
            _stages[ind_stage]._sinds[12].is_checked = false;
            if (ret)
                foundany = true;
            else
                foundany = Exists("", codelist, "", "", "");
            return ret;
        }

        private bool CountMedAssessments(int ind, int bucket_size)
        {
            int ct;
            string codelist;
            string reslist;
            List<gBucket> buckets;
            bool ret = false;

            SetBucketSize(bucket_size);

            ////
            ////Diabetic Med group
            ////
            buckets = new List<gBucket>();
            codelist = "9990000344170,3040007191,304000344160,30400012412,304000344150,ORD06";
            AddBuckets(buckets, "", codelist, "", "");
            AddMedBucketsDrugClass33(buckets);
            codelist = "9993040000345";
            AddBuckets(buckets, "", codelist, "", "", "fruit juice,iv/medication");
            AnalyzeBuckets(buckets, ind, bucket_size, "med Diabetic Mgmt", true);


            buckets = new List<gBucket>();
            codelist = "9991070011101,9991600100259,99910701000111,9991733000016,9993040100610";
            AddBuckets(buckets, "", codelist, "", "");
            bool q4 = AddMedBuckets(buckets);
            if (q4) SetInd(12, "Med Mgt q4 based on med found for Pain/Withdrawal");
            AnalyzeBuckets(buckets, ind, bucket_size, "med Pain/Withdrawal", true);

            buckets = new List<gBucket>();
            codelist = "DIPS,ROUI,BDFPH,UPHB";
            AddBuckets(buckets, "", codelist, "", "", "");
            AddGeneralMedBuckets(buckets);
            AnalyzeBuckets(buckets, ind, bucket_size, "med General Med", true);


            buckets = new List<gBucket>();
            bool obmeds = AddOBMedBuckets(buckets);
            //AnalyzeBuckets(buckets, ind, bucket_size, "OB Med", true);
            if (obmeds) SetInd(14, "med OB Meds present.");

            return ret;

        }

        private void AddMedBucketsDrugClass33(List<gBucket> bucket_list)
        {
            string descript = "";
            string drugclass = "";
            string rte = "";
            string result = "";

            bool bres1 = false;
            bool bres2 = false;
            bool bclass1 = false;
            bool bclass2 = false;
            bool bclass3 = false;
            bool bmed1 = false;

            var query = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
            query = query.Where(e => e.CODE.ToLower().StartsWith("med"));
            //query = query.Where(e => e.DESCRIPTION.Contains(";;;7;;;") || e.DESCRIPTION.Contains(";;;33;;;") || e.DESCRIPTION.Contains(";;;1;;;") || e.DESCRIPTION.Contains(";;;2;;;") || e.DESCRIPTION.Contains(";;;6;;;"));
            query = query.Where(e => e.DESCRIPTION.Contains(";;;33;;;"));
            query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;given") || e.DESCRIPTION.ToLower().Contains(";;;syringe") || e.DESCRIPTION.ToLower().ContainsAny(meds_rate) || e.DESCRIPTION.ToLower().Contains(";;;new"));
            //query = query.Where(e => e.EVENT_DATETIME <= PFSUtility.MaxDateTime(_pat.unit_departure, _pat.effective_out));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            //            Program.VerboseAudit("Num Meds found: " + query.Count());
            foreach (var item in query)
            {
                var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                if (arr.GetUpperBound(0) >= 3)
                {
                    descript = arr[0];
                    drugclass = arr[1];
                    rte = arr[3];
                    result = "";
                    if (arr.Length == 5) result = arr[4];
                }
                //Program.VerboseAudit("med desc: " + descript + "//" + "med class: " + drugclass + "//" + "med result: " + result);
                //exclude #29, 35, 48
                bres1 = false;
                bres2 = false;
                bclass1 = false;
                bclass2 = false;
                bclass3 = false;
                bmed1 = false;

                bres1 = result.ToLower().Contains("given");
                bclass3 = (drugclass == "33") && (rte.ToLower() == "sc");
                if (bres1 && bclass3)
                {

                    var b = new gBucket();
                    b.bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size);
                    b.code = item.CODE;
                    b.evdt = item.EVENT_DATETIME;
                    b.has_all_deps = true;
                    gBucket f = bucket_list.Find(x => x.evdt == item.EVENT_DATETIME);
                    if (f.evdt != item.EVENT_DATETIME) bucket_list.Add(b);
                }
            }
        }
        private bool AddMedBuckets(List<gBucket> bucket_list)
        {
            bool ret = false;
            string descript = "";
            string drugclass = "";
            string rte = "";
            string result = "";

            bool bres1 = false;
            bool bres2 = false;
            bool bclass1 = false;
            bool bclass2 = false;
            bool bclass3 = false;
            bool bmed1 = false;
            Program.VerboseAudit("Add Med Buckets...");

            string[] medclass = { "1", "2", "6" };

            string[] pamed1417 = { "Zofran","Ondansetron","Compazine","Prochlorperazine",
"Phenergan","Promethazine","Reglan","Metoclopramide","Marinol","Dronabinol","Droperidol",
"Bupivacaine","Ropivacaine","Naloxone"};

            var query = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
            query = query.Where(e => e.CODE.ToLower().StartsWith("med"));
            query = query.Where(e =>
                medclass.Any(item1 => e.DESCRIPTION.Contains(";;;" + item1 + ";;;")) ||
               pamed1417.Any(item1 => e.DESCRIPTION.ToUpper().StartsWith(item1.ToUpper())));
            query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;given") || e.DESCRIPTION.ToLower().Contains(";;;syringe") || e.DESCRIPTION.ToLower().ContainsAny(meds_rate) || e.DESCRIPTION.ToLower().Contains(";;;new"));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            Program.VerboseAudit("Num PA-MED-12,14,17 Meds found: " + query.Count());
            foreach (var item in query)
            {
                var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                if (arr.GetUpperBound(0) >= 3)
                {
                    descript = arr[0];
                    drugclass = arr[1];
                    rte = arr[3];
                    result = "";
                    if (arr.Length == 5) result = arr[4];
                }
                Program.VerboseAudit("PA-MED-12,14,17 med desc: " + descript + "//" + "med class: " + drugclass + "//" + "med result: " + result);
                //exclude #29, 35, 48
                bres1 = false;
                bres2 = false;
                bclass1 = false;
                bclass2 = false;
                bclass3 = false;
                bmed1 = false;

                bres1 = result.ToLower().Contains("given");
                bres2 = result.ToLower().Contains("syringe") || result.ToLower().ContainsAny(meds_rate) || result.ToLower().Contains("newbag") || result.ToLower().Contains("new bag");
                bclass1 = (drugclass == "1") || (drugclass == "2") || (drugclass == "6");
                bclass2 = false; //(drugclass == "7");
                bclass3 = false;  //(drugclass == "33") && (rte.ToLower() == "sc");
                bmed1 = pamed1417.Any(item2 => descript.ToUpper().StartsWith(item2.ToUpper()));
                if (
                    ((bres1 || bres2) && bclass1) ||
                    bmed1)
                {
                    Program.VerboseAudit("PA-MED-12,14,17 med desc: " + descript + "//" + "med class: " + drugclass + "//" + "med result: " + result);
                    ret = true;
                    var b = new gBucket();
                    b.bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size);
                    b.code = item.CODE;
                    b.evdt = item.EVENT_DATETIME;
                    b.has_all_deps = true;
                    gBucket f = bucket_list.Find(x => x.evdt == item.EVENT_DATETIME);
                    if (f.evdt != item.EVENT_DATETIME) bucket_list.Add(b);
                }
            }
            return ret;
        }
        private void AddGeneralMedBuckets(List<gBucket> bucket_list)
        {
            string descript = "";
            string drugclass = "";
            string rte = "";
            string result = "";

            bool bres1 = false;
            bool bres2 = false;
            bool bclass1 = false;
            bool bclass2 = false;
            bool bclass3 = false;
            bool bmed1 = false;

            string[] medclass = { "7" };

            string[] pamed1417 = { "Ativan",
"Lorazepam","Imodium","Loperamide","Pepto-Bismol","Kaopectate","Bismuth subsalicylate",
"Lomotil","DIPHENOXYLATE-ATROPINE","OPIUM TINCTURE","Lactulose","Labetalol","Dexamethasone"};

            var query = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
            query = query.Where(e => e.CODE.ToLower().StartsWith("med"));
            //query = query.Where(e => e.DESCRIPTION.Contains(";;;7;;;") || 
            //                         e.DESCRIPTION.Contains(";;;33;;;") || 
            //                         e.DESCRIPTION.Contains(";;;1;;;") || 
            //                         e.DESCRIPTION.Contains(";;;2;;;") || 
            //                         e.DESCRIPTION.Contains(";;;6;;;") || 
            query = query.Where(e =>
                medclass.Any(item1 => e.DESCRIPTION.Contains(";;;" + item1 + ";;;")) ||
               pamed1417.Any(item2 => e.DESCRIPTION.ToUpper().StartsWith(item2.ToUpper())));
            query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;given") || e.DESCRIPTION.ToLower().Contains(";;;syringe") || e.DESCRIPTION.ToLower().ContainsAny(meds_rate) || e.DESCRIPTION.ToLower().Contains(";;;new"));
            //query = query.Where(e => e.EVENT_DATETIME <= PFSUtility.MaxDateTime(_pat.unit_departure, _pat.effective_out));
            query = query.OrderBy(e => e.EVENT_DATETIME);

            // get the chart items for the assessments
            query = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
            query = query.Where(e => e.CODE.ToLower().StartsWith("med"));
            //query = query.Where(e => e.DESCRIPTION.Contains(";;;7;;;") || e.DESCRIPTION.Contains(";;;33;;;") || e.DESCRIPTION.Contains(";;;1;;;") || e.DESCRIPTION.Contains(";;;2;;;") || e.DESCRIPTION.Contains(";;;6;;;"));
            query = query.Where(e =>
               medclass.Any(item1 => e.DESCRIPTION.Contains(";;;" + item1 + ";;;")) ||
               pamed1417.Any(item2 => e.DESCRIPTION.ToUpper().StartsWith(item2.ToUpper())));
            query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;given") || e.DESCRIPTION.ToLower().Contains(";;;syringe") || e.DESCRIPTION.ToLower().ContainsAny(meds_rate) || e.DESCRIPTION.ToLower().Contains(";;;new"));
            //query = query.Where(e => e.EVENT_DATETIME <= PFSUtility.MaxDateTime(_pat.unit_departure, _pat.effective_out));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            //            Program.VerboseAudit("Num Meds found: " + query.Count());
            foreach (var item in query)
            {
                var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                if (arr.GetUpperBound(0) >= 3)
                {
                    descript = arr[0];
                    drugclass = arr[1];
                    rte = arr[3];
                    result = "";
                    if (arr.Length == 5) result = arr[4];
                }
                //Program.VerboseAudit("med desc: " + descript + "//" + "med class: " + drugclass + "//" + "med result: " + result);
                //exclude #29, 35, 48
                bres1 = false;
                bres2 = false;
                bclass1 = false;
                bclass2 = false;
                bclass3 = false;
                bmed1 = false;

                bres1 = result.ToLower().Contains("given");
                bres2 = result.ToLower().Contains("syringe") || result.ToLower().ContainsAny(meds_rate) || result.ToLower().Contains("newbag") || result.ToLower().Contains("new bag");
                bclass1 = false; //(drugclass == "1") || (drugclass == "2") || (drugclass == "6");
                bclass2 = (drugclass == "7");
                bclass3 = false;  //(drugclass == "33") && (rte.ToLower() == "sc");
                bmed1 = pamed1417.Any(item2 => descript.ToUpper().StartsWith(item2.ToUpper()));
                if ((bres1 && (bclass2 || bclass3)) ||
                    ((bres1 || bres2) && bclass1) ||
                    bmed1)
                {
                    var b = new gBucket();
                    b.bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size);
                    b.code = item.CODE;
                    b.evdt = item.EVENT_DATETIME;
                    b.has_all_deps = true;
                    gBucket f = bucket_list.Find(x => x.evdt == item.EVENT_DATETIME);
                    if (f.evdt != item.EVENT_DATETIME) bucket_list.Add(b);
                }
            }
        }
        private bool AddOBMedBuckets(List<gBucket> bucket_list)
        {
            string descript = "";
            string drugclass = "";
            string rte = "";
            string result = "";

            bool bres1 = false;
            bool bres2 = false;
            bool bclass1 = false;
            bool bclass2 = false;
            bool bclass3 = false;
            bool bmed1 = false;
            bool foundpamed1417 = false;

            //string[] medclass = { "7" };

            string[] pamed1417 = { "Misoprostol", "Oxytocin", "Magnesium Sulfate", "Carboprost" };

            var query = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
            query = query.Where(e => e.CODE.ToLower().StartsWith("med"));
            query = query.Where(e =>
               //medclass.Any(item1 => e.DESCRIPTION.Contains(";;;" + item1 + ";;;")) ||
               pamed1417.Any(item2 => e.DESCRIPTION.ToLower().StartsWith(item2.ToLower())));
            query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;given") || e.DESCRIPTION.ToLower().Contains(";;;syringe") || e.DESCRIPTION.ToLower().ContainsAny(meds_rate) || e.DESCRIPTION.ToLower().Contains(";;;new"));
            //query = query.Where(e => e.EVENT_DATETIME <= PFSUtility.MaxDateTime(_pat.unit_departure, _pat.effective_out));
            query = query.OrderBy(e => e.EVENT_DATETIME);

            //// get the chart items for the assessments
            //query = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
            //query = query.Where(e => e.CODE.ToLower().StartsWith("med"));
            ////query = query.Where(e => e.DESCRIPTION.Contains(";;;7;;;") || e.DESCRIPTION.Contains(";;;33;;;") || e.DESCRIPTION.Contains(";;;1;;;") || e.DESCRIPTION.Contains(";;;2;;;") || e.DESCRIPTION.Contains(";;;6;;;"));
            //query = query.Where(e =>
            //   //medclass.Any(item1 => e.DESCRIPTION.Contains(";;;" + item1 + ";;;")) ||
            //   pamed1417.Any(item2 => e.DESCRIPTION.ToUpper().StartsWith(item2.ToUpper())));
            //query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;given") || e.DESCRIPTION.ToLower().Contains(";;;syringe") || e.DESCRIPTION.ToLower().ContainsAny(meds_rate) && (e.UNIT_ID == _pat.unit_id) || e.DESCRIPTION.ToLower().Contains(";;;new"));
            ////query = query.Where(e => e.EVENT_DATETIME <= PFSUtility.MaxDateTime(_pat.unit_departure, _pat.effective_out));
            //query = query.OrderBy(e => e.EVENT_DATETIME);
            Program.VerboseAudit("Num OB Meds found: " + query.Count() + " _pat.unit_id=" + _pat.unit_id);
            foreach (var item in query)
            {
                var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                if (arr.GetUpperBound(0) >= 3)
                {
                    descript = arr[0];
                    drugclass = arr[1];
                    rte = arr[3];
                    result = "";
                    if (arr.Length == 5) result = arr[4];
                }
                Program.VerboseAudit("med desc: " + descript + "//" + "med class: " + drugclass + "//" + "med result: " + result);
                //exclude #29, 35, 48
                bres1 = false;
                bres2 = false;
                bclass1 = false;
                bclass2 = false;
                bclass3 = false;
                bmed1 = false;

                bres1 = result.ToLower().Contains("given");
                bres2 = result.ToLower().Contains("syringe") || result.ToLower().ContainsAny(meds_rate) || result.ToLower().Contains("newbag") || result.ToLower().Contains("new bag");
                bclass1 = false; //(drugclass == "1") || (drugclass == "2") || (drugclass == "6");
                bclass2 = false; //(drugclass == "7");
                bclass3 = false;  //(drugclass == "33") && (rte.ToLower() == "sc");
                bmed1 = pamed1417.Any(item2 => descript.ToUpper().StartsWith(item2.ToUpper()));
                foundpamed1417 |= bmed1;
                if ((bres1 && (bclass2 || bclass3)) ||
                    ((bres1 || bres2) && bclass1) ||
                    bmed1)
                {
                    var b = new gBucket();
                    b.bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size);
                    b.code = item.CODE;
                    b.evdt = item.EVENT_DATETIME;
                    b.has_all_deps = true;
                    gBucket f = bucket_list.Find(x => x.evdt == item.EVENT_DATETIME);
                    if (f.evdt != item.EVENT_DATETIME) bucket_list.Add(b);
                }
            }
            return foundpamed1417;
        }
        // (this isn't really a Q1h count -- it is just a count) Make this a q1hr count
        //private int ReturnQ1HrCount(string code_list, string result_list)
        //{
        //    var buckets = new List<gBucket>();
        //    SetBucketSize(60);
        //    AddBuckets(buckets, "", code_list, "", "", result_list);
        //    return CountBuckets(buckets);
        //}

        //private bool IsQ1Hour(int count)
        //{
        //    return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q1H);
        //}
        //private bool IsQ15(int count)
        //{
        //    return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q15M);
        //}


        //private void Check_13()
        //{
        //    string reslist;
        //    int ct = 0;
        //    int medct = 0;

        //    Program.VerboseAudit("---------------");
        //    Program.VerboseAudit("ED Visit 13. Medication Management >= 5 Meds");
        //    Program.VerboseAudit("---------------");

        //    // COUNT OF MEDS > 5 goes here
        //    medct = CountMeds();

        //    reslist = "4 Ounces fruit juice,8 ounces fruit juice,IV/Medication (See MAR)";
        //    ct += CountResultContains("", "9993040000345", "", "", reslist);
        //    ct += CountResultContains("", "9990000344170", "", "", "");
        //    ct += CountResultContains("", "3040007191", "", "", "");

        //    if (ct + medct >= 5) SetInd(13, "Total Med Count=" + (ct + medct));

        //}

        //private int CountMeds()
        //{
        //    string descript;
        //    string drugclass;
        //    int ct = 0;
        //    List<med_data> mlist = new List<med_data>();
        //    bool toadd = true;
        //    bool desc_contains_phrase = false;

        //    var query = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
        //    query = query.Where(e => e.CODE.StartsWith("med"));
        //    query = query.Where(e => e.EVENT_DATETIME <= _pat.unit_departure);
        //    query = query.OrderBy(e => e.EVENT_DATETIME);
        //    Program.VerboseAudit("Num Meds found: " + query.Count());
        //    foreach (var item in query)
        //    {
        //        Program.VerboseAudit("====Med found====");
        //        Program.VerboseAudit("code: " + item.CODE + " desc: " + item.DESCRIPTION + " evdt: " + item.EVENT_DATETIME.ToString());

        //        var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
        //        Program.VerboseAudit("Count Meds ub=" + arr.GetUpperBound(0) + " descript=" + item.DESCRIPTION);
        //        if (arr.GetUpperBound(0) >= 1)
        //        {
        //            descript = arr[0];
        //            drugclass = arr[1];
        //            Program.VerboseAudit("MED desc: " + descript + " drugclass: " + drugclass);
        //            descript = descript.ToLower();
        //            desc_contains_phrase = (descript.Contains("0.9 % SODIUM CHLORIDE".ToLower())
        //                || descript.Contains("D5".ToLower())
        //                || descript.Contains("LACTATED RINGERS".ToLower())
        //                || descript.Contains("NACL 0.45".ToLower())
        //                || descript.Contains("NACL 0.9".ToLower())
        //                || descript.Contains("SALINE FLUSH INJ".ToLower())
        //                || descript.Contains("SODIUM CHLORIDE 0.45".ToLower())
        //                || descript.Contains("SODIUM CHLORIDE 0.9".ToLower()));
        //            //exclude #29 with exceptions, 35, 48
        //            if ((drugclass != "29" || (drugclass == "29" && !desc_contains_phrase)) && (drugclass != "35") && (drugclass != "48"))
        //            {
        //                var md = new med_data();
        //                md.code = item.CODE.ToUpper();
        //                md.descript = descript;
        //                md.drugclass = drugclass;
        //                md.evdt = item.EVENT_DATETIME;
        //                toadd = true;
        //                //if (ct > 0)
        //                //{ 
        //                //    foreach (var m in mlist)
        //                //    {
        //                //        if (m.code == item.CODE.ToUpper()) toadd = false;
        //                //    }
        //                //}
        //                if (toadd && (ct < 5))
        //                {
        //                    mlist.Add(md);
        //                    ct++;
        //                }

        //            }
        //        }
        //    }

        //    int a = 0;
        //    Program.VerboseAudit("Meds found=" + ct);
        //    foreach (var m in mlist)
        //    {
        //        Program.VerboseAudit("  " + a++ + ": code=" + m.code + "; drugclass=" + m.drugclass + "; time=" + m.evdt.ToString() + "; descript=" + m.descript);
        //    }
        //    return ct;
        //}

        //private void CheckAssessment(int count, string desc)
        //{
        //    //if (_inds[12].is_checked) return;          //skip if highest already checked
        //    if (count == 0) return;                    //skip if none

        //    // This should work the same as the original code:
        //    switch (FreqForCount(_pat.los_hours, count))
        //    {
        //        case Frequencies.Q15M:
        //            SetInd(12, desc);
        //            break;
        //        case Frequencies.Q1H:
        //            SetInd(11, desc);
        //            break;
        //        //case Frequencies.Q2H:
        //        //    SetInd(16, desc);
        //        //    break;
        //        //case Frequencies.Q4H:
        //        //    SetInd(15, desc);
        //        //    break;
        //        default:
        //            break;
        //    }

        //}
        private void ShowBuckets(List<gBucket> buckets)
        {
            string s = "";
            foreach (var e in buckets)
            {
                s = "(" + e.bucket + "," + e.code + "," + e.evdt.ToString() + ")";
                Program.VerboseAudit("Assessmt item:=" + s);
            }
        }

        //private bool CheckPIV()
        //{
        //    var query2 = StartNewQuery(SearchDepth.SearchPullPlus);
        //    query2 = AndDescriptionInList(query2, "Placement,Removal,Dressing");
        //    query2 = AndDescriptionInList(query2, "Midline Dual Lumen Catheter,Midline Single Lumen Catheter,Peripheral IV");
        //    query2 = query2.Where(e => e.EVENT_DATETIME <= _pat.unit_departure.AddMinutes(15));
        //    int ct = query2.Count();
        //    return (ct > 0);
        //}

        //=====================================================================
        // Group like assessments into buckets of time
        // Each bucket counts as one assessment in the frequency count

        private void SetBucketSize(int minutes)
        {
            _bucket_size = minutes;
        }

        //Add to the list of bucket numbers (redundant buckets are fine)
        private void AddBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field)
        {
            AddBuckets(bucket_list, cat, code_list, desc, field, "");
        }
        private void AddBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field, string result_list)
        {
            // Assign (virtual) bucket numbers to each assessment.  
            // Every x minutes after that is a new bucket.  Bucket zero = pull_start time.

            // get the chart items for the assessments
            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = AndItemFilter(query, cat, code_list, desc, field, result_list);

            // This step is needed for those who want to count the # within a bucket, like Shands #24.
            // This will have no effect on those that count buckets.
            var query2 = (from item in query select new { item.EVENT_DATETIME, item.CODE });

            // figure out what buckets the events belong to
            var query3 = from item in query2
                         select new
                         {
                             bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                             code = item.CODE,
                             evdt = item.EVENT_DATETIME
                         };
            // Add to the list
            foreach (var item in query3)
            {
                var b = new gBucket();
                b.bucket = item.bucket;
                b.code = item.code;
                b.evdt = item.evdt;
                b.has_all_deps = true;
                bucket_list.Add(b);
            }

            // This is just for auditing: Say what we are looking for and how many were found
            if (!String.IsNullOrEmpty(result_list)) {
                // print each word and if it was found or not
                int i = CountResultContains(cat, code_list, desc, field, result_list);
            } else {
                // print how many were found
                Program.VerboseAudit(Describe(cat, code_list, desc, field, ""));
            }
        } //AddBuckets

        //private void AddBucketsWhereResult(List<gBucket> bucket_list, string code_list, string res_op, int res_val)
        //{
        //    // res_op = GTE LTE
        //    // res_val = some integer

        //    // get the chart items for the assessments
        //    var query = StartNewQuery(SearchDepth.SearchDefault);
        //    query = AndItemFilter(query, "", code_list, "", "", "");

        //    // This step is needed for those who want to count the # within a bucket, like Shands #24.
        //    // This will have no effect on those that count buckets.
        //    var query2 = (from item in query
        //                  where item.RESULT.Val() >= res_val
        //                  select new { item.EVENT_DATETIME, item.CODE });
        //    if (res_op == "LTE")
        //        query2 = (from item in query
        //                      where item.RESULT.Val() <= res_val
        //                      select new { item.EVENT_DATETIME, item.CODE });
        //    else if (res_op == "EQU")
        //        query2 = (from item in query
        //                  where item.RESULT.Val() == res_val
        //                  select new { item.EVENT_DATETIME, item.CODE });


        //    // figure out what buckets the events belong to
        //    var query3 = from item in query2
        //                 select new
        //                 {
        //                     bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
        //                     code = item.CODE,
        //                     evdt = item.EVENT_DATETIME
        //                 };
        //    // Add to the list
        //    foreach (var item in query3)
        //    {
        //        var b = new gBucket();
        //        b.bucket = item.bucket;
        //        b.code = item.code;
        //        b.evdt = item.evdt;
        //        b.has_all_deps = true;
        //        bucket_list.Add(b);
        //    }

        //    Program.VerboseAudit(code_list + (res_op == "LTE" ? "<=" : ">=") + res_val);
        //} //AddBucketsWhereResult

        //private void AddDependentBuckets(List<gBucket> bucket_list, string codelist1, string reslist1, string codelist2, string reslist2)
        //{
        //    AddDependentBuckets(bucket_list, codelist1, reslist1, codelist2, reslist2, "", "");
        //}

        //private void AddDependentBuckets(List<gBucket> bucket_list, string codelist1, string reslist1, string codelist2, string reslist2, string codelist3,string reslist3)
        //{
        //    bool dep3 = true;
        //    // get the chart items for the assessments
        //    var query1 = StartNewQuery();
        //    query1 = AndItemFilter(query1, "", codelist1, "", "", reslist1);
        //    var query2 = StartNewQuery();
        //    query2 = AndItemFilter(query2, "", codelist2, "", "", reslist2);
        //    if (codelist3.Trim() == "")
        //    {
        //        dep3 = false;
        //        codelist3 = "Hello, this is a phantom code";
        //    }
        //    var query3 = StartNewQuery();
        //    query3 = AndItemFilter(query3, "", codelist3, "", "", reslist3);

        //    // figure out what buckets the events belong to
        //    var query1a = from item in query1
        //                  select new
        //                  {
        //                      bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
        //                      code = item.CODE,
        //                      evdt = item.EVENT_DATETIME
        //                  };
        //    var query2a = from item in query2
        //                  select new
        //                  {
        //                      bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
        //                      code = item.CODE,
        //                      evdt = item.EVENT_DATETIME
        //                  };
        //    var query3a = from item in query3
        //                      select new
        //                      {
        //                          bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
        //                          code = item.CODE,
        //                          evdt = item.EVENT_DATETIME
        //                      };

        //    //string s = "BucketList1 for " + codelist1 + ": ";
        //    //foreach (var item in query1a)
        //    //{
        //    //    s += item.bucket + ",";
        //    //}
        //    //Program.VerboseAudit(s);

        //    //s = "BucketList2 for " + codelist2 + ": ";
        //    //foreach (var item in query2a)
        //    //{
        //    //    s += item.bucket + ",";
        //    //}
        //    //if (dep3)
        //    //{
        //    //    s = "BucketList3 for " + codelist3 + ": ";
        //    //    foreach (var item in query3a)
        //    //    {
        //    //        s += item.bucket + ",";
        //    //    }
        //    //}
        //    //Program.VerboseAudit(s);
        //    // Add to the list IFF items in both lists occur in same bucket
        //    foreach (var item1 in query1a)
        //    {
        //        foreach (var item2 in query2a)
        //        {
        //            if (item1.bucket == item2.bucket)
        //            {
        //                if (dep3)
        //                {
        //                    foreach (var item3 in query3a)
        //                    {
        //                        if (item1.bucket == item3.bucket)
        //                        {
        //                            var b = new gBucket();
        //                            b.bucket = item1.bucket;
        //                            b.code = item1.code;
        //                            b.evdt = item1.evdt;
        //                            bucket_list.Add(b);
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    foreach (var item3 in query2a)
        //                    {
        //                        if (item1.bucket == item2.bucket)
        //                        {
        //                            var b = new gBucket();
        //                            b.bucket = item1.bucket;
        //                            b.code = item1.code;
        //                            b.evdt = item1.evdt;
        //                            bucket_list.Add(b);
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }

        //}


        //private int CountBuckets(List<gBucket> bucket_list)       // bucket list: (ha ha)
        //{
        //    int x = -99;
        //    int result = 0;
        //    var query = from b in bucket_list
        //                orderby b.bucket ascending
        //                select b;
        //    foreach (var b in query)
        //    {
        //        if (x != b.bucket)
        //        {
        //            result++;
        //            x = b.bucket;
        //        }
        //    }
        //    if (result > 0) Program.VerboseAudit(result + " unique");
        //    return result;

        //}


        private void AtLeastOneADL()
        {
            Program.Audit("---------------");
            if (!(_inds[2].is_checked || _inds[3].is_checked || _inds[4].is_checked)) {
                // Make "#2 ADL - Assist" the default.  (90% of patients)
                SetInd(2, "Defaulting to ADL Self due to lack of documentation.");
            }
        }

        private void HighestIndicatorInEachGroupWins()
        {
            int i, g;
            bool highest_is_on;
            string ind_list;

            Program.Audit("Select highest indicator in each group");

            g = 0;
            highest_is_on = false;
            // Go from bottom up - highest (lowest) indicator in each group wins
            for (i = MAX_INDS; (i >= 1); i--) {
                if (_stages[ind_stage]._sinds[i].radio_group > 0) {
                    if (_stages[ind_stage]._sinds[i].radio_group != g) {
                        //this is a new group
                        g = _stages[ind_stage]._sinds[i].radio_group;
                        highest_is_on = _stages[ind_stage]._sinds[i].is_checked;
                    } else {
                        //same group
                        if (highest_is_on) {
                            _stages[ind_stage]._sinds[i].is_checked = false;             //uncheck a lower number
                        } else {
                            highest_is_on = _stages[ind_stage]._sinds[i].is_checked;     //save this one
                        }
                    }
                }
            } // next i

            //Echo the indicators for an Audit (no classification will be saved)
            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++) {
                if (_stages[ind_stage]._sinds[i].is_checked) ind_list += "," + i;
            } // next i

            Program.Audit("Final list = [" + ind_list + "]");
            Program.Audit("---------------");
        }



        private void CheckProcedures()
        {
            //CheckProc_1();
            CheckProc_20Amnio1(false);
            CheckProc_20Amnio2(false);
            CheckProc_21ATT(false);
            CheckProc_24Circ(true);
            CheckProc_27DC(true);
            CheckProc_28DE(true);
            CheckProc_25CCP(true);
            CheckProc_26CCR(true);
            CheckProc_29EV(false);
            CheckProc_40OtherSurg(true);
            CheckProc_42Bereave(false);
            CheckProc_43PUBS(true);
            CheckProc_44Safety(false);
            CheckProc_46TL(true);
            CheckProc_48WD(true);

            Program.Audit("shortOPLOS="+shortOPLOS + " procs count="+_procs.Count);
            if (shortOPLOS && _procs.Count > 0) //then add ending Obs&Assess proc.
            {
                var proc = new proc_data();
                proc.procedure_number = 1;
                proc.start = DateTime.MinValue;
                proc.finish = DateTime.MinValue;
                proc.recoverydt = DateTime.MinValue;
                //first find the last proc's finish time.  this will be the start time.
                proc.start = _procs.Last().finish;
                proc.finish = proc.start.AddDays(1);
                Program.Audit("shortOPLOS=true procstart="+proc.start + " procfin="+proc.finish);

                string uname = "";
                for (int i = 1; i <= _pat.num_locs; i++)
                {
                    if (proc.start >= _pat.locary[i].time_in && proc.start <= _pat.locary[i].time_out)
                    {
                        uname = _pat.locary[i].uname;
                    }
                }
                Program.Audit("uname="+uname);
                proc.stage = 0;
                proc.uname = uname;
                proc.is_surg = false;
                _procs.Add(proc);
                Program.Audit("Procedure 1: Terminating Obs&Assess at " + proc.start);
            }

        }

        private int OLDGetStageForProcTime(int procnum, DateTime proctime, out string uname)
        {
            int stage = -1;
            int stage_unit_id = -1;
            uname = "";
            Program.VerboseAudit("in GetStageForProcTime...procnum=" + procnum + "  proctime=" + proctime + "  numclass=" + _pat.num_class);

            if (_pat.num_class == 0)
            {
                for (int s = 0; s <= stageidx; s++)
                {
                    if (_stages[s].start <= proctime && proctime <= _stages[s].finish)
                    {
                        stage = (int)_stages[s].stage;
                        uname = _stages[s].uname;
                        stage_unit_id = _stages[s].unitid;
                    }

                }
            }
            else
            {
                for (int i = _pat.num_class; i >= 1; i--)
                {
                    Program.VerboseAudit("i=" + i + " classary[i].stage=" + _pat.classary[i].stage);
                    if (proctime >= _pat.classary[i].time_in)
                    {
                        if (stage == -1)
                        {
                            stage = _pat.classary[i].stage;
                            uname = _pat.classary[i].uname;
                            stage_unit_id = _pat.classary[i].unit_id;
                        }
                    }
                }
                if (stageidx == 0)
                { 
                    uname = _stages[0].uname;
                }
            }

            Program.VerboseAudit("...targeting class stage=" + stage + " in unit=" + uname + " stageunit_id=" + stage_unit_id);
            bool get_first = false;
            if (stage == -1) get_first = true;

            int ret_stage = -1;
            int last_stage = 0;
            int final_stage = 0;

            bool qfound = false;
            string sql = "select psr.stage from unit as u";
            sql += " inner join unit_param as up on (u.unit_id = up.unit_id)";
            sql += " inner join PROCEDURE_STAFF_RATIO as psr on (up.UNIT_PARAM_ID=psr.UNIT_PARAM_ID)";
            sql += " where getdate() between up.EFFECTIVE_DATETIME and up.EXPIRATION_DATETIME";
            sql += " and u.UNIT_ID=" + stage_unit_id + " and psr.PROCEDURE_NUMBER=" + procnum;
            sql += " order by psr.stage desc";
            Program.VerboseAudit("get procedure info:" + sql);
            var db = PFSDBUtility.NewSqlConnection();
            var cmd = new SqlCommand(sql, db);
            SqlDataReader dr = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr.Read() && !qfound)
            {
                ret_stage = PFSDBUtility.DBToInt(dr[0]);
                last_stage = ret_stage;
                if (get_first || ret_stage <= stage) //assign the nearest matching stage
                {
                    final_stage = ret_stage;
                    qfound = true;
                }
            }
            dr.Close();
            if (!qfound) final_stage = last_stage;
            return final_stage;
        }
        private int GetStageForProcTime(int procnum, DateTime proctime, int uid, out string uname)
        {
            int stage = -1;
            int stage_unit_id = -1;
            uname = "";
            Program.VerboseAudit("in GetStageForProcTime...procnum=" + procnum + "  proctime=" + proctime + "  numclass=" + _pat.num_class);

            //for (int i = 1; i <= _pat.num_locs; i++)
            //{
            //    Program.VerboseAudit("i=" + i + " locary[i].time_in=" + _pat.locary[i].time_in + " locary[i].time_out=" + _pat.locary[i].time_out);
            //    if ((i == 1 && proctime < _pat.locary[i].time_in) || (i == _pat.num_locs && proctime >= _pat.locary[i].time_out))
            //    {
            //        uname = _pat.locary[i].uname;
            //        stage_unit_id = _pat.locary[i].unit_id;
            //        Program.VerboseAudit("...proc time is before first location. using unit=" + uname + " stageunit_id=" + stage_unit_id);
            //        Program.VerboseAudit("...i=" + i + " locary[i].time_in=" + _pat.locary[i].time_in);
            //    }
            //    else if (proctime >= _pat.locary[i].time_in && proctime < _pat.locary[i].time_out)
            //    {
            //        if (stage == -1)
            //        {
            //            stage = 10;
            //            uname = _pat.locary[i].uname;
            //            stage_unit_id = _pat.locary[i].unit_id;
            //            Program.VerboseAudit("...found location unit=" + uname + " stageunit_id=" + stage_unit_id);
            //            Program.VerboseAudit("...i=" + i + " locary[i].time_in=" + _pat.locary[i].time_in);
            //        }
            //    }
            //}

            //Program.VerboseAudit("...targeting class stage=" + stage + " in unit=" + uname + " stageunit_id=" + stage_unit_id);
            bool get_first = false;
            if (stage == -1) get_first = true;

            int ret_stage = -1;
            int last_stage = 0;
            int final_stage = 0;
            string xuname = "";
            bool qfound = false;
            string sql = "select psr.stage,u.name from unit as u";
            sql += " inner join unit_param as up on (u.unit_id = up.unit_id)";
            sql += " inner join PROCEDURE_STAFF_RATIO as psr on (up.UNIT_PARAM_ID=psr.UNIT_PARAM_ID)";
            sql += " inner join FACILITY as f on (u.facility_id=f.facility_id)";
            sql += " where f.classification_facility_code='" + _pat.facilty_code + "'";
            sql += " and u.unit_id=" + uid;
            sql += " and up.methodology_id=27";
            sql += " and " + PFSDBUtility.SQLDateTime(proctime) + " between up.EFFECTIVE_DATETIME and up.EXPIRATION_DATETIME";
            //sql += " and u.UNIT_ID=" + stage_unit_id + " and psr.PROCEDURE_NUMBER=" + procnum;
            //sql += " and psr.STAGE>=" + _pat.stage;
            sql += " and psr.PROCEDURE_NUMBER=" + procnum;
            sql += " order by psr.stage desc";
            Program.VerboseAudit("get procedure info:" + sql);
            var db = PFSDBUtility.NewSqlConnection();
            var cmd = new SqlCommand(sql, db);
            SqlDataReader dr = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr.Read() && !qfound)
            {
                final_stage = PFSDBUtility.DBToInt(dr[0]);
                uname = PFSDBUtility.DBToString(dr[1]);
                qfound = true;

                //ret_stage = PFSDBUtility.DBToInt(dr[0]);
                //xuname = PFSDBUtility.DBToString(dr[1]);
                //last_stage = ret_stage;
                //if (get_first || ret_stage <= stage) //assign the nearest matching stage
                //{
                //    uname = xuname;
                //    final_stage = ret_stage;
                //    qfound = true;
                //}
            }
            dr.Close();
            return final_stage;
        }
        private bool EVDTinLocAry(DateTime evdt)
        {
            bool retval = false;
            for (int i=1; i<=_pat.num_locs; i++)
            {
                retval = retval || (evdt >= _pat.locary[i].time_in && evdt <= _pat.locary[i].time_out);
            }
            if (!retval)
                Program.Audit("Abandon procedure because Start Time is not in a perinatal location=" + evdt);
            return retval;
        }

        private void CheckProc_20Amnio1(bool issurg)
        {
            int mins = 0;
            string found_what;
            DateTime evdt;
            DateTime enddt = DateTime.MinValue;
            int uid = 0;
            int st = -1;
            string uname = "";
            var proc = new proc_data();
            proc.procedure_number = 20;
            proc.start = DateTime.MinValue;
            proc.finish = DateTime.MinValue;
            proc.recoverydt = DateTime.MinValue;

            Program.Audit("Checking 1Amnio20...");

            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = query.Where(e => e.CODE == "9991025032516");
            query = query.Where(e => e.RESULT.ToUpper().Contains("AMNIOINFUSION".ToUpper()));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            int amcount = query.Count();
            Program.Audit("Amnio start records count=" + amcount);

            if (amcount > 0)
            {
                mins = 120;
                evdt = query.First().EVENT_DATETIME;
                uid = query.First().UNIT_ID;
                enddt = evdt.AddMinutes(mins);
                if (!EVDTinLocAry(evdt)) return;

                if (ProcExistsInDB(20, evdt, enddt))
                {
                    Program.Audit("Procedure 20: already exists");
                    return;
                }
                else
                {
                    proc.procedure_number = 20;
                    proc.start = evdt;
                    proc.finish = enddt;
                    st = GetStageForProcTime(proc.procedure_number, evdt, uid, out uname);
                    if (st >= 0)
                    {
                        //stname = Enum.GetName(typeof(Stages), (int)_stages[s].stage);
                        proc.stage = st;
                        proc.uname = uname;
                        proc.is_surg = issurg;
                        _procs.Add(proc);
                        Program.Audit("Procedure 20: Found Amnio between " + evdt + " and " + enddt);
                    }
                }
            }
        } //end proc

        private void CheckProc_20Amnio2(bool issurg)
        {
            int mins = 0;
            string found_what;
            DateTime evdt;
            DateTime enddt = DateTime.MinValue;
            int uid = 0;
            int st = -1;
            string uname = "";
            var proc = new proc_data();
            proc.procedure_number = 20;
            proc.start = DateTime.MinValue;
            proc.finish = DateTime.MinValue;
            proc.recoverydt = DateTime.MinValue;

            Program.Audit("Checking 2Amnio20...");

            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = query.Where(e => e.CODE.ToUpper() == "SCH4686".ToUpper());
            query = query.Where(e => e.DESCRIPTION.ToUpper().Contains("AMNIOINFUSION".ToUpper()));
            query = query.Where(e => (e.DESCRIPTION.ToUpper().Contains(";;;Anes Start".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;In Room".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;Proc Start".ToUpper())));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            int dcstartcount = query.Count();
            Program.Audit("2Amnio start records count=" + dcstartcount);

            var query2 = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = query.Where(e => e.CODE.ToUpper() == "SCH4686".ToUpper());
            query = query.Where(e => e.DESCRIPTION.ToUpper().Contains("AMNIOINFUSION".ToUpper()));
            query2 = query2.Where(e => (e.DESCRIPTION.ToUpper().Contains(";;;Proc Close".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;Proc Finish".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;Out Room".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;Anes Stop".ToUpper())
              ));
            query2 = query2.OrderBy(e => e.EVENT_DATETIME);
            int dcstopcount = query2.Count();
            Program.Audit("2Amnio stop records count=" + dcstopcount);

            if (dcstartcount > 0 && dcstopcount > 0)
            {
                evdt = query.First().EVENT_DATETIME;
                uid = query.First().UNIT_ID;
                enddt = query2.Last().EVENT_DATETIME;
                mins = 120;
                if (!EVDTinLocAry(evdt)) return;

                if (ProcExistsInDB(20, evdt, enddt.AddMinutes(120)))
                {
                    Program.Audit("Procedure 20: already exists");
                    return;
                }
                else
                {
                    proc.procedure_number = 20;
                    proc.start = evdt;
                    proc.recoverydt = enddt;
                    proc.finish = enddt.AddMinutes(120);
                    Program.Audit("Adding procedure 20: start=" + evdt + " recov=" + enddt + " finish=" + proc.finish);

                    st = GetStageForProcTime(proc.procedure_number, evdt, uid, out uname);
                    if (st >= 0)
                    {
                        //stname = Enum.GetName(typeof(Stages), (int)_stages[s].stage);
                        proc.stage = st;
                        proc.uname = uname;
                        proc.is_surg = issurg;
                        _procs.Add(proc);
                        Program.Audit("Procedure 20: Found Amnio2 between " + evdt + " and " + proc.finish);
                    }
                }
            }
        } //end proc


        private void CheckProc_27DC(bool issurg)
        {
            int mins = 0;
            string found_what;
            DateTime evdt;
            DateTime enddt = DateTime.MinValue;
            int uid = 0;
            int st = -1;
            string uname = "";
            var proc = new proc_data();
            proc.procedure_number = 27;
            proc.start = DateTime.MinValue;
            proc.finish = DateTime.MinValue;
            proc.recoverydt = DateTime.MinValue;

            Program.Audit("Checking DC27...");

            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = query.Where(e => e.CODE.ToUpper().StartsWith("SCH".ToUpper()));
            query = query.Where(e => e.DESCRIPTION.ToUpper().Contains("CURETTAGE".ToUpper()));
            query = query.Where(e => (e.DESCRIPTION.ToUpper().Contains(";;;Anes Start".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;In Room".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;Proc Start".ToUpper())));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            int dcstartcount = query.Count();
            Program.Audit("DC start records count=" + dcstartcount);

            var query2 = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query2 = query2.Where(e => e.CODE.ToUpper().StartsWith("SCH".ToUpper()));
            query2 = query2.Where(e => e.DESCRIPTION.ToUpper().Contains("CURETTAGE".ToUpper()));
            query2 = query2.Where(e => (e.DESCRIPTION.ToUpper().Contains(";;;Proc Close".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;Proc Finish".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;Out Room".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;Anes Stop".ToUpper())
              ));
            query2 = query2.OrderBy(e => e.EVENT_DATETIME);
            int dcstopcount = query2.Count();
            Program.Audit("DC stop records count=" + dcstopcount);

            if (dcstartcount > 0 && dcstopcount > 0)
            {
                evdt = query.First().EVENT_DATETIME;
                uid = query.First().UNIT_ID;
                enddt = query2.Last().EVENT_DATETIME;
                mins = 120;
                proc.finish = ExtendProcFinish(enddt);
                if (proc.finish == enddt) proc.finish = enddt.AddMinutes(120);
                if (!EVDTinLocAry(evdt)) return;

                if (ProcExistsInDB(27, evdt, proc.finish))
                {
                    Program.Audit("Procedure 27: already exists");
                    return;
                }
                else
                {
                    proc.procedure_number = 27;
                    proc.start = evdt;
                    proc.recoverydt = enddt;
//                    proc.finish = enddt.AddMinutes(120);
                    Program.Audit("Adding procedure 27: start=" + evdt + " recov=" + enddt + " finish=" + proc.finish);

                    st = GetStageForProcTime(proc.procedure_number, evdt, uid, out uname);
                    if (st >= 0)
                    {
                        //stname = Enum.GetName(typeof(Stages), (int)_stages[s].stage);
                        proc.stage = st;
                        proc.uname = uname;
                        proc.is_surg = issurg;
                        _procs.Add(proc);
                        Program.Audit("Procedure 27: Found DC27 between " + evdt + " and " + proc.finish);
                    }
                }
            }
        } //end proc

        private void CheckProc_28DE(bool issurg)
        {
            int mins = 0;
            string found_what;
            DateTime evdt;
            DateTime enddt = DateTime.MinValue;
            int uid = 0;
            int st = -1;
            string uname = "";
            var proc = new proc_data();
            proc.procedure_number = 27;
            proc.start = DateTime.MinValue;
            proc.finish = DateTime.MinValue;
            proc.recoverydt = DateTime.MinValue;

            Program.Audit("Checking DE28...");

            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = query.Where(e => e.CODE.ToUpper().StartsWith("SCH".ToUpper()));
            query = query.Where(e => e.DESCRIPTION.ToUpper().Contains("EVACUATION".ToUpper()));
            query = query.Where(e => (e.DESCRIPTION.ToUpper().Contains(";;;Anes Start".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;In Room".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;Proc Start".ToUpper())));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            int dcstartcount = query.Count();
            Program.Audit("DE start records count=" + dcstartcount);

            var query2 = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query2 = query2.Where(e => e.CODE.ToUpper().StartsWith("SCH".ToUpper()));
            query2 = query2.Where(e => e.DESCRIPTION.ToUpper().Contains("EVACUATION".ToUpper()));
            query2 = query2.Where(e => (e.DESCRIPTION.ToUpper().Contains(";;;Proc Close".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;Proc Finish".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;Out Room".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;Anes Stop".ToUpper())
              ));
            query2 = query2.OrderBy(e => e.EVENT_DATETIME);
            int dcstopcount = query2.Count();
            Program.Audit("DE stop records count=" + dcstopcount);

            if (dcstartcount > 0 && dcstopcount > 0)
            {
                evdt = query.First().EVENT_DATETIME;
                uid = query.First().UNIT_ID;
                enddt = query2.Last().EVENT_DATETIME;
                mins = 120;
                if (!EVDTinLocAry(evdt)) return;

                if (ProcExistsInDB(28, evdt, enddt.AddMinutes(120)))
                {
                    Program.Audit("Procedure 28: already exists");
                    return;
                }
                else
                {
                    proc.procedure_number = 28;
                    proc.start = evdt;
                    proc.recoverydt = enddt;
                    proc.finish = enddt.AddMinutes(120);
                    Program.Audit("Adding procedure 28: start=" + evdt + " recov=" + enddt + " finish=" + proc.finish);

                    st = GetStageForProcTime(proc.procedure_number, evdt, uid, out uname);
                    if (st >= 0)
                    {
                        //stname = Enum.GetName(typeof(Stages), (int)_stages[s].stage);
                        proc.stage = st;
                        proc.uname = uname;
                        proc.is_surg = issurg;
                        _procs.Add(proc);
                        Program.Audit("Procedure 28: Found DE28 between " + evdt + " and " + proc.finish);
                    }
                }
            }
        } //end proc

        private void CheckProc_21ATT(bool issurg)
        {
            int mins = 0;
            string found_what;
            DateTime enddt;
            DateTime stdt = DateTime.MinValue;
            int uid = 0;
            int st = -1;
            string uname = "";
            var proc = new proc_data();
            proc.procedure_number = 21;
            proc.start = DateTime.MinValue;
            proc.finish = DateTime.MinValue;
            proc.recoverydt = DateTime.MinValue;

            Program.Audit("Checking ATT...");
            //9991025062116 looking for 60 or 30 where the evdt is end tim.
            //start time is 90 or 60 mins earlier

            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = query.Where(e => e.CODE == "9991025062116");
            query = query.OrderBy(e => e.EVENT_DATETIME);
            int count = query.Count();
            Program.Audit("ATT count=" + count);

            if (count > 0)
            {
                enddt = query.First().EVENT_DATETIME;
                uid = query.First().UNIT_ID;
                found_what = query.First().RESULT;
                int duration_mins = (int)found_what.Val();
                if (duration_mins == 30) mins = 60;
                if (duration_mins == 60) mins = 120;
                stdt = enddt.AddMinutes(-mins);
                if (!EVDTinLocAry(stdt)) return;

                if (ProcExistsInDB(21, stdt, enddt))
                {
                    Program.Audit("Procedure 21: already exists");
                    return;
                }
                else
                {
                    proc.procedure_number = 21;
                    proc.start = stdt;
                    proc.finish = enddt;
                    Program.Audit("Adding procedure 21: start=" + stdt + " finish=" + proc.finish);

                    st = GetStageForProcTime(proc.procedure_number, stdt, uid, out uname);
                    if (st >= 0)
                    {
                        //stname = Enum.GetName(typeof(Stages), (int)_stages[s].stage);
                        proc.stage = st;
                        proc.uname = uname;
                        proc.is_surg = issurg;
                        _procs.Add(proc);
                        Program.Audit("Procedure 21: Found ATT21 between " + stdt + " and " + enddt);
                    }
                }
            }
        } //end proc


        private void CheckProc_24Circ(bool issurg)
        {
            int mins = 0;
            string found_what;
            DateTime evdt;
            DateTime enddt = DateTime.MinValue;
            int uid = 0;
            int st = -1;
            string uname = "";
            var proc = new proc_data();
            proc.procedure_number = 24;
            proc.start = DateTime.MinValue;
            proc.finish = DateTime.MinValue;
            proc.recoverydt = DateTime.MinValue;

            Program.Audit("Checking Circ...use_stage_query="+_use_stage_query);
            //9990000026972 looking for Yes  where the evdt is end tim.
            //30 mins later go to Recovery.  105 mins later Depart.
            _use_stage_query = false;

            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = query.Where(e => (e.CODE == "9990000026972" && e.RESULT.ToLower()=="yes")
                            || (e.CODE == "9991140100003" && e.RESULT.ToLower() == "done"));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            int count = query.Count();
            Program.Audit("Circ count=" + count);

            if (count > 0)
            {
                evdt = query.First().EVENT_DATETIME.AddMinutes(-30);
                uid = query.First().UNIT_ID;
                found_what = query.First().RESULT;
                enddt = evdt.AddMinutes(30);
                if (!EVDTinLocAry(evdt))
                {
                    Program.Audit("Abandoning Procedure 24: Time not within location history=" + evdt);
                    return;
                }

                Program.Audit("Procedure 24: Found result=" + found_what);

                if (ProcExistsInDB(24, evdt, enddt.AddMinutes(105)))
                {
                    Program.Audit("Procedure 24: already exists");
                    return;
                }
                else
                {
                    proc.procedure_number = 24;
                    proc.start = evdt;
                    proc.recoverydt = evdt.AddMinutes(30);
                    proc.finish = proc.recoverydt.AddMinutes(105);
                    Program.Audit("Adding procedure 24: start=" + evdt + " recov=" + proc.recoverydt + " finish=" + proc.finish);

                    st = GetStageForProcTime(proc.procedure_number, evdt, uid, out uname);
                    if (st >= 0)
                    {
                        //stname = Enum.GetName(typeof(Stages), (int)_stages[s].stage);
                        proc.stage = st;
                        proc.uname = uname;
                        proc.is_surg = issurg;
                        _procs.Add(proc);
                        Program.Audit("Procedure 24: Found Circ between " + evdt + " and " + proc.finish);
                    }
                }
            }
        } //end proc

        private DateTime ExtendProcFinish(DateTime currfinishdt)
        {
            DateTime newfinishdt = currfinishdt;
            if (_pat.eff_departuredt != DateTime.MinValue
                && _pat.eff_departuredt > currfinishdt)
            {
                if (currfinishdt.AddHours(8) >= _pat.eff_departuredt)
                    newfinishdt = _pat.eff_departuredt;
                else
                    newfinishdt = currfinishdt.AddHours(8);
            }
            return newfinishdt;
        }

        private void CheckProc_25CCP(bool issurg)
        {
            int mins = 0;
            string found_what;
            DateTime evdt;
            DateTime enddt = DateTime.MinValue;
            int uid = 0;
            int st = -1;
            string uname = "";
            var proc = new proc_data();
            proc.procedure_number = 25;
            proc.start = DateTime.MinValue;
            proc.finish = DateTime.MinValue;
            proc.recoverydt = DateTime.MinValue;

            Program.Audit("Checking 25CCP...");

            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = query.Where(e => e.CODE.ToUpper() == "SCH559".ToUpper());
            query = query.Where(e => e.DESCRIPTION.ToUpper().Contains("PLACEMENT CERCLAGE".ToUpper()));
            query = query.Where(e => (e.DESCRIPTION.ToUpper().Contains(";;;Anes Start".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;In Room".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;Proc Start".ToUpper())));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            int dcstartcount = query.Count();
            Program.Audit("CCP start records count=" + dcstartcount);

            var query2 = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query2 = query2.Where(e => e.CODE.ToUpper() == "SCH559".ToUpper());
            query2 = query2.Where(e => e.DESCRIPTION.ToUpper().Contains("PLACEMENT CERCLAGE".ToUpper()));
            query2 = query2.Where(e => (e.DESCRIPTION.ToUpper().Contains(";;;Proc Close".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;Proc Finish".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;Out Room".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;Anes Stop".ToUpper())
              ));
            query2 = query2.OrderBy(e => e.EVENT_DATETIME);
            int dcstopcount = query2.Count();
            Program.Audit("CCP stop records count=" + dcstopcount);

            if (dcstartcount > 0 && dcstopcount > 0)
            {
                evdt = query.First().EVENT_DATETIME;
                uid = query.First().UNIT_ID;
                enddt = query2.Last().EVENT_DATETIME;
                mins = 120;
                proc.finish = ExtendProcFinish(enddt);
                if (proc.finish == enddt) proc.finish = enddt.AddMinutes(120);
                if (!EVDTinLocAry(evdt)) return;

                if (ProcExistsInDB(25, evdt, proc.finish))
                {
                    Program.Audit("Procedure 25: already exists");
                    return;
                }
                else
                {
                    proc.procedure_number = 25;
                    proc.start = evdt;
                    proc.recoverydt = enddt;
                    Program.Audit("Adding procedure 25: start=" + evdt + " recov=" + enddt + " finish=" + proc.finish);

                    st = GetStageForProcTime(proc.procedure_number, evdt, uid, out uname);
                    if (st >= 0)
                    {
                        //stname = Enum.GetName(typeof(Stages), (int)_stages[s].stage);
                        proc.stage = st;
                        proc.uname = uname;
                        proc.is_surg = issurg;
                        _procs.Add(proc);
                        Program.Audit("Procedure 25: Found CCP between " + evdt + " and " + proc.finish);
                    }
                }
            }
        } //end proc

        private void CheckProc_26CCR(bool issurg)
        {
            int mins = 0;
            string found_what;
            DateTime evdt;
            DateTime enddt = DateTime.MinValue;
            int uid = 0;
            int st = -1;
            string uname = "";
            var proc = new proc_data();
            proc.procedure_number = 26;
            proc.start = DateTime.MinValue;
            proc.finish = DateTime.MinValue;
            proc.recoverydt = DateTime.MinValue;

            Program.Audit("Checking 26CCR...");

            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = query.Where(e => e.CODE.ToUpper() == "SCH559".ToUpper());
            query = query.Where(e => e.DESCRIPTION.ToUpper().Contains("REMOVAL CERCLAGE".ToUpper()));
            query = query.Where(e => (e.DESCRIPTION.ToUpper().Contains(";;;Anes Start".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;In Room".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;Proc Start".ToUpper())));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            int dcstartcount = query.Count();
            Program.Audit("CCP start records count=" + dcstartcount);

            var query2 = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query2 = query2.Where(e => e.CODE.ToUpper() == "SCH559".ToUpper());
            query2 = query2.Where(e => e.DESCRIPTION.ToUpper().Contains("REMOVAL CERCLAGE".ToUpper()));
            query2 = query2.Where(e => (e.DESCRIPTION.ToUpper().Contains(";;;Proc Close".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;Proc Finish".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;Out Room".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;Anes Stop".ToUpper())
              ));
            query2 = query2.OrderBy(e => e.EVENT_DATETIME);
            int dcstopcount = query2.Count();
            Program.Audit("CCR stop records count=" + dcstopcount);

            if (dcstartcount > 0 && dcstopcount > 0)
            {
                evdt = query.First().EVENT_DATETIME;
                uid = query.First().UNIT_ID;
                enddt = query2.Last().EVENT_DATETIME;
                mins = 120;
                proc.finish = ExtendProcFinish(enddt);
                if (proc.finish == enddt) proc.finish = enddt.AddMinutes(120);
                if (!EVDTinLocAry(evdt)) return;

                if (ProcExistsInDB(26, evdt, proc.finish))
                {
                    Program.Audit("Procedure 26: already exists");
                    return;
                }
                else
                {
                    proc.procedure_number = 26;
                    proc.start = evdt;
                    proc.recoverydt = enddt;
//                    proc.finish = enddt.AddMinutes(120);
                    Program.Audit("Adding procedure 26: start=" + evdt + " recov=" + enddt + " finish=" + proc.finish);

                    st = GetStageForProcTime(proc.procedure_number, evdt, uid, out uname);
                    if (st >= 0)
                    {
                        //stname = Enum.GetName(typeof(Stages), (int)_stages[s].stage);
                        proc.stage = st;
                        proc.uname = uname;
                        proc.is_surg = issurg;
                        _procs.Add(proc);
                        Program.Audit("Procedure 26: Found CCR between " + evdt + " and " + proc.finish);
                    }
                }
            }
        } //end proc

        private void CheckProc_29EV(bool issurg)
        {
            int mins = 0;
            string found_what;
            DateTime evdt;
            DateTime enddt = DateTime.MinValue;
            int uid = 0;
            int st = -1;
            string uname = "";
            var proc = new proc_data();
            proc.procedure_number = 29;
            proc.start = DateTime.MinValue;
            proc.finish = DateTime.MinValue;
            proc.recoverydt = DateTime.MinValue;

            Program.Audit("Checking 29EV...");

            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = query.Where(e => e.CODE == "9991025081718");
            query = query.OrderBy(e => e.EVENT_DATETIME);
            int dcstartcount = query.Count();
            Program.Audit("EV start records count=" + dcstartcount);

            var query2 = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query2 = query2.Where(e => e.CODE == "9991025081817");
            query2 = query2.OrderBy(e => e.EVENT_DATETIME);
            int dcstopcount = query2.Count();
            Program.Audit("EV stop records count=" + dcstopcount);

            if (dcstartcount > 0 && dcstopcount > 0)
            {
                evdt = query.First().EVENT_DATETIME;
                uid = query.First().UNIT_ID;
                string st_str = query.First().RESULT;
                enddt = query2.First().EVENT_DATETIME;
                string st2_str = query2.First().RESULT;
                mins = 120;

                evdt = evdt.Date.AddHours(st_str.Substring(0, 2).Val()).AddMinutes(st_str.Substring(2, 2).Val());
                enddt = enddt.Date.AddHours(st2_str.Substring(0, 2).Val()).AddMinutes(st2_str.Substring(2, 2).Val());
                proc.finish = ExtendProcFinish(enddt);
                if (!EVDTinLocAry(evdt)) return;

                if (ProcExistsInDB(29, evdt, proc.finish))
                {
                    Program.Audit("Procedure 29: already exists");
                    return;
                }
                else
                {
                    proc.procedure_number = 29;
                    proc.start = evdt;
//                    proc.finish = enddt;
                    Program.Audit("Adding procedure 29: start=" + evdt + " finish=" + proc.finish);

                    st = GetStageForProcTime(proc.procedure_number, evdt, uid, out uname);
                    if (st >= 0)
                    {
                        //stname = Enum.GetName(typeof(Stages), (int)_stages[s].stage);
                        proc.stage = st;
                        proc.uname = uname;
                        proc.is_surg = issurg;
                        _procs.Add(proc);
                        Program.Audit("Procedure 29: Found EV between " + evdt + " and " + proc.finish);
                    }
                }
            }
        } //end proc


        private void CheckProc_40OtherSurg(bool issurg)
        {
            int mins = 0;
            string found_what;
            DateTime evdt;
            DateTime enddt = DateTime.MinValue;
            int uid = 0;
            int st = -1;
            string uname = "";
            var proc = new proc_data();
            proc.procedure_number = 40;
            proc.start = DateTime.MinValue;
            proc.finish = DateTime.MinValue;
            proc.recoverydt = DateTime.MinValue;

            Program.Audit("Checking 40OtherSurg...");

            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = query.Where(e => e.CODE.ToUpper().StartsWith("SCH"));
            query = query.Where(e => (e.DESCRIPTION.ToUpper().Contains("Fetal Invasive".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains("In Utero".ToUpper())));
            query = query.Where(e => (e.DESCRIPTION.ToUpper().Contains(";;;Anes Start".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;In Room".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;Proc Start".ToUpper())));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            int dcstartcount = query.Count();
            Program.Audit("Other Surg start records count=" + dcstartcount);

            var query2 = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query2 = query2.Where(e => e.CODE.ToUpper().StartsWith("SCH"));
            query2 = query2.Where(e => (e.DESCRIPTION.ToUpper().Contains("Fetal Invasive".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains("In Utero".ToUpper())));
            query2 = query2.Where(e => (e.DESCRIPTION.ToUpper().Contains(";;;Proc Close".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;Proc Finish".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;Out Room".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;Anes Stop".ToUpper())
              ));
            query2 = query2.OrderBy(e => e.EVENT_DATETIME);
            int dcstopcount = query2.Count();
            Program.Audit("Other Surg stop records count=" + dcstopcount);

            if (dcstartcount > 0 && dcstopcount > 0)
            {
                evdt = query.First().EVENT_DATETIME;
                uid = query.First().UNIT_ID;
                enddt = query2.Last().EVENT_DATETIME;
                mins = 120;
                proc.finish = ExtendProcFinish(enddt);
                if (proc.finish == enddt) proc.finish = enddt.AddMinutes(120);
                if (!EVDTinLocAry(evdt)) return;

                if (ProcExistsInDB(40, evdt, proc.finish))
                {
                    Program.Audit("Procedure 40: already exists");
                    return;
                }
                else
                {
                    proc.procedure_number = 40;
                    proc.start = evdt;
                    proc.recoverydt = enddt;
//                    proc.finish = enddt.AddMinutes(120);
                    Program.Audit("Adding procedure 40: start=" + evdt + " recov=" + enddt + " finish=" + proc.finish);

                    st = GetStageForProcTime(proc.procedure_number, evdt, uid, out uname);
                    if (st >= 0)
                    {
                        //stname = Enum.GetName(typeof(Stages), (int)_stages[s].stage);
                        proc.stage = st;
                        proc.uname = uname;
                        proc.is_surg = issurg;
                        _procs.Add(proc);
                        Program.Audit("Procedure 40: Found Other Surg between " + evdt + " and " + proc.finish);
                    }
                }
            }
        } //end proc

        private void CheckProc_42Bereave(bool issurg)
        {
            int mins = 0;
            string found_what;
            DateTime evdt;
            DateTime enddt = DateTime.MinValue;
            int uid = 0;
            int st = -1;
            string uname = "";
            var proc = new proc_data();
            proc.procedure_number = 42;
            proc.start = DateTime.MinValue;
            proc.finish = DateTime.MinValue;
            proc.recoverydt = DateTime.MinValue;

            //            "Bereavement Memories
            //Mementos Obtained"	
            //                "9991020100509
            //9991020100510"

            Program.Audit("Checking 42Bereave...");

            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = query.Where(e => e.CODE == "9991020100509" || e.CODE == "9991020100510");
            query = query.OrderBy(e => e.EVENT_DATETIME);
            int amcount = query.Count();
            Program.Audit("Bereave start records count=" + amcount);

            if (amcount > 0)
            {
                mins = 60;
                evdt = query.First().EVENT_DATETIME;
                uid = query.First().UNIT_ID;
                enddt = evdt.AddMinutes(mins);

                if (!EVDTinLocAry(evdt)) return;

                if (ProcExistsInDB(42, evdt, enddt))
                {
                    Program.Audit("Procedure 42: already exists");
                    return;
                }
                else
                {
                    proc.procedure_number = 42;
                    proc.start = evdt;
                    proc.finish = enddt;
                    st = GetStageForProcTime(proc.procedure_number, evdt, uid, out uname);
                    if (st >= 0)
                    {
                        //stname = Enum.GetName(typeof(Stages), (int)_stages[s].stage);
                        proc.stage = st;
                        proc.uname = uname;
                        proc.is_surg = issurg;
                        _procs.Add(proc);
                        Program.Audit("Procedure 42: Found Bereave between " + evdt + " and " + enddt);
                    }
                }
            }

        }

        private void CheckProc_43PUBS(bool issurg)
        {
            int mins = 0;
            string found_what;
            DateTime evdt;
            DateTime enddt = DateTime.MinValue;
            int uid = 0;
            int st = -1;
            string uname = "";
            var proc = new proc_data();
            proc.procedure_number = 43;
            proc.start = DateTime.MinValue;
            proc.finish = DateTime.MinValue;
            proc.recoverydt = DateTime.MinValue;

            Program.Audit("Checking 43PUBS...");

            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = query.Where(e => e.CODE.ToUpper().StartsWith("SCH4688"));
            query = query.Where(e => (e.DESCRIPTION.ToUpper().Contains("TRANSFUSION INTRAUTERINE FETAL".ToUpper())));
            query = query.Where(e => (e.DESCRIPTION.ToUpper().Contains(";;;Anes Start".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;In Room".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;Proc Start".ToUpper())));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            int dcstartcount = query.Count();
            Program.Audit("PUBS-IUBS start records count=" + dcstartcount);

            var query2 = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query2 = query2.Where(e => e.CODE.ToUpper().StartsWith("SCH4688"));
            query2 = query2.Where(e => (e.DESCRIPTION.ToUpper().Contains("TRANSFUSION INTRAUTERINE FETAL".ToUpper())));
            query2 = query2.Where(e => (e.DESCRIPTION.ToUpper().Contains(";;;Proc Close".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;Proc Finish".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;Out Room".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;Anes Stop".ToUpper())
              ));
            query2 = query2.OrderBy(e => e.EVENT_DATETIME);
            int dcstopcount = query2.Count();
            Program.Audit("PUBS-IUBS stop records count=" + dcstopcount);

            if (dcstartcount > 0 && dcstopcount > 0)
            {
                evdt = query.First().EVENT_DATETIME;
                uid = query.First().UNIT_ID;
                enddt = query2.Last().EVENT_DATETIME;
                mins = 120;
                proc.finish = ExtendProcFinish(enddt);
                if (proc.finish == enddt) proc.finish = enddt.AddMinutes(120);
                if (!EVDTinLocAry(evdt)) return;

                if (ProcExistsInDB(43, evdt, proc.finish))
                {
                    Program.Audit("Procedure 43: already exists");
                    return;
                }
                else
                {
                    proc.procedure_number = 43;
                    proc.start = evdt;
                    proc.recoverydt = enddt;
//                    proc.finish = enddt.AddMinutes(120);
                    Program.Audit("Adding procedure 43: start=" + evdt + " recov=" + enddt + " finish=" + proc.finish);

                    st = GetStageForProcTime(proc.procedure_number, evdt, uid, out uname);
                    if (st >= 0)
                    {
                        //stname = Enum.GetName(typeof(Stages), (int)_stages[s].stage);
                        proc.stage = st;
                        proc.uname = uname;
                        proc.is_surg = issurg;
                        _procs.Add(proc);
                        Program.Audit("Procedure 43: Found Other Surg between " + evdt + " and " + proc.finish);
                    }
                }
            }
        } //end proc

        private void DisableItemControlStatus(string code, DateTime evdt) //, bool use_proc_start)
        {
            //update ORDER_STATUS = 'X' for _pat.encounter_id and ordid
            var db = PFSDBUtility.NewSqlConnection();
            //string q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and code='" + code + "' and (result='initiated' or result='continued') and event_datetime<=" + PFSDBUtility.SQLDateTime(evdt);
            string q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and code='" + code + "' and event_datetime<=" + PFSDBUtility.SQLDateTime(evdt);
            //if (use_proc_start)
            //    q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and code='" + code + "' and procedure_start is not null and procedure_start<=" + PFSDBUtility.SQLDateTime(evdt);
            SqlCommand cmd = new SqlCommand(q, db);
            cmd.ExecuteNonQuery();
            db.Close();

        }

        private void CheckProc_44Safety(bool issurg)
        {
            int mins = 0;
            string found_what;
            DateTime evdt;
            DateTime enddt = DateTime.MinValue;
            int uid = 0;
            int st = -1;
            string uname = "";

            Program.Audit("Checking 44Safety...");

            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = query.Where(e => e.CODE == "9993040009123");
            query = query.Where(e => (e.RESULT.ToUpper().Contains("Impulsive".ToUpper())
              || e.RESULT.ToUpper().Contains("Lack of safety awareness".ToUpper())
              || e.RESULT.ToUpper().Contains("Does not use call light or ask for assistance".ToUpper())
              || e.RESULT.ToUpper().Contains("Danger to self or others".ToUpper())
              || e.RESULT.ToUpper().Contains("Danger from others".ToUpper())
              || e.RESULT.ToUpper().Contains("Exit seeking".ToUpper())
              || e.RESULT.ToUpper().Contains("Epilepsy monitoring".ToUpper())));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            int dcstartcount = query.Count();
            Program.Audit("Safety1:1 start records count=" + dcstartcount);

            if (dcstartcount == 0) return;

                var query2 = StartNewQuery(SearchDepth.SearchSinceAdmission);
                query2 = query2.Where(e => e.CODE == "9993040009234");
                query2 = query2.Where(e => e.ORDER_STATUS != "X");
                query2 = query2.Where(e => e.RESULT.ToUpper().Contains("Continuous observation".ToUpper())
                 || e.RESULT.ToUpper().Contains("q".ToUpper()));
                int dcstopcount = query2.Count();
                Program.Audit("Safety1:1 start/stop records count=" + dcstopcount);

                DateTime sttime = DateTime.MinValue;
                bool found_sttime = false;
                foreach (var item in query2)
                {
                    Program.Audit("Safety1:1 start/stop=" + item.RESULT + " at:" + item.EVENT_DATETIME);
                    if (item.RESULT.ToUpper().Contains("Continuous observation".ToUpper()))
                    {
                        sttime = item.EVENT_DATETIME;
                        found_sttime = true;
                    }
                    else if (found_sttime && item.EVENT_DATETIME > sttime)
                    {
                        evdt = sttime;
                        enddt = item.EVENT_DATETIME;
                        uid = item.UNIT_ID;
                    //found_sttime = false;
                    if (!EVDTinLocAry(evdt)) return;

                        //if (evdt.AddMinutes(60) <= enddt)
                        {
                            //add proc
                            if (ProcExistsInDB(44, evdt, enddt))
                            {
                                Program.Audit("Procedure 44: already exists");
                                return;
                            }
                            else
                            {
                                var proc = new proc_data();
                                proc.procedure_number = 44;
                                proc.start = DateTime.MinValue;
                                proc.finish = DateTime.MinValue;
                                proc.recoverydt = DateTime.MinValue;

                                proc.procedure_number = 44;
                                proc.start = evdt;
                                proc.finish = enddt;
                                Program.Audit("Adding procedure 44: start=" + evdt + " finish=" + proc.finish);

                                st = GetStageForProcTime(proc.procedure_number, evdt, uid, out uname);
                                if (st >= 0)
                                {
                                    //stname = Enum.GetName(typeof(Stages), (int)_stages[s].stage);
                                    proc.stage = st;
                                    proc.uname = uname;
                                    proc.is_surg = issurg;
                                    _procs.Add(proc);
                                    Program.Audit("Procedure 44: Found Safety1:1 between " + evdt + " and " + proc.finish);
                                    DisableItemControlStatus(item.CODE,item.EVENT_DATETIME);
                                }
                            }

                        }
                    }
                } //query2
        } //end proc

        private void CheckProc_46TL(bool issurg)
        {
            int mins = 0;
            string found_what;
            DateTime evdt;
            DateTime enddt = DateTime.MinValue;
            int uid = 0;
            int st = -1;
            string uname = "";
            var proc = new proc_data();
            proc.procedure_number = 46;
            proc.start = DateTime.MinValue;
            proc.finish = DateTime.MinValue;
            proc.recoverydt = DateTime.MinValue;

            Program.Audit("Checking 46TL...");

            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = query.Where(e => e.CODE.ToUpper() == "SCH3167".ToUpper());
            query = query.Where(e => e.DESCRIPTION.ToUpper().Contains("LIGATION TUBAL".ToUpper()));
            query = query.Where(e => (e.DESCRIPTION.ToUpper().Contains(";;;Anes Start".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;In Room".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;Proc Start".ToUpper())));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            int dcstartcount = query.Count();
            Program.Audit("TL start records count=" + dcstartcount);

            var query2 = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = query.Where(e => e.CODE.ToUpper() == "SCH3167".ToUpper());
            query = query.Where(e => e.DESCRIPTION.ToUpper().Contains("LIGATION TUBAL".ToUpper()));
            query2 = query2.Where(e => (e.DESCRIPTION.ToUpper().Contains(";;;Proc Close".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;Proc Finish".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;Out Room".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;Anes Stop".ToUpper())
              ));
            query2 = query2.OrderBy(e => e.EVENT_DATETIME);
            int dcstopcount = query2.Count();
            Program.Audit("TL stop records count=" + dcstopcount);

            if (dcstartcount > 0 && dcstopcount > 0)
            {
                evdt = query.First().EVENT_DATETIME;
                uid = query.First().UNIT_ID;
                enddt = query2.Last().EVENT_DATETIME;
                mins = 120;
                proc.finish = ExtendProcFinish(enddt);
                if (proc.finish == enddt) proc.finish = enddt.AddMinutes(120);
                if (!EVDTinLocAry(evdt)) return;

                if (ProcExistsInDB(46, evdt, proc.finish))
                {
                    Program.Audit("Procedure 46: already exists");
                    return;
                }
                else
                {
                    proc.procedure_number = 46;
                    proc.start = evdt;
                    proc.recoverydt = enddt;
//                    proc.finish = enddt.AddMinutes(120);
                    Program.Audit("Adding procedure 46: start=" + evdt + " recov=" + enddt + " finish=" + proc.finish);

                    st = GetStageForProcTime(proc.procedure_number, evdt, uid, out uname);
                    if (st >= 0)
                    {
                        //stname = Enum.GetName(typeof(Stages), (int)_stages[s].stage);
                        proc.stage = st;
                        proc.uname = uname;
                        proc.is_surg = issurg;
                        _procs.Add(proc);
                        Program.Audit("Procedure 46: Found TL between " + evdt + " and " + proc.finish);
                    }
                }
            }
        } //end proc


    private void CheckProc_48WD(bool issurg)
        {
            int mins = 0;
            string found_what;
            DateTime evdt;
            DateTime enddt = DateTime.MinValue;
            int uid = 0;
            int st = -1;
            string uname = "";
            var proc = new proc_data();
            proc.procedure_number = 48;
            proc.start = DateTime.MinValue;
            proc.finish = DateTime.MinValue;
            proc.recoverydt = DateTime.MinValue;

            Program.Audit("Checking 48WD...");

//            SCH778 ^ DEBRIDEMENT AND IRRIGATION WOUND
//SCH772 ^ DEBRIDEMENT AND IRRIGATION
//SCH149 ^ _________WOUND VACUUM


            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = query.Where(e => ((e.CODE.ToUpper() == "SCH778".ToUpper() || e.CODE.ToUpper() == "SCH772".ToUpper())
                                       && e.DESCRIPTION.ToUpper().Contains("DEBRIDEMENT AND IRRIGATION".ToUpper()))
                                       ||
                                       (e.CODE.ToUpper() == "SCH149".ToUpper() && e.DESCRIPTION.ToUpper().Contains("WOUND VACUUM".ToUpper()))
                                       ||
                                       (e.CODE.ToUpper() == "SCH4693".ToUpper() && e.DESCRIPTION.ToUpper().Contains("EXPLORATORY LAPAROTOMY".ToUpper())));
            query = query.Where(e => (e.DESCRIPTION.ToUpper().Contains(";;;Anes Start".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;In Room".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;Proc Start".ToUpper())));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            int dcstartcount = query.Count();
            Program.Audit("WD start records count=" + dcstartcount);

            var query2 = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query2 = query2.Where(e => ((e.CODE.ToUpper() == "SCH778".ToUpper() || e.CODE.ToUpper() == "SCH772".ToUpper())
                                       && e.DESCRIPTION.ToUpper().Contains("DEBRIDEMENT AND IRRIGATION".ToUpper()))
                                       ||
                                       (e.CODE.ToUpper() == "SCH149".ToUpper() && e.DESCRIPTION.ToUpper().Contains("WOUND VACUUM".ToUpper()))
                                       ||
                                       (e.CODE.ToUpper() == "SCH4693".ToUpper() && e.DESCRIPTION.ToUpper().Contains("EXPLORATORY LAPAROTOMY".ToUpper())));
            query2 = query2.Where(e => (e.DESCRIPTION.ToUpper().Contains(";;;Proc Close".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;Proc Finish".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;Out Room".ToUpper())
              || e.DESCRIPTION.ToUpper().Contains(";;;Anes Stop".ToUpper())
              ));
            query2 = query2.OrderBy(e => e.EVENT_DATETIME);
            int dcstopcount = query2.Count();
            Program.Audit("WD stop records count=" + dcstopcount);

            if (dcstartcount > 0 && dcstopcount > 0)
            {
                evdt = query.First().EVENT_DATETIME;
                uid = query.First().UNIT_ID;
                enddt = query2.Last().EVENT_DATETIME;
                mins = 120;
                proc.finish = ExtendProcFinish(enddt);
                if (proc.finish == enddt) proc.finish = enddt.AddMinutes(120);
                if (!EVDTinLocAry(evdt)) return;

                if (ProcExistsInDB(48, evdt, proc.finish))
                {
                    Program.Audit("Procedure 48: already exists");
                    return;
                }
                else
                {
                    proc.procedure_number = 48;
                    proc.start = evdt;
                    proc.recoverydt = enddt;
//                    proc.finish = enddt.AddMinutes(120);
                    Program.Audit("Adding procedure 48: start=" + evdt + " recov=" + enddt + " finish=" + proc.finish);

                    st = GetStageForProcTime(proc.procedure_number, evdt, uid, out uname);
                    if (st >= 0)
                    {
                        //stname = Enum.GetName(typeof(Stages), (int)_stages[s].stage);
                        proc.stage = st;
                        proc.uname = uname;
                        proc.is_surg = issurg;
                        _procs.Add(proc);
                        Program.Audit("Procedure 48: Found WD between " + evdt + " and " + proc.finish);
                    }
                }
            }
        } //end proc

        //private void DoProc(int pnum, string code)
        //{
        //    double mins = 0;
        //    string found_what;
        //    DateTime evdt;
        //    DateTime enddt = DateTime.MinValue;

        //    if (GetResultAndEVDT("", code, "", "", out found_what, out evdt))
        //    {
        //        mins = 60.0 * found_what.ToDouble();
        //        enddt = evdt.AddMinutes(mins);

        //        if (ProcExistsInDB(pnum, evdt, enddt))
        //        {
        //            Program.Audit("Procedure " + pnum+ ": already exists");
        //        }
        //        else
        //        {
        //            if (!QueuedProcOverlaps(pnum, evdt, enddt))
        //            {
        //                var proc = new proc_data();
        //                proc.procedure_number = pnum;
        //                proc.start = evdt;
        //                proc.finish = enddt;
        //                _procs.Add(proc);
        //                Program.Audit("Procedure " + pnum + ": Found " + code + " between " + evdt + " and " + enddt);
        //            }
        //        }

        //    }

        //}

        //private bool OnlyHasED()
        //{
        //    var db = PFSDBUtility.NewPfsDataContext();
        //    var query = from el in db.ENCOUNTER_LOCATIONs
        //                join u in db.UNITs on el.UNIT_ID equals u.UNIT_ID
        //                where (el.ENCOUNTER_ID == _pat.encounter_id)
        //                where (u.IS_ED == null || u.IS_ED.ToString().ToUpper() == "N")
        //                select new { u.NAME };
        //    return (query.Count() == 0);
        //}

        private bool ProcExistsInDB(int pnum, DateTime startdt, DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            //LoadPatientProceduresIfNeeded();
            //var query = from proc in _procedure_events
            //            from ans in proc.PROCEDURE_ANSWERs
            //            where (proc.ENCOUNTER_ID == _pat.encounter_id)
            //                && (proc.PROCEDURE_DATETIME == startdt)
            //                //&& (proc.DEPARTURE_DATETIME == enddt)
            //                && (ans.PROCEDURE_NUMBER == pnum)
            //            select new { proc.PROCEDURE_EVENT_ID };

            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && ((proc.PROCEDURE_DATETIME <= startdt) && (proc.DEPARTURE_DATETIME > startdt)
                              ||
                              (proc.PROCEDURE_DATETIME < enddt) && (proc.DEPARTURE_DATETIME > startdt))
                            //&& (proc.DEPARTURE_DATETIME == enddt)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new { proc.PROCEDURE_EVENT_ID };


            return (query.Count() > 0);
        }

        //private bool QueuedProcOverlaps(int pnum, DateTime startdt, DateTime enddt)
        //{
        //    bool overlap = false;

        //    proc_data[] pary = _procs.ToArray();
        //    for (int i = 0; i <= pary.GetUpperBound(0); i++)
        //    {
        //        if (pary[i].procedure_number == pnum)
        //        {
        //            //overlap if   p.start between stardt and enddt
        //            //             p.finish between startdt and enddt
        //            //             startdt >= p.start and enddt <= p.finish
        //            if ((pary[i].start >= startdt) && (pary[i].start < enddt) ||
        //                (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
        //                (startdt >= pary[i].start) && (enddt <= pary[i].finish))
        //            {
        //                overlap = true;
        //            }

        //            if ((pary[i].start >= startdt) && (pary[i].start < enddt) && (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
        //                (pary[i].start < startdt) && (pary[i].finish > startdt) && (pary[i].finish < enddt)
        //                )
        //            {
        //                // the proc in the list is completely contained in startdt/endt pair
        //                // change this proc to be the startdt/enddt
        //                // overlap is still true, but the list item will be updated with the encompassing times
        //                //Program.Audit("Procedure " + pnum + ": " + startdt + " - " + enddt + " will trump " + pary[i].start + " - " + pary[i].finish);
        //                pary[i].start = startdt;
        //                pary[i].finish = enddt;
        //            }
        //        }
        //    }
        //    _procs = pary.ToList();
        //    return overlap;
        //}


        //private void CheckProc_1()
        //{
        //    //Program.VerboseAudit("---------------");
        //    //Program.VerboseAudit("P1. 1-1 safety observation by RN");
        //    //Program.VerboseAudit("---------------");
        //}

        //private void CheckProc_2()
        //{
        //    string nowstr;
        //    string toddtstr;
        //    string yesdtstr;
        //    string timea ="";
        //    DateTime timea_startdt, timea_enddt;
        //    DateTime timeb_startdt, timeb_enddt;
        //    DateTime nowdt = _pat.pull_finish;              // "now" is pull time
        //    DateTime yesdt;

        //    Program.VerboseAudit("---------------");
        //    Program.VerboseAudit("P2. 1-1 safety observation by non-RN");
        //    Program.VerboseAudit("---------------");

        //    nowstr = nowdt.ToString("yyyyMMddHHmm");
        //    yesdt = nowdt.AddDays(-1);
        //    toddtstr = nowdt.ToString("yyyyMMdd");
        //    yesdtstr = yesdt.ToString("yyyyMMdd");

        //    //when is now? (yesterday/today)
        //    //yes 7am -- yes 7p  -- tod 7a -- tod 7p
        //    //                                   A                   B
        //    //if nowdt >= tod7pm then check  tod 7am-tod 7pm and tod 7pm-tom 7a
        //    //if nowdt >= tod7am then check  yes 7p - tod 7a     tod 7a-tod 7p
        //    //if nowdt >= yes7pm then check  yes 7a-yes 7p       yes 7p-tod 7a
        //    if (nowstr.CompareTo(toddtstr + "1900") >= 0) {
        //        timea = toddtstr + "0700";
        //    } else if (nowstr.CompareTo(toddtstr + "0700") >= 0) {
        //        timea = yesdtstr + "1900";
        //    } else if (nowstr.CompareTo(yesdtstr + "1900") >= 0) {
        //        timea = yesdtstr + "0700";
        //    }

        //    timea_startdt = PFSUtility.ISOToDateTime(timea);
        //    timea_enddt = timea_startdt.AddHours(12);
        //    MaybeAddSitter(timea_startdt, timea_enddt);

        //    timeb_startdt = timea_enddt;
        //    timeb_enddt = timeb_startdt.AddHours(12);
        //    MaybeAddSitter(timeb_startdt, timeb_enddt);
        //}

        //private void MaybeAddSitter(DateTime startdt, DateTime enddt)
        //{
        //    var query = StartNewQuery(SearchDepth.SearchSinceAdmission);    // add custom time range below
        //    query = query.Where(e => (e.EVENT_DATETIME >= startdt) && (e.EVENT_DATETIME < enddt));
        //    query = AndCodeInList(query, "Sitter");
        //    query = AndResultInList(query,"continued, initiated");
        //    query = AndResultNotInList(query, "discontinued");

        //    if (query.Count() > 0) {
        //        if (ProcExists(2, startdt, enddt)) {
        //            Program.Audit("Procedure 2: already exists");
        //        } else {
        //            var proc = new proc_data();
        //            proc.procedure_number = 2;
        //            proc.start = startdt;
        //            proc.finish = enddt;
        //            _procs.Add(proc);
        //            Program.Audit("Procedure 2: Found Sitter between " + startdt + " and " + enddt);
        //        }
        //    }

        //}

        private bool ProcExists(int pnum, DateTime startdt, DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && (proc.PROCEDURE_DATETIME == startdt)
                            && (proc.DEPARTURE_DATETIME == enddt)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new {proc.PROCEDURE_EVENT_ID};
            return (query.Count() > 0);
        }



        private const string DATETIME_FORMAT = "yyyyMMddHHmmss";              // ISO Date/Time w/o seconds
        private void OutputStages()
        {
            string outstr, ind_list, desc, str_pull_dt;
            int i, tc_event_id;
            string stname = "";

            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         212 242 304
            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1
            //1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901 3456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
            //1       |10 5            |                |                |        |10023121779         |ALEXANDER                       |GLORIA                          |                                |0508    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289530  |           |201403030400                                                                      |NNYNNNNYYNNYNNNYNNNYYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
            //1       |10 5            |                |                |        |10024544908         |ARMSTRONG                       |AARON                           |                                |0535    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289531  |           |201403030400                                                                      |NYNNYNNNNNNYNNNYNNNNYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
            //|0547    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289532  |           |201403030400                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN
            //|0547    |A   |201403030700|                |201403030700    |18  |C|    |3040|1440|18289532  |           |201403030700                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN
            //; perinatal classification by stage; no indicators required
            //| C | Ant |
            //| C | El | m
            //| C | Al | m
            //| C | Del | m
            //| C | Rec |
            //| C | PP | m
            //| C | IC | ba
            //| C | TC | b
            //| C | NBN |
            if (stageidx < 0) return;

            for (int s = 0; s <= stageidx; s++)
            {
            stname = Enum.GetName(typeof(Stages), (int)_stages[s].stage);
   Program.VerboseAudit("OutputStage: " + stname + " index s=" + s + " stageidx="+stageidx + " remove="+ _stages[s].remove.ToString() + 
       " start=" + _stages[s].start + " q4="+ Program.g_pull_finish_q4 
       + (_stages[s].start <= Program.g_pull_finish_q4 ? "=>HALTED: Too early" : "=>Proceeding..."));
                if (!_stages[s].remove && _stages[s].start <= Program.g_pull_finish_q4)
                {
                    if ((int)_stages[s].stage == 0)
                    {
                        _stages[s].finish = SetOPFinishTime();
                    }
                    stname = Enum.GetName(typeof(Stages), (int)_stages[s].stage);
                    Program.VerboseAudit("1stname="+stname + " uname="+ _stages[s].uname + " stagenum="+ _stages[s].stage);
                    Program.gLogUnitID = _stages[s].unitid;
                    if (Program.g_is_test)
                        tc_event_id = 9999;
                    else
                        tc_event_id = PFSDBUtility.NextGID();                         //get a unique id for this class
                                                                                      //            str_pull_dt = _pat.pull_finish.ToString(DATETIME_FORMAT);
                    str_pull_dt = _pat.pull_start.ToString(DATETIME_FORMAT);
                    outstr = _pat.facilty_code.FixedWidth(8);                       //(facility code)
                    outstr += "|" + _stages[s].uname.FixedWidth(16);
                    outstr += "|" + "".FixedWidth(16);                               //(unit code)
                    outstr += "|" + "".FixedWidth(16);                               //(area code)
                    outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
                    outstr += "|" + _pat.acct.FixedWidth(20);
                    outstr += "|" + _pat.last_name.FixedWidth(32);
                    outstr += "|" + _pat.first_name.FixedWidth(32);
                    outstr += "|" + _pat.middle_name.FixedWidth(32);
                    outstr += "|" + _pat.room.FixedWidth(8);
                    outstr += "|" + _pat.bed.FixedWidth(4);
                    outstr += "|" + _stages[s].start.ToString(DATETIME_FORMAT);                      //class datetime (could change)
                    outstr += "|" + "".FixedWidth(16);                               //(login)
                    outstr += "|" + _stages[s].start.ToString(DATETIME_FORMAT);                      //class datetime (could change)
                    outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
                    if ((int)_stages[s].stage == 0)
                    {
                        outstr += "|" + "P".FixedWidth(1);   //if OP then Procedure
                        _stages[s]._sinds[1].is_checked = true;
                    }
                    else
                        outstr += "|" + "C".FixedWidth(1);   //record type = class
                    outstr += "|" + stname.FixedWidth(4);     //(stage)
                    outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
                    outstr += "|" + _pat.range.ToString().FixedWidth(4);             //TC pull range
                    outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
                    outstr += "|";
                    outstr = outstr.FixedWidth(294);
                    outstr += "|" + _stages[s].start.ToString(DATETIME_FORMAT);  //class datetime (could change)
                    outstr = outstr.FixedWidth(346);
                    if ((int)_stages[s].stage == 0 && _stages[s].finish > DateTime.MinValue)
                    {
                        outstr += "|" + _stages[s].finish.ToString(DATETIME_FORMAT);
                    }
                    outstr = outstr.FixedWidth(377);
                    outstr += "|";

                    ind_list = " Stage ";
                    ind_list += stname.FixedWidth(4) + ": ";
                    for (i = 1; (i <= MAX_INDS); i++)
                    {
                        if (_stages[s]._sinds[i].is_checked &&
                             (_stages[s].stage != Stages.NBN ||
                               ( (i != 10 && i != 17) || (i == 10 || i == 17) && !_stages[s].NBN_was_split)
                             )
                           )
                        {
                                outstr += "Y";
                            ind_list += "," + i;
                        }
                        else
                        {
                            outstr += "N";
                        }
                    } // next i
                    if (ind_list != "") ind_list = ind_list.Substring(1);                           //strip leading comma

                    Program.outfile.WriteLine(outstr);   //output to transparent.txt

                    Program.Audit("");
                    desc = "Classified: " + ind_list;
                    //if (Program.g_is_test)
                    {
                        Program.Audit(desc);
                    }
                    //else
                    //{
                    //Save the selected indicators plus both Program.Audits in the event log; link with tc_event_id
                    //PFSEventLog.AddTransparentMappingEventLogEntry(
                    //    desc, Program.gLogUnitID, Program.gLogEncounterID,
                    //    tc_event_id, Program.gLogMapperVersion,
                    //    Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
                    bool evlog_saved = false;
                    int save_attempts = 0;
                    while (!evlog_saved && (save_attempts < 3))
                    {
                        PFSEventLog.AddTransparentMappingEventLogEntry(
                        desc, Program.gLogUnitID, Program.gLogEncounterID,
                        tc_event_id, Program.gLogMapperVersion,
                        Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());

                        save_attempts++;
                        //seek for event_id
                        // if !exist then wait/sleep 1000milli sec and retry
                        evlog_saved = TC_Event_IDExists(tc_event_id);
                        if (!evlog_saved) PFSUtility.Sleep(500); // wait half a second
                    }

                    //}
                    if (_stages[s].stage != 0) // don't span locs with OP proc
                    {
                        if (_stages[s].has_addl_locs)
                        {
                            Program.VerboseAudit("output has addl locs...");
                            OutputAddlLocClass(_stages[s].stage, s);
                        }
                    }
                 if ((int)_stages[s].stage == 0) //then check if there are other locations in this stage range and give them this OP procedure.
                    {
                        for (int k = 2; k <= _pat.num_locs; k++)
                        {
                            Program.VerboseAudit("Additional locations for procedure....k="+k + " s="+s+" stage="+ _stages[s].start+"-"+ _stages[s].finish + " loc="+ _pat.locary[k].time_in+"-"+ _pat.locary[k].time_out);
                            if (_pat.locary[k].time_in >= _stages[s].start)
                            { //then give this location the same OP
                              //s= s.Remove(3, 2).Insert(3, "ZX");
                                Program.VerboseAudit("loc=" + _pat.locary[k].time_in+ " uname="+ _pat.locary[k].uname);
                                string addlLocOPstr = outstr;
                                string s26 = addlLocOPstr.Substring(25);
                                string s294 = addlLocOPstr.Substring(294+14+1);
                                string s348 = addlLocOPstr.Substring(346+14-1);
                                Program.VerboseAudit("s26=" + s26);
                                Program.VerboseAudit("s294=" + s294);
                                Program.VerboseAudit("s348=" + s348);
                                addlLocOPstr = addlLocOPstr.Substring(0,9)+_pat.locary[k].uname.FixedWidth(16)+s26;
                                Program.VerboseAudit("s=" + addlLocOPstr);
                                addlLocOPstr = addlLocOPstr.Substring(0, 295) + _pat.locary[k].time_in.ToString(DATETIME_FORMAT) + s294;
                                Program.VerboseAudit("s=" + addlLocOPstr);
                                //addlLocOPstr = addlLocOPstr.Substring(0, 347) + _pat.locary[k].time_out.ToString(DATETIME_FORMAT) + s348;
                                //Program.VerboseAudit("s=" + addlLocOPstr);
                                Program.outfile.WriteLine(addlLocOPstr);
                            }
                        }
                    }
                } //if !removed stage
            } //for
        }

        private bool TC_Event_IDExists(int evid)
        {
            bool idexists = false;
            string sql = "select tc_event_id from EVENT_LOG";
            sql += " where timestamp>dateadd(mi,-5,getdate()) and TC_EVENT_ID=" + evid;
            Program.VerboseAudit(sql);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd = new SqlCommand(sql, db2);
            SqlDataReader dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr2.Read())
            {
                if (dr2["TC_EVENT_ID"] != DBNull.Value)
                    idexists = true;
            }
            db2.Close();
            return idexists;
        }

        private DateTime SetOPFinishTime()
        {
            DateTime ptime = DateTime.MaxValue;
            foreach (var p in _procs)
            {
                if (p.start < ptime) //choose the earliest time among all procedures
                {
                    ptime = p.start;
                }
            }
            if (ptime < DateTime.MaxValue)
                return ptime;
            else
                return DateTime.MinValue;
        }
        private void OutputAddlLocClass(Stages locstage, int s)
        {
            string outstr, ind_list, desc, str_pull_dt;
            int i, tc_event_id;
            string stname = "";

            for (int k=1; k<=num_addl_loc; k++)
            {
                if (_stages_addl_loc[k].stage == locstage && _stages_addl_loc[k].start <= Program.g_pull_finish_q4)
                {
                    _stages_addl_loc[k]._sinds = _stages[s]._sinds;

                    stname = Enum.GetName(typeof(Stages), (int)_stages[s].stage);
                    Program.VerboseAudit("2stname=" + stname + " uname=" + _stages[s].uname + " stagenum=" + _stages[s].stage);
                    Program.gLogUnitID = _stages_addl_loc[k].unitid;
                    if (Program.g_is_test)
                        tc_event_id = 9999;
                    else
                        tc_event_id = PFSDBUtility.NextGID();                         //get a unique id for this class
                                                                                      //            str_pull_dt = _pat.pull_finish.ToString(DATETIME_FORMAT);
                    str_pull_dt = _pat.pull_start.ToString(DATETIME_FORMAT);
                    outstr = _pat.facilty_code.FixedWidth(8);                       //(facility code)
                    outstr += "|" + _stages_addl_loc[k].uname.FixedWidth(16);
                    outstr += "|" + "".FixedWidth(16);                               //(unit code)
                    outstr += "|" + "".FixedWidth(16);                               //(area code)
                    outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
                    outstr += "|" + _pat.acct.FixedWidth(20);
                    outstr += "|" + _pat.last_name.FixedWidth(32);
                    outstr += "|" + _pat.first_name.FixedWidth(32);
                    outstr += "|" + _pat.middle_name.FixedWidth(32);
                    outstr += "|" + _pat.room.FixedWidth(8);
                    outstr += "|" + _pat.bed.FixedWidth(4);
                    outstr += "|" + _stages_addl_loc[k].start.ToString(DATETIME_FORMAT);                      //class datetime (could change)
                    outstr += "|" + "".FixedWidth(16);                               //(login)
                    outstr += "|" + _stages_addl_loc[k].start.ToString(DATETIME_FORMAT);                      //class datetime (could change)
                    outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
                    outstr += "|" + "C".FixedWidth(1);   //record type = class
                    outstr += "|" + stname.FixedWidth(4);     //(stage)
                    outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
                    outstr += "|" + _pat.range.ToString().FixedWidth(4);             //TC pull range
                    outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
                    outstr += "|";
                    outstr = outstr.FixedWidth(294);
                    outstr += "|" + _stages_addl_loc[k].start.ToString(DATETIME_FORMAT);  //class datetime (could change)
                    outstr = outstr.FixedWidth(346);
                    //outstr += "|" + _stages[s].finish.ToString(DATETIME_FORMAT);
                    outstr = outstr.FixedWidth(377);
                    outstr += "|";

                    ind_list = " Stage ";
                    ind_list += stname.FixedWidth(4) + ": ";
                    for (i = 1; (i <= MAX_INDS); i++)
                    {
                        if (_stages_addl_loc[k]._sinds[i].is_checked &&
                            (locstage != Stages.TC || 
                             ((i==10 || i==17) && 
                               _stages_addl_loc[k]._sinds[i].originating_unit == _stages_addl_loc[k].unitid) 
                            )
                           )
                        {
                            outstr += "Y";
                            ind_list += "," + i;
                        }
                        else
                        {
                            outstr += "N";
                        }
                    } // next i
                    if (ind_list != "") ind_list = ind_list.Substring(1);                           //strip leading comma

                    Program.outfile.WriteLine(outstr);                          //output to transparent.txt

                    Program.Audit("");
                    desc = "Classified: " + ind_list;
                    //if (Program.g_is_test)
                    {
                        Program.Audit(desc);
                    }
                    //else
                    //{
                    //Save the selected indicators plus both Program.Audits in the event log; link with tc_event_id
                    PFSEventLog.AddTransparentMappingEventLogEntry(
                        desc, Program.gLogUnitID, Program.gLogEncounterID,
                        tc_event_id, Program.gLogMapperVersion,
                        Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());

                } // if stage==locstage
            } //for
        }

//        private void OutputClass()
//        {
//            string outstr, ind_list, desc, str_pull_dt;
//            int i, tc_event_id;

//            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         212 242 304
//            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1
//            //1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901 3456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
//            //1       |10 5            |                |                |        |10023121779         |ALEXANDER                       |GLORIA                          |                                |0508    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289530  |           |201403030400                                                                      |NNYNNNNYYNNYNNNYNNNYYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
////1       |10 5            |                |                |        |10024544908         |ARMSTRONG                       |AARON                           |                                |0535    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289531  |           |201403030400                                                                      |NYNNYNNNNNNYNNNYNNNNYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
////|0547    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289532  |           |201403030400                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN
////|0547    |A   |201403030700|                |201403030700    |18  |C|    |3040|1440|18289532  |           |201403030700                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN

//            if (Program.g_is_test)
//                tc_event_id = 9999;
//            else
//                tc_event_id = PFSDBUtility.NextGID();                         //get a unique id for this class
////            str_pull_dt = _pat.pull_finish.ToString(DATETIME_FORMAT);
//            str_pull_dt = _pat.unit_arrival.ToString(DATETIME_FORMAT);
//            outstr = _pat.facilty_code.FixedWidth(8);                       //(facility code)
//            outstr += "|" + _pat.unit_name.FixedWidth(16);
//            outstr += "|" + "".FixedWidth(16);                               //(unit code)
//            outstr += "|" + "".FixedWidth(16);                               //(area code)
//            outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
//            outstr += "|" + _pat.acct.FixedWidth(20);
//            outstr += "|" + _pat.last_name.FixedWidth(32);
//            outstr += "|" + _pat.first_name.FixedWidth(32);
//            outstr += "|" + _pat.middle_name.FixedWidth(32);
//            outstr += "|" + _pat.room.FixedWidth(8);
//            outstr += "|" + _pat.bed.FixedWidth(4);
//          outstr += "|" + str_pull_dt.FixedWidth(12);                      //class datetime (could change)
//            outstr += "|" + "".FixedWidth(16);                               //(login)
//          outstr += "|" + str_pull_dt.FixedWidth(16);                      //(employee)/(pull datetime)
//            outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
//            outstr += "|" + "C".FixedWidth(1);                               //record type = class
//            outstr += "|" + "".FixedWidth(4);                                //(stage)
//            outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
//            outstr += "|" + _pat.range.ToString().FixedWidth(4);             //TC pull range
//            outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
//            outstr += "|";
//            outstr = outstr.FixedWidth(294);
//          outstr += "|" + _pat.unit_arrival.ToString(DATETIME_FORMAT);        //IN
//            outstr = outstr.FixedWidth(377);
//            outstr += "|";
            
//            ind_list = "";
//            for (i = 1; (i <= MAX_INDS); i++) {
//                if (_inds[i].is_checked) {
//                    outstr += "Y";
//                    ind_list += "," + i;
//                } else {
//                    outstr += "N";
//                }
//            } // next i
//            if (ind_list != "") ind_list = ind_list.Substring(1);                           //strip leading comma
  
//            Program.outfile.WriteLine(outstr);                          //output to transparent.txt

//            Program.Audit("");
//            desc = "Classified: " + ind_list;
//            if (Program.g_is_test) {
//                Program.Audit(desc);
//            } else {
//                //Save the selected indicators plus both Program.Audits in the event log; link with tc_event_id
//                PFSEventLog.AddTransparentMappingEventLogEntry(
//                    desc, Program.gLogUnitID, Program.gLogEncounterID,
//                    tc_event_id, Program.gLogMapperVersion,
//                    Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
//            }
//        }

        private void OutputProcs()
        {
            int i;
            string outstr, proc_list, desc;
            int tc_event_id;

            Program.VerboseAudit("In OutputProcs...");

            foreach (var proc in _procs)
            {
                Program.VerboseAudit("...proc.="+proc.procedure_number + "  proc.start=" + proc.start);
                if (Program.g_is_test)
                    tc_event_id = 9999;
                else
                    tc_event_id = PFSDBUtility.NextGID();                         //get a unique id for this proc

                string stname = Enum.GetName(typeof(Stages), proc.stage);
                Program.VerboseAudit("OutputProcs: Stage=" + stname);

                outstr = _pat.facilty_code.FixedWidth(8);                       //(facility code)
                outstr += "|" + proc.uname.FixedWidth(16);
                outstr += "|" + "".FixedWidth(16);                               //(unit code)
                outstr += "|" + "".FixedWidth(16);                               //(area code)
                outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
                outstr += "|" + _pat.acct.FixedWidth(20);
                outstr += "|" + _pat.last_name.FixedWidth(32);
                outstr += "|" + _pat.first_name.FixedWidth(32);
                outstr += "|" + _pat.middle_name.FixedWidth(32);
                outstr += "|" + _pat.room.FixedWidth(8);
                outstr += "|" + _pat.bed.FixedWidth(4);
                outstr += "|" + proc.start.ToString(DATETIME_FORMAT);                      //class datetime (could change)
                outstr += "|" + "".FixedWidth(16);                               //(login)
                outstr += "|" + proc.start.ToString(DATETIME_FORMAT);                      //class datetime (could change)
                outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
                if (proc.is_surg)
                    outstr += "|" + "S".FixedWidth(1);
                else
                    outstr += "|" + "P".FixedWidth(1);
                outstr += "|" + stname.FixedWidth(4);     //(stage)
                outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
                outstr += "|" + _pat.range.ToString().FixedWidth(4);             //TC pull range
                outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
                outstr += "|";
                outstr = outstr.FixedWidth(294);
                outstr += "|" + proc.start.ToString(DATETIME_FORMAT);  //class datetime (could change)
                if (proc.recoverydt > DateTime.MinValue)
                {
                    outstr = outstr.FixedWidth(320);
                    outstr += "|" + proc.recoverydt.ToString(DATETIME_FORMAT);  //class datetime (could change)
                }
                outstr = outstr.FixedWidth(346);
                outstr += "|" + proc.finish.ToString(DATETIME_FORMAT);
                outstr = outstr.FixedWidth(377);
                outstr += "|";

                proc_list = "";
                for (i = 1; (i < MAX_PROCS); i++)
                {
                    if (proc.procedure_number == i)
                    {
                        outstr += "Y";
                        proc_list += "," + i;
                    }
                    else
                    {
                        outstr += "N";
                    }
                } // next i
                proc_list = proc_list.Substring(1);                             //strip leading comma

                Program.outfile.WriteLine(outstr);                              //output to transparent.txt

                desc = "Procedures: " + proc_list;
                if (Program.g_is_test)
                {
                    Program.Audit(desc);
                }
                else
                {
                    //Save the selected procedures plus both Program.Audits in the event log; link with tc_event_id
                    //(this assumes the procedure Program.Audit is mixed in with the class Program.Audit)
                    PFSEventLog.AddTransparentMappingEventLogEntry(desc, Program.gLogUnitID, Program.gLogEncounterID,
                        tc_event_id, Program.gLogMapperVersion,
                        Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
                }
            } // next proc
        }

    }

}
