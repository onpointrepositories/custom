﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;                     // for LINQ; add a reference
using System.Data.SqlClient;                // for SqlConnection
using System.IO;                            // for Directory, Path, Stream
using PfsShared;                            // add a reference to Shared2 project

// ============================================================================
// Mental Health transparent mapping -- GOES HERE --
// MAYO ROCHESTER EPIC
// ============================================================================
// This processes one patient.  Only one database query is used.  :)
//
// All search functions use exact match for category, description and field name.
// Codes and results can be a single word or a list of words.
// Codes default to exact match; prefix with CODE_CODE_LIKE_PREFIX if desired (single word only)
// Results default to like matching; prefix with RESULT_EXACT_MATCH_PREFIX if desired (single word only)
//
// All searches are case insensitive.
//
//
namespace TransparentMapping
{
    class MentalHealth
    {
        private const int MAX_INDS = 120;
        private const int MAX_PROCS = 20;

        private const string CODE_LIKE_PREFIX = "%!";       // use to make codes %like%
        private const string EXACT_MATCH_PREFIX = "&!";     // use to make desc/result exact match
        private const string CHAR_COMMA = "||";             // use to insert literal comma in word list
        private const string NOT_PREFIX = "!!";             // use to insert literal comma in word list
        private const string EXACT_SQL_PREFIX = "!SQL!";     // use to give sql constraint as given
        private const string STARTS_WITH = "[!";     // use to give sql constraint as given

        private struct indicator_data {
            public bool is_checked;
            public int radio_group;
        }

        private struct proc_data {
            public int procedure_number;
            public DateTime start;
            public DateTime finish;
        }

        // These are database CHART_ITEMs for this patient
        //private CHART_ITEM[] _chart_items_since_admission;
        //private CHART_ITEM[] _chart_items_since_unit_arrival;
        //private CHART_ITEM[] _chart_items_during_pull_period;
        //private CHART_ITEM[] _chart_items_pull_period_plus;
        //private CHART_ITEM[] _chart_items_since24hrs;
        //private CHART_ITEM[] _chart_items_since13hrs;
        private CHART_ITEM[] _chart_items_since25hrs;
        //private CHART_ITEM[] _chart_items_since9hrs;
        // These are database PROCEDURE_EVENTs for this patient
        private PROCEDURE_EVENT[] _procedure_events;

        private indicator_data[] _inds;
        private List<proc_data> _procs;
        private List<proc_data> _outcomes;

        private const int DEFAULT_BUCKET_SIZE = 20;       //min

        private PatientInfo _pat;
        private int _bucket_size = DEFAULT_BUCKET_SIZE;
        private bool is_default;
        private string txarea;


        private enum SearchDepth
        {
            SearchDefault,
            SearchPullRange,            //search within the current pull     -- default
            SearchSinceArrival,         //search since arrival to the unit
            SearchSinceAdmission,        //search everything since admission to the hospital
            SearchPullPlus,
            SearchSince25Hrs,
            SearchSince24Hrs,
            SearchSince16Hrs,
            SearchSince13Hrs,
            SearchSince9Hrs
        }

        private enum CountMode {
            CountAll,
            CountFirst                  //stop after one is found
        }

        private enum GetValueMode {
            GetTotal,
            GetMax,
            GetLast
        }

        enum Frequencies {
            QNONE,
            Q4H,
            Q2H,
            Q1H,
            Q30M
        }

        private struct fmapRow {
            public double los_high;                       //the LOS being testing
            public int[] freq;                           //the count required for each Q value
        }
        private List<fmapRow> _freq_map;                    //1,2,4,8,12,24 hours

        private struct gBucket
        {
            public int bucket;
            public string code;
            public DateTime evdt;
        }


        //
        // This is the main entry point
        //
        public void ProcessPatient(PatientInfo pat)
        {
            _pat = pat;
            bool use_default = false;

            InitIndicators(); // sets is_default
            InitProcs();
            if (!is_default)
            {
//                1   ADL - Self / Minimal Care
//2   ADL - Partial Care
//3   ADL - Complete / ADL - 2 or more Caregivers

//4   ADL - Supervision

//5   Cognitive Support
//6   Cognitive Support q 1 Hour

//7   Safety Management q30 Minutes
//8   Safety Management q15 Minutes
//9   Safety Management q 5 Minutes

//10  Behavior / Emotional Management
//11  Behavior / Emotional Management q 4 Hours
//12  Behavior / Emotional Management q 2 Hours
//13  Behavior / Emotional Management q 1 Hour
//14  Behavior / Emotional Management q 30 Minutes

//15  Behavior Crisis Prevention Plan

//16  Medication Management q 4 Hours
//17  Medication Management q 2 Hours

//18  Physiological Assessment q 4 Hours
//19  Physiological Assessment q 2 Hours
//20  Physiological Assessment q 1 Hours

//21  Fluid Management

//22  Wound / Injury Management

//23  Patient / Family Education >= 1 Hour by RN

//24  Coordination of Care >= 1 Hour by RN
                LoadFreqTable();
                LoadPatientChart();
                Check_1_2_3();
                Check_4();
                Check_5_6();
                //Check_7_8_9();
                //Check_10_11_12_13_14();
                //Check_15();
                //Check_16_17();
                //Check_18_19_20();
                //Check_21();
                //Check_22();
                Check_23();
                Check_24();
                CheckUserDefined();
                //Check_Activities();
                AtLeastOneADL();
            }

            HighestIndicatorInEachGroupWins();

            //if (!is_default)
            //{
            //    CheckProcs();
            //    CheckOutcomes();
            //}

            if (Program.g_no_output) return;
            if (_pat.default_ptype > DeterminePtypeOfIndicators())
            { // if the default pt type is higher than the pt type of this class
                // then if there is a default classification in the past 16 hrs
                // then make these indicators the default indicators.
                use_default = ExistDefaultInPast16hrs();
                if (use_default)
                {
                    Program.VerboseAudit("Default indicators will be used");
                }
            }
            OutputClass(use_default);
            //OutputProcs();
            //OutputOutcomes();
        }


        private void InitIndicators()
        {
            // Make a new (empty) indicator array
            _inds = new indicator_data[MAX_INDS + 1];           // This 1 based so add one
            is_default = false;
            txarea = "";
            if (_pat.los_hours <= 4.0)
            {
                is_default = true;
                Program.VerboseAudit("Patient was here 4 hrs or less. Will receive default indicators " + _pat.default_inds_str);
                foreach (var ind in _pat.default_inds)
                {
                    if (ind <= _inds.GetUpperBound(0))
                    {
                        _inds[ind].is_checked = true;
                    }
                }
            }


                // get indicator radio groups from the database
                // ** (This database access can be replaced once we have a C# methodolgy cache)
                var db = PFSDBUtility.NewPfsDataContext();
            var query = from idef in db.INDICATOR_DEFINITIONs
                        where (idef.METHODOLOGY_ID == _pat.meth_id)
                        select idef;
            foreach (var idef in query) {
                if (idef.INDICATOR_NUMBER <= _inds.GetUpperBound(0)) {
                    // (convert nulls to zero)
                    _inds[idef.INDICATOR_NUMBER].radio_group = PFSDBUtility.DBToInt(idef.RADIO_GROUP);
                }
            }
        }

        private void InitProcs()
        {
            _procs = new List<proc_data>();
            _outcomes = new List<proc_data>();
        }

        private fmapRow LoadFreqTableRow(double los_high, string values)
        {
            fmapRow fmrow;

            fmrow.los_high = los_high;
            fmrow.freq = new int[Enum.GetNames(typeof(Frequencies)).Length];

            var arr = values.Split(',');
            for (int i = 0; i <= fmrow.freq.GetUpperBound(0); i++)
            {
                fmrow.freq[i] = arr[i].ToInteger();
            }
            return fmrow;
        }

        // These are the number of times you need to do something during a certain time period (LOS)
        // in order for it to be considered one of the frequencies listed.
        private void LoadFreqTable()
        {
            _freq_map = new List<fmapRow>();
            //                              LOS,  None Q4h Q2h Q1h Q30m
            _freq_map.Add(LoadFreqTableRow(1, "    0,  0,  0,  1,  2"));
            _freq_map.Add(LoadFreqTableRow(2, "    0,  0,  1,  2,  4"));
            _freq_map.Add(LoadFreqTableRow(4, "    0,  1,  2,  4,  8"));
            _freq_map.Add(LoadFreqTableRow(6, "    0,  2,  3,  6,  9"));
            _freq_map.Add(LoadFreqTableRow(8, "    0,  2,  4,  8, 12"));
            _freq_map.Add(LoadFreqTableRow(12, "   0,  3,  6, 12, 18"));
            _freq_map.Add(LoadFreqTableRow(16, "   0,  4,  8, 16, 24"));
            _freq_map.Add(LoadFreqTableRow(24, "   0,  6, 12, 24, 36"));
            //New freq table 2/5/14
            //q4	q2	q1	q30     q30
            //            Non-ICU	ICU & SD
            // 4	8	15	29	    36
            // 3	5	9	17	    24
            // 2	4	7	13	    19
            // 2	3	5	10	    13

            //to read this table: if you have at least 6 asessmenets in a 12-hour period, you get Q1h
            //(LOS=12, column Q1h has a count of 6)
        }

        private Frequencies FreqForCount(double los_hours, int count)
        {
            foreach (var fmrow in _freq_map) {
                if (los_hours <= fmrow.los_high) {
                    // Option: pro-rate the count upward if the LOS is less than the row we are interested in.
                    //         This will bump the count to what it might have been at the full LOS.
                    // Note: truncate the result; rounding inflates the value too much.
                    int prorated_count = (int)((fmrow.los_high / los_hours) * count);

                    // foreach goes low to high; go from high to low instead
                    for (int j = (int)Frequencies.Q30M; (j > (int)Frequencies.QNONE); j--) { //search right to left
                        if (prorated_count >= fmrow.freq[j]) {
                            return (Frequencies)j;
                        }
                    } // next j
                }
            }

            return Frequencies.QNONE;
        }

        //
        // Load patient chart from the database (one DB access)
        // Load everything since admission
        //
        private int LoadPatientChart()
        {
            int ct_in_25hrs = 0;
            // Get the entire patient chart (all units and dates for this patient)
            var db = PFSDBUtility.NewPfsDataContext();
            var query = from item in db.CHART_ITEMs
                        where (item.ENCOUNTER_ID == _pat.encounter_id)
                        where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-25))
                        select item;
            // Save the result
            ct_in_25hrs = query.Count();
            Program.VerboseAudit("Since 25 hrs count=" + ct_in_25hrs);
            _chart_items_since25hrs = query.ToArray();

            // Convert all results to lower case for case insensitive comparisons
            foreach (var item in _chart_items_since25hrs)
            {
                if (item.CATEGORY != null) item.CATEGORY = item.CATEGORY.ToLower();
                if (item.CODE != null) item.CODE = item.CODE.ToLower();
                if (item.DESCRIPTION != null) item.DESCRIPTION = item.DESCRIPTION.ToLower();
                if (item.FIELD_NAME != null) item.FIELD_NAME = item.FIELD_NAME.ToLower();
                if (item.RESULT != null) item.RESULT = item.RESULT.ToLower();
            }

            // Prepare more versions of the chart

            //var query2 = from item in _chart_items_since25hrs
            //             where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-13)) && (item.EVENT_DATETIME <= _pat.pull_finish)
            //             select item;
            //Program.VerboseAudit("Since 13 hrs count=" + query2.Count());
            //_chart_items_since13hrs = query2.ToArray();

            //query2 = from item in _chart_items_since25hrs
            //             //                     where (item.EVENT_DATETIME >= _pat.pull_start) && (item.EVENT_DATETIME <= _pat.pull_finish)
            //         where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-8)) && (item.EVENT_DATETIME <= _pat.pull_finish)
            //         select item;
            //_chart_items_during_pull_period = query2.ToArray();

            return ct_in_25hrs;
        }

        // Delay looking for patient procedures until they are wanted
        private void LoadPatientProceduresIfNeeded()
        {
            if (_procedure_events != null) return;

            var db = PFSDBUtility.NewPfsDataContext();
            var query = from proc in db.PROCEDURE_EVENTs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                        select proc;
            _procedure_events = query.ToArray();
        }



        // Started a new chart item query (of a certain depth)
        // Default is the chart during the pull range.  Options for unit arrival and admission to hospital.
        private IEnumerable<CHART_ITEM> StartNewQuery()
        {
                return StartNewQuery(SearchDepth.SearchDefault);
        }
        private IEnumerable<CHART_ITEM> StartNewQuery(SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;

            switch (search_depth)
            {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    return (from item in _chart_items_since25hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-8) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince25Hrs:
                    return (from item in _chart_items_since25hrs where (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince24Hrs:
                    return (from item in _chart_items_since25hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-24) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince16Hrs:
                    return (from item in _chart_items_since25hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-16) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince13Hrs:
                    return (from item in _chart_items_since25hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-13) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince9Hrs:
                    return (from item in _chart_items_since25hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-9) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
            }
            return null;
        }

        // Is this search term a word or a list of words?
        private bool ValueIsAList(string s)
        {
            return (s != null) && s.Contains(',');                //is this a comma-separated list?
        }

        // Break apart a wordlist and prepare its members
        private string[] SplitOnCommaAndPrepareElements(string s)
        {
            // If a word list has a space after each comma, we will get leading blanks in each term
            var arr = s.Split(',');
            for (int i = 0; i <= arr.GetUpperBound(0); i++) {
                arr[i] = arr[i].Trim();                         // get rid of leading blanks
                arr[i] = arr[i].Replace(CHAR_COMMA, ",");       // insert a literal comma into search word
                arr[i] = arr[i].ToLower();                      // convert for case-insensitive comparisons
            }
            return arr;
        }

        // Add a basic chart item filter to a LINQ query
        //
        // cat = exact match
        // code_list = exact match; optional "like" match
        // desc_list = "like" match; optional exact match
        // field = exact match
        // result_list = "like" match; optional exact match
        //
        // These are comma-separated lists.  
        // Search for a literal comma by using CHAR_COMMA in the string.
        private IEnumerable<CHART_ITEM> AndItemFilter(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list)
        {
            // Converted all values to lower case for case-insensitive comparisons.
            if (!String.IsNullOrEmpty(cat))
            {
                cat = cat.ToLower();
                query = query.Where(e => e.CATEGORY == cat);
            }

            if (!String.IsNullOrEmpty(code_list))
            {
                code_list = code_list.ToLower();
                if (code_list.Left(2) == CODE_LIKE_PREFIX)
                {            // override for "like" match?
                    query = query.Where(e => e.CODE.Contains(code_list.Substring(2)));
                }
                else if (code_list.Left(2) == EXACT_MATCH_PREFIX)
                {
                    query = query.Where(e => e.CODE == code_list.Substring(2));
                }
                else if (ValueIsAList(code_list))
                {
                    query = AndCodeInList(query, code_list);            // find one of the words
                }
                else
                {
                    query = query.Where(e => e.CODE.Contains(code_list));      // find this word
                }
            }

            if (!String.IsNullOrEmpty(desc_list))
            {
                desc_list = desc_list.ToLower();
                if (desc_list.Left(2) == EXACT_MATCH_PREFIX)
                {          // exact match override?
                    query = query.Where(e => e.DESCRIPTION == desc_list.Substring(2));
                }
                else if (ValueIsAList(desc_list))
                {
                    query = AndDescriptionInList(query, desc_list);
                }
                else
                {
                    desc_list = desc_list.Replace(CHAR_COMMA, ",");     // look for literal comma requests
                    query = query.Where(e => e.DESCRIPTION.Contains(desc_list));
                }
            }

            if (!String.IsNullOrEmpty(field))
            {
                field = field.ToLower();
                query = query.Where(e => e.FIELD_NAME == field);
            }

            if (!String.IsNullOrEmpty(result_list))
            {
                query = query.Where(e => e.RESULT != null);
                result_list = result_list.ToLower();                        // make all results case insensitve
                if (result_list.Left(2) == EXACT_MATCH_PREFIX)
                {     // exact match override?
                    query = query.Where(e => e.RESULT == result_list.Substring(2));
                }
                else if (ValueIsAList(result_list))
                {
                    query = AndResultInList(query, result_list);
                }
                else if (result_list.Left(2) == STARTS_WITH)
                {
                    query = query.Where(e => e.RESULT.StartsWith(result_list.Substring(2)));
                }
                else
                {
                    result_list = result_list.Replace(CHAR_COMMA, ",");     // look for literal comma requests
                    query = query.Where(e => e.RESULT.Contains(result_list));
                }
            }
            return query;
        }

        // Look for a code that is one of the words in the list.
        private IEnumerable<CHART_ITEM> AndCodeInList(IEnumerable<CHART_ITEM> query, string code_list)
        {
            if (String.IsNullOrEmpty(code_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(code_list);
//            return query.Where(e => arr.Any(x => x.Contains(e.CODE))); // "like" match
            return query.Where(e => e.CODE.ContainsAny(arr)); // "like" match
        }

        // Look for a description that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndDescriptionInList(IEnumerable<CHART_ITEM> query, string desc_list)
        {
            if (String.IsNullOrEmpty(desc_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(desc_list);
            //return query.Where(e => arr.Any(x => x.Contains(e.DESCRIPTION))); // "like" match
            return query.Where(e => e.DESCRIPTION.ContainsAny(arr)); // "like" match
        }

        // Look for a result that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultInList(IEnumerable<CHART_ITEM> query, string result_list)
        {
            bool use_start_with = false;

            if (String.IsNullOrEmpty(result_list)) return query;

            if (result_list.Left(2) == STARTS_WITH)
            {
                use_start_with = true;
                result_list = result_list.Substring(2);
            }

            var arr = SplitOnCommaAndPrepareElements(result_list);
            //            return query.Where(e => arr.Any(x => x.Contains(e.RESULT))); // "like" match
            if (use_start_with)
                return query.Where(e => arr.Any(item => e.RESULT.StartsWith(item)));
            else
                return query.Where(e => e.RESULT.ContainsAny(arr)); // "like" match

        }

        // Look for a result that does not contain one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultNotInList(IEnumerable<CHART_ITEM> query, string result_list)
        {
            if (String.IsNullOrEmpty(result_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(result_list);
//            return query.Where(e => !arr.Any(x => x.Contains(e.RESULT))); // "like" match
            return query.Where(e => !e.RESULT.ContainsAny(arr)); // "like" match
        }

        private string DescribeSearchDepth(SearchDepth search_depth)
        {
            string result = "";

            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    //result = "in pull range"
                    result = "";                         //be quiet since this is default
                    break;
                case SearchDepth.SearchSinceArrival:
                    result = "since arrival to unit";
                    break;
                case SearchDepth.SearchSinceAdmission:
                    result = "since admission";
                    break;
                case SearchDepth.SearchPullPlus:
                    result = "in LOS range plus 4 hours after";
                    break;
            }

            return result;
        }

        // Describe a query that has already been run
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Make a simple query and describe the results
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            var arr = query.ToArray();
            return Describe(arr, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private string LookingFor(string so_far, string what, string op, string value)
        {
            string result;
            if (String.IsNullOrEmpty(value)) return so_far;

            if (so_far == "")
                result = "looking for " + what;
            else
                result = so_far + " and " + what;

            if (value.Left(2) == EXACT_MATCH_PREFIX)
                result += "='" + value.Substring(2) + "'";
            else if (value.Left(2) == CODE_LIKE_PREFIX)
                result += " contains " + "'" + value.Substring(2) + "'";
            else if (ValueIsAList(value))
                result += " in '" + value + "'";
            else
                result += op + "'" + value + "'";

            return result;
        }
        private string Describe(CHART_ITEM[] arr, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string result = "";
            result = LookingFor(result, "cat", "=", cat);
            result = LookingFor(result, "code", "=", code_list);
            result = LookingFor(result, "desc", " contains ", desc_list);
            result = LookingFor(result, "field", "=", field);
            result = LookingFor(result, "result", " contains ", result_list);

            if (arr.Count() == 0) {
                result += "; not found " + DescribeSearchDepth(search_depth);
            } else {
                var e = arr[0];
                result = "FOUND: " + result + "; found";
                // We might have searched for a pattern or word list in several fields - show what was found
                if (e.CATEGORY != null) result += " cat='" + e.CATEGORY + "'";
                if (e.CODE != null) result += " code='" + e.CODE + "'";
                if (e.DESCRIPTION != null) result += " desc='" + e.DESCRIPTION + "'";
                if (e.FIELD_NAME != null) result += " field='" + e.FIELD_NAME + "'";
                if (e.RESULT != null) result += " result='" + e.RESULT + "'";
                // Are there more results?  Just say how many; we aren't going to list them.
                if (arr.Count() == 2) {
                    result += " (1 more result)";
                } else if (arr.Count() > 2) {
                    result += " (" + (arr.Count() - 1) + " more results)";
                }
            }

            return result;
        }


        //Set an indicator for this reason (low level)
        private void SetInd(int inum, string reason)
        {
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Set Ind #" + inum + ": " + reason);          //already set - repeat for verbose only
            } else {
                _inds[inum].is_checked = true;
                Program.Audit("Set Ind #" + inum + ": " + reason);
            }
        }

        //Clear an indicator for this reason (low level)
        private void ClrInd(int inum, string reason)
        {
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Clr Ind #" + inum + ": " + reason);          //already clear - repeat verbose only
            } else {
                _inds[inum].is_checked = false;
                Program.Audit("Clr Ind #" + inum + ": " + reason);
            }
        }

        // Count how many items meet the conditions
        // All items are listed
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountItems(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            int count = query.Count();

            // always return what was found
            found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
            // echo the result?
            if (trace) Program.VerboseAudit(found_what);

            return count;
        }


        // Count how many items meet the conditions
        // This has the option to list every entry or give a total
        private int CountResultInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what = "";
            string s = "";

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);

            var arr = SplitOnCommaAndPrepareElements(result_list);
//Walker Schlundt
//Set Ind #9: found '' in cat='' code='301870' field='' result='oriented x4' 

            foreach (var item in query) {
                // Figure out which of the search words were found
                for (int i = 0; i <= arr.GetUpperBound(0); i++)
                {
                    if (item.RESULT.Contains(arr[i]))
                    {
                        count++;
                        s = arr[i];
                        found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' code='" + item.CODE + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "'";
                        //if (trace) Program.VerboseAudit(found_what);
                        Program.VerboseAudit(found_what);
                    }
                }
                if (count_mode != CountMode.CountAll) break;        //stop counting

            }

            if (count > 0) {
                //We already printed what was found; maybe add how many?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        // Count how many items meet the conditions
        // This has the option to list every entry or give a total
        private int CountResultInListEXCEPTList(string cat, string code_list, string desc_list, string field, string result_list, string negresult_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what = "";

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            query = AndResultNotInList(query, negresult_list);

            var arr = SplitOnCommaAndPrepareElements(result_list);

            foreach (var item in query)
            {
                // Figure out which of the search words were found
                var s = item.RESULT.ContainsWhich(arr);
                found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' code='" + item.CODE + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "'";
                if (trace) Program.VerboseAudit(found_what);
                count++;
                if (count_mode != CountMode.CountAll) break;        //stop counting
            }

            if (count > 0)
            {
                //We already printed what was found; maybe add how many?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            }
            else
            {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        // Count how many items contain ALL results in list
        private int CountResultInListAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what = "";

            var arr = SplitOnCommaAndPrepareElements(result_list);

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            for (int i = 0; i <= arr.GetUpperBound(0); i++)
            {
                if (arr[i].StartsWith(NOT_PREFIX))
                {
                    query = query.Where(e => !e.RESULT.Contains(arr[i].Substring(2)));
                }
                else
                {
                    query = query.Where(e => e.RESULT.Contains(arr[i]));
                }
            }

            count = query.Count();

            if (count > 0)
            {
                found_what = "found item with all results in '" + result_list + "' without those prefixed by !!";
                if (trace) Program.VerboseAudit(found_what);
            }
            else
            {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (ValueIsAList(result_list))
            {
                return CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what);
            } else {
                return CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what);
            }
        }

        // Is there an item with this result?
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContains(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (ValueIsAList(result_list))
            {
                return (CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            } else {
                return (CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
            }
        }

        // Result contains all in list
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (ValueIsAList(result_list))
            {
                return (CountResultInListAll(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            }
            else
            {
                found_what = "Not valid for non-list results.";
                return false;
            }
        }
        // Count how many items meet the conditions, with none of the results in result_list.
        // This has the option to say how many were found rather than list all.
        private int CountResultNotInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0, rec_count = 0;
            bool found_one;

            found_what = "";
            var arr = SplitOnCommaAndPrepareElements(result_list);         // also deals with CHAR_COMMA

            //Do not filter by result here; we need all results
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            foreach (var item in query) {
                rec_count++;
                //Look for each search word in the result
                found_one = false;

                foreach (string s in arr) {
                    if (String.Equals(item.RESULT, s)) {
                        found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "' -- ignore this";
                        Program.VerboseAudit(found_what);
                        found_one = true;
                        break;
                    }
                }

                if (!found_one) {
                    //none of the words were found - good!
                    found_what = Describe(cat, code_list, desc_list, field, "", search_depth) + " -- does not contain '" + result_list + "'";
                    Program.VerboseAudit(found_what);
                    count++;
                    if (count_mode != CountMode.CountAll) break;
                }
            }

            if (count > 0) {
                //We already printed what was found; maybe show the total?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else if (rec_count > 0) {
                //We already printed what we ignored
            } else {
                //Print what did not exist
                found_what = Describe(cat, code_list, desc_list, field, "", search_depth);      //not found
                Program.VerboseAudit(found_what);
            }

            return count;
        }

        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, string found_what)
        {
            // Search in the pull range with trace on
            return CountResultDoesNotContain(cat, code_list, desc_list, field, result_list, out found_what, SearchDepth.SearchDefault, true);
        }
        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, out string found_what, SearchDepth search_depth, bool trace)
        {
            return CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0;
        }

        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            return (CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0);
        }

        // Set the indicator if the conditions are met
        //
        private bool SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SearchDepth s = SearchDepth.SearchDefault;
            if (inum == 4) s = SearchDepth.SearchSince13Hrs;
            if (inum == 5) s = SearchDepth.SearchSince16Hrs;
            if (inum == 6) s = SearchDepth.SearchSince16Hrs;
            if (inum == 7) s = SearchDepth.SearchSince13Hrs;
            if (inum == 8) s = SearchDepth.SearchSince24Hrs;
            if (inum == 9) s = SearchDepth.SearchSince16Hrs;
            if (inum == 10) s = SearchDepth.SearchSince9Hrs;
            if (inum == 11) s = SearchDepth.SearchSince9Hrs;
            if (inum == 12) s = SearchDepth.SearchSince13Hrs;
            if (inum == 13) s = SearchDepth.SearchSince13Hrs;
            if (inum == 14) s = SearchDepth.SearchSince25Hrs;
            if (inum == 19) s = SearchDepth.SearchSince9Hrs;
            if (inum == 20) s = SearchDepth.SearchSince9Hrs;
            if (inum == 21) s = SearchDepth.SearchSince25Hrs;
            if (inum == 22) s = SearchDepth.SearchSince13Hrs;
            if (inum == 23) s = SearchDepth.SearchSince9Hrs;
            if (inum == 24) s = SearchDepth.SearchSince9Hrs;
            return SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, s);
        }
        private bool SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            bool b = false;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return true;

            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what)) {
                SetInd(inum, found_what);                          //echo here - found
                b = true;
            } else {
                Program.VerboseAudit(found_what);                  //and here - not found
            }
            return b;
        }

        private void SetIndIfResultBetween(int inum, string cat, string code_list, string desc_list, string field, int loval, int hival)
        {
            SetIndIfResultBetween(inum, cat, code_list, desc_list, field, loval, hival, SearchDepth.SearchDefault);
        }

        private void SetIndIfResultBetween(int inum, string cat, string code_list, string desc_list, string field, int loval, int hival, SearchDepth search_depth)
        {
            int count = 0;
            string found_what = "";

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");
            foreach (var item in query)
            {
                if (item.RESULT.IsNumeric())
                    {
                    if (item.RESULT.Val() >= loval && item.RESULT.Val() <= hival)
                    {
                        count++;
                        found_what = "found code=" + item.CODE + " with result=" + item.RESULT;
                        Program.VerboseAudit(found_what);
                    }
                    }
            }

            if (count > 0)
            {
                SetInd(inum, found_what);
            }
        }


        // Special contains all
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContainsAll(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return;

            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                          //echo here - found
            }
            else
            {
                Program.VerboseAudit(found_what);                  //and here - not found
            }
        }
        // Set the indicator if the result does not contain any of the words in result_list
        //
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultDoesNotContain(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return;

            //Turn trace off for ResultDoesNotContain() and echo what was set below with SetInd
            if (ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                           //echo here - not found
            } else {
                Program.VerboseAudit(found_what);                   //and here - found
            }
        }

        // Clear the indicator if the result contains one of the words in the result_list
        //
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already clear
            if (!_inds[inum].is_checked) return;

            //Turn trace off for ResultContains() and echo what was set below with SetInd
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what)) {
                ClrInd(inum, found_what);                           //echo here - found
            } else {
                Program.VerboseAudit(found_what);                   //and here - not found
            }
        }


        //These slightly smaller functions are meant for places where you aren't really looking for a chart result
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }

        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            return (CountResultContains(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
        }

        // Variations with optional parameters (can switch to optional parameters in VS 2010)
        //
        private void SetIndIfFound(int inum, string cat)
        {
            SetIndIfResultContains(inum, cat, "", "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list)
        {
            SetIndIfResultContains(inum, cat, code_list, "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private void ClrIndIfFound(int inum, string cat)
        {
            ClrIndIfResultContains(inum, cat, "", "", "", "", SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }


        // Get the max/total value from a result (usually in the middle of the text)
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetIntValue(get_mode, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            int value;
            int result = 0;
            bool found_one = false;

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);

            //Look for a number in the result

            foreach (var item in query) {
                // RESULT might be null so convert to empty string so Split won't blow up
                var arr = PFSDBUtility.DBToString(item.RESULT).Split(' ');
                foreach (string s in arr) {
                    //Look for 1st character numeric; IsNumeric("60min") will fail so look at 1st char only
                    if (s.Left(1).IsNumeric()) {
                        value = (int)s.Val();                           //Use Val; ToInteger will error on "60min"
                        switch (get_mode) {
                            case GetValueMode.GetMax:
                                result = Math.Max(value, result);       //max
                                break;
                            case GetValueMode.GetTotal:
                                result += value;                        //total
                                break;
                            case GetValueMode.GetLast:
                                result = value;                         //last
                                break;
                        }

                        // print what we are searching for (the first time)
                        if (!found_one) {
                            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
                        }
                        found_one = true;
                        // print each value found
                        Program.VerboseAudit("  found numeric value " + result);
                        //Keep going in case there are more
                    }
                }
            }

            if (!found_one) {
                //show what was not found
                Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
            }

            return result;
        }

        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetMaxValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return GetIntValue(GetValueMode.GetMax, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetTotalValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return GetIntValue(GetValueMode.GetTotal, cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Get a result; returns true if found with return_result
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result)
        {
            return GetResult(cat, code_list, desc_list, field, out return_result, SearchDepth.SearchDefault);
        }
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0) {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
            } else {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }
        private bool GetResultForInspection(string cat, string code_list, string desc_list, string field, string result, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result);

            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
            }
            else
            {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }

        private void GetResultforOther(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "Other");
            return_result = "";
            foreach (var item in query)
            {
                if (!String.Equals(item.RESULT, "Other"))
                {
                    return_result = item.RESULT;
                    //Program.VerboseAudit(found_what);
                    break;
                }

            }
            return;
        }

        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, out string return_result, out DateTime return_evdt)
        {
            return GetResultAndEVDT(cat, code_list, desc_list, field, out return_result, out return_evdt, SearchDepth.SearchDefault);
        }
        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, out string return_result, out DateTime return_evdt, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
                return_evdt = PFSDBUtility.DBToDateTime(query.First().EVENT_DATETIME);
            }
            else
            {
                return_result = "";
                return_evdt = DateTime.MinValue;
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }


        //======================================================================================
        // Mapping starts here
        //======================================================================================

        private void Check_1_2_3()
        {
            string found_what;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("1. ADL Self");
            Program.VerboseAudit("2. ADL Assist");
            Program.VerboseAudit("3. ADL Complete/2+");
            Program.VerboseAudit("---------------");

            // 4. COMPLETE CARE

            if (_pat.age < 4.0) SetInd(4, "Age < 4 years");
            if (_pat.age >= 4.0 && _pat.age < 7.0) SetInd(2, "Age 4-6 years");
            //if (_inds[4].is_checked) return;

            string reslist = "";

            reslist = "Ambulate in hall,Ambulate in room,Bathroom privileges,Bedrest,Bike,Chair,Commode,Dangle,Supervised exercise,Held,Stand at bedside,Tilt table,Turn,Up in chair,Wagon,In bed,bedpan";
            bool B1 = Exists("", "9990000305560", "", "", reslist, SearchDepth.SearchSince13Hrs);
            reslist = "1 Assist,2 Assist,3 Assist,>4 Assist,4 or more Assist,Supervision required";
            bool B2 = Exists("", "9993040109530", "", "", reslist, SearchDepth.SearchSince13Hrs);
            reslist = "Lying right side,Lying left side,Supine,Semi Fowler,Pillow support,Prone,Sitting,Standing,Up in chair,Micro turn left,Micro turn right,Do Not Turn,Unstable to turn,Turn right side,Turn left side";
            bool B3 = Exists("", "9990000400604", "", "", reslist, SearchDepth.SearchSince13Hrs);
            if (B1 && B2) SetInd(2, "B1 and B2");
            if (B2 && B3) SetInd(2, "B2 and B3");

            reslist = "Assist,Total Care";
            bool B4 = Exists("", "9991025006475", "", "", reslist, SearchDepth.SearchSince13Hrs);
            reslist = "Left side,Left tilt,Right side,Right tilt,Semi-fowler,Fowlers,Knee chest,Chair,Rocking chair,Supine,Trendelenberg,Squatting,Standing,Kneeling,Walking";
            bool B5 = Exists("", "1028000027", "", "", reslist, SearchDepth.SearchSince13Hrs);
            if (B4 && B5) SetInd(2, "B4 and B5");

            reslist = "1 Assist,2 Assist,3 Assist,>4 Assist,Supervised";
            bool B6 = Exists("", "9990007060350", "", "", reslist, SearchDepth.SearchSince25Hrs);
            reslist = "Bathed,Chlorhexidine Bath,Showered,Bath in a bag,Catheter care,Foley care,Peri care,Hair washed,Hair dried/curled,Shaved";
            bool B7 = Exists("", "9990000342030", "", "", reslist, SearchDepth.SearchSince25Hrs);
            if (B6 && B7) SetInd(2, "B6 and B7");

            reslist = "Partial assist,Complete assist";
            bool B8 = Exists("", "9990000305650", "", "", reslist, SearchDepth.SearchSince25Hrs);
            if (B7 && B8) SetInd(2, "B7 and B8");

            reslist = "Needs assist,Supervised,Total assist";
            SetIndIfResultContains(2, "", "9990000700380", "", "", reslist, SearchDepth.SearchSince25Hrs);
            reslist = "";
            bool c10 = SetIndIfResultContains(2, "", "3043040100003", "", "", reslist, SearchDepth.SearchSince25Hrs);
            bool c11 = SetIndIfResultContains(2, "", "9993040100004", "", "", reslist, SearchDepth.SearchSince25Hrs);
            bool c12 = SetIndIfResultContains(2, "", "3043040101422", "", "", reslist, SearchDepth.SearchSince25Hrs);
            bool c13 = SetIndIfResultContains(2, "", "9993040101423", "", "", reslist, SearchDepth.SearchSince25Hrs);

            reslist = "";
            bool ADLLDA10 = SetIndIfResultContains(2, "", "3045001094", "", "", reslist, SearchDepth.SearchSince25Hrs);
            reslist = "NG tube,OG tube,G-tube,J-tube,ND tube,OD tube,PEG-tube";
            bool ADLLDA11 = SetIndIfResultContains(2, "", "9991733565652", "", "", reslist, SearchDepth.SearchSince25Hrs);
            bool ADLLDA12 = SetIndIfResultContains(2, "", "9991733569855", "", "", "", SearchDepth.SearchSince25Hrs);
            bool ADLLDA13 = SetIndIfResultContains(2, "", "9993040304520", "", "", "", SearchDepth.SearchSince25Hrs);

            reslist = "Yes";
            bool c15 = SetIndIfResultContains(2, "", EXACT_MATCH_PREFIX + "23", "", "", reslist, SearchDepth.SearchSince13Hrs);
            reslist = "";
            bool c16 = SetIndIfResultContains(2, "", "3045001090", "", "", reslist, SearchDepth.SearchSince13Hrs);
            bool c17 = SetIndIfResultContains(2, "", "3045001089", "", "", reslist, SearchDepth.SearchSince13Hrs);
            reslist = "Yes";
            bool c18 = SetIndIfResultContains(2, "", EXACT_MATCH_PREFIX + "16", "", "", reslist, SearchDepth.SearchSince13Hrs);

            reslist = "";
            bool ADLLDA1 = SetIndIfResultContains(2, "", "3045001088", "", "", reslist, SearchDepth.SearchSince13Hrs);
            bool ADLLDA2 = SetIndIfResultContains(2, "", "9990007085310", "", "", reslist, SearchDepth.SearchSince13Hrs);
            bool ADLLDA3 = SetIndIfResultContains(2, "", "9993040104906", "", "", reslist, SearchDepth.SearchSince13Hrs);
            bool ADLLDA4 = SetIndIfResultContains(2, "", "9990000396150", "", "", reslist, SearchDepth.SearchSince13Hrs);
            bool ADLLDA5 = SetIndIfResultContains(2, "", "9990007085490", "", "", reslist, SearchDepth.SearchSince13Hrs);

            reslist = "Done";
            bool B26 = Exists("", "9991600100064", "", "", reslist, SearchDepth.SearchSince13Hrs);
            if (B6 && B26) SetInd(2, "B6 and B26");

            reslist = "Stand-by assist ,One staff assist ,Two staff assist ";
            bool C27 = SetIndIfResultContains(2, "", "9991600100065", "", "", reslist, SearchDepth.SearchSince13Hrs);//c27
            reslist = "Stand-by assist,One staff assist,Two staff assist";
            bool C28 = SetIndIfResultContains(2, "", "9991600100066", "", "", reslist, SearchDepth.SearchSince13Hrs);//c28
            reslist = "Bedpan,Catheter,Commode,Incontinence pad";
            bool C29 = SetIndIfResultContains(2, "", "9991600100067", "", "", reslist, SearchDepth.SearchSince13Hrs);//c29
            reslist = "Bed bath";
            bool C30 = SetIndIfResultContains(2, "", "9991600100068", "", "", reslist, SearchDepth.SearchSince13Hrs);//c30

            reslist = "3";
            SetIndIfResultContains(2, "", "9993040000407", "", "", reslist, SearchDepth.SearchSince13Hrs);
            SetIndIfResultBetween(2, "", "3045001023", "", "", 9, 12, SearchDepth.SearchSince13Hrs);
            SetIndIfResultBetween(2, "", "9993040001207", "", "", 9, 12, SearchDepth.SearchSince13Hrs);


            // FOR ADL Extended rows C-1 through C-21, 
            //must include charting for 3 of the 4 bullets below on the shift:
            //   - C1 & C2 or C2&C3 or C4&C5(mobility)
            //   - C6 & 7 or C7&8(hygiene, please carry over for night shift)
            //   -Any of C-9, C - 10, C - 11, C - 12, C - 13, ADL - LDA - 10 or ADL-LDA - 11(feeding, please carry over for night shift)
            //   -any row between C - 15 to C-21 or ADL-LDA - 1, 2, 3, 4 ro 5(toileting)
            reslist = "Ambulate in hall,Ambulate in room,Bathroom privileges,Bedrest,Bike,Chair,Commode,Dangle,Supervised exercise,Held,Stand at bedside,Tilt table,Turn,Up in chair,Wagon,In bed,bedpan";
            bool c1 = Exists("", "9990000305560", "", "", reslist, SearchDepth.SearchSince13Hrs);
            reslist = "1 Assist,2 Assist,3 Assist,>4 Assist,4 or more Assist";
            bool c2 = Exists("", "9993040109530", "", "", reslist, SearchDepth.SearchSince13Hrs);
            reslist = "Lying right side,Lying left side,Supine,Semi Fowler,Pillow support,Prone,Sitting,Standing,Up in chair,Micro turn left,Micro turn right,Do Not Turn,Unstable to turn,Turn right side,Turn left side";
            bool c3 = Exists("", "9990000400604", "", "", reslist, SearchDepth.SearchSince13Hrs);
            reslist = "Assist,Total Care";
            bool c4 = Exists("", "9991025006475", "", "", reslist, SearchDepth.SearchSince13Hrs);
            reslist = "Left side,Left tilt,Right side,Right tilt,Semi-fowler,Fowler,Knee chest,Chair,Rocking chair,Supine,Trendelenberg,Squatting,Standing,Kneeling,Walking";
            bool c5 = Exists("", "1028000027", "", "", reslist, SearchDepth.SearchSince13Hrs);
            reslist = "1 Assist,2 Assist,3 Assist,>4 Assist";

            bool c6 = Exists("", "9990007060350", "", "", reslist, SearchDepth.SearchSince25Hrs);
            reslist = "Bathed,Chlorhexidine Bath,Showered,Bath in a bag,Catheter care,Foley care,Peri care";
            bool c7 = Exists("", "9990000342030", "", "", reslist, SearchDepth.SearchSince25Hrs);
            reslist = "Partial assist,Complete assist";
            bool c8 = Exists("", "9990000305650", "", "", reslist, SearchDepth.SearchSince25Hrs);

            reslist = "Needs assist,Total assist";
            bool c9 = Exists("", "9990000700380", "", "", reslist, SearchDepth.SearchSince25Hrs);

            bool c19 = (ADLLDA1 || ADLLDA2 || ADLLDA3 || ADLLDA4 || ADLLDA5);

            reslist = "Done";
            bool C26 = Exists("", "9991600100064", "", "", reslist, SearchDepth.SearchSince13Hrs);//c26

            //   - C1 & C2 or C2&C3 or C4&C5(mobility)
            //   - C6 & 7 or C7&8(hygiene, please carry over for night shift)
            //   -Any of C-9, C - 10, C - 11, C - 12, C - 13, ADL - LDA - 10 or ADL-LDA - 11(feeding, please carry over for night shift)
            //   -any row between C - 15 to C-21 or ADL-LDA - 1, 2, 3, 4 ro 5(toileting)
            bool Cgroup1 = ((c1 && c2) || (c2 && c3) || (c4 && c5));
            bool Cgroup2 = ((c6 && c7) || (c7 && c8) || (c6 && C26));
            bool Cgroup3 = (c9 || c10 || c11 || c12 || c13 || ADLLDA10 || ADLLDA11 || ADLLDA12 || ADLLDA13);
            bool Cgroup4 = (c15 || c16 || c17 || c18 || c19);
            Program.Audit("D groups: Group1=((d1 & d2) or (d2 & d3) or (d4 & d5))=" + (Cgroup1 ? "Y" : "N") + " Group2=((d6 & d7) or (d7 & d8) or (d6 & d26))=" + (Cgroup2 ? "Y" : "N") + " Group3=(any one of d9-d13,ADL-LDA 10-13)=" + (Cgroup3 ? "Y" : "N") + " Group4=(any one of d15-d19,ADL-LDA 1-5)=" + (Cgroup4 ? "Y" : "N"));
            if (Cgroup1 && !Cgroup2)
            {
                Program.Audit("Mobility is true and Hygiene is false: Force Hygiene Dgroup2 to be true.");
                Cgroup2 = true;
            }
            int CgroupSUM = (Cgroup1 ? 1 : 0) + (Cgroup2 ? 1 : 0) + (Cgroup3 ? 1 : 0) + (Cgroup4 ? 1 : 0);
            if (CgroupSUM >= 3) SetInd(3, "3 or more D groups.");

            if ((C26 || C30) && C27 && (C28 || C29))
                SetInd(3, "[D26 or D30] and D27 and [D28 or D29]");

            reslist = "Strong stimuli to arouse";
            SetIndIfResultContains(3, "", "3045001046", "", "", reslist, SearchDepth.SearchSince13Hrs);//c33


            reslist = "Supervision required";
            bool E2 = Exists("", "9993040109530", "", "", reslist, SearchDepth.SearchSince13Hrs);
            if (E2 && (B1 || B3))
                SetInd(4, "E2 and (E1 or E3)");


            //reslist = "Bedrest,Held,Stand at bedside,Tilt table,Turn,In bed,bedpan";
            //bool d1 = Exists("", "9990000305560", "", "", reslist, SearchDepth.SearchSince13Hrs);
            //reslist = "Ambulate in hall,Ambulate in room,Bathroom privileges,Bike,Commode,Supervised exercise,Up ad lib";
            //bool d1x = Exists("", "9990000305560", "", "", reslist, SearchDepth.SearchSince13Hrs);

            //reslist = "1 Assist,2 Assist,3 Assist,>4 Assist,4 or more Assist";
            //bool d2 = Exists("", "9993040109530", "", "", reslist, SearchDepth.SearchSince13Hrs);
            //reslist = "Lying right side,Lying left side,Supine,Semi Fowler,Pillow support,Prone,Sitting,Standing,Up in chair,Micro turn left,Micro turn right,Do Not Turn,Unstable to turn,Turn right side,Turn left side";
            //bool d3 = Exists("", "9990000400604", "", "", reslist, SearchDepth.SearchSince13Hrs);
            //reslist = "Continuous lateral rotation";
            //SetIndIfResultContains(4, "", "9990000400604", "", "", reslist, SearchDepth.SearchSince13Hrs);

            //reslist = "Total Care";
            //bool d4 = Exists("", "9991025006475", "", "", reslist, SearchDepth.SearchSince13Hrs);
            //reslist = "Left side,Left tilt,Right side,Right tilt,Semi-fowlers,Fowlers,Chair,Supine,Trendelenberg";
            //bool d5 = Exists("", "1028000027", "", "", reslist, SearchDepth.SearchSince13Hrs);
            //reslist = "1 Assist,2 Assist,3 Assist,>4 Assist";
            //bool d6 = Exists("", "9990007060350", "", "", reslist, SearchDepth.SearchSince25Hrs);
            //reslist = "Bathed,Bath in a bag,Catheter care,Chlorhexidine Bath,Foley care,Peri care";
            //bool d7 = Exists("", "9990000342030", "", "", reslist, SearchDepth.SearchSince25Hrs);
            //reslist = "Complete assist";
            //bool d8 = Exists("", "9990000305650", "", "", reslist, SearchDepth.SearchSince25Hrs);

            //reslist = "Total assist";
            //bool d9 = Exists("", "9990000700380", "", "", reslist, SearchDepth.SearchSince25Hrs);

            //reslist = "";
            //bool d10 = Exists("", "3043040100003", "", "", reslist, SearchDepth.SearchSince25Hrs);
            //bool d11 = Exists("", "9993040100004", "", "", reslist, SearchDepth.SearchSince25Hrs);
            //bool d12 = Exists("", "3043040101422", "", "", reslist, SearchDepth.SearchSince25Hrs);
            //bool d13 = Exists("", "9993040101423", "", "", reslist, SearchDepth.SearchSince25Hrs);
            //bool d14 = (ADLLDA10 || ADLLDA11 || ADLLDA12 || ADLLDA13);

            //bool d15 = c15;
            //bool d16 = c16;
            //bool d17 = c17;
            //bool d18 = c18;
            //bool d19 = c19;
            ////bool d20 = c20;

            //reslist = "";
            //bool d21 = Exists("", "3040011360", "", "", reslist, SearchDepth.SearchSince13Hrs);
            //bool d22 = Exists("", "3040002155", "", "", reslist, SearchDepth.SearchSince13Hrs);
            //bool d23 = Exists("", "9993040011213", "", "", reslist, SearchDepth.SearchSince13Hrs);
            //bool d24 = Exists("", "9993041001004", "", "", reslist, SearchDepth.SearchSince13Hrs);
            //bool d25 = Exists("", "9993041001003", "", "", reslist, SearchDepth.SearchSince13Hrs);
            //bool d26 = C26;

            ////"FOR ADL COMPLETE rows D-1 through D-25, must include charting for all of the bullets below on the shift:
            ////- D1 & D2 or D2&D3 or D4&D5(mobility)
            ////- D6 & 7 or D7&8(hygiene, please carry over for night shift)
            ////-Any of D-9, D - 10, D - 11, D - 12, D - 13, D - 14(ADL - LDA - 10 or ADL - LDA - 11)(feeding, please carry over for night shift)
            ////-any rows between D - 15 and D-25 or ADL-LDA - 1, 2,3,4,or 5)  (toileting)
            //bool Dgroup1 = ((d1 && d2) || (d2 && d3) || (d4 && d5));
            //bool Dgroup2 = ((d6 && d7) || (d7 && d8) || (d6 && d26));
            //bool Dgroup3 = (d9 || d10 || d11 || d12 || d13 || d14);
            //bool Dgroup4 = (d15 || d16 || d17 || d18 || d19 || d20 || d21 || d22 || d23 || d24 || d25 || c19);
            //Program.Audit("D groups: Group1=((d1 & d2) or (d2 & d3) or (d4 & d5))=" + (Dgroup1 ? "Y" : "N") + " Group2=((d6 & d7) or (d7 & d8) or (d6 & d26))=" + (Dgroup2 ? "Y" : "N") + " Group3=(any one of d9-d14,ADL-LDA 10-13)=" + (Dgroup3 ? "Y" : "N") + " Group4=(any one of d15-d25,ADL-LDA 1-5)=" + (Dgroup4 ? "Y" : "N"));
            //if (d1x)
            //{
            //    Program.Audit("D1 shows mobility = Force Mobility D Group1 to false.");
            //    Dgroup1 = false;
            //}
            //if (Dgroup1 && !Dgroup2)
            //{
            //    Program.Audit("Mobility is true and Hygiene is false: Force Hygiene Dgroup2 to be true.");
            //    Dgroup2 = true;
            //}
            //int DgroupSUM = (Dgroup1 ? 1 : 0) + (Dgroup2 ? 1 : 0) + (Dgroup3 ? 1 : 0) + (Dgroup4 ? 1 : 0);
            //if (DgroupSUM == 4) SetInd(4, "All four D Groups are true.");

            //SetIndIfResultBetween(4, "", "3045001023", "", "", 0, 7, SearchDepth.SearchSince13Hrs);
            //SetIndIfResultBetween(4, "", "9993040001207", "", "", 0, 7, SearchDepth.SearchSince13Hrs);
            //reslist = "Unconscious with motor response,Unconscious with no motor response,Pharmacologically paralyzed";
            //SetIndIfResultContains(4, "", "3045001046", "", "", reslist, SearchDepth.SearchSince13Hrs);

            ////reslist = "Ambulate in hall,Ambulate in room,Bathroom privileges, Commode, Supervised exercise,Up ad lib";
            ////d1 = Exists("", "9990000305560", "", "", reslist, SearchDepth.SearchSince13Hrs);
            //if (!d1x)
            //{
            //    reslist = "CPAP";
            //    SetIndIfResultDoesNotContain(4, "", "3045001117", "", "", reslist, SearchDepth.SearchSince13Hrs);
            //    reslist = "PACU Ventilator Rows,Trilogy Ventilator Rows,HFO Ventilator Rows,Abbreviated Settings,NAVA Rows";
            //    SetIndIfResultContains(4, "", "9993040000635", "", "", reslist, SearchDepth.SearchSince13Hrs);
            //}

            //reslist = "Somnolence,RUE paresis ,RLE paresis,LUE paresis,LLE paresis";
            //SetIndIfResultContains(4, "", "9990000450600", "", "", reslist, SearchDepth.SearchSince13Hrs);
            //reslist = "Unresponsive,Loss Of Consciousness";
            //SetIndIfResultContains(4, "", "9990160239301", "", "", reslist, SearchDepth.SearchSince13Hrs);
            //reslist = "Respiratory arrest,Cardiac arrest,Unknown,Other";
            //SetIndIfResultContains(4, "", "9991600100646", "", "", reslist, SearchDepth.SearchSince13Hrs);
            //reslist = "";
            //SetIndIfResultContains(4, "", "9993040001002", "", "", reslist, SearchDepth.SearchSince13Hrs);

            if (!_inds[1].is_checked && !_inds[2].is_checked && !_inds[3].is_checked)
            {
                SetInd(1, "Defaulting to ADL Self");
            }

        }


        private void Check_4()
        {
            string reslist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("4. ADL Supervision");
            Program.VerboseAudit("---------------");

            reslist = "Supervised";
            SetIndIfResultContains(4, "", "9990000700380", "", "", reslist,SearchDepth.SearchSince13Hrs);
            SetIndIfResultContains(4, "", "9993040009678", "", "", "", SearchDepth.SearchSince13Hrs);
            reslist = "Locked Bathroom,Post-prandial monitoring";
            SetIndIfResultContains(4, "", "9993040000395", "", "", "", SearchDepth.SearchSince13Hrs);

        }


        private void Check_5_6()
        {
            string reslist;
            bool s1, s2, s3, s4;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("5. Cognitive Support");
            Program.VerboseAudit("6. Cognitive Support q1 hour");
            Program.VerboseAudit("---------------");

            reslist = "Confused";
            SetIndIfResultContains(5, "", "3045001046", "", "", reslist);


            reslist = "Oriented to person,Oriented to place,Oriented to time";
            if (!AllOriented())
            {
                reslist = "Disoriented X4,Disoriented to place,Disoriented to time,Disoriented to situation,Disoriented to person";
                SetIndIfResultContains(5, "", "9990000301870", "", "", reslist);

                //any UP TO 2 ONLY of these
                s1 = Exists("", "9990000301870", "", "", "Disoriented x3", SearchDepth.SearchSince16Hrs);
                s2 = Exists("", "9990000301870", "", "", "Oriented to person", SearchDepth.SearchSince16Hrs);
                s3 = Exists("", "9990000301870", "", "", "Oriented to place", SearchDepth.SearchSince16Hrs);
                s4 = Exists("", "9990000301870", "", "", "Oriented to time", SearchDepth.SearchSince16Hrs);
                int CgroupSUM = (s1 ? 1 : 0) + (s2 ? 1 : 0) + (s3 ? 1 : 0) + (s4 ? 1 : 0);
                if (CgroupSUM >= 1 && CgroupSUM <= 2) SetInd(5, "One or Two of: Disoriented x3,Oriented to person,Oriented to place,Oriented to time");

            }

            reslist = "Short term memory loss,Unable to follow commands";
            SetIndIfResultContains(5, "", "9990000301880", "", "", reslist);
            reslist = "4,3,confused,inappropriate words";
            SetIndIfResultContains(5, "", "3045001024", "", "", reslist);
            SetIndIfResultBetween(5, "", "9990000398002", "", "", 4, 7, SearchDepth.SearchSince16Hrs);
            SetIndIfResultBetween(5, "", "9990000398004", "", "", 4, 7, SearchDepth.SearchSince16Hrs);
            SetIndIfResultBetween(5, "", "9990000398006", "", "", 4, 7, SearchDepth.SearchSince16Hrs);

            reslist = "Disoriented for place or person";
            SetIndIfResultContains(5, "", "9990000398010", "", "", reslist);
            reslist = "Hallucination,Illusions,Patient responding to internal Stimuli";
            SetIndIfResultContains(5, "", "9993040105625", "", "", reslist);
            reslist = "Auditory,Visual,Tactile,Command,Olfactory";
            SetIndIfResultContains(5, "", "9991540100181", "", "", reslist);
            reslist = "Confused,Disoriented,Incoherent";
            SetIndIfResultContains(5, "", "9993040105626", "", "", reslist);

            reslist = "Delusions,Paranoia";
            SetIndIfResultContains(5, "", "9993040105627", "", "", reslist);
            reslist = "Persecution,Grandeur,Obsessions,Religiosity,Phobias,Influence,Reference,Sexual delusions,Somatization,Thought broadcasting,Thought insertion,Thought withdrawal";
            SetIndIfResultContains(5, "", "9991540100180", "", "", reslist);
            reslist = "Disoriented X4";
            SetIndIfResultContains(5, "", "9993040105631", "", "", reslist);
            reslist = "Impaired short term memory";
            SetIndIfResultContains(5, "", "9991540100198", "", "", reslist);

            //if #12 or #13 gets triggered, then trigger #6.

        }
        private bool AllOriented()
        {
            int ct = 0;

            var query = StartNewQuery(SearchDepth.SearchSince16Hrs);    // add custom time range below
            query = query.Where(e => e.CODE.StartsWith("9990000301870"));
            query = query.Where(e => (e.RESULT.ToLower().StartsWith("oriented to person") || e.RESULT.ToLower().Contains(";oriented to person"))
            && (e.RESULT.ToLower().StartsWith("oriented to place") || e.RESULT.ToLower().Contains(";oriented to place"))
            && (e.RESULT.ToLower().StartsWith("oriented to time") || e.RESULT.ToLower().Contains(";oriented to time")));
            ct = query.Count();
            if (ct > 0)
                Program.VerboseAudit("All 3 Orientation found: " + query.Count());
            return (ct > 0);
        }

        private void Check_7_8_9()
        {
            string reslist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("7. Safety Management q30 min");
            Program.VerboseAudit("8. Safety Management q15 min");
            Program.VerboseAudit("9. Safety Management q5 min");
            Program.VerboseAudit("---------------");

            // 9993040009234 + Q30 min 
            CheckSafety();

            reslist = "24 hours";
            SetIndIfResultContains(9, "", "9990000300101", "", "", reslist);
            reslist = "4 hour,2 hour,1 hour";
            SetIndIfResultContains(9, "", "9990000300001", "", "", reslist);
            reslist = "1:1 Nsg care for seclusion for the first hour";
            reslist += ",Constant Nsg care for restraints for the first hour";
            reslist += ",Record behavior every 5 minutes";
            reslist += ",Review strengths";
            reslist += ",Administration of medication";
            reslist += ",Respiratory status assessed";
            reslist += ",Restraints checked every 15 minutes";
            reslist += ",RN hourly assessment";
            reslist += ",Offer fluids every 2 hours";
            reslist += ",Offer toileting every 2 hours";
            reslist += ",ROM to joints every 2 hours";
            reslist += ",Hygiene PRN";
            reslist += ",Meals and snacks offered";
            reslist += ",Other";
            SetIndIfResultContains(9, "", "9990007096219", "", "", reslist);
            reslist = "Implemented - Seclusion Treatment Policy";
            reslist += ",Implemented - Behavioral Restraint Policy";
            reslist += ",Procedure explained to patient";
            reslist += ",Reason for seclusion";
            reslist += ",Informed the patient of the goal";
            reslist += ",Mattress checked for dangerous items";
            reslist += ",check room for lighting";
            reslist += ",Dangerous items and jewelry removed";
            reslist += ",Respiratory status monitored";
            reslist += ",Pt assisted in achieving goal";
            SetIndIfResultContains(9, "", "9990007096227", "", "", reslist);
        }
        private int CheckSafety()
        {
            int ind = 0, ct = 0;
            string[] safety_list = { "q30","q15","q5", "Line of sight","Continuous observation by RN with patient","Continuous observation by non-RN staff with patient","Continuous observation by two staff with patient" };

            var query = StartNewQuery(SearchDepth.SearchSince16Hrs);
            query = query.Where(e => e.CODE.StartsWith("9993040009234"));
            query = query.Where(e => safety_list.Any(item => e.RESULT.ToLower().StartsWith(item.ToLower())));
            query = query.OrderByDescending(e => e.EVENT_DATETIME);
            ct = query.Count();
            if (ct > 0)
            {
                string res = query.First().RESULT;
                ind = 9;
                if (res.ToLower().StartsWith("q30")) ind = 7;
                if (res.ToLower().StartsWith("q15")) ind = 8;
                SetInd(ind, "Latest result is = " + res);
            }
            return ind;
        }


        private void Check_10_11_12_13_14()
        {
            string reslist, found_what;
            //int ct;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("10. Behavior/Emotional Management");
            Program.VerboseAudit("11. Behavior/Emotional Mgmt - q4 Hour");
            Program.VerboseAudit("12. Behavior/Emotional Mgmt - q2 Hour");
            Program.VerboseAudit("13. Behavior/Emotional Mgmt - q1 Hour");
            Program.VerboseAudit("14. Behavior/Emotional Mgmt - q30 min");
            Program.VerboseAudit("---------------");

            reslist = "1:1 RN Time,Assign a task,Aromatherapy,Assisted relaxation,Consistent response,Exposure therapy,Limit setting,Medicated (see MAR),Neutral response,Physical redirection,Quiet room,Reorientation,Time out,Verbal redirection,Following behavior safety plan (See notes),Multiple Staff for acute behavioral intervention";
            bool j2 = Exists("", "9993040000352", "", "", reslist);
            if (j2)
            {
                reslist = "Agitated,Behavior plan,Catatonic,Combative,Compulsive,Crying,Demanding,Destructive,Disorganized,Echopraxic,Exit seeking,Fussy,Guarded,Hostile,Hyperactive,Impulsive,Intrusive,Motor perseveration,Oppositional,Pacing,Preoccupied,Disruptive,Psychomotor retardation,Pushing limits,Reckless,Resistant to care,Rigid,Ritualistic,Tearful,Staff seeking,Seclusive,Sexually inappropriate,Tics,Withdrawn,dismissive";
                SetIndIfResultContains(10, "", "9993040105629", "", "", reslist);
                reslist = "Agitated,Combative,Compulsive,Crying,Destructive,Disorganized,Disruptive,Guarded,Hostile,Hyperactive,Intrusive,Oppositional,Pacing,Preoccupied,Pushing limits,Reckless,Resistant to care,Rigid,Ritualistic,Tearful,Staff seeking,Sexually inappropriate,Tics,Withdrawn";
                SetIndIfResultContains(10, "", "9993040009823", "", "", reslist);
                reslist = "Angry,Anxious,Apathetic,Depressed,Dysphoric,Elevated,Euphoric,Fearful,Guilty,Irritable,Manic,Sad";
                SetIndIfResultContains(10, "", "9993040105622", "", "", reslist);
            }

            reslist = "Yes";
            SetIndIfResultContains(10, "", "9993040105637", "", "", reslist);
            reslist = "Acts of violence,Threats of violence";
            SetIndIfResultContains(10, "", "9993040105640", "", "", reslist);

            if (j2)
            {
                reslist = "Disruptive";
                SetIndIfResultContains(10, "", "9993040000405", "", "", reslist);
                reslist = "Hallucination,Illusions,Patient responding to internal Stimuli";
                SetIndIfResultContains(10, "", "9993040105625", "", "", reslist);
                reslist = "Auditory,Visual,Tactile,Command,Olfactory";
                SetIndIfResultContains(10, "", "9991540100181", "", "", reslist);
                reslist = "Persecution,Grandeur,Obsessions,Religiosity,Phobias,Influence,Reference,Sexual delusions,Somatization,Thought broadcasting,Thought insertion,Thought withdrawal";
                SetIndIfResultContains(10, "", "9991540100180", "", "", reslist);
            }

            reslist = "Pillow,Memory box,Bereavement package,Other";
            SetIndIfResultContains(10, "", "9991020100509", "", "", reslist);
            reslist = "Baby bracelet,Blanket,Clay molds,Clothing,Complimentary birth certificate,Crib card,Foot prints,Hand prints,Hat,Lock of hair,Photos,Pin,Prism,Tape measure,Other";
            SetIndIfResultContains(10, "", "9991020100510", "", "", reslist);
            reslist = "Done";
            SetIndIfResultContains(10, "", "9991020100506", "", "", reslist);

            reslist = "Agitated,Behavior plan,Catatonic,Combative,Compulsive,Crying,Demanding,Destructive,Disorganized,Echopraxic,Exit seeking,Fussy,Guarded,Hostile,Hyperactive,Impulsive,Intrusive,Motor perseveration,Oppositional,Pacing,Preoccupied,Disruptive,Psychomotor retardation,Pushing limits,Reckless,Resistant to care,Rigid,Ritualistic,Tearful,Staff seeking,Seclusive,Sexually inappropriate,Tics,Withdrawn,dismissive";
            bool k1 = Exists("", "9993040105629", "", "", reslist);
            reslist = "1:1 RN Time,Assign a task,Aromatherapy,Assisted relaxation,Consistent response,Exposure therapy,Limit setting,Medicated (see MAR),Neutral response,Physical redirection,Quiet room,Reorientation,Time out,Verbal redirection";
            bool k2a = Exists("", "9993040000352", "", "", reslist);
            reslist = "Following behavior safety plan,Multiple Staff for acute behavioral intervention";
            bool k2b = Exists("", "9993040000352", "", "", reslist);
            reslist = "Agitated,Combative,Compulsive,Crying,Destructive,Disorganized,Disruptive,Guarded,Hostile,Hyperactive,Intrusive,Oppositional,Pacing,Preoccupied,Pushing limits,Reckless,Resistant to care,Rigid,Ritualistic,Tearful,Staff seeking,Sexually inappropriate,Tics,Withdrawn";
            bool k3 = Exists("", "9993040009823", "", "", reslist);

            //            In addition to the possibility of the above rows(K - 1 to K - 10) 
            //charted every 30 minutes as noted in the frequency, the rows K-1 to K-7 
            //should automatically trigger indicator #14
//            9993040105629
//9993040000352
//9993040009823
//9993040105622
//9993040105637
//9993040105640
//9993040000405


            reslist = "4 hours,2 hours,1 hour";
            SetIndIfResultContains(11, "", "9990000300001", "", "", reslist);
            reslist = "1:1 Nsg care for seclusion for the first hour,Constant Nsg care for restraints for the first hour,Record behavior every 5 minutes,Review strengths/comfort measures - assist pt in reaching goal for discontinuation,Administration of medication - to help client regain previous level of functioning,Respiratory status assessed/documented each check - to ensure adequate air exchange,Restraints checked every 15 minutes - to prevent injury from cuffs constricting normal blood flow or irritation of skin,RN hourly assessment - mental, behavior and respiratory status - to minimize length of the procedure,Offer fluids every 2 hours - to provide elimination opportunities,Offer toileting every 2 hours - to provide elimination opportunities,ROM to joints every 2 hours - to prevent injury from occurring when client is in restraints,Hygiene PRN - to provide comfort and support,Meals and snacks offered - to maintain nutrition,Other";
            SetIndIfResultContains(11, "", "9990007096219", "", "", reslist);
            reslist = "Implemented - Seclusion Treatment Policy,Implemented - Behavioral Restraint Policy,Procedure explained to patient,Reason for seclusion/restraints explained,Informed the patient of the goal,Mattress checked for dangerous items,Mattress checked for dangerous items,check room for lighting, temperature and safety,Dangerous items and jewelry removed,Respiratory status monitored,Pt assisted in achieving goal for d/c";
            SetIndIfResultContains(11, "", "9990007096227", "", "", reslist);




        }

        // (this isn't really a Q1h count -- it is just a count) Make this a q1hr count
        private int ReturnQ1HrCount(string code_list, string result_list)
        {
            var buckets = new List<gBucket>();
            SetBucketSize(60);
            AddBuckets(buckets, "", code_list, "", "", result_list);
            return CountBuckets(buckets);
        }

        private bool IsQ1Hour(int count)
        {
            return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q1H);
        }
        private bool IsQ2Hour(int count)
        {
            return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q2H);
        }

        // Use this if counting safety buckets
        //private void CheckSafety(int count, string desc)
        //{
        //    if (_inds[13].is_checked) return;             //skip if highest already checked
        //    if (count == 0) return;
        //    switch (FreqForCount(_pat.los_hours, count))
        //    {
        //    case Frequencies.Q30M:
        //        SetInd(13, desc + " q30min");
        //        break;
        //    case Frequencies.Q1H:
        //    case Frequencies.Q2H:
        //        SetInd(12, desc + " q2h");
        //        break;
        //    default:
        //        Program.VerboseAudit(desc + ": " + count + " in " + Math.Round(_pat.los_hours) + " hours is not enough");
        //        break;
        //    }
        //}

        private void Check_12_13()
        {
            bool suppress13 = false;
            string reslist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("12. Safety Management - q 2 Hours");
            Program.VerboseAudit("13. Safety Management - q 30 Minutes");
            Program.VerboseAudit("---------------");

            reslist = "Impulsive,Lack of safety awareness,Does not use call light or ask for assistance,Danger to self or others,Danger from others,Exit seeking";
            bool l1 = Exists("", "9993040009123", "", "", reslist);
            reslist = "Q2 hour,Q1 hour";
            bool l2 = Exists("", "9993040009234", "", "", reslist);
            if (l1 && l2) SetInd(12, "L1 and L2");

            reslist = "Start,Continue";
            SetIndIfResultContains(12, "", "9993040009567", "", "", reslist);
            reslist = "On";
            SetIndIfResultContains(12, "", "9990007096444", "", "", reslist);

            reslist = "NG tube,OG tube,G-tube,J-tube,ND tube,OD tube,PEG-tube";
            bool l5 = Exists("", "9991733565652", "", "", reslist);
            reslist = "Bolus per gravity,Bolus per pump";
            bool l6 = Exists("", "9990007074010", "", "", reslist);
            if (l5 && l6) SetInd(12, "L5 and L6");

            reslist = "Impulsive,Lack of safety awareness,Does not use call light or ask for assistance,Danger to self or others,Danger from others,Exit seeking";
            bool m1 = Exists("", "9993040009123", "", "", reslist);
            reslist = "Q30 min,Q15 min,Q5 min,Line of sight,Continuous observation by RN with patient,Continuous observation by non-RN staff with patient,Continuous observation by two staff with patient";
            bool m2 = Exists("", "9993040009234", "", "", reslist);
            if (m1 && m2) SetInd(13, "M1 and M2");

            reslist = "24 hours";
            SetIndIfResultContains(13, "", "9990000300101", "", "", reslist);
            reslist = "4 hours,2 hours,1 hour";
            SetIndIfResultContains(13, "", "9990000300001", "", "", reslist);
            reslist = "1:1 Nsg care for seclusion for the first hour,Constant Nsg care for restraints for the first hour,Record behavior every 5 minutes,Review strengths/comfort measures - assist pt in reaching goal for discontinuation,Administration of medication - to help client regain previous level of functioning,Respiratory status assessed/documented each check - to ensure adequate air exchange,Restraints checked every 15 minutes - to prevent injury from cuffs constricting normal blood flow or irritation of skin,RN hourly assessment - mental, behavior and respiratory status - to minimize length of the procedure,Offer fluids every 2 hours - to provide elimination opportunities,Offer toileting every 2 hours - to provide elimination opportunities,ROM to joints every 2 hours - to prevent injury from occurring when client is in restraints,Hygiene PRN - to provide comfort and support,Meals and snacks offered - to maintain nutrition (document reason for not providing and meal or snack at regular times),Other";
            SetIndIfResultContains(13, "", "9990007096219", "", "", reslist);
            reslist = "Implemented - Seclusion Treatment Policy,Implemented - Behavioral Restraint Policy,Procedure explained to patient,Reason for seclusion/restraints explained,Informed the patient of the goal,Mattress checked for dangerous items,Mattress checked for dangerous items,check room for lighting, temperature and safety,Dangerous items and jewelry removed,Respiratory status monitored,Pt assisted in achieving goal for d/c";
            SetIndIfResultContains(13, "", "9990007096227", "", "", reslist);

            reslist = "NG tube,OG tube,G-tube,J-tube,ND tube,OD tube,PEG-tube";
            bool m7 = Exists("", "9991733565652", "", "", reslist);
            reslist = "Continous pump,Continuous pump";
            bool m8 = Exists("", "9990007074010", "", "", reslist);
            if (m7 && m8) SetInd(13, "M7 and M8");


        }

        private void Check_14()
        {
            string reslist;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("14. Isolation");
            Program.VerboseAudit("---------------");

            reslist = "ISO1,ISO2,ISO4,ISO8,ISO9,ISO10";
            SetIndIfResultContains(14, "", reslist, "", "", "");

        }

        private void CheckAssessment(int count, string desc)
        {
            //if (_inds[18].is_checked) return;          //skip if highest already checked
            if (count == 0) return;                    //skip if none

            // This should work the same as the original code:
            switch (FreqForCount(_pat.los_hours, count))
            {
                case Frequencies.Q30M:
                    SetInd(18, desc);
                    break;
                case Frequencies.Q1H:
                    SetInd(17, desc);
                    break;
                case Frequencies.Q2H:
                    SetInd(16, desc);
                    break;
                case Frequencies.Q4H:
                    SetInd(15, desc);
                    break;
                default:
                    break;
            }

        }

        private void ShowBuckets(List<gBucket> buckets)
        {
            string s = "";
            foreach (var e in buckets)
            {
                s = "(" + e.bucket + "," + e.code + "," + e.evdt.ToString() + ")";
                Program.VerboseAudit("Assessmt item:=" + s);
            }
        }


        private void Check_15_16_17_18()
        {
            string reslist;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("15. Assessment q4h");
            Program.VerboseAudit("16. Assessment q2h");
            Program.VerboseAudit("17. Assessment q1h");
            Program.VerboseAudit("18. Assessment q30min");
            Program.VerboseAudit("---------------");

            //CountAssessments(30);               // always do q30 buckets--but use different freq tables for icu vs non-icu.

            reslist = "";
            SetIndIfResultContains(15, "", "304987655", "", "", reslist);
            SetIndIfResultContains(15, "", "9990007096285", "", "", reslist);
            SetIndIfResultContains(15, "", "304987657", "", "", reslist);
            SetIndIfResultContains(15, "", "3045001025", "", "", reslist);
            SetIndIfResultContains(15, "", "3045001064", "", "", reslist);
            SetIndIfResultContains(15, "", "304987666", "", "", reslist);
            SetIndIfResultContains(15, "", "9990304100017", "", "", reslist);
            SetIndIfResultContains(15, "", "30454321", "", "", reslist);
            SetIndIfResultContains(15, "", "9990000006294", "", "", reslist);
            SetIndIfResultContains(15, "", "9993041120042", "", "", reslist);
            SetIndIfResultContains(15, "", "3045001041", "", "", reslist);
            SetIndIfResultContains(15, "", "9990304001499", "", "", reslist);
            SetIndIfResultContains(15, "", "3045001018", "", "", reslist);
            SetIndIfResultContains(15, "", "9993040103255", "", "", reslist);

            reslist = "Non-rebreather mask,Nasal cannula,High flow,Aerosol mask,Bag valve device,BiPAP,Blow-by,CPAP,Face tent,Isolette,Oxyhood,Oxymask,Partial rebreather mask,Reservoir nasal cannula,Simple mask,T-piece,Trach mask,Ventilator,Venturi mask";
            SetIndIfResultContains(15, "", "3045001051", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(15, "", "3045001052", "", "", reslist);
            SetIndIfResultContains(15, "", "3045001053", "", "", reslist);
            reslist = "Agonal,Apnea,Bradypnea,Cheyne-Stokes,Kussmaul,Obstructed,Periodic,Regular,Tachypnea";
            SetIndIfResultContains(15, "", "9990000302570", "", "", reslist);
            reslist = "Regular,Irregular,Shallow,Deep";
            SetIndIfResultContains(15, "", "9993040109339", "", "", reslist);
            reslist = "Labored,Unlabored";
            SetIndIfResultContains(15, "", "9993040109337", "", "", reslist);
            reslist = "Abdominal muscle use,Accessory muscle use,Drooling,Gasping,Grunting,Head bobbing,Nasal flaring,Pursed lips,Retractions,Tripod position";
            SetIndIfResultContains(15, "", "9993040109338", "", "", reslist);
            reslist = "Symmetrical,Asymmetrical,Trachea deviates right,Trachea midline,Trachea deviates left,Paradoxical,Sunken chest,Pigeon chest,Subcutaneous emphysema (Describe),No chest expansion,Cylinder shaped,Flattened,Right clavicular deformity,Left clavicular deformity";
            SetIndIfResultContains(15, "", "9990000302580", "", "", reslist);
            reslist = "Clear,Diminished,Fine,Coarse,Rales,Rhonchi,Crackles,Expiratory wheezes,Inspiratory wheezes,Stridor,Pleural rub,Tubular,Absent";
            SetIndIfResultContains(15, "", "9990000302390", "", "", reslist);
            reslist = "Clear,Diminished,Fine,Coarse,Rales,Rhonchi,Crackles,Expiratory wheezes,Inspiratory wheezes,Stridor,Pleural rub,Tubular,Absent";
            SetIndIfResultContains(15, "", "9990000302400", "", "", reslist);
            reslist = "Clear,Diminished,Fine,Coarse,Rales,Rhonchi,Crackles,Expiratory wheezes,Inspiratory wheezes,Stridor,Pleural rub,Tubular,Absent";
            SetIndIfResultContains(15, "", "9990000302410", "", "", reslist);
            reslist = "Copious,Large,Moderate,Small,None";
            SetIndIfResultContains(15, "", "9990000451120", "", "", reslist);
            reslist = "Copious,Large,Moderate,Small,None";
            SetIndIfResultContains(15, "", "9990000451080", "", "", reslist);
            reslist = "Copious,Large,Moderate,Small,None";
            SetIndIfResultContains(15, "", "9990000451040", "", "", reslist);
            reslist = "Bed therapy,Cough assist,CPAP,EzPAP,Flutter valve,IPV device,Manual percussion,NT suction,PEP Therapy,Percusser,Postural drainage,Vibralung,Vest";
            SetIndIfResultContains(15, "", "9990000315800", "", "", reslist);
            reslist = "Tollerated well,Tollerated fairly well,Tolerated poorly,Other";
            SetIndIfResultContains(15, "", "9990000325950", "", "", reslist);
            reslist = "CPAP facial,CPAP gap present,CPAP nasal,Incubator oxygen,Laryngeal mask airway,Nasal cannula gap present,Nasal cannula high flow,Nasal cannula low flow";
            SetIndIfResultContains(15, "", "9991733888883", "", "", reslist);
            reslist = "Apnea,Bradycardia,Desaturation,Other";
            SetIndIfResultContains(15, "", "9991733666660", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(15, "", "9990000344210", "", "", reslist);
            SetIndIfResultContains(15, "", "9990000316380", "", "", reslist);
            SetIndIfResultContains(15, "", "9990000344220", "", "", reslist);
            SetIndIfResultContains(15, "", "9990000344230", "", "", reslist);
            SetIndIfResultContains(15, "", "9990007096450", "", "", reslist);

            reslist = "Blow-by oxygen,Oxygen,Positive pressure ventilation,Self limiting,Suction,Tactile stimulation";
            SetIndIfResultContains(15, "", "9990000344250", "", "", reslist);
            reslist = "Aminophylline,Caffeine,High flow oxygen,Intubated,Medication dose change,Nasal cannula,Nasal CPAP";
            SetIndIfResultContains(15, "", "9990000344280", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(15, "", "9990000006808", "", "", reslist);
            SetIndIfResultContains(15, "", "3041733124512", "", "", reslist);
            SetIndIfResultContains(15, "", "9991733666661", "", "", reslist);
            reslist = "Bilevel,CPAP,Auto-Bilevel,Auto-CPAP,AVAPS,PCV";
            SetIndIfResultContains(15, "", "3045001108", "", "", reslist);
            reslist = "Nasal pillows,Nasal mask,Nasal Prongs,Nasal pharyngeal,Full face mask,Performax,Total face mask,Endotracheal,Tracheostomy";
            SetIndIfResultContains(15, "", "9993040000637", "", "", reslist);
            reslist = "Mask,Tubing,CPAP,BPAP,Patient able to manage equipment on own,Equipment inspected (per site policy),Waiver signed (per site policy),Other";
            //SetIndIfResultContains(, "", "9993040000639", "", "", reslist);
            reslist = "Point of care,To lab";
            SetIndIfResultContains(15, "", "9991600100035", "", "", reslist);
            reslist = "Point of care,To lab";
            SetIndIfResultContains(15, "", "9991600100039", "", "", reslist);
            reslist = "String";
            SetIndIfResultContains(15, "", "3045001117", "", "", reslist);
            reslist = "PACU Ventilator Rows,Trilogy Ventilator Rows,HFO Ventilator Rows,BPAP/CPAP Rows,Abbreviated Settings,NAVA Rows";
            SetIndIfResultContains(15, "", "9993040000635", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(15, "", "3045001128", "", "", reslist);

            reslist = "Oral mouthpiece,Mask,Trach,Ventilator,NPPV,Blow-by,Other";
            SetIndIfResultContains(15, "", "9991600100048", "", "", reslist);

            reslist = "Clear,Diminished,Absent,Crackles,Stridor,Wheeze,Other";
            SetIndIfResultContains(15, "", "9990160238401", "", "", reslist);
            reslist = "Clear,Diminished,Absent,Crackles,Stridor,Wheeze,Other";
            SetIndIfResultContains(15, "", "9990160238501", "", "", reslist);


            reslist = "Scant,Small,Moderate,Copious,Thin,Thick,Clear,Blood tinged,Tan,White,Yellow";
            SetIndIfResultContains(15, "", "9993040104500", "", "", reslist);


            //reslist = "Sinus rhythm,Sinus bradycardia,Sinus tachycardia,Sinus arrest,Sinus arrhythmia,Atrial paced,Ventricular paced,A-V Sequential paced,Agonal,Asystole,Atrial fibrillation,Atrial fibrillation w/rapid ventricular response,Atrial flutter,Heart block,Junctional accelerated,Junctional rhythm,Junctional tachycardia,Pulseless electrical activity,Supraventricular tachycardia,Torsades de Pointes,Ventricular fibrillation,Ventricular tachycardia";
            //SetIndIfResultContains(15, "", "3045001065", "", "", reslist);

            reslist = EXACT_MATCH_PREFIX + "SR";
            SetIndIfResultContains(15, "", "3045001065", "", "", reslist);
            reslist = EXACT_MATCH_PREFIX + "SB";
            SetIndIfResultContains(15, "", "3045001065", "", "", reslist);
            reslist = EXACT_MATCH_PREFIX + "ST";
            SetIndIfResultContains(15, "", "3045001065", "", "", reslist);
            reslist = "Sinus rhythm,Sinus bradycardia,Sinus tachycardia,Sinus arrest,Sinus arrhythmia,Agonal,Asystole,Atrial flutter,Heart block,Junctional accelerated,Junctional rhythm,Junctional tachycardia";
            SetIndIfResultContains(15, "", "3045001065", "", "", reslist);
            reslist = "A-paced,V-paced,A-V paced,Atrial paced,Ventricular paced,A-V Sequential paced,A-fib,A-fib w/RVR,Atrial fibrillation,PEA,Pulseless electrical activity,SVT,Supraventricular tachycardia,Torsades,VF,VT,Ventricular fibrillation,Ventricular tachycardia,Other";
            SetIndIfResultContains(15, "", "3045001065", "", "", reslist);
            reslist = "1st degree AVB,2nd degree AVB (Mobitz I, Wenckebach),2nd degree AVB (Mobitz II),3rd degree AVB,Bundle branch block,Idioventricular";
            SetIndIfResultContains(15, "", "3045001066", "", "", reslist);
            reslist = "PVCs,Premature ventricular contractions,Unifocal PVCs,Multifocal PVCs,Couplet PVCs,Triplet PVCs,PACs,Premature atrial contractions,Aberrent conduction,Ectopic beats,Fusion beats,Junctional escape beats,Non-conducted PACs,Paroxysmal atrial tachycardia,PSVT,PJCS,Paroxysmal supraventricular tachycardia,Premature junctional contractions,Ventricular escape beats,Other";
            SetIndIfResultContains(15, "", "3045001067", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(15, "", "9993040108650", "", "", reslist);
            SetIndIfResultContains(15, "", "9993040108651", "", "", reslist);
            SetIndIfResultContains(15, "", "9993040108652", "", "", reslist);
            SetIndIfResultContains(15, "", "9993040108653", "", "", reslist);
            reslist = "S1, S2,S3,S4,Click,Device,Distant,Friction rub,Gallop,Holosystolic murmur,Mechanical valve click,Muffled,Murmur,Pericardial rub,No adventitious heart sounds";
            SetIndIfResultContains(15, "", "9993040100446", "", "", reslist);
            reslist = "Off,A paced,V paced,A/V paced,AAI,AAI-DDD (MVP),AOO,DVI,DOO,DDD,VVI,VOO,VDD,AAR,AAR-DDDR (MVP),AOOR,DVR,DOOR,DOI,DDDR,DDR,VVR,VOOR,VDDR";
            SetIndIfResultContains(15, "", "9993040101320", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(15, "", "9993040101316", "", "", reslist);
            SetIndIfResultContains(15, "", "9993040101317", "", "", reslist);
            reslist = "Red,Pink,Pale,Dusky,Purple,Mottled,Ecchymotic,Soft,Firm";
            SetIndIfResultContains(15, "", "9993040001044", "", "", reslist);
            reslist = "Greater than 3 seconds,Less than or equal to 3 seconds,Brisk,Sluggish";
            SetIndIfResultContains(15, "", "9993040001045", "", "", reslist);
            reslist = "Verified,Absent";
            SetIndIfResultContains(15, "", "9993040001048", "", "", reslist);
            reslist = "Verified,Absent";
            SetIndIfResultContains(15, "", "9993040001049", "", "", reslist);
            reslist = "0,+1,+2,+3,+4,Doppler";
            SetIndIfResultContains(15, "", "9990000303200", "", "", reslist);
            SetIndIfResultContains(15, "", "9990000303210", "", "", reslist);
            SetIndIfResultContains(15, "", "9990000303220", "", "", reslist);
            SetIndIfResultContains(15, "", "3045001045", "", "", reslist);
            SetIndIfResultContains(15, "", "9990000303270", "", "", reslist);
            SetIndIfResultContains(15, "", "9990000303280", "", "", reslist);
            SetIndIfResultContains(15, "", "9990000303320", "", "", reslist);
            SetIndIfResultContains(15, "", "9990000303330", "", "", reslist);
            SetIndIfResultContains(15, "", "3045001012", "", "", reslist);
            SetIndIfResultContains(15, "", "3045001014", "", "", reslist);
            SetIndIfResultContains(15, "", "9990000303390", "", "", reslist);
            SetIndIfResultContains(15, "", "9990000303400", "", "", reslist);
            SetIndIfResultContains(15, "", "3045001013", "", "", reslist);
            SetIndIfResultContains(15, "", "3045001015", "", "", reslist);

            reslist = "Less than/equal to 2 seconds,Greater than/equal to 3 seconds,No return";
            SetIndIfResultContains(15, "", "3045001004", "", "", reslist);
            SetIndIfResultContains(15, "", "3045001002", "", "", reslist);
            SetIndIfResultContains(15, "", "3045001003", "", "", reslist);
            SetIndIfResultContains(15, "", "3045001001", "", "", reslist);

            reslist = "SVT,Supraventricular tachycardia,VT,Ventricular tachycardia,A-fib w/RVR,Atrial fibrillation,Other";
            SetIndIfResultContains(15, "", "9991600100613", "", "", reslist);
            reslist = "Sinus,SR,SB,Sinus rhythm,Sinus bradycardia,A-paced,V-paced,A-V paced,Atrial paced,Ventricular paced,A-V Sequential paced,A-fib,A-fib w/RVR,Atrial fibrillation,Atrial flutter,Heart block,Junctional,SVT,Supraventricular tachycardia,Torsades,Asystole,PEA,VF,VT,Pulseless electrical activity,Ventricular fibrillation,Ventricular tachycardia,Other";
            SetIndIfResultContains(15, "", "9991600100614", "", "", reslist);
            reslist = EXACT_MATCH_PREFIX + "ST";
            SetIndIfResultContains(15, "", "9991600100614", "", "", reslist);
            reslist = "Initiated,Settings Change,Discontinued";
            SetIndIfResultContains(15, "", "9991600100147", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(15, "", "9990000302900", "", "", reslist);
            SetIndIfResultContains(15, "", "9993045006597", "", "", reslist);
            SetIndIfResultContains(15, "", "9990000302920", "", "", reslist);
            SetIndIfResultContains(15, "", "9993045006598", "", "", reslist);
            SetIndIfResultContains(15, "", "9993040101373", "", "", reslist);
            SetIndIfResultContains(15, "", "9993040101374", "", "", reslist);
            SetIndIfResultContains(15, "", "9993040101375", "", "", reslist);
            SetIndIfResultContains(15, "", "9993040101376", "", "", reslist);
            SetIndIfResultContains(15, "", "3040000000022", "", "", reslist);
            SetIndIfResultContains(15, "", "99930400000027", "", "", reslist);
            SetIndIfResultContains(15, "", "3040000000028", "", "", reslist);
            SetIndIfResultContains(15, "", "9993040000062", "", "", reslist);
            SetIndIfResultContains(15, "", "99930400000056", "", "", reslist);
            SetIndIfResultContains(15, "", "3040000000050", "", "", reslist);
            SetIndIfResultContains(15, "", "9993040000044", "", "", reslist);
            SetIndIfResultContains(15, "", "9993040000035", "", "", reslist);
            SetIndIfResultContains(15, "", "9993040000045", "", "", reslist);
            SetIndIfResultContains(15, "", "9993040000049", "", "", reslist);
            SetIndIfResultContains(15, "", "9993047096403", "", "", reslist);
            SetIndIfResultContains(15, "", "9993040000050", "", "", reslist);
            SetIndIfResultContains(15, "", "9993040000064", "", "", reslist);
            SetIndIfResultContains(15, "", "9993040000065", "", "", reslist);
            SetIndIfResultContains(15, "", "3045001056", "", "", reslist);
            SetIndIfResultContains(15, "", "3045001055", "", "", reslist);


            reslist = "Acrocyanosis,Capillary refill no return,Capillary refill sluggish (>2 seconds),Circumoral,Cyanosis, centralized,Cyanosis, peripheral,Generalized,Localized,Mottled,Pale,Ruddy";
            SetIndIfResultContains(15, "", "9991733888889", "", "", reslist);
            reslist = "Absent,Murmur,Murmer-intermittent,Other ";
            SetIndIfResultContains(15, "", "9990000302650", "", "", reslist);
            reslist = "0,+3,+1,Unequal,Other";
            SetIndIfResultContains(15, "", "9991733888890", "", "", reslist);
            reslist = "Moderate,Non-pitting,Severe,Other";
            SetIndIfResultContains(15, "", "9991733999991", "", "", reslist);

            reslist = "Circumocular,Circumoral,Nailbed,Acrocyanosis,Facial,Generalized,Oral mucosa,Underlying,Other";
            SetIndIfResultContains(15, "", "9993040100223", "", "", reslist);
            reslist = "Normal (less than/equal to 2 seconds, all extremities),Sluggish (greater than/equal to 3 seconds, all extremities),No return,Other";
            SetIndIfResultContains(15, "", "3045001000", "", "", reslist);
            reslist = "Acrocyanosis,Appropriate for ethnicity,Ashen,Black,Bronze,Dusky,Ecchymosis,Flushed,Gray,Jaundiced";
            SetIndIfResultContains(15, "", "9990000303180", "", "", reslist);
            SetIndIfResultContains(15, "", "9990000303240", "", "", reslist);
            SetIndIfResultContains(15, "", "9990000303300", "", "", reslist);
            SetIndIfResultContains(15, "", "9990000303370", "", "", reslist);

            reslist = "Capillary refill less than 3 sec,Cool fingers/toes,Capillary refill greater than 3 sec,Dusky fingers/toes,Numbness to fingers/toes,Pale fingers/toes,Tingling to fingers/toes,Weakness to fingers/toes";
            SetIndIfResultContains(15, "", "9990007061360", "", "", reslist);
            reslist = "Present,Absent";
            SetIndIfResultContains(15, "", "9993040103299", "", "", reslist);
            reslist = "Present,Absent";
            SetIndIfResultContains(15, "", "9993040103300", "", "", reslist);


            reslist = "0/4,1/4,2/4,3/4,4/4";
            SetIndIfResultContains(15, "", "9993040101409", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(15, "", "3045001007", "", "", reslist);
            SetIndIfResultContains(15, "", "3045001039", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(15, "", "9993040006316", "", "", reslist);
            SetIndIfResultContains(15, "", "9990304006317", "", "", reslist);
            SetIndIfResultContains(15, "", "9993040006318", "", "", reslist);
            SetIndIfResultContains(15, "", "9993040006319", "", "", reslist);
            SetIndIfResultContains(15, "", "9993040006320", "", "", reslist);
            SetIndIfResultContains(15, "", "9990000301930", "", "", reslist);
            reslist = "Brisk,Sluggish,Nonreactive";
            SetIndIfResultContains(15, "", "9990000301920", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(15, "", "9990000301910", "", "", reslist);
            reslist = "Brisk,Sluggish,Nonreactive";
            SetIndIfResultContains(15, "", "9990000301900", "", "", reslist);
            reslist = "Absent,Weak,Moderate,Strong,Contracture";
            SetIndIfResultContains(15, "", "9993040101162", "", "", reslist);
            reslist = "Absent,Weak,Moderate,Strong,Contracture";
            SetIndIfResultContains(15, "", "9993040101163", "", "", reslist);
            reslist = "Present,Absent";
            SetIndIfResultContains(15, "", "9990000002280", "", "", reslist);
            reslist = "Present,Absent";
            SetIndIfResultContains(15, "", "9990000002279", "", "", reslist);
            reslist = "Present,Absent";
            SetIndIfResultContains(15, "", "3045001017", "", "", reslist);

            reslist = "Absent,Weak,Moderate,Strong,Contracture";
            SetIndIfResultContains(15, "", "3045001016", "", "", reslist);
            SetIndIfResultContains(15, "", "9993040101166", "", "", reslist);
            SetIndIfResultContains(15, "", "9993040101167", "", "", reslist);

            string codelist = "9990000301980,9990000301940,9990000302000,9990000301960";
            reslist = "Responds to commands,Normal extension,Normal flexion,Tremors,Flaccid,Abnormal extension (Decerebrate),Abnormal flexion (Decorticate),Movement to painful stimulus,No movement to painful stimulus,Non-purposeful movement,No tremor,Spastic";
            SetIndIfResultContains(15, "", codelist, "", "", reslist);
            reslist = STARTS_WITH + "0,-1,-2,-3,-4,P,SP,NP,Stim,Pain,DC,DB";
            SetIndIfResultContains(15, "", codelist, "", "", reslist);

            reslist = "Decreased,No sensation,Numbness,Pain,Tingling,Full sensation,No numbness,No pain,No tingling";
            SetIndIfResultContains(15, "", "9990000301990", "", "", reslist);
            reslist = "Normal power,Cannot overcome resistance,Overcomes gravity,Can overcome resistance,Flicker of muscle,None";
            SetIndIfResultContains(15, "", "9993040102775", "", "", reslist);
            reslist = "Decreased,No sensation,Numbness,Pain,Tingling,Full sensation,No numbness,No pain,No tingling";
            SetIndIfResultContains(15, "", "9990000301950", "", "", reslist);
            reslist = "Normal power,Cannot overcome resistance,Overcomes gravity,Can overcome resistance,Flicker of muscle,None";
            SetIndIfResultContains(15, "", "9993040102776", "", "", reslist);
            reslist = "Decreased,No sensation,Numbness,Pain,Tingling,Full sensation,No numbness,No pain,No tingling";
            SetIndIfResultContains(15, "", "9990000302010", "", "", reslist);
            reslist = "Normal power,Cannot overcome resistance,Overcomes gravity,Can overcome resistance,Flicker of muscle,None";
            SetIndIfResultContains(15, "", "9993040102777", "", "", reslist);
            reslist = "Decreased,No sensation,Numbness,Pain,Tingling,Full sensation,No numbness,No pain,No tingling";
            SetIndIfResultContains(15, "", "9990000301970", "", "", reslist);
            reslist = "Normal power,Cannot overcome resistance,Overcomes gravity,Can overcome resistance,Flicker of muscle,None";
            SetIndIfResultContains(15, "", "9993040102778", "", "", reslist);
            reslist = "C1,C2,C3,C4,C5,C6,C7,C8,T1,T2,T3,T4,T5,T6,T7,T8,T9,T10,T11,T12,L1,L2,L3,L4,L5,S1,S2,S3,S4,S5";
            SetIndIfResultContains(15, "", "9990000302190", "", "", reslist);
            reslist = "C1,C2,C3,C4,C5,C6,C7,C8,T1,T2,T3,T4,T5,T6,T7,T8,T9,T10,T11,T12,L1,L2,L3,L4,L5,S1,S2,S3,S4,S5";
            SetIndIfResultContains(15, "", "9990000302180", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(15, "", "3045001023", "", "", reslist);
            SetIndIfResultContains(15, "", "9993040001207", "", "", reslist);
            SetIndIfResultContains(15, "", "3045001080", "", "", reslist);
            SetIndIfResultContains(15, "", "3045001081", "", "", reslist);
            //reslist = "Any number between -5 and + 4";
            SetIndIfResultBetween(15, "", "3045001079", "", "", -5, 4);
            reslist = "S, 1, 2, 3, 4 ";
            SetIndIfResultContains(15, "", "9993040104675", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(15, "", "9990000398011", "", "", reslist);
            reslist = "Positive,Negative";
            SetIndIfResultContains(15, "", "9993040001216", "", "", reslist);
            reslist = "Positive,Negative";
            SetIndIfResultContains(15, "", "3045001011", "", "", reslist);
            reslist = "Present,Weak,Absent";
            SetIndIfResultContains(15, "", "9990000450430", "", "", reslist);
            reslist = "Present,Weak,Absent";
            SetIndIfResultContains(15, "", "9990000450420", "", "", reslist);
            reslist = "Intact,Impaired,Absent";
            SetIndIfResultContains(15, "", "9990000450990", "", "", reslist);
            reslist = "Intact,Impaired,Absent";
            SetIndIfResultContains(15, "", "9990000450410", "", "", reslist);
            reslist = "Absent,Present";
            SetIndIfResultContains(15, "", "9993040103168", "", "", reslist);
            reslist = "Absent,Present";
            SetIndIfResultContains(15, "", "9993040103169", "", "", reslist);
            reslist = "Absent,Present,Weak";
            SetIndIfResultContains(15, "", "9993040109102", "", "", reslist);
            reslist = "Absent,Present,Weak";
            SetIndIfResultContains(15, "", "9990000450470", "", "", reslist);

            reslist = "Absent,Present,Weak,Strong,Coordinated,Uncoordinated,Gag present,Gag absent";
            SetIndIfResultContains(15, "", "9990000450480", "", "", reslist);
            //reslist = "Number > 1";
            SetIndIfResultBetween(15, "", "3045001047", "", "", 2, 999);
            reslist = "Blindness - right,Blindness - left,Blurred vision,Visual field cut- right side,Visual field cut- right upper,Visual field cut- right lower,Visual field cut- left side,Visual field cut- left upper,Visual field cut- left lower,Diplopia- Bilateral,Diplopia - right";
            SetIndIfResultContains(15, "", "9993040001091", "", "", reslist);
            reslist = "Strabismus - right,Strabismus - left,Droopy eyelid - right,Droopy eyelid - left,Double vision - right,Double vision - left,Unable to assess";
            SetIndIfResultContains(15, "", "9990000002216", "", "", reslist);
            reslist = "Double vision - right,Double vision - left,Unable to look downward and inward - right,Unable to look downward and inward - left,Unable to assess";
            SetIndIfResultContains(15, "", "9990000002217", "", "", reslist);
            reslist = "Unable to chew - right,Unable to chew - left,Unable to clench - right,Unable to clench - left,Absence of sensation - right,Absence of sensation - left";
            SetIndIfResultContains(15, "", "9990000002218", "", "", reslist);
            reslist = "Medial strabismus -  right,Medial strabismus -  left,Unable to look laterally - right,Unable to look laterally - left,Double vision - right,Double vision - left";
            SetIndIfResultContains(15, "", "9990000002219", "", "", reslist);
            reslist = "Facial paralysis - right,Facial paralysis - left,Loss of taste - right,Loss of taste - left";
            SetIndIfResultContains(15, "", "9990000002220", "", "", reslist);
            reslist = "Unable to hear - right,Unable to hear - left,Ringing in ear - right,Ringing in ear - left,Involuntary eye movement - right,Involuntary eye movement - left";
            SetIndIfResultContains(15, "", "9990000002221", "", "", reslist);
            reslist = "Altered gag reflex";
            SetIndIfResultContains(15, "", "9990000002222", "", "", reslist);
            reslist = "Weakness in turning head - right,Weakness in turning head - left,Unable to shrug - right,Unable to shrug - left";
            SetIndIfResultContains(15, "", "9990000002223", "", "", reslist);
            reslist = "Deviation of tongue - right,Deviation of tongue - left";
            SetIndIfResultContains(15, "", "9990000002224", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(15, "", "9993040001057", "", "", reslist);
            reslist = "Clear,Cloudy,Serous,Sanguineous,Serosanguineous,Purulent,Cherry,Straw,Yellow";
            SetIndIfResultContains(15, "", "9990000302300", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(15, "", "9990000002230", "", "", reslist);
            SetIndIfResultContains(15, "", "9990000304510", "", "", reslist);
            SetIndIfResultContains(15, "", "9991070011101", "", "", reslist);
            reslist = "No untoward effects noted,Use of reversal agent(s),Hypoxemia < 90% for > 1 min,Hypotension of bradycardia requiring intervention,Respiratory failure requiring intervention,Cardiac arrest or death,Sedation recovery time > 60 min,Unplanned admission or higher level of care,Respiratory distress,Unanticipated need for anesthesia involvement,Inability to complete procedure,No responsible adult for discharge escort";
            SetIndIfResultContains(15, "", "9991600100259", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(15, "", "99910701000111", "", "", reslist);


            reslist = "Aggression,Aura,Behavior pause,Bowel incontinence,Deja vu,Fearful,Giggles,Nausea,Oral Trauma,Smirks,Tremors,Urine incontinence,Vocalize  ";
            SetIndIfResultContains(15, "", "9990000450560", "", "", reslist);
            reslist = "Eyes right,Eyes left,Head right,Head left ,Other";
            SetIndIfResultContains(15, "", "9990000450570", "", "", reslist);
            reslist = "Head,Face,Eye,Hand,Arm,Leg,Foot,Jerking,Stiffening,Staring,Tonic-clonic Motion,Twitching,Drop";
            SetIndIfResultContains(15, "", "9990000450580", "", "", reslist);
            reslist = "Head,Face,Eye,Hand,Arm,Leg,Foot,Jerking,Stiffening,Staring,Tonic-clonic Motion,Twitching,Drop";
            SetIndIfResultContains(15, "", "9993040108685", "", "", reslist);
            reslist = "Head,Face,Eye,Hand,Arm,Leg,Foot,Jerking,Stiffening,Staring,Tonic-clonic Motion,Twitching,Drop";
            SetIndIfResultContains(15, "", "9993040108686", "", "", reslist);
            reslist = "any number";
            SetIndIfResultContains(15, "", "9990000450590", "", "", reslist);
            reslist = "Somnolence,Aphasic,RUE paresis,LUE paresis,RLE paresis,LLE paresis";
            SetIndIfResultContains(15, "", "9990000450600", "", "", reslist);
            reslist = "Aware of seizure,Word given ,Word recalled,Word not recalled,Normal speech,Abnormal speech,Unable to respond ";
            SetIndIfResultContains(15, "", "9990000450620", "", "", reslist);
            reslist = "Generalized,Right,Left,Hand,Leg,Face";
            SetIndIfResultContains(15, "", "9993040101196", "", "", reslist);
            //reslist = "Number > 1";
            SetIndIfResultBetween(15, "", "9993040101198", "", "", 2, 999);
            reslist = "Absent,Arching back or neck,Bicycling,Clonic jerking,Extension is greater than flexion,Jerky,Jittery/tremors,Lip smacking,Movements cease with containment,Movements continue despite containment,Rowing,Seizure activity,Tonic extension,Tonic flexion";
            SetIndIfResultContains(15, "", "9993040100079", "", "", reslist);
            reslist = "Bicycling,Eye deviation,Lip smacking,Movement ceases with containment,Movements continue despite containment,Tongue thrusting";
            SetIndIfResultContains(15, "", "9991733999990", "", "", reslist);
            reslist = "Abnormal reflex,Extensionx,Frantic movement,Inconsolable,Lethargic,Medically paralyzed,Sedated";
            SetIndIfResultContains(15, "", "9993040102780", "", "", reslist);
            reslist = "Quiet alert,Sleeping,Active alert,Lusty cry,Drowsy,Active with stimulation,Hoarse cry,Irritable,Jittery,Lethargic,Shrill cry";
            SetIndIfResultContains(15, "", "9991140100006", "", "", reslist);
            reslist = "Hypertonic generalized,Hypertonic localized,Hypotonic generalized,Hypotonic localized";
            SetIndIfResultContains(15, "", "9993040100076", "", "", reslist);
            reslist = "Absent,Present,Weak,Brisk,Clonus,Clonus Sustained";
            SetIndIfResultContains(15, "", "9990000451750", "", "", reslist);
            reslist = "Absent,Asymmetric,Symmetric,UTA";
            SetIndIfResultContains(15, "", "9990000450440", "", "", reslist);
            reslist = "Absent,Present,Weak,UTA";
            SetIndIfResultContains(15, "", "9990000450450", "", "", reslist);
            reslist = "Absent,Present,Clonus,Hyperreflexive,Hyporeflexive,Weak,UTA";
            SetIndIfResultContains(15, "", "9990000450460", "", "", reslist);
            reslist = "Absent,Present,Weak,Unable to assess";
            SetIndIfResultContains(15, "", "9993040109103", "", "", reslist);
            reslist = "Absent,Asymmetrical,Symmetrical,Unable to assess";
            SetIndIfResultContains(15, "", "9993040109104", "", "", reslist);
            reslist = "Boggy,Bulging,Caput saecundum,Cephalohematoma,Closed,Depressed,Sunken,Flat,Full,Soft,Sutures approximated";
            SetIndIfResultContains(15, "", "9991140100013", "", "", reslist);
            reslist = "Boggy,Bulging,Caput saecundum,Cephalohematoma,Closed,Depressed,Sunken,Flat,Full,Soft,Sutures approximated";
            SetIndIfResultContains(15, "", "9991140100014", "", "", reslist);



            reslist = "";
            SetIndIfResultContains(17, "", "3045001036", "", "", reslist);
            SetIndIfResultContains(17, "", "3045001034", "", "", reslist);
            reslist = "1:1,1:2,1:3";
            SetIndIfResultContains(17, "", "3045001038", "", "", reslist);
            reslist = "Continuous veno-venous hemofiltration,Continuous veno-venous hemodialysis,Continuous veno-venous hemodiafiltration,Slow continuous ultrafiltration";
            SetIndIfResultContains(17, "", "9990008100010", "", "", reslist);
            reslist = "Initiated,Continuous,Restarted,Off/System charge,Off/Recirculating,Off/Blood returned,Off/No blood returned";
            SetIndIfResultContains(17, "", "9990008100020", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(17, "", "9993041001004", "", "", reslist);
            SetIndIfResultContains(17, "", "9993041001003", "", "", reslist);


            reslist = "4 Ounces fruit juice,8 ounces fruit juice,IV/Medication (See MAR)";
            SetIndIfResultContains(15, "", "9993040000345", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(15, "", "9990000344170", "", "", reslist);
            SetIndIfResultContains(15, "", "3040007191", "", "", reslist);
            SetIndIfResultContains(15, "", "304000344160", "", "", reslist);
            SetIndIfResultContains(15, "", "30400012412", "", "", reslist);
            SetIndIfResultContains(15, "", "304000344150", "", "", reslist);





            reslist = "";
            SetIndIfResultContains(15, "", "9990007096390", "", "", reslist);
            SetIndIfResultContains(15, "", "9990000305290", "", "", reslist);
            SetIndIfResultContains(15, "", "3045001090", "", "", reslist);
            SetIndIfResultContains(15, "", "3045001089", "", "", reslist);
            reslist = "Amber,Clear,Cloudy,Dark,Fibrin,Light,Pink,Red,Yellow";
            SetIndIfResultContains(15, "", "9990000370180", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(15, "", "3040011360", "", "", reslist);


            reslist = "Heat applied,Cold applied,Compression,Aromatherapy (see Cares/Safety),Cold pack,Cutaneous stimulation,Guided imagery,Herbal therapy,Warm moist pack,Warm pack";
            SetIndIfResultContains(15, "", "9990000301120", "", "", reslist);
            reslist = "0 - No pain,1,2,3,4,5 - Moderate pain,6,7,8,9,10 - Worst possible pain";
            SetIndIfResultContains(15, "", "304987659", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(15, "", "9990007090220", "", "", reslist);
            SetIndIfResultContains(15, "", "9993040104228", "", "", reslist);
            SetIndIfResultContains(15, "", "3045001058", "", "", reslist);
            SetIndIfResultContains(15, "", "9993040101151", "", "", reslist);
            SetIndIfResultContains(15, "", "9993040101157", "", "", reslist);
            SetIndIfResultContains(15, "", "9993040101160", "", "", reslist);
            SetIndIfResultContains(15, "", "9993040104125", "", "", reslist);
            SetIndIfResultContains(15, "", "9990007090780", "", "", reslist);


            reslist = "Denies,Blurred,Floaters,Flashes,Decreased visual field,Hx. of visual disturbances,Unsure,Other";
            SetIndIfResultContains(15, "", "9991025006827", "", "", reslist);
            reslist = "absent,diminished,normal,brisk,brisk/hyperactive,sustained";
            SetIndIfResultContains(15, "", "9990000012301", "", "", reslist);
            reslist = "absent,diminished,normal,brisk,brisk/hyperactive,sustained";
            SetIndIfResultContains(15, "", "9990000012302", "", "", reslist);
            reslist = "absent,diminished,normal,brisk,brisk/hyperactive,sustained";
            SetIndIfResultContains(15, "", "9990000012303", "", "", reslist);
            reslist = "absent,diminished,normal,brisk,brisk/hyperactive,sustained";
            SetIndIfResultContains(15, "", "9990000012305", "", "", reslist);
            reslist = "Absent,1 beat,2 beats,3 beats,4 beats,Sustained";
            SetIndIfResultContains(15, "", "9990000012306", "", "", reslist);
            reslist = "Absent,1 beat,2 beats,3 beats,4 beats,Sustained";
            SetIndIfResultContains(15, "", "9990000012304", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(15, "", "1028000004", "", "", reslist);
            SetIndIfResultContains(15, "", "1028000001", "", "", reslist);
            SetIndIfResultContains(15, "", "9990102521012", "", "", reslist);
            SetIndIfResultContains(15, "", "1028000002", "", "", reslist);
            SetIndIfResultContains(15, "", "1028000003", "", "", reslist);
            SetIndIfResultContains(15, "", "9991020100645", "", "", reslist);
            SetIndIfResultContains(15, "", "9991020100655", "", "", reslist);

            reslist = "Normal,Bradycardia,Tachycardia,Indeterminate";
            SetIndIfResultContains(15, "", "1028000007", "", "", reslist);
            reslist = "Normal,Bradycardia,Tachycardia,Indeterminate";
            SetIndIfResultContains(15, "", "1028000008", "", "", reslist);
            reslist = "Normal,Bradycardia,Tachycardia,Indeterminate";
            SetIndIfResultContains(15, "", "1028000009", "", "", reslist);
            reslist = "Normal,Bradycardia,Tachycardia,Indeterminate";
            SetIndIfResultContains(15, "", "9991020100648", "", "", reslist);
            reslist = "Normal,Bradycardia,Tachycardia,Indeterminate";
            SetIndIfResultContains(15, "", "9991020100657", "", "", reslist);
            reslist = "Minimal,Moderate,Marked,Absent";
            SetIndIfResultContains(15, "", "1028000010", "", "", reslist);
            reslist = "Minimal,Moderate,Marked,Absent";
            SetIndIfResultContains(15, "", "1028000011", "", "", reslist);
            reslist = "Minimal,Moderate,Marked,Absent";
            SetIndIfResultContains(15, "", "1028000012", "", "", reslist);
            reslist = "15x15,10x10,Absent";
            SetIndIfResultContains(15, "", "1028000013", "", "", reslist);
            reslist = "15x15,10x10,Absent";
            SetIndIfResultContains(15, "", "1028000014", "", "", reslist);
            reslist = "15x15,10x10,Absent";
            SetIndIfResultContains(15, "", "1028000015", "", "", reslist);
            reslist = "None,Early,Variable,Late,Prolonged ,Intermittent,Recurrent";
            SetIndIfResultContains(15, "", "1028000016", "", "", reslist);
            reslist = "None,Early,Variable,Late,Prolonged,Intermittent,Recurrent";
            SetIndIfResultContains(15, "", "1028000017", "", "", reslist);
            reslist = "None,Early,Variable,Late,Prolonged,Intermittent,Recurrent";
            SetIndIfResultContains(15, "", "1028000018", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(15, "", "9991025051116", "", "", reslist);
            SetIndIfResultContains(15, "", "9991025111616", "", "", reslist);

            reslist = "Category I,Category II,Category III";
            SetIndIfResultContains(15, "", "1028000022", "", "", reslist);
            reslist = "Category I,Category II,Category III";
            SetIndIfResultContains(15, "", "1028000023", "", "", reslist);
            reslist = "Category I,Category II,Category III";
            SetIndIfResultContains(15, "", "1028000024", "", "", reslist);
            reslist = "Category I,Category II,Category III";
            SetIndIfResultContains(15, "", "9991020100652", "", "", reslist);
            reslist = "Category I,Category II,Category III";
            SetIndIfResultContains(15, "", "9991020100661", "", "", reslist);
            reslist = "Reactive,Non-reactive";
            SetIndIfResultContains(15, "", "9990000012331", "", "", reslist);
            reslist = "Reassuring,Non-reassuring";
            SetIndIfResultContains(15, "", "9990000012332", "", "", reslist);
            reslist = "Intact,Spontaneous,AROM,PROM,PPROM,Bulging";
            SetIndIfResultContains(15, "", "1028000040", "", "", reslist);
            reslist = "Closed,Fingertip,1,2,3,4,5,6,7,8,9,Lip/rim,10";
            SetIndIfResultContains(15, "", "1028000046", "", "", reslist);
            reslist = "Vaginal pack,Uterine balloon,Uterotonic agent given (see MAR),Oxygen on,Legs elevated,MD notified,Bimanual exam per MD,Labs ordered";
            SetIndIfResultContains(15, "", "9991020100568", "", "", reslist);
            reslist = "Firm,Firm with massage,Boggy";
            SetIndIfResultContains(15, "", "9990000012127", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(17, "", "331197", "", "", reslist);

            SetIndIfResultContains(18, "", "3040105197", "", "", reslist);
            SetIndIfResultContains(18, "", "3040101240", "", "", reslist);
            SetIndIfResultContains(18, "", "3040101294", "", "", reslist);
            SetIndIfResultContains(18, "", "3040101306", "", "", reslist);
            SetIndIfResultContains(18, "", "331262", "", "", reslist);

            bool mr20 = Exists("", "331222", "", "", reslist);
            bool mr21 = Exists("", "331232", "", "", reslist);
            bool mr22 = Exists("", "331227", "", "", reslist);
            bool mr23 = Exists("", "331237", "", "", reslist);
            bool mr24 = Exists("", "331036", "", "", reslist);
            bool mr25 = Exists("", "3040101288", "", "", reslist);
            bool mr26 = Exists("", "331202", "", "", reslist);

            bool mr50 = Exists("", "3047588", "", "", reslist);
            bool mr51 = Exists("", "3040101222", "", "", reslist);
            bool mr52 = Exists("", "3040105283", "", "", reslist);
            bool mr53 = Exists("", "3040101252", "", "", reslist);
            bool mr54 = Exists("", "331212", "", "", reslist);
            bool mr55 = Exists("", "3040101258", "", "", reslist);
            bool mr56 = Exists("", "331242", "", "", reslist);
            bool mr57 = Exists("", "331247", "", "", reslist);
            bool mr58 = Exists("", "3040101264", "", "", reslist);
            bool mr59 = Exists("", "3040101270", "", "", reslist);
            bool mr60 = Exists("", "331292", "", "", reslist);
            bool mr61 = Exists("", "331277", "", "", reslist);
            bool mr62 = Exists("", "3040101276", "", "", reslist);
            bool mr63 = Exists("", "3040101282", "", "", reslist);
            bool mr64 = Exists("", "7080015", "", "", reslist);
            bool mr65 = Exists("", "331257", "", "", reslist);
            bool mr66 = Exists("", "3040102621", "", "", reslist);
            bool mr67 = Exists("", "3040010410", "", "", reslist);
            bool mr68 = Exists("", "331272", "", "", reslist);
            bool mr69 = Exists("", "331282", "", "", reslist);
            bool mr70 = Exists("", "331217", "", "", reslist);
            bool mr71 = Exists("", "3040104320", "", "", reslist);
            bool mr72 = Exists("", "3040105310", "", "", reslist);
            bool mr73 = Exists("", "3040105315", "", "", reslist);
            bool mr74 = Exists("", "3040105325", "", "", reslist);
            bool mr75 = Exists("", "3040105330", "", "", reslist);
            bool mr76 = Exists("", "7080039", "", "", reslist);
            bool mr77 = Exists("", "7556", "", "", reslist);

            reslist = "Continuous veno-venous hemofiltration,Continuous veno-venous hemodialysis,Continuous veno-venous hemodiafiltration,Slow continuous ultrafiltration";
            SetIndIfResultContains(18, "", "9990008100010", "", "", reslist);
            reslist = "Initiated,Continuous,Restarted";
            SetIndIfResultContains(18, "", "9990008100020", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(18, "", "9993041001004", "", "", reslist);
            SetIndIfResultContains(18, "", "9993041001003", "", "", reslist);
            SetIndIfResultContains(18, "", "9993040001002", "", "", reslist);
            SetIndIfResultContains(18, "", "9993040001003", "", "", reslist);
            SetIndIfResultContains(18, "", "9993040101000", "", "", reslist);
            SetIndIfResultContains(18, "", "9993040001093", "", "", reslist);
            SetIndIfResultContains(18, "", "3045001056", "", "", reslist);
            SetIndIfResultContains(18, "", "3045001055", "", "", reslist);
            reslist = "Bilevel,CPAP,Auto-Bilevel,Auto-CPAP,AVAPS,PCV";
            SetIndIfResultContains(18, "", "3045001108", "", "", reslist);
            reslist = "Nasal pillows,Nasal mask,Nasal Prongs;,Nasal pharyngeal,Full face mask,Performax,Total face mask,Endotracheal,Tracheostomy";
            SetIndIfResultContains(18, "", "9993040000637", "", "", reslist);
            reslist = "Mask,Tubing,CPAP,BPAP,Patient able to manage equipment on own,Equipment inspected (per site policy),Waiver signed (per site policy),Other";

            bool p16 = Exists("", "9993040000639", "", "", reslist);

            reslist = "String";
            SetIndIfResultContains(18, "", "3045001117", "", "", reslist);
            reslist = "PACU Ventilator Rows,Trilogy Ventilator Rows,HFO Ventilator Rows,BPAP/CPAP Rows,Abbreviated Settings,NAVA Rows";
            SetIndIfResultContains(18, "", "9993040000635", "", "", reslist);
            //reslist = "Number > 60";
            SetIndIfResultBetween(18, "", "3045001053", "", "", 61, 999);


            //reslist = "Number > 10";
            SetIndIfResultBetween(18, "", "3045001120", "", "", 11, 999);
            reslist = "Yes";
            SetIndIfResultContains(18, "", "9993040011344", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(18, "", "9993040102551", "", "", reslist);
            SetIndIfResultContains(18, "", "9993040102552", "", "", reslist);
            //reslist = "Number > 35"; 
            SetIndIfResultBetween(18, "", "3045001128", "", "", 36, 999);
            //reslist = "Number > 25";
            SetIndIfResultBetween(18, "", "3045001119", "", "", 26, 999);
            reslist = "Continuous lateral rotation";
            SetIndIfResultContains(18, "", "9990000400604", "", "", reslist);
            reslist = "Reposition,Suction,Jaw thrust,Chin lift,Foreign object removal";
            SetIndIfResultContains(18, "", "9991010010010", "", "", reslist);
            reslist = "Endotracheal tube,Esophageal - tracheal tube,Laryngeal mask airway (LMA),Nasopharyngeal airway (NPA),Oropharyngeal airway (OPA),Tracheostomy,Other";
            SetIndIfResultContains(18, "", "9991600100681", "", "", reslist);
            reslist = "Bag-valve-mask,Bag-valve-ET tube,Bag-valve-tracheostomy,Ventilator,Bilevel positive airway pressure (BiPAP),Continuous positive airway pressure (CPAP),CPAP nasal,CPAP mask,Positive pressure ventilation (PPV),Other";
            SetIndIfResultContains(18, "", "9991600100682", "", "", reslist);
            reslist = "Respiratory arrest,Cardiac arrest,Unknown,Other";
            SetIndIfResultContains(18, "", "9991600100646", "", "", reslist);
            reslist = "SVT,Supraventricular tachycardia,VT,Ventricular tachycardia,A-fib w/RVR,Atrial fibrillation,Other";
            SetIndIfResultContains(18, "", "9991600100613", "", "", reslist);
            reslist = "Sinus,SR,SB,Sinus rhythm,Sinus bradycardia,A-paced,V-paced,A-V paced,Atrial paced,Ventricular paced,A-V Sequential paced,A-fib,A-fib w/RVR,Atrial fibrillation,Atrial flutter,Heart block,Junctional,SVT,Supraventricular tachycardia,Torsades,Asystole,PEA,VF,VT,Pulseless electrical activity,Ventricular fibrillation,Ventricular tachycardia,Other";
            SetIndIfResultContains(18, "", "9991600100614", "", "", reslist);
            reslist = EXACT_MATCH_PREFIX + "ST";
            SetIndIfResultContains(18, "", "9991600100614", "", "", reslist);
            reslist = "CPAP facial,CPAP gap present,CPAP nasal";
            SetIndIfResultContains(18, "", "9991733888883", "", "", reslist);
            reslist = "CPAP vent,ETT nasal,ETT oral";
            SetIndIfResultContains(18, "", "9991733888883", "", "", reslist);
            reslist = "4";
            SetIndIfResultContains(18, "", "9993040000407", "", "", reslist);
            reslist = "Apnea,Bradycardia,Desaturation,Other";
            SetIndIfResultContains(18, "", "9991733666660", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(18, "", "9990007096450", "", "", reslist);

        }

        private bool IsRehab()
        {
            switch (_pat.unit_name)
            {
                case "137 - Rehab":
                    return true;
                default:
                    return false;
            }
        }


        //private bool IsICU()
        //{
        //    switch (_pat.unit_name)
        //    {
        //        case "ICU":
        //        case "3BR":
        //        case "CTIC":
        //        case "NIC":
        //        case "PCU":
        //        case "TICU":
        //        case "CCB":
        //        case "RICU":
        //        case "CCUS":
        //            return true;
        //        default:
        //            return false;
        //    }
        //}

        //private bool IsTele()
        //{
        //    switch (_pat.unit_name)
        //    {
        //        case "4EST":
        //        case "2NW":
        //        case "3AE":
        //        case "5MEH":
        //        case "5NW":
        //        case "6MEH":
        //        case "6NW":
        //        case "7MEH":
        //        case "B4W":
        //        case "B5S":
        //        case "B5W":
        //        case "2NOB":
        //        case "CPLX":
        //        case "TELMS":
        //        case "MAN4":
        //            return true;
        //        default:
        //            return false;
        //    }
        //}


        private void CountAssessments(int bucket_size)
        {
            int ct;
            string codelist;
            string reslist;
            List<gBucket> buckets;

            SetBucketSize(bucket_size);

            //buckets = new List<gBucket>();
            //AddBuckets(buckets, "", "7812497", "", "", "Endotracheal,Nasal,Nasal Tracheal Suction,Oral Suction,Tracheal Suction");
            //codelist = "117646,117590,117589,";
            //codelist += "117531,17343783,305994900,117638,141615,142506593,12499648,";
            //codelist += "117509,117498,117496,117584,305994913,305994918,3059991317,";
            //codelist += "117544,292718281,305995220,130920968,350850553,";
            //codelist += "305056309,305056294,305056394,633685506,305056509,305056484,";
            //codelist += "481751481,481752177,304208931,316381365,305056531,316346132,";
            //codelist += "305056541,117533,117643,304208961,7812336,18802339,305994957,";
            //codelist += "7812341,7812342,7812343,7812365,18802341,7812366,12499648,";
            //codelist += "305995290,7812367,305995300,7812371,7812372,7812373,7812374,7812375,";
            //codelist += "7812376,7812377,7812464,7812465,7812466,7812467,7812468,7812469,";
            //codelist += "7812470,7812493,18802342,7812494,7812495,7812496,";
            //codelist += "7812506,7812522,18802343,7812531,18802344,7812540,18802345,";
            //codelist += "758359245,325564484,302915761,302915771,592033203,592033208,";
            //codelist += "592033213,345372930,412500531,302916056,302916076,758629716,";
            //codelist += "758608948,758642066,758669207,303692843,383992195,303692914,";
            //codelist += "383992207,303693055,383992201,772531865,772522579,303693186,";
            //codelist += "303693191,303693196,303693201,303693206,303693211,303693216,";
            //codelist += "303693221,7812529,305056504,406392859,406392865,303693351,";
            //codelist += "303693371,303693346";
            //AddBuckets(buckets, "", codelist, "", "");
            //AddBuckets(buckets, "", "MED", "", "Heparin,Bivalrudin,Argatroban,Sodium Bicarb");
            //ct = CountBuckets(buckets);
            //CheckAssessment(ct, "All assessments count=" + ct);
            //ShowBuckets(buckets);

            buckets = new List<gBucket>();
            //AddDependentBuckets(buckets, "117531,17343783", "117590,117589");
            codelist = EXACT_MATCH_PREFIX + "5";
            AddBuckets(buckets, "", codelist, "", "");
            codelist = EXACT_MATCH_PREFIX + "6";
            AddBuckets(buckets, "", codelist, "", "");
            codelist = EXACT_MATCH_PREFIX + "8";
            AddBuckets(buckets, "", codelist, "", "");
            codelist = EXACT_MATCH_PREFIX + "9";
            AddBuckets(buckets, "", codelist, "", "");
            codelist = EXACT_MATCH_PREFIX + "14106";
            AddBuckets(buckets, "", codelist, "", "");
            codelist = EXACT_MATCH_PREFIX + "14107";
            AddBuckets(buckets, "", codelist, "", "");
            codelist = EXACT_MATCH_PREFIX + "301260";
            AddBuckets(buckets, "", codelist, "", "");
            codelist = EXACT_MATCH_PREFIX + "301280";
            AddBuckets(buckets, "", codelist, "", "");
            codelist += "450010,30402999013,3040104659,3040108953,30403000161,7090010,7060180,3040104228";
            AddBuckets(buckets, "", codelist, "", "");
            codelist = EXACT_MATCH_PREFIX + "7429";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Vitals=" + ct);
            ShowBuckets(buckets);

            //SetIndIfResultContains(15, "", "301860", "", "", "New agitation,Alert,Responds to Voice,Responds to Pain,Unresponsive,Awake,Quiet,Drowsy,Easily aroused,Eyes do not open to stimulation,Pharmacologically paralyzed,Sedated,Sleeping,Other");
            //SetIndIfResultContains(15, "", "302380", "", "", "WDL=Within Defined Limits,X=Exceptions to WDL");
            //SetIndIfResultContains(15, "", "303010", "", "", "WDL=Within Defined Limits,X=Exceptions to WDL");
            //SetIndIfResultContains(15, "", "304360", "", "", "WDL=Within Defined Limits,X=Exceptions to WDL");
            //SetIndIfResultContains(15, "", "304370", "", "", "WDL=Within Defined Limits,X=Exceptions to WDL");
            //SetIndIfResultContains(15, "", "304420", "", "", "WDL=Within Defined Limits,X=Exceptions to WDL");
            //SetIndIfResultContains(15, "", "304430", "", "", "WDL=Within Defined Limits,X=Exceptions to WDL");
            //SetIndIfResultContains(15, "", "400630", "", "", "WDL=Within Defined Limits,X=Exceptions to WDL");
            //SetIndIfResultContains(15, "", "2105", "", "", "WDL=Within Defined Limits,X=Exceptions to WDL");


            buckets = new List<gBucket>();
            codelist = EXACT_MATCH_PREFIX + "301320";
            AddBuckets(buckets, "", codelist, "", "");
            codelist = EXACT_MATCH_PREFIX + "2240";
            AddBuckets(buckets, "", codelist, "", "");
            codelist = EXACT_MATCH_PREFIX + "8";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Cardiac=" + ct);
            ShowBuckets(buckets);


            buckets = new List<gBucket>();
            codelist = "3040104228,3040104659,3040100967,301130,301180,7060860,7060910";
            codelist += ",7061000,1170,7061120,401610,401620,401630,401640,3040103243,3040103244,3040103245";
            codelist += ",3040101151,3040101157,3040101160";
            AddBuckets(buckets, "", codelist, "", "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Pain=" + ct);
            ShowBuckets(buckets);
            //if (_inds[18].is_checked) return;

            //buckets = new List<gBucket>();
            ////codelist = "12499648,117509,7812365,18802341,7812366,12499648,305995290,7812367,";
            ////codelist += "305995300,7812371,7812372,7812373,7812374,7812375,7812376,7812377,";
            ////codelist += "7812464,7812465,7812466,7812467,7812468,7812469,7812470,303031350,";
            ////codelist += "325754541,303031329,532319071,345591779,303693186,303693191,303693196,";
            ////codelist += "303693201,303693206,303693211,303693216,303693221";
            ////AddBuckets(buckets, "", codelist, "", "");
            //AddDependentBuckets(buckets, "305994900", "117638");
            //codelist = "7812493,18802342,7812494,7812495,7812496,142506593";
            //AddBuckets(buckets, "", codelist, "", "");
            //reslist = "Endotracheal,Nasal,Oral Suction,Tracheal Suction";
            //AddBuckets(buckets, "", "7812497", "", "", reslist);
            //ct = CountBuckets(buckets);
            //CheckAssessment(ct, "Respiratory=" + ct);
            //ShowBuckets(buckets);
            ////if (_inds[18].is_checked) return;

            buckets = new List<gBucket>();
            AddBuckets(buckets, "", "3040104125", "", "");
            codelist = EXACT_MATCH_PREFIX + "2239";
            AddBuckets(buckets, "", codelist, "", "");
            ct = CountBuckets(buckets);
            CheckAssessment(ct, "Neurological=" + ct);
            ShowBuckets(buckets);
            if (_inds[18].is_checked) return;

            //buckets = new List<gBucket>();
            ////codelist = "7812540,18802345,304015183,345603431";
            //codelist = "7812540,18802345,303692843,383992195,303692914,383992207";
            //codelist += ",303693055,383992201,772531865,772522579";
            //AddBuckets(buckets, "", codelist, "", "");
            //ct = CountBuckets(buckets);
            //CheckAssessment(ct, "Wound=" + ct);
            //ShowBuckets(buckets);
            ////if (_inds[18].is_checked) return;

            //buckets = new List<gBucket>();
            //codelist = "130920968,350850553";
            //AddBuckets(buckets, "", codelist, "", "");
            ////reslist = "Heparin,Bivalrudin,Argatroban,Sodium Bicarb";
            ////AddBuckets(buckets, "", "MED", reslist, "");
            //ct = CountBuckets(buckets);
            //CheckAssessment(ct, "Medications=" + ct);
            //ShowBuckets(buckets);
            ////if (_inds[18].is_checked) return;

            //buckets = new List<gBucket>();
            //codelist = "305056394,633685506,305056509,481751481,481752177,304208931,316381365,";
            //codelist += "305056531,316346132,117533,117643,304208961,7812529,305056504,406392859,406392865";
            //AddBuckets(buckets, "", codelist, "", "");
            //reslist = "APD,Biliary,Blake,CAPD,Drain,Constavac,Davol,G-J Tube,G-Tube,";
            //reslist += "Hemovac,JP,J-Tube,PEG tube,Pelvic Drain,Pendrose Drain,";
            //reslist += "Pericardial,T-Tube,Other";
            //AddBuckets(buckets, "", "305056269", "", "", reslist);
            //AddBuckets(buckets, "", "305056309,305056294,305056484,305056541", "", "", "");
            //ct = CountBuckets(buckets);
            //CheckAssessment(ct, "Fluid Mgt=" + ct);
            //ShowBuckets(buckets);
            ////if (_inds[18].is_checked) return;



            //buckets = new List<gBucket>();
            //codelist = "7812506,7812522,18802343,7812531,18802344";
            //AddBuckets(buckets, "", codelist, "", "");
            //ct = CountBuckets(buckets);
            //CheckAssessment(ct, "GI/GU=" + ct);
            //ShowBuckets(buckets);
            ////if (_inds[18].is_checked) return;
        }

        //=====================================================================
        // Group like assessments into buckets of time
        // Each bucket counts as one assessment in the frequency count

        private void AddDependentBuckets(List<gBucket> bucket_list, string codelist1, string codelist2)
        {
            // get the chart items for the assessments
            var query1 = StartNewQuery();
            query1 = AndItemFilter(query1, "", codelist1, "", "", "");
            var query2 = StartNewQuery();
            query2 = AndItemFilter(query2, "", codelist2, "", "", "");

            // figure out what buckets the events belong to
            var query1a = from item in query1
                          select new
                          {
                              bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                              code = item.CODE,
                              evdt = item.EVENT_DATETIME
                          };
            var query2a = from item in query2
                          select new
                          {
                              bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                              code = item.CODE,
                              evdt = item.EVENT_DATETIME
                          };
            string s = "BucketList1 for " + codelist1 + ": ";
            foreach (var item in query1a)
            {
                s += item.bucket + ",";
            }
            Program.VerboseAudit(s);

            s = "BucketList2 for " + codelist2 + ": ";
            foreach (var item in query2a)
            {
                s += item.bucket + ",";
            }
            Program.VerboseAudit(s);
            // Add to the list IFF items in both lists occur in same bucket
            foreach (var item1 in query1a)
            {
                foreach (var item2 in query2a)
                {
                    if (item1.bucket == item2.bucket)
                    {
                        var b = new gBucket();
                        b.bucket = item1.bucket;
                        b.code = item1.code;
                        b.evdt = item1.evdt;
                        bucket_list.Add(b);
                    }
                }
            }

        }


        private void SetBucketSize(int minutes)
        {
            _bucket_size = minutes;
        }

        //Add to the list of bucket numbers (redundant buckets are fine)
        private void AddBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field)
        {
            AddBuckets(bucket_list, cat, code_list, desc, field, "");
        }
        private void AddBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field, string result_list)
        {
            // Assign (virtual) bucket numbers to each assessment.  
            // Every x minutes after that is a new bucket.  Bucket zero = pull_start time.

            // get the chart items for the assessments
            var query = StartNewQuery();
            query = AndItemFilter(query, cat, code_list, desc, field, result_list);

            // This step is needed for those who want to count the # within a bucket, like Shands #24.
            // This will have no effect on those that count buckets.
            //var query2 = (from item in query select new { item.EVENT_DATETIME }).Distinct();
            var query2 = (from item in query select new { item.EVENT_DATETIME, item.CODE });

            // figure out what buckets the events belong to
            //var query3 = from item in query2
            //             select new {
            //                bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size)
            //             };
            //// Add to the list
            //foreach (var item in query3) {
            //    bucket_list.Add(item.bucket);
            //}

            var query3 = from item in query2
                         select new
                         {
                             bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                             code = item.CODE,
                             evdt = item.EVENT_DATETIME
                         };
            // Add to the list
            foreach (var item in query3)
            {
                var b = new gBucket();
                b.bucket = item.bucket;
                b.code = item.code;
                b.evdt = item.evdt;
                bucket_list.Add(b);
            }

            // This is just for auditing: Say what we are looking for and how many were found
            if (!String.IsNullOrEmpty(result_list))
            {
                // print each word and if it was found or not
                int i = CountResultContains(cat, code_list, desc, field, result_list);
            }
            else
            {
                // print how many were found
                Program.VerboseAudit(Describe(cat, code_list, desc, field, ""));
            }
        }

        private int CountBuckets(List<gBucket> bucket_list)       // bucket list: (ha ha)
        {
            //int result = bucket_list.Distinct().Count();
            //if (result > 0) Program.VerboseAudit(result + " unique");
            //return result;
            int x = -99;
            int result = 0;
            //int result = bucket_list.Distinct().Count();
            var query = from b in bucket_list
                        orderby b.bucket ascending
                        select b;
            foreach (var b in query)
            {
                if (x != b.bucket)
                {
                    result++;
                    x = b.bucket;
                }
            }
            if (result > 0) Program.VerboseAudit(result + " unique");
            return result;
        }

        private void Check_19()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("19. Vascular Access Site Mgt q1 Hour");
            Program.VerboseAudit("---------------");

            int ct = 0;
            ct = CountResultContains("", "7096770", "", "", "Cap changed,Connections checked and tightened,Line pulled back,Tubing changed,Zeroed and calibrated,Leveled,Transducer changed,Other");
            if (IsQ1Hour(ct))
                SetInd(19, "Line care intaosseous= " + ct);

        }

        private void Check_20()
        {
            string reslist;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("20. Medication Activity >= 20 minutes");
            Program.VerboseAudit("---------------");

            reslist = "Coughs with swallowing,Unable to swallow";
            SetIndIfResultContains(20, "", "9990000002113", "", "", reslist);
            reslist = "3-Unable to swallow";
            SetIndIfResultContains(20, "", "9990304000118", "", "", reslist);
            reslist = "Delayed swallow or interrupted swallow,Coughs, clears throat, chokes, gags up to 1 min after drinking,Unable to say,Wet voice when saying,Drools after swallowing,Desaturates 2% or > from baseline seconds after swallow,Swallows more than one time after drinking,Unable to assess";
            SetIndIfResultContains(20, "", "9993040108712", "", "", reslist);
            //reslist = "No signs or symptoms";
            //SetIndIfResultContains(20, "", "9993040108724", "", "", reslist);
            reslist = "Cryoprecipitate,Fresh Frozen Plasma,Platelets,Red Blood Cells,Other";
            SetIndIfResultContains(20, "", "9993040004071", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(20, "", "3041150001701", "", "", reslist);
            SetIndIfResultContains(20, "", "9993040401122", "", "", reslist);

        }

        private void Check_21_22()
        {
            string reslist;
            int ct;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("21. Wound/Injury Mgmt");
            Program.VerboseAudit("22. Wound/Injury Mgmt >= 30 Minutes");
            Program.VerboseAudit("---------------");

            reslist = "Stage II,Stage III,Stage IV,Unstageable,Deep tissue injury";
            SetIndIfResultContains(21, "", "9993041000044", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(21, "", "9990000303750", "", "", reslist);
            SetIndIfResultContains(21, "", "9990007061190", "", "", reslist);
            SetIndIfResultContains(21, "", "9993046629812", "", "", reslist);
            SetIndIfResultContains(21, "", "9990007061270", "", "", reslist);

            SetIndIfResultContains(21, "", "9990000001493", "", "", reslist);
            SetIndIfResultContains(21, "", "9990000001494", "", "", reslist);
            SetIndIfResultContains(21, "", "9990007061240", "", "", reslist);
            SetIndIfResultContains(21, "", "9990000303780", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040102846", "", "", reslist);

            SetIndIfResultContains(21, "", "9990000303800", "", "", reslist);
            SetIndIfResultContains(21, "", "9990000303820", "", "", reslist);
            SetIndIfResultContains(21, "", "9990000304850", "", "", reslist);

            SetIndIfResultContains(21, "", "9993040104036", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040104037", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040023765", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040100564", "", "", reslist);
            SetIndIfResultContains(21, "", "9991420100006", "", "", reslist);
            SetIndIfResultContains(21, "", "9991420100007", "", "", reslist);
            SetIndIfResultContains(21, "", "9991420100009", "", "", reslist);
            SetIndIfResultContains(21, "", "9990000304500", "", "", reslist);
            SetIndIfResultContains(21, "", "9990000304510", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040103946", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040103947", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040103948", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040103951", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040103954", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040103957", "", "", reslist);
            SetIndIfResultContains(21, "", "9990007070177", "", "", reslist);
            SetIndIfResultContains(21, "", "9990007070178", "", "", reslist);
            SetIndIfResultContains(21, "", "9990007070179", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040001021", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040000088", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040001022", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040103228", "", "", reslist);
            SetIndIfResultContains(21, "", "9990007085420", "", "", reslist);

            SetIndIfResultContains(21, "", "9990000016070", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040102649", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040102644", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040102648", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040102660", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040102643", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040102662", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040102742", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040102748", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040102741", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040102744", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040102747", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040102750", "", "", reslist);
            SetIndIfResultContains(21, "", "9990000396120", "", "", reslist);
            SetIndIfResultContains(21, "", "9990007061290", "", "", reslist);
            SetIndIfResultContains(21, "", "9990000006333", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040108525", "", "", reslist);
            SetIndIfResultContains(21, "", "3045001032", "", "", reslist);
            SetIndIfResultContains(21, "", "9990007096660", "", "", reslist);
            SetIndIfResultContains(21, "", "9990000304850", "", "", reslist);
            SetIndIfResultContains(21, "", "9990007073550", "", "", reslist);
            SetIndIfResultContains(21, "", "3045001041", "", "", reslist);
            SetIndIfResultContains(21, "", "9990000301270", "", "", reslist);

            SetIndIfResultContains(21, "", "9990007080680", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040103205", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040103206", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040103209", "", "", reslist);
            SetIndIfResultContains(21, "", "9993040000199", "", "", reslist);
            SetIndIfResultContains(21, "", "9990304840001", "", "", reslist);
            SetIndIfResultContains(21, "", "9990304840002", "", "", reslist);
            SetIndIfResultContains(21, "", "9990000370090", "", "", reslist);

            reslist = "Traction (Describe)";
            SetIndIfResultContains(21, "", "9990000304110", "", "", reslist);
            reslist = "Traction (Describe)";
            SetIndIfResultContains(21, "", "9990000304130", "", "", reslist);
            reslist = "3-Ulceration with or without bleeding";
            SetIndIfResultContains(21, "", "9990304000122", "", "", reslist);
            reslist = "Ulcerations present";
            SetIndIfResultContains(21, "", "9990000002113", "", "", reslist);
            reslist = "Tea,Rusty,Peach,Cherry,Pink,Ketchup";
            SetIndIfResultContains(21, "", "9990000006298", "", "", reslist);
            reslist = "Peritoneal port";
            SetIndIfResultContains(21, "", "9993040401216", "", "", reslist);
            reslist = "Red,Pink,Pale,Dusky,Purple,Mottled,Ecchymotic,Soft,Firm";
            SetIndIfResultContains(21, "", "9993040001044", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "3040001333", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "3040001334", "", "", reslist);
            reslist = "Clean,No clot";
            SetIndIfResultContains(21, "", "9993040101378", "", "", reslist);
            reslist = "Done";
            SetIndIfResultContains(21, "", "9993040101377", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(21, "", "9991020100022", "", "", reslist);
            reslist = "Vaginal pack,Uterine balloon,Uterotonic agent given (see MAR),Oxygen on,Legs elevated,MD notified,Bimanual exam per MD,Labs ordered";
            SetIndIfResultContains(21, "", "9991020100568", "", "", reslist);
            reslist = "Placed,Present,Removed";
            SetIndIfResultContains(21, "", "9991020100569", "", "", reslist);

            reslist = "Applied (comment number),Changed  (comment number),Marked,Reinforced,Site care,Staples removed (comment number),Marked,Reinforced,Site care,Staples removed (comment number)";
            SetIndIfResultContains(21, "", "9991733888867", "", "", reslist);
            reslist = "Drainage,Malodorous,Red";
            SetIndIfResultContains(21, "", "9991733888850", "", "", reslist);
            reslist = "Dry,Moist,Cannulated,Clamp off,Clamp on,Care done,2 cord vessels,3 cord vessels";
            SetIndIfResultContains(21, "", "9991140100024", "", "", reslist);
            reslist = "Gauze in place,Petroleum jelly applied,Petroleum jelly gauze applied,Other";
            SetIndIfResultContains(21, "", "9991140100026", "", "", reslist);
            reslist = "Bleeding,Edematous,Necrotic,Pink,Reddened,Serosanguinous drainage,Serous drainage,Other";
            SetIndIfResultContains(21, "", "9991140100027", "", "", reslist);

            reslist = "Chest washout";
            SetIndIfResultContains(22, "", "9993040001021", "", "", reslist);
            reslist = "> 30 minutes,> 60 minutes,> 90 minutes,> 120 minutes,> 180 minutes";
            SetIndIfResultContains(22, "", "9993040006782", "", "", reslist);



        }

        //private void CheckExtensiveWound()
        //{
        //    DateTime evdt = DateTime.MinValue;
        //    DateTime st_time = DateTime.MinValue;
        //    DateTime en_time = DateTime.MinValue;
        //    var query = StartNewQuery(SearchDepth.SearchDefault);
        //    query = AndCodeInList(query, "1375032519");
        //    query = AndResultInList(query, "extensive wound");
        //    query = query.OrderBy(e => e.EVENT_DATETIME);
        //    if (query.Count() == 0) return;
        //    foreach (var ci in query)
        //    {
        //        if (!_inds[22].is_checked)
        //        {
        //            evdt = ci.EVENT_DATETIME;
        //            st_time = GetResultTime("1375032547", evdt, 2);
        //            en_time = GetResultTime("1375032561", evdt, 2);
        //            if ((st_time != DateTime.MinValue) &&
        //                (en_time != DateTime.MinValue) && (st_time <= en_time))
        //            {
        //                if (st_time.AddMinutes(30) <= en_time)
        //                    SetInd(22, "Extensive Wound Management>=30min start:" + st_time.ToString() + " end:" + en_time.ToString());
        //            }
        //        }
        //    }
        //}

        // Use this if you can total the education time
        //private void CheckEducation(int total)
        //{
        //    if (_inds[23].is_checked) return;             //skip if already checked

        //    if (total >= 60) {
        //        SetInd(23, "education >= 60 min");
        //    }
        //}

        int EducMins(string timecode, string nursecode)
        {
            string res;
            int val = 0;
            if (ResultContains("", nursecode, "", "", "Nursing"))
            {
                if (GetResult("", timecode, "", "", out res))
                {
                    if (res.Left(1).IsNumeric())
                    {
                        val += (int)res.Val();
                    }
                }
            }
            return val;
        }

        private void Check_23()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("23. Healthcare Mgmt Education >= 1 Hour");
            Program.VerboseAudit("---------------");
            string reslist;
            int val = 0;
            int value = 0;

            reslist = "METHOD E,METHOD D,METHOD I"; 
            SetIndIfResultContains(23, "", "3040019564", "", "", reslist);

            SetIndIfResultContains(23, "", "3040018802", "", "", reslist);
            SetIndIfResultContains(23, "", "3040019429", "", "", reslist);
            SetIndIfResultContains(23, "", "3040019626", "", "", reslist);

            SetIndIfResultContains(23, "", "3040019773", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020569", "", "", reslist);
            SetIndIfResultContains(23, "", "3040018770", "", "", reslist);

            SetIndIfResultContains(23, "", "3040019597", "", "", reslist);
            SetIndIfResultContains(23, "", "3040019587", "", "", reslist);
            SetIndIfResultContains(23, "", "3040018788", "", "", reslist);
            SetIndIfResultContains(23, "", "3040018690", "", "", reslist);
            SetIndIfResultContains(23, "", "3040019462", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020762", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020565", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020708", "", "", reslist);

            SetIndIfResultContains(23, "", EXACT_MATCH_PREFIX+"940", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020757", "", "", reslist);
            SetIndIfResultContains(23, "", "3040019438", "", "", reslist);
            SetIndIfResultContains(23, "", "3040018686", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020288", "", "", reslist);

            SetIndIfResultContains(23, "", "3040020278", "", "", reslist);

            SetIndIfResultContains(23, "", "3040019172", "", "", reslist);

            SetIndIfResultContains(23, "", "3040020314", "", "", reslist);
            SetIndIfResultContains(23, "", "3040019604", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020664", "", "", reslist);
            SetIndIfResultContains(23, "", "3040019143", "", "", reslist);
            SetIndIfResultContains(23, "", "3040018773", "", "", reslist);
            SetIndIfResultContains(23, "", "3040018769", "", "", reslist);
            SetIndIfResultContains(23, "", "3040019385", "", "", reslist);
            SetIndIfResultContains(23, "", "3040018689", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020820", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020559", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020534", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020463", "", "", reslist);
            SetIndIfResultContains(23, "", "3040019415", "", "", reslist);

            SetIndIfResultContains(23, "", "3040020413", "", "", reslist);
            SetIndIfResultContains(23, "", "3040018515", "", "", reslist);

            SetIndIfResultContains(23, "", "3040020310", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020404", "", "", reslist);

            SetIndIfResultContains(23, "", "3040020512", "", "", reslist);

            SetIndIfResultContains(23, "", "3040018603", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020282", "", "", reslist);

            SetIndIfResultContains(23, "", "3040018908", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020466", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020255", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020262", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020297", "", "", reslist);

            SetIndIfResultContains(23, "", "3040019782", "", "", reslist);
            SetIndIfResultContains(23, "", "3040019666", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020328", "", "", reslist);
            SetIndIfResultContains(23, "", "3040018684", "", "", reslist);

            SetIndIfResultContains(23, "", "3040002644", "", "", reslist);
            SetIndIfResultContains(23, "", "3040001000", "", "", reslist);
            SetIndIfResultContains(23, "", "3040019679", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020321", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020477", "", "", reslist);
            SetIndIfResultContains(23, "", "3040018533", "", "", reslist);
            SetIndIfResultContains(23, "", "3040019736", "", "", reslist);
            SetIndIfResultContains(23, "", "3040018932", "", "", reslist);

            SetIndIfResultContains(23, "", "3040019011", "", "", reslist);

            SetIndIfResultContains(23, "", "3040018647", "", "", reslist);

            SetIndIfResultContains(23, "", "3040000907", "", "", reslist);

            SetIndIfResultContains(23, "", "3040019707", "", "", reslist);
            SetIndIfResultContains(23, "", "3040018906", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020694", "", "", reslist);
            SetIndIfResultContains(23, "", "3040019762", "", "", reslist);
            SetIndIfResultContains(23, "", "3040018587", "", "", reslist);
            SetIndIfResultContains(23, "", "3040018530", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020741", "", "", reslist);
            SetIndIfResultContains(23, "", "3040019360", "", "", reslist);
            SetIndIfResultContains(23, "", "3040019721", "", "", reslist);
            SetIndIfResultContains(23, "", "3040019691", "", "", reslist);
            SetIndIfResultContains(23, "", "3040019352", "", "", reslist);

            SetIndIfResultContains(23, "", "3040018525", "", "", reslist);
            SetIndIfResultContains(23, "", "3040018688", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020418", "", "", reslist);

            SetIndIfResultContains(23, "", "3040018687", "", "", reslist);
            SetIndIfResultContains(23, "", "3040018692", "", "", reslist);
            SetIndIfResultContains(23, "", "3040018691", "", "", reslist);
            SetIndIfResultContains(23, "", "3040018613", "", "", reslist);
            SetIndIfResultContains(23, "", "3040018631", "", "", reslist);

            SetIndIfResultContains(23, "", "3040020809", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020801", "", "", reslist);
            SetIndIfResultContains(23, "", "3040018872", "", "", reslist);

            SetIndIfResultContains(23, "", "3040020265", "", "", reslist);

            SetIndIfResultContains(23, "", "3040020550", "", "", reslist);

            SetIndIfResultContains(23, "", "3040018910", "", "", reslist);

            SetIndIfResultContains(23, "", "3040020431", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020730", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020546", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020540", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020991", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020982", "", "", reslist);

            SetIndIfResultContains(23, "", "3040020600", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020615", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020622", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020610", "", "", reslist);

            SetIndIfResultContains(23, "", "3040018556", "", "", reslist);

            SetIndIfResultContains(23, "", "3040020717", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020717", "", "", reslist);
            SetIndIfResultContains(23, "", "3040019405", "", "", reslist);

            SetIndIfResultContains(23, "", "3040018772", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020317", "", "", reslist);

            SetIndIfResultContains(23, "", "3040018685", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020554", "", "", reslist);
            SetIndIfResultContains(23, "", "3040019749", "", "", reslist);

            SetIndIfResultContains(23, "", "3040019448", "", "", reslist);

            SetIndIfResultContains(23, "", "3040004179", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020576", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020581", "", "", reslist);

            SetIndIfResultContains(23, "", "3040020744", "", "", reslist);

            SetIndIfResultContains(23, "", "3040019147", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020425", "", "", reslist);

            SetIndIfResultContains(23, "", "3040020437", "", "", reslist);
            SetIndIfResultContains(23, "", "3040019576", "", "", reslist);

            SetIndIfResultContains(23, "", "3040018909", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020324", "", "", reslist);
            SetIndIfResultContains(23, "", "3040019158", "", "", reslist);
            SetIndIfResultContains(23, "", "3040018598", "", "", reslist);
            SetIndIfResultContains(23, "", "3040018771", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020644", "", "", reslist);
            SetIndIfResultContains(23, "", EXACT_MATCH_PREFIX+"959", "", "", reslist);
            SetIndIfResultContains(23, "", "3040020635", "", "", reslist);

        }

        private void Check_24()
        {
            string reslist = "";
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("24. 1 to 1 Physiological Interv. >= 2 Hours");
            Program.VerboseAudit("---------------");

            reslist = "Continuous veno-venous hemofiltration,Continuous veno-venous hemodialysis,Continuous veno-venous hemodiafiltration,Slow continuous ultrafiltration";
            SetIndIfResultContains(24, "", "9990008100010", "", "", reslist);
            reslist = "Initiated,Continuous,Restarted";
            SetIndIfResultContains(24, "", "9990008100020", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(24, "", "9993041001004", "", "", reslist);
            SetIndIfResultContains(24, "", "9993041001003", "", "", reslist);
            SetIndIfResultContains(24, "", "9993040001002", "", "", reslist);
            SetIndIfResultContains(24, "", "9993040001003", "", "", reslist);
            SetIndIfResultContains(24, "", "9993040101000", "", "", reslist);
            SetIndIfResultContains(24, "", "9993040001093", "", "", reslist);
            reslist = "Bilevel,CPAP,Auto-Bilevel,Auto-CPAP,AVAPS,PCV";
            SetIndIfResultContains(24, "", "3045001108", "", "", reslist);
            reslist = "Nasal pillows,Nasal mask,Nasal Prongs;,Nasal pharyngeal,Full face mask,Performax,Total face mask,Endotracheal,Tracheostomy";
            SetIndIfResultContains(24, "", "9993040000637", "", "", reslist);
            reslist = "Mask,Tubing,CPAP,BPAP,Patient able to manage equipment on own,Equipment inspected (per site policy),Waiver signed (per site policy),Other";
            SetIndIfResultContains(24, "", "9993040000639", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(24, "", "9993040102551", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(24, "", "9993040102552", "", "", reslist);

            reslist = "Endotracheal tube,Esophageal - tracheal tube,Laryngeal mask airway (LMA),Nasopharyngeal airway (NPA),Oropharyngeal airway (OPA),Tracheostomy,Other";
            SetIndIfResultContains(24, "", "9991600100681", "", "", reslist);
            reslist = "Bag-valve-mask,Bag-valve-ET tube,Bag-valve-tracheostomy,Ventilator,Bilevel positive airway pressure (BiPAP),Continuous positive airway pressure (CPAP),CPAP nasal,CPAP mask,Positive pressure ventilation (PPV),Other";
            SetIndIfResultContains(24, "", "9991600100682", "", "", reslist);
            reslist = "Respiratory arrest,Cardiac arrest,Unknown,Other";
            SetIndIfResultContains(24, "", "9991600100646", "", "", reslist);
            reslist = "CPAP vent,ETT nasal,ETT oral";
            SetIndIfResultContains(24, "", "9991733888883", "", "", reslist);
            reslist = "4";
            SetIndIfResultContains(24, "", "9993040000407", "", "", reslist);
            reslist = "String";
            SetIndIfResultContains(24, "", "3045001117", "", "", reslist);
            reslist = "PACU Ventilator Rows,Trilogy Ventilator Rows,HFO Ventilator Rows,BPAP/CPAP Rows,Abbreviated Settings,NAVA Rows";
            SetIndIfResultContains(24, "", "9993040000635", "", "", reslist);
        }

        private void CheckUserDefined()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("User-Defined indicators");
            Program.VerboseAudit("---------------");
            string reslist;

            reslist = "Patient arrived during downtime,Patient was cared for during downtime,Patient departed during downtime";
            SetIndIfResultContains(99, "", "9991600100203", "", "", reslist);
            reslist = "Yes";
            SetIndIfResultContains(99, "", "9990000006437", "", "", reslist);
            reslist = "Green,Yellow ,Red ,Black ";
            SetIndIfResultContains(99, "", "9991600100265", "", "", reslist);
            reslist = "Downtime";
            SetIndIfResultContains(99, "", "9990007096330", "", "", reslist);


        }

        //private bool IsICU()
        //{
        //    switch (_pat.unit_name)
        //    {
        //        case "ICU":
        //        case "3BR":
        //        case "CTIC":
        //        case "NIC":
        //        case "PCU":
        //        case "TICU":
        //        case "CCB":
        //        case "RICU":
        //        case "CCUS":
        //            return true;
        //        default:
        //            return false;
        //    }
        //}

        //private bool IsTele()
        //{
        //    switch (_pat.unit_name)
        //    {
        //        case "4EST":
        //        case "2NW":
        //        case "3AE":
        //        case "5MEH":
        //        case "5NW":
        //        case "6MEH":
        //        case "6NW":
        //        case "7MEH":
        //        case "B4W":
        //        case "B5S":
        //        case "B5W":
        //        case "2NOB":
        //        case "CPLX":
        //        case "TELMS":
        //        case "MAN4":
        //            return true;
        //        default:
        //            return false;
        //    }
        //}



        //=====================================================================
        // Group like assessments into buckets of time
        // Each bucket counts as one assessment in the frequency count

        private void AddDependentBuckets(List<gBucket> bucket_list, string codelist1, string reslist1, string codelist2, string reslist2)
        {
            AddDependentBuckets(bucket_list, codelist1, reslist1, codelist2, reslist2, "Phantom", "");
        }

        private void AddDependentBuckets(List<gBucket> bucket_list, string codelist1, string reslist1, string codelist2, string reslist2, string codelist3,string reslist3)
        {
            bool dep3 = true;
            // get the chart items for the assessments
            var query1 = StartNewQuery();
            query1 = AndItemFilter(query1, "", codelist1, "", "", reslist1);
            var query2 = StartNewQuery();
            query2 = AndItemFilter(query2, "", codelist2, "", "", reslist2);
            if (codelist3.Trim() == "Phantom")
            {
                dep3 = false;
                codelist3 = "Hello, this is a phantom code";
            }
            var query3 = StartNewQuery();
            query3 = AndItemFilter(query3, "", codelist3, "", "", reslist3);

            // figure out what buckets the events belong to
            var query1a = from item in query1
                          select new
                          {
                              bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                              code = item.CODE,
                              evdt = item.EVENT_DATETIME
                          };
            var query2a = from item in query2
                          select new
                          {
                              bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                              code = item.CODE,
                              evdt = item.EVENT_DATETIME
                          };
            var query3a = from item in query3
                              select new
                              {
                                  bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                                  code = item.CODE,
                                  evdt = item.EVENT_DATETIME
                              };
            
            string s = "BucketList1 for " + codelist1 + ": ";
            foreach (var item in query1a)
            {
                s += item.bucket + ",";
            }
            Program.VerboseAudit(s);

            s = "BucketList2 for " + codelist2 + ": ";
            foreach (var item in query2a)
            {
                s += item.bucket + ",";
            }
            if (dep3)
            {
                s = "BucketList3 for " + codelist3 + ": ";
                foreach (var item in query3a)
                {
                    s += item.bucket + ",";
                }
            }
            Program.VerboseAudit(s);
            // Add to the list IFF items in both lists occur in same bucket
            foreach (var item1 in query1a)
            {
                foreach (var item2 in query2a)
                {
                    if (item1.bucket == item2.bucket)
                    {
                        if (dep3)
                        {
                            foreach (var item3 in query3a)
                            {
                                if (item1.bucket == item3.bucket)
                                {
                                    var b = new gBucket();
                                    b.bucket = item1.bucket;
                                    b.code = item1.code;
                                    b.evdt = item1.evdt;
                                    bucket_list.Add(b);
                                }
                            }
                        }
                        else
                        {
                            foreach (var item3 in query2a)
                            {
                                if (item1.bucket == item2.bucket)
                                {
                                    var b = new gBucket();
                                    b.bucket = item1.bucket;
                                    b.code = item1.code;
                                    b.evdt = item1.evdt;
                                    bucket_list.Add(b);
                                }
                            }
                        }
                    }
                }
            }

        }

        private void Check_Activities()
        {
            string reslist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit(" Activities");
            Program.VerboseAudit("---------------");

            reslist = "> 60 minutes,> 90 minutes,> 120 minutes,> 180 minutes";
            SetIndIfResultContains(101, "", "9993040002233", "", "", reslist);
            reslist = "Impulsive,Lack of safety awareness,Does not use call light or ask for assistance,Danger to self or others,Danger from others,Exit seeking";
            SetIndIfResultContains(101, "", "9993040009123", "", "", reslist);
            reslist = "Continuous observation by RN with patient";
            SetIndIfResultContains(101, "", "9993040009234", "", "", reslist);
            reslist = "Continuous observation by two staff with patient";
            SetIndIfResultContains(101, "", "9993040009234", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(101, "", "9993040009678", "", "", reslist);

            reslist = "Impulsive,Lack of safety awareness,Does not use call light or ask for assistance,Danger to self or others,Danger from others,Exit seeking";
            SetIndIfResultContains(102, "", "9993040009123", "", "", reslist);
            reslist = "Continuous observation by non-RN staff with patient";
            SetIndIfResultContains(102, "", "9993040009234", "", "", reslist);
            reslist = "Continuous observation by two staff with patient";
            SetIndIfResultContains(102, "", "9993040009234", "", "", reslist);


            //reslist = "Start and End Time";
            //SetIndIfResultContains(3, "", "", "", "", reslist);
            reslist = "Left unit with RN,Returned to unit";
            SetIndIfResultContains(103, "", "9993040000355", "", "", reslist);



            //reslist = "Start and End Time";
            //SetIndIfResultContains(4, "", "", "", "", reslist);
            reslist = "Left unit with nursing unlicensed staff,,Returned to unit";
            SetIndIfResultContains(104, "", "9993040000355", "", "", reslist);

            //reslist = ""; SetIndIfResultContains(5, "", "", "", "", reslist);


            reslist = "> 60 minutes,> 90 minutes,> 120 minutes,> 180 minutes";
            SetIndIfResultContains(106, "", "9993040006782", "", "", reslist);
            reslist = "Unit Based Nurse,Additional Nurse assist required (#),Patient completed with assistance";
            SetIndIfResultContains(106, "", "9993040007891", "", "", reslist);


            reslist = "> 60 minutes,> 90 minutes,> 120 minutes,> 180 minutes";
            SetIndIfResultContains(107, "", "9993040006782", "", "", reslist);
            reslist = "Additional unlicensed assist required";
            SetIndIfResultContains(107, "", "9993040007891", "", "", reslist);


            reslist = "Care conference";
            SetIndIfResultContains(108, "", "3047060440", "", "", reslist);
            reslist = "60 minutes,75 minutes,90 minutes,120 minutes,180 minutes";
            SetIndIfResultContains(108, "", "9993040004578", "", "", reslist);

            reslist = "Applied,Removed";
            SetIndIfResultContains(109, "", "9993040001033", "", "", reslist);
            reslist = "Applied,Removed";
            SetIndIfResultContains(109, "", "9993040001041", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(109, "", "9993040001560", "", "", reslist);
            reslist = "1,2,3,4+";
            SetIndIfResultContains(109, "", "9993040001561", "", "", reslist);
            reslist = "60 minutes";
            SetIndIfResultContains(109, "", "9991025062116", "", "", reslist);

            reslist = "Yes";
            SetIndIfResultContains(109, "", "99910701000111", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(109, "", "99910701000111", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(109, "", "9991150001752", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(109, "", "9991150001751", "", "", reslist);

            //reslist = "Start and End Time";
            //SetIndIfResultContains(10, "", "", "", "", reslist);

            //reslist = "Start and End Time";
            //SetIndIfResultContains(11, "", "", "", "", reslist);
            reslist = "Endotracheal tube,Esophageal - tracheal tube,Laryngeal mask airway (LMA),Nasopharyngeal airway (NPA),Oropharyngeal airway (OPA),Tracheostomy,Other";
            SetIndIfResultContains(111, "", "9991600100681", "", "", reslist);
            reslist = "Bag-valve-mask,Bag-valve-ET tube,Bag-valve-tracheostomy,Ventilator,Bilevel positive airway pressure (BiPAP),Continuous positive airway pressure (CPAP),CPAP nasal,CPAP mask,Positive pressure ventilation (PPV),Other";
            SetIndIfResultContains(111, "", "9991600100682", "", "", reslist);
            reslist = "Respiratory arrest,Cardiac arrest,Unknown,Other";
            SetIndIfResultContains(111, "", "9991600100646", "", "", reslist);
            reslist = "CPR";
            SetIndIfResultContains(111, "", "9993040001021", "", "", reslist);
            reslist = "Cardiopulmonary arrest";
            SetIndIfResultContains(111, "", "9993040001022", "", "", reslist);



        }


        private void AtLeastOneADL()
        {
            Program.Audit("---------------");
            if (_inds[1].is_checked)
            {
                var list = new List<int> { 305, 355 }; // N$G C2A
                if (list.Contains(_pat.unit_id))
                {
                    SetInd(2, "Promoting to ADL partial care for this unit.");
                }
            }

        }

        private void HighestIndicatorInEachGroupWins()
        {
            int i, g;
            bool highest_is_on;
            string ind_list;

            Program.Audit("Select highest indicator in each group");
            
            g = 0;
            highest_is_on = false;
            // Go from bottom up - highest (lowest) indicator in each group wins
            for (i = MAX_INDS; (i >= 1); i--) {
                if (_inds[i].radio_group > 0) {
                    if (_inds[i].radio_group != g) {
                        //this is a new group
                        g = _inds[i].radio_group;
                        highest_is_on = _inds[i].is_checked;
                    } else {
                        //same group
                        if (highest_is_on) {
                            _inds[i].is_checked = false;             //uncheck a lower number
                        } else {
                            highest_is_on = _inds[i].is_checked;     //save this one
                        }
                    }
                }
            } // next i
            
            //Echo the indicators for an Audit (no classification will be saved)
            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++) {
                if (_inds[i].is_checked) ind_list += "," + i;
            } // next i

            Program.Audit("Final list = " + ind_list.Substring(1));
            Program.Audit("---------------");
        }


        private void CheckProcs()
        {
            //CheckProc_1();
            //CheckProc_2();
            //CheckProc_3();
            //CheckProc_4();
            //CheckProc_5();
            //CheckProc_6();
            //CheckProc_7();
            //CheckProc_8();
            //CheckProc_9();
            //CheckProc_10();
            //CheckProc_11();

        }

        private void DoProc(int pnum, string code)
        {
            double mins = 0;
            string found_what;
            DateTime evdt;
            DateTime enddt = DateTime.MinValue;

            if (GetResultAndEVDT("", code, "", "", out found_what, out evdt))
            {
                mins = 60.0 * found_what.ToDouble();
                enddt = evdt.AddMinutes(mins);

                if (ProcExistsInDB(pnum, evdt, enddt))
                {
                    Program.Audit("Procedure " + pnum+ ": already exists");
                }
                else
                {
                    if (!QueuedProcOverlaps(pnum, evdt, enddt))
                    {
                        var proc = new proc_data();
                        proc.procedure_number = pnum;
                        proc.start = evdt;
                        proc.finish = enddt;
                        _procs.Add(proc);
                        Program.Audit("Procedure " + pnum + ": Found " + code + " between " + evdt + " and " + enddt);
                    }
                }

            }

        }

        private bool ProcExistsInDB(int pnum, DateTime startdt, DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && (proc.PROCEDURE_DATETIME == startdt)
                            && (proc.DEPARTURE_DATETIME == enddt)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new { proc.PROCEDURE_EVENT_ID };
            return (query.Count() > 0);
        }

        private bool QueuedProcOverlaps(int pnum, DateTime startdt, DateTime enddt)
        {
            bool overlap = false;

            proc_data[] pary = _procs.ToArray();
            for (int i = 0; i <= pary.GetUpperBound(0); i++)
            {
                if (pary[i].procedure_number == pnum)
                {
                    //overlap if   p.start between stardt and enddt
                    //             p.finish between startdt and enddt
                    //             startdt >= p.start and enddt <= p.finish
                    if ((pary[i].start >= startdt) && (pary[i].start < enddt) ||
                        (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
                        (startdt >= pary[i].start) && (enddt <= pary[i].finish))
                    {
                        overlap = true;
                    }

                    if ((pary[i].start >= startdt) && (pary[i].start < enddt) && (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
                        (pary[i].start < startdt) && (pary[i].finish > startdt) && (pary[i].finish < enddt)
                        )
                    {
                        // the proc in the list is completely contained in startdt/endt pair
                        // change this proc to be the startdt/enddt
                        // overlap is still true, but the list item will be updated with the encompassing times
                        //Program.Audit("Procedure " + pnum + ": " + startdt + " - " + enddt + " will trump " + pary[i].start + " - " + pary[i].finish);
                        pary[i].start = startdt;
                        pary[i].finish = enddt;
                    }
                }
            }
            _procs = pary.ToList();
            return overlap;
        }


        private void CheckProc_1()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P1. 1-1 safety observation by RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_2()
        {
            string nowstr;
            string toddtstr;
            string yesdtstr;
            string timea ="";
            DateTime timea_startdt, timea_enddt;
            DateTime timeb_startdt, timeb_enddt;
            DateTime nowdt = _pat.pull_finish;              // "now" is pull time
            DateTime yesdt;
            
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P2. 1-1 safety observation by non-RN");
            Program.VerboseAudit("---------------");

            nowstr = nowdt.ToString("yyyyMMddHHmm");
            yesdt = nowdt.AddDays(-1);
            toddtstr = nowdt.ToString("yyyyMMdd");
            yesdtstr = yesdt.ToString("yyyyMMdd");
            
            //when is now? (yesterday/today)
            //yes 7am -- yes 7p  -- tod 7a -- tod 7p
            //                                   A                   B
            //if nowdt >= tod7pm then check  tod 7am-tod 7pm and tod 7pm-tom 7a
            //if nowdt >= tod7am then check  yes 7p - tod 7a     tod 7a-tod 7p
            //if nowdt >= yes7pm then check  yes 7a-yes 7p       yes 7p-tod 7a
            if (nowstr.CompareTo(toddtstr + "1900") >= 0) {
                timea = toddtstr + "0700";
            } else if (nowstr.CompareTo(toddtstr + "0700") >= 0) {
                timea = yesdtstr + "1900";
            } else if (nowstr.CompareTo(yesdtstr + "1900") >= 0) {
                timea = yesdtstr + "0700";
            }
            
            timea_startdt = PFSUtility.ISOToDateTime(timea);
            timea_enddt = timea_startdt.AddHours(12);
            MaybeAddSitter(timea_startdt, timea_enddt);

            timeb_startdt = timea_enddt;
            timeb_enddt = timeb_startdt.AddHours(12);
            MaybeAddSitter(timeb_startdt, timeb_enddt);
        }

        private void MaybeAddSitter(DateTime startdt, DateTime enddt)
        {
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);    // add custom time range below
            query = query.Where(e => (e.EVENT_DATETIME >= startdt) && (e.EVENT_DATETIME < enddt));
            query = AndCodeInList(query, "Sitter");
            query = AndResultInList(query,"continued, initiated");
            query = AndResultNotInList(query, "discontinued");

            if (query.Count() > 0) {
                if (ProcExists(2, startdt, enddt)) {
                    Program.Audit("Procedure 2: already exists");
                } else {
                    var proc = new proc_data();
                    proc.procedure_number = 2;
                    proc.start = startdt;
                    proc.finish = enddt;
                    _procs.Add(proc);
                    Program.Audit("Procedure 2: Found Sitter between " + startdt + " and " + enddt);
                }
            }
            
        }

        private bool ProcExists(int pnum, DateTime startdt, DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && (proc.PROCEDURE_DATETIME == startdt)
                            && (proc.DEPARTURE_DATETIME == enddt)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new {proc.PROCEDURE_EVENT_ID};
            return (query.Count() > 0);
        }


        private void CheckProc_3()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P3. Off unit accompanied by RN");
            Program.VerboseAudit("---------------");
            DoProc(3, "A_MHAcuOffUnit");
        }

        private void CheckProc_4()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P4. Off unit accompanied by non-RN");
            Program.VerboseAudit("---------------");
            DoProc(4, "A_MHAcuOffUNonRN");
        }

        private void CheckProc_5()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P5. Patient/family education by RN");
            Program.VerboseAudit("---------------");
            DoProc(5, "A_MHAcuPtFamEduc");
        }

        private void CheckProc_6()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P6. Extensive wound management by RN");
            Program.VerboseAudit("---------------");
            DoProc(6, "A_MHAcuExtensive");
        }

        private void CheckProc_7()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P7. Extensive wound management by non-RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_8()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P8. Coordination of care by RN");
            Program.VerboseAudit("---------------");
            DoProc(8, "A_MHAcuCoordinat");
        }

        private void CheckProc_9()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P9 1:1 RN at bedside");
            Program.VerboseAudit("---------------");
            DoProc(9, "A_MHAcu1:1byURN");
        }

        private void CheckProc_10()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P10. 1:1 non-RN at bedside");
            Program.VerboseAudit("---------------");
            DoProc(10, "A_MhAcu1:1UNonRN");
        }

        private void CheckProc_11()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P11. 2:1 by RN at bedside");
            Program.VerboseAudit("---------------");
            DoProc(11, "A_MHAcu2:1by URN");
        }

        private void CheckOutcomes()
        {
            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = AndItemFilter(query, "", "A_MHPostFalltime", "", "", "");
            foreach (var ch in query)
            {
                var outc = new proc_data();
                outc.procedure_number = 1;
                outc.start = ch.EVENT_DATETIME;
                _outcomes.Add(outc);
                Program.Audit("Outcomes 1: Found A_MHPostFalltime at " + outc.start);
            }
        }
        

        private const string DATETIME_FORMAT = "yyyyMMddHHmm";              // ISO Date/Time w/o seconds

        private void OutputClass(bool use_default)
        {
            string outstr, ind_list, desc, str_pull_dt;
            int i, tc_event_id;

            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         212 242 304
            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1
            //1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901 3456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
            //1       |10 5            |                |                |        |10023121779         |ALEXANDER                       |GLORIA                          |                                |0508    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289530  |           |201403030400                                                                      |NNYNNNNYYNNYNNNYNNNYYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//1       |10 5            |                |                |        |10024544908         |ARMSTRONG                       |AARON                           |                                |0535    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289531  |           |201403030400                                                                      |NYNNYNNNNNNYNNNYNNNNYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//|0547    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289532  |           |201403030400                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//|0547    |A   |201403030700|                |201403030700    |18  |C|    |3040|1440|18289532  |           |201403030700                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN

            if (Program.g_is_test)
                tc_event_id = 9999;
            else
                tc_event_id = PFSDBUtility.NextGID();                         //get a unique id for this class
                                                                              //            str_pull_dt = _pat.pull_finish.ToString(DATETIME_FORMAT);
            str_pull_dt = _pat.pull_finish.ToString(DATETIME_FORMAT);

            outstr = _pat.facilty_code.FixedWidth(8);                       //(facility code)
            outstr += "|" + _pat.unit_name.FixedWidth(16);
            outstr += "|" + "".FixedWidth(16);                               //(unit code)
            outstr += "|" + txarea.FixedWidth(16);                               //(area code)
            outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
            outstr += "|" + _pat.acct.FixedWidth(20);
            outstr += "|" + _pat.last_name.FixedWidth(32);
            outstr += "|" + _pat.first_name.FixedWidth(32);
            outstr += "|" + _pat.middle_name.FixedWidth(32);
            outstr += "|" + _pat.room.FixedWidth(8);
            outstr += "|" + _pat.bed.FixedWidth(4);
            outstr += "|" + str_pull_dt.FixedWidth(12);                      //class datetime (could change)
            outstr += "|" + "".FixedWidth(16);                               //(login)
            outstr += "|" + str_pull_dt.FixedWidth(16);                      //(employee)/(pull datetime)
            outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
            outstr += "|" + "C".FixedWidth(1);                               //record type = class
            outstr += "|" + "".FixedWidth(4);                                //(stage)
            outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
            outstr += "|" + _pat.range.ToString().FixedWidth(4);             //TC pull range
            outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
            outstr += "|";
            outstr = outstr.FixedWidth(294);
            outstr += "|" + _pat.effective.ToString(DATETIME_FORMAT);        //IN
            outstr = outstr.FixedWidth(377);
            outstr += "|";

            if (use_default)
            { //make all is_checked = false and then mark defaults
                Program.VerboseAudit("Patient Will receive default indicators " + _pat.default_inds_str);
                for (i = 1; (i <= MAX_INDS); i++)
                {
                    _inds[i].is_checked = false;
                }
                foreach (var ind in _pat.default_inds)
                {
                    if (ind <= _inds.GetUpperBound(0))
                    {
                        _inds[ind].is_checked = true;
                    }
                }
            }

            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++) {
                if (_inds[i].is_checked) {
                    outstr += "Y";
                    ind_list += "," + i;
                } else {
                    outstr += "N";
                }
            } // next i
            ind_list = ind_list.Substring(1);                           //strip leading comma
  
            Program.outfile.WriteLine(outstr);                          //output to transparent.txt

            Program.Audit("");
            desc = "Classified: " + ind_list;
            if (Program.g_is_test) {
                Program.Audit(desc);
            } else {
                //Save the selected indicators plus both Program.Audits in the event log; link with tc_event_id
                PFSEventLog.AddTransparentMappingEventLogEntry(
                    desc, Program.gLogUnitID, Program.gLogEncounterID,
                    tc_event_id, Program.gLogMapperVersion,
                    Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
            }
        }

        private int DeterminePtypeOfIndicators()
        {
            int i;
            var pscore = 0.0;
            int pt_type = 6;
            var indlist = new List<int>();
            string indstr = "";

            Program.VerboseAudit("Determine patient type of triggered indicators");

            for (i = 1; (i <= MAX_INDS); i++)
            {
                if (_inds[i].is_checked)
                {
                    indlist.Add(i);
                    indstr += i.ToString() + ",";
                }
            }

            var db = PFSDBUtility.NewPfsDataContext();
            var query_ind_def = from ind_def in db.INDICATOR_DEFINITIONs
                                where (ind_def.METHODOLOGY_ID == PFSGlobal.METH_ID_APLUS_MENTAL_HEALTH) &&
                                  indlist.Contains(ind_def.INDICATOR_NUMBER)
                                select new
                                {
                                    ind_def.WEIGHT
                                };
            foreach (var wgts in query_ind_def)
            {
                pscore += wgts.WEIGHT;
            }
            Program.VerboseAudit("indicators=" + indstr);
            Program.VerboseAudit("score=" + pscore.ToString());

            var query_ptype = from ptype in db.PATIENT_TYPEs
                              where (ptype.METHODOLOGY_ID == PFSGlobal.METH_ID_APLUS_MENTAL_HEALTH)
                              orderby ptype.PATIENT_TYPE1
                              select new
                              {
                                  ptype.PATIENT_TYPE1,
                                  ptype.POINTS_HIVAL
                              };

            foreach (var ptypes in query_ptype)
            {
                //                Program.DebugTrace("type,hival=" + ptypes.PATIENT_TYPE1.ToString() + "," + ptypes.POINTS_HIVAL.ToString(), "");
                if (pscore <= ptypes.POINTS_HIVAL)
                {
                    if (pt_type > ptypes.PATIENT_TYPE1)
                    {
                        pt_type = ptypes.PATIENT_TYPE1;
                    }
                }
            }
            Program.VerboseAudit("patient type=" + pt_type.ToString());

            return pt_type;

        }


        private bool ExistDefaultInPast16hrs()
        {
            DateTime cdt1 = DateTime.MinValue;
            DateTime cdt2 = DateTime.MinValue;
            int cnt_all = 0;
            int cnt_def = 0;
            //get max class date of last non-default class = 1
            //get max class date of last default = 2
            // if 1 >= 2 then false
            // else
            //   if 2 > 1 and date is <= 16 hrs ago then true
            //   else false
            var db = PFSDBUtility.NewPfsDataContext();
            //var query = from ce in db.CLASSIFICATION_EVENTs
            //            where (ce.ENCOUNTER_ID == _pat.encounter_id)
            //            && (ce.CLASSIFIED_BY_ID != -2)
            //            && (System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(ce.CLASSIFICATION_DATETIME, Program.g_pull_start) <= 960)
            //            select ce;
            //cnt_all = query.Count();
            //if (cnt_all > 0)
            //{
            //    cdt1 = PFSUtility.DBToDateTime(query.Max(x => x.CLASSIFICATION_DATETIME));
            //    Program.VerboseAudit("Last regular classification was at:"+cdt1.ToString());
            //}
            //else {
            //    Program.VerboseAudit("No regular classifications within the past 16 hours");
            //}

            var query = from ce in db.CLASSIFICATION_EVENTs
                        where (ce.ENCOUNTER_ID == _pat.encounter_id)
                        && (ce.CLASSIFIED_BY_ID == -2)
                        && (System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(ce.CLASSIFICATION_DATETIME, Program.g_pull_finish) <= 960)
                        select ce;
            cnt_def = query.Count();

            if (cnt_def > 0)
            {
                cdt2 = PFSDBUtility.DBToDateTime(query.Max(x => x.CLASSIFICATION_DATETIME));
                Program.VerboseAudit("Last default classification was at:" + cdt2.ToString());
            }
            else
            {
                Program.VerboseAudit("No default classifications within the past 16 hours");
            }
            return (cnt_def > 0);

        }


        //private void OutputProcs()
        //{
        //    int i;
        //    string outstr, proc_list, desc;
        //    int tc_event_id;

        //    foreach(var proc in _procs) {
        //        if (Program.g_is_test)
        //            tc_event_id = 9999;
        //        else
        //            tc_event_id = PFSDBUtility.NextGID();                         //get a unique id for this proc

        //        outstr = _pat.facilty_code.FixedWidth(8);
        //        outstr += "|" + _pat.unit_name;                                 //10
        //        outstr = outstr.FixedWidth(68);
        //        outstr += "|" + _pat.acct.FixedWidth(20);                       //90
        //        outstr += "|" + _pat.last_name.FixedWidth(32);
        //        outstr += "|" + _pat.first_name.FixedWidth(32);
        //        outstr += "|" + _pat.middle_name.FixedWidth(32);
        //        outstr = outstr.FixedWidth(202);
        //        outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //204 proc dt
        //        outstr = outstr.FixedWidth(254);
        //        outstr += "|P";                                                 //256 procedure type record
        //        outstr += "|" + "".FixedWidth(4);                               //(stage)
        //        outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);     //TC source ID
        //        outstr += "|" + _pat.range.ToString().FixedWidth(4);            //TC pull range
        //        outstr += "|" + tc_event_id.ToString().FixedWidth(10);          //TC event ID
        //        outstr += "|";
        //        outstr = outstr.FixedWidth(294);
        //        outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //296 procdt in
        //        outstr = outstr.FixedWidth(346);
        //        outstr += "|" + proc.finish.ToString(DATETIME_FORMAT);          //348 procdt out
        //        outstr = outstr.FixedWidth(377);
        //        outstr += "|";

        //        proc_list = "";
        //        for (i = 1; (i < MAX_PROCS); i++) {
        //            if (proc.procedure_number == i) {
        //                outstr += "Y";
        //                proc_list += "," + i;
        //            } else {
        //                outstr += "N";
        //            }
        //        } // next i
        //        proc_list = proc_list.Substring(1);                             //strip leading comma

        //        Program.outfile.WriteLine(outstr);                              //output to transparent.txt

        //        desc = "Procedures: " + proc_list;
        //        if (Program.g_is_test) {
        //            Program.Audit(desc);
        //        } else {
        //            //Save the selected procedures plus both Program.Audits in the event log; link with tc_event_id
        //            //(this assumes the procedure Program.Audit is mixed in with the class Program.Audit)
        //            PFSEventLog.AddTransparentMappingEventLogEntry(desc, Program.gLogUnitID, Program.gLogEncounterID,
        //                tc_event_id, Program.gLogMapperVersion,
        //                Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
        //        }
        //    } // next proc
        //}

    }
}
