﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;                // for SqlConnection
using System.IO;                            // for Directory, Path, Stream
using System.Windows.Forms;                 // for Application (also add reference)
using PfsShared;                            // add a reference to Shared2 project
// PARTNERS TRANSLATION 
// 
// NOTE: Each unit must set the "use transparent" flag.
//
// sample: -efftime=0700 -effdate=yesterday -pulltime=0700 -pulldate=today -range=1440
//
// This is designed to be run as a background job, so never stop for user input -- that means no error MsgBox.
// All communication is done through log files and the event log; it is OK to print to the console.
//
// In order to work with the AcuityPlus Patient Selection and Transparent import:
//
//   * The program name must be TransparentMapping.exe, placed in AcuityPlus\bin
//   * The audit file is called TransparentAudit.log, placed in AcuityPlus\log
//   * The output file is Transparent.txt, placed in AcuityPlus\load_me
//   * Events are saved in the database event log
//
namespace TransparentMapping
{
    public class PatientInfo
    {
        public int      encounter_id;
        public string   last_name;
        public string   first_name;
        public string   middle_name;
        public string   acct;
        public double   age;                    // age (in years) at admission
        public string   lang_lvc;  //data is formatted as xxx;;yyy xxx=preferred yyy=written
        public DateTime dob;
        public string   room;
        public string   bed;
        public DateTime unit_arrival;
        public DateTime unit_departure;
        public DateTime effective;              // patient specific (may be > g_effdt)
        public DateTime effective_out;              // patient specific (may be > g_effdt)
        public DateTime pull_start;             // patient specific (may be > g_pull_start)
        public DateTime pull_finish;            // patient specific (may be < g_pull_finish)
        public int      range;                  // patient specific (may be < g_range) - minutes
        public double   los_hours;              // hours during pull
        public int      TC_source_id;           // TCP port (chart source)
        // unit info
        public string   short_name;           // class import facility code
        public string   unit_name;
        public int      unit_id;
        public bool     is_ED;
        public bool     is_ICU;
        public int      meth_id;
        public string adt_unit_name;
        public DateTime sod;
        public int upid;
        public int ptype;
//        public List<PatientLocation> patloclist;
        public int loc_idx;
        //public string default_inds_str;         // "1,6,8"
        //public int default_ptype;
    }
    public struct med_data
    {
        public string code;
        public string descript;
        public string drugclass;
        public string result;
        public DateTime evdt;
    }

    public struct LOAtypePrecision
    {
        public DateTime startdt;
        public DateTime enddt;
        public DateTime timestamp;
        public bool cancel;
        public bool retain;
    }
    public class PatientLocation
    {
        public int unit_id;
        public string unit_name;
        public int methid;
        public string room;
        public string bed;
        public string service;
        public DateTime in_time;
        public DateTime out_time;
        public int los_mins;
        public bool remove;  //flag for compacting locs
        public DateTime arr_time;
        public DateTime loc_start;
        public DateTime los_start;
        public DateTime los_end;
        public int special_unit_id;
        public bool is_last_loc;
        public int loc_idx;
    }
    public struct EventLog_Data
    {
        public DateTime timestamp;
        public string srctxt;
    }

    public static class Program
    {

        const string    MAPPER_VERSION = "1.00";
        const string    TRANSP_FILENAME = "Transparent.txt";    // THE output file (for class import)
        const string TRANSP_FILENAME2 = "Transparent5am.txt";    // THE output file (for class import)
        const string    TRANSP_AUDIT_LOG = "IPTransparentAudit.log";  // Legacy debug/audit log
        const int       CHART_ITEM_LIFE = 360;                   // days in the chart_item table
        const int       DEFAULT_RANGE = 1440;                   // 24 hrs in minutes; override with -range

        public static bool g_abort;                         // stop!
        public static bool g_debug;                         // output to console?
        public static bool g_log;                           // output to log file? (legacy feature)
        public static bool g_no_output;                     // audit file only?
        public static bool g_is_test;                       // output to file w/dummy ids and no event log
        public static bool g_no_delete;                     // keep old chart items?
        public static bool g_stop_on_error;
        public static bool g_import_when_done;
        public static bool g_use_all_chart_items=false;
        public static bool g_do_MH = true; //do MH classifications
        public static bool g_do_OW = true; //do Other Workload
        public static bool g_hiacuity_only = false;
        public static bool g_make5am = false;
        public static bool g_noactivities = false;
        public static bool g_onlydnc = false;
        private static int g_prearrival_range = 24; //24 hour lookback for pre-arrival orders

        public static DateTime      g_effdt;                // effective datetime

        public static StreamWriter  outfile;                // Transparent.txt
        public static StreamWriter outfile2;                // Transparent2.txt
        public static StreamWriter  logfile;                // TransparentLog.txt
        public static StreamWriter medlogfile;                // medLog.txt
        public static StreamWriter psymedlogfile;                // psymedLog.txt
        public static StreamWriter oralmedlogfile;                // oralmedLog.txt

        public static int           gLogUnitID;
        public static int           gLogEncounterID;
        public static string        gLogSourceText;
        public static string        gLogMapperVersion;

        public static StringBuilder gBriefAudit;            // the complete brief audit
        public static StringBuilder gVerboseAudit;          // the complete verbose audit

        public static DateTime      g_pull_start;           // global pull start  (not limited by patient values)
        public static DateTime      g_pull_finish;          // global pull finish
        public static int           g_range;                // global range

        private static string   _this_acct;                 // acct filter
        private static string   _this_unit_name;            // unit name filter
        private static int      _this_unit_id;              // unit id filter
        private static string   _these_facs;     // -facs=2,5  facilities filter based on F.class_fac_code
          //example:  AND SUBSTRING(F.CLASSIFICATION_FACILITY_CODE, 1, 1) in (2,5)

        static string           _import_path;
        static string           _log_path;
        public static List<PatientLocation> patloclist;
        public static List<PatientLocation> patperioplist;

        //private static string _category;
        //private static string _fndcode;
        //private static string _descript;
        //private static string _group;
        private static string _result;
        //private static string _perfdate;
        private static PatientInfo pat;
        private static int seq = 0;
        private static DateTime arrival_within24hrs_minlocdt = DateTime.MinValue;
        public static long _t1 = 0;
        public static long _t2 = 0;
        public static long _t3 = 0;
        public static long _t4 = 0;
        public static long _t5 = 0;
        public static long _t6 = 0;
        public static long _t7 = 0;
        public static long _t8 = 0;
        public static System.Diagnostics.Stopwatch _sw;
        public static DateTime g_pull_start_save;           // global pull start  (not limited by patient values)
        public static DateTime g_pull_finish_save;          // global pull finish
        public static DateTime startTime, endTime;

        static void Main(string[] args)
        {
            try
            {
                InitGlobals();

                ParseCommandLine(args);
                SetMapperVersion();

                gLogSourceText = String.Join(" ", args);           // add command line to log
                LogInfo("Begin mapping and translation", PFSEventLog.EventLogCategory.EVENT_CATEGORY_STARTUP_SHUTDOWN);
                gLogSourceText = "";

                OpenOutputFiles();
                ReadMedAdminFile("highacuityextract_prd.txt","HIACUMED");
                ReadMedAdminFile("mentalhealthextract_prd.txt", "PSYMED");
                ReadMedAdminFile("highacuityoralextract_prd.txt", "ORALMED");
                UpdateMEELocs();

                if (!g_hiacuity_only)
                {
                    ProcessPatients();
                }

//                DeleteOldChartItems();

                LogInfo("Mapping complete", PFSEventLog.EventLogCategory.EVENT_CATEGORY_STARTUP_SHUTDOWN);

                //MaybeRunImport();
                CloseOutputFiles();
            }
            catch (Exception e)
            {
                Console.WriteLine("Unexpected Error: {0}", e.Message);
                Console.WriteLine("{0}", e.StackTrace);
                LogUnexpectedError(e.Message, e.StackTrace);
            }

            if (g_debug)
            {
                Console.WriteLine(Environment.NewLine);
                Console.Write("Press any key...");
                Console.ReadKey();
            }
        }

        public static void sttimer()
        {
            _sw = System.Diagnostics.Stopwatch.StartNew();
        }
        public static long entimer(string s,long t)
        {
            _sw.Stop();
            t += _sw.ElapsedMilliseconds;
            VerboseAudit(s + "=" + t);
            return t;
        }

        static void InitGlobals()
        {
            gLogSourceText = "";
            gBriefAudit = new StringBuilder();
            gVerboseAudit = new StringBuilder();
        }

        static void ParseCommandLine(string[] args)
        {
            string nowdate, nowtime;
            string value;
            string effdate, efftime;
            string pulldate, pulltime;

            var nowdt = DateTime.Now;
            nowdate = nowdt.ToString("yyyyMMdd");
            nowtime = nowdt.ToString("HHmm");
            effdate = "";
            efftime = "";
            pulldate = "";
            pulltime = "";

            _import_path = PFSUtility.DefaultImportPath();          // ...\load_me
            _log_path = PFSUtility.DefaultLogPath();                // ...\log

            foreach (var arg in args)
            {
                var arr = arg.Split('=');
                value = (arr.GetUpperBound(0) > 0) ? arr[1] : "";

                switch (arr[0])
                {
                    case "-acct":                               // process this patient only
                        _this_acct = value;
                        break;
                        
                    case "-debug":                              // output to console and pause at end
                        g_debug = true;
                        break;

                    case "-effdate":                            // effective date: yyyymmdd
                        if (value == "yesterday")
                            effdate = DateTime.Now.AddDays(-1).ToString("yyyyMMdd");
                        else
                            effdate = value.Left(8);
                        break;
                    
                    case "-efftime":                            // effective time: HHMM or HH
                        efftime = value.Left(4);
                        efftime = efftime.PadRight(4,'0');
                        break;
                    
                    case "-import":                             // run transparent import when done
                        g_import_when_done = true;
                        break;

                    case "-log":                                // create TransparentAudit.log
                        g_log = true;                           // (verbose audit)
                        break;

                    case "-path":                               // override default import path
                        _import_path = value;
                        break;

                    case "-logpath":                               // override default import path
                        _log_path = value;
                        break;
                    case "-n":                                  // no output to transparent.txt 
                        g_no_output = true;                     // (audit only)
                        break;

                    case "-nodelete":                           // do not delete old chart items
                        g_no_delete = true;
                        break;

                    case "-pulldate":                           // pull date: yyyymmdd
                        if (value == "today")
                            pulldate = DateTime.Now.ToString("yyyyMMdd");
                        else
                            pulldate = value.Left(8);
                        break;
                    
                    case "-pulltime":                           // pull time: HHMM or HH
                        pulltime = value.Left(4);
                        pulltime = pulltime.PadRight(4,'0');
                        break;

                    case "-range":                              // range in minutes; default = 1440 = 24hr
                        g_range = value.ToInteger();
                        break;
                        
                    case "-stoponerror":
                        g_stop_on_error = true;
                        break;

                    case "-test":
                        g_is_test = true;
                        break;

                    case "-citest":
                        g_use_all_chart_items = true;
                        break;

                    case "-make5am":
                        g_make5am = true;
                        break;

                    case "-unit":                               // process this unit only
                        _this_unit_name = value;
                        break;
                    case "-unit_id":                            // process this unit only
                        _this_unit_id = value.ToInteger();
                        break;
                    case "-facs":                               // process these facs only
                        _these_facs = value;
                        break;
                    case "-omitMH":
                        g_do_MH = false;
                        break;
                    case "-omitOW":
                        g_do_OW = false;
                        break;
                    case "-medsonly":
                        g_hiacuity_only = true;
                        break;
                    case "-noactiv":
                        g_noactivities = true;
                        break;
                    case "-onlydnc":
                        g_onlydnc = true;
                        break;
                    case "-prearr":
                        g_prearrival_range = value.ToInteger();
                        break;

                    default:
                        Console.WriteLine("unexpected argument: {0}", arg);
                        break;
                }
            }

            if (!String.IsNullOrEmpty(effdate) && !effdate.StartsWith("20"))
            {
                if (_these_facs.StartsWith("6"))
                {
                    DateTime tempdt = PFSUtility.ISOToDateTime(nowdate);
                    if (efftime.StartsWith("23"))
                    {
                        tempdt = tempdt.AddDays(-1);
                        effdate = PFSUtility.DateTimeToISODate(tempdt);
                    }
                    else
                        effdate = nowdate;
                    pulldate = effdate;
                }
            }
            if (String.IsNullOrEmpty(effdate))
                effdate = nowdate;
            if (String.IsNullOrEmpty(efftime))
                efftime = nowtime;

            // Note: pulldate defaults to effdate, not nowdate
            if (String.IsNullOrEmpty(pulldate))
                pulldate = effdate;
            if (String.IsNullOrEmpty(pulltime))
                pulltime = efftime;

            if (g_range == 0)
                g_range = DEFAULT_RANGE;

            DebugTrace("Import path=", _import_path);
            DebugTrace("Log path=", _log_path);
            DebugTrace("effdate={0}", effdate);
            DebugTrace("efftime={0}", efftime);
            DebugTrace("pulldate={0}", pulldate);
            DebugTrace("pulltime={0}", pulltime);
            DebugTrace("range={0}", g_range);

            // class IN time
            g_effdt = PFSUtility.ISOToDateTime(effdate + efftime);
            // range for chart item queries
            g_pull_finish = PFSUtility.ISOToDateTime(pulldate + pulltime);
            g_pull_start = g_pull_finish.AddMinutes(-g_range);
            g_pull_start_save = g_pull_start;
            g_pull_finish_save = g_pull_finish;

        }

        static void UpdateMEELocs()
        {//update enc_loc records with unit=10 PEDI sent there via MEE PERIOP alias
            // with age>=18.  Leave the <18 pts alone.
            string sqlmee = "update el set el.unit_id=-1, el.WORKING_UNIT_ID=-1, HOSPITAL_SERVICE_LVC='TC'";
            sqlmee += " from encounter_location as el";
            sqlmee += " inner join encounter as e on (el.encounter_id= e.encounter_id)";
            sqlmee += " inner join unit as u on (el.unit_id=u.unit_id)";
            sqlmee += " where u.name='10 PEDI' and el.ADT_UNIT_NAME='MEEMPROP' and e.AGE_AT_ADMISSION>=18";
            sqlmee += " and el.datetime_in>dateadd(day,-14," + PFSDBUtility.SQLDateTime(g_pull_start) + ")";

            var db = PFSDBUtility.NewSqlConnection();
            var cmd = new SqlCommand(sqlmee, db);
            cmd.ExecuteNonQuery();
            db.Close();

        }

        static void ReadMedAdminFile(string fn, string itemcode)
        {//IMPORTANT:  These are orders, so the date is only the inception. 
            // the fact that they are present means they are continued/in progress.
            //mrno      acctnum     date        time    descript
            //3249666	2000751639	20190716	0825	HEPARIN (PORCINE) 5,000 UNIT/ML INJECTION SOLUTION
            //assume file is sorted via windows sort /+
            string line = "x";
            string acct_num = "a";
            string prev_acct_num = "x";
            string code;
            string desc = "";
            short seq = 0;
            int unitid = 0;
            int encid = 0;
            string orderid = "";
            bool showmsg = false;
            string med_dt = "";
            string med_tm = "";
            string orderctrl = "NW";

            int hiacumedtype = 0;
            if (itemcode == "PSYMED") hiacumedtype = -99;
            if (itemcode == "ORALMED") hiacumedtype = -98;
            code = itemcode;
            switch (hiacumedtype)
            {
                case 0: VerboseAudit("Reading in High Acuity Meds orders..."); break;
                case -99: VerboseAudit("Reading in Psychotropic Meds orders..."); break;
                case -98: VerboseAudit("Reading in High Acuity Oral Meds orders..."); break;
            }

            string[] list1_meds = {
"Ado-trastuzumab emtansine","Aldesleukin","Alemtuzumab","Arsenic trioxide","Atezolizumab",
"Axicatbtagene ciloleucel","Belinostat","Bendamustine","Bevacizumab ","Bevacizumab-awwb",
"Blinatumomab","Brentuximab","Busulfan","Cabazitaxel","Carboplatin",
"Carmustine","Cemiplimab-rwlc","Cetuximab","Cisplatin","Cladribine",
"Clofarabine","Copanlisib","Cyclophosphamide","Dacarbazine","Dactinomycin",
"Daratumumab","Daruvalumab","Daunorubicin","Daunorubicin/cytarabine","Decitabine",
"Denileukin diftitox","Dinutuximab","Docetaxel","Doxorubicin","Elotuzumab",
"Epirubicin","Eribulin","Etoposide","Fludarabine","Fluorouracil (5FU)",
"Gemcitabine","Gemtuzumab","Ibritumomab","Idarubicin","Ifosfamide",
"Inotuzumab","Interferon alfa-2B","Iobenguane","Irinotecan","Irinotecan liposomal",
"Ipilimumab","Ixabepilone","Melphalan","Mitomycin","Mitoxantrone",
"Mogamulizumab-kpkc","Moxetumomab pasudotox-tdfk","Necitumumab","Nelarabine","Nivolumab",
"Obinutuzumab","Ofatumumab","Olaratumab","Oxaliplatin","Paclitaxel",
"Paclitaxel-protein bound","Panitumumab","Pembrolizumab","Pemetrexed","Pentostatin",
"Pertuzumab","Polatuzumab vedotin-piiq","Porfimer","Pralatrexate","Ramurcirumab",
"Rituximab","Romidepsin","Siltuximab","Sipuleucel-T","Streptozocin",
"Tagraxofusp-erzs","Tastuzumab-anns","Temsirolimus","Teniposide","Tigenlecleucel",
"Temozolomide","Trabectedin","Topotecan","Tositumomab","Trastuzumab",
"Vinorelbine","Vinblastine","Vincristine","Ziv-afilibercept" };
            var list1_meds_lower = list1_meds.Select(s => s.ToLower()).ToArray();
            //LIST #2 - This medication list to trigger indicator only if medication administered through route of peripheral IV:
            string[] list2_meds = {
"Adrenalin","DDVAP","Desmopressin","Dobutamine","Dopamine",
"Droxidopa","Ephedrine","Epinephrine","Inamrinone","Isoproterenol",
"Mephentermine","Midodrine","Milrinone","Norepinephrine","Phenylephrine","Vasopressin" };
            var list2_meds_lower = list2_meds.Select(s => s.ToLower()).ToArray();

            string path = "c:\\users\\public\\acuityplus\\load_me\\";
            string pathfn = path + fn;


                if (File.Exists(path + fn))
                {
                    File.Copy(path + fn, path + PFSUtility.DateTimeToISODateTime(DateTime.Now) + fn);
                    using (StreamReader file = new StreamReader(@pathfn))
                    {
                    using (var db = PFSDBUtility.NewSqlConnection())
                    {
                        while ((line = file.ReadLine()) != null)
                        {
                            if (hiacumedtype >= 0) hiacumedtype = 0;
                            //VerboseAudit(line);
                            //MessageBox.Show(line);
                            if (line.Trim() != "")
                            {
                                string[] col = line.Split('|');
                                orderid = col[0].ToString().Trim();
                                acct_num = col[1].ToString().Trim();
                                med_dt = col[2].ToString().Trim();
                                med_tm = col[3].ToString().Trim();
                                desc = col[4].ToString().Trim();
                                code = itemcode + "_" + desc.Substring(0, 10);
                                //MessageBox.Show("orderid="+orderid+ " code="+code);
                                //MessageBox.Show(desc);
                                if (acct_num != prev_acct_num)
                                {
                                    //VerboseAudit("encid lookup");
                                    GetEncID(acct_num, out encid, out unitid);
                                    //seq = 0;
                                    prev_acct_num = acct_num;
                                }
                                if (encid != 0)
                                {
                                    seq++;
                                    DateTime isodt = g_pull_finish;// PFSUtility.ISOToDateTime(col[2].ToString() + col[3].ToString());
                                    DateTime timestmp = DateTime.Now;

                                    if (hiacumedtype < 0)
                                        isodt = PFSUtility.ISOToDateTime(med_dt + med_tm);
                                    if (hiacumedtype >= 0)
                                    {
                                        if (desc.ToLower().ContainsAny(list1_meds_lower)) hiacumedtype = 1;
                                        if (desc.ToLower().ContainsAny(list2_meds_lower)) hiacumedtype = 2;
                                    }
                                    if (itemcode=="HIACUMED")
                                        medlogfile.WriteLine(line);
                                    if (itemcode == "PSYMED")
                                        psymedlogfile.WriteLine(line);
                                    if (itemcode == "ORALMED")
                                        oralmedlogfile.WriteLine(line);
string q = "INSERT INTO chart_item (encounter_id,event_datetime,code,description,timestamp,sequence,unit_id,order_id,order_control,field_name) select @encid, @evdt, @code, @desc, @ts,@seq,@unit,@oid,@octrl,@medlist where not exists  (select encounter_id,code,event_datetime from chart_item where encounter_id=" + encid.ToString() + " and code='" + code + "' and event_datetime=" + PFSDBUtility.SQLDateTime(isodt) + ")";
                                    VerboseAudit("ciq=" + q);
                                //string q = "INSERT INTO chart_item (encounter_id,event_datetime,code,description,timestamp,sequence,unit_id,order_id) values (@encid, @evdt, @code, @desc, @ts,@seq,@unit,@oid)";
                                    SqlCommand cmd = new SqlCommand(q, db);
                                    cmd.Parameters.AddWithValue("@encid", encid);
                                    cmd.Parameters.AddWithValue("@evdt", isodt);
                                    cmd.Parameters.AddWithValue("@code", code);
                                    cmd.Parameters.AddWithValue("@desc", desc);
                                    cmd.Parameters.AddWithValue("@ts", timestmp);
                                    cmd.Parameters.AddWithValue("@seq", seq);
                                    cmd.Parameters.AddWithValue("@unit", unitid);
                                    cmd.Parameters.AddWithValue("@oid", orderid);
                                    cmd.Parameters.AddWithValue("@octrl", orderctrl);
                                    cmd.Parameters.AddWithValue("@medlist", hiacumedtype);//type of med;list into which med falls;goes into field_name
                                    cmd.ExecuteNonQuery();
                                }
                                else
                                {
                                    if (itemcode == "HIACUMED")
                                        medlogfile.WriteLine("ACCT NOT FOUND:" + line);
                                    if (itemcode == "PSYMED")
                                        psymedlogfile.WriteLine("ACCT NOT FOUND:" + line);
                                    if (itemcode == "ORALMED")
                                        oralmedlogfile.WriteLine("ACCT NOT FOUND:" + line);
                                }
                            }
                        } //while
                        file.Close();
                        File.Delete(pathfn);

                    } //using db
                } //using streamreader
            } //if file exists

        }

        static CHART_ITEM NewChartItem(int encid, DateTime isodt, string code, string desc, short seq, int unitid, string orderid)
        {
            var rec = new CHART_ITEM();
            
            rec.EVENT_DATETIME = isodt;
            rec.ENCOUNTER_ID = encid;
            rec.UNIT_ID = unitid;
            rec.SEQUENCE = seq;                     // needed in case of duplicate codes
            rec.CODE = code;
            rec.DESCRIPTION = desc;
            rec.ORDER_ID = orderid;
            // We should reach out and get the server time, but this should be running on the server anyway
            rec.TIMESTAMP = DateTime.Now;                        // ** should use server time

            return rec;
        }

        static void OpenOutputFiles()
        {
            // Append to existing file
            outfile = new StreamWriter(Path.Combine(_import_path, TRANSP_FILENAME), true);
            if (g_make5am)
                outfile2 = new StreamWriter(Path.Combine(_import_path, TRANSP_FILENAME2), true);
            medlogfile = new StreamWriter(Path.Combine(_import_path, "hiacumed"+ PFSUtility.DateTimeToISODateTime(DateTime.Now)+".txt"), true);
            psymedlogfile = new StreamWriter(Path.Combine(_import_path, "psymed" + PFSUtility.DateTimeToISODateTime(DateTime.Now) + ".txt"), true);
            oralmedlogfile = new StreamWriter(Path.Combine(_import_path, "oralmed" + PFSUtility.DateTimeToISODateTime(DateTime.Now) + ".txt"), true);

            if (g_log)
            {
                // re-write new file
                logfile = new StreamWriter(Path.Combine(_log_path, DateTime.Now.ToString("yyyyMMddHHmm") + TRANSP_AUDIT_LOG));

            }
        }

        static void CloseOutputFiles()
        {
            outfile.Close();
            medlogfile.Close();
            psymedlogfile.Close();
            oralmedlogfile.Close();
            if (g_make5am)
                outfile2.Close();

            if (logfile != null) {
                logfile.Close();
                logfile = null;
            }
        }

        static void SetMapperVersion()
        {
            gLogMapperVersion = MAPPER_VERSION;
        }

        static void ProcessPatients()
        {
            string sql, audit_header;
            int count, prev_enc_id = 0;
            //parent_id = 2817
            //'Prod env:
            //vunit_id = 43729784
            //iunit_id = 43730689
            // Make a list of all patients with chart items during the pull period.
            //
            // Include only units that have the "use transparent" flag set.
            // Limit to one patient if -acct is given.  Limit to one unit with -unit.
            // Left join encounter_location - the patient may be discharged at pull time.
            // Look for the latest patient arrival to the unit before the pull time; ignore transfers within the unit.
            // Look for a patient departure (if any) before the pull time; ignore transfers within the unit.

        sql =
            " SELECT LIST.UNIT_ID, LIST.ENCOUNTER_ID, PARAM.METHODOLOGY_ID,\n" +
            "     UNIT.NAME AS UNIT_NAME, EL.ROOM, EL.BED, EL.DATETIME_IN AS BED_ARRIVAL,\n" +
            "     F.SHORT_NAME,\n" +
//            "     ARRIVE.DATETIME_IN AS UNIT_ARRIVAL, DEPART.DATETIME_OUT AS UNIT_DEPARTURE,\n" +
            "     ARRIVE.DATETIME_IN AS UNIT_ARRIVAL, ARRIVE.DATETIME_OUT AS UNIT_DEPARTURE,\n" +
            "     E.ACCT_NUMBER, P.LAST_NAME, P.FIRST_NAME, P.MIDDLE_NAME,P.LANGUAGE_LVC,P.DOB,\n" +
            "     E.AGE_AT_ADMISSION,\n" +
            "     UNIT.IS_ED, UNIT.IS_ICU,PARAM.START_OF_DAY,PARAM.UNIT_PARAM_ID\n" +
            " FROM (\n" +
            "     SELECT DISTINCT CI.UNIT_ID, CI.ENCOUNTER_ID\n" +
            "     FROM CHART_ITEM AS CI\n" +
            "     INNER JOIN UNIT ON (UNIT.UNIT_ID = CI.UNIT_ID)\n" +
            "     WHERE UNIT.USE_TRANSPARENT_CLASSIFICATION='Y' and (UNIT.IS_ED is null or UNIT.IS_ED <> 'Y')\n" +
            //"     AND EVENT_DATETIME BETWEEN " + PFSDBUtility.SQLDateTime(g_pull_start) + " AND " + PFSDBUtility.SQLDateTime(g_pull_finish) +
            " ) AS LIST" +
            " INNER JOIN UNIT ON (UNIT.UNIT_ID = LIST.UNIT_ID)\n" +
            " INNER JOIN UNIT_PARAM AS PARAM ON (PARAM.UNIT_ID = UNIT.UNIT_ID) AND (" + PFSDBUtility.SQLDateTime(g_pull_finish) + " BETWEEN PARAM.EFFECTIVE_DATETIME AND PARAM.EXPIRATION_DATETIME)\n" +
            " INNER JOIN FACILITY AS F ON (F.FACILITY_ID = UNIT.FACILITY_ID)\n" +
            " INNER JOIN ENCOUNTER AS E ON (E.ENCOUNTER_ID = LIST.ENCOUNTER_ID)\n" +
            " INNER JOIN PERSON AS P ON (P.PERSON_ID = E.PERSON_ID)\n" +
            " LEFT  JOIN ENCOUNTER_LOCATION AS EL ON (EL.ENCOUNTER_ID = E.ENCOUNTER_ID) AND (" + PFSDBUtility.SQLDateTime(g_pull_finish) + " BETWEEN EL.EFFECTIVE_DATETIME_IN AND EL.EFFECTIVE_DATETIME_OUT)\n" +
            " INNER JOIN (\n" +
//            "    SELECT WORKING_UNIT_ID AS UNIT_ID, ENCOUNTER_ID, MAX(DATETIME_IN) AS DATETIME_IN\n" +
            "    SELECT WORKING_UNIT_ID AS UNIT_ID, ENCOUNTER_ID, DATETIME_IN, DATETIME_OUT\n" +
            "    FROM ENCOUNTER_LOCATION AS EL\n" +
            "    WHERE WORKING_UNIT_ID IN (SELECT UNIT_ID FROM UNIT WHERE USE_TRANSPARENT_CLASSIFICATION='Y')\n" +
"    AND DATETIME_IN <= " + PFSDBUtility.SQLDateTime(g_pull_finish) +
"    AND (DATETIME_OUT is null or DATETIME_OUT >= " + PFSDBUtility.SQLDateTime(g_pull_start) + ")\n" +
            //            "    AND IS_TRANSFER_IN='Y'\n" +
            //            "    GROUP BY WORKING_UNIT_ID, ENCOUNTER_ID\n" +
            " ) AS ARRIVE ON (ARRIVE.UNIT_ID = UNIT.UNIT_ID) AND (ARRIVE.ENCOUNTER_ID = E.ENCOUNTER_ID)\n" +
            //" LEFT JOIN (\n" +
            //"    SELECT WORKING_UNIT_ID AS UNIT_ID, ENCOUNTER_ID, MAX(DATETIME_OUT) AS DATETIME_OUT\n" +
            //"    FROM ENCOUNTER_LOCATION AS EL\n" +
            //"    WHERE WORKING_UNIT_ID IN (SELECT UNIT_ID FROM UNIT WHERE USE_TRANSPARENT_CLASSIFICATION='Y')\n" +
            //"    AND DATETIME_OUT < " + PFSDBUtility.SQLDateTime(g_pull_finish) +
            //"    AND ((NEXT_WORKING_UNIT_ID<>WORKING_UNIT_ID) OR (NEXT_WORKING_UNIT_ID IS NULL))\n" +
            //"    GROUP BY WORKING_UNIT_ID, ENCOUNTER_ID\n" +
            //" ) AS DEPART ON (DEPART.UNIT_ID = UNIT.UNIT_ID) AND (DEPART.ENCOUNTER_ID = E.ENCOUNTER_ID)\n" +
            " WHERE (1=1)\n";

            if (!String.IsNullOrEmpty(_these_facs))
            {
                sql += " AND SUBSTRING(F.CLASSIFICATION_FACILITY_CODE,1,1) in (" + _these_facs + ")\n";
            }
            //AND SUBSTRING(F.CLASSIFICATION_FACILITY_CODE, 1, 1) in (2,5)

            if (!String.IsNullOrEmpty(_this_acct)) {
                sql += " AND E.ACCT_NUMBER=" + PFSDBUtility.SQLString(_this_acct) + "\n";
            }
            if (!String.IsNullOrEmpty(_this_unit_name)) {
                sql += " AND UNIT.NAME=" + PFSDBUtility.SQLString(_this_unit_name) + "\n";
            }
            if (_this_unit_id > 0) {
                sql += " AND UNIT.UNIT_ID=" + _this_unit_id + "\n";
            }
            sql += " ORDER BY E.ACCT_NUMBER";
            //sql += " ORDER BY UNIT.NAME, P.LAST_NAME, P.FIRST_NAME, E.ACCT_NUMBER, UNIT_ARRIVAL\n";

            sql =
                " SELECT UNIT.UNIT_ID, EL.ENCOUNTER_ID, PARAM.METHODOLOGY_ID,\n" +
                "     UNIT.NAME AS UNIT_NAME, EL.ROOM, EL.BED, \n" +
                "     F.SHORT_NAME,\n" +
                "     EL.DATETIME_IN AS UNIT_ARRIVAL, EL.DATETIME_OUT AS UNIT_DEPARTURE,\n" +
                "     E.ACCT_NUMBER, P.LAST_NAME, P.FIRST_NAME, P.MIDDLE_NAME,P.LANGUAGE_LVC,P.DOB,\n" +
                "     E.AGE_AT_ADMISSION,\n" +
                "     UNIT.IS_ED, UNIT.IS_ICU,PARAM.START_OF_DAY,PARAM.UNIT_PARAM_ID\n" +
                " FROM ENCOUNTER_LOCATION AS EL\n" +
                " INNER JOIN UNIT ON (UNIT.UNIT_ID = EL.UNIT_ID)\n" +
                " INNER JOIN UNIT_PARAM AS PARAM ON (PARAM.UNIT_ID = UNIT.UNIT_ID) AND (" + PFSDBUtility.SQLDateTime(g_pull_finish) + " BETWEEN PARAM.EFFECTIVE_DATETIME AND PARAM.EXPIRATION_DATETIME)\n" +
                " INNER JOIN FACILITY AS F ON (F.FACILITY_ID = UNIT.FACILITY_ID)\n" +
                " INNER JOIN ENCOUNTER AS E ON (E.ENCOUNTER_ID = EL.ENCOUNTER_ID)\n" +
                " INNER JOIN PERSON AS P ON (P.PERSON_ID = E.PERSON_ID)\n" +
                " WHERE (1=1)" +
            "  AND EL.DATETIME_IN <= " + PFSDBUtility.SQLDateTime(g_pull_finish) +
            "  AND (EL.DATETIME_OUT is null or EL.DATETIME_OUT >= " + PFSDBUtility.SQLDateTime(g_pull_start) + ")\n" +
            "  AND UNIT.USE_TRANSPARENT_CLASSIFICATION='Y'";
            if (!String.IsNullOrEmpty(_these_facs))
            {
                sql += " AND SUBSTRING(F.CLASSIFICATION_FACILITY_CODE,1,1) in (" + _these_facs + ")\n";
            }
            if (!String.IsNullOrEmpty(_this_acct))
            {
                sql += " AND E.ACCT_NUMBER=" + PFSDBUtility.SQLString(_this_acct) + "\n";
            }
            if (!String.IsNullOrEmpty(_this_unit_name))
            {
                sql += " AND UNIT.NAME=" + PFSDBUtility.SQLString(_this_unit_name) + "\n";
            }
            if (_this_unit_id > 0)
            {
                sql += " AND UNIT.UNIT_ID=" + _this_unit_id + "\n";
            }
            sql += " ORDER BY E.ACCT_NUMBER";
            //sql += " ORDER BY UNIT.NAME, P.LAST_NAME, P.FIRST_NAME, E.ACCT_NUMBER, UNIT_ARRIVAL\n";


            VerboseAudit(sql);
            var db = PFSDBUtility.NewSqlConnection();
            var cmd = new SqlCommand(sql, db);
            SqlDataReader dr = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);

            //
            // Process all patients
            //
            count = 0;
            var default_inds = new List<int>();
            //var default_inds_str = "";
            //var default_ptype = 0;
            patloclist = new List<PatientLocation>();

            //   STOP WATCH CODE without stopping a watch:
            //    DateTime startTime, endTime;
            //    startTime = DateTime.Now;
            //loopstart: do stuff
            //endTime = DateTime.Now;
            //Double elapsedMillisecs = ((TimeSpan)(endTime - startTime)).TotalMilliseconds;
            //    if (elapsedMillisecs >= 30000)
            //    {
            //        //   wait 5 secs.
            //        //   startTime=DateTime.Now;
            //    }
            //loopend
            startTime = DateTime.Now;
            while (dr.Read())
            {
                endTime = DateTime.Now;
                Double elapsedMillisecs = ((TimeSpan)(endTime - startTime)).TotalMilliseconds;
                if (elapsedMillisecs >= 30000)
                {
                    VerboseAudit("..........Waiting 5 seconds..........");
                    System.Threading.Thread.Sleep(5000);
                    startTime =DateTime.Now;
                }

                count++;
                Console.Write("\rProcessing patient {0}", count);

                pat = new PatientInfo();
                pat.unit_id = PFSDBUtility.DBToInt(dr["UNIT_ID"]);
                pat.encounter_id = PFSDBUtility.DBToInt(dr["ENCOUNTER_ID"]);
                pat.meth_id = PFSDBUtility.DBToInt(dr["METHODOLOGY_ID"]);
                pat.acct = PFSDBUtility.DBToString(dr["ACCT_NUMBER"]);
                pat.last_name = PFSDBUtility.DBToString(dr["LAST_NAME"]);
                pat.first_name = PFSDBUtility.DBToString(dr["FIRST_NAME"]);
                pat.middle_name = PFSDBUtility.DBToString(dr["MIDDLE_NAME"]);
                pat.lang_lvc=  PFSDBUtility.DBToString(dr["LANGUAGE_LVC"]);
                pat.short_name = PFSDBUtility.DBToString(dr["SHORT_NAME"]);
                VerboseAudit("fac.short name=" + pat.short_name);
                pat.unit_name = PFSDBUtility.DBToString(dr["UNIT_NAME"]);
                pat.room = PFSDBUtility.DBToString(dr["ROOM"]);
                pat.bed = PFSDBUtility.DBToString(dr["BED"]);
                pat.dob= PFSDBUtility.DBToDateTime(dr["DOB"]);
                pat.age = PFSDBUtility.DBToDouble(dr["AGE_AT_ADMISSION"]);
                pat.unit_arrival = PFSDBUtility.DBToDateTime(dr["UNIT_ARRIVAL"]);
                pat.unit_departure = PFSDBUtility.DBToDateTime(dr["UNIT_DEPARTURE"]);
                Program.VerboseAudit("pat.unit_departure=" + pat.unit_departure.ToString() + "  pat.unit_arrival=" + pat.unit_arrival.ToString());
                pat.effective = PFSUtility.MaxDateTime(g_effdt, pat.unit_arrival);
                pat.pull_start = PFSUtility.MaxDateTime(g_pull_start, pat.unit_arrival);
                pat.sod = PFSDBUtility.DBToDateTime(dr["START_OF_DAY"]);
                pat.upid = PFSDBUtility.DBToInt(dr["UNIT_PARAM_ID"]);
                VerboseAudit("pat.pull_start=" + pat.pull_start+ " max of g_pull_start=" + g_pull_start + " and pat.unit_arrival=" + pat.unit_arrival);
                pat.pull_finish = g_pull_finish;
                //pat.pull_finish = PFSUtility.MinDateTime(g_pull_finish, pat.unit_departure);
                if (pat.pull_finish < pat.pull_start)
                {
                    pat.pull_finish = g_pull_finish;
                }
                //pat.range = (int)PFSUtility.DateDiffInMinutes(pat.pull_start, pat.pull_finish);
                //                pat.range = (int)PFSUtility.DateDiffInMinutes(pat.unit_arrival, pat.unit_departure);
                //                pat.range = (int)PFSUtility.DateDiffInMinutes(pat.pull_start, pat.pull_finish);

                //At 1100.Process 0300 - 1100.Time the classification for 0700
                //At 1500.Process 0700 - 1500.Time the classification for 1100
                //At 1900.Process 1100 - 1900.Time the classification for 1500
                //At 2300.Process 1500 - 2300.Time the classification for 1900
                //At 0300.Process 1900 - 0300.Time the classification for 2300
                //At 0700.Process 2300 - 0700.Time the classification for 0300 and duplicate for 0700.  (The 0700 will be over - written for the 1100 run)

//if patient was admitted in this pull range, then get PreArrivalItems
                bool do_class = false;
                bool skip_pt = false;
                if (prev_enc_id != pat.encounter_id)
                {
                    VerboseAudit("prev_enc_id=" + prev_enc_id + "  pat.encounter_id=" + pat.encounter_id);
                    prev_enc_id = pat.encounter_id;
                    skip_pt = false;
                    sttimer();
                    g_pull_start = g_pull_start_save;
                    g_pull_finish = g_pull_finish_save;
                    try
                    {
                        patloclist = GetPatientLocations(pat.encounter_id, g_pull_start, g_pull_finish, out pat.pull_start, out pat.pull_finish);
                        patperioplist = GetPeriopLocations(pat.encounter_id, g_pull_start, g_pull_finish);
                        GetPreArrivalOrders(); //goes into c_i 
                        GetPreArrivalLDAs(); //goes into c_i
                    }
                    catch (Exception e)
                    {
                        skip_pt = true;
                        VerboseAudit("Skipping patient:" + pat.acct + " msg=" + e.Message + " source=" + e.StackTrace);
                        LogUnexpectedError(e.Message, e.StackTrace);
                    }

                    _t1 = entimer("t1=", _t1);
                    if (!skip_pt) { 
                        foreach (var ploc in patloclist)
                        {
                            do_class = false;
                            Audit("unitid=" + ploc.unit_id + " start= " + ploc.in_time + " end=" + ploc.out_time);
                            //if (ploc.out_time < pat.pull_finish) pat.pull_finish = ploc.out_time;
                            //if (ploc.out_time == g_pull_finish || (pat.unit_departure <= g_pull_finish  && pat.unit_departure >= g_pull_finish.AddHours(-4)))
                            {
                                if (ploc.in_time > pat.pull_start) pat.pull_start = ploc.in_time;
                                if (ploc.out_time > pat.pull_finish) pat.pull_finish = ploc.out_time;
                                //Audit("start=" + pat.pull_start + " finish= " + pat.pull_finish);
                                Audit("start=" + ploc.in_time + " finish= " + ploc.out_time);
                                pat.unit_id = ploc.unit_id;
                                pat.unit_arrival = ploc.arr_time;
                                pat.unit_departure = ploc.out_time;
                                //g_pull_finish = ploc.out_time;
                                pat.pull_finish = g_pull_finish;
                                g_pull_start = ploc.in_time;
                                Program.VerboseAudit("pat.unit_departure=" + pat.unit_departure.ToString() + "  g_pull_finish=" + g_pull_finish.ToString());

                                //if (g_pull_finish.Hour == 7)
                                if (true)
                                {
                                    do_class = true;
                                    pat.effective = g_pull_finish.AddMinutes(-g_range);
                                    pat.effective_out = g_pull_finish;
                                    pat.range = g_range;   //4 * 60;

                                    //if (pat.unit_departure == DateTime.MinValue)
                                    //{
                                    //    if (pat.unit_arrival < g_pull_finish.AddHours(-2 * 4))
                                    //    {
                                    //        pat.effective = g_pull_finish.AddHours(-4);
                                    //        pat.effective_out = g_pull_finish;
                                    //        pat.range = g_range;   //4 * 60;
                                    //        Program.VerboseAudit("A: eff=" + pat.effective.ToString() + " effout=" + pat.effective_out.ToString());
                                    //    }
                                    //    else if (pat.unit_arrival < g_pull_finish)
                                    //    {
                                    //        pat.effective = pat.unit_arrival;
                                    //        pat.range = (int)PFSUtility.DateDiffInMinutes(pat.unit_arrival, g_pull_finish);
                                    //        pat.effective_out = g_pull_finish;
                                    //        Program.VerboseAudit("B: eff=" + pat.effective.ToString() + " effout=" + pat.effective_out.ToString());
                                    //    }
                                    //}
                                    //else if (pat.unit_departure >= g_pull_finish.AddHours(-4) && pat.unit_departure <= g_pull_finish)
                                    //{
                                    //    if (pat.unit_arrival < g_pull_finish.AddHours(-2 * 4))
                                    //    {
                                    //        pat.effective = g_pull_finish.AddHours(-4);
                                    //        pat.effective_out = g_pull_finish;
                                    //        pat.range = g_range;   //4 * 60;
                                    //        Program.VerboseAudit("C2: eff=" + pat.effective.ToString() + " effout=" + pat.effective_out.ToString());
                                    //    }
                                    //    else
                                    //    {
                                    //        pat.effective = pat.unit_arrival;
                                    //        pat.range = (int)PFSUtility.DateDiffInMinutes(pat.unit_arrival, pat.unit_departure);
                                    //        pat.effective_out = pat.pull_finish; // pat.unit_departure;
                                    //        Program.VerboseAudit("C: eff=" + pat.effective.ToString() + " effout=" + pat.effective_out.ToString());
                                    //    }
                                    //}
                                    //else if (pat.unit_departure >= g_pull_finish.AddHours(-4) && pat.unit_departure > g_pull_finish)
                                    //{
                                    //    if (pat.unit_arrival < g_pull_finish.AddHours(-2 * 4))
                                    //    {
                                    //        pat.effective = g_pull_finish.AddHours(-4);
                                    //        pat.range = g_range;
                                    //        pat.effective_out = g_pull_finish;
                                    //        Program.VerboseAudit("D2: eff=" + pat.effective.ToString() + " effout=" + pat.effective_out.ToString());
                                    //    }
                                    //    else
                                    //    {
                                    //        pat.effective = pat.unit_arrival;
                                    //        pat.range = (int)PFSUtility.DateDiffInMinutes(pat.unit_arrival, g_pull_finish);
                                    //        pat.effective_out = g_pull_finish;
                                    //        Program.VerboseAudit("D: eff=" + pat.effective.ToString() + " effout=" + pat.effective_out.ToString());

                                    //    }
                                    //}
                                    //else
                                    //{
                                    //    do_class = false;
                                    //    Program.VerboseAudit("X: eff=" + pat.effective.ToString() + " effout=" + pat.effective_out.ToString());
                                    //}
                                }
                                else //not 7am
                                {
                                    if (pat.unit_departure == DateTime.MinValue)
                                    {
                                        Program.VerboseAudit("checkpoint 2");
                                        //don't classify if only on unit LT 2 hours
                                        if (pat.pull_start <= g_pull_finish.AddHours(-2))
                                        {
                                            Program.VerboseAudit("checkpoint 2a");
                                            if (pat.unit_arrival < g_pull_finish.AddHours(-4))
                                            {
                                                Program.VerboseAudit("checkpoint 2b");
                                                if (pat.unit_arrival >= g_pull_finish.AddHours(-8))
                                                {
                                                    pat.effective = pat.unit_arrival;
                                                    g_range = (int)PFSUtility.DateDiffInMinutes(pat.unit_arrival, g_pull_finish);
                                                }
                                                else
                                                {
                                                    pat.effective = g_pull_finish.AddHours(-4);
                                                }
                                                pat.range = g_range;
                                                pat.effective_out = g_pull_finish;
                                                do_class = true;
                                                Program.VerboseAudit("E: eff=" + pat.effective.ToString() + " effout=" + pat.effective_out.ToString());
                                            }
                                            else
                                            {
                                                pat.effective = pat.unit_arrival;
                                                pat.range = g_range; // 4 * 60;
                                                pat.effective_out = g_pull_finish;
                                                do_class = true;
                                                Program.VerboseAudit("F: eff=" + pat.effective.ToString() + " effout=" + pat.effective_out.ToString());
                                            }
                                        }
                                    }
                                    else if (pat.unit_departure >= g_pull_finish.AddHours(-8))
                                    {
                                        Program.VerboseAudit("checkpoint 3");
                                        if (pat.unit_arrival < g_pull_finish.AddHours(-2 * 4))
                                        {
                                            Program.VerboseAudit("checkpoint 3a");
                                            //pat.effective = g_pull_finish.AddHours(-4);
                                            pat.effective = PFSUtility.MaxDateTime(g_pull_finish.AddHours(-4), ploc.in_time);
                                            Program.VerboseAudit("3a: pat.effective=" + pat.effective + "  ploc.in_time=" + ploc.in_time);
                                            //pat.range = (int)PFSUtility.DateDiffInMinutes(g_pull_finish.AddHours(-4), pat.unit_departure);
                                            pat.range = 480;// (int)PFSUtility.DateDiffInMinutes(pat.unit_departure.AddHours(-4), pat.unit_departure);

                                            ////pat.effective = g_pull_finish.AddHours(-4);
                                            //Program.VerboseAudit("G: ploc.in_time=" + ploc.in_time + " ploc.out_time=" + ploc.out_time);
                                            //Program.VerboseAudit("G: g_pull_finish=" + g_pull_finish);

                                            //if (ploc.out_time < g_pull_finish.AddHours(-4))
                                            //    pat.effective = ploc.in_time;
                                            //else
                                            //    pat.effective = PFSUtility.MaxDateTime(g_pull_finish.AddHours(-g_range), ploc.in_time);
                                            ////pat.range = (int)PFSUtility.DateDiffInMinutes(g_pull_finish.AddHours(-4), pat.unit_departure);
                                            //pat.range = (int)PFSUtility.DateDiffInMinutes(pat.unit_departure.AddMinutes(-4), pat.unit_departure);


                                            pat.effective_out = pat.unit_departure;
                                            if (pat.pull_finish == g_pull_finish)
                                            {
                                                do_class = true;
                                                pat.unit_departure = DateTime.MinValue;
                                                pat.effective_out = g_pull_finish;
                                                pat.range = (int)PFSUtility.DateDiffInMinutes(pat.effective, pat.effective_out);
                                                Program.VerboseAudit("Gb: eff=" + pat.effective.ToString() + " effout=" + pat.effective_out.ToString());
                                                if (ploc.out_time <= pat.effective)
                                                {
                                                    do_class = false;
                                                    VerboseAudit("Omitting classification: loc out time is less than effective time.");
                                                }
                                            }
                                            if (pat.unit_departure > pat.effective)
                                            {
                                                do_class = true;
                                                Program.VerboseAudit("G: eff=" + pat.effective.ToString() + " effout=" + pat.effective_out.ToString());
                                            }
                                        }
                                        else if (pat.unit_arrival <= g_pull_finish.AddHours(-2))
                                        {
                                            var tm = TimeSpan.Parse("07:00:00");
                                            //pat.effective = pat.unit_arrival;
                                            pat.effective = PFSUtility.MaxDateTime(pat.unit_arrival, ploc.in_time);
                                            Program.VerboseAudit("H: pat.effective=" + pat.effective.ToString() + " pat.unit_arrival=" + pat.unit_arrival.ToString() + "  ploc.in_time=" + ploc.in_time);
                                            Program.VerboseAudit("H: pat.pull_finish=" + pat.pull_finish.ToString() + " g_pull_finish=" + g_pull_finish.ToString());
                                            pat.range = (int)PFSUtility.DateDiffInMinutes(pat.unit_arrival, pat.unit_departure);
                                            pat.effective_out = pat.unit_departure;
                                            Program.VerboseAudit("H: pat.effective_out=" + pat.effective_out.ToString());
                                            if (pat.pull_finish == g_pull_finish)
                                            {
                                                Program.VerboseAudit("H1: pat.effective_out=" + pat.effective_out.ToString());
                                                Program.VerboseAudit("H1: pat.unit_departure=" + pat.unit_departure.ToString() + "  g_pull_finish=" + g_pull_finish.ToString());
                                                Program.VerboseAudit("H1: pat.range=" + pat.range);
                                                pat.unit_departure = DateTime.MinValue;
                                                pat.effective_out = g_pull_finish;
                                                if (pat.effective.Date == pat.effective_out.Date && pat.effective.Hour < 7 && pat.effective_out.Hour >= 7)
                                                    pat.effective = pat.effective.Date + tm;
                                                pat.range = (int)PFSUtility.DateDiffInMinutes(pat.effective, pat.effective_out);
                                                Program.VerboseAudit("H2: pat.effective_out=" + pat.effective_out.ToString());
                                                Program.VerboseAudit("H2: pat.unit_departure=" + pat.unit_departure.ToString());
                                                Program.VerboseAudit("H2: pat.range=" + pat.range);
                                            }
                                            do_class = true;
                                            Program.VerboseAudit("H: eff=" + pat.effective.ToString() + " effout=" + pat.effective_out.ToString());
                                        }

                                    }

                                }

                                pat.los_hours = (int)PFSUtility.DateDiffInMinutes(pat.pull_start, pat.pull_finish) / 60.0;//pat.range / 60.0;
                                pat.TC_source_id = 5399; // PFSDBUtility.DBToInt(dr["TC_SOURCE_ID"]);        //TCP port
                                pat.is_ED = PFSDBUtility.DBToBool(dr["IS_ED"]);
                                pat.is_ICU = PFSDBUtility.DBToBool(dr["IS_ICU"]);
                                // Package the patient info
                                // NOTE: ''dr["FIRST_NAME"] as string'' looks great but it will save nulls.
                                //       use ''PFSDBUtility.DBToString(dr["FIRST_NAME"])'' to convert to empty strings.

                                // Get the list of default indicators on admission (if any)
                                //if (pat.unit_id != prev_unit_id)
                                //{
                                //    default_inds = GetUnitDefaultIndicators(pat.unit_id, g_pull_start, out default_inds_str, out default_ptype);
                                //    prev_unit_id = pat.unit_id;
                                //}
                                //pat.default_inds = default_inds;
                                //pat.default_inds_str = default_inds_str;
                                //pat.default_ptype = default_ptype;

                                // Get ready for event log entries for this patient
                                gLogUnitID = pat.unit_id;
                                gLogEncounterID = pat.encounter_id;
                                gLogSourceText = "";

                                // Reset both audit strings and make headers for both
                                gBriefAudit = new StringBuilder();
                                gVerboseAudit = new StringBuilder();
                                audit_header =
                                    new String('=', 80) + Environment.NewLine +
                                    "TRANSPARENT MAPPING AUDIT FILE                    Run Time=" + DateTime.Now + Environment.NewLine +
                                    "Version:   " + gLogMapperVersion + Environment.NewLine +
                                    "Pull:      " + g_pull_finish + Environment.NewLine +
                                    "Effective: " + g_effdt + Environment.NewLine +
                                    "Range:     " + g_range + " (" + Math.Round(g_range / 60.0, 1) + " hrs) starting " + g_pull_start;

                                // This will add to both brief and verbose audits
                                Audit(audit_header);
                                Audit(new String('=', 80));
                                Audit(count + ": Patient " + pat.last_name + ", " + pat.first_name + " " + pat.middle_name +
                                    " in " + ploc.unit_name + " " + ploc.room + " " + ploc.bed +
                                    " acct=" + pat.acct + " age=" + pat.age);
                                //                Audit("Here from " + pat.effective + " to " + pat.effective.AddMinutes(pat.range) +

                                //Audit("Here from " + pat.effective + " to " + pat.effective_out +
                                //    "  (.LOS=" + Math.Round(pat.los_hours, 2) + ")");
                                //VerboseAudit("pat.pull_start=" + pat.pull_start.ToString() + " pat.pull_finish=" + pat.pull_finish.ToString());
                                Audit("Here from " + ploc.in_time + " to " + ploc.out_time +
                                    "  (.LOS=" + Math.Round(ploc.los_mins / 60.0, 2) + ")");
                                //VerboseAudit("pat.pull_start=" + pat.pull_start.ToString() + " pat.pull_finish=" + pat.pull_finish.ToString());

                                //Audit("Here from " + pat.pull_start + " to " + pat.pull_finish +
                                //    "  (LOS=" + Math.Round(pat.los_hours, 2) + ")");
                                //} // prev_enc_id != pat.encounter_id

                                //if (ploc.special_unit_id < 0 && ploc.is_last_loc)
                                //{
                                //    do_class = false;
                                //    VerboseAudit("Omitting classification for this location: is special+is current.");
                                //}
                                if (ploc.out_time < g_effdt && g_effdt.Hour != 7)
                                {
                                    do_class = false;
                                    VerboseAudit("Omitting classification due to departure prior to class time: out=" + ploc.out_time + " desired class time=" + g_effdt);
                                }
                                if (do_class)
                                {
                                    pat.unit_name = ploc.unit_name;
                                    pat.meth_id = ploc.methid;
                                    pat.loc_idx = ploc.loc_idx;
                                    try
                                    {
                                        // Run the appropriate mapping for this unit and date
                                        //
                                        switch (ploc.methid)
                                        {
                                            case 18:
                                            case 20:
                                                var ev = new Inpatient();
                                                ev.ProcessPatient(pat);
                                                break;
                                            case 26:
                                                if (g_do_MH && !g_onlydnc)
                                                {
                                                    var emh = new MentalHealth();
                                                    emh.ProcessPatient(pat);
                                                }
                                                break;
                                            default:
                                                LogWarning("Methodology " + pat.meth_id + " in unit " + pat.unit_name + " is not supported");
                                                break;
                                        }

                                        gLogUnitID = 0;
                                        gLogEncounterID = 0;
                                    }
                                    catch (Exception e)
                                    {
                                        LogUnexpectedError(e.Message, e.StackTrace);
                                        // Stop or keep going?
                                        if (Program.g_stop_on_error) g_abort = true;
                                    }
                                }
                                if (g_abort) break;
                            }
                        } //foreach
                    }//if !skip pt
                }
            }

            Console.WriteLine();
            //dr.Close();
            db.Close();
            
            if (count < 1) {
                Audit("");
                if(!String.IsNullOrEmpty(_this_acct)) {
                    LogWarning("The selected patient has no chart items in the given time range");
                } else {
                    LogWarning("No chart items found to process - have the unit(s) been enabled for transparent classification?");
                }
            }   
        }

        static void GetEncID(string acctnum,out int encid,out int unitid)
        {
            encid = 0;
            unitid = 0;
            string sql = "";
            sql = "select encounter_id,working_unit_id from encounter where acct_number='" + acctnum + "'";
            var db = PFSDBUtility.NewSqlConnection();
            var cmd = new SqlCommand(sql, db);
            SqlDataReader dr = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr.Read())
            {
                encid = PFSDBUtility.DBToInt(dr["ENCOUNTER_ID"]);
                unitid = PFSDBUtility.DBToInt(dr["WORKING_UNIT_ID"]);
            }
            //dr.Close();
            db.Close();
        }

        static List<PatientLocation> GetPatientLocations(int encid, DateTime start_dt, DateTime end_dt, out DateTime patstart, out DateTime patfinish)
        {
            var result = new List<PatientLocation>();                   // make an empty list
            //var db = PFSDBUtility.NewPfsDataContext();        // look for the unit default admission profile

            string sql="";
            sql = "select el.unit_id,el.effective_datetime_in,el.effective_datetime_out,el.room,el.bed,";
            sql += "case when el.special_unit_id is null then 0 else el.special_unit_id end as special_unit_id,u.name,up.methodology_id from encounter_location as el";
            sql += " inner join unit as u on (el.unit_id = u.unit_id)";
            sql += " inner join unit_param as up on (u.unit_id = up.unit_id)";
            sql += " where el.encounter_id=" + encid;
            //sql += " and (el.special_unit_id is null or (el.special_unit_id is not null))";// and el.special_unit_id<>-6))";
            sql += " and getdate() between up.EFFECTIVE_DATETIME and up.EXPIRATION_DATETIME";
            sql += " and ( ('" + start_dt.ToString() + "' between el.effective_datetime_in and el.effective_datetime_out)";
            sql += " or ('" + end_dt.ToString() + "' between el.effective_datetime_in and el.effective_datetime_out)";
            sql += " or (('" + start_dt.ToString() + "'<el.effective_datetime_in) and ('" + end_dt.ToString() + "'>=el.effective_datetime_out)) )";
            sql += " order by el.effective_datetime_in";
            VerboseAudit("q1:"+sql);
            VerboseAudit("start_dt=" + start_dt + "  end_dt=" + end_dt);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd = new SqlCommand(sql, db2);
            cmd.CommandTimeout = 180;
            using (SqlDataReader dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection))
            {
                while (dr2.Read())
                {
                    var ptloc = new PatientLocation();
                    ptloc.unit_id = PFSDBUtility.DBToInt(dr2["UNIT_ID"]);
                    ptloc.unit_name = PFSDBUtility.DBToString(dr2["NAME"]);
                    ptloc.room = PFSDBUtility.DBToString(dr2["ROOM"]);
                    ptloc.bed = PFSDBUtility.DBToString(dr2["BED"]);
                    ptloc.special_unit_id = PFSDBUtility.DBToInt(dr2["SPECIAL_UNIT_ID"]);
                    if (dr2["METHODOLOGY_ID"] == null)
                        ptloc.methid = 0;
                    else
                        ptloc.methid = PFSDBUtility.DBToInt(dr2["METHODOLOGY_ID"]);
                    DateTime effin = PFSDBUtility.DBToDateTime(dr2["EFFECTIVE_DATETIME_IN"]);
                    DateTime effout = PFSDBUtility.DBToDateTime(dr2["EFFECTIVE_DATETIME_OUT"]);

                    VerboseAudit("effin=" + effin + "  effout=" + effout);

                    if (effin <= start_dt)
                        ptloc.los_start = start_dt;
                    else
                        ptloc.los_start = effin;

                    if (effout <= end_dt)
                        ptloc.los_end = effout;
                    else
                        ptloc.los_end = end_dt;



                    if ((start_dt >= effin) && (start_dt < effout))
                    {
                        ptloc.in_time = start_dt;
                        ptloc.loc_start = effin;
                    }
                    else
                    {
                        ptloc.in_time = effin;
                        ptloc.loc_start = effin;
                    }
                    if ((end_dt >= effin) && (end_dt <= effout))
                    {
                        ptloc.out_time = end_dt;
                    }
                    else
                    {
                        ptloc.out_time = effout;
                        if (ptloc.out_time > end_dt) ptloc.out_time = end_dt;
                    }
                    if (ptloc.methid == 20 || ptloc.methid == 26)
                    {
                        Audit("loc " + ptloc.unit_name + "  in= " + ptloc.in_time + "  out= " + ptloc.out_time + "  meth=" + ptloc.methid);
                        result.Add(ptloc);
                    }
                }
            }//using
        //dr2.Close();
        db2.Close();


            PatientLocation[] locary = result.ToArray();
            int num_locs = 0;
            //  where there are adjacent same-locations, mark the duplicates as remove.
//            VerboseAudit("locary1: ub=" + locary.GetUpperBound(0));
            for (int i = 0; i <= locary.GetUpperBound(0); i++)
            {
                num_locs++;
                locary[i].remove = false;
                if (i >= 1)
                {
                    if ((locary[i - 1].unit_id == locary[i].unit_id)
                        //&& (locary[i-1].special_unit_id != -6)
                        //&& (locary[i].special_unit_id != -6)
                        && (locary[i - 1].out_time == locary[i].in_time))
                        //&& (locary[i - 1].service == locary[i].service))
                    {
                        locary[i].remove = true;
                    }
                }
            }
            //  where there are several adjacent same-unit locations, need to combine them into 1 loc record.
            //  make the outdt of that record be the latest outdt
            int lastgoodidx = 0;
            int lastadj = 0;
            for (int i = 0; i <= locary.GetUpperBound(0); i++)
            {
                if (!locary[i].remove)
                {
                    lastgoodidx = i;
                    lastadj = i;
                }
                else
                {
                    if (lastadj == i - 1)
                    {
                        locary[lastgoodidx].out_time = locary[i].out_time;
                        lastadj = i;
                    }
                }
            }
            // get rid of the removes
            int numkeep = 0;
            for (int i = 0; i <= locary.GetUpperBound(0); i++)
            {
                //Audit("i=" + i + " remove=" + (locary[i].remove ? "true" : "false") + " unit_id=" + locary[i].unit_id + " intime=" + locary[i].in_time);
                if (!locary[i].remove)
                {
                    numkeep++;
                    locary[numkeep - 1] = locary[i]; //-1 for 0-based ary                
                }
            }
  //          VerboseAudit("locary1a: numkeep=" + numkeep + " num_locs=" + num_locs);
            if (numkeep < num_locs)
            {
                Array.Resize(ref locary, numkeep);
            }
            //now determine los_mins
            for (int i = 0; i <= locary.GetUpperBound(0); i++)
            {
                locary[i].los_mins = (int)PFSUtility.DateDiffInMinutes(locary[i].in_time, locary[i].out_time);
            }

            // Now Get Unit Arrival Times
            patstart = g_pull_start;
            patfinish = g_pull_finish;
    //        VerboseAudit("locary2: ub=" + locary.GetUpperBound(0));
            for (int i = 0; i <= locary.GetUpperBound(0); i++)
            {
                locary[i].is_last_loc = false;
                locary[i].loc_idx = i;
                VerboseAudit("i:" + i + " unitid=" + locary[i].unit_id + " locintime="+ locary[i].in_time);
                locary[i].arr_time = GetArrivalTime(encid, locary[i].unit_id, locary[i].in_time);
                if (i == 0) patstart = locary[i].in_time;
                if (i == locary.GetUpperBound(0))
                {
                    VerboseAudit("Set patfinish to " + locary[i].out_time);
                    patfinish = locary[i].out_time;
                }
                if (i == locary.GetUpperBound(0)) locary[i].is_last_loc = true;
                Audit("i=" + i + " remove=" + (locary[i].remove ? "true" : "false") + " unit_id=" + locary[i].unit_id + " intime=" + locary[i].in_time + " outtime=" + locary[i].out_time + " specialunitid="+locary[i].special_unit_id);
            }
            result = locary.ToList();
            return result;
        }

        static List<PatientLocation> GetPeriopLocations(int encid, DateTime start_dt, DateTime end_dt)
        {
            var result = new List<PatientLocation>();                   // make an empty list
            //var db = PFSDBUtility.NewPfsDataContext();        // look for the unit default admission profile

            string sql = "";
            sql = "select el.unit_id,el.effective_datetime_in,el.effective_datetime_out,u.name from encounter_location as el";
            sql += " inner join unit as u on(el.unit_id = u.unit_id)";
            sql += " where el.special_unit_id in (-1,-5,-6) and el.encounter_id=" + encid;
            sql += " and ( (('" + start_dt.ToString() + "'>=el.effective_datetime_in) and ('" + start_dt.ToString() + "'<=el.effective_datetime_out))";
            sql += " or (('" + end_dt.ToString() + "'>=el.effective_datetime_in) and ('" + end_dt.ToString() + "'<=el.effective_datetime_out))";
            sql += " or (('" + start_dt.ToString() + "'<el.effective_datetime_in) and ('" + end_dt.ToString() + "'>=el.effective_datetime_out)) )";
            sql += " order by el.effective_datetime_in";
            VerboseAudit("q2:"+sql);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd = new SqlCommand(sql, db2);
            cmd.CommandTimeout = 180;
            SqlDataReader dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr2.Read())
            {
                var ptloc = new PatientLocation();
                ptloc.unit_id = PFSDBUtility.DBToInt(dr2["UNIT_ID"]);
                ptloc.unit_name = PFSDBUtility.DBToString(dr2["NAME"]);
                DateTime effin = PFSDBUtility.DBToDateTime(dr2["EFFECTIVE_DATETIME_IN"]);
                DateTime effout = PFSDBUtility.DBToDateTime(dr2["EFFECTIVE_DATETIME_OUT"]);

                if ((start_dt >= effin) && (start_dt < effout))
                {
                    ptloc.in_time = start_dt;
                    ptloc.loc_start = effin;
                }
                else
                {
                    ptloc.in_time = effin;
                    ptloc.loc_start = effin;
                }
                if ((end_dt >= effin) && (end_dt <= effout))
                {
                    ptloc.out_time = end_dt;
                }
                else
                {
                    ptloc.out_time = effout;
                    if (ptloc.out_time > end_dt) ptloc.out_time = end_dt;
                }
                Audit("PERIOP loc " + ptloc.unit_name + "  in= " + ptloc.in_time + "  out= " + ptloc.out_time);
                result.Add(ptloc);
            }

            //dr2.Close();
            db2.Close();
            PatientLocation[] locary = result.ToArray();
            result = locary.ToList();
            return result;
        }


        static DateTime GetArrivalTime(int encid, int unitid, DateTime intime)
        {
            string sql = "";
            DateTime arrtime = DateTime.MinValue;
            sql = "select effective_datetime_in,effective_datetime_out from encounter_location";
            sql += " where encounter_id=" + encid + " and working_unit_id=" + unitid;
            sql += " and effective_datetime_in<='" + intime.ToString() + "'";
            sql += " order by effective_datetime_in";
            VerboseAudit("qa:" + sql);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd = new SqlCommand(sql, db2);
            bool first = true;
            DateTime prev_in = DateTime.MinValue;
            DateTime prev_out = DateTime.MinValue;
            DateTime effin = DateTime.MinValue;
            DateTime effout = DateTime.MinValue;
            cmd.CommandTimeout = 180;
            using (SqlDataReader dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection))
            {
                while (dr2.Read())
                {
                    effin = PFSDBUtility.DBToDateTime(dr2["EFFECTIVE_DATETIME_IN"]);
                    effout = PFSDBUtility.DBToDateTime(dr2["EFFECTIVE_DATETIME_OUT"]);
                    if (first)
                    {
                        first = false;
                        prev_in = effin;
                        prev_out = effout;
                        arrtime = prev_in;
                    }
                    else
                    {
                        if (prev_out != effin)
                        {
                            arrtime = effin;
                        }
                        prev_in = effin;
                        prev_out = effout;
                    }
                }
            }//using
            db2.Close();
            return arrtime;

            //DateTime arrtime = DateTime.MinValue;
            //var db = PFSDBUtility.NewPfsDataContext();        // look for the unit default admission profile
            //var query = from loc in db.ENCOUNTER_LOCATIONs
            //            where (loc.UNIT_ID == unitid)
            //            && (loc.ENCOUNTER_ID == encid)
            //            && (loc.EFFECTIVE_DATETIME_IN <= intime)
            //            orderby loc.EFFECTIVE_DATETIME_IN
            //            select new
            //            {
            //                loc.UNIT_ID,
            //                loc.EFFECTIVE_DATETIME_IN,
            //                loc.EFFECTIVE_DATETIME_OUT
            //            };

            //// example: unit a is the target, but the query returned b as the first unit
            //// loc b
            ////     a
            ////     a
            ////     c
            //bool first = true;
            //DateTime prev_in = DateTime.MinValue;
            //DateTime prev_out = DateTime.MinValue;
            //foreach (var locitem in query)
            //{
            //    if (first)
            //    {
            //        first = false;
            //        prev_in = (DateTime)locitem.EFFECTIVE_DATETIME_IN;
            //        prev_out = (DateTime)locitem.EFFECTIVE_DATETIME_OUT;
            //        arrtime = (DateTime)locitem.EFFECTIVE_DATETIME_IN;
            //    }
            //    else
            //    {
            //        if (prev_out != (DateTime)locitem.EFFECTIVE_DATETIME_IN)
            //        {
            //            arrtime = (DateTime)locitem.EFFECTIVE_DATETIME_IN;
            //        }
            //        prev_in = (DateTime)locitem.EFFECTIVE_DATETIME_IN;
            //        prev_out = (DateTime)locitem.EFFECTIVE_DATETIME_OUT;
            //    }
            //}
            //return arrtime;
        }


            //MSH|^~\&|EPIC_ORM|PHS|AcuityPlus||202003121803||ORM^O01|75606583|P|2.3
            //PID|1||10110124^^^NSMC^MRN|10110124^^^NSMC^MRN|O'LEARY^ERIN^M||19820617|F|O'LEARY^ERIN^^~OLEARY^ERIN^^~OLEARY^ERIN^M^||5 ROCKDALE AVENUE^^LYNN^MA^01905|ESSEX|(781)309-8762|||Married/CU||3299133073
            //ORC|CA|660502650^EPC||3299133073|||^^^20200311023800^^S^^
            //OBR|1|660502650^EPC||3040000025001^STRICT ISOLATION STATUS^APEAP^^STRICT ISOLATION||202003110238|20200312180331
            //NTE|1||Strict isolation is a combination of Airborne, Contact and Eye protection.|


            static void GetPreArrivalOrders()
        {
            string sql;
            string room;
            string dt;
            int car;
            DateTime minlocdt = DateTime.MaxValue;

            sql = "select min(effective_datetime_in) as minloctime from encounter_location where encounter_id=" + pat.encounter_id;
            Program.VerboseAudit(sql);
            var db4 = PFSDBUtility.NewSqlConnection();
            var cmd4 = new SqlCommand(sql, db4);
            cmd4.CommandTimeout = 180;
            SqlDataReader dr4 = cmd4.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            //var pfs4 = PFSDBUtility.NewPfsDataContext();
            while (dr4.Read())
            {
                if (dr4["minloctime"] != null)
                {
                    minlocdt = PFSDBUtility.DBToDateTime(dr4["minloctime"]);
                }
            }
            //dr4.Close();
            db4.Close();
            if (minlocdt.AddHours(g_prearrival_range) < pat.pull_start)
            { // this run/pullstart is more than 24 hours after the first location--
              //--in other words, beyond the window of time where there could be 
              // anything received pre-arrival.
                arrival_within24hrs_minlocdt = DateTime.MinValue;
                return;
            }
            arrival_within24hrs_minlocdt = minlocdt;
            VerboseAudit("min location time=" + minlocdt + "  current run=" + pat.pull_start + "  timestart=" + DateTime.Now);

            //  Gets and saves all Chart Items that were sent pre-arrival before the A04.
            //  These are in the form of REJECTED R01 messages (rejected because pt not yet registered).
            //  Up to 2 hours before A04. 
            // REJECTED event log message=(source,type,category)=(1,4,4)

            //            MSH |^ ~\&| EPIC_ORM | PHS | AcuityPlus || 202003121803 || ORM ^ O01 | 75606583 | P | 2.3
            //PID | 1 || 10110124 ^^^ NSMC ^ MRN | 10110124 ^^^ NSMC ^ MRN | O'LEARY^ERIN^M||19820617|F|O'LEARY ^ ERIN ^^ ~OLEARY ^ ERIN ^^ ~OLEARY ^ ERIN ^ M ^|| 5 ROCKDALE AVENUE^^ LYNN ^ MA ^ 01905 | ESSEX | (781)309 - 8762 ||| Married / CU || 3299133073
            //ORC | CA | 660502650 ^ EPC || 3299133073 |||^^^ 20200311023800 ^^ S ^^
            //OBR | 1 | 660502650 ^ EPC || 3040000025001 ^ STRICT ISOLATION STATUS^ APEAP ^^ STRICT ISOLATION || 202003110238 | 20200312180331
            //NTE | 1 || Strict isolation is a combination of Airborne, Contact and Eye protection.|
            DateTime dtminus48 = minlocdt;
            if (minlocdt >= DateTime.MinValue.AddHours(48))
                dtminus48 = minlocdt.AddHours(-48);
            else
                dtminus48 = DateTime.MinValue;
            sql = "select count(*)";
//            sql += " from EVENT_LOG where TIMESTAMP between dateadd(hour,-" + g_prearrival_range + "," + PFSDBUtility.SQLDateTime(minlocdt) + ") and " + PFSDBUtility.SQLDateTime(minlocdt);
            sql += " from EVENT_LOG where TIMESTAMP between " + PFSDBUtility.SQLDateTime(dtminus48) + " and " + PFSDBUtility.SQLDateTime(minlocdt);
            sql += " and (description like 'O01%" + pat.acct + "%')";
            sql += " and event_source=1 and event_type=4 and event_category=4";
            Program.VerboseAudit(sql);
            var db1 = PFSDBUtility.NewSqlConnection();
            var cmd1 = new SqlCommand(sql, db1);
            cmd1.CommandTimeout = 180;
            SqlDataReader dr1 = cmd1.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            bool prearr_exists = false;
            int ct = 0;
            while (dr1.Read())
            {
                ct = PFSDBUtility.DBToInt(dr1[0]);
            }
            db1.Close();
            prearr_exists = (ct > 0);
            Program.VerboseAudit("count(*)="+ct);
            if (!prearr_exists) return;

            sql = "select timestamp,source_text";
            //sql += " case when CHARINDEX('ORC|NW', source_text) > 0 or CHARINDEX('ORC|CA', source_text) > 0 then";
            //sql += " substring(source_text, CHARINDEX('ORC|', source_text), 32) else null end as ORC,";
            //sql += " case when CHARINDEX('OBR|', source_text) > 0 then";
            //sql += " substring(source_text, CHARINDEX('OBR|', source_text), 64) else null end as OBR";
            sql += " from EVENT_LOG where TIMESTAMP between dateadd(hour,-"+ g_prearrival_range + "," + PFSDBUtility.SQLDateTime(minlocdt) + ") and " + PFSDBUtility.SQLDateTime(minlocdt);
            sql += " and (description like 'O01%" + pat.acct + "%')";
            sql += " and event_source=1 and event_type=4 and event_category=4";
            sql += " order by timestamp";
            Program.VerboseAudit(sql);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd2 = new SqlCommand(sql, db2);
            cmd2.CommandTimeout = 180;
            SqlDataReader dr2 = cmd2.ExecuteReader(System.Data.CommandBehavior.CloseConnection);

            string orcstr , obrstr, timestr, srctxt;
            DateTime evdt;
            int seq = 50;

            List<EventLog_Data> eventlog_list = new List<EventLog_Data>();   // make an empty list
            EventLog_Data evrec;

            while (dr2.Read())
            {
                srctxt = PFSDBUtility.DBToString(dr2["SOURCE_TEXT"]);
                DateTime timestmp = PFSDBUtility.DBToDateTime(dr2["timestamp"]);
                evrec.timestamp = timestmp;
                evrec.srctxt = srctxt;
                eventlog_list.Add(evrec);
            }
            db2.Close();

            foreach (var e in eventlog_list)
            {
                VerboseAudit("e.srctxt=" + e.srctxt.Substring(0,80) + " timestamp=" + e.timestamp);

                int orcpos = e.srctxt.IndexOf("ORC|NW") + e.srctxt.IndexOf("ORC|CA");
                int obrpos = e.srctxt.IndexOf("OBR|");
                orcstr = "";
                obrstr = "";
                if (orcpos > 0)
                    orcstr = e.srctxt.Substring(orcpos, 32);
                if (obrpos > 0)
                    obrstr = e.srctxt.Substring(obrpos, 64);
                //orcstr = PFSDBUtility.DBToString(dr2["ORC"]);
                //obrstr = PFSDBUtility.DBToString(dr2["OBR"]);
                if (orcstr.Trim() != "" && orcstr != null)
                {
                    //DateTime timestmp = PFSDBUtility.DBToDateTime(dr2["timestamp"]);
                    timestr = e.timestamp.ToString("yyyyMMddHHmm");
                    //VerboseAudit("timestmp=" + timestr);

                    //ORC|NW|669769268^EPC||3290947318
                    //OBR|1|669769268^EPC||40517^DIET^APEAP^^DIET||202004180752|202004 
                    //OBX|1|DT|3042394421&2714^Placement Date-Urinary Catheter Temperature probe 16 Fr.^ACUITYFDC||20200616||||||F|||202006160041||13003^PELLETIER^EMILY^A.R.N.^
                    //OBX|2|TM|3042394422&2714^Placement Time-Urinary Catheter Temperature probe 16 Fr.^ACUITYFDC||00400000||||||F|||202006160041||13003^PELLETIER^EMILY^A.R.N.^
                    //OBX | 3 | ST | 304239316 & 2714 ^ Catheter Type - Urinary Catheter Temperature probe 16 Fr.^ ACUITYFDC || Temperature probe |||||| F ||| 202006160041 || 13003 ^ PELLETIER ^ EMILY ^ A.R.N.^

                    VerboseAudit("Pre-arrival order ORC: " + orcstr);
                    VerboseAudit("Pre-arrival order OBR: " + obrstr);
                    var arrorc = SplitOnPipeAndPrepareElements(orcstr);
                    var arrobr = SplitOnPipeAndPrepareElements(obrstr);
                    string ordid = "";
                    string ordctrl = "";
                    string ordcode = "";
                    string orddesc = "";

                    ordctrl = arrorc[1];
                    VerboseAudit("ordctrl: " + ordctrl);
                    ordid = arrorc[2];
                    //VerboseAudit("ordid: " + ordid);
                    int ordidpos = ordid.IndexOf("^");
                    if (ordidpos > 0) ordid = ordid.Substring(0, ordidpos);
                    //VerboseAudit("ordidfinal: " + ordid);

                    ordcode = arrobr[4];
                    //VerboseAudit("ordcode: " + ordcode);
                    int ordcodepos = ordcode.IndexOf("^");
                    if (ordcodepos > 0)
                    {
                        orddesc = ordcode.Substring(ordcodepos + 1);
                        ordcode = ordcode.Substring(0, ordcodepos);
                    }
                    //VerboseAudit("ordcodefinal: " + ordcode);

                    //VerboseAudit("Adding: orderid=" + ordid + " ctrl=" + ordctrl + " code=" + ordcode + " evdt=" + timestr + " desc=" + orddesc);

                    if (arrorc.GetUpperBound(0) >= 2)
                    {
                        using (var db = PFSDBUtility.NewSqlConnection())
                        {
                            seq++;
                            evdt = PFSUtility.ISOToDateTime(timestr);
                            VerboseAudit("Evdt=" + evdt.ToString());
                            string q = "INSERT INTO chart_item (encounter_id,event_datetime,code,description,timestamp,sequence,unit_id,order_id,order_control)";
                            q += " select @encid, @evdt, @code, @desc, @ts, @seq, @unit, @oid, @octrl";
                            //q += " where not exists (select encounter_id,code,event_datetime,unit_id,sequence from chart_item";
                            //q += " where encounter_id=" + pat.encounter_id.ToString() + " and code='" + ordcode + "' and event_datetime='" + evdt.ToString() + "' and unit_id=-1 and sequence=" + seq + ")";
                            q += " where not exists (select encounter_id,code,event_datetime from chart_item";
                            q += " where encounter_id=" + pat.encounter_id.ToString() + " and code='" + ordcode + "' and event_datetime='" + evdt.ToString() + "' and sequence=" + seq + ")";
                            VerboseAudit("qpaord=" + q);
                            //string q = "INSERT INTO chart_item (encounter_id,event_datetime,code,description,timestamp,sequence,unit_id,order_id) values (@encid, @evdt, @code, @desc, @ts,@seq,@unit,@oid)";
                            SqlCommand cmd = new SqlCommand(q, db);
                            cmd.Parameters.AddWithValue("@encid", pat.encounter_id);
                            cmd.Parameters.AddWithValue("@evdt", evdt);
                            cmd.Parameters.AddWithValue("@code", ordcode);
                            cmd.Parameters.AddWithValue("@desc", orddesc);
                            cmd.Parameters.AddWithValue("@ts", e.timestamp);// DateTime.Now);
                            cmd.Parameters.AddWithValue("@seq", seq);
                            cmd.Parameters.AddWithValue("@unit", -1);
                            cmd.Parameters.AddWithValue("@oid", ordid);
                            cmd.Parameters.AddWithValue("@octrl", ordctrl);
                            cmd.ExecuteNonQuery();
                        }

                    }
                }
                //_descript = "";
                //if (dr2["OBX1"] != null)
                //{
                //    // ci.timestamp = PFSDBUtility.DBToDateTime(dr2["TIMESTAMP"]);
                //    // ci.event_datetime=ci.timestamp;
                //    _fndcode = PFSDBUtility.DBToString(dr2["OBX1"]);
                //    car = _fndcode.IndexOf('^', 0);
                //    if (car > 0)
                //    {
                //        _fndcode = _fndcode.Left(car);
                //        _result = PFSDBUtility.DBToString(dr2["OBX1_5"]);
                //        _descript = _fndcode.Substring(car);
                //        SaveChartItem(pfs);
                //    }
                //    // ci.sequence=seq++;
                //}
                //if (dr2["OBX2"] != null)
                //{
                //    // ci.timestamp = PFSDBUtility.DBToDateTime(dr2["TIMESTAMP"]);
                //    // ci.event_datetime=ci.timestamp;
                //    _fndcode = PFSDBUtility.DBToString(dr2["OBX2"]);
                //    car = _fndcode.IndexOf('^', 0);
                //    if (car > 0)
                //    {
                //        _fndcode = _fndcode.Left(car);
                //        _result = PFSDBUtility.DBToString(dr2["OBX2_5"]);
                //        _descript = _fndcode.Substring(car);
                //        SaveChartItem(pfs);
                //    }
                //    // ci.sequence=seq++;
                //}
            } //dr read
            //dr2.Close();
            VerboseAudit("timefinish=" + DateTime.Now);
            return;
        }

        static void GetPreArrivalLDAs()
        {
            
            string obx1str, obx2str, obx3str, obx4str;
            const int SRC_LEN_GRAB = 140;  //chars to grab 

            if (arrival_within24hrs_minlocdt == DateTime.MinValue) return;
            //select timestamp,
            //case when CHARINDEX('OBX|4', source_text) > 0 then
            //substring(source_text, CHARINDEX('OBX|4', source_text), 130) else null end as OBX1

            //from event_log where event_source = 1 and event_type = 4 and event_category = 4

            //and description like 'R01%' and timestamp> '6/10/20'

            //and(SOURCE_TEXT like '%OBX%3042394421%urinary%' or

            //SOURCE_TEXT like '%OBX%3042394421%fecal%' or

            //SOURCE_TEXT like '%OBX%3042394421%uvc%')
            string sql = "";
            sql += "select timestamp,source_text";
            //sql += " case when CHARINDEX('OBX|1', source_text) > 0 then";
            //sql += " substring(source_text, CHARINDEX('OBX|1', source_text), 130) else null end as OBX1,";
            //sql += " case when CHARINDEX('OBX|2', source_text) > 0 then";
            //sql += " substring(source_text, CHARINDEX('OBX|2', source_text), 130) else null end as OBX2,";
            //sql += " case when CHARINDEX('OBX|3', source_text) > 0 then";
            //sql += " substring(source_text, CHARINDEX('OBX|3', source_text), 130) else null end as OBX3,";
            //sql += " case when CHARINDEX('OBX|4', source_text) > 0 then";
            //sql += " substring(source_text, CHARINDEX('OBX|4', source_text), 130) else null end as OBX4";
            sql += " from EVENT_LOG where TIMESTAMP between dateadd(hour,-24," + PFSDBUtility.SQLDateTime(arrival_within24hrs_minlocdt) + ") and " + PFSDBUtility.SQLDateTime(arrival_within24hrs_minlocdt);
            sql += " and event_source=1 and event_type=4 and event_category=4";
            sql += " and description like 'R01%" + pat.acct + "%'";
            sql += " and (source_text like '%OBX%90070%urinary%'";
            sql += " or source_text like '%OBX%90070%fecal%'";
            sql += " or source_text like '%OBX%90070%uvc%')";
            Program.VerboseAudit("qLDA=" + sql);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd2 = new SqlCommand(sql, db2);
            cmd2.CommandTimeout = 180;
            SqlDataReader dr2 = cmd2.ExecuteReader(System.Data.CommandBehavior.CloseConnection);

            string srctxt;
            List<EventLog_Data> eventlog_list = new List<EventLog_Data>();   // make an empty list
            EventLog_Data evrec;

            while (dr2.Read())
            {
                srctxt = PFSDBUtility.DBToString(dr2["SOURCE_TEXT"]);
                evrec.srctxt = srctxt;
                DateTime timestmp = PFSDBUtility.DBToDateTime(dr2["timestamp"]);
                evrec.timestamp = timestmp;
                eventlog_list.Add(evrec);
            }
            db2.Close();

            int obxpos;
            int srclen;
            int codepos;
            foreach (var e in eventlog_list)
            {
                //sql += " case when CHARINDEX('OBX|1', source_text) > 0 then";
                //sql += " substring(source_text, CHARINDEX('OBX|1', source_text), 130) else null end as OBX1,";
                //sql += " case when CHARINDEX('OBX|2', source_text) > 0 then";
                //sql += " substring(source_text, CHARINDEX('OBX|2', source_text), 130) else null end as OBX2,";
                //sql += " case when CHARINDEX('OBX|3', source_text) > 0 then";
                //sql += " substring(source_text, CHARINDEX('OBX|3', source_text), 130) else null end as OBX3,";
                //sql += " case when CHARINDEX('OBX|4', source_text) > 0 then";
                //obx1str = PFSDBUtility.DBToString(dr2["OBX1"]);
                //obx2str = PFSDBUtility.DBToString(dr2["OBX2"]);
                //obx3str = PFSDBUtility.DBToString(dr2["OBX3"]);
                //obx4str = PFSDBUtility.DBToString(dr2["OBX4"]);
                srclen = e.srctxt.Length;
                obxpos = e.srctxt.IndexOf("OBX|1");
                if (obxpos > 0)
                {
                    if (obxpos+ SRC_LEN_GRAB <= srclen)
                        obx1str = e.srctxt.Substring(obxpos, SRC_LEN_GRAB);
                    else
                        obx1str = e.srctxt.Substring(obxpos);
                    ProcessLDAOBX(obx1str);
                }

                obxpos = e.srctxt.IndexOf("OBX|2");
                if (obxpos > 0)
                {
                    if (obxpos + SRC_LEN_GRAB <= srclen)
                        obx2str = e.srctxt.Substring(obxpos, SRC_LEN_GRAB);
                    else
                        obx2str = e.srctxt.Substring(obxpos);
                    ProcessLDAOBX(obx2str);
                }

                obxpos = e.srctxt.IndexOf("OBX|3");
                if (obxpos > 0)
                {
                    if (obxpos + SRC_LEN_GRAB <= srclen)
                        obx3str = e.srctxt.Substring(obxpos, SRC_LEN_GRAB);
                    else
                        obx3str = e.srctxt.Substring(obxpos);
                    ProcessLDAOBX(obx3str);
                }

                obxpos = e.srctxt.IndexOf("OBX|4");
                if (obxpos > 0)
                {
                    if (obxpos + SRC_LEN_GRAB <= srclen)
                        obx4str = e.srctxt.Substring(obxpos, SRC_LEN_GRAB);
                    else
                        obx4str = e.srctxt.Substring(obxpos);
                    ProcessLDAOBX(obx4str);
                }
            } //dr read
            //VerboseAudit("timefinish=" + DateTime.Now);

        }

        static void ProcessLDAOBX(string obxstr)
        {
            string timestr;
            DateTime evdt;
            int seq = 0;
            string ldacode = "";
            string ldadescript = "";
            string obx3 = "";
            string obx14 = "";
            string result = "";

            if (obxstr.Trim() != "" && obxstr != null)
            {
                //timestr = timestmp.ToString("yyyyMMddhhmm");
                //VerboseAudit("timestmp=" + timestr);

                //OBX|1|DT|3042394421&2714^Placement Date-Urinary Catheter Temperature probe 16 Fr.^ACUITYFDC||20200616||||||F|||202006160041||13003^PELLETIER^EMILY^A.R.N.^
                //OBX|2|TM|3042394422&2714^Placement Time-Urinary Catheter Temperature probe 16 Fr.^ACUITYFDC||00400000||||||F|||202006160041||13003^PELLETIER^EMILY^A.R.N.^

                VerboseAudit("Pre-arrival LDA: " + obxstr);
                var arrobx = SplitOnPipeAndPrepareElements(obxstr);
                if (arrobx.GetUpperBound(0) >= 5)
                {
                    ldacode = "";
                    ldadescript = "";
                    obx3 = "";
                    obx14 = "";
                    result = "";

                    obx3 = arrobx[3];
                    result = arrobx[5];
                    //VerboseAudit("ordctrl: " + ordctrl);
                    //obx14 = arrobx[14];
                    //evdt = PFSUtility.ISOToDateTime(obx14);
                    //VerboseAudit("ordid: " + ordid);
                    int ldacodepos = obx3.IndexOf("^");
                    if (ldacodepos > 0)
                    {
                        ldacode = obx3.Substring(0, ldacodepos);
                        ldadescript = obx3.Substring(ldacodepos + 1);

                        if (ldacode.Contains("304239442") || ldacode.Contains("90070"))
                        {
                            VerboseAudit("Adding LDA: code=" + ldacode + " desc=" + ldadescript + " evdt=" + obx14);
                            using (var db = PFSDBUtility.NewSqlConnection())
                            {
                                seq++;
                                evdt = arrival_within24hrs_minlocdt;
                                VerboseAudit("Evdt=" + arrival_within24hrs_minlocdt.ToString());
                                string q = "INSERT INTO chart_item (encounter_id,event_datetime,code,description,timestamp,sequence,unit_id,result)";
                                q += " select @encid, @evdt, @code, @desc, @ts, @seq, @unit, @result";
                                q += " where not exists (select encounter_id,code,event_datetime,unit_id,sequence from chart_item";
                                q += " where encounter_id=" + pat.encounter_id.ToString() + " and code='" + ldacode + "' and event_datetime='" + evdt.ToString() + "' and unit_id=-1 and sequence=" + seq + ")";
                                //string q = "INSERT INTO chart_item (encounter_id,event_datetime,code,description,timestamp,sequence,unit_id,order_id) values (@encid, @evdt, @code, @desc, @ts,@seq,@unit,@oid)";
                                SqlCommand cmd = new SqlCommand(q, db);
                                cmd.Parameters.AddWithValue("@encid", pat.encounter_id);
                                cmd.Parameters.AddWithValue("@evdt", evdt);
                                cmd.Parameters.AddWithValue("@code", ldacode);
                                cmd.Parameters.AddWithValue("@desc", ldadescript);
                                cmd.Parameters.AddWithValue("@ts", DateTime.Now);
                                cmd.Parameters.AddWithValue("@seq", seq);
                                cmd.Parameters.AddWithValue("@unit", -1);
                                cmd.Parameters.AddWithValue("@result", result);
                                cmd.ExecuteNonQuery();
                            }
                        }
                    }
                }
            }

        }
        public static string[] SplitOnPipeAndPrepareElements(string s)
        {
            // If a word list has a space after each comma, we will get leading blanks in each term
            var arr = s.Split('|');
            for (int i = 0; i <= arr.GetUpperBound(0); i++)
            {
                arr[i] = arr[i].Trim();                         // get rid of leading blanks
            }
            return arr;
        }

//        static void SaveChartItem(PfsDataContext pfs)
//        {
//////////////////////stubbed for debug
//            return;
//////////////////////stubbed for debug
//            //DateTime pdate = PFSUtility.ISOToDateTime(_perfdate);
//            int ct = 0;
//            string sql = "select count(*) from chart_item as ci";
//            sql += " inner join encounter as e on (e.encounter_id=ci.encounter_id)";
//            sql += " where e.acct_number=" + PFSDBUtility.SQLString(pat.acct);
//            sql += " and ci.code=" + PFSDBUtility.SQLString(_fndcode);
//            //Program.VerboseAudit(sql);
//            var db2 = PFSDBUtility.NewSqlConnection();
//            var cmd2 = new SqlCommand(sql, db2);
//            SqlDataReader dr2 = cmd2.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
//            while (dr2.Read())
//            {
//                ct = dr2.GetInt32(0);
//            } //dr read
//            //dr2.Close();
//            db2.Close();
//            if (ct > 0) return;

//            var ci = new CHART_ITEM();
//            //string format = "yyyyMMddHHmm";
//            //if (_perfdate.Length == 12)
//            //    ci.EVENT_DATETIME = DateTime.ParseExact(_perfdate, format, System.Globalization.CultureInfo.InvariantCulture);
//            //else
//            ci.EVENT_DATETIME = pat.unit_arrival;
//            ci.ENCOUNTER_ID = pat.encounter_id;
//            ci.UNIT_ID = pat.unit_id;
//            ci.CODE = _fndcode;
//            Program.VerboseAudit("adding code=" + _fndcode);
//            //ci.CATEGORY = _category;
//            ci.DESCRIPTION = _descript;
//            //ci.FIELD_NAME = _group;
//            if (_result == null) _result = "";
//            ci.RESULT = _result;
//            ci.TIMESTAMP = pat.unit_arrival;
//            ci.SEQUENCE = (short)seq++;
//            //ci.SOURCE_TEXT = _sourcefn;

//            pfs.CHART_ITEMs.InsertOnSubmit(ci);
//            SubmitOverwrite(pfs);

//        }

        static void SubmitOverwrite(PfsDataContext pfs)
        {
            bool moreToSubmit = true;
            do
            {
                try
                {
                    pfs.SubmitChanges(System.Data.Linq.ConflictMode.ContinueOnConflict);
                    moreToSubmit = false;
                }
                catch (System.Data.Linq.ChangeConflictException)
                {
                    foreach (System.Data.Linq.ObjectChangeConflict occ in pfs.ChangeConflicts)
                    {
                        // All database values overwrite current values with 
                        //values from database
                        occ.Resolve(System.Data.Linq.RefreshMode.OverwriteCurrentValues);
                    }
                }
            }
            while (moreToSubmit);

        }

        //static List<int> GetUnitDefaultIndicators(int unit_id, DateTime pull_dt, out string default_inds_str, out int default_ptype)
        //{
        //    var result = new List<int>();                   // make an empty list
        //    default_inds_str = "<none>";
        //    default_ptype = 6;
        //    var db = PFSDBUtility.NewPfsDataContext();        // look for the unit default admission profile
        //    var query = from param in db.UNIT_PARAMs
        //                from profile in param.PATIENT_PROFILEs
        //                where (param.UNIT_ID == unit_id)
        //                && (pull_dt >= param.EFFECTIVE_DATETIME) && (pull_dt < param.EXPIRATION_DATETIME)
        //                && (profile.PROFILE_NUMBER == param.DEFAULT_ADMISSION_PROFILE)
        //                select new
        //                {
        //                    profile.INDICATORS              // comma-separated indicator list
        //                };
        //    foreach (var inds in query)
        //    {
        //        default_inds_str = inds.INDICATORS;
        //        string s = inds.INDICATORS;
        //        if (!String.IsNullOrEmpty(s))
        //        {
        //            var arr = s.Split(',');
        //            foreach (var t in arr)
        //            {
        //                if (t.IsNumeric())
        //                {                // add an indicator number to the list
        //                    result.Add(t.ToInteger());
        //                }
        //            }
        //        }
        //    }

        //    var query_ind_def = from ind_def in db.INDICATOR_DEFINITIONs
        //                        where (ind_def.METHODOLOGY_ID == PFSGlobal.METH_ID_APLUS_INPATIENT2) &&
        //                          result.Contains(ind_def.INDICATOR_NUMBER)
        //                        select new
        //                        {
        //                            ind_def.WEIGHT
        //                        };
        //    var score = 0.0;
        //    foreach (var wgts in query_ind_def)
        //    {
        //        score += wgts.WEIGHT;
        //    }
        //    DebugTrace("indicators=" + default_inds_str, "");
        //    DebugTrace("score=" + score.ToString(), "");

        //    var query_ptype = from ptype in db.PATIENT_TYPEs
        //                      where (ptype.METHODOLOGY_ID == PFSGlobal.METH_ID_APLUS_INPATIENT2)
        //                      orderby ptype.PATIENT_TYPE1
        //                      select new
        //                      {
        //                          ptype.PATIENT_TYPE1,
        //                          ptype.POINTS_HIVAL
        //                      };

        //    foreach (var ptypes in query_ptype)
        //    {
        //        //DebugTrace("type,hival=" + ptypes.PATIENT_TYPE1.ToString()+","+ptypes.POINTS_HIVAL.ToString(), "");
        //        if (score <= ptypes.POINTS_HIVAL)
        //        {
        //            if (default_ptype > ptypes.PATIENT_TYPE1)
        //            {
        //                default_ptype = ptypes.PATIENT_TYPE1;
        //            }
        //        }
        //    }
        //    DebugTrace("def patient type=" + default_ptype.ToString(), "");

        //    return result;
        //}


        static void DeleteOldChartItems()
        {
            string sql;
            DateTime dt;

            if (g_no_output || g_no_delete) return;

            DebugTrace("About to delete old char items...");
            DebugPause();                                       // give a chance to ^C in debug mode

            LogInfo("Delete old chart items...");
            dt = g_effdt.AddDays(-CHART_ITEM_LIFE);
            sql = "DELETE FROM CHART_ITEM WHERE EVENT_DATETIME < " + PFSDBUtility.SQLDateTime(dt);
            PFSDBUtility.ExecuteSQL(sql);
            LogInfo("Done");
        }

        //static void MaybeRunImport()
        //{
        //    if (! g_import_when_done) return;

        //    string path = Path.GetDirectoryName(Application.ExecutablePath);
        //    if (path.Right(3) != "bin") {                       // running in visual studio?
        //        path = @"C:\qmdev\AcuityPlus\main\bin";
        //    }
        //    if (Directory.Exists(path))
        //    {
        //        LogInfo("Running transparent import...");
        //        // Import will add its own log entries; no need to add any here
        //        int rc = PFSUtility.ExecuteAndWait(Path.Combine(path, PFSGlobal.TRANSPARENT_IMPORT_EXE_NAME));
        //        LogInfo("Done; result=" + rc);
        //    }
        //    else
        //    {
        //        LogWarning("Can't find " + path);
        //    }
        //}


        //    Dim rs As New Recordset
        //    Dim rs2 As New Recordset
        //    Dim sql As String, sql2 As String
        //    Dim startloa(MAX_LOA) As LOAtypePrecision
        //    Dim finishloa(MAX_LOA) As LOAtypePrecision
        //    Dim numstartloa As Integer, numendloa As Integer, i As Integer, j As Integer
        //    Dim done As Boolean

        //'there shouldnt be 2 starts in a row
        //'-- choose the later timestamp and cancel the earlier one
        //'ONLY check for starts less than the finish time--dont want to do future dncs.
        //    numstartloa = 0
        //    sql = "select convert(varchar,timestamp,20) from chart_item where encounter_id=" & pat.encounter_id
        //    sql = sql & " and order_control<>'X' and code='@start_loa' and event_datetime<" & g_dbutil.SQL_DateTime(g_pull_finish)
        //    sql = sql & " order by timestamp"
        //'dvprint sql
        //    rs.Open sql, g_cnADO
        //    Do While Not rs.EOF
        //        numstartloa = numstartloa + 1
        //        startloa(numstartloa).startdt = rs(0)
        //        rs.MoveNext
        //    Loop
        //    rs.Close

        //    'now cancel the earlier starts
        //    For i = 1 To numstartloa - 1
        //        sql = "select count(*) from chart_item where encounter_id=" & pat.encounter_id
        //        sql = sql & " and order_control<>'X' and code='@finish_loa'"
        //        sql = sql & " and convert(varchar,timestamp,20) between '" & startloa(i).startdt & "' and '" & startloa(i + 1).startdt & "'"
        //'dvprint sql
        //        rs.Open sql, g_cnADO
        //        If rs(0) = 0 Then 'no finish between the starts-cancel the earlier one
        //            startloa(i).Cancel = True
        //        End If
        //        rs.Close
        //    Next i

        //'do the same for finishes
        //'there shouldnt be 2 finishes in a row
        //    numendloa = 0
        //    sql = "select convert(varchar,timestamp,20) from chart_item where encounter_id=" & pat.encounter_id
        //    sql = sql & " and order_control<>'X' and code='@finish_loa' order by timestamp"
        //'dvprint sql
        //    rs.Open sql, g_cnADO
        //    Do While Not rs.EOF
        //        numendloa = numendloa + 1
        //        finishloa(numendloa).enddt = rs(0)
        //        rs.MoveNext
        //    Loop
        //    rs.Close

        //    'now cancel the earlier finishes
        //    For i = 1 To numendloa - 1
        //        sql = "select count(*) from chart_item where encounter_id=" & pat.encounter_id
        //        sql = sql & " and order_control<>'X' and code='@start_loa'"
        //        sql = sql & " and convert(varchar,timestamp,20) between '" & finishloa(i).enddt & "' and '" & finishloa(i + 1).enddt & "'"
        //'dvprint sql
        //        rs.Open sql, g_cnADO
        //        If rs(0) = 0 Then 'no start between the finishes-cancel the earlier one
        //            finishloa(i).Cancel = True
        //        End If
        //        rs.Close
        //    Next i

        //'do the actual cancels in the db.
        //    For i = 1 To numstartloa
        //        If startloa(i).Cancel Then
        //            sql = "update chart_item set order_control='X' where encounter_id=" & pat.encounter_id
        //            sql = sql & " and code='@start_loa' and convert(varchar,timestamp,20)='" & startloa(i).startdt & "'"
        //'dvprint sql
        //            g_cnADO.Execute sql
        //        End If
        //    Next i

        //    For i = 1 To numendloa
        //        If finishloa(i).Cancel Then
        //            sql = "update chart_item set order_control='X' where encounter_id=" & pat.encounter_id
        //            sql = sql & " and code='@finish_loa' and convert(varchar,timestamp,20)='" & finishloa(i).enddt & "'"
        //'dvprint sql
        //            g_cnADO.Execute sql
        //        End If
        //    Next i

        //'Now there are no consecutive starts and no consec finishes;
        //'  each start can be paired with its finish if it exists
        //    sql = "select event_datetime,timestamp from chart_item where encounter_id=" & pat.encounter_id
        //    sql = sql & " and order_control<>'X' and code='@start_loa' order by timestamp"
        //'dvprint sql
        //    rs.Open sql, g_cnADO
        //    Do While Not rs.EOF
        //        pat.num_loa = pat.num_loa + 1
        //        pat.loa(pat.num_loa).startdt = rs(0)
        //'dvprint "Start " & pat.num_loa & "=" & pat.loa(pat.num_loa).startdt
        //        sql2 = "select min(event_datetime) from chart_item where encounter_id=" & pat.encounter_id
        //        sql2 = sql2 & "  and order_control<>'X' and code='@finish_loa' and timestamp>=" & g_dbutil.SQL_DateTime(rs(1))
        //'dvprint sql2
        //        rs2.Open sql2, g_cnADO
        //        If IsNull(rs2(0)) Then
        //            pat.loa(pat.num_loa).retain = True 'still havent received returnloa
        //            If pat.loa(pat.num_loa).startdt >= g_pull_finish Then
        //                pat.loa(pat.num_loa).enddt = DateAdd("h", 24, g_pull_finish)
        //            Else
        //                pat.loa(pat.num_loa).enddt = g_pull_finish
        //            End If
        //        ElseIf rs2(0) > g_pull_finish Then
        //            pat.loa(pat.num_loa).retain = True 'want to end the dnc at 7am and have the partial dnc start at 7am tomorrow
        //            pat.loa(pat.num_loa).enddt = g_pull_finish
        //        Else
        //            pat.loa(pat.num_loa).retain = False
        //            pat.loa(pat.num_loa).enddt = rs2(0)
        //        End If
        //'dvprint "Finish " & pat.num_loa & "=" & pat.loa(pat.num_loa).enddt
        //        rs2.Close
        //        rs.MoveNext
        //    Loop
        //    rs.Close

        //    'now reconcile the pairs
        //    'cancel the pairs where start = finish
        //    For i = 1 To pat.num_loa
        //        If pat.loa(i).startdt = pat.loa(i).enddt Then
        //            pat.loa(i).Cancel = True
        //            sql = "update chart_item set order_control='X' where encounter_id=" & pat.encounter_id
        //            sql = sql & " and code='@start_loa' and event_datetime=" & g_dbutil.SQL_DateTime(pat.loa(i).startdt)
        //'dvprint sql
        //            g_cnADO.Execute sql
        //            sql = "update chart_item set order_control='X' where encounter_id=" & pat.encounter_id
        //            sql = sql & " and code='@finish_loa' and event_datetime=" & g_dbutil.SQL_DateTime(pat.loa(i).enddt)
        //'dvprint sql
        //            g_cnADO.Execute sql
        //        Else
        //'            dvprint "StartDNC=" & pat.loa(i).startdt & " EndDNC=" & pat.loa(i).enddt
        //        End If
        //    Next i

        //    'now crunch the ary so we dont have cancels
        //    CrunchLOA pat



        //    Exit Sub
        //errHandler:
        //    Debug.Print Err.Description
        //End Sub

        //Private Sub CrunchLOA(pat As PatientInfo)
        //    Dim i As Integer
        //    Dim num_done As Integer

        //'    dvprint "CrunchLOA"
        //    num_done = 0
        //    For i = 1 To pat.num_loa
        //        If Not pat.loa(i).Cancel Then
        //            If(pat.loa(i).startdt >= g_pull_start And pat.loa(i).startdt <= g_pull_finish) Or _
        //              (pat.loa(i).enddt >= g_pull_start And pat.loa(i).enddt <= g_pull_finish) Or _
        //              (pat.loa(i).startdt <= g_pull_start And pat.loa(i).enddt >= g_pull_finish) Then
        //               num_done = num_done + 1
        //                pat.loa(num_done) = pat.loa(i)
        //    dvprint "Pass/DNC: " & g_dbutil.SQL_DateTime(pat.loa(num_done).startdt) & g_dbutil.SQL_DateTime(pat.loa(num_done).enddt)
        //            End If
        //        End If
        //    Next i
        //    pat.num_loa = num_done

        //'    dvprint "END CrunchLOA"

        //End Sub




        //=====================================================================
        // Audits and log files
        //=====================================================================
        // Print to console if debug is set
        public static void DebugTrace(string format, params object[] values)
        {
            if (g_debug)
            {
                Console.Write(DateTime.Now.ToString() + " ");
                Console.WriteLine(format, values);
            }
        }

        public static void DebugPause()
        {
            if (g_debug)
            {
                Console.Write("Press any key...");
                Console.ReadKey();
                Console.WriteLine("");
            }
        }

        // Save in both audit files
        static public void Audit(string s)
        {
            DebugTrace(s);
            if (logfile != null) logfile.WriteLine(s);
            gBriefAudit.AppendLine(s);                  // Add to both audit reports
            gVerboseAudit.AppendLine(s);
        }

        // Save in verbose audit only
        static public void VerboseAudit(string s)
        {
            DebugTrace(s);
            if (logfile != null) logfile.WriteLine(s);
            gVerboseAudit.AppendLine(s);                // Add to verbose audit only
        }

        public static void AddLogEntry(PFSEventLog.EventLogType type, string msg, PFSEventLog.EventLogCategory category)
        {
            Audit(msg);

            PFSEventLog.AddEventLogEntry(
                PFSEventLog.EventLogSource.EVENT_SOURCE_TRANSPARENT_MAPPING,
                type, category, msg, gLogSourceText, 0, gLogUnitID, gLogEncounterID);
        }

        public static void LogInfo(string msg, PFSEventLog.EventLogCategory category)
        {
            AddLogEntry(PFSEventLog.EventLogType.EVENT_TYPE_INFO, msg, category);
        }
        public static void LogInfo(string msg)
        {
            LogInfo(msg, PFSEventLog.EventLogCategory.EVENT_CATEGORY_PROCESSED);
        }

        public static void LogWarning(string msg, PFSEventLog.EventLogCategory category)
        {
            AddLogEntry(PFSEventLog.EventLogType.EVENT_TYPE_WARNING, msg, category);
        }
        public static void LogWarning(string msg)
        {
            LogWarning(msg, PFSEventLog.EventLogCategory.EVENT_CATEGORY_VALIDATION);
        }

        public static void LogError(string msg, PFSEventLog.EventLogCategory category)
        {
            AddLogEntry(PFSEventLog.EventLogType.EVENT_TYPE_ERROR, msg, category);
        }
        public static void LogError(string msg)
        {
            LogError(msg, PFSEventLog.EventLogCategory.EVENT_CATEGORY_VALIDATION);
        }
        public static void LogUnexpectedError(string msg, string stack_trace)
        {
            // Add the message and stack trace to event log; message only goes to screen
            gLogSourceText = stack_trace;
            AddLogEntry(PFSEventLog.EventLogType.EVENT_TYPE_ERROR, msg, PFSEventLog.EventLogCategory.EVENT_CATEGORY_UNEXPECTED);
            gLogSourceText = "";
            
            // Add the stack trace to screen and log files
            Audit(msg);
            Audit(stack_trace);
        }



    }
}
