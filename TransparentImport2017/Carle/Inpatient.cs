﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;                     // for LINQ; add a reference
using System.Data.SqlClient;                // for SqlConnection
using System.IO;                            // for Directory, Path, Stream
using PfsShared;                            // add a reference to Shared2 project

// ============================================================================
// Inpatient transparent mapping -- GOES HERE --
// CARLE Epic
// ============================================================================
// This processes one patient.  Only one database query is used.  :)
//
// All search functions use exact match for category, description and field name.
// Codes and results can be a single word or a list of words.
// Codes default to exact match; prefix with CODE_CODE_LIKE_PREFIX if desired (single word only)
// Results default to like matching; prefix with RESULT_EXACT_MATCH_PREFIX if desired (single word only)
//
// All searches are case insensitive.
//
//
namespace TransparentMapping
{
    class Inpatient
    {
        private const int MAX_INDS = 120;
        private const int MAX_PROCS = 20;

        private const string CODE_LIKE_PREFIX = "%!";       // use to make codes %like%
        private const string EXACT_MATCH_PREFIX = "&!";     // use to make desc/result exact match
        private const string CHAR_COMMA = "||";             // use to insert literal comma in word list
        private const string NOT_PREFIX = "!!";             // use to insert literal comma in word list
        private const string AVOID_NEGATIVE = "!;";
        private const string EXACT_SQL_PREFIX = "!SQL!";     // use to give sql constraint as given
        private const string STARTS_WITH = "[!";     // use to give sql constraint as given

        private struct indicator_data {
            public bool is_checked;
            public int radio_group;
            public int weight;
        }

        private struct proc_data {
            public int procedure_number;
            public DateTime start;
            public DateTime finish;
        }

        // These are database CHART_ITEMs for this patient
        private CHART_ITEM[] _chart_items_since_admission;
        //private CHART_ITEM[] _chart_items_since_unit_arrival;
        //private CHART_ITEM[] _chart_items_during_pull_period;
        //private CHART_ITEM[] _chart_items_pull_period_plus;
        //private CHART_ITEM[] _chart_items_since24hrs;
        //private CHART_ITEM[] _chart_items_since13hrs;
        private CHART_ITEM[] _chart_items_since24hrs;
        //private CHART_ITEM[] _chart_items_since9hrs;
        // These are database PROCEDURE_EVENTs for this patient
        private PROCEDURE_EVENT[] _procedure_events;

        private indicator_data[] _inds;
        private List<proc_data> _procs;
        private List<proc_data> _outcomes;

        private const int DEFAULT_BUCKET_SIZE = 60;       //min

        private PatientInfo _pat;
        private int _bucket_size = DEFAULT_BUCKET_SIZE;
        private bool is_default;
        private string txarea;
        private bool periop_found_inpast13hrs = false;

        private bool exclude_periop_data = false;
        private bool exclude_periph_iv = false;
        private bool coma = false;
        private bool g_toi4 = false;
        private bool g_gitube_attempted = false;
        private bool g_gitube = false;
        private DateTime loc_in;
        private DateTime loc_out;
        private DateTime loc_arrtime;
        int rassct12 = 0;
        int rassct34 = 0;
        int rassctLTzero = 0;
        private LOAtypePrecision[] aryloa = new LOAtypePrecision[10];
        private int numloa = 0;
        private LOAtypePrecision[] ary_hemodial = new LOAtypePrecision[5];
        private int numhemodial = 0;
        private LOAtypePrecision[] aryclassdnc = new LOAtypePrecision[10];
        private int numclassdnc = 0;
        private bool g_lda6yo = false;
        private bool g_iabp = false;
        private DateTime leftunit_dt = DateTime.MinValue;
        private DateTime retunit_dt = DateTime.MinValue;
        private bool g_dialysis = false;

        private string assessgrouplabel = "";
        private DateTime _level_of_care_threshold = DateTime.MinValue;//this time affects AddBuckets

        private enum SearchDepth {
            SearchDefault,
            SearchPullRange,//search within the current pull     -- default
            SearchSinceArrival,         //search since arrival to the unit
            SearchSinceAdmission,        //search everything since admission to the hospital
            SearchPullPlus,
            SearchSince24Hrs,
            SearchSince13Hrs,
            SearchSince12Hrs,
            SearchSince16Hrs,
            SearchSince9Hrs,
            SearchSince4Hrs
        }

        private enum CountMode {
            CountAll,
            CountFirst                  //stop after one is found
        }

        private enum GetValueMode {
            GetTotal,
            GetMax,
            GetLast
        }

        enum Frequencies {
            QNONE,
            Q4H,
            Q2H,
            Q1H,
            Q30M
        }

        private struct fmapRow {
            public double los_high;                       //the LOS being testing
            public int[] freq;                           //the count required for each Q value
        }
        private List<fmapRow> _freq_map;                    //1,2,4,8,12,24 hours

        private struct gBucket
        {
            public int bucket;
            public string code;
            public DateTime evdt;
            public bool using_waiver;
            public bool has_all_deps; //has all dependents
            public int num_addl_items;
            public string description;
        }
        private struct gGap
        {
            public int gap;
            public DateTime evdt1;
            public DateTime evdt2;
        }

        private struct MedChartItem
        {
            public string code;
            public string orderid;
            public DateTime evdt;
            public bool valid;
        }
        //private string[] meds_rate = { "newbag", "new bag", "rateverify", "rate verify", "restarted", "ratechange", "rate change", "started", "continued" };
        //private struct med2026and5077
        //{
        //    public string name;
        //    public bool found;
        //}
        //
        // This is the main entry point
        //
        public void ProcessPatient(PatientInfo pat)
        {
            _pat = pat;
            bool use_default = false;
            bool no_chart_items_in_24hrs = false;
            bool loa_exists = false;
            bool do_class = true;

            InitIndicators(); // sets is_default
            InitProcs();
            if (!is_default)
            {
                LoadFreqTable();
                //CheckIfPeriopInPast13hrs();
                no_chart_items_in_24hrs = (LoadPatientChart() == 0);
                Program.VerboseAudit("New query default scope = " + loc_in + " to " + loc_out);
                if (no_chart_items_in_24hrs)
                    Program.Audit("No chart items received in past 24 hrs.");
                if (!Program.g_onlydnc && do_class)
                {
                    Check_1_2_3_4();
                    Check_5();
                    Check_6_7();
                    Check_8();
                    Check_9();
                    Check_10_11();
                    Check_12_13();
                    Check_14();
                    Check_15_16_17_18();
                    Check_19();
                    Check_20();
                    Check_21_22();
                    Check_23();
                    Check_24();
                    CheckUserDefined();
                    AtLeastOneADL();
                }
            }

            //if (!no_chart_items_in_24hrs)
            {
                if (!Program.g_onlydnc && do_class)
                {
                    HighestIndicatorInEachGroupWins();

                    if (!is_default)
                    {
                        _pat.ptype = DeterminePtypeOfIndicators();
                        //Program.sttimer();
                        if (!Program.g_noactivities) CheckProcs();
                        //Program._t4 = Program.entimer("t4=", Program._t4);
                        //CheckOutcomes();
                        //if (Program.g_do_OW) CheckOtherWorkload();
                    }

                    if (Program.g_no_output) return;
                    //if (_pat.default_ptype > DeterminePtypeOfIndicators())
                    //{ // if the default pt type is higher than the pt type of this class
                    //  // then if there is a default classification in the past 16 hrs
                    //  // then make these indicators the default indicators.
                    //    use_default = ExistDefaultInPast16hrs();
                    //    if (use_default)
                    //    {
                    //        Program.VerboseAudit("Default indicators will be used");
                    //    }
                    //}
                    //OutputClassAndDNCTimes();
                    OutputClassAndDNC(do_class);
                    if (!Program.g_noactivities) OutputProcs();
                }
                else
                {
                    //if (!Program.g_noactivities) OutputDNC();
                }
                //OutputOutcomes();
            }
        }

        private void CheckIfPeriopInPast13hrs()
        {
            foreach (var perioploc in Program.patperioplist)
            {
                periop_found_inpast13hrs = periop_found_inpast13hrs
                    || (perioploc.in_time >= _pat.pull_finish.AddHours(-13))
                    || (perioploc.out_time >= _pat.pull_finish.AddHours(-13));
            }
        }

        private void InitIndicators()
        {
            // Make a new (empty) indicator array
            _inds = new indicator_data[MAX_INDS + 1];           // This 1 based so add one
            is_default = false;
            txarea = "";
            //if (_pat.los_hours <= 4.0)
            //{
            //    is_default = true;
            //    Program.VerboseAudit("Patient was here 4 hrs or less. Will receive default indicators " + _pat.default_inds_str);
            //    foreach (var ind in _pat.default_inds)
            //    {
            //        if (ind <= _inds.GetUpperBound(0))
            //        {
            //            _inds[ind].is_checked = true;
            //        }
            //    }
            //}


            // get indicator radio groups from the database
            // ** (This database access can be replaced once we have a C# methodolgy cache)
            var db = PFSDBUtility.NewPfsDataContext();
            var query = from idef in db.INDICATOR_DEFINITIONs
                        where (idef.METHODOLOGY_ID == _pat.meth_id)
                        select idef;
            foreach (var idef in query) {
                if (idef.INDICATOR_NUMBER <= _inds.GetUpperBound(0)) {
                    // (convert nulls to zero)
                    _inds[idef.INDICATOR_NUMBER].radio_group = PFSDBUtility.DBToInt(idef.RADIO_GROUP);
                    _inds[idef.INDICATOR_NUMBER].weight = PFSDBUtility.DBToInt(idef.WEIGHT);
                }
            }
        }

        private void InitProcs()
        {
            _procs = new List<proc_data>();
            _outcomes = new List<proc_data>();
        }

        private fmapRow LoadFreqTableRow(double los_high, string values)
        {
            fmapRow fmrow;

            fmrow.los_high = los_high;
            fmrow.freq = new int[Enum.GetNames(typeof(Frequencies)).Length];

            var arr = values.Split(',');
            for (int i = 0; i <= fmrow.freq.GetUpperBound(0); i++)
            {
                fmrow.freq[i] = arr[i].ToInteger();
            }
            return fmrow;
        }

        // These are the number of times you need to do something during a certain time period (LOS)
        // in order for it to be considered one of the frequencies listed.
        private void LoadFreqTable()
        {
            _freq_map = new List<fmapRow>();
            //                              LOS,  None Q4h Q2h Q1h Q30m
            _freq_map.Add(LoadFreqTableRow(1, "    0,  0,  0,  1,  2"));
            _freq_map.Add(LoadFreqTableRow(2, "    0,  0,  1,  2,  4"));
            _freq_map.Add(LoadFreqTableRow(4, "    0,  1,  2,  4,  8"));
            _freq_map.Add(LoadFreqTableRow(6, "    0,  2,  3,  6,  9"));
            _freq_map.Add(LoadFreqTableRow(8, "    0,  2,  4,  8, 12"));
            _freq_map.Add(LoadFreqTableRow(12, "   0,  3,  6, 12, 18"));
            _freq_map.Add(LoadFreqTableRow(16, "   0,  4,  8, 16, 24"));
            _freq_map.Add(LoadFreqTableRow(24, "   0,  6, 12, 24, 36"));
            //New freq table 2/5/14
            //q4	q2	q1	q30     q30
            //            Non-ICU	ICU & SD
            // 4	8	15	29	    36
            // 3	5	9	17	    24
            // 2	4	7	13	    19
            // 2	3	5	10	    13

            //to read this table: if you have at least 6 asessmenets in a 12-hour period, you get Q1h
            //(LOS=12, column Q1h has a count of 6)
        }

        private int LoadPatientChart()
        {
            foreach (var p in Program.patloclist)
            {
                if (p.loc_idx == _pat.loc_idx)
                {
                    loc_in = p.in_time;
                    loc_out = p.out_time;
                    loc_arrtime = p.arr_time;
                }
            }
            _pat.los_hours = PFSUtility.DateDiffInMinutes(loc_in, loc_out) / 60.0;
            Program.VerboseAudit("LoadChart los=" + _pat.los_hours);
            //Program.VerboseAudit("LoadChart unit=" + p.unit_name + " locidx=" + p.loc_idx + " in=" + p.in_time + " out=" + p.out_time);

            int ct_in_24hrs = 0;
            int ctperiop = 0;
            // Get the entire patient chart (all units and dates for this patient)
            var dba = PFSDBUtility.NewPfsDataContext();
            var queryall = from item in dba.CHART_ITEMs
                           where (item.ENCOUNTER_ID == _pat.encounter_id)
                           where (item.EVENT_DATETIME <= loc_out)
                           orderby item.EVENT_DATETIME
                           select item;
            var querya = from g in queryall
                         select g;
            // Save the result
            _chart_items_since_admission = querya.ToArray();
            Program.VerboseAudit("Count since adm=" + querya.Count());


            var query = from item in _chart_items_since_admission
                        where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-24)
                               && (item.EVENT_DATETIME <= _pat.pull_finish))
                        select item;
            // Save the result
            ct_in_24hrs = query.Count();
            _chart_items_since24hrs = query.ToArray();

            // Convert all results to lower case for case insensitive comparisons
            foreach (var item in _chart_items_since24hrs) {
                item.SOURCE_TEXT = null;
                if (item.CATEGORY != null) item.CATEGORY = item.CATEGORY.ToLower();
                if (item.CODE != null) item.CODE = item.CODE.ToLower();
                if (item.DESCRIPTION != null) item.DESCRIPTION = item.DESCRIPTION.ToLower();
                if (item.FIELD_NAME != null) item.FIELD_NAME = item.FIELD_NAME.ToLower();
                if (item.RESULT != null) item.RESULT = item.RESULT.ToLower();
            }

            Program.VerboseAudit("Since 24 hrs count items=" + ct_in_24hrs);
            Program.VerboseAudit("Since 24 hrs count of HD items=" + ctperiop);

            // Jul 19 2022
            //Ignore documentation between chartings of code = 12067428 “Left unit” and 
            // “Returned to unit”.  
            // This time range should also affect the LOS calculation for frequency.

            int numleftunit = 0;
            int numretunit = 0;
            var ignorequery = StartNewQuery(SearchDepth.SearchSince24Hrs);
            ignorequery = AndItemFilter(ignorequery, "", "12067428", "", "", "left unit");
            ignorequery = ignorequery.OrderByDescending(e => e.EVENT_DATETIME);
            if (ignorequery.Count() > 0)
            {
                leftunit_dt = ignorequery.First().EVENT_DATETIME;
                numleftunit = ignorequery.Count();
            }

            var ignorequery2 = StartNewQuery(SearchDepth.SearchSince24Hrs);
            ignorequery2 = AndItemFilter(ignorequery2, "", "12067428", "", "", "returned to unit");
            ignorequery2 = ignorequery2.OrderByDescending(e => e.EVENT_DATETIME);
            if (ignorequery2.Count() > 0)
            {
                retunit_dt = ignorequery2.First().EVENT_DATETIME;
                numretunit = ignorequery2.Count();
            }

            if (numleftunit > 0)
                Program.VerboseAudit("numleftunit=" + numleftunit + " leftunit_dt=" + leftunit_dt + " retunit_dt=" + retunit_dt);
            if (numretunit > 0)
                Program.VerboseAudit("numretunit=" + numretunit + " leftunit_dt=" + leftunit_dt + " retunit_dt=" + retunit_dt);

            return ct_in_24hrs;
        }

        // Delay looking for patient procedures until they are wanted
        private void LoadPatientProceduresIfNeeded()
        {
            if (_procedure_events != null) return;

            var db = PFSDBUtility.NewPfsDataContext();
            var query = from proc in db.PROCEDURE_EVENTs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                        where (proc.PROCEDURE_DATETIME >= _pat.pull_finish.AddHours(-24))
                        select proc;
            _procedure_events = query.ToArray();
        }



        // Started a new chart item query (of a certain depth)
        // Default is the chart during the pull range.  Options for unit arrival and admission to hospital.
        private IEnumerable<CHART_ITEM> StartNewQuery()
        {
            return StartNewQuery(SearchDepth.SearchDefault);
        }
        private IEnumerable<CHART_ITEM> StartNewQuery(SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;

            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    if (exclude_periop_data)
                        //return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_start && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= loc_in && (item.EVENT_DATETIME <= loc_out)) select item);
                    //return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-12)) select item);
                    else
                        //return (from item in _chart_items_since24hrs where (item.EVENT_DATETIME >= _pat.pull_start && item.EVENT_DATETIME <= _pat.pull_finish) select item);
                        return (from item in _chart_items_since24hrs where (item.EVENT_DATETIME >= loc_in && item.EVENT_DATETIME <= loc_out) select item);
                //return (from item in _chart_items_since24hrs where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-12)) select item);
                //if (exclude_periop_data)
                //    return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-8) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                //else
                //    return (from item in _chart_items_since24hrs where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-8) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                //    return (from item in _chart_items_since_unit_arrival select item);
                case SearchDepth.SearchSinceAdmission:
                    return (from item in _chart_items_since_admission select item);
                //case SearchDepth.SearchPullPlus:
                //    return (from item in _chart_items_pull_period_plus select item);
                case SearchDepth.SearchSince24Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                    else
                        return (from item in _chart_items_since24hrs where (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince16Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-16) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since24hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-16) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince13Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-13) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since24hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-13) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince12Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-12) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since24hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-12) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince9Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-9) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since24hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-9) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince4Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME > _pat.pull_finish.AddHours(-4) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since24hrs where item.EVENT_DATETIME > _pat.pull_finish.AddHours(-4) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
            }
            return null;
        }

        // Is this search term a word or a list of words?
        private bool ValueIsAList(string s)
        {
            return (s != null) && s.Contains(',');                //is this a comma-separated list?
        }

        // Break apart a wordlist and prepare its members
        private string[] SplitOnCommaAndPrepareElements(string s)
        {
            // If a word list has a space after each comma, we will get leading blanks in each term
            var arr = s.Split(',');
            for (int i = 0; i <= arr.GetUpperBound(0); i++) {
                arr[i] = arr[i].Trim();                         // get rid of leading blanks
                arr[i] = arr[i].Replace(CHAR_COMMA, "");       // insert a literal comma into search word
                arr[i] = arr[i].ToLower();                      // convert for case-insensitive comparisons
            }
            return arr;
        }

        // Add a basic chart item filter to a LINQ query
        //
        // cat = exact match
        // code_list = exact match; optional "like" match
        // desc_list = "like" match; optional exact match
        // field = exact match
        // result_list = "like" match; optional exact match
        //
        // These are comma-separated lists.  
        // Search for a literal comma by using CHAR_COMMA in the string.
        private IEnumerable<CHART_ITEM> AndItemFilter(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list)
        {
            // Converted all values to lower case for case-insensitive comparisons.
            if (!String.IsNullOrEmpty(cat))
            {
                cat = cat.ToLower();
                query = query.Where(e => e.CATEGORY.ToLower() == cat);
            }

            if (!String.IsNullOrEmpty(code_list))
            {
                code_list = code_list.ToLower();
                if (code_list.Left(2) == CODE_LIKE_PREFIX)
                {            // override for "like" match?
                    query = query.Where(e => e.CODE.Contains(code_list.Substring(2)));
                }
                else if (code_list.Left(2) == EXACT_MATCH_PREFIX)
                {
                    query = query.Where(e => e.CODE == code_list.Substring(2));
                }
                else if (ValueIsAList(code_list))
                {
                    query = AndCodeInList(query, code_list);            // find one of the words
                }
                else
                {
                    query = query.Where(e => e.CODE.StartsWith(code_list));      // find this word
                }
            }

            if (!String.IsNullOrEmpty(desc_list))
            {
                desc_list = desc_list.ToLower();
                if (desc_list.Left(2) == EXACT_MATCH_PREFIX)
                {          // exact match override?
                    query = query.Where(e => e.DESCRIPTION == desc_list.Substring(2));
                }
                else if (desc_list.Left(2) == NOT_PREFIX)
                {
                    // query = query.Where(e => !e.DESCRIPTION.Contains(desc_list.Substring(2)));
                    query = AndDescriptionNOTInList(query, desc_list.Substring(2));
                }
                else if (ValueIsAList(desc_list))
                {
                    query = AndDescriptionInList(query, desc_list);
                }
                else
                {
                    desc_list = desc_list.Replace(CHAR_COMMA, "");     // look for literal comma requests
                    query = query.Where(e => e.DESCRIPTION.Contains(desc_list));
                }
            }

            if (!String.IsNullOrEmpty(field))
            {
                field = field.ToLower();
                query = query.Where(e => e.FIELD_NAME == field);
            }

            if (!String.IsNullOrEmpty(result_list))
            {
                query = query.Where(e => e.RESULT != null);
                result_list = result_list.ToLower();                        // make all results case insensitve
                if (result_list.Left(2) == EXACT_MATCH_PREFIX)
                {     // exact match override?
                    query = query.Where(e => e.RESULT == result_list.Substring(2));
                }
                else if (result_list.Left(2) == AVOID_NEGATIVE)
                {
                    query = query.Where(e => (!e.RESULT.Contains("no " + result_list.Substring(2))) && ((e.RESULT == result_list.Substring(2)) || (e.RESULT.Contains(";" + result_list.Substring(2)))));
                }
                else if (ValueIsAList(result_list))
                {
                    query = AndResultInList(query, result_list);
                }
                else if (result_list.Left(2) == STARTS_WITH)
                {
                    query = query.Where(e => e.RESULT.StartsWith(result_list.Substring(2)));
                }
                else
                {
                    result_list = result_list.Replace(CHAR_COMMA, "");     // look for literal comma requests
                    query = query.Where(e => e.RESULT.Replace(",", "").Contains(result_list));
                }
            }
            return query;
        }

        // Look for a code that is one of the words in the list.
        private IEnumerable<CHART_ITEM> AndCodeInList(IEnumerable<CHART_ITEM> query, string code_list)
        {
            if (String.IsNullOrEmpty(code_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(code_list);
            //            return query.Where(e => arr.Any(x => x.Contains(e.CODE))); // "like" match
            return query.Where(e => e.CODE.ContainsAny(arr)); // "like" match
        }

        // Look for a description that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndDescriptionInList(IEnumerable<CHART_ITEM> query, string desc_list)
        {
            if (String.IsNullOrEmpty(desc_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(desc_list);
            //return query.Where(e => arr.Any(x => x.Contains(e.DESCRIPTION))); // "like" match
            query = query.Where(e => e.DESCRIPTION.ContainsAny(arr)); // "like" match
            //Program.VerboseAudit("AndDescriptionInList ct=" + query.Count() + " desclist="+desc_list);
            return query;
            //return query.Where(e => e.DESCRIPTION.ContainsAny(arr)); // "like" match
        }
        private IEnumerable<CHART_ITEM> AndDescriptionNOTInList(IEnumerable<CHART_ITEM> query, string desc_list)
        {
            if (String.IsNullOrEmpty(desc_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(desc_list);
            //return query.Where(e => arr.Any(x => x.Contains(e.DESCRIPTION))); // "like" match
            return query.Where(e => !e.DESCRIPTION.ContainsAny(arr)); // "like" match
        }

        // Look for a result that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultInList(IEnumerable<CHART_ITEM> query, string result_list)
        {
            bool use_start_with = false;

            if (String.IsNullOrEmpty(result_list)) return query;

            if (result_list.Left(2) == STARTS_WITH)
            {
                use_start_with = true;
                result_list = result_list.Substring(2);
            }

            var arr = SplitOnCommaAndPrepareElements(result_list);
            //            return query.Where(e => arr.Any(x => x.Contains(e.RESULT))); // "like" match
            if (use_start_with)
                return query.Where(e => arr.Any(item => e.RESULT.StartsWith(item)));
            else
                return query.Where(e => e.RESULT.ContainsAny(arr)); // "like" match
                                                                    //query = query.Where(e => (meds_mr2026.Any(item => e.DESCRIPTION.ToUpper().StartsWith(item))
                                                                    //return query.Where(e => arr.Any(item => e.RESULT.ToLower().Contains(item.ToLower())));
        }

        // Look for a result that does not contain one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultNotInList(IEnumerable<CHART_ITEM> query, string result_list)
        {
            if (String.IsNullOrEmpty(result_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(result_list);
            //            return query.Where(e => !arr.Any(x => x.Contains(e.RESULT))); // "like" match
            return query.Where(e => !e.RESULT.ContainsAny(arr)); // "like" match
        }

        private string DescribeSearchDepth(SearchDepth search_depth)
        {
            string result = "";
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;

            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    //result = "in pull range"
                    result = "";                         //be quiet since this is default
                    break;
                case SearchDepth.SearchSinceArrival:
                    result = "since arrival to unit";
                    break;
                case SearchDepth.SearchSinceAdmission:
                    result = "since admission";
                    break;
                case SearchDepth.SearchPullPlus:
                    result = "in LOS range plus 4 hours after";
                    break;
                case SearchDepth.SearchSince24Hrs:
                    result = "since 24 hours ago";
                    break;
                case SearchDepth.SearchSince16Hrs:
                    result = "since 16 hours ago";
                    break;
                case SearchDepth.SearchSince13Hrs:
                    result = "since 13 hours ago";
                    break;
                case SearchDepth.SearchSince9Hrs:
                    result = "since 9 hours ago";
                    break;
                case SearchDepth.SearchSince4Hrs:
                    result = "since 4 hours ago";
                    break;
            }

            return result;
        }

        // Describe a query that has already been run
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Make a simple query and describe the results
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            var arr = query.ToArray();
            return Describe(arr, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private string LookingFor(string so_far, string what, string op, string value)
        {
            string result;
            if (String.IsNullOrEmpty(value)) return so_far;

            if (so_far == "")
                result = "look for " + what;
            else
                result = so_far + " and " + what;

            if (value.Left(2) == EXACT_MATCH_PREFIX)
                result += "=" + value.Substring(2) + "";
            else if (value.Left(2) == CODE_LIKE_PREFIX)
                result += " contains " + "" + value.Substring(2) + "";
            else if (ValueIsAList(value))
                result += " in " + value + "";
            else
                result += op + "" + value + "";

            return result;
        }
        private string Describe(CHART_ITEM[] arr, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string result = "";
            //result = LookingFor(result, "cat", "=", cat);
            result = LookingFor(result, "code", "=", code_list);
            //result = LookingFor(result, "desc", " contains ", desc_list.Left(20));
            //result = LookingFor(result, "field", "=", field);
            //result = LookingFor(result, "result", " contains ", result_list.Left(20));

            if (arr.Count() == 0) {
                result += "; not found " + DescribeSearchDepth(search_depth);
            } else {
                var e = arr[0];
                result = "FOUND: " + result;
                // We might have searched for a pattern or word list in several fields - show what was found
                //if (e.CATEGORY != null && e.CATEGORY != "") result += " cat=" + e.CATEGORY + "";
                if (e.CODE != null && e.CODE != "") result += " code=" + e.CODE + "";
                if (e.DESCRIPTION != null && e.DESCRIPTION != "") result += " desc=" + e.DESCRIPTION + "";
                //if (e.FIELD_NAME != null && e.FIELD_NAME != "") result += " field=" + e.FIELD_NAME + "";
                if (e.RESULT != null && e.RESULT != "") result += " result=" + e.RESULT + "";
                // Are there more results?  Just say how many; we aren't going to list them.
                if (arr.Count() == 2) {
                    result += " (1 more result)";
                } else if (arr.Count() > 2) {
                    result += " (" + (arr.Count() - 1) + " more results)";
                }
            }

            return result;
        }


        //Set an indicator for this reason (low level)
        private void SetInd(int inum, string reason)
        {
            if (inum <= 0)
            {
                Program.VerboseAudit(reason); // dont set indicator, just output reason
                return;
            }
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Set Ind #" + inum + ": " + reason);          //already set - repeat for verbose only
            } else {
                _inds[inum].is_checked = true;
                Program.Audit("Set Ind #" + inum + ": " + reason);
            }
        }

        //Clear an indicator for this reason (low level)
        private void ClrInd(int inum, string reason)
        {
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Clr Ind #" + inum + ": " + reason);          //already clear - repeat verbose only
            } else {
                _inds[inum].is_checked = false;
                Program.Audit("Clr Ind #" + inum + ": " + reason);
            }
        }

        // Count how many items meet the conditions
        // All items are listed
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountItems(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)

        {
            bool first = true;
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            if (exclude_periph_iv)
                query = AndDescriptionNOTInList(query, "peripheral iv");
            int count = query.Count();
            found_what = "";
            if (count > 0 && trace)
            {
                foreach (var item in query)
                {
                    if (first)
                    {
                        // always return what was found
                        //            found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                        found_what = "Found desc=" + item.DESCRIPTION + ";result=" + item.RESULT + ";code=" + item.CODE + ";evdt=" + item.EVENT_DATETIME + ";items found=" + count;
                        // echo the result?
                        Program.VerboseAudit(found_what);
                        first = false;
                    }

                }
            }

            return count;
        }


        // Count how many items meet the conditions
        // This has the option to list every entry or give a total
        private int CountResultInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            int count = 0;
            found_what = "";
            string s = "";

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            if (exclude_periph_iv)
                query = AndDescriptionNOTInList(query, "peripheral iv");
            var arr = SplitOnCommaAndPrepareElements(result_list);
            //Walker Schlundt
            //Set Ind #9: found '' in cat='' code='301870' field='' result='oriented x4' 

            foreach (var item in query) {
                // Figure out which of the search words were found
                for (int i = 0; i <= arr.GetUpperBound(0); i++)
                {
                    if (item.RESULT.Contains(arr[i]))
                    {
                        count++;
                        s = arr[i];
                        found_what = "Found desc:" + item.DESCRIPTION + ";result=" + item.RESULT + ";code=" + item.CODE + "; evdt=" + item.EVENT_DATETIME;
                        //if (trace) Program.VerboseAudit(found_what);
                        Program.VerboseAudit(found_what);
                    }
                }
                if (count_mode != CountMode.CountAll) break;        //stop counting

            }

            //if (count > 0) {
            //    //We already printed what was found; maybe add how many?
            //    if (trace && count > 0) Program.VerboseAudit("found " + count + " total");
            //} else {
            //    // Describe what was *not* found
            //    //found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
            //    //if (trace) Program.VerboseAudit(found_what);
            //}

            return count;
        }

        // Count how many items meet the conditions
        // This has the option to list every entry or give a total
        private int CountResultInListEXCEPTList(string cat, string code_list, string desc_list, string field, string result_list, string negresult_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            int count = 0;
            found_what = "";

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            query = AndResultNotInList(query, negresult_list);

            var arr = SplitOnCommaAndPrepareElements(result_list);

            foreach (var item in query)
            {
                // Figure out which of the search words were found
                var s = item.RESULT.ContainsWhich(arr);
                found_what = "found " + s + "' in cat=" + item.CATEGORY + "' code=" + item.CODE + "' field=" + item.FIELD_NAME + "' result=" + item.RESULT + "";
                if (trace) Program.VerboseAudit(found_what);
                count++;
                if (count_mode != CountMode.CountAll) break;        //stop counting
            }

            if (count > 0)
            {
                //We already printed what was found; maybe add how many?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            }
            else
            {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        // Count how many items contain ALL results in list
        private int CountResultInListAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            int count = 0;
            found_what = "";

            var arr = SplitOnCommaAndPrepareElements(result_list);

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            Program.VerboseAudit("arr ub=" + arr.GetUpperBound(0));
            for (int i = 0; i <= arr.GetUpperBound(0); i++)
            {
                if (arr[i].StartsWith(NOT_PREFIX))
                {
                    //query = query.Where(e => !e.RESULT.Contains(arr[i].Substring(2)));
                    query = AndResultNotInList(query, arr[i]);
                }
                else
                {
                    Program.VerboseAudit(i + ":" + arr[i]);
                    //query = query.Where(e => e.RESULT.Contains(arr[i]));
                    query = AndResultInList(query, arr[i]);
                }
            }
            //            Program.VerboseAudit("out of for loop");

            count = query.Count();
            //          Program.VerboseAudit("query count = " +count);

            if (count > 0)
            {
                found_what = "found item with all results in " + result_list + "' without those prefixed by !!";
                if (trace) Program.VerboseAudit(found_what);
            }
            else
            {
                // Describe what was *not* found
                //found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                //if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            if (ValueIsAList(result_list))
            {
                return CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what);
            } else {
                return CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what);
            }
        }

        // Is there an item with this result?
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContains(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            if (ValueIsAList(result_list))
            {
                return (CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            } else {
                return (CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
            }
        }

        // Result contains all in list
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            if (ValueIsAList(result_list))
            {
                return (CountResultInListAll(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            }
            else
            {
                found_what = "Not valid for non-list results.";
                return false;
            }
        }
        // Count how many items meet the conditions, with none of the results in result_list.
        // This has the option to say how many were found rather than list all.
        private int CountResultNotInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            int count = 0, rec_count = 0;
            bool found_one;

            found_what = "";
            var arr = SplitOnCommaAndPrepareElements(result_list);         // also deals with CHAR_COMMA

            //Do not filter by result here; we need all results
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            foreach (var item in query) {
                rec_count++;
                //Look for each search word in the result
                found_one = false;

                foreach (string s in arr) {
                    //                    if (String.Equals(item.RESULT, s)) {
                    if (item.RESULT.Contains(s))
                    {
                        found_what = "found " + s + ";result=" + item.RESULT + " -- exclude this";
                        Program.VerboseAudit(found_what);
                        found_one = true;
                        break;
                    }
                }

                if (!found_one) {
                    //none of the words were found - good!
                    found_what = Describe(cat, code_list, desc_list, field, "", search_depth) + " -- does not contain " + result_list + "";
                    Program.VerboseAudit(found_what);
                    count++;
                    if (count_mode != CountMode.CountAll) break;
                }
            }

            if (count > 0) {
                //We already printed what was found; maybe show the total?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else if (rec_count > 0) {
                //We already printed what we ignored
            } else {
                //Print what did not exist
                found_what = Describe(cat, code_list, desc_list, field, "", search_depth);      //not found
                Program.VerboseAudit(found_what);
            }

            return count;
        }

        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, string found_what)
        {
            // Search in the pull range with trace on
            return CountResultDoesNotContain(cat, code_list, desc_list, field, result_list, out found_what, SearchDepth.SearchDefault, true);
        }
        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, out string found_what, SearchDepth search_depth, bool trace)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            return CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0;
        }

        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            return (CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
        }

        // Set the indicator if the conditions are met
        //
        private bool SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SearchDepth s = SearchDepth.SearchDefault;
            //if (inum == 5) s = SearchDepth.SearchSince13Hrs;
            //if (inum == 6) s = SearchDepth.SearchSince13Hrs;
            //if (inum == 7) s = SearchDepth.SearchSince13Hrs;
            //if (inum == 8) s = SearchDepth.SearchSince16Hrs;
            //if (inum == 9) s = SearchDepth.SearchSince16Hrs;
            //if (inum == 10) s = SearchDepth.SearchSince13Hrs;
            //if (inum == 11) s = SearchDepth.SearchSince9Hrs;
            //if (inum == 12) s = SearchDepth.SearchSince16Hrs;
            //if (inum == 13) s = SearchDepth.SearchSince16Hrs;
            //if (inum == 14) s = SearchDepth.SearchSince24Hrs;
            //if (inum == 19) s = SearchDepth.SearchSince9Hrs;
            //if (inum == 20) s = SearchDepth.SearchSince9Hrs;
            //if (inum == 21) s = SearchDepth.SearchSince16Hrs;
            //if (inum == 22) s = SearchDepth.SearchSince13Hrs;
            //if (inum == 23) s = SearchDepth.SearchSince9Hrs;
            //if (inum == 24) s = SearchDepth.SearchSince9Hrs;
            return SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, s);

        }
        private bool SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            bool b = false;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return true;

            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what)) {
                SetInd(inum, found_what);                          //echo here - found
                b = true;
            } else {
                //   Program.VerboseAudit(found_what);                  //and here - not found
            }
            return b;
        }

        private void SetIndIfResultBetween(int inum, string cat, string code_list, string desc_list, string field, int loval, int hival)
        {
            SetIndIfResultBetween(inum, cat, code_list, desc_list, field, loval, hival, SearchDepth.SearchDefault);
        }

        private void SetIndIfResultBetween(int inum, string cat, string code_list, string desc_list, string field, int loval, int hival, SearchDepth search_depth)
        {
            int count = 0;
            string found_what = "";

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");
            foreach (var item in query)
            {
                Program.VerboseAudit("ResBetween: code=" + item.CODE + " result=" + item.RESULT);
                if (item.RESULT.IsNumeric())
                {
                    Program.VerboseAudit("  result is numeric");
                    if (item.RESULT.Val() >= loval && item.RESULT.Val() <= hival)
                    {
                        count++;
                        found_what = "found code=" + item.CODE + " with result=" + item.RESULT;
                        Program.VerboseAudit(found_what);
                    }
                }
            }

            if (count > 0)
            {
                SetInd(inum, found_what);
            }
        }


        // Special contains all
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SearchDepth s = SearchDepth.SearchDefault;
            //if (inum == 8) s = SearchDepth.SearchSince24Hrs;
            SetIndIfResultContainsAll(inum, cat, code_list, desc_list, field, result_list, s);
        }
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return;

            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                          //echo here - found
            }
            else
            {
                //Program.VerboseAudit(found_what);                  //and here - not found
            }
        }
        // Set the indicator if the result does not contain any of the words in result_list
        //
        private void SetIndIfResultDoesNotContain(int inum, string cat, string desc_list, string code_list, string field, string result_list)
        {
            SetIndIfResultDoesNotContain(inum, cat, desc_list, code_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultDoesNotContain(int inum, string cat, string desc_list, string code_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return;

            //Turn trace off for ResultDoesNotContain() and echo what was set below with SetInd
            if (ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                           //echo here - not found
            } else {
                //Program.VerboseAudit(found_what);                   //and here - found
            }
        }

        // Clear the indicator if the result contains one of the words in the result_list
        //
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            //avoid more queries if the indicator is already clear
            if (!_inds[inum].is_checked) return;

            //Turn trace off for ResultContains() and echo what was set below with SetInd
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what)) {
                ClrInd(inum, found_what);                           //echo here - found
            } else {
                Program.VerboseAudit(found_what);                   //and here - not found
            }
        }


        //These slightly smaller functions are meant for places where you aren't really looking for a chart result
        private bool Exists(string desc_list, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists("", "", desc_list, "", result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool Exists(string desc_list, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists("", "", desc_list, "", result_list, search_depth, true, out found_what);
        }
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            return (CountResultContains(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
        }

        // Variations with optional parameters (can switch to optional parameters in VS 2010)
        //
        private void SetIndIfFound(int inum, string cat)
        {
            SetIndIfResultContains(inum, cat, "", "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list)
        {
            SetIndIfResultContains(inum, cat, code_list, "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private void ClrIndIfFound(int inum, string cat)
        {
            ClrIndIfResultContains(inum, cat, "", "", "", "", SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Get the max/total value from a result (usually in the middle of the text)
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetIntValue(get_mode, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            int value;
            int result = 0;
            bool found_one = false;

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);

            //Look for a number in the result

            foreach (var item in query) {
                // RESULT might be null so convert to empty string so Split won't blow up
                var arr = PFSDBUtility.DBToString(item.RESULT).Split(' ');
                foreach (string s in arr) {
                    //Look for 1st character numeric; IsNumeric("60min") will fail so look at 1st char only
                    if (s.Left(1).IsNumeric()) {
                        value = (int)s.Val();                           //Use Val; ToInteger will error on "60min"
                        switch (get_mode) {
                            case GetValueMode.GetMax:
                                result = Math.Max(value, result);       //max
                                break;
                            case GetValueMode.GetTotal:
                                result += value;                        //total
                                break;
                            case GetValueMode.GetLast:
                                result = value;                         //last
                                break;
                        }

                        // print what we are searching for (the first time)
                        if (!found_one) {
                            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
                        }
                        found_one = true;
                        // print each value found
                        Program.VerboseAudit("  found numeric value " + result);
                        //Keep going in case there are more
                    }
                }
            }

            if (!found_one) {
                //show what was not found
                Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
            }

            return result;
        }

        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetMaxValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            return GetIntValue(GetValueMode.GetMax, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetTotalValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            return GetIntValue(GetValueMode.GetTotal, cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Get a result; returns true if found with return_result
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result)
        {
            return GetResult(cat, code_list, desc_list, field, out return_result, SearchDepth.SearchDefault);
        }
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0) {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
            } else {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }

        private bool GetLatestResult(string code_list, out string return_result, SearchDepth search_depth)
        {
            return GetLatestResult("", code_list, "", "", out return_result, search_depth);
        }
        private bool GetLatestResult(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");
            query = query.OrderByDescending(e => e.EVENT_DATETIME);
            //FOUND: look for code=Level of Care code=level of care result=icu (10 more results)
            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
                Program.VerboseAudit("code=" + code_list + " result=" + return_result + " charted at " + query.First().EVENT_DATETIME);
            }
            else
            {
                return_result = "";
            }

            return (!String.IsNullOrEmpty(return_result));
        }

        private bool GetResultForInspection(string cat, string code_list, string desc_list, string field, string result, out string return_result, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result);

            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
            }
            else
            {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }

        private void GetResultforOther(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "Other");
            return_result = "";
            foreach (var item in query)
            {
                if (!String.Equals(item.RESULT, "Other"))
                {
                    return_result = item.RESULT;
                    //Program.VerboseAudit(found_what);
                    break;
                }

            }
            return;
        }

        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, string res, out string return_result, out DateTime return_evdt)
        {
            return GetResultAndEVDT(cat, code_list, desc_list, field, res, out return_result, out return_evdt, SearchDepth.SearchDefault);
        }
        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, string res, out string return_result, out DateTime return_evdt, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, res);

            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
                return_evdt = PFSDBUtility.DBToDateTime(query.First().EVENT_DATETIME);
            }
            else
            {
                return_result = "";
                return_evdt = DateTime.MinValue;
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }
        private bool GetEVDT(string cat, string code_list, string desc_list, string res, string field, int comparison, DateTime compevdt, out DateTime return_evdt)
        {
            return GetEVDT(cat, code_list, desc_list, field, res, comparison, compevdt, out return_evdt, SearchDepth.SearchDefault);
        }
        private bool GetEVDT(string cat, string code_list, string desc_list, string field, string res, int comparison, DateTime compevdt, out DateTime return_evdt, SearchDepth search_depth)
        {
            bool sort_ascending = false;
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, res);
            if (compevdt != DateTime.MinValue)
            {
                if (comparison == 0) // equal
                    query = query.Where(e => e.EVENT_DATETIME == compevdt);
                else if (comparison == 1) // GTE
                    query = query.Where(e => e.EVENT_DATETIME >= compevdt);
                else if (comparison == 2) // GT
                    query = query.Where(e => e.EVENT_DATETIME > compevdt);
                else if (comparison == 22) // Least GT
                {
                    query = query.Where(e => e.EVENT_DATETIME > compevdt);
                    sort_ascending = true;
                }
                else if (comparison == 3) // LT
                    query = query.Where(e => e.EVENT_DATETIME < compevdt);
            }
            if (sort_ascending)
                query = query.OrderBy(e => e.EVENT_DATETIME);
            else
                query = query.OrderByDescending(e => e.EVENT_DATETIME);
            Program.VerboseAudit("getevdt count=" + query.Count());
            if (query.Count() > 0)
            {
                return_evdt = PFSDBUtility.DBToDateTime(query.First().EVENT_DATETIME);
            }
            else
            {
                return_evdt = DateTime.MinValue;
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, res, search_depth));
            return (return_evdt != DateTime.MinValue);
        }

        private int SetIndIfCodeBtwn(int ind, int locode, int hicode, SearchDepth search_depth)

        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = query.Where(e => e.CODE.ToLower().StartsWith("edu"));
            query = query.Where(e => e.CODE.Length >= 12);
            query = query.Where(e => e.CODE.Substring(3, 9).Val() >= locode);
            query = query.Where(e => e.CODE.Substring(3, 9).Val() <= hicode);
            int count = query.Count();
            // always return what was found
            string found_what = "There were " + count + " items found with code between " + locode + " and " + hicode;
            if (count >= 8) SetInd(ind, found_what);
            // echo the result?
            //               if (trace) Program.VerboseAudit(found_what);

            return count;
        }

        private DateTime LatestEVDT(string code_list, string desc_list, string res, SearchDepth search_depth)
        {
            DateTime latestdt = DateTime.MinValue;

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, "", code_list, desc_list, "", res);
            query = query.OrderByDescending(e => e.EVENT_DATETIME);
            int ct = query.Count();

            if (ct > 0)
            {
                latestdt = PFSDBUtility.DBToDateTime(query.First().EVENT_DATETIME);
            }
            else
            {
                latestdt = DateTime.MinValue;
            }
            return latestdt;
        }

        private int CountItemsWithinNhours(string code_list, string result_list, SearchDepth search_depth, int Nhrs)
        {
            int ctwithin = 0;
            int ct = 0;
            DateTime evdt = DateTime.MinValue;

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, "", code_list, "", "", result_list);
            query = query.OrderBy(e => e.EVENT_DATETIME);
            int count = query.Count();
            if (count > 0)
            {
                foreach (var item in query)
                {
                    if (item.EVENT_DATETIME > evdt)
                    {
                        evdt = item.EVENT_DATETIME;

                        var q1 = StartNewQuery(search_depth);
                        q1 = AndItemFilter(q1, "", code_list, "", "", result_list);
                        q1 = q1.Where(e => e.EVENT_DATETIME >= item.EVENT_DATETIME && e.EVENT_DATETIME <= item.EVENT_DATETIME.AddHours(Nhrs));

                        ct = q1.Count();
                        if (ct > ctwithin) ctwithin = ct;
                    }
                }
            }
            return ctwithin;
        }

        private bool GetEVDTIfAllResults(string code_list, string result_list, int comparison, DateTime compevdt, out DateTime return_evdt, SearchDepth search_depth)
        {
            var arr = SplitOnCommaAndPrepareElements(result_list);

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, "", code_list, "", "", "");
            for (int i = 0; i <= arr.GetUpperBound(0); i++)
            {
                query = AndResultInList(query, arr[i]);
            }
            int count = query.Count();

            if (compevdt != DateTime.MinValue)
            {
                if (comparison == 0) // equal
                    query = query.Where(e => e.EVENT_DATETIME == compevdt);
                else if (comparison == 1) // GTE
                    query = query.Where(e => e.EVENT_DATETIME >= compevdt);
                else if (comparison == 2) // GT
                    query = query.Where(e => e.EVENT_DATETIME > compevdt);
                else if (comparison == 22) // Least GT
                {
                    query = query.Where(e => e.EVENT_DATETIME > compevdt);
                }
                else if (comparison == 3) // LT
                    query = query.Where(e => e.EVENT_DATETIME < compevdt);
            }
            query = query.OrderBy(e => e.EVENT_DATETIME);
            Program.VerboseAudit("getevdt count=" + query.Count());
            if (query.Count() > 0)
            {
                return_evdt = PFSDBUtility.DBToDateTime(query.First().EVENT_DATETIME);
            }
            else
            {
                return_evdt = DateTime.MinValue;
            }

            Program.VerboseAudit(Describe("", code_list, "", "", "", search_depth));
            return (return_evdt != DateTime.MinValue);
        }

        //======================================================================================
        // Mapping starts here
        //======================================================================================

        private void Check_1_2_3_4()
        {
            Program.VerboseAudit("Default ADL Search Scope = " + _pat.pull_finish.AddHours(-24) + " to " + _pat.pull_finish);


            Program.VerboseAudit("---------------");
            Program.VerboseAudit("1. ADL Self");
            Program.VerboseAudit("2. ADL Assist");
            Program.VerboseAudit("3. ADL Extended");
            Program.VerboseAudit("4. ADL Complete");
            Program.VerboseAudit("---------------");

            string desc_found = "";
            string reslist = "";

            bool feed1 = false;
            bool feed2 = false;
            bool feed3 = false;
            bool feed4 = false;

            bool A1 = false;
            bool A2 = false;
            bool A3 = false;
            bool A4 = false;

            bool mainhyg1 = false;
            bool mainhyg2 = false;
            bool mainhyg3 = false;
            bool mainhyg4 = false;

            bool hyg1 = false;
            bool hyg2 = false;
            bool hyg3 = false;
            bool hyg4 = false;

            bool mob1 = false;
            bool mob2 = false;
            bool mob3 = false;
            bool mob4 = false;

            reslist = "independent";
            A1 = Exists("", "12026547", "", "", reslist);
            reslist = "minimum assist,moderate assist";
            A2 = Exists("", "12026547", "", "", reslist);
            reslist = "maximum assist";
            A3 = Exists("", "12026547", "", "", reslist);
            reslist = "dependent";
            A4 = Exists("", "12026547", "", "", STARTS_WITH + reslist);
            //A4 = Exists("", "12026547", "", "", reslist);
            Program.VerboseAudit("12026547: 1=" + A1 + " 2=" + A2 + " 3=" + A3 + " 4=" + A4);

            //mainfeed
            reslist = "none";
            feed1 = Exists("", "12025931", "", "", reslist);
            reslist = "tray set-up";
            feed2 = Exists("", "12025931", "", "", reslist);
            reslist = "meal set-up provided,supplemental drinks provided,supplemental foods provided";
            feed2 |= Exists("", "1224006", "", "", reslist);
            reslist = "assisted with feeding";
            feed3 = Exists("", "12025931", "", "", reslist);
            reslist = "total feed";
            feed4 = Exists("", "12025931", "", "", reslist);

            if (_pat.age < 4.0)
            {
                feed4 = true;
                SetInd(4, "ADL complete due to age<4=" + _pat.age);
            }

            Program.VerboseAudit("OVERALL FEED: 1=" + feed1 + " 2=" + feed2 + " 3=" + feed3 + " 4=" + feed4);

            //mainhyg
            reslist = "bathed,showered,pericare,hair washed,nail care,shaved,other (comment) ";
            hyg1 = A1 && Exists("", "12052027", "", "", reslist);
            reslist = "bathed,showered,chlorhexidine bath,back rub,foley care,pericare,hair washed,nail care,shaved,other (comment) ";
            hyg2 = A2 && Exists("", "12052027", "", "", reslist);

            reslist = "chlorhexidine bath, back rub,foley care";
            hyg2 = Exists("", "12052027", "", "", reslist); //because what if independent+foley

            reslist = "bathed,showered,chlorhexidine bath,back rub,foley care,pericare,hair washed,nail care,shaved,other (comment) ";
            hyg3 = A3 && Exists("", "12052027", "", "", reslist);
            reslist = "bathed,chlorhexidine bath,back rub,foley care,pericare,hair washed,nail care,shaved,other (comment) ";
            hyg4 = A4 && Exists("", "12052027", "", "", reslist);
            Program.VerboseAudit("hyg: 1=" + hyg1 + " 2=" + hyg2 + " 3=" + hyg3 + " 4=" + hyg4);

            bool oral1 = false;
            bool oral2 = false;
            bool oral3 = false;
            bool oral4 = false;
            reslist = "teethbrushed,denture care,lip moisturizer applied,mouth swabbed,mouth moisturizer,mouth rinsed,mouth suctioned,suction toothette,suction toothette toothbrush,with chlorhexidine,with half strength hydrogen peroxide,with mouth wash,with normal saline,with baking soda solution,other (comment)";
            bool oral = Exists("", "12042967", "", "", reslist);
            if (oral)
            {
                oral1 = A1;
                oral2 = A2;
                oral3 = A3;
                oral4 = A4;
            }
            Program.VerboseAudit("oralhyg: 1=" + oral1 + " 2=" + oral2 + " 3=" + oral3 + " 4=" + oral4);

            bool peri1 = false;
            bool peri2 = false;
            bool peri3 = false;
            bool peri4 = false;
            reslist = "applied,changed,removed,absorbent pad,absorbent undergarment,diaper,incontinence pouch,panty liner,protective pad,sanitary pad,training pants,other (comment)";
            bool peri = Exists("", "12039320", "", "", reslist);
            if (peri)
            {
                peri1 = A1;
                peri2 = A2;
                peri3 = A3;
                peri4 = A4;
            }
            Program.VerboseAudit("perihyg: 1=" + peri1 + " 2=" + peri2 + " 3=" + peri3 + " 4=" + peri4);

            bool skin1 = false;
            bool skin2 = false;
            bool skin3 = false;
            bool skin4 = false;
            reslist = "foam skin cleanser,granulex spray,incontinent cleanser,moisture barrier,protective barrier,wound cleanser,linens changed,other (comment)";
            bool skin = Exists("", "12216704", "", "", reslist);
            if (skin)
            {
                skin1 = A1;
                skin2 = A2;
                skin3 = A3;
                skin4 = A4;
            }
            Program.VerboseAudit("skinhyg: 1=" + skin1 + " 2=" + skin2 + " 3=" + skin3 + " 4=" + skin4);

            mainhyg4 = hyg4 || oral4 || peri4 || skin4;
            mainhyg3 = hyg3 || oral3 || peri3 || skin3;
            mainhyg2 = hyg2 || oral2 || peri2 || skin2;
            mainhyg1 = hyg1 || oral1 || peri1 || skin1;
            Program.VerboseAudit("OVERALL HYG: 1=" + mainhyg1 + " 2=" + mainhyg2 + " 3=" + mainhyg3 + " 4=" + mainhyg4);

            //mainmob
            reslist = "4-->no limitation";
            bool brad1 = Exists("", "1222725", "", "", reslist);
            reslist = "3-->slightly limited";
            bool brad2 = Exists("", "1222725", "", "", reslist);
            reslist = "2-->very limited";
            bool brad3 = Exists("", "1222725", "", "", reslist);
            reslist = "1-->completely immobile";
            bool brad4 = Exists("", "1222725", "", "", reslist);
            Program.VerboseAudit("bradmob: 1=" + brad1 + " 2=" + brad2 + " 3=" + brad3 + " 4=" + brad4);

            bool mobshf1 = false;
            bool mobshf2 = false;
            bool mobshf3 = false;
            bool mobshf4 = false;
            reslist = "able to walk, w/ or w/o assist";
            mobshf1 = A1 && Exists("", "12047554", "", "", reslist);
            reslist = "able to walk, w/ or w/o assist,transfer to chair,dangle, move leg against gravity,able to sit upright in bed, self supported,unable to sit upright & supported by bed";
            mobshf2 = A2 && Exists("", "12047554", "", "", reslist);
            reslist = "able to walk, w/ or w/o assist,transfer to chair,dangle, move leg against gravity,able to sit upright in bed, self supported,unable to sit upright & supported by bed";
            mobshf3 = A3 && Exists("", "12047554", "", "", reslist);
            reslist = "transfer to chair,dangle, move leg against gravity,able to sit upright in bed, self supported,unable to sit upright & supported by bed";
            mobshf4 = A4 && Exists("", "12047554", "", "", reslist);
            Program.VerboseAudit("mobshift: 1=" + mobshf1 + " 2=" + mobshf2 + " 3=" + mobshf3 + " 4=" + mobshf4);

            bool bodypos1 = false;
            bool bodypos2 = false;
            bool bodypos3 = false;
            bool bodypos4 = false;
            reslist = "patient independent";
            bodypos1 = A1 && Exists("", "1223657", "", "", reslist);

            reslist = "turned,position maintained,foot of bed elevated,hands-and-knees,knee-chest position,legs elevated,log-rolled,lower extremity elevated, left,lower extremity elevated, right,neutral body alignment,neutral head position,prone,reverse Trendelenburg,semi-prone, left,semi-prone, right,side-lying, left,side-lying, right,sitting up in bed,squatting,supine,supine, legs elevated,upper extremity elevated, left,upper extremity elevated, right,weight shift assistance provided,other (see comments)";
            bool b1 = Exists("", "1223657", "", "", reslist);
            reslist = "positioned with 1";
            if (Exists("", "12041232", "", "", reslist))
                bodypos2 = b1;

            reslist = "positioned with 2";
            if (Exists("", "12041232", "", "", reslist))
                bodypos3 = b1;

            reslist = "positioned with 3";
            if (Exists("", "12041232", "", "", reslist))
                bodypos4 = b1;
            Program.VerboseAudit("bodypos: 1=" + bodypos1 + " 2=" + bodypos2 + " 3=" + bodypos3 + " 4=" + bodypos4);



            mob4 = brad4 || mobshf4 || bodypos4;
            mob3 = brad3 || mobshf3 || bodypos3;
            mob2 = brad2 || mobshf2 || bodypos2;
            mob1 = brad1 || mobshf1 || bodypos1;
            Program.VerboseAudit("OVERALL MOB: 1=" + mob1 + " 2=" + mob2 + " 3=" + mob3 + " 4=" + mob4);

            //main toileting
            bool toil1 = false;
            bool toil2 = false;
            bool toil3 = false;
            bool toil4 = false;
            reslist = "up to bedside commode,up to toilet,urinal within reach";
            toil1 = A1 && Exists("", "12038800", "", "", reslist);
            reslist = "bed pad changed,bedpan provided,incontinence pad changed,up to bedside commode,up to toilet,urinal provided,urinal within reach,other (see comments)";
            bool t1 = Exists("", "12038800", "", "", reslist);
            toil2 = A2 && t1;
            toil2 |= Exists("", "12046782,12062954,12046765", "", "", "");
            toil3 = A3 && t1;
            toil4 = A4 && t1;

            Program.VerboseAudit("OVERALL TOIL: 1=" + toil1 + " 2=" + toil2 + " 3=" + toil3 + " 4=" + toil4);

            //main dressing
            reslist = "0";
            bool dress1 = Exists("", "1222814", "", "", reslist);
            reslist = "1,2";
            bool dress2 = Exists("", "1222814", "", "", reslist);
            reslist = "3";
            bool dress3 = Exists("", "1222814", "", "", reslist);
            reslist = "4";
            bool dress4 = Exists("", "1222814", "", "", reslist);
            Program.VerboseAudit("OVERALL DRESS: 1=" + dress1 + " 2=" + dress2 + " 3=" + dress3 + " 4=" + dress4);


            if ((feed4 ? 1 : 0) + (mainhyg4 ? 1 : 0) + (mob4 ? 1 : 0) + (toil4 ? 1 : 0) + (dress4 ? 1 : 0) >= 3)
                SetInd(4, "At least 3 ADL categories found at the Complete level.");
            //            SetInd(4, "At least 4 ADL categories found at the Complete level.");
            else if ((feed3 || feed4 ? 1 : 0) + (mainhyg3 || mainhyg4 ? 1 : 0) + (mob3 || mob4 ? 1 : 0) + (toil3 || toil4 ? 1 : 0) + (dress3 || dress4 ? 1 : 0) >= 1)
                SetInd(3, "At least 1 ADL category found at the Extended level.");
            else if ((feed2 || feed3 || feed4 ? 1 : 0) + (mainhyg2 || mainhyg3 || mainhyg4 ? 1 : 0) + (mob2 || mob3 || mob4 ? 1 : 0) + (toil2 || toil3 || toil4 ? 1 : 0) + (dress2 || dress3 || dress4 ? 1 : 0) >= 1)
                SetInd(2, "At least 1 ADL category found at the Partial or Extended or Complete level.");
            else if (feed1 || mainhyg1 || mob1 || toil1)
                SetInd(1, "At least 1 ADL category found at the Independent level.");

            if (!_inds[1].is_checked && !_inds[2].is_checked && !_inds[3].is_checked && !_inds[4].is_checked)
            {
                SetInd(1, "Defaulting to ADL Self.  No documentation supporting higher than independent.");
            }

        }

        private void UpdatePtChartArrays(string orderid, string ordstatus, string newres)
        {
            foreach (var item in _chart_items_since24hrs)
            {
                if (item.ORDER_ID == orderid)
                {
                    if (ordstatus != "")
                        item.ORDER_STATUS = ordstatus;
                    if (newres != "")
                        item.RESULT = newres;
                }
            }
        }

        private void UpdatePtChartArraysCode(string code, string ordstatus, DateTime dcdt)
        {
            foreach (var item in _chart_items_since_admission)
            {
                if (item.CODE == code && item.EVENT_DATETIME <= dcdt)
                {
                    item.ORDER_STATUS = ordstatus;
                }
            }
        }



        private void Check_5()
        {
            string reslist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("5. ADL Rehab");
            Program.VerboseAudit("---------------");

            exclude_periop_data = false;

            reslist = "Bedpan,Commode,Bathroom";
            SetIndIfResultContains(5, "", "122196", "", "", reslist);
            reslist = "Bedside commode,Bedpan,Incontinent,No results,Other";
            SetIndIfResultContains(5, "", "1226709", "", "", reslist);
            reslist = "abdominal binder utilized,abdomen massaged,biofeedback utilized,bowel agents administered,breathing techniques encouraged,colostomy management provided,consistent time/schedule established,device set-up provided,digital stimulation provided,fiber intake encouraged,fluid intake encouraged,upright positioning encouraged,Valsalva maneuver utilized,other";
            SetIndIfResultContains(5, "", "12042499", "", "", reslist);
            reslist = "bedpan,bedside commode,diaper/absorbent pad,ostomy wafer/bag,other";
            SetIndIfResultContains(5, "", "12043025", "", "", reslist);
            reslist = "device set-up provided,fluid intake encouraged,intermittent catheterization,medications used for management,perineal skin care techniques,standing position for voiding utilized,stimulation to initiate voiding utilized,toileting schedule encouraged,voiding diary utilized,other";
            SetIndIfResultContains(5, "", "12040166", "", "", reslist);
            reslist = "bedpan,bedside commode,catheter,diaper/absorbent pad,urinal,other";
            SetIndIfResultContains(5, "", "12037556", "", "", reslist);
            reslist = "Set up assistance,Mod assistance,Total assistance";
            SetIndIfResultContains(5, "", "122174", "", "", reslist);


        }


        private void Check_6_7()
        {
            string reslist;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("6. ADL 2-3 Caregivers");
            Program.VerboseAudit("7. ADL 4 or more Caregivers");
            Program.VerboseAudit("---------------");


            reslist = "moderate assist,maximum assist";
            SetIndIfResultContains(6, "", "12026547", "", "", reslist);
            //reslist = "maximum assist";
            //SetIndIfResultContains(7, "", "12026547", "", "", reslist);

            reslist = "2 people";
            SetIndIfResultContains(6, "", "12026223", "", "", reslist);
            reslist = "3 or more people";
            SetIndIfResultContains(7, "", "12026223", "", "", reslist);

            reslist = "positioned with 2";
            SetIndIfResultContains(6, "", "12041232", "", "", reslist);
            reslist = "positioned with 3";
            SetIndIfResultContains(7, "", "12041232", "", "", reslist);

        }

        private void Check_8()
        {
            string reslist;
            string found_what;
            string return_result;
            DateTime return_evdt;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("8. Communication");
            Program.VerboseAudit("---------------");

            //CUSTOM MAPPING NOTE FOR INDICATOR 8:  Pediatrics = parents with language 
            //other than English trigger Communication Support. 
            //Anyone in a coma/ sedated plus trach ET tube/ Unable to speak would not trigger 
            //unless dealing with family.

            reslist = "cognition,hearing impairment,language,physical,visual impairment,other (see comments)";
            SetIndIfResultContains(8, "", "122259", "", "", reslist);
            reslist = "Not to slightly comfortable";
            SetIndIfResultContains(8, "", "12019684", "", "", reslist);
            reslist = "Yes";
            SetIndIfResultContains(8, "", "12023492", "", "", reslist);
            reslist = "certified interpreter,phone,family member,friend,video,other";
            SetIndIfResultContains(8, "", "1226347", "", "", reslist);
            reslist = "confused,sedated,semicomatose,unresponsive";
            SetIndIfResultContains(8, "", "1222528", "", "", reslist);
            reslist = "endotracheal tube,garbled,illogical,incoherent,unable to speak";
            SetIndIfResultContains(8, "", "1222530", "", "", reslist);
            reslist = "spouse,caregiver,daughter,durable power of attorney,family,father,foster parent,friend,grandchild(ren),grandparent,guardian,mother,parent,sibling,significant other,son,step-parent,other";
            SetIndIfResultContains(8, "", "12025460", "", "", reslist);

            if (_pat.lang_lvc.Length > 0)
            {
                string[] langlist = { "PATIENT REFUSED", "ENGLISH", "NULL", "*" };
                bool found = false;
                foreach (var s in langlist)
                {
                    found |= (_pat.lang_lvc.ToUpper() == s);
                }
                if (!found)
                    SetInd(8, "Language=" + _pat.lang_lvc);
            }

        }



        private void Check_9()
        {
            string reslist;
            bool s2, s3, s4;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("9. Cognitive Support");
            Program.VerboseAudit("---------------");

            reslist = "Yes";
            SetIndIfResultContains(9, "", "12023492", "", "", reslist);
            reslist = "Not to slightly comfortable";
            SetIndIfResultContains(9, "", "12019684", "", "", reslist);
            reslist = "1-Altered awareness of immediate physical environment,2-Impulsive,4-Lack of understanding of one";
            SetIndIfResultContains(9, "", "12046376", "", "", reslist);
            reslist = "disoriented to,disoriented x 4";
            SetIndIfResultContains(9, "", "1222529", "", "", reslist);
            reslist = "Positive";
            SetIndIfResultContains(9, "", "12065364", "", "", reslist);
            reslist = "Positive";
            SetIndIfResultContains(9, "", "12066139", "", "", reslist);
            reslist = "score >/=9";
            SetIndIfResultBetween(9, "", "12025525", "", "", 9, 99, SearchDepth.SearchDefault);
            reslist = "reorientation provided";
            SetIndIfResultContains(9, "", "1223696", "", "", reslist);
            reslist = "short-term loss,long-term loss,forgetful,new learning, recall loss";
            SetIndIfResultContains(9, "", "1222531", "", "", reslist);
            reslist = "4-->combative,3-->very agitated,2-->agitated,1-->restless,-1-->drowsy,-2-->light sedation,-3-->moderate sedation,-4-->deep sedation,-5-->unarousable";
            SetIndIfResultContains(9, "", "12025422", "", "", reslist);
            reslist = ">/=9 ";
            SetIndIfResultBetween(9, "", "12062006", "", "", 9, 99, SearchDepth.SearchDefault);
            reslist = "2-->bio-behavioral concerns/neuro limitations due to illness";
            SetIndIfResultContains(9, "", "12026701", "", "", reslist);
            reslist = " -3 Unresponsive,-2 Responsive to noxious stimuli,+1 Restless and difficult to calm,+2 Agitated";
            SetIndIfResultContains(9, "", "12055258", "", "", reslist);
            reslist = "disoriented to,disoriented x 3,disoriented x 4";
            SetIndIfResultContains(9, "", "12026580", "", "", reslist);
            reslist = "blaming others,denies responsibility,inappropriately focused on discharge,inappropriately goal directed,insight not appropriate to situation,judgment not appropriate to situation";
            SetIndIfResultContains(9, "", "12025599", "", "", reslist);
            reslist = "score >/=5";
            SetIndIfResultBetween(9, "", "12043105", "", "", 5, 99, SearchDepth.SearchDefault);
            reslist = "score >/=3";
            SetIndIfResultBetween(9, "", "12055252", "", "", 3, 99, SearchDepth.SearchDefault);

        }


        private void Check_10_11()
        {
            string reslist;
            bool is_peds = false;
            //int ct;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("10. Behavior/Emotional Management");
            Program.VerboseAudit("11. Behavior/Emotional Mgmt - q 1 Hour");
            Program.VerboseAudit("---------------");


            reslist = "afraid/fearful,anxious,frustrated,irritable,repeated requests,restless,sad,withdrawn";
            int ctA = CountItems("", "1222804", "", "", reslist);
            if (ctA == 2)
                SetInd(10, "Observed emotional state items found="+ctA);
            else if (ctA >= 3)
                SetInd(11, "Observed emotional state items found=" + ctA);


            reslist = "agitated,angry,combative,inconsolable,panic,shocked,tearful/crying,uncooperative";
            SetIndIfResultContains(11, "", "1222804", "", "", reslist);

            reslist = "anxiety,depression,disbelief,frustration,grief,guilt,hopelessness,powerlessness,sadness";
            SetIndIfResultContains(10, "", "1222805", "", "", reslist);
            reslist = "anger,fear,loneliness,suicidal thoughts";
            SetIndIfResultContains(11, "", "1222805", "", "", reslist);

            reslist = "not attentive to patient,no interaction with patient,not participating in care";
            SetIndIfResultContains(10, "", "12043115", "", "", reslist);
            reslist = "disruptive to patient";
            SetIndIfResultContains(11, "", "12043115", "", "", reslist);

            //Trust relationship:  change to x2=reg x3=q1
            reslist = "care explained,choices provided,emotional support provided,empathic listening provided,questions answered,questions encouraged,reassurance provided,thoughts/feelings acknowledged,other";
            //SetIndIfResultContains(10, "", "12025449", "", "", reslist);
            //reslist = "choices provided,emotional support provided";
            //SetIndIfResultContains(11, "", "12025449", "", "", reslist);
            string found_what = "";
            int count = 0;
            count = CountItems("", "12025449", "", "", reslist, SearchDepth.SearchDefault, true, out found_what);
            if (count >= 3)
                SetInd(11, "3 or more items found for Trust Relationship: count=" + count + " found=" + found_what);
            else if (count == 2)
                SetInd(10, "2 items found for Trust Relationship: " + found_what);

            reslist = "caregiver stress acknowledged,family care conference arranged,involvement promoted,presence promoted,self-care encouraged,support provided,other";
            count = CountItems("", "12025465", "", "", reslist, SearchDepth.SearchDefault, true, out found_what);
            if (count >= 3)
                SetInd(11, "3 or more items found for Family/Support System Care: count=" + count + " found=" + found_what);
            else if (count == 2)
                SetInd(10, "2 items found for Family/Support System Care: " + found_what);

            reslist = "coloring/artwork,computer,games,individual hobbies,movies,music,play,playroom/recreation room,puzzles,puzzles,smartphone,tablet,television,toys,video games,other";
            count = CountItems("", "12025464", "", "", reslist, SearchDepth.SearchDefault, true, out found_what);
            if (count >= 4)
                SetInd(11, "4 or more items found for Diversional Activities: count=" + count + " found=" + found_what);
            else if (count >= 2)
                SetInd(10, "2 items found for Diversional Activities: " + found_what);

            //reslist = "any option 4x Q-shift";
            //SetIndIfResultContains(11, "", "12025464", "", "", reslist);
            int ct = CountResultContains("", "12025464", "", "", reslist);
            if (ct >= 4)
                SetInd(11, "Count=" + ct + " of 12025464 with " + reslist);

            reslist = "body image/self-concept,financial impact,functional changes,loss of normal life,loss of control,new medical regimes,re-evaluation of priorities/goals/dreams,school/education changes,sexuality,side effects of treatment/procedure,caregiver,care-receiver,family,professional,social,other (see comments)";
            SetIndIfResultContains(10, "", "12025459", "", "", reslist);
            //reslist = "any option 4x Q-shift";
            //SetIndIfResultContains(11, "", "12025459", "", "", reslist);
            ct = CountResultContains("", "12025459", "", "", reslist);
            if (ct >= 4)
                SetInd(11, "Count=" + ct + " of 12025459 with " + reslist);

            reslist = "advocacy support provided,assisted with advanced directive,conflict resolution provided,cultural accommodation/coordination provided,decision-making facilitated,end-of-life care assistance provided,prayer support provided,referral provided,resource assistance provided,ritual assistance provided,sacrament administered,scripture assistance provided,spiritual accommodation/coordination provided,staff consultation provided,supportive conversation provided,theological discussion provided,other (see comments)";
            SetIndIfResultContains(10, "", "12025475", "", "", reslist);

            reslist = "argumentative,avoids social contact,flirtatious,frequently seeks out staff,guarded,indifferent,irrational,mistrustful,poor boundaries,regressive,responding to internal stimuli,suspicious,uncooperative";
            SetIndIfResultContains(10, "", "12025588", "", "", reslist);
            reslist = "asks for medication frequently,assaultive to others,avoids social contact,demanding,elopement behaviors,frequently seeks out staff,guarded,hostile,self-harm,threatening,verbally abusive,violent";
            SetIndIfResultContains(11, "", "12025588", "", "", reslist);

            reslist = "caring behavior modeled,cue recognition promoted,face-to-face positioning promoted,interaction encouraged,parent/caregiver presence encouraged,participation in care promoted,positive reinforcement provided,rooming-in promoted,skin-to-skin contact encouraged,strengths emphasized,other";
            SetIndIfResultContains(10, "", "12042031", "", "", reslist);

            reslist = "restraints initiated,aggression,violence,attempted suicide,involuntary commitment";
            SetIndIfResultContains(10, "", "12045761", "", "", reslist);
            reslist = "behavioral assist,security involvement";
            SetIndIfResultContains(11, "", "12045761", "", "", reslist);

            reslist = "agitated,anxious,flat affect,hypoactive,labile,restless,sad,withdrawn";
            SetIndIfResultContains(10, "", "12211040", "", "", reslist);
            reslist = "angry,combative,excitable,hyperactive,sexually inappropriate,threatening,uncooperative";
            SetIndIfResultContains(11, "", "12211040", "", "", reslist);

            reslist = "agitated,apprehensive,cogwheeling,compulsions,extrapyramidal symptoms";
            reslist += ",eye contact inappropriate,foot-tapping,gait unsteady,grimace,handwringing";
            reslist += ",high energy,lethargic,nail biting,notable mannerisms/gestures";
            reslist += ",pacing,posturing,psychomotor retardation,restless,rigid,sedentary";
            reslist += ",shuffling,slouched,staggering,tense,tic,tremor";
            reslist += ",erect posture,head raised,steady gait,other";
            ct = CountItems("", "12025589", "", "", reslist);
            if (ct == 1)
                SetInd(10, "Motor movement found.");
            else if (ct > 1)
                SetInd(11, "Motor movement found count=" + ct);

        }

        private int CountLTZero()
        {
            int ct = 0;

            return ct;
        }
        private int CountGTZero(out int ct12, out int ct34)
        {
            int ct = 0;
            ct12 = 0;
            ct34 = 0;


            ct = ct12 + ct34;
            return ct;
        }

        //private bool IsQ4Freq()
        //{
        //    string codelist;
        //    bool ret = false;
        //    List<gBucket> buckets;

        //    SetBucketSize(60);
        //    buckets = new List<gBucket>();
        //    codelist = "304239398";
        //    AddBuckets(buckets, "", codelist, "", "", "Yes");
        //    ret = AnalyzeBuckets(buckets, 15, 60, "indicator 11 attestation", false);

        //    return (ret);

        //}
        private bool IsQ1Freq()
        {
            bool ret = false;


            return ret;
        }

        // (this isn't really a Q1h count -- it is just a count) Make this a q1hr count
        //private int ReturnQ1HrCount(string code_list, string result_list)
        //{
        //    var buckets = new List<gBucket>();
        //    SetBucketSize(60);
        //    AddBuckets(buckets, "", code_list, "", "", result_list);
        //    return CountBuckets(buckets);
        //}

        //private bool IsQ1Hour(int count)
        //{
        //    return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q1H);
        //}
        //private bool IsQ2Hour(int count)
        //{
        //    return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q2H);
        //}

        // Use this if counting safety buckets
        //private void CheckSafety(int count, string desc)
        //{
        //    if (_inds[13].is_checked) return;             //skip if highest already checked
        //    if (count == 0) return;
        //    switch (FreqForCount(_pat.los_hours, count))
        //    {
        //    case Frequencies.Q30M:
        //        SetInd(13, desc + " q30min");
        //        break;
        //    case Frequencies.Q1H:
        //    case Frequencies.Q2H:
        //        SetInd(12, desc + " q2h");
        //        break;
        //    default:
        //        Program.VerboseAudit(desc + ": " + count + " in " + Math.Round(_pat.los_hours) + " hours is not enough");
        //        break;
        //    }
        //}

        private void FindLatest(string code, string reslist, out string res, out DateTime evdt)
        {
            res = "???";
            evdt = DateTime.MinValue;

            var query = StartNewQuery(SearchDepth.SearchSince16Hrs);
            query = AndItemFilter(query, "", code, "", "", reslist);
            CHART_ITEM ch = query.OrderByDescending(e => e.EVENT_DATETIME).FirstOrDefault();
            if (ch == null) return;
            res = ch.RESULT;
            evdt = ch.EVENT_DATETIME;
            Program.VerboseAudit("Latest result and time:" + ch.RESULT + ch.EVENT_DATETIME.ToString());

        }

        private void Check_12_13()
        {
            string reslist;
            bool is_peds = false;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("12. Safety Management - q 2 Hours");
            Program.VerboseAudit("13. Safety Management - q 30 Minutes");
            Program.VerboseAudit("---------------");

            //CUSTOM MAPPING NOTE FOR INDICATOR 12 and 13: <8 years old if documented 
            //family /support not at bedside then trigger indicator q30 and 
            //trigger q2 if age 8-17(rowG3)


            reslist = "side rails padded,soft boundaries provided";
            SetIndIfResultContains(12, "", "12211668", "", "", reslist);

            reslist = "bed alarm set,chair alarm set,monitored by video,security transponder on";
            SetIndIfResultContains(12, "", "12042599", "", "", reslist);
            reslist = "safety attendant at bedside";
            SetIndIfResultContains(13, "", "12042599", "", "", reslist);

            //peds see custom mapping
            if (_pat.age < 18)
            {
                reslist = "family at bedside,HCT/nursing tech bedside sitter,patient care assistant,house/staff bedside sitter,video remote sitter,nurse sitter,other";
                if (!Exists("", "12024936", "", "", reslist))
                {
                    Program.VerboseAudit("No support/sitter at bedside.");
                    reslist = "";
                    if (Exists("", "12043115 Involvement in care", "", "", reslist))
                        if (_pat.age < 8)
                        {
                            SetInd(13, "12043115 Involvement in care and No support/sitter at bedside and age=" + _pat.age);
                        }
                        else if (_pat.age < 18)
                        {
                            SetInd(12, "12043115 Involvement in care and No support/sitter at bedside and age=" + _pat.age);
                        }
                }
            }

            reslist = "";
            SetIndIfResultContains(12, "", "12211236", "", "", reslist);
            //reslist = "";
            //SetIndIfResultContains(13, "", "12211236", "", "", reslist);

            if (_pat.age >= 18)
            {
                reslist = "family at bedside,HCT/nursing tech bedside sitter,patient care assistant,house/staff bedside sitter,video remote sitter,nurse sitter,other";
                SetIndIfResultContains(12, "", "12024936", "", "", reslist);
                //reslist = "family at bedside,HCT/nursing tech bedside sitter,patient care assistant,house/staff bedside sitter (lay),video remote sitter,nurse sitter,other (see comments)";
                //SetIndIfResultContains(13, "", "12024936", "", "", reslist);
            }

            reslist = "in bed,up in chair,ambulating,up to bathroom,therapy,visiting with family,test/appt.,out of room,back in room,other";
            SetIndIfResultContains(12, "", "12037074", "", "", reslist);
            //reslist = "in bed,up in chair,ambulating,up to bathroom,therapy,visiting with family,test/appt.,out of room,back in room,other (see comments)";
            //SetIndIfResultContains(13, "", "12037074", "", "", reslist);

            reslist = "agitated,calm,cooperative,crying,getting out of bed,getting out of chair,impulsive,laughing,moaning,pulling at tube/lines,removing mitts,removing theraputic devices,repeating thoughts,restless,sleeping,withdrawn,yelling,other";
            SetIndIfResultContains(12, "", "12024930", "", "", reslist);
            //reslist = "agitated,calm,cooperative,crying,getting out of bed,getting out of chair,impulsive,laughing,moaning,pulling at tube/lines,removing mitts,removing theraputic devices,repeating thoughts,restless,sleeping,withdrawn,yelling,other (see comments)";
            //SetIndIfResultContains(13, "", "12024930", "", "", reslist);

            reslist = "Called RN,Called HCT,Called UNL/Charge RN,accompanied patient off unit,diversion,managed safety of medical devices (LDA, infusions, etc),re-directed,reoriented,reported behavior changes to RN,repositioned,staff in room,VRS set-up complete, number in comments,camera stat alarm,camera privacy mode on,camera privacy mode off,camera downtime start,camera downtime stop,other";
            SetIndIfResultContains(12, "", "12024931", "", "", reslist);
            //reslist = "Called RN,Called HCT,Called UNL/Charge RN,accompanied patient off unit,diversion,managed safety of medical devices (LDA, infusions, etc),re-directed,reoriented,reported behavior changes to RN,repositioned,staff in room,VRS set-up complete, number in comments,camera stat alarm,camera privacy mode on,camera privacy mode off,camera downtime start,camera downtime stop,other (see comments)";
            //SetIndIfResultContains(13, "", "12024931", "", "", reslist);

            reslist = "direct visualization within arms reach,direct visualization to maintain staff safety";
            SetIndIfResultContains(12, "", "12060704", "", "", reslist);
            //reslist = "direct visualization within arms reach,direct visualization to maintain staff safety";
            //SetIndIfResultContains(13, "", "12060704", "", "", reslist);

            reslist = "direct visualization,direct visualization within arms reach,direct visualization to maintain staff safety";
            SetIndIfResultContains(12, "", "12065900", "", "", reslist);
            //reslist = "direct visualization,direct visualization within arms reach,direct visualization to maintain staff safety";
            //SetIndIfResultContains(13, "", "12065900", "", "", reslist);

            reslist = "ligature resistant environment, assessed for contraband,Paper Scrubs,Medications secured,hazardous chemicals secured,hand sanitizer removed,phones removed,bedside tables, extra furniture and decorations removed,all non-essential care cords removed from room,air, oxygen tank, suction regulators removed,razors, glass, alumninum cans, belts, shoelaces, ties, needles, syringes, and all sharps removed,dietary notification for disposable items,plastic bags including garbage liners removed";
            SetIndIfResultContains(12, "", "1229165", "", "", reslist);
            //reslist = "";
            //SetIndIfResultContains(13, "", "1229165", "", "", reslist);

            reslist = "security transponder on,bed alarm set,chair alarm set,locked unit,monitored by video,safety attendant at bedside,security presence,other";
            SetIndIfResultContains(12, "", "12065748", "", "", reslist);
            //reslist = "";
            //SetIndIfResultContains(13, "", "12065748", "", "", reslist);
            int[] ord12 = { 340401, 351033 };
            int[] ord13 = { 3861, 340405, 340404, 340403, 351023, 351500, 351034 };
            string desc_found = "";
            foreach (int s in ord12)
            {
                if (OrderInProgressByCode(s.ToString(), out desc_found))
                    SetInd(12, "Safety q2 Order in effect: " + desc_found);
            }
            desc_found = "";
            foreach (int s in ord13)
            {
                if (OrderInProgressByCode(s.ToString(), out desc_found))
                    SetInd(13, "Safety q30 Order in effect: " + desc_found);
            }


        }

        private bool IsValidDate(string yyyymmdd)
        { //expecting yyyymmdd
            Program.VerboseAudit("IsValidDate inpt: " + yyyymmdd);
            bool ret = true;
            if (yyyymmdd.Length < 8)
                return false;
            if (yyyymmdd.Length > 8)
                yyyymmdd = yyyymmdd.Substring(0, 8);
            ret = (yyyymmdd.Substring(0, 4).Val() > 2000
                && yyyymmdd.Substring(4, 2).Val() >= 1
                && yyyymmdd.Substring(4, 2).Val() <= 12
                && yyyymmdd.Substring(6, 2).Val() >= 1
                && yyyymmdd.Substring(6, 2).Val() <= 31);
            return ret;
        }

        private void Check_14()
        {
            string desc_found = "";
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("14. Isolation");
            Program.VerboseAudit("---------------");

            string[] ordercode_str =
            { "340003","340001","340009","340007","340006","340005","340008","340010","340002"};

            foreach (string s in ordercode_str)
            {
                if (OrderInProgressByCode(s, out desc_found))
                    SetInd(14, "Order in effect: " + desc_found);
            }

        }

        private bool OrderInProgressByCode(string code, out string found_what)
        {
            bool ret = false;
            code = code.ToLower();
            found_what = "";
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = query.Where(e => e.CODE.ToLower() == code
                                  && e.EVENT_DATETIME < loc_out);
            //&& e.ORDER_CONTROL.ToLower() == "nw"
            //&& e.ORDER_STATUS.ToLower() == "ip");
            query = query.OrderByDescending(e => e.EVENT_DATETIME);

            int count = query.Count();
            Program.VerboseAudit("Order in progress: count=" + count + " since:" + loc_out);

            foreach (var itemA in query)
            {
                var query2 = StartNewQuery(SearchDepth.SearchSinceAdmission);
                query2 = query2.Where(e => e.CODE.ToLower() == code);
                query2 = query2.Where(e => e.ORDER_ID == itemA.ORDER_ID);
                query2 = query2.Where(e => e.ORDER_CONTROL.ToLower() == "ca");
                int ct2 = query2.Count();

                if (ct2 > 0)
                {
                    Program.VerboseAudit("Order in progress: order_id=" + itemA.ORDER_ID + " _pat.pull_start=" + _pat.pull_start + " ct2=" + ct2);
                    //                  Program.VerboseAudit("order in progress: q2ct=" + ct2);
                    foreach (var x in query2)
                    {
                        Program.VerboseAudit("x.event_datetime=" + x.EVENT_DATETIME);
                        if (x.EVENT_DATETIME >= _pat.pull_start)
                        {
                            ret = true;
                            found_what += "Order ID=" + itemA.ORDER_ID + ": " + itemA.DESCRIPTION + "; range start-end:" + itemA.EVENT_DATETIME.ToString() + " - " + x.EVENT_DATETIME.ToString();
                            DisableOrder(x.ORDER_ID);
                        }
                        else
                        {
                            DisableOrder(x.ORDER_ID);
                        }
                    }
                }
                else
                {
                    ret = true;
                    found_what = "Order ID=" + itemA.ORDER_ID + ": " + itemA.DESCRIPTION + "; result=" + itemA.RESULT + "; starting:" + itemA.EVENT_DATETIME.ToString();
                    //Program.VerboseAudit("AUDIT ORDER:" + found_what);
                }
            }


            return ret;
        }


        private bool OrderInProgressByCode(string code, out string found_what, out DateTime ordercatime)
        {
            bool ret = false;
            code = code.ToLower();
            found_what = "";
            ordercatime = DateTime.MinValue;
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = query.Where(e => e.CODE.ToLower() == code
                                  && e.EVENT_DATETIME < loc_out);
            //&& e.ORDER_CONTROL.ToLower() == "nw"
            //&& e.ORDER_STATUS.ToLower() == "ip");
            query = query.OrderByDescending(e => e.EVENT_DATETIME);

            int count = query.Count();
            Program.VerboseAudit("Order in progress: count=" + count + " since:" + loc_out);

            foreach (var itemA in query)
            {
                var query2 = StartNewQuery(SearchDepth.SearchSinceAdmission);
                query2 = query2.Where(e => e.CODE.ToLower() == code);
                query2 = query2.Where(e => e.ORDER_ID == itemA.ORDER_ID);
                query2 = query2.Where(e => e.ORDER_CONTROL.ToLower() == "ca");
                int ct2 = query2.Count();

                if (ct2 > 0)
                {
                    Program.VerboseAudit("Order in progress: order_id=" + itemA.ORDER_ID + " _pat.pull_start=" + _pat.pull_start + " ct2=" + ct2);
                    //                  Program.VerboseAudit("order in progress: q2ct=" + ct2);
                    foreach (var x in query2)
                    {
                        Program.VerboseAudit("x.event_datetime=" + x.EVENT_DATETIME);
                        if (x.EVENT_DATETIME >= _pat.pull_start)
                        {
                            ret = true;
                            found_what += "Order ID=" + itemA.ORDER_ID + ": " + itemA.DESCRIPTION + "; range start-end:" + itemA.EVENT_DATETIME.ToString() + " - " + x.EVENT_DATETIME.ToString();
                            ordercatime = x.EVENT_DATETIME;
                            DisableOrder(x.ORDER_ID);
                        }
                        else
                        {
                            DisableOrder(x.ORDER_ID);
                        }
                    }
                }
                else
                {
                    ret = true;
                    found_what = "Order ID=" + itemA.ORDER_ID + ": " + itemA.DESCRIPTION + "; result=" + itemA.RESULT + "; starting:" + itemA.EVENT_DATETIME.ToString();
                    Program.VerboseAudit("ORDER active:" + found_what);
                }
            }


            return ret;
        }


        private bool OrderInProgress(string desc, out string found_what)
        {
            bool ret = false;
            desc = desc.ToLower();
            found_what = "";
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = query.Where(e => e.DESCRIPTION.ToLower() == desc
                                  && e.EVENT_DATETIME < loc_out
                                  && e.ORDER_CONTROL.ToLower() == "nw"
                                  && e.ORDER_STATUS.ToLower() == "ip");
            query = query.OrderByDescending(e => e.EVENT_DATETIME);

            int count = query.Count();
            Program.VerboseAudit("order in progress: count=" + count + " since:" + loc_out);

            foreach (var itemA in query)
            {
                var query2 = StartNewQuery(SearchDepth.SearchSinceAdmission);
                query2 = query2.Where(e => e.DESCRIPTION.ToLower() == desc);
                query2 = query2.Where(e => e.ORDER_ID == itemA.ORDER_ID);
                query2 = query2.Where(e => e.ORDER_CONTROL.ToLower() == "ca");
                int ct2 = query2.Count();

                if (ct2 > 0)
                {
                    Program.VerboseAudit("order in progress: order_id=" + itemA.ORDER_ID + " _pat.pull_start=" + _pat.pull_start + " ct2=" + ct2);
                    //                  Program.VerboseAudit("order in progress: q2ct=" + ct2);
                    foreach (var x in query2)
                    {
                        Program.VerboseAudit("x.event_datetime=" + x.EVENT_DATETIME);
                        if (x.EVENT_DATETIME >= _pat.pull_start)
                        {
                            ret = true;
                            found_what += "Order ID=" + itemA.ORDER_ID + ": " + itemA.DESCRIPTION + "; range start-end:" + itemA.EVENT_DATETIME.ToString() + " - " + x.EVENT_DATETIME.ToString();
                        }
                        else
                        {
                            DisableOrder(x.ORDER_ID);
                        }
                    }
                }
                else
                {
                    ret = true;
                    found_what = "Order ID=" + itemA.ORDER_ID + ": " + itemA.DESCRIPTION + "; result=" + itemA.RESULT + "; starting:" + itemA.EVENT_DATETIME.ToString();
                    //Program.VerboseAudit("AUDIT ORDER:" + found_what);
                }
            }


            return ret;
        }

        private void DisableOrder(string ordid)
        {
            //update ORDER_CONTROL = 'XNW' for _pat.encounter_id and ordid and NW
            var db = PFSDBUtility.NewSqlConnection();
            string q = "UPDATE chart_item set ORDER_CONTROL='XN' where encounter_id=" + _pat.encounter_id + " and order_id='" + ordid + "' and order_control='nw'";
            SqlCommand cmd = new SqlCommand(q, db);
            cmd.ExecuteNonQuery();
            db.Close();
        }

        private void DisableSitter(string code, DateTime evdt, bool use_proc_start)
        {
            //update ORDER_STATUS = 'X' for _pat.encounter_id and ordid
            var db = PFSDBUtility.NewSqlConnection();
            //string q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and code='" + code + "' and (result='initiated' or result='continued') and event_datetime<=" + PFSDBUtility.SQLDateTime(evdt);
            string q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and code='" + code + "' and event_datetime<=" + PFSDBUtility.SQLDateTime(evdt);
            if (use_proc_start)
                q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and code='" + code + "' and procedure_start is not null and procedure_start<=" + PFSDBUtility.SQLDateTime(evdt);
            SqlCommand cmd = new SqlCommand(q, db);
            cmd.ExecuteNonQuery();
            db.Close();

        }
        private void DisableSitter(string code, DateTime evdt)
        {
            DisableSitter(code, evdt, false);
        }

        //private void CheckAssessment(int count, string desc)
        //{
        //    //if (_inds[18].is_checked) return;          //skip if highest already checked
        //    if (count == 0) return;                    //skip if none

        //    // This should work the same as the original code:
        //    switch (FreqForCount(_pat.los_hours, count))
        //    {
        //        case Frequencies.Q30M:
        //            SetInd(18, desc);
        //            break;
        //        case Frequencies.Q1H:
        //            SetInd(17, desc);
        //            break;
        //        case Frequencies.Q2H:
        //            SetInd(16, desc);
        //            break;
        //        case Frequencies.Q4H:
        //            SetInd(15, desc);
        //            break;
        //        default:
        //            break;
        //    }

        //}

        private void ShowBuckets(List<gBucket> buckets)
        {
            string s = "";
            foreach (var e in buckets)
            {
                s = "(" + e.bucket + "" + e.code + "" + e.evdt.ToString() + ")";
                Program.VerboseAudit("Assessmt item:=" + s);
            }
        }


        private void Check_15_16_17_18()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("15. Assessment q4h");
            Program.VerboseAudit("16. Assessment q2h");
            Program.VerboseAudit("17. Assessment q1h");
            Program.VerboseAudit("18. Assessment q30min");
            Program.VerboseAudit("---------------");

            string prevloc;
            string currloc;
            DateTime locevdt;
            int levelofcare = GetLatestLevelOfCare(out prevloc, out currloc, out locevdt); // 17,16,15
            bool use_lower_levelcare = LocationSubjectToVSLowering(levelofcare);

            if (use_lower_levelcare && levelofcare >= 15)
                _level_of_care_threshold = locevdt;   //this time affects AddBuckets
            else
                _level_of_care_threshold = DateTime.MinValue;

            SetBucketSize(60);
            AnalyzeAssessments(15, 60, levelofcare * Convert.ToInt32(use_lower_levelcare));

            SetBucketSize(30);
            AnalyzeAssessments(18, 30, levelofcare * Convert.ToInt32(use_lower_levelcare));

            SetBucketSize(60);

            _level_of_care_threshold = DateTime.MinValue;

        }

        private bool LocationSubjectToVSLowering(int levcare)
        {
            bool unit_eligible = false;
            bool do_lower = false;
            // return truth of: this location matches a facility and unit that can
            // have its VS lowered to levcare, and that level of care is lower than
            // the location.
            if (_pat.short_name.ToUpper() == "CFH")
            {
                unit_eligible = ((_pat.unit_name.ToUpper() == "Carle Tower 7A".ToUpper()) || (_pat.unit_name.ToUpper() == "Carle Tower 6A".ToUpper()) || (_pat.unit_name.ToUpper() == "North Tower 8".ToUpper()));
            }
            else if (_pat.short_name.ToUpper() == "CBMC")
            {
                unit_eligible = ((_pat.unit_name.ToUpper() == "ICU"));
            }
            else if (_pat.short_name.ToUpper() == "CRMH")
            {
                unit_eligible = ((_pat.unit_name.ToUpper() == "ICU"));
            }
            if (unit_eligible && (levcare == 16 || levcare == 15))
                do_lower = true;

            if (_pat.short_name.ToUpper() == "CFH")
            {
                unit_eligible = ((_pat.unit_name.ToUpper() == "Carle Tower 7 B".ToUpper()) || (_pat.unit_name.ToUpper() == "Carle Tower 6B".ToUpper()) || (_pat.unit_name.ToUpper() == "North Tower 8".ToUpper()));
            }
            else if (_pat.short_name.ToUpper() == "CBMC")
            {
                unit_eligible = ((_pat.unit_name.ToUpper() == "CVCU") || (_pat.unit_name.ToUpper() == "PCU"));
            }
            if (unit_eligible && (levcare == 15))
                do_lower = true;

            return do_lower;
        }

        private int GetLatestLevelOfCare(out string prevloc, out string currloc, out DateTime evdt)
        {
            int levcare = 0;
            string return_result = "";

            prevloc = "";
            currloc = "";
            evdt = DateTime.MinValue;

            // Applies to these units: CFH 6A, 7A, NT8
            // CBMC ICU
            // CRMH ICU

            string[] q1levelcare =
                    { "Intensive Care","Critical Care","Neonatal Intensive Care","Pediatric Intensive Care","Labor and Delivery"};

            // Applies to these units: CFH 7B, 6B, NT8
            // CBMC CVCU, PCU

            string[] q2levelcare =
                    { "Adv. Care","Advanced Care","Progressive Care","Pediatric Intermediate Care","Cardiovascular Care","CV Advanced Care"};
            string[] q4levelcare =
                    { "Medical","Cardiac","Neurology","Surgical","Pediatric","Oncology","Newborn","Antepartum","Inpatient Rehab","Skilled Nursing"};

            //Vitals q 15 min - 1 hour
            //  Intensive Care
            //  Critical Care
            //  Neonatal Intensive Care
            //Pediatric Intensive Care
            //Labor and Delivery

            //Vitals Q 2 hours
            //Advanced Care
            //Progressive Care
            //Pediatric Intermediate Care
            //Cardiovascular Care
            //CV Advanced Care

            //Vitals Q 4 hours
            //Medical
            //Cardiac
            //Neurology
            //Surgical
            //Pediatric
            //Onocology
            //Newborn
            //Antepartum
            //Inpatient Rehab
            //Skilled Nursing
            var locquery = StartNewQuery(SearchDepth.SearchDefault);
            locquery = locquery.Where(e => e.CODE.ToLower() == "level of care");
            locquery = locquery.OrderBy(e => e.EVENT_DATETIME);
            int count = locquery.Count();
            Program.VerboseAudit("Level of Care: count=" + count);
            if (count == 0) return 0;

            CHART_ITEM[] locitemsary;
            locitemsary = locquery.ToArray();
            string prev="";
            bool is_lower = false;
            for (int i = 0; i <= locitemsary.GetUpperBound(0); i++)
            {
                if (i >= 1)
                {
                    if (prev != locitemsary[i].RESULT.ToLower() && prev != "")
                    {
                        Program.VerboseAudit("Level of Care: " + locitemsary[i].RESULT.ToLower() + " : " + locitemsary[i].EVENT_DATETIME);
                        is_lower = LevelIsLower(prev, locitemsary[i].RESULT.ToLower());
                        if (is_lower)
                        {
                            Program.VerboseAudit("Level of Care is lower: " + locitemsary[i].RESULT.ToLower() + " : " + locitemsary[i].EVENT_DATETIME);
                            prevloc = prev;
                            currloc = locitemsary[i].RESULT.ToLower();
                            evdt = locitemsary[i].EVENT_DATETIME;
                        }
                        prev = locitemsary[i].RESULT.ToLower();
                    }
                }
                else
                {
                    prev = locitemsary[i].RESULT.ToLower();
                } 

            }
            // incontinuity issues = extended
            // dependent is a word found in INDEPENDENT

//            GetLatestResult("", "Level of Care", "", "", out return_result, SearchDepth.SearchSince24Hrs);
            foreach (var s in q1levelcare)
            {
                if (currloc == s.ToLower())
                    levcare = 17;
                //                    SetInd(18, "q30 Level of Care = " + s);
            }
            foreach (var s in q2levelcare)
            {
                if (currloc == s.ToLower())
                    levcare = 16;
                //    SetInd(16, "q2 Level of Care = " + s);
            }
            foreach (var s in q4levelcare)
            {
                if (currloc == s.ToLower())
                    levcare = 15;
                //    SetInd(15, "q4 Level of Care = " + s);
            }

            return levcare;

        }

        private bool LevelIsLower(string lev1, string lev2)
        {//rturn true if lev2 is lower
            string[] q1levelcare =
                    { "Intensive Care","Critical Care","Neonatal Intensive Care","Pediatric Intensive Care","Labor and Delivery"};

            // Applies to these units: CFH 7B, 6B, NT8
            // CBMC CVCU, PCU

            string[] q2levelcare =
                    { "Adv. Care","Advanced Care","Progressive Care","Pediatric Intermediate Care","Cardiovascular Care","CV Advanced Care"};
            string[] q4levelcare =
                    { "Medical","Cardiac","Neurology","Surgical","Pediatric","Oncology","Newborn","Antepartum","Inpatient Rehab","Skilled Nursing"};

            int lev1_val = 0;
            int lev2_val = 0;
            foreach (string L in q1levelcare)
            {
                if (L.ToLower() == lev1.ToLower())
                    lev1_val = 1;
                if (L.ToLower() == lev2.ToLower())
                    lev2_val = 1;
            }
            foreach (string L in q2levelcare)
            {
                if (L.ToLower() == lev1.ToLower())
                    lev1_val = 2;
                if (L.ToLower() == lev2.ToLower())
                    lev2_val = 2;
            }
            foreach (string L in q4levelcare)
            {
                if (L.ToLower() == lev1.ToLower())
                    lev1_val = 4;
                if (L.ToLower() == lev2.ToLower())
                    lev2_val = 4;
            }
            return (lev2_val > lev1_val);
        }

        private int GetAssessInd(DateTime loc_out_time, out DateTime classdt)
        {
            int ind = 0;
            classdt = DateTime.MinValue;
            //get assessment indicator at location out time = loc_out_time
            var db = PFSDBUtility.NewPfsDataContext();        // look for the unit default admission profile
            var query = from ce in db.CLASSIFICATION_EVENTs
                        from ia in db.INDICATOR_ANSWERs
                        where (ce.CLASSIFICATION_EVENT_ID == ia.CLASSIFICATION_EVENT_ID)
                        && (ce.ENCOUNTER_ID == _pat.encounter_id)
                        && (ce.EFFECTIVE_DATETIME_IN <= loc_out_time)
                        && (ia.INDICATOR_NUMBER >= 15 && ia.INDICATOR_NUMBER <= 18)
                        orderby ce.EFFECTIVE_DATETIME_IN descending
                        select new
                        {
                            ia.INDICATOR_NUMBER,
                            ce.CLASSIFICATION_DATETIME
                        };
            if (query.Count() > 0)
            {
                ind = query.First().INDICATOR_NUMBER;
                classdt = query.First().CLASSIFICATION_DATETIME;
            }
            return ind;
        }

        private void AnalyzeAssessments(int ind, int bucket_size, int levcare)
        {
            string codelist;
            string reslist;
            List<gBucket> buckets;
            string freqstr = "";

            if (bucket_size == 60) freqstr = "====Q4/Q2/Q1 HR EVALUATION================";
            if (bucket_size == 30) freqstr = "====Q30 MIN EVALUATION====================";
            Program.VerboseAudit(freqstr + " bucket size=" + bucket_size + "  _bucket size=" + _bucket_size);

            //        Vitals:
            string assessgrouplabel = "Vitals";
            buckets = new List<gBucket>();
            codelist = EXACT_MATCH_PREFIX + "9006";
            AddBuckets(buckets, "", codelist, "",  "");
            codelist = EXACT_MATCH_PREFIX + "9005";
            AddBuckets(buckets, "", codelist, "",  "");
            codelist = EXACT_MATCH_PREFIX + "9009";
            AddBuckets(buckets, "", codelist, "",  "");
            codelist = EXACT_MATCH_PREFIX + "9008";
            AddBuckets(buckets, "", codelist, "",  "");
            codelist = EXACT_MATCH_PREFIX + "90010";
            AddBuckets(buckets, "", codelist, "",  "");
            codelist = EXACT_MATCH_PREFIX + "90011";
            AddBuckets(buckets, "", codelist, "",  "");
            codelist = EXACT_MATCH_PREFIX + "90014";
            AddBuckets(buckets, "", codelist, "",  "");
            codelist = "12210793,12210718,12214784,12095757,12210766,12210717,12210771,";
            codelist += "12063623,12063624,12063628,12210514,12210765,12210768,12210704";
            AddBuckets(buckets, "", codelist, "",  "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);


            //Cardiovascular:
            assessgrouplabel = "Cardiovascular";
            buckets = new List<gBucket>();
            codelist = "12025687,12026614,12037481,12046555,12023500,12050420,12025698,12025699,12025700,12025701,1222673";
            codelist += ",1224032,12057351,12025717,12026627,1223056,12211867,12051046,12050983,12041189,12049937,12050014";
            codelist += ",12025720,12026630,12025718,12026628,12025721,12026631,12025719,12026629";
            codelist += ",12048481,12050546,12051923,12051462,12025757,12025758,12025759,12025760,12025761,12025762,12025763";
            codelist += ",12025764,12025765,12025766,12025767,12025768,12025769,12025770,12025771,12025772,12025722,12025723";
            codelist += ",12025724,12025725,12025726,12025727,12025728,12025729,12025730,12025731,12025732,12025733,12025734";
            codelist += ",12025735,12025736,12025737,12025738,12025739,12025740,12025741,12025742,12025743,12025744,12025745";
            codelist += ",12025746,12025747,12025748,12025749,12025750,12025751,12046950,12061043,12025787,12053879,12059649";
            codelist += ",12059647,12059347";
            AddBuckets(buckets, "", codelist, "",  "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            //Pulmonary:
            assessgrouplabel = "Pulmonary";
            buckets = new List<gBucket>();
            codelist = "12025603,1224100,12042115,12025605,12211344,1222683,1222684,1222685,12025608,12025609,12025610,12025611";
            codelist += ",12025612,12025613,12025614,12211451,12042035,12211387,12041633,1224040";
            codelist += ",12211386,12037933,12053885,12046676,12053882,12053883,12046763,12053887,12046675,12047531,1223592";
            codelist += ",12212145,3040111472,12047293,12046718,12055738,12212137,12066685";
            AddBuckets(buckets, "", codelist, "",  "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            //Neuro:
            assessgrouplabel = "Neuro";
            buckets = new List<gBucket>();
            codelist = "12065483,1222796,1222531,12035935,12042918,12064140,12064108,12063800,12063802,12063859,12063799";
            codelist += ",12063801,12063959,12063803,12063707,12063964,12025529,12057578,12056762,12056751,12056763,12056750";
            codelist += ",12056754,1222567,12051779,12025536,12039029,12038297,12025537,12025538,12025539,12025540,12211050";
            codelist += ",12211051,12211052,12211054,12211056,12211057,12211058,12211059,12211060,12211061,12211062,12211063";
            codelist += ",12038604,12040773,12041405,12040711,12042629,1222537,1222536,12211080,12211081,12211082,12211083";
            codelist += ",12211147,12211148,1222540,12025546,12025547,12025549,12025550,12025552,12025553,12025555,12025556";
            codelist += ",12025558,12025559,12037282,12041823,12025563,12025564,12049426,12049428,12049425,12049427,12052160";
            codelist += ",12050307,12025565,12049581,12049583,12049580,12049582,12052419,12051262,12025566,12049343,12049345";
            codelist += ",12049342,12049344,12050639,12050523,12025568,12048584,12048588,12048583,12048587,12052580,12052122";
            codelist += ",12025569,12050747,12050749,12050746,12050748,12051219,12052514,12025576,12025580,12211108,1222578";
            codelist += ",12052110,12025541,12025542,1222631,12025543,12025593,12046801,12046661,12055196,12026595,12046860";
            codelist += ",12046758,12056391,12056392,12026588,12036870";
            codelist += ",12051899,12051875,9001570063006,9001570063012,9001570063155,9001570063169";
            AddBuckets(buckets, "", codelist, "",  "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            //Gi / GU Fluid Mgt:
            assessgrouplabel = "Gi/GU Fluid Mgt";
            buckets = new List<gBucket>();
            codelist = "12025790,12051971,12050851,12025792,122333,12211487,1222944,12026505,12211498,12211499,12211500,12211492";
            codelist += ",12025797,12025798,12050067,122196,122197,12042499,12043025,12025799,12055183,12055184,12055185";
            codelist += ",122147,122149,122151,122152,122153,12047252,12047253,1225313,12021765,12047318,12047249,12046730";
            codelist += ",12054035,12057083,12025803,12026637,1222885,1222887,12045746,12061146,12061147,12046707";
            codelist += ",12054062,12054065,12054068,12025152,12055206,12047060,12054070,12054072,12063123,12056288,12055214";
            codelist += ",12046706,12046770,12047323,12055212,12055213,12046781,12046709,12046753,12046820,12038505,12211689";
            codelist += ",12025805,12025806,12051868,1223425,12025815,12025829,12054067,12054068,1224684,1224686,1224685";
            codelist += ",12046691,12046690,12030860,12046711,12047264,12046687";
            AddBuckets(buckets, "", codelist, "", "");
            codelist = "9001570063078,9001570063018";
            AddBuckets(buckets, "", codelist, "", "1,2,3,4,5,6");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            //Wound:
            assessgrouplabel = "Wound";
            buckets = new List<gBucket>();
            codelist = "12025835,12041992,1222718,1222719,1222720,12051480,12211583,1222728,12059731,12050264,12051662,12050920";
            codelist += ",12051847,12025884,12026652,12025886,12049389,12025888,12025889,12026648,12026649,12048806,12026651";
            codelist += ",12033760,12026943,12026944,12026946,12026947,12064244,12064195,12063787,12063729,12063908,12064099";
            codelist += ",1223684,12026645,1223207,12026646,12026647,1224156,12040412,12042625,12057328";
            codelist += ",12025892,12025893,12050989,12050991,12050988,12050990,12037746";
            codelist += ",12056851,12025900,1224164,12025901,12025902,12211558,12211580,12051232,12051706,12051890,12050356";
            codelist += ",12048731,12050185,12048582,12041044,12040695,12211604,12212080,12211605,12212083,1222783,12211606";
            codelist += ",12026644,12025866,12059306,12048640,1222765,1222766,1222767,9001570010009,12049880,12050558,12048494";
            codelist += ",12055349,12050832,12059271,12059272,12059273,12055211,12046995,12046999,12047000,12047003,12047004";
            codelist += ",12047014,12047259,12047262,12047257,12046695";
            AddBuckets(buckets, "", codelist, "",  "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            //Medication:
            assessgrouplabel = "Medication";
            buckets = new List<gBucket>();
            codelist = "12063944,12025402,12064552,12065154,12044478,12065955"; //,12059338"; //,12025384,12025385,12025386";
            codelist += ",12025395,12052458,12025428,12210951"; //",12025387,12025388,12025389,12025395,12052458,12025428,12210951";
            codelist += ",12025422,12025525,12043105,12065364,12066139,12025410,12059615,12055258";
            AddBuckets(buckets, "", codelist, "",  "");
            string codelist1 = "12059338";
            string codelist2 = "12025409";
            string reslist1 = "complains of pain/discomfort,non-verbal indicators present,assumed presence of pain";
            string reslist2 = "";
            AddDependentBuckets(buckets, codelist1, reslist1, codelist2, reslist2, "", "");
            codelist1 = "12025403,12054146";
            codelist2 = "12025409";
            reslist1 = "";
            reslist2 = "";
            AddDependentBuckets(buckets, codelist1, reslist1, codelist2, reslist2, "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

        }

        private bool AnalyzeBuckets(List<gBucket> buckets, int ind, int bucketsize, string group)
        {
            return AnalyzeBuckets(buckets, ind, bucketsize, group, true);

        }

        private bool AnalyzeBuckets(List<gBucket> buckets, int ind, int bucketsize, string group, bool set_ind)
        {
            DateTime dt = DateTime.MinValue;
            DateTime firstdt = DateTime.MinValue;

            bool all_ok = OLDAnalyzeBuckets(buckets, ind, bucketsize, group, set_ind);
            return all_ok;


        }
        private bool DoShortLOSAssessEval(List<gBucket> buckets, int ind, int bucketsize, string group, bool set_ind)
        {
            int ct = 0;
            DateTime starttm, endtm;
            DateTime prevtm = DateTime.MinValue;
            int loshrs = (int)(Math.Round(_pat.los_hours));
            var arr = buckets.ToArray();
            bool set18 = false;
            Program.VerboseAudit("----Entering Short LOS Analysis for group: " + group + " LOS=" + loshrs + "  bucketsizew=" + bucketsize);

            for (int i = 0; (i < arr.GetUpperBound(0)); i++)
            {
                prevtm = DateTime.MinValue;
                starttm = arr[i].evdt;
                endtm = starttm.AddHours(loshrs);
                ct = 0;
                for (int j = i; (j < arr.GetUpperBound(0)); j++)
                {
                    if (prevtm < arr[j].evdt && arr[j].evdt >= starttm && arr[j].evdt <= endtm)
                    {
                        ct++;
                        prevtm = arr[j].evdt;
                    }
                }
                if (ct >= 9 / (4.0/loshrs))
                {
                    SetInd(18, ct.ToString() + " or more items in LOS=" + loshrs + " hours.");
                    set18 = true;
                    i = 999;
                }
            }
            Program.VerboseAudit("----Exiting Short LOS Analysis for group: " + group + " LOS=" + loshrs + "  bucketsizew=" + bucketsize);

            return set18;
        }


//Proposed change to remove consecutive factor; use straight scale with Short LOS exception:  
//	                        LOS=12 hrs      LOS = 6 hrs     LOS = 4 hrs     LOS = 2 hrs     Short LOS< 5 hrs
//q30 75% of 30-min buckets	3/4 x 24  = 18	3/4 x 12  = 9	3/4 x 8  = 6	3/4 x 4  = 3	5 or more VS
//q1 66% of 60-min buckets	2/3 x 12  = 8	2/3 x 6  = 4	2/3 x 4  = 2.67=>3	2/3 x 2  = 1.33=>1	3 VS + examine 30min 
//q2 50 % of 60-min buckets	.5 x 12  = 6	.5 x 6  = 3	.5 x 4  = 2	.5 x 2  = 1	2 VS
//q4 33% of 60-min buckets	1/3 x 12  = 4 2	1/3 x 6  = 2 1	1/3 x 4  = 1.33=>1	1/3 x 2  = .67=>1	1 VS
        private bool OLDAnalyzeBuckets(List<gBucket> buckets, int ind, int bucketsize, string group, bool set_ind)
        {
            DateTime dt = DateTime.MinValue;
            int bnum = 0;
            int numbucket = -99;
            int lobucket = 99;
            int hibucket = 0;
            int numitems = 0;
            int numconsec = 0;
            int greatestnumconsec = 0;
            int num_unique_times = 0;
            DateTime prev_time = DateTime.MinValue;
            bool all_ok = false;
            List<gBucket> dtlist = new List<gBucket>();
            Program.VerboseAudit("----Begin Assessment Group = " + group + " ---- bucketsizew=" + bucketsize + " start time of first bucket=" + _pat.pull_start);
            //if (ind==18 && Math.Round(_pat.los_hours) <= 4.0)
            //{
            //    all_ok = DoShortLOSAssessEval(buckets,ind,bucketsize,group,set_ind);
            //    Program.VerboseAudit("---- End Assessment Group = " + group + " ----");
            //    return all_ok;
            //}

            //Program.VerboseAudit("buckets count=" + buckets.Count());
            var b = buckets.OrderBy(e => e.evdt).ToList();

            //numitems = buckets.Count();
            foreach (var item in b)
            {
                if (prev_time != item.evdt)
                {
                    num_unique_times++;
                    prev_time = item.evdt;
                }
                if (numbucket < item.bucket)
                {
                    numbucket = item.bucket;
                    numitems++;
                    Program.VerboseAudit(item.bucket + ") " + item.evdt.ToString() + " := " + item.code);
                }
                //if (numbucket < item.bucket)
                //{
                //    //add dt to ary
                //    bnum++;
                //    dtlist.Add(item);
                //    if (numbucket == -99 || item.bucket - numbucket == 1)
                //    {
                //        numconsec++;
                //        if (greatestnumconsec < numconsec)
                //            greatestnumconsec = numconsec;
                //    }
                //    else
                //    {
                //        numconsec = 1;
                //    }
                //    numbucket = item.bucket;
                //    if (hibucket < item.bucket) hibucket = item.bucket;
                //    if (lobucket > item.bucket) lobucket = item.bucket;
                //    Program.VerboseAudit(item.bucket + ")." + item.evdt.ToString() + " := " + item.code);// + "  [distinct times only]");
                //}
            }

            //if (bnum <= 1)
            //{
            //    Program.VerboseAudit("---- End Assessment Group = " + group + " ----");
            //    return false;
            //}

            //Program.VerboseAudit("numitems=" + numitems);
            //Program.VerboseAudit("bucket count=" + bnum);
            //int half_los = (int)(_pat.los_hours * (30.0 / bucketsize));//for q30 this will be los_hours.  for q60 this will be .5 * los_hours
            //Program.VerboseAudit("half-LOS bucket count=" + half_los + " at " + bucketsize + "mins each.");
            //Program.VerboseAudit("greatest consec=" + greatestnumconsec);

            int num_buckets_in_los = (int)(_pat.los_hours * (60.0 / bucketsize));//for q30 this will be 2xlos_hours.  for q60 this will be los_hours
            Program.VerboseAudit("total bucket count in LOS=" + num_buckets_in_los);
            Program.VerboseAudit("num buckets filled=" + numitems);
            //Program.VerboseAudit("half-LOS bucket count=" + half_los + " at " + bucketsize + "mins each.");
            //Program.VerboseAudit("greatest consec=" + greatestnumconsec);

            //double bucketratio = (hibucket-lobucket) / (1.0 * (bnum-1));
            //q30 75% of 30-min buckets	3/4 x 24  = 18	3/4 x 12  = 9	3/4 x 8  = 6	3/4 x 4  = 3	5 or more VS
            //q1 66% of 60-min buckets	2/3 x 12  = 8	2/3 x 6  = 4	2/3 x 4  = 2.67=>3	2/3 x 2  = 1.33=>1	3 VS + examine 30min 
            //q2 50 % of 60-min buckets	.5 x 12  = 6	.5 x 6  = 3	.5 x 4  = 2	.5 x 2  = 1	2 VS
            //q4 33% of 60-min buckets	1/3 x 12  = 4 2	1/3 x 6  = 2 1	1/3 x 4  = 1.33=>1	1/3 x 2  = .67=>1	1 VS
            int q30need = (int)Math.Round(0.75 * 2 * _pat.los_hours);
            int q1need = (int)Math.Round(0.667 * _pat.los_hours);
            int q2need = (int)Math.Round(0.5 * _pat.los_hours);
            int q4need = 1;
            if (ind < 18)
            {
                if (_pat.los_hours > 5)
                {
                    if (_pat.los_hours < 8)
                    {
                        q4need = 1;
                        q2need = 2; //Jan27 2021
                    }
                    else
                    {
                        q4need = 2;
                        q2need = 4; // Feb3 2021 4;//Jan27 2021
                    }

                    if (numitems >= q1need)
                    {
                        SetInd(17 * Convert.ToInt32(set_ind), "Qualifies for q1 because numcharted=" + numitems + " is >=" + q1need + " LOS=" + Math.Round(_pat.los_hours, 2) + " x .667=" + q1need);
                        all_ok = (ind <= 17);
                    }
                    else if (numitems >= q2need)
                    {
                        //Jan27 2021 SetInd(16 * Convert.ToInt32(set_ind), "Qualifies for q2 because numcharted=" + numitems + " is >=" + q2need + " LOS=" + Math.Round(_pat.los_hours, 2) + " x .5=" + q2need);
                        SetInd(16 * Convert.ToInt32(set_ind), "Qualifies for q2 because numcharted=" + numitems + " is >=" + q2need + " for LOS=" + Math.Round(_pat.los_hours, 2) + "  >=8hrs needs 5 else 2");
                        all_ok = (ind <= 16);
                    }
                    else if (numitems >= q4need)
                    {
                        SetInd(15 * Convert.ToInt32(set_ind), "Qualifies for q4 because numcharted=" + numitems + " is >=" + q4need + " LOS=" + Math.Round(_pat.los_hours, 2) + "  >=8hrs needs 2 else 1");
                        all_ok = (ind <= 15);
                    }
                }
                else //short los
                {
                    q1need = 3;
                    q2need = 2;
                    q4need = 1;

                    if (numitems >= q1need)
                    {
                        SetInd(17 * Convert.ToInt32(set_ind), "ShortLOS Qualifies for q1 because numcharted=" + numitems + " is >=" + q1need + " LOS=" + Math.Round(_pat.los_hours, 2) + " needs just 3");
                        all_ok = (ind <= 17);
                    }
                    else if (numitems >= q2need)
                    {
                        SetInd(16 * Convert.ToInt32(set_ind), "ShortLOS Qualifies for q2 because numcharted=" + numitems + " is >=" + q2need + " LOS=" + Math.Round(_pat.los_hours, 2) + " needs just 2");
                        all_ok = (ind <= 16);
                    }
                    else if (numitems >= q4need)
                    {
                        SetInd(15 * Convert.ToInt32(set_ind), "ShortLOS Qualifies for q4 because numcharted=" + numitems + " is >=" + q4need + " LOS=" + Math.Round(_pat.los_hours, 2) + " needs just 1");
                        all_ok = (ind <= 15);
                    }

                }
//                if (greatestnumconsec >= half_los || bucketratio <= 1.5 && bnum >= half_los-1)
//                {
//                    SetInd(17 * Convert.ToInt32(set_ind), "Qualifies for q1hr because chartings are at least q1hr for duration of half-LOS=" + half_los + " maxconsec=" + greatestnumconsec);
//                    all_ok = (ind <= 17);
//                }
//                //                else if (bucketratio <= 1.25 && bnum >= half_los && half_los >= 4)
//                //else if (bucketratio <= 1.25 && bnum >= half_los && half_los >= 4)
//                //{
//                //    SetInd(17 * Convert.ToInt32(set_ind), "Qualifies for q1hr because charting to bucket ratio is less than 1.25: hibucket-lobucket=" + hibucket +"-" +lobucket+"="+ (hibucket - lobucket) + " divided by num buckets=" + bnum + " equals " + bucketratio);
//                //    all_ok = (ind >= 17);
//                //}
//                //else if (bucketratio <= 2.5 && bnum >= 2)
//                //                else if (bnum >= half_los/2)
//                else if (bucketratio < 3 && bnum >= 3 && 
//                    (
//                    (half_los >= 6 && (hibucket - lobucket) >= half_los-1)
//                    ||
//                    ((half_los >= 4 && half_los <= 5) && (hibucket - lobucket) >= half_los - 1)
//                    ||
//                    ((half_los <= 3) && (hibucket - lobucket) >= half_los)
//                    )
//                    )
//                {
//                    SetInd(16 * Convert.ToInt32(set_ind), "Qualifies for q2hr because charting to bucket ratio is less than 3: hibucket-lobucket=" + hibucket + "-" + lobucket + "=" + (hibucket - lobucket) + " divided by num buckets=" + (bnum-1) + " equals " + bucketratio);
//                    all_ok = (ind <= 16);
//                }
////                else if (bucketratio > 2.5 && bucketratio <= 5 && bnum >= 2)
////                else if (bnum >= half_los/4)
//                else if ((bucketratio <= 6 && bnum >= 2) && //change to <=6 Oct15/2020
//                    (
//                    (half_los >= 6 && (hibucket - lobucket) >= half_los - 2)
//                    ||
//                    ((half_los >= 4 && half_los <= 5) && (hibucket - lobucket) >= half_los - 1)
//                    ||
//                    ((half_los <= 3) && (hibucket - lobucket) >= half_los)
//                    )
//                    )
//                {
//                    SetInd(15 * Convert.ToInt32(set_ind), "Qualifies for q4hr because charting to bucket ratio is <=6: hibucket-lobucket=" + hibucket + "-" + lobucket + "=" + (hibucket - lobucket) + " divided by num buckets=" + (bnum-1) + " equals " + bucketratio);
//                    all_ok = (ind == 15);
//                }
            }
            else // ind==18
            {
                if (_pat.los_hours > 2)
                {
                    if (numitems >= q30need)
                    {
                        SetInd(18 * Convert.ToInt32(set_ind), "Qualifies for q30 because numcharted=" + numitems + " is >=" + q30need + " LOS=" + Math.Round(_pat.los_hours, 2) + " x 2 x .75=" + q30need);
                        all_ok = (ind <= 18);
                    }
                }
                else  //for los<=2 need 3 items for q30 is 
                {
                    q30need = 3;
                    if (num_unique_times >= q30need)
                    {
                        SetInd(18 * Convert.ToInt32(set_ind), "ShortLOS Qualifies for q30 because numcharted=" + numitems + " is >=" + q30need + " LOS=" + Math.Round(_pat.los_hours, 2) + " with num_unique_times=" + num_unique_times + " Need:" + q30need);
                        all_ok = (ind <= 18);
                    }
                }
                //if (greatestnumconsec >= half_los)
                //{
                //    SetInd(18 * Convert.ToInt32(set_ind), "Qualifies for q30min because consecutive chartings are at least q30 for duration of half-LOS=" + half_los + " maxconsec=" + greatestnumconsec);
                //    all_ok = (ind <= 18);
                //}
            }
            Program.VerboseAudit("---- End Assessment Group = " + group + " ----");
            return all_ok;
        }


        private void SetBucketSize(int minutes)
        {
            _bucket_size = minutes;
        }

        //Add to the list of bucket numbers (redundant buckets are fine)
        private bool AddBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field)
        {
            return AddBuckets(bucket_list, cat, code_list, desc, field, "", SearchDepth.SearchDefault);
        }
        private bool AddBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field, string result_list)
        {
            return AddBuckets(bucket_list, cat, code_list, desc, field, result_list, SearchDepth.SearchDefault);

        }
        private bool AddBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field, string result_list, SearchDepth search_depth)
        {
            // Assign (virtual) bucket numbers to each assessment.  
            // Every x minutes after that is a new bucket.  Bucket zero = pull_start time.
//            Program.VerboseAudit("----Locating items for group: " + assessgrouplabel + "   bucketsize=" + _bucket_size);

            // get the chart items for the assessments
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc, field, result_list);
            if (_level_of_care_threshold > DateTime.MinValue)
                query = query.Where(e => e.EVENT_DATETIME >= _level_of_care_threshold);
            query = query.Where(e => e.EVENT_DATETIME < _pat.pull_finish);
            int ct = query.Count();
            if (ct == 0) return false;

            Program.VerboseAudit("ct=" + ct + " of codelist=" + code_list);
            // This step is needed for those who want to count the # within a bucket, like Shands #24.
            // This will have no effect on those that count buckets.
            //var query2 = (from item in query select new { item.EVENT_DATETIME }).Distinct();
            //var query2 = (from item in query select new { item.EVENT_DATETIME, item.CODE });

            //foreach (var x in query2)
            //{
            //    Program.VerboseAudit("patpullstart=" + _pat.pull_start.ToString() + " itemevdt=" + x.EVENT_DATETIME.ToString() + " diff=" + (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, x.EVENT_DATETIME)) + " bucketsize=" + _bucket_size + " bucket=" + (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, x.EVENT_DATETIME) / _bucket_size));
            //}

            // figure out what buckets the events belong to
            //var query3 = from item in query2
            //             select new {
            //                bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size)
            //             };
            //// Add to the list
            //foreach (var item in query3) {
            //    bucket_list.Add(item.bucket);
            //}

            var query3 = from item in query
                         select new
                         {
                             bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                             code = item.CODE,
                             desc = item.DESCRIPTION,
                             evdt = item.EVENT_DATETIME
                         };
            // Add to the list
            int cta = 0;
            foreach (var item in query3)
            {
                Program.VerboseAudit("  item: bucket=" + item.bucket + " c=" + item.code + "desc=" + item.desc + " dt=" + item.evdt);
                var b = new gBucket();
                b.bucket = item.bucket;
                b.code = item.code;
                b.description = item.desc;
                b.evdt = item.evdt;
                b.has_all_deps = true;
                gBucket f = bucket_list.Find(x => x.evdt == item.evdt);
                if (f.evdt != item.evdt)
                {
                    bucket_list.Add(b);
                    cta++;
                }
            }
            Program.VerboseAudit("add ct=" + cta);
            return true;
        }


        private void AddVitalsBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field, string result_list, SearchDepth search_depth)
        {
            // Assign (virtual) bucket numbers to each assessment.  
            // Every x minutes after that is a new bucket.  Bucket zero = pull_start time.
            Program.VerboseAudit("----Locating Vital Signs: Filling buckets...Bucketsize=" + _bucket_size + " start time of first bucket=" + _pat.pull_start);

            // get the chart items for the assessments
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc, field, result_list);

            // This step is needed for those who want to count the # within a bucket, like Shands #24.
            // This will have no effect on those that count buckets.
            //var query2 = (from item in query select new { item.EVENT_DATETIME }).Distinct();
            //var query2 = (from item in query select new { item.EVENT_DATETIME, item.CODE });

            //foreach (var x in query2)
            //{
            //    Program.VerboseAudit("patpullstart=" + _pat.pull_start.ToString() + " itemevdt=" + x.EVENT_DATETIME.ToString() + " diff=" + (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, x.EVENT_DATETIME)) + " bucketsize=" + _bucket_size + " bucket=" + (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, x.EVENT_DATETIME) / _bucket_size));
            //}

            // figure out what buckets the events belong to
            //var query3 = from item in query2
            //             select new {
            //                bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size)
            //             };
            //// Add to the list
            //foreach (var item in query3) {
            //    bucket_list.Add(item.bucket);
            //}

            var query3 = from item in query
                         select new
                         {
                             bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                             code = item.CODE,
                             evdt = item.EVENT_DATETIME,
                             desc = item.DESCRIPTION
                         };
            var query4 = from item in query3
                         orderby item.bucket,item.code
                         select item;

            // Add to the list
            int currb = -1;
//            int prevb = -1;
            string currcode = "xx";
            DateTime currevdt = DateTime.MinValue;
            int currct = 0;
  //          bool addcurr = false;
            //Data is: 0, "123"
            //         0, "234"  0733
            //         0, "234"  0734
            //         1, "123"
            //         2, "234"
            //         2, "345"
            foreach (var item in query4)
            {
                int maxLength = Math.Min(item.desc.Length, 16);
                string d = item.desc.Substring(0, maxLength);
                Program.VerboseAudit("Vitals item: bucket=" + item.bucket + " dt=" + item.evdt + " desc=" + d + " c=" + item.code);
                if (currb != item.bucket)
                {
                    currb = item.bucket;
                    currcode = item.code;
                    currevdt = item.evdt;
                    currct = 1;
                }
                else
                {
                    if (currcode != item.code)
                    {  //minimum of 2 different VS codes
                        if (currct == 1)
                        { // add the first code first
                            var b = new gBucket();
                            b.bucket = item.bucket;
                            b.code = currcode;
                            b.evdt = currevdt;
                            b.has_all_deps = true;
                            //gBucket f = bucket_list.Find(x => x.evdt == item.evdt);
                            //if (f.evdt != item.evdt) bucket_list.Add(b);
                            bucket_list.Add(b);
                            Program.VerboseAudit("  adding item1: b=" + b.bucket + " c=" + b.code + " dt=" + b.evdt);
                        }
                        currct++;
                        currcode = item.code; //guarantees not to add code again in same bucket
                            
                        var b2 = new gBucket();
                        b2.bucket = item.bucket;
                        b2.code = item.code;
                        b2.evdt = item.evdt;
                        b2.has_all_deps = true;
                        //gBucket f = bucket_list.Find(x => x.evdt == item.evdt);
                        //if (f.evdt != item.evdt) bucket_list.Add(b);
                        bucket_list.Add(b2);
                        Program.VerboseAudit("  adding item2: b=" + b2.bucket + " c=" + b2.code + " dt=" + b2.evdt);
                    }
                }
            }

            // This is just for auditing: Say what we are looking for and how many were found
            if (!String.IsNullOrEmpty(result_list))
            {
                // print each word and if it was found or not
                //int i = CountResultContains(cat, code_list, desc, field, result_list);
            }
            else
            {
                // print how many were found
                //Program.VerboseAudit(Describe(cat, code_list, desc, field, ""));
            }
            Program.VerboseAudit("----End of Locating Vital Signs: Filling buckets...Bucketsize=" + _bucket_size);
        }


        private int CountBuckets(List<gBucket> bucket_list)       // bucket list: (ha ha)
        {
            //int result = bucket_list.Distinct().Count();
            //if (result > 0) Program.VerboseAudit(result + " unique");
            //return result;
            int x = -99;
            int result = 0;
            //int result = bucket_list.Distinct().Count();
            var query = from b in bucket_list
                        orderby b.bucket ascending
                        select b;
            foreach (var b in query)
            {
                if (x != b.bucket)
                {
                    result++;
                    x = b.bucket;
                }
            }
            if (result > 0) Program.VerboseAudit(result + " unique");
            return result;
        }
        private void Check_19()
        {
            string reslist;
            string codelist;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("19. Vascular Access Site Mgt q1 Hour");
            Program.VerboseAudit("---------------");

            //SetBucketSize(60);
            //exclude_periop_data = true;

            int[] medclass = {19,130,129,128,127,104,248,310,1171,532,111,
401589,401110,102,1512,376,175,233,11754,134,426};

            //string[] med19 = { "48058","401589","500077","500075","500076","500238","500192","500239" };
            string[] desc19 = { "dextrose 50 % injection", "sodium chloride 3 %" };

            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = query.Where(e => e.CODE.ToLower().StartsWith("mediv"));
            query = query.Where(e =>
                medclass.Any(item1 => e.CATEGORY == item1.ToString() || 
                                      e.CATEGORY.StartsWith(item1.ToString()+";") || 
                                      e.CATEGORY.Contains(";" + item1.ToString() + ";") || 
                                      e.CATEGORY.EndsWith(";" + item1.ToString())) || //bc the class can be 149 or 149;;255 or 255;;149;;331 or 244;;149
               desc19.Any(item2 => e.DESCRIPTION.ToLower().Contains(item2.ToLower())) ||
               e.DESCRIPTION.ToLower().Contains("nafcillin"));
            //               med19.Any(item2 => e.CODE.Contains(item2.ToUpper())));
            query = query.Where(e => e.RESULT.ToLower().Contains("given"));
            //query = query.Where(e => e.RESULT.Contains("^RN"));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            foreach (var item in query)
            {
                SetInd(19, "IV Med found: time=" +item.EVENT_DATETIME+" med="+ item.DESCRIPTION + " class="+item.CATEGORY+ " action="+item.FIELD_NAME+ " code="+item.CODE);
            }

            string found_what = "";
            if (CheckDialysis(out found_what))
            {
                g_dialysis = true;
                SetInd(19, "Dialysis found: " + found_what);
            }



        }

    private bool CheckDialysis(out string found_what)
        {
            bool ret = false;
            found_what = "";
            int[] medclass = { 557 };
            var q = StartNewQuery(SearchDepth.SearchDefault);
            q = q.Where(e => e.CODE.ToLower().StartsWith("med"));
            q = q.Where(e =>
                medclass.Any(item1 => e.CATEGORY == item1.ToString() ||
                                      e.CATEGORY.StartsWith(item1.ToString() + ";") ||
                                      e.CATEGORY.Contains(";" + item1.ToString() + ";") ||
                                      e.CATEGORY.EndsWith(";" + item1.ToString())));
            q = q.Where(e => e.RESULT.ToLower().Contains("given") || e.RESULT.ToLower().Contains("start") || e.RESULT.ToLower().Contains("newbag"));
            q = q.OrderByDescending(e => e.EVENT_DATETIME);
            if (q.Count() > 0)
            {
                ret = true;
                found_what = q.First().EVENT_DATETIME + " med=" + q.First().DESCRIPTION + " class=" + q.First().CATEGORY + " action=" + q.First().FIELD_NAME + " code=" + q.First().CODE;
            }
            return ret;
        }

    private void CheckRouteCath()
        {
            int ct = 0;
            //OBX|1|DT|MED9003 ^ ALTEPLASE 50 MG INTRAVENOUS SOLUTION;;;20;;;0.5;;;cath; ; ; RateVerify | RateVerify | 20190802000000 |||||| F ||| 20190802000000
            //OBX|1|DT|MED9003 ^ HEPARIN  MG INTRAVENOUS SOLUTION;;;20;;;0.5;;;cath; ; ; RateVerify | RateVerify | 20190802000000 |||||| F ||| 20190802000000
            var query = StartNewQuery(SearchDepth.SearchSince9Hrs);    // add custom time range below
            query = query.Where(e => e.CODE.StartsWith("med"));
            query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;cath"));
            query = query.Where(e => e.DESCRIPTION.ToUpper().Contains("ALTEPLASE") ||
                                     e.DESCRIPTION.ToUpper().Contains("HEPARIN"));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            Program.VerboseAudit("Med ALTEPLASE or HEPARIN with cath found: " + query.Count());
            foreach (var item in query)
            {
                Program.VerboseAudit("====Med found: ALTEPLASE or HEPARIN====");
                Program.VerboseAudit("med code: " + item.CODE + " desc: " + item.DESCRIPTION + "  evdt: " + item.EVENT_DATETIME.ToString());

                var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                if (arr.GetUpperBound(0) >= 3)
                {
                    string rte = arr[3];
                    if (rte.ToLower() == "cath") ct++;
                }
            }
            if (ct > 0)
                SetInd(19, "Found ALTEPLASE or HEPARIN with route cath.");
        }

        private void Check_20()
        {
            string reslist;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("20. Medication Activity >= 20 minutes");
            Program.VerboseAudit("---------------");

            //            "MAR Route Tube-Gastric, J-Tube 
            //and
            //MAR Action Given"
            string[] route20 = { "tube", "nebulization", "inhalation", "intrapleural"};

            var routeq = StartNewQuery(SearchDepth.SearchDefault);
            routeq = routeq.Where(e => e.CODE.ToLower().StartsWith("med"));
            routeq = routeq.Where(e => route20.Any(item1 => e.RESULT.ToLower().Contains(item1.ToLower())));
            routeq = routeq.Where(e => e.RESULT.ToLower().Contains("given"));
            routeq = routeq.Where(e => e.RESULT.ToUpper().Contains("^RN"));
            routeq = routeq.OrderBy(e => e.EVENT_DATETIME);
            if (routeq.Count() > 0)
                foreach (var item in routeq)
                {
                    foreach (var r in route20)
                    {
                        if (item.RESULT.ToLower().Contains(r.ToLower()))
                            SetInd(20, "Med given via " + r.ToUpper() + ": time=" + item.EVENT_DATETIME + " med=" + item.DESCRIPTION + " class=" + item.CATEGORY + " route/action=" + item.RESULT);
                    }
                }

            //Any MAR meds needing dual verification
            //look for "dilute with" in admin instructions
            //Any
            //"Med route ""Nebulization"" or ""Inhalation""
            //and
            //MAR Action Given
            //and
            //Adminstered by RN / LPN"

            //"Med route ""Intrapleural""
            //and
            //MAR Action Given "

            //"> 8 meds in 30min
            //and
            //MAR Action Given
            //and
            //Adminstered by RN / LPN"

            //"MAR Route IV
            //and
            //> 3 MAR Action of ""Rate change"" and / or ""Given"" and / or ""New Bag"" and / or ""Rate verify"" in an 8 hour time frame "


            //Active Order

            string[] act20 = { "Given", "NewBag", "RateVerify", "RateChange", "Restarted" };
            var ivquery = StartNewQuery(SearchDepth.SearchDefault);
            ivquery = ivquery.Where(e => e.CODE.ToLower().StartsWith("mediv"));
            ivquery = ivquery.Where(e => act20.Any(item1 => e.RESULT.ToLower().Contains(item1.ToLower())));
            ivquery = ivquery.OrderBy(e => e.EVENT_DATETIME);
            if (ivquery.Count() >= 3)
            {
                Program.VerboseAudit("Found IV Meds with actions, count=" + ivquery.Count());
                foreach (var item in ivquery)
                {
                    SetInd(20, "IV Med found at least 3 in 8 hrs: time=" + item.EVENT_DATETIME + " med=" + item.DESCRIPTION + " class=" + item.CATEGORY + " route/action=" + item.RESULT);
                }
            }

            SetIndIfResultContains(20, "", "12216843", "", "", "");

            //string[] medclass = { "CHOLINESTERASE", "HEPARIN"};
            int[] medclass = { 241, 293 };
            var query1 = StartNewQuery(SearchDepth.SearchDefault);
            query1 = query1.Where(e => e.CODE.ToLower().StartsWith("med"));
            //query1 = query1.Where(e => medclass.Any(item1 => e.CATEGORY.ToLower().Contains(item1.ToLower())));
            query1 = query1.Where(e => medclass.Any(item1 => e.CATEGORY == item1.ToString()));
            query1 = query1.Where(e => e.RESULT.ToLower().Contains("given"));
            query1 = query1.Where(e => e.RESULT.ToUpper().Contains("^RN"));
            query1 = query1.OrderBy(e => e.EVENT_DATETIME);
            foreach (var item in query1)
            {
                SetInd(20, "Heparin or Cholinesterase Med Given: time=" + item.EVENT_DATETIME + " med=" + item.DESCRIPTION + " class=" + item.CATEGORY + " route/action=" + item.RESULT);
            }

            var query8 = StartNewQuery(SearchDepth.SearchDefault);
            query8 = query8.Where(e => e.CODE.ToLower().StartsWith("med"));
            query8 = query8.Where(e => e.RESULT.ToLower().Contains("given"));
            query8 = query8.Where(e => e.RESULT.ToUpper().Contains("^RN"));
            query8 = query8.OrderBy(e => e.EVENT_DATETIME);
            if (query8.Count() >= 8)
            {
                Program.VerboseAudit("Found at least 8 meds given in 8 hours, count=" + query8.Count());
                Program.VerboseAudit("Now determine if there are 8 within 30 minutes....");
                DateTime evdt = DateTime.MinValue;
                foreach (var item in query8)
                {
                    if (item.EVENT_DATETIME > evdt)
                    {
                        evdt = item.EVENT_DATETIME;
                        var query8b = StartNewQuery(SearchDepth.SearchDefault);
                        query8b = query8b.Where(e => e.CODE.ToLower().StartsWith("med"));
                        query8b = query8b.Where(e => e.RESULT.ToLower().Contains("given"));
                        query8b = query8b.Where(e => e.RESULT.ToUpper().Contains("^RN"));
                        query8b = query8b.Where(e => e.EVENT_DATETIME >= evdt
                                                &&
                                        e.EVENT_DATETIME <= evdt.AddMinutes(30));
                        if (query8b.Count() >= 8)
                            SetInd(20, "Found >=8 meds given by RN in 30 mins, count=" + query8b.Count() + " starting at=" + evdt);
                    }
                }
            }

        }


        private void CheckMedGiven(int ind, string desc)
        {

            string[] transplantmed_actions = { "given", "newbag", "new bag", "rateverify", "rate verify", "rate change", "ratechange", "restarted", "started/down" };

            for (int i = 0; i <= transplantmed_actions.GetUpperBound(0); i++)
            {
                transplantmed_actions[i] = ";;;" + transplantmed_actions[i];
            }

            int ct = 0;
            var query = StartNewQuery(SearchDepth.SearchSince9Hrs);
            query = AndItemFilter(query, "", "MED", desc, "", "");
            query = query.Where(e => e.DESCRIPTION.ToLower().ContainsAny(transplantmed_actions));
            ct = query.Count();
            if (ct > 0) SetInd(ind, "Found Med=" + desc);

        }

        private int CountMedsIn30min()
        {
            string descript = "";
            string drugclass = "";
            string result = "";
            int ct = 0;
            DateTime evdt = DateTime.MinValue;
            DateTime endevdt = DateTime.MinValue;

            //  CALCIUM CHLORIDE 100 MG / ML(10 %) INTRAVENOUS SYRINGE; ; ; 29; ; ; 1; ; ; IV; ; ; Given
            //  SODIUM BICARBONATE 8.4 % (1 MEQ / ML) INJECTION(WRAPPER); ; ; 29; ; ; 50; ; ; osse; ; ; Given
            var query = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
            query = query.Where(e => e.CODE.ToUpper().StartsWith("MED"));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            //Program.VerboseAudit("Num Meds found: " + query.Count());
            foreach (var item in query)
            {
                Program.VerboseAudit("Med=" + item.EVENT_DATETIME + ": " + item.DESCRIPTION);
                if (item.EVENT_DATETIME > evdt)
                {
                    evdt = item.EVENT_DATETIME;
                    endevdt = evdt.AddMinutes(30);
                    if (endevdt > loc_out) endevdt = loc_out;
                    var query2 = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
                    query2 = query2.Where(e => e.CODE.ToUpper().StartsWith("MED"));
                    query2 = query2.Where(e => e.EVENT_DATETIME >= evdt && e.EVENT_DATETIME <= endevdt);
                    if (query2.Count() > ct)
                        ct = query2.Count();
                }
            }

            Program.VerboseAudit("Greatest count of unique Admin times in a 30 minute period=" + ct);
            return ct;
        }
        private int CountIntravenous()
        {
            int ct = 0;
            int count = 0;
            DateTime evdt = DateTime.MinValue;
            DateTime endevdt = DateTime.MinValue;

            var query = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
            query = query.Where(e => e.CODE.ToUpper().StartsWith("MED"));
            query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;intravenous"));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            //Program.VerboseAudit("Num Meds found: " + query.Count());
            foreach (var item in query)
            {
                Program.VerboseAudit("IV Med=" + item.EVENT_DATETIME + ": " + item.DESCRIPTION);
                if (item.EVENT_DATETIME > evdt)
                {
                    evdt = item.EVENT_DATETIME;
                    endevdt = evdt.AddHours(8);
                    if (endevdt > loc_out) endevdt = loc_out;
                    var query2 = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
                    query2 = query2.Where(e => e.CODE.ToUpper().StartsWith("MED"));
                    query2 = query2.Where(e => e.DESCRIPTION.ToLower().Contains(";;intravenous"));
                    query2 = query2.Where(e => e.EVENT_DATETIME >= evdt && e.EVENT_DATETIME <= endevdt);
                    count = query2.Count();
                    if (count > ct)
                        ct = count;
                }
            }

            Program.VerboseAudit("Greatest count of IV Admin times in an 8-hour period=" + ct);
            return ct;
        }


        private void Check_21_22()
        {
            string reslist;
            bool st1 = false;
            string piv;
            string codelist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("21. Wound/Injury Mgmt");
            Program.VerboseAudit("22. Wound/Injury Mgmt >= 30 Minutes");
            Program.VerboseAudit("---------------");


            reslist = "";
            SetIndIfResultContains(22, "", EXACT_MATCH_PREFIX+"702", "", "", reslist);

            reslist = "dressing changed";
            SetIndIfResultContains(22, "", "800785", "", "", reslist);

            reslist = "dressing applied,dressing changed,dressing moistened,dressing reinforced,dressing removed,antimicrobial agent applied,skin substitute applied,skin barrier agent applied,wound filler utilized,packing removed,wound pouch applied,wound pouch removed,wound pouch intact";
            SetIndIfResultContains(21, "", "4012548", "", "", reslist);
            reslist = "packed with";
            SetIndIfResultContains(22, "", "4012548", "", "", reslist);

            reslist = "cleansed with,antimicrobial agent applied,barrier applied,collegenase agent applied,dressing removed,enzymatic agent applied,growth factor agent applied,honey applied,liquid bandage applied,neutral pH skin disinfectant applied,pressure removed,pressure dressing removed,silver agent applied,surfectant agent applied";
            SetIndIfResultContains(21, "", "4010210", "", "", reslist);
            reslist = "irrigated with,pressure applied,pressure dressing applied,hydrotherapy provided,negative pressure wound therapy,phototherapy,pulsatile high pressure lavage,ultrasound therapy";
            SetIndIfResultContains(22, "", "4010210", "", "", reslist);

            reslist = ">3 of these present";
            int ct = CountResultContains("", "12053619", "", "", "");
            if (ct >= 3)
                SetInd(22, "Count=" + ct + " of 12053619.");

            reslist = "";
            SetIndIfResultContains(22, "", "12059274", "", "", reslist);

            reslist = "wound/incision splinting encouraged";
            SetIndIfResultContains(21, "", "1224156", "", "", reslist);
            reslist = "abdominal binder secured in place,exposed bowel covered with sterile saline soaked gauze,prepared for surgical intervention,other (see comments)";
            SetIndIfResultContains(22, "", "1224156", "", "", reslist);

            reslist = "Dressing changed,Dressing reinforced by provider,Gauze applied,Removed,Transparent semipermeable dressing applied,Other (Comment)";
            SetIndIfResultContains(21, "", "12059557", "", "", reslist);
            reslist = "Antimicrobial dressing applied,Securement device changed";
            SetIndIfResultContains(22, "", "12059557", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(21, "", "12054035", "", "", reslist);
            SetIndIfResultContains(21, "", "12046729", "", "", reslist);
            SetIndIfResultContains(21, "", "12034275", "", "", reslist);
            SetIndIfResultContains(21, "", "12055487", "", "", reslist);

            
            SetIndIfResultContains(22, "", "12046749", "", "", "arter");
            SetIndIfResultContains(22, "", "12211558", "", "", "dressing changed");
            reslist = "dressing applied,dressing changed,dressing moistened,dressing reinforced";
            reslist += ",dressing removed,antimicrobial agent applied,skin substitute applied";
            reslist += ",skin barrier agent applied,wound filler utilized,packing removed";
            reslist += ",wound pouch applied,wound pouch removed,wound pouch intact";
            SetIndIfResultContains(21, "", "12050832", "", "", reslist);
            SetIndIfResultContains(22, "", "12050832", "", "", "packed with");

            reslist = "cleansed with,antimicrobial agent applied,barrier applied,collegenase agent applied";
            reslist += ",dressing removed,enzymatic agent applied,growth factor agent applied,honey applied";
            reslist += ",liquid bandage applied,neutral pH skin disinfectant applied,pressure removed";
            reslist += ",pressure dressing removed,silver agent applied,surfectant agent applied";
            SetIndIfResultContains(21, "", "12048494", "", "", reslist);

            reslist = "irrigated with,pressure applied,pressure dressing applied,hydrotherapy provided";
            reslist += ",negative pressure wound therapy,phototherapy,pulsatile high pressure lavage,ultrasound therapy";
            SetIndIfResultContains(22, "", "12048494", "", "", reslist);

            ct = CountItems("", "12053619", "", "", "");
            if (ct >= 3)
                SetInd(22, "Wound count gt 3 count=" + ct);
        }

        private void CheckDrainLDA()
        {
            string codelist = "9993040108530,3045001091";
            DateTime dt = DateTime.MinValue;
            var query1 = StartNewQuery(SearchDepth.SearchSince16Hrs);
            query1 = AndItemFilter(query1, "", codelist, "", "", "");
            query1 = query1.OrderBy(e => e.EVENT_DATETIME);
            foreach (var item in query1)
            { // for each start, find the number of records within 1 hr of it.
                if (dt == DateTime.MinValue)
                {
                    int ct = CountDrainLDAIn1Hour(codelist, item.EVENT_DATETIME);
                    if (ct >= 6)
                    {
                        SetInd(22, "Count of Drain LDAs within 1 hour=" + ct + " starting at: " + item.EVENT_DATETIME.ToString());
                        dt = item.EVENT_DATETIME;
                    }
                }
            }
            return;
        }

        private int CountDrainLDAIn1Hour(string codelist, DateTime startdt)
        {
            int ct = 0;
            var query1 = StartNewQuery(SearchDepth.SearchSince16Hrs);
            query1 = AndItemFilter(query1, "", codelist, "", "", "");
            query1 = query1.Where(e => e.EVENT_DATETIME >= startdt);
            query1 = query1.Where(e => e.EVENT_DATETIME <= startdt.AddMinutes(60));
            ct = query1.Count();
            return ct;

        }

        int EducMins(string timecode, string nursecode)
        {
            string res;
            int val = 0;
            if (ResultContains("", nursecode, "", "", "Nursing"))
            {
                if (GetResult("", timecode, "", "", out res))
                {
                    if (res.Left(1).IsNumeric())
                    {
                        val += (int)res.Val();
                    }
                }
            }
            return val;
        }

        private void Check_23()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("23. Healthcare Mgmt Education >= 1 Hour");
            Program.VerboseAudit("---------------");

            string reslist;
            reslist = "Anything >60";
            SetIndIfResultBetween(23, "", "12036190", "", "", 60, 999);
            reslist = "Anything >60";
            SetIndIfResultBetween(23, "", "12060940", "", "", 60, 999);
            reslist = "Anything >60";
            SetIndIfResultBetween(23, "", "12060943", "", "", 60, 999);

            reslist = "";
            SetIndIfResultContains(23, "", "4025269", "", "", reslist);
            SetIndIfResultContains(23, "", "4025426", "", "", reslist);
            SetIndIfResultContains(23, "", "4025275", "", "", reslist);
            SetIndIfResultContains(23, "", "4025625", "", "", reslist);
            SetIndIfResultContains(23, "", "4025309", "", "", reslist);
            SetIndIfResultContains(23, "", "4025359", "", "", reslist);
            SetIndIfResultContains(23, "", "4033298", "", "", reslist);
            SetIndIfResultContains(23, "", "4033264", "", "", reslist);
            SetIndIfResultContains(23, "", "4033294", "", "", reslist);
            SetIndIfResultContains(23, "", "4025279", "", "", reslist);
            SetIndIfResultContains(23, "", "4025310", "", "", reslist);
            SetIndIfResultContains(23, "", "4025315", "", "", reslist);
            SetIndIfResultContains(23, "", "4033255", "", "", reslist);
            SetIndIfResultContains(23, "", "4025442", "", "", reslist);
            SetIndIfResultContains(23, "", "4025357", "", "", reslist);
            SetIndIfResultContains(23, "", "4025606", "", "", reslist);
            SetIndIfResultContains(23, "", "4025292", "", "", reslist);
            SetIndIfResultContains(23, "", "4025491", "", "", reslist);
            SetIndIfResultContains(23, "", "4033297", "", "", reslist);
            SetIndIfResultContains(23, "", "4025472", "", "", reslist);
            SetIndIfResultContains(23, "", "4025508", "", "", reslist);
            SetIndIfResultContains(23, "", "4025540", "", "", reslist);
            SetIndIfResultContains(23, "", "4033278", "", "", reslist);
            SetIndIfResultContains(23, "", "4033258", "", "", reslist);
            SetIndIfResultContains(23, "", "4033303", "", "", reslist);
            SetIndIfResultContains(23, "", "4025492", "", "", reslist);
            SetIndIfResultContains(23, "", "4025384", "", "", reslist);
            SetIndIfResultContains(23, "", "4025376", "", "", reslist);
            SetIndIfResultContains(23, "", "4025556", "", "", reslist);
            SetIndIfResultContains(23, "", "4025438", "", "", reslist);
            SetIndIfResultContains(23, "", "4025659", "", "", reslist);

        }
        private void AddEducActivity(DateTime evdt)
        {
            DateTime enddt;
            Program.VerboseAudit("Activity 5: Found at " + evdt.AddHours(-1).ToString());
            if (!QueuedProcOverlaps(5, evdt.AddHours(-1), evdt))
                if (!ProcExistsInDB(5, evdt.AddHours(-1), out enddt))
                {
                    var proc = new proc_data();
                    proc.procedure_number = 5;
                    proc.start = evdt.AddHours(-1);
                    proc.finish = evdt;
                    _procs.Add(proc);
                    Program.Audit("Activity 5: Found at " + evdt);
                }
        }

        private void Check_24()
        {
            string reslist = "";
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("24. 1 to 1 Physiological Interv. >= 2 Hours");
            Program.VerboseAudit("---------------");

            string desc_found = "";
            string[] ordercode_str = {"658","5482","5827","3074","2697","1967","3160",
                "4063","5472","512","5310","479","5351","3069","404","1086","5035",
                "771",
                "5887","2709","351137", "340711","350414",
"341093","341098","341000","340200","351201","351149","350419","223533","380250"};

            string[] ordercode2_str = { "340201","300500"};
            foreach (string ordstr in ordercode2_str)
            {
                if (OrderInProgressByCode(ordstr, out desc_found))
                    SetInd(24, "Order in effect: " + desc_found);
            }

            SearchDepth sd = SearchDepth.SearchSince4Hrs;

            string codelist = "5887,2709,351137,340711,350414,341093,341098,341000,340200,351201,351149,350419,223533,380250";
            SetIndIfResultContains(24, "", codelist, "", "", "", sd);

            int ct =CountItemsWithinNhours("1228513", "", sd, 2);
            if (ct >= 9)
                SetInd(24, "Uploaded Blood Glucose count>=9 =" + ct);

            reslist = ""; // "Capture Time";
            SetIndIfResultContains(24, "", "12056677", "", "", reslist,sd);
            reslist = "";
            SetIndIfResultContains(24, "", "12056679", "", "", reslist, sd);

            

            //reslist = "fall (during this admission),hypoglycemic event";
            //SetIndIfResultDoesNotContain(24, "", "12045758", "", "", reslist, sd);
            //SetIndIfResultDoesNotContain(24, "", "12065233", "", "", reslist, sd);
            reslist = "adverse medication reaction,medical code,OB alert,rapid response,seizure,stemi alert,stroke alert";
            SetIndIfResultContains(24, "", "12045758", "", "", reslist, sd);
            //SetIndIfResultContains(24, "", "12065233", "", "", reslist, sd);

            //string codelist = "658,5482,5827,3074,2697,1967,3160,4063,5472,512,5310,479,5351,3069,404,1086";
            //codelist += ",5035,771,5887,2709,351137,340131,239926,239928,620886,16865,340711";
            //SetIndIfResultContains(24, "", codelist, "", "", "");

            if (g_dialysis)
                SetInd(24, "Dialysis found: see indicator 19 for details.");


            Program.VerboseAudit("Locating Vitals sets....");
            //Complete vitals is considered bp / pulse / resp > 8 sets of vs in 2 hrs or more
            //9005 = bp 
            //9008 = pulse
            //9009 = resp
            SetBucketSize(60);   // set 60-min buckets
            var buckets = new List<gBucket>();    // list of bucket numbers (for each match)
            var buckets2 = new List<gBucket>();    // list of bucket numbers (for each match)
            var buckets3 = new List<gBucket>();    // list of bucket numbers (for each match)
            int buck_ct = 0;

            codelist = "9005,12210717";
            if (!AddBuckets(buckets, "", codelist, "", "", "", sd)) return;
            // Instead of counting the number of buckets to get the frequency,
            // we want to see if any two hours in a row had 8 or more assessments in the buckets.
            // Make a query that returns each bucket number and its item count.
            buck_ct = buckets.ToList().Count();
            Program.VerboseAudit("#buckets for " + codelist + " = " + buck_ct);
            if (buck_ct < 1) return;
            var query = buckets.GroupBy(x => x.bucket)   // group by the hour
                               .Select(g => new { Hour = g.Key, Count = g.Count() })
                               .OrderBy(e => e.Hour);
            var arr = query.ToArray();

            string codelist2 = "9008,12210768,12210704";
            if (!AddBuckets(buckets2, "", codelist2, "", "", "", sd)) return;
            buck_ct = buckets2.ToList().Count();
            Program.VerboseAudit("#buckets for " + codelist2 + " = " + buck_ct);
            if (buck_ct < 1) return;
            var query2 = buckets2.GroupBy(x => x.bucket)   // group by the hour (x is an int)
                               .Select(g => new { Hour = g.Key, Count = g.Count() })
                               .OrderBy(e => e.Hour);
            var arr2 = query2.ToArray();

            string codelist3 = "9009";
            if (!AddBuckets(buckets3, "", codelist3, "", "", "", sd)) return;
            buck_ct = buckets3.ToList().Count();
            Program.VerboseAudit("#buckets for " + codelist3 + " = " + buck_ct);
            if (buck_ct < 1) return;
            var query3 = buckets3.GroupBy(x => x.bucket)   // group by the hour (x is an int)
                               .Select(g => new { Hour = g.Key, Count = g.Count() })
                               .OrderBy(e => e.Hour);
            var arr3 = query3.ToArray();

            Program.VerboseAudit("Locating Vitals sets....1");
            string s = "";
            foreach (var e in arr)
            {
                s += ",(" + e.Hour + "," + e.Count + ")";
            }
            if (s.Left(1) == ",") s = s.Substring(1);
            Program.VerboseAudit("Hourly counts for " + codelist + " = " + s);

            s = "";
            foreach (var e in arr2)
            {
                s += ",(" + e.Hour + "," + e.Count + ")";
            }
            if (s.Left(1) == ",") s = s.Substring(1);
            Program.VerboseAudit("Hourly counts for " + codelist2 + " = " + s);

            s = "";
            foreach (var e in arr3)
            {
                s += ",(" + e.Hour + "," + e.Count + ")";
            }
            if (s.Left(1) == ",") s = s.Substring(1);
            Program.VerboseAudit("Hourly counts for " + codelist3 + " = " + s);

            Program.VerboseAudit("Locating Vitals sets....2");
            //===A===
            codelist = "9005,12210717";
            var bucketsA = new List<gBucket>();                      // list of bucket numbers (for each match)
            AddBuckets(bucketsA, "", codelist, "", "", "", sd);
            var queryA = bucketsA.GroupBy(x => x.bucket)                 // group by the hour (x is an int)
                               .Select(g => new { Hour = g.Key, Count = g.Count() })
                               .OrderBy(e => e.Hour);
            var arrA = queryA.ToArray();

            s = "";
            foreach (var e in arrA)
            {
                s += ",(" + e.Hour + "," + e.Count + ")";
            }
            if (s.Left(1) == ",") s = s.Substring(1);
            Program.VerboseAudit("VS hourly counts BP: " + s);
            //===A===
            //===B===
            codelist = "9008,12210768,12210704";
            var bucketsB = new List<gBucket>();                      // list of bucket numbers (for each match)
            AddBuckets(bucketsB, "", codelist, "", "", "", sd);
            var queryB = bucketsB.GroupBy(x => x.bucket)                 // group by the hour (x is an int)
                               .Select(g => new { Hour = g.Key, Count = g.Count() })
                               .OrderBy(e => e.Hour);
            var arrB = queryB.ToArray();

            s = "";
            foreach (var e in arrB)
            {
                s += ",(" + e.Hour + "," + e.Count + ")";
            }
            if (s.Left(1) == ",") s = s.Substring(1);
            Program.VerboseAudit("VS hourly counts PULSE: " + s);
            //===B===
            //===C===
            codelist = "9009";
            var bucketsC = new List<gBucket>();                      // list of bucket numbers (for each match)
            AddBuckets(bucketsC, "", codelist, "", "", "", sd);
            var queryC = bucketsC.GroupBy(x => x.bucket)                 // group by the hour (x is an int)
                               .Select(g => new { Hour = g.Key, Count = g.Count() })
                               .OrderBy(e => e.Hour);
            var arrC = queryC.ToArray();

            s = "";
            foreach (var e in arrC)
            {
                s += ",(" + e.Hour + "," + e.Count + ")";
            }
            if (s.Left(1) == ",") s = s.Substring(1);
            Program.VerboseAudit("VS hourly counts RESP: " + s);
            //===C===
            for (int i = 0; (i < arrA.GetUpperBound(0)); i++)
            {
                // Are these two hours in a row? (beware missing hours) 
                // .Hour is the key which is a record: bucket,code,evdt,descr
                if (arrA[i].Hour == arrA[i + 1].Hour-1)
                {
                    // Add these two consecutive hours
                    // One set is one of each bp/puls/resp, so need at least 8 of each in 2 hrs
                    if (arrA[i].Count + arrA[i + 1].Count > 8)
                    {
                        for (int j = 0; (j < arrB.GetUpperBound(0)); j++)
                        {
                            if ((arrB[j].Hour == arrA[i].Hour) && (arrB[j].Hour == arrB[j + 1].Hour - 1))
                            {
                                if (arrB[j].Count + arrB[j + 1].Count > 8)
                                {
                                    for (int k = 0; (k < arrC.GetUpperBound(0)); k++)
                                    {
                                        if ((arrC[k].Hour == arrA[i].Hour) && (arrC[k].Hour == arrC[k + 1].Hour - 1))
                                        {
                                            if (arrC[k].Count + arrC[k + 1].Count > 8)
                                            {
                                                SetInd(24, "At greater than 8 sets of VS found within 2 hours");
                                                //ind24_via_vs = true;
                                                return;
                                            }
                                        }
                                    }

                                }
                            }
                        }

                    }
                }
            }



        } // Check_24


        private void CheckUserDefined()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("User-Defined indicators");
            Program.VerboseAudit("---------------");

            //reslist = "Patient arrived during downtime,Patient was cared for during downtime,Patient departed during downtime";
            //SetIndIfResultContains(99, "", "9991600100203", "", "", reslist);
            //reslist = "Yes";
            //SetIndIfResultContains(99, "", "9990000006437", "", "", reslist);
            //reslist = "Green,Yellow ,Red ,Black ";
            //SetIndIfResultContains(99, "", "9991600100265", "", "", reslist);
            //reslist = "Downtime";
            //SetIndIfResultContains(99, "", "9990007096330", "", "", reslist);

        }


        //=====================================================================
        // Group like assessments into buckets of time
        // Each bucket counts as one assessment in the frequency count

        private void AddDependentBuckets(List<gBucket> bucket_list, string codelist1, string reslist1, string codelist2, string reslist2)
        {
            AddDependentBuckets(bucket_list, codelist1, reslist1, codelist2, reslist2, "", "");
        }

        private void AddDependentBuckets(List<gBucket> bucket_list, string codelist1, string reslist1, string codelist2, string reslist2, string codelist3, string reslist3)
        {
            bool dep3 = true;
            // get the chart items for the assessments
            var query1 = StartNewQuery();
            query1 = AndItemFilter(query1, "", codelist1, "", "", reslist1);
            var query2 = StartNewQuery();
            query2 = AndItemFilter(query2, "", codelist2, "", "", reslist2);
            if (codelist3.Trim() == "")
            {
                dep3 = false;
                codelist3 = "Hello, this is a phantom code";
            }
            var query3 = StartNewQuery();
            query3 = AndItemFilter(query3, "", codelist3, "", "", reslist3);

            // figure out what buckets the events belong to
            var query1a = from item in query1
                          select new
                          {
                              bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                              code = item.CODE,
                              evdt = item.EVENT_DATETIME
                          };
            var query2a = from item in query2
                          select new
                          {
                              bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                              code = item.CODE,
                              evdt = item.EVENT_DATETIME
                          };
            var query3a = from item in query3
                          select new
                          {
                              bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                              code = item.CODE,
                              evdt = item.EVENT_DATETIME
                          };

            string s = "BucketList1 for " + codelist1 + ": ";
            foreach (var item in query1a)
            {
                s += item.bucket + ": dt=" + item.evdt +" ";
            }
            Program.VerboseAudit(s);

            s = "BucketList2 for " + codelist2 + ": ";
            foreach (var item in query2a)
            {
                s += item.bucket + ": dt=" + item.evdt + " ";
            }
            if (dep3)
            {
                s = "BucketList3 for " + codelist3 + ": ";
                foreach (var item in query3a)
                {
                    s += item.bucket + "";
                }
            }
            Program.VerboseAudit(s);
            // Add to the list IFF items in both lists occur in same bucket
            foreach (var item1 in query1a)
            {
                foreach (var item2 in query2a)
                {
                    if (item1.evdt == item2.evdt)
                    {
                        if (dep3)
                        {
                            foreach (var item3 in query3a)
                            {
                                if (item1.bucket == item3.bucket)
                                {
                                    var b = new gBucket();
                                    b.bucket = item1.bucket;
                                    b.code = item1.code;
                                    b.evdt = item1.evdt;
                                    bucket_list.Add(b);
                                }
                            }
                        }
                        else
                        {
                            var b = new gBucket();
                            b.bucket = item1.bucket;
                            b.code = item1.code;
                            b.evdt = item1.evdt;
                            bucket_list.Add(b);
                        }
                    }
                }
            }

        }

        private void AddSimpleProc(int pnum, DateTime evdt, DateTime enddt)
        {
            if (pnum <= 0) return;

            if (ProcExistsInMemory(pnum, evdt, enddt))
            {
                Program.Audit("Activity " + pnum + " at start=" + evdt + " was already triggered earlier in this run.");
            }
            else if (ProcExists(pnum, evdt, enddt))
            {
                Program.Audit("Activity " + pnum + ": already exists");
            }
            else
            {
                //if (ActivityFits(evdt, enddt))
                {
                    bool overlap=ProcOverlapsInDB_PEID(pnum, evdt, enddt); // then delete the db
                    if (!overlap)
                    {
                        var proc = new proc_data();
                        proc.procedure_number = pnum;
                        proc.start = evdt;
                        proc.finish = enddt;
                        _procs.Add(proc);
                        Program.Audit("Activity " + pnum + ": Found between " + evdt + " and " + enddt);
                    }
                }
            }
        }

        private bool ProcExistsInMemory(int pnum, DateTime st, DateTime fin)
        {
            bool ret = false;
            foreach (var p in _procs)
            {
                if (p.procedure_number == pnum && p.start == st)
                    ret = true;
            }
            return ret;
        }

        private bool ActivityFits(DateTime beg, DateTime fin)
        {
            bool ok = false;
            int unit_id = 0;
            string sql = "select el.unit_id from ENCOUNTER_LOCATION as el";
            sql += " where el.ENCOUNTER_ID=" + _pat.encounter_id;
            //sql += " and el.SPECIAL_UNIT_ID is null";
            sql += " and el.EFFECTIVE_DATETIME_IN<=" + beg.ToString() + "";
            sql += " and el.EFFECTIVE_DATETIME_OUT>=" + fin.ToString() + "";

            Program.VerboseAudit(sql);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd = new SqlCommand(sql, db2);
            SqlDataReader dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr2.Read())
            {
                if (dr2["UNIT_ID"] != DBNull.Value)
                    unit_id = PFSDBUtility.DBToInt(dr2["UNIT_ID"]);
            }
            ok = (unit_id > 0);

            if (ok)
            {
                db2.Close();
                return ok;
            }
            //Now check if two adjacent same units contains the activity.
            int unitid1 = 0;
            int unitid2 = 0;
            sql = "select el.unit_id from ENCOUNTER_LOCATION as el";
            sql += " where el.ENCOUNTER_ID=" + _pat.encounter_id;
            sql += " and " + beg.ToString() + "' between el.EFFECTIVE_DATETIME_IN and el.EFFECTIVE_DATETIME_OUT";
            cmd = new SqlCommand(sql, db2);
            dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr2.Read())
            {
                if (dr2["UNIT_ID"] != DBNull.Value)
                    unitid1 = PFSDBUtility.DBToInt(dr2["UNIT_ID"]);
            }
            sql = "select el.unit_id from ENCOUNTER_LOCATION as el";
            sql += " where el.ENCOUNTER_ID=" + _pat.encounter_id;
            sql += " and " + fin.ToString() + "' between el.EFFECTIVE_DATETIME_IN and el.EFFECTIVE_DATETIME_OUT";
            cmd = new SqlCommand(sql, db2);
            dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr2.Read())
            {
                if (dr2["UNIT_ID"] != DBNull.Value)
                    unitid2 = PFSDBUtility.DBToInt(dr2["UNIT_ID"]);
            }
            ok = (unitid1 > 0 && unitid1 == unitid2);
            //db2.Close();
            return ok;


        }

        //6	2019-09-18 15:00:00.000	2019-09-18 19:00:00.000
        //6	2019-09-19 07:00:00.000	2019-09-19 11:00:00.000

        private bool ActivityDuringClassType6(DateTime beg, DateTime fin, out DateTime classout)
        {
            bool ok = false;
            int pt = 0;
            classout = DateTime.MinValue;
            string sql = "select ce.PATIENT_TYPE,ce.effective_datetime_in,ce.effective_datetime_out from CLASSIFICATION_EVENT as ce";
            sql += " where ce.ENCOUNTER_ID=" + _pat.encounter_id;
            sql += " and ce.PATIENT_TYPE=6";
            sql += " and (" + beg.ToString() + "' >=ce.EFFECTIVE_DATETIME_IN and " + beg.ToString() + "' <ce.EFFECTIVE_DATETIME_OUT";
            sql += " or " + beg.ToString() + "' <ce.EFFECTIVE_DATETIME_IN and " + fin.ToString() + "' >ce.EFFECTIVE_DATETIME_OUT";
            sql += " or " + fin.ToString() + "' >ce.EFFECTIVE_DATETIME_IN and " + fin.ToString() + "' <=ce.EFFECTIVE_DATETIME_OUT)";
            Program.VerboseAudit(sql);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd = new SqlCommand(sql, db2);
            SqlDataReader dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr2.Read())
            {
                if (dr2["PATIENT_TYPE"] != DBNull.Value)
                {
                    pt = PFSDBUtility.DBToInt(dr2["PATIENT_TYPE"]);
                    classout = PFSDBUtility.DBToDateTime(dr2["EFFECTIVE_DATETIME_OUT"]);
                }
            }
            ok = (pt == 6);
            db2.Close();
            return ok;
        }

        private void AtLeastOneADL()
        {
            Program.Audit("---------------");

            if (!(_inds[2].is_checked || _inds[3].is_checked || _inds[4].is_checked)) {
                // Make "#2 ADL - Assist" the default.  (90% of patients)
                SetInd(1, "Defaulting to ADL Self due to lack of documentation.");
            }
        }

        private void HighestIndicatorInEachGroupWins()
        {
            int i, g;
            bool highest_is_on;
            string ind_list;

            Program.Audit("Select highest indicator in each group");

            g = 0;
            highest_is_on = false;
            // Go from bottom up - highest (lowest) indicator in each group wins
            for (i = MAX_INDS; (i >= 1); i--) {
                if (_inds[i].radio_group > 0) {
                    if (_inds[i].radio_group != g) {
                        //this is a new group
                        g = _inds[i].radio_group;
                        highest_is_on = _inds[i].is_checked;
                    } else {
                        //same group
                        if (highest_is_on) {
                            _inds[i].is_checked = false;             //uncheck a lower number
                        } else {
                            highest_is_on = _inds[i].is_checked;     //save this one
                        }
                    }
                }
            } // next i

            //Echo the indicators for an Audit (no classification will be saved)
            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++) {
                if (_inds[i].is_checked) ind_list += "," + i;
            } // next i

            Program.Audit("Final list = " + ind_list.Substring(1));
            Program.Audit("---------------");
        }


        private void CheckProcs()
        {
            //CheckProc_1();
            CheckProc_2();
            CheckProc_3();
            CheckProc_4();
            //CheckProc_5();
            CheckProc_6();
            CheckProc_7();
            //CheckProc_8();
            CheckProc_9();
            //CheckProc_10();
            //CheckProc_11();

        }

        //private void DoProc(int pnum, string code)
        //{
        //    double mins = 0;
        //    string found_what;
        //    DateTime evdt;
        //    DateTime enddt = DateTime.MinValue;

        //    if (GetResultAndEVDT("", code, "", "", out found_what, out evdt))
        //    {
        //        //mins = 60.0 * found_what.ToDouble();
        //        if (found_what.Contains("180")) mins = 180;
        //        else if (found_what.Contains("120")) mins = 120;
        //        else if (found_what.Contains("90")) mins = 90;
        //        else if (found_what.Contains("60")) mins = 60;
        //        enddt = evdt.AddMinutes(mins);

        //        if (ProcExistsInDB(pnum, evdt, enddt))
        //        {
        //            Program.Audit("Activity " + pnum+ ": already exists");
        //        }
        //        else
        //        {
        //            if (!QueuedProcOverlaps(pnum, evdt, enddt))
        //            {
        //                var proc = new proc_data();
        //                proc.procedure_number = pnum;
        //                proc.start = evdt;
        //                proc.finish = enddt;
        //                _procs.Add(proc);
        //                Program.Audit("Activity " + pnum + ": Found " + code + " between " + evdt + " and " + enddt);
        //            }
        //        }

        //    }

        //}

        private bool ProcOverlapsInDB_PEID(int pnum, DateTime startdt, DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            //            LoadPatientProceduresIfNeeded();
            bool overlap_exists = false;
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && ((proc.PROCEDURE_DATETIME >= startdt) && (proc.PROCEDURE_DATETIME < enddt)
                                ||
                                (proc.DEPARTURE_DATETIME > startdt) && (proc.DEPARTURE_DATETIME <= enddt)
                                ||
                                (proc.PROCEDURE_DATETIME < startdt) && (proc.DEPARTURE_DATETIME > enddt)
                                )
                            //&& ( ! (proc.PROCEDURE_DATETIME == startdt) && (proc.DEPARTURE_DATETIME == enddt))
                            && (proc.CLASSIFIED_BY_ID < 0)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new { proc.PROCEDURE_EVENT_ID, proc.PROCEDURE_DATETIME, proc.DEPARTURE_DATETIME };
            overlap_exists = (query.Count() > 0);
            //foreach (var a in query)
            //{
            //    Program.VerboseAudit("Will Delete act: procdt=" + a.PROCEDURE_DATETIME.ToString() + "  depdt=" + a.DEPARTURE_DATETIME.ToString());
            //    Program.VerboseAudit("because it overlays startdt=" + startdt.ToString() + "  enddt=" + enddt.ToString());
            //    DeleteActivity(a.PROCEDURE_EVENT_ID);
            //}
            //            peid = 0;
            return (overlap_exists);
        }
        private void DeleteActivity(int peid)
        {
            //            delete PROCEDURE_ANSWER where PROCEDURE_EVENT_ID=7211351
            //delete RPT_PROC_BY_DAY where PROCEDURE_EVENT_ID=7211351
            //delete PROCEDURE_EVENT where PROCEDURE_EVENT_ID=7211351
            if (peid == 0) return;

            Program.VerboseAudit("db ProcAnsw Deleting peid=" + peid);
            var db = PFSDBUtility.NewPfsDataContext();
            var query = from ia in db.PROCEDURE_ANSWERs
                        where (ia.PROCEDURE_EVENT_ID == peid)
                        select ia;
            if (query.Count() > 0)
            {
                var items = query.ToList();
                foreach (var item in items)
                    db.PROCEDURE_ANSWERs.DeleteOnSubmit(item);
                db.SubmitChanges();
            }

            Program.VerboseAudit("db RptProc Deleting peid=" + peid);
            var db2 = PFSDBUtility.NewPfsDataContext();
            var query2 = from r in db2.RPT_PROC_BY_DAYs
                         where (r.PROCEDURE_EVENT_ID == peid)
                         select r;
            if (query2.Count() > 0)
            {
                var items2 = query2.ToList();
                foreach (var item2 in items2)
                    db2.RPT_PROC_BY_DAYs.DeleteOnSubmit(item2);
                db2.SubmitChanges();
            }

            Program.VerboseAudit("db ProcEvent Deleting peid=" + peid);
            var db3 = PFSDBUtility.NewPfsDataContext();
            var query3 = from ce in db3.PROCEDURE_EVENTs
                         where (ce.PROCEDURE_EVENT_ID == peid)
                         select ce;
            if (query3.Count() > 0)
            {
                var items3 = query3.ToList();
                foreach (var item3 in items3)
                    db3.PROCEDURE_EVENTs.DeleteOnSubmit(item3);
                db3.SubmitChanges();
            }
        }



        private bool QueuedProcOverlaps(int pnum, DateTime startdt, DateTime enddt)
        {
            bool overlap = false;

            Program.VerboseAudit("Activity " + pnum + ": Check for dup at " + startdt.ToString() + " - " + enddt.ToString());
            proc_data[] pary = _procs.ToArray();
            for (int i = 0; i <= pary.GetUpperBound(0); i++)
            {
                if (pary[i].procedure_number == pnum)
                {
                    //overlap if   p.start between stardt and enddt
                    //             p.finish between startdt and enddt
                    //             startdt >= p.start and enddt <= p.finish
                    if ((pary[i].start >= startdt) && (pary[i].start < enddt) ||
                        (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
                        (startdt >= pary[i].start) && (enddt <= pary[i].finish))
                    {
                        overlap = true;
                    }

                    if ((pary[i].start >= startdt) && (pary[i].start < enddt) && (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
                        (pary[i].start < startdt) && (pary[i].finish > startdt) && (pary[i].finish < enddt)
                        )
                    {
                        // the proc in the list is completely contained in startdt/endt pair
                        // change this proc to be the startdt/enddt
                        // overlap is still true, but the list item will be updated with the encompassing times
                        //Program.Audit("Procedure " + pnum + ": " + startdt + " - " + enddt + " will trump " + pary[i].start + " - " + pary[i].finish);
                        pary[i].start = startdt;
                        pary[i].finish = enddt;
                    }
                }
            }
            _procs = pary.ToList();
            Program.VerboseAudit("Activity " + pnum + ": Check for dup returns " + overlap);
            return overlap;
        }

        private bool ProcExistsInDB(int pnum, DateTime startdt, out DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            Program.VerboseAudit("ProcExistsInDB: pnum=" + pnum + " startdt=" + startdt.ToString());
            int ct = 0;
            enddt = DateTime.MinValue;
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            //&& (proc.PROCEDURE_DATETIME <= startdt)
                            && (proc.DEPARTURE_DATETIME > startdt)
                            && (((pnum > 2) && (ans.PROCEDURE_NUMBER == pnum)) || ((pnum <= 2) && (ans.PROCEDURE_NUMBER <= 2)))
                        orderby proc.DEPARTURE_DATETIME descending
                        select new { proc.DEPARTURE_DATETIME };
            ct = query.Count();
            //Program.VerboseAudit("ProcExistsInDB: ct=" + ct);
            //if (ct > 0)
            //{
            //    foreach (var x in query)
            //    {
            //        Program.VerboseAudit("ProcExistsInDB: x=" + x.DEPARTURE_DATETIME);
            //    }
            //}
            if (ct > 0)
                enddt = (DateTime)query.First().DEPARTURE_DATETIME;
            //else //see if there is a saved proc that ended before this startdt
            //{
            //    query = from proc in _procedure_events
            //            from ans in proc.PROCEDURE_ANSWERs
            //            where (proc.ENCOUNTER_ID == _pat.encounter_id)
            //                //&& (proc.PROCEDURE_DATETIME <= startdt)
            //                && (proc.DEPARTURE_DATETIME > startdt.AddHours(-4))
            //                && (ans.PROCEDURE_NUMBER == pnum)
            //            orderby proc.DEPARTURE_DATETIME descending
            //            select new { proc.DEPARTURE_DATETIME };
            //    ct = query.Count();
            //    if (ct > 0)
            //        enddt = (DateTime)query.First().DEPARTURE_DATETIME;
            //}
            Program.VerboseAudit("ProcExistsInDB: pnum=" + pnum + " returns " + ct);
            return ct > 0;
        }
        private void ProcessProc(int pnum, int groupnum, string desc)
        {

        }
        private void NEWProcessProc(int pnum, string desc, string res, string termphrase)
        {
            string DSC1 = "NURS Med/Surg Activities";
            string DSC2 = "NURS Med/Surg Activites Freq";
            string DSC3 = "NURS Med/Surg Activities Start";

            string DSC4 = "NURSI Hour Activities";
            string DSC5 = "NURSI Hour Activities Freq";
            string DSC6 = "NURSI Hour Activities Start";

            string desc1="x", desc2 = "x", desc3 = "x";


            var arr = SplitOnCommaAndPrepareElements(res);
            //return query.Where(e => arr.Any(x => x.Contains(e.DESCRIPTION))); // "like" match

            DateTime evdt = DateTime.MinValue;
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = query.Where(e => e.DESCRIPTION.ToLower().StartsWith(desc.ToLower()));
            query = query.Where(e => e.RESULT.ToLower().ContainsAny(arr));
            query = query.Where(e => e.ORDER_CONTROL != "X");
            query = query.OrderByDescending(e => e.EVENT_DATETIME);
            Program.VerboseAudit("qct: " + query.Count());
            if (query.Count() == 0) return;

            DateTime start1 = query.First().EVENT_DATETIME; //this is the latest time


            bool found_dur = false;
            bool found_st = false;
            bool done = false;
            string dur = "";
            int stimecolpos;

            string stime; //14:00
            int durint;
            DateTime tempdate;
            string stimehr;
            string stimemin;
            DateTime date_of_event;
            DateTime dt_of_event;
            DateTime sdt;
            DateTime event_dt_plus1hr;

            foreach (var q in query)
            {
                if (evdt < q.EVENT_DATETIME && !done)
                {
                    evdt = q.EVENT_DATETIME;
                    var query2 = StartNewQuery(SearchDepth.SearchDefault);
                    query2 = query2.Where(e => e.DESCRIPTION.ToLower().StartsWith(desc2.ToLower()));
                    query2 = query2.Where(e => e.EVENT_DATETIME == evdt);
                    Program.VerboseAudit("q2ct: " + query2.Count());
                    if (query2.Count() >= 0)
                    {
                        found_dur = true;
                        done = true;
                        dur = query2.First().RESULT;
                        stimecolpos = dur.IndexOf(" ");
                        dur = dur.Substring(0, stimecolpos);
                        Program.VerboseAudit("q2 duration: " + dur);
                    }

                }
            }

            if (!found_dur)
            {
                Program.VerboseAudit("For activity: " + desc2 + " there was no frequency found.");
                return;
            }

            done = false;
            var query3 = StartNewQuery(SearchDepth.SearchDefault);
            query3 = query3.Where(e => e.DESCRIPTION.ToLower().StartsWith(desc3.ToLower()));
            query3 = query3.Where(e => e.EVENT_DATETIME == evdt);
            Program.VerboseAudit("q3ct: " + query3.Count());
            if (query3.Count() == 0)
            {
                Program.VerboseAudit("For activity: " + desc2 + " there was no start time found.");
                return;
            }

            found_st = true;
            done = true;
            stime = query3.First().RESULT;
            Program.VerboseAudit("q3 stime: " + stime);

//'determine the date for stime that is before rs(0)  say rs(0) = 2/1 01:00  stime=23:00
//' associate stime with rs(0) date.  compare this dt with rs(0)  
// if positive then ok because 2/1 23:00 is After 01:00
//' else associate stime with rs(0)date-1.  1/31 23:00


            stimecolpos = stime.IndexOf(":");
            stimehr = stime.Substring(0, stimecolpos);
            Program.VerboseAudit("q3 stimeHR: " + stimehr);
            if (stimehr.Length == 1) stimehr = "0" + stimehr;
            stimemin = stime.Substring(stimecolpos + 1);
            Program.VerboseAudit("q3 stimeMN: " + stimemin);

            event_dt_plus1hr = evdt.AddHours(1);
            Program.VerboseAudit("event_dt_plus1hr: " + event_dt_plus1hr);
            date_of_event = event_dt_plus1hr.Date;//Format$(event_dt_plus1hr, "yyyymmdd")  ' example 20150122 1400
            Program.VerboseAudit("date_of_event: " + date_of_event);
            dt_of_event = event_dt_plus1hr;// ' add an hour: 201501221500


            sdt = date_of_event.AddHours(stimehr.Val()).AddMinutes(stimemin.Val());
            Program.VerboseAudit("sdt: " + sdt);
            if (dt_of_event < sdt)  //'decrease date_of_event by 1 day
            {
                tempdate = event_dt_plus1hr;
                tempdate = tempdate.AddDays(-1);
                date_of_event = tempdate.Date;  //Format$(tempdate, "yyyymmdd")
                sdt = date_of_event.AddHours(stimehr.Val()).AddMinutes(stimemin.Val());
            }

            tempdate = sdt;

            if (found_dur && found_st)
            {
                //  durint = CInt(dur)
                //numprocs = numprocs + 1;
                //procs(numprocs).pnum = pnum
                //procs(numprocs).start = tempdate
                //procs(numprocs).finish = DateAdd("h", durint, tempdate)
                AddSimpleProc(pnum, sdt, sdt.AddMinutes(dur.Val()));
                Program.VerboseAudit("AddSimpleProc: " + pnum + "  sdt="+sdt + " dur="+dur);
            }


        }

        private void CheckProc_1()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A1. 1-1 safety observation by RN");
            Program.VerboseAudit("---------------");

            string desc = "SNCH_restraint type Level II";
            string res = "4-Point and Constant Observation";
            string terminate_phrase = "restraint removed";
            NEWProcessProc(1, desc, res, terminate_phrase);

            desc = "SNCH_Restraint Action Codes";
            res = "initial application,continued restraint,release and reapply";
            NEWProcessProc(1, desc,res,terminate_phrase);

        }

        private void CheckProc_2()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A2. 1-1 safety observation by non-RN");
            Program.VerboseAudit("---------------");

            string desc_found = "";
            DateTime orderCAtime, tm;
            string s = "351023";
            if (OrderInProgressByCode(s, out desc_found, out orderCAtime))
            {
                if (orderCAtime != DateTime.MinValue)
                    tm = orderCAtime;
                else
                    tm = loc_out;
                Program.VerboseAudit("Attempt to add sitter: start=" + loc_in + " end="+tm);
                AddSimpleProc(2, loc_in, tm);
            }

        }


        private void AddSitter(string code, DateTime initdt, DateTime evdt)
        {
            string desc = "TC: Direct Observer";
            string res = "Initiated";
            DateTime timestmp = DateTime.Now;
            short seq = 0;
            int unitid = _pat.unit_id;

            using (var db = PFSDBUtility.NewSqlConnection())
            {
                string q = "INSERT INTO chart_item (encounter_id,event_datetime,code,description,result,timestamp,sequence,unit_id,procedure_start)";
                q += " select @encid, @evdt, @code, @desc, @res, @ts,@seq,@unit,@procstart where not exists";
                q += " (select encounter_id,code,event_datetime from chart_item where encounter_id=" + _pat.encounter_id.ToString() + " and code='" + code + "' and event_datetime=" + PFSDBUtility.SQLDateTime(evdt) + ")";
                //string q = "INSERT INTO chart_item (encounter_id,event_datetime,code,description,timestamp,sequence,unit_id,order_id) values (@encid, @evdt, @code, @desc, @ts,@seq,@unit,@oid)";
                SqlCommand cmd = new SqlCommand(q, db);
                cmd.Parameters.AddWithValue("@encid", _pat.encounter_id);
                cmd.Parameters.AddWithValue("@evdt", evdt);
                cmd.Parameters.AddWithValue("@code", code);
                cmd.Parameters.AddWithValue("@desc", desc);
                cmd.Parameters.AddWithValue("@res", res);
                cmd.Parameters.AddWithValue("@ts", timestmp);
                cmd.Parameters.AddWithValue("@seq", seq);
                cmd.Parameters.AddWithValue("@unit", unitid);
                cmd.Parameters.AddWithValue("@procstart", initdt);
                cmd.ExecuteNonQuery();
                db.Close();
            } //using db

        }

        private int GetSitterType(DateTime evdt)
        { // get the sitter type RN or non-RN from the charting that should exist
            //at the same time as the initiation of the observer 1540100298
            int pnum = 0;
            string return_result = "";
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);    // add custom time range below
            query = query.Where(e => e.CODE == "1540100298");
            query = query.Where(e => e.EVENT_DATETIME == evdt);
            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
                if (return_result.Trim().ToUpper().StartsWith("RN")) pnum = 1;
                if (return_result.Trim().ToUpper().StartsWith("NON-RN")) pnum = 2;
            }
            if (pnum == 0)
            {
                Program.VerboseAudit("Sitter type RN or non-RN not found. Defaulting to non_RN.");
                pnum = 2;
            }
            return pnum;
        }

        private void CheckActivity(int actnum,string actcode, string actst, string actlencode)
        {
            int actlen;
            string actsthhmm = "0000";
        
            var query = StartNewQuery(SearchDepth.SearchSince24Hrs);    // add custom time range below
            query = query.Where(e => e.CODE == actcode);
            query = query.Where(e => e.ORDER_STATUS == null || e.ORDER_STATUS == ""); // need to update order_status=x
            query = query.OrderBy(e => e.EVENT_DATETIME);
            Program.VerboseAudit("for code=" + actcode + " count=" + query.Count());
            foreach (var item in query)
            {
                if (item.RESULT.ToLower() == "yes")
                { //09500000
                    var q2 = StartNewQuery(SearchDepth.SearchSince24Hrs);    // add custom time range below
                    q2 = q2.Where(e => e.CODE == actst);
                    q2 = q2.Where(e => e.EVENT_DATETIME == item.EVENT_DATETIME);
                    Program.VerboseAudit("for code=" + actst + " count=" + q2.Count());
                    if (q2.Count() == 0) return;

                    actsthhmm = q2.First().RESULT;
                    Program.VerboseAudit("actsthhmm=" + actsthhmm);
                    if (actsthhmm.Length >= 4)
                        actsthhmm = actsthhmm.Substring(0, 4);
                    Program.VerboseAudit("actsthhmm=" + actsthhmm);

                    var q3 = StartNewQuery(SearchDepth.SearchSince24Hrs);    // add custom time range below
                    q3 = q3.Where(e => e.CODE == actlencode);
                    q3 = q3.Where(e => e.EVENT_DATETIME == item.EVENT_DATETIME);
                    Program.VerboseAudit("for code=" + actlencode + " count=" + q3.Count());
                    if (q3.Count() == 0) return;

                    actlen = (int)q3.First().RESULT.Val();
                    DateTime stdt = DateTime.MinValue;
                    if (actlen >= 60)
                        {
                        TimeSpan start = new TimeSpan(0, 0, 0); //0 o'clock
                        TimeSpan end = new TimeSpan(6, 0, 0); // 6 o'clock
                        TimeSpan item_ts = item.EVENT_DATETIME.TimeOfDay;
                        TimeSpan actst_ts = new TimeSpan((int)actsthhmm.Substring(0, 2).Val(), (int)actsthhmm.Substring(2, 2).Val(), 0);
                        stdt = item.EVENT_DATETIME.Date + actst_ts;
                        if ((item_ts >= start) && (item_ts <= end))
                        {
                            if (actst_ts > item_ts) // then it belongs to prev cal day.
                                stdt = item.EVENT_DATETIME.Date.AddDays(-1) + actst_ts;
                        }
//                      DateTime stdt = item.EVENT_DATETIME.Date.AddHours(actsthhmm.Substring(0, 2).Val()).AddMinutes(actsthhmm.Substring(2, 2).Val());
                        Program.VerboseAudit("stdt=" + stdt);
                        AddSimpleProc(actnum, stdt, stdt.AddMinutes(actlen));
                        NullifyActivity(actcode, item.EVENT_DATETIME);
                        }
                }
            }

        }

        private void NullifyActivity(string actcode, DateTime evdt)
        {
            var db = PFSDBUtility.NewSqlConnection();
            string q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and event_datetime=" + PFSDBUtility.SQLDateTime(evdt) + " and code='" + actcode + "'";
            SqlCommand cmd = new SqlCommand(q, db);
            cmd.ExecuteNonQuery();
            db.Close();
        }

        private DateTime NextFinish(DateTime startdt)
        {
            DateTime dt = DateTime.MinValue;

            int b = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, startdt) / (4 * 60));
            dt = _pat.pull_start.AddMinutes((b + 1) * 240);
            Program.VerboseAudit("NextFinish: startdt=" + startdt.ToString() + " b=" + b + " dt=" + dt.ToString());
            return dt;
        }


        private bool ProcExists(int pnum, DateTime startdt, DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && (proc.PROCEDURE_DATETIME == startdt)
                            && (proc.DEPARTURE_DATETIME == enddt)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new { proc.PROCEDURE_EVENT_ID };
            return (query.Count() > 0);
        }


        private void CheckProc_3()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A3. Off unit accompanied by RN");
            Program.VerboseAudit("---------------");

            DateTime evdt;
            //if (GetEVDT("", "12067428", "", "", "Left unit", 1, loc_in, out evdt, SearchDepth.SearchDefault))
            if (GetEVDTIfAllResults("12067428", "Left Unit,Nurse", 1, loc_in, out evdt, SearchDepth.SearchDefault))
                if (evdt > DateTime.MinValue)
                    AddSimpleProc(3, evdt, evdt.AddHours(1));
            
            //string res = "Off unit accompanied by RN";
            //ProcessProc(3, 1, res);
            //ProcessProc(3, 2, res);

        }
        private void CheckProc_4()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A4. Off unit accompanied by Non-RN");
            Program.VerboseAudit("---------------");

            DateTime evdt;
            if (GetEVDTIfAllResults("12067428", "Left Unit,HCT", 1, loc_in, out evdt, SearchDepth.SearchDefault))
                if (evdt > DateTime.MinValue)
                    AddSimpleProc(4, evdt, evdt.AddHours(1));

        }

        private void CheckProc_5()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A5. Patient/family education by RN");
            Program.VerboseAudit("---------------");

            string res = "Patient/family education by RN";
            ProcessProc(5, 1, res);
            ProcessProc(5, 2, res);

        }

        private void CheckProc_6()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A6. Extensive wound management by RN");
            Program.VerboseAudit("---------------");


            DateTime evdt;
            if (GetEVDT("", "12053619", "", "", "", 1, loc_in, out evdt, SearchDepth.SearchDefault))
                AddSimpleProc(6, evdt, evdt.AddHours(1));

            //string res = "Extensive wound management by RN";
            //ProcessProc(6, 1, res);
            //ProcessProc(6, 2, res);

        }

        private void CheckProc_7()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A7. Extensive wound management by non-RN");
            Program.VerboseAudit("---------------");

            DateTime evdt;
            if (GetEVDT("", "12053623", "", "", "", 1, loc_in, out evdt, SearchDepth.SearchDefault))
                AddSimpleProc(7, evdt, evdt.AddHours(1));
            

            //string res = "Extensive wound management by non-RN";
            //ProcessProc(7, 1, res);
            //ProcessProc(7, 2, res);

        }

        private void CheckProc_8()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A8. Coordination of care by RN");
            Program.VerboseAudit("---------------");

            string res = "Discharge coordination";
            ProcessProc(8, 1, res);
            ProcessProc(8, 2, res);

            res = "Outside facility transfer coordination";
            ProcessProc(8, 1, res);
            ProcessProc(8, 2, res);

            res = "Interdisciplinary care conference/coordination";
            ProcessProc(8, 1, res);
            ProcessProc(8, 2, res);

        }

        private void CheckProc_9()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A9. 1:1 RN at bedside");
            Program.VerboseAudit("---------------");

            DateTime evdt;
            if (GetEVDT("", "12046750", "", "", "4", 1, loc_in, out evdt, SearchDepth.SearchDefault))
                AddSimpleProc(9, evdt, evdt.AddHours(1));
            if (GetEVDT("", "12045759", "", "", "", 1, loc_in, out evdt, SearchDepth.SearchDefault))
                AddSimpleProc(9, evdt, evdt.AddHours(1));
            if (GetEVDT("", "12045760", "", "", "", 1, loc_in, out evdt, SearchDepth.SearchDefault))
                AddSimpleProc(9, evdt, evdt.AddHours(1));
            if (GetEVDT("", "12060714", "", "", "", 1, loc_in, out evdt, SearchDepth.SearchDefault))
                AddSimpleProc(9, evdt, evdt.AddHours(1));
            if (GetEVDT("", "12045761", "", "", "", 1, loc_in, out evdt, SearchDepth.SearchDefault))
                AddSimpleProc(9, evdt, evdt.AddHours(1));
            if (GetEVDT("", "12061068", "", "", "", 1, loc_in, out evdt, SearchDepth.SearchDefault))
                AddSimpleProc(9, evdt, evdt.AddHours(1));

        }

        private void CheckProc_10()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A10. 1:1 non-RN at bedside");
            Program.VerboseAudit("---------------");

            string res = "1:1 by non-RN at the bedside";
            ProcessProc(10, 1, res);
            ProcessProc(10, 2, res);

        }

        private void CheckProc_11()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A11. 2:1 by RN at bedside");
            Program.VerboseAudit("---------------");

            string res = "2:1 by RN at the bedside";
            ProcessProc(11, 1, res);
            ProcessProc(11, 2, res);

        }

        private void OutputClassAndDNC(bool do_class)
        {
            int i;
            if (numclassdnc == 0 && do_class)
                OutputClass(DateTime.MinValue, DateTime.MinValue);
            //else
            //{
            //    for (i = 1; i <= numclassdnc; i++)
            //    {
            //        if (aryclassdnc[i].retain)
            //            OutputDNC(aryclassdnc[i].startdt, aryclassdnc[i].enddt);
            //        else // do_class will never be false here
            //            OutputClass(aryclassdnc[i].startdt, aryclassdnc[i].enddt);
            //    }
            //}
        }

        //        private const string DATETIME_FORMAT = "yyyyMMddHHmm";              // ISO Date/Time w/o seconds
        private const string DATETIME_FORMAT = "yyyyMMddHHmm";              // ISO Date/Time w/o seconds

        private void OutputClass(DateTime dncstdt,DateTime dncendt)
        {
            string outstr, ind_list, desc, str_pull_dt, str_in_dt, str_out_dt = "";
            int i, tc_event_id;
            bool repeat_output = false;
            string repeat_time = "";
            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         212 242 304
            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1
            //1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901 3456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
            //1       |10 5            |                |                |        |10023121779         |ALEXANDER                       |GLORIA                          |                                |0508    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289530  |           |201403030400                                                                      |NNYNNNNYYNNYNNNYNNNYYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
            //1       |10 5            |                |                |        |10024544908         |ARMSTRONG                       |AARON                           |                                |0535    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289531  |           |201403030400                                                                      |NYNNYNNNNNNYNNNYNNNNYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
            //|0547    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289532  |           |201403030400                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN
            //|0547    |A   |201403030700|                |201403030700    |18  |C|    |3040|1440|18289532  |           |201403030700                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//            1RO | RO MB5BG / 6E |                |                |        | 2000180316769 | PARISIEN | GREYSON | LAWRENCE | RMB5514 | P | 20190319030000 |                               | 20 | C |    | 5399 | 480 | 62040560 |           | 20190319030000 | 20190319070000 | NNNYNNNNNNNNYNNNYNYNYNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN

            if (Program.g_is_test)
                tc_event_id = 9999;
            else
                tc_event_id = PFSDBUtility.NextGID();                         //get a unique id for this class
                                                                              //            str_pull_dt = _pat.pull_finish.ToString(DATETIME_FORMAT);
                                                                              //str_pull_dt = _pat.pull_finish.ToString(DATETIME_FORMAT);
                                                                              //if (_pat.unit_departure != null && _pat.unit_departure != DateTime.MinValue)
                                                                              //if (dncstdt == DateTime.MinValue)
                                                                              //{
                                                                              //    //                if (_pat.effective_out == Program.g_pull_finish && _pat.unit_departure == DateTime.MinValue)
                                                                              //    Program.VerboseAudit("Loc arrtime=" + loc_arrtime + " gpull_start_save=" + Program.g_pull_start_save);
                                                                              //    if (loc_arrtime >= Program.g_pull_start_save)
                                                                              //    {
                                                                              //        //str_in_dt = Program.g_effdt.ToString(DATETIME_FORMAT);
                                                                              //        str_in_dt = loc_arrtime.ToString(DATETIME_FORMAT);
                                                                              //        str_out_dt = loc_out.ToString(DATETIME_FORMAT);

            //        if (loc_arrtime <= Program.g_effdt)
            //        {
            //            repeat_output = true;//also make the desired class time
            //            repeat_time = Program.g_effdt.ToString(DATETIME_FORMAT);
            //        }
            //    }
            //    else
            //    {
            //        str_in_dt = Program.g_effdt.ToString(DATETIME_FORMAT);
            //        str_out_dt = "";// _pat.effective_out.ToString(DATETIME_FORMAT);
            //        str_out_dt = loc_out.ToString(DATETIME_FORMAT);
            //    }
            //}
            //else //dnc
            //{
            //    str_in_dt = dncstdt.ToString(DATETIME_FORMAT);
            //    str_out_dt = dncendt.ToString(DATETIME_FORMAT);
            //}
            //if (!repeat_output)
            //    str_out_dt = "";//leave open-ended for night ASSIGNMENT MODULE 4/14/21

            if (loc_arrtime >= Program.g_effdt.AddHours(-4))
            {
                //str_in_dt = Program.g_effdt.ToString(DATETIME_FORMAT);
                str_in_dt = loc_arrtime.ToString(DATETIME_FORMAT);
                str_out_dt = loc_out.ToString(DATETIME_FORMAT);

                repeat_output = true;//also make the desired class time
                repeat_time = Program.g_effdt.ToString(DATETIME_FORMAT);
            }
            else
            {
                str_in_dt = Program.g_effdt.ToString(DATETIME_FORMAT);
                str_out_dt = "";// _pat.effective_out.ToString(DATETIME_FORMAT);
                str_out_dt = loc_out.ToString(DATETIME_FORMAT);
            }
            if (!repeat_output)
                str_out_dt = "";//leave open-ended 5/3/21


            //outstr = _pat.facilty_code.FixedWidth(8);       //(facility code)
            outstr = _pat.short_name.FixedWidth(8);                       //(facility code)
            outstr += "|" + _pat.unit_name.FixedWidth(16);
            outstr += "|" + "".FixedWidth(16);                               //(unit code)
            outstr += "|" + txarea.FixedWidth(16);                               //(area code)
            outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
            outstr += "|" + _pat.acct.FixedWidth(20);
            outstr += "|" + _pat.last_name.FixedWidth(32);
            outstr += "|" + _pat.first_name.FixedWidth(32);
            outstr += "|" + _pat.middle_name.FixedWidth(32);
            outstr += "|" + _pat.room.FixedWidth(8);
            outstr += "|" + _pat.bed.FixedWidth(4);
            outstr += "|" + str_in_dt;        //CLASS dt
            outstr += "|" + "".FixedWidth(14);                               //(login)
            outstr = outstr.FixedWidth(232);
            outstr += "|" + _pat.effective_out.ToString(DATETIME_FORMAT);//str_out_dt;        //TC Data End Point
            outstr = outstr.FixedWidth(249);
            outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
            outstr += "|" + "C".FixedWidth(1);                               //record type = class
            outstr += "|" + "".FixedWidth(4);                                //(stage)
            outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
            outstr += "|" + _pat.range.ToString().FixedWidth(4);             //TC pull range
            outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
            outstr += "|";
            outstr = outstr.FixedWidth(294);
            outstr += "|" + str_in_dt;        //IN
            outstr = outstr.FixedWidth(346);
            //if (_pat.unit_departure != null && _pat.unit_departure != DateTime.MinValue)
                //                outstr += "|" + _pat.unit_departure.ToString(DATETIME_FORMAT);        //OUT
                outstr += "|" + str_out_dt;        //OUT
            outstr = outstr.FixedWidth(377);
            outstr += "|";

            //if (use_default)
            //{ //make all is_checked = false and then mark defaults
            //    Program.VerboseAudit("Patient Will receive default indicators " + _pat.default_inds_str);
            //    for (i = 1; (i <= MAX_INDS); i++)
            //    {
            //        _inds[i].is_checked = false;
            //    }
            //    foreach (var ind in _pat.default_inds)
            //    {
            //        if (ind <= _inds.GetUpperBound(0))
            //        {
            //            _inds[ind].is_checked = true;
            //        }
            //    }
            //}

            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++) {
                if (_inds[i].is_checked) {
                    outstr += "Y";
                    ind_list += "" + i;
                } else {
                    outstr += "N";
                }
            } // next i
            ind_list = ind_list.Substring(1);                           //strip leading comma
                                                                        //                                                                                                   1                                                                                                   2                                                                                                   3
                                                                        //         1         2         3         4         5         6         7         8         9         0         1         2         3         4         5         6         7         8         9         0         1         2         3         4         5         6         7         8         9         0         1         2         3         4         5         6         7         8         9         0
                                                                        //1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
                                                                        //1       |DO6D            |                |                |        |2000192224892       |BEHNAM                          |KENDRA                          |LEE                             |RDO6311 |P   |20180717110000|                               |20  |C|    |5399|480 |56103278  |           |20180717110000                                     |                              |YNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
            string str5am;
            Program.outfile.WriteLine(outstr);      //1900yesterd output to transparent.txt
            if (repeat_output) // with repeat_time
            {
                string repeat_line = outstr.Substring(0, 203) + repeat_time + outstr.Substring(215, 80) + repeat_time + " ".Repeat(71) + outstr.Substring(378, 120);
                Program.outfile.WriteLine(repeat_line);
            }
            //if (Program.g_make5am)
            //{
            //    //if (str_out_dt.Substring(8, 4) == "0500") //create the 7am at 3am
            //    {
            //        string strdttm = Program.g_pull_finish.ToString(DATETIME_FORMAT);
            //        strdttm = strdttm.Substring(0, 8) + "0500";
            //        //str5am = outstr.Substring(0, 203) + Program.g_pull_finish.ToString(DATETIME_FORMAT) + outstr.Substring(217, 78) + Program.g_pull_finish.ToString(DATETIME_FORMAT) + " ".Repeat(69) + outstr.Substring(378, 120);
            //        str5am = outstr.Substring(0, 203) + strdttm + outstr.Substring(215, 80) + strdttm + " ".Repeat(71) + outstr.Substring(378, 120);
            //        Program.outfile2.WriteLine(str5am);
            //    }
            //}

            Program.Audit("");
            desc = "Classified: " + ind_list;
            if (Program.g_is_test) {
                Program.Audit(desc);
            } else {
                //Save the selected indicators plus both Program.Audits in the event log; link with tc_event_id
                PFSEventLog.AddTransparentMappingEventLogEntry(
                    desc, Program.gLogUnitID, Program.gLogEncounterID,
                    tc_event_id, Program.gLogMapperVersion,
                    Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
            }
        }

        //private void OutputClassAndDNCTimes()
        //{
        //    int i;
        //    DateTime dt1 = Program.g_effdt;
        //    numclassdnc = 0;
        //    DateTime lastend = Program.g_effdt;
        //    // retain has nothing to do with keeping the record. it only means "is DNC".

        //    for (i = 1; i <= numloa; i++)
        //    {
        //        //Why are these historicals stubbed off?? let it go through 10/09/20
        //        //if (aryloa[i].startdt < dt1) 
        //        //{
        //        //    Program.VerboseAudit("historical aryloa i:" + i + " aryloa[].startdt=" + aryloa[i].startdt + " aryloa[].enddt=" + aryloa[i].enddt + " dt1="+dt1);
        //        //}
        //        //else
        //        {
        //            if (numclassdnc == 0)
        //            {
        //                if (aryloa[i].startdt > dt1)
        //                {
        //                    numclassdnc++;
        //                    aryclassdnc[numclassdnc].startdt = dt1;
        //                    aryclassdnc[numclassdnc].enddt = aryloa[i].startdt;
        //                    aryclassdnc[numclassdnc].retain = false; //false=Class
        //                }
        //                numclassdnc++;
        //                aryclassdnc[numclassdnc].startdt = aryloa[i].startdt;
        //                aryclassdnc[numclassdnc].enddt = aryloa[i].enddt;
        //                aryclassdnc[numclassdnc].retain = true; //true=DNC
        //            }
        //            else
        //            {
        //                numclassdnc++;
        //                aryclassdnc[numclassdnc].startdt = aryloa[i - 1].enddt;
        //                aryclassdnc[numclassdnc].enddt = aryloa[i].startdt;
        //                aryclassdnc[numclassdnc].retain = false; //false=Class

        //                numclassdnc++;
        //                aryclassdnc[numclassdnc].startdt = aryloa[i].startdt;
        //                aryclassdnc[numclassdnc].enddt = aryloa[i].enddt;
        //                aryclassdnc[numclassdnc].retain = true; //true=DNC
        //            }
        //            lastend = aryloa[i].enddt;
        //        }
        //    } //for
        //    if (numclassdnc > 0 && lastend != dt1.AddHours(12)) 
        //        // then the loa ended before this draw period end
        //    {
        //        numclassdnc++;
        //        aryclassdnc[numclassdnc].startdt = lastend; // aryloa[i - 1].enddt;
        //        aryclassdnc[numclassdnc].enddt = dt1.AddHours(12);
        //        aryclassdnc[numclassdnc].retain = false; //false=Class
        //    }
        //    for (i=1;i<=numclassdnc;i++)
        //    {
        //        string classdnctype="";
        //        if (aryclassdnc[i].retain)
        //            classdnctype = "DNC";
        //        else
        //            classdnctype = "Class";
        //        Program.VerboseAudit(classdnctype + ": " + aryclassdnc[i].startdt + "-" + aryclassdnc[i].enddt);
        //    }
        //}

        //private void OutputDNC(DateTime dncstdt, DateTime dncendt)
        //{
        //    string outstr, ind_list, desc, str_pull_dt, str_in_dt, str_out_dt = "";
        //    int i, tc_event_id;

        //    if (Program.g_is_test)
        //        tc_event_id = 9999;
        //    else
        //        tc_event_id = PFSDBUtility.NextGID();                         //get a unique id for this class

        //    //str_in_dt = aryloa[i].startdt.ToString(DATETIME_FORMAT);
        //    //str_out_dt = aryloa[i].enddt.ToString(DATETIME_FORMAT);
        //    str_in_dt = dncstdt.ToString(DATETIME_FORMAT);
        //    str_out_dt = dncendt.ToString(DATETIME_FORMAT);

        //    //outstr = _pat.facilty_code.FixedWidth(8);                       //(facility code)
        //    outstr = "".FixedWidth(8);                       //(facility code)
        //    outstr += "|" + _pat.unit_name.FixedWidth(16);
        //    outstr += "|" + "".FixedWidth(16);                               //(unit code)
        //    outstr += "|" + "".FixedWidth(16);                               //(area code)
        //    outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
        //    outstr += "|" + _pat.acct.FixedWidth(20);
        //    outstr += "|" + _pat.last_name.FixedWidth(32);
        //    outstr += "|" + _pat.first_name.FixedWidth(32);
        //    outstr += "|" + _pat.middle_name.FixedWidth(32);
        //    outstr += "|" + _pat.room.FixedWidth(8);
        //    outstr += "|" + _pat.bed.FixedWidth(4);
        //    outstr += "|" + str_in_dt;        //CLASS dt
        //    outstr += "|" + "".FixedWidth(14);                               //(login)
        //    outstr = outstr.FixedWidth(232);
        //    outstr += "|" + str_out_dt;  // TC END POINT_pat.effective_out.ToString(DATETIME_FORMAT);//TC Data End Point
        //    outstr = outstr.FixedWidth(249);
        //    outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
        //    outstr += "|" + "D".FixedWidth(1);                               //record type = class
        //    outstr += "|" + "".FixedWidth(4);                                //(stage)
        //    outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
        //    outstr += "|" + "".FixedWidth(4);             //TC pull range
        //    outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
        //    outstr += "|";
        //    outstr = outstr.FixedWidth(294);
        //    outstr += "|" + str_in_dt;        //IN
        //    outstr = outstr.FixedWidth(346);
        //    outstr += "|" + str_out_dt;        //OUT
        //    outstr = outstr.FixedWidth(377);
        //    outstr += "|NNNNY";

        //    Program.outfile.WriteLine(outstr);      //1900yesterd output to transparent.txt
        //    Program.Audit("");
        //    desc = "Classified DoNotClassify: NNNNY";
        //        //Save the selected indicators plus both Program.Audits in the event log; link with tc_event_id
        //        PFSEventLog.AddTransparentMappingEventLogEntry(
        //            desc, Program.gLogUnitID, Program.gLogEncounterID,
        //            tc_event_id, Program.gLogMapperVersion,
        //            Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());

        //}


        private int DeterminePtypeOfIndicators()
        {
            int i;
            var pscore = 0.0;
            int pt_type = 6;
            var indlist = new List<int>();
            string indstr = "";

            Program.VerboseAudit("Determine patient type of triggered indicators");

            for (i = 1; (i <= MAX_INDS); i++)
            {
                if (_inds[i].is_checked)
                {
                    indlist.Add(i);
                    indstr += i.ToString() + ",";
                    pscore += _inds[i].weight;
                }
            }

            Program.VerboseAudit("indicators=" + indstr + " score=" + pscore.ToString());

            var db = PFSDBUtility.NewPfsDataContext();
            var query_ptype = from ptype in db.PATIENT_TYPEs
                              where (ptype.METHODOLOGY_ID == _pat.meth_id)
                              orderby ptype.PATIENT_TYPE1
                              select new
                              {
                                  ptype.PATIENT_TYPE1,
                                  ptype.POINTS_HIVAL
                              };

            foreach (var ptypes in query_ptype)
            {
                //                Program.DebugTrace("type,hival=" + ptypes.PATIENT_TYPE1.ToString() + "" + ptypes.POINTS_HIVAL.ToString(), "");
                if (pscore <= ptypes.POINTS_HIVAL)
                {
                    if (pt_type > ptypes.PATIENT_TYPE1)
                    {
                        pt_type = ptypes.PATIENT_TYPE1;
                    }
                }
            }
            Program.VerboseAudit("patient type=" + pt_type.ToString());

            return pt_type;

        }


        //private bool ExistDefaultInPast16hrs()
        //{
        //    DateTime cdt1 = DateTime.MinValue;
        //    DateTime cdt2 = DateTime.MinValue;
        //    int cnt_all = 0;
        //    int cnt_def = 0;
        //    //get max class date of last non-default class = 1
        //    //get max class date of last default = 2
        //    // if 1 >= 2 then false
        //    // else
        //    //   if 2 > 1 and date is <= 16 hrs ago then true
        //    //   else false
        //    var db = PFSDBUtility.NewPfsDataContext();
        //    //var query = from ce in db.CLASSIFICATION_EVENTs
        //    //            where (ce.ENCOUNTER_ID == _pat.encounter_id)
        //    //            && (ce.CLASSIFIED_BY_ID != -2)
        //    //            && (System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(ce.CLASSIFICATION_DATETIME, Program.g_pull_start) <= 960)
        //    //            select ce;
        //    //cnt_all = query.Count();
        //    //if (cnt_all > 0)
        //    //{
        //    //    cdt1 = PFSUtility.DBToDateTime(query.Max(x => x.CLASSIFICATION_DATETIME));
        //    //    Program.VerboseAudit("Last regular classification was at:"+cdt1.ToString());
        //    //}
        //    //else {
        //    //    Program.VerboseAudit("No regular classifications within the past 16 hours");
        //    //}

        //    var query = from ce in db.CLASSIFICATION_EVENTs
        //                where (ce.ENCOUNTER_ID == _pat.encounter_id)
        //                && (ce.CLASSIFIED_BY_ID == -2)
        //                && (System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(ce.CLASSIFICATION_DATETIME, Program.g_pull_finish) <= 960)
        //                select ce;
        //    cnt_def = query.Count();

        //    if (cnt_def > 0)
        //    {
        //        cdt2 = PFSDBUtility.DBToDateTime(query.Max(x => x.CLASSIFICATION_DATETIME));
        //        Program.VerboseAudit("Last default classification was at:" + cdt2.ToString());
        //    }
        //    else
        //    {
        //        Program.VerboseAudit("No default classifications within the past 16 hours");
        //    }
        //    return (cnt_def > 0);

        //}

        private void OutputProcs()
        {
            int i;
            string outstr, proc_list, desc;
            int tc_event_id;

            foreach (var proc in _procs)
            {
                if (Program.g_is_test)
                    tc_event_id = 9999;
                else
                    tc_event_id = PFSDBUtility.NextGID();                         //get a unique id for this proc

                outstr = _pat.short_name.FixedWidth(8);                       //(facility code)
                outstr += "|" + _pat.unit_name.FixedWidth(16);
                outstr += "|" + "".FixedWidth(16);                               //(unit code)
                outstr += "|" + txarea.FixedWidth(16);                               //(area code)
                outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
                outstr += "|" + _pat.acct.FixedWidth(20);
                outstr += "|" + _pat.last_name.FixedWidth(32);
                outstr += "|" + _pat.first_name.FixedWidth(32);
                outstr += "|" + _pat.middle_name.FixedWidth(32);
                outstr += "|" + _pat.room.FixedWidth(8);
                outstr += "|" + _pat.bed.FixedWidth(4);
                outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //204 proc dt
                outstr += "|" + "".FixedWidth(14);                               //(login)
                outstr = outstr.FixedWidth(249);
                outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
                outstr += "|" + "P".FixedWidth(1);                               //record type = class
                outstr += "|" + "".FixedWidth(4);                                //(stage)
                outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
                outstr += "|" + _pat.range.ToString().FixedWidth(4);             //TC pull range
                outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
                outstr += "|";
                outstr = outstr.FixedWidth(294);
                outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //296 procdt in
                outstr = outstr.FixedWidth(346);
                //if (_pat.unit_departure != null && _pat.unit_departure != DateTime.MinValue)
                //                outstr += "|" + _pat.unit_departure.ToString(DATETIME_FORMAT);        //OUT
                outstr += "|" + proc.finish.ToString(DATETIME_FORMAT);          //348 procdt out
                outstr = outstr.FixedWidth(377);
                outstr += "|";

                proc_list = "";
                for (i = 1; (i < MAX_PROCS); i++)
                {
                    if (proc.procedure_number == i)
                    {
                        outstr += "Y";
                        proc_list += "" + i;
                    }
                    else
                    {
                        outstr += "N";
                    }
                } // next i
                proc_list = proc_list.Substring(1);                             //strip leading comma

                Program.outfile.WriteLine(outstr);                              //output to transparent.txt

                desc = "Activities: " + proc_list;
                if (Program.g_is_test)
                {
                    Program.Audit(desc);
                }
                else
                {
                    //Save the selected procedures plus both Program.Audits in the event log; link with tc_event_id
                    //(this assumes the procedure Program.Audit is mixed in with the class Program.Audit)
                    PFSEventLog.AddTransparentMappingEventLogEntry(desc, Program.gLogUnitID, Program.gLogEncounterID,
                        tc_event_id, Program.gLogMapperVersion,
                        Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
                }
            } // next proc
        }

    }
}


