﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;                     // for LINQ; add a reference
using System.Data.SqlClient;                // for SqlConnection
using System.IO;                            // for Directory, Path, Stream
using PfsShared;                            // add a reference to Shared2 project

// ============================================================================
// Mental Health transparent mapping -- GOES HERE --
// Carle MH Epic
// ============================================================================
// This processes one patient.  Only one database query is used.  :)
//
// All search functions use exact match for category, description and field name.
// Codes and results can be a single word or a list of words.
// Codes default to exact match; prefix with CODE_CODE_LIKE_PREFIX if desired (single word only)
// Results default to like matching; prefix with RESULT_EXACT_MATCH_PREFIX if desired (single word only)
//
// All searches are case insensitive.
//
//
namespace TransparentMapping
{
    class MentalHealth
    {
        private const int MAX_INDS = 120;
        private const int MAX_PROCS = 20;

        private const string CODE_LIKE_PREFIX = "%!";       // use to make codes %like%
        private const string EXACT_MATCH_PREFIX = "&!";     // use to make desc/result exact match
        private const string CHAR_COMMA = "||";             // use to insert literal comma in word list
        private const string NOT_PREFIX = "!!";             // use to insert literal comma in word list
        private const string AVOID_NEGATIVE = "!;";
        private const string EXACT_SQL_PREFIX = "!SQL!";     // use to give sql constraint as given
        private const string STARTS_WITH = "[!";     // use to give sql constraint as given

        private struct indicator_data
        {
            public bool is_checked;
            public int radio_group;
        }

        private struct proc_data
        {
            public int procedure_number;
            public DateTime start;
            public DateTime finish;
        }

        // These are database CHART_ITEMs for this patient
        private CHART_ITEM[] _chart_items_since_admission;
        //private CHART_ITEM[] _chart_items_since_unit_arrival;
        //private CHART_ITEM[] _chart_items_during_pull_period;
        //private CHART_ITEM[] _chart_items_pull_period_plus;
        private CHART_ITEM[] _chart_items_since24hrs;
        //private CHART_ITEM[] _chart_items_since13hrs;
        //private CHART_ITEM[] _chart_items_since25hrs;
        //private CHART_ITEM[] _chart_items_since9hrs;
        // These are database PROCEDURE_EVENTs for this patient
        private PROCEDURE_EVENT[] _procedure_events;

        private indicator_data[] _inds;
        private List<proc_data> _procs;
        //private List<proc_data> _outcomes;

        private const int DEFAULT_BUCKET_SIZE = 20;       //min

        private PatientInfo _pat;
        private int _bucket_size = DEFAULT_BUCKET_SIZE;
        private bool is_default;
        private string txarea;
        private bool g_toi4 = false;
        private bool g_gitube = false;
        private DateTime loc_in;
        private DateTime loc_out;
        private DateTime loc_arrtime;

        private bool exclude_periop_data = false;
        private LOAtypePrecision[] ary_hemodial = new LOAtypePrecision[5];
        private int numhemodial = 0;
        private string psyinterv_filter = "";
        private enum SearchDepth
        {
            SearchDefault,
            SearchPullRange,            //search within the current pull     -- default
            SearchSinceArrival,         //search since arrival to the unit
            SearchSinceAdmission,        //search everything since admission to the hospital
            SearchPullPlus,
            SearchSince25Hrs,
            SearchSince24Hrs,
            SearchSince16Hrs,
            SearchSince13Hrs,
            SearchSince9Hrs
        }

        private enum CountMode
        {
            CountAll,
            CountFirst                  //stop after one is found
        }

        private enum GetValueMode
        {
            GetTotal,
            GetMax,
            GetLast
        }

        enum Frequencies
        {
            QNONE,
            Q4H,
            Q2H,
            Q1H,
            Q30M
        }

        private struct fmapRow
        {
            public double los_high;                       //the LOS being testing
            public int[] freq;                           //the count required for each Q value
        }
        private List<fmapRow> _freq_map;                    //1,2,4,8,12,24 hours

        private struct gBucket
        {
            public int bucket;
            public string code;
            public DateTime evdt;
            public bool using_waiver;
            public bool has_all_deps;
            public int num_addl_items;
        }
        private struct gGap
        {
            public int gap;
            public DateTime evdt1;
            public DateTime evdt2;
        }
        private struct MedChartItem
        {
            public string code;
            public string orderid;
            public DateTime evdt;
            public bool valid;
        }


    //
    // This is the main entry point
    //
    public void ProcessPatient(PatientInfo pat)
        {
            _pat = pat;
            bool use_default = false;
            bool no_chart_items_in_24hrs = false;

            InitIndicators(); // sets is_default
            InitProcs();
            if (!is_default)
            {
                LoadFreqTable();
                no_chart_items_in_24hrs = (LoadPatientChart() == 0);
                if (no_chart_items_in_24hrs)
                    Program.Audit("No chart items received in past 24 hrs.");
                {
                    Check_1_2_3();
                    Check_4();
                    Check_5_6();
                    Check_7_8_9();
                    Check_10_11_12_13_14();
                    Check_15();
                    Check_16_17();
                    Check_18_19_20();
                    Check_21();
                    Check_22();
                    Check_23();
                    Check_24();
                    //CheckUserDefined();
                    AtLeastOneADL();
                }
            }

            //if (!no_chart_items_in_24hrs)
            {

                HighestIndicatorInEachGroupWins();

                if (!is_default)
                {
                    if (!Program.g_noactivities) CheckProcs();
                }

                if (Program.g_no_output) return;

                OutputClass(use_default);
                if (!Program.g_noactivities) OutputProcs();
            }
        }


        private void InitIndicators()
        {
            // Make a new (empty) indicator array
            _inds = new indicator_data[MAX_INDS + 1];           // This 1 based so add one
            is_default = false;
            txarea = "";

            // get indicator radio groups from the database
            // ** (This database access can be replaced once we have a C# methodolgy cache)
            var db = PFSDBUtility.NewPfsDataContext();
            var query = from idef in db.INDICATOR_DEFINITIONs
                        where (idef.METHODOLOGY_ID == _pat.meth_id)
                        select idef;
            foreach (var idef in query)
            {
                if (idef.INDICATOR_NUMBER <= _inds.GetUpperBound(0))
                {
                    // (convert nulls to zero)
                    _inds[idef.INDICATOR_NUMBER].radio_group = PFSDBUtility.DBToInt(idef.RADIO_GROUP);
                }
            }
        }

        private void InitProcs()
        {
            _procs = new List<proc_data>();
            //_outcomes = new List<proc_data>();
        }

        private fmapRow LoadFreqTableRow(double los_high, string values)
        {
            fmapRow fmrow;

            fmrow.los_high = los_high;
            fmrow.freq = new int[Enum.GetNames(typeof(Frequencies)).Length];

            var arr = values.Split(',');
            for (int i = 0; i <= fmrow.freq.GetUpperBound(0); i++)
            {
                fmrow.freq[i] = arr[i].ToInteger();
            }
            return fmrow;
        }

        // These are the number of times you need to do something during a certain time period (LOS)
        // in order for it to be considered one of the frequencies listed.
        private void LoadFreqTable()
        {
            _freq_map = new List<fmapRow>();
            //                              LOS,  None Q4h Q2h Q1h Q30m
            _freq_map.Add(LoadFreqTableRow(1, "    0,  0,  0,  1,  2"));
            _freq_map.Add(LoadFreqTableRow(2, "    0,  0,  1,  2,  4"));
            _freq_map.Add(LoadFreqTableRow(4, "    0,  1,  2,  4,  8"));
            _freq_map.Add(LoadFreqTableRow(6, "    0,  2,  3,  6,  9"));
            _freq_map.Add(LoadFreqTableRow(8, "    0,  2,  4,  8, 12"));
            _freq_map.Add(LoadFreqTableRow(12, "   0,  3,  6, 12, 18"));
            _freq_map.Add(LoadFreqTableRow(16, "   0,  4,  8, 16, 24"));
            _freq_map.Add(LoadFreqTableRow(24, "   0,  6, 12, 24, 36"));
            //New freq table 2/5/14
            //q4	q2	q1	q30     q30
            //            Non-ICU	ICU & SD
            // 4	8	15	29	    36
            // 3	5	9	17	    24
            // 2	4	7	13	    19
            // 2	3	5	10	    13

            //to read this table: if you have at least 6 asessmenets in a 12-hour period, you get Q1h
            //(LOS=12, column Q1h has a count of 6)
        }

        private Frequencies FreqForCount(double los_hours, int count)
        {
            foreach (var fmrow in _freq_map)
            {
                if (los_hours <= fmrow.los_high)
                {
                    // Option: pro-rate the count upward if the LOS is less than the row we are interested in.
                    //         This will bump the count to what it might have been at the full LOS.
                    // Note: truncate the result; rounding inflates the value too much.
                    int prorated_count = (int)((fmrow.los_high / los_hours) * count);

                    // foreach goes low to high; go from high to low instead
                    for (int j = (int)Frequencies.Q30M; (j > (int)Frequencies.QNONE); j--)
                    { //search right to left
                        if (prorated_count >= fmrow.freq[j])
                        {
                            return (Frequencies)j;
                        }
                    } // next j
                }
            }

            return Frequencies.QNONE;
        }

        //
        // Load patient chart from the database (one DB access)
        // Load everything since admission
        //
        //private int LoadPatientChart()
        //{
        //    int ct_in_25hrs = 0;
        //    int ctperiop = 0;
        //    // Get the entire patient chart (all units and dates for this patient)
        //    var db = PFSDBUtility.NewPfsDataContext();
        //    var query = from item in db.CHART_ITEMs
        //                where (item.ENCOUNTER_ID == _pat.encounter_id)
        //                where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-25))
        //                select item;
        //    // Save the result
        //    ct_in_25hrs = query.Count();
        //    Program.VerboseAudit("Since 25 hrs count=" + ct_in_25hrs);
        //    _chart_items_since25hrs = query.ToArray();

        //    // Convert all results to lower case for case insensitive comparisons
        //    foreach (var item in _chart_items_since25hrs)
        //    {
        //        if (item.CATEGORY != null) item.CATEGORY = item.CATEGORY.ToLower();
        //        if (item.CODE != null) item.CODE = item.CODE.ToLower();
        //        if (item.DESCRIPTION != null) item.DESCRIPTION = item.DESCRIPTION.ToLower();
        //        if (item.FIELD_NAME != null) item.FIELD_NAME = item.FIELD_NAME.ToLower();
        //        if (item.RESULT != null) item.RESULT = item.RESULT.ToLower();
        //        //mark items during periop
        //        foreach (var perioploc in Program.patperioplist)
        //        {
        //            if (item.EVENT_DATETIME >= perioploc.in_time && item.EVENT_DATETIME <= perioploc.out_time)
        //            {
        //                item.UNIT_ID = -6;
        //                ctperiop++;
        //            }
        //        }

        //    }

        //    Program.VerboseAudit("Since 25 hrs count of periop/temp items=" + ctperiop);
        //    // Prepare more versions of the chart

        //    //var query2 = from item in _chart_items_since25hrs
        //    //             where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-13)) && (item.EVENT_DATETIME <= _pat.pull_finish)
        //    //             select item;
        //    //Program.VerboseAudit("Since 13 hrs count=" + query2.Count());
        //    //_chart_items_since13hrs = query2.ToArray();

        //    //query2 = from item in _chart_items_since25hrs
        //    //             //                     where (item.EVENT_DATETIME >= _pat.pull_start) && (item.EVENT_DATETIME <= _pat.pull_finish)
        //    //         where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-8)) && (item.EVENT_DATETIME <= _pat.pull_finish)
        //    //         select item;
        //    //_chart_items_during_pull_period = query2.ToArray();

        //    return ct_in_25hrs;
        //}

        private int LoadPatientChart()
        {
            // Hemodialysis   Started Completed  no more than 4 hours
            // 3040100125

            //            Placement of UVC
            //3042394421 date
            //3042394422 time
            //900700 date
            //900701 time

            //Removal of UVC
            //3042394424
            //3042394425
            //900702
            //900703
            foreach (var p in Program.patloclist)
            {
                if (p.loc_idx == _pat.loc_idx)
                {
                    loc_in = p.in_time;
                    loc_out = p.out_time;
                    loc_arrtime = p.arr_time;
                }
            }
            _pat.los_hours = PFSUtility.DateDiffInMinutes(loc_in, loc_out) / 60.0;
            Program.VerboseAudit("LoadChart los=" + _pat.los_hours);
            //Program.VerboseAudit("LoadChart unit=" + p.unit_name + " locidx=" + p.loc_idx + " in=" + p.in_time + " out=" + p.out_time);

            int ct_in_24hrs = 0;
            int ctperiop = 0;
            // Get the entire patient chart (all units and dates for this patient)
            var dba = PFSDBUtility.NewPfsDataContext();
            var queryall = from item in dba.CHART_ITEMs
                           where (item.ENCOUNTER_ID == _pat.encounter_id)
                           orderby item.EVENT_DATETIME
                           select item;
            //Get the ECT Aldrete score times
            var queryECT = (from e in queryall
                            where (e.CODE == "304239656")
                            select e.EVENT_DATETIME).Distinct().ToList();
            foreach (var ect in queryECT)
            {
                Program.VerboseAudit("ECT time=" + ect);
            }
            //Exclude the ECT times
            var querya = from g in queryall
                         where (!queryECT.Contains(g.EVENT_DATETIME)
                                || _pat.short_name == "MEE")
                         select g;
            // Save the result
            _chart_items_since_admission = querya.ToArray();

            var query = from item in _chart_items_since_admission
                        where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-24)
                               && (item.EVENT_DATETIME <= _pat.pull_finish))
                        select item;
            // Exclude hemodialysis items between Started and Completed (max=Started+4hrs)
            // Find Started/Completed pairs:
            bool starta = false;
            DateTime starta_dt = DateTime.MinValue;
            foreach (var c in query)
            {
                if (c.CODE == "3040100125")
                {
                    if (c.RESULT.ToLower().StartsWith("start"))
                    {
                        if (!starta)
                        {
                            starta = true;
                            starta_dt = c.EVENT_DATETIME;
                        }
                        else // then found a second start after the initial start
                        {
                            numhemodial++;
                            ary_hemodial[numhemodial].startdt = starta_dt;
                            if (c.EVENT_DATETIME >= starta_dt.AddHours(4))
                                ary_hemodial[numhemodial].enddt = starta_dt.AddHours(4);
                            else
                                ary_hemodial[numhemodial].enddt = c.EVENT_DATETIME;
                            starta_dt = c.EVENT_DATETIME;
                            Program.VerboseAudit("Ignoring HD items between: " + ary_hemodial[numhemodial].startdt + " => " + ary_hemodial[numhemodial].enddt + " [3]");
                        }
                    }
                    else if (c.RESULT.ToLower().StartsWith("complete"))
                    {
                        if (!starta) //then complete without a start: go back 4 hours.
                        {
                            numhemodial++;
                            ary_hemodial[numhemodial].startdt = c.EVENT_DATETIME.AddHours(-4);
                            ary_hemodial[numhemodial].enddt = c.EVENT_DATETIME;
                            Program.VerboseAudit("Ignoring HD items between: " + ary_hemodial[numhemodial].startdt + " => " + ary_hemodial[numhemodial].enddt + " [2]");
                        }
                        else
                        {
                            numhemodial++;
                            ary_hemodial[numhemodial].startdt = starta_dt;
                            ary_hemodial[numhemodial].enddt = c.EVENT_DATETIME;
                            Program.VerboseAudit("Ignoring HD items between: " + ary_hemodial[numhemodial].startdt + " => " + ary_hemodial[numhemodial].enddt + " [1]");
                        }
                        starta = false;
                    }
                }
            }
            if (starta)
            {
                numhemodial++;
                ary_hemodial[numhemodial].startdt = starta_dt;
                ary_hemodial[numhemodial].enddt = starta_dt.AddHours(4);
                Program.VerboseAudit("Ignoring HD items between: " + ary_hemodial[numhemodial].startdt + " => " + ary_hemodial[numhemodial].enddt + " [4]");
            }

            // Save the result
            ct_in_24hrs = query.Count();
            _chart_items_since24hrs = query.ToArray();

            // Convert all results to lower case for case insensitive comparisons
            foreach (var item in _chart_items_since24hrs)
            {
                item.SOURCE_TEXT = null;
                if (item.CATEGORY != null) item.CATEGORY = item.CATEGORY.ToLower();
                if (item.CODE != null) item.CODE = item.CODE.ToLower();
                if (item.DESCRIPTION != null) item.DESCRIPTION = item.DESCRIPTION.ToLower();
                if (item.FIELD_NAME != null) item.FIELD_NAME = item.FIELD_NAME.ToLower();
                if (item.RESULT != null) item.RESULT = item.RESULT.ToLower();
                for (int i = 1; i <= numhemodial; i++)
                {
                    if (item.EVENT_DATETIME >= ary_hemodial[i].startdt
                        && item.EVENT_DATETIME <= ary_hemodial[i].enddt)
                    {
                        item.UNIT_ID = -6;
                        ctperiop++;
                    }
                }
            }

            Program.VerboseAudit("Since 24 hrs count items=" + ct_in_24hrs);
            Program.VerboseAudit("Since 24 hrs count of HD items=" + ctperiop);

            return ct_in_24hrs;
        }

        //private int LoadPatientChart()
        //{
        //    int ct_in_24hrs = 0;
        //    int ctperiop = 0;

        //    foreach (var p in Program.patloclist)
        //    {
        //        if (p.loc_idx == _pat.loc_idx)
        //        {
        //            loc_in = p.in_time;
        //            loc_out = p.out_time;
        //        }
        //    }
        //    _pat.los_hours = PFSUtility.DateDiffInMinutes(loc_in, loc_out) / 60.0;
        //    Program.VerboseAudit("LoadChart los=" + _pat.los_hours);



        //    // Get the entire patient chart (all units and dates for this patient)
        //    var dba = PFSDBUtility.NewPfsDataContext();
        //    var querya = from item in dba.CHART_ITEMs
        //                 where (item.ENCOUNTER_ID == _pat.encounter_id)
        //                 //where (item.CODE == "304239375"
        //                 //|| item.CODE == "304239487"
        //                 //|| item.CODE == "1540100298")
        //                 select item;
        //    // Save the result
        //    _chart_items_since_admission = querya.ToArray();

        //    var db = PFSDBUtility.NewPfsDataContext();
        //    var query = from item in db.CHART_ITEMs
        //                where (item.ENCOUNTER_ID == _pat.encounter_id)
        //                where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-24))
        //                where (item.EVENT_DATETIME <= _pat.pull_finish)
        //                select item;
        //    // Save the result
        //    ct_in_24hrs = query.Count();
        //    _chart_items_since24hrs = query.ToArray();

        //    // Convert all results to lower case for case insensitive comparisons
        //    foreach (var item in _chart_items_since24hrs)
        //    {
        //        item.SOURCE_TEXT = null;
        //        if (item.CATEGORY != null) item.CATEGORY = item.CATEGORY.ToLower();
        //        if (item.CODE != null) item.CODE = item.CODE.ToLower();
        //        if (item.DESCRIPTION != null) item.DESCRIPTION = item.DESCRIPTION.ToLower();
        //        if (item.FIELD_NAME != null) item.FIELD_NAME = item.FIELD_NAME.ToLower();
        //        if (item.RESULT != null) item.RESULT = item.RESULT.ToLower();
        //        foreach (var perioploc in Program.patperioplist)
        //        {
        //            if (item.EVENT_DATETIME >= perioploc.in_time && item.EVENT_DATETIME <= perioploc.out_time)
        //            {
        //                item.UNIT_ID = -6;
        //                ctperiop++;
        //            }
        //        }
        //    }

        //    Program.VerboseAudit("Since 24 hrs count items=" + ct_in_24hrs);
        //    Program.VerboseAudit("Since 24 hrs count of periop/temp items=" + ctperiop);


        //    return ct_in_24hrs;
        //}


        // Delay looking for patient procedures until they are wanted
        private void LoadPatientProceduresIfNeeded()
        {
            if (_procedure_events != null) return;

            var db = PFSDBUtility.NewPfsDataContext();
            var query = from proc in db.PROCEDURE_EVENTs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                        where (proc.PROCEDURE_DATETIME >= _pat.pull_finish.AddHours(-24))
                        select proc;
            _procedure_events = query.ToArray();
        }



        // Started a new chart item query (of a certain depth)
        // Default is the chart during the pull range.  Options for unit arrival and admission to hospital.
        private IEnumerable<CHART_ITEM> StartNewQuery()
        {
            return StartNewQuery(SearchDepth.SearchDefault);
        }
        private IEnumerable<CHART_ITEM> StartNewQuery(SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;

            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= loc_in && (item.EVENT_DATETIME <= loc_out)) select item);
                    else
                        return (from item in _chart_items_since24hrs where (item.EVENT_DATETIME >= loc_in && item.EVENT_DATETIME <= loc_out) select item);
                case SearchDepth.SearchSinceAdmission:
                    return (from item in _chart_items_since_admission select item);
                //case SearchDepth.SearchPullPlus:
                //    return (from item in _chart_items_pull_period_plus select item);
                case SearchDepth.SearchSince24Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                    else
                        return (from item in _chart_items_since24hrs where (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince16Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-16) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since24hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-16) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince13Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-13) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since24hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-13) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                //case SearchDepth.SearchSince12Hrs:
                //    if (exclude_periop_data)
                //        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-12) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                //    else
                //        return (from item in _chart_items_since24hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-12) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince9Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-9) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since24hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-9) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                //case SearchDepth.SearchSince4Hrs:
                //    if (exclude_periop_data)
                //        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME > _pat.pull_finish.AddHours(-4) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                //    else
                //        return (from item in _chart_items_since24hrs where item.EVENT_DATETIME > _pat.pull_finish.AddHours(-4) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
            }
            return null;
        }

        // Is this search term a word or a list of words?
        private bool ValueIsAList(string s)
        {
            return (s != null) && s.Contains(',');                //is this a comma-separated list?
        }

        // Break apart a wordlist and prepare its members
        private string[] SplitOnCommaAndPrepareElements(string s)
        {
            // If a word list has a space after each comma, we will get leading blanks in each term
            var arr = s.Split(',');
            for (int i = 0; i <= arr.GetUpperBound(0); i++)
            {
                arr[i] = arr[i].Trim();                         // get rid of leading blanks
                arr[i] = arr[i].Replace(CHAR_COMMA, ",");       // insert a literal comma into search word
                arr[i] = arr[i].ToLower();                      // convert for case-insensitive comparisons
            }
            return arr;
        }

        // Add a basic chart item filter to a LINQ query
        //
        // cat = exact match
        // code_list = exact match; optional "like" match
        // desc_list = "like" match; optional exact match
        // field = exact match
        // result_list = "like" match; optional exact match
        //
        // These are comma-separated lists.  
        // Search for a literal comma by using CHAR_COMMA in the string.
        private IEnumerable<CHART_ITEM> AndItemFilter(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list)
        {
            // Converted all values to lower case for case-insensitive comparisons.
            if (!String.IsNullOrEmpty(cat))
            {
                cat = cat.ToLower();
                query = query.Where(e => e.CATEGORY == cat);
            }

            if (!String.IsNullOrEmpty(code_list))
            {
                code_list = code_list.ToLower();
                if (code_list.Left(2) == CODE_LIKE_PREFIX)
                {            // override for "like" match?
                    query = query.Where(e => e.CODE.Contains(code_list.Substring(2)));
                }
                else if (code_list.Left(2) == EXACT_MATCH_PREFIX)
                {
                    query = query.Where(e => e.CODE == code_list.Substring(2));
                }
                else if (ValueIsAList(code_list))
                {
                    query = AndCodeInList(query, code_list);            // find one of the words
                }
                else
                {
                    query = query.Where(e => e.CODE.Contains(code_list));      // find this word
                }
            }

            if (!String.IsNullOrEmpty(desc_list))
            {
                desc_list = desc_list.ToLower();
                if (desc_list.Left(2) == EXACT_MATCH_PREFIX)
                {          // exact match override?
                    query = query.Where(e => e.DESCRIPTION == desc_list.Substring(2));
                }
                else if (desc_list.Left(2) == NOT_PREFIX)
                {
                    // query = query.Where(e => !e.DESCRIPTION.Contains(desc_list.Substring(2)));
                    query = AndDescriptionNOTInList(query, desc_list.Substring(2));
                }
                else if (ValueIsAList(desc_list))
                {
                    query = AndDescriptionInList(query, desc_list);
                }
                else
                {
                    desc_list = desc_list.Replace(CHAR_COMMA, ",");     // look for literal comma requests
                    query = query.Where(e => e.DESCRIPTION.Contains(desc_list));
                }
            }

            if (!String.IsNullOrEmpty(field))
            {
                field = field.ToLower();
                query = query.Where(e => e.FIELD_NAME == field);
            }

            if (!String.IsNullOrEmpty(result_list))
            {
                query = query.Where(e => e.RESULT != null);
                result_list = result_list.ToLower();                        // make all results case insensitve
                if (result_list.Left(2) == EXACT_MATCH_PREFIX)
                {     // exact match override?
                    query = query.Where(e => e.RESULT == result_list.Substring(2));
                }
                else if (result_list.Left(2) == AVOID_NEGATIVE)
                {
                    query = query.Where(e => (!e.RESULT.Contains("no " + result_list.Substring(2))) && ((e.RESULT == result_list.Substring(2)) || (e.RESULT.Contains(";" + result_list.Substring(2)))));
                }
                else if (ValueIsAList(result_list))
                {
                    query = AndResultInList(query, result_list);
                }
                else if (result_list.Left(2) == STARTS_WITH)
                {
                    query = query.Where(e => e.RESULT.StartsWith(result_list.Substring(2)));
                }
                else
                {
                    result_list = result_list.Replace(CHAR_COMMA, ",");     // look for literal comma requests
                    query = query.Where(e => e.RESULT.Contains(result_list));
                }
            }
            return query;
        }

        // Look for a code that is one of the words in the list.
        private IEnumerable<CHART_ITEM> AndCodeInList(IEnumerable<CHART_ITEM> query, string code_list)
        {
            if (String.IsNullOrEmpty(code_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(code_list);
            //            return query.Where(e => arr.Any(x => x.Contains(e.CODE))); // "like" match
            return query.Where(e => e.CODE.ContainsAny(arr)); // "like" match
        }

        // Look for a description that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndDescriptionInList(IEnumerable<CHART_ITEM> query, string desc_list)
        {
            if (String.IsNullOrEmpty(desc_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(desc_list);
            //return query.Where(e => arr.Any(x => x.Contains(e.DESCRIPTION))); // "like" match
            return query.Where(e => e.DESCRIPTION.ContainsAny(arr)); // "like" match
        }

        private IEnumerable<CHART_ITEM> AndDescriptionNOTInList(IEnumerable<CHART_ITEM> query, string desc_list)
        {
            if (String.IsNullOrEmpty(desc_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(desc_list);
            //return query.Where(e => arr.Any(x => x.Contains(e.DESCRIPTION))); // "like" match
            return query.Where(e => !e.DESCRIPTION.ContainsAny(arr)); // "like" match
        }


        // Look for a result that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultInList(IEnumerable<CHART_ITEM> query, string result_list)
        {
            bool use_start_with = false;

            if (String.IsNullOrEmpty(result_list)) return query;

            if (result_list.Left(2) == STARTS_WITH)
            {
                use_start_with = true;
                result_list = result_list.Substring(2);
            }

            var arr = SplitOnCommaAndPrepareElements(result_list);
            //            return query.Where(e => arr.Any(x => x.Contains(e.RESULT))); // "like" match
            if (use_start_with)
                return query.Where(e => arr.Any(item => e.RESULT.StartsWith(item)));
            else
                return query.Where(e => e.RESULT.ContainsAny(arr)); // "like" match

        }

        // Look for a result that does not contain one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultNotInList(IEnumerable<CHART_ITEM> query, string result_list)
        {
            if (String.IsNullOrEmpty(result_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(result_list);
            //            return query.Where(e => !arr.Any(x => x.Contains(e.RESULT))); // "like" match
            return query.Where(e => !e.RESULT.ContainsAny(arr)); // "like" match
        }

        private string DescribeSearchDepth(SearchDepth search_depth)
        {
            string result = "";

            switch (search_depth)
            {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    //result = "in pull range"
                    result = "";                         //be quiet since this is default
                    break;
                case SearchDepth.SearchSinceArrival:
                    result = "since arrival to unit";
                    break;
                case SearchDepth.SearchSinceAdmission:
                    result = "since admission";
                    break;
                case SearchDepth.SearchPullPlus:
                    result = "in LOS range plus 4 hours after";
                    break;
            }

            return result;
        }

        // Describe a query that has already been run
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Make a simple query and describe the results
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            var arr = query.ToArray();
            return Describe(arr, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private string LookingFor(string so_far, string what, string op, string value)
        {
            string result;
            if (String.IsNullOrEmpty(value)) return so_far;

            if (so_far == "")
                result = "looking for " + what;
            else
                result = so_far + " and " + what;

            if (value.Left(2) == EXACT_MATCH_PREFIX)
                result += "='" + value.Substring(2) + "'";
            else if (value.Left(2) == CODE_LIKE_PREFIX)
                result += " contains " + "'" + value.Substring(2) + "'";
            else if (ValueIsAList(value))
                result += " in '" + value + "'";
            else
                result += op + "'" + value + "'";

            return result;
        }
        private string Describe(CHART_ITEM[] arr, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string result = "";
            result = LookingFor(result, "code", "=", code_list);
            result = LookingFor(result, "desc", " contains ", desc_list);
            result = LookingFor(result, "result", " contains ", result_list);

            if (arr.Count() == 0)
            {
                result += "; not found " + DescribeSearchDepth(search_depth);
            }
            else
            {
                var e = arr[0];
                result = "FOUND: " + result + "; found";
                // We might have searched for a pattern or word list in several fields - show what was found
                if (e.CODE != null) result += " code='" + e.CODE + "'";
                if (e.DESCRIPTION != null) result += " desc='" + e.DESCRIPTION + "'";
                if (e.RESULT != null) result += " result='" + e.RESULT + "'";
                // Are there more results?  Just say how many; we aren't going to list them.
                if (arr.Count() == 2)
                {
                    result += " (1 more result)";
                }
                else if (arr.Count() > 2)
                {
                    result += " (" + (arr.Count() - 1) + " more results)";
                }
            }

            return result;
        }


        //Set an indicator for this reason (low level)
        private void SetInd(int inum, string reason)
        {
            if (_inds[inum].is_checked)
            {
                Program.VerboseAudit("Set Ind #" + inum + ": " + reason);          //already set - repeat for verbose only
            }
            else
            {
                _inds[inum].is_checked = true;
                Program.Audit("Set Ind #" + inum + ": " + reason);
            }
        }

        //Clear an indicator for this reason (low level)
        private void ClrInd(int inum, string reason)
        {
            if (_inds[inum].is_checked)
            {
                Program.VerboseAudit("Clr Ind #" + inum + ": " + reason);          //already clear - repeat verbose only
            }
            else
            {
                _inds[inum].is_checked = false;
                Program.Audit("Clr Ind #" + inum + ": " + reason);
            }
        }

        // Count how many items meet the conditions
        // All items are listed
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountItems(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            bool first = true;
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            //if (exclude_periph_iv)
            //    query = AndDescriptionNOTInList(query, "peripheral iv");
            int count = query.Count();
            found_what = "";
            //Program.VerboseAudit("countitems: result_list="+result_list + " code=" + code_list);
            if (count > 0 && trace)
            {
                foreach (var item in query)
                {
                    if (first)
                    {
                        // always return what was found
                        //            found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                        found_what = "Found desc=" + item.DESCRIPTION + ";result=" + item.RESULT + ";code=" + item.CODE + ";items found=" + count;
                        // echo the result?
                        Program.VerboseAudit(found_what);
                        first = false;
                    }

                }
            }
            return count;
        }


        // Count how many items meet the conditions
        // This has the option to list every entry or give a total
        private int CountResultInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what = "";
            string s = "";

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            //if (exclude_periph_iv)
            //    query = AndDescriptionNOTInList(query, "peripheral iv");
            var arr = SplitOnCommaAndPrepareElements(result_list);

            foreach (var item in query)
            {
                // Figure out which of the search words were found
                for (int i = 0; i <= arr.GetUpperBound(0); i++)
                {
                    if (item.RESULT.Contains(arr[i]))
                    {
                        count++;
                        s = arr[i];
                        found_what = "Found desc:" + item.DESCRIPTION + ";result=" + item.RESULT + ";code=" + item.CODE;
                        //if (trace) Program.VerboseAudit(found_what);
                        Program.VerboseAudit(found_what);
                    }
                }
                if (count_mode != CountMode.CountAll) break;        //stop counting

            }

            return count;
        }

        // Count how many items meet the conditions
        // This has the option to list every entry or give a total
        private int CountResultInListEXCEPTList(string cat, string code_list, string desc_list, string field, string result_list, string negresult_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what = "";

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            query = AndResultNotInList(query, negresult_list);

            var arr = SplitOnCommaAndPrepareElements(result_list);

            foreach (var item in query)
            {
                // Figure out which of the search words were found
                var s = item.RESULT.ContainsWhich(arr);
                found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' code='" + item.CODE + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "'";
                if (trace) Program.VerboseAudit(found_what);
                count++;
                if (count_mode != CountMode.CountAll) break;        //stop counting
            }

            if (count > 0)
            {
                //We already printed what was found; maybe add how many?
                if (trace && count > 0) Program.VerboseAudit("found " + count + " total");
            }
            else
            {
                // Describe what was *not* found
                //found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                //if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        // Count how many items contain ALL results in list
        private int CountResultInListAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what = "";

            var arr = SplitOnCommaAndPrepareElements(result_list);

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            for (int i = 0; i <= arr.GetUpperBound(0); i++)
            {
                if (arr[i].StartsWith(NOT_PREFIX))
                {
                    query = query.Where(e => !e.RESULT.Contains(arr[i].Substring(2)));
                }
                else
                {
                    query = query.Where(e => e.RESULT.Contains(arr[i]));
                }
            }

            count = query.Count();

            if (count > 0)
            {
                found_what = "found item with all results in '" + result_list + "' without those prefixed by !!";
                if (trace) Program.VerboseAudit(found_what);
            }
            else
            {
                // Describe what was *not* found
                //found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                //if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (ValueIsAList(result_list))
            {
                return CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what);
            }
            else
            {
                return CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what);
            }
        }

        // Is there an item with this result?
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContains(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (ValueIsAList(result_list))
            {
                return (CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            }
            else
            {
                return (CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
            }
        }

        // Result contains all in list
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (ValueIsAList(result_list))
            {
                return (CountResultInListAll(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            }
            else
            {
                found_what = "Not valid for non-list results.";
                return false;
            }
        }
        // Count how many items meet the conditions, with none of the results in result_list.
        // This has the option to say how many were found rather than list all.
        private int CountResultNotInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0, rec_count = 0;
            bool found_one;

            found_what = "";
            var arr = SplitOnCommaAndPrepareElements(result_list);         // also deals with CHAR_COMMA

            //Do not filter by result here; we need all results
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            foreach (var item in query)
            {
                rec_count++;
                //Look for each search word in the result
                found_one = false;

                foreach (string s in arr)
                {
                    if (String.Equals(item.RESULT, s))
                    {
                        found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "' -- ignore this";
                        Program.VerboseAudit(found_what);
                        found_one = true;
                        break;
                    }
                }

                if (!found_one)
                {
                    //none of the words were found - good!
                    found_what = Describe(cat, code_list, desc_list, field, "", search_depth) + " -- does not contain '" + result_list + "'";
                    Program.VerboseAudit(found_what);
                    count++;
                    if (count_mode != CountMode.CountAll) break;
                }
            }

            if (count > 0)
            {
                //We already printed what was found; maybe show the total?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            }
            else if (rec_count > 0)
            {
                //We already printed what we ignored
            }
            else
            {
                //Print what did not exist
                found_what = Describe(cat, code_list, desc_list, field, "", search_depth);      //not found
                Program.VerboseAudit(found_what);
            }

            return count;
        }

        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, string found_what)
        {
            // Search in the pull range with trace on
            return CountResultDoesNotContain(cat, code_list, desc_list, field, result_list, out found_what, SearchDepth.SearchDefault, true);
        }
        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, out string found_what, SearchDepth search_depth, bool trace)
        {
            return CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0;
        }

        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            return (CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0);
        }

        // Set the indicator if the conditions are met
        //
        private bool SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SearchDepth s = SearchDepth.SearchDefault;
            //if (inum <= 3) s = SearchDepth.SearchSince25Hrs;
            //if (inum == 4) s = SearchDepth.SearchSince13Hrs;
            //if (inum == 5 || inum == 6) s = SearchDepth.SearchSince25Hrs;
            //if (inum >= 7 && inum <= 9) s = SearchDepth.SearchSince25Hrs;
            //if (inum >= 10 && inum <= 14) s = SearchDepth.SearchSince13Hrs;
            //if (inum == 15) s = SearchDepth.SearchSince25Hrs;
            //if (inum >= 16 && inum <= 20) s = SearchDepth.SearchSince9Hrs;
            //if (inum == 21) s = SearchDepth.SearchSince25Hrs;
            //if (inum == 22) s = SearchDepth.SearchSince25Hrs;
            //if (inum == 23) s = SearchDepth.SearchSince9Hrs;
            //if (inum == 24) s = SearchDepth.SearchSince9Hrs;
            return SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, s);
        }
        private bool SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            bool b = false;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return true;

            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what))
            {
                SetInd(inum, found_what);                          //echo here - found
                b = true;
            }
            else
            {
                //Program.VerboseAudit(found_what);                  //and here - not found
            }
            return b;
        }

        private void SetIndIfResultBetween(int inum, string cat, string code_list, string desc_list, string field, int loval, int hival)
        {
            SetIndIfResultBetween(inum, cat, code_list, desc_list, field, loval, hival, SearchDepth.SearchDefault);
        }

        private void SetIndIfResultBetween(int inum, string cat, string code_list, string desc_list, string field, int loval, int hival, SearchDepth search_depth)
        {
            int count = 0;
            string found_what = "";

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");
            foreach (var item in query)
            {
                Program.VerboseAudit("ResBetween: code=" + item.CODE + " result=" + item.RESULT);
                if (item.RESULT.IsNumeric())
                {
                    Program.VerboseAudit("  result is numeric");
                    if (item.RESULT.Val() >= loval && item.RESULT.Val() <= hival)
                    {
                        count++;
                        found_what = "found code=" + item.CODE + " with result=" + item.RESULT;
                        Program.VerboseAudit(found_what);
                    }
                }
            }

            if (count > 0)
            {
                SetInd(inum, found_what);
            }
        }


        // Special contains all
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContainsAll(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return;

            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                          //echo here - found
            }
            else
            {
                //Program.VerboseAudit(found_what);                  //and here - not found
            }
        }
        // Set the indicator if the result does not contain any of the words in result_list
        //
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultDoesNotContain(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return;

            //Turn trace off for ResultDoesNotContain() and echo what was set below with SetInd
            if (ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                           //echo here - not found
            }
            else
            {
                //Program.VerboseAudit(found_what);                   //and here - found
            }
        }

        // Clear the indicator if the result contains one of the words in the result_list
        //
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already clear
            if (!_inds[inum].is_checked) return;

            //Turn trace off for ResultContains() and echo what was set below with SetInd
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                ClrInd(inum, found_what);                           //echo here - found
            }
            else
            {
                Program.VerboseAudit(found_what);                   //and here - not found
            }
        }


        //These slightly smaller functions are meant for places where you aren't really looking for a chart result
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }

        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            return (CountResultContains(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
        }

        // Variations with optional parameters (can switch to optional parameters in VS 2010)
        //
        private void SetIndIfFound(int inum, string cat)
        {
            SetIndIfResultContains(inum, cat, "", "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list)
        {
            SetIndIfResultContains(inum, cat, code_list, "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private void ClrIndIfFound(int inum, string cat)
        {
            ClrIndIfResultContains(inum, cat, "", "", "", "", SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }


        // Get the max/total value from a result (usually in the middle of the text)
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetIntValue(get_mode, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            int value;
            int result = 0;
            bool found_one = false;

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);

            //Look for a number in the result

            foreach (var item in query)
            {
                // RESULT might be null so convert to empty string so Split won't blow up
                var arr = PFSDBUtility.DBToString(item.RESULT).Split(' ');
                foreach (string s in arr)
                {
                    //Look for 1st character numeric; IsNumeric("60min") will fail so look at 1st char only
                    if (s.Left(1).IsNumeric())
                    {
                        value = (int)s.Val();                           //Use Val; ToInteger will error on "60min"
                        switch (get_mode)
                        {
                            case GetValueMode.GetMax:
                                result = Math.Max(value, result);       //max
                                break;
                            case GetValueMode.GetTotal:
                                result += value;                        //total
                                break;
                            case GetValueMode.GetLast:
                                result = value;                         //last
                                break;
                        }

                        // print what we are searching for (the first time)
                        if (!found_one)
                        {
                            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
                        }
                        found_one = true;
                        // print each value found
                        Program.VerboseAudit("  found numeric value " + result);
                        //Keep going in case there are more
                    }
                }
            }

            if (!found_one)
            {
                //show what was not found
                Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
            }

            return result;
        }

        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetMaxValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return GetIntValue(GetValueMode.GetMax, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetTotalValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return GetIntValue(GetValueMode.GetTotal, cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Get a result; returns true if found with return_result
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result)
        {
            return GetResult(cat, code_list, desc_list, field, out return_result, SearchDepth.SearchDefault);
        }
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
            }
            else
            {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }
        private bool GetResultForInspection(string cat, string code_list, string desc_list, string field, string result, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result);

            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
            }
            else
            {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }

        private void GetResultforOther(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "Other");
            return_result = "";
            foreach (var item in query)
            {
                if (!String.Equals(item.RESULT, "Other"))
                {
                    return_result = item.RESULT;
                    //Program.VerboseAudit(found_what);
                    break;
                }

            }
            return;
        }

        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, out string return_result, out DateTime return_evdt)
        {
            return GetResultAndEVDT(cat, code_list, desc_list, field, out return_result, out return_evdt, SearchDepth.SearchDefault);
        }
        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, out string return_result, out DateTime return_evdt, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
                return_evdt = PFSDBUtility.DBToDateTime(query.First().EVENT_DATETIME);
            }
            else
            {
                return_result = "";
                return_evdt = DateTime.MinValue;
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }

        private bool GetEVDT(string cat, string code_list, string desc_list, string res, string field, int comparison, DateTime compevdt, out DateTime return_evdt)
        {
            return GetEVDT(cat, code_list, desc_list, field, res, comparison, compevdt, out return_evdt, SearchDepth.SearchDefault);
        }
        private bool GetEVDT(string cat, string code_list, string desc_list, string field, string res, int comparison, DateTime compevdt, out DateTime return_evdt, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, res);
            if (compevdt != DateTime.MinValue)
            {
                if (comparison == 0) // equal
                    query = query.Where(e => e.EVENT_DATETIME == compevdt);
                else if (comparison == 1) // GTE
                    query = query.Where(e => e.EVENT_DATETIME >= compevdt);
                else if (comparison == 2) // GT
                    query = query.Where(e => e.EVENT_DATETIME > compevdt);
                else if (comparison == 3) // LT
                    query = query.Where(e => e.EVENT_DATETIME < compevdt);
            }
            query = query.OrderByDescending(e => e.EVENT_DATETIME);
            Program.VerboseAudit("getevdt count=" + query.Count());
            if (query.Count() > 0)
            {
                return_evdt = PFSDBUtility.DBToDateTime(query.First().EVENT_DATETIME);
            }
            else
            {
                return_evdt = DateTime.MinValue;
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, res, search_depth));
            return (return_evdt != DateTime.MinValue);
        }

        private bool GetLatestResult(string code_list, out string return_result, SearchDepth search_depth)
        {
            return GetLatestResult("", code_list, "", "", out return_result, search_depth);
        }
        private bool GetLatestResult(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");
            query = query.OrderByDescending(e => e.EVENT_DATETIME);

            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT) + " charted at " + query.First().EVENT_DATETIME;
            }
            else
            {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }


        //======================================================================================
        // Mapping starts here
        //======================================================================================

        private void Check_1_2_3()
        {
            Program.VerboseAudit("Default ADL Search Scope = " + _pat.pull_finish.AddHours(-24) + " to " + _pat.pull_finish);
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 1. ADL Self");
            Program.VerboseAudit("MH 2. ADL Assist");
            Program.VerboseAudit("MH 3. ADL Complete/2+");
            Program.VerboseAudit("---------------");


            string reslist = "";

            bool feed1 = false;
            bool feed2 = false;
            bool feed3 = false;

            bool A1 = false;
            bool A2 = false;
            bool A3 = false;

            bool mainhyg1 = false;
            bool mainhyg2 = false;
            bool mainhyg3 = false;

            bool hyg1 = false;
            bool hyg2 = false;
            bool hyg3 = false;

            bool mob1 = false;
            bool mob2 = false;
            bool mob3 = false;

            reslist = "independent";
            A1 = Exists("", "12026547", "", "", reslist);
            reslist = "minimum assist";
            A2 = Exists("", "12026547", "", "", reslist);
            reslist = "moderate assist,maximum assist";
            A3 = Exists("", "12026547", "", "", reslist);
            reslist = "dependent";
            A3 = A3 || Exists("", "12026547", "", "", EXACT_MATCH_PREFIX + reslist);
            Program.VerboseAudit("12026547: 1=" + A1 + " 2=" + A2 + " 3=" + A3);

            //mainfeed
            reslist = "none";
            feed1 = Exists("", "12025931", "", "", reslist);
            reslist = "tray set-up";
            feed2 = Exists("", "12025931", "", "", reslist);
            reslist = "meal set-up provided,supplemental drinks provided,supplemental foods provided";
            feed2 |= Exists("", "1224006", "", "", reslist);
            reslist = "assisted with feeding,total feed";
            feed3 = Exists("", "12025931", "", "", reslist);

            if (_pat.age < 4.0)
            {
                feed3 = true;
                SetInd(3, "ADL complete due to age<4=" + _pat.age);
            }

            Program.VerboseAudit("OVERALL FEED: 1=" + feed1 + " 2=" + feed2 + " 3=" + feed3);

            //mainhyg
            reslist = "bathed,showered";
            hyg1 = A1 && Exists("", "12052027", "", "", reslist);
            reslist = "bathed,showered,pericare,hair washed,nail care,shaved,other (comment)";
            hyg2 = A2 && Exists("", "12052027", "", "", reslist);
            reslist = "pericare,hair washed,nail care,shaved,other (comment)";
            hyg2 = Exists("", "12052027", "", "", reslist); //because what if independent+Pericare
            reslist = "bathed,showered,pericare,hair washed,nail care,shaved,other (comment)";
            hyg3 = A3 && Exists("", "12052027", "", "", reslist);
            Program.VerboseAudit("hyg: 1=" + hyg1 + " 2=" + hyg2 + " 3=" + hyg3);

            bool oral1 = false;
            bool oral2 = false;
            bool oral3 = false;
            reslist = "teethbrushed,denture care,lip moisturizer applied,mouth swabbed,mouth moisturizer,mouth rinsed,mouth suctioned,suction toothette,suction toothette toothbrush,with chlorhexidine,with half strength hydrogen peroxide,with mouth wash,with normal saline,with baking soda solution,other (comment)";
            bool oral = Exists("", "12042967", "", "", reslist);
            if (oral)
            {
                oral1 = A1;
                oral2 = A2;
                oral3 = A3;
            }
            Program.VerboseAudit("oralhyg: 1=" + oral1 + " 2=" + oral2 + " 3=" + oral3);

            bool peri1 = false;
            bool peri2 = false;
            bool peri3 = false;
            reslist = "applied,changed,removed,absorbent pad,absorbent undergarment,diaper,incontinence pouch,panty liner,protective pad,sanitary pad,training pants,other (comment)";
            bool peri = Exists("", "12039320", "", "", reslist);
            if (peri)
            {
                peri1 = A1;
                peri2 = A2;
                peri3 = A3;
            }
            Program.VerboseAudit("perihyg: 1=" + peri1 + " 2=" + peri2 + " 3=" + peri3);

            bool skin1 = false;
            bool skin2 = false;
            reslist = "linens changed";
            bool skin = Exists("", "12216704", "", "", reslist);
            if (skin)
            {
                skin1 = A1;
            }
            reslist = "foam skin cleanser,granulex spray,incontinent cleanser,moisture barrier,protective barrier,wound cleanser,linens changed,other (comment)";
            skin = Exists("", "12216704", "", "", reslist);
            if (skin)
            {
                skin2 = A2;
            }
            Program.VerboseAudit("skinhyg: 1=" + skin1 + " 2=" + skin2);

            mainhyg3 = hyg3 || oral3 || peri3;
            mainhyg2 = hyg2 || oral2 || peri2 || skin2;
            mainhyg1 = hyg1 || oral1 || peri1 || skin1;
            Program.VerboseAudit("OVERALL HYG: 1=" + mainhyg1 + " 2=" + mainhyg2 + " 3=" + mainhyg3);

            //mainmob
            reslist = "4";
            bool edm1 = Exists("", "12039049", "", "", reslist);
            reslist = "3";
            bool edm2 = Exists("", "12039049", "", "", reslist);
            reslist = "2,1";
            bool edm3 = Exists("", "12039049", "", "", reslist);
            Program.VerboseAudit("edmonsonmob: 1=" + edm1 + " 2=" + edm2 + " 3=" + edm3);

            reslist = "4";
            bool brad1 = Exists("", "1222725", "", "", reslist);
            reslist = "3";
            bool brad2 = Exists("", "1222725", "", "", reslist);
            reslist = "2,1";
            bool brad3 = Exists("", "1222725", "", "", reslist);
            Program.VerboseAudit("bradmob: 1=" + brad1 + " 2=" + brad2 + " 3=" + brad3);

            bool bodypos1 = false;
            bool bodypos2 = false;
            bool bodypos3 = false;
            reslist = "patient independent";
            bodypos1 = Exists("", "1223657", "", "", reslist);
            reslist = "turned,position maintained,foot of bed elevated,hands-and-knees,knee-chest position,legs elevated,log-rolled,lower extremity elevated, left,lower extremity elevated, right,neutral body alignment,neutral head position,prone,reverse Trendelenburg,semi-prone, left,semi-prone, right,side-lying, left,side-lying, right,sitting up in bed,squatting,supine,supine, legs elevated,upper extremity elevated, left,upper extremity elevated, right,weight shift assistance provided,other (see comments)";
            bodypos2 = Exists("", "1223657", "", "", reslist);
            reslist = "log-rolled";
            bodypos3 = Exists("", "1223657", "", "", reslist);
            Program.VerboseAudit("bodypos: 1=" + bodypos1 + " 2=" + bodypos2 + " 3=" + bodypos3);

            mob3 = brad3 || edm3 || bodypos3;
            mob2 = brad2 || edm2 || bodypos2;
            mob1 = brad1 || edm1 || bodypos1;
            Program.VerboseAudit("OVERALL MOB: 1=" + mob1 + " 2=" + mob2 + " 3=" + mob3);

            //main toileting
            bool toil2 = false;
            reslist = "bed pad changed,bedpan provided,incontinence pad changed,up to bedside commode,up to toilet,urinal provided,urinal within reach,other (see comments)";
            toil2 = Exists("", "12038800", "", "", reslist);
            toil2 |= Exists("", "12046782,12062954,12046765", "", "", "");
            Program.VerboseAudit("OVERALL TOIL: 2=" + toil2);

            //main dressing
            reslist = "0";
            bool dress1 = Exists("", "1222814", "", "", reslist);
            reslist = "1,2";
            bool dress2 = Exists("", "1222814", "", "", reslist);
            reslist = "3,4";
            bool dress3 = Exists("", "1222814", "", "", reslist);
            Program.VerboseAudit("OVERALL DRESS: 1=" + dress1 + " 2=" + dress2 + " 3=" + dress3);


            if ((feed3 ? 1 : 0) + (mainhyg3 ? 1 : 0) + (mob3 ? 1 : 0) + (dress3 ? 1 : 0) >= 3)
                SetInd(3, "At least 3 ADL categories found at the Highest level.");
            else if ((feed2 || feed3 ? 1 : 0) + (mainhyg2 || mainhyg3 ? 1 : 0) + (mob2 || mob3 ? 1 : 0) + (toil2 ? 1 : 0) + (dress2 || dress3 ? 1 : 0) >= 1)
                SetInd(2, "At least 1 ADL category found at the Partial or Higher level.");
            else if (feed1 || mainhyg1 || mob1 || dress1)
                SetInd(1, "At least 1 ADL category found at the Independent level.");

            if (!_inds[1].is_checked && !_inds[2].is_checked && !_inds[3].is_checked)
            {
                SetInd(1, "Defaulting to ADL Self.  No documentation supporting higher than independent.");
            }

        }

        private void CheckLDA(string LDA_type, string startdt_id, string starttm_id, string enddt_id, string endtm_id)
        {
            DateTime uvc_start;
            DateTime uvc_end;
            string linenum;
            string placecodeline;
            string removecodedateline;
            string removecodetimeline;
            LDA_type = LDA_type.ToUpper();
            //Placement of UVC
            //3042394421 date
            //3042394422 time
            //900700 date
            //900701 time

            //Removal of UVC
            //3042394424
            //3042394425
            //900702
            //900703
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);    // add custom time range below
            query = query.Where(e => e.CODE.StartsWith(startdt_id));
            if (LDA_type == "URINARY")
                query = query.Where(e => e.DESCRIPTION.ToUpper().Contains(LDA_type) || e.DESCRIPTION.ToUpper().Contains("FECAL"));
            else if (LDA_type == "GI TUBE")
                query = query.Where(e => e.DESCRIPTION.ToUpper().Contains(LDA_type) || e.DESCRIPTION.ToUpper().Contains("GJ TUBE"));
            else
                query = query.Where(e => e.DESCRIPTION.ToUpper().Contains(LDA_type));
            query = query.Where(e => e.ORDER_STATUS == null || e.ORDER_STATUS == "");
            query = query.OrderBy(e => e.EVENT_DATETIME);
            if (query.Count() == 0) return;
            foreach (var item in query)
            { //we now have the date code for this line
                // get the line number for this LDA
                linenum = item.CODE.Substring(item.CODE.IndexOf("&") + 1);
                Program.VerboseAudit("code=" + item.CODE + "  linenum of " + LDA_type + "=" + linenum);
                // assemble the parts for the time code
                placecodeline = starttm_id + "&" + linenum;
                Program.VerboseAudit("placecodeline=" + placecodeline);
                var q2 = StartNewQuery(SearchDepth.SearchSinceAdmission);
                q2 = q2.Where(e => e.CODE == placecodeline);
                q2 = q2.Where(e => e.EVENT_DATETIME == item.EVENT_DATETIME);
                Program.VerboseAudit("placecodeline q2 code=" + placecodeline + " evdt=" + item.EVENT_DATETIME);
                Program.VerboseAudit("placecodeline q2 count=" + q2.Count());
                if (q2.Count() == 0)
                    uvc_start = PFSUtility.ISOToDateTime(item.RESULT + "0000");
                else
                {
                    foreach (var i2 in q2)
                    {
                        Program.VerboseAudit("Date " + item.RESULT + " Time=" + i2.RESULT.Substring(0, 4));
                        uvc_start = PFSUtility.ISOToDateTime(item.RESULT + i2.RESULT.Substring(0, 4));
                        Program.VerboseAudit("LDA " + i2.DESCRIPTION + " start=" + uvc_start.ToString());
                        removecodedateline = enddt_id + "&" + linenum;
                        var qremove = StartNewQuery(SearchDepth.SearchSinceAdmission);
                        qremove = qremove.Where(e => e.CODE == removecodedateline);
                        if (qremove.Count() == 0)
                        {
                            //if (LDA_type == "UVC")
                            //{
                            //    SetInd(12, "LDA " + i2.DESCRIPTION + " placed: " + uvc_start.ToString());
                            //    SetInd(19, "LDA " + i2.DESCRIPTION + " placed: " + uvc_start.ToString());
                            //}
                            if (LDA_type == "Urinary".ToUpper())
                            {
                                g_toi4 = true;
                            }
                            if (LDA_type == "GI Tube".ToUpper())
                            {
                                g_gitube = true;
                            }
                        }
                        else
                        {
                            foreach (var r1 in qremove)
                            {
                                removecodetimeline = endtm_id + "&" + linenum;
                                var qremove2 = StartNewQuery(SearchDepth.SearchSinceAdmission);
                                qremove2 = qremove2.Where(e => e.CODE == removecodetimeline);
                                if (qremove2.Count() == 0)
                                {
                                    uvc_end = PFSUtility.ISOToDateTime(r1.RESULT + "0000");
                                    if (_pat.pull_start >= uvc_end)
                                        DisableLDA(item.CODE);
                                    else if (_pat.pull_start >= uvc_start)
                                    {
                                        //if (LDA_type == "UVC")
                                        //{
                                        //    SetInd(12, "LDA " + r1.DESCRIPTION + " placed: " + uvc_start.ToString());
                                        //    SetInd(19, "LDA " + r1.DESCRIPTION + " placed: " + uvc_start.ToString());
                                        //}
                                        if (LDA_type == "Urinary".ToUpper())
                                        {
                                            g_toi4 = true;
                                        }
                                        if (LDA_type == "GI Tube".ToUpper())
                                        {
                                            g_gitube = true;
                                        }
                                    }
                                }
                                else
                                {
                                    foreach (var r2 in qremove2)
                                    {
                                        uvc_end = PFSUtility.ISOToDateTime(r1.RESULT + r2.RESULT.Substring(0, 4));
                                        Program.VerboseAudit("LDA " + LDA_type + " removal=" + uvc_end.ToString());
                                        if (_pat.pull_start >= uvc_end)
                                            DisableLDA(item.CODE);
                                        else if (_pat.pull_start >= uvc_start)
                                        {
                                            //if (LDA_type == "UVC")
                                            //{
                                            //    SetInd(12, "LDA " + r2.DESCRIPTION + " placed: " + uvc_start.ToString());
                                            //    SetInd(19, "LDA " + r2.DESCRIPTION + " placed: " + uvc_start.ToString());
                                            //}
                                            if (LDA_type == "Urinary".ToUpper())
                                            {
                                                g_toi4 = true;
                                            }
                                            if (LDA_type == "GI Tube".ToUpper())
                                            {
                                                g_gitube = true;
                                            }
                                        }

                                    }
                                }

                            }
                        }
                    }
                }
            }
            return;
        }

        private void DisableLDA(string exact_code)
        {
            var db = PFSDBUtility.NewSqlConnection();
            string q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and code='" + exact_code + "'";
            SqlCommand cmd = new SqlCommand(q, db);
            cmd.ExecuteNonQuery();
            db.Close();

        }

        //private bool GetNWOrder(string CAordcode, DateTime CAts, DateTime CAevdt)
        //{ //Get NW order that wasn't saved because there was a previous CA order at same time.
        //    string sql;
        //    bool ret = false;
        //    //string room;
        //    //string dt;
        //    //bool readyforEDclass = true;
        //    //int car;
        //    //DateTime minlocdt = DateTime.MaxValue;

        //    string CAevdtISO = PFSUtility.DateTimeToISODateTime(CAevdt);

        //    sql = "select timestamp,";
        //    sql += " case when CHARINDEX('ORC|NW', source_text) > 0 then";
        //    sql += " substring(source_text, CHARINDEX('ORC|', source_text), 32) else null end as ORC,";

        //    sql += " case when CHARINDEX('OBR|', source_text) > 0 and CHARINDEX('|" + CAevdtISO + "', source_text) > 0 then";
        //    sql += " substring(source_text, CHARINDEX('OBR|', source_text), 200) else null end as OBR";

        //    sql += " from EVENT_LOG where TIMESTAMP between dateadd(minute,-10," + PFSDBUtility.SQLDateTime(CAts) + ") and dateadd(minute, 10," + PFSDBUtility.SQLDateTime(CAts) + ")";
        //    sql += " and (description like 'O01%') and description like '%" + _pat.acct + "%'";
        //    sql += " and event_source = 1 and event_type = 1 and event_category = 4";//NW item was not rejected
        //    Program.VerboseAudit(sql);
        //    var db2 = PFSDBUtility.NewSqlConnection();
        //    var cmd2 = new SqlCommand(sql, db2);
        //    SqlDataReader dr2 = cmd2.ExecuteReader(System.Data.CommandBehavior.CloseConnection);

        //    string orcstr, obrstr, timestr;
        //    DateTime evdt = DateTime.MinValue;
        //    int seq = 0;
        //    while (dr2.Read())
        //    {
        //        orcstr = PFSDBUtility.DBToString(dr2["ORC"]);
        //        obrstr = PFSDBUtility.DBToString(dr2["OBR"]);
        //        if (orcstr.Trim() != "" && orcstr != null)
        //        {
        //            //ORC|NW|669769268^EPC||3290947318
        //            //OBR|1|669769268^EPC||40517^DIET^APEAP^^DIET||202004180752|202004180800

        //            Program.VerboseAudit("Searching for unsaved order code: " + CAordcode + " at time " + CAevdt);
        //            var arrorc = Program.SplitOnPipeAndPrepareElements(orcstr);
        //            var arrobr = Program.SplitOnPipeAndPrepareElements(obrstr);
        //            string ordid = "";
        //            string ordctrl = "";
        //            string ordcode = "";
        //            string orddesc = "";

        //            if (arrorc.GetUpperBound(0) >= 3 && arrobr.GetUpperBound(0) >= 4)
        //            {
        //                Program.VerboseAudit("Unsaved NW order ORC: " + orcstr);
        //                Program.VerboseAudit("Unsaved NW order OBR: " + obrstr);

        //                ordctrl = arrorc[1];
        //                Program.VerboseAudit("ordctrl: " + ordctrl);
        //                ordid = arrorc[2];
        //                //VerboseAudit("ordid: " + ordid);
        //                int ordidpos = ordid.IndexOf("^");
        //                if (ordidpos > 0) ordid = ordid.Substring(0, ordidpos);
        //                //VerboseAudit("ordidfinal: " + ordid);

        //                ordcode = arrobr[4];
        //                //VerboseAudit("ordcode: " + ordcode);
        //                int ordcodepos = ordcode.IndexOf("^");
        //                if (ordcodepos > 0)
        //                {
        //                    orddesc = ordcode.Substring(ordcodepos + 1);
        //                    ordcode = ordcode.Substring(0, ordcodepos);
        //                }
        //                //VerboseAudit("ordcodefinal: " + ordcode);

        //                if (ordcode.Trim() == CAordcode.Trim())
        //                {
        //                    ret = true;
        //                    evdt = CAevdt.AddMinutes(-1);
        //                    Program.VerboseAudit("Adding: orderid=" + ordid + " ctrl=" + ordctrl + " code=" + ordcode + " evdt=" + evdt + " desc=" + orddesc);
        //                    if (arrorc.GetUpperBound(0) >= 2)
        //                    {
        //                        using (var db = PFSDBUtility.NewSqlConnection())
        //                        {
        //                            seq++;
        //                            //evdt = PFSUtility.ISOToDateTime(timestr);
        //                            Program.VerboseAudit("Evdt=" + evdt.ToString());
        //                            string q = "INSERT INTO chart_item (encounter_id,event_datetime,code,description,timestamp,sequence,unit_id,order_id,order_control)";
        //                            q += " select @encid, @evdt, @code, @desc, @ts, @seq, @unit, @oid, @octrl";
        //                            q += " where not exists (select encounter_id,code,event_datetime,unit_id,sequence from chart_item";
        //                            q += " where encounter_id=" + _pat.encounter_id.ToString() + " and code='" + ordcode + "' and event_datetime='" + evdt.ToString() + "' and unit_id=-1 and sequence=" + seq + ")";
        //                            //string q = "INSERT INTO chart_item (encounter_id,event_datetime,code,description,timestamp,sequence,unit_id,order_id) values (@encid, @evdt, @code, @desc, @ts,@seq,@unit,@oid)";
        //                            SqlCommand cmd = new SqlCommand(q, db);
        //                            cmd.Parameters.AddWithValue("@encid", _pat.encounter_id);
        //                            cmd.Parameters.AddWithValue("@evdt", evdt);
        //                            cmd.Parameters.AddWithValue("@code", ordcode);
        //                            cmd.Parameters.AddWithValue("@desc", orddesc);
        //                            cmd.Parameters.AddWithValue("@ts", DateTime.Now);
        //                            cmd.Parameters.AddWithValue("@seq", seq);
        //                            cmd.Parameters.AddWithValue("@unit", -1);
        //                            cmd.Parameters.AddWithValue("@oid", ordid);
        //                            cmd.Parameters.AddWithValue("@octrl", ordctrl);
        //                            cmd.ExecuteNonQuery();
        //                        }

        //                    }
        //                }// if codes match: add chart item 1 minute earlier
        //            } // if arrays upper bounds are large enough
        //        } // if the orc and obr strings are not null
        //    } //dr read
        //    //dr2.Close();
        //    db2.Close();
        //    return ret;
        //}


        //private bool OrderInProgress(string code, out string found_what)
        //{
        //    bool ret = false;
        //    found_what = "";
        //    //look for latest code with NW and order_status != 'x'
        //    var db = PFSDBUtility.NewPfsDataContext();
        //    var query = from item in db.CHART_ITEMs
        //                where (item.ENCOUNTER_ID == _pat.encounter_id)
        //                where (item.CODE == code)
        //                where (item.EVENT_DATETIME < DateTime.Now)
        //                where (item.ORDER_CONTROL.ToUpper() == "NW")
        //                where (item.ORDER_STATUS == null || item.ORDER_STATUS == "")
        //                orderby item.EVENT_DATETIME descending
        //                select item;
        //    foreach (var itemA in query)
        //    {
        //        //Program.VerboseAudit("Order ID=" + itemA.ORDER_ID + ": " + itemA.DESCRIPTION + " result=" + itemA.RESULT + " range:" + itemA.EVENT_DATETIME.ToString());
        //        var db2 = PFSDBUtility.NewPfsDataContext();
        //        var query2 = from item2 in db2.CHART_ITEMs
        //                     where (item2.CODE == code)
        //                     where (item2.ORDER_ID == itemA.ORDER_ID)
        //                     where (item2.EVENT_DATETIME >= itemA.EVENT_DATETIME)
        //                     where (item2.ORDER_CONTROL.ToUpper() == "CA")
        //                     select item2;
        //        int ct = query2.Count();
        //        if (ct > 0)
        //        {
        //            foreach (var x in query2)
        //            {
        //                if (x.EVENT_DATETIME >= _pat.pull_start)
        //                {
        //                    ret = true;
        //                    found_what = "Order ID=" + itemA.ORDER_ID + ": " + itemA.DESCRIPTION + " result=" + itemA.RESULT + " range:" + itemA.EVENT_DATETIME.ToString() + " - " + x.EVENT_DATETIME.ToString();
        //                }
        //                else
        //                {
        //                    DisableOrder(x.ORDER_ID);
        //                }
        //            }
        //        }
        //        else
        //        {
        //            ret = true;
        //            found_what += "Order ID=" + itemA.ORDER_ID + ": " + itemA.DESCRIPTION + " result=" + itemA.RESULT + " range:" + itemA.EVENT_DATETIME.ToString() + " - open";
        //        }

        //    }
        //    return ret;
        //}

        private bool OrderInProgressByCode(string code, out string found_what)
        {
            bool ret = false;
            code = code.ToLower();
            found_what = "";
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = query.Where(e => e.CODE.ToLower() == code
                                  && e.EVENT_DATETIME < loc_out
                                  && e.ORDER_CONTROL.ToLower() == "nw"
                                  && e.ORDER_STATUS.ToLower() == "ip");
            query = query.OrderByDescending(e => e.EVENT_DATETIME);

            int count = query.Count();
            Program.VerboseAudit("Order in progress: count=" + count + " since:" + loc_out);

            foreach (var itemA in query)
            {
                var query2 = StartNewQuery(SearchDepth.SearchSinceAdmission);
                query2 = query2.Where(e => e.CODE.ToLower() == code);
                query2 = query2.Where(e => e.ORDER_ID == itemA.ORDER_ID);
                query2 = query2.Where(e => e.ORDER_CONTROL.ToLower() == "ca");
                int ct2 = query2.Count();

                if (ct2 > 0)
                {
                    Program.VerboseAudit("Order in progress: order_id=" + itemA.ORDER_ID + " _pat.pull_start=" + _pat.pull_start + " ct2=" + ct2);
                    //                  Program.VerboseAudit("order in progress: q2ct=" + ct2);
                    foreach (var x in query2)
                    {
                        Program.VerboseAudit("x.event_datetime=" + x.EVENT_DATETIME);
                        if (x.EVENT_DATETIME >= _pat.pull_start)
                        {
                            ret = true;
                            found_what += "Order ID=" + itemA.ORDER_ID + ": " + itemA.DESCRIPTION + "; range start-end:" + itemA.EVENT_DATETIME.ToString() + " - " + x.EVENT_DATETIME.ToString();
                        }
                        else
                        {
                            DisableOrder(x.ORDER_ID);
                        }
                    }
                }
                else
                {
                    ret = true;
                    found_what = "Order ID=" + itemA.ORDER_ID + ": " + itemA.DESCRIPTION + "; result=" + itemA.RESULT + "; starting:" + itemA.EVENT_DATETIME.ToString();
                    //Program.VerboseAudit("AUDIT ORDER:" + found_what);
                }
            }
            return ret;
        }
        //private bool OrderInProgress(string code, out string found_what)
        //{
        //    bool ret = false;
        //    found_what = "";
        //    var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
        //    query = query.Where(e => e.CODE.ToUpper() == code
        //                          && e.EVENT_DATETIME < loc_out
        //                          && e.ORDER_CONTROL.ToLower() == "nw"
        //                          && (e.ORDER_STATUS == "" || e.ORDER_STATUS == null));
        //    query = query.OrderByDescending(e => e.EVENT_DATETIME);

        //    int count = query.Count();
        //    //Program.VerboseAudit("count order in progress=" + count);

        //    foreach (var itemA in query)
        //    {
        //        var query2 = StartNewQuery(SearchDepth.SearchSinceAdmission);
        //        query2 = query2.Where(e => e.CODE.ToUpper() == code);
        //        query2 = query2.Where(e => e.ORDER_ID == itemA.ORDER_ID);
        //        query2 = query2.Where(e => e.ORDER_CONTROL.ToLower() == "ca");
        //        int ct2 = query2.Count();

        //        if (ct2 > 0)
        //        {
        //            Program.VerboseAudit("order in progress: order_id=" + itemA.ORDER_ID + " _pat.pull_start=" + _pat.pull_start + " ct2=" + ct2);
        //            //                  Program.VerboseAudit("order in progress: q2ct=" + ct2);
        //            foreach (var x in query2)
        //            {
        //                Program.VerboseAudit("x.event_datetime=" + x.EVENT_DATETIME);
        //                if (x.EVENT_DATETIME >= _pat.pull_start)
        //                {
        //                    ret = true;
        //                    found_what = "Order ID=" + itemA.ORDER_ID + ": " + itemA.DESCRIPTION + "; result=" + itemA.RESULT + "; range:" + PFSUtility.DateTimeToISODateTime(itemA.EVENT_DATETIME) + " - " + PFSUtility.DateTimeToISODateTime(x.EVENT_DATETIME);
        //                }
        //                else
        //                {
        //                    DisableOrder(x.ORDER_ID);
        //                }
        //                ret |= GetNWOrder(code, x.TIMESTAMP, x.EVENT_DATETIME);
        //            }
        //        }
        //        else
        //        {
        //            ret = true;
        //            found_what = "Order ID=" + itemA.ORDER_ID + ": " + itemA.DESCRIPTION + "; result=" + itemA.RESULT + "; starting:" + PFSUtility.DateTimeToISODateTime(itemA.EVENT_DATETIME);
        //            //Program.VerboseAudit("AUDIT ORDER:" + found_what);
        //        }
        //    }


        //    return ret;
        //}

        private void DisableOrder(string ordid)
        {
            //update ORDER_STATUS = 'X' for _pat.encounter_id and ordid
            var db = PFSDBUtility.NewSqlConnection();
            string q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and order_id='" + ordid + "'";
            SqlCommand cmd = new SqlCommand(q, db);
            cmd.ExecuteNonQuery();
            db.Close();
        }

        //private bool RestraintOrderInProgress(string code, out string found_what)
        //{
        //    bool ret = false;
        //    found_what = "";
        //    var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
        //    query = query.Where(e => e.CODE.ToUpper() == code
        //                          && e.EVENT_DATETIME < loc_out
        //                          && e.ORDER_CONTROL.ToLower() == "nw"
        //                          && (e.ORDER_STATUS == "" || e.ORDER_STATUS == null));
        //    query = query.OrderByDescending(e => e.EVENT_DATETIME);

        //    int count = query.Count();
        //    //Program.VerboseAudit("count order in progress=" + count);

        //    foreach (var itemA in query)
        //    {
        //        var query2 = StartNewQuery(SearchDepth.SearchSinceAdmission);
        //        query2 = query2.Where(e => e.CODE.ToUpper() == code);
        //        query2 = query2.Where(e => e.ORDER_ID == itemA.ORDER_ID);
        //        query2 = query2.Where(e => e.ORDER_CONTROL.ToLower() == "ca");
        //        int ct2 = query2.Count();

        //        if (ct2 > 0)
        //        {
        //            Program.VerboseAudit("order in progress: order_id=" + itemA.ORDER_ID + " _pat.pull_start=" + _pat.pull_start + " ct2=" + ct2);
        //            //                  Program.VerboseAudit("order in progress: q2ct=" + ct2);
        //            foreach (var x in query2)
        //            {
        //                Program.VerboseAudit("x.event_datetime=" + x.EVENT_DATETIME);
        //                if (x.EVENT_DATETIME >= _pat.pull_start)
        //                {
        //                    ret = true;
        //                    found_what = "Order ID=" + itemA.ORDER_ID + ": " + itemA.DESCRIPTION + "; result=" + itemA.RESULT + "; range:" + itemA.EVENT_DATETIME.ToString() + " - " + x.EVENT_DATETIME.ToString();
        //                }
        //                else
        //                {
        //                    DisableOrder(x.ORDER_ID);
        //                }
        //                ret |= GetNWOrder(code, x.TIMESTAMP, x.EVENT_DATETIME);
        //            }
        //        }
        //        else
        //        {
        //            if (itemA.EVENT_DATETIME.AddHours(2) > _pat.pull_start)
        //            {
        //                ret = true;
        //                found_what = "Order ID=" + itemA.ORDER_ID + ": " + itemA.DESCRIPTION + "; result=" + itemA.RESULT + "; starting:" + itemA.EVENT_DATETIME.ToString() + "; Set to Auto-Expire at:" + itemA.EVENT_DATETIME.AddHours(2);
        //            }
        //            else
        //                DisableOrder(itemA.ORDER_ID);
        //            //Program.VerboseAudit("AUDIT ORDER:" + found_what);
        //        }
        //    }


        //    return ret;
        //}

        //private bool Check_b79()
        //        {
        //            bool b = false;

        //            var query = StartNewQuery(SearchDepth.SearchSince13Hrs);
        //            query = query.Where(e => e.CODE.StartsWith("MED"));
        //            query = query.Where(e => e.DESCRIPTION.ToUpper().Contains("PEG3350 100 GRAM-SOD SUL") && e.DESCRIPTION.ToLower().Contains(";;;given"));
        //            b = (query.Count() > 0);
        //            return b;
        //        }

        private void Check_4()
        {
            string reslist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 4. ADL Supervision");
            Program.VerboseAudit("---------------");
            exclude_periop_data = true;

            if (_pat.age < 6.0) SetInd(4, "Age < 6 years");


            reslist = "minimum assist,moderate assist,maximum assist,dependent";
            SetIndIfResultContains(4, "", "12026547", "", "", reslist);
            reslist = "assistance";
            SetIndIfResultContains(4, "", "12026223", "", "", reslist);
            reslist = "positioned with 1 assist,positioned with 2 assist,positioned with 3 or more assist";
            SetIndIfResultContains(4, "", "12041232", "", "", reslist);
            reslist = "Use own body wash,Use own deodorant with supervision,Use own shampoo,Use own toothpaste/brush,Hairbrush with supervision";
            SetIndIfResultContains(4, "", "2251549", "", "", reslist);

        }


        private void Check_5_6()
        {
            string reslist;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 5. Cognitive Support");
            Program.VerboseAudit("MH 6. Cognitive Support q1H");
            Program.VerboseAudit("---------------");
            exclude_periop_data = true;


            reslist = "Yes";
            SetIndIfResultContains(5, "", "12023492", "", "", reslist);

            reslist = "12";
            SetIndIfResultContains(5, "", "12041161", "", "", reslist);
            reslist = "13,14";
            SetIndIfResultContains(6, "", "12041161", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(5, "", "12025513", "", "", reslist);

            reslist = "confused,lethargic,obtunded,somnolent";
            SetIndIfResultContains(5, "", "1222528", "", "", reslist);
            reslist = "sedated,semicomatose,unresponsive";
            SetIndIfResultContains(6, "", "1222528", "", "", reslist);

            reslist = "disoriented to,disoriented x 4";
            SetIndIfResultContains(5, "", "1222529", "", "", reslist);

            reslist = "score >/=9";
            SetIndIfResultBetween(5, "", "12025525", "", "", 9, 99, SearchDepth.SearchDefault);

            reslist = "score >/=5";
            SetIndIfResultBetween(5, "", "12043105", "", "", 5, 99, SearchDepth.SearchDefault);

            reslist = "reorientation provided";
            List<gBucket> buckets = new List<gBucket>();
            AddBuckets(buckets, "", "1223696", "", "", reslist);
            bool trigger = AnalyzeBuckets(buckets, 20, 60, "", false);
            if (trigger)
                SetInd(6, "Re-orientation charting at freq of q1H");
            else
                SetIndIfResultContains(5, "", "1223696", "", "", reslist);


            reslist = "short-term loss,long-term loss,forgetful,new learning, recall loss";
            SetIndIfResultContains(5, "", "1222531", "", "", reslist);

            reslist = "auditory,command,gustatory,olfactory,tactile,visual,denies hallucinations,other (see comments)";
            SetIndIfResultContains(5, "", "12025595", "", "", reslist);

            reslist = "depersonalization,derealization,illusions,consistent with reality,other (see comments)";
            SetIndIfResultContains(5, "", "12050806", "", "", reslist);

            reslist = "control,grandeur,obsessions,paranoid,persecutory,referential,religiosity,sexual,somatic,no delusions,other (see comments)";
            SetIndIfResultContains(5, "", "12025598", "", "", reslist);

            reslist = "aggressive,clang association,compulsion,denial,helplessness,homicidal,hopelessness,impoverished,ideas of reference,obsession,perseveration,phobia,poverty of content,preoccupation,somatic concerns,suicidal thoughts,suspiciousness,violent thoughts,worthlessness,logical,relevant,other (see comments) ";
            SetIndIfResultContains(5, "", "12025600", "", "", reslist);

            reslist = "circumstantial thought,disorganized,flight of ideas,illogical,loose associations,neologisms,tangential,thought blocking,thought broadcasting,thought insertion,word salad,logical,linear thought process,relevant,other (see comments)";
            SetIndIfResultContains(5, "", "1002233", "", "", reslist);

            reslist = "disoriented to person,disoriented to place,disoriented to situation,disoriented to time,fabricates experiences to compensate for memory loss,fabricates experiences to compensate for memory loss,impaired concentration,memory deficit, immediate,memory deficit, recent,memory deficit, remote,mind wandering,repetition of questions,unable to follow commands,unable to learn new information,unable to perform tasks,unable to understand directions,unable to understand explanation/reasoning,able to comprehend,immediate";
            SetIndIfResultContains(5, "", "12025602", "", "", reslist);


        }
        //private bool AllOriented(string code1)
        //{
        //    int ct = 0;

        //    var query = StartNewQuery(SearchDepth.SearchSince24Hrs);    // add custom time range below
        //    query = query.Where(e => e.CODE.StartsWith(code1));
        //    query = query.Where(e => (e.RESULT.ToLower().StartsWith("oriented to person") || e.RESULT.ToLower().Contains(";oriented to person"))
        //    && (e.RESULT.ToLower().StartsWith("oriented to place") || e.RESULT.ToLower().Contains(";oriented to place"))
        //    && (e.RESULT.ToLower().StartsWith("oriented to time") || e.RESULT.ToLower().Contains(";oriented to time")));
        //    ct = query.Count();
        //    if (ct > 0)
        //        Program.VerboseAudit("All 3 Orientation found: " + query.Count());
        //    return (ct > 0);
        //}

        private void Check_7_8_9()
        {
            string reslist;
            string[] mhunits = { "BLK 11","West 5 BehavHlt","NSM Ad Epstein 5","NSM Ad Epstein 4","NSM Sr Epstein 3",
            "NWH 3EU3","2 SOUTH" ,"NSM Ch Epstein 2"};

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 7. Safety Management q30m");
            Program.VerboseAudit("MH 8. Safety Management q15m");
            Program.VerboseAudit("MH 9. Safety Management q5m");
            Program.VerboseAudit("---------------");
            exclude_periop_data = false;

            

            string desc_found = "";
            string[] ordercode_str = {"351500","351023"};
            foreach (string ordstr in ordercode_str)
            {
                if (OrderInProgressByCode(ordstr, out desc_found))
                    SetInd(8, "Order in effect: " + desc_found);
            }

            SetIndIfResultContains(8, "", "12062302", "", "", "");
            SetIndIfResultContains(8, "", "12062301", "", "", "");
            SetIndIfResultContains(8, "", "12062300", "", "", "");

            reslist = "appropriate behavior reinforced,diversional activity encouraged";
            reslist += ",family involvement requested,increased round frequency";
            reslist += ",medication administered,medication offered,physical activity promoted";
            reslist += ",quiet time facilitated,reoriented,stimulation decreased";
            reslist += ",time out facilitated,verbally redirected,other (see comments)";
            SetIndIfResultContains(8, "", "12040611", "", "", reslist);

            reslist = "1:1 observation initiated";
            SetIndIfResultContains(9, "", "12040611", "", "", reslist);
            
            SetIndIfResultContains(7, "", "12062446", "", "", "no assistance");
            SetIndIfResultContains(8, "", "12062446", "", "", "with assistance");
        }


        private void UpdatePtChartArrays(string orderid, string ordstatus, string newres)
        {
            foreach (var item in _chart_items_since24hrs)
            {
                if (item.ORDER_ID == orderid)
                {
                    if (ordstatus != "")
                        item.ORDER_STATUS = ordstatus;
                    if (newres != "")
                        item.RESULT = newres;
                }
            }
        }


        //private int CheckSafety()
        //{
        //    int ind = 0, ct = 0;
        //    string[] safety_list = { "q30", "q15", "q5", "Line of sight", "Continuous observation by RN with patient", "Continuous observation by non-RN staff with patient", "Continuous observation by two staff with patient" };

        //    var query = StartNewQuery(SearchDepth.SearchSince24Hrs);
        //    query = query.Where(e => e.CODE.StartsWith("9993040009234"));
        //    query = query.Where(e => safety_list.Any(item => e.RESULT.ToLower().StartsWith(item.ToLower())));
        //    query = query.OrderByDescending(e => e.EVENT_DATETIME);
        //    ct = query.Count();
        //    if (ct > 0)
        //    {
        //        string res = query.First().RESULT;
        //        ind = 9;
        //        if (res.ToLower().StartsWith("q30")) ind = 7;
        //        if (res.ToLower().StartsWith("q15")) ind = 8;
        //        SetInd(ind, "Latest result is = " + res);
        //    }
        //    return ind;
        //}

        private void AddIntoSearch(string interv)
        {
            if (psyinterv_filter == "")
                psyinterv_filter = " description like '" + interv + "%'";
            else
                psyinterv_filter += " or description like '" + interv + "%'";
        }

        private void Check_10_11_12_13_14()
        {
            string reslist;
            //int ct;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 10. Behavior/Emotional Management");
            Program.VerboseAudit("MH 11. Behavior/Emotional Mgmt-q4H");
            Program.VerboseAudit("MH 12. Behavior/Emotional Mgmt-q2H");
            Program.VerboseAudit("MH 13. Behavior/Emotional Mgmt-q1H");
            Program.VerboseAudit("MH 14. Behavior/Emotional Mgmt-q30m");
            Program.VerboseAudit("---------------");
            exclude_periop_data = false;

            //if (_pat.unit_name.ToUpper() == "MENTAL HEALTH")
            //    SetInd(10, "Defaulting for Mental Health unit.");

            reslist = "inappropriately goal directed,insight not appropriate to situation,judgment not appropriate to situation";
            SetIndIfResultContains(10, "", "12025599", "", "", reslist);
            reslist = "blaming others,denies responsibility";
            SetIndIfResultContains(11, "", "12025599", "", "", reslist);
            reslist = "inappropriately focused on discharge";
            SetIndIfResultContains(13, "", "12025599", "", "", reslist);

            reslist = "flat affect,hypoactive,sad,withdrawn,other (see comments)";
            SetIndIfResultContains(10, "", "12211040", "", "", reslist);
            reslist = "anxious,excitable,labile";
            SetIndIfResultContains(11, "", "12211040", "", "", reslist);
            reslist = "restless,uncooperative";
            SetIndIfResultContains(12, "", "12211040", "", "", reslist);
            reslist = "angry,hyperactive";
            SetIndIfResultContains(13, "", "12211040", "", "", reslist);
            reslist = "agitated,combative,sexually inappropriate,threatening";
            SetIndIfResultContains(14, "", "12211040", "", "", reslist);

            reslist = "avoids social contact,eye contact darting,eye contact, hesitant to make,eye contact intense,eye contact intermittent,fearful,indifferent,other (see comments)";
            SetIndIfResultContains(10, "", "12025588", "", "", reslist);
            reslist = "guarded,suspicious";
            SetIndIfResultContains(11, "", "12025588", "", "", reslist);
            reslist = "irrational,mistrustful,poor boundaries,regressive,uncooperative";
            SetIndIfResultContains(12, "", "12025588", "", "", reslist);
            reslist = "argumentative,asks for medication frequently,demanding,frequently seeks out staff,responding to internal stimuli";
            SetIndIfResultContains(13, "", "12025588", "", "", reslist);
            reslist = "assaultive to others,destructive to property,elopement behaviors,hostile,self-harm,threatening,verbally abusive,violent";
            SetIndIfResultContains(14, "", "12025588", "", "", reslist);

            reslist = "disbelief,fear,grief,loneliness,relief,sadness,other (see comments)";
            SetIndIfResultContains(10, "", "1222805", "", "", reslist);
            reslist = "anxiety,depression,frustration,suicidal thoughts";
            SetIndIfResultContains(11, "", "1222805", "", "", reslist);
            reslist = "guilt,hopelessness,powerlessness";
            SetIndIfResultContains(12, "", "1222805", "", "", reslist);
            reslist = "anger";
            SetIndIfResultContains(14, "", "1222805", "", "", reslist);

            reslist = "agrees with comment";
            SetIndIfResultContains(10, "", "12040717", "", "", reslist);
            reslist = "disagrees";
            SetIndIfResultContains(11, "", "12040717", "", "", reslist);
            reslist = "refuses to participate";
            SetIndIfResultContains(12, "", "12040717", "", "", reslist);
            reslist = "unable to participate";
            SetIndIfResultContains(13, "", "12040717", "", "", reslist);

            reslist = "caregiver stress acknowledged";
            SetIndIfResultContains(10, "", "12025465", "", "", reslist);

            reslist = "care explained,choices provided,emotional support provided,empathic listening provided,questions answered,questions encouraged,reassurance provided,thoughts/feelings acknowledged,other (see comments)";
            SetIndIfResultContains(10, "", "12025449", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "12025459", "", "", reslist);

            reslist = "day-dreamy,expansive,facial expression incongruent,pessimistic,sad,somber,fearful,melancholic,calm,euthymic,facial expression relaxed,appropriate to situation,other";
            SetIndIfResultContains(10, "", "12025591", "", "", reslist);
            reslist = "anxious,depressed,hopeless";
            SetIndIfResultContains(11, "", "12025591", "", "", reslist);
            reslist = "distrustful/suspicious,tense,irritable";
            SetIndIfResultContains(12, "", "12025591", "", "", reslist);
            reslist = "angry,elevated,euphoric";
            SetIndIfResultContains(13, "", "12025591", "", "", reslist);

            reslist = "yes";
            SetIndIfResultContains(14, "", "12027111", "", "", reslist);

            reslist = "yes";
            SetIndIfResultContains(12, "", "12050084", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(14, "", "12215016", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(14, "", "12050847", "", "", reslist);

            reslist = "environmental self-harm risks assessed,environment modified for self-harm risk";
            SetIndIfResultContains(10, "", "12064275", "", "", reslist);
            reslist = "observed one-to-one";
            SetIndIfResultContains(14, "", "12064275", "", "", reslist);

            reslist = "Low Risk";
            SetIndIfResultContains(10, "", "12056613", "", "", reslist);
            reslist = "Moderate Risk";
            SetIndIfResultContains(12, "", "12056613", "", "", reslist);
            reslist = "High Risk";
            SetIndIfResultContains(14, "", "12056613", "", "", reslist);

            reslist = "auditory stimulation minimized,auditory stimulation provided,care clustered,lighting decreased,music on,quiet environment promoted,tactile stimulation minimized,tactile stimulation provided,television on,visitors limited,visual stimulation minimized,visual stimulation provided,other (see comments)";
            SetIndIfResultContains(10, "", "12025528", "", "", reslist);

            reslist = "not participating in treatment";
            SetIndIfResultContains(10, "", "12039580", "", "", reslist);
            reslist = "not participating in treatment";
            SetIndIfResultContains(11, "", "12039580", "", "", reslist);
            reslist = "not participating in treatment";
            SetIndIfResultContains(12, "", "12039580", "", "", reslist);
            reslist = "disruptive to milieu";
            SetIndIfResultContains(13, "", "12039580", "", "", reslist);
            reslist = "disruptive to milieu";
            SetIndIfResultContains(14, "", "12039580", "", "", reslist);

            reslist = "not participating in treatment";
            bool not_participating = Exists("", "12039580", "", "", reslist);
            reslist = "disruptive to milieu";
            bool disruptive = Exists("", "12039580", "", "", reslist);

            if (not_participating)
            {
                reslist = "";
                SetIndIfResultContains(10, "", "12039179", "", "", reslist);
                reslist = "dysfunctional";
                SetIndIfResultContains(11, "", "12039179", "", "", reslist);
                reslist = "inappropriate";
                SetIndIfResultContains(12, "", "12039179", "", "", reslist);
            }
            if (disruptive)
            {
                reslist = " ";
                SetIndIfResultContains(13, "", "12039179", "", "", reslist);
                reslist = "dysfunctional,inappropriate";
                SetIndIfResultContains(14, "", "12039179", "", "", reslist);
            }

            
            reslist = "behavioral plan developed,other";
            SetIndIfResultContains(10, "", "1223718", "", "", reslist);
            reslist = "behavioral plan reviewed";
            SetIndIfResultContains(11, "", "1223718", "", "", reslist);
            reslist = "boundaries reinforced";
            SetIndIfResultContains(12, "", "1223718", "", "", reslist);
            reslist = "impulse control promoted";
            SetIndIfResultContains(13, "", "1223718", "", "", reslist);
            reslist = "security enhancements provided";
            SetIndIfResultContains(14, "", "1223718", "", "", reslist);

            reslist = "active listening utilized,counseling provided,decision-making supported";
            reslist += ",goal-setting facilitated,guided imagery facilitated,journaling promoted";
            reslist += ",positive reinforcement provided,problem-solving facilitated,relaxation techniques promoted";
            reslist += ",self-care encouraged,self-reflection promoted,self-responsibility promoted";
            reslist += ",verbalization of feelings encouraged,other";
            int ct = CountItems("", "12025462", "", "", reslist);
            if (ct >= 2)
                SetInd(10, "At least 2 items found for Supportive Measures: count=" + ct);

            reslist = "agitated,apprehensive,cogwheeling,compulsions,extrapyramidal symptoms";
            reslist += ",eye contact inappropriate,foot-tapping,gait unsteady,grimace,handwringing";
            reslist += ",high energy,lethargic,nail biting,notable mannerisms/gestures";
            reslist += ",pacing,posturing,psychomotor retardation,restless,rigid,sedentary";
            reslist += ",shuffling,slouched,staggering,tense,tic,tremor";
            reslist += ",erect posture,head raised,steady gait,other";
            ct = CountItems("", "12025589", "", "", reslist);
            if (ct == 1)
                SetInd(10, "Motor movement found.");
            else if (ct > 1)
                SetInd(11, "Motor movement found count=" + ct);

        }


        private int GetMaxBehavInd(DateTime start, DateTime fin, out DateTime classdt)
        {
            int ind = 0;
            classdt = DateTime.MinValue;
            //get assessment indicator at location out time = loc_out_time
            var db = PFSDBUtility.NewPfsDataContext();        // look for the unit default admission profile
            var query = from ce in db.CLASSIFICATION_EVENTs
                        from ia in db.INDICATOR_ANSWERs
                        where (ce.CLASSIFICATION_EVENT_ID == ia.CLASSIFICATION_EVENT_ID)
                        && (ce.ENCOUNTER_ID == _pat.encounter_id)
                        && (ce.CLASSIFICATION_DATETIME >= start)
                        && (ce.CLASSIFICATION_DATETIME < fin)
                        && (ia.INDICATOR_NUMBER >= 10 && ia.INDICATOR_NUMBER <= 14)
                        select new
                        {
                            ia.INDICATOR_NUMBER,
                            ce.CLASSIFICATION_DATETIME
                        };
            if (query.Count() > 0)
            {
                //ind = query.First().INDICATOR_NUMBER;
                //classdt = query.First().CLASSIFICATION_DATETIME;
                foreach (var c in query)
                {
                    if (ind < c.INDICATOR_NUMBER)
                    {
                        ind = c.INDICATOR_NUMBER;
                        classdt = c.CLASSIFICATION_DATETIME;
                    }
                }
            }
            return ind;
        }

        // (this isn't really a Q1h count -- it is just a count) Make this a q1hr count
        //private int ReturnQ1HrCount(string code_list, string result_list)
        //{
        //    var buckets = new List<gBucket>();
        //    SetBucketSize(60);
        //    AddBuckets(buckets, "", code_list, "", "", result_list);
        //    return CountBuckets(buckets);
        //}

        //private bool IsQ1Hour(int count)
        //{
        //    return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q1H);
        //}
        //private bool IsQ2Hour(int count)
        //{
        //    return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q2H);
        //}

        private void Check_15()
        {
            string reslist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 15. Behavior Prevention Plan");
            Program.VerboseAudit("---------------");
            exclude_periop_data = false;


        }

        private void Check_16_17()
        {
            string reslist;
            string codelist;
            bool found22 = false;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 16. Medication Management q4H"); //2
            Program.VerboseAudit("MH 17. Medication Management q2H"); //3 or more Psymed
            Program.VerboseAudit("---------------");
            exclude_periop_data = true;

            int[] medclass = { 938,1512,943,288,113,236,722,255,254};

            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = query.Where(e => e.CODE.ToLower().StartsWith("med"));
            query = query.Where(e =>
                medclass.Any(item1 => e.CATEGORY == item1.ToString() ||
                                      e.CATEGORY.StartsWith(item1.ToString() + ";") ||
                                      e.CATEGORY.Contains(";" + item1.ToString() + ";") ||
                                      e.CATEGORY.EndsWith(";" + item1.ToString())) 
           //because the class can be formatted as: 149 or 149;;255 or 255;;149;;331 or 244;;149
                            );
            query = query.Where(e => e.RESULT.ToLower().Contains("given"));
            //query = query.Where(e => e.RESULT.Contains("^RN"));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            int count = query.Count();
            if (count >= 2)
            {
                SetInd(17, "Psy Meds found: count of 2 or more=" + count);
            }
            else if (count == 1)
            {
                SetInd(16, "Psy Meds found: count=" + count);
            }


            //List < gBucket> buckets = new List<gBucket>();
            //AddBuckets(buckets, "", "", "NURS Symptom Assessment Reason", "Reasessment after medication intervention");
            //AddBuckets(buckets, "", "", "Nurs Symptom Intervention", "medication given as ordered");
            //AddBuckets(buckets, "", "", "NURS Pain Reason For Assessment", "Reassessment following intervention");
            //bool trigger=AnalyzeBuckets(buckets, 18, 60, "", false);
            //if (trigger)
            //    SetInd(16, "Symptoms q4H");

            //trigger=AnalyzeBuckets(buckets, 19, 60, "", false);
            //if (trigger)
            //    SetInd(17, "Symptoms q2H");

        }

        private void CheckAssessment(int count, string desc)
        {
            //if (_inds[18].is_checked) return;          //skip if highest already checked
            if (count == 0) return;                    //skip if none

            // This should work the same as the original code:
            switch (FreqForCount(_pat.los_hours, count))
            {
                case Frequencies.Q30M:
                    SetInd(18, desc);
                    break;
                case Frequencies.Q1H:
                    SetInd(17, desc);
                    break;
                case Frequencies.Q2H:
                    SetInd(16, desc);
                    break;
                case Frequencies.Q4H:
                    SetInd(15, desc);
                    break;
                default:
                    break;
            }

        }

        private void ShowBuckets(List<gBucket> buckets)
        {
            string s = "";
            foreach (var e in buckets)
            {
                s = "(" + e.bucket + "," + e.code + "," + e.evdt.ToString() + ")";
                Program.VerboseAudit("Assessmt item:=" + s);
            }
        }

        private void Check_18_19_20()
        {
            string reslist = "";
            string codelist = "";
            List<gBucket> buckets;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 18. Physiological Assessment q4H");
            Program.VerboseAudit("MH 19. Physiological Assessment q2H");
            Program.VerboseAudit("MH 20. Physiological Assessment q1H");
            Program.VerboseAudit("---------------");

            exclude_periop_data = true;

            string freqstr = "====Q4/Q2/Q1 HR EVALUATION================";
            Program.VerboseAudit(freqstr + " bucket size=60" );

            int ind = 18;
            int bucket_size = 60;
            //        Vitals:
            string assessgrouplabel = "Vitals";
            buckets = new List<gBucket>();
            codelist = EXACT_MATCH_PREFIX + "9006";
            AddBuckets(buckets, "", codelist, "",  "");
            codelist = EXACT_MATCH_PREFIX + "9005";
            AddBuckets(buckets, "", codelist, "",  "");
            codelist = EXACT_MATCH_PREFIX + "9009";
            AddBuckets(buckets, "", codelist, "",  "");
            codelist = EXACT_MATCH_PREFIX + "9008";
            AddBuckets(buckets, "", codelist, "",  "");
            codelist = EXACT_MATCH_PREFIX + "90010";
            AddBuckets(buckets, "", codelist, "",  "");
            //codelist = EXACT_MATCH_PREFIX + "90011";
            //AddBuckets(buckets, "", codelist, "",  "");
            codelist = EXACT_MATCH_PREFIX + "90014";
            AddBuckets(buckets, "", codelist, "",  "");
            codelist = "12210793,12210718,12214784,12095757,1228513,ISTAT";
            AddBuckets(buckets, "", codelist, "",  "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            //Cardiovascular:
            assessgrouplabel = "Cardiovascular";
            buckets = new List<gBucket>();
            codelist = "12025687,12037481,12046555,12023500,12050420,12025698,12025699,12025700,12025701,1222673,1224032";
            codelist += ",12057351,12025717,1223056,12051046,4003997,12049937,12050014,12048481,12050546,12051923,12051462";
            codelist += ",12025757,12025758,12025759,12025760,12025761,12025762,12025763,12025764,12025765,12025766,12025767";
            codelist += ",12025768,12025769,12025770,12025771,12025772,12025722,12025723,12025724,12025725,12025726,12025727";
            codelist += ",12025728,12025729,12025730,12025731,12025732,12025733,12025734,12025735,12025736,12025737,12025738";
            codelist += ",12025739,12025740,12025741,12025742,12025743,12025744,12025745,12025746,12025747,12025748,12025749";
            codelist += ",12025750,12025751,12046950,12061043,12025787,12025720,12025718,12025721,12025719";
            AddBuckets(buckets, "", codelist, "",  "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            //Pulmonary:
            assessgrouplabel = "Pulmonary";
            buckets = new List<gBucket>();
            codelist = "12025603,1224100,12025605,12211344,1222683,1222684,1222685,12025608,12025609,12025610,12025611";
            codelist += ",12025612,12025613,12025614,12211451,12055588,12211405";
            AddBuckets(buckets, "", codelist, "",  "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            //Neuro:
            assessgrouplabel = "Neuro";
            buckets = new List<gBucket>();
            codelist = "12065483,1222796,1222531,12035935,12042918,12064140,12064108,12063800,12063802,12063859,12063799";
            codelist += ",12063801,12063959,12063803,12063707,12063964,12025529,12057578,12056762,12056751,12056763,12056750";
            codelist += ",12056754,1222567,12051779,12025536,12039029,12038297,12025537,12025538,12025539,12025540,12211050";
            codelist += ",12211051,12211052,12211054,12211056,12211057,12211058,12211059,12211060,12211061,12211062,12211063";
            codelist += ",12038604,12040773,12041405,12040711,12042629,1222537,1222536,12211080,12211081,12211082,12211083";
            codelist += ",12211147,12211148,1222540,12025546,12025547,12025549,12025550,12025552,12025553,12025555,12025556";
            codelist += ",12025558,12025559,12037282,12041823,12025563,12025564,12049426,12049428,12049425,12049427,12052160";
            codelist += ",12050307,12025565,12049581,12049583,12049580,12049582,12052419,12051262,12025566,12049343,12049345";
            codelist += ",12049342,12049344,12050639,12050523,12025568,12048584,12048588,12048583,12048587,12052580,12052122";
            codelist += ",12025569,12050747,12050749,12050746,12050748,12051219,12052514,12025576,12025580,12211108,1222578";
            codelist += ",12052110,12025541,12025542,1222631,12025543,12025593,12026588,12036870";
            AddBuckets(buckets, "", codelist, "",  "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            //Gi / GU Fluid Mgt:
            assessgrouplabel = "Gi/GU Fluid Mgt";
            buckets = new List<gBucket>();
            codelist = "12025790,12025792,122333,12211487,1222944,12026505,12211498,12211499,12211500,12211492,12025799";
            codelist += ",12057083,12025803,1222885,1222887,12045746,12061146,12061147,12038505,12211689,12025805,12025806";
            codelist += ",12051868,1223425,12025815,12025829,1224686,1224685,12047264,12046687";
            AddBuckets(buckets, "", codelist, "",  "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            //Wound:
            assessgrouplabel = "Wound";
            buckets = new List<gBucket>();
            codelist = "12025835,1222718,1222719,1222720,12051480,12211583,1222728,12040412,12042625,12211611,12211612";
            codelist += ",12057328,1224150,12025892,12025893,12050989,12050991,12050988,12050990,12037746,12056851,12051232";
            codelist += ",12051706,12051890,12050356,12048731,12050185,12048582,12041044,12040695,12211604,12211605,1222783";
            codelist += ",12211606,12025866,12059306,12048640,1222765,1222766,1222767,12049880,12050558,12048494,12055349";
            AddBuckets(buckets, "", codelist, "",  "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            //Medication:
            assessgrouplabel = "Medication";
            buckets = new List<gBucket>();
            codelist = "12063944,12025402,12064552,12065154,12044478,12059338,12025384,12025385,12025386,12025387,12025388,12025389";
            codelist += ",12025422,12025525,12043105,12065364,12066139,12025410,12059615,12055258";
            AddBuckets(buckets, "", codelist, "",  "");
            string codelist1 = "12059338,12025403,12054146";
            string codelist2 = "12025409";
            string reslist1 = "";
            string reslist2 = "";
            AddDependentBuckets(buckets, codelist1, reslist1, codelist2, reslist2, "", "");

            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);
        }


        private void AddVitalsBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field, string result_list, SearchDepth search_depth)
        {
            // Assign (virtual) bucket numbers to each assessment.  
            // Every x minutes after that is a new bucket.  Bucket zero = pull_start time.
            Program.VerboseAudit("----Locating Vital Signs: Filling buckets...Bucketsize=" + _bucket_size);

            // get the chart items for the assessments
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc, field, result_list);

            var query3 = from item in query
                         select new
                         {
                             bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                             code = item.CODE,
                             evdt = item.EVENT_DATETIME
                         };
            var query4 = from item in query3
                         orderby item.bucket, item.code
                         select item;

            // Add to the list
            int currb = -1;
            //            int prevb = -1;
            string currcode = "xx";
            DateTime currevdt = DateTime.MinValue;
            int currct = 0;
            //          bool addcurr = false;
            //Data is: 0, "123"
            //         0, "234"  0733
            //         0, "234"  0734
            //         1, "123"
            //         2, "234"
            //         2, "345"
            foreach (var item in query4)
            {
                //Program.VerboseAudit("q4 item: b=" + item.bucket + " c=" + item.code + " dt=" + item.evdt);
                if (currb != item.bucket)
                {
                    currb = item.bucket;
                    currcode = item.code;
                    currevdt = item.evdt;
                    currct = 1;
                }
                else
                {
                    if (currcode != item.code)
                    {  //minimum of 2 different VS codes
                        if (currct == 1)
                        { // add the first code first
                            var b = new gBucket();
                            b.bucket = item.bucket;
                            b.code = currcode;
                            b.evdt = currevdt;
                            b.has_all_deps = true;
                            //gBucket f = bucket_list.Find(x => x.evdt == item.evdt);
                            //if (f.evdt != item.evdt) bucket_list.Add(b);
                            bucket_list.Add(b);
                            Program.VerboseAudit("  adding item1: b=" + b.bucket + " c=" + b.code + " dt=" + b.evdt);
                        }
                        currct++;
                        currcode = item.code; //guarantees not to add code again in same bucket

                        var b2 = new gBucket();
                        b2.bucket = item.bucket;
                        b2.code = item.code;
                        b2.evdt = item.evdt;
                        b2.has_all_deps = true;
                        //gBucket f = bucket_list.Find(x => x.evdt == item.evdt);
                        //if (f.evdt != item.evdt) bucket_list.Add(b);
                        bucket_list.Add(b2);
                        Program.VerboseAudit("  adding item2: b=" + b2.bucket + " c=" + b2.code + " dt=" + b2.evdt);
                    }
                }
            }

            // This is just for auditing: Say what we are looking for and how many were found
            if (!String.IsNullOrEmpty(result_list))
            {
            }
            else
            {
            }
            Program.VerboseAudit("----End of Locating Vital Signs: Filling buckets...Bucketsize=" + _bucket_size);
        }


        private bool AnalyzeBuckets(List<gBucket> buckets, int ind, int bucketsize, string group)
        {
            return AnalyzeBuckets(buckets, ind, bucketsize, group, true);
        }

        private bool AnalyzeBuckets(List<gBucket> buckets, int ind, int bucketsize, string group, bool set_ind)
        {
            DateTime dt = DateTime.MinValue;
            int bnum = 0;
            List<gBucket> dtlist = new List<gBucket>();

            bool all_ok = OLDAnalyzeBuckets(buckets, ind, bucketsize, group, set_ind);
            return all_ok;

            //bool all_ok = NEWAnalyzeBuckets(buckets, ind, bucketsize, group, set_ind);
            //return all_ok;

        }

        private bool OLDAnalyzeBuckets(List<gBucket> buckets, int ind, int bucketsize, string group, bool set_ind)
        {
            DateTime dt = DateTime.MinValue;
            int bnum = 0;
            int numbucket = -99;
            int lobucket = 99;
            int hibucket = 0;
            int numitems = 0;
            int numconsec = 0;
            int greatestnumconsec = 0;
            bool all_ok = false;
            List<gBucket> dtlist = new List<gBucket>();
            Program.VerboseAudit("----Begin Assessment Group = " + group + " ---- bucketsizew=" + bucketsize + " start time of first bucket=" + _pat.pull_start);
            //if (ind==18 && Math.Round(_pat.los_hours) <= 4.0)
            //{
            //    all_ok = DoShortLOSAssessEval(buckets,ind,bucketsize,group,set_ind);
            //    Program.VerboseAudit("---- End Assessment Group = " + group + " ----");
            //    return all_ok;
            //}

            //Program.VerboseAudit("buckets count=" + buckets.Count());
            var b = buckets.OrderBy(e => e.evdt).ToList();

            //numitems = buckets.Count();
            foreach (var item in b)
            {
                if (numbucket < item.bucket)
                {
                    numbucket = item.bucket;
                    numitems++;
                    Program.VerboseAudit(item.bucket + ") " + item.evdt.ToString() + " := " + item.code);
                }
            }

            int num_buckets_in_los = (int)(_pat.los_hours * (60.0 / bucketsize));//for q30 this will be 2xlos_hours.  for q60 this will be los_hours
            Program.VerboseAudit("total bucket count in LOS=" + num_buckets_in_los);
            Program.VerboseAudit("num buckets filled=" + numitems);
            //Program.VerboseAudit("half-LOS bucket count=" + half_los + " at " + bucketsize + "mins each.");
            //Program.VerboseAudit("greatest consec=" + greatestnumconsec);

            //double bucketratio = (hibucket-lobucket) / (1.0 * (bnum-1));
            //q30 75% of 30-min buckets	3/4 x 24  = 18	3/4 x 12  = 9	3/4 x 8  = 6	3/4 x 4  = 3	5 or more VS
            //q1 66% of 60-min buckets	2/3 x 12  = 8	2/3 x 6  = 4	2/3 x 4  = 2.67=>3	2/3 x 2  = 1.33=>1	3 VS + examine 30min 
            //q2 50 % of 60-min buckets	.5 x 12  = 6	.5 x 6  = 3	.5 x 4  = 2	.5 x 2  = 1	2 VS
            //q4 33% of 60-min buckets	1/3 x 12  = 4 2	1/3 x 6  = 2 1	1/3 x 4  = 1.33=>1	1/3 x 2  = .67=>1	1 VS
            int q30need = (int)Math.Round(0.75 * 2 * _pat.los_hours);
            int q1need = (int)Math.Round(0.667 * _pat.los_hours);
            int q2need = (int)Math.Round(0.5 * _pat.los_hours);
            int q4need = 1;
            if (ind <= 20)
            {
                if (_pat.los_hours > 5)
                {
                    if (_pat.los_hours < 8)
                    {
                        q4need = 1;
                        q2need = 2; 
                    }
                    else
                    {
                        q4need = 2;
                        q2need = 4; 
                    }

                    if (numitems >= q1need)
                    {
                        SetInd(20 * Convert.ToInt32(set_ind), "Qualifies for q1 because numcharted=" + numitems + " is >=" + q1need + " LOS=" + Math.Round(_pat.los_hours, 2) + " x .667=" + q1need);
                        all_ok = (ind <= 20);
                    }
                    else if (numitems >= q2need)
                    {
                        //Jan27 2021 SetInd(16 * Convert.ToInt32(set_ind), "Qualifies for q2 because numcharted=" + numitems + " is >=" + q2need + " LOS=" + Math.Round(_pat.los_hours, 2) + " x .5=" + q2need);
                        SetInd(19 * Convert.ToInt32(set_ind), "Qualifies for q2 because numcharted=" + numitems + " is >=" + q2need + " for LOS=" + Math.Round(_pat.los_hours, 2) + "  >=8hrs needs 5 else 2");
                        all_ok = (ind <= 19);
                    }
                    else if (numitems >= q4need)
                    {
                        SetInd(18 * Convert.ToInt32(set_ind), "Qualifies for q4 because numcharted=" + numitems + " is >=" + q4need + " LOS=" + Math.Round(_pat.los_hours, 2) + "  >=8hrs needs 2 else 1");
                        all_ok = (ind <= 18);
                    }
                }
                else //short los
                {
                    q1need = 3;
                    q2need = 2;
                    q4need = 1;

                    if (numitems >= q1need)
                    {
                        SetInd(20 * Convert.ToInt32(set_ind), "ShortLOS Qualifies for q1 because numcharted=" + numitems + " is >=" + q1need + " LOS=" + Math.Round(_pat.los_hours, 2) + " needs just 3");
                        all_ok = (ind <= 20);
                    }
                    else if (numitems >= q2need)
                    {
                        SetInd(19 * Convert.ToInt32(set_ind), "ShortLOS Qualifies for q2 because numcharted=" + numitems + " is >=" + q2need + " LOS=" + Math.Round(_pat.los_hours, 2) + " needs just 2");
                        all_ok = (ind <= 19);
                    }
                    else if (numitems >= q4need)
                    {
                        SetInd(18 * Convert.ToInt32(set_ind), "ShortLOS Qualifies for q4 because numcharted=" + numitems + " is >=" + q4need + " LOS=" + Math.Round(_pat.los_hours, 2) + " needs just 1");
                        all_ok = (ind <= 18);
                    }

                }
            }
            //else // ind==18
            //{
            //    if (_pat.los_hours > 5)
            //    {
            //        if (numitems >= q30need)
            //        {
            //            SetInd(18 * Convert.ToInt32(set_ind), "Qualifies for q30 because numcharted=" + numitems + " is >=" + q30need + " LOS=" + Math.Round(_pat.los_hours, 2) + " x 2 x .75=" + q30need);
            //            all_ok = (ind <= 18);
            //        }
            //    }
            //    else
            //    {
            //        q30need = 5;
            //        if (numitems >= q30need)
            //        {
            //            SetInd(18 * Convert.ToInt32(set_ind), "ShortLOS Qualifies for q30 because numcharted=" + numitems + " is >=" + q30need + " LOS=" + Math.Round(_pat.los_hours, 2) + " x 2 needs " + q30need);
            //            all_ok = (ind <= 18);
            //        }
            //    }
            //    //if (greatestnumconsec >= half_los)
            //    //{
            //    //    SetInd(18 * Convert.ToInt32(set_ind), "Qualifies for q30min because consecutive chartings are at least q30 for duration of half-LOS=" + half_los + " maxconsec=" + greatestnumconsec);
            //    //    all_ok = (ind <= 18);
            //    //}
            //}
            Program.VerboseAudit("---- End Assessment Group = " + group + " ----");
            return all_ok;
        }


        private void SetBucketSize(int minutes)
        {
            _bucket_size = minutes;
        }

        //Add to the list of bucket numbers (redundant buckets are fine)
        private void AddBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field)
        {
            AddBuckets(bucket_list, cat, code_list, desc, field, "", SearchDepth.SearchDefault);
        }
        private void AddBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field, string result_list)
        {
            AddBuckets(bucket_list, cat, code_list, desc, field, result_list, SearchDepth.SearchDefault);

        }

        private void AddBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field, string result_list, SearchDepth search_depth)
        {
            // Assign (virtual) bucket numbers to each assessment.  
            // Every x minutes after that is a new bucket.  Bucket zero = pull_start time.
            //            Program.VerboseAudit("----Locating items for group: " + assessgrouplabel + "   bucketsize=" + _bucket_size);

            // get the chart items for the assessments
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc, field, result_list);
            int ct = query.Count();
            Program.VerboseAudit("ct=" + ct + " of codelist=" + code_list);

            var query3 = from item in query
                         select new
                         {
                             bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                             code = item.CODE,
                             evdt = item.EVENT_DATETIME
                         };
            // Add to the list
            int cta = 0;
            foreach (var item in query3)
            {
                var b = new gBucket();
                b.bucket = item.bucket;
                b.code = item.code;
                b.evdt = item.evdt;
                b.has_all_deps = true;
                gBucket f = bucket_list.Find(x => x.evdt == item.evdt);
                if (f.evdt != item.evdt)
                {
                    bucket_list.Add(b);
                    cta++;
                }
            }
            Program.VerboseAudit("add ct=" + cta);

            // This is just for auditing: Say what we are looking for and how many were found
            if (!String.IsNullOrEmpty(result_list))
            {
            }
            else
            {
            }

        }

        private void AddDependentBuckets(List<gBucket> bucket_list, string codelist1, string reslist1, string codelist2, string reslist2)
        {
            AddDependentBuckets(bucket_list, codelist1, reslist1, codelist2, reslist2, "", "");
        }

        private void AddDependentBuckets(List<gBucket> bucket_list, string codelist1, string reslist1, string codelist2, string reslist2, string codelist3, string reslist3)
        {
            bool dep3 = true;
            // get the chart items for the assessments
            var query1 = StartNewQuery();
            query1 = AndItemFilter(query1, "", codelist1, "", "", reslist1);
            var query2 = StartNewQuery();
            query2 = AndItemFilter(query2, "", codelist2, "", "", reslist2);
            if (codelist3.Trim() == "")
            {
                dep3 = false;
                codelist3 = "Hello, this is a phantom code";
            }
            var query3 = StartNewQuery();
            query3 = AndItemFilter(query3, "", codelist3, "", "", reslist3);

            // figure out what buckets the events belong to
            var query1a = from item in query1
                          select new
                          {
                              bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                              code = item.CODE,
                              evdt = item.EVENT_DATETIME
                          };
            var query2a = from item in query2
                          select new
                          {
                              bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                              code = item.CODE,
                              evdt = item.EVENT_DATETIME
                          };
            var query3a = from item in query3
                          select new
                          {
                              bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                              code = item.CODE,
                              evdt = item.EVENT_DATETIME
                          };

            string s = "BucketList1 for " + codelist1 + ": ";
            foreach (var item in query1a)
            {
                s += item.bucket + ": dt=" + item.evdt + " ";
            }
            Program.VerboseAudit(s);

            s = "BucketList2 for " + codelist2 + ": ";
            foreach (var item in query2a)
            {
                s += item.bucket + ": dt=" + item.evdt + " ";
            }
            if (dep3)
            {
                s = "BucketList3 for " + codelist3 + ": ";
                foreach (var item in query3a)
                {
                    s += item.bucket + "";
                }
            }
            Program.VerboseAudit(s);
            // Add to the list IFF items in both lists occur in same bucket
            foreach (var item1 in query1a)
            {
                foreach (var item2 in query2a)
                {
                    if (item1.evdt == item2.evdt)
                    {
                        if (dep3)
                        {
                            foreach (var item3 in query3a)
                            {
                                if (item1.bucket == item3.bucket)
                                {
                                    var b = new gBucket();
                                    b.bucket = item1.bucket;
                                    b.code = item1.code;
                                    b.evdt = item1.evdt;
                                    bucket_list.Add(b);
                                }
                            }
                        }
                        else
                        {
                            var b = new gBucket();
                            b.bucket = item1.bucket;
                            b.code = item1.code;
                            b.evdt = item1.evdt;
                            bucket_list.Add(b);
                        }
                    }
                }
            }

        }

        //private int CountBuckets(List<gBucket> bucket_list)       // bucket list: (ha ha)
        //{
        //    //int result = bucket_list.Distinct().Count();
        //    //if (result > 0) Program.VerboseAudit(result + " unique");
        //    //return result;
        //    int x = -99;
        //    int result = 0;
        //    //int result = bucket_list.Distinct().Count();
        //    var query = from b in bucket_list
        //                orderby b.bucket ascending
        //                select b;
        //    foreach (var b in query)
        //    {
        //        if (x != b.bucket)
        //        {
        //            result++;
        //            x = b.bucket;
        //        }
        //    }
        //    if (result > 0) Program.VerboseAudit(result + " unique");
        //    return result;
        //}

        private void Check_21()
        {
            string reslist;
            string codelist;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 21. Fluid Management");
            Program.VerboseAudit("---------------");
            exclude_periop_data = false;

            string desc_found = "";
            if (OrderInProgressByCode("350708", out desc_found))
                SetInd(21, "Order in effect: " + desc_found);

        }

        private void Check_22()
        {
            string reslist;
            bool st1 = false;
            string piv;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 22. Wound/Injury Mgmt");
            Program.VerboseAudit("---------------");

            exclude_periop_data = false;

            reslist = "dressing applied,dressing changed,dressing moistened,dressing reinforced";
            reslist += ",dressing removed,abdominal binder utilized,antimicrobial agent applied,calcium alginate";
            reslist += ",collagen,elastic bandage,foam,wet-to-dry,wet-to-moist";
            reslist += ",hydrocolloid,hydrofiber,low-adherent,non-adherent,open to air";
            reslist += ",petroleum-based,silver impregnated,skin substitute applied";
            reslist += ",sodium chloride impregnated,transparent film,skin barrier agent applied";
            reslist += ",tubular wrap,wound filler utilized,packing removed,packed with,gauze, antimicrobial";
            reslist += ",iodoform,packing strip,wound pouch applied,wound pouch removed,wound pouch intact,other (see comments)";
            SetIndIfResultContains(22, "", "4012548", "", "", reslist);

            reslist = "cleansed with,irrigated with,antimicrobial agent applied,soap and water,sterile half normal saline";
            reslist += ",steril normal saline,sterile water,water,wound cleanser,barrier applied,debrided";
            reslist += ",collegenase agent applied,dressing removed,enzymatic agent applied,growth factor agent applied";
            reslist += ",honey applied,liquid bandage applied,moist wound environment maintained,neutral pH skin disinfectant applied";
            reslist += ",pressure applied,pressure removed,pressure dressing applied,pressure dressing removed,silver agent applied";
            reslist += ",surfectant agent applied,wound temperature maintained,biological therapy,eletrical stimulation";
            reslist += ",hydrotherapy provided,induced electrical stimulation,negative pressure wound therapy,phototherapy";
            reslist += ",pulsatile high pressure lavage,ultrasound therapy,other (see comments)";
            SetIndIfResultContains(22, "", "4010210", "", "", reslist);

            SetIndIfResultContains(22, "", "12054035", "", "", "");
            SetIndIfResultContains(22, "", "12046729", "", "", "");
            SetIndIfResultContains(22, "", "12055487", "", "", "");


            reslist = "dressing applied,dressing changed,dressing moistened,dressing reinforced";
            reslist += ",dressing removed,abdominal binder utilized,antimicrobial agent applied,calcium alginate";
            reslist += ",collagen,elastic bandage,foam,gauze,hydrocolloid,hydrofiber,low-adherent,non-adherent";
            reslist += ",open to air,petroleum -based,silver impregnated,skin substitute applied,sodium chloride impregnated";
            reslist += ",transparent film,skin barrier agent applied,tubular wrap,wound filler utilized,packing removed";
            reslist += ",packed with,packing strip,wound pouch applied,wound pouch removed,wound pouch intact,other";
            SetIndIfResultContains(22, "", "12050832", "", "", reslist);


            reslist = "cleansed with,irrigated with,antimicrobial agent applied,soap and water,sterile half normal saline";
            reslist += ",steril normal saline,sterile water,water,wound cleanser,barrier applied,debrided,collegenase agent applied";
            reslist += ",dressing removed,enzymatic agent applied,growth factor agent applied,honey applied,liquid bandage applied";
            reslist += ",moist wound environment maintained,neutral pH skin disinfectant applied,pressure applied";
            reslist += ",pressure removed,pressure dressing applied,pressure dressing removed,silver agent applied,surfectant agent applied";
            reslist += ",wound temperature maintained,biological therapy,eletrical stimulation,hydrotherapy provided";
            reslist += ",induced electrical stimulation,negative pressure wound therapy,phototherapy,pulsatile high pressure lavage,ultrasound therapy,other";
            SetIndIfResultContains(22, "", "12048494", "", "", reslist);

            int ct = CountItems("", "12053619", "", "", "");
            if (ct >= 3)
                SetInd(22, "Wound count gt 3 count=" + ct);

        }


        int EducMins(string timecode, string nursecode)
        {
            string res;
            int val = 0;
            if (ResultContains("", nursecode, "", "", "Nursing"))
            {
                if (GetResult("", timecode, "", "", out res))
                {
                    if (res.Left(1).IsNumeric())
                    {
                        val += (int)res.Val();
                    }
                }
            }
            return val;
        }

        private void Check_23()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 23. Patient/Family Education >= 1 Hour by RN");
            Program.VerboseAudit("---------------");
            exclude_periop_data = true;
            exclude_periop_data = false;

            string codelist = "4025449,4033238,4025360,4025413,4033256,4025597,4033287";
            SetIndIfResultContains(23, "", codelist, "", "", "");

        }
        private int SetIndIfCodeBtwn(int ind, int locode, int hicode, SearchDepth search_depth)

        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = query.Where(e => e.CODE.ToLower().StartsWith("edu"));
            query = query.Where(e => e.CODE.Substring(3, 9).Val() >= locode);
            query = query.Where(e => e.CODE.Substring(3, 9).Val() <= hicode);
            int count = query.Count();
            // always return what was found
            string found_what = "There were " + count + " items found with code between " + locode + " and " + hicode;
            if (count >= 8) SetInd(ind, found_what);
            // echo the result?
            //               if (trace) Program.VerboseAudit(found_what);

            return count;
        }

        private void CheckEDUtab(string educode)
        {
            int ub, i;
            string[] desc = new string[3];
            DateTime evdt;
            string cd1, cd2, res1, res2, topic1, topic2;

            //OBX | 2 | ST | EDU3040004179METHOD ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC STROKE EVERY MINUTE MATTERS MC6074| 1 | E |||||| F ||| 20170612113300
            //OBX | 4 | ST | EDU3040004179RESPONSE ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC STROKE EVERY MINUTE MATTERS MC6074| 1 | TB |||||| F ||| 20170612113300
            //OBX | 6 | ST | EDU3040004179METHOD ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC YOU CAN IMPROVE YOUR HEALTH MC2066 - 09 | 1 | E |||||| F ||| 20170612113300
            //OBX | 8 | ST | EDU3040004179RESPONSE ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC YOU CAN IMPROVE YOUR HEALTH MC2066 - 09 | 1 | TB |||||| F ||| 20170612113300
            //OBX | 10 | ST | EDU3040004179METHOD ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC HIGH BLOOD PRESSURE (HYPERTENSION)MC5056 | 1 | E |||||| F ||| 20170612113300
            //OBX | 12 | ST | EDU3040004179RESPONSE ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC HIGH BLOOD PRESSURE (HYPERTENSION)MC5056 | 1 | TB |||||| F ||| 20170612113300
            var query1 = StartNewQuery(SearchDepth.SearchSince9Hrs);
            query1 = AndItemFilter(query1, "", "EDU" + educode + "METHOD", "", "", "E,D,I");
            query1 = query1.Where(e => e.EVENT_DATETIME <= PFSUtility.MaxDateTime(_pat.unit_departure, _pat.effective_out));
            foreach (var item1 in query1)
            {
                evdt = item1.EVENT_DATETIME;
                res1 = item1.RESULT;
                cd1 = item1.CODE;
                var arr = item1.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                ub = arr.GetUpperBound(0);
                for (i = 0; (i <= Math.Min(ub, 2)); i++)
                    desc[i] = arr[i];
                if (ub >= 2)
                    topic1 = desc[2].Trim();
                else
                    topic1 = "";
                var query2 = StartNewQuery(SearchDepth.SearchSince9Hrs);
                query2 = AndItemFilter(query2, "", "EDU" + educode + "RESPONSE", "", "", "IP,TB,NR");
                query2 = query2.Where(e => e.EVENT_DATETIME == evdt);
                foreach (var item2 in query2)
                {
                    res2 = item2.RESULT;
                    cd2 = item2.CODE;
                    var arr2 = item2.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                    ub = arr.GetUpperBound(0);
                    for (i = 0; (i <= Math.Min(ub, 2)); i++)
                        desc[i] = arr[i];
                    if (ub >= 2)
                        topic2 = desc[2].Trim();
                    else
                        topic2 = "";
                    if (topic1 == topic2)
                    {
                        SetInd(23, "Found EDU" + educode + ": " + desc[0] + "^" + topic1 + " at:" + evdt.ToString() + " METHOD=" + res1 + " RESPONSE=" + res2);
                    }
                }

            }

        }

        private void Check_24()
        {
            string reslist = "";
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MH 24. Coordination of Care >= 1 Hour by RN");
            Program.VerboseAudit("---------------");
            exclude_periop_data = true;

            reslist = "community resources reviewed,crisis management plan promoted";
            reslist += "crisis management plan verbalized,follow-up care coordinated";
            reslist += "follow-up care discussed,other (see comments)";
            SetIndIfResultContains(24, "", "12040792", "", "", reslist);
        }

        private void CheckLanguage()
        {
            //            "Via ADT Intterface PID-15:
            //Trigger Indicator #8 Communication if this field's value is anything other than: (blank), eng, english, unk, dc, or unv."
            //"Via ADT Intterface in PID-15:
            //Trigger Indicator #8 Communication if this field's value is anything other than: (blank), Eng, UNK, dc, or  unv."

            int sep_idx = _pat.lang_lvc.IndexOf(";;");
            //Program.VerboseAudit("Language=" + _pat.lang_lvc + " sep_idx="+sep_idx);
            if (sep_idx <= 0) return;
            string pref_lang = _pat.lang_lvc.Substring(0, sep_idx);
            string writ_lang = _pat.lang_lvc.Substring(sep_idx + 2, _pat.lang_lvc.Length - sep_idx - 2);
            bool pref_ok = (pref_lang.Trim() == "" || pref_lang.Trim().ToUpper().StartsWith("ENG")
                || pref_lang.Trim().ToUpper().StartsWith("DC")
                || pref_lang.Trim().ToUpper().StartsWith("UNK")
                || pref_lang.Trim().ToUpper().StartsWith("UNV"));
            bool writ_ok = (writ_lang.Trim() == "" || writ_lang.Trim().ToUpper().StartsWith("ENG")
                || writ_lang.Trim().ToUpper().StartsWith("DC")
                || writ_lang.Trim().ToUpper().StartsWith("UNK")
                || writ_lang.Trim().ToUpper().StartsWith("UNV"));
            if (!(pref_ok && writ_ok))
                SetInd(24, "Preferred language=" + pref_lang + " Written=" + writ_lang);
        }

        private void Check_Activities()
        {

            Program.VerboseAudit("---------------");
            Program.VerboseAudit(" Activities");
            Program.VerboseAudit("---------------");


        }

        private void AtLeastOneADL()
        {
            Program.Audit("---------------");

            if (!(_inds[2].is_checked || _inds[3].is_checked))
            {
                SetInd(1, "Defaulting to ADL Self.");
            }
        }

        private void HighestIndicatorInEachGroupWins()
        {
            int i, g;
            bool highest_is_on;
            string ind_list;

            Program.Audit("Select highest indicator in each group");

            g = 0;
            highest_is_on = false;
            // Go from bottom up - highest (lowest) indicator in each group wins
            for (i = MAX_INDS; (i >= 1); i--)
            {
                if (_inds[i].radio_group > 0)
                {
                    if (_inds[i].radio_group != g)
                    {
                        //this is a new group
                        g = _inds[i].radio_group;
                        highest_is_on = _inds[i].is_checked;
                    }
                    else
                    {
                        //same group
                        if (highest_is_on)
                        {
                            _inds[i].is_checked = false;             //uncheck a lower number
                        }
                        else
                        {
                            highest_is_on = _inds[i].is_checked;     //save this one
                        }
                    }
                }
            } // next i

            //Echo the indicators for an Audit (no classification will be saved)
            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++)
            {
                if (_inds[i].is_checked) ind_list += "," + i;
            } // next i

            Program.Audit("Final list = " + ind_list.Substring(1));
            Program.Audit("---------------");
        }


        private void CheckProcs()
        {
            CheckProc_2();
            CheckProc_3();
            CheckProc_4();
            CheckProc_5();
            CheckProc_6();
            CheckProc_7();

        }

        private void CheckActivity(int actnum, string actcode, string actst, string actlencode)
        {
            int actlen;
            string actsthhmm = "0000";

            var query = StartNewQuery(SearchDepth.SearchSince24Hrs);    // add custom time range below
            query = query.Where(e => e.CODE == actcode);
            query = query.Where(e => e.ORDER_STATUS == null || e.ORDER_STATUS == ""); // need to update order_status=x
            query = query.OrderBy(e => e.EVENT_DATETIME);
            Program.VerboseAudit("for code=" + actcode + " count=" + query.Count());
            foreach (var item in query)
            {
                if (item.RESULT.ToLower() == "yes")
                { //09500000
                    var q2 = StartNewQuery(SearchDepth.SearchSince24Hrs);    // add custom time range below
                    q2 = q2.Where(e => e.CODE == actst);
                    q2 = q2.Where(e => e.EVENT_DATETIME == item.EVENT_DATETIME);
                    Program.VerboseAudit("for code=" + actst + " count=" + q2.Count());
                    if (q2.Count() == 0) return;

                    actsthhmm = q2.First().RESULT;
                    Program.VerboseAudit("actsthhmm=" + actsthhmm);
                    if (actsthhmm.Length >= 4)
                        actsthhmm = actsthhmm.Substring(0, 4);
                    Program.VerboseAudit("actsthhmm=" + actsthhmm);

                    var q3 = StartNewQuery(SearchDepth.SearchSince24Hrs);    // add custom time range below
                    q3 = q3.Where(e => e.CODE == actlencode);
                    q3 = q3.Where(e => e.EVENT_DATETIME == item.EVENT_DATETIME);
                    Program.VerboseAudit("for code=" + actlencode + " count=" + q3.Count());
                    if (q3.Count() == 0) return;

                    actlen = (int)q3.First().RESULT.Val();
                    DateTime stdt = DateTime.MinValue;
                    if (actlen >= 60)
                    {
                        TimeSpan start = new TimeSpan(0, 0, 0); //0 o'clock
                        TimeSpan end = new TimeSpan(6, 0, 0); // 6 o'clock
                        TimeSpan item_ts = item.EVENT_DATETIME.TimeOfDay;
                        TimeSpan actst_ts = new TimeSpan((int)actsthhmm.Substring(0, 2).Val(), (int)actsthhmm.Substring(2, 2).Val(), 0);
                        stdt = item.EVENT_DATETIME.Date + actst_ts;
                        if ((item_ts >= start) && (item_ts <= end))
                        {
                            if (actst_ts > item_ts) // then it belongs to prev cal day.
                                stdt = item.EVENT_DATETIME.Date.AddDays(-1) + actst_ts;
                        }
                        //                      DateTime stdt = item.EVENT_DATETIME.Date.AddHours(actsthhmm.Substring(0, 2).Val()).AddMinutes(actsthhmm.Substring(2, 2).Val());
                        Program.VerboseAudit("stdt=" + stdt);
                        AddSimpleProc(actnum, stdt, stdt.AddMinutes(actlen));
                        NullifyActivity(actcode, item.EVENT_DATETIME);
                    }
                }
            }

        }

        private void NullifyActivity(string actcode, DateTime evdt)
        {
            var db = PFSDBUtility.NewSqlConnection();
            string q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and event_datetime=" + PFSDBUtility.SQLDateTime(evdt) + " and code='" + actcode + "'";
            SqlCommand cmd = new SqlCommand(q, db);
            cmd.ExecuteNonQuery();
            db.Close();
        }

        //====================================================
        //====================================================

        private void CheckProc_2()
        {
            if (_pat.short_name.ToUpper() != "MGH") return;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MHA1. 1-1 safety observation by RN");
            Program.VerboseAudit("MHA2. 1-1 safety observation by non-RN");
            Program.VerboseAudit("---------------");

            //54610   2020 - 01 - 29 11:00:00.000 Initiated   304239487
            //54610   2020 - 01 - 30 10:00:00.000 Continued   304239487
            //54610   2020 - 01 - 30 22:00:00.000 Discontinued   304239487
            //            304239487

            //            "Initiated
            //Continued
            //Discontinued"
            bool stop = false;
            DateTime dcdt = DateTime.MinValue;
            bool found_dc = false;
            bool first = true;
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);    // add custom time range below
            query = query.Where(e => e.CODE == "304239487");
            query = query.Where(e => e.ORDER_STATUS == null || e.ORDER_STATUS == ""); // need to update order_status=x
            query = query.Where(e => e.RESULT.ToLower() == "initiated" || e.RESULT.ToLower() == "continued");
            query = query.OrderBy(e => e.EVENT_DATETIME);
            Program.VerboseAudit("qct: " + query.Count());
            if (query.Count() > 0)
            {
                foreach (var item in query)
                {
                    if (item.EVENT_DATETIME > dcdt) //skip any intermediate starts
                    {
                        if (item.PROCEDURE_START == null)
                            item.PROCEDURE_START = DateTime.MaxValue;
                        Program.VerboseAudit("item:" + item.EVENT_DATETIME + " original start=" + item.PROCEDURE_START);
                        found_dc = MakeActivityIfFindDiscontinue((DateTime)item.PROCEDURE_START, item.EVENT_DATETIME, out dcdt);
                        Program.VerboseAudit("Sitter: " + item.RESULT + " " + item.EVENT_DATETIME.ToString());
                        first = false;
                        if (found_dc)
                        {
                            Program.VerboseAudit("founddc: true");
                            UpdatePtChartArraysCode("304239487", "X", dcdt);
                        }
                    }
                }
            }
        }

        private bool MakeActivityIfFindDiscontinue(DateTime origstart, DateTime initdt, out DateTime dcdt)
        {
            int pnum = 0;
            bool found_dc = false;
            dcdt = DateTime.MinValue;
            bool first = true;
            DateTime save_initdt = initdt;

            Program.VerboseAudit("q origstart=" + origstart + " initdt=" + initdt + " g_pull_finist=" + Program.g_pull_finish);
            if (origstart < initdt) initdt = origstart;
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);    // add custom time range below
            query = query.Where(e => e.CODE == "304239487");
            query = query.Where(e => e.ORDER_STATUS == null || e.ORDER_STATUS == ""); // need to update order_status=x
            query = query.Where(e => e.RESULT.ToLower() == "discontinued");
            query = query.Where(e => e.EVENT_DATETIME >= initdt);
            query = query.OrderBy(e => e.EVENT_DATETIME);
            Program.VerboseAudit("qcount=" + query.Count());
            if (query.Count() > 0)
            {
                found_dc = true;
                foreach (var item in query)
                {
                    if (first)
                    {
                        first = false;
                        if (item.EVENT_DATETIME >= initdt) //valid discontinued exists
                        {
                            dcdt = item.EVENT_DATETIME;
                            if (origstart < Program.g_pull_finish.AddHours(-12)
                                && origstart >= Program.g_pull_finish.AddHours(-24))
                            {
                                //make for origstart to item.evdt
                                Program.VerboseAudit("2A: dc time=" + item.EVENT_DATETIME);
                                pnum = GetSitterType(origstart);
                                AddSimpleProc(pnum, origstart, item.EVENT_DATETIME);
                                DisableSitter(item.CODE, origstart, true);
                            }
                            else if (item.EVENT_DATETIME <= Program.g_pull_finish.AddHours(-12))
                            { // discontinued in past; neutralize this pair.
                                Program.VerboseAudit("1: dc time=" + item.EVENT_DATETIME);
                            }
                            else if (initdt < Program.g_pull_finish.AddHours(-12))
                            {
                                //make for pull start to item.evdt
                                Program.VerboseAudit("2: dc time=" + item.EVENT_DATETIME);
                                pnum = GetSitterType(initdt);
                                AddSimpleProc(pnum, Program.g_pull_finish.AddHours(-12), item.EVENT_DATETIME);
                            }
                            else if (item.EVENT_DATETIME <= Program.g_pull_finish)
                            {
                                Program.VerboseAudit("3: dc time=" + item.EVENT_DATETIME);
                                //make for initdt to item.evdt
                                pnum = GetSitterType(initdt);
                                AddSimpleProc(pnum, initdt, item.EVENT_DATETIME);
                            }
                            else
                            {
                                Program.VerboseAudit("4: dc time=" + item.EVENT_DATETIME);
                                //make for initdt to _pat.pull finish
                                pnum = GetSitterType(initdt);
                                AddSimpleProc(pnum, initdt, Program.g_pull_finish);
                            }
                            DisableSitter(item.CODE, item.EVENT_DATETIME);
                        }
                        Program.VerboseAudit("Sitter: " + item.RESULT + " " + item.EVENT_DATETIME.ToString());
                    }
                }
            }
            else if (initdt <= Program.g_pull_finish.AddHours(-12)) //no discontinued
            {
                //make for pull start to pull finish
                Program.VerboseAudit("5: dc time=" + Program.g_pull_finish);
                dcdt = Program.g_pull_finish;
                pnum = GetSitterType(initdt);
                AddSimpleProc(pnum, Program.g_pull_finish.AddHours(-12), Program.g_pull_finish);
            }
            else if (initdt < Program.g_pull_finish) // if initdt==g_pull_finish then dont create
            {
                Program.VerboseAudit("6: dc time=" + Program.g_pull_finish);
                dcdt = Program.g_pull_finish;
                found_dc = true;
                pnum = GetSitterType(initdt);
                AddSimpleProc(pnum, initdt, Program.g_pull_finish);
                DisableSitter("304239487", initdt);
                AddSitter("304239487", initdt, Program.g_pull_finish);
            }
            return found_dc;
        }

        private void AddSitter(string code, DateTime initdt, DateTime evdt)
        {
            string desc = "TC: Direct Observer";
            string res = "Initiated";
            DateTime timestmp = DateTime.Now;
            short seq = 0;
            int unitid = _pat.unit_id;

            using (var db = PFSDBUtility.NewSqlConnection())
            {
                string q = "INSERT INTO chart_item (encounter_id,event_datetime,code,description,result,timestamp,sequence,unit_id,procedure_start)";
                q += " select @encid, @evdt, @code, @desc, @res, @ts,@seq,@unit,@procstart where not exists";
                q += " (select encounter_id,code,event_datetime from chart_item where encounter_id=" + _pat.encounter_id.ToString() + " and code='" + code + "' and event_datetime=" + PFSDBUtility.SQLDateTime(evdt) + ")";
                //string q = "INSERT INTO chart_item (encounter_id,event_datetime,code,description,timestamp,sequence,unit_id,order_id) values (@encid, @evdt, @code, @desc, @ts,@seq,@unit,@oid)";
                SqlCommand cmd = new SqlCommand(q, db);
                cmd.Parameters.AddWithValue("@encid", _pat.encounter_id);
                cmd.Parameters.AddWithValue("@evdt", evdt);
                cmd.Parameters.AddWithValue("@code", code);
                cmd.Parameters.AddWithValue("@desc", desc);
                cmd.Parameters.AddWithValue("@res", res);
                cmd.Parameters.AddWithValue("@ts", timestmp);
                cmd.Parameters.AddWithValue("@seq", seq);
                cmd.Parameters.AddWithValue("@unit", unitid);
                cmd.Parameters.AddWithValue("@procstart", initdt);
                cmd.ExecuteNonQuery();
                db.Close();
            } //using db

        }

        private int GetSitterType(DateTime evdt)
        { // get the sitter type RN or non-RN from the charting that should exist
            //at the same time as the initiation of the observer 1540100298
            int pnum = 0;
            string return_result = "";
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);    // add custom time range below
            query = query.Where(e => e.CODE == "1540100298");
            query = query.Where(e => e.EVENT_DATETIME == evdt);
            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
                if (return_result.Trim().ToUpper().StartsWith("RN")) pnum = 1;
                if (return_result.Trim().ToUpper().StartsWith("NON-RN")) pnum = 2;
            }
            if (pnum == 0)
            {
                Program.VerboseAudit("Sitter type RN or non-RN not found. Defaulting to non_RN.");
                pnum = 2;
            }
            return pnum;
        }


        //====================================================
        //====================================================

        //private void CheckProc_1_2()
        //{
        //    Program.VerboseAudit("---------------");
        //    Program.VerboseAudit("MHA1. 1-1 safety observation by RN");
        //    Program.VerboseAudit("MHA2. 1-1 safety observation by non-RN");
        //    Program.VerboseAudit("---------------");
        //    //            304239487
        //    //1540100298
        //    bool stop = false;
        //    DateTime dcdt = DateTime.MinValue;
        //    while (!stop)
        //    {
        //        bool found_dc = false;
        //        bool first = true;
        //        var query = StartNewQuery(SearchDepth.SearchSinceAdmission);    // add custom time range below
        //        query = query.Where(e => e.CODE == "304239487");
        //        query = query.Where(e => e.ORDER_STATUS == null || e.ORDER_STATUS == ""); // need to update order_status=x
        //        query = query.Where(e => e.RESULT.ToLower() == "initiated" || e.RESULT.ToLower() == "continued");
        //        query = query.OrderBy(e => e.EVENT_DATETIME);
        //        if (query.Count() > 0)
        //        {
        //            foreach (var item in query)
        //            {
        //                if (first)
        //                {
        //                    found_dc = MakeActivityIfFindDiscontinue(item.EVENT_DATETIME);
        //                    dcdt = item.EVENT_DATETIME;
        //                    Program.VerboseAudit("Sitter: " + item.RESULT + " " + item.EVENT_DATETIME.ToString());
        //                    first = false;
        //                }
        //            }
        //        }
        //        if (found_dc)
        //        {
        //            UpdatePtChartArraysCode("304239487", "X", dcdt);
        //            stop = false;
        //        }
        //        else
        //            stop = true;
        //    }

        //}

        private void DisableSitter(string code, DateTime evdt, bool use_proc_start)
        {
            //update ORDER_STATUS = 'X' for _pat.encounter_id and ordid
            var db = PFSDBUtility.NewSqlConnection();
            //string q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and code='" + code + "' and (result='initiated' or result='continued') and event_datetime<=" + PFSDBUtility.SQLDateTime(evdt);
            string q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and code='" + code + "' and event_datetime<=" + PFSDBUtility.SQLDateTime(evdt);
            if (use_proc_start)
                q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and code='" + code + "' and procedure_start is not null and procedure_start<=" + PFSDBUtility.SQLDateTime(evdt);
            SqlCommand cmd = new SqlCommand(q, db);
            cmd.ExecuteNonQuery();
            db.Close();

        }
        private void DisableSitter(string code, DateTime evdt)
        {
            DisableSitter(code, evdt, false);
        }

        private void UpdatePtChartArraysCode(string code, string ordstatus, DateTime dcdt)
        {
            foreach (var item in _chart_items_since_admission)
            {
                if (item.CODE == code && item.EVENT_DATETIME <= dcdt)
                {
                    item.ORDER_STATUS = ordstatus;
                }
            }
        }

        private bool ProcExistsInDB(int pnum, DateTime startdt, out DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            Program.VerboseAudit("ProcExistsInDB: pnum=" + pnum + " startdt=" + startdt.ToString());
            int ct = 0;
            enddt = DateTime.MinValue;
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            //&& (proc.PROCEDURE_DATETIME <= startdt)
                            && (proc.DEPARTURE_DATETIME > startdt)
                            && (((pnum > 2) && (ans.PROCEDURE_NUMBER == pnum)) || ((pnum <= 2) && (ans.PROCEDURE_NUMBER <= 2)))
                        orderby proc.DEPARTURE_DATETIME descending
                        select new { proc.DEPARTURE_DATETIME };
            ct = query.Count();
            if (ct > 0)
                enddt = (DateTime)query.First().DEPARTURE_DATETIME;
            Program.VerboseAudit("ProcExistsInDB: pnum=" + pnum + " returns " + ct);
            return ct > 0;
        }
        private void CheckProc_3()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MHA3. Off unit accompanied by RN");
            Program.VerboseAudit("---------------");

            int actnum = 3;
            string actcode = "304239042";
            string actlencode = "304239043";
            string actst = "304237002101";
            CheckActivity(actnum, actcode, actst, actlencode);

        }


        private void CheckProc_4()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MHA4. Off unit accompanied by non-RN");
            Program.VerboseAudit("---------------");

        int actnum = 4;
        string actcode = "304239044";
        string actlencode = "304239045";
        string actst = "304237002111";
        CheckActivity(actnum, actcode, actst, actlencode);

        }

        private void AddSimpleProc(int pnum, DateTime evdt, DateTime enddt)
        {
            if (pnum <= 0) return;

            if (ProcExists(pnum, evdt, enddt))
            {
                Program.Audit("Activity " + pnum + ": already exists");
            }
            else
            {
                //if (ActivityFits(evdt, enddt))
                //{
                    ProcOverlapsInDB_PEID(pnum, evdt, enddt); // then delete the db
                    var proc = new proc_data();
                    proc.procedure_number = pnum;
                    proc.start = evdt;
                    proc.finish = enddt;
                    _procs.Add(proc);
                    Program.Audit("Activity " + pnum + ": Found between " + evdt + " and " + enddt);
                //}
            }
        }
        private bool ActivityFits(DateTime beg, DateTime fin)
        {
            bool ok = false;
            int unit_id = 0;
            string sql = "select el.unit_id from ENCOUNTER_LOCATION as el";
            sql += " where el.ENCOUNTER_ID=" + _pat.encounter_id;
            sql += " and el.SPECIAL_UNIT_ID is null";
            sql += " and el.EFFECTIVE_DATETIME_IN<='" + beg.ToString() + "'";
            sql += " and el.EFFECTIVE_DATETIME_OUT>='" + fin.ToString() + "'";

            Program.VerboseAudit(sql);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd = new SqlCommand(sql, db2);
            SqlDataReader dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr2.Read())
            {
                if (dr2["UNIT_ID"] != DBNull.Value)
                    unit_id = PFSDBUtility.DBToInt(dr2["UNIT_ID"]);
            }
            ok = (unit_id > 0);
            db2.Close();
            return ok;
        }
        private bool ProcOverlapsInDB_PEID(int pnum, DateTime startdt, DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            //            LoadPatientProceduresIfNeeded();
            bool overlap_exists = false;
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && ((proc.PROCEDURE_DATETIME >= startdt) && (proc.PROCEDURE_DATETIME < enddt)
                                ||
                                (proc.DEPARTURE_DATETIME > startdt) && (proc.DEPARTURE_DATETIME <= enddt)
                                ||
                                (proc.PROCEDURE_DATETIME < startdt) && (proc.DEPARTURE_DATETIME > enddt)
                                )
                            //&& ( ! (proc.PROCEDURE_DATETIME == startdt) && (proc.DEPARTURE_DATETIME == enddt))
                            && (proc.CLASSIFIED_BY_ID < 0)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new { proc.PROCEDURE_EVENT_ID, proc.PROCEDURE_DATETIME, proc.DEPARTURE_DATETIME };
            overlap_exists = (query.Count() > 0);
            foreach (var a in query)
            {
                Program.VerboseAudit("Will Delete act: procdt=" + a.PROCEDURE_DATETIME.ToString() + "  depdt=" + a.DEPARTURE_DATETIME.ToString());
                Program.VerboseAudit("because it overlays startdt=" + startdt.ToString() + "  enddt=" + enddt.ToString());
                DeleteActivity(a.PROCEDURE_EVENT_ID);
            }
            //            peid = 0;
            return (overlap_exists);
        }
        private void DeleteActivity(int peid)
        {
            //            delete PROCEDURE_ANSWER where PROCEDURE_EVENT_ID=7211351
            //delete RPT_PROC_BY_DAY where PROCEDURE_EVENT_ID=7211351
            //delete PROCEDURE_EVENT where PROCEDURE_EVENT_ID=7211351
            if (peid == 0) return;

            Program.VerboseAudit("db ProcAnsw Deleting peid=" + peid);
            var db = PFSDBUtility.NewPfsDataContext();
            var query = from ia in db.PROCEDURE_ANSWERs
                        where (ia.PROCEDURE_EVENT_ID == peid)
                        select ia;
            if (query.Count() > 0)
            {
                var items = query.ToList();
                foreach (var item in items)
                    db.PROCEDURE_ANSWERs.DeleteOnSubmit(item);
                db.SubmitChanges();
            }

            Program.VerboseAudit("db RptProc Deleting peid=" + peid);
            var db2 = PFSDBUtility.NewPfsDataContext();
            var query2 = from r in db2.RPT_PROC_BY_DAYs
                         where (r.PROCEDURE_EVENT_ID == peid)
                         select r;
            if (query2.Count() > 0)
            {
                var items2 = query2.ToList();
                foreach (var item2 in items2)
                    db2.RPT_PROC_BY_DAYs.DeleteOnSubmit(item2);
                db2.SubmitChanges();
            }

            Program.VerboseAudit("db ProcEvent Deleting peid=" + peid);
            var db3 = PFSDBUtility.NewPfsDataContext();
            var query3 = from ce in db3.PROCEDURE_EVENTs
                         where (ce.PROCEDURE_EVENT_ID == peid)
                         select ce;
            if (query3.Count() > 0)
            {
                var items3 = query3.ToList();
                foreach (var item3 in items3)
                    db3.PROCEDURE_EVENTs.DeleteOnSubmit(item3);
                db3.SubmitChanges();
            }
        }

        private bool QueuedProcOverlaps(int pnum, DateTime startdt, DateTime enddt)
        {
            bool overlap = false;

            proc_data[] pary = _procs.ToArray();
            for (int i = 0; i <= pary.GetUpperBound(0); i++)
            {
                if (pary[i].procedure_number == pnum)
                {
                    //overlap if   p.start between stardt and enddt
                    //             p.finish between startdt and enddt
                    //             startdt >= p.start and enddt <= p.finish
                    if ((pary[i].start >= startdt) && (pary[i].start < enddt) ||
                        (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
                        (startdt >= pary[i].start) && (enddt <= pary[i].finish))
                    {
                        overlap = true;
                    }

                    if ((pary[i].start >= startdt) && (pary[i].start < enddt) && (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
                        (pary[i].start < startdt) && (pary[i].finish > startdt) && (pary[i].finish < enddt)
                        )
                    {
                        // the proc in the list is completely contained in startdt/endt pair
                        // change this proc to be the startdt/enddt
                        // overlap is still true, but the list item will be updated with the encompassing times
                        //Program.Audit("Procedure " + pnum + ": " + startdt + " - " + enddt + " will trump " + pary[i].start + " - " + pary[i].finish);
                        pary[i].start = startdt;
                        pary[i].finish = enddt;
                    }
                }
            }
            _procs = pary.ToList();
            return overlap;
        }


        private bool ProcExists(int pnum, DateTime startdt, DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && (proc.PROCEDURE_DATETIME == startdt)
                            && (proc.DEPARTURE_DATETIME == enddt)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new { proc.PROCEDURE_EVENT_ID };
            return (query.Count() > 0);
        }
        private void CheckProc_5()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MHA5. 1:1 Continuous Observation >1 Hour by RN");
            Program.VerboseAudit("---------------");

            int actnum = 5;
            string actcode = "304239046";
            string actlencode = "304239047";
            string actst = "304237002121";
            CheckActivity(actnum, actcode, actst, actlencode);

        }

        private void CheckProc_6()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MHA6. 1:1 Continuous Observation >1 Hour by non-RN");
            Program.VerboseAudit("---------------");
            int actnum = 6;
            string actcode = "304239048";
            string actlencode = "304239049";
            string actst = "304237002131";
            CheckActivity(actnum, actcode, actst, actlencode);
        }

        private void CheckProc_7()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("MHA7. 2:1 Continuous Observation >1 Hour by RN");
            Program.VerboseAudit("---------------");

            int actnum = 7;
            string actcode = "304239215";
            string actlencode = "304239217";
            string actst = "304237002141";
            CheckActivity(actnum, actcode, actst, actlencode);

        }


        private const string DATETIME_FORMAT = "yyyyMMddHHmm";              // ISO Date/Time w/o seconds

        private void OutputClass(bool use_default)
        {
            string outstr, ind_list, desc, str_pull_dt, str_in_dt, str_out_dt = "";
            int i, tc_event_id;
            bool repeat_output = false;
            string repeat_time = "";

            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         212 242 304
            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1
            //1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901 3456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
            //1       |10 5            |                |                |        |10023121779         |ALEXANDER                       |GLORIA                          |                                |0508    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289530  |           |201403030400                                                                      |NNYNNNNYYNNYNNNYNNNYYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
            //1       |10 5            |                |                |        |10024544908         |ARMSTRONG                       |AARON                           |                                |0535    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289531  |           |201403030400                                                                      |NYNNYNNNNNNYNNNYNNNNYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
            //|0547    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289532  |           |201403030400                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN
            //|0547    |A   |201403030700|                |201403030700    |18  |C|    |3040|1440|18289532  |           |201403030700                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN
            //            1RO | RO MB5BG / 6E |                |                |        | 2000180316769 | PARISIEN | GREYSON | LAWRENCE | RMB5514 | P | 20190319030000 |                               | 20 | C |    | 5399 | 480 | 62040560 |           | 20190319030000 | 20190319070000 | NNNYNNNNNNNNYNNNYNYNYNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN

            if (Program.g_is_test)
                tc_event_id = 9999;
            else
                tc_event_id = PFSDBUtility.NextGID();                         //get a unique id for this class
                                                                              //            str_pull_dt = _pat.pull_finish.ToString(DATETIME_FORMAT);
                                                                              //str_pull_dt = _pat.pull_finish.ToString(DATETIME_FORMAT);
                                                                              //if (_pat.unit_departure != null && _pat.unit_departure != DateTime.MinValue)
            //if (_pat.effective_out == Program.g_pull_finish && _pat.unit_departure == DateTime.MinValue)
            //{
            //    //str_in_dt = Program.g_effdt.ToString(DATETIME_FORMAT);
            //    str_in_dt = loc_in.ToString(DATETIME_FORMAT);
            //    str_out_dt = "";
            //}
            //else
            //{
            //    //str_in_dt = Program.g_effdt.ToString(DATETIME_FORMAT);
            //    str_in_dt = loc_in.ToString(DATETIME_FORMAT);
            //    str_out_dt = "";// _pat.effective_out.ToString(DATETIME_FORMAT);
            //}

            //if (loc_in != Program.g_pull_start_save)
            //{
            //    //str_in_dt = Program.g_effdt.ToString(DATETIME_FORMAT);
            //    str_in_dt = loc_in.ToString(DATETIME_FORMAT);
            //    str_out_dt = "";
            //    str_out_dt = loc_out.ToString(DATETIME_FORMAT);
            //}
            //else
            //{
            //    str_in_dt = Program.g_effdt.ToString(DATETIME_FORMAT);
            //    str_out_dt = "";// _pat.effective_out.ToString(DATETIME_FORMAT);
            //    str_out_dt = loc_out.ToString(DATETIME_FORMAT);
            //}

            //                if (_pat.effective_out == Program.g_pull_finish && _pat.unit_departure == DateTime.MinValue)

            //Program.VerboseAudit("Loc arrtime=" + loc_arrtime + " gpull_start_save=" + Program.g_pull_start_save);
            //if (loc_arrtime >= Program.g_pull_start_save)
            //{
            //    //str_in_dt = Program.g_effdt.ToString(DATETIME_FORMAT);
            //    str_in_dt = loc_arrtime.ToString(DATETIME_FORMAT);
            //    str_out_dt = loc_out.ToString(DATETIME_FORMAT);

            //    if (loc_arrtime <= Program.g_effdt)
            //    {
            //        repeat_output = true;//also make the desired class time
            //        repeat_time = Program.g_effdt.ToString(DATETIME_FORMAT);
            //    }
            //}
            //else
            //{
            //    str_in_dt = Program.g_effdt.ToString(DATETIME_FORMAT);
            //    str_out_dt = "";// _pat.effective_out.ToString(DATETIME_FORMAT);
            //    str_out_dt = loc_out.ToString(DATETIME_FORMAT);
            //}
            //if (!repeat_output)
            //    str_out_dt = "";//leave open-ended 5/3/21


            Program.VerboseAudit("Loc arrtime=" + loc_arrtime + " gpull_start_save=" + Program.g_pull_start_save);
            if (loc_arrtime >= Program.g_effdt.AddHours(-4))
            {
                //str_in_dt = Program.g_effdt.ToString(DATETIME_FORMAT);
                str_in_dt = loc_arrtime.ToString(DATETIME_FORMAT);
                str_out_dt = loc_out.ToString(DATETIME_FORMAT);

                    repeat_output = true;//also make the desired class time
                    repeat_time = Program.g_effdt.ToString(DATETIME_FORMAT);
            }
            else
            {
                str_in_dt = Program.g_effdt.ToString(DATETIME_FORMAT);
                str_out_dt = "";// _pat.effective_out.ToString(DATETIME_FORMAT);
                str_out_dt = loc_out.ToString(DATETIME_FORMAT);
            }
            if (!repeat_output)
                str_out_dt = "";//leave open-ended 5/3/21



            outstr = _pat.short_name.FixedWidth(8);                       //(facility code)
            outstr += "|" + _pat.unit_name.FixedWidth(16);
            outstr += "|" + "".FixedWidth(16);                               //(unit code)
            outstr += "|" + txarea.FixedWidth(16);                               //(area code)
            outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
            outstr += "|" + _pat.acct.FixedWidth(20);
            outstr += "|" + _pat.last_name.FixedWidth(32);
            outstr += "|" + _pat.first_name.FixedWidth(32);
            outstr += "|" + _pat.middle_name.FixedWidth(32);
            outstr += "|" + _pat.room.FixedWidth(8);
            outstr += "|" + _pat.bed.FixedWidth(4);
            outstr += "|" + str_in_dt;        //CLASS dt
            outstr += "|" + "".FixedWidth(14);                               //(login)
            outstr = outstr.FixedWidth(232);
            outstr += "|" + _pat.effective_out.ToString(DATETIME_FORMAT);//str_out_dt;        //TC Data End Point
            outstr = outstr.FixedWidth(249);
            outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
            outstr += "|" + "C".FixedWidth(1);                               //record type = class
            outstr += "|" + "".FixedWidth(4);                                //(stage)
            outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
            outstr += "|" + _pat.range.ToString().FixedWidth(4);             //TC pull range
            outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
            outstr += "|";
            outstr = outstr.FixedWidth(294);
            outstr += "|" + str_in_dt;        //IN
            outstr = outstr.FixedWidth(346);
            //if (_pat.unit_departure != null && _pat.unit_departure != DateTime.MinValue)
            //                outstr += "|" + _pat.unit_departure.ToString(DATETIME_FORMAT);        //OUT
            outstr += "|" + str_out_dt;        //OUT
            outstr = outstr.FixedWidth(377);
            outstr += "|";

            //if (use_default)
            //{ //make all is_checked = false and then mark defaults
            //    Program.VerboseAudit("Patient Will receive default indicators " + _pat.default_inds_str);
            //    for (i = 1; (i <= MAX_INDS); i++)
            //    {
            //        _inds[i].is_checked = false;
            //    }
            //    foreach (var ind in _pat.default_inds)
            //    {
            //        if (ind <= _inds.GetUpperBound(0))
            //        {
            //            _inds[ind].is_checked = true;
            //        }
            //    }
            //}

            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++)
            {
                if (_inds[i].is_checked)
                {
                    outstr += "Y";
                    ind_list += "" + i;
                }
                else
                {
                    outstr += "N";
                }
            } // next i
            ind_list = ind_list.Substring(1);                           //strip leading comma
                                                                        //                                                                                                   1                                                                                                   2                                                                                                   3
                                                                        //         1         2         3         4         5         6         7         8         9         0         1         2         3         4         5         6         7         8         9         0         1         2         3         4         5         6         7         8         9         0         1         2         3         4         5         6         7         8         9         0
                                                                        //1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
                                                                        //1       |DO6D            |                |                |        |2000192224892       |BEHNAM                          |KENDRA                          |LEE                             |RDO6311 |P   |20180717110000|                               |20  |C|    |5399|480 |56103278  |           |20180717110000                                     |                              |YNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN


            //string str5am;
            //Program.outfile.WriteLine(outstr);      //1900yesterd output to transparent.txt
            //if (Program.g_make5am)
            //{
            //    //if (str_out_dt.Substring(8, 4) == "0500") //create the 7am at 3am
            //    {
            //        string strdttm = Program.g_pull_finish.ToString(DATETIME_FORMAT);
            //        strdttm = strdttm.Substring(0, 8) + "0500";
            //        //str5am = outstr.Substring(0, 203) + Program.g_pull_finish.ToString(DATETIME_FORMAT) + outstr.Substring(217, 78) + Program.g_pull_finish.ToString(DATETIME_FORMAT) + " ".Repeat(69) + outstr.Substring(378, 120);
            //        str5am = outstr.Substring(0, 203) + strdttm + outstr.Substring(215, 80) + strdttm + " ".Repeat(71) + outstr.Substring(378, 120);
            //        Program.outfile2.WriteLine(str5am);
            //    }
            //}

            string str5am;
            Program.outfile.WriteLine(outstr);      //1900yesterd output to transparent.txt
            if (repeat_output) // with repeat_time
            {
                string repeat_line = outstr.Substring(0, 203) + repeat_time + outstr.Substring(215, 80) + repeat_time + " ".Repeat(71) + outstr.Substring(378, 120);
                Program.outfile.WriteLine(repeat_line);
            }


            Program.Audit("");
            desc = "Classified: " + ind_list;
            if (Program.g_is_test)
            {
                Program.Audit(desc);
            }
            else
            {
                //Save the selected indicators plus both Program.Audits in the event log; link with tc_event_id
                PFSEventLog.AddTransparentMappingEventLogEntry(
                    desc, Program.gLogUnitID, Program.gLogEncounterID,
                    tc_event_id, Program.gLogMapperVersion,
                    Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
            }
        }

        private void OutputProcs()
        {
            int i;
            string outstr, proc_list, desc;
            int tc_event_id;

            foreach (var proc in _procs)
            {
                if (Program.g_is_test)
                    tc_event_id = 9999;
                else
                    tc_event_id = PFSDBUtility.NextGID();                         //get a unique id for this proc

                outstr = _pat.short_name.FixedWidth(8);                       //(facility code)
                outstr += "|" + _pat.unit_name.FixedWidth(16);
                outstr += "|" + "".FixedWidth(16);                               //(unit code)
                outstr += "|" + txarea.FixedWidth(16);                               //(area code)
                outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
                outstr += "|" + _pat.acct.FixedWidth(20);
                outstr += "|" + _pat.last_name.FixedWidth(32);
                outstr += "|" + _pat.first_name.FixedWidth(32);
                outstr += "|" + _pat.middle_name.FixedWidth(32);
                outstr += "|" + _pat.room.FixedWidth(8);
                outstr += "|" + _pat.bed.FixedWidth(4);
                outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //204 proc dt
                outstr += "|" + "".FixedWidth(14);                               //(login)
                outstr = outstr.FixedWidth(249);
                outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
                outstr += "|" + "P".FixedWidth(1);                               //record type = class
                outstr += "|" + "".FixedWidth(4);                                //(stage)
                outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
                outstr += "|" + _pat.range.ToString().FixedWidth(4);             //TC pull range
                outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
                outstr += "|";
                outstr = outstr.FixedWidth(294);
                outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //296 procdt in
                outstr = outstr.FixedWidth(346);
                //if (_pat.unit_departure != null && _pat.unit_departure != DateTime.MinValue)
                //                outstr += "|" + _pat.unit_departure.ToString(DATETIME_FORMAT);        //OUT
                outstr += "|" + proc.finish.ToString(DATETIME_FORMAT);          //348 procdt out
                outstr = outstr.FixedWidth(377);
                outstr += "|";

                proc_list = "";
                for (i = 1; (i < MAX_PROCS); i++)
                {
                    if (proc.procedure_number == i)
                    {
                        outstr += "Y";
                        proc_list += "," + i;
                    }
                    else
                    {
                        outstr += "N";
                    }
                } // next i
                proc_list = proc_list.Substring(1);                             //strip leading comma

                Program.outfile.WriteLine(outstr);                              //output to transparent.txt

                desc = "Activities: " + proc_list;
                if (Program.g_is_test)
                {
                    Program.Audit(desc);
                }
                else
                {
                    //Save the selected procedures plus both Program.Audits in the event log; link with tc_event_id
                    //(this assumes the procedure Program.Audit is mixed in with the class Program.Audit)
                    PFSEventLog.AddTransparentMappingEventLogEntry(desc, Program.gLogUnitID, Program.gLogEncounterID,
                        tc_event_id, Program.gLogMapperVersion,
                        Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
                }
            } // next proc
        } // OutputProcs

    } //class MentalHealth
}
