﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;                     // for LINQ; add a reference
using System.Data.SqlClient;                // for SqlConnection
using System.IO;                            // for Directory, Path, Stream
using PfsShared;                            // add a reference to Shared2 project

// ============================================================================
// Inpatient transparent mapping -- GOES HERE --
// Albany2022 Soarian
// ============================================================================
// This processes one patient.  Only one database query is used.  :)
//
// All search functions use exact match for category, description and field name.
// Codes and results can be a single word or a list of words.
// Codes default to exact match; prefix with CODE_CODE_LIKE_PREFIX if desired (single word only)
// Results default to like matching; prefix with RESULT_EXACT_MATCH_PREFIX if desired (single word only)
//
// All searches are case insensitive.
//
//
namespace TransparentMapping
{
    class Inpatient
    {
        private const int MAX_INDS = 120;
        private const int MAX_PROCS = 20;

        private const string CODE_LIKE_PREFIX = "%!";       // use to make codes %like%
        private const string EXACT_MATCH_PREFIX = "&!";     // use to make desc/result exact match
        private const string CHAR_COMMA = "||";             // use to insert literal comma in word list
        private const string NOT_PREFIX = "!!";             // use to insert literal comma in word list
        private const string AVOID_NEGATIVE = "!;";
        private const string EXACT_SQL_PREFIX = "!SQL!";     // use to give sql constraint as given
        private const string STARTS_WITH = "[!";     // use to give sql constraint as given

        private struct indicator_data {
            public bool is_checked;
            public int radio_group;
            public int weight;
        }

        private struct proc_data {
            public int procedure_number;
            public DateTime start;
            public DateTime finish;
        }

        // These are database CHART_ITEMs for this patient
        private CHART_ITEM[] _chart_items_since_admission;
        //private CHART_ITEM[] _chart_items_since_unit_arrival;
        //private CHART_ITEM[] _chart_items_during_pull_period;
        //private CHART_ITEM[] _chart_items_pull_period_plus;
        //private CHART_ITEM[] _chart_items_since24hrs;
        //private CHART_ITEM[] _chart_items_since13hrs;
        private CHART_ITEM[] _chart_items_since24hrs;
        //private CHART_ITEM[] _chart_items_since9hrs;
        // These are database PROCEDURE_EVENTs for this patient
        private PROCEDURE_EVENT[] _procedure_events;

        private indicator_data[] _inds;
        private List<proc_data> _procs;
        private List<proc_data> _outcomes;

        private const int DEFAULT_BUCKET_SIZE = 60;       //min

        private PatientInfo _pat;
        private int _bucket_size = DEFAULT_BUCKET_SIZE;
        private bool is_default;
        private string txarea;
        private bool periop_found_inpast13hrs = false;

        private bool exclude_periop_data = false;
        private bool exclude_periph_iv = false;
        private bool coma = false;
        private bool g_toi4 = false;
        private bool g_gitube_attempted = false;
        private bool g_gitube = false;
        private DateTime loc_in;
        private DateTime loc_out;
        private DateTime loc_arrtime;
        int rassct12 = 0;
        int rassct34 = 0;
        int rassctLTzero = 0;
        private LOAtypePrecision[] aryloa = new LOAtypePrecision[10];
        private int numloa = 0;
        private LOAtypePrecision[] ary_hemodial = new LOAtypePrecision[5];
        private int numhemodial = 0;
        private LOAtypePrecision[] aryclassdnc = new LOAtypePrecision[10];
        private int numclassdnc = 0;
        private bool g_lda6yo = false;
        private bool g_iabp = false;

        private string assessgrouplabel = "";

        private enum SearchDepth {
            SearchDefault,
            SearchPullRange,//search within the current pull     -- default
            SearchSinceArrival,         //search since arrival to the unit
            SearchSinceAdmission,        //search everything since admission to the hospital
            SearchPullPlus,
            SearchSince24Hrs,
            SearchSince13Hrs,
            SearchSince12Hrs,
            SearchSince16Hrs,
            SearchSince9Hrs,
            SearchSince4Hrs
        }

        private enum CountMode {
            CountAll,
            CountFirst                  //stop after one is found
        }

        private enum GetValueMode {
            GetTotal,
            GetMax,
            GetLast
        }

        enum Frequencies {
            QNONE,
            Q4H,
            Q2H,
            Q1H,
            Q30M
        }

        private struct fmapRow {
            public double los_high;                       //the LOS being testing
            public int[] freq;                           //the count required for each Q value
        }
        private List<fmapRow> _freq_map;                    //1,2,4,8,12,24 hours

        private struct gBucket
        {
            public int bucket;
            public string code;
            public DateTime evdt;
            public bool using_waiver;
            public bool has_all_deps; //has all dependents
            public int num_addl_items;
            public string description;
        }
        private struct gGap
        {
            public int gap;
            public DateTime evdt1;
            public DateTime evdt2;
        }

        private struct MedChartItem
        {
            public string code;
            public string orderid;
            public DateTime evdt;
            public bool valid;
        }
        //private string[] meds_rate = { "newbag", "new bag", "rateverify", "rate verify", "restarted", "ratechange", "rate change", "started", "continued" };
        //private struct med2026and5077
        //{
        //    public string name;
        //    public bool found;
        //}
        //
        // This is the main entry point
        //
        public void ProcessPatient(PatientInfo pat)
        {
            _pat = pat;
            bool use_default = false;
            bool no_chart_items_in_24hrs = false;
            bool loa_exists = false;
            bool do_class = true;

            InitIndicators(); // sets is_default
            InitProcs();
            if (!is_default)
            {
                LoadFreqTable();
                //CheckIfPeriopInPast13hrs();
                no_chart_items_in_24hrs = (LoadPatientChart() == 0);
                Program.VerboseAudit("New query default scope = " + loc_in + " to " + loc_out);
                if (no_chart_items_in_24hrs)
                    Program.Audit("No chart items received in past 24 hrs.");
                if (!Program.g_onlydnc && do_class)
                {
                    Check_1_2_3_4();
                    Check_5();
                    Check_6_7();
                    Check_8();
                    Check_9();
                    Check_10_11();
                    Check_12_13();
                    Check_14();
                    Check_15_16_17_18();
                    Check_19();
                    Check_20();
                    Check_21_22();
                    Check_23();
                    Check_24();
                    //CheckUserDefined();
                    AtLeastOneADL();
                }
            }

            //if (!no_chart_items_in_24hrs)
            {
                if (!Program.g_onlydnc && do_class)
                {
                    HighestIndicatorInEachGroupWins();

                    if (!is_default)
                    {
                        _pat.ptype = DeterminePtypeOfIndicators();
                        //Program.sttimer();
                        if (!Program.g_noactivities) CheckProcs();
                        //Program._t4 = Program.entimer("t4=", Program._t4);
                        //CheckOutcomes();
                        //if (Program.g_do_OW) CheckOtherWorkload();
                    }

                    if (Program.g_no_output) return;
                    //if (_pat.default_ptype > DeterminePtypeOfIndicators())
                    //{ // if the default pt type is higher than the pt type of this class
                    //  // then if there is a default classification in the past 16 hrs
                    //  // then make these indicators the default indicators.
                    //    use_default = ExistDefaultInPast16hrs();
                    //    if (use_default)
                    //    {
                    //        Program.VerboseAudit("Default indicators will be used");
                    //    }
                    //}
                    //OutputClassAndDNCTimes();
                    OutputClassAndDNC(do_class);
                    if (!Program.g_noactivities) OutputProcs();
                }
                else
                {
                    //if (!Program.g_noactivities) OutputDNC();
                }
                //OutputOutcomes();
            }
        }

        private void CheckIfPeriopInPast13hrs()
        {
            foreach (var perioploc in Program.patperioplist)
            {
                periop_found_inpast13hrs = periop_found_inpast13hrs
                    || (perioploc.in_time >= _pat.pull_finish.AddHours(-13))
                    || (perioploc.out_time >= _pat.pull_finish.AddHours(-13));
            }
        }

        private void InitIndicators()
        {
            // Make a new (empty) indicator array
            _inds = new indicator_data[MAX_INDS + 1];           // This 1 based so add one
            is_default = false;
            txarea = "";
            //if (_pat.los_hours <= 4.0)
            //{
            //    is_default = true;
            //    Program.VerboseAudit("Patient was here 4 hrs or less. Will receive default indicators " + _pat.default_inds_str);
            //    foreach (var ind in _pat.default_inds)
            //    {
            //        if (ind <= _inds.GetUpperBound(0))
            //        {
            //            _inds[ind].is_checked = true;
            //        }
            //    }
            //}


            // get indicator radio groups from the database
            // ** (This database access can be replaced once we have a C# methodolgy cache)
            var db = PFSDBUtility.NewPfsDataContext();
            var query = from idef in db.INDICATOR_DEFINITIONs
                        where (idef.METHODOLOGY_ID == _pat.meth_id)
                        select idef;
            foreach (var idef in query) {
                if (idef.INDICATOR_NUMBER <= _inds.GetUpperBound(0)) {
                    // (convert nulls to zero)
                    _inds[idef.INDICATOR_NUMBER].radio_group = PFSDBUtility.DBToInt(idef.RADIO_GROUP);
                    _inds[idef.INDICATOR_NUMBER].weight = PFSDBUtility.DBToInt(idef.WEIGHT);
                }
            }
        }

        private void InitProcs()
        {
            _procs = new List<proc_data>();
            _outcomes = new List<proc_data>();
        }

        private fmapRow LoadFreqTableRow(double los_high, string values)
        {
            fmapRow fmrow;

            fmrow.los_high = los_high;
            fmrow.freq = new int[Enum.GetNames(typeof(Frequencies)).Length];

            var arr = values.Split(',');
            for (int i = 0; i <= fmrow.freq.GetUpperBound(0); i++)
            {
                fmrow.freq[i] = arr[i].ToInteger();
            }
            return fmrow;
        }

        // These are the number of times you need to do something during a certain time period (LOS)
        // in order for it to be considered one of the frequencies listed.
        private void LoadFreqTable()
        {
            _freq_map = new List<fmapRow>();
            //                              LOS,  None Q4h Q2h Q1h Q30m
            _freq_map.Add(LoadFreqTableRow(1, "    0,  0,  0,  1,  2"));
            _freq_map.Add(LoadFreqTableRow(2, "    0,  0,  1,  2,  4"));
            _freq_map.Add(LoadFreqTableRow(4, "    0,  1,  2,  4,  8"));
            _freq_map.Add(LoadFreqTableRow(6, "    0,  2,  3,  6,  9"));
            _freq_map.Add(LoadFreqTableRow(8, "    0,  2,  4,  8, 12"));
            _freq_map.Add(LoadFreqTableRow(12, "   0,  3,  6, 12, 18"));
            _freq_map.Add(LoadFreqTableRow(16, "   0,  4,  8, 16, 24"));
            _freq_map.Add(LoadFreqTableRow(24, "   0,  6, 12, 24, 36"));
            //New freq table 2/5/14
            //q4	q2	q1	q30     q30
            //            Non-ICU	ICU & SD
            // 4	8	15	29	    36
            // 3	5	9	17	    24
            // 2	4	7	13	    19
            // 2	3	5	10	    13

            //to read this table: if you have at least 6 asessmenets in a 12-hour period, you get Q1h
            //(LOS=12, column Q1h has a count of 6)
        }

        private int LoadPatientChart()
        {
            foreach (var p in Program.patloclist)
            {
                if (p.loc_idx == _pat.loc_idx)
                {
                    loc_in = p.in_time;
                    loc_out = p.out_time;
                    loc_arrtime = p.arr_time;
                }
            }
            _pat.los_hours = PFSUtility.DateDiffInMinutes(loc_in, loc_out) / 60.0;
            Program.VerboseAudit("LoadChart los=" + _pat.los_hours);
            //Program.VerboseAudit("LoadChart unit=" + p.unit_name + " locidx=" + p.loc_idx + " in=" + p.in_time + " out=" + p.out_time);

            int ct_in_24hrs = 0;
            int ctperiop = 0;
            // Get the entire patient chart (all units and dates for this patient)
            var dba = PFSDBUtility.NewPfsDataContext();
            var queryall = from item in dba.CHART_ITEMs
                           where (item.ENCOUNTER_ID == _pat.encounter_id)
                           where (item.EVENT_DATETIME <= loc_out)
                           orderby item.EVENT_DATETIME
                           select item;
            var querya = from g in queryall
                         select g;
            // Save the result
            _chart_items_since_admission = querya.ToArray();
            Program.VerboseAudit("Count since adm=" + querya.Count());


            var query = from item in _chart_items_since_admission
                        where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-24)
                               && (item.EVENT_DATETIME <= _pat.pull_finish))
                        select item;
            // Save the result
            ct_in_24hrs = query.Count();
            _chart_items_since24hrs = query.ToArray();

            // Convert all results to lower case for case insensitive comparisons
            foreach (var item in _chart_items_since24hrs) {
                item.SOURCE_TEXT = null;
                if (item.CATEGORY != null) item.CATEGORY = item.CATEGORY.ToLower();
                if (item.CODE != null) item.CODE = item.CODE.ToLower();
                if (item.DESCRIPTION != null) item.DESCRIPTION = item.DESCRIPTION.ToLower();
                if (item.FIELD_NAME != null) item.FIELD_NAME = item.FIELD_NAME.ToLower();
                if (item.RESULT != null) item.RESULT = item.RESULT.ToLower();
            }

            Program.VerboseAudit("Since 24 hrs count items=" + ct_in_24hrs);
            Program.VerboseAudit("Since 24 hrs count of HD items=" + ctperiop);

            return ct_in_24hrs;
        }

        // Delay looking for patient procedures until they are wanted
        private void LoadPatientProceduresIfNeeded()
        {
            if (_procedure_events != null) return;

            var db = PFSDBUtility.NewPfsDataContext();
            var query = from proc in db.PROCEDURE_EVENTs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                        where (proc.PROCEDURE_DATETIME >= _pat.pull_finish.AddHours(-24))
                        select proc;
            _procedure_events = query.ToArray();
        }



        // Started a new chart item query (of a certain depth)
        // Default is the chart during the pull range.  Options for unit arrival and admission to hospital.
        private IEnumerable<CHART_ITEM> StartNewQuery()
        {
            return StartNewQuery(SearchDepth.SearchDefault);
        }
        private IEnumerable<CHART_ITEM> StartNewQuery(SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;

            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    if (exclude_periop_data)
                        //return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_start && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= loc_in && (item.EVENT_DATETIME <= loc_out)) select item);
                    //return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-12)) select item);
                    else
                        //return (from item in _chart_items_since24hrs where (item.EVENT_DATETIME >= _pat.pull_start && item.EVENT_DATETIME <= _pat.pull_finish) select item);
                        return (from item in _chart_items_since24hrs where (item.EVENT_DATETIME >= loc_in && item.EVENT_DATETIME <= loc_out) select item);
                //return (from item in _chart_items_since24hrs where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-12)) select item);
                //if (exclude_periop_data)
                //    return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-8) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                //else
                //    return (from item in _chart_items_since24hrs where (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-8) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                //    return (from item in _chart_items_since_unit_arrival select item);
                case SearchDepth.SearchSinceAdmission:
                    return (from item in _chart_items_since_admission select item);
                //case SearchDepth.SearchPullPlus:
                //    return (from item in _chart_items_pull_period_plus select item);
                case SearchDepth.SearchSince24Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                    else
                        return (from item in _chart_items_since24hrs where (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince16Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-16) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since24hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-16) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince13Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-13) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since24hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-13) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince12Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-12) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since24hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-12) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince9Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-9) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since24hrs where item.EVENT_DATETIME >= _pat.pull_finish.AddHours(-9) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
                case SearchDepth.SearchSince4Hrs:
                    if (exclude_periop_data)
                        return (from item in _chart_items_since24hrs where item.UNIT_ID != -6 && (item.EVENT_DATETIME > _pat.pull_finish.AddHours(-4) && (item.EVENT_DATETIME <= _pat.pull_finish)) select item);
                    else
                        return (from item in _chart_items_since24hrs where item.EVENT_DATETIME > _pat.pull_finish.AddHours(-4) && (item.EVENT_DATETIME <= _pat.pull_finish) select item);
            }
            return null;
        }

        // Is this search term a word or a list of words?
        private bool ValueIsAList(string s)
        {
            return (s != null) && s.Contains(',');                //is this a comma-separated list?
        }

        // Break apart a wordlist and prepare its members
        private string[] SplitOnCommaAndPrepareElements(string s)
        {
            // If a word list has a space after each comma, we will get leading blanks in each term
            var arr = s.Split(',');
            for (int i = 0; i <= arr.GetUpperBound(0); i++) {
                arr[i] = arr[i].Trim();                         // get rid of leading blanks
                arr[i] = arr[i].Replace(CHAR_COMMA, "");       // insert a literal comma into search word
                arr[i] = arr[i].ToLower();                      // convert for case-insensitive comparisons
            }
            return arr;
        }

        // Add a basic chart item filter to a LINQ query
        //
        // cat = exact match
        // code_list = exact match; optional "like" match
        // desc_list = "like" match; optional exact match
        // field = exact match
        // result_list = "like" match; optional exact match
        //
        // These are comma-separated lists.  
        // Search for a literal comma by using CHAR_COMMA in the string.
        private IEnumerable<CHART_ITEM> AndItemFilter(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list)
        {
            // Converted all values to lower case for case-insensitive comparisons.
            if (!String.IsNullOrEmpty(cat))
            {
                cat = cat.ToLower();
                query = query.Where(e => e.CATEGORY.ToLower() == cat);
            }

            if (!String.IsNullOrEmpty(code_list))
            {
                code_list = code_list.ToLower();
                if (code_list.Left(2) == CODE_LIKE_PREFIX)
                {            // override for "like" match?
                    query = query.Where(e => e.CODE.Contains(code_list.Substring(2)));
                }
                else if (code_list.Left(2) == EXACT_MATCH_PREFIX)
                {
                    query = query.Where(e => e.CODE == code_list.Substring(2));
                }
                else if (ValueIsAList(code_list))
                {
                    query = AndCodeInList(query, code_list);            // find one of the words
                }
                else
                {
                    query = query.Where(e => e.CODE.StartsWith(code_list));      // find this word
                }
            }

            if (!String.IsNullOrEmpty(desc_list))
            {
                desc_list = desc_list.ToLower();
                if (desc_list.Left(2) == EXACT_MATCH_PREFIX)
                {          // exact match override?
                    query = query.Where(e => e.DESCRIPTION == desc_list.Substring(2));
                }
                else if (desc_list.Left(2) == NOT_PREFIX)
                {
                    // query = query.Where(e => !e.DESCRIPTION.Contains(desc_list.Substring(2)));
                    query = AndDescriptionNOTInList(query, desc_list.Substring(2));
                }
                else if (ValueIsAList(desc_list))
                {
                    query = AndDescriptionInList(query, desc_list);
                }
                else
                {
                    desc_list = desc_list.Replace(CHAR_COMMA, "");     // look for literal comma requests
                    query = query.Where(e => e.DESCRIPTION.Contains(desc_list));
                }
            }

            if (!String.IsNullOrEmpty(field))
            {
                field = field.ToLower();
                query = query.Where(e => e.FIELD_NAME == field);
            }

            if (!String.IsNullOrEmpty(result_list))
            {
                query = query.Where(e => e.RESULT != null);
                result_list = result_list.ToLower();                        // make all results case insensitve
                if (result_list.Left(2) == EXACT_MATCH_PREFIX)
                {     // exact match override?
                    query = query.Where(e => e.RESULT == result_list.Substring(2));
                }
                else if (result_list.Left(2) == AVOID_NEGATIVE)
                {
                    query = query.Where(e => (!e.RESULT.Contains("no " + result_list.Substring(2))) && ((e.RESULT == result_list.Substring(2)) || (e.RESULT.Contains(";" + result_list.Substring(2)))));
                }
                else if (ValueIsAList(result_list))
                {
                    query = AndResultInList(query, result_list);
                }
                else if (result_list.Left(2) == STARTS_WITH)
                {
                    query = query.Where(e => e.RESULT.StartsWith(result_list.Substring(2)));
                }
                else
                {
                    result_list = result_list.Replace(CHAR_COMMA, "");     // look for literal comma requests
                    query = query.Where(e => e.RESULT.Replace(",", "").Contains(result_list));
                }
            }
            return query;
        }

        // Look for a code that is one of the words in the list.
        private IEnumerable<CHART_ITEM> AndCodeInList(IEnumerable<CHART_ITEM> query, string code_list)
        {
            if (String.IsNullOrEmpty(code_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(code_list);
            //            return query.Where(e => arr.Any(x => x.Contains(e.CODE))); // "like" match
            return query.Where(e => e.CODE.ContainsAny(arr)); // "like" match
        }

        // Look for a description that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndDescriptionInList(IEnumerable<CHART_ITEM> query, string desc_list)
        {
            if (String.IsNullOrEmpty(desc_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(desc_list);
            //return query.Where(e => arr.Any(x => x.Contains(e.DESCRIPTION))); // "like" match
            query = query.Where(e => e.DESCRIPTION.ContainsAny(arr)); // "like" match
            //Program.VerboseAudit("AndDescriptionInList ct=" + query.Count() + " desclist="+desc_list);
            return query;
            //return query.Where(e => e.DESCRIPTION.ContainsAny(arr)); // "like" match
        }
        private IEnumerable<CHART_ITEM> AndDescriptionNOTInList(IEnumerable<CHART_ITEM> query, string desc_list)
        {
            if (String.IsNullOrEmpty(desc_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(desc_list);
            //return query.Where(e => arr.Any(x => x.Contains(e.DESCRIPTION))); // "like" match
            return query.Where(e => !e.DESCRIPTION.ContainsAny(arr)); // "like" match
        }

        // Look for a result that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultInList(IEnumerable<CHART_ITEM> query, string result_list)
        {
            bool use_start_with = false;

            if (String.IsNullOrEmpty(result_list)) return query;

            if (result_list.Left(2) == STARTS_WITH)
            {
                use_start_with = true;
                result_list = result_list.Substring(2);
            }

            var arr = SplitOnCommaAndPrepareElements(result_list);
            //            return query.Where(e => arr.Any(x => x.Contains(e.RESULT))); // "like" match
            if (use_start_with)
                return query.Where(e => arr.Any(item => e.RESULT.StartsWith(item)));
            else
                return query.Where(e => e.RESULT.ContainsAny(arr)); // "like" match
                                                                    //query = query.Where(e => (meds_mr2026.Any(item => e.DESCRIPTION.ToUpper().StartsWith(item))
                                                                    //return query.Where(e => arr.Any(item => e.RESULT.ToLower().Contains(item.ToLower())));
        }

        // Look for a result that does not contain one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultNotInList(IEnumerable<CHART_ITEM> query, string result_list)
        {
            if (String.IsNullOrEmpty(result_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(result_list);
            //            return query.Where(e => !arr.Any(x => x.Contains(e.RESULT))); // "like" match
            return query.Where(e => !e.RESULT.ContainsAny(arr)); // "like" match
        }

        private string DescribeSearchDepth(SearchDepth search_depth)
        {
            string result = "";
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;

            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    //result = "in pull range"
                    result = "";                         //be quiet since this is default
                    break;
                case SearchDepth.SearchSinceArrival:
                    result = "since arrival to unit";
                    break;
                case SearchDepth.SearchSinceAdmission:
                    result = "since admission";
                    break;
                case SearchDepth.SearchPullPlus:
                    result = "in LOS range plus 4 hours after";
                    break;
                case SearchDepth.SearchSince24Hrs:
                    result = "since 24 hours ago";
                    break;
                case SearchDepth.SearchSince16Hrs:
                    result = "since 16 hours ago";
                    break;
                case SearchDepth.SearchSince13Hrs:
                    result = "since 13 hours ago";
                    break;
                case SearchDepth.SearchSince9Hrs:
                    result = "since 9 hours ago";
                    break;
                case SearchDepth.SearchSince4Hrs:
                    result = "since 4 hours ago";
                    break;
            }

            return result;
        }

        // Describe a query that has already been run
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Make a simple query and describe the results
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            var arr = query.ToArray();
            return Describe(arr, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private string LookingFor(string so_far, string what, string op, string value)
        {
            string result;
            if (String.IsNullOrEmpty(value)) return so_far;

            if (so_far == "")
                result = "look for " + what;
            else
                result = so_far + " and " + what;

            if (value.Left(2) == EXACT_MATCH_PREFIX)
                result += "=" + value.Substring(2) + "";
            else if (value.Left(2) == CODE_LIKE_PREFIX)
                result += " contains " + "" + value.Substring(2) + "";
            else if (ValueIsAList(value))
                result += " in " + value + "";
            else
                result += op + "" + value + "";

            return result;
        }
        private string Describe(CHART_ITEM[] arr, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string result = "";
            //result = LookingFor(result, "cat", "=", cat);
            result = LookingFor(result, "code", "=", code_list);
            //result = LookingFor(result, "desc", " contains ", desc_list.Left(20));
            //result = LookingFor(result, "field", "=", field);
            //result = LookingFor(result, "result", " contains ", result_list.Left(20));

            if (arr.Count() == 0) {
                result += "; not found " + DescribeSearchDepth(search_depth);
            } else {
                var e = arr[0];
                result = "FOUND: " + result;
                // We might have searched for a pattern or word list in several fields - show what was found
                //if (e.CATEGORY != null && e.CATEGORY != "") result += " cat=" + e.CATEGORY + "";
                if (e.CODE != null && e.CODE != "") result += " code=" + e.CODE + "";
                if (e.DESCRIPTION != null && e.DESCRIPTION != "") result += " desc=" + e.DESCRIPTION + "";
                //if (e.FIELD_NAME != null && e.FIELD_NAME != "") result += " field=" + e.FIELD_NAME + "";
                if (e.RESULT != null && e.RESULT != "") result += " result=" + e.RESULT + "";
                // Are there more results?  Just say how many; we aren't going to list them.
                if (arr.Count() == 2) {
                    result += " (1 more result)";
                } else if (arr.Count() > 2) {
                    result += " (" + (arr.Count() - 1) + " more results)";
                }
            }

            return result;
        }


        //Set an indicator for this reason (low level)
        private void SetInd(int inum, string reason)
        {
            if (inum <= 0)
            {
                Program.VerboseAudit(reason); // dont set indicator, just output reason
                return;
            }
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Set Ind #" + inum + ": " + reason);          //already set - repeat for verbose only
            } else {
                _inds[inum].is_checked = true;
                Program.Audit("Set Ind #" + inum + ": " + reason);
            }
        }

        //Clear an indicator for this reason (low level)
        private void ClrInd(int inum, string reason)
        {
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Clr Ind #" + inum + ": " + reason);          //already clear - repeat verbose only
            } else {
                _inds[inum].is_checked = false;
                Program.Audit("Clr Ind #" + inum + ": " + reason);
            }
        }

        // Count how many items meet the conditions
        // All items are listed
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountItems(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)

        {
            bool first = true;
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            if (exclude_periph_iv)
                query = AndDescriptionNOTInList(query, "peripheral iv");
            int count = query.Count();
            found_what = "";
            if (count > 0 && trace)
            {
                foreach (var item in query)
                {
                    if (first)
                    {
                        // always return what was found
                        //            found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                        found_what = "Found desc=" + item.DESCRIPTION + ";result=" + item.RESULT + ";code=" + item.CODE + ";evdt=" + item.EVENT_DATETIME + ";items found=" + count;
                        // echo the result?
                        Program.VerboseAudit(found_what);
                        first = false;
                    }

                }
            }

            return count;
        }


        // Count how many items meet the conditions
        // This has the option to list every entry or give a total
        private int CountResultInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            int count = 0;
            found_what = "";
            string s = "";

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            if (exclude_periph_iv)
                query = AndDescriptionNOTInList(query, "peripheral iv");
            var arr = SplitOnCommaAndPrepareElements(result_list);
            //Walker Schlundt
            //Set Ind #9: found '' in cat='' code='301870' field='' result='oriented x4' 

            foreach (var item in query) {
                // Figure out which of the search words were found
                for (int i = 0; i <= arr.GetUpperBound(0); i++)
                {
                    if (item.RESULT.Contains(arr[i]))
                    {
                        count++;
                        s = arr[i];
                        found_what = "Found desc:" + item.DESCRIPTION + ";result=" + item.RESULT + ";code=" + item.CODE + "; evdt="+item.EVENT_DATETIME;
                        //if (trace) Program.VerboseAudit(found_what);
                        Program.VerboseAudit(found_what);
                    }
                }
                if (count_mode != CountMode.CountAll) break;        //stop counting

            }

            //if (count > 0) {
            //    //We already printed what was found; maybe add how many?
            //    if (trace && count > 0) Program.VerboseAudit("found " + count + " total");
            //} else {
            //    // Describe what was *not* found
            //    //found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
            //    //if (trace) Program.VerboseAudit(found_what);
            //}

            return count;
        }

        // Count how many items meet the conditions
        // This has the option to list every entry or give a total
        private int CountResultInListEXCEPTList(string cat, string code_list, string desc_list, string field, string result_list, string negresult_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            int count = 0;
            found_what = "";

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            query = AndResultNotInList(query, negresult_list);

            var arr = SplitOnCommaAndPrepareElements(result_list);

            foreach (var item in query)
            {
                // Figure out which of the search words were found
                var s = item.RESULT.ContainsWhich(arr);
                found_what = "found " + s + "' in cat=" + item.CATEGORY + "' code=" + item.CODE + "' field=" + item.FIELD_NAME + "' result=" + item.RESULT + "";
                if (trace) Program.VerboseAudit(found_what);
                count++;
                if (count_mode != CountMode.CountAll) break;        //stop counting
            }

            if (count > 0)
            {
                //We already printed what was found; maybe add how many?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            }
            else
            {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        // Count how many items contain ALL results in list
        private int CountResultInListAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            int count = 0;
            found_what = "";

            var arr = SplitOnCommaAndPrepareElements(result_list);

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            Program.VerboseAudit("arr ub=" + arr.GetUpperBound(0));
            for (int i = 0; i <= arr.GetUpperBound(0); i++)
            {
                if (arr[i].StartsWith(NOT_PREFIX))
                {
                    //query = query.Where(e => !e.RESULT.Contains(arr[i].Substring(2)));
                    query = AndResultNotInList(query, arr[i]);
                }
                else
                {
                    Program.VerboseAudit(i + ":" + arr[i]);
                    //query = query.Where(e => e.RESULT.Contains(arr[i]));
                    query = AndResultInList(query, arr[i]);
                }
            }
            //            Program.VerboseAudit("out of for loop");

            count = query.Count();
            //          Program.VerboseAudit("query count = " +count);

            if (count > 0)
            {
                found_what = "found item with all results in " + result_list + "' without those prefixed by !!";
                if (trace) Program.VerboseAudit(found_what);
            }
            else
            {
                // Describe what was *not* found
                //found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                //if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            if (ValueIsAList(result_list))
            {
                return CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what);
            } else {
                return CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what);
            }
        }

        // Is there an item with this result?
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContains(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            if (ValueIsAList(result_list))
            {
                return (CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            } else {
                return (CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
            }
        }

        // Result contains all in list
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            if (ValueIsAList(result_list))
            {
                return (CountResultInListAll(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            }
            else
            {
                found_what = "Not valid for non-list results.";
                return false;
            }
        }
        // Count how many items meet the conditions, with none of the results in result_list.
        // This has the option to say how many were found rather than list all.
        private int CountResultNotInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            int count = 0, rec_count = 0;
            bool found_one;

            found_what = "";
            var arr = SplitOnCommaAndPrepareElements(result_list);         // also deals with CHAR_COMMA

            //Do not filter by result here; we need all results
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            foreach (var item in query) {
                rec_count++;
                //Look for each search word in the result
                found_one = false;

                foreach (string s in arr) {
                    //                    if (String.Equals(item.RESULT, s)) {
                    if (item.RESULT.Contains(s))
                    {
                        found_what = "found " + s + ";result=" + item.RESULT + " -- exclude this";
                        Program.VerboseAudit(found_what);
                        found_one = true;
                        break;
                    }
                }

                if (!found_one) {
                    //none of the words were found - good!
                    found_what = Describe(cat, code_list, desc_list, field, "", search_depth) + " -- does not contain " + result_list + "";
                    Program.VerboseAudit(found_what);
                    count++;
                    if (count_mode != CountMode.CountAll) break;
                }
            }

            if (count > 0) {
                //We already printed what was found; maybe show the total?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else if (rec_count > 0) {
                //We already printed what we ignored
            } else {
                //Print what did not exist
                found_what = Describe(cat, code_list, desc_list, field, "", search_depth);      //not found
                Program.VerboseAudit(found_what);
            }

            return count;
        }

        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, string found_what)
        {
            // Search in the pull range with trace on
            return CountResultDoesNotContain(cat, code_list, desc_list, field, result_list, out found_what, SearchDepth.SearchDefault, true);
        }
        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, out string found_what, SearchDepth search_depth, bool trace)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            return CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0;
        }

        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            return (CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
        }

        // Set the indicator if the conditions are met
        //
        private bool SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SearchDepth s = SearchDepth.SearchDefault;
            //if (inum == 5) s = SearchDepth.SearchSince13Hrs;
            //if (inum == 6) s = SearchDepth.SearchSince13Hrs;
            //if (inum == 7) s = SearchDepth.SearchSince13Hrs;
            //if (inum == 8) s = SearchDepth.SearchSince16Hrs;
            //if (inum == 9) s = SearchDepth.SearchSince16Hrs;
            //if (inum == 10) s = SearchDepth.SearchSince13Hrs;
            //if (inum == 11) s = SearchDepth.SearchSince9Hrs;
            //if (inum == 12) s = SearchDepth.SearchSince16Hrs;
            //if (inum == 13) s = SearchDepth.SearchSince16Hrs;
            //if (inum == 14) s = SearchDepth.SearchSince24Hrs;
            //if (inum == 19) s = SearchDepth.SearchSince9Hrs;
            //if (inum == 20) s = SearchDepth.SearchSince9Hrs;
            //if (inum == 21) s = SearchDepth.SearchSince16Hrs;
            //if (inum == 22) s = SearchDepth.SearchSince13Hrs;
            //if (inum == 23) s = SearchDepth.SearchSince9Hrs;
            //if (inum == 24) s = SearchDepth.SearchSince9Hrs;
            return SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, s);

        }
        private bool SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            bool b = false;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return true;

            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what)) {
                SetInd(inum, found_what);                          //echo here - found
                b = true;
            } else {
                //   Program.VerboseAudit(found_what);                  //and here - not found
            }
            return b;
        }

        private bool SetIndIfResultBetween(int inum, string cat, string code_list, string desc_list, string field, int loval, int hival)
        {
            return SetIndIfResultBetween(inum, cat, code_list, desc_list, field, loval, hival, SearchDepth.SearchDefault);
        }

        private bool SetIndIfResultBetween(int inum, string cat, string code_list, string desc_list, string field, int loval, int hival, SearchDepth search_depth)
        {
            int count = 0;
            string found_what = "";

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");
            foreach (var item in query)
            {
                Program.VerboseAudit("ResBetween: code=" + item.CODE + " result=" + item.RESULT);
                if (item.RESULT.IsNumeric())
                {
                    Program.VerboseAudit("  result is numeric");
                    if (item.RESULT.Val() >= loval && item.RESULT.Val() <= hival)
                    {
                        count++;
                        found_what = "found code=" + item.CODE + " with result=" + item.RESULT;
                        Program.VerboseAudit(found_what);
                    }
                }
            }

            if (count > 0)
            {
                SetInd(inum, found_what);
            }
            return (count > 0);
        }


        // Special contains all
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SearchDepth s = SearchDepth.SearchDefault;
            //if (inum == 8) s = SearchDepth.SearchSince24Hrs;
            SetIndIfResultContainsAll(inum, cat, code_list, desc_list, field, result_list, s);
        }
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return;

            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                          //echo here - found
            }
            else
            {
                //Program.VerboseAudit(found_what);                  //and here - not found
            }
        }
        // Set the indicator if the result does not contain any of the words in result_list
        //
        private void SetIndIfResultDoesNotContain(int inum, string cat, string desc_list, string code_list, string field, string result_list)
        {
            SetIndIfResultDoesNotContain(inum, cat, desc_list, code_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultDoesNotContain(int inum, string cat, string desc_list, string code_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return;

            //Turn trace off for ResultDoesNotContain() and echo what was set below with SetInd
            if (ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                           //echo here - not found
            } else {
                //Program.VerboseAudit(found_what);                   //and here - found
            }
        }

        // Clear the indicator if the result contains one of the words in the result_list
        //
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            string found_what;
            //avoid more queries if the indicator is already clear
            if (!_inds[inum].is_checked) return;

            //Turn trace off for ResultContains() and echo what was set below with SetInd
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what)) {
                ClrInd(inum, found_what);                           //echo here - found
            } else {
                Program.VerboseAudit(found_what);                   //and here - not found
            }
        }


        //These slightly smaller functions are meant for places where you aren't really looking for a chart result
        private bool Exists(string desc_list, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists("", "", desc_list, "", result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool Exists(string desc_list, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists("", "", desc_list, "", result_list, search_depth, true, out found_what);
        }
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            return (CountResultContains(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
        }

        // Variations with optional parameters (can switch to optional parameters in VS 2010)
        //
        private void SetIndIfFound(int inum, string cat)
        {
            SetIndIfResultContains(inum, cat, "", "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list)
        {
            SetIndIfResultContains(inum, cat, code_list, "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private void ClrIndIfFound(int inum, string cat)
        {
            ClrIndIfResultContains(inum, cat, "", "", "", "", SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Get the max/total value from a result (usually in the middle of the text)
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetIntValue(get_mode, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            int value;
            int result = 0;
            bool found_one = false;

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);

            //Look for a number in the result

            foreach (var item in query) {
                // RESULT might be null so convert to empty string so Split won't blow up
                var arr = PFSDBUtility.DBToString(item.RESULT).Split(' ');
                foreach (string s in arr) {
                    //Look for 1st character numeric; IsNumeric("60min") will fail so look at 1st char only
                    if (s.Left(1).IsNumeric()) {
                        value = (int)s.Val();                           //Use Val; ToInteger will error on "60min"
                        switch (get_mode) {
                            case GetValueMode.GetMax:
                                result = Math.Max(value, result);       //max
                                break;
                            case GetValueMode.GetTotal:
                                result += value;                        //total
                                break;
                            case GetValueMode.GetLast:
                                result = value;                         //last
                                break;
                        }

                        // print what we are searching for (the first time)
                        if (!found_one) {
                            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
                        }
                        found_one = true;
                        // print each value found
                        Program.VerboseAudit("  found numeric value " + result);
                        //Keep going in case there are more
                    }
                }
            }

            if (!found_one) {
                //show what was not found
                Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
            }

            return result;
        }

        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetMaxValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            return GetIntValue(GetValueMode.GetMax, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetTotalValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            return GetIntValue(GetValueMode.GetTotal, cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Get a result; returns true if found with return_result
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result)
        {
            return GetResult(cat, code_list, desc_list, field, out return_result, SearchDepth.SearchDefault);
        }
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0) {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
            } else {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }

        private bool GetLatestResult(string code_list, out string return_result, SearchDepth search_depth)
        {
            return GetLatestResult("", code_list, "", "", out return_result, search_depth);
        }
        private bool GetLatestResult(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");
            query = query.OrderByDescending(e => e.EVENT_DATETIME);

            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT) + " charted at " + query.First().EVENT_DATETIME;
            }
            else
            {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }

        private bool GetResultForInspection(string cat, string code_list, string desc_list, string field, string result, out string return_result, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result);

            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
            }
            else
            {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }

        private void GetResultforOther(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "Other");
            return_result = "";
            foreach (var item in query)
            {
                if (!String.Equals(item.RESULT, "Other"))
                {
                    return_result = item.RESULT;
                    //Program.VerboseAudit(found_what);
                    break;
                }

            }
            return;
        }

        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, string res, out string return_result, out DateTime return_evdt)
        {
            return GetResultAndEVDT(cat, code_list, desc_list, field, res, out return_result, out return_evdt, SearchDepth.SearchDefault);
        }
        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, string res, out string return_result, out DateTime return_evdt, SearchDepth search_depth)
        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, res);

            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
                return_evdt = PFSDBUtility.DBToDateTime(query.First().EVENT_DATETIME);
            }
            else
            {
                return_result = "";
                return_evdt = DateTime.MinValue;
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }
        private bool GetEVDT(string cat, string code_list, string desc_list, string res, string field, int comparison, DateTime compevdt, out DateTime return_evdt)
        {
            return GetEVDT(cat, code_list, desc_list, field, res, comparison, compevdt, out return_evdt, SearchDepth.SearchDefault);
        }
        private bool GetEVDT(string cat, string code_list, string desc_list, string field, string res, int comparison, DateTime compevdt, out DateTime return_evdt, SearchDepth search_depth)
        {
            bool sort_ascending = false;
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, res);
            if (compevdt != DateTime.MinValue)
            {
                if (comparison == 0) // equal
                    query = query.Where(e => e.EVENT_DATETIME == compevdt);
                else if (comparison == 1) // GTE
                    query = query.Where(e => e.EVENT_DATETIME >= compevdt);
                else if (comparison == 2) // GT
                    query = query.Where(e => e.EVENT_DATETIME > compevdt);
                else if (comparison == 22) // Least GT
                {
                    query = query.Where(e => e.EVENT_DATETIME > compevdt);
                    sort_ascending = true;
                }
                else if (comparison == 3) // LT
                    query = query.Where(e => e.EVENT_DATETIME < compevdt);
            }
            if (sort_ascending)
                query = query.OrderBy(e => e.EVENT_DATETIME);
            else
                query = query.OrderByDescending(e => e.EVENT_DATETIME);
            Program.VerboseAudit("getevdt count=" + query.Count());
            if (query.Count() > 0)
            {
                return_evdt = PFSDBUtility.DBToDateTime(query.First().EVENT_DATETIME);
            }
            else
            {
                return_evdt = DateTime.MinValue;
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, res, search_depth));
            return (return_evdt != DateTime.MinValue);
        }

        private int SetIndIfCodeBtwn(int ind, int locode, int hicode, SearchDepth search_depth)

        {
            if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = query.Where(e => e.CODE.ToLower().StartsWith("edu"));
            query = query.Where(e => e.CODE.Length >= 12);
            query = query.Where(e => e.CODE.Substring(3, 9).Val() >= locode);
            query = query.Where(e => e.CODE.Substring(3, 9).Val() <= hicode);
            int count = query.Count();
            // always return what was found
            string found_what = "There were " + count + " items found with code between " + locode + " and " + hicode;
            if (count >= 8) SetInd(ind, found_what);
            // echo the result?
            //               if (trace) Program.VerboseAudit(found_what);

            return count;
        }

        private DateTime LatestEVDT(string code_list, string desc_list, string res, SearchDepth search_depth)
        {
            DateTime latestdt = DateTime.MinValue;

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, "", code_list, desc_list, "", res);
            query = query.OrderByDescending(e => e.EVENT_DATETIME);
            int ct = query.Count();

            if (ct > 0)
            {
                latestdt = PFSDBUtility.DBToDateTime(query.First().EVENT_DATETIME);
            }
            else
            {
                latestdt = DateTime.MinValue;
            }
            return latestdt;
        }

        private int CountItemsWithinNhours(string code_list, string result_list, SearchDepth search_depth, int Nhrs)
        {
            int ctwithin = 0;
            int ct = 0;
            DateTime evdt = DateTime.MinValue;

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, "", code_list, "", "", result_list);
            query = query.OrderBy(e => e.EVENT_DATETIME);
            int count = query.Count();
            if (count > 0)
            {
                foreach (var item in query)
                {
                    if (item.EVENT_DATETIME > evdt)
                    {
                        evdt = item.EVENT_DATETIME;

                        var q1 = StartNewQuery(search_depth);
                        q1 = AndItemFilter(q1, "", code_list, "", "", result_list);
                        q1 = q1.Where(e => e.EVENT_DATETIME >= item.EVENT_DATETIME && e.EVENT_DATETIME <= item.EVENT_DATETIME.AddHours(Nhrs));

                        ct = q1.Count();
                        if (ct > ctwithin) ctwithin = ct;
                    }
                }
            }
            return ctwithin;
        }

        private bool GetEVDTIfAllResults(string code_list, string result_list, int comparison, DateTime compevdt, out DateTime return_evdt, SearchDepth search_depth)
        {
            var arr = SplitOnCommaAndPrepareElements(result_list);

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, "", code_list, "", "", "");
            for (int i = 0; i <= arr.GetUpperBound(0); i++)
            {
                query = AndResultInList(query, arr[i]);
            }
            int count = query.Count();

            if (compevdt != DateTime.MinValue)
            {
                if (comparison == 0) // equal
                    query = query.Where(e => e.EVENT_DATETIME == compevdt);
                else if (comparison == 1) // GTE
                    query = query.Where(e => e.EVENT_DATETIME >= compevdt);
                else if (comparison == 2) // GT
                    query = query.Where(e => e.EVENT_DATETIME > compevdt);
                else if (comparison == 22) // Least GT
                {
                    query = query.Where(e => e.EVENT_DATETIME > compevdt);
                }
                else if (comparison == 3) // LT
                    query = query.Where(e => e.EVENT_DATETIME < compevdt);
            }
            query = query.OrderBy(e => e.EVENT_DATETIME);
            Program.VerboseAudit("getevdt count=" + query.Count());
            if (query.Count() > 0)
            {
                return_evdt = PFSDBUtility.DBToDateTime(query.First().EVENT_DATETIME);
            }
            else
            {
                return_evdt = DateTime.MinValue;
            }

            Program.VerboseAudit(Describe("", code_list, "", "", "", search_depth));
            return (return_evdt != DateTime.MinValue);
        }

        //======================================================================================
        // Mapping starts here
        //======================================================================================
        private void Check_1_2_3_4()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("1. ADL Self");
            Program.VerboseAudit("2. ADL Assist");
            Program.VerboseAudit("3. ADL Extended");
            Program.VerboseAudit("4. ADL Complete");
            Program.VerboseAudit("---------------");

            bool mobraw1 = SetIndIfResultBetween(1, "", "MOBRAWSC", "", "", 22, 24, SearchDepth.SearchSince24Hrs);
            bool actraw1 = SetIndIfResultBetween(1, "", "ACTRAWSC", "", "", 22, 24, SearchDepth.SearchSince24Hrs);
            bool mobraw2 = SetIndIfResultBetween(2, "", "MOBRAWSC", "", "", 18, 21, SearchDepth.SearchSince24Hrs);
            bool actraw2 = SetIndIfResultBetween(2, "", "ACTRAWSC", "", "", 18, 21, SearchDepth.SearchSince24Hrs);
            bool mobraw3 = SetIndIfResultBetween(3, "", "MOBRAWSC", "", "", 13, 17, SearchDepth.SearchSince24Hrs);
            bool actraw3 = SetIndIfResultBetween(3, "", "ACTRAWSC", "", "", 13, 17, SearchDepth.SearchSince24Hrs);
            bool mobraw4 = SetIndIfResultBetween(4, "", "MOBRAWSC", "", "", 6, 12, SearchDepth.SearchSince24Hrs);
            bool actraw4 = SetIndIfResultBetween(4, "", "ACTRAWSC", "", "", 6, 12, SearchDepth.SearchSince24Hrs);

            // 4. COMPLETE CARE

            if (_pat.age <= 5.0)
            {
                SetInd(4, "Age <=5 years");
            }
            if (_inds[4].is_checked) return;

            SetIndIfResultContains(4, "", "COMPDEPEN", "", "", "Yes");
            string reslist = "Complete Care";
            bool dress4 = ResultContains("", "DRESSING", "", "", reslist, SearchDepth.SearchSince24Hrs);
            bool oral4 = ResultContains("", "ORALHYG", "", "", reslist, SearchDepth.SearchSince24Hrs);
            bool toil4 = ResultContains("", "TOILETING", "", "", reslist, SearchDepth.SearchSince24Hrs);
            bool groom4 = ResultContains("", "GROOMING", "", "", reslist, SearchDepth.SearchSince24Hrs);
            bool eat4 = ResultContains("", "EATING", "", "", reslist, SearchDepth.SearchSince24Hrs);
            bool bath4 = ResultContains("", "BATHING", "", "", reslist, SearchDepth.SearchSince24Hrs);
            //if ((dress4 ? 1 : 0) + (oral4 ? 1 : 0) + (toil4 ? 1 : 0) + (groom4 ? 1 : 0) + (eat4 ? 1 : 0) == 5)
            if ((bath4 ? 1 : 0) + (eat4 ? 1 : 0) == 2)
            {
                SetInd(4, "Eating and Bathing are Complete Care");
            }

            reslist = "Partial Care";
            bool dress3 = ResultContains("", "DRESSING", "", "", reslist, SearchDepth.SearchSince24Hrs);
            bool oral3 = ResultContains("", "ORALHYG", "", "", reslist, SearchDepth.SearchSince24Hrs);
            bool toil3 = ResultContains("", "TOILETING", "", "", reslist, SearchDepth.SearchSince24Hrs);
            bool groom3 = ResultContains("", "GROOMING", "", "", reslist, SearchDepth.SearchSince24Hrs);
            bool eat3 = ResultContains("", "EATING", "", "", reslist, SearchDepth.SearchSince24Hrs);
            bool bath3 = ResultContains("", "BATHING", "", "", reslist, SearchDepth.SearchSince24Hrs);
            bool act2 = ResultContains("", "ACTIVITY", "", "", "Ambulatory,bed rest,OOB to Chair,OOB with Assist", SearchDepth.SearchSince24Hrs);
            if ((bath4 || bath3 ? 1 : 0) + (dress4 || dress3 ? 1 : 0) + (oral4 || oral3 ? 1 : 0) + (toil4 || toil3 ? 1 : 0) + (groom4 || groom3 ? 1 : 0) + (eat4 || eat3 ? 1 : 0) >= 4)
            {
                SetInd(3, "At least 4 ADL activities are Partial/Complete Care");
            }

            if ((bath4 || bath3 ? 1 : 0) + (dress4 || dress3 ? 1 : 0) + (oral4 || oral3 ? 1 : 0) + (toil4 || toil3 ? 1 : 0) + (groom4 || groom3 ? 1 : 0) + (eat4 || eat3 ? 1 : 0) + (act2 ? 1 : 0) >= 1)
            {
                SetInd(2, "At least 1 ADL activity is Partial/Complete Care");
            }

            reslist = "Self/Minimal";
            bool dress1 = ResultContains("", "DRESSING", "", "", reslist, SearchDepth.SearchSince24Hrs);
            bool oral1 = ResultContains("", "ORALHYG", "", "", reslist, SearchDepth.SearchSince24Hrs);
            bool toil1 = ResultContains("", "TOILETING", "", "", reslist, SearchDepth.SearchSince24Hrs);
            bool groom1 = ResultContains("", "GROOMING", "", "", reslist, SearchDepth.SearchSince24Hrs);
            bool eat1 = ResultContains("", "EATING", "", "", reslist, SearchDepth.SearchSince24Hrs);
            bool bath1 = ResultContains("", "BATHING", "", "", reslist, SearchDepth.SearchSince24Hrs);
            if (dress1 || oral1 || toil1 || groom1 || eat1 || bath1)
            {
                SetInd(1, "At least 1 ADL activity is Self/Minimal Care");
            }
            SetIndIfResultContains(1, "", "ACTIVITY", "", "", "Up ad lib",SearchDepth.SearchSince24Hrs);

        }

        private void UpdatePtChartArrays(string orderid, string ordstatus, string newres)
        {
            foreach (var item in _chart_items_since24hrs)
            {
                if (item.ORDER_ID == orderid)
                {
                    if (ordstatus != "")
                        item.ORDER_STATUS = ordstatus;
                    if (newres != "")
                        item.RESULT = newres;
                }
            }
        }

        private void UpdatePtChartArraysCode(string code, string ordstatus, DateTime dcdt)
        {
            foreach (var item in _chart_items_since_admission)
            {
                if (item.CODE == code && item.EVENT_DATETIME <= dcdt)
                {
                    item.ORDER_STATUS = ordstatus;
                }
            }
        }

        private void Check_5()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("5. ADL Rehab");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(5, "", "SPEECHDOC", "", "", "", SearchDepth.SearchSince24Hrs);
            SetIndIfResultContains(5, "", "PTDOC", "", "", "", SearchDepth.SearchSince24Hrs);
            SetIndIfResultContains(5, "", "OTDOC", "", "", "", SearchDepth.SearchSince24Hrs);

            CheckRehabOrder("CON00015");
            CheckRehabOrder("CON00009");
            CheckRehabOrder("CON00012");

        }
        private void CheckRehabOrder(string ocode)
        {
            DateTime max_dt = DateTime.MinValue;
            string max_status = "";

            var query_nw = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query_nw = AndItemFilter(query_nw, "", ocode, "", "", "");
            query_nw = query_nw.Where(e => e.ORDER_STATUS != "CM");
            query_nw = query_nw.Where(e => e.ORDER_STATUS == "AC" || e.ORDER_STATUS == "IP");
            query_nw = query_nw.OrderByDescending(e => e.TIMESTAMP);
            foreach (var rec in query_nw)
            {
                if (max_dt == DateTime.MinValue)
                {
                    max_status = rec.ORDER_STATUS;
                    max_dt = rec.EVENT_DATETIME;
                }
            }
            if (max_dt > DateTime.MinValue)
                if (max_status == "AC" || max_status == "IP")
                {
                    SetInd(5, "Found " + ocode + " with latest status of " + max_status + " on " + max_dt.ToString());
                }
        }


        private void Check_6_7()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("6. ADL 2-3 Caregivers");
            Program.VerboseAudit("7. ADL 4 or more Caregivers");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(7, "", "NUMASSISTS", "", "", "3,4,5,6", SearchDepth.SearchSince24Hrs);
            SetIndIfResultContains(7, "", "ASSREPO", "", "", "3,4,5,6", SearchDepth.SearchSince24Hrs);

            SetIndIfResultContains(6, "", "NUMASSISTS", "", "", "2", SearchDepth.SearchSince24Hrs);
            SetIndIfResultContains(6, "", "ASSREPO", "", "", "2", SearchDepth.SearchSince24Hrs);
        }

        private void Check_8()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("8. Communication");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(8, "", "VISUAL", "", "", "Blind", SearchDepth.SearchSinceAdmission);
            SetIndIfResultContains(8, "", "HEARIMP", "", "", "Deaf", SearchDepth.SearchSinceAdmission);
            SetIndIfResultContains(8, "", "INTERP", "", "", "Yes", SearchDepth.SearchSinceAdmission);
            SetIndIfResultContains(8, "", "LANGINT", "", "", "Yes", SearchDepth.SearchSinceAdmission);
            SetIndIfResultDoesNotContain(8, "", "SPEECH", "", "", "Unable to assess",SearchDepth.SearchSince24Hrs);
            SetIndIfResultContains(8, "", "NEUROVERB", "", "", "Inappropriate Words,Incomprehensible Sounds",SearchDepth.SearchSince24Hrs);

            SetIndIfResultContains(8, "", "LOC", "", "", "", SearchDepth.SearchSince24Hrs);
            SetIndIfResultContains(8, "", "TRACHTYPE", "", "", "", SearchDepth.SearchSince24Hrs);

            //SetIndIfResultContains(8, "", "COMMDEV", "", "", "", SearchDepth.SearchSinceAdmission);

            //if (!Exists("", "LOC", "", "", ""))
            //{
            //    SetIndIfResultContains(8, "", "TRACHTYPE", "", "", "");
            //    SetIndIfResultContains(8, "", "ETTUBE", "", "", "");
            //    SetIndIfResultContains(8, "", "LARYNGEC", "", "", "Yes");
            //}

        }

        private void Check_9()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("9. Cognitive Support");
            Program.VerboseAudit("---------------");

            SetIndIfResultDoesNotContain(9, "", "DISORIENT", "", "", "Unable to assess", SearchDepth.SearchSince24Hrs);
            SetIndIfResultContains(9, "", "COGNITIVE", "", "", "Decreased Awareness,Delusional,Hallucinating,Memory Loss", SearchDepth.SearchSince24Hrs);
            SetIndIfResultContains(9, "", "NEUROVERB", "", "", "Confused", SearchDepth.SearchSince24Hrs);

            SetIndIfResultContains(9, "", "SPEECHTREAT", "", "", "Cognitive therapy", SearchDepth.SearchSince24Hrs);
            SetIndIfResultContains(9, "", "HXCOGDELAY", "", "", "Yes", SearchDepth.SearchSince24Hrs);
            SetIndIfResultContains(9, "", "DELCOG", "", "", "", SearchDepth.SearchSince24Hrs);

        }

        private void Check_10_11()
        {
            string res;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("10. Behavior/Emotional Management");
            Program.VerboseAudit("11. Behavior/Emotional Mgmt - q 1 Hour");
            Program.VerboseAudit("---------------");

            //SetIndIfResultDoesNotContain(10, "", EXACT_MATCH_PREFIX + "BEHAV", "", "", "Cooperative,Dependent,Passive,Sedated,Sleeping");
            SetIndIfResultDoesNotContain(10, "", EXACT_MATCH_PREFIX + "BEHAV", "", "", "Passive,Sedated", SearchDepth.SearchSince24Hrs);
            SetIndIfResultContains(10, "", "PRNTBEHAV", "", "", "Argumentative,Combative,Uncooperative,Verbally Abusive", SearchDepth.SearchSince24Hrs);
            SetIndIfResultContains(10, "", "AFFECT", "", "", "Angry,Anxious,Irritable,Withdrawn", SearchDepth.SearchSince24Hrs);
            if (GetResult("", "FAMBEHAV", "", "", out res, SearchDepth.SearchSince24Hrs))
                if (res != "")
                    SetInd(10, "FAMBEHAV found with " + res);
            SetIndIfResultContains(10, "", "PGBEHAV", "", "", "", SearchDepth.SearchSince24Hrs);
            SetIndIfResultContains(10, "", "BEHAVASSESS", "", "", "Yes", SearchDepth.SearchSince24Hrs);

            SetIndIfResultContains(11, "", "RESTRAINTS", "", "", EXACT_MATCH_PREFIX + "Violent");
            SetIndIfResultContains(11, "", "SUICOBSV", "", "", "Yes,True");
            //SetIndIfResultContains(11, "", "NUR1039", "", "", "");
            //SetIndIfResultContains(11, "", "NUR1051", "", "", "");


        }

        private int CountLTZero()
        {
            int ct = 0;

            return ct;
        }
        private int CountGTZero(out int ct12, out int ct34)
        {
            int ct = 0;
            ct12 = 0;
            ct34 = 0;


            ct = ct12 + ct34;
            return ct;
        }

        //private bool IsQ4Freq()
        //{
        //    string codelist;
        //    bool ret = false;
        //    List<gBucket> buckets;

        //    SetBucketSize(60);
        //    buckets = new List<gBucket>();
        //    codelist = "304239398";
        //    AddBuckets(buckets, "", codelist, "", "", "Yes");
        //    ret = AnalyzeBuckets(buckets, 15, 60, "indicator 11 attestation", false);

        //    return (ret);

        //}
        private bool IsQ1Freq()
        {
            bool ret = false;


            return ret;
        }

        // (this isn't really a Q1h count -- it is just a count) Make this a q1hr count
        //private int ReturnQ1HrCount(string code_list, string result_list)
        //{
        //    var buckets = new List<gBucket>();
        //    SetBucketSize(60);
        //    AddBuckets(buckets, "", code_list, "", "", result_list);
        //    return CountBuckets(buckets);
        //}

        //private bool IsQ1Hour(int count)
        //{
        //    return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q1H);
        //}
        //private bool IsQ2Hour(int count)
        //{
        //    return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q2H);
        //}

        // Use this if counting safety buckets
        //private void CheckSafety(int count, string desc)
        //{
        //    if (_inds[13].is_checked) return;             //skip if highest already checked
        //    if (count == 0) return;
        //    switch (FreqForCount(_pat.los_hours, count))
        //    {
        //    case Frequencies.Q30M:
        //        SetInd(13, desc + " q30min");
        //        break;
        //    case Frequencies.Q1H:
        //    case Frequencies.Q2H:
        //        SetInd(12, desc + " q2h");
        //        break;
        //    default:
        //        Program.VerboseAudit(desc + ": " + count + " in " + Math.Round(_pat.los_hours) + " hours is not enough");
        //        break;
        //    }
        //}

        private void FindLatest(string code, string reslist, out string res, out DateTime evdt)
        {
            res = "???";
            evdt = DateTime.MinValue;

            var query = StartNewQuery(SearchDepth.SearchSince16Hrs);
            query = AndItemFilter(query, "", code, "", "", reslist);
            CHART_ITEM ch = query.OrderByDescending(e => e.EVENT_DATETIME).FirstOrDefault();
            if (ch == null) return;
            res = ch.RESULT;
            evdt = ch.EVENT_DATETIME;
            Program.VerboseAudit("Latest result and time:" + ch.RESULT + ch.EVENT_DATETIME.ToString());

        }

        private void Check_12_13()
        {
            string codelist;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("12. Safety Management - q 2 Hours");
            Program.VerboseAudit("13. Safety Management - q 30 Minutes");
            Program.VerboseAudit("---------------");

            //SetIndIfResultContains(13, "", "SUICOBSV", "", "", "Yes");
            //SetIndIfResultContains(13, "", "RESTRAINTS", "", "", "Violent (4pt or 5pt)");
            //SetIndIfResultContains(13, "", "RESTRAINTS", "", "", "Non-Violent");
            string ret_result = "";
            if (GetLatestResult("", "HENSCORE", "","", out ret_result, SearchDepth.SearchSince24Hrs))
            {
                if (ret_result.Val() >= 5)
                    SetInd(12, "HENSCORE=" + ret_result);
            }
            CheckFallsScore("PEDFALL", 8);
            SetIndIfResultDoesNotContain(12, "", "DISORIENT", "", "", "Unable to assess", SearchDepth.SearchSince24Hrs);
            SetIndIfResultContains(12, "", "COGNITIVE", "", "", "Decreased Awareness,Delusional,Hallucinating,Memory Loss", SearchDepth.SearchSince24Hrs);
            SetIndIfResultContains(12, "", "NEUROVERB", "", "", "Confused", SearchDepth.SearchSince24Hrs);
            SetIndIfResultContains(12, "", EXACT_MATCH_PREFIX+"LOC", "", "", "", SearchDepth.SearchSince24Hrs);
            SetIndIfResultContains(12, "", "SPEECHTREAT", "", "", "Cognitive Therapy", SearchDepth.SearchSince24Hrs);
            SetIndIfResultContains(12, "", "HXCOGDELAY", "", "", "Yes", SearchDepth.SearchSince24Hrs);
            SetIndIfResultContains(12, "", "DELCOG", "", "", "", SearchDepth.SearchSince24Hrs);

            SetIndIfResultContains(13, "", "SUICOBSV", "", "", "Yes,True", SearchDepth.SearchSince24Hrs);
            SetIndIfResultContains(13, "", "RESTRAINTS", "", "", "", SearchDepth.SearchSince24Hrs);
            SetIndIfResultContains(13, "", "CONVISMON", "", "", "Yes", SearchDepth.SearchSince24Hrs);
            SetIndIfResultContains(13, "", "SITTER", "", "", "Start,Continue", SearchDepth.SearchSince24Hrs);
            SetIndIfResultContains(13, "", "MEDSITTER", "", "", "Start,Continue", SearchDepth.SearchSince24Hrs);

            string found_what;
            if (OrderInProgressByCode("NUR0146", out found_what))
                SetInd(12, found_what);
            if (OrderInProgressByCode("NUR2036", out found_what))
                SetInd(12, found_what);
            if (OrderInProgressByCode("NUR1039", out found_what))
                SetInd(13, found_what);
            if (OrderInProgressByCode("NUR1051", out found_what))
                SetInd(13, found_what);

            //SetIndIfResultContains(12, "", "PRNTPRSNT", "", "", "No");


            //if ((_pat.unit_name == "B4") || (_pat.unit_name == "C7P") || (_pat.unit_name == "D7N") || (_pat.unit_name == "E7"))
            //{
            //    if (!Exists("", "LOC", "", "", "Unarousable,Lethargic,Sedated"))
            //    {
            //        codelist = "IV1,IV2,IV3,IV4";
            //        SetIndIfResultContains(12, "", codelist, "", "", "", SearchDepth.SearchSince24Hrs);
            //        codelist = "GITUBE1,GITUBE2,GITUBE3";
            //        SetIndIfResultContains(12, "", codelist, "", "", "", SearchDepth.SearchSince24Hrs);
            //        codelist = "CTTYPE1,CTTYPE2,CTTYPE3";
            //        SetIndIfResultContains(12, "", codelist, "", "", "", SearchDepth.SearchSince24Hrs);
            //        codelist = "GUTYPE1,GUTYPE2,GUTYPE3";
            //        SetIndIfResultContains(12, "", codelist, "", "", "", SearchDepth.SearchSince24Hrs);
            //        codelist = "NEURO1,NEURO2,NEURO3,NEURO4";
            //        SetIndIfResultContains(12, "", codelist, "", "", "", SearchDepth.SearchSince24Hrs);
            //        codelist = "WNDINC1,WNDINC2,WNDINC3";
            //        SetIndIfResultContains(12, "", codelist, "", "", "", SearchDepth.SearchSince24Hrs);
            //    }
            //}



        }

        private bool StandardOrderIsActive(string ocode)
        {
            bool result = false;
            bool mid_result;
            var query_nw = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query_nw = AndItemFilter(query_nw, "", ocode, "", "", "");
            query_nw = query_nw.Where(e => e.ORDER_STATUS.Contains("AC") || e.ORDER_STATUS.Contains("IP"));
            query_nw = query_nw.Where(e => e.EVENT_DATETIME <= Program.g_pull_finish);
            if (query_nw.Count() > 0)  //these are the orders that have been started since admission
            {
                foreach (var c in query_nw)
                {
                    mid_result = true;
                    //look at this order_id.  Is there one with DC?  
                    var query_dc = StartNewQuery(SearchDepth.SearchDefault);
                    query_dc = AndItemFilter(query_dc, "", ocode, "", "", "");
                    query_dc = query_dc.Where(e => e.ORDER_ID == c.ORDER_ID);
                    query_dc = query_dc.Where(e => e.ORDER_STATUS.Contains("DC"));
                    query_dc = query_dc.Where(e => e.EVENT_DATETIME <= Program.g_pull_start);
                    int ctdc = query_dc.Count();
                    if (ctdc > 0)
                    {  // dc exists before this pull.  dont triger indicator
                        mid_result = false;
                    }
                    result = result || mid_result;
                } //end foreach
            }
            return result;
        }


        private void CheckFallsScore(string code, int minval)
        {
            int result = 0;
            int value = 0;

            var query = StartNewQuery(SearchDepth.SearchSince24Hrs);
            query = AndItemFilter(query, "", code, "", "", "");
            query = query.OrderByDescending(e => e.EVENT_DATETIME);
            if (query.Count() == 0) return;
            //query = query.Where(e => e.EVENT_DATETIME >= Program.g_pull_finish.AddHours(-24));
            //query = query.Where(e => e.EVENT_DATETIME <= Program.g_pull_finish);

            //Look for a number in the result
            string ret_result = query.First().RESULT;
            if (ret_result.Val() >= minval)
            {
                SetInd(12, code + " Fall Risk Score = " + ret_result);
            }
        }

        private void Check_14()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("14. Isolation");
            Program.VerboseAudit("---------------");

            //SetIndIfResultContains(14, "", "ISOPRE", "", "", "",SearchDepth.SearchSinceAdmission);
            //SetIndIfResultContains(14, "", "NUR0416", "", "", "", SearchDepth.SearchSinceAdmission);
            string found_what;
            if (OrderInProgress("ISOPRE", out found_what))
                SetInd(14, found_what);
            if (OrderInProgress("NUR0416", out found_what))
                SetInd(14, found_what);
            if (OrderInProgress("NUR0703", out found_what))
                SetInd(14, found_what);
        }


    private bool OrderInProgressByCode(string code, out string found_what)
        {
            bool ret = false;
            code = code.ToLower();
            found_what = "";
            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = query.Where(e => e.CODE.ToLower() == code
                                  && e.EVENT_DATETIME < loc_out
                                  && (e.ORDER_STATUS.ToLower() == "ac" ||
                                      e.ORDER_STATUS.ToLower() == "ip"));
            query = query.OrderByDescending(e => e.EVENT_DATETIME);
            Program.VerboseAudit("Seeking order: code=" + code + " since:"+loc_in + " upto:" + loc_out);

            int count = query.Count();
            if (count == 0) return ret;

            var itemA = query.First();
            found_what = "Order ID=" + itemA.ORDER_ID + ": " + itemA.DESCRIPTION + "; result=" + itemA.RESULT + "; starting:" + itemA.EVENT_DATETIME.ToString() + " freq=" + itemA.ORDER_TIMING;
            Program.VerboseAudit(found_what);
            return true;

            //foreach (var itemA in query)
            //{
            //    var query2 = StartNewQuery(SearchDepth.SearchSinceAdmission);
            //    query2 = query2.Where(e => e.CODE.ToLower() == code);
            //    query2 = query2.Where(e => e.ORDER_ID == itemA.ORDER_ID);
            //    query2 = query2.Where(e => e.ORDER_STATUS.ToLower() == "dc");
            //    int ct2 = query2.Count();

            //    if (ct2 > 0)
            //    {
            //        Program.VerboseAudit("Order in progress: order_id=" + itemA.ORDER_ID + " _pat.pull_start=" + _pat.pull_start + " ct2=" + ct2);
            //        //                  Program.VerboseAudit("order in progress: q2ct=" + ct2);
            //        foreach (var x in query2)
            //        {
            //            Program.VerboseAudit("x.event_datetime=" + x.EVENT_DATETIME);
            //            if (x.EVENT_DATETIME >= _pat.pull_start)
            //            {
            //                ret = true;
            //                found_what += "Order ID=" + itemA.ORDER_ID + ": " + itemA.DESCRIPTION + "; range start-end:" + itemA.EVENT_DATETIME.ToString() + " - " + x.EVENT_DATETIME.ToString() + " freq=" + itemA.ORDER_TIMING;
            //            }
            //            else
            //            {
            //                DisableOrder(x.ORDER_ID);
            //            }
            //        }
            //    }
            //    else
            //    {
            //        ret = true;
            //        found_what = "Order ID=" + itemA.ORDER_ID + ": " + itemA.DESCRIPTION + "; result=" + itemA.RESULT + "; starting:" + itemA.EVENT_DATETIME.ToString() + " freq=" + itemA.ORDER_TIMING;
            //        //Program.VerboseAudit("AUDIT ORDER:" + found_what);
            //    }
            //}

            //return ret;
        }


        private bool OrderInProgress(string code, out string found_what)
        {
            bool ret = false;
            code = code.ToLower();
            found_what = "";
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = query.Where(e => e.CODE.ToLower() == code
                                  && e.EVENT_DATETIME < loc_out
                                  && (e.ORDER_STATUS.ToLower() == "ac" || 
                                      e.ORDER_STATUS.ToLower() == "ip"));

            query = query.OrderByDescending(e => e.EVENT_DATETIME);

            int count = query.Count();
            if (count == 0) return ret;

            Program.VerboseAudit("order in progress: count=" + count + " since:"+loc_out);

            foreach (var itemA in query)
            {
                var query2 = StartNewQuery(SearchDepth.SearchSinceAdmission);
                query2 = query2.Where(e => e.CODE.ToLower() == code);
                query2 = query2.Where(e => e.ORDER_ID == itemA.ORDER_ID);
                query2 = query2.Where(e => e.ORDER_STATUS.ToLower() == "dc");
                int ct2 = query2.Count();

                if (ct2 > 0)
                {
                    Program.VerboseAudit("order in progress: order_id=" + itemA.ORDER_ID + " _pat.pull_start=" + _pat.pull_start + " ct2="+ct2);
                    //                  Program.VerboseAudit("order in progress: q2ct=" + ct2);
                    foreach (var x in query2)
                    {
                        Program.VerboseAudit("x.event_datetime=" + x.EVENT_DATETIME);
                        if (x.EVENT_DATETIME >= _pat.pull_start)
                        {
                            ret = true;
                            found_what += "Order ID=" + itemA.ORDER_ID + ": " + itemA.DESCRIPTION + "; range start-end:" + itemA.EVENT_DATETIME.ToString() + " - " + x.EVENT_DATETIME.ToString();
                        }
                        else
                        {
                            DisableOrder(x.ORDER_ID);
                        }
                    }
                }
                else
                {
                    ret = true;
                    found_what = "Order ID=" + itemA.ORDER_ID + ": " + itemA.DESCRIPTION + "; result=" + itemA.RESULT + "; starting:" + itemA.EVENT_DATETIME.ToString();
                    //Program.VerboseAudit("AUDIT ORDER:" + found_what);
                }
            }


            return ret;
        }

        private void DisableOrder(string ordid)
        {
            //update ORDER_CONTROL = 'XNW' for _pat.encounter_id and ordid and NW
            var db = PFSDBUtility.NewSqlConnection();
            string q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and order_id='" + ordid + "' and (order_status='nw' or order_status='ip')";
            SqlCommand cmd = new SqlCommand(q, db);
            cmd.ExecuteNonQuery();
            db.Close();
        }

        private void DisableSitter(string code, DateTime evdt, bool use_proc_start)
        {
            //update ORDER_STATUS = 'X' for _pat.encounter_id and ordid
            var db = PFSDBUtility.NewSqlConnection();
            //string q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and code='" + code + "' and (result='initiated' or result='continued') and event_datetime<=" + PFSDBUtility.SQLDateTime(evdt);
            string q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and code='" + code + "' and event_datetime<=" + PFSDBUtility.SQLDateTime(evdt);
            if (use_proc_start)
                q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and code='" + code + "' and procedure_start is not null and procedure_start<=" + PFSDBUtility.SQLDateTime(evdt);
            SqlCommand cmd = new SqlCommand(q, db);
            cmd.ExecuteNonQuery();
            db.Close();

        }
        private void DisableSitter(string code, DateTime evdt)
        {
            DisableSitter(code, evdt, false);
        }

        //private void CheckAssessment(int count, string desc)
        //{
        //    //if (_inds[18].is_checked) return;          //skip if highest already checked
        //    if (count == 0) return;                    //skip if none

            //    // This should work the same as the original code:
            //    switch (FreqForCount(_pat.los_hours, count))
            //    {
            //        case Frequencies.Q30M:
            //            SetInd(18, desc);
            //            break;
            //        case Frequencies.Q1H:
            //            SetInd(17, desc);
            //            break;
            //        case Frequencies.Q2H:
            //            SetInd(16, desc);
            //            break;
            //        case Frequencies.Q4H:
            //            SetInd(15, desc);
            //            break;
            //        default:
            //            break;
            //    }

            //}

        private void ShowBuckets(List<gBucket> buckets)
        {
            string s = "";
            foreach (var e in buckets)
            {
                s = "(" + e.bucket + "" + e.code + "" + e.evdt.ToString() + ")";
                Program.VerboseAudit("Assessmt item:=" + s);
            }
        }


        private void Check_15_16_17_18()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("15. Assessment q4h");
            Program.VerboseAudit("16. Assessment q2h");
            Program.VerboseAudit("17. Assessment q1h");
            Program.VerboseAudit("18. Assessment q30min");
            Program.VerboseAudit("---------------");

            int freq = 0;

            if (_pat.age <= 5.0)
            {
                SetInd(16, "Age <=5 years. Defaulting to Q2 assessments minimally.");
            }

            string found_what;
            if (OrderInProgressByCode("NUR0550", out found_what))
                SetIndBasedOnOrder(found_what);
                    //if (!SetIndBasedOnOrder(found_what))
                    //SetInd(15, found_what);
            if (OrderInProgressByCode("NUR0785", out found_what))
                SetIndBasedOnOrder(found_what);
            if (OrderInProgressByCode("NUR0057", out found_what))
                SetIndBasedOnOrder(found_what);
            if (OrderInProgressByCode("NUR0735", out found_what))
                SetIndBasedOnOrder(found_what);
            if (OrderInProgressByCode("NUR1439", out found_what))
                SetIndBasedOnOrder(found_what);

            SetIndIfResultContains(16, "", "RNACT", "", "", "Suctioning Q2H");
            SetIndIfResultContains(17, "", "RNACT", "", "", "Suctioning Q1H");

            SetIndIfResultContains(17, "", "ARTPRES", "", "", "");
            SetIndIfResultContains(17, "", "CARDPRES", "", "", "");
            SetIndIfResultContains(17, "", EXACT_MATCH_PREFIX + "IAB", "", "", "");
            SetIndIfResultContains(17, "", EXACT_MATCH_PREFIX + "VAD", "", "", "");
            SetIndIfResultContains(17, "", "DIALTYPE", "", "", "");
            SetIndIfResultContains(17, "", "DIALTPOTH", "", "", "");
            SetIndIfResultContains(17, "", "CARDPAC", "", "", "");

            if ((_pat.unit_name == "B2") || (_pat.unit_name == "D2E") || (_pat.unit_name == "D2N") || (_pat.unit_name == "D3N") || (_pat.unit_name == "D2V") || (_pat.unit_name == "D3E"))
            {
                if (OrderInProgressByCode("NUR0550", out found_what))
                    SetInd(17, "ICU:" + found_what);
                if (OrderInProgressByCode("NUR0785", out found_what))
                    SetInd(17, "ICU:" + found_what);

                //CheckOrder("NUR0550", out freq);
                //if (freq >= 1)
                //{
                //    SetInd(14 + freq, "Order NUR0550 found; unit is in {B2, D2E, D2N, D2V, D3E, D3N}");
                //}

                //CheckOrder("NUR0785", out freq);
                //if (freq >= 1)
                //{
                //    SetInd(14 + freq, "Order NUR0785 found; unit is in {B2, D2E, D2N, D2V, D3E, D3N}");
                //}

            }
            //CheckOrder("NUR0846", out freq);
            //if (freq >= 1)
            //{
            //    SetInd(14 + freq, "Order NUR0846 found");
            //}
            //CheckOrder("NUR0015", out freq);
            //if (freq >= 1)
            //{
            //    SetInd(14 + freq, "Order NUR0015 found");
            //}
            //CheckOrder("NUR0523", out freq);
            //if (freq >= 1)
            //{
            //    SetInd(14 + freq, "Order NUR0523 found");
            //}
            //CheckOrder("NUR0518", out freq);
            //if (freq >= 1)
            //{
            //    SetInd(14 + freq, "Order NUR0518 found");
            //}
            //CheckOrder("NUR0784", out freq);
            //if (freq >= 1)
            //{
            //    SetInd(14 + freq, "Order NUR0784 found");
            //}
            //CheckOrder("NUR0738", out freq);
            //if (freq >= 1)
            //{
            //    SetInd(14 + freq, "Order NUR0738 found");
            //}

            if (OrderInProgressByCode("NUR0784", out found_what))
                if (!SetIndBasedOnOrder(found_what))
                    SetInd(17, found_what);
            if (OrderInProgressByCode("NUR1287", out found_what))
                if (!SetIndBasedOnOrder(found_what))
                    SetInd(17, found_what);
            if (OrderInProgressByCode("NUR1544", out found_what))
                if (!SetIndBasedOnOrder(found_what))
                    SetInd(17, found_what);
            if (OrderInProgressByCode("NUR1545", out found_what))
                if (!SetIndBasedOnOrder(found_what))
                    SetInd(17, found_what);
            if (OrderInProgressByCode("NUR1590", out found_what))
                if (!SetIndBasedOnOrder(found_what))
                    SetInd(17, found_what);
            if (OrderInProgressByCode("NUR1591", out found_what))
                if (!SetIndBasedOnOrder(found_what))
                    SetInd(17, found_what);
            if (OrderInProgressByCode("NUR2021", out found_what))
                if (!SetIndBasedOnOrder(found_what))
                    SetInd(17, found_what);
            if (OrderInProgressByCode("NUR2022", out found_what))
                if (!SetIndBasedOnOrder(found_what))
                    SetInd(17, found_what);
            if (OrderInProgressByCode("NUR2023", out found_what))
                if (!SetIndBasedOnOrder(found_what))
                    SetInd(17, found_what);
            if (OrderInProgressByCode("NUR2025", out found_what))
                if (!SetIndBasedOnOrder(found_what))
                    SetInd(17, found_what);
            if (OrderInProgressByCode("NUR2026", out found_what))
                if (!SetIndBasedOnOrder(found_what))
                    SetInd(17, found_what);


            SetBucketSize(60);
            AnalyzeAssessments(15, 60);

            SetBucketSize(30);
            AnalyzeAssessments(18, 30);

            SetBucketSize(60);

        }

        private bool SetIndBasedOnOrder(string found_what)
        {
            bool ret = false;
            found_what = found_what.ToLower();
            if (found_what.Contains("freq=q30"))
            {
                SetInd(18, found_what);
                ret = true;
            }
            else if (found_what.Contains("freq=q1h"))
            {
                SetInd(17, found_what);
                ret = true;
            }
            else if (found_what.Contains("freq=q2h"))
            {
                SetInd(16, found_what);
                ret = true;
            }
            else if (found_what.Contains("freq=q4h"))
            {
                SetInd(15, found_what);
                ret = true;
            }
            return ret;
        }

        private void CheckOrder(string ocode, out int freq_out)
        {
            DateTime evdt1 = DateTime.MinValue;
            DateTime evdt2 = DateTime.MinValue;

            //order_id 7820510
            //order_control  NW SC
            //order_status  AC  DC
            //order_timing  Q4H DAILY
            //result  NUR0550
            // Q15 MIN DAILY, Q30 MIN DAILY, Q1H DAILY, Q2H DAILY, Q4H DAILY, Q6H DAILY, Q8H DAILY, 2XDAY DAILY, 3XDAY DAILY, CONTINUOUS_ DAILY
            //find latest time of attnd_safety since admission that is not discontinued
            // order_control=X means discontinue already processed
            var query_nw = StartNewQuery(SearchDepth.SearchDefault);
            query_nw = AndItemFilter(query_nw, "", ocode, "", "", "");
            query_nw = query_nw.Where(e => e.ORDER_STATUS.Contains("AC") || e.ORDER_STATUS.Contains("IP"));

            //get the list of order_id to use
            var order_id_list = new List<string>();
            foreach (var c in query_nw)
            {
                //look at this order_id.  Is there one with DC?
                var query_dc = StartNewQuery(SearchDepth.SearchDefault);
                query_dc = AndItemFilter(query_dc, "", ocode, "", "", "");
                query_dc = query_dc.Where(e => e.ORDER_ID == c.ORDER_ID);
                query_dc = query_dc.Where(e => e.ORDER_STATUS.Contains("DC"));
                int ctdc = query_dc.Count();
                if (ctdc == 0)
                {  // no dc exists, add this to the list
                    order_id_list.Add(c.ORDER_ID);
                }

            }

            query_nw = StartNewQuery(SearchDepth.SearchDefault);
            query_nw = AndItemFilter(query_nw, "", ocode, "", "", "");
            query_nw = query_nw.Where(e => e.ORDER_STATUS.Contains("AC") || e.ORDER_STATUS.Contains("IP"));
            query_nw = query_nw.Where(e => order_id_list.Contains(e.ORDER_ID));

            var q15 = query_nw.Where(e => e.ORDER_TIMING.Contains("Q15 MIN DAILY"));
            var q30 = query_nw.Where(e => e.ORDER_TIMING.Contains("Q30 MIN DAILY"));
            var q1 = query_nw.Where(e => e.ORDER_TIMING.Contains("Q1H DAILY"));
            var q2 = query_nw.Where(e => e.ORDER_TIMING.Contains("Q2H DAILY"));
            var q4 = query_nw.Where(e => e.ORDER_TIMING.Contains("Q4H DAILY"));
            int ct15 = q15.Count();
            int ct30 = q30.Count();
            int ct1 = q1.Count();
            int ct2 = q2.Count();
            int ct4 = q4.Count();

            double[] dur = new double[5];
            dur[4] = ct15 * 0.25;
            dur[3] = ct30 * 0.5;
            dur[2] = ct1;
            dur[1] = ct2 * 2;
            dur[0] = ct4 * 4;

            string[] freq = new string[5];
            freq[4] = "Q15 MIN DAILY";
            freq[3] = "Q30 MIN DAILY";
            freq[2] = "Q1H DAILY";
            freq[1] = "Q2H DAILY";
            freq[0] = "Q4H DAILY";

            double max_dur = dur.Max();
            int max_idx = dur.ToList().IndexOf(dur.Max());

            if (max_dur > 0)
                Program.VerboseAudit(ocode + " was found with a frequency of " + freq[max_idx] + " and duration of " + max_dur + " hours");

            if (max_dur >= 2)
            {
                if (max_idx == 4)  //q15 should be limited to q30
                    freq_out = 4;
                else
                    freq_out = max_idx + 1;
            }
            else
                freq_out = -1;

        }

        private int GetAssessInd(DateTime loc_out_time, out DateTime classdt)
        {
            int ind = 0;
            classdt = DateTime.MinValue;
            //get assessment indicator at location out time = loc_out_time
            var db = PFSDBUtility.NewPfsDataContext();        // look for the unit default admission profile
            var query = from ce in db.CLASSIFICATION_EVENTs
                        from ia in db.INDICATOR_ANSWERs
                        where (ce.CLASSIFICATION_EVENT_ID == ia.CLASSIFICATION_EVENT_ID)
                        && (ce.ENCOUNTER_ID == _pat.encounter_id)
                        && (ce.EFFECTIVE_DATETIME_IN <= loc_out_time)
                        && (ia.INDICATOR_NUMBER >= 15 && ia.INDICATOR_NUMBER <= 18)
                        orderby ce.EFFECTIVE_DATETIME_IN descending
                        select new
                        {
                            ia.INDICATOR_NUMBER,
                            ce.CLASSIFICATION_DATETIME
                        };
            if (query.Count() > 0)
            {
                ind = query.First().INDICATOR_NUMBER;
                classdt = query.First().CLASSIFICATION_DATETIME;
            }
            return ind;
        }


        private void AnalyzeAssessments(int ind, int bucket_size)
        {
            string codelist;
            string reslist;
            List<gBucket> buckets;
            string freqstr = "";
            string orderlist;

            if (bucket_size == 60) freqstr = "====Q4/Q2/Q1 HR EVALUATION================";
            if (bucket_size == 30) freqstr = "====Q30 MIN EVALUATION====================";
            Program.VerboseAudit(freqstr + " bucket size=" + bucket_size + "  _bucket size=" + _bucket_size);

            string assessgrouplabel = "Fluid Mgt";
            //buckets = new List<gBucket>();
            //orderlist = "NUR0057";
            //AddOrderBuckets(buckets, orderlist);
            //AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            assessgrouplabel = "Medications";
            buckets = new List<gBucket>();
            //codelist = "PCEATIME,PCEADATE,PCRATIME,PCRADATE,MEDSPCA,MAKPAIN";
            codelist = "MAKPAIN";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            assessgrouplabel = "Pulmonary";
            buckets = new List<gBucket>();
            codelist = "RESPIRATIONS,PULSEOX,O2SAT2,THERGIVEN,SUCFREQ";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            assessgrouplabel = "Cardio";
            buckets = new List<gBucket>();
            codelist = "FLAPTYPE,BP1,BP2,PULSE";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            assessgrouplabel = "Neuro";
            buckets = new List<gBucket>();
            codelist = "COLDTAN,COLTMAN,NVSAVEDT,NVSAVETM";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            // These are now q1 one-offs
            //assessgrouplabel = "Tech";
            //buckets = new List<gBucket>();
            //codelist = "ARTPRES,CARDPRES,IAB,VAD,DIALTYPE,DIALTPOTH,CARDPAC";
            //AddBuckets(buckets, "", codelist, "", "");
            //AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            assessgrouplabel = "Wound";
            buckets = new List<gBucket>();
            codelist = "URINECOL";
            AddBuckets(buckets, "", codelist, "", "Blood");
            codelist = "PUNCSITE";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            //buckets = new List<int>();
            //codelist = "A_MHMPain,A_MHPain,A_Pain";
            //AddBuckets(buckets, "", codelist, "", "");
            //ct = CountBuckets(buckets);
            //CheckAssessment(ct, "Pain=" + ct);
            //if (_inds[18].is_checked) return;




        }
        private void AddOrderBuckets(List<gBucket> bucket_list, string order_list)
        {
            // Assign (virtual) bucket numbers to each assessment.  
            // Every x minutes after that is a new bucket.  Bucket zero = pull_start time.

            // get the chart items for the assessments
            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = AndItemFilter(query, "", order_list, "", "", "");
            query = query.Where(e => e.ORDER_STATUS.Contains("AC") || e.ORDER_STATUS.Contains("IP"));
            query = query.Where(e => e.EVENT_DATETIME >= _pat.pull_start && e.EVENT_DATETIME <= _pat.pull_finish);

            //get the list of order_ids that are not DCed
            var order_id_list = new List<string>();
            foreach (var c in query)
            {
                //look at this order_id.  Is there one with DC?
                var query_dc = StartNewQuery(SearchDepth.SearchDefault);
                query_dc = query_dc.Where(e => e.EVENT_DATETIME >= _pat.pull_start && e.EVENT_DATETIME <= _pat.pull_finish);
                query_dc = AndItemFilter(query_dc, "", order_list, "", "", "");
                query_dc = query_dc.Where(e => e.ORDER_ID == c.ORDER_ID);
                query_dc = query_dc.Where(e => e.ORDER_STATUS.Contains("DC"));
                int ctdc = query_dc.Count();
                if (ctdc == 0)
                {  // no dc exists, add this to the list
                    order_id_list.Add(c.ORDER_ID);
                    Program.VerboseAudit("Adding orderid=" + c.ORDER_ID);
                }
            }

            var query_nw = StartNewQuery(SearchDepth.SearchDefault);
            query_nw = query_nw.Where(e => e.EVENT_DATETIME >= _pat.pull_start && e.EVENT_DATETIME <= _pat.pull_finish);
            query_nw = AndItemFilter(query_nw, "", order_list, "", "", "");
            query_nw = query_nw.Where(e => e.ORDER_STATUS.Contains("AC") || e.ORDER_STATUS.Contains("IP"));
            query_nw = query_nw.Where(e => order_id_list.Contains(e.ORDER_ID));
            query_nw = query_nw.OrderBy(e => e.EVENT_DATETIME);
            Program.VerboseAudit("Count of orders=" + query_nw.Count().ToString());

            // This step is needed for those who want to count the # within a bucket, like Shands #24.
            // This will have no effect on those that count buckets.

            var query2 = from item in query_nw
                         select new
                         {
                             bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                             code = item.CODE,
                             desc = item.DESCRIPTION,
                             evdt = item.EVENT_DATETIME
                         };

            //// figure out what buckets the events belong to
            //Program.VerboseAudit("Bucket size=" + _bucket_size.ToString());
            //var query3 = from item in query2
            //             select new
            //             {
            //                 bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.evdt) / _bucket_size),
            //                 code = item.code,
            //                 desc = item.desc,
            //                 evdt = item.evdt

            //             };
            // Add to the list
            DateTime nw_evdt = DateTime.MinValue;
            foreach (var item in query2)
            {
                if (item.evdt > nw_evdt)
                {
                    nw_evdt = item.evdt;
                    Program.VerboseAudit("Adding this bucket=" + item.bucket.ToString());

                    var b = new gBucket();
                    b.bucket = item.bucket;
                    b.code = item.code;
                    b.description = item.desc;
                    b.evdt = item.evdt;

                    bucket_list.Add(b);
                }
            }

            // print how many were found`
            Program.VerboseAudit(Describe("", order_list, "", "", ""));
        }

        private int CountBuckets(List<int> bucket_list)       // bucket list: (ha ha)
        {
            int result = bucket_list.Distinct().Count();
            if (result > 0) Program.VerboseAudit(result + " unique");
            return result;
        }


        private bool AnalyzeBuckets(List<gBucket> buckets, int ind, int bucketsize, string group)
        {
            return AnalyzeBuckets(buckets, ind, bucketsize, group, true);

        }

        private bool AnalyzeBuckets(List<gBucket> buckets, int ind, int bucketsize, string group, bool set_ind)
        {
            DateTime dt = DateTime.MinValue;
            DateTime firstdt = DateTime.MinValue;

            bool all_ok = OLDAnalyzeBuckets(buckets, ind, bucketsize, group, set_ind);
            return all_ok;


        }
        private bool DoShortLOSAssessEval(List<gBucket> buckets, int ind, int bucketsize, string group, bool set_ind)
        {
            int ct = 0;
            DateTime starttm, endtm;
            DateTime prevtm = DateTime.MinValue;
            int loshrs = (int)(Math.Round(_pat.los_hours));
            var arr = buckets.ToArray();
            bool set18 = false;
            Program.VerboseAudit("----Entering Short LOS Analysis for group: " + group + " LOS=" + loshrs + "  bucketsizew=" + bucketsize);

            for (int i = 0; (i < arr.GetUpperBound(0)); i++)
            {
                prevtm = DateTime.MinValue;
                starttm = arr[i].evdt;
                endtm = starttm.AddHours(loshrs);
                ct = 0;
                for (int j = i; (j < arr.GetUpperBound(0)); j++)
                {
                    if (prevtm < arr[j].evdt && arr[j].evdt >= starttm && arr[j].evdt <= endtm)
                    {
                        ct++;
                        prevtm = arr[j].evdt;
                    }
                }
                if (ct >= 9 / (4.0/loshrs))
                {
                    SetInd(18, ct.ToString() + " or more items in LOS=" + loshrs + " hours.");
                    set18 = true;
                    i = 999;
                }
            }
            Program.VerboseAudit("----Exiting Short LOS Analysis for group: " + group + " LOS=" + loshrs + "  bucketsizew=" + bucketsize);

            return set18;
        }


//Proposed change to remove consecutive factor; use straight scale with Short LOS exception:  
//	                        LOS=12 hrs      LOS = 6 hrs     LOS = 4 hrs     LOS = 2 hrs     Short LOS< 5 hrs
//q30 75% of 30-min buckets	3/4 x 24  = 18	3/4 x 12  = 9	3/4 x 8  = 6	3/4 x 4  = 3	5 or more VS
//q1 66% of 60-min buckets	2/3 x 12  = 8	2/3 x 6  = 4	2/3 x 4  = 2.67=>3	2/3 x 2  = 1.33=>1	3 VS + examine 30min 
//q2 50 % of 60-min buckets	.5 x 12  = 6	.5 x 6  = 3	.5 x 4  = 2	.5 x 2  = 1	2 VS
//q4 33% of 60-min buckets	1/3 x 12  = 4 2	1/3 x 6  = 2 1	1/3 x 4  = 1.33=>1	1/3 x 2  = .67=>1	1 VS
        private bool OLDAnalyzeBuckets(List<gBucket> buckets, int ind, int bucketsize, string group, bool set_ind)
        {
            DateTime dt = DateTime.MinValue;
            int bnum = 0;
            int numbucket = -99;
            int lobucket = 99;
            int hibucket = 0;
            int numitems = 0;
            int numconsec = 0;
            int greatestnumconsec = 0;
            bool all_ok = false;
            List<gBucket> dtlist = new List<gBucket>();
            Program.VerboseAudit("----Begin Assessment Group = " + group + " ---- bucketsizew=" + bucketsize + " start time of first bucket=" + _pat.pull_start);
            //if (ind==18 && Math.Round(_pat.los_hours) <= 4.0)
            //{
            //    all_ok = DoShortLOSAssessEval(buckets,ind,bucketsize,group,set_ind);
            //    Program.VerboseAudit("---- End Assessment Group = " + group + " ----");
            //    return all_ok;
            //}

            //Program.VerboseAudit("buckets count=" + buckets.Count());
            var b = buckets.OrderBy(e => e.evdt).ToList();

            //numitems = buckets.Count();
            foreach (var item in b)
            {
                if (numbucket < item.bucket)
                {
                    numbucket = item.bucket;
                    numitems++;
                    Program.VerboseAudit(item.bucket + ") " + item.evdt.ToString() + " := " + item.code);
                }
                //if (numbucket < item.bucket)
                //{
                //    //add dt to ary
                //    bnum++;
                //    dtlist.Add(item);
                //    if (numbucket == -99 || item.bucket - numbucket == 1)
                //    {
                //        numconsec++;
                //        if (greatestnumconsec < numconsec)
                //            greatestnumconsec = numconsec;
                //    }
                //    else
                //    {
                //        numconsec = 1;
                //    }
                //    numbucket = item.bucket;
                //    if (hibucket < item.bucket) hibucket = item.bucket;
                //    if (lobucket > item.bucket) lobucket = item.bucket;
                //    Program.VerboseAudit(item.bucket + ")." + item.evdt.ToString() + " := " + item.code);// + "  [distinct times only]");
                //}
            }

            //if (bnum <= 1)
            //{
            //    Program.VerboseAudit("---- End Assessment Group = " + group + " ----");
            //    return false;
            //}

            //Program.VerboseAudit("numitems=" + numitems);
            //Program.VerboseAudit("bucket count=" + bnum);
            //int half_los = (int)(_pat.los_hours * (30.0 / bucketsize));//for q30 this will be los_hours.  for q60 this will be .5 * los_hours
            //Program.VerboseAudit("half-LOS bucket count=" + half_los + " at " + bucketsize + "mins each.");
            //Program.VerboseAudit("greatest consec=" + greatestnumconsec);

            int num_buckets_in_los = (int)(_pat.los_hours * (60.0 / bucketsize));//for q30 this will be 2xlos_hours.  for q60 this will be los_hours
            Program.VerboseAudit("total bucket count in LOS=" + num_buckets_in_los);
            Program.VerboseAudit("num buckets filled=" + numitems);
            //Program.VerboseAudit("half-LOS bucket count=" + half_los + " at " + bucketsize + "mins each.");
            //Program.VerboseAudit("greatest consec=" + greatestnumconsec);

            //double bucketratio = (hibucket-lobucket) / (1.0 * (bnum-1));
            //q30 75% of 30-min buckets	3/4 x 24  = 18	3/4 x 12  = 9	3/4 x 8  = 6	3/4 x 4  = 3	5 or more VS
            //q1 66% of 60-min buckets	2/3 x 12  = 8	2/3 x 6  = 4	2/3 x 4  = 2.67=>3	2/3 x 2  = 1.33=>1	3 VS + examine 30min 
            //q2 50 % of 60-min buckets	.5 x 12  = 6	.5 x 6  = 3	.5 x 4  = 2	.5 x 2  = 1	2 VS
            //q4 33% of 60-min buckets	1/3 x 12  = 4 2	1/3 x 6  = 2 1	1/3 x 4  = 1.33=>1	1/3 x 2  = .67=>1	1 VS
            int q30need = (int)Math.Round(0.75 * 2 * _pat.los_hours);
            int q1need = (int)Math.Round(0.667 * _pat.los_hours);
            int q2need = (int)Math.Round(0.5 * _pat.los_hours);
            int q4need = 1;
            if (ind < 18)
            {
                if (_pat.los_hours > 5)
                {
                    if (_pat.los_hours < 8)
                    {
                        q4need = 1;
                        q2need = 2; //Jan27 2021
                    }
                    else
                    {
                        q4need = 2;
                        q2need = 4; // Feb3 2021 4;//Jan27 2021
                    }

                    if (numitems >= q1need)
                    {
                        SetInd(17 * Convert.ToInt32(set_ind), "Qualifies for q1 because numcharted=" + numitems + " is >=" + q1need + " LOS=" + Math.Round(_pat.los_hours, 2) + " x .667=" + q1need);
                        all_ok = (ind <= 17);
                    }
                    else if (numitems >= q2need)
                    {
                        //Jan27 2021 SetInd(16 * Convert.ToInt32(set_ind), "Qualifies for q2 because numcharted=" + numitems + " is >=" + q2need + " LOS=" + Math.Round(_pat.los_hours, 2) + " x .5=" + q2need);
                        SetInd(16 * Convert.ToInt32(set_ind), "Qualifies for q2 because numcharted=" + numitems + " is >=" + q2need + " for LOS=" + Math.Round(_pat.los_hours, 2) + "  >=8hrs needs 5 else 2");
                        all_ok = (ind <= 16);
                    }
                    else if (numitems >= q4need)
                    {
                        SetInd(15 * Convert.ToInt32(set_ind), "Qualifies for q4 because numcharted=" + numitems + " is >=" + q4need + " LOS=" + Math.Round(_pat.los_hours, 2) + "  >=8hrs needs 2 else 1");
                        all_ok = (ind <= 15);
                    }
                }
                else //short los
                {
                    q1need = 3;
                    q2need = 2;
                    q4need = 1;

                    if (numitems >= q1need)
                    {
                        SetInd(17 * Convert.ToInt32(set_ind), "ShortLOS Qualifies for q1 because numcharted=" + numitems + " is >=" + q1need + " LOS=" + Math.Round(_pat.los_hours, 2) + " needs just 3");
                        all_ok = (ind <= 17);
                    }
                    else if (numitems >= q2need)
                    {
                        SetInd(16 * Convert.ToInt32(set_ind), "ShortLOS Qualifies for q2 because numcharted=" + numitems + " is >=" + q2need + " LOS=" + Math.Round(_pat.los_hours, 2) + " needs just 2");
                        all_ok = (ind <= 16);
                    }
                    else if (numitems >= q4need)
                    {
                        SetInd(15 * Convert.ToInt32(set_ind), "ShortLOS Qualifies for q4 because numcharted=" + numitems + " is >=" + q4need + " LOS=" + Math.Round(_pat.los_hours, 2) + " needs just 1");
                        all_ok = (ind <= 15);
                    }

                }
//                if (greatestnumconsec >= half_los || bucketratio <= 1.5 && bnum >= half_los-1)
//                {
//                    SetInd(17 * Convert.ToInt32(set_ind), "Qualifies for q1hr because chartings are at least q1hr for duration of half-LOS=" + half_los + " maxconsec=" + greatestnumconsec);
//                    all_ok = (ind <= 17);
//                }
//                //                else if (bucketratio <= 1.25 && bnum >= half_los && half_los >= 4)
//                //else if (bucketratio <= 1.25 && bnum >= half_los && half_los >= 4)
//                //{
//                //    SetInd(17 * Convert.ToInt32(set_ind), "Qualifies for q1hr because charting to bucket ratio is less than 1.25: hibucket-lobucket=" + hibucket +"-" +lobucket+"="+ (hibucket - lobucket) + " divided by num buckets=" + bnum + " equals " + bucketratio);
//                //    all_ok = (ind >= 17);
//                //}
//                //else if (bucketratio <= 2.5 && bnum >= 2)
//                //                else if (bnum >= half_los/2)
//                else if (bucketratio < 3 && bnum >= 3 && 
//                    (
//                    (half_los >= 6 && (hibucket - lobucket) >= half_los-1)
//                    ||
//                    ((half_los >= 4 && half_los <= 5) && (hibucket - lobucket) >= half_los - 1)
//                    ||
//                    ((half_los <= 3) && (hibucket - lobucket) >= half_los)
//                    )
//                    )
//                {
//                    SetInd(16 * Convert.ToInt32(set_ind), "Qualifies for q2hr because charting to bucket ratio is less than 3: hibucket-lobucket=" + hibucket + "-" + lobucket + "=" + (hibucket - lobucket) + " divided by num buckets=" + (bnum-1) + " equals " + bucketratio);
//                    all_ok = (ind <= 16);
//                }
////                else if (bucketratio > 2.5 && bucketratio <= 5 && bnum >= 2)
////                else if (bnum >= half_los/4)
//                else if ((bucketratio <= 6 && bnum >= 2) && //change to <=6 Oct15/2020
//                    (
//                    (half_los >= 6 && (hibucket - lobucket) >= half_los - 2)
//                    ||
//                    ((half_los >= 4 && half_los <= 5) && (hibucket - lobucket) >= half_los - 1)
//                    ||
//                    ((half_los <= 3) && (hibucket - lobucket) >= half_los)
//                    )
//                    )
//                {
//                    SetInd(15 * Convert.ToInt32(set_ind), "Qualifies for q4hr because charting to bucket ratio is <=6: hibucket-lobucket=" + hibucket + "-" + lobucket + "=" + (hibucket - lobucket) + " divided by num buckets=" + (bnum-1) + " equals " + bucketratio);
//                    all_ok = (ind == 15);
//                }
            }
            else // ind==18
            {
                if (_pat.los_hours > 5)
                {
                    if (numitems >= q30need)
                    {
                        SetInd(18 * Convert.ToInt32(set_ind), "Qualifies for q30 because numcharted=" + numitems + " is >=" + q30need + " LOS=" + Math.Round(_pat.los_hours, 2) + " x 2 x .75=" + q30need);
                        all_ok = (ind <= 18);
                    }
                }
                else
                {
                    q30need = 5;
                    if (numitems >= q30need)
                    {
                        SetInd(18 * Convert.ToInt32(set_ind), "ShortLOS Qualifies for q30 because numcharted=" + numitems + " is >=" + q30need + " LOS=" + Math.Round(_pat.los_hours, 2) + " x 2 needs " + q30need);
                        all_ok = (ind <= 18);
                    }
                }
                //if (greatestnumconsec >= half_los)
                //{
                //    SetInd(18 * Convert.ToInt32(set_ind), "Qualifies for q30min because consecutive chartings are at least q30 for duration of half-LOS=" + half_los + " maxconsec=" + greatestnumconsec);
                //    all_ok = (ind <= 18);
                //}
            }
            Program.VerboseAudit("---- End Assessment Group = " + group + " ----");
            return all_ok;
        }


        private void SetBucketSize(int minutes)
        {
            _bucket_size = minutes;
        }

        //Add to the list of bucket numbers (redundant buckets are fine)
        private bool AddBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field)
        {
            return AddBuckets(bucket_list, cat, code_list, desc, field, "", SearchDepth.SearchDefault);
        }
        private bool AddBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field, string result_list)
        {
            return AddBuckets(bucket_list, cat, code_list, desc, field, result_list, SearchDepth.SearchDefault);

        }
        private bool AddBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field, string result_list, SearchDepth search_depth)
        {
            // Assign (virtual) bucket numbers to each assessment.  
            // Every x minutes after that is a new bucket.  Bucket zero = pull_start time.
//            Program.VerboseAudit("----Locating items for group: " + assessgrouplabel + "   bucketsize=" + _bucket_size);

            // get the chart items for the assessments
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc, field, result_list);
      query = query.Where(e => e.EVENT_DATETIME < _pat.pull_finish);
            int ct = query.Count();
            if (ct == 0) return false;

            Program.VerboseAudit("ct=" + ct + " of codelist=" + code_list);
            // This step is needed for those who want to count the # within a bucket, like Shands #24.
            // This will have no effect on those that count buckets.
            //var query2 = (from item in query select new { item.EVENT_DATETIME }).Distinct();
            //var query2 = (from item in query select new { item.EVENT_DATETIME, item.CODE });

            //foreach (var x in query2)
            //{
            //    Program.VerboseAudit("patpullstart=" + _pat.pull_start.ToString() + " itemevdt=" + x.EVENT_DATETIME.ToString() + " diff=" + (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, x.EVENT_DATETIME)) + " bucketsize=" + _bucket_size + " bucket=" + (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, x.EVENT_DATETIME) / _bucket_size));
            //}

            // figure out what buckets the events belong to
            //var query3 = from item in query2
            //             select new {
            //                bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size)
            //             };
            //// Add to the list
            //foreach (var item in query3) {
            //    bucket_list.Add(item.bucket);
            //}

            var query3 = from item in query
                         select new
                         {
                             bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                             code = item.CODE,
                             desc = item.DESCRIPTION,
                             evdt = item.EVENT_DATETIME
                         };
            // Add to the list
            int cta = 0;
            foreach (var item in query3)
            {
                Program.VerboseAudit("  item: bucket=" + item.bucket + " c=" + item.code + "desc=" + item.desc + " dt=" + item.evdt);
                var b = new gBucket();
                b.bucket = item.bucket;
                b.code = item.code;
                b.description = item.desc;
                b.evdt = item.evdt;
                b.has_all_deps = true;
                gBucket f = bucket_list.Find(x => x.evdt == item.evdt);
                if (f.evdt != item.evdt)
                {
                    bucket_list.Add(b);
                    cta++;
                }
            }
            Program.VerboseAudit("add ct=" + cta);
            return true;
        }


        private void AddVitalsBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field, string result_list, SearchDepth search_depth)
        {
            // Assign (virtual) bucket numbers to each assessment.  
            // Every x minutes after that is a new bucket.  Bucket zero = pull_start time.
            Program.VerboseAudit("----Locating Vital Signs: Filling buckets...Bucketsize=" + _bucket_size + " start time of first bucket=" + _pat.pull_start);

            // get the chart items for the assessments
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc, field, result_list);

            // This step is needed for those who want to count the # within a bucket, like Shands #24.
            // This will have no effect on those that count buckets.
            //var query2 = (from item in query select new { item.EVENT_DATETIME }).Distinct();
            //var query2 = (from item in query select new { item.EVENT_DATETIME, item.CODE });

            //foreach (var x in query2)
            //{
            //    Program.VerboseAudit("patpullstart=" + _pat.pull_start.ToString() + " itemevdt=" + x.EVENT_DATETIME.ToString() + " diff=" + (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, x.EVENT_DATETIME)) + " bucketsize=" + _bucket_size + " bucket=" + (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, x.EVENT_DATETIME) / _bucket_size));
            //}

            // figure out what buckets the events belong to
            //var query3 = from item in query2
            //             select new {
            //                bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size)
            //             };
            //// Add to the list
            //foreach (var item in query3) {
            //    bucket_list.Add(item.bucket);
            //}

            var query3 = from item in query
                         select new
                         {
                             bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                             code = item.CODE,
                             evdt = item.EVENT_DATETIME,
                             desc = item.DESCRIPTION
                         };
            var query4 = from item in query3
                         orderby item.bucket,item.code
                         select item;

            // Add to the list
            int currb = -1;
//            int prevb = -1;
            string currcode = "xx";
            DateTime currevdt = DateTime.MinValue;
            int currct = 0;
  //          bool addcurr = false;
            //Data is: 0, "123"
            //         0, "234"  0733
            //         0, "234"  0734
            //         1, "123"
            //         2, "234"
            //         2, "345"
            foreach (var item in query4)
            {
                int maxLength = Math.Min(item.desc.Length, 16);
                string d = item.desc.Substring(0, maxLength);
                Program.VerboseAudit("Vitals item: bucket=" + item.bucket + " dt=" + item.evdt + " desc=" + d + " c=" + item.code);
                if (currb != item.bucket)
                {
                    currb = item.bucket;
                    currcode = item.code;
                    currevdt = item.evdt;
                    currct = 1;
                }
                else
                {
                    if (currcode != item.code)
                    {  //minimum of 2 different VS codes
                        if (currct == 1)
                        { // add the first code first
                            var b = new gBucket();
                            b.bucket = item.bucket;
                            b.code = currcode;
                            b.evdt = currevdt;
                            b.has_all_deps = true;
                            //gBucket f = bucket_list.Find(x => x.evdt == item.evdt);
                            //if (f.evdt != item.evdt) bucket_list.Add(b);
                            bucket_list.Add(b);
                            Program.VerboseAudit("  adding item1: b=" + b.bucket + " c=" + b.code + " dt=" + b.evdt);
                        }
                        currct++;
                        currcode = item.code; //guarantees not to add code again in same bucket
                            
                        var b2 = new gBucket();
                        b2.bucket = item.bucket;
                        b2.code = item.code;
                        b2.evdt = item.evdt;
                        b2.has_all_deps = true;
                        //gBucket f = bucket_list.Find(x => x.evdt == item.evdt);
                        //if (f.evdt != item.evdt) bucket_list.Add(b);
                        bucket_list.Add(b2);
                        Program.VerboseAudit("  adding item2: b=" + b2.bucket + " c=" + b2.code + " dt=" + b2.evdt);
                    }
                }
            }

            // This is just for auditing: Say what we are looking for and how many were found
            if (!String.IsNullOrEmpty(result_list))
            {
                // print each word and if it was found or not
                //int i = CountResultContains(cat, code_list, desc, field, result_list);
            }
            else
            {
                // print how many were found
                //Program.VerboseAudit(Describe(cat, code_list, desc, field, ""));
            }
            Program.VerboseAudit("----End of Locating Vital Signs: Filling buckets...Bucketsize=" + _bucket_size);
        }


        private int CountBuckets(List<gBucket> bucket_list)       // bucket list: (ha ha)
        {
            //int result = bucket_list.Distinct().Count();
            //if (result > 0) Program.VerboseAudit(result + " unique");
            //return result;
            int x = -99;
            int result = 0;
            //int result = bucket_list.Distinct().Count();
            var query = from b in bucket_list
                        orderby b.bucket ascending
                        select b;
            foreach (var b in query)
            {
                if (x != b.bucket)
                {
                    result++;
                    x = b.bucket;
                }
            }
            if (result > 0) Program.VerboseAudit(result + " unique");
            return result;
        }
        private void Check_19()
        {
            string reslist;
            string codelist;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("19. Vascular Access Site Mgt q1 Hour");
            Program.VerboseAudit("---------------");

            //DateTime nowdt = _pat.pull_finish;

            ////SetIndIfResultContains(19, "", "IV1,IV2,IV3", "", "", "", SearchDepth.SearchDefault);
            //string nowstr = nowdt.ToString("yyyyMMdd");
            //EqualDate(19, EXACT_MATCH_PREFIX + "IV1", nowstr);
            //EqualDate(19, EXACT_MATCH_PREFIX + "IV2", nowstr);
            //EqualDate(19, EXACT_MATCH_PREFIX + "IV3", nowstr);
            bool ivsite = false;
            ivsite = Exists("", "PUNCSITE,IVSAVEDATE,IVSAVETIME,IVSITE1,IVSITE2,IVSITE3,IVSITE4", "", "", "", SearchDepth.SearchSince24Hrs);
            if (!ivsite) return;

            //antineoplastic should trigger both 19 and 20
            int num_iv = 0;
            int num_meds = 0;
            bool found_dual = false;
            string found_what = "";
            bool is_antineo = false;
            string found_antineo = "";
            bool is_vasc = false;
            string found_vasc = "";

            QueryMeds(out num_meds, out num_iv, out found_dual, out found_what, out is_antineo, out found_antineo, out is_vasc, out found_vasc);

            if (is_vasc)
                SetInd(19, "Found Vascular Access med: " + found_vasc);

            if (is_antineo)
                SetInd(19, "Found Antineoplastic med: " + found_antineo);

        }
        private bool EqualDate(int ind, string itemcode, string nowdatestr)
        {
            string dtstr;
            bool isequal = false;

            if (GetResult("", itemcode, "", "", out dtstr, SearchDepth.SearchSince24Hrs))
            {
                if (dtstr.Trim() == nowdatestr)
                {
                    if (itemcode.Left(2) == EXACT_MATCH_PREFIX) itemcode = itemcode.Substring(2);
                    SetInd(ind, itemcode + " date is equal to " + nowdatestr);
                    isequal = true;
                }
            }
            return isequal;
        }


        private void CheckRouteCath()
        {
            int ct = 0;
            //OBX|1|DT|MED9003 ^ ALTEPLASE 50 MG INTRAVENOUS SOLUTION;;;20;;;0.5;;;cath; ; ; RateVerify | RateVerify | 20190802000000 |||||| F ||| 20190802000000
            //OBX|1|DT|MED9003 ^ HEPARIN  MG INTRAVENOUS SOLUTION;;;20;;;0.5;;;cath; ; ; RateVerify | RateVerify | 20190802000000 |||||| F ||| 20190802000000
            var query = StartNewQuery(SearchDepth.SearchSince9Hrs);    // add custom time range below
            query = query.Where(e => e.CODE.StartsWith("med"));
            query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;cath"));
            query = query.Where(e => e.DESCRIPTION.ToUpper().Contains("ALTEPLASE") ||
                                     e.DESCRIPTION.ToUpper().Contains("HEPARIN"));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            Program.VerboseAudit("Med ALTEPLASE or HEPARIN with cath found: " + query.Count());
            foreach (var item in query)
            {
                Program.VerboseAudit("====Med found: ALTEPLASE or HEPARIN====");
                Program.VerboseAudit("med code: " + item.CODE + " desc: " + item.DESCRIPTION + "  evdt: " + item.EVENT_DATETIME.ToString());

                var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                if (arr.GetUpperBound(0) >= 3)
                {
                    string rte = arr[3];
                    if (rte.ToLower() == "cath") ct++;
                }
            }
            if (ct > 0)
                SetInd(19, "Found ALTEPLASE or HEPARIN with route cath.");
        }

        private void Check_20()
        {
            string reslist;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("20. Medication Activity >= 20 minutes");
            Program.VerboseAudit("---------------");
            //CUSTOM MAPPING NOTE FOR INDICATOR 20:  
            //1) Trigger if there are 8 meds or more over a 30 min period. 
            //2) Trigger if there is 1 high risk med labels as high alert medication.  
            //3.Trigger if 3 or more medication administrations with the route contain " 
            //Intravenous" Can be same or different medications. 
            //4.Trigger for med route nebulizer treatments age less than or equal to 6 yrs. 
            //5.Trigger if med administered via these routes: Jejunostomy Tube, 
            //NasoGastric Tube, Orogastric, PEG Tube 
            //6.Any medication in NICU
            //if code=MEDDUxxx then dual signoff

            //for this enc_id,
            // look at med's order Id
            // search for code=ORD<order_id> with order_id=<order_id>
            // to get route in description

            int num_iv = 0;
            int num_meds = 0;
            bool found_dual = false;
            string found_what = "";
            bool is_antineo = false;
            string found_antineo = "";
            bool is_vasc = false;
            string found_vasc = "";

            QueryMeds(out num_meds, out num_iv, out found_dual, out found_what, out is_antineo, out found_antineo, out is_vasc, out found_vasc);

            if (num_meds >= 8)
                SetInd(20, "Meds count>=8 found:" + num_meds);
            if (num_iv >= 3)
                SetInd(20, "Meds IV or IM count>=3 found:" + num_iv);
            if (found_dual)
                SetInd(20, "Found dual signoff med: " + found_what);
            if (is_antineo)
                SetInd(20, "Found Antineoplastic med: " + found_antineo);


        }

        private void QueryMeds(out int num_meds,out int num_iv,out bool found_dual,out string found_what,out bool is_antineoplastic, out string found_antineoplastic, out bool is_vascular, out string found_vascular)
        {
            string sql = "select distinct cimed.encounter_id,cimed.event_datetime,cimed.code,cimed.order_id,cimed.DESCRIPTION as MedDescript";
            sql += ",ciord.DESCRIPTION as OrderRoute,ciord.ORDER_CONTROL,cimed.result,hrmed.THERA_CLASS";
            sql += " from chart_item as cimed";
            sql += " inner join chart_item as ciord on(cimed.encounter_id = ciord.encounter_id) and(cimed.order_id = ciord.order_id)";
            sql += " left join meds_hirisk as hrmed on(hrmed.GENERIC_NAME like(substring(cimed.description, 1, 6) + '%'))";
            sql += " where cimed.encounter_id=" + _pat.encounter_id;
            sql += " and cimed.event_datetime between '" + loc_in + "' and '" +loc_out +"'";
            sql += " and cimed.code like 'med%' and ciord.code like 'ord%'";
            sql += " order by cimed.order_id";
            Program.VerboseAudit(sql);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd = new SqlCommand(sql, db2);
            SqlDataReader dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);

            string order_ctrl;
            num_iv = 0;
            string route = "";
            num_meds = 0;
            string medcode;
            found_dual = false;
            found_what = "";
            is_antineoplastic = false;
            found_antineoplastic = "";
            string thera_class = "";
            string med_descript = "";
            DateTime evdt = DateTime.MinValue;
            string order_id = "";
            bool hasrows = dr2.HasRows;

             is_vascular = false;
             found_vascular = "";

            if (!hasrows)
            {
                Program.VerboseAudit("No meds found.");
                return;
            }
            while (dr2.Read())
            {
                order_ctrl = PFSDBUtility.DBToString(dr2["ORDER_CONTROL"]).ToUpper();
                if (order_ctrl != "DC" && order_ctrl != "CA")
                {
                    num_meds++;
                    if (dr2["OrderRoute"] != DBNull.Value)
                    {
                        route = PFSDBUtility.DBToString(dr2["OrderRoute"]).ToUpper();
                        if (route.StartsWith("IV") || route.StartsWith("IM"))
                            num_iv++;
                    }//route

                    medcode = PFSDBUtility.DBToString(dr2["CODE"]).ToUpper();
                    evdt = PFSDBUtility.DBToDateTime(dr2["event_datetime"]);
                    med_descript = PFSDBUtility.DBToString(dr2["MedDescript"]);
                    order_id = PFSDBUtility.DBToString(dr2["ORDER_ID"]);
                    if (medcode.StartsWith("MEDDU"))
                    {
                        found_dual = true;
                        found_what = med_descript + " at=" + evdt + " route=" + route + " orderid=" + order_id;
                    }

                    thera_class= PFSDBUtility.DBToString(dr2["THERA_CLASS"]);
                    if (thera_class.StartsWith("10"))
                    {
                        is_antineoplastic = true;
                        found_antineoplastic=med_descript + " at=" + evdt + " route=" + route + " orderid=" + order_id;
                    }
                    else if (route.StartsWith("IV") && (thera_class.StartsWith("52:32") || 
                             thera_class.StartsWith("12:12.08") || 
                             thera_class.StartsWith("24:04.04") || 
                             thera_class.StartsWith("24:04.08") || 
                             thera_class.StartsWith("24:04.92")))
                    {
                        is_vascular = true;
                        found_vascular = med_descript + " at=" + evdt + " route=" + route + " orderid=" + order_id;
                    }

                }//order_ctrl
            }//while

        }


        private void CheckMedGiven(int ind, string desc)
        {

            string[] transplantmed_actions = { "given", "newbag", "new bag", "rateverify", "rate verify", "rate change", "ratechange", "restarted", "started/down" };

            for (int i = 0; i <= transplantmed_actions.GetUpperBound(0); i++)
            {
                transplantmed_actions[i] = ";;;" + transplantmed_actions[i];
            }

            int ct = 0;
            var query = StartNewQuery(SearchDepth.SearchSince9Hrs);
            query = AndItemFilter(query, "", "MED", desc, "", "");
            query = query.Where(e => e.DESCRIPTION.ToLower().ContainsAny(transplantmed_actions));
            ct = query.Count();
            if (ct > 0) SetInd(ind, "Found Med=" + desc);

        }

        private int CountMedsIn30min()
        {
            string descript = "";
            string drugclass = "";
            string result = "";
            int ct = 0;
            DateTime evdt = DateTime.MinValue;
            DateTime endevdt = DateTime.MinValue;

            //  CALCIUM CHLORIDE 100 MG / ML(10 %) INTRAVENOUS SYRINGE; ; ; 29; ; ; 1; ; ; IV; ; ; Given
            //  SODIUM BICARBONATE 8.4 % (1 MEQ / ML) INJECTION(WRAPPER); ; ; 29; ; ; 50; ; ; osse; ; ; Given
            var query = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
            query = query.Where(e => e.CODE.ToUpper().StartsWith("MED"));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            //Program.VerboseAudit("Num Meds found: " + query.Count());
            foreach (var item in query)
            {
                Program.VerboseAudit("Med=" + item.EVENT_DATETIME + ": " + item.DESCRIPTION);
                if (item.EVENT_DATETIME > evdt)
                {
                    evdt = item.EVENT_DATETIME;
                    endevdt = evdt.AddMinutes(30);
                    if (endevdt > loc_out) endevdt = loc_out;
                    var query2 = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
                    query2 = query2.Where(e => e.CODE.ToUpper().StartsWith("MED"));
                    query2 = query2.Where(e => e.EVENT_DATETIME >= evdt && e.EVENT_DATETIME <= endevdt);
                    if (query2.Count() > ct)
                        ct = query2.Count();
                }
            }

            Program.VerboseAudit("Greatest count of unique Admin times in a 30 minute period=" + ct);
            return ct;
        }
        private int CountIntravenous()
        {
            int ct = 0;
            int count = 0;
            DateTime evdt = DateTime.MinValue;
            DateTime endevdt = DateTime.MinValue;

            var query = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
            query = query.Where(e => e.CODE.ToUpper().StartsWith("MED"));
            query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;intravenous"));
            query = query.OrderBy(e => e.EVENT_DATETIME);
            //Program.VerboseAudit("Num Meds found: " + query.Count());
            foreach (var item in query)
            {
                Program.VerboseAudit("IV Med=" + item.EVENT_DATETIME + ": " + item.DESCRIPTION);
                if (item.EVENT_DATETIME > evdt)
                {
                    evdt = item.EVENT_DATETIME;
                    endevdt = evdt.AddHours(8);
                    if (endevdt > loc_out) endevdt = loc_out;
                    var query2 = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
                    query2 = query2.Where(e => e.CODE.ToUpper().StartsWith("MED"));
                    query2 = query2.Where(e => e.DESCRIPTION.ToLower().Contains(";;intravenous"));
                    query2 = query2.Where(e => e.EVENT_DATETIME >= evdt && e.EVENT_DATETIME <= endevdt);
                    count = query2.Count();
                    if (count > ct)
                        ct = count;
                }
            }

            Program.VerboseAudit("Greatest count of IV Admin times in an 8-hour period=" + ct);
            return ct;
        }


        private void Check_21_22()
        {
            string reslist;
            bool st1 = false;
            string piv;
            string codelist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("21. Wound/Injury Mgmt");
            Program.VerboseAudit("22. Wound/Injury Mgmt >= 30 Minutes");
            Program.VerboseAudit("---------------");

            DateTime nowdt = _pat.pull_finish;
            string res;

            if (Exists("", "IVTYPE1", "", "", "Sheath", SearchDepth.SearchSince24Hrs))
                SetIndIfResultContains(22, "", "IVDCDT1", "", "", "", SearchDepth.SearchSince24Hrs);
            if (Exists("", "IVTYPE2", "", "", "Sheath", SearchDepth.SearchSince24Hrs))
                SetIndIfResultContains(22, "", "IVDCDT2", "", "", "", SearchDepth.SearchSince24Hrs);
            if (Exists("", "IVTYPE3", "", "", "Sheath", SearchDepth.SearchSince24Hrs))
                SetIndIfResultContains(22, "", "IVDCDT3", "", "", "", SearchDepth.SearchSince24Hrs);
            if (Exists("", "IVTYPE4", "", "", "Sheath", SearchDepth.SearchSince24Hrs))
                SetIndIfResultContains(22, "", "IVDCDT4", "", "", "", SearchDepth.SearchSince24Hrs);

            //SetIndIfResultContains(22, "", "NUR0792", "", "", "");
            //SetIndIfResultContains(22, "", "RESP1DRS,RESP2DRS,RESP3DRS", "", "", "");
            SetIndIfResultContains(22, "", "WNDDRST1,WNDDRST2,WNDDRST3,WNDDRST4,WNDDRST5,WNDDRST6", "", "", "Wet To Dry,Wound Vac,Diakins Solution,Prevena", SearchDepth.SearchSince24Hrs);
            if (GetResult("", "NPWT", "", "", out res, SearchDepth.SearchSince24Hrs))
                if (res != "")
                    SetInd(22, "NPWT found with " + res);
            SetIndIfResultContains(22, "", "PRSULCST1,PRSULCST2,PRSULCST3,PRSULCST4,PRSULCST5,PRSULCST6,PRSULCST7,PRSULCST8", "", "", "3,4,Unstageable", SearchDepth.SearchSince24Hrs);
            //CheckPressUlcer22();  count of 3 or more
            SetIndIfResultContains(22, "", "APPLNCCHG1", "", "", "",SearchDepth.SearchSince24Hrs);
            SetIndIfResultContains(22, "", "APPLNCCHG2", "", "", "", SearchDepth.SearchSince24Hrs);
            SetIndIfResultContains(22, "", "DTDRSCHG1", "", "", "", SearchDepth.SearchSince24Hrs);
            SetIndIfResultContains(22, "", "DTDRSCHG2", "", "", "", SearchDepth.SearchSince24Hrs);
            SetIndIfResultContains(22, "", "DTDRSCHG3", "", "", "", SearchDepth.SearchSince24Hrs);
            SetIndIfResultContains(22, "", "NUR0092", "", "", "");
            SetIndIfResultContains(22, "", "NUR0793", "", "", "");



            SetIndIfResultContains(21, "", "CON00002", "", "", "");
            SetIndIfResultContains(21, "", "CIRCCOND", "", "", "", SearchDepth.SearchSince24Hrs);
            SetIndIfResultContains(21, "", "UMBILICAL", "", "", "", SearchDepth.SearchSince24Hrs);
            SetIndIfResultContains(21, "", "PERINEUM", "", "", "", SearchDepth.SearchSince24Hrs);
            SetIndIfResultContains(21, "", "DRSCOND", "", "", "", SearchDepth.SearchSince24Hrs);
            SetIndIfResultContains(21, "", "OSTOMY", "", "", "", SearchDepth.SearchSince24Hrs);
            SetIndIfResultContains(21, "", "PERIWND1,PERIWND2,PERIWND3,PERIWND4,PERIWND5,PERIWND6", "", "", "", SearchDepth.SearchSince24Hrs);
            SetIndIfResultContains(21, "", "WNDDRST1,WNDDRST2,WNDDRST3,WNDDRST4,WNDDRST5,WNDDRST6", "", "", "", SearchDepth.SearchSince24Hrs);
            SetIndIfResultContains(21, "", "PRSULCST1,PRSULCST2,PRSULCST3,PRSULCST4,PRSULCST5,PRSULCST6,PRSULCST7,PRSULCST8", "", "", "1,2,Suspected Deep Tissue", SearchDepth.SearchSince24Hrs);

            if (_inds[21].is_checked) return;
            CheckDressing("RESP1DRSCHG", "RESPDCDT1");
            CheckDressing("RESP2DRSCHG", "RESPDCDT2");
            CheckDressing("RESP3DRSCHG", "RESPDCDT3");
            CheckDressing("RESP4DRSCHG", "RESPDCDT4");
            CheckDressing("RESP5DRSCHG", "RESPDCDT5");
            CheckDressing("RESP6DRSCHG", "RESPDCDT6");

            if (_inds[21].is_checked) return;
            string nowstr = nowdt.ToString("yyyyMMdd");
            EqualDate(21, "IV1DRSCHG", nowstr);
            EqualDate(21, "IV2DRSCHG", nowstr);
            EqualDate(21, "IV3DRSCHG", nowstr);
            EqualDate(21, "IV4DRSCHG", nowstr);
            //EqualDate("RESP1DRSCHG", nowstr);
            //EqualDate("RESP2DRSCHG", nowstr);
            //EqualDate("RESP3DRSCHG", nowstr);
            //SetIndIfResultContains(21, "", "IV1DRSCHG,IV2DRSCHG,IV3DRSCHG,IV4DRSCHG", "", "", "");
            // date comes across as 20141023 if today then trigger.

            //These checks are expensive--leave as last resort
            if (_inds[21].is_checked) return;
            reslist = "Fecal Bag,Fecal Containment Device,Nasogastric L,Nasogastric R";
            reslist += ",Oral Gastric Tube,Rectal Tube,Weighted Feeding Tube";
            CheckItemsForDiscontinue("GITUBE1,GITUBE2,GITUBE3", "GIDSCH1,GIDSCH2,GIDSCH3", reslist);
            reslist = "";
            CheckItemsForDiscontinue("GUTYPE1,GUTYPE2", "GUDSCH1,GUDSCH2", reslist);
            CheckItemsForDiscontinue("NEURO1,NEURO2,NEURO3,NEURO4", "NEURDSCH1,NEURDSCH2,NEURDSCH3,NEURDSCH4", reslist);
            CheckItemsForDiscontinue("WNDINC1,WNDINC2,WNDINC3", "SKDSCH1,SKDSCH2,SKDSCH3", reslist);



        }
        private void CheckDressing(string drscode, string drsDCcode)
        {
            string dtstr;
            string dt2str;

            if (GetResult("", drscode, "", "", out dtstr, SearchDepth.SearchSince24Hrs))
            {
                if (dtstr.Length > 0)
                {
                    if (GetResult("", drsDCcode, "", "", out dt2str, SearchDepth.SearchSince24Hrs))
                    {
                        if (dt2str.Length > 0)
                        {
                            Program.VerboseAudit(drscode + " found with value: " + dtstr + " AND " + drsDCcode + " found with value: " + dt2str);
                        }
                    }
                    else
                    {
                        SetInd(21, drscode + " found with value: " + dtstr);
                    }
                }
            }
        }
        private void CheckItemsForDiscontinue(string startlist, string finlist, string not_reslist)
        {
            int ct = 0;
            int fct = 0;
            DateTime sevdt = DateTime.MinValue;
            DateTime fminevdt = DateTime.MinValue;
            string fresult;

            if (_inds[21].is_checked) return;

            var sarr = SplitOnCommaAndPrepareElements(startlist);
            var farr = SplitOnCommaAndPrepareElements(finlist);
            if (sarr.GetUpperBound(0) != farr.GetUpperBound(0)) return;

            for (int i = 0; i <= sarr.GetUpperBound(0); i++)
            {
                //search for the latest sarr[i]   get its eventdt
                //if found then
                //  get the first farr[i] that has eventdt > arr[i]
                //  if the result of farr[i] is a date that is  <= today-1 then dont trigger
                //  else trigger
                var query = StartNewQuery();
                query = AndItemFilter(query, "", sarr[i].ToString(), "", "", "");
                query = query.Where(e => e.EVENT_DATETIME >= DateTime.Today.AddHours(-24));
                query = query.Where(e => e.RESULT.Trim() != "");
                if (not_reslist != "")
                    query = query.Where(e => !not_reslist.ToLower().Contains(e.RESULT.ToLower()));
                ct = query.Count();
                if (ct > 0)
                {
                    sevdt = query.Select(e => e.EVENT_DATETIME).Max();
                    Program.VerboseAudit("start event time of " + sarr[i].ToString() + " = " + sevdt.ToString());
                    var fquery = StartNewQuery();
                    fquery = AndItemFilter(fquery, "", farr[i].ToString(), "", "", "");
                    fquery = fquery.Where(e => e.EVENT_DATETIME >= sevdt);
                    fct = fquery.Count();
                    if (fct > 0)
                    {
                        fminevdt = fquery.Select(e => e.EVENT_DATETIME).Min();
                        Program.VerboseAudit("corresponding stop evdt " + farr[i].ToString() + " = " + fminevdt.ToString());
                        fquery = fquery.Where(e => e.EVENT_DATETIME == fminevdt);
                        fresult = fquery.Select(e => e.RESULT).First();
                        Program.VerboseAudit("corresponding stop res " + farr[i].ToString() + " = " + fresult);
                        //fresult now has the result string: yyyymmdd
                        var stopDate = DateTime.ParseExact(fresult, "yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                        if (DateTime.Compare(stopDate, DateTime.Today.AddDays(-1).Date) > 0) //stop>today-1
                            SetInd(21, sarr[i].ToString() + " with " + farr[i].ToString() + " value of " + fresult);
                        //if (DateTime.Compare(t1, t2) >  0) Console.WriteLine("t1 > t2"); 
                        //if (DateTime.Compare(t1, t2) == 0) Console.WriteLine("t1 == t2"); 
                        //if (DateTime.Compare(t1, t2) <  0) Console.WriteLine("t1 < t2");
                    }
                    else
                        SetInd(21, sarr[i].ToString() + " with no corresponding discontinue.");
                }

            }

        }

        private void CheckDrainLDA()
        {
            string codelist = "9993040108530,3045001091";
            DateTime dt = DateTime.MinValue;
            var query1 = StartNewQuery(SearchDepth.SearchSince16Hrs);
            query1 = AndItemFilter(query1, "", codelist, "", "", "");
            query1 = query1.OrderBy(e => e.EVENT_DATETIME);
            foreach (var item in query1)
            { // for each start, find the number of records within 1 hr of it.
                if (dt == DateTime.MinValue)
                {
                    int ct = CountDrainLDAIn1Hour(codelist, item.EVENT_DATETIME);
                    if (ct >= 6)
                    {
                        SetInd(22, "Count of Drain LDAs within 1 hour=" + ct + " starting at: " + item.EVENT_DATETIME.ToString());
                        dt = item.EVENT_DATETIME;
                    }
                }
            }
            return;
        }

        private int CountDrainLDAIn1Hour(string codelist, DateTime startdt)
        {
            int ct = 0;
            var query1 = StartNewQuery(SearchDepth.SearchSince16Hrs);
            query1 = AndItemFilter(query1, "", codelist, "", "", "");
            query1 = query1.Where(e => e.EVENT_DATETIME >= startdt);
            query1 = query1.Where(e => e.EVENT_DATETIME <= startdt.AddMinutes(60));
            ct = query1.Count();
            return ct;

        }

        int EducMins(string timecode, string nursecode)
        {
            string res;
            int val = 0;
            if (ResultContains("", nursecode, "", "", "Nursing"))
            {
                if (GetResult("", timecode, "", "", out res))
                {
                    if (res.Left(1).IsNumeric())
                    {
                        val += (int)res.Val();
                    }
                }
            }
            return val;
        }

        private void Check_23()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("23. Healthcare Mgmt Education >= 1 Hour");
            Program.VerboseAudit("---------------");

            var ed1 = new bool[7 + 1];           // This 1 based so add one
            var ed2 = new bool[23 + 1];

            int ct = 0;
            string res = "";

            string codelist = "ASTHMA,DIABETES,HRTFAIL,RENALFAIL,STROKE,PNEUMON,REHAB,CANCER,MEDED,POSTED,";
            codelist += "VADED,TRNED,HOMECARE,SECSMOKE";
            SetIndIfResultContains(23, "", codelist, "", "", "");

            if (GetResult("", "HOMECARE", "", "", out res))
                if (res != "") SetInd(23, "HOMECARE found with " + res);
            //if (GetResult("", "EDUCGIVENTO", "", "", out res))
            //    if (res != "") SetInd(23, "EDUCGIVENTO found with " + res);
            if (GetResult("", "WRITWARF", "", "", out res))
                if (res != "") SetInd(23, "WRITWARF found with " + res);
            if (GetResult("", "WRITORAL", "", "", out res))
                if (res != "") SetInd(23, "WRITORAL found with " + res);
            if (GetResult("", "WRITINJ", "", "", out res))
                if (res != "") SetInd(23, "WRITINJ found with " + res);

            //codelist = "ASTHMA,DIABETES,HRTFAIL,RENALFAIL,STROKE,PNEUMON,REHAB,CANCER";
            //codelist += ",POSTED,VADED,TRNED";
            //SetIndIfResultContains(23, "", codelist, "", "", "");
            SetIndIfResultContains(23, "", "MEDED", "", "", "Self Administration");

            int i = 0;
            ed2[i++] = ResultContains("", "INFCARE", "", "", "");
            ed2[i++] = ResultContains("", "INFBHV", "", "", "");
            ed2[i++] = ResultContains("", "INFTEMP", "", "", "");
            ed2[i++] = ResultContains("", "INFBATH", "", "", "");
            ed2[i++] = ResultContains("", "FORMFEED", "", "", "");
            ed2[i++] = ResultContains("", "BRSTFEED", "", "", "");
            ed2[i++] = ResultContains("", "SPECFEED", "", "", "");
            ed2[i++] = ResultContains("", "INFNUTR", "", "", "");
            ed2[i++] = ResultContains("", "INFPROPH", "", "", "");
            ed2[i++] = ResultContains("", "INFINFCT", "", "", "");
            ed2[i++] = ResultContains("", "INFCARST", "", "", "");
            ed2[i++] = ResultContains("", "INFBLS", "", "", "");
            ed2[i++] = ResultContains("", "SHKNBABY", "", "", "");
            ed2[i++] = ResultContains("", "SIDSPREV", "", "", "");
            ed2[i++] = ResultContains("", "JAUNDICE", "", "", "");
            ed2[i++] = ResultContains("", "INFHEAR", "", "", "");
            ed2[i++] = ResultContains("", "INFCIRCUM", "", "", "");
            ed2[i++] = ResultContains("", "SKINTOSKIN", "", "", "");
            ed2[i++] = ResultContains("", "CONGHRT", "", "", "");
            ed2[i++] = ResultContains("", "INFMED", "", "", "");
            ed2[i++] = ResultContains("", "INFBPD", "", "", "");
            ed2[i++] = ResultContains("", "INFCALL", "", "", "");
            ed2[i++] = ResultContains("", "OTHTOPIC", "", "", "");
            ct = 0;
            foreach (var b in ed2)
            {
                if (b) ct++;
            }

            if (ct >= 3)
            {
                SetInd(23, "At least 3 of INFCARE..OTHTOPIC found");
            }

            ct = CountItems("", "SPECFEED", "", "", "");
            if (ct >= 2)
            {
                SetInd(23, "At least 2 of SPECFEED found");
            }


        }
        private void AddEducActivity(DateTime evdt)
        {
            DateTime enddt;
            Program.VerboseAudit("Activity 5: Found at " + evdt.AddHours(-1).ToString());
            if (!QueuedProcOverlaps(5, evdt.AddHours(-1), evdt))
                if (!ProcExistsInDB(5, evdt.AddHours(-1), out enddt))
                {
                    var proc = new proc_data();
                    proc.procedure_number = 5;
                    proc.start = evdt.AddHours(-1);
                    proc.finish = evdt;
                    _procs.Add(proc);
                    Program.Audit("Activity 5: Found at " + evdt);
                }
        }

        private void Check_24()
        {
            int v;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("24. 1 to 1 Physiological Interv. >= 2 Hours");
            Program.VerboseAudit("---------------");


            SetIndIfResultContains(24, "", "REASFORACT", "", "", "");
            
            string found_what = "";
            if (OrderInProgressByCode("NUR0987", out found_what))
                SetInd(24, found_what);

            //v = GetTotalValue("", "MINSPHYSINT", "", "", "");
            //if (v >= 2)
            //{
            //    SetInd(24, "MINSPHYSINT hours = " + v);
            //    if (v >= 6)
            //    {
            //        SetInd(18, "MINSPHYSINT hours = " + v);
            //    }

            //}
        }


        private void CheckUserDefined()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("User-Defined indicators");
            Program.VerboseAudit("---------------");

            //reslist = "Patient arrived during downtime,Patient was cared for during downtime,Patient departed during downtime";
            //SetIndIfResultContains(99, "", "9991600100203", "", "", reslist);
            //reslist = "Yes";
            //SetIndIfResultContains(99, "", "9990000006437", "", "", reslist);
            //reslist = "Green,Yellow ,Red ,Black ";
            //SetIndIfResultContains(99, "", "9991600100265", "", "", reslist);
            //reslist = "Downtime";
            //SetIndIfResultContains(99, "", "9990007096330", "", "", reslist);

        }


        //=====================================================================
        // Group like assessments into buckets of time
        // Each bucket counts as one assessment in the frequency count

        //private void AddDependentBuckets(List<gBucket> bucket_list, string codelist1, string reslist1, string codelist2, string reslist2)
        //{
        //    AddDependentBuckets(bucket_list, codelist1, reslist1, codelist2, reslist2, "", "");
        //}

        //private void AddDependentBuckets(List<gBucket> bucket_list, string codelist1, string reslist1, string codelist2, string reslist2, string codelist3,string reslist3)
        //{
        //    bool dep3 = true;
        //    // get the chart items for the assessments
        //    var query1 = StartNewQuery();
        //    query1 = AndItemFilter(query1, "", codelist1, "", "", reslist1);
        //    var query2 = StartNewQuery();
        //    query2 = AndItemFilter(query2, "", codelist2, "", "", reslist2);
        //    if (codelist3.Trim() == "")
        //    {
        //        dep3 = false;
        //        codelist3 = "Hello, this is a phantom code";
        //    }
        //    var query3 = StartNewQuery();
        //    query3 = AndItemFilter(query3, "", codelist3, "", "", reslist3);

        //    // figure out what buckets the events belong to
        //    var query1a = from item in query1
        //                  select new
        //                  {
        //                      bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
        //                      code = item.CODE,
        //                      evdt = item.EVENT_DATETIME
        //                  };
        //    var query2a = from item in query2
        //                  select new
        //                  {
        //                      bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
        //                      code = item.CODE,
        //                      evdt = item.EVENT_DATETIME
        //                  };
        //    var query3a = from item in query3
        //                      select new
        //                      {
        //                          bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
        //                          code = item.CODE,
        //                          evdt = item.EVENT_DATETIME
        //                      };

        //    string s = "BucketList1 for " + codelist1 + ": ";
        //    foreach (var item in query1a)
        //    {
        //        s += item.bucket + "";
        //    }
        //    Program.VerboseAudit(s);

        //    s = "BucketList2 for " + codelist2 + ": ";
        //    foreach (var item in query2a)
        //    {
        //        s += item.bucket + "";
        //    }
        //    if (dep3)
        //    {
        //        s = "BucketList3 for " + codelist3 + ": ";
        //        foreach (var item in query3a)
        //        {
        //            s += item.bucket + "";
        //        }
        //    }
        //    Program.VerboseAudit(s);
        //    // Add to the list IFF items in both lists occur in same bucket
        //    foreach (var item1 in query1a)
        //    {
        //        foreach (var item2 in query2a)
        //        {
        //            if (item1.bucket == item2.bucket)
        //            {
        //                if (dep3)
        //                {
        //                    foreach (var item3 in query3a)
        //                    {
        //                        if (item1.bucket == item3.bucket)
        //                        {
        //                            var b = new gBucket();
        //                            b.bucket = item1.bucket;
        //                            b.code = item1.code;
        //                            b.evdt = item1.evdt;
        //                            bucket_list.Add(b);
        //                        }
        //                    }
        //                }
        //                else
        //                {
        //                    foreach (var item3 in query2a)
        //                    {
        //                        if (item1.bucket == item2.bucket)
        //                        {
        //                            var b = new gBucket();
        //                            b.bucket = item1.bucket;
        //                            b.code = item1.code;
        //                            b.evdt = item1.evdt;
        //                            bucket_list.Add(b);
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }

        //}

        private void AddSimpleProc(int pnum, DateTime evdt, DateTime enddt)
        {
            if (pnum <= 0) return;

            if (ProcExists(pnum, evdt, enddt))
            {
                Program.Audit("Activity " + pnum + ": already exists");
            }
            else
            {
                //if (ActivityFits(evdt, enddt))
                {
                    ProcOverlapsInDB_PEID(pnum, evdt, enddt); // then delete the db
                    var proc = new proc_data();
                    proc.procedure_number = pnum;
                    proc.start = evdt;
                    proc.finish = enddt;
                    _procs.Add(proc);
                    Program.Audit("Activity " + pnum + ": Found between " + evdt + " and " + enddt);
                }
            }
        }

        private bool ActivityFits(DateTime beg, DateTime fin)
        {
            bool ok = false;
            int unit_id = 0;
            string sql = "select el.unit_id from ENCOUNTER_LOCATION as el";
            sql += " where el.ENCOUNTER_ID=" + _pat.encounter_id;
            //sql += " and el.SPECIAL_UNIT_ID is null";
            sql += " and el.EFFECTIVE_DATETIME_IN<=" + beg.ToString() + "";
            sql += " and el.EFFECTIVE_DATETIME_OUT>=" + fin.ToString() + "";

            Program.VerboseAudit(sql);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd = new SqlCommand(sql, db2);
            SqlDataReader dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr2.Read())
            {
                if (dr2["UNIT_ID"] != DBNull.Value)
                    unit_id = PFSDBUtility.DBToInt(dr2["UNIT_ID"]);
            }
            ok = (unit_id > 0);

            if (ok)
            {
                db2.Close();
                return ok;
            }
            //Now check if two adjacent same units contains the activity.
            int unitid1 = 0;
            int unitid2 = 0;
            sql = "select el.unit_id from ENCOUNTER_LOCATION as el";
            sql += " where el.ENCOUNTER_ID=" + _pat.encounter_id;
            sql += " and " + beg.ToString() + "' between el.EFFECTIVE_DATETIME_IN and el.EFFECTIVE_DATETIME_OUT";
            cmd = new SqlCommand(sql, db2);
            dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr2.Read())
            {
                if (dr2["UNIT_ID"] != DBNull.Value)
                    unitid1 = PFSDBUtility.DBToInt(dr2["UNIT_ID"]);
            }
            sql = "select el.unit_id from ENCOUNTER_LOCATION as el";
            sql += " where el.ENCOUNTER_ID=" + _pat.encounter_id;
            sql += " and " + fin.ToString() + "' between el.EFFECTIVE_DATETIME_IN and el.EFFECTIVE_DATETIME_OUT";
            cmd = new SqlCommand(sql, db2);
            dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr2.Read())
            {
                if (dr2["UNIT_ID"] != DBNull.Value)
                    unitid2 = PFSDBUtility.DBToInt(dr2["UNIT_ID"]);
            }
            ok = (unitid1 > 0 && unitid1 == unitid2);
            //db2.Close();
            return ok;


        }

        //6	2019-09-18 15:00:00.000	2019-09-18 19:00:00.000
        //6	2019-09-19 07:00:00.000	2019-09-19 11:00:00.000

        private bool ActivityDuringClassType6(DateTime beg, DateTime fin, out DateTime classout)
        {
            bool ok = false;
            int pt = 0;
            classout = DateTime.MinValue;
            string sql = "select ce.PATIENT_TYPE,ce.effective_datetime_in,ce.effective_datetime_out from CLASSIFICATION_EVENT as ce";
            sql += " where ce.ENCOUNTER_ID=" + _pat.encounter_id;
            sql += " and ce.PATIENT_TYPE=6";
            sql += " and (" + beg.ToString() + "' >=ce.EFFECTIVE_DATETIME_IN and " + beg.ToString() + "' <ce.EFFECTIVE_DATETIME_OUT";
            sql += " or " + beg.ToString() + "' <ce.EFFECTIVE_DATETIME_IN and " + fin.ToString() + "' >ce.EFFECTIVE_DATETIME_OUT";
            sql += " or " + fin.ToString() + "' >ce.EFFECTIVE_DATETIME_IN and " + fin.ToString() + "' <=ce.EFFECTIVE_DATETIME_OUT)";
            Program.VerboseAudit(sql);
            var db2 = PFSDBUtility.NewSqlConnection();
            var cmd = new SqlCommand(sql, db2);
            SqlDataReader dr2 = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr2.Read())
            {
                if (dr2["PATIENT_TYPE"] != DBNull.Value)
                {
                    pt = PFSDBUtility.DBToInt(dr2["PATIENT_TYPE"]);
                    classout = PFSDBUtility.DBToDateTime(dr2["EFFECTIVE_DATETIME_OUT"]);
                }
            }
            ok = (pt == 6);
            db2.Close();
            return ok;
        }

        private void AtLeastOneADL()
        {
            Program.Audit("---------------");

            if (!(_inds[2].is_checked || _inds[3].is_checked || _inds[4].is_checked)) {
                // Make "#2 ADL - Assist" the default.  (90% of patients)
                SetInd(1, "Defaulting to ADL Self due to lack of documentation.");
            }
        }

        private void HighestIndicatorInEachGroupWins()
        {
            int i, g;
            bool highest_is_on;
            string ind_list;

            Program.Audit("Select highest indicator in each group");

            g = 0;
            highest_is_on = false;
            // Go from bottom up - highest (lowest) indicator in each group wins
            for (i = MAX_INDS; (i >= 1); i--) {
                if (_inds[i].radio_group > 0) {
                    if (_inds[i].radio_group != g) {
                        //this is a new group
                        g = _inds[i].radio_group;
                        highest_is_on = _inds[i].is_checked;
                    } else {
                        //same group
                        if (highest_is_on) {
                            _inds[i].is_checked = false;             //uncheck a lower number
                        } else {
                            highest_is_on = _inds[i].is_checked;     //save this one
                        }
                    }
                }
            } // next i

            //Echo the indicators for an Audit (no classification will be saved)
            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++) {
                if (_inds[i].is_checked) ind_list += "," + i;
            } // next i

            Program.Audit("Final list = " + ind_list.Substring(1));
            Program.Audit("---------------");
        }


        private void CheckProcs()
        {
            //CheckProc_1();
            CheckProc_2();
            CheckProc_3();
            CheckProc_4();
            //CheckProc_5();
            CheckProc_6();
            CheckProc_7();
            CheckProc_8();
            CheckProc_9();
            CheckProc_10();
            CheckProc_11();

        }

        //private void DoProc(int pnum, string code)
        //{
        //    double mins = 0;
        //    string found_what;
        //    DateTime evdt;
        //    DateTime enddt = DateTime.MinValue;

        //    if (GetResultAndEVDT("", code, "", "", out found_what, out evdt))
        //    {
        //        //mins = 60.0 * found_what.ToDouble();
        //        if (found_what.Contains("180")) mins = 180;
        //        else if (found_what.Contains("120")) mins = 120;
        //        else if (found_what.Contains("90")) mins = 90;
        //        else if (found_what.Contains("60")) mins = 60;
        //        enddt = evdt.AddMinutes(mins);

        //        if (ProcExistsInDB(pnum, evdt, enddt))
        //        {
        //            Program.Audit("Activity " + pnum+ ": already exists");
        //        }
        //        else
        //        {
        //            if (!QueuedProcOverlaps(pnum, evdt, enddt))
        //            {
        //                var proc = new proc_data();
        //                proc.procedure_number = pnum;
        //                proc.start = evdt;
        //                proc.finish = enddt;
        //                _procs.Add(proc);
        //                Program.Audit("Activity " + pnum + ": Found " + code + " between " + evdt + " and " + enddt);
        //            }
        //        }

        //    }

        //}

        private bool ProcOverlapsInDB_PEID(int pnum, DateTime startdt, DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            //            LoadPatientProceduresIfNeeded();
            bool overlap_exists = false;
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && ((proc.PROCEDURE_DATETIME >= startdt) && (proc.PROCEDURE_DATETIME < enddt)
                                ||
                                (proc.DEPARTURE_DATETIME > startdt) && (proc.DEPARTURE_DATETIME <= enddt)
                                ||
                                (proc.PROCEDURE_DATETIME < startdt) && (proc.DEPARTURE_DATETIME > enddt)
                                )
                            //&& ( ! (proc.PROCEDURE_DATETIME == startdt) && (proc.DEPARTURE_DATETIME == enddt))
                            && (proc.CLASSIFIED_BY_ID < 0)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new { proc.PROCEDURE_EVENT_ID, proc.PROCEDURE_DATETIME, proc.DEPARTURE_DATETIME };
            overlap_exists = (query.Count() > 0);
            foreach (var a in query)
            {
                Program.VerboseAudit("Will Delete act: procdt=" + a.PROCEDURE_DATETIME.ToString() + "  depdt=" + a.DEPARTURE_DATETIME.ToString());
                Program.VerboseAudit("because it overlays startdt=" + startdt.ToString() + "  enddt=" + enddt.ToString());
                DeleteActivity(a.PROCEDURE_EVENT_ID);
            }
            //            peid = 0;
            return (overlap_exists);
        }
        private void DeleteActivity(int peid)
        {
            //            delete PROCEDURE_ANSWER where PROCEDURE_EVENT_ID=7211351
            //delete RPT_PROC_BY_DAY where PROCEDURE_EVENT_ID=7211351
            //delete PROCEDURE_EVENT where PROCEDURE_EVENT_ID=7211351
            if (peid == 0) return;

            Program.VerboseAudit("db ProcAnsw Deleting peid=" + peid);
            var db = PFSDBUtility.NewPfsDataContext();
            var query = from ia in db.PROCEDURE_ANSWERs
                        where (ia.PROCEDURE_EVENT_ID == peid)
                        select ia;
            if (query.Count() > 0)
            {
                var items = query.ToList();
                foreach (var item in items)
                    db.PROCEDURE_ANSWERs.DeleteOnSubmit(item);
                db.SubmitChanges();
            }

            Program.VerboseAudit("db RptProc Deleting peid=" + peid);
            var db2 = PFSDBUtility.NewPfsDataContext();
            var query2 = from r in db2.RPT_PROC_BY_DAYs
                         where (r.PROCEDURE_EVENT_ID == peid)
                         select r;
            if (query2.Count() > 0)
            {
                var items2 = query2.ToList();
                foreach (var item2 in items2)
                    db2.RPT_PROC_BY_DAYs.DeleteOnSubmit(item2);
                db2.SubmitChanges();
            }

            Program.VerboseAudit("db ProcEvent Deleting peid=" + peid);
            var db3 = PFSDBUtility.NewPfsDataContext();
            var query3 = from ce in db3.PROCEDURE_EVENTs
                         where (ce.PROCEDURE_EVENT_ID == peid)
                         select ce;
            if (query3.Count() > 0)
            {
                var items3 = query3.ToList();
                foreach (var item3 in items3)
                    db3.PROCEDURE_EVENTs.DeleteOnSubmit(item3);
                db3.SubmitChanges();
            }
        }



        private bool QueuedProcOverlaps(int pnum, DateTime startdt, DateTime enddt)
        {
            bool overlap = false;

            Program.VerboseAudit("Activity " + pnum + ": Check for dup at " + startdt.ToString() + " - " + enddt.ToString());
            proc_data[] pary = _procs.ToArray();
            for (int i = 0; i <= pary.GetUpperBound(0); i++)
            {
                if (pary[i].procedure_number == pnum)
                {
                    //overlap if   p.start between stardt and enddt
                    //             p.finish between startdt and enddt
                    //             startdt >= p.start and enddt <= p.finish
                    if ((pary[i].start >= startdt) && (pary[i].start < enddt) ||
                        (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
                        (startdt >= pary[i].start) && (enddt <= pary[i].finish))
                    {
                        overlap = true;
                    }

                    if ((pary[i].start >= startdt) && (pary[i].start < enddt) && (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
                        (pary[i].start < startdt) && (pary[i].finish > startdt) && (pary[i].finish < enddt)
                        )
                    {
                        // the proc in the list is completely contained in startdt/endt pair
                        // change this proc to be the startdt/enddt
                        // overlap is still true, but the list item will be updated with the encompassing times
                        //Program.Audit("Procedure " + pnum + ": " + startdt + " - " + enddt + " will trump " + pary[i].start + " - " + pary[i].finish);
                        pary[i].start = startdt;
                        pary[i].finish = enddt;
                    }
                }
            }
            _procs = pary.ToList();
            Program.VerboseAudit("Activity " + pnum + ": Check for dup returns " + overlap);
            return overlap;
        }

        private bool ProcExistsInDB(int pnum, DateTime startdt, out DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            Program.VerboseAudit("ProcExistsInDB: pnum=" + pnum + " startdt=" + startdt.ToString());
            int ct = 0;
            enddt = DateTime.MinValue;
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            //&& (proc.PROCEDURE_DATETIME <= startdt)
                            && (proc.DEPARTURE_DATETIME > startdt)
                            && (((pnum > 2) && (ans.PROCEDURE_NUMBER == pnum)) || ((pnum <= 2) && (ans.PROCEDURE_NUMBER <= 2)))
                        orderby proc.DEPARTURE_DATETIME descending
                        select new { proc.DEPARTURE_DATETIME };
            ct = query.Count();
            //Program.VerboseAudit("ProcExistsInDB: ct=" + ct);
            //if (ct > 0)
            //{
            //    foreach (var x in query)
            //    {
            //        Program.VerboseAudit("ProcExistsInDB: x=" + x.DEPARTURE_DATETIME);
            //    }
            //}
            if (ct > 0)
                enddt = (DateTime)query.First().DEPARTURE_DATETIME;
            //else //see if there is a saved proc that ended before this startdt
            //{
            //    query = from proc in _procedure_events
            //            from ans in proc.PROCEDURE_ANSWERs
            //            where (proc.ENCOUNTER_ID == _pat.encounter_id)
            //                //&& (proc.PROCEDURE_DATETIME <= startdt)
            //                && (proc.DEPARTURE_DATETIME > startdt.AddHours(-4))
            //                && (ans.PROCEDURE_NUMBER == pnum)
            //            orderby proc.DEPARTURE_DATETIME descending
            //            select new { proc.DEPARTURE_DATETIME };
            //    ct = query.Count();
            //    if (ct > 0)
            //        enddt = (DateTime)query.First().DEPARTURE_DATETIME;
            //}
            Program.VerboseAudit("ProcExistsInDB: pnum=" + pnum + " returns " + ct);
            return ct > 0;
        }
        private void ProcessProc(int pnum, int groupnum, string desc)
        {

        }
        private void NEWProcessProc(int pnum, string desc, string res, string termphrase)
        {
            string DSC1 = "NURS Med/Surg Activities";
            string DSC2 = "NURS Med/Surg Activites Freq";
            string DSC3 = "NURS Med/Surg Activities Start";

            string DSC4 = "NURSI Hour Activities";
            string DSC5 = "NURSI Hour Activities Freq";
            string DSC6 = "NURSI Hour Activities Start";

            string desc1="x", desc2 = "x", desc3 = "x";


            var arr = SplitOnCommaAndPrepareElements(res);
            //return query.Where(e => arr.Any(x => x.Contains(e.DESCRIPTION))); // "like" match

            DateTime evdt = DateTime.MinValue;
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);
            query = query.Where(e => e.DESCRIPTION.ToLower().StartsWith(desc.ToLower()));
            query = query.Where(e => e.RESULT.ToLower().ContainsAny(arr));
            query = query.Where(e => e.ORDER_CONTROL != "X");
            query = query.OrderByDescending(e => e.EVENT_DATETIME);
            Program.VerboseAudit("qct: " + query.Count());
            if (query.Count() == 0) return;

            DateTime start1 = query.First().EVENT_DATETIME; //this is the latest time


            bool found_dur = false;
            bool found_st = false;
            bool done = false;
            string dur = "";
            int stimecolpos;

            string stime; //14:00
            int durint;
            DateTime tempdate;
            string stimehr;
            string stimemin;
            DateTime date_of_event;
            DateTime dt_of_event;
            DateTime sdt;
            DateTime event_dt_plus1hr;

            foreach (var q in query)
            {
                if (evdt < q.EVENT_DATETIME && !done)
                {
                    evdt = q.EVENT_DATETIME;
                    var query2 = StartNewQuery(SearchDepth.SearchDefault);
                    query2 = query2.Where(e => e.DESCRIPTION.ToLower().StartsWith(desc2.ToLower()));
                    query2 = query2.Where(e => e.EVENT_DATETIME == evdt);
                    Program.VerboseAudit("q2ct: " + query2.Count());
                    if (query2.Count() >= 0)
                    {
                        found_dur = true;
                        done = true;
                        dur = query2.First().RESULT;
                        stimecolpos = dur.IndexOf(" ");
                        dur = dur.Substring(0, stimecolpos);
                        Program.VerboseAudit("q2 duration: " + dur);
                    }

                }
            }

            if (!found_dur)
            {
                Program.VerboseAudit("For activity: " + desc2 + " there was no frequency found.");
                return;
            }

            done = false;
            var query3 = StartNewQuery(SearchDepth.SearchDefault);
            query3 = query3.Where(e => e.DESCRIPTION.ToLower().StartsWith(desc3.ToLower()));
            query3 = query3.Where(e => e.EVENT_DATETIME == evdt);
            Program.VerboseAudit("q3ct: " + query3.Count());
            if (query3.Count() == 0)
            {
                Program.VerboseAudit("For activity: " + desc2 + " there was no start time found.");
                return;
            }

            found_st = true;
            done = true;
            stime = query3.First().RESULT;
            Program.VerboseAudit("q3 stime: " + stime);

//'determine the date for stime that is before rs(0)  say rs(0) = 2/1 01:00  stime=23:00
//' associate stime with rs(0) date.  compare this dt with rs(0)  
// if positive then ok because 2/1 23:00 is After 01:00
//' else associate stime with rs(0)date-1.  1/31 23:00


            stimecolpos = stime.IndexOf(":");
            stimehr = stime.Substring(0, stimecolpos);
            Program.VerboseAudit("q3 stimeHR: " + stimehr);
            if (stimehr.Length == 1) stimehr = "0" + stimehr;
            stimemin = stime.Substring(stimecolpos + 1);
            Program.VerboseAudit("q3 stimeMN: " + stimemin);

            event_dt_plus1hr = evdt.AddHours(1);
            Program.VerboseAudit("event_dt_plus1hr: " + event_dt_plus1hr);
            date_of_event = event_dt_plus1hr.Date;//Format$(event_dt_plus1hr, "yyyymmdd")  ' example 20150122 1400
            Program.VerboseAudit("date_of_event: " + date_of_event);
            dt_of_event = event_dt_plus1hr;// ' add an hour: 201501221500


            sdt = date_of_event.AddHours(stimehr.Val()).AddMinutes(stimemin.Val());
            Program.VerboseAudit("sdt: " + sdt);
            if (dt_of_event < sdt)  //'decrease date_of_event by 1 day
            {
                tempdate = event_dt_plus1hr;
                tempdate = tempdate.AddDays(-1);
                date_of_event = tempdate.Date;  //Format$(tempdate, "yyyymmdd")
                sdt = date_of_event.AddHours(stimehr.Val()).AddMinutes(stimemin.Val());
            }

            tempdate = sdt;

            if (found_dur && found_st)
            {
                //  durint = CInt(dur)
                //numprocs = numprocs + 1;
                //procs(numprocs).pnum = pnum
                //procs(numprocs).start = tempdate
                //procs(numprocs).finish = DateAdd("h", durint, tempdate)
                AddSimpleProc(pnum, sdt, sdt.AddMinutes(dur.Val()));
                Program.VerboseAudit("AddSimpleProc: " + pnum + "  sdt="+sdt + " dur="+dur);
            }


        }

        private void CheckProc_1()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A1. 1-1 safety observation by RN");
            Program.VerboseAudit("---------------");

            string desc = "SNCH_restraint type Level II";
            string res = "4-Point and Constant Observation";
            string terminate_phrase = "restraint removed";
            NEWProcessProc(1, desc, res, terminate_phrase);

            desc = "SNCH_Restraint Action Codes";
            res = "initial application,continued restraint,release and reapply";
            NEWProcessProc(1, desc,res,terminate_phrase);

        }

        private void CheckProc_2()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A2. 1-1 safety observation by non-RN");
            Program.VerboseAudit("---------------");

            DateTime evdt;
            if (GetEVDT("", "351023", "", "", "", 1, loc_in, out evdt, SearchDepth.SearchDefault))
                AddSimpleProc(2, evdt, evdt.AddHours(1));



            //string res = "1:1 Safety observation by non-RN";
            //ProcessProc(2, 1, res);
            //ProcessProc(2, 2, res);
        }


        private void AddSitter(string code, DateTime initdt, DateTime evdt)
        {
            string desc = "TC: Direct Observer";
            string res = "Initiated";
            DateTime timestmp = DateTime.Now;
            short seq = 0;
            int unitid = _pat.unit_id;

            using (var db = PFSDBUtility.NewSqlConnection())
            {
                string q = "INSERT INTO chart_item (encounter_id,event_datetime,code,description,result,timestamp,sequence,unit_id,procedure_start)";
                q += " select @encid, @evdt, @code, @desc, @res, @ts,@seq,@unit,@procstart where not exists";
                q += " (select encounter_id,code,event_datetime from chart_item where encounter_id=" + _pat.encounter_id.ToString() + " and code='" + code + "' and event_datetime=" + PFSDBUtility.SQLDateTime(evdt) + ")";
                //string q = "INSERT INTO chart_item (encounter_id,event_datetime,code,description,timestamp,sequence,unit_id,order_id) values (@encid, @evdt, @code, @desc, @ts,@seq,@unit,@oid)";
                SqlCommand cmd = new SqlCommand(q, db);
                cmd.Parameters.AddWithValue("@encid", _pat.encounter_id);
                cmd.Parameters.AddWithValue("@evdt", evdt);
                cmd.Parameters.AddWithValue("@code", code);
                cmd.Parameters.AddWithValue("@desc", desc);
                cmd.Parameters.AddWithValue("@res", res);
                cmd.Parameters.AddWithValue("@ts", timestmp);
                cmd.Parameters.AddWithValue("@seq", seq);
                cmd.Parameters.AddWithValue("@unit", unitid);
                cmd.Parameters.AddWithValue("@procstart", initdt);
                cmd.ExecuteNonQuery();
                db.Close();
            } //using db

        }

        private int GetSitterType(DateTime evdt)
        { // get the sitter type RN or non-RN from the charting that should exist
            //at the same time as the initiation of the observer 1540100298
            int pnum = 0;
            string return_result = "";
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);    // add custom time range below
            query = query.Where(e => e.CODE == "1540100298");
            query = query.Where(e => e.EVENT_DATETIME == evdt);
            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
                if (return_result.Trim().ToUpper().StartsWith("RN")) pnum = 1;
                if (return_result.Trim().ToUpper().StartsWith("NON-RN")) pnum = 2;
            }
            if (pnum == 0)
            {
                Program.VerboseAudit("Sitter type RN or non-RN not found. Defaulting to non_RN.");
                pnum = 2;
            }
            return pnum;
        }

        private void CheckActivity(int actnum,string actcode, string actst, string actlencode)
        {
            int actlen;
            string actsthhmm = "0000";
        
            var query = StartNewQuery(SearchDepth.SearchSince24Hrs);    // add custom time range below
            query = query.Where(e => e.CODE == actcode);
            query = query.Where(e => e.ORDER_STATUS == null || e.ORDER_STATUS == ""); // need to update order_status=x
            query = query.OrderBy(e => e.EVENT_DATETIME);
            Program.VerboseAudit("for code=" + actcode + " count=" + query.Count());
            foreach (var item in query)
            {
                if (item.RESULT.ToLower() == "yes")
                { //09500000
                    var q2 = StartNewQuery(SearchDepth.SearchSince24Hrs);    // add custom time range below
                    q2 = q2.Where(e => e.CODE == actst);
                    q2 = q2.Where(e => e.EVENT_DATETIME == item.EVENT_DATETIME);
                    Program.VerboseAudit("for code=" + actst + " count=" + q2.Count());
                    if (q2.Count() == 0) return;

                    actsthhmm = q2.First().RESULT;
                    Program.VerboseAudit("actsthhmm=" + actsthhmm);
                    if (actsthhmm.Length >= 4)
                        actsthhmm = actsthhmm.Substring(0, 4);
                    Program.VerboseAudit("actsthhmm=" + actsthhmm);

                    var q3 = StartNewQuery(SearchDepth.SearchSince24Hrs);    // add custom time range below
                    q3 = q3.Where(e => e.CODE == actlencode);
                    q3 = q3.Where(e => e.EVENT_DATETIME == item.EVENT_DATETIME);
                    Program.VerboseAudit("for code=" + actlencode + " count=" + q3.Count());
                    if (q3.Count() == 0) return;

                    actlen = (int)q3.First().RESULT.Val();
                    DateTime stdt = DateTime.MinValue;
                    if (actlen >= 60)
                        {
                        TimeSpan start = new TimeSpan(0, 0, 0); //0 o'clock
                        TimeSpan end = new TimeSpan(6, 0, 0); // 6 o'clock
                        TimeSpan item_ts = item.EVENT_DATETIME.TimeOfDay;
                        TimeSpan actst_ts = new TimeSpan((int)actsthhmm.Substring(0, 2).Val(), (int)actsthhmm.Substring(2, 2).Val(), 0);
                        stdt = item.EVENT_DATETIME.Date + actst_ts;
                        if ((item_ts >= start) && (item_ts <= end))
                        {
                            if (actst_ts > item_ts) // then it belongs to prev cal day.
                                stdt = item.EVENT_DATETIME.Date.AddDays(-1) + actst_ts;
                        }
//                      DateTime stdt = item.EVENT_DATETIME.Date.AddHours(actsthhmm.Substring(0, 2).Val()).AddMinutes(actsthhmm.Substring(2, 2).Val());
                        Program.VerboseAudit("stdt=" + stdt);
                        AddSimpleProc(actnum, stdt, stdt.AddMinutes(actlen));
                        NullifyActivity(actcode, item.EVENT_DATETIME);
                        }
                }
            }

        }

        private void NullifyActivity(string actcode, DateTime evdt)
        {
            var db = PFSDBUtility.NewSqlConnection();
            string q = "UPDATE chart_item set ORDER_STATUS='X' where encounter_id=" + _pat.encounter_id + " and event_datetime=" + PFSDBUtility.SQLDateTime(evdt) + " and code='" + actcode + "'";
            SqlCommand cmd = new SqlCommand(q, db);
            cmd.ExecuteNonQuery();
            db.Close();
        }

        private DateTime NextFinish(DateTime startdt)
        {
            DateTime dt = DateTime.MinValue;

            int b = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, startdt) / (4 * 60));
            dt = _pat.pull_start.AddMinutes((b + 1) * 240);
            Program.VerboseAudit("NextFinish: startdt=" + startdt.ToString() + " b=" + b + " dt=" + dt.ToString());
            return dt;
        }


        private bool ProcExists(int pnum, DateTime startdt, DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && (proc.PROCEDURE_DATETIME == startdt)
                            && (proc.DEPARTURE_DATETIME == enddt)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new { proc.PROCEDURE_EVENT_ID };
            return (query.Count() > 0);
        }


        private void CheckProc_3()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A3. Off unit accompanied by RN");
            Program.VerboseAudit("---------------");

            DateTime evdt;
            //if (GetEVDT("", "12067428", "", "", "Left unit", 1, loc_in, out evdt, SearchDepth.SearchDefault))
            if (GetEVDTIfAllResults("12067428", "Left Unit,Nurse", 1, loc_in, out evdt, SearchDepth.SearchDefault))
                AddSimpleProc(3, evdt, evdt.AddHours(1));

            if (GetEVDT("", "RNACT", "", "", "Off Unit Accompanied by RN > 1 Hour", 1, loc_in, out evdt, SearchDepth.SearchDefault))
                AddSimpleProc(3, evdt, evdt.AddHours(1));

            //string res = "Off unit accompanied by RN";
            //ProcessProc(3, 1, res);
            //ProcessProc(3, 2, res);

        }
        private void CheckProc_4()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A4. Off unit accompanied by Non-RN");
            Program.VerboseAudit("---------------");

            DateTime evdt;
            if (GetEVDTIfAllResults("12067428", "Left Unit,HCT", 1, loc_in, out evdt, SearchDepth.SearchDefault))
                AddSimpleProc(4, evdt, evdt.AddHours(1));

            if (GetEVDT("", "NONRNACT", "", "", "Off Unit Accompanied by Non RN > 1 Hour", 1, loc_in, out evdt, SearchDepth.SearchDefault))
                AddSimpleProc(4, evdt, evdt.AddHours(1));

        }

        private void CheckProc_5()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A5. Patient/family education by RN");
            Program.VerboseAudit("---------------");

            string res = "Patient/family education by RN";
            ProcessProc(5, 1, res);
            ProcessProc(5, 2, res);

        }

        private void CheckProc_6()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A6. Extensive wound management by RN");
            Program.VerboseAudit("---------------");


            DateTime evdt;
            if (GetEVDT("", "12053619", "", "", "", 1, loc_in, out evdt, SearchDepth.SearchDefault))
                AddSimpleProc(6, evdt, evdt.AddHours(1));

            if (GetEVDT("", "RNACT", "", "", "Extensive Wound Management by RN > 1 Hour", 1, loc_in, out evdt, SearchDepth.SearchDefault))
                AddSimpleProc(6, evdt, evdt.AddHours(1));

            //string res = "Extensive wound management by RN";
            //ProcessProc(6, 1, res);
            //ProcessProc(6, 2, res);

        }

        private void CheckProc_7()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A7. Extensive wound management by non-RN");
            Program.VerboseAudit("---------------");

            DateTime evdt;
            if (GetEVDT("", "12053623", "", "", "", 1, loc_in, out evdt, SearchDepth.SearchDefault))
                AddSimpleProc(7, evdt, evdt.AddHours(1));

            //string res = "Extensive wound management by non-RN";
            //ProcessProc(7, 1, res);
            //ProcessProc(7, 2, res);

        }

        private void CheckProc_8()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A8. Coordination of care by RN");
            Program.VerboseAudit("---------------");

            DateTime evdt;
            if (GetEVDT("", "RNACT", "", "", "Coordination of Care by RN > 1 Hour", 1, loc_in, out evdt, SearchDepth.SearchDefault))
                AddSimpleProc(8, evdt, evdt.AddHours(1));

            string res = "Discharge coordination";
            ProcessProc(8, 1, res);
            ProcessProc(8, 2, res);

            res = "Outside facility transfer coordination";
            ProcessProc(8, 1, res);
            ProcessProc(8, 2, res);

            res = "Interdisciplinary care conference/coordination";
            ProcessProc(8, 1, res);
            ProcessProc(8, 2, res);

        }

        private void CheckProc_9()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A9. 1:1 RN at bedside");
            Program.VerboseAudit("---------------");

            DateTime evdt;
            if (GetEVDT("", "12046750", "", "", "4", 1, loc_in, out evdt, SearchDepth.SearchDefault))
                AddSimpleProc(9, evdt, evdt.AddHours(1));
            if (GetEVDT("", "12045759", "", "", "", 1, loc_in, out evdt, SearchDepth.SearchDefault))
                AddSimpleProc(9, evdt, evdt.AddHours(1));
            if (GetEVDT("", "12045760", "", "", "", 1, loc_in, out evdt, SearchDepth.SearchDefault))
                AddSimpleProc(9, evdt, evdt.AddHours(1));
            if (GetEVDT("", "12060714", "", "", "", 1, loc_in, out evdt, SearchDepth.SearchDefault))
                AddSimpleProc(9, evdt, evdt.AddHours(1));
            if (GetEVDT("", "12045761", "", "", "", 1, loc_in, out evdt, SearchDepth.SearchDefault))
                AddSimpleProc(9, evdt, evdt.AddHours(1));
            if (GetEVDT("", "12061068", "", "", "", 1, loc_in, out evdt, SearchDepth.SearchDefault))
                AddSimpleProc(9, evdt, evdt.AddHours(1));

            if (GetEVDT("", "RNACT", "", "", "1:1 by RN Bedside Procedure > 1 Hour", 1, loc_in, out evdt, SearchDepth.SearchDefault))
                AddSimpleProc(9, evdt, evdt.AddHours(1));
        }

        private void CheckProc_10()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A10. 1:1 non-RN at bedside");
            Program.VerboseAudit("---------------");

            DateTime evdt;
            if (GetEVDT("", "NONRNACT", "", "", "1:1 by Non RN Bedside for Procedure > 1 Hour", 1, loc_in, out evdt, SearchDepth.SearchDefault))
                AddSimpleProc(10, evdt, evdt.AddHours(1));

            string res = "1:1 by non-RN at the bedside";
            ProcessProc(10, 1, res);
            ProcessProc(10, 2, res);

        }

        private void CheckProc_11()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("A11. 2:1 by RN at bedside");
            Program.VerboseAudit("---------------");

            DateTime evdt;
            if (GetEVDT("", "RNACT", "", "", "2:1 by RN Bedside Procedure > 1 Hour", 1, loc_in, out evdt, SearchDepth.SearchDefault))
                AddSimpleProc(11, evdt, evdt.AddHours(1));

            string res = "2:1 by RN at the bedside";
            ProcessProc(11, 1, res);
            ProcessProc(11, 2, res);

        }

        private void OutputClassAndDNC(bool do_class)
        {
            int i;
            if (numclassdnc == 0 && do_class)
                OutputClass(DateTime.MinValue, DateTime.MinValue);
            //else
            //{
            //    for (i = 1; i <= numclassdnc; i++)
            //    {
            //        if (aryclassdnc[i].retain)
            //            OutputDNC(aryclassdnc[i].startdt, aryclassdnc[i].enddt);
            //        else // do_class will never be false here
            //            OutputClass(aryclassdnc[i].startdt, aryclassdnc[i].enddt);
            //    }
            //}
        }

        //        private const string DATETIME_FORMAT = "yyyyMMddHHmm";              // ISO Date/Time w/o seconds
        private const string DATETIME_FORMAT = "yyyyMMddHHmm";              // ISO Date/Time w/o seconds

        private void OutputClass(DateTime dncstdt,DateTime dncendt)
        {
            string outstr, ind_list, desc, str_pull_dt, str_in_dt, str_out_dt = "";
            int i, tc_event_id;
            bool repeat_output = false;
            string repeat_time = "";
            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         212 242 304
            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1
            //1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901 3456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
            //1       |10 5            |                |                |        |10023121779         |ALEXANDER                       |GLORIA                          |                                |0508    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289530  |           |201403030400                                                                      |NNYNNNNYYNNYNNNYNNNYYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
            //1       |10 5            |                |                |        |10024544908         |ARMSTRONG                       |AARON                           |                                |0535    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289531  |           |201403030400                                                                      |NYNNYNNNNNNYNNNYNNNNYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
            //|0547    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289532  |           |201403030400                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN
            //|0547    |A   |201403030700|                |201403030700    |18  |C|    |3040|1440|18289532  |           |201403030700                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//            1RO | RO MB5BG / 6E |                |                |        | 2000180316769 | PARISIEN | GREYSON | LAWRENCE | RMB5514 | P | 20190319030000 |                               | 20 | C |    | 5399 | 480 | 62040560 |           | 20190319030000 | 20190319070000 | NNNYNNNNNNNNYNNNYNYNYNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN

            if (Program.g_is_test)
                tc_event_id = 9999;
            else
                tc_event_id = PFSDBUtility.NextGID();                         //get a unique id for this class
                                                                              //            str_pull_dt = _pat.pull_finish.ToString(DATETIME_FORMAT);
                                                                              //str_pull_dt = _pat.pull_finish.ToString(DATETIME_FORMAT);
                                                                              //if (_pat.unit_departure != null && _pat.unit_departure != DateTime.MinValue)
            if (dncstdt == DateTime.MinValue)
            {
                //                if (_pat.effective_out == Program.g_pull_finish && _pat.unit_departure == DateTime.MinValue)
                Program.VerboseAudit("Loc arrtime=" + loc_arrtime + " gpull_start_save=" + Program.g_pull_start_save);
                if (loc_arrtime >= Program.g_pull_start_save)
                {
                    //str_in_dt = Program.g_effdt.ToString(DATETIME_FORMAT);
                    str_in_dt = loc_arrtime.ToString(DATETIME_FORMAT);
                    str_out_dt = loc_out.ToString(DATETIME_FORMAT);

                    if (loc_arrtime <= Program.g_effdt)
                    {
                        repeat_output = true;//also make the desired class time
                        repeat_time = Program.g_effdt.ToString(DATETIME_FORMAT);
                    }
                }
                else
                {
                    str_in_dt = Program.g_effdt.ToString(DATETIME_FORMAT);
                    str_out_dt = "";// _pat.effective_out.ToString(DATETIME_FORMAT);
                    str_out_dt = loc_out.ToString(DATETIME_FORMAT);
                }
            }
            else //dnc
            {
                str_in_dt = dncstdt.ToString(DATETIME_FORMAT);
                str_out_dt = dncendt.ToString(DATETIME_FORMAT);
            }
            str_out_dt = "";//leave open-ended for night ASSIGNMENT MODULE 4/14/21

            //outstr = _pat.facilty_code.FixedWidth(8);       //(facility code)
            outstr = _pat.short_name.FixedWidth(8);                       //(facility code)
            outstr += "|" + _pat.unit_name.FixedWidth(16);
            outstr += "|" + "".FixedWidth(16);                               //(unit code)
            outstr += "|" + txarea.FixedWidth(16);                               //(area code)
            outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
            outstr += "|" + _pat.acct.FixedWidth(20);
            outstr += "|" + _pat.last_name.FixedWidth(32);
            outstr += "|" + _pat.first_name.FixedWidth(32);
            outstr += "|" + _pat.middle_name.FixedWidth(32);
            outstr += "|" + _pat.room.FixedWidth(8);
            outstr += "|" + _pat.bed.FixedWidth(4);
            outstr += "|" + str_in_dt;        //CLASS dt
            outstr += "|" + "".FixedWidth(14);                               //(login)
            outstr = outstr.FixedWidth(232);
            outstr += "|" + _pat.effective_out.ToString(DATETIME_FORMAT);//str_out_dt;        //TC Data End Point
            outstr = outstr.FixedWidth(249);
            outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
            outstr += "|" + "C".FixedWidth(1);                               //record type = class
            outstr += "|" + "".FixedWidth(4);                                //(stage)
            outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
            outstr += "|" + _pat.range.ToString().FixedWidth(4);             //TC pull range
            outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
            outstr += "|";
            outstr = outstr.FixedWidth(294);
            outstr += "|" + str_in_dt;        //IN
            outstr = outstr.FixedWidth(346);
            //if (_pat.unit_departure != null && _pat.unit_departure != DateTime.MinValue)
                //                outstr += "|" + _pat.unit_departure.ToString(DATETIME_FORMAT);        //OUT
                outstr += "|" + str_out_dt;        //OUT
            outstr = outstr.FixedWidth(377);
            outstr += "|";

            //if (use_default)
            //{ //make all is_checked = false and then mark defaults
            //    Program.VerboseAudit("Patient Will receive default indicators " + _pat.default_inds_str);
            //    for (i = 1; (i <= MAX_INDS); i++)
            //    {
            //        _inds[i].is_checked = false;
            //    }
            //    foreach (var ind in _pat.default_inds)
            //    {
            //        if (ind <= _inds.GetUpperBound(0))
            //        {
            //            _inds[ind].is_checked = true;
            //        }
            //    }
            //}

            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++) {
                if (_inds[i].is_checked) {
                    outstr += "Y";
                    ind_list += "" + i;
                } else {
                    outstr += "N";
                }
            } // next i
            ind_list = ind_list.Substring(1);                           //strip leading comma
                                                                        //                                                                                                   1                                                                                                   2                                                                                                   3
                                                                        //         1         2         3         4         5         6         7         8         9         0         1         2         3         4         5         6         7         8         9         0         1         2         3         4         5         6         7         8         9         0         1         2         3         4         5         6         7         8         9         0
                                                                        //1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
                                                                        //1       |DO6D            |                |                |        |2000192224892       |BEHNAM                          |KENDRA                          |LEE                             |RDO6311 |P   |20180717110000|                               |20  |C|    |5399|480 |56103278  |           |20180717110000                                     |                              |YNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
            string str5am;
            Program.outfile.WriteLine(outstr);      //1900yesterd output to transparent.txt
            if (repeat_output) // with repeat_time
            {
                string repeat_line = outstr.Substring(0, 203) + repeat_time + outstr.Substring(215, 80) + repeat_time + " ".Repeat(71) + outstr.Substring(378, 120);
                Program.outfile.WriteLine(repeat_line);
            }
            //if (Program.g_make5am)
            //{
            //    //if (str_out_dt.Substring(8, 4) == "0500") //create the 7am at 3am
            //    {
            //        string strdttm = Program.g_pull_finish.ToString(DATETIME_FORMAT);
            //        strdttm = strdttm.Substring(0, 8) + "0500";
            //        //str5am = outstr.Substring(0, 203) + Program.g_pull_finish.ToString(DATETIME_FORMAT) + outstr.Substring(217, 78) + Program.g_pull_finish.ToString(DATETIME_FORMAT) + " ".Repeat(69) + outstr.Substring(378, 120);
            //        str5am = outstr.Substring(0, 203) + strdttm + outstr.Substring(215, 80) + strdttm + " ".Repeat(71) + outstr.Substring(378, 120);
            //        Program.outfile2.WriteLine(str5am);
            //    }
            //}

            Program.Audit("");
            desc = "Classified: " + ind_list;
            if (Program.g_is_test) {
                Program.Audit(desc);
            } else {
                //Save the selected indicators plus both Program.Audits in the event log; link with tc_event_id
                PFSEventLog.AddTransparentMappingEventLogEntry(
                    desc, Program.gLogUnitID, Program.gLogEncounterID,
                    tc_event_id, Program.gLogMapperVersion,
                    Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
            }
        }

        //private void OutputClassAndDNCTimes()
        //{
        //    int i;
        //    DateTime dt1 = Program.g_effdt;
        //    numclassdnc = 0;
        //    DateTime lastend = Program.g_effdt;
        //    // retain has nothing to do with keeping the record. it only means "is DNC".

        //    for (i = 1; i <= numloa; i++)
        //    {
        //        //Why are these historicals stubbed off?? let it go through 10/09/20
        //        //if (aryloa[i].startdt < dt1) 
        //        //{
        //        //    Program.VerboseAudit("historical aryloa i:" + i + " aryloa[].startdt=" + aryloa[i].startdt + " aryloa[].enddt=" + aryloa[i].enddt + " dt1="+dt1);
        //        //}
        //        //else
        //        {
        //            if (numclassdnc == 0)
        //            {
        //                if (aryloa[i].startdt > dt1)
        //                {
        //                    numclassdnc++;
        //                    aryclassdnc[numclassdnc].startdt = dt1;
        //                    aryclassdnc[numclassdnc].enddt = aryloa[i].startdt;
        //                    aryclassdnc[numclassdnc].retain = false; //false=Class
        //                }
        //                numclassdnc++;
        //                aryclassdnc[numclassdnc].startdt = aryloa[i].startdt;
        //                aryclassdnc[numclassdnc].enddt = aryloa[i].enddt;
        //                aryclassdnc[numclassdnc].retain = true; //true=DNC
        //            }
        //            else
        //            {
        //                numclassdnc++;
        //                aryclassdnc[numclassdnc].startdt = aryloa[i - 1].enddt;
        //                aryclassdnc[numclassdnc].enddt = aryloa[i].startdt;
        //                aryclassdnc[numclassdnc].retain = false; //false=Class

        //                numclassdnc++;
        //                aryclassdnc[numclassdnc].startdt = aryloa[i].startdt;
        //                aryclassdnc[numclassdnc].enddt = aryloa[i].enddt;
        //                aryclassdnc[numclassdnc].retain = true; //true=DNC
        //            }
        //            lastend = aryloa[i].enddt;
        //        }
        //    } //for
        //    if (numclassdnc > 0 && lastend != dt1.AddHours(12)) 
        //        // then the loa ended before this draw period end
        //    {
        //        numclassdnc++;
        //        aryclassdnc[numclassdnc].startdt = lastend; // aryloa[i - 1].enddt;
        //        aryclassdnc[numclassdnc].enddt = dt1.AddHours(12);
        //        aryclassdnc[numclassdnc].retain = false; //false=Class
        //    }
        //    for (i=1;i<=numclassdnc;i++)
        //    {
        //        string classdnctype="";
        //        if (aryclassdnc[i].retain)
        //            classdnctype = "DNC";
        //        else
        //            classdnctype = "Class";
        //        Program.VerboseAudit(classdnctype + ": " + aryclassdnc[i].startdt + "-" + aryclassdnc[i].enddt);
        //    }
        //}

        //private void OutputDNC(DateTime dncstdt, DateTime dncendt)
        //{
        //    string outstr, ind_list, desc, str_pull_dt, str_in_dt, str_out_dt = "";
        //    int i, tc_event_id;

        //    if (Program.g_is_test)
        //        tc_event_id = 9999;
        //    else
        //        tc_event_id = PFSDBUtility.NextGID();                         //get a unique id for this class

        //    //str_in_dt = aryloa[i].startdt.ToString(DATETIME_FORMAT);
        //    //str_out_dt = aryloa[i].enddt.ToString(DATETIME_FORMAT);
        //    str_in_dt = dncstdt.ToString(DATETIME_FORMAT);
        //    str_out_dt = dncendt.ToString(DATETIME_FORMAT);

        //    //outstr = _pat.facilty_code.FixedWidth(8);                       //(facility code)
        //    outstr = "".FixedWidth(8);                       //(facility code)
        //    outstr += "|" + _pat.unit_name.FixedWidth(16);
        //    outstr += "|" + "".FixedWidth(16);                               //(unit code)
        //    outstr += "|" + "".FixedWidth(16);                               //(area code)
        //    outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
        //    outstr += "|" + _pat.acct.FixedWidth(20);
        //    outstr += "|" + _pat.last_name.FixedWidth(32);
        //    outstr += "|" + _pat.first_name.FixedWidth(32);
        //    outstr += "|" + _pat.middle_name.FixedWidth(32);
        //    outstr += "|" + _pat.room.FixedWidth(8);
        //    outstr += "|" + _pat.bed.FixedWidth(4);
        //    outstr += "|" + str_in_dt;        //CLASS dt
        //    outstr += "|" + "".FixedWidth(14);                               //(login)
        //    outstr = outstr.FixedWidth(232);
        //    outstr += "|" + str_out_dt;  // TC END POINT_pat.effective_out.ToString(DATETIME_FORMAT);//TC Data End Point
        //    outstr = outstr.FixedWidth(249);
        //    outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
        //    outstr += "|" + "D".FixedWidth(1);                               //record type = class
        //    outstr += "|" + "".FixedWidth(4);                                //(stage)
        //    outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
        //    outstr += "|" + "".FixedWidth(4);             //TC pull range
        //    outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
        //    outstr += "|";
        //    outstr = outstr.FixedWidth(294);
        //    outstr += "|" + str_in_dt;        //IN
        //    outstr = outstr.FixedWidth(346);
        //    outstr += "|" + str_out_dt;        //OUT
        //    outstr = outstr.FixedWidth(377);
        //    outstr += "|NNNNY";

        //    Program.outfile.WriteLine(outstr);      //1900yesterd output to transparent.txt
        //    Program.Audit("");
        //    desc = "Classified DoNotClassify: NNNNY";
        //        //Save the selected indicators plus both Program.Audits in the event log; link with tc_event_id
        //        PFSEventLog.AddTransparentMappingEventLogEntry(
        //            desc, Program.gLogUnitID, Program.gLogEncounterID,
        //            tc_event_id, Program.gLogMapperVersion,
        //            Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());

        //}


        private int DeterminePtypeOfIndicators()
        {
            int i;
            var pscore = 0.0;
            int pt_type = 6;
            var indlist = new List<int>();
            string indstr = "";

            Program.VerboseAudit("Determine patient type of triggered indicators");

            for (i = 1; (i <= MAX_INDS); i++)
            {
                if (_inds[i].is_checked)
                {
                    indlist.Add(i);
                    indstr += i.ToString() + ",";
                    pscore += _inds[i].weight;
                }
            }

            Program.VerboseAudit("indicators=" + indstr + " score=" + pscore.ToString());

            var db = PFSDBUtility.NewPfsDataContext();
            var query_ptype = from ptype in db.PATIENT_TYPEs
                              where (ptype.METHODOLOGY_ID == _pat.meth_id)
                              orderby ptype.PATIENT_TYPE1
                              select new
                              {
                                  ptype.PATIENT_TYPE1,
                                  ptype.POINTS_HIVAL
                              };

            foreach (var ptypes in query_ptype)
            {
                //                Program.DebugTrace("type,hival=" + ptypes.PATIENT_TYPE1.ToString() + "" + ptypes.POINTS_HIVAL.ToString(), "");
                if (pscore <= ptypes.POINTS_HIVAL)
                {
                    if (pt_type > ptypes.PATIENT_TYPE1)
                    {
                        pt_type = ptypes.PATIENT_TYPE1;
                    }
                }
            }
            Program.VerboseAudit("patient type=" + pt_type.ToString());

            return pt_type;

        }


        //private bool ExistDefaultInPast16hrs()
        //{
        //    DateTime cdt1 = DateTime.MinValue;
        //    DateTime cdt2 = DateTime.MinValue;
        //    int cnt_all = 0;
        //    int cnt_def = 0;
        //    //get max class date of last non-default class = 1
        //    //get max class date of last default = 2
        //    // if 1 >= 2 then false
        //    // else
        //    //   if 2 > 1 and date is <= 16 hrs ago then true
        //    //   else false
        //    var db = PFSDBUtility.NewPfsDataContext();
        //    //var query = from ce in db.CLASSIFICATION_EVENTs
        //    //            where (ce.ENCOUNTER_ID == _pat.encounter_id)
        //    //            && (ce.CLASSIFIED_BY_ID != -2)
        //    //            && (System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(ce.CLASSIFICATION_DATETIME, Program.g_pull_start) <= 960)
        //    //            select ce;
        //    //cnt_all = query.Count();
        //    //if (cnt_all > 0)
        //    //{
        //    //    cdt1 = PFSUtility.DBToDateTime(query.Max(x => x.CLASSIFICATION_DATETIME));
        //    //    Program.VerboseAudit("Last regular classification was at:"+cdt1.ToString());
        //    //}
        //    //else {
        //    //    Program.VerboseAudit("No regular classifications within the past 16 hours");
        //    //}

        //    var query = from ce in db.CLASSIFICATION_EVENTs
        //                where (ce.ENCOUNTER_ID == _pat.encounter_id)
        //                && (ce.CLASSIFIED_BY_ID == -2)
        //                && (System.Data.Linq.SqlClient.SqlMethods.DateDiffMinute(ce.CLASSIFICATION_DATETIME, Program.g_pull_finish) <= 960)
        //                select ce;
        //    cnt_def = query.Count();

        //    if (cnt_def > 0)
        //    {
        //        cdt2 = PFSDBUtility.DBToDateTime(query.Max(x => x.CLASSIFICATION_DATETIME));
        //        Program.VerboseAudit("Last default classification was at:" + cdt2.ToString());
        //    }
        //    else
        //    {
        //        Program.VerboseAudit("No default classifications within the past 16 hours");
        //    }
        //    return (cnt_def > 0);

        //}

        private void OutputProcs()
        {
            int i;
            string outstr, proc_list, desc;
            int tc_event_id;

            foreach (var proc in _procs)
            {
                if (Program.g_is_test)
                    tc_event_id = 9999;
                else
                    tc_event_id = PFSDBUtility.NextGID();                         //get a unique id for this proc

                outstr = _pat.short_name.FixedWidth(8);                       //(facility code)
                outstr += "|" + _pat.unit_name.FixedWidth(16);
                outstr += "|" + "".FixedWidth(16);                               //(unit code)
                outstr += "|" + txarea.FixedWidth(16);                               //(area code)
                outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
                outstr += "|" + _pat.acct.FixedWidth(20);
                outstr += "|" + _pat.last_name.FixedWidth(32);
                outstr += "|" + _pat.first_name.FixedWidth(32);
                outstr += "|" + _pat.middle_name.FixedWidth(32);
                outstr += "|" + _pat.room.FixedWidth(8);
                outstr += "|" + _pat.bed.FixedWidth(4);
                outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //204 proc dt
                outstr += "|" + "".FixedWidth(14);                               //(login)
                outstr = outstr.FixedWidth(249);
                outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
                outstr += "|" + "P".FixedWidth(1);                               //record type = class
                outstr += "|" + "".FixedWidth(4);                                //(stage)
                outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
                outstr += "|" + _pat.range.ToString().FixedWidth(4);             //TC pull range
                outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
                outstr += "|";
                outstr = outstr.FixedWidth(294);
                outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //296 procdt in
                outstr = outstr.FixedWidth(346);
                //if (_pat.unit_departure != null && _pat.unit_departure != DateTime.MinValue)
                //                outstr += "|" + _pat.unit_departure.ToString(DATETIME_FORMAT);        //OUT
                outstr += "|" + proc.finish.ToString(DATETIME_FORMAT);          //348 procdt out
                outstr = outstr.FixedWidth(377);
                outstr += "|";

                proc_list = "";
                for (i = 1; (i < MAX_PROCS); i++)
                {
                    if (proc.procedure_number == i)
                    {
                        outstr += "Y";
                        proc_list += "" + i;
                    }
                    else
                    {
                        outstr += "N";
                    }
                } // next i
                proc_list = proc_list.Substring(1);                             //strip leading comma

                Program.outfile.WriteLine(outstr);                              //output to transparent.txt

                desc = "Activities: " + proc_list;
                if (Program.g_is_test)
                {
                    Program.Audit(desc);
                }
                else
                {
                    //Save the selected procedures plus both Program.Audits in the event log; link with tc_event_id
                    //(this assumes the procedure Program.Audit is mixed in with the class Program.Audit)
                    PFSEventLog.AddTransparentMappingEventLogEntry(desc, Program.gLogUnitID, Program.gLogEncounterID,
                        tc_event_id, Program.gLogMapperVersion,
                        Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
                }
            } // next proc
        }

    }
}


