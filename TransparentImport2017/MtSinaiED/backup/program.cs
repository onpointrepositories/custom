﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;                // for SqlConnection
using System.IO;                            // for Directory, Path, Stream
using System.Windows.Forms;                 // for Application (also add reference)
using PfsShared;                            // add a reference to Shared2 project
//
// Transparent Mapping main program.
// NOTE: Each unit must set the "use transparent" flag.
//
// sample: -efftime=0700 -effdate=yesterday -pulltime=0700 -pulldate=today -range=1440
//
// This is designed to be run as a background job, so never stop for user input -- that means no error MsgBox.
// All communication is done through log files and the event log; it is OK to print to the console.
//
// In order to work with the AcuityPlus Patient Selection and Transparent import:
//
//   * The program name must be TransparentMapping.exe, placed in AcuityPlus\bin
//   * The audit file is called TransparentAudit.log, placed in AcuityPlus\log
//   * The output file is Transparent.txt, placed in AcuityPlus\load_me
//   * Events are saved in the database event log
//
namespace TransparentMapping
{
    public class PatientInfo
    {
        public int      encounter_id;
        public string   last_name;
        public string   first_name;
        public string   middle_name;
        public string   acct;
        public double   age;                    // age (in years) at admission
        public string   room;
        public string   bed;
        public DateTime unit_arrival;
        public DateTime unit_departure;
        public DateTime effective;              // patient specific (may be > g_effdt)
        public DateTime pull_start;             // patient specific (may be > g_pull_start)
        public DateTime pull_finish;            // patient specific (may be < g_pull_finish)
        public int      range;                  // patient specific (may be < g_range) - minutes
        public double   los_hours;              // hours during pull
        public int      TC_source_id;           // TCP port (chart source)
        // unit info
        public string   facilty_code;           // class import facility code
        public string   unit_name;
        public int      unit_id;
        public bool     is_ED;
        public bool     is_ICU;
        public int      meth_id;
        public string adt_unit_name;
        //public List<int> default_inds;          // unit admission profile indicators for recent arrivals
        //public string default_inds_str;         // "1,6,8"
    }

    public static class Program
    {

        const string    MAPPER_VERSION = "1.00";
        const string    TRANSP_FILENAME = "Transparent.txt";    // THE output file (for class import)
        const string    TRANSP_AUDIT_LOG = "TransparentAudit.log";  // Legacy debug/audit log
        const int       CHART_ITEM_LIFE = 30;                   // days in the chart_item table
        const int       DEFAULT_RANGE = 1440;                   // 24 hrs in minutes; override with -range

        public static bool g_abort;                         // stop!
        public static bool g_debug;                         // output to console?
        public static bool g_log;                           // output to log file? (legacy feature)
        public static bool g_no_output;                     // audit file only?
        public static bool g_is_test;                       // output to file w/dummy ids and no event log
        public static bool g_no_delete;                     // keep old chart items?
        public static bool g_stop_on_error;
        public static bool g_import_when_done;

        public static DateTime      g_effdt;                // effective datetime

        public static StreamWriter  outfile;                // Transparent.txt
        public static StreamWriter  logfile;                // TransparentLog.txt

        public static int           gLogUnitID;
        public static int           gLogEncounterID;
        public static string        gLogSourceText;
        public static string        gLogMapperVersion;

        public static StringBuilder gBriefAudit;            // the complete brief audit
        public static StringBuilder gVerboseAudit;          // the complete verbose audit

        public static DateTime      g_pull_start;           // global pull start  (not limited by patient values)
        public static DateTime      g_pull_finish;          // global pull finish
        public static int           g_range;                // global range

        private static string   _this_acct;                 // acct filter
        private static string   _this_unit_name;            // unit name filter
        private static int      _this_unit_id;              // unit id filter

        static string           _import_path;
        static string           _log_path;

        static void Main(string[] args)
        {
            try
            {
                InitGlobals();

                ParseCommandLine(args);
                SetMapperVersion();

                gLogSourceText = String.Join(" ", args);           // add command line to log
                LogInfo("Begin mapping and translation", PFSEventLog.EventLogCategory.EVENT_CATEGORY_STARTUP_SHUTDOWN);
                gLogSourceText = "";

                OpenOutputFiles();
                ProcessPatients();

//                DeleteOldChartItems();

                LogInfo("Mapping complete", PFSEventLog.EventLogCategory.EVENT_CATEGORY_STARTUP_SHUTDOWN);

                MaybeRunImport();
                CloseOutputFiles();
            }
            catch (Exception e)
            {
                Console.WriteLine("Unexpected Error: {0}", e.Message);
                Console.WriteLine("{0}", e.StackTrace);
                LogUnexpectedError(e.Message, e.StackTrace);
            }

            if (g_debug)
            {
                Console.WriteLine(Environment.NewLine);
                Console.Write("Press any key...");
                Console.ReadKey();
            }
        }

        static void InitGlobals()
        {
            gLogSourceText = "";
            gBriefAudit = new StringBuilder();
            gVerboseAudit = new StringBuilder();
        }

        static void ParseCommandLine(string[] args)
        {
            string nowdate, nowtime;
            string value;
            string effdate, efftime;
            string pulldate, pulltime;

            var nowdt = DateTime.Now;
            nowdate = nowdt.ToString("yyyyMMdd");
            nowtime = nowdt.ToString("HHmm");
            effdate = "";
            efftime = "";
            pulldate = "";
            pulltime = "";

            _import_path = PFSUtility.DefaultImportPath();          // ...\load_me
            _log_path = PFSUtility.DefaultLogPath();                // ...\log

            foreach (var arg in args)
            {
                var arr = arg.Split('=');
                value = (arr.GetUpperBound(0) > 0) ? arr[1] : "";

                switch (arr[0])
                {
                    case "-acct":                               // process this patient only
                        _this_acct = value;
                        break;
                        
                    case "-debug":                              // output to console and pause at end
                        g_debug = true;
                        break;

                    case "-effdate":                            // effective date: yyyymmdd
                        if (value == "yesterday")
                            effdate = DateTime.Now.AddDays(-1).ToString("yyyyMMdd");
                        else
                            effdate = value.Left(8);
                        break;
                    
                    case "-efftime":                            // effective time: HHMM or HH
                        efftime = value.Left(4);
                        efftime = efftime.PadRight(4,'0');
                        break;
                    
                    case "-import":                             // run transparent import when done
                        g_import_when_done = true;
                        break;

                    case "-log":                                // create TransparentAudit.log
                        g_log = true;                           // (verbose audit)
                        break;

                    case "-path":                               // override default import path
                        _import_path = value;
                        break;

                    case "-logpath":                               // override default import path
                        _log_path = value;
                        break;
                    case "-n":                                  // no output to transparent.txt 
                        g_no_output = true;                     // (audit only)
                        break;

                    case "-nodelete":                           // do not delete old chart items
                        g_no_delete = true;
                        break;

                    case "-pulldate":                           // pull date: yyyymmdd
                        if (value == "today")
                            pulldate = DateTime.Now.ToString("yyyyMMdd");
                        else
                            pulldate = value.Left(8);
                        break;
                    
                    case "-pulltime":                           // pull time: HHMM or HH
                        pulltime = value.Left(4);
                        pulltime = pulltime.PadRight(4,'0');
                        break;

                    case "-range":                              // range in minutes; default = 1440 = 24hr
                        g_range = value.ToInteger();
                        break;
                        
                    case "-stoponerror":
                        g_stop_on_error = true;
                        break;

                    case "-test":
                        g_is_test = true;
                        break;

                    case "-unit":                               // process this unit only
                        _this_unit_name = value;
                        break;
                    case "-unit_id":                            // process this unit only
                        _this_unit_id = value.ToInteger();
                        break;
                        
                    default:
                        Console.WriteLine("unexpected argument: {0}", arg);
                        break;
                }
            }

            if (String.IsNullOrEmpty(effdate))
                effdate = nowdate;
            if (String.IsNullOrEmpty(efftime))
                efftime = nowtime;

            // Note: pulldate defaults to effdate, not nowdate
            if (String.IsNullOrEmpty(pulldate))
                pulldate = effdate;
            if (String.IsNullOrEmpty(pulltime))
                pulltime = efftime;

            if (g_range == 0)
                g_range = DEFAULT_RANGE;

            DebugTrace("Import path=", _import_path);
            DebugTrace("Log path=", _log_path);
            DebugTrace("effdate={0}", effdate);
            DebugTrace("efftime={0}", efftime);
            DebugTrace("pulldate={0}", pulldate);
            DebugTrace("pulltime={0}", pulltime);
            DebugTrace("range={0}", g_range);

            // class IN time
            g_effdt = PFSUtility.ISOToDateTime(effdate + efftime);
            // range for chart item queries
            g_pull_finish = PFSUtility.ISOToDateTime(pulldate + pulltime);
            g_pull_start = g_pull_finish.AddMinutes(-g_range);

        }

        static void OpenOutputFiles()
        {
            // Append to existing file
            outfile = new StreamWriter(Path.Combine(_import_path, TRANSP_FILENAME), true);

            if (g_log) {
                // re-write new file
                logfile = new StreamWriter(Path.Combine(_log_path, TRANSP_AUDIT_LOG));
            }
        }

        static void CloseOutputFiles()
        {
            outfile.Close();
            
            if (logfile != null) {
                logfile.Close();
                logfile = null;
            }
        }

        static void SetMapperVersion()
        {
            gLogMapperVersion = MAPPER_VERSION;
        }

        static void ProcessPatients()
        {
            string sql, audit_header;
            int count, prev_unit_id, prev_enc_id = 0;
    //parent_id = 2817
    //'Prod env:
    //vunit_id = 43729784
    //iunit_id = 43730689
            // Make a list of all patients with chart items during the pull period.
            //
            // Include only units that have the "use transparent" flag set.
            // Limit to one patient if -acct is given.  Limit to one unit with -unit.
            // Left join encounter_location - the patient may be discharged at pull time.
            // Look for the latest patient arrival to the unit before the pull time; ignore transfers within the unit.
            // Look for a patient departure (if any) before the pull time; ignore transfers within the unit.
            sql =
"select p.last_name,p.first_name,e.acct_number,e.age_at_admission,el.encounter_id,"
+ "u.unit_id,u.name,f.CLASSIFICATION_FACILITY_CODE,el.ADT_UNIT_NAME,el.DATETIME_IN,el.DATETIME_OUT as UNIT_DEPARTURE,el.room,el.bed,"
+ "ce.CLASSIFICATION_EVENT_ID,ce.unit_id as class_unit_id\n"
+ "from encounter_location as el\n"
+ "inner join unit as u on (el.UNIT_ID=u.UNIT_ID)\n"
+ "inner join facility as f on (f.FACILITY_ID=u.FACILITY_ID)\n"
+ "inner join ENCOUNTER as e on (el.ENCOUNTER_ID=e.ENCOUNTER_ID)\n"
+ "inner join PERSON as p on (e.person_id=p.PERSON_ID)\n"
+ "left join CLASSIFICATION_EVENT as ce on (el.encounter_id=ce.encounter_id) and (ce.EFFECTIVE_DATETIME_IN between el.EFFECTIVE_DATETIME_IN and el.EFFECTIVE_DATETIME_OUT or ce.EFFECTIVE_DATETIME_OUT between el.EFFECTIVE_DATETIME_IN and el.EFFECTIVE_DATETIME_OUT)\n"
+ "where u.unit_id=2817 and u.is_ed='y' and u.use_transparent_classification='y' and\n"
//+ "(el.datetime_out is null or el.datetime_in>=dateadd(d,-3," + PFSUtility.SQLDateTime(g_pull_start) + ")) and (ce.METHODOLOGY_ID is null)\n";
+ "(el.datetime_out is not null and el.datetime_in>=dateadd(d,-3," + PFSUtility.SQLDateTime(g_pull_start) + ")) and (ce.METHODOLOGY_ID is null)\n";

            //sql = 
            //    " SELECT LIST.UNIT_ID, LIST.ENCOUNTER_ID, LIST.TC_SOURCE_ID, PARAM.METHODOLOGY_ID,\n" +
            //    "     UNIT.NAME AS UNIT_NAME, EL.ROOM, EL.BED, EL.DATETIME_IN AS BED_ARRIVAL,\n" +
            //    "     F.CLASSIFICATION_FACILITY_CODE,\n" +
            //    "     ARRIVE.DATETIME_IN AS UNIT_ARRIVAL, DEPART.DATETIME_OUT AS UNIT_DEPARTURE,\n" +
            //    "     E.ACCT_NUMBER, P.LAST_NAME, P.FIRST_NAME, P.MIDDLE_NAME,\n" +
            //    "     E.AGE_AT_ADMISSION,\n" +
            //    "     UNIT.IS_ED, UNIT.IS_ICU\n" +
            //    " FROM (\n" +
            //    "     SELECT DISTINCT CI.UNIT_ID, CI.ENCOUNTER_ID, CI.TC_SOURCE_ID\n" +
            //    "     FROM CHART_ITEM AS CI\n" +
            //    "     INNER JOIN UNIT ON (UNIT.UNIT_ID = CI.UNIT_ID)\n" +
            //    "     WHERE UNIT.USE_TRANSPARENT_CLASSIFICATION='Y'\n" +
            //    "     AND EVENT_DATETIME BETWEEN " + PFSUtility.SQLDateTime(g_pull_start) + " AND " + PFSUtility.SQLDateTime(g_pull_finish) +
            //    " ) AS LIST" +
            //    " INNER JOIN UNIT ON (UNIT.UNIT_ID = LIST.UNIT_ID)\n" +
            //    " INNER JOIN UNIT_PARAM AS PARAM ON (PARAM.UNIT_ID = UNIT.UNIT_ID) AND (" + PFSUtility.SQLDateTime(g_pull_finish) + " BETWEEN PARAM.EFFECTIVE_REPORT_DATE AND EXPIRATION_REPORT_DATE)\n" +
            //    " INNER JOIN FACILITY AS F ON (F.FACILITY_ID = UNIT.FACILITY_ID)\n" +
            //    " INNER JOIN ENCOUNTER AS E ON (E.ENCOUNTER_ID = LIST.ENCOUNTER_ID)\n" +
            //    " INNER JOIN PERSON AS P ON (P.PERSON_ID = E.PERSON_ID)\n" +
            //    " LEFT  JOIN ENCOUNTER_LOCATION AS EL ON (EL.ENCOUNTER_ID = E.ENCOUNTER_ID) AND (" + PFSUtility.SQLDateTime(g_pull_finish) + " BETWEEN EL.EFFECTIVE_DATETIME_IN AND EL.EFFECTIVE_DATETIME_OUT)\n" +
            //    " INNER JOIN (\n" +
            //    "    SELECT WORKING_UNIT_ID AS UNIT_ID, ENCOUNTER_ID, MAX(DATETIME_IN) AS DATETIME_IN\n" +
            //    "    FROM ENCOUNTER_LOCATION AS EL\n" +
            //    "    WHERE WORKING_UNIT_ID IN (SELECT UNIT_ID FROM UNIT WHERE USE_TRANSPARENT_CLASSIFICATION='Y')\n" +
            //    "    AND DATETIME_IN < " + PFSUtility.SQLDateTime(g_pull_finish) +
            //    "    AND IS_TRANSFER_IN='Y'\n" +
            //    "    GROUP BY WORKING_UNIT_ID, ENCOUNTER_ID\n" +
            //    " ) AS ARRIVE ON (ARRIVE.UNIT_ID = UNIT.UNIT_ID) AND (ARRIVE.ENCOUNTER_ID = E.ENCOUNTER_ID)\n" +
            //    " LEFT JOIN (\n" +
            //    "    SELECT WORKING_UNIT_ID AS UNIT_ID, ENCOUNTER_ID, MAX(DATETIME_OUT) AS DATETIME_OUT\n" +
            //    "    FROM ENCOUNTER_LOCATION AS EL\n" +
            //    "    WHERE WORKING_UNIT_ID IN (SELECT UNIT_ID FROM UNIT WHERE USE_TRANSPARENT_CLASSIFICATION='Y')\n" +
            //    "    AND DATETIME_OUT < " + PFSUtility.SQLDateTime(g_pull_finish) +
            //    "    AND ((NEXT_WORKING_UNIT_ID<>WORKING_UNIT_ID) OR (NEXT_WORKING_UNIT_ID IS NULL))\n" +
            //    "    GROUP BY WORKING_UNIT_ID, ENCOUNTER_ID\n" +
            //    " ) AS DEPART ON (DEPART.UNIT_ID = UNIT.UNIT_ID) AND (DEPART.ENCOUNTER_ID = E.ENCOUNTER_ID)\n" +
            //    " WHERE (1=1)\n";
            
            if (!String.IsNullOrEmpty(_this_acct)) {
                sql += " AND E.ACCT_NUMBER=" + PFSUtility.SQLString(_this_acct) + "\n";
            }
            if (!String.IsNullOrEmpty(_this_unit_name)) {
                sql += " AND U.NAME=" + PFSUtility.SQLString(_this_unit_name) + "\n";
            }
            if (_this_unit_id > 0) {
                sql += " AND UNIT.UNIT_ID=" + _this_unit_id + "\n";
            }
            sql += " ORDER BY E.ACCT_NUMBER, EL.DATETIME_IN, U.NAME, P.LAST_NAME, P.FIRST_NAME\n";

            //DebugTrace(sql);
            var db = PFSUtility.NewSqlConnection();
            var cmd = new SqlCommand(sql, db);
            SqlDataReader dr = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);

            //
            // Process all patients
            //
            count = 0;
            prev_unit_id = 0;
            var default_inds = new List<int>();
            var default_inds_str = "";

            while (dr.Read())
            {
                count++;
                Console.Write("\rProcessing patient {0}", count);

                // Package the patient info
                // NOTE: ''dr["FIRST_NAME"] as string'' looks great but it will save nulls.
                //       use ''PFSUtility.DBToString(dr["FIRST_NAME"])'' to convert to empty strings.
                var pat = new PatientInfo();
                pat.adt_unit_name = PFSUtility.DBToString(dr["ADT_UNIT_NAME"]);
                pat.unit_id = PFSUtility.DBToInt(dr["UNIT_ID"]);
                pat.encounter_id = PFSUtility.DBToInt(dr["ENCOUNTER_ID"]);
                pat.meth_id = 24; // PFSUtility.DBToInt(dr["METHODOLOGY_ID"]);
                pat.acct = PFSUtility.DBToString(dr["ACCT_NUMBER"]);
                pat.last_name = PFSUtility.DBToString(dr["LAST_NAME"]);
                pat.first_name = PFSUtility.DBToString(dr["FIRST_NAME"]);
                pat.middle_name = "";// PFSUtility.DBToString(dr["MIDDLE_NAME"]);
                pat.facilty_code = PFSUtility.DBToString(dr["CLASSIFICATION_FACILITY_CODE"]);
                pat.unit_name = PFSUtility.DBToString(dr["NAME"]);
                pat.room = PFSUtility.DBToString(dr["ROOM"]);
                pat.bed = PFSUtility.DBToString(dr["BED"]);
                pat.age = PFSUtility.DBToDouble(dr["AGE_AT_ADMISSION"]);                
                pat.unit_arrival = PFSUtility.DBToDateTime(dr["DATETIME_IN"]);
                pat.unit_departure = PFSUtility.DBToDateTime(dr["UNIT_DEPARTURE"]);
                pat.effective = PFSUtility.MaxDateTime(g_effdt, pat.unit_arrival);
                pat.pull_start = PFSUtility.MaxDateTime(g_pull_start, pat.unit_arrival);
                pat.pull_finish = PFSUtility.MinDateTime(g_pull_finish, pat.unit_departure);
                if (pat.pull_finish < pat.pull_start) {
                    pat.pull_finish = g_pull_finish;
                }
                pat.range = (int)PFSUtility.DateDiffInMinutes(pat.unit_arrival, pat.unit_departure);
                pat.los_hours = pat.range / 60.0;
                //pat.TC_source_id = PFSUtility.DBToInt(dr["TC_SOURCE_ID"]);        //TCP port
                //pat.is_ED = PFSUtility.DBToBool(dr["IS_ED"]);
                //pat.is_ICU = PFSUtility.DBToBool(dr["IS_ICU"]);

                // Get the list of default indicators on admission (if any)
                //if (pat.unit_id != prev_unit_id) {
                //    default_inds = GetUnitDefaultIndicators(pat.unit_id, g_pull_start, out default_inds_str);
                //    prev_unit_id = pat.unit_id;
                //}
                //pat.default_inds = default_inds;
                //pat.default_inds_str = default_inds_str;
                
                // Get ready for event log entries for this patient
                gLogUnitID = pat.unit_id;
                gLogEncounterID = pat.encounter_id;
                gLogSourceText = "";

                // Reset both audit strings and make headers for both
                gBriefAudit = new StringBuilder();
                gVerboseAudit = new StringBuilder();
                audit_header =
                    new String('=', 80) + Environment.NewLine +
                    "TRANSPARENT MAPPING AUDIT FILE                    Run Time=" + DateTime.Now + Environment.NewLine +
                    "Version:   " + gLogMapperVersion + Environment.NewLine +
                    "Pull:      " + g_pull_finish + Environment.NewLine +
                    "Effective: " + g_effdt + Environment.NewLine +
                    "Range:     " + g_range + " (" + Math.Round(g_range / 60.0, 1) + " hrs) starting " + g_pull_start;

                // This will add to both brief and verbose audits
                Audit(audit_header);
                Audit(new String('=', 80));
                Audit("Patient " + pat.last_name + ", " + pat.first_name + " " + pat.middle_name +
                    " in " + pat.unit_name + " " + pat.room + " " + pat.bed +
                    " acct=" + pat.acct + " age=" + Math.Round(pat.age, 2));
                Audit("Here from " + pat.pull_start + " to " + pat.pull_finish +
                    "  (LOS=" + Math.Round(pat.los_hours, 2) + ")");

                if (pat.encounter_id != prev_enc_id)
                {
                    prev_enc_id = pat.encounter_id;
                    // Process this patient
                    // Catch any unexpected errors and continue with next patient.
                    try
                    {
                        // Run the appropriate mapping for this unit and date
                        //
                        switch (pat.meth_id)
                        {
                            case 24:
                                var ev = new EDVisit();
                                ev.ProcessPatient(pat);
                                break;
                            case 25:
                                var ei = new EDInpatient();
                                ei.ProcessPatient(pat);
                                break;
                            default:
                                LogWarning("Methodology " + pat.meth_id + " in unit " + pat.unit_name + " is not supported");
                                break;
                        }

                        gLogUnitID = 0;
                        gLogEncounterID = 0;
                    }
                    catch (Exception e)
                    {
                        LogUnexpectedError(e.Message, e.StackTrace);
                        // Stop or keep going?
                        if (Program.g_stop_on_error) g_abort = true;
                    }
                }
                if (g_abort) break;
            }

            Console.WriteLine();
            dr.Close();
            
            if (count < 1) {
                Audit("");
                if(!String.IsNullOrEmpty(_this_acct)) {
                    LogWarning("The selected patient has no chart items in the given time range");
                } else {
                    LogWarning("No chart items found to process - have the unit(s) been enabled for transparent classification?");
                }
            }   
        }

        static List<int> GetUnitDefaultIndicators(int unit_id, DateTime pull_dt, out string default_inds_str) 
        {
            var result = new List<int>();                   // make an empty list
            default_inds_str = "<none>";
            var db = PFSUtility.NewPfsDataContext();        // look for the unit default admission profile
            var query = from param in db.UNIT_PARAMs
                        from profile in param.PATIENT_PROFILEs
                        where (param.UNIT_ID == unit_id)
                        && (pull_dt >= param.EFFECTIVE_DATETIME) && (pull_dt < param.EXPIRATION_DATETIME)
                        && (profile.PROFILE_NUMBER == param.DEFAULT_ADMISSION_PROFILE)
                        select new {
                            profile.INDICATORS              // comma-separated indicator list
                        };
            foreach (var inds in query) {
                default_inds_str = inds.INDICATORS;
                string s = inds.INDICATORS;
                if (!String.IsNullOrEmpty(s)) {
                    var arr = s.Split(',');
                    foreach (var t in arr) {
                        if (t.IsNumeric()) {                // add an indicator number to the list
                            result.Add(t.ToInteger());
                        }
                    }
                }
            }
            return result;
        }
        
        static void DeleteOldChartItems()
        {
            string sql;
            DateTime dt;

            if (g_no_output || g_no_delete) return;

            DebugTrace("About to delete old char items...");
            DebugPause();                                       // give a chance to ^C in debug mode

            LogInfo("Delete old chart items...");
            dt = g_effdt.AddDays(-CHART_ITEM_LIFE);
            sql = "DELETE FROM CHART_ITEM WHERE EVENT_DATETIME < " + PFSUtility.SQLDateTime(dt);
            PFSUtility.ExecuteSQL(sql);
            LogInfo("Done");
        }

        static void MaybeRunImport()
        {
            if (! g_import_when_done) return;
            
            string path = Path.GetDirectoryName(Application.ExecutablePath);
            if (path.Right(3) != "bin") {                       // running in visual studio?
                path = @"C:\qmdev\AcuityPlus\main\bin";
            }
            if (Directory.Exists(path))
            {
                LogInfo("Running transparent import...");
                // Import will add its own log entries; no need to add any here
                int rc = PFSUtility.ExecuteAndWait(Path.Combine(path, PFSGlobal.TRANSPARENT_IMPORT_EXE_NAME));
                LogInfo("Done; result=" + rc);
            }
            else
            {
                LogWarning("Can't find " + path);
            }
        }



        //=====================================================================
        // Audits and log files
        //=====================================================================
        // Print to console if debug is set
        public static void DebugTrace(string format, params object[] values)
        {
            if (g_debug)
            {
                Console.Write(DateTime.Now.ToString() + " ");
                Console.WriteLine(format, values);
            }
        }

        public static void DebugPause()
        {
            if (g_debug)
            {
                Console.Write("Press any key...");
                Console.ReadKey();
                Console.WriteLine("");
            }
        }

        // Save in both audit files
        static public void Audit(string s)
        {
            DebugTrace(s);
            if (logfile != null) logfile.WriteLine(s);
            gBriefAudit.AppendLine(s);                  // Add to both audit reports
            gVerboseAudit.AppendLine(s);
        }

        // Save in verbose audit only
        static public void VerboseAudit(string s)
        {
            DebugTrace(s);
            if (logfile != null) logfile.WriteLine(s);
            gVerboseAudit.AppendLine(s);                // Add to verbose audit only
        }

        public static void AddLogEntry(PFSEventLog.EventLogType type, string msg, PFSEventLog.EventLogCategory category)
        {
            Audit(msg);

            PFSEventLog.AddEventLogEntry(
                PFSEventLog.EventLogSource.EVENT_SOURCE_TRANSPARENT_MAPPING,
                type, category, msg, gLogSourceText, 0, gLogUnitID, gLogEncounterID);
        }

        public static void LogInfo(string msg, PFSEventLog.EventLogCategory category)
        {
            AddLogEntry(PFSEventLog.EventLogType.EVENT_TYPE_INFO, msg, category);
        }
        public static void LogInfo(string msg)
        {
            LogInfo(msg, PFSEventLog.EventLogCategory.EVENT_CATEGORY_PROCESSED);
        }

        public static void LogWarning(string msg, PFSEventLog.EventLogCategory category)
        {
            AddLogEntry(PFSEventLog.EventLogType.EVENT_TYPE_WARNING, msg, category);
        }
        public static void LogWarning(string msg)
        {
            LogWarning(msg, PFSEventLog.EventLogCategory.EVENT_CATEGORY_VALIDATION);
        }

        public static void LogError(string msg, PFSEventLog.EventLogCategory category)
        {
            AddLogEntry(PFSEventLog.EventLogType.EVENT_TYPE_ERROR, msg, category);
        }
        public static void LogError(string msg)
        {
            LogError(msg, PFSEventLog.EventLogCategory.EVENT_CATEGORY_VALIDATION);
        }
        public static void LogUnexpectedError(string msg, string stack_trace)
        {
            // Add the message and stack trace to event log; message only goes to screen
            gLogSourceText = stack_trace;
            AddLogEntry(PFSEventLog.EventLogType.EVENT_TYPE_ERROR, msg, PFSEventLog.EventLogCategory.EVENT_CATEGORY_UNEXPECTED);
            gLogSourceText = "";
            
            // Add the stack trace to screen and log files
            Audit(msg);
            Audit(stack_trace);
        }

    }
}
