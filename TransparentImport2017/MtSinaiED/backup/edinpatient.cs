﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;                     // for LINQ; add a reference
using System.Data.SqlClient;                // for SqlConnection
using System.IO;                            // for Directory, Path, Stream
using PfsShared;                            // add a reference to Shared2 project

// ============================================================================
// ED Inpatient transparent mapping -- GOES HERE --
// Mayo Roch
// ============================================================================
// This processes one patient.  Only one database query is used.  :)
//
// All search functions use exact match for category, description and field name.
// Codes and results can be a single word or a list of words.
// Codes default to exact match; prefix with CODE_CODE_LIKE_PREFIX if desired (single word only)
// Results default to like matching; prefix with RESULT_EXACT_MATCH_PREFIX if desired (single word only)
//
// All searches are case insensitive.
//
//
namespace TransparentMapping
{
    class EDInpatient
    {
        private const int MAX_INDS = 50;
        private const int MAX_PROCS = 20;

        private const string CODE_LIKE_PREFIX = "%!";       // use to make codes %like%
        private const string EXACT_MATCH_PREFIX = "&!";     // use to make desc/result exact match
        private const string CHAR_COMMA = "||";             // use to insert literal comma in word list
        private const string NOT_PREFIX = "!!";             // use to insert literal comma in word list
        private const string EXACT_SQL_PREFIX = "!SQL!";     // use to give sql constraint as given

        private struct indicator_data {
            public bool     is_checked;     
            public int      radio_group;       
        }

        private struct proc_data {                      
            public int      procedure_number;
            public DateTime start;   
            public DateTime finish;
        }

        // These are database CHART_ITEMs for this patient
        private CHART_ITEM[] _chart_items_since_admission;
        private CHART_ITEM[] _chart_items_since_unit_arrival;
        private CHART_ITEM[] _chart_items_during_pull_period;
        private CHART_ITEM[] _chart_items_pull_period_plus;
        // These are database PROCEDURE_EVENTs for this patient
        private PROCEDURE_EVENT[] _procedure_events;

        private indicator_data[] _inds;
        private List<proc_data> _procs;
        private List<proc_data> _outcomes;

        private const int DEFAULT_BUCKET_SIZE = 20;       //min

        private PatientInfo _pat;
        private bool adl23;
        private bool adl4;
        private bool tubefeed;
        private bool coma;
        private int  _bucket_size = DEFAULT_BUCKET_SIZE;
        private bool is_default;
        private bool isEDonly = false;


        private enum SearchDepth {
            SearchDefault,
            SearchPullRange,            //search within the current pull     -- default
            SearchSinceArrival,         //search since arrival to the unit
            SearchSinceAdmission,        //search everything since admission to the hospital
            SearchPullPlus
        }

        private enum CountMode {
            CountAll,
            CountFirst                  //stop after one is found
        }

        private enum GetValueMode {
            GetTotal,
            GetMax,
            GetLast
        }

        enum Frequencies {
            QNONE,
            Q4H,
            Q2H,
            Q1H,
            Q15M
        }

        private struct fmapRow {
            public double   los_high;                       //the LOS being testing
            public int[]    freq;                           //the count required for each Q value
        }
        private List<fmapRow> _freq_map;                    //1,2,4,8,12,24 hours

     
        //
        // This is the main entry point
        //
        public void ProcessPatient(PatientInfo pat)
        {
            _pat = pat;

            InitIndicators(); // sets is_default
            InitProcs();
            isEDonly = OnlyHasED();
            if (! is_default)
                {
                LoadFreqTable();
                LoadPatientChart();
                //123 4 5 67 89 10 1112 13 14 151617
                Check_123();
                Check_4();
                Check_5();
                Check_67();
                Check_89();
                Check_10();
                Check_1112();
                Check_13();
                Check_14();
                Check_151617();
            }

            HighestIndicatorInEachGroupWins();

            //if (!is_default)
            //{
            //    CheckProcs();
            //    CheckOutcomes();
            //}

            if (Program.g_no_output) return;
            OutputClass();
            //OutputProcs();
            //OutputOutcomes();
        }

       
        private void InitIndicators()
        {
            // Make a new (empty) indicator array
            _inds = new indicator_data[MAX_INDS + 1];           // This 1 based so add one
            is_default = false;
            //if (_pat.los_hours <= 4.0) {
            //    is_default = true;
            //    Program.VerboseAudit("Patient was here 4 hrs or less. Will receive default indicators " + _pat.default_inds_str);
            //    foreach (var ind in _pat.default_inds) {
            //        if (ind <= _inds.GetUpperBound(0)) {
            //            _inds[ind].is_checked = true;
            //        }
            //    }
            //}

            adl23 = false;
            adl4 = false;
            tubefeed = false;
            coma = false;

            // get indicator radio groups from the database
            // ** (This database access can be replaced once we have a C# methodolgy cache)
            var db = PFSUtility.NewPfsDataContext();
            var query = from idef in db.INDICATOR_DEFINITIONs
                        where (idef.METHODOLOGY_ID == _pat.meth_id)
                        select idef;
            foreach (var idef in query) {
                if (idef.INDICATOR_NUMBER <= _inds.GetUpperBound(0)) {
                    // (convert nulls to zero)
                    _inds[idef.INDICATOR_NUMBER].radio_group = PFSUtility.DBToInt(idef.RADIO_GROUP);
                }
            }
        }

        private void InitProcs()
        {
            _procs = new List<proc_data>();
            _outcomes = new List<proc_data>();
        }

        private fmapRow LoadFreqTableRow(double los_high, string values)
        {
            fmapRow fmrow;
            
            fmrow.los_high = los_high;
            fmrow.freq = new int[5];

            var arr = values.Split(',');
            for (int i = 0; i <= fmrow.freq.GetUpperBound(0) ; i++ )
            {
                fmrow.freq[i] = arr[i].ToInteger();
            }
            return fmrow;
        }

        // These are the number of times you need to do something during a certain time period (LOS)
        // in order for it to be considered one of the frequencies listed.
        private void LoadFreqTable()
        {
            _freq_map = new List<fmapRow>();
            //                              LOS,  None Q4h Q2h Q1h Q15m
            _freq_map.Add(LoadFreqTableRow(1, "    0,  0,  0,  1,  2"));
            _freq_map.Add(LoadFreqTableRow(2, "    0,  0,  1,  1,  4"));
            _freq_map.Add(LoadFreqTableRow(4, "    0,  1,  2,  2,  8"));
            _freq_map.Add(LoadFreqTableRow(6, "    0,  1,  2,  3, 12"));
            _freq_map.Add(LoadFreqTableRow(8, "    0,  1,  2,  4, 16"));
            _freq_map.Add(LoadFreqTableRow(12, "   0,  2,  4,  6, 24"));
            _freq_map.Add(LoadFreqTableRow(16, "   0,  3,  5,  8, 32"));
            _freq_map.Add(LoadFreqTableRow(24, "   0,  4,  8, 12, 48"));
            _freq_map.Add(LoadFreqTableRow(36, "   0,  4,  8, 18, 72"));
            _freq_map.Add(LoadFreqTableRow(48, "   0,  4,  8, 24, 96"));
            //New freq table 2/5/14
//q4	q2	q1	q30     q30
//            Non-ICU	ICU & SD
// 4	8	15	29	    36
// 3	5	9	17	    24
// 2	4	7	13	    19
// 2	3	5	10	    13

            //to read this table: if you have at least 6 asessmenets in a 12-hour period, you get Q1h
            //(LOS=12, column Q1h has a count of 6)
        }

        private Frequencies FreqForCount(double los_hours, int count) 
        {
            foreach(var fmrow in _freq_map) {
                if (los_hours <= fmrow.los_high) {
                    // Option: pro-rate the count upward if the LOS is less than the row we are interested in.
                    //         This will bump the count to what it might have been at the full LOS.
                    // Note: truncate the result; rounding inflates the value too much.
                    int prorated_count = (int)((fmrow.los_high / los_hours) * count);

                    // foreach goes low to high; go from high to low instead
                    for (int j = (int)Frequencies.Q15M; (j >= (int)Frequencies.QNONE); j--) { //search right to left
                        if (prorated_count >= fmrow.freq[j]) {
                            return (Frequencies)j;
                        }
                    } // next j
                }
            }
            
            return Frequencies.QNONE;
        }

        //
        // Load patient chart from the database (one DB access)
        // Load everything since admission
        //
        private void LoadPatientChart()
        {
            // Get the entire patient chart (all units and dates for this patient)
            var db = PFSUtility.NewPfsDataContext();
            var query = from item in db.CHART_ITEMs
                        where (item.ENCOUNTER_ID == _pat.encounter_id)
                        select item;
            // Save the result
            _chart_items_since_admission = query.ToArray();

            // Convert all results to lower case for case insensitive comparisons
            foreach (var item in _chart_items_since_admission) {
                if (item.CATEGORY != null) item.CATEGORY = item.CATEGORY.ToLower();
                if (item.CODE != null) item.CODE = item.CODE.ToLower();
                if (item.DESCRIPTION != null) item.DESCRIPTION = item.DESCRIPTION.ToLower();
                if (item.FIELD_NAME != null) item.FIELD_NAME = item.FIELD_NAME.ToLower();
                if (item.RESULT != null) item.RESULT = item.RESULT.ToLower();
            }

            // Prepare two more versions of the chart
            var query2 = from item in _chart_items_since_admission
                    where(item.EVENT_DATETIME >= _pat.unit_arrival) && (item.EVENT_DATETIME <= _pat.pull_finish)
                    select item;
            _chart_items_since_unit_arrival = query2.ToArray();

            query2 = from item in _chart_items_since_admission
                     where (item.EVENT_DATETIME >= _pat.pull_start) && (item.EVENT_DATETIME <= _pat.pull_finish)
                     select item;
            _chart_items_during_pull_period = query2.ToArray();
            query2 = from item in _chart_items_since_admission
                     where (item.EVENT_DATETIME >= _pat.pull_start) && (item.EVENT_DATETIME <= _pat.pull_finish.AddHours(4))
                     select item;
            _chart_items_pull_period_plus = query2.ToArray();
        }

        // Delay looking for patient procedures until they are wanted
        private void LoadPatientProceduresIfNeeded()
        {
            if (_procedure_events != null) return;

            var db = PFSUtility.NewPfsDataContext();
            var query = from proc in db.PROCEDURE_EVENTs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                        select proc;
            _procedure_events = query.ToArray();
        }



        // Started a new chart item query (of a certain depth)
        // Default is the chart during the pull range.  Options for unit arrival and admission to hospital.
        private IEnumerable<CHART_ITEM> StartNewQuery()
        {
            if (isEDonly)
                return StartNewQuery(SearchDepth.SearchSinceAdmission);
            else
                return StartNewQuery(SearchDepth.SearchDefault);
        }
        private IEnumerable<CHART_ITEM> StartNewQuery(SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;

            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    return (from item in _chart_items_during_pull_period select item);
                case SearchDepth.SearchSinceArrival:
                    return (from item in _chart_items_since_unit_arrival select item);
                case SearchDepth.SearchSinceAdmission:
                    return (from item in _chart_items_since_admission select item);
                case SearchDepth.SearchPullPlus:
                    return (from item in _chart_items_pull_period_plus select item);
            }
            return null;
        }

        // Is this search term a word or a list of words?
        private bool ValueIsAList(string s) 
        {
            return (s != null) && s.Contains(',');                //is this a comma-separated list?
        }

        // Break apart a wordlist and prepare its members
        private string[] SplitOnCommaAndPrepareElements(string s)
        {
            // If a word list has a space after each comma, we will get leading blanks in each term
            var arr = s.Split(',');
            for (int i = 0; i <= arr.GetUpperBound(0); i++) {
                arr[i] = arr[i].Trim();                         // get rid of leading blanks
                arr[i] = arr[i].Replace(CHAR_COMMA, ",");       // insert a literal comma into search word
                arr[i] = arr[i].ToLower();                      // convert for case-insensitive comparisons
            }
            return arr;
        }

        // Add a basic chart item filter to a LINQ query
        //
        // cat = exact match
        // code_list = exact match; optional "like" match
        // desc_list = "like" match; optional exact match
        // field = exact match
        // result_list = "like" match; optional exact match
        //
        // These are comma-separated lists.  
        // Search for a literal comma by using CHAR_COMMA in the string.
        private IEnumerable<CHART_ITEM> AndItemFilter(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list) 
        {
            // Converted all values to lower case for case-insensitive comparisons.
            if (!String.IsNullOrEmpty(cat)) {
                cat = cat.ToLower();
                query = query.Where(e => e.CATEGORY == cat);
            }

            if (!String.IsNullOrEmpty(code_list)) {
                code_list = code_list.ToLower();
                if (code_list.Left(2) == EXACT_MATCH_PREFIX) {          // exact match override?
                    query = query.Where(e => e.CODE == code_list.Substring(2));
                } else if (code_list.Left(2) == CODE_LIKE_PREFIX)
                {            // override for "like" match?
                    query = query.Where(e => e.CODE.Contains(code_list.Substring(2)));
                } else if (ValueIsAList(code_list)) {
                    query = AndCodeInList(query, code_list);            // find one of the words
                } else {
                    query = query.Where(e => e.CODE.Contains(code_list));      // find this word
                }
            }
            
            if (!String.IsNullOrEmpty(desc_list)) {
                bool special1 = false; // (desc_list == ";ROUTE=IV");
                desc_list = desc_list.ToLower();
                if (desc_list.Left(2) == EXACT_MATCH_PREFIX) {          // exact match override?
                    query = query.Where(e => e.DESCRIPTION == desc_list.Substring(2));
                } else if (ValueIsAList(desc_list)) {
                    query = AndDescriptionInList(query, desc_list);
                } else {
                    desc_list = desc_list.Replace(CHAR_COMMA, ",");     // look for literal comma requests
                    query = query.Where(e => e.DESCRIPTION.Contains(desc_list));
                    if (special1)
                    {
                        query = query.Where(e => !e.DESCRIPTION.Contains(";ROUTE=IV SLOW PUSH"));
                        query = query.Where(e => !e.DESCRIPTION.Contains(";ROUTE=IV PUSH"));
                    }

                }
            }
            
            if (!String.IsNullOrEmpty(field)) {
                field = field.ToLower();
                query = query.Where(e => e.FIELD_NAME == field);
            }
            
            if (!String.IsNullOrEmpty(result_list)) {
                result_list = result_list.ToLower();                        // make all results case insensitve
                if (result_list.Left(2) == EXACT_MATCH_PREFIX) {     // exact match override?
                    query = query.Where(e => e.RESULT == result_list.Substring(2));
                } else if (ValueIsAList(result_list)) {
                    query = AndResultInList(query, result_list);
                } else {
                    result_list = result_list.Replace(CHAR_COMMA, ",");     // look for literal comma requests
                    query = query.Where(e => e.RESULT.Contains(result_list));
                }
            }
            return query;
        }

        // Look for a code that is one of the words in the list.
        private IEnumerable<CHART_ITEM> AndCodeInList(IEnumerable<CHART_ITEM> query, string code_list) 
        {
            if (String.IsNullOrEmpty(code_list)) return query;
            
            var arr = SplitOnCommaAndPrepareElements(code_list);
            return query.Where(e => e.CODE.ContainsAny(arr));  // use like match. Exact match = arr.Contains(e.CODE));   
        }

        // Look for a description that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndDescriptionInList(IEnumerable<CHART_ITEM> query, string desc_list) 
        {
            if (String.IsNullOrEmpty(desc_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(desc_list);
            return query.Where(e => e.DESCRIPTION.ContainsAny(arr));    // "like" match
        }

        // Look for a result that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultInList(IEnumerable<CHART_ITEM> query, string result_list) 
        {
            if (String.IsNullOrEmpty(result_list)) return query;
            
            var arr = SplitOnCommaAndPrepareElements(result_list);
            return query.Where(e => e.RESULT.ContainsAny(arr));         // "like" match
        }

        // Look for a result that does not contain one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultNotInList(IEnumerable<CHART_ITEM> query, string result_list)
        {
            if (String.IsNullOrEmpty(result_list)) return query;
            
            var arr = SplitOnCommaAndPrepareElements(result_list);
            return query.Where(e => !e.RESULT.ContainsAny(arr));        // "like" match
        }

        private string DescribeSearchDepth(SearchDepth search_depth) 
        {
            string result = "";
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            
            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    //result = "in pull range"
                    result = "";                         //be quiet since this is default
                    break;
                case SearchDepth.SearchSinceArrival:
                    result = "since arrival to unit";
                    break;
                case SearchDepth.SearchSinceAdmission:
                    result = "since admission";
                    break;
                case SearchDepth.SearchPullPlus:
                    result = "in LOS range plus 4 hours after";
                    break;
            }
            
            return result;
        }

        // Describe a query that has already been run
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Make a simple query and describe the results
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            var arr = query.ToArray();
            return Describe(arr, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private string LookingFor(string so_far, string what, string op, string value)
        {
            string result;
            if (String.IsNullOrEmpty(value)) return so_far;

            if (so_far == "")
                result = "looking for " + what;
            else
                result = so_far + " and " + what;

            if (value.Left(2) == EXACT_MATCH_PREFIX)
                result += "='" + value.Substring(2) + "'";
            else if (value.Left(2) == CODE_LIKE_PREFIX)
                result += " contains " + "'" + value.Substring(2) + "'";
            else if (ValueIsAList(value))
                result += " in '" + value + "'";
            else
                result += op + "'" + value + "'";
            
            return result;
        }
        private string Describe(CHART_ITEM[] arr, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            string result = "";
            result = LookingFor(result, "cat", "=", cat);
            result = LookingFor(result, "code", "=", code_list);
            result = LookingFor(result, "desc", " contains ", desc_list);
            result = LookingFor(result, "field", "=", field);
            result = LookingFor(result, "result", " contains ", result_list);

            if (arr.Count() == 0) {
                result += "; not found " + DescribeSearchDepth(search_depth);
            } else {
                var e = arr[0];
                result = "FOUND: " + result + "; found";
                // We might have searched for a pattern or word list in several fields - show what was found
                if (e.CATEGORY != null)     result += " cat='" + e.CATEGORY + "'";
                if (e.CODE != null)         result += " code='" + e.CODE + "'";
                if (e.DESCRIPTION != null)  result += " desc='" + e.DESCRIPTION + "'";
                if (e.FIELD_NAME != null)   result += " field='" + e.FIELD_NAME + "'";
                if (e.RESULT != null)       result += " result='" + e.RESULT + "'";
                // Are there more results?  Just say how many; we aren't going to list them.
                if (arr.Count() == 2) {
                    result += " (1 more result)";
                } else if (arr.Count() > 2) {
                    result += " (" + (arr.Count() - 1) + " more results)";
                }
            }
            
            return result;
        }


        //Set an indicator for this reason (low level)
        private void SetInd(int inum, string reason)
        {
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Set Ind #" + inum + ": " + reason);          //already set - repeat for verbose only
            } else {
                _inds[inum].is_checked = true;
                Program.Audit("Set Ind #" + inum + ": " + reason);
            }
        }

        //Clear an indicator for this reason (low level)
        private void ClrInd(int inum, string reason)
        {
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Clr Ind #" + inum + ": " + reason);          //already clear - repeat verbose only
            } else {
                _inds[inum].is_checked = false;
                Program.Audit("Clr Ind #" + inum + ": " + reason);
            }
        }

        // Count how many items meet the conditions
        // All items are listed
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountItems(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            int count = query.Count();

            // always return what was found
            found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
            // echo the result?
            if (trace) Program.VerboseAudit(found_what);
            
            return count;
        }

        // Count how many items meet the conditions
        // This has the option to list every entry or give a total
        private int CountResultInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what="";

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);

            var arr = SplitOnCommaAndPrepareElements(result_list);

            foreach (var item in query) {
                // Figure out which of the search words were found
                var s = item.RESULT.ContainsWhich(arr);
                found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' code='" + item.CODE + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "'";
                if (trace) Program.VerboseAudit(found_what);
                count++;
                if (count_mode != CountMode.CountAll) break;        //stop counting
            }
            
            if (count > 0) {
                //We already printed what was found; maybe add how many?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }
            
            return count;
        }

        // Count how many items meet the conditions
        // This has the option to list every entry or give a total
        private int CountResultInListEXCEPTList(string cat, string code_list, string desc_list, string field, string result_list, string negresult_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what = "";

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            query = AndResultNotInList(query, negresult_list);

            var arr = SplitOnCommaAndPrepareElements(result_list);

            foreach (var item in query)
            {
                // Figure out which of the search words were found
                var s = item.RESULT.ContainsWhich(arr);
                found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' code='" + item.CODE + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "'";
                if (trace) Program.VerboseAudit(found_what);
                count++;
                if (count_mode != CountMode.CountAll) break;        //stop counting
            }

            if (count > 0)
            {
                //We already printed what was found; maybe add how many?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            }
            else
            {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        // Count how many items contain ALL results in list
        private int CountResultInListAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what = "";

            var arr = SplitOnCommaAndPrepareElements(result_list);

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            for (int i = 0; i <= arr.GetUpperBound(0); i++)
            {
                if (arr[i].StartsWith(NOT_PREFIX))
                {
                    query = query.Where(e => !e.RESULT.Contains(arr[i].Substring(2)));
                }
                else
                {
                    query = query.Where(e => e.RESULT.Contains(arr[i]));
                }
            }

            count = query.Count();

            if (count > 0)
            {
                found_what = "found item with all results in '" + result_list + "' without those prefixed by !!";
                if (trace) Program.VerboseAudit(found_what);
            }
            else
            {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            if (ValueIsAList(result_list))
            {
                return CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what);
            } else {
                return CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what);
            }
        }

        // Is there an item with this result?
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list) 
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth) 
        {
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContains(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what) 
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            if (ValueIsAList(result_list))
            {
                return (CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            } else {
                return (CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
            }
        }

        // Result contains all in list
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            if (ValueIsAList(result_list))
            {
                return (CountResultInListAll(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            }
            else
            {
                found_what = "Not valid for non-list results.";
                return false;
            }
        }
        // Count how many items meet the conditions, with none of the results in result_list.
        // This has the option to say how many were found rather than list all.
        private int CountResultNotInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what) 
        {
            int count = 0, rec_count = 0;
            bool found_one;

            found_what = "";
            var arr = SplitOnCommaAndPrepareElements(result_list);         // also deals with CHAR_COMMA
            
            //Do not filter by result here; we need all results
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");
            
            foreach(var item in query) {
                rec_count++;
                //Look for each search word in the result
                found_one = false;
                
                foreach (string s in arr) {
                    if (String.Equals(item.RESULT, s)) {
                        found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "' -- ignore this";
                        Program.VerboseAudit(found_what);
                        found_one = true;
                        break;
                    }
                }

                if (! found_one) {
                    //none of the words were found - good!
                    found_what = Describe(cat, code_list, desc_list, field, "", search_depth) + " -- does not contain '" + result_list + "'";
                    Program.VerboseAudit(found_what);
                    count++;
                    if (count_mode != CountMode.CountAll) break;
                }
            }
            
            if (count > 0) {
                //We already printed what was found; maybe show the total?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else if (rec_count > 0) {
                //We already printed what we ignored
            } else {
                //Print what did not exist
                found_what = Describe(cat, code_list, desc_list, field, "", search_depth);      //not found
                Program.VerboseAudit(found_what);
            }
            
            return count;
        }

        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, string found_what) 
        {
            // Search in the pull range with trace on
            return CountResultDoesNotContain(cat, code_list, desc_list, field, result_list, out found_what, SearchDepth.SearchDefault, true);
        }
        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, out string found_what, SearchDepth search_depth, bool trace)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            return CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0;
        }

        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            return (CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0);
        }

        // Set the indicator if the conditions are met
        //
        private bool SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);

        }
        private bool SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            bool b = false;
            //avoid more queries if the indicator is already set
            if (_inds[inum].is_checked) return true;

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what)) {
                SetInd(inum, found_what);                          //echo here - found
                b = true;
            } else {
                Program.VerboseAudit(found_what);                  //and here - not found
            }
            return b;
        }
        // Special contains all
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContainsAll(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already set
            if (_inds[inum].is_checked) return;

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                          //echo here - found
            }
            else
            {
                Program.VerboseAudit(found_what);                  //and here - not found
            }
        }
        // Set the indicator if the result does not contain any of the words in result_list
        //
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultDoesNotContain(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {   
            string found_what;
            //avoid more queries if the indicator is already set
            if (_inds[inum].is_checked) return;

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            //Turn trace off for ResultDoesNotContain() and echo what was set below with SetInd
            if (ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                           //echo here - not found
            } else {
                Program.VerboseAudit(found_what);                   //and here - found
            }
        }

        // Clear the indicator if the result contains one of the words in the result_list
        //
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already clear
            if (! _inds[inum].is_checked) return;

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            //Turn trace off for ResultContains() and echo what was set below with SetInd
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what)) {
                ClrInd(inum, found_what);                           //echo here - found
            } else {
                Program.VerboseAudit(found_what);                   //and here - not found
            }
        }


        //These slightly smaller functions are meant for places where you aren't really looking for a chart result
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            return (CountResultContains(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
        }

        // Variations with optional parameters (can switch to optional parameters in VS 2010)
        //
        private void SetIndIfFound(int inum, string cat)
        {
            SetIndIfResultContains(inum, cat, "", "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list)
        {
            SetIndIfResultContains(inum, cat, code_list, "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private void ClrIndIfFound(int inum, string cat)
        {
            ClrIndIfResultContains(inum, cat, "","","","", SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }


        // Get the max/total value from a result (usually in the middle of the text)
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetIntValue(get_mode, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            int value;
            int result = 0;
            bool found_one = false;

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            
            //Look for a number in the result
            
            foreach(var item in query) {
                // RESULT might be null so convert to empty string so Split won't blow up
                var arr = PFSUtility.DBToString(item.RESULT).Split(' ');
                foreach (string s in arr) {
                    //Look for 1st character numeric; IsNumeric("60min") will fail so look at 1st char only
                    if (s.Left(1).IsNumeric()) {
                        value = (int)s.Val();                           //Use Val; ToInteger will error on "60min"
                        switch (get_mode) {
                            case GetValueMode.GetMax:
                                result = Math.Max(value, result);       //max
                                break;
                            case GetValueMode.GetTotal:
                                result += value;                        //total
                                break;
                            case GetValueMode.GetLast:
                                result = value;                         //last
                                break;
                        }
                        
                        // print what we are searching for (the first time)
                        if (! found_one) {
                            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
                        }
                        found_one = true;
                        // print each value found
                        Program.VerboseAudit("  found numeric value " + result);
                        //Keep going in case there are more
                    }
                } 
            }
            
            if (! found_one) {
                //show what was not found
                Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
            }
            
            return result;
        }

        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetMaxValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            return GetIntValue(GetValueMode.GetMax, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetTotalValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            return GetIntValue(GetValueMode.GetTotal, cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Get a result; returns true if found with return_result
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result)
        {
            return GetResult(cat, code_list, desc_list, field, out return_result, SearchDepth.SearchDefault);
        }
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0) {
                return_result = PFSUtility.DBToString(query.First().RESULT);
            } else {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }
        private void GetResultforOther(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "Other (comment)");
            return_result="";
            foreach (var item in query)
            {
                if (!String.Equals(item.RESULT, "Other (comment)"))
                {
                    return_result = item.RESULT;
                    //Program.VerboseAudit(found_what);
                    break;
                }

            }
            return;
        }

        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, out string return_result, out DateTime return_evdt)
        {
            return GetResultAndEVDT(cat, code_list, desc_list, field, out return_result, out return_evdt, SearchDepth.SearchDefault);
        }
        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, out string return_result, out DateTime return_evdt, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0)
            {
                return_result = PFSUtility.DBToString(query.First().RESULT);
                return_evdt = PFSUtility.DBToDateTime(query.First().EVENT_DATETIME);
            }
            else
            {
                return_result = "";
                return_evdt = DateTime.MinValue;
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }


        //======================================================================================
        // Mapping starts here
        //======================================================================================

//1	ADL - Self Care	1
//2	ADL - Assist	1
//3	ADL - Extended	1
//4	ADL - Two or More	NULL
//5	Communication Support	NULL
//6	Safety Management - q 30 Minutes	6
//7	Safety Management - q 15 Minutes	6
//8	Behavior/Emotional Management	8
//9	Behavior/Emotional Management >= 30 Minutes	8
//10	Fluid Management	NULL
//11	Physiologic Assessment - q 1 Hour	11
//12	Physiologic Assessment - q 30 Minutes	11
//13	Wound/Injury Management	NULL
//14	Medication Management >= 5 Medications	14
//15	Admitted	15
//16	Transferred to another Facility	15
//17	Expired	15

        private void Check_123()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Inpt 1. ADL - Self Care");
            Program.VerboseAudit("ED Inpt 2. ADL - Assist");
            Program.VerboseAudit("ED Inpt 3. ADL - Extended");
            Program.VerboseAudit("---------------");
            string reslist = "";

            SetIndIfResultContains(2, "", "3045001065", "", "", reslist);

        }


        private void Check_4()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Inpt 4. ADL - Two or more");
            Program.VerboseAudit("---------------");
            string reslist = "", res;

            SetIndIfResultContains(4, "", "9990000305560", "", "", reslist);
            SetIndIfResultContains(4, "", "9990304000778", "", "", reslist);
            SetIndIfResultContains(4, "", "9990000400604", "", "", reslist);
            SetIndIfResultContains(4, "", "9990007060350", "", "", reslist);
            SetIndIfResultContains(4, "", "9990000342030", "", "", reslist);
            SetIndIfResultContains(4, "", "9991600100065", "", "", reslist);


            reslist = "Ambulate in hall,Ambulate in room,Bathroom privileges,";
            reslist += "Bedrest,Bike,Chair,Commode,Dangle,Supervised exercise,Held,";
            reslist += "Stand at bedside,Tilt table,Turn,Up in chair,Wagon";
            SetIndIfResultContains(3, "", "9990000305560", "", "", reslist);

            reslist = "1 Assist,2 Assist,3 Assist,>4 Assist,Supervision required (Comment)";
            SetIndIfResultContains(3, "", "9990304000778", "", "", reslist);

 reslist = "Lying right side,Lying left side,Supine,Semi Fowler,Pillow support,Prone,";
 reslist += "Sitting,Standing,Up in chair,Continuous lateral rotation,Micro turn left,";
 reslist += "Micro turn right,Do Not Turn,Unstable to turn";
SetIndIfResultContains(3,"","9990000400604","","",reslist);

reslist = "Assist,Total Care";
SetIndIfResultContains(3,"","9991025006475","","",reslist);

            reslist = "Left side,Left tilt,Right side,Right tilt,Semi-fowlers,Fowlers,Knee chest,";
reslist += "Chair,Rocking chair,Supine,Trendelenberg,Squatting,Standing,Kneeling,Walking";
SetIndIfResultContains(3, "", "1028000027", "", "", reslist);

reslist = "1 Assist,2 Assist,3 Assist,>4 Assist,Supervised (Comment)";
SetIndIfResultContains(3, "", "9990007060350", "", "", reslist);

            reslist = "Bathed,Chlorhexidine Bath,Shower,Foley care,Peri care,";
            reslist += "Hair washed,Hair dried or curled,Shaved";
SetIndIfResultContains(3, "", "9990000342030", "", "", reslist);

            reslist = "Partial assist,Complete assist";
SetIndIfResultContains(3, "", "9990000305650", "", "", reslist);

reslist = "Needs assist,Supervised,Total assist";
reslist = "";
SetIndIfResultContains(3, "", "9990000700380", "", "", reslist);
SetIndIfResultContains(3, "", "3043040100003", "", "", reslist);
SetIndIfResultContains(3, "", "9993040100004", "", "", reslist);
SetIndIfResultContains(3, "", "3043040101422", "", "", reslist);
SetIndIfResultContains(3, "", "9993040101423", "", "", reslist);	   

SetIndIfResultContains(3,"",EXACT_MATCH_PREFIX+"23","","","Yes");
reslist = "";
SetIndIfResultContains(3, "", "3045001090", "", "", reslist);
reslist = "";
SetIndIfResultContains(3, "", "3045001089", "", "", reslist);
SetIndIfResultContains(3, "", EXACT_MATCH_PREFIX+"16", "", "", "Yes");
reslist = "Amber,Clear,Cloudy,Dark,Fibrin,Light,Pink,Red,Yellow";
SetIndIfResultContains(3, "", "9990000370180", "", "", reslist);
reslist = "";
SetIndIfResultContains(3, "", "3040011360", "", "", "");
reslist = "";
SetIndIfResultContains(3, "", "9991600100064", "", "", "Done");
reslist = "Stand-by assist,One staff assist,Two staff assist";
SetIndIfResultContains(3, "", "9991600100065", "", "", reslist);
SetIndIfResultContains(3, "", "9991600100066", "", "", reslist);
reslist = "Bedpan,Catheter,Commode,Incontinence pad";
SetIndIfResultContains(3, "", "9991600100067", "", "", reslist);
reslist = "Bed bath";
SetIndIfResultContains(3, "", "9991600100068", "", "", reslist);
reslist = "3";
SetIndIfResultContains(3, "", "9993040000407", "", "", reslist);

if (GetResult("", "3045001023", "", "", out res))
{
    if (res.Left(1).IsNumeric())
    {
        int value = (int)res.Val();
        if ((value >= 9) && (value <= 12)) SetInd(3, "3045001023 value=" + res);
    }
}
if (GetResult("", "9993040001207", "", "", out res))
{
    if (res.Left(1).IsNumeric())
    {
        int value = (int)res.Val();
        if ((value >= 9) && (value <= 12)) SetInd(3, "9993040001207 value=" + res);
    }
}

reslist = "";
SetIndIfResultContains(3, "", "3045001046", "", "", "Confused");
reslist = "Disoriented X4,Disoriented to person,Disoriented to place,Disoriented to time,Disoriented to situation";
SetIndIfResultContains(3, "", "9990000301870", "", "", reslist);
reslist = "";
SetIndIfResultContains(3, "", "9990000398010", "", "", "4");
reslist = "Blind,Nystagmus";
SetIndIfResultContains(3, "", "9990000002106", "", "", reslist);
SetIndIfResultContains(3, "", "9990000002107", "", "", reslist);
reslist = "";
SetIndIfResultContains(3, "", "9990007090070", "", "", "2,3");
reslist = "Blindness - right,Blindness - left";
SetIndIfResultContains(3, "", "9993040001091", "", "", reslist);
reslist = "Strabismus - right,Strabismus - left";
SetIndIfResultContains(3, "", "9990000002216", "", "", reslist);
reslist = "1,2,3,4,Amputation or joint fusion";
SetIndIfResultContains(3, "", "9990007090090", "", "", reslist);
SetIndIfResultContains(3, "", "9990007090100", "", "", reslist);
SetIndIfResultContains(3, "", "9990007090110", "", "", reslist);
SetIndIfResultContains(3, "", "9990007090120", "", "", reslist);
SetIndIfResultContains(3, "", "9990007090130", "", "", reslist);
reslist = "Tremors,Flaccid,Abnormal extension (Decerebrate),";
reslist += "Abnormal flexion (Decorticate),No movement to painful stimulus,";
reslist += "Non-purposeful movement,Spastic";
SetIndIfResultContains(3, "", "9990000301980", "", "", reslist);
SetIndIfResultContains(3, "", "9990000301940", "", "", reslist);
SetIndIfResultContains(3, "", "9990000302000", "", "", reslist);
SetIndIfResultContains(3, "", "9990000301960", "", "", reslist);
reslist = "Injury/Trauma,Limited movement,Deformity,Paralysis,Rotated,Immobilizer";
SetIndIfResultContains(3, "", "9990000304100", "", "", reslist);
SetIndIfResultContains(3, "", "9990000304120", "", "", reslist);
reslist = "Injury/Trauma,Limited movement,Deformity";
SetIndIfResultContains(3, "", "9990000303930", "", "", reslist);
SetIndIfResultContains(3, "", "9990000303970", "", "", reslist);
SetIndIfResultContains(3, "", "9990000304010", "", "", reslist);
SetIndIfResultContains(3, "", "9990000304050", "", "", reslist);
SetIndIfResultContains(3, "", "9990000304090", "", "", reslist);
reslist = "Cock up wrist velcro,Ulnar gutter,Volar,Arm aluminum,Short arm,Long arm,";
reslist += "Short arm fiberglass,Long arm fiberglass,Sugar tong,Double sugar tong- arm,";
reslist += "Short leg,Long leg,Short leg fiberglass,Long leg fiberglass,Sugar tong leg,";
reslist += "Robert Jones,Modified Robert Jones,Air cast stirrup,Posterior ankle,Cadillac (stirrup/posterior combo)";
SetIndIfResultContains(3, "", "9991600100112", "", "", reslist);
reslist = "Boxer,Colles,Cylinder,Short arm,Long arm,Long arm hanging,Short arm fiberglass,";
reslist += "Long arm fiberglass,Navicular fiberglass,Short arm navicular,Long arm navicular,";
reslist += "Short leg,Long leg,Short leg fiberglass,Long leg fiberglass,Cylinder fiberglass,";
reslist += "Patella with bearing,Spica single,Spica double,Spica single fiberglass,Spica double fiberglass,";
reslist += "Reinforce" + CHAR_COMMA +" minor,Reinforce"+CHAR_COMMA+" major,";
reslist += "Reinforced fiberglass,Monovalve,BiValve,Window,Window by tech,Other (Comment)";
SetIndIfResultContains(3, "", "9991600100115", "", "", reslist);
reslist = "Boot,Shoe,Post-op,Walking,Cam walker,Air Cast,Other (Comment)";
SetIndIfResultContains(3, "", "9991600100117", "", "", reslist);
reslist = "Shoulder immobilizer,Walk wraps,Humeral fracture brace,Knee immobilizer,Neoprene knee brace,Other (Comment)";
SetIndIfResultContains(3, "", "9991600100118", "", "", reslist);
reslist = "Abductor pillow,Arm elevation pillow,Arm sling,Rib belt,Clavicle shoulder strap,Dressing, soft wrap knee,Elastic Wrist";
SetIndIfResultContains(3, "", "9991600100119", "", "", reslist);
reslist = "Complaining of back injury,Complaining of head/neck injury,Fall,Injury,";
reslist += "MVC,Sporting accident,UTA=Unable to assess,Other (Comments)";
SetIndIfResultContains(3, "", "9991600100122", "", "", reslist);
reslist = "Rigid cervical collar,Backboard,Soft cervical collar,Head blocks or towels,Cervical immobilization device,Other (Comment)";
SetIndIfResultContains(3, "", "9991600100123", "", "", reslist);
reslist = "Skeletal,Skin,Cervical,Other";
SetIndIfResultContains(3, "", "9991600000289", "", "", reslist);
reslist = "Yes,No";
SetIndIfResultContains(3, "", "9991600100062", "", "", reslist);
reslist = "";
SetIndIfResultContains(3, "", "9990160101903", "", "", "Yes");
reslist = "Carried,Crutches,Restrained,Stretcher,Stroller";
SetIndIfResultContains(3, "", "9991600100022", "", "", reslist);
reslist = "Sinus rhythm,Sinus bradycardia,Sinus tachycardia,Sinus arrest,Sinus arrhythmia,";
reslist += "Atrial paced,Ventricular paced,A-V Sequential paced,Agonal,Asystole,Atrial fibrillation,";
reslist += "Atrial fibrillation w/rapid ventricular response,Atrial flutter,Heart block,";
reslist += "Junctional accelerated,Junctional rhythm,Junctional tachycardia,Pulseless electrical activity,";
reslist += "Supraventricular tachycardia,Torsades de Pointes,Ventricular fibrillation,Ventricular tachycardia";
SetIndIfResultContains(3, "", "3045001065", "", "", reslist);

reslist = "Mask,Tubing,CPAP,BPAP,Patient able to manage equipment on own,";
reslist += "Equipment inspected (per site policy),Waiver signed (per site policy),Other";
if (!Exists("", "9993040000639", "", "", reslist))
{
    reslist = "Bilevel,CPAP,Auto-Bilevel,Auto-CPAP,AVAPS,PCV";
    SetIndIfResultContains(3, "", "3045001108", "", "", reslist);
    reslist = "Nasal pillows,Nasal mask,Nasal Prongs;,Nasal pharyngeal,Full face mask,";
    reslist += "Performax,Total face mask,Endotracheal,Tracheostomy";
    SetIndIfResultContains(3, "", "9993040000637", "", "", reslist);
   
    reslist = "Delayed swallow or interrupted swallow,";
    reslist += "Coughs"+CHAR_COMMA+" clears throat"+CHAR_COMMA+" chokes"+CHAR_COMMA+" gags up to 1 min after drinking,";
    reslist += "Unable to say \"ah\" or count to 3 immediately after drinking,";
    reslist += "Wet voice when saying \"ah\" or counting to 3,";
    reslist += "Drools after swallowing,";
    reslist += "Desaturates 2% or > from baseline seconds after swallow,";
    reslist += "Swallows more than one time after drinking,";
    reslist += "Unable to assess (Comment)";
    SetIndIfResultContains(3, "", "9993040108712", "", "", reslist);	   
}

 
reslist = "Ambulate in hall,Ambulate in room,Bathroom privileges,Bedrest,Bike,";
reslist += "Chair,Commode,Dangle,Supervised exercise,Held,Stand at bedside,Tilt table,Turn,Up in chair,Wagon";
bool D1 = Exists("","9990000305560","","",reslist);	 

reslist = "1 Assist,2 Assist,3 Assist,>4 Assist";
bool D2 = Exists("","9990304000778","","",reslist);	   

reslist = "Lying right side,Lying left side,Supine,Semi Fowler,Pillow support,";
reslist += "Prone,Sitting,Standing,Up in chair,Continuous lateral rotation,Micro turn left,";
reslist += "Micro turn right,Do Not Turn,Unstable to turn";
bool D3 = Exists("","9990000400604","","",reslist);	   

reslist = "Assist,Total Care";
bool D4 = Exists("","9991025006475","","",reslist);	   

reslist = "Left side,Left tilt,Right side,Right tilt,Semi-fowler,Fowler,";
reslist += "Knee chest,Chair,Rocking chair,Supine,Trendelenberg,Squatting,Standing,Kneeling,Walking";
bool D5 = Exists("","1028000027","","",reslist);	   

reslist = "1 Assist,2 Assist,3 Assist,>4 Assist";
bool D6 = Exists("","9990007060350","","",reslist);	   

reslist = "Bathed,Chlorhexidine Bath,Shower,Foley care,Peri care";
bool D7 = Exists("","9990000342030","","",reslist);	   

reslist = "Partial assist,Complete assist";
bool D8 = Exists("","9990000305650","","",reslist);	   

reslist = "Needs assist,Total assist";
bool D9 = Exists("","9990000700380","","",reslist);	   

reslist = "";
bool D10 = Exists("","3043040100003","","",reslist);	   
bool D11 = Exists("","9993040100004","","",reslist);	   
bool D12 = Exists("","3043040101422","","",reslist);	   
bool D13 = Exists("","9993040101423","","",reslist);	   

bool D15 = Exists("",EXACT_MATCH_PREFIX+"23","","","Yes");
reslist = "";
bool D16 = Exists("","3045001090","","",reslist);	   
bool D17 = Exists("","3045001089","","",reslist);	   
bool D18 = Exists("",EXACT_MATCH_PREFIX+"16","","","Yes");	   

bool D19 = false;

reslist = "Amber,Clear,Cloudy,Dark,Fibrin,Light,Pink,Red,Yellow";
bool D20 = Exists("","9990000370180","","",reslist);	   

reslist = "";
bool D21 = Exists("","3040011360","","",reslist);	   

bool Dgroup1 = ((D1 && D2) || (D2 && D3) || (D4 && D5));
bool Dgroup2 = ((D6 && D7) || (D7 && D8));
bool Dgroup3 = (D9 || D10 || D11 || D12 || D13);
bool Dgroup4 = (D15 || D16 || D17 || D18 || D20 || D21);
//"FOR ADL Extended rows D-1 through D-19, must include charting for 3 of the 4 bullets below on the visit:
//- D1&D2 or D2&D3 or D4&D5   (mobility)
//- D6&7 or D7&D8 (hygiene)
//- Any of D-9, D-10, D-11, D-12, D-13, ADL-LDA-21 or ADL-LDA-25 (feeding)
//- any of D-15, D-16, D-17, D-18, D-20, D-21 or ADL-LDA-12 to  ADL-LDA-20 (toileting)"	
if ((Dgroup1 ? 1 : 0) + (Dgroup2 ? 1 : 0) + (Dgroup3 ? 1 : 0) + (Dgroup4 ? 1 : 0) >= 3)
{
    SetInd(4, "Three or more ADL D-groups present: Group1=" + (Dgroup1 ? 1 : 0) + " Group2=" + (Dgroup2 ? 1 : 0) + " Group3=" + (Dgroup3 ? 1 : 0) + " Group4=" + (Dgroup4 ? 1 : 0));
}

bool D26 = Exists("","9991600100064","","","Done");	   
reslist = "Stand-by assist,One staff assist,Two staff assist";
bool D27 = Exists("","9991600100065","","",reslist);	   
bool D28 = Exists("","9991600100066","","",reslist);	   
reslist = "Bedpan,Catheter,Commode,Incontinence pad";
bool D29 = Exists("","9991600100067","","",reslist);	   
bool D30 = Exists("","9991600100068","","","Bed bath");	   
if ((D26 || D30) && (D27) && (D28 || D29))
{
    SetInd(4, "All ED ADL D-groups present: D26=" + (D26 ? 1 : 0) + " D27=" + (D27 ? 1 : 0) + " D28=" + (D28 ? 1 : 0) + " D29=" + (D29 ? 1 : 0)+ " D30=" + (D30 ? 1 : 0));
}

SetIndIfResultContains(4, "", "9993040000407", "", "", "4");
if (GetResult("", "3045001023", "", "", out res))
{
    if (res.Left(1).IsNumeric())
    {
        int value = (int)res.Val();
        if ((value <= 8)) SetInd(4, "3045001023 value=" + res);
    }
}
if (GetResult("", "9993040001207", "", "", out res))
{
    if (res.Left(1).IsNumeric())
    {
        int value = (int)res.Val();
        if ((value <= 8)) SetInd(4, "9993040001207 value=" + res);
    }
}
reslist = "Strong stimuli to arouse,";
reslist += "Unconscious with motor response (withdrawal, flexion, extension),";
reslist += "Unconscious with no motor response";
reslist += "Pharmacologically paralyzed";
SetIndIfResultContains(4,"","3045001046","","",reslist);

SetIndIfResultContains(4,"","3045001080","","","6");
SetIndIfResultContains(4,"","3045001081","","","0,1");
if (GetResult("", "3045001079", "", "", out res))
{
    if (res.Left(1).IsNumeric())
    {
        int value = (int)res.Val();
        if ((value == -3)||(value == -4)||(value == -5)) SetInd(4, "3045001079 value=" + res);
    }
}
SetIndIfResultContains(4,"","9993040104675","","","4");
SetIndIfResultContains(4,"","3045001117","","",""); //any characters
reslist = "PACU Ventilator Rows,Trilogy Ventilator Rows,HFO Ventilator Rows,";
reslist += "BPAP/CPAP Rows,Abbreviated Settings,NAVA Rows";
SetIndIfResultContains(4, "", "9993040000635", "", "", reslist);
reslist = "Somnolence,RUE paresis,RLE paresis,LUE paresis,LLE paresis";
SetIndIfResultContains(4,"","9990000450600","","",reslist);
reslist = "Unresponsive,Paralized,Sedated,Loss Of Consciousness";
SetIndIfResultContains(4,"","9990160239301","","",reslist);
reslist = "Respiratory arrest,Cardiac arrest,Unknown,Other (Comments)";
SetIndIfResultContains(4,"","9991600100646","","",reslist);
reslist = "Reposition,Suction,Jaw thrust,Chin lift,Foreign object removal";
SetIndIfResultContains(4,"","9991010010010","","",reslist);
reslist = "Endotracheal tube,Esophageal - tracheal tube,Laryngeal mask airway (LMA),Nasopharyngeal airway (NPA),Oropharyngeal airway (OPA),Tracheostomy,Other (Comment)";
SetIndIfResultContains(4,"","9991600100681","","",reslist);
reslist = "Bag-valve-mask,Bag-valve-ET tube,Bag-valve-tracheostomy,Ventilator,";
reslist += "Bilevel positive airway pressure (BiPAP),Continuous positive airway pressure (CPAP),";
reslist += "CPAP nasal,CPAP mask,Positive pressure ventilation (PPV),Other (Comments)";
SetIndIfResultContains(4,"","9991600100682","","",reslist);
SetIndIfResultContains(4,"","9990000016054","","","1,2");
reslist = "Level red,Level yellow";
SetIndIfResultContains(4,"","9991600100426","","",reslist);
SetIndIfResultContains(4,"","9990160100151","","",reslist);

AtLeastOneADL();

        }


        private void Check_5()
        {
            string reslist = "";            
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Inpt 5. Communication Support");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(5, "", "9991733444441", "", "", reslist);
            SetIndIfResultContains(5, "", "9993040109069", "", "", reslist);
            SetIndIfResultContains(5, "", "9993040109071", "", "", reslist);
            SetIndIfResultContains(5, "", "9993040108551", "", "", reslist);
            SetIndIfResultContains(5, "", "9990007070581", "", "", reslist);
            SetIndIfResultContains(5, "", "9993040108693", "", "", reslist);


            reslist = "Artificial airway,Attempts to verbalize,Delayed responses,Dysphasia,";
reslist += "Expressive aphasia,Garbled,Global aphasia,Incomprehensible,Nods/gestures appropriately,";
reslist = "Non-verbal,Receptive aphasia,Slurred,Uses communication aid(s)";
SetIndIfResultContains(5,"","9990000301890","","",reslist);	   

reslist = "Impaired vision- not corrected,Blind";
bool F2 = Exists("","9990000002106","","",reslist);	   
bool F3 = Exists("","9990000002107","","",reslist);	   
            if (F2 && F3) SetInd(5, "Both F2 and F3 present.");

reslist = "Impaired hearing - not corrected,Acute hearing loss,Deaf";
bool F4 = Exists("","9990000002108","","",reslist);	   
bool F5 = Exists("","9990000002109","","",reslist);
if (F4 && F5) SetInd(5, "Both F4 and F5 present.");

reslist = "Difficulty talking,Trach,Hoarse,Muffled,Speaking valve,Voice amplifier";
            SetIndIfResultContains(5,"","9990000002115","","",reslist);	   

SetIndIfResultContains(5,"","9990304000117","","","3");	   

SetIndIfResultContains(5,"","3045001024","","","Inappropriate words");	   
reslist = "Delayed,Impoverished,Mumbled,Mute,Overproductive,Pressured,Rambling,Rapid,Slurred,Stutter";
SetIndIfResultContains(5,"","9993040105630","","",reslist);	   

SetIndIfResultContains(5,"","9990007090150","","","1,2,3");	   
SetIndIfResultContains(5,"","9990007090160","","","1,2");	   
SetIndIfResultContains(5,"","9990007090070","","","2,3");

  
SetIndIfResultContainsAll(5,"","9993040001091","","","Blindness - right,Blindness - left");	   

SetIndIfResultContainsAll(5,"","9990000002221","","","Unable to hear - right,Unable to hear - left");	   
reslist = "In person,iPad,Phone,Other (Comment)";
SetIndIfResultContains(5,"","9991733444441","","",reslist);	   
reslist = "Admission,Assessment,Consent,Discharge instructions,Education,Plan of care,Other (Comment)";
SetIndIfResultContains(5,"","9993040109069","","",reslist);	   
reslist = "Hospital/clinic approved on site interpreter,Telephone interpreter,Video remote interpreter,Other (Comment)";
SetIndIfResultContains(5,"","9993040109071","","",reslist);	   

SetIndIfResultContains(5,"","9993040108551","","","");
reslist = "Braille,Communication board,Hearing aid,Interpreter (Legal),Sign language";
SetIndIfResultContains(5,"","9990007070581","","",reslist);	   

SetIndIfResultContains(5,"","9993040108693","","","Yes");

            reslist = "Mask,Tubing,CPAP,BPAP,Patient able to manage equipment on own,Equipment inspected (per site policy),Waiver signed (per site policy),Other";
bool F23 = Exists("", "9993040000639", "", "", reslist);
if (!F23)
{
    reslist = "Bilevel,CPAP,Auto-Bilevel,Auto-CPAP,AVAPS,PCV";
    SetIndIfResultContains(5, "", "3045001108", "", "", reslist);
    reslist = "Full face mask,Performax,Total face mask,Endotracheal,Tracheostomy";
    SetIndIfResultContains(5, "", "9993040000637", "", "", reslist);
}


        }


        private void Check_67()
        {
            string reslist = "";
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Inpt 6. Safety Management - q 30 min");
            Program.VerboseAudit("ED Inpt 7. Safety Management - q 15 min");
            Program.VerboseAudit("---------------");


            SetIndIfResultContains(6, "", "9993040009123", "", "", reslist);
            SetIndIfResultContains(6, "", "9993040009234", "", "", reslist);


            SetIndIfResultContains(7, "", "9993040009123", "", "", reslist);
            SetIndIfResultContains(7, "", "9993040009234", "", "", reslist);
            SetIndIfResultContains(7, "", "9990000300101", "", "", reslist);
            SetIndIfResultContains(7, "", "9990000300001", "", "", reslist);
            SetIndIfResultContains(7, "", "9990007096219", "", "", reslist);
            SetIndIfResultContains(7, "", "9990007096227", "", "", reslist);

            reslist = "Impulsive,Lack of safety awarenesss,Does not use call light or ask for assistance,Danger to self or others,Danger from others,Exit seeking";
            bool G1 = Exists("", "9993040009123", "", "", reslist);
            reslist = "Q30 min,Q15 min,Q5 min,Line of sight";
            bool G2 = Exists("", "9993040009234", "", "", reslist);
            if (G1 && G2) SetInd(6, "Both G1 and G2 present.");

            SetIndIfResultContains(6, "", "9990000300101", "", "", "24 hours");
            reslist = "4 hours (Age 18 and older),2 hours (Age 9 to 17),1 hour (Age 8 and younger)";
            SetIndIfResultContains(6, "", "9990000300001", "", "", reslist);

            reslist = "1:1 Nsg care for seclusion for the first hour,";
reslist += ",Constant Nsg care for restraints for the first hour";
reslist += ",Record behavior every 5 minutes";
reslist += ",Review strengths/comfort measures - assist pt in reaching goal for discontinuation";
reslist += ",Administration of medication - to help client regain previous level of functioning";
reslist += ",Respiratory status assessed/documented each check - to ensure adequate air exchange";
reslist += ",Restraints checked every 15 minutes - to prevent injury from cuffs constricting normal blood flow or irritation of skin";
reslist += ",RN hourly assessment - mental"+CHAR_COMMA+" behavior and respiratory status - to minimize length of the procedure";
reslist += ",Offer fluids every 2 hours - to provide elimination opportunities";
reslist += ",Offer toileting every 2 hours - to provide elimination opportunities";
reslist += ",ROM to joints every 2 hours - to prevent injury from occurring when client is in restraints";
reslist += ",Hygiene PRN - to provide comfort and support";
reslist += ",Meals and snacks offered - to maintain nutrition (document reason for not providing and meal or snack at regular times)";
reslist += ",Other (Comment)";
            SetIndIfResultContains(6, "", "9990007096219", "", "", reslist);

            reslist = "Implemented - Seclusion Treatment Policy";
reslist += ",Implemented - Behavioral Restraint Policy";
reslist += ",Procedure explained to patient";
reslist += ",Reason for seclusion/restraints explained";
reslist += ",Informed the patient of the goal\\Mattress checked for dangerous items";
reslist += ",Mattress checked for dangerous items";
reslist += ",check room for lighting"+CHAR_COMMA+" temperature and safety";
reslist += ",Dangerous items and jewelry removed";
reslist += ",Respiratory status monitored";
reslist += ",Pt assisted in achieving goal for d/c";
            SetIndIfResultContains(6, "", "9990007096227", "", "", reslist);

            reslist = "Impulsive,Lack of safety awarenesss,Does not use call light or ask for assistance,Danger to self or others,Danger from others,Exit seeking";
            bool H1 = Exists("", "9993040009123", "", "", reslist);
            reslist = "Continuous observation by RN with patient,";
reslist += ",Continuous observation by non-RN staff with patient";
reslist += ",Continuous observation by two staff with patient";
            bool H2 = Exists("", "9993040009234", "", "", reslist);
            if (H1 && H2) SetInd(7, "Both H1 and H2 present.");

        }

        private void Check_89()
        {
            string reslist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Inpt 8. Behavior/Emotional Mgt");
            Program.VerboseAudit("ED Inpt 9. Behavior/Emotional Mgt >= 30min");
            Program.VerboseAudit("---------------");

            reslist = "Agitated,Behavior plan,Catatonic,Combative,Compulsive,Crying";
reslist += ",Demanding,Destructive,Disorganized,Echopraxic,Exit seeking,Fussy (infant)";
reslist += ",Guarded,Hostile,Hyperactive,Impulsive,Intrusive,Motor perseveration,Oppositional";
reslist += ",Pacing,Preoccupied,Disruptive,Psychomotor retardation,Pushing limits,Reckless";
reslist += ",Resistant to care,Rigid,Ritualistic,Tearful,Staff seeking,Seclusive,Sexually inappropriate,Tics,Withdrawn";
            bool I2 = Exists("", "9993040105629", "", "", reslist);

            reslist = "1:1 RN Time,Assign a task,Aromatherapy,Assisted relaxation";
reslist += ",Consistent response,Exposure therapy,Limit setting,Medicated (see MAR)";
reslist += ",Neutral response,Physical redirection,Quiet room,Reorientation,Time out";
reslist += ",Verbal redirection,Following behavior safety plan (See notes),Multiple Staff for acute behavioral intervention";
            bool I3 = Exists("", "9993040009823", "", "", reslist);
            bool J2 = Exists("", "9993040000352", "", "", reslist);

            if (I2 && I3) SetInd(8, "Both I2 and I3 present.");

            reslist = "Angry,Anxious,Apathetic,Depressed,Dysphoric,Elevated,Euphoric";
            reslist += ",Fearful,Guilty,Irritable,Manic,Sad";
            bool I4 = Exists("", "9993040105622", "", "", reslist);
            if (I4 && J2) SetInd(8, "Both I4 and J2 present.");

            SetIndIfResultContains(8, "", "9993040105637", "", "", "Yes");
            SetIndIfResultContains(8, "", "9993040105640", "", "", "Acts of violence (Comment),Threats of violence (Comment)");

            SetIndIfResultContains(8, "", "9993040000405", "", "", "Disruptive");

            reslist = "Hallucination,Illusions,Patient responding to internal Stimuli";
            SetIndIfResultContains(8, "", "9993040105625", "", "", reslist);

            reslist = "Auditory (Comment),Visual (Comment),Tactile (Comment),Command,Olfactory (Comment)";
            SetIndIfResultContains(8, "", "9991540100181", "", "", reslist);

            reslist = "Persecution,Grandeur,Obsessions,Religiosity,Phobias,Influence";
            reslist += ",Reference,Sexual delusions,Somatization,Thought broadcasting,Thought insertion,Thought withdrawal";
            SetIndIfResultContains(8, "", "9991540100180", "", "", reslist);

            reslist = "Combative,Destructive,Hostile,Disruptive,Pushing limits,Sexually inappropriate";
            SetIndIfResultContains(9, "", "9993040105629", "", "", reslist);

            reslist = "Agitated,Behavior plan,Catatonic,Compulsive,Crying,Demanding";
reslist += ",Disorganized,Echopraxic,Exit seeking,Fussy (infant),Guarded,Hyperactive";
reslist += ",Impulsive,Intrusive,Motor perseveration,Oppositional,Pacing,Preoccupied";
reslist += ",Psychomotor retardation,Reckless,Resistant to care,Rigid,Ritualistic";
reslist += ",Tearful,Staff seeking,Seclusive,Tics,Withdrawn";
            bool J1b = Exists("", "9993040105629", "", "", reslist);
            if (J1b && J2) SetInd(9, "Both J1b and J2 present.");

            reslist = "Agitated,Combative,Compulsive,Crying,Destructive,Disorganized";
reslist += ",Disruptive,Guarded,Hostile,Hyperactive,Intrusive,Oppositional,Pacing";
reslist += ",Preoccupied,Pushing limits,Reckless,Resistant to care,Rigid,Ritualistic";
reslist += ",Tearful,Staff seeking,Sexually inappropriate,Tics,Withdrawn";
            bool J3 = Exists("", "9993040009823", "", "", reslist);
            if (J3 && J2) SetInd(9, "Both J3 and J2 present.");

            SetIndIfResultContains(9, "", "9990000300001", "", "", "4 hours,2 hours,1 hour");

reslist = "1:1 Nsg care for seclusion for the first hour";
reslist += ",Constant Nsg care for restraints for the first hour";
reslist += ",Record behavior every 5 minutes";
reslist += ",Review strengths/comfort measures - assist pt in reaching goal for discontinuation";
reslist += ",Administration of medication - to help client regain previous level of functioning";
reslist += ",Respiratory status assessed/documented each check - to ensure adequate air exchange";
reslist += ",Restraints checked every 15 minutes - to prevent injury from cuffs constricting normal blood flow or irritation of skin";
reslist += ",RN hourly assessment - mental, behavior and respiratory status - to minimize length of the procedure";
reslist += ",Offer fluids every 2 hours - to provide elimination opportunities";
reslist += ",Offer toileting every 2 hours - to provide elimination opportunities";
reslist += ",ROM to joints every 2 hours - to prevent injury from occurring when client is in restraints";
reslist += ",Hygiene PRN - to provide comfort and support";
reslist += ",Meals and snacks offered - to maintain nutrition (document reason for not providing and meal or snack at regular times)";
reslist += ",Other (Comment)";
            SetIndIfResultContains(9, "", "9990007096219", "", "", reslist);

reslist = "Implemented - Seclusion Treatment Policy";
reslist += ",Implemented - Behavioral Restraint Policy";
reslist += ",Procedure explained to patient";
reslist += ",Reason for seclusion/restraints explained";
reslist += ",Informed the patient of the goal\\Mattress checked for dangerous items";
reslist += ",Mattress checked for dangerous items";
reslist += ",check room for lighting"+CHAR_COMMA+" temperature and safety";
reslist += ",Dangerous items and jewelry removed";
reslist += ",Respiratory status monitored";
reslist += ",Pt assisted in achieving goal for d/c";
            SetIndIfResultContains(9, "", "9990007096227", "", "", reslist);

            reslist = "Pillow,Memory box,Bereavement package,Other";
            SetIndIfResultContains(9, "", "9991020100509", "", "", reslist);

            reslist = "Baby bracelet,Blanket,Clay molds,Clothing,Complimentary birth certificate";
reslist += ",Crib card,Foot prints,Hand prints,Hat,Lock of hair,Photos";
reslist += ",Pin,Prism,Tape measure,Other";
            SetIndIfResultContains(9, "", "9991020100510", "", "", reslist);

            SetIndIfResultContains(9, "", "9991020100506", "", "", "Done");

        }

        private void Check_10()
        {
            string reslist = "";
            List<int> buckets;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Inpt 10. Fluid Management");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(10, "", "26", "", "", reslist);
            SetIndIfResultContains(10, "", "9990007070009", "", "", reslist);
            SetIndIfResultContains(10, "", "28", "", "", reslist);
            SetIndIfResultContains(10, "", "9993040100475", "", "", reslist);
            SetIndIfResultContains(10, "", "20", "", "", reslist);
            SetIndIfResultContains(10, "", "14", "", "", reslist);
            SetIndIfResultContains(10, "", "9993047073811", "", "", reslist);
            SetIndIfResultContains(10, "", "29", "", "", reslist);
            SetIndIfResultContains(10, "", "3040001333", "", "", reslist);
            SetIndIfResultContains(10, "", "3040001334", "", "", reslist);
            SetIndIfResultContains(10, "", "9990000305290", "", "", reslist);
            SetIndIfResultContains(10, "", "3045001090", "", "", reslist);
            SetIndIfResultContains(10, "", "3045001089", "", "", reslist);
            SetIndIfResultContains(10, "", "9990160000252", "", "", reslist);
            SetIndIfResultContains(10, "", "9990160000253", "", "", reslist);
            SetIndIfResultContains(10, "", "3043040100003", "", "", reslist);
            SetIndIfResultContains(10, "", "9993040100004", "", "", reslist);
            SetIndIfResultContains(10, "", "3043040101422", "", "", reslist);
            SetIndIfResultContains(10, "", "9993040101423", "", "", reslist);
            SetIndIfResultContains(10, "", "", "", "", reslist);



        }

        private void Check_1112()
        {
            string reslist;
            int ct;
            List<int> buckets;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Inpt 11. Physiologic Assessment q 1 hr");
            Program.VerboseAudit("ED Inpt 12. Physiologic Assessment q 30 min");
            Program.VerboseAudit("---------------");




SetIndIfResultContains(12,"","3045001046","","","Pharmacologically paralyzed");
SetIndIfResultContains(12,"","9990000016054","","","1,2");
SetIndIfResultContains(12,"","9991600100426","","","Level red,Level yellow");
SetIndIfResultContains(12,"","9990160100151","","","Level red,Level yellow");
SetIndIfResultContains(12,"","9991600100335","","","STEMI,Arrest,Medical resuscitation");

SetIndIfResultContains(12,"","3045001117","","","");
            reslist = "PACU Ventilator Rows,Trilogy Ventilator Rows,HFO Ventilator Rows";
reslist += ",BPAP/CPAP Rows,Abbreviated Settings,NAVA Rows";
SetIndIfResultContains(12,"","9993040000635","","",reslist);

            reslist = "Mask,Tubing,CPAP,BPAP";
reslist += ",Patient able to manage equipment on own";
reslist += ",Equipment inspected (per site policy)";
reslist += ",Waiver signed (per site policy),Other";
bool N11 = Exists("","9993040000639","","",reslist);
            if (!N11)
            {
            reslist = "Bilevel,CPAP,Auto-Bilevel,Auto-CPAP,AVAPS,PCV";
SetIndIfResultContains(12,"","3045001108","","",reslist);
            reslist = "Nasal pillows,Nasal mask,Nasal Prongs,Nasal pharyngeal";
reslist += ",Full face mask,Performax,Total face mask,Endotracheal,Tracheostomy";
SetIndIfResultContains(12,"","9993040000637","","",reslist);
            }
reslist = "Respiratory arrest,Cardiac arrest,Unknown,Other (Comments)";
SetIndIfResultContains(12,"","9991600100646","","",reslist);
reslist = "Supraventricular tachycardia,Ventricular tachycardia";
reslist += ",Atrial fibrillation w/rapid ventricular response,Other (Comment)";
SetIndIfResultContains(12,"","9991600100613","","",reslist);
reslist = "Sinus,Sinus bradycardia,Sinus tachycardia,Atrial paced,Ventricular paced";
reslist += ",Atrial and ventricular paced,Atrial fibrillation,Atrial fibrillation w/rapid ventricular response";
reslist += ",Atrial flutter,Heart block,Junctional,Supraventricular tachycardia,Torsades de Pointes";
reslist += ",Asystole,Pulseless electrical activity,Ventricular fibrillation,Ventricular tachycardia,Other (Comment)";
SetIndIfResultContains(12,"","9991600100614","","",reslist);
reslist = "Initiated,Settings Change,Discontinued";
SetIndIfResultContains(12,"","9991600100147","","",reslist);
reslist = "Reposition,Suction,Jaw thrust,Chin lift,Foreign object removal";
SetIndIfResultContains(12,"","9991010010010","","",reslist);
reslist = "Endotracheal tube,Esophageal - tracheal tube,Laryngeal mask airway (LMA)";
reslist += ",Nasopharyngeal airway (NPA),Oropharyngeal airway (OPA),Tracheostomy,Other (Comment)";
SetIndIfResultContains(12,"","9991600100681","","",reslist);
reslist = "Bag-valve-mask,Bag-valve-ET tube,Bag-valve-tracheostomy,Ventilator";
reslist += ",Bilevel positive airway pressure (BiPAP),Continuous positive airway pressure (CPAP)";
reslist += ",CPAP nasal,CPAP mask,Positive pressure ventilation (PPV),Other (Comments)";
SetIndIfResultContains(12,"","9991600100682","","",reslist);
reslist = "To Cardiac Cath. Lab,To Operating Room,Admitted,Transferred,Other (Comment)";
SetIndIfResultContains(12,"","9991601001082","","",reslist);
reslist = "Altered mental status,Change in level of consciousness,Stroke symptoms";
reslist += ",Chest pain,ST elevation,Dysrhythmia,Bradycardia,Tachycardia,Shortness of breath";
reslist += ",Bradypnea,Tachypnea,Hypoxemia,Hyperthermia or fever,Hypothermia,Hypotension";
reslist += ",Hypertension,Decreased MAP,Increased MAP,Increased FiO2,Acute bleeding";
reslist += ",Acute change in urine output,RN or MD Concern (Comments),Peer consultation";
reslist += ",Failure to respond to treatment,Other (Comments)";
SetIndIfResultContains(12,"","9991600100632","","",reslist);

SetIndIfResultContains(12,"","9991020100386","","","");

reslist = "None,Dystocia,Preterm labor,Abruptio placentae,Placenta previa,Placental retention";
reslist += ",Cord prolapse,Uterine rupture,Amniotic embolism,Other (Comment)";
SetIndIfResultContains(12,"","9990000012126","","",reslist);

reslist = "CPAP facial,CPAP gap present,CPAP nasal,CPAP vent,ETT nasal,ETT oral";
reslist += ",Incubator oxygen,Laryngeal mask airway,Nasal cannula gap present";
reslist += ",Nasal cannula high flow,Nasal cannula low flow";
SetIndIfResultContains(12,"","9991733888883","","",reslist);

SetIndIfResultContains(12,"","3045001036","","","");
SetIndIfResultContains(12,"","3045001034","","","");

SetIndIfResultContains(12,"","3045001038","","","1:1,1:2,1:3");
reslist = "Continuous veno-venous hemofiltration";
reslist += ",Continuous veno-venous hemodialysis";
reslist += ",Continuous veno-venous hemodiafiltration";
reslist += ",Slow continuous ultrafiltration";
SetIndIfResultContains(12, "", "9990008100010", "", "", reslist);
reslist = "Initiated,Continuous,Restarted,Off/System charge,Off/Recirculating";
reslist += ",Off/Blood returned,Off/No blood returned,Discontinued";
SetIndIfResultContains(12,"","9990008100020","","",reslist);

SetIndIfResultContains(12,"","9993041001004","","","");
SetIndIfResultContains(12,"","9993041001003","","","");



        }

        // (this isn't really a Q1h count -- it is just a count) Make this a q1hr count
        private int ReturnQ1HrCount(string code_list, string result_list)
        {
            var buckets = new List<int>();
            SetBucketSize(60);
            AddBuckets(buckets, "", code_list, "", "", result_list);
            return CountBuckets(buckets);
        }

        private bool IsQ1Hour(int count)
        {
            return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q1H);
        }
        private bool IsQ15(int count)
        {
            return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q15M);
        }

        // Use this if counting safety buckets
        //private void CheckSafety(int count, string desc)
        //{
        //    if (_inds[13].is_checked) return;             //skip if highest already checked
        //    if (count == 0) return;
        //    switch (FreqForCount(_pat.los_hours, count))
        //    {
        //    case Frequencies.Q30M:
        //        SetInd(13, desc + " q30min");
        //        break;
        //    case Frequencies.Q1H:
        //    case Frequencies.Q2H:
        //        SetInd(12, desc + " q2h");
        //        break;
        //    default:
        //        Program.VerboseAudit(desc + ": " + count + " in " + Math.Round(_pat.los_hours) + " hours is not enough");
        //        break;
        //    }
        //}

        private void Check_13()
        {
            string reslist;
            int ct = 0;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Inpt 13. Wound/Injury Management");
            Program.VerboseAudit("---------------");

            // COUNT OF MEDS > 5 goes here

            reslist = "4 Ounces fruit juice,8 ounces fruit juice,IV/Medication (See MAR)";
            ct += CountResultContains("", "9993040000345", "", "", reslist);
            ct += CountResultContains("", "9990000344170", "", "", "");
            ct += CountResultContains("", "3040007191", "", "", "");

            if (ct > 5) SetInd(13, "Count of Meds=" + ct);

        }

        //private void CheckAssessment(int count, string desc)
        //{
        //    if (_inds[18].is_checked) return;          //skip if highest already checked
        //    if (count == 0) return;                    //skip if none
            
        //    // This should work the same as the original code:
        //    switch (FreqForCount(_pat.los_hours, count)) {
        //        case Frequencies.Q30M:
        //            SetInd(18, desc);
        //            break;
        //        case Frequencies.Q1H:
        //            SetInd(17, desc);
        //            break;
        //        case Frequencies.Q2H:
        //            SetInd(16, desc);
        //            break;
        //        case Frequencies.Q4H:
        //            SetInd(15, desc);
        //            break;
        //        default:
        //            break;
        //    }

        //}

        private void Check_14()
        {
            string reslist = "";

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Inpt 14. Medication Management >= 5 Meds");
            Program.VerboseAudit("---------------");

            
            SetIndIfResultContains(14, "", "9993040000345", "", "", reslist);
            SetIndIfResultContains(14, "", "9990000344170", "", "", reslist);
            SetIndIfResultContains(14, "", "3040007191", "", "", reslist);

                        //LDA tab mappings  currently no codes

            reslist = "Injury/trauma,Traction (Describe)";
            SetIndIfResultContains(14, "", "9990000304110", "", "", reslist);
            reslist = "Injury/trauma,Traction (Describe)";
            SetIndIfResultContains(14, "", "9990000304130", "", "", reslist);
            SetIndIfResultContains(14, "", "9990304000122", "", "", "3");
            reslist = "Tea,Rusty,Peach,Cherry,Pink,Ketchup";
            SetIndIfResultContains(14, "", "9990000006298", "", "", reslist);
            
            SetIndIfResultContains(14, "", "9993040401216", "", "", "Peritoneal port");
            reslist = "Red,Pink,Pale,Dusky,Purple,Mottled,Ecchymotic,Soft,Firm";
            SetIndIfResultContains(14, "", "9993040001044", "", "", reslist);

            SetIndIfResultContains(14, "", "3040001333", "", "", "");
            SetIndIfResultContains(14, "", "3040001334", "", "", "");

            SetIndIfResultContains(14, "", "9993040101378", "", "", "Clean,No clot");
            SetIndIfResultContains(14, "", "9993040101377", "", "", "Done");
            SetIndIfResultContains(14, "", "9991020100022", "", "", "");
            reslist = "Vaginal pack,Uterine balloon,Uterotonic agent given (see MAR)";
            reslist += ",Oxygen on,Legs elevated,MD notified,Bimanual exam per MD,Labs ordered";
            SetIndIfResultContains(14, "", "9991020100568", "", "", reslist);
            reslist = "Placed,Present,Removed";
            SetIndIfResultContains(14, "", "9991020100569", "", "", reslist);
            reslist = "Applied (comment number),Changed  (comment number),Marked";
            reslist += ",Reinforced,Site care,Staples removed (comment number)";
            SetIndIfResultContains(14, "", "9991733888867", "", "", reslist);
            reslist = "Drainage,Malodorous,Red";
            SetIndIfResultContains(14, "", "9991733888850", "", "", reslist);
            reslist = "Dry,Moist,Cannulated,Clamp off,Clamp on,Care done,2 cord vessels,3 cord vessels";
            SetIndIfResultContains(14, "", "9991140100024", "", "", reslist);
            reslist = "Gauze in place,Petroleum jelly applied,Petroleum jelly gauze applied,Other";
            SetIndIfResultContains(14, "", "9991140100026", "", "", reslist);
            reslist = "Bleeding,Edematous,Necrotic,Pink,Reddened";
            reslist += ",Serosanguinous drainage,Serous drainage,Other (Comment)";
            SetIndIfResultContains(14, "", "9991140100027", "", "", reslist);

            SetIndIfResultContains(14, "", "9991601001106", "", "", "");
            SetIndIfResultContains(14, "", "9991601001122", "", "", "");
            reslist = "Finger aluminum,Finger guard aluminum,Finger padded aluminum";
reslist += ",Finger frog aluminum,Cock up wrist velcro,Ulnar gutter,Volar";
reslist += ",Arm aluminum,Short arm,Long arm,Short arm fiberglass,Long arm fiberglass";
reslist += ",Sugar tong,Double sugar tong- arm,Short leg,Long leg,Short leg fiberglass";
reslist += ",Long leg fiberglass,Sugar tong leg,Robert Jones,Modified Robert Jones";
reslist += ",Air cast stirrup,Posterior ankle,Cadillac (stirrup/posterior combo),Other (Comment)";
            SetIndIfResultContains(14, "", "9991600100112", "", "", reslist);
            reslist = "Boxer,Colles,Cylinder,Short arm,Long arm,Long arm hanging";
reslist += ",Short arm fiberglass,Long arm fiberglass,Navicular fiberglass";
reslist += ",Short arm navicular,Long arm navicular,Short leg,Long leg,Short leg fiberglass";
reslist += ",Long leg fiberglass,Cylinder fiberglass,Patella with bearing,Spica single";
reslist += ",Spica double,Spica single fiberglass,Spica double fiberglass";
reslist += ",Reinforce"+CHAR_COMMA+" minor,Reinforce"+CHAR_COMMA+" major,Reinforced fiberglass,Monovalve";
reslist += ",BiValve,Window,Window by tech,Other (Comment)";
            SetIndIfResultContains(14, "", "9991600100115", "", "", reslist);
            reslist = "Boot,Shoe,Post-op,Walking,Cam walker,Air Cast,Other (Comment)";
            SetIndIfResultContains(14, "", "9991600100117", "", "", reslist);
            reslist = "Shoulder immobilizer,Walk wraps,Humeral fracture brace";
reslist += ",Knee immobilizer,Neoprene knee brace,Other (Comment)";
            SetIndIfResultContains(14, "", "9991600100118", "", "", reslist);
reslist = "Abductor pillow,Arm elevation pillow,Elastic bandage, 2 inch";
reslist += ",Elastic bandage"+CHAR_COMMA+" 3 inch,Elastic bandage"+CHAR_COMMA+" 4 inch,Arm sling";
reslist += ",Rib belt,Clavicle shoulder strap,Dressing"+CHAR_COMMA+" soft wrap knee";
reslist += ",Other (Comment),Elastic Wrist";
            SetIndIfResultContains(14, "", "9991600100119", "", "", reslist);
            reslist = "Complaining of back injury,Complaining of head/neck injury";
reslist += ",Fall,Injury,MVC,Sporting accident,UTA=Unable to assess,Other (Comments)";
            SetIndIfResultContains(14, "", "9991600100122", "", "", reslist);
            reslist = "Rigid cervical collar,Backboard,Soft cervical collar";
reslist += ",Head blocks or towels,Cervical immobilization device,Other (Comment)";
            SetIndIfResultContains(14, "", "9991600100123", "", "", reslist);
            reslist = "Skeletal,Skin,Cervical,Other";
            SetIndIfResultContains(14, "", "9991600000289", "", "", reslist);
            reslist = "Suture repair,Wound care,Fracture care,Joint immobilization,Joint reduction";
            SetIndIfResultContains(14, "", "9990000008445", "", "", reslist);

 //DATE WITHIN 5 DAYS SetIndIfResultContains(14, "", "9991600100348", "", "", date);
// need to know format of date in RESULT

            reslist = "Level red,Level yellow,Non-activation (Green)";
            SetIndIfResultContains(14, "", "9991600100426", "", "", reslist);
            SetIndIfResultContains(14, "", "9990160100151", "", "", reslist);
            reslist = "Trauma/Injury";
            SetIndIfResultContains(14, "", "9990000304900", "", "", reslist);
            SetIndIfResultContains(14, "", "9990000002106", "", "", reslist);
            SetIndIfResultContains(14, "", "9990000002107", "", "", reslist);
            SetIndIfResultContains(14, "", "9990000002108", "", "", reslist);
            SetIndIfResultContains(14, "", "9990000002109", "", "", reslist);
            SetIndIfResultContains(14, "", "9993040108746", "", "", reslist);
            SetIndIfResultContains(14, "", "9990000304930", "", "", reslist);
            SetIndIfResultContains(14, "", "9990000002117", "", "", reslist);
            SetIndIfResultContains(14, "", "9990000304940", "", "", reslist);

            reslist = "Ulcerations present";
            SetIndIfResultContains(14, "", "9990000002113", "", "", reslist);

            reslist = "Injury/Trauma";
            SetIndIfResultContains(14, "", "9990000304100", "", "", reslist);
            SetIndIfResultContains(14, "", "9990000304120", "", "", reslist);
            SetIndIfResultContains(14, "", "9990000303930", "", "", reslist);
            SetIndIfResultContains(14, "", "9990000303970", "", "", reslist);
            SetIndIfResultContains(14, "", "9990000304010", "", "", reslist);
            SetIndIfResultContains(14, "", "9990000304050", "", "", reslist);
            SetIndIfResultContains(14, "", "9990000304090", "", "", reslist);
            SetIndIfResultContains(14, "", "9990000450250", "", "", reslist);
            SetIndIfResultContains(14, "", "9990000450260", "", "", reslist);
            SetIndIfResultContains(14, "", "9990000304190", "", "", reslist);

        }


        private void Check_151617()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Inpt 15. Admitted");
            Program.VerboseAudit("ED Inpt 16. Transferred to another Facility");
            Program.VerboseAudit("ED Inpt 17. Expired");
            Program.VerboseAudit("---------------");


        }

        //private bool IsICU()
        //{
        //    switch (_pat.unit_name)
        //    {
        //        case "ICU":
        //        case "3BR":
        //        case "CTIC":
        //        case "NIC":
        //        case "PCU":
        //        case "TICU":
        //        case "CCB":
        //        case "RICU":
        //        case "CCUS":
        //            return true;
        //        default:
        //            return false;
        //    }
        //}

        //private bool IsTele()
        //{
        //    switch (_pat.unit_name)
        //    {
        //        case "4EST":
        //        case "2NW":
        //        case "3AE":
        //        case "5MEH":
        //        case "5NW":
        //        case "6MEH":
        //        case "6NW":
        //        case "7MEH":
        //        case "B4W":
        //        case "B5S":
        //        case "B5W":
        //        case "2NOB":
        //        case "CPLX":
        //        case "TELMS":
        //        case "MAN4":
        //            return true;
        //        default:
        //            return false;
        //    }
        //}


        //private void CountAssessments(int bucket_size)
        //{
        //    int ct;
        //    string codelist;
        //    string reslist;
        //    List<int> buckets;

        //    SetBucketSize(bucket_size);

        //    buckets = new List<int>();

        //    codelist = "97036712,118064,118066,118058,118132,305991507,118520,14305,142506594";
        //    codelist += ",12293217,118193,118068,118011,118146,306182838,306182805,305991322";
        //    codelist += ",118498,29718286,306327425,131172918,353080778";
        //    AddBuckets(buckets, "", codelist, "", "");
        //    reslist = "APD - All Purpose Drain (AZ),Biliary,Blake,CAPD,Drain,Constavac,Davol,G-J Tube,G-Tube,Hemovac";
        //    reslist += ",JP,J-Tube,PEG tube,Pelvic Drain,Pendrose Drain,Pericardial,T-Tube,Other";
        //    AddBuckets(buckets, "", "305142088", "", "", reslist);
        //    codelist = "305433923,633686082,305433661,481751506,481752199,304541034,316381370,305434227";
        //    codelist += ",316380803,119127,119141,304541048,7847308,18674661,306327593,7849724";
        //    codelist += ",7849861,7849891,7851959,18674834,7852147,12293217,30600122,7852257";
        //    codelist += ",306001049,7853949,7931900,7852980,7852983,7853157,7853190,7931902";
        //    codelist += ",7853207,7903597,7853235,7903643,7853418,7853426,7853430,7866522";
        //    codelist += ",18674837,7866633,14197072,7932422";
        //    AddBuckets(buckets, "", codelist, "", "");
        //    AddBuckets(buckets, "", "7868669", "", "", "Endotracheal,Nasal,Nasal Tracheal Suction,Oral Suction,Tracheal Suction");
        //    codelist = "7870437,7904435,18674857,7932312,18674862,7907957,18674869";
        //    codelist += ",303031350,325754541,303031329,532319071,345591779,304015183";
        //    codelist += ",345603431,303722796,303722810,303722826,303722841,303722857";
        //    codelist += ",303722871,303722885,303722900,790715,305433789,383694315";
        //    codelist += ",383695492,303722345,303693371,303722782";
        //    AddBuckets(buckets, "", codelist, "", "");
            
        //    ct = CountBuckets(buckets);
        //    CheckAssessment(ct, "All assessments count=" + ct);
        //    if (_inds[18].is_checked) return;


        //    //buckets = new List<int>();
        //    //codelist = "MH_ORD_Respiratory";
        //    //AddBuckets(buckets, "", codelist, "", "");
        //    //ct = CountBuckets(buckets);
        //    //CheckAssessment(ct, "Pulmonary=" + ct);
        //    //if (_inds[18].is_checked) return;

        //    //buckets = new List<int>();
        //    //codelist = "MH_ORD_VitalSigns";
        //    //AddBuckets(buckets, "", codelist, "", "");
        //    //ct = CountBuckets(buckets);
        //    //CheckAssessment(ct, "VS=" + ct);
        //    //if (_inds[18].is_checked) return;

        //    //buckets = new List<int>();
        //    //codelist = "CARDRHYTHM";
        //    //AddBuckets(buckets, "", codelist, "", "");
        //    //ct = CountBuckets(buckets);
        //    //CheckAssessment(ct, "CardRhythm=" + ct);
        //    //if (_inds[18].is_checked) return;

        //    //buckets = new List<int>();
        //    //codelist = "A_GlasgowComaScl,SC_S_ASMT_FregNeuroChks,A_MHPNeuroTube";
        //    //AddBuckets(buckets, "", codelist, "", "");
        //    //ct = CountBuckets(buckets);
        //    //CheckAssessment(ct, "Neuro=" + ct);
        //    //if (_inds[18].is_checked) return;

        //    //buckets = new List<int>();
        //    //codelist = "A_MHMPain,A_MHPain,A_Pain";
        //    //AddBuckets(buckets, "", codelist, "", "");
        //    //ct = CountBuckets(buckets);
        //    //CheckAssessment(ct, "Pain=" + ct);
        //    //if (_inds[18].is_checked) return;


        //}

        //=====================================================================
        // Group like assessments into buckets of time
        // Each bucket counts as one assessment in the frequency count

        private void SetBucketSize(int minutes)
        {
            _bucket_size = minutes;
        }

        //Add to the list of bucket numbers (redundant buckets are fine)
        private void AddBuckets(List<int> bucket_list, string cat, string code_list, string desc, string field)
        {
            AddBuckets(bucket_list, cat, code_list, desc, field, "");
        }
        private void AddBuckets(List<int> bucket_list, string cat, string code_list, string desc, string field, string result_list)
        {
            // Assign (virtual) bucket numbers to each assessment.  
            // Every x minutes after that is a new bucket.  Bucket zero = pull_start time.

            // get the chart items for the assessments
            var query = StartNewQuery(SearchDepth.SearchPullPlus);
            query = AndItemFilter(query, cat, code_list, desc, field, result_list);

            // This step is needed for those who want to count the # within a bucket, like Shands #24.
            // This will have no effect on those that count buckets.
            var query2 = (from item in query select new { item.EVENT_DATETIME }).Distinct();

            // figure out what buckets the events belong to
            var query3 = from item in query2
                         select new {
                            bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size)
                         };
            // Add to the list
            foreach (var item in query3) {
                bucket_list.Add(item.bucket);
            }

            // This is just for auditing: Say what we are looking for and how many were found
            if (!String.IsNullOrEmpty(result_list)) {
                // print each word and if it was found or not
                int i = CountResultContains(cat, code_list, desc, field, result_list);
            } else {
                // print how many were found
                Program.VerboseAudit(Describe(cat, code_list, desc, field, ""));
            }
        }

        private int CountBuckets(List<int> bucket_list)       // bucket list: (ha ha)
        {
            int result = bucket_list.Distinct().Count();
            if (result > 0) Program.VerboseAudit(result + " unique");
            return result;
        }


        private void AtLeastOneADL()
        {
            Program.Audit("---------------");
            if (!(_inds[2].is_checked || _inds[3].is_checked || _inds[4].is_checked)) {
                // Make "#2 ADL - Assist" the default.  (90% of patients)
                SetInd(2, "Defaulting to ADL Self due to lack of documentation.");
            }
        }

        private void HighestIndicatorInEachGroupWins()
        {
            int i, g;
            bool highest_is_on;
            string ind_list;

            Program.Audit("Select highest indicator in each group");
            
            g = 0;
            highest_is_on = false;
            // Go from bottom up - highest (lowest) indicator in each group wins
            for (i = MAX_INDS; (i >= 1); i--) {
                if (_inds[i].radio_group > 0) {
                    if (_inds[i].radio_group != g) {
                        //this is a new group
                        g = _inds[i].radio_group;
                        highest_is_on = _inds[i].is_checked;
                    } else {
                        //same group
                        if (highest_is_on) {
                            _inds[i].is_checked = false;             //uncheck a lower number
                        } else {
                            highest_is_on = _inds[i].is_checked;     //save this one
                        }
                    }
                }
            } // next i
            
            //Echo the indicators for an Audit (no classification will be saved)
            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++) {
                if (_inds[i].is_checked) ind_list += "," + i;
            } // next i

            Program.Audit("Final list = " + ind_list.Substring(1));
            Program.Audit("---------------");
        }


        private void CheckProcs()
        {
            //CheckProc_1();
            //CheckProc_2();
            //CheckProc_3();
            //CheckProc_4();
            //CheckProc_5();
            //CheckProc_6();
            //CheckProc_7();
            //CheckProc_8();
            //CheckProc_9();
            //CheckProc_10();
            //CheckProc_11();

        }

        private void DoProc(int pnum, string code)
        {
            double mins = 0;
            string found_what;
            DateTime evdt;
            DateTime enddt = DateTime.MinValue;

            if (GetResultAndEVDT("", code, "", "", out found_what, out evdt))
            {
                mins = 60.0 * found_what.ToDouble();
                enddt = evdt.AddMinutes(mins);

                if (ProcExistsInDB(pnum, evdt, enddt))
                {
                    Program.Audit("Procedure " + pnum+ ": already exists");
                }
                else
                {
                    if (!QueuedProcOverlaps(pnum, evdt, enddt))
                    {
                        var proc = new proc_data();
                        proc.procedure_number = pnum;
                        proc.start = evdt;
                        proc.finish = enddt;
                        _procs.Add(proc);
                        Program.Audit("Procedure " + pnum + ": Found " + code + " between " + evdt + " and " + enddt);
                    }
                }

            }

        }

        private bool OnlyHasED()
        {
            var db = PFSUtility.NewPfsDataContext();
            var query = from el in db.ENCOUNTER_LOCATIONs
                        join u in db.UNITs on el.UNIT_ID equals u.UNIT_ID
                        where (el.ENCOUNTER_ID == _pat.encounter_id)
                        where (u.IS_ED.ToString().ToUpper() == "N")
                        select new { u.NAME };
            return (query.Count() == 0);
        }

        private bool ProcExistsInDB(int pnum, DateTime startdt, DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && (proc.PROCEDURE_DATETIME == startdt)
                            && (proc.DEPARTURE_DATETIME == enddt)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new { proc.PROCEDURE_EVENT_ID };
            return (query.Count() > 0);
        }

        private bool QueuedProcOverlaps(int pnum, DateTime startdt, DateTime enddt)
        {
            bool overlap = false;

            proc_data[] pary = _procs.ToArray();
            for (int i = 0; i <= pary.GetUpperBound(0); i++)
            {
                if (pary[i].procedure_number == pnum)
                {
                    //overlap if   p.start between stardt and enddt
                    //             p.finish between startdt and enddt
                    //             startdt >= p.start and enddt <= p.finish
                    if ((pary[i].start >= startdt) && (pary[i].start < enddt) ||
                        (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
                        (startdt >= pary[i].start) && (enddt <= pary[i].finish))
                    {
                        overlap = true;
                    }

                    if ((pary[i].start >= startdt) && (pary[i].start < enddt) && (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
                        (pary[i].start < startdt) && (pary[i].finish > startdt) && (pary[i].finish < enddt)
                        )
                    {
                        // the proc in the list is completely contained in startdt/endt pair
                        // change this proc to be the startdt/enddt
                        // overlap is still true, but the list item will be updated with the encompassing times
                        //Program.Audit("Procedure " + pnum + ": " + startdt + " - " + enddt + " will trump " + pary[i].start + " - " + pary[i].finish);
                        pary[i].start = startdt;
                        pary[i].finish = enddt;
                    }
                }
            }
            _procs = pary.ToList();
            return overlap;
        }


        private void CheckProc_1()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P1. 1-1 safety observation by RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_2()
        {
            string nowstr;
            string toddtstr;
            string yesdtstr;
            string timea ="";
            DateTime timea_startdt, timea_enddt;
            DateTime timeb_startdt, timeb_enddt;
            DateTime nowdt = _pat.pull_finish;              // "now" is pull time
            DateTime yesdt;
            
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P2. 1-1 safety observation by non-RN");
            Program.VerboseAudit("---------------");

            nowstr = nowdt.ToString("yyyyMMddHHmm");
            yesdt = nowdt.AddDays(-1);
            toddtstr = nowdt.ToString("yyyyMMdd");
            yesdtstr = yesdt.ToString("yyyyMMdd");
            
            //when is now? (yesterday/today)
            //yes 7am -- yes 7p  -- tod 7a -- tod 7p
            //                                   A                   B
            //if nowdt >= tod7pm then check  tod 7am-tod 7pm and tod 7pm-tom 7a
            //if nowdt >= tod7am then check  yes 7p - tod 7a     tod 7a-tod 7p
            //if nowdt >= yes7pm then check  yes 7a-yes 7p       yes 7p-tod 7a
            if (nowstr.CompareTo(toddtstr + "1900") >= 0) {
                timea = toddtstr + "0700";
            } else if (nowstr.CompareTo(toddtstr + "0700") >= 0) {
                timea = yesdtstr + "1900";
            } else if (nowstr.CompareTo(yesdtstr + "1900") >= 0) {
                timea = yesdtstr + "0700";
            }
            
            timea_startdt = PFSUtility.ISOToDateTime(timea);
            timea_enddt = timea_startdt.AddHours(12);
            MaybeAddSitter(timea_startdt, timea_enddt);

            timeb_startdt = timea_enddt;
            timeb_enddt = timeb_startdt.AddHours(12);
            MaybeAddSitter(timeb_startdt, timeb_enddt);
        }

        private void MaybeAddSitter(DateTime startdt, DateTime enddt)
        {
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);    // add custom time range below
            query = query.Where(e => (e.EVENT_DATETIME >= startdt) && (e.EVENT_DATETIME < enddt));
            query = AndCodeInList(query, "Sitter");
            query = AndResultInList(query,"continued, initiated");
            query = AndResultNotInList(query, "discontinued");

            if (query.Count() > 0) {
                if (ProcExists(2, startdt, enddt)) {
                    Program.Audit("Procedure 2: already exists");
                } else {
                    var proc = new proc_data();
                    proc.procedure_number = 2;
                    proc.start = startdt;
                    proc.finish = enddt;
                    _procs.Add(proc);
                    Program.Audit("Procedure 2: Found Sitter between " + startdt + " and " + enddt);
                }
            }
            
        }

        private bool ProcExists(int pnum, DateTime startdt, DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && (proc.PROCEDURE_DATETIME == startdt)
                            && (proc.DEPARTURE_DATETIME == enddt)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new {proc.PROCEDURE_EVENT_ID};
            return (query.Count() > 0);
        }


        private void CheckProc_3()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P3. Off unit accompanied by RN");
            Program.VerboseAudit("---------------");
            DoProc(3, "A_MHAcuOffUnit");
        }

        private void CheckProc_4()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P4. Off unit accompanied by non-RN");
            Program.VerboseAudit("---------------");
            DoProc(4, "A_MHAcuOffUNonRN");
        }

        private void CheckProc_5()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P5. Patient/family education by RN");
            Program.VerboseAudit("---------------");
            DoProc(5, "A_MHAcuPtFamEduc");
        }

        private void CheckProc_6()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P6. Extensive wound management by RN");
            Program.VerboseAudit("---------------");
            DoProc(6, "A_MHAcuExtensive");
        }

        private void CheckProc_7()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P7. Extensive wound management by non-RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_8()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P8. Coordination of care by RN");
            Program.VerboseAudit("---------------");
            DoProc(8, "A_MHAcuCoordinat");
        }

        private void CheckProc_9()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P9 1:1 RN at bedside");
            Program.VerboseAudit("---------------");
            DoProc(9, "A_MHAcu1:1byURN");
        }

        private void CheckProc_10()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P10. 1:1 non-RN at bedside");
            Program.VerboseAudit("---------------");
            DoProc(10, "A_MhAcu1:1UNonRN");
        }

        private void CheckProc_11()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P11. 2:1 by RN at bedside");
            Program.VerboseAudit("---------------");
            DoProc(11, "A_MHAcu2:1by URN");
        }

        private void CheckOutcomes()
        {
            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = AndItemFilter(query, "", "A_MHPostFalltime", "", "", "");
            foreach (var ch in query)
            {
                var outc = new proc_data();
                outc.procedure_number = 1;
                outc.start = ch.EVENT_DATETIME;
                _outcomes.Add(outc);
                Program.Audit("Outcomes 1: Found A_MHPostFalltime at " + outc.start);
            }
        }
        

        private const string DATETIME_FORMAT = "yyyyMMddHHmm";              // ISO Date/Time w/o seconds

        private void OutputClass()
        {
            string outstr, ind_list, desc, str_pull_dt;
            int i, tc_event_id;

            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         212 242 304
            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1
            //1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901 3456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
            //1       |10 5            |                |                |        |10023121779         |ALEXANDER                       |GLORIA                          |                                |0508    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289530  |           |201403030400                                                                      |NNYNNNNYYNNYNNNYNNNYYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//1       |10 5            |                |                |        |10024544908         |ARMSTRONG                       |AARON                           |                                |0535    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289531  |           |201403030400                                                                      |NYNNYNNNNNNYNNNYNNNNYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//|0547    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289532  |           |201403030400                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//|0547    |A   |201403030700|                |201403030700    |18  |C|    |3040|1440|18289532  |           |201403030700                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN

            if (Program.g_is_test)
                tc_event_id = 9999;
            else
                tc_event_id = PFSUtility.NextGID();                         //get a unique id for this class
//            str_pull_dt = _pat.pull_finish.ToString(DATETIME_FORMAT);
            str_pull_dt = _pat.unit_arrival.ToString(DATETIME_FORMAT);

            outstr = _pat.facilty_code.FixedWidth(8);                       //(facility code)
            outstr += "|" + "ED Visit Subunit".FixedWidth(16); // _pat.unit_name.FixedWidth(16);
            outstr += "|" + "".FixedWidth(16);                               //(unit code)
            outstr += "|" + "".FixedWidth(16);                               //(area code)
            outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
            outstr += "|" + _pat.acct.FixedWidth(20);
            outstr += "|" + _pat.last_name.FixedWidth(32);
            outstr += "|" + _pat.first_name.FixedWidth(32);
            outstr += "|" + _pat.middle_name.FixedWidth(32);
            outstr += "|" + _pat.room.FixedWidth(8);
            outstr += "|" + _pat.bed.FixedWidth(4);
          outstr += "|" + str_pull_dt.FixedWidth(12);                      //class datetime (could change)
            outstr += "|" + "".FixedWidth(16);                               //(login)
          outstr += "|" + str_pull_dt.FixedWidth(16);                      //(employee)/(pull datetime)
            outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
            outstr += "|" + "C".FixedWidth(1);                               //record type = class
            outstr += "|" + "".FixedWidth(4);                                //(stage)
            outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
            outstr += "|" + _pat.range.ToString().FixedWidth(4);             //TC pull range
            outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
            outstr += "|";
            outstr = outstr.FixedWidth(294);
          outstr += "|" + _pat.unit_arrival.ToString(DATETIME_FORMAT);        //IN
            outstr = outstr.FixedWidth(377);
            outstr += "|";
            
            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++) {
                if (_inds[i].is_checked) {
                    outstr += "Y";
                    ind_list += "," + i;
                } else {
                    outstr += "N";
                }
            } // next i
            ind_list = ind_list.Substring(1);                           //strip leading comma
  
            Program.outfile.WriteLine(outstr);                          //output to transparent.txt

            Program.Audit("");
            desc = "Classified: " + ind_list;
            if (Program.g_is_test) {
                Program.Audit(desc);
            } else {
                //Save the selected indicators plus both Program.Audits in the event log; link with tc_event_id
                PFSEventLog.AddTransparentMappingEventLogEntry(
                    desc, Program.gLogUnitID, Program.gLogEncounterID,
                    tc_event_id, Program.gLogMapperVersion,
                    Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
            }
        }

        //private void OutputProcs()
        //{
        //    int i;
        //    string outstr, proc_list, desc;
        //    int tc_event_id;

        //    foreach(var proc in _procs) {
        //        if (Program.g_is_test)
        //            tc_event_id = 9999;
        //        else
        //            tc_event_id = PFSUtility.NextGID();                         //get a unique id for this proc

        //        outstr = _pat.facilty_code.FixedWidth(8);
        //        outstr += "|" + _pat.unit_name;                                 //10
        //        outstr = outstr.FixedWidth(68);
        //        outstr += "|" + _pat.acct.FixedWidth(20);                       //90
        //        outstr += "|" + _pat.last_name.FixedWidth(32);
        //        outstr += "|" + _pat.first_name.FixedWidth(32);
        //        outstr += "|" + _pat.middle_name.FixedWidth(32);
        //        outstr = outstr.FixedWidth(202);
        //        outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //204 proc dt
        //        outstr = outstr.FixedWidth(254);
        //        outstr += "|P";                                                 //256 procedure type record
        //        outstr += "|" + "".FixedWidth(4);                               //(stage)
        //        outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);     //TC source ID
        //        outstr += "|" + _pat.range.ToString().FixedWidth(4);            //TC pull range
        //        outstr += "|" + tc_event_id.ToString().FixedWidth(10);          //TC event ID
        //        outstr += "|";
        //        outstr = outstr.FixedWidth(294);
        //        outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //296 procdt in
        //        outstr = outstr.FixedWidth(346);
        //        outstr += "|" + proc.finish.ToString(DATETIME_FORMAT);          //348 procdt out
        //        outstr = outstr.FixedWidth(377);
        //        outstr += "|";
                
        //        proc_list = "";
        //        for (i = 1; (i < MAX_PROCS); i++) {
        //            if (proc.procedure_number == i) {
        //                outstr += "Y";
        //                proc_list += "," + i;
        //            } else {
        //                outstr += "N";
        //            }
        //        } // next i
        //        proc_list = proc_list.Substring(1);                             //strip leading comma

        //        Program.outfile.WriteLine(outstr);                              //output to transparent.txt

        //        desc = "Procedures: " + proc_list;
        //        if (Program.g_is_test) {
        //            Program.Audit(desc);
        //        } else {
        //            //Save the selected procedures plus both Program.Audits in the event log; link with tc_event_id
        //            //(this assumes the procedure Program.Audit is mixed in with the class Program.Audit)
        //            PFSEventLog.AddTransparentMappingEventLogEntry(desc, Program.gLogUnitID, Program.gLogEncounterID,
        //                tc_event_id, Program.gLogMapperVersion,
        //                Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
        //        }
        //    } // next proc
        //}

    }
}
