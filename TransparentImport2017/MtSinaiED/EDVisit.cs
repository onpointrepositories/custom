﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;                     // for LINQ; add a reference
using System.Data.SqlClient;                // for SqlConnection
using System.IO;                            // for Directory, Path, Stream
using PfsShared;                            // add a reference to Shared2 project

// ============================================================================
// ED Visit transparent mapping -- GOES HERE --
// Mt Sinai (from Mayo)
// ============================================================================
// This processes one patient.  Only one database query is used.  :)
//
// All search functions use exact match for category, description and field name.
// Codes and results can be a single word or a list of words.
// Codes default to exact match; prefix with CODE_CODE_LIKE_PREFIX if desired (single word only)
// Results default to like matching; prefix with RESULT_EXACT_MATCH_PREFIX if desired (single word only)
//
// All searches are case insensitive.
//
//
namespace TransparentMapping
{
    class EDVisit
    {
        private const int MAX_INDS = 50;
        private const int MAX_PROCS = 20;
        private const int MAX_BUCKETS = 2880;

        private const string CODE_LIKE_PREFIX = "%!";       // use to make codes %like%
        private const string EXACT_MATCH_PREFIX = "&!";     // use to make desc/result exact match
        private const string CHAR_COMMA = "||";             // use to insert literal comma in word list
        private const string NOT_PREFIX = "!!";             // use to insert literal comma in word list
        private const string EXACT_SQL_PREFIX = "!SQL!";     // use to give sql constraint as given
        private const string AVOID_NEGATIVE = "!;";
        private const string STARTS_WITH = "[!";     // use to give sql constraint as given

        private struct indicator_data {
            public bool is_checked;
            public int radio_group;
        }

        private struct proc_data {
            public int procedure_number;
            public DateTime start;
            public DateTime finish;
        }

        // These are database CHART_ITEMs for this patient
        private CHART_ITEM[] _chart_items_since_admission;
        private CHART_ITEM[] _chart_items_since_unit_arrival;
        private CHART_ITEM[] _chart_items_during_pull_period;
        private CHART_ITEM[] _chart_items_pull_period_plus;
        // These are database PROCEDURE_EVENTs for this patient
        private PROCEDURE_EVENT[] _procedure_events;

        private indicator_data[] _inds;
        private List<proc_data> _procs;
        private List<proc_data> _outcomes;

        private const int DEFAULT_BUCKET_SIZE = 20;       //min

        private PatientInfo _pat;
        private int _bucket_size = DEFAULT_BUCKET_SIZE;
        private bool is_default;
        private bool isEDonly = false;
        private bool _trauma_triage_was_set = false;
        private string char183 = "" + (char)183;
        private string char9 = "" + (char)9;
        private bool esi_trauma_1 = false;
        private bool esi_trauma_2 = false;
         //   if (_trauma_triage1_was_set)

        private enum SearchDepth {
            SearchDefault,
            SearchPullRange,            //search within the current pull     -- default
            SearchSinceArrival,         //search since arrival to the unit
            SearchSinceAdmission,        //search everything since admission to the hospital
            SearchPullPlus
        }

        private enum CountMode {
            CountAll,
            CountFirst                  //stop after one is found
        }

        private enum GetValueMode {
            GetTotal,
            GetMax,
            GetLast
        }

        enum Frequencies {
            QNONE,
            //Q4H,
            //Q2H,
            Q1H,
            //Q30M,
            Q15M
        }

        private struct fmapRow {
            public double los_high;                       //the LOS being testing
            public int[] freq;                           //the count required for each Q value
        }
        private List<fmapRow> _freq_map;                    //1,2,4,8,12,24 hours

        private struct gBucket
        {
            public int bucket;
            public string code;
            public DateTime evdt;
            public bool using_waiver;
        }


        //
        // This is the main entry point
        //
        public void ProcessPatient(PatientInfo pat)
        {
            _pat = pat;
            InitIndicators(); // sets is_default
            InitProcs();
            isEDonly = OnlyHasED();
            if (!is_default)
            {
                LoadFreqTable();
                LoadPatientChart();
                Check_1();
                Check_234();
                Check_5();
                Check_67();
                Check_89();
                Check_10();
                Check_1112();
                Check_13();
                Check_14();
                Check_15();
                Check_16();
                Check_17();
                Check_18();
                Check_192021();
            }

            HighestIndicatorInEachGroupWins();

            //if (!is_default)
            //{
            //    CheckProcs();
            //    CheckOutcomes();
            //}

            if (Program.g_no_output) return;
            OutputClass();
            //OutputProcs();
            //OutputOutcomes();
        }


        private void InitIndicators()
        {
            // Make a new (empty) indicator array
            _inds = new indicator_data[MAX_INDS + 1];           // This 1 based so add one
            is_default = false;
            //if (_pat.los_hours <= 4.0) {
            //    is_default = true;
            //    Program.VerboseAudit("Patient was here 4 hrs or less. Will receive default indicators " + _pat.default_inds_str);
            //    foreach (var ind in _pat.default_inds) {
            //        if (ind <= _inds.GetUpperBound(0)) {
            //            _inds[ind].is_checked = true;
            //        }
            //    }
            //}


            // get indicator radio groups from the database
            // ** (This database access can be replaced once we have a C# methodolgy cache)
            var db = PFSDBUtility.NewPfsDataContext();
            var query = from idef in db.INDICATOR_DEFINITIONs
                        where (idef.METHODOLOGY_ID == _pat.meth_id)
                        select idef;
            foreach (var idef in query) {
                if (idef.INDICATOR_NUMBER <= _inds.GetUpperBound(0)) {
                    // (convert nulls to zero)
                    _inds[idef.INDICATOR_NUMBER].radio_group = PFSDBUtility.DBToInt(idef.RADIO_GROUP);
                }
            }
        }

        private void InitProcs()
        {
            _procs = new List<proc_data>();
            _outcomes = new List<proc_data>();
        }

        private fmapRow LoadFreqTableRow(double los_high, string values)
        {
            fmapRow fmrow;

            fmrow.los_high = los_high;
            fmrow.freq = new int[Enum.GetNames(typeof(Frequencies)).Length];

            var arr = values.Split(',');
            for (int i = 0; i <= fmrow.freq.GetUpperBound(0); i++)
            {
                fmrow.freq[i] = arr[i].ToInteger();
            }
            return fmrow;
        }

        // These are the number of times you need to do something during a certain time period (LOS)
        // in order for it to be considered one of the frequencies listed.
        private void LoadFreqTable()
        {
            _freq_map = new List<fmapRow>();
            //                              LOS,  None Q4h Q2h Q1h Q15m
            //_freq_map.Add(LoadFreqTableRow(1, "    0,  0,  0,  1,  2"));
            //_freq_map.Add(LoadFreqTableRow(2, "    0,  0,  1,  1,  4"));
            //_freq_map.Add(LoadFreqTableRow(4, "    0,  1,  2,  2,  8"));
            //_freq_map.Add(LoadFreqTableRow(6, "    0,  1,  2,  3, 12"));
            //_freq_map.Add(LoadFreqTableRow(8, "    0,  1,  2,  4, 16"));
            //_freq_map.Add(LoadFreqTableRow(12, "   0,  2,  4,  6, 24"));
            //_freq_map.Add(LoadFreqTableRow(16, "   0,  3,  5,  8, 32"));
            //_freq_map.Add(LoadFreqTableRow(24, "   0,  4,  8, 12, 48"));
            //_freq_map.Add(LoadFreqTableRow(36, "   0,  4,  8, 18, 72"));
            //_freq_map.Add(LoadFreqTableRow(48, "   0,  4,  8, 24, 96"));
            //New freq table 2/5/14
            //q4	q2	q1	q30     q30
            //            Non-ICU	ICU & SD
            // 4	8	15	29	    36
            // 3	5	9	17	    24
            // 2	4	7	13	    19
            // 2	3	5	10	    13
            //                                         q1H q15min
            _freq_map.Add(LoadFreqTableRow(1, "   0,  1, 3"));
            _freq_map.Add(LoadFreqTableRow(3, "   0,  2, 6"));
            _freq_map.Add(LoadFreqTableRow(6, "   0,  3, 12"));
            _freq_map.Add(LoadFreqTableRow(12, "   0,  6, 24"));
            _freq_map.Add(LoadFreqTableRow(24, "   0,  12, 48"));
            _freq_map.Add(LoadFreqTableRow(48, "   0,  24, 96"));
            _freq_map.Add(LoadFreqTableRow(72, "   0,  36, 144"));
            _freq_map.Add(LoadFreqTableRow(96, "   0,  48, 192"));
            _freq_map.Add(LoadFreqTableRow(192, "  0,  96, 384"));
            _freq_map.Add(LoadFreqTableRow(384, "  0,  192, 768"));
            _freq_map.Add(LoadFreqTableRow(768, "  0,  384, 1536"));

            //to read this table: if you have at least 6 asessmenets in a 12-hour period, you get Q1h
            //(LOS=12, column Q1h has a count of 6)
        }

        //---------------
        //ED Visit 11. Physiologic Assessment q1hr
        //ED Visit 12. Physiologic Assessment q15min
        //---------------
        //FOUND: looking for code in '304987655,9990007096285,304987657,3045001025,3045001064,304987666,9990304100017,30454321,9990000006294,9993041120042,3045001041,9990304001499,3045001018,9993040103255'; found cat='' code='304987655' desc='temp' field='' result='36.5' (4 more results)
        //1 unique
        //Index was outside the bounds of the array.
        //Index was outside the bounds of the array.
        //   at TransparentMapping.EDVisit.FreqForCount(Double los_hours, Int32 count)
        //   at TransparentMapping.EDVisit.CheckAssessment(Int32 count, String desc)
        //   at TransparentMapping.EDVisit.CountAssessments(Int32 bucket_size, Int32 ind)
        //   at TransparentMapping.EDVisit.Check_1112()
        //   at TransparentMapping.EDVisit.ProcessPatient(PatientInfo pat)
        //   at TransparentMapping.Program.ProcessPatients()
        //Mapping complete


        private Frequencies FreqForCount(double los_hours, int count)
        {
            foreach (var fmrow in _freq_map) {
                if (los_hours <= fmrow.los_high) {
                    // Option: pro-rate the count upward if the LOS is less than the row we are interested in.
                    //         This will bump the count to what it might have been at the full LOS.
                    // Note: truncate the result; rounding inflates the value too much.
                    int prorated_count = (int)((fmrow.los_high / los_hours) * count);

                    // foreach goes low to high; go from high to low instead
                    for (int j = (int)Frequencies.Q15M; (j > (int)Frequencies.QNONE); j--) { //search right to left
                        if (prorated_count >= fmrow.freq[j]) {
                            return (Frequencies)j;
                        }
                    } // next j
                }
            }

            return Frequencies.QNONE;
        }

        //
        // Load patient chart from the database (one DB access)
        // Load everything since admission
        //
        private void LoadPatientChart()
        {
            // Get the entire patient chart (all units and dates for this patient)
            var db = PFSDBUtility.NewPfsDataContext();
            var query = from item in db.CHART_ITEMs
                        where (item.ENCOUNTER_ID == _pat.encounter_id)
                        select item;
            // Save the result
            _chart_items_since_admission = query.ToArray();

            // Convert all results to lower case for case insensitive comparisons
            foreach (var item in _chart_items_since_admission) {
                if (item.CATEGORY != null) item.CATEGORY = item.CATEGORY.ToLower();
                if (item.CODE != null) item.CODE = item.CODE.ToLower();
                if (item.DESCRIPTION != null) item.DESCRIPTION = item.DESCRIPTION.ToLower();
                if (item.FIELD_NAME != null) item.FIELD_NAME = item.FIELD_NAME.ToLower();
                if (item.RESULT != null) item.RESULT = item.RESULT.ToLower();
            }

            // Prepare two more versions of the chart
            var query2 = from item in _chart_items_since_admission
                         where (item.EVENT_DATETIME >= _pat.unit_arrival) && (item.EVENT_DATETIME <= _pat.pull_finish)
                         select item;
            _chart_items_since_unit_arrival = query2.ToArray();

            query2 = from item in _chart_items_since_admission
                         //where (item.EVENT_DATETIME >= _pat.pull_start) && (item.EVENT_DATETIME <= _pat.pull_finish)
                     where (item.EVENT_DATETIME <= _pat.unit_departure.AddMinutes(10))
                     select item;
            _chart_items_during_pull_period = query2.ToArray();
            query2 = from item in _chart_items_since_admission
                         //                     where (item.EVENT_DATETIME >= _pat.pull_start) && (item.EVENT_DATETIME <= _pat.pull_finish.AddHours(4))
                     select item;
            _chart_items_pull_period_plus = query2.ToArray();
        }

        // Delay looking for patient procedures until they are wanted
        private void LoadPatientProceduresIfNeeded()
        {
            if (_procedure_events != null) return;

            var db = PFSDBUtility.NewPfsDataContext();
            var query = from proc in db.PROCEDURE_EVENTs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                        select proc;
            _procedure_events = query.ToArray();
        }



        // Started a new chart item query (of a certain depth)
        // Default is the chart during the pull range.  Options for unit arrival and admission to hospital.
        private IEnumerable<CHART_ITEM> StartNewQuery()
        {
            if (isEDonly)
                return StartNewQuery(SearchDepth.SearchSinceAdmission);
            else
                return StartNewQuery(SearchDepth.SearchDefault);
        }
        private IEnumerable<CHART_ITEM> StartNewQuery(SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;

            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    return (from item in _chart_items_during_pull_period select item);
                case SearchDepth.SearchSinceArrival:
                    return (from item in _chart_items_since_unit_arrival select item);
                case SearchDepth.SearchSinceAdmission:
                    return (from item in _chart_items_since_admission select item);
                case SearchDepth.SearchPullPlus:
                    return (from item in _chart_items_pull_period_plus select item);
            }
            return null;
        }

        // Is this search term a word or a list of words?
        private bool ValueIsAList(string s)
        {
            return (s != null) && s.Contains(',');                //is this a comma-separated list?
        }

        // Break apart a wordlist and prepare its members
        private string[] SplitOnCommaAndPrepareElements(string s)
        {
            // If a word list has a space after each comma, we will get leading blanks in each term
            var arr = s.Split(',');
            for (int i = 0; i <= arr.GetUpperBound(0); i++) {
                arr[i] = arr[i].Trim();                         // get rid of leading blanks
                arr[i] = arr[i].Replace(CHAR_COMMA, ",");       // insert a literal comma into search word
                arr[i] = arr[i].ToLower();                      // convert for case-insensitive comparisons
            }
            return arr;
        }

        // Add a basic chart item filter to a LINQ query
        //
        // cat = exact match
        // code_list = exact match; optional "like" match
        // desc_list = "like" match; optional exact match
        // field = exact match
        // result_list = "like" match; optional exact match
        //
        // These are comma-separated lists.  
        // Search for a literal comma by using CHAR_COMMA in the string.
        private IEnumerable<CHART_ITEM> AndItemFilter(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list)
        {
            // Converted all values to lower case for case-insensitive comparisons.
            if (!String.IsNullOrEmpty(cat)) {
                cat = cat.ToLower();
                query = query.Where(e => e.CATEGORY == cat);
            }

            if (!String.IsNullOrEmpty(code_list)) {
                code_list = code_list.ToLower();
                if (code_list.Left(2) == EXACT_MATCH_PREFIX) {          // exact match override?
                    query = query.Where(e => e.CODE == code_list.Substring(2));
                } else if (code_list.Left(2) == CODE_LIKE_PREFIX)
                {            // override for "like" match?
                    query = query.Where(e => e.CODE.Contains(code_list.Substring(2)));
                } else if (ValueIsAList(code_list)) {
                    query = AndCodeInList(query, code_list);            // find one of the words
                } else {
                    query = query.Where(e => e.CODE.Contains(code_list));      // find this word
                }
            }

            if (!String.IsNullOrEmpty(desc_list)) {
                bool special1 = false; // (desc_list == ";ROUTE=IV");
                desc_list = desc_list.ToLower();
                if (desc_list.Left(2) == EXACT_MATCH_PREFIX) {          // exact match override?
                    query = query.Where(e => e.DESCRIPTION == desc_list.Substring(2));
                } else if (ValueIsAList(desc_list)) {
                    query = AndDescriptionInList(query, desc_list);
                } else {
                    desc_list = desc_list.Replace(CHAR_COMMA, ",");     // look for literal comma requests
                    query = query.Where(e => e.DESCRIPTION.Contains(desc_list));
                    if (special1)
                    {
                        query = query.Where(e => !e.DESCRIPTION.Contains(";ROUTE=IV SLOW PUSH"));
                        query = query.Where(e => !e.DESCRIPTION.Contains(";ROUTE=IV PUSH"));
                    }

                }
            }

            if (!String.IsNullOrEmpty(field)) {
                field = field.ToLower();
                query = query.Where(e => e.FIELD_NAME == field);
            }

            if (!String.IsNullOrEmpty(result_list)) {
                result_list = result_list.ToLower();                        // make all results case insensitve
                if (result_list.Left(2) == EXACT_MATCH_PREFIX) {     // exact match override?
                    query = query.Where(e => e.RESULT == result_list.Substring(2));
                }
                else if (result_list.Left(2) == AVOID_NEGATIVE)
                {
                    query = query.Where(e => (!e.RESULT.ToLower().Contains("no " + result_list.Substring(2))) && (e.RESULT.ToLower().Contains(result_list.Substring(2)))); // == result_list.Substring(2)) || (e.RESULT.Contains(";" + result_list.Substring(2)))));
                }
                else if (ValueIsAList(result_list)) {
                    query = AndResultInList(query, result_list);
                }
                else if (result_list.Left(2) == STARTS_WITH)
                {
                    query = query.Where(e => e.RESULT.StartsWith(result_list.Substring(2)));
                }
                else
                {
                    result_list = result_list.Replace(CHAR_COMMA, ",");     // look for literal comma requests
                    query = query.Where(e => e.RESULT.Contains(result_list));
                }
            }
            return query;
        }

        // Look for a code that is one of the words in the list.
        private IEnumerable<CHART_ITEM> AndCodeInList(IEnumerable<CHART_ITEM> query, string code_list)
        {
            if (String.IsNullOrEmpty(code_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(code_list);
            return query.Where(e => e.CODE.ContainsAny(arr));  // use like match. Exact match = arr.Contains(e.CODE));   
        }

        // Look for a description that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndDescriptionInList(IEnumerable<CHART_ITEM> query, string desc_list)
        {
            if (String.IsNullOrEmpty(desc_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(desc_list);
            return query.Where(e => e.DESCRIPTION.ContainsAny(arr));    // "like" match
        }

        // Look for a result that contains one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultInList(IEnumerable<CHART_ITEM> query, string result_list)
        {
            bool use_start_with = false;

            if (String.IsNullOrEmpty(result_list)) return query;

            if (result_list.Left(2) == STARTS_WITH)
            {
                use_start_with = true;
                result_list = result_list.Substring(2);
            }

            var arr = SplitOnCommaAndPrepareElements(result_list);

            if (use_start_with)
                return query.Where(e => arr.Any(item => e.RESULT.StartsWith(item)));
            else
                return query.Where(e => e.RESULT.ContainsAny(arr)); // "like" match
        }

        // Look for a result that does not contain one of the words in the list.
        private IEnumerable<CHART_ITEM> AndResultNotInList(IEnumerable<CHART_ITEM> query, string result_list)
        {
            if (String.IsNullOrEmpty(result_list)) return query;

            var arr = SplitOnCommaAndPrepareElements(result_list);
            return query.Where(e => !e.RESULT.ContainsAny(arr)); // "like" match
        }

        private string DescribeSearchDepth(SearchDepth search_depth)
        {
            string result = "";
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;

            switch (search_depth) {
                case SearchDepth.SearchDefault:
                case SearchDepth.SearchPullRange:
                    //result = "in pull range"
                    result = "";                         //be quiet since this is default
                    break;
                case SearchDepth.SearchSinceArrival:
                    result = "since arrival to unit";
                    break;
                case SearchDepth.SearchSinceAdmission:
                    result = "since admission";
                    break;
                case SearchDepth.SearchPullPlus:
                    result = "in LOS range plus 4 hours after";
                    break;
            }

            return result;
        }

        // Describe a query that has already been run
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(IEnumerable<CHART_ITEM> query, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            return Describe(query.ToArray(), cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Make a simple query and describe the results
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return Describe(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private string Describe(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            var arr = query.ToArray();
            return Describe(arr, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private string LookingFor(string so_far, string what, string op, string value)
        {
            string result;
            if (String.IsNullOrEmpty(value)) return so_far;

            if (so_far == "")
                result = "looking for " + what;
            else
                result = so_far + " and " + what;

            if (value.Left(2) == EXACT_MATCH_PREFIX)
                result += "='" + value.Substring(2) + "'";
            else if (value.Left(2) == CODE_LIKE_PREFIX)
                result += " contains " + "'" + value.Substring(2) + "'";
            else if (ValueIsAList(value))
                result += " in '" + value + "'";
            else
                result += op + "'" + value + "'";

            return result;
        }
        private string Describe(CHART_ITEM[] arr, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            string result = "";
            result = LookingFor(result, "cat", "=", cat);
            result = LookingFor(result, "code", "=", code_list);
            result = LookingFor(result, "desc", " contains ", desc_list);
            result = LookingFor(result, "field", "=", field);
            result = LookingFor(result, "result", " contains ", result_list);

            if (arr.Count() == 0) {
                result += "; not found " + DescribeSearchDepth(search_depth);
            } else {
                var e = arr[0];
                result = "FOUND: " + result + "; found";
                // We might have searched for a pattern or word list in several fields - show what was found
                if (e.CATEGORY != null) result += " cat='" + e.CATEGORY + "'";
                if (e.CODE != null) result += " code='" + e.CODE + "'";
                if (e.DESCRIPTION != null) result += " desc='" + e.DESCRIPTION + "'";
                if (e.FIELD_NAME != null) result += " field='" + e.FIELD_NAME + "'";
                if (e.RESULT != null) result += " result='" + e.RESULT + "'";
                // Are there more results?  Just say how many; we aren't going to list them.
                if (arr.Count() == 2) {
                    result += " (1 more result)";
                } else if (arr.Count() > 2) {
                    result += " (" + (arr.Count() - 1) + " more results)";
                }
            }

            return result;
        }


        //Set an indicator for this reason (low level)
        private void SetInd(int inum, string reason)
        {
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Set Ind #" + inum + ": " + reason);          //already set - repeat for verbose only
            } else {
                _inds[inum].is_checked = true;
                Program.Audit("Set Ind #" + inum + ": " + reason);
            }
        }

        //Clear an indicator for this reason (low level)
        private void ClrInd(int inum, string reason)
        {
            if (_inds[inum].is_checked) {
                Program.VerboseAudit("Clr Ind #" + inum + ": " + reason);          //already clear - repeat verbose only
            } else {
                _inds[inum].is_checked = false;
                Program.Audit("Clr Ind #" + inum + ": " + reason);
            }
        }

        // Count how many items meet the conditions
        // All items are listed
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountItems(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountItems(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            int count = query.Count();

            // always return what was found
            found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
            // echo the result?
            if (trace) Program.VerboseAudit(found_what);

            return count;
        }

        // Count how many items meet the conditions
        // This has the option to list every entry or give a total
        private int CountResultInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what = "";

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);

            var arr = SplitOnCommaAndPrepareElements(result_list);

            foreach (var item in query) {
                // Figure out which of the search words were found
                var s = item.RESULT.ContainsWhich(arr);
                found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' code='" + item.CODE + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "'";
                if (trace) Program.VerboseAudit(found_what);
                count++;
                if (count_mode != CountMode.CountAll) break;        //stop counting
            }

            if (count > 0) {
                //We already printed what was found; maybe add how many?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        // Count how many items meet the conditions
        // This has the option to list every entry or give a total
        private int CountResultInListEXCEPTList(string cat, string code_list, string desc_list, string field, string result_list, string negresult_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what = "";

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);
            query = AndResultNotInList(query, negresult_list);

            var arr = SplitOnCommaAndPrepareElements(result_list);

            foreach (var item in query)
            {
                // Figure out which of the search words were found
                var s = item.RESULT.ContainsWhich(arr);
                found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' code='" + item.CODE + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "'";
                if (trace) Program.VerboseAudit(found_what);
                count++;
                if (count_mode != CountMode.CountAll) break;        //stop counting
            }

            if (count > 0)
            {
                //We already printed what was found; maybe add how many?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            }
            else
            {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        // Count how many items contain ALL results in list
        private int CountResultInListAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0;
            found_what = "";

            var arr = SplitOnCommaAndPrepareElements(result_list);

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            Program.VerboseAudit("result list arr ub=" + arr.GetUpperBound(0));
            for (int i = 0; i <= arr.GetUpperBound(0); i++)
            {
                Program.VerboseAudit("result list[" + i + "]=" + arr[i]);
                if (arr[i].StartsWith(NOT_PREFIX))
                {
                    query = AndResultNotInList(query, arr[i]);
                    //query = query.Where(e => !e.RESULT.Contains(arr[i].Substring(2)));
                }
                else
                {
                    query = AndResultInList(query, arr[i]);
                    //query = query.Where(e => e.RESULT.Contains(arr[i]));
                }
            }

            count = query.Count();

            if (count > 0)
            {
                found_what = "found item with all results in '" + result_list + "' without those prefixed by !!";
                if (trace) Program.VerboseAudit(found_what);
            }
            else
            {
                // Describe what was *not* found
                found_what = Describe(cat, code_list, desc_list, field, result_list, search_depth);
                if (trace) Program.VerboseAudit(found_what);
            }

            return count;
        }

        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Count in the pull range with trace on
            return CountResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private int CountResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            if (ValueIsAList(result_list))
            {
                return CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what);
            } else {
                return CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what);
            }
        }

        // Is there an item with this result?
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContains(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContains(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContains(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            if (ValueIsAList(result_list))
            {
                return (CountResultInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            } else {
                return (CountItems(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
            }
        }

        // Result contains all in list
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on; give choice of search depth
            return ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultContainsAll(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            if (ValueIsAList(result_list))
            {
                return (CountResultInListAll(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountFirst, trace, out found_what) > 0);
            }
            else
            {
                found_what = "Not valid for non-list results.";
                return false;
            }
        }
        // Count how many items meet the conditions, with none of the results in result_list.
        // This has the option to say how many were found rather than list all.
        private int CountResultNotInList(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, CountMode count_mode, bool trace, out string found_what)
        {
            int count = 0, rec_count = 0;
            bool found_one;

            found_what = "";
            var arr = SplitOnCommaAndPrepareElements(result_list);         // also deals with CHAR_COMMA

            //Do not filter by result here; we need all results
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            foreach (var item in query) {
                rec_count++;
                //Look for each search word in the result
                found_one = false;

                foreach (string s in arr) {
                    if (String.Equals(item.RESULT, s)) {
                        found_what = "found '" + s + "' in cat='" + item.CATEGORY + "' field='" + item.FIELD_NAME + "' result='" + item.RESULT + "' -- ignore this";
                        Program.VerboseAudit(found_what);
                        found_one = true;
                        break;
                    }
                }

                if (!found_one) {
                    //none of the words were found - good!
                    found_what = Describe(cat, code_list, desc_list, field, "", search_depth) + " -- does not contain '" + result_list + "'";
                    Program.VerboseAudit(found_what);
                    count++;
                    if (count_mode != CountMode.CountAll) break;
                }
            }

            if (count > 0) {
                //We already printed what was found; maybe show the total?
                if (trace && (count_mode == CountMode.CountAll)) Program.VerboseAudit("found " + count + " total");
            } else if (rec_count > 0) {
                //We already printed what we ignored
            } else {
                //Print what did not exist
                found_what = Describe(cat, code_list, desc_list, field, "", search_depth);      //not found
                Program.VerboseAudit(found_what);
            }

            return count;
        }

        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, string found_what)
        {
            // Search in the pull range with trace on
            return CountResultDoesNotContain(cat, code_list, desc_list, field, result_list, out found_what, SearchDepth.SearchDefault, true);
        }
        private bool CountResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, out string found_what, SearchDepth search_depth, bool trace)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            return CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0;
        }

        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            // Search in the pull range with trace on
            return ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, true, out found_what);
        }
        private bool ResultDoesNotContain(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            return (CountResultNotInList(cat, code_list, desc_list, field, result_list, search_depth, CountMode.CountAll, trace, out found_what) > 0);
        }

        // Set the indicator if the conditions are met
        //
        private bool SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);

        }
        private bool SetIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            bool b = false;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return true;

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what)) {
                SetInd(inum, found_what);                          //echo here - found
                b = true;
            } else {
                Program.VerboseAudit(found_what);                  //and here - not found
            }
            return b;
        }
        // Special contains all
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContainsAll(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultContainsAll(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return;

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            //Turn trace off for ResultContains() so the result can be placed on the "SetInd#" reason below
            if (ResultContainsAll(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                          //echo here - found
            }
            else
            {
                Program.VerboseAudit(found_what);                  //and here - not found
            }
        }
        // Set the indicator if the result does not contain any of the words in result_list
        //
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultDoesNotContain(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfResultDoesNotContain(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already set
            //if (_inds[inum].is_checked) return;

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            //Turn trace off for ResultDoesNotContain() and echo what was set below with SetInd
            if (ResultDoesNotContain(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what))
            {
                SetInd(inum, found_what);                           //echo here - not found
            } else {
                Program.VerboseAudit(found_what);                   //and here - found
            }
        }

        // Clear the indicator if the result contains one of the words in the result_list
        //
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfResultContains(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            string found_what;
            //avoid more queries if the indicator is already clear
            if (!_inds[inum].is_checked) return;

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            //Turn trace off for ResultContains() and echo what was set below with SetInd
            if (ResultContains(cat, code_list, desc_list, field, result_list, search_depth, false, out found_what)) {
                ClrInd(inum, found_what);                           //echo here - found
            } else {
                Program.VerboseAudit(found_what);                   //and here - not found
            }
        }

        private bool SetIndIfResultStartsWith(int inum, string cat, string code_list, string desc_list, string field, bool starts_with, string start_char, SearchDepth search_depth)
        {
            int count = 0;
            string found_what = "";
            bool b = false;

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");
            if (starts_with)
                query = query.Where(e => e.RESULT.StartsWith(start_char.ToLower()));
            else
                query = query.Where(e => !e.RESULT.StartsWith(start_char.ToLower()));
            foreach (var item in query)
            {
                count++;
                found_what = "found code=" + item.CODE + " with result=" + item.RESULT;
                b = true;
            }

            if (count > 0)
                SetInd(inum, found_what);
            return b;
        }


        //These slightly smaller functions are meant for places where you aren't really looking for a chart result
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list)
        {
            string found_what;
            // Search in the pull range with trace on
            return Exists(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault, true, out found_what);
        }
        private bool Exists(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth, bool trace, out string found_what)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            return (CountResultContains(cat, code_list, desc_list, field, result_list, search_depth, trace, out found_what) > 0);
        }

        // Variations with optional parameters (can switch to optional parameters in VS 2010)
        //
        private void SetIndIfFound(int inum, string cat)
        {
            SetIndIfResultContains(inum, cat, "", "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list)
        {
            SetIndIfResultContains(inum, cat, code_list, "", "", "", SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void SetIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            SetIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private void ClrIndIfFound(int inum, string cat)
        {
            ClrIndIfResultContains(inum, cat, "", "", "", "", SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list)
        {
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private void ClrIndIfFound(int inum, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            ClrIndIfResultContains(inum, cat, code_list, desc_list, field, result_list, search_depth);
        }


        // Get the max/total value from a result (usually in the middle of the text)
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetIntValue(get_mode, cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetIntValue(GetValueMode get_mode, string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            int value;
            int result = 0;
            bool found_one = false;

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, result_list);

            //Look for a number in the result

            foreach (var item in query) {
                // RESULT might be null so convert to empty string so Split won't blow up
                var arr = PFSDBUtility.DBToString(item.RESULT).Split(' ');
                foreach (string s in arr) {
                    //Look for 1st character numeric; IsNumeric("60min") will fail so look at 1st char only
                    if (s.Left(1).IsNumeric()) {
                        value = (int)s.Val();                           //Use Val; ToInteger will error on "60min"
                        switch (get_mode) {
                            case GetValueMode.GetMax:
                                result = Math.Max(value, result);       //max
                                break;
                            case GetValueMode.GetTotal:
                                result += value;                        //total
                                break;
                            case GetValueMode.GetLast:
                                result = value;                         //last
                                break;
                        }

                        // print what we are searching for (the first time)
                        if (!found_one) {
                            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
                        }
                        found_one = true;
                        // print each value found
                        Program.VerboseAudit("  found numeric value " + result);
                        //Keep going in case there are more
                    }
                }
            }

            if (!found_one) {
                //show what was not found
                Program.VerboseAudit(Describe(cat, code_list, desc_list, field, result_list, search_depth));
            }

            return result;
        }

        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetMaxValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetMaxValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            return GetIntValue(GetValueMode.GetMax, cat, code_list, desc_list, field, result_list, search_depth);
        }

        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list)
        {
            return GetTotalValue(cat, code_list, desc_list, field, result_list, SearchDepth.SearchDefault);
        }
        private int GetTotalValue(string cat, string code_list, string desc_list, string field, string result_list, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            return GetIntValue(GetValueMode.GetTotal, cat, code_list, desc_list, field, result_list, search_depth);
        }

        // Get a result; returns true if found with return_result
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result)
        {
            return GetResult(cat, code_list, desc_list, field, out return_result, SearchDepth.SearchDefault);
        }
        private bool GetResult(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0) {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
            } else {
                return_result = "";
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }

        private void GetResultforOther(string cat, string code_list, string desc_list, string field, out string return_result, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "Other");
            return_result = "";
            foreach (var item in query)
            {
                if (!String.Equals(item.RESULT, "Other"))
                {
                    return_result = item.RESULT;
                    //Program.VerboseAudit(found_what);
                    break;
                }

            }
            return;
        }

        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, out string return_result, out DateTime return_evdt)
        {
            return GetResultAndEVDT(cat, code_list, desc_list, field, out return_result, out return_evdt, SearchDepth.SearchDefault);
        }
        private bool GetResultAndEVDT(string cat, string code_list, string desc_list, string field, out string return_result, out DateTime return_evdt, SearchDepth search_depth)
        {
            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");

            if (query.Count() > 0)
            {
                return_result = PFSDBUtility.DBToString(query.First().RESULT);
                return_evdt = PFSDBUtility.DBToDateTime(query.First().EVENT_DATETIME);
            }
            else
            {
                return_result = "";
                return_evdt = DateTime.MinValue;
            }

            Program.VerboseAudit(Describe(cat, code_list, desc_list, field, "", search_depth));
            return (!String.IsNullOrEmpty(return_result));
        }
        private void SetIndIfResultBetween(int inum, string cat, string code_list, string desc_list, string field, double loval, double hival)
        {
            SetIndIfResultBetween(inum, cat, code_list, desc_list, field, loval, hival, SearchDepth.SearchDefault);
        }

        private void SetIndIfResultBetween(int inum, string cat, string code_list, string desc_list, string field, double loval, double hival, SearchDepth search_depth)
        {
            //if (Program.g_use_all_chart_items) search_depth = SearchDepth.SearchSinceAdmission;
            int count = 0;
            string found_what = "";

            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, "");
            foreach (var item in query)
            {
                //                Program.VerboseAudit("ResBetween: code=" + item.CODE + " result=" + item.RESULT);
                if (item.RESULT.IsNumeric())
                {
                    //Program.VerboseAudit("  result is numeric");
                    if (item.RESULT.Val() >= loval && item.RESULT.Val() <= hival)
                    {
                        count++;
                        found_what = "found code=" + item.CODE + " with result=" + item.RESULT + ": " + item.EVENT_DATETIME;
                        Program.VerboseAudit(found_what);
                    }
                }
            }

            if (count > 0)
            {
                SetInd(inum, found_what);
            }
        }


        //======================================================================================
        // Mapping starts here
        //======================================================================================

        private void Check_1()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Visit 1. Initial Assessment > 20 min");
            Program.VerboseAudit("---------------");

            esi_trauma_1 = false;
            _trauma_triage_was_set = false;

            string reslist = "1,2";
            SetIndIfResultContains(1, "", "cas esi trg acuity", "", "", reslist);
            esi_trauma_1 |= Exists("", "cas esi trg acuity", "", "", "1");

        DateTime evdt;
            if (LookAtTraumaTriageNote("Trauma Triage Nursing Note v3", char183 + " Trauma Level" + char9, "Activation Level I", out evdt))
            {
                SetInd(1, "Trauma Triage Nursing Note with Activation Level I" + " at" + evdt);
//                esi_trauma_1 = true;
                _trauma_triage_was_set = true;
            }
            if (LookAtTraumaTriageNote("Trauma Triage Nursing Note v3", char183 + " Trauma Level" + char9, "Activation Level II", out evdt))
            {
                SetInd(1, "Trauma Triage Nursing Note with Activation Level II" + " at" + evdt);
                _trauma_triage_was_set = true;
            }
            if (LookAtTraumaTriageNote("Trauma Triage Nursing Note v3", char183 + " ESI Triage Acuity level" + char9, "1", out evdt))
            {
                SetInd(1, "Trauma Triage Nursing Note with ESI Triage Level 1" + " at" + evdt);
                esi_trauma_1 = true;
            }
            if (LookAtTraumaTriageNote("Trauma Triage Nursing Note v3", char183 + " ESI Triage Acuity level" + char9, "2", out evdt))
            {
                SetInd(1, "Trauma Triage Nursing Note with ESI Triage Level 2" + " at" + evdt);
                esi_trauma_1 = true;
            }

            Program.VerboseAudit("esi_trauma_1=" + esi_trauma_1);
            Program.VerboseAudit("_trauma_triage_was_set=" + _trauma_triage_was_set);

        }

        private bool LookAtTraumaTriageNote(string code, string res1, string res2, out DateTime evdt)
        {
            bool ret = false;
            evdt = DateTime.MinValue;

            var query = StartNewQuery();
            query = query.Where(e => e.CODE.ToUpper().StartsWith(code.ToUpper()));
            query = query.Where(e => e.RESULT.ToUpper()==(res1 + res2).ToUpper());
            query = query.OrderByDescending(e => e.EVENT_DATETIME);
            ret = (query.Count() > 0);
            if (ret)
            {
                evdt = query.First().EVENT_DATETIME;
                _trauma_triage_was_set = true; //will trigger wound mgt
            }

            return ret;
        }


        private void Check_234()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Visit 2. ADL - Self Care");
            Program.VerboseAudit("ED Visit 3. ADL - Assist");
            Program.VerboseAudit("ED Visit 4. ADL - Extended");
            Program.VerboseAudit("---------------");

            if (_pat.age < 4.0)
                SetInd(4, "Age=" + _pat.age);
            else if (_pat.age < 7.0)
                SetInd(3, "Age=" + _pat.age);

            string reslist, res;

            reslist = "";
            SetIndIfResultContains(2, "", "eas trg alert", "", "", reslist);
            reslist = "constant observation, safety watch/fall risk";
            SetIndIfResultContains(3, "", "eas trg alert", "", "", reslist);
            reslist = "dysphagia";
            SetIndIfResultContains(4, "", "eas trg alert", "", "", reslist);


            reslist = "passed";
            SetIndIfResultContains(2, "", "SNCH Dysphagia Result SR", "", "", reslist);
            reslist = "failed";
            SetIndIfResultContains(4, "", "SNCH Dysphagia Result SR", "", "", reslist);

            reslist = "expanded capacity";
            SetIndIfResultContains(4, "", "eas trg alert", "", "", reslist);

            reslist = "<11";
            SetIndIfResultBetween(2, "", "snch sc humdum score peds cal", "", "",0,11);
            reslist = "12 or greater";
            SetIndIfResultBetween(4, "", "snch sc humdum score peds cal", "", "", 12,99);

            reslist = "no risk, low risk";
            SetIndIfResultContains(2, "", "casc fall morse risk level FT", "", "", reslist);
            reslist = "moderate risk";
            SetIndIfResultContains(3, "", "casc fall morse risk level FT", "", "", reslist);
            reslist = "high risk";
            SetIndIfResultContains(4, "", "casc fall morse risk level FT", "", "", reslist);

            reslist = "no";
            SetIndIfResultContains(2, "", "snch audit c exclusion: cognitively impaired:", "", "", reslist);
            reslist = "yes";
            SetIndIfResultContains(4, "", "snch audit c exclusion: cognitively impaired:", "", "", reslist);

            reslist = "safety watch/fall risk, agitated, constant observation";
            SetIndIfResultContains(3, "", "eas trg alert", "", "", reslist);
            reslist = "expanded capacity, ETOH";
            SetIndIfResultContains(4, "", "eas trg alert", "", "", reslist);

            reslist = "arrived to ER with catheter in place, change catheter, yes";
            SetIndIfResultContains(2, "", "snch ed int gu yn", "", "", reslist);
            reslist = "";
            SetIndIfResultContains(4, "", "snch ed int gu yn", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(4, "", "SNCH_EIO in constant bladder irrigation", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(4, "", "EIO out urine cath indwell urethral NU 27E4", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(4, "", "EIO out urine voided NU B0FA", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(4, "", "EIO urine diaper count NU 61EF", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(4, "", "EIO out urine diaper weight NU 7C70", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(4, "", "EIO out gi rectal tube NU C975", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(4, "", "EIO out ebl estimated blood loss NU ADBE", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(4, "", "EIO out ebl ob bleed clots in ml NU 1492", "", "", reslist);

            reslist = "dime-sized";
            SetIndIfResultContains(2, "", "EIO out ebl clot size 7656", "", "", reslist);
            reslist = "quarter-sized, grape-sized";
            SetIndIfResultContains(3, "", "EIO out ebl clot size 7656", "", "", reslist);
            reslist = "lemon-sized, orange-sized";
            SetIndIfResultContains(4, "", "EIO out ebl clot size 7656", "", "", reslist);

            reslist = "1";
            SetIndIfResultContains(2, "", "EIO out ebl ob pad count NU 2665", "", "", reslist);
            reslist = "2";
            SetIndIfResultContains(3, "", "EIO out ebl ob pad count NU 2665", "", "", reslist);
            reslist = "3";
            SetIndIfResultContains(4, "", "EIO out ebl ob pad count NU 2665", "", "", reslist);

            reslist = "constant observation, safety watch/fall risk, agitated";
            SetIndIfResultContains(3, "", "eas trg alert", "", "", reslist);
            reslist = "constant observation, expanded capacity, ETOH";
            SetIndIfResultContains(4, "", "eas trg alert", "", "", reslist);

            if (_trauma_triage_was_set)
                SetInd(4, "Make this pt Extended due to: ESI/Trauma Triage Level 1 or 2.");// or 2.");

            if (!_inds[2].is_checked && !_inds[3].is_checked && !_inds[4].is_checked)
                SetInd(2, "Defaulting to ADL-Self due to lack of documentation.");

        }


        private bool Check_b79(out string found_what)
        {
            bool b = false;
            string d = "";
            var query = StartNewQuery();
            query = query.Where(e => e.CODE.ToUpper().StartsWith("MED"));
            query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;given") && (e.DESCRIPTION.ToUpper().Contains("PEG3350 100 GRAM-SOD SUL") || e.DESCRIPTION.ToUpper().Contains("PEG 3350-ELECTROLYTES 236")));
            query = query.OrderByDescending(e => e.EVENT_DATETIME);
            //query = query.Where(e => e.DESCRIPTION.ToLower().Contains(";;;given"));
            b = (query.Count() > 0);
            if (b) d = query.First().DESCRIPTION;
            found_what = d;
            return b;
        }

        private void CheckOrient3()
        {
            SearchDepth search_depth = SearchDepth.SearchDefault;
            bool do_not_set = false;
            //oriented to person, oriented to place, oriented to time
            bool o1 = ResultContains("", "9990000301870", "", "", "oriented to person");
            bool o2 = ResultContains("", "9990000301870", "", "", "oriented to place");
            bool o3 = ResultContains("", "9990000301870", "", "", "oriented to time");
            int oTot = (o1 ? 1 : 0) + (o2 ? 1 : 0) + (o3 ? 1 : 0);
            if ((oTot > 0) && (oTot < 3))
            {
                SetInd(3, "Oriented to at least one but less than three aspects.");
            }
            if (oTot != 3) return; // otherwise see if the 3 times are the same....

            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var q1 = StartNewQuery(search_depth);
            q1 = AndItemFilter(q1, "", "9990000301870", "", "", "oriented to person");
            foreach (var item in q1)
            {
                var q2 = StartNewQuery(search_depth);
                q2 = AndItemFilter(q2, "", "9990000301870", "", "", "oriented to place");
                q2 = q2.Where(e2 => e2.EVENT_DATETIME == item.EVENT_DATETIME);
                var ct2 = q2.Count();
                if (ct2 > 0)
                {
                    var q3 = StartNewQuery(search_depth);
                    q3 = AndItemFilter(q3, "", "9990000301870", "", "", "oriented to time");
                    q3 = q3.Where(e3 => e3.EVENT_DATETIME == item.EVENT_DATETIME);
                    var ct3 = q3.Count();
                    if (ct3 > 0)
                    {
                        do_not_set = true;
                    }

                }
            }
            if (!do_not_set) SetInd(3, "Not oriented to all three aspects at the same time.");
        }

        private void Check_5()
        {
            string reslist;            
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Visit 5. Communication Support");
            Program.VerboseAudit("---------------");


            reslist = "no";
            SetIndIfResultContains(5, "", "snch_pref lang y/n 2", "", "", reslist);


            reslist = " hearing loss";
            SetIndIfResultContains(5, "", "cas edn eent ear disturbances", "", "", reslist);

            reslist = "yes";
            SetIndIfResultContains(5, "", "cas edn eent visual disturbances", "", "", reslist);

            reslist = "confused, disoriented, nonresponsive, possible head injury";
            SetIndIfResultContains(5, "", " cas trg disability", "", "", reslist);

            reslist = "aphasia, slurred speech";
            SetIndIfResultContains(5, "", "CCCP trg associated symptoms pos", "", "", reslist);

            reslist = "yes";
            SetIndIfResultContains(5, "", "SNCH ED FAST S yn", "", "", reslist);

            reslist = "bag-valve mask, CPR, intubation";
            SetIndIfResultContains(5, "", "eadm trg rx prior arrival 71Y9", "", "", reslist);

        }


        private void Check_67()
        {
            string reslist;
            bool is_peds = false;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Visit 6. Safety Management - q30min");
            Program.VerboseAudit("ED Visit 7. Safety Management - 1 to 1");
            Program.VerboseAudit("---------------");

            is_peds = (_pat.age <= 14.0);

            //SNCH_lethality_observation with initiated/maintained  q shift 12 hours
            // ALSO NOTE THAT SNCH_restraint% is not coming across at all 7/13/22

            reslist = "";
            SetIndIfResultContains(6, "", "SNCH_restraint type", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(7, "", "SNCH_restraint type Level II", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(6, "", "AS seizure interventions", "", "", reslist);

            reslist = "12 or greater";
            SetIndIfResultBetween(6, "", "snch sc humdum score peds cal", "", "", 12,99);

            reslist = "high risk";
            SetIndIfResultContains(6, "", "casc fall morse risk level FT", "", "", reslist);

            reslist = "observing patient, yellow clip applied to armband, skid-free slippers applied, call bell within patient's reach, assist with all ambulation, education regarding fall risk, education re call for assistance with ambulation, safety measures reinforced with patient/family";
            SetIndIfResultContains(6, "", "EAS trg scrn harm precaut 11C3", "", "", reslist);
            reslist = "restraints applied";
            SetIndIfResultContains(7, "", "EAS trg scrn harm precaut 11C3", "", "", reslist);


        }

        private void Check_89()
        {
            string reslist;
            bool is_peds = false;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Visit 8. Behavior/Emotional Mgt");
            Program.VerboseAudit("ED Visit 9. Behavior/Emotional Mgt >= 30min");
            Program.VerboseAudit("---------------");

            is_peds = (_pat.age <= 14.0);


            reslist = "0,-1,-2,-3";
            SetIndIfResultContains(8, "", "AS SC rass scale", "", "", reslist);
            reslist = "-4,-5,+1,+2,+3,+4";
            SetIndIfResultContains(9, "", "AS SC rass scale", "", "", reslist);


            reslist = "anxious, flat affect, sad, hypoactive (quiet, withdrawn), withdrawn, tearful,";
            SetIndIfResultContains(8, "", "AS mood behavior", "", "", reslist);
            reslist = "agitated, angry, combative (aggressive), excitable, hostile, hyperactive (agitated, impulsive), labile, restless, sexually inappropriate,  threatening, uncooperative";
            SetIndIfResultContains(9, "", "AS mood behavior", "", "", reslist);

            reslist = "code gray";
            SetIndIfResultContains(9, "", "snch_eas event code", "", "", reslist);

            reslist = "apprehensive, denial, flat, frustrated, grieving, overwhelmed, sad, tearful/crying, withdrawn";
            SetIndIfResultContains(8, "", "AS emotional state observe", "", "", reslist);
            reslist = "afraid/fearful, agitated, angry, anxious, attention-seeking behavior, combative, crying continuous/inconsolable, hopeless, irritable, overstimulated/overactive, panic, repeated requests, restless, shocked, uncooperative, withholds information";
            SetIndIfResultContains(9, "", "AS emotional state observe", "", "", reslist);



        }
        private bool CheckResultFor11RNTime(string cat, string code_list, string desc_list, string field, string result, SearchDepth search_depth)
        {
            bool qualifies = false;
            int ct = 0;
            //            "Selection of
            //1:1 RN Time (# minutes) with time > 30 minutes 

            //Or

            //Selection of:
            //Following behavior safety plan(See notes)
            //or

            //Selection of:
            //Multiple Staff for acute behavioral intervention"


            if (isEDonly) search_depth = SearchDepth.SearchSinceAdmission;
            var query = StartNewQuery(search_depth);
            query = AndItemFilter(query, cat, code_list, desc_list, field, 
                "Following behavior safety plan,Multiple Staff for acute behavioral intervention,1:1 RN Time,1:1 Staff Time");
            ct = query.Count();
            foreach (var item in query)
            {
                if (!qualifies)
                {
                    qualifies |= (item.RESULT.ToLower().Contains("Following behavior safety plan".ToLower()));
                    qualifies |= (item.RESULT.ToLower().Contains("Multiple Staff for acute behavioral intervention".ToLower()));
                }
            }
            if (!qualifies && (ct > 0)) // then 1:1 RN are the only results.  need to check their NTE.
            {
                // Query to look for OBX + NTE
                //select patindex('%|9993040000352%NTE|%', SOURCE_TEXT),
                //      substring(SOURCE_TEXT, patindex('%|9993040000352%NTE|%', SOURCE_TEXT), 240) from EVENT_LOG
                //where source_text like '%9993040000352%NTE|%' and timestamp>'2/12/19'

                //OBX|1|ST|9993040000352^Staff Interventions||1:1 Staff Time(# minutes)||||||F|||20190211203400||IDMPROD20979444^BIRCHLER^SARAH^M
                //NTE|1||60
                DateTime tstamp;
                foreach (var item in query)
                {
                    if (!qualifies && 
                        (item.RESULT.ToLower().Contains("1:1 RN Time".ToLower()) || item.RESULT.ToLower().Contains("1:1 Staff Time".ToLower())))
                    {
                        tstamp = PFSDBUtility.DBToDateTime(item.TIMESTAMP);
                        Program.VerboseAudit("Check NTE segment for 1:1 Time after:" + tstamp);
                        qualifies = CheckForNTE(tstamp);
                    }

                }

            }

            return qualifies;
        }
        private bool CheckForNTE(DateTime tstamp)
        {
            bool qualifies = false;
            int v = 0;
            //event_log timestamp is always later than the chartitem timestamp it created.
            string sql = "select substring(SOURCE_TEXT, patindex('%|9993040000352%NTE|%', SOURCE_TEXT)-3, 240) from EVENT_LOG";
            sql += " where encounter_id=" + _pat.encounter_id.ToString();
            sql += " and timestamp>=" + PFSDBUtility.SQLDateTime(tstamp);
            sql += " and timestamp<" + PFSDBUtility.SQLDateTime(tstamp.AddMinutes(3));
            sql += " and source_text like '%9993040000352%NTE|%'";
            var db = PFSDBUtility.NewSqlConnection();
            var cmd = new SqlCommand(sql, db);
            SqlDataReader dr = cmd.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            while (dr.Read())
            {
                string s = PFSDBUtility.DBToString(dr[0]);
                Program.VerboseAudit("String containing NTE segment: " + s);
                int posNTE = s.IndexOf("NTE|",0);
                int posOBX = s.IndexOf("OBX|",0);
                Program.VerboseAudit("posNTE=" + posNTE + " posOBX="+posOBX + " s.Length="+s.Length);
                if (posNTE > 0)
                {
                    string str_posNTE = s.Substring(posNTE, s.Length - posNTE);
                    //Program.VerboseAudit("posNTE=" + posNTE + "  posOBX=" + posOBX);
                    if ((posNTE > 0) && (posOBX > 0) && (posNTE < posOBX)) // then this NTE is associated with the item
                    {
                        //look for the value in NTE-3
                        if (s.Length >= 32)
                        {
                            //s = s.Substring(posNTE, 32);
                            var arr = str_posNTE.Split(new string[] { "|" }, StringSplitOptions.None);
                            if (arr.GetUpperBound(0) >= 3)
                            {
                                int numpos = arr[3].IndexOf(" ");
                                if (numpos <= 0)
                                    numpos = arr[3].IndexOf("OBX");
                                string numstr;
                                if (numpos <= 0)
                                    numstr = arr[3];
                                else
                                    numstr = arr[3].Substring(0, numpos);
                                //Program.VerboseAudit("numpos=" + numpos + " arr3.len="+arr[3].Length + " numstr=" + numstr);
                                if (numstr.IsNumeric())
                                {
                                    v = (int)numstr.Val();
                                }
                                Program.VerboseAudit("NTE-3=" + arr[3] + " v=" + v);
                                qualifies |= (v >= 30);
                            }
                        }
                    }
                }
            }
            dr.Close();
            return qualifies;
        }

        private void Check_10()
        {
            string reslist;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Visit 10. Fluid Management IV Route");
            Program.VerboseAudit("---------------");

            reslist = "arrived with IV in place, for medication, for hydration administration";
            SetIndIfResultContains(10, "", "snch ed int iv diff sr", "", "", reslist);

            reslist = "arrived with IV in place, for medication, for hydration administration";
            SetIndIfResultContains(10, "", "snch ed int iv diff sr", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO urine bladder volume per us NU 9A6B", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out urine cath indwell urethral NU 27E4", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out urine cath intermit urethral NU 4BF1", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out urine condom catheter NU E6F5", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out urine voided NU B0FA", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO urine diaper count NU 61EF", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out urine diaper weight NU 7C70", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out urine nephrostomy NU D64D", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out urine suprapubic NU E0A8", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out urine ureteral cath NU 6498", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out urine penis pouch NU FF4E", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out urine urostomy NU 2C3E", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out urine or NU 9599", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out urine void not save count NU 4452", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out urine incontinence count NU 2665", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH_EIO out constant bladder irrigation", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out gi blakemore NU F9C5", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out gi cholecystostomy NU 3683", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out gi emesis NU 1FCC", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out gi emesis count NU DDAC", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out gi gastrostomy NU 9B61", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out gi ileostomy NU 5EDC", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out gi jejunostomy NU 0DB4", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out gi levine NU C29F", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out gi miller abbott NU 3901", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out gi minnesota NU 5EAC", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out gi nasoduodenal NU 9F2A", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out gi nasogastric NU EDFA", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out gi orogastric tube NU E520", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out gi other gastric NU C0F8", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out gi other intestinal NU E88D", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out gi rectal tube NU C975", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out gi salem sump NU 94E8", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out gi stool NU 939C", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out gi stool count NU 439E", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out drain blake NU 1F37", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out drain chest tube NU 7493", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out drain hemovac NU F1F5", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out drain jackson pratt NU 38A0", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out drain mediastinal NU 7E58", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out drain ng tube NU 0F41", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out drain og tube NU 58FD", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out drain penrose NU 29C2", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out amount penrose DDCB", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out drain pericardial NU 6EBC", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out drain t tube NU 0A23", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out drain ventriculostomy NU 183A", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out drain wounds NU E12D", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out dialysis hemodialysis NU 052B", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out dialysis peritoneal NU", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out ebl estimated blood loss NU ADBE", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out ebl clot size 7656", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out ebl ob pad count NU 2665", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out ebl lab draws NU 106E", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out ebl ob bleed clots in ml NU 1492", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO out tf tube feed residual NU AFA8", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in oral fluid NU E8A8", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in oral med fluid NU E800", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in oral breast milk NU 7C1D", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in bld cryoprecipate NU 0261", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in bld factor viii NU D4AF", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in bld factor ix NU 80A1", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH eio in bld Frozen Plasma", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH eio in bld Platelets", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH eio in bld Red Blood Cells", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH eio in bld Rh Immune Globulin", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH eio in bld Other Product", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH eio in col Albumin 5%, 2.5g", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH eio in col Albumin 5%, 12.5g", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH eio in col Albumin 25%, 12.5g", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in pn fat emulsion 10 percent NU D640", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in pn fat emulsion 20 percent NU 6C67", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in ivpb d5w E11E", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in ivpb lr C716", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in ivpb nacl 0.9 BB8E", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in pn ppn NU 78ED", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in pn tpn NU 97CD", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in pn pediatric tpn NU 351D", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in pn fat emulsion 10 percent NU D640", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in pn fat emulsion 20 percent NU 6C67", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH eio in cry d2.5w 500 mL", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH eio in cry d5w 500mL", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d5w 1000ml NU E6CB", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d5w and alcohol 5 percent 1000ml NU CD00", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d5w plus electrolyte mb 1000ml NU A8F2", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d5w and electrolyte 48 1000ml NU 08FA", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d5w plus 10meq kcl 1000ml NU 60BB", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d5w plus 20meq kcl 1000ml NU 05EE", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d5w plus 30meq kcl 1000ml NU 1FF0", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d5w plus 40meq kcl 1000ml NU 8D0C", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d5w plus 20meq kpo4 1000ml NU 9544", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d5w plus 50eq nahco3 1000ml NU D3E9", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH eio in cry d5w nacl 0.2 500 mL", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d5 and nacl 0.2 plus 10meq kcl 1000ml NU 57AA", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d5 and nacl 0.2 plus 20meq kcl 1000ml NU C087", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d5 and nacl 0.2 plus 30meq kcl 1000ml NU D52E", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d5 and nacl 0.2 plus 40meq kcl 1000ml NU ED27", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d5 and nacl 0.2 plus 20meq kpo4 1000ml NU 5842", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d5 and nacl 0.2 plus 50meq nahco3 1000ml NU 41E2", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH eio in cry d5w nacl 0.33% 500mL", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH eio in cry d5w nacl 0.33% 1000mL", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH eio in cry d5w nacl 0.45% 500mL", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d5 and nacl 0.45 1000ml NU 156E", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH eio in cry d5w nacl 0.45% 500mL+10mEqKCL", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH eio in cry d5w nacl 0.45% 500mL+20mEqKCL", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d5 and nacl 0.45 plus 10meq kcl 1000ml NU 665A", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d5 and nacl 0.45 plus 20meq kcl 1000ml NU 4C64", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d5 and nacl 0.45 plus 30meq kcl 1000ml NU A64D", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d5 and nacl 0.45 plus 40meq kcl 1000ml NU BE37", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d5 and nacl 0.45 plus 20meq kpo4 1000ml NU 8457", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d5 and nacl 0.45 plus 50meq nahco3 1000ml NU B356", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH eio in cry d5w nacl 0.9% 500mL", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH eio in cry d5w nacl 0.9% 500mL+10mEqKCL", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH eio in cry d5w nacl 0.9% 500mL+20mEqKCL", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d5 and nacl 0.9 1000ml NU 4241", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d5 and nacl 0.9 plus 10meq kcl 1000ml NU 5AAB", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d5 and nacl 0.9 plus 20meq kcl 1000ml NU EAB5", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d5 and nacl 0.9 plus 30meq kcl 1000ml NU 6202", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d5 and nacl 0.9 plus 40meq kcl 1000ml NU 67DF", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d5 and nacl 0.9 plus 20meq kpo4 1000ml NU 3E93", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH eio in cry d5w lr 500mL", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH eio in cry d5w lr 500mL+ 10mEq KCL", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH eio in cry d5w lr 500mL+ 20mEq KCL", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d5 and lr 1000ml NU 0B89", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d5 and lr plus 10meq kcl 1000ml NU 44AC", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d5 and lr plus 20meq kcl 1000ml NU CCE2", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d5 and lr plus 30meq kcl 1000ml NU 44AC", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d5 and lr plus 40 meq kcl 1000ml NU 666E", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d5 and lr plus 20meq kpo4 1000ml NU 53AC", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d5 and lr plus 50meq nahco3 1000ml NU 5AEE", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH eio in cry d7.5w 500 mL", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH eio in cry d10w 500 mL", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d10w 1000ml NU 7C1E", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d10w and amino acids 10 percent 1000ml NU 7F3D", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d10w plus 20meq kcl 1000ml NU 2968", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d10w plus 20meq kpo4 1000ml NU 03C8", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d10w plus 50meq nahco3 1000ml NU 2699", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d10 and nacl 0.2 plus 20meq kcl 1000ml NU EBC7", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d10 and nacl 0.45 plus 20meq kcl 1000ml NU 4BE1", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d10 and nacl 0.45 plus 20meq kpo4 1000ml NU D341", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d10 and nacl 0.45 plus 50meq nahco3 1000ml NU 8553", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH eio in cry d12.5w 500 mL", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d12.5w 1000ml NU 6F4A", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d12.5 and nacl 0.45 1000ml NU 8E62", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH eio in cry d15w 500 mL", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d15w 1000ml NU B082", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d15w and amino acids 10 percent 1000ml NU 0AA2", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH eio in cry d17.5w 500 mL", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH eio in cry d20w 500 mL", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d20w 1000ml NU E78A", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d20w and amino acids 10 percent 1000ml NU 6177", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d20w plus 20meq kcl 1000ml NU 8C7B", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d20w plus 20meq kpo4 1000ml NU E0C9", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d20w plus 50meq nahco3 1000ml NU BC8B", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d20 and nacl 0.2 1000ml NU 4070", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d20 and nacl 0.45 1000ml NU 05AD", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d25w 1000ml NU 3D08", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry d25w and amino acids 10 percent 1000ml NU 37F3", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH eio in cry lr 500mL", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH eio in cry lr 500mL+ 10mEq KCl", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH eio in cry lr 500mL+ 20mEq KCl", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry lr 1000ml NU 1421", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry lr plus 10meq kcl 1000ml NU 109A", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry lr plus 20meq kcl 1000ml NU 6F71", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry lr plus 30meq kcl 1000ml NU 7120", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry lr plus 40meq kcl 1000ml NU 2F81", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry lr plus 20meq kpo4 1000ml NU 755D", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry lr plus 50meq nahco3 1000ml NU EBE4", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry mannitol 5 percent NU A325", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry mannitol 10 percent NU ED94", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry mannitol 15 percent NU AE09", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry mannitol 20 percent NU B680", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry mannitol 25 percent NU 2913", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH eio in cry nacl 1.5% 500 mL", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH eio in cry nacl 0.45 500mL", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "snch eio in cry nacl0.45 500ml+10meq kcl", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry nacl 0.45 1000ml NU DDF4", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry nacl 0.45 plus 10meq kcl 1000ml NU 5691", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry nacl 0.45 plus 20meq kcl 1000ml NU C3DC", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry nacl 0.45 plus 30meq kcl 1000ml 371B", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry nacl 0.45 plus 40meq kcl 1000ml NU 47A2", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry nacl 0.45 plus 20meq kpo4 1000ml NU 3384", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry nacl 0.45 plus 50meq nahco3 1000ml NU 149F", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH eio in nacl 0.9 500mL", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "snch eio in cry nacl0.9 500ml+10meq kcl", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "snch eio in cry nacl0.9 500ml+20meq kcl", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry nacl 0.9 1000ml NU C5B3", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry nacl 0.9 plus 10meq kcl 1000ml NU EA64", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH eio NS0.9 100+MVI+Thiame+FolicAcid", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry nacl 0.9 plus 20meq kcl 1000ml NU A1E3", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry nacl 0.9 plus 40meq kcl 1000ml NU 6B04", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry nacl 0.9 plus 20meq kpo4 1000ml NU 8DB5", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry nacl 0.9 plus 50meq nahco3 1000ml NU 5C16", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH eio in cry nacl 3% 500 mL", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry nacl 3 percent 1000ml NU CE2C", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH eio in cry nacl 5% 500 mL", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry nacl 5 percent 1000ml NU 3FF2", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry na bicarbonate NU A36A", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry na acetate NU 2EF9", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in cry na hydroxide NU CEF4", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH eio in cry Non Formulary IV Fluid", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip abciximab NU DB3D", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip acyclovir NU", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip alprostadil NU 309F", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH EIO in drip glucagon NU", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip alteplase NU DCE7", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip amino 3%+Glycerin3%+Lytes NU", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip amino 5.4% NU", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip amino 8.5% NU", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip aminocaproic acid NU 2B12", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip aminophylline NU 567A", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip amiodarone NU 267D", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip amphotericin B NU", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip amphotericin B Lipid Complex NU", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip argatroban NU 4ADC", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH EIO in drip Bivalirudin NU", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip bumetanide NU 3CD2", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip cefoxitin NU", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip cisatracurium NU 805F", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip Clevidipine NU SNCH", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip clindamycin NU", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip dexmedetomidine NU CE32", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip diltiazem NU EEC4", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip dobutamineacls NU F16E", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip dopamine NU A8DB", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip dopamineacls NU 3DC0", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip epinephrine NU 5A92", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip eptifibatide NU F8CF", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip esmolol NU 5114", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip fentanyl NU E4E8", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip furosemide NU 9AA6", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH_EIO in drp Heparin NU", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip hydromorphone NU F712", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH EIO in drip Remifentani NU", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH EIO in drip Rocuronium NU", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip indomethacin NU", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH_EIO in drp Insulin NU", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip isoproteranol NU B1CD", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH EIO in drip Ketamine NU", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip labetalol NU 25FE", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip lidocaine NU D1E8", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip lorazepam NU AB8F", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip magnesium sulfate NU 4AD6", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH EIO in drip Magnesium Sulfate Loading Dose", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip midazolam NU AB9C", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip milrinone NU 0C8E", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip morphine NU D08E", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip naloxone NU A160", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip nesiritide NU 7A5A", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip nicardipine NU A2AB", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip nitroglycerine NU 4531", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip nitroprusside NU A0D6", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip norepinephrine NU 6214", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip octreotide NU 11AD", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH_EIO in drp Oxytocin NU", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip pancuronium NU F91B", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH EIO in drip pantoprazole NU", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip phenylephrine NU 42ED", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip procainamide NU 2ED2", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip propofol NU 7CC2", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip propranolol NU", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip sufentanil NU EF29", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip terbutaline NU FF31", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip thiopental NU D0A4", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH_EIO in drp Vasopressin NU", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in drip vecuronium NU 805F", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO Order Based Drip", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in tf brst milk NU D783", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in tf free water bolus NU 7994", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "SNCH eio tf formula tube feeding", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in tf glucose 5 percent water NU 4469", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in enteral tube flush NU", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in tf Glucerna NU", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in tf Jevity NU", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in tf Nepro NU", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in tf Osmolyte NU", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in tf Pulmocare NU", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in tf Suplena NU", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in tf Two Cal NU", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(10, "", "EIO in tf Vital NU", "", "", reslist);



        }

        private void CountFluids()
        {
            string descript;
            string drugclass;
            bool newbag = false;
//OBX|1|DT|MED9811^DEXTROSE 2.5 % AND 0.45 % SODIUM CHLORIDE INTRAV;;;29;;;50;;;IV;;;NewBag|NewBag|20170609120700||||||F|||20170609120700

            var query = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
            query = query.Where(e => e.CODE.StartsWith("med"));
            query = query.Where(e => e.EVENT_DATETIME <= _pat.unit_departure);
            query = query.OrderBy(e => e.EVENT_DATETIME);
            foreach (var item in query)
            {
                var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                Program.VerboseAudit("Count Fluids ub=" + arr.GetUpperBound(0) + " descript="+item.DESCRIPTION);
                if (arr.GetUpperBound(0) >= 4)
                {
                    descript = arr[0];
                    drugclass = arr[1];
                    newbag = (arr[4].ToLower() == "newbag" || arr[4].ToLower() == "new bag" || arr[4].ToLower().Contains("bolus"));
                    if ((drugclass == "29") && (newbag))
                    {
                        SetInd(10, "Found Fluids in Meds with NewBag:" + item.EVENT_DATETIME.ToString() + "; drugclass=" + drugclass + "; descript=" + descript);
                    }
                }
            }

        }

        private void Check_1112()
        {
            string reslist;
            int ct;
            List<int> buckets;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Visit 11. Physiologic Assessment q1hr");
            Program.VerboseAudit("ED Visit 12. Physiologic Assessment q15min");
            Program.VerboseAudit("---------------");

            SetIndIfResultContains(12, "", "ED Nurse Note v1", "", "", "10.25  PT CODED SEE CODE SHEET");
            if (_trauma_triage_was_set)
                SetInd(12, "Trauma Triage Nursing Note was found.");

            SetBucketSize(60);
            AnalyzeAssessments(11, 60);

            SetBucketSize(15);
            AnalyzeAssessments(12, 15);

            SetBucketSize(60);

        }
        private void AnalyzeAssessments(int ind, int bucket_size)
        {
            string codelist;
            string reslist;
            List<gBucket> buckets;
            string freqstr = "";


            if (bucket_size == 60) freqstr = "====Q1 HR EVALUATION================";
            if (bucket_size == 15) freqstr = "====Q15 MIN EVALUATION====================";
            Program.VerboseAudit(freqstr + " bucket size=" + bucket_size + "  _bucket size=" + _bucket_size);

            //        Vitals:
            string assessgrouplabel = "Vitals";
            buckets = new List<gBucket>();
            codelist = "EVS bp arterial systolic NU 89F8,EVS bp arterial diastolic NU 1CD2,CVS bp noninvasive systolic mmhg NU";
            codelist += ",CVS bp noninvasive diastolic mmhg NU,EVS orthostat bp systolic lying NU 398B,EVS orthostat bp diastolic lying NU 3211";
            codelist += ",EVS orthostat bp systolic sitting NU 8577,EVS orthostat bp diastolic sitting NU 34CC,EVS orthostat bp systolic stand NU BE32";
            codelist += ",EVS orthostat bp diastolic stand NU A902,CVS hr heart rate beats per min NU,CVS resp rate breaths per min NU";
            codelist += ",SNCH EVS ETCO2 NU,CVS temp fahrenheit CAL,CVS temp celsius CAL,CVS spo2 percent NU,EVS cvp mmhg NU 662C";
            codelist += ",EVS cvp cm h2o NU 3720,EVS pap systolic NU 89BD,EVS pap diastolic NU A7A3,EVS pap mean CAL  56F5";
            codelist += ",EVS pap wedge NU D40D,EVS icp NU C327,EVS icp monitor cpp read ACAL";
            AddBuckets(buckets, "", codelist, "", "");
            codelist = "snch cpe edp mark pert sys wdl";
            AddBuckets(buckets, "", codelist, "", "wdl");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            //Cardiovascular:
            assessgrouplabel = "Cardiovascular";
            buckets = new List<gBucket>();
            codelist = "cas edn cardio assess,cas edn peripheral vascular,cas edn musc assess";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            //Pulmonary:
            assessgrouplabel = "Pulmonary";
            buckets = new List<gBucket>();
            codelist = "cas edn resp assess,cas edn eent assess";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            //Neuro:
            assessgrouplabel = "Neuro";
            buckets = new List<gBucket>();
            codelist = "cas edn neuro assess,cas edn psychsoc assess";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            //Gi / GU Fluid Mgt:
            assessgrouplabel = "Gi/GU";
            buckets = new List<gBucket>();
            codelist = "cas edn gastro assess,cas edn genitourinary assess";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            //Wound:
            assessgrouplabel = "Wound";
            buckets = new List<gBucket>();
            codelist = "cas edn integ assess";
            AddBuckets(buckets, "", codelist, "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

            //Medication:
            assessgrouplabel = "Medication";
            buckets = new List<gBucket>();
            codelist = "AS pain presence";
            AddBucketsResDoesNotContain(buckets, "", codelist, "", "", "", "denies");
            codelist = "AS pain rest NU,AS pain activity NU,AS pain word rest,AS pain word activity";
            codelist += ",AS pain faces rest,AS pain faces activity,AS SC rflacc rest score CAL,AS SC painad score CAL";
            codelist += ",AS pain preferred scale infant,AS SC cries score CAL,AS SC nips score CAL,AS SC pipp score CAL";
            codelist += ",AS SC npass pain score CAL,AS SC npass sed score CAL,EVS pain infusion type 2957,SNCH EVS pca mg med D489";
            codelist += ",SNCH EVS pca mcg med D489,AS SC cpot score CAL,AS SC painad score CAL,AS nerve block site assessment";
            codelist += ",AS SC rass scale,Opioid POSS Scale SR,snch bc titr start event dt";
            AddBuckets(buckets, "", codelist, "", "");
            codelist = "AS drain type";
            AddBuckets(buckets, "", codelist, "", "analgesia infusion");
            AnalyzeBuckets(buckets, ind, bucket_size, assessgrouplabel);

        }

        private bool AnalyzeBuckets(List<gBucket> buckets, int ind, int bucketsize, string group, bool set_ind)
        {
            DateTime dt = DateTime.MinValue;
            int bnum = 0;
            int numbucket = -99;
            int lobucket = 99;
            int hibucket = 0;
            int numitems = 0;
            int numconsec = 0;
            int greatestnumconsec = 0;
            int num_unique_times = 0;
            DateTime prev_time = DateTime.MinValue;
            bool all_ok = false;
            List<gBucket> dtlist = new List<gBucket>();
            Program.VerboseAudit("----Begin Assessment Group = " + group + " ---- bucketsizew=" + bucketsize + " start time of first bucket=" + _pat.pull_start);
            //if (ind==18 && Math.Round(_pat.los_hours) <= 4.0)
            //{
            //    all_ok = DoShortLOSAssessEval(buckets,ind,bucketsize,group,set_ind);
            //    Program.VerboseAudit("---- End Assessment Group = " + group + " ----");
            //    return all_ok;
            //}

            //Program.VerboseAudit("buckets count=" + buckets.Count());
            var b = buckets.OrderBy(e => e.evdt).ToList();

            //numitems = buckets.Count();
            foreach (var item in b)
            {
                if (prev_time != item.evdt)
                {
                    num_unique_times++;
                    prev_time = item.evdt;
                }
                if (numbucket < item.bucket)
                {
                    numbucket = item.bucket;
                    numitems++;
                    Program.VerboseAudit(item.bucket + ") " + item.evdt.ToString() + " := " + item.code);
                }
            }

            int num_buckets_in_los = (int)(_pat.los_hours * (60.0 / bucketsize));//for q30 this will be 2xlos_hours.  for q60 this will be los_hours
            Program.VerboseAudit("total bucket count in LOS=" + num_buckets_in_los);
            Program.VerboseAudit("num buckets filled=" + numitems);
            //q30 75% of 30-min buckets	3/4 x 24  = 18	3/4 x 12  = 9	3/4 x 8  = 6	3/4 x 4  = 3	5 or more VS
            //q1 66% of 60-min buckets	2/3 x 12  = 8	2/3 x 6  = 4	2/3 x 4  = 2.67=>3	2/3 x 2  = 1.33=>1	3 VS + examine 30min 
            //q2 50 % of 60-min buckets	.5 x 12  = 6	.5 x 6  = 3	.5 x 4  = 2	.5 x 2  = 1	2 VS
            //q4 33% of 60-min buckets	1/3 x 12  = 4 2	1/3 x 6  = 2 1	1/3 x 4  = 1.33=>1	1/3 x 2  = .67=>1	1 VS
            int q15need = (int)Math.Round(0.75 * 4 * _pat.los_hours);
            int q1need = (int)Math.Round(0.667 * _pat.los_hours);
            int q2need = (int)Math.Round(0.5 * _pat.los_hours);
            int q4need = 1;
            if (ind < 12)
            {
                if (_pat.los_hours > 5)
                {
                    if (numitems >= q1need)
                    {
                        SetInd(11 * Convert.ToInt32(set_ind), "Qualifies for q1 because numcharted=" + numitems + " is >=" + q1need + " LOS=" + Math.Round(_pat.los_hours, 2) + " x .667=" + q1need);
                        all_ok = (ind <= 11);
                    }
                }
                else //short los
                {
                    q1need = 3;
                    q2need = 2;
                    q4need = 1;

                    if (numitems >= q1need)
                    {
                        SetInd(11 * Convert.ToInt32(set_ind), "ShortLOS Qualifies for q1 because numcharted=" + numitems + " is >=" + q1need + " LOS=" + Math.Round(_pat.los_hours, 2) + " needs just 3");
                        all_ok = (ind <= 11);
                    }
                }
            }
            else // ind==12
            {
                if (_pat.los_hours > 2)
                {
                    if (numitems >= q15need)
                    {
                        SetInd(12 * Convert.ToInt32(set_ind), "Qualifies for q30 because numcharted=" + numitems + " is >=" + q15need + " LOS=" + Math.Round(_pat.los_hours, 2) + " x 2 x .75=" + q15need);
                        all_ok = (ind <= 12);
                    }
                }
                else  //for los<=2 need 3 items for q15 is 
                {
                    q15need = 6;
                    if (num_unique_times >= q15need)
                    {
                        SetInd(12 * Convert.ToInt32(set_ind), "ShortLOS Qualifies for q30 because numcharted=" + numitems + " is >=" + q15need + " LOS=" + Math.Round(_pat.los_hours, 2) + " with num_unique_times=" + num_unique_times + " Need:" + q15need);
                        all_ok = (ind <= 12);
                    }
                }
            }
            Program.VerboseAudit("---- End Assessment Group = " + group + " ----");
            return all_ok;
        }

        private int SetMaxWaivers(int imins)
        {
            string qstr = "";
            string rstr = "";
            double losx = _pat.los_hours * 60;
            int num_waivers = 0;

            if (imins == 20)
            {//then 'q15
                if (losx >= 180)
                    num_waivers = 2;
                else if (losx >= 80)
                    num_waivers = 1;
                else
                    num_waivers = 0;
            }
            else if (imins == 70) // Then 'q1
            {
                if (losx < 360)
                    num_waivers = 0;
                else
                    num_waivers = (int)losx / 360;
            }
            else if (imins == 40) // Then 'q30
            {
                if (losx < 180)
                    num_waivers = 0;
                else
                    num_waivers = (int)losx / 180;
            }
            if (imins == 20)
                qstr = "q15min";
            else if (imins == 70)
                qstr = "q1hr";
            else if (imins == 40)
                qstr = "q30min";
            Program.VerboseAudit(qstr + ":Max waivers allowed=" + num_waivers + " (LOS=" + losx + ")");

            return num_waivers;
        }

        private void AnalyzeBuckets(List<gBucket> buckets, int ind, int bucketsize, string group)
        {
            DateTime dt = DateTime.MinValue;
            int bnum = 0;
            List<gBucket> dtlist = new List<gBucket>();
            Program.VerboseAudit("---- Begin Assessment Group = " + group + " ----");

            var b = buckets.OrderBy(e => e.evdt).ToList();
            foreach (var item in b)
            {
 Program.VerboseAudit(item.evdt.ToString() + " := " + item.code);
                if (dt < item.evdt)
                {
                    //add dt to ary
                    bnum++;
                    dtlist.Add(item);
                    dt = item.evdt;
                    Program.VerboseAudit("Adding time: " + item.evdt.ToString() + " with " + item.code);
                }
            }
Program.VerboseAudit("bnum=" + bnum);

            int i, j, istart;
            double addmins = _pat.los_hours * 30.0; // 60/2.0;
 Program.VerboseAudit("addmins := " + addmins);
            int minupperidx = 0;
            bool all_ok = false;
            int j_cannot_use_waiver = 0;
            int imins = bucketsize;
            int num_waivers = SetMaxWaivers(bucketsize);
            int w = num_waivers;
            DateTime upperdt;
            gBucket[] dtary = dtlist.ToArray();
            for (i=0; i <= bnum-2; i++)
            {
                istart = i;
                upperdt = dtary[i].evdt.AddMinutes(addmins);
Program.VerboseAudit("i:" + i + " dtary[i].evdt = " + dtary[i].evdt.ToString() + "=> upperdt: " + upperdt.ToString());
                //Program.VerboseAudit("i:="+i+"  upperdt:=" + upperdt.ToString());

                //what is the min evdt >= upperdt?
                minupperidx = 0;
                if (addmins >= bucketsize / 2.0)
                {
                    for (j = i + 1; j <= bnum - 1; j++)  //For j = i + 1 To b2num
                    {
                        //Program.VerboseAudit("dtary[" + j + "].evdt:=" + dtary[j].evdt.ToString());
                        if (dtary[j].evdt >= upperdt)
                        {
Program.VerboseAudit("j:" + j + " dtary[j].evdt = " + dtary[j].evdt.ToString() + "<=> upperdt: " + upperdt.ToString());
                            minupperidx = j;
                            //dvprint "min upper idx=" & minupperidx
                            //dvprint "min upper time=" & bucket2(j).eventdt
                            j = bnum - 1 + 1;
                        }
                    } // j
                }

Program.VerboseAudit("minupperidx=" + minupperidx);
                //Program.VerboseAudit("minupperidx:=" + minupperidx);
                if (minupperidx == 0)  //then half LOS not possible
                {
                    all_ok = false;
                    i = bnum-1; //end loop
                }
                else
                {
                    j_cannot_use_waiver = 0;
                    all_ok = true;
                    //Program.VerboseAudit("dtary[" + i + "].evdt:=" + dtary[i].evdt.ToString()); //dvprint "i time=" & bucket2(i).eventdt
                    for (j = i; j <= minupperidx - 1; j++) //For j = i To minupperidx -1
                    {
 Program.VerboseAudit("j:" + j + " dtary[j].evdt = " + dtary[j].evdt.ToString() + " imins = " + imins + " dtary[j+1].evdt = " + dtary[j+1].evdt.ToString() + "  w="+ w + "  j_cannot_use_waiver=" + j_cannot_use_waiver);
                        if (dtary[j].evdt.AddMinutes(imins) < dtary[j + 1].evdt)
                            if (w > 0)  //then 'we can use a waiver
                                if (j != j_cannot_use_waiver) //  Then 'we can
                                    if (dtary[j].evdt.AddMinutes(2 * imins) >= dtary[j + 1].evdt)
                                    {
                                        dtary[j].using_waiver = true;
                                        j_cannot_use_waiver = j + 1;
                                        w = w - 1;
                                    }
                                    else
                                        all_ok = false;
                                else
                                    all_ok = false;
                            else
                                all_ok = false;

                    }
                    //For j = i To minupperidx -1
//                    If DateAdd("n", imins, bucket2(j).eventdt) < bucket2(j + 1).eventdt Then
//                        If w > 0 Then 'we can use a waiver
//                      If j <> j_cannot_use_waiver Then 'we can
//'dvprint "check within sequence"
//                        If DateAdd("n", 2 * imins, bucket2(j).eventdt) >= bucket2(j + 1).eventdt Then
//                            bucket2(j).using_waiver = True
//                            j_cannot_use_waiver = j + 1
//                            w = w - 1
//                        Else
//                            all_ok = False
//                        End If
//                      Else
//                        all_ok = False
//                      End If
//                    Else
//                      all_ok = False
//                    End If
//                End If
//            Next j
//            If all_ok Then


                    if (all_ok)
                        i = bnum; // 'end loop
                    else
                        //'reset waivers
                        for (j = 0; j <= bnum-1; j++)
                            dtary[j].using_waiver = false;
                }

            }

        if (all_ok)
            SetInd(ind, "Qualifies for q" + imins + "mins for duration of half-LOS=" + addmins.ToString() + " minutes.");
        else
        { 
            Program.VerboseAudit("Does not meet frequency criteria for indicator #" + ind);
            //            'assign indexes for the dump to follow
            istart = 0;
            minupperidx = bnum-1;
        }

        if (num_waivers > w)
        {
            int w2 = 0;
            for (j=0; j < bnum; j++)
                if (dtary[j].using_waiver)
                {
                    w2++;
                    Program.VerboseAudit("Waiver " + w2 + ": " + dtary[j].evdt.AddMinutes(imins).ToString());
                }
        }

            Program.VerboseAudit("---- End Assessment Group = " + group + " ----");

            //sql = "select event_datetime,category,description,field_name,result from chart_item " & WhereBase & b_filter & b_excl
            //sql = sql & " and event_datetime between " & g_dbutil.SQL_DateTime(bucket2(istart).eventdt)
            //sql = sql & " and " & g_dbutil.SQL_DateTime(bucket2(minupperidx).eventdt) & " order by event_datetime"
            //'dvprint sql
            //rs.Open sql, g_cnADO
            //Do While Not rs.EOF
            //    dprint "  " & rs(0) & ": " & g_dbutil.DBToString(rs(1)) & "; " & g_dbutil.DBToString(rs(2)) & "; " & g_dbutil.DBToString(rs(3)) & "; " & g_dbutil.DBToString(rs(4))
            //    rs.MoveNext
            //Loop
            //rs.Close


            //ResetWaivers



        }

        private void CountAssessments(int ind, int bucket_size)
        {
            int ct;
            string codelist;
            string reslist;
            List<gBucket> buckets;

            SetBucketSize(bucket_size);

            //
            //VS group
            //
            buckets = new List<gBucket>();
            codelist = "304987655,9990007096285";
            codelist += ",9990000006294,9993041120042,3045001041";
            codelist += ",9990304001499,3045001018,9993040103255";
            AddBuckets(buckets, "", codelist, "", "");

            bool has_trach = Exists("", "9990007070177,9990007070178,9990007070179", "", "", "");
            if (_pat.age >= 8.0 && !has_trach)
                AddDependentBuckets(buckets, "304987657,3045001025", "", "3045001064,30454321", "", "304987666,9990304100017,9993049900009", "");
            else
                AddDependentBuckets(buckets, "304987657,3045001025", "", "3045001064,30454321", "");
            reslist = "Non-rebreather mask,Nasal cannula,High flow,Aerosol mask";
            reslist += ",Bag valve device,BiPAP,Blow-by,CPAP,Face tent,Isolette,Oxyhood";
            reslist += ",Oxymask,Partial rebreather mask,Reservoir nasal cannula,Simple mask";
            reslist += ",T-piece,Trach mask,Ventilator,Venturi mask";
            reslist += ",Speaking valve,Incubator,Bag-self inflating,Bag-flow inflating,Helium oxygen,Sub-ambient Oxygen";
            AddDependentBuckets(buckets, "30454321,9990000006294", "", "3045001064", "", "3045001051", reslist);
            AnalyzeBuckets(buckets, ind, bucket_size, "VS");
            //ct = CountBuckets(buckets);
            //CheckAssessment(ct, "VS=" + ct);
            //ShowBuckets(buckets);


            //
            //Pulmonary group
            //
            buckets = new List<gBucket>();
            codelist = "3045001051";
            reslist = "Non-rebreather mask,Nasal cannula,High flow,Aerosol mask";
            reslist += ",Bag valve device,BiPAP,Blow-by,CPAP,Face tent,Isolette,Oxyhood";
            reslist += ",Oxymask,Partial rebreather mask,Reservoir nasal cannula,Simple mask";
            reslist += ",T-piece,Trach mask,Ventilator,Venturi mask";
            reslist += ",Speaking valve,Incubator,Bag-self inflating,Bag-flow inflating,Helium oxygen,Sub-ambient Oxygen";
            AddDependentBuckets(buckets, codelist, reslist, "30454321", "", "3045001064", "");

            codelist = "3045001052,3045001053";
            AddDependentBuckets(buckets, codelist, "", "30454321", "", "3045001064", "");
            codelist = "9990000302570";
            reslist = "Agonal,Apnea,Bradypnea,Cheyne-Stokes,Kussmaul";
            reslist += ",Obstructed,Periodic,Regular,Tachypnea";
            AddDependentBuckets(buckets, codelist, reslist, "3045001064", "");
            codelist = "9993040109339";
            reslist = "Regular,Irregular,Shallow,Deep";
            AddDependentBuckets(buckets, codelist, reslist, "3045001064", "");
            codelist = "9993040109337";
            reslist = "Labored,Unlabored";
            AddDependentBuckets(buckets, codelist, reslist, "3045001064", "");

            codelist = "9993040109338";
            reslist = "Abdominal muscle use,Accessory muscle use,Asymetrical chest excursion,Drooling,Gasping";
            reslist += ",Grunting,Head bobbing,Nasal flaring,Pursed lips,Retractions,Tripod position";
            AddDependentBuckets(buckets, codelist, reslist, "3045001064", "");

            codelist = "9990000302580";
            reslist = "Symmetrical,Asymmetrical,Trachea deviates right,Trachea midline";
            reslist += ",Trachea deviates left,Paradoxical,Sunken chest,Pigeon chest";
            reslist += ",Subcutaneous emphysema,No chest expansion,Cylinder shaped";
            reslist += ",Flattened,Right clavicular deformity,Left clavicular deformity,Crepitus";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000302390,9990000302400,9990000302410";
            reslist = "Clear,Diminished,Fine,Coarse,Rales,Rhonchi,Crackles";
            reslist += ",Expiratory wheezes,Inspiratory wheezes,Stridor,Pleural rub";
            reslist += ",Tubular,Absent";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000451120,9990000451080,9990000451040";
            reslist = "Copious,Large,Moderate,Small,None";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000315800";
            reslist = "Bed therapy,Cough assist,CPAP,EzPAP,Flutter valve";
            reslist += ",IPV device,Manual percussion,NT suction,PEP Therapy";
            reslist += ",Percusser,Postural drainage,Vibralung,Vest";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000325950";
            reslist = "Tollerated well,Tollerated fairly well,Tolerated poorly,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);
            codelist = "9991733666660";
            reslist = "Apnea,Bradycardia,Desaturation,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000344210,9990000316380,9990000344220,9990000344230,9990007096450";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "9990000344250";
            reslist = "Blow-by oxygen,Oxygen,Positive pressure ventilation,Self limiting,Suction,Tactile stimulation,Position Change";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000344280";
            reslist = "Aminophylline,Caffeine,High flow oxygen,Intubated,Medication dose change,Nasal cannula,Nasal CPAP";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000006808,3041733124512,9991733666661";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "9991600100035,9991600100039";
            reslist = "Point of care,To lab";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991600100048";
            reslist = "Oral mouthpiece,Mask,Trach,Ventilator,NPPV,Blow-by,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990160238401,9990160238501";
            reslist = "Clear,Diminished,Absent,Crackles,Stridor,Wheeze,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);
            codelist = "9993040104500";
            reslist = "Scant,Small,Moderate,Copious,Thin,Thick,Clear,Blood tinged,Tan,White,Yellow";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000400613,9993040102736,9993040102752,9993040106219,9993040106220,9993040106221,9993040106222";
            AddBuckets(buckets, "", codelist, "", "", "");

            codelist = "9993040108077,9993040108078,9993040108079";
            AddBuckets(buckets, "", codelist, "", "", "");

            AnalyzeBuckets(buckets, ind, bucket_size, "Pulmonary");
            //ct = CountBuckets(buckets);
            //CheckAssessment(ct, "Pulmonary=" + ct);
            //ShowBuckets(buckets);

            //
            //Cardiovascular group
            //
            buckets = new List<gBucket>();
            codelist = "3045001065";
            reslist = "SR,SB,Sinus rhythm,Sinus bradycardia,Sinus tachycardia,Sinus arrest";
            reslist += ",Sinus arrhythmia,A-paced,V-paced,A-V paced,Atrial paced,Ventricular paced,A-V Sequential paced";
            reslist += ",Agonal,Asystole,A-fib,A-fib w/RVR,Atrial fibrillation";
            reslist += ",Atrial flutter,Heart block,Junctional accelerated,Junctional rhythm";
            reslist += ",Junctional tachycardia,PEA,SVT,Pulseless electrical activity,Supraventricular tachycardia,Ventricular fibrillation,Ventricular tachycardia";
            reslist += ",Torsades,VF,VT,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);
            reslist = EXACT_MATCH_PREFIX + "ST";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "3045001066";
            reslist = "1st degree AVB,2nd degree AVB (Mobitz I, Wenckebach)";
            reslist += ",2nd degree AVB (Mobitz II),3rd degree AVB,Bundle branch block,Idioventricular";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "3045001067";
            reslist = "PVCs,Premature ventricular contractions,Unifocal PVCs,Multifocal PVCs";
            reslist += ",Couplet PVCs,Triplet PVCs,Premature atrial contractions,PACs,Aberrent conduction";
            reslist += ",Ectopic beats,Fusion beats,Junctional escape beats,Non-conducted PACs";
            reslist += ",Paroxysmal atrial tachycardia,Paroxysmal supraventricular tachycardia,PSVT,PJCS";
            reslist += ",Premature junctional contractions,Ventricular escape beats";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040108650,9993040108651,9993040108652,9993040108653";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "9993040100446";
            reslist = "S1,S2,S3,S4,Click,Device,Distant,Friction rub";
            reslist += ",Gallop,Holosystolic murmur,Mechanical valve click,Muffled";
            reslist += ",Murmur,Pericardial rub,No adventitious heart sounds";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040101320";
            //reslist = "Off,A paced,V paced,A/V paced,AAI,AAI-DDD (MVP),AOO,DVI,DOO,DDD";
            //reslist += ",VVI,VOO,VDD,AAR,AAR-DDDR (MVP),AOOR,DVR,DOOR,DOI,DDDR,DDR,VVR";
            //reslist += ",VOOR,VDDR";
            reslist = "";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040101316,9993040101317";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "9993040001044";
            reslist = "Red,Pink,Pale,Dusky,Purple,Mottled,Ecchymotic,Soft,Firm";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040108605,9993040108606,9993040108607";
            reslist = "Red,Pink,Pale,Dusky,Purple,Mottled,Ecchymotic,Soft,Firm";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040001045";
            reslist = "Greater than 3 seconds,Less than or equal to 3 seconds,Brisk,Sluggish";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040108611,9993040108612,9993040108613";
            reslist = "Greater than 3 seconds,Less than or equal to 3 seconds,Brisk,Sluggish";
            AddBuckets(buckets, "", codelist, "", "", reslist);


            codelist = "9993040001048,9993040001049";
            reslist = "Verified,Absent";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000303200,9990000303210,9990000303220,3045001045,9990000303270,9990000303280";
            codelist += ",9990000303320,9990000303330,3045001012,3045001014,9990000303390";
            codelist += ",9990000303400,3045001013,3045001015";
            reslist = "0,+1,+2,+3,+4,Doppler";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "3045001004,3045001002,3045001003,3045001001";
            reslist = "Less than/equal to 2 seconds,Greater than/equal to 3 seconds,No return";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000302900,9993045006597,9990000302920,9993045006598,9993040101373";
            codelist += ",9993040101374,9993040101375,9993040101376,3040000000022,99930400000027";
            codelist += ",3040000000028,9993040000062,99930400000056,3040000000050,9993040000044";
            codelist += ",9993040000035,9993040000045,9993040000049,9993047096403,9993040000050";
            codelist += ",9993040000064,9993040000065,3045001056,3045001055";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "9991733888889";
            reslist = "Acrocyanosis,Capillary refill no return,Capillary refill sluggish (>2 seconds)";
            reslist += ",Circumoral,Cyanosis" + CHAR_COMMA + " centralized,Cyanosis" + CHAR_COMMA + " peripheral";
            reslist += ",Generalized,Localized,Mottled,Pale,Ruddy";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000302650";
            reslist = "Absent,Murmur,Other";
            reslist += ",S1,S2,S3,S4,Distant,Friction rub,Click,Metal valve click,Gallop,Muffled";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991733888890";
            reslist = "0,+3,+1,Unequal,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991733999991";
            reslist = "Moderate,Non-pitting,Severe,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040100223";
            reslist = "Circumocular,Circumoral,Nailbed,Acrocyanosis,Facial";
            reslist += ",Generalized,Oral mucosa,Underlying,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "3045001000";
            reslist = "Normal";
            reslist += ",Sluggish,1 to 2 seconds,3 seconds,4 seconds,5 seconds";
            reslist += ",No return,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000303180,9990000303240,9990000303300,9990000303370";
            reslist = "Acrocyanosis,Appropriate for ethnicity,Ashen,Black,Bronze";
            reslist += ",Dusky,Ecchymosis,Flushed,Gray,Jaundiced";
            reslist += ",Mottled,Pale,Pink,Purple,Red,Ruddy,Unable to assess,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990007061360";
            reslist = "Capillary refill less than 3 sec,Cool fingers";
            reslist += ",Capillary refill greater than 3 sec,Dusky fingers";
            reslist += ",Numbness to fingers,Pale fingers";
            reslist += ",Tingling to fingers,Weakness to fingers";
            AddBuckets(buckets, "", codelist, "", "", reslist);
            codelist = "9993040103299,9993040103300";
            reslist = "Present,Absent";
            AddBuckets(buckets, "", codelist, "", "", reslist);
            codelist = "9993040001046,9993040108614,9993040108615,9993040108616";
            reslist = "Verified,Absent";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040101321,9993040101322,9993040101323,9993040101324";
            reslist = "0,+1,+2,+3,+4,Doppler";
            AddBuckets(buckets, "", codelist, "", "", reslist);
            codelist = "99930434002,9993043034001";
            reslist = "0,+1,+2,+3,+4,Doppler,Sheath In";
            AddBuckets(buckets, "", codelist, "", "", reslist);
            codelist = "9993040107859,9993040107862";
            AddBuckets(buckets, "", codelist, "", "", "");

            AnalyzeBuckets(buckets, ind, bucket_size, "Cardiovascular");

            //ct = CountBuckets(buckets);
            //CheckAssessment(ct, "Cardiovascular=" + ct);
            //ShowBuckets(buckets);

            //
            //Neurological group
            //
            buckets = new List<gBucket>();
            codelist = "9993040101409";
            reslist = "0/4,1/4,2/4,3/4,4/4";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "3045001007,3045001039,9993040006316,9990304006317,9993040006318";
            codelist += ",9993040006319,9993040006320,9990000301930,9990000301910";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "9990000301920,9990000301900";
            reslist = "Brisk,Sluggish,Nonreactive,0,-2,-4";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040101162,9993040101163";
            reslist = "Absent,Weak,Moderate,Strong,Contracture";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000002280,9990000002279";
            reslist = "Present,Absent";
            AddBuckets(buckets, "", codelist, "", "", reslist);
            codelist = "3045001017";
            reslist = "Present,Absent,0,-2,-4,C";
            AddBuckets(buckets, "", codelist, "", "", reslist);


            codelist = "3045001016,9993040101166,9993040101167";
            reslist = "Absent,Weak,Moderate,Strong,Contracture,0,-2,-4,C";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000301980,9990000301940,9990000302000,9990000301960";
            reslist = "Responds to commands,Normal extension,Normal flexion,Tremors";
            reslist += ",Flaccid,Abnormal extension,Abnormal flexion";
            reslist += ",Movement to painful stimulus,No movement to painful stimulus";
            reslist += ",Non-purposeful movement,No tremor,Spastic";
            AddBuckets(buckets, "", codelist, "", "", reslist);
            reslist = STARTS_WITH + "0,-1,-2,-3,-4,P,SP,NP,Stim,Pain,DC,DB";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000301990,9990000301950,9990000302010,9990000301970";
            reslist = "Decreased,No sensation,Numbness,Pain,Tingling,Full sensation";
            reslist += ",No numbness,No pain,No tingling";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040102775,9993040102776,9993040102777,9993040102778";
            reslist = "Normal power,Cannot overcome resistance,Overcomes gravity";
            reslist += ",Can overcome resistance,Flicker of muscle,None";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000302190,9990000302180";
            reslist = "C1,C2,C3,C4,C5,C6,C7,C8,T1,T2,T3,T4,T5,T6,T7,T8,T9,T10,T11,T12";
            reslist += ",L1,L2,L3,L4,L5,S1,S2,S3,S4,S5";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "3045001023,9993040001207,3045001080,3045001081,9990000398011";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "3045001079";
            reslist = "-5,-4,-3,-2,-1,0,+1,+2,+3,+4"; //Any number between -5 and + 4
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040104675";
            reslist = "S,1,2,3,4";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040001216,3045001011";
            reslist = "Positive,Negative";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000450430,9990000450420";
            reslist = "Present,Weak,Absent";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000450990,9990000450410";
            reslist = "Intact,Impaired,Absent";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040103168,9993040103169";
            reslist = "Absent,Present";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040109102,9990000450470";
            reslist = "Absent,Present,Weak";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000450480";
            reslist = "Absent,Present,Weak,Strong,Coordinated,Uncoordinated,Gag present,Gag absent";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "3045001047";
            AddBuckets(buckets, "", codelist, "", "");
            
            codelist = "9993040001091"; 
            reslist = "Blindness - right,Blindness - left,Blindness,Blurred vision";
            reslist += ",Visual field cut- right side,Visual field cut- right upper";
            reslist += ",Visual field cut- right lower,Visual field cut- left side";
            reslist += ",Visual field cut- left upper,Visual field cut- left lower";
            reslist += ",Visual field cut,Diplopia";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000002216";
            reslist = "Strabismus - right,Strabismus - left,Droopy eyelid - right";
            reslist += ",Droopy eyelid - left,Double vision - right,Double vision - left,Unable to assess";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000002217";
            reslist = "Double vision - right,Double vision - left";
            reslist += ",Unable to look downward and inward - right,Unable to look downward and inward - left";
            reslist += ",Unable to assess";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000002218";
            reslist = "Unable to chew - right,Unable to chew - left";
            reslist += ",Unable to clench - right,Unable to clench - left";
            reslist += ",Absence of sensation - right,Absence of sensation - left";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000002219";
            reslist = "Medial strabismus -  right,Medial strabismus -  left";
            reslist += ",Unable to look laterally - right,Unable to look laterally - left";
            reslist += ",Double vision - right,Double vision - left";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000002220";
            reslist = "Facial paralysis - right,Facial paralysis - left,Loss of taste - right,Loss of taste - left";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000002221";
            reslist = "Unable to hear - right,Unable to hear - left";
            reslist += ",Ringing in ear - right,Ringing in ear - left";
            reslist += ",Involuntary eye movement - right,Involuntary eye movement - left";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000002222";
            reslist = "Altered gag reflex";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000002223";
            reslist = "Weakness in turning head - right,Weakness in turning head - left";
            reslist += ",Unable to shrug - right,Unable to shrug - left";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000002224";
            reslist = "Deviation of tongue - right,Deviation of tongue - left";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040001057";
            AddBuckets(buckets, "", codelist, "", "");
            codelist = "9990000302300";
            reslist = "Clear,Cloudy,Serous,Sanguineous,Serosanguineous";
            reslist += ",Purulent,Cherry,Straw,Yellow";
            AddBuckets(buckets, "", codelist, "", "");
            codelist = "9990000002230,9990000304510,9993040109138";
            AddBuckets(buckets, "", codelist, "", "");


            codelist = "9991600100259";
            reslist = "No untoward effects noted,Use of reversal agent(s)";
            reslist += ",Hypoxemia < 90% for > 1 min,Hypotension of bradycardia requiring intervention";
            reslist += ",Respiratory failure requiring intervention,Cardiac arrest or death";
            reslist += ",Sedation recovery time > 60 min,Unplanned admission or higher level of care";
            reslist += ",Respiratory distress,Unanticipated need for anesthesia involvement";
            reslist += ",Inability to complete procedure,No responsible adult for discharge escort";
            reslist += ",Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991070011101,99910701000111";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "9990000450560";
            reslist = "Aggression,Aura,Behavior pause,Bowel incontinence,Deja vu";
            reslist += ",Fearful,Giggles,Nausea,Oral Trauma,Smirks,Tremors,Urine incontinence,Vocalize";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000450570";
            reslist = "Eyes right,Eyes left,Head right,Head left,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000450580,9993040108685,9993040108686";
            reslist = "Head,Face,Eye,Hand,Arm,Leg,Foot,Jerking,Stiffening";
            reslist += ",Staring,Tonic-clonic Motion,Twitching,Drop";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000450590";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "9990000450600";
            reslist = "Somnolence,Aphasic,RUE paresis,LUE paresis,RLE paresis,LLE paresis";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000450620";
            reslist = "Aware of seizure,Word given,Word recalled,Word not recalled";
            reslist += ",Normal speech,Abnormal speech,Unable to respond";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040101196";
            reslist = "Generalized,Right,Left,Hand,Leg,Face,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040101198";
            reslist = "2,3,4,5,6,7,8,9,10"; // num > 1
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040100079";
            reslist = "Absent,Arching back or neck,Bicycling,Clonic jerking";
            reslist += ",Extension is greater than flexion,Jerky,Jittery/tremors";
            reslist += ",Lip smacking,Movements cease with containment,Movements continue despite containment";
            reslist += ",Rowing,Seizure activity,Tonic extension,Tonic flexion,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991733999990";
            reslist = "Bicycling,Eye deviation,Lip smacking,Movement ceases with containment";
            reslist += ",Movements continue despite containment,Tongue thrusting,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040102780";
            reslist = "Abnormal reflex,Dorsiflexion,Extension,Frantic movement,Grip";
            reslist += ",Inconsolable,Lethargic,Motor response,Motor strength,Pronator Drift,Plantar flexion,Sensation,Wiggle";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991140100006";
            reslist = "Quiet alert,Sleeping,Active alert,Lusty cry,Drowsy,Active with stimulation";
            reslist += ",Hoarse cry,Irritable,Jittery,Lethargic,Shrill cry,Weak cry";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040100076";
            reslist = "Hypertonic generalized,Hypertonic localized,Hypotonic generalized,Hypotonic localized,Other";
            reslist += ",Decreased,Flaccid,Increased,Myoclonus,Tone appropriate for age";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000451750";
            reslist = "Absent,Present,Weak,Brisk,Clonus,Clonus Sustained";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000450440";
            reslist = "Absent,Asymmetric,Symmetric,UTA";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000450450";
            reslist = "Absent,Present,Weak,UTA";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000450460";
            reslist = "Absent,Present,Clonus,Hyperreflexive,Hyporeflexive,Weak,UTA";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040109103";
            reslist = "Absent,Present,Weak,Unable to assess";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9993040109104";
            reslist = "Absent,Asymmetrical,Symmetrical,Unable to assess";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991140100013,9991140100014";
            //reslist = "Boggy,Bulging,Caput saecundum,Cephalohematoma,Closed,Depressed";
            //reslist += ",Sunken,Flat,Full,Soft,Sutures approximated";
            AddBuckets(buckets, "", codelist, "", "", reslist);
            AnalyzeBuckets(buckets, ind, bucket_size, "Neurological");
            //ct = CountBuckets(buckets);
            //CheckAssessment(ct, "Neurologic=" + ct);
            //ShowBuckets(buckets);

            //
            //OB assessment group
            //
            buckets = new List<gBucket>();
            codelist = "9991025006827";
            reslist = "Denies,Blurred,Floaters,Flashes,Decreased visual field,Hx. of visual disturbances";
            reslist += ",Unsure,Other";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991025006826";
            reslist = "Denies,Present,Unsure";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991025006828";
            reslist = "Denies,Aching,Burning,Constant,Intermittent,Sharp,Unsure";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991025006829";
            reslist = "Denies,Present";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991025006830";
            reslist = "Denies,Nausea only,Vomiting,Dry Heaves";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000012301,9990000012302,9990000012303,9990000012305";
            reslist = "absent,diminished,normal,brisk,brisk/hyperactive,sustained";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000012306,9990000012304";
            reslist = "Absent,1 beat,2 beats,3 beats,4 beats,Sustained";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "1028000004,1028000001,9990102521012,1028000003,9991020100645,9991020100655";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "1028000007,1028000008,1028000009,9991020100648,9991020100657";
            reslist = "Normal,Bradycardia,Tachycardia,Indeterminate";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "1028000010,1028000011,1028000012";
            reslist = "Minimal,Moderate,Marked,Absent";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "1028000013,1028000014,1028000015";
            reslist = "15x15,10x10,Absent";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "1028000016,1028000017,1028000018";
            reslist = "None,Early,Variable,Late,Prolonged ,Intermittent,Recurrent";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991025051116,9991025111616";
            AddBuckets(buckets, "", codelist, "", "");

            codelist = "1028000022,1028000023,1028000024,9991020100652,9991020100661";
            reslist = "Category I,Category II,Category III";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000012331";
            reslist = "Reactive,Non-reactive";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000012332";
            reslist = "Reassuring,Non - reassuring";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "1028000040";
            reslist = "Intact,Spontaneous,AROM,PROM,PPROM,Bulging";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "128000043";
            reslist = "Clear,Meconium,Bloody,Purulent";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "128000045";
            reslist = "Scant,Small,Moderate,Large";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "1028000046";
            reslist = "Closed,Fingertip,1,2,3,4,5,6,7,8,9,Lip/rim,10";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991020100568";
            reslist = "Vaginal pack,Uterine balloon,Uterotonic agent given (see MAR)";
            reslist += ",Oxygen on,Legs elevated,MD notified,Bimanual exam per MD,Labs ordered";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9991020100569";
            reslist = "Placed,Present,Removed";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000012127";
            reslist = "Firm,Firm with massage,Boggy";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000012128";
            reslist = "Midline,Right,Left";
            AddBuckets(buckets, "", codelist, "", "", reslist);

            codelist = "9990000012309,9991025012136,9991025012137,9991025012139,9991025012144";
            AddBuckets(buckets, "", codelist, "", "", "");

            codelist = "9993040102317,9993040102318,9993040102319,9993040102320";
            AddBuckets(buckets, "", codelist, "", "", "");

            AnalyzeBuckets(buckets, ind, bucket_size, "OB");

            //ct = CountBuckets(buckets);
            //CheckAssessment(ct, "OB Assess=" + ct);
            //ShowBuckets(buckets);
            buckets = new List<gBucket>();
            codelist = "9993040108530,3045001091";
            AddBuckets(buckets, "", codelist, "", "", "");
            AnalyzeBuckets(buckets, ind, bucket_size, "Drains");


        }

        // (this isn't really a Q1h count -- it is just a count) Make this a q1hr count
        private int ReturnQ1HrCount(string code_list, string result_list)
        {
            var buckets = new List<gBucket>();
            SetBucketSize(60);
            AddBuckets(buckets, "", code_list, "", "", result_list);
            return CountBuckets(buckets);
        }

        private bool IsQ1Hour(int count)
        {
            return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q1H);
        }
        private bool IsQ15(int count)
        {
            return (FreqForCount(_pat.los_hours, count) >= Frequencies.Q15M);
        }

        // Use this if counting safety buckets
        //private void CheckSafety(int count, string desc)
        //{
        //    if (_inds[13].is_checked) return;             //skip if highest already checked
        //    if (count == 0) return;
        //    switch (FreqForCount(_pat.los_hours, count))
        //    {
        //    case Frequencies.Q30M:
        //        SetInd(13, desc + " q30min");
        //        break;
        //    case Frequencies.Q1H:
        //    case Frequencies.Q2H:
        //        SetInd(12, desc + " q2h");
        //        break;
        //    default:
        //        Program.VerboseAudit(desc + ": " + count + " in " + Math.Round(_pat.los_hours) + " hours is not enough");
        //        break;
        //    }
        //}


        //        Now that we have examples for our medication interface:

        //•	The drug class field is the first number that appears after the drug name and;;;

        //The drug class should help us separate out the meds from the fluids.

        //•	The Medication indicator(#13) includes most of the categories. 
        //Do not include the drug class of #29, 35, 48 as these are not really medications. 
        //We are looking for a count > or = 5 medications in any of the other drug classes. 
        //All 5 can be in the same or in different categories, just looking for >= 5 “final” messages coming in.

//•	The Fluid Mgmt IV route indicator (#10) is the drug class of #29 with the “New bag” charted


//Let me know if this type of mapping will work.Samples are below from our earlier testing.
//Once mapped, should be easy to verify. (I need to let Ken know that these are two great indicators for transparency if this works!)



//2017-06-09 12:15:20.24 HL7 Information Processed

//R01 - Unsolicited Observation: QUADRAMED, STORK : Acct=2000000923294, MRN=11212327, Type='I', Location=EI33 REI3OR2 P 6/1/2017 12:28:00 PM

//MSH|^~\&|Epic||||20170609121515||ORU^R01|6261|T|2.5.1| 
//PID|||11212327||QUADRAMED^STORK^^^^^D||19940601|F||||||||||2000000923294
//OBX|1|DT|MED128467^ACETAMINOPHEN 650 MG/20.3 ML ORAL SOLUTION;;;1;;;650;;;gastric tube;;;Given|Given|20170609121500||||||F|||20170609121500

//Patient QUADRAMED, STORK  acct=2000000923294



//2017-06-09 12:07:40.09 HL7 Information Processed

//R01 - Unsolicited Observation: QUADRAMED, STORK : Acct=2000000923294, MRN=11212327, Type='I', Location=EI33 REI3OR2 P 6/1/2017 12:28:00 PM

//MSH|^~\&|Epic||||20170609120730||ORU^R01|6255|T|2.5.1| 
//PID|||11212327||QUADRAMED^STORK^^^^^D||19940601|F||||||||||2000000923294
//OBX|1|DT|MED9811^DEXTROSE 2.5 % AND 0.45 % SODIUM CHLORIDE INTRAVENOUS SOLUTION;;;29;;;50;;;IV;;;NewBag|NewBag|20170609120700||||||F|||20170609120700

//Patient QUADRAMED, STORK  acct=2000000923294


        private void Check_13()
        {
            string desclist="";
            int ct = 0;
            int medct = 0;

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Visit 13. Medication Management >= 5 Meds");
            Program.VerboseAudit("---------------");

            desclist = "Transfuse 1 unit of Red Blood Cells,Transfuse 1 unit of Platelets,Transfuse 1 unit of Frozen Plasma,Transfuse 1 unit of Cryoprecipitate,Transfuse 1 unit of Special Blood Product,Transfuse 1 unit of Albumin";
            SetIndIfResultContains(13, "", "XS", desclist, "", "");

            ct = CountMeds();
            if (ct >= 5)
                SetInd(13, "Num meds found >=5 : " + ct);

        }

        private int CountMeds()
        {
            string descript;
            string drugclass;
            int ct = 0;
            List<med_data> mlist = new List<med_data>();
            bool toadd = true;
            bool desc_contains_phrase = false;

            var query = StartNewQuery(SearchDepth.SearchDefault);    // add custom time range below
            query = query.Where(e => e.CODE.StartsWith("med"));
            query = query.Where(e => e.EVENT_DATETIME <= _pat.unit_departure);
            query = query.OrderBy(e => e.EVENT_DATETIME);
            ct = query.Count();
            Program.VerboseAudit("Num Meds found: " + query.Count());
            if (ct < 5) return 0;

            foreach (var item in query)
            {
                Program.VerboseAudit("====Med found====");
                Program.VerboseAudit("code: " + item.CODE + " desc: " + item.DESCRIPTION + " evdt: " + item.EVENT_DATETIME.ToString());

                var arr = item.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                Program.VerboseAudit("Count Meds ub=" + arr.GetUpperBound(0) + " descript=" + item.DESCRIPTION);
                //if (arr.GetUpperBound(0) >= 1)
                //{
                //    descript = arr[0];
                //    drugclass = arr[1];
                //    Program.VerboseAudit("MED desc: " + descript + " drugclass: " + drugclass);
                //    descript = descript.ToLower();
                //    desc_contains_phrase = (descript.Contains("0.9 % SODIUM CHLORIDE".ToLower())
                //        || descript.Contains("D5".ToLower())
                //        || descript.Contains("LACTATED RINGERS".ToLower())
                //        || descript.Contains("NACL 0.45".ToLower())
                //        || descript.Contains("NACL 0.9".ToLower())
                //        || descript.Contains("SALINE FLUSH INJ".ToLower())
                //        || descript.Contains("SODIUM CHLORIDE 0.45".ToLower())
                //        || descript.Contains("SODIUM CHLORIDE 0.9".ToLower()));
                //    //exclude #29 with exceptions, 35, 48
                //    if ((drugclass != "29" || (drugclass == "29" && !desc_contains_phrase)) && (drugclass != "35") && (drugclass != "48"))
                //    {
                //        var md = new med_data();
                //        md.code = item.CODE.ToUpper();
                //        md.descript = descript;
                //        md.drugclass = drugclass;
                //        md.evdt = item.EVENT_DATETIME;
                //        toadd = true;
                //        //if (ct > 0)
                //        //{ 
                //        //    foreach (var m in mlist)
                //        //    {
                //        //        if (m.code == item.CODE.ToUpper()) toadd = false;
                //        //    }
                //        //}
                //        if (toadd && (ct < 5))
                //        {
                //            mlist.Add(md);
                //            ct++;
                //        }

                //    }
                //}
            }

            int a = 0;
            Program.VerboseAudit("Meds found=" + ct);
            foreach (var m in mlist)
            {
                Program.VerboseAudit("  " + a++ + ": code=" + m.code + "; drugclass=" + m.drugclass + "; time=" + m.evdt.ToString() + "; descript=" + m.descript);
            }
            return ct;
        }

        private void CheckAssessment(int count, string desc)
        {
            //if (_inds[12].is_checked) return;          //skip if highest already checked
            if (count == 0) return;                    //skip if none

            // This should work the same as the original code:
            switch (FreqForCount(_pat.los_hours, count))
            {
                case Frequencies.Q15M:
                    SetInd(12, desc);
                    break;
                case Frequencies.Q1H:
                    SetInd(11, desc);
                    break;
                //case Frequencies.Q2H:
                //    SetInd(16, desc);
                //    break;
                //case Frequencies.Q4H:
                //    SetInd(15, desc);
                //    break;
                default:
                    break;
            }

        }
        private void ShowBuckets(List<gBucket> buckets)
        {
            string s = "";
            foreach (var e in buckets)
            {
                s = "(" + e.bucket + "," + e.code + "," + e.evdt.ToString() + ")";
                Program.VerboseAudit("Assessmt item:=" + s);
            }
        }

        private void Check_14()
        {
            string reslist="";

            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Visit 14. Wound/Injury Management");
            Program.VerboseAudit("---------------");


            //reslist = "";
            //SetIndIfResultContains(14, "", "snch ed int wnd mr", "", "", reslist);

            //reslist = "";
            //SetIndIfResultContains(14, "", "cas edn skin wound additional", "", "", reslist);
            DateTime evdt = DateTime.MinValue;
            string res;
            res = "burn";
            if (LookAtEDNurseNote("ED Nurse Note", "Skin Integrity", res, out evdt))
                SetInd(14, "ED Nurse Note: Skin Integrity " + res + " found at: " + evdt);
            res = "incision";
            if (LookAtEDNurseNote("ED Nurse Note", "Skin Integrity", res, out evdt))
                SetInd(14, "ED Nurse Note: Skin Integrity " + res + " found at: " + evdt);
            res = "pressure ulcer";
            if (LookAtEDNurseNote("ED Nurse Note", "Skin Integrity", res, out evdt))
                SetInd(14, "ED Nurse Note: Skin Integrity " + res + " found at: " + evdt);
            res = "wound";
            if (LookAtEDNurseNote("ED Nurse Note", "Skin Integrity", res, out evdt))
                SetInd(14, "ED Nurse Note: Skin Integrity " + res + " found at: " + evdt);

            //reslist = "";
            //SetIndIfResultContains(14, "", "cas edn skin burn additional", "", "", reslist);

            if (_trauma_triage_was_set)
                SetInd(14, "Trauma Triage Level 1 or 2 was found.");

        }

        private bool LookAtEDNurseNote(string code, string res1, string res2, out DateTime evdt)
        {
            bool ret = false;
            evdt = DateTime.MinValue;

            var query = StartNewQuery();
            query = query.Where(e => e.CODE.ToUpper().StartsWith(code.ToUpper()));
            query = query.Where(e => e.RESULT.ToUpper().Contains(res1.ToUpper()));
            query = query.Where(e => e.RESULT.ToUpper().Contains(res2.ToUpper()));
            query = query.OrderByDescending(e => e.EVENT_DATETIME);
            ret = (query.Count() > 0);
            if (ret)
            {
                evdt = query.First().EVENT_DATETIME;
                _trauma_triage_was_set = true; //will trigger wound mgt
            }

            return ret;
        }


        private bool CheckPIV()
        {
            var query2 = StartNewQuery(SearchDepth.SearchPullPlus);
            query2 = AndDescriptionInList(query2, "Placement,Removal,Dressing");
            query2 = AndDescriptionInList(query2, "Midline Dual Lumen Catheter,Midline Single Lumen Catheter,Peripheral IV");
            query2 = query2.Where(e => e.EVENT_DATETIME <= _pat.unit_departure.AddMinutes(15));
            int ct = query2.Count();
            return (ct > 0);
        }

        private void Check_15()
         {
            string reslist;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Visit 15. Urgent Intervention > 1 staff");
            Program.VerboseAudit("---------------");

            reslist = "1";
            SetIndIfResultContains(15, "", "cas esi trg acuity", "", "", reslist);

            SetIndIfResultContains(15, "", "ED Nurse Note v1", "", "", "10.25  PT CODED SEE CODE SHEET");

            if (esi_trauma_1)
                SetInd(15, "Trauma Triage Nursing Note was found.");

        }

        private void Check_16()
        {
            string found_what;
            string reslist, negreslist;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Visit 16. Educational Needs");
            Program.VerboseAudit("---------------");

            //reslist = "all discharge instructions have been explained, discharge instructions given";
            //SetIndIfResultContains(16, "", "snch ed lb int d/c mr", "", "", reslist);

            DateTime evdt;
            if (LookAtEventNote("ED Nurse Note v1", char183 + " Discharge Instructions", "all discharge instructions have been", out evdt))
                SetInd(16, "ED Nurse Note on Education" + " at" + evdt);
            if (LookAtEventNote("ED Nurse Note v1", char183 + " Discharge Instructions", "given to", out evdt))
                SetInd(16, "ED Nurse Note on Education" + " at" + evdt);

        }

        private bool LookAtEventNote(string code, string res1, string res2, out DateTime evdt)
        {
        bool ret = false;
        evdt = DateTime.MinValue;

        var query = StartNewQuery();
        query = query.Where(e => e.CODE.ToUpper().StartsWith(code.ToUpper()));
        query = query.Where(e => e.RESULT.ToUpper().Contains(res1.ToUpper()));
        query = query.Where(e => e.RESULT.ToUpper().Contains(res2.ToUpper()));
        query = query.OrderByDescending(e => e.EVENT_DATETIME);
        ret = (query.Count() > 0);
        if (ret)
        {
            evdt = query.First().EVENT_DATETIME;
        }

        return ret;
        }


    private void CheckEDUtab(string educode)
        {
            int ub,i;
            string[] desc = new string[3];
            DateTime evdt;
            string cd1,cd2,res1,res2,topic1,topic2;

            //OBX | 2 | ST | EDU3040004179METHOD ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC STROKE EVERY MINUTE MATTERS MC6074| 1 | E |||||| F ||| 20170612113300
            //OBX | 4 | ST | EDU3040004179RESPONSE ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC STROKE EVERY MINUTE MATTERS MC6074| 1 | TB |||||| F ||| 20170612113300
            //OBX | 6 | ST | EDU3040004179METHOD ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC YOU CAN IMPROVE YOUR HEALTH MC2066 - 09 | 1 | E |||||| F ||| 20170612113300
            //OBX | 8 | ST | EDU3040004179RESPONSE ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC YOU CAN IMPROVE YOUR HEALTH MC2066 - 09 | 1 | TB |||||| F ||| 20170612113300
            //OBX | 10 | ST | EDU3040004179METHOD ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC HIGH BLOOD PRESSURE (HYPERTENSION)MC5056 | 1 | E |||||| F ||| 20170612113300
            //OBX | 12 | ST | EDU3040004179RESPONSE ^ MC STROKE; ; ; MC STROKE PREVENTION; ; ; MC HIGH BLOOD PRESSURE (HYPERTENSION)MC5056 | 1 | TB |||||| F ||| 20170612113300
            var query1 = StartNewQuery(SearchDepth.SearchPullPlus);
            query1 = AndItemFilter(query1, "", "EDU"+ educode +"METHOD", "", "", "E,D,I");
            query1 = query1.Where(e => e.EVENT_DATETIME <= _pat.unit_departure.AddMinutes(15));
            foreach (var item1 in query1)
            {
                evdt = item1.EVENT_DATETIME;
                res1 = item1.RESULT;
                cd1 = item1.CODE;
                var arr = item1.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                ub = arr.GetUpperBound(0);
                for (i = 0; (i <= Math.Min(ub,2)); i++)
                    desc[i] = arr[i];
                if (ub >= 2)
                    topic1 = desc[2].Trim();
                else
                    topic1 = "";
                var query2 = StartNewQuery(SearchDepth.SearchPullPlus);
                query2 = AndItemFilter(query2, "", "EDU" + educode + "RESPONSE", "", "", "IP,TB,NR");
                query2 = query2.Where(e => e.EVENT_DATETIME == evdt);
                foreach (var item2 in query2)
                {
                    res2 = item2.RESULT;
                    cd2 = item2.CODE;
                    var arr2 = item2.DESCRIPTION.Split(new string[] { ";;;" }, StringSplitOptions.None);
                    ub = arr.GetUpperBound(0);
                    for (i = 0; (i <= Math.Min(ub, 2)); i++)
                        desc[i] = arr[i];
                    if (ub >= 2)
                        topic2 = desc[2].Trim();
                    else
                        topic2 = "";
                    if (topic1 == topic2)
                    {
                        SetInd(16, "Found EDU" +educode+": " + desc[0] + "^" + topic1 + " at:"+evdt.ToString() + " METHOD="+res1+ " RESPONSE="+res2);
                    }
                }

            }

        }


        private void Check_17()
        {
            string reslist;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Visit 17. One-to-one Continuous Supervision Off Unit");
            Program.VerboseAudit("---------------");

            //reslist = "RN transported to unit";
            //SetIndIfResultContains(17, "", "sn_ed eas disch accompanied by", "", "", reslist);

            DateTime evdt;
            if (LookAtEventNote("ED Nurse Note v1", char183 + " Accompanied By" + char9, "RN transported to unit", out evdt))
                SetInd(17, "ED Nurse Note on Education" + " at" + evdt);

        }

        private void Check_18()
        {
            string reslist;
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Visit 18. Procedure >= 30 min");
            Program.VerboseAudit("---------------");

            DateTime evdt;
            if (LookAtEventNote("ED Triage Note Adult v1", char183 + " Visit Reason" + char9, "code", out evdt))
                SetInd(18, "ED Triage Note Adult v1: CODE" + " at" + evdt);
            if (LookAtEventNote("ED Triage Note Adult v1", char183 + " Visit Reason" + char9, "unresponsive", out evdt))
                SetInd(18, "ED Triage Note Adult v1: UNRESPONSIVE" + " at" + evdt);

            reslist = "Anoscopy, Arterial Puncture/Cannulation, Arthrocentesis, Cardioversion, Central Line Insertion, Chest Tube, Dental Block, External Jugular Line Insertion, Feeding Tube Replacement, Foreign Body Removal, Fracture Reduction, Gastric Intubation/Gastric Lavage, General, Incision & Drainage, Intraosseous Infusion, Joint Dislocation Reduction, Laceration Repair, Lumbar Puncture, Nasal Procedures, Paracentesis, Pericardiocentesis, Peripheral Line Insertion, PICC Line Insertion, Procedural Sedation, Regional Block/Digital, Regional Block/Radial, Thoracentesis, Tracheal Intubation, Transcutaneous Pacemaker, Transvenous Pacemaker, Urinary Device Placement";
            SetIndIfResultContains(18, "", "cproc ed name1", "", "", reslist);

            reslist = "Anoscopy, Arterial Puncture/Cannulation, Arthrocentesis, Cardioversion, Central Line Insertion, Chest Tube, Dental Block, External Jugular Line Insertion, Feeding Tube Replacement, Foreign Body Removal, Fracture Reduction, Gastric Intubation/Gastric Lavage, General, Incision & Drainage, Intraosseous Infusion, Joint Dislocation Reduction, Laceration Repair, Lumbar Puncture, Nasal Procedures, Paracentesis, Pericardiocentesis, Peripheral Line Insertion, PICC Line Insertion, Procedural Sedation, Regional Block/Digital, Regional Block/Radial, Thoracentesis, Tracheal Intubation, Transcutaneous Pacemaker, Transvenous Pacemaker, Urinary Device Placement";
            SetIndIfResultContains(18, "", "cproc ed name2", "", "", reslist);

            reslist = "Anoscopy, Arterial Puncture/Cannulation, Arthrocentesis, Cardioversion, Central Line Insertion, Chest Tube, Dental Block, External Jugular Line Insertion, Feeding Tube Replacement, Foreign Body Removal, Fracture Reduction, Gastric Intubation/Gastric Lavage, General, Incision & Drainage, Intraosseous Infusion, Joint Dislocation Reduction, Laceration Repair, Lumbar Puncture, Nasal Procedures, Paracentesis, Pericardiocentesis, Peripheral Line Insertion, PICC Line Insertion, Procedural Sedation, Regional Block/Digital, Regional Block/Radial, Thoracentesis, Tracheal Intubation, Transcutaneous Pacemaker, Transvenous Pacemaker, Urinary Device Placement";
            SetIndIfResultContains(18, "", "cproc ed name3", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(18, "", "Initiate Massive Transfusion Protocol", "", "", reslist);

            reslist = "";
            SetIndIfResultContains(18, "", "ED Consult Order to Telemed Psychiatry", "", "", reslist);

            reslist = " ";
            SetIndIfResultContains(18, "", "Consult Order to Telemed Stroke", "", "", reslist);


        }

        private void Check_192021()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("ED Visit 19. Admitted");
            Program.VerboseAudit("ED Visit 20. Transferred to External Facility");
            Program.VerboseAudit("ED Visit 21. Expired");
            Program.VerboseAudit("---------------");

            if (!isEDonly)
                SetInd(19, "Patient has non-ED locations.");

            string[] discharge_status = { "02", "03", "04", "05", "62", "63", "65", "69", "70", "72", "75", "76"};
            foreach (var s in discharge_status)
            {
                if (_pat.dc_status == s)
                {
                    SetInd(20, "Found Transferred Status at discharge=" + s + "="+_pat.dc_status_str);
                }
            }

            if (_pat.dc_status == "08")
                SetInd(21, "Discharge status = expired.");

        }


        //private bool IsICU()
        //{
        //    switch (_pat.unit_name)
        //    {
        //        case "ICU":
        //        case "3BR":
        //        case "CTIC":
        //        case "NIC":
        //        case "PCU":
        //        case "TICU":
        //        case "CCB":
        //        case "RICU":
        //        case "CCUS":
        //            return true;
        //        default:
        //            return false;
        //    }
        //}

        //private bool IsTele()
        //{
        //    switch (_pat.unit_name)
        //    {
        //        case "4EST":
        //        case "2NW":
        //        case "3AE":
        //        case "5MEH":
        //        case "5NW":
        //        case "6MEH":
        //        case "6NW":
        //        case "7MEH":
        //        case "B4W":
        //        case "B5S":
        //        case "B5W":
        //        case "2NOB":
        //        case "CPLX":
        //        case "TELMS":
        //        case "MAN4":
        //            return true;
        //        default:
        //            return false;
        //    }
        //}



        //=====================================================================
        // Group like assessments into buckets of time
        // Each bucket counts as one assessment in the frequency count

        private void SetBucketSize(int minutes)
        {
            _bucket_size = minutes;
        }

        //Add to the list of bucket numbers (redundant buckets are fine)
        private void AddBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field)
        {
            AddBuckets(bucket_list, cat, code_list, desc, field, "");
        }
        private void AddBuckets(List<gBucket> bucket_list, string cat, string code_list, string desc, string field, string result_list)
        {
            // Assign (virtual) bucket numbers to each assessment.  
            // Every x minutes after that is a new bucket.  Bucket zero = pull_start time.

            // get the chart items for the assessments
            var query = StartNewQuery(SearchDepth.SearchPullPlus);
            query = AndItemFilter(query, cat, code_list, desc, field, result_list);

            // This step is needed for those who want to count the # within a bucket, like Shands #24.
            // This will have no effect on those that count buckets.
            var query2 = (from item in query select new { item.EVENT_DATETIME, item.CODE });

            // figure out what buckets the events belong to
            var query3 = from item in query2
                         select new
                         {
                             bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                             code = item.CODE,
                             evdt = item.EVENT_DATETIME
                         };
            // Add to the list
            foreach (var item in query3)
            {
                var b = new gBucket();
                b.bucket = item.bucket;
                b.code = item.code;
                b.evdt = item.evdt;
                bucket_list.Add(b);
            }

            // This is just for auditing: Say what we are looking for and how many were found
            if (!String.IsNullOrEmpty(result_list)) {
                // print each word and if it was found or not
                int i = CountResultContains(cat, code_list, desc, field, result_list);
            } else {
                // print how many were found
                Program.VerboseAudit(Describe(cat, code_list, desc, field, ""));
            }
        }
        private void AddBucketsResDoesNotContain(List<gBucket> bucket_list, string cat, string code_list, string desc, string field, string result_list, string negating_res)
        {
            // Assign (virtual) bucket numbers to each assessment.  
            // Every x minutes after that is a new bucket.  Bucket zero = pull_start time.

            // get the chart items for the assessments
            var query = StartNewQuery(SearchDepth.SearchPullPlus);
            query = AndItemFilter(query, cat, code_list, desc, field, result_list);
            query = AndResultNotInList(query, negating_res);

            // This step is needed for those who want to count the # within a bucket, like Shands #24.
            // This will have no effect on those that count buckets.
            var query2 = (from item in query select new { item.EVENT_DATETIME, item.CODE });

            // figure out what buckets the events belong to
            var query3 = from item in query2
                         select new
                         {
                             bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                             code = item.CODE,
                             evdt = item.EVENT_DATETIME
                         };
            // Add to the list
            foreach (var item in query3)
            {
                var b = new gBucket();
                b.bucket = item.bucket;
                b.code = item.code;
                b.evdt = item.evdt;
                bucket_list.Add(b);
            }

            // This is just for auditing: Say what we are looking for and how many were found
            if (!String.IsNullOrEmpty(result_list))
            {
                // print each word and if it was found or not
                int i = CountResultContains(cat, code_list, desc, field, result_list);
            }
            else
            {
                // print how many were found
                Program.VerboseAudit(Describe(cat, code_list, desc, field, ""));
            }
        }

        private void AddDependentBuckets(List<gBucket> bucket_list, string codelist1, string reslist1, string codelist2, string reslist2)
        {
            AddDependentBuckets(bucket_list, codelist1, reslist1, codelist2, reslist2, "", "");
        }

        private void AddDependentBuckets(List<gBucket> bucket_list, string codelist1, string reslist1, string codelist2, string reslist2, string codelist3,string reslist3)
        {
            bool dep3 = true;
            // get the chart items for the assessments
            var query1 = StartNewQuery();
            query1 = AndItemFilter(query1, "", codelist1, "", "", reslist1);
            var query2 = StartNewQuery();
            query2 = AndItemFilter(query2, "", codelist2, "", "", reslist2);
            if (codelist3.Trim() == "")
            {
                dep3 = false;
                codelist3 = "Hello, this is a phantom code";
            }
            var query3 = StartNewQuery();
            query3 = AndItemFilter(query3, "", codelist3, "", "", reslist3);

            // figure out what buckets the events belong to
            var query1a = from item in query1
                          select new
                          {
                              bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                              code = item.CODE,
                              evdt = item.EVENT_DATETIME
                          };
            var query2a = from item in query2
                          select new
                          {
                              bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                              code = item.CODE,
                              evdt = item.EVENT_DATETIME
                          };
            var query3a = from item in query3
                              select new
                              {
                                  bucket = (int)(PFSUtility.DateDiffInMinutes(_pat.pull_start, item.EVENT_DATETIME) / _bucket_size),
                                  code = item.CODE,
                                  evdt = item.EVENT_DATETIME
                              };
            
            //string s = "BucketList1 for " + codelist1 + ": ";
            //foreach (var item in query1a)
            //{
            //    s += item.bucket + ",";
            //}
            //Program.VerboseAudit(s);

            //s = "BucketList2 for " + codelist2 + ": ";
            //foreach (var item in query2a)
            //{
            //    s += item.bucket + ",";
            //}
            //if (dep3)
            //{
            //    s = "BucketList3 for " + codelist3 + ": ";
            //    foreach (var item in query3a)
            //    {
            //        s += item.bucket + ",";
            //    }
            //}
            //Program.VerboseAudit(s);
            // Add to the list IFF items in both lists occur in same bucket
            foreach (var item1 in query1a)
            {
                foreach (var item2 in query2a)
                {
                    if (item1.bucket == item2.bucket)
                    {
                        if (dep3)
                        {
                            foreach (var item3 in query3a)
                            {
                                if (item1.bucket == item3.bucket)
                                {
                                    var b = new gBucket();
                                    b.bucket = item1.bucket;
                                    b.code = item1.code;
                                    b.evdt = item1.evdt;
                                    bucket_list.Add(b);
                                }
                            }
                        }
                        else
                        {
                            foreach (var item3 in query2a)
                            {
                                if (item1.bucket == item2.bucket)
                                {
                                    var b = new gBucket();
                                    b.bucket = item1.bucket;
                                    b.code = item1.code;
                                    b.evdt = item1.evdt;
                                    bucket_list.Add(b);
                                }
                            }
                        }
                    }
                }
            }

        }


        private int CountBuckets(List<gBucket> bucket_list)       // bucket list: (ha ha)
        {
            int x = -99;
            int result = 0;
            var query = from b in bucket_list
                        orderby b.bucket ascending
                        select b;
            foreach (var b in query)
            {
                if (x != b.bucket)
                {
                    result++;
                    x = b.bucket;
                }
            }
            if (result > 0) Program.VerboseAudit(result + " unique");
            return result;

        }


        private void AtLeastOneADL()
        {
            Program.Audit("---------------");
            if (!(_inds[2].is_checked || _inds[3].is_checked || _inds[4].is_checked)) {
                // Make "#2 ADL - Assist" the default.  (90% of patients)
                SetInd(2, "Defaulting to ADL Self due to lack of documentation.");
            }
        }

        private void HighestIndicatorInEachGroupWins()
        {
            int i, g;
            bool highest_is_on;
            string ind_list;

            Program.Audit("Select highest indicator in each group");
            
            g = 0;
            highest_is_on = false;
            // Go from bottom up - highest (lowest) indicator in each group wins
            for (i = MAX_INDS; (i >= 1); i--) {
                if (_inds[i].radio_group > 0) {
                    if (_inds[i].radio_group != g) {
                        //this is a new group
                        g = _inds[i].radio_group;
                        highest_is_on = _inds[i].is_checked;
                    } else {
                        //same group
                        if (highest_is_on) {
                            _inds[i].is_checked = false;             //uncheck a lower number
                        } else {
                            highest_is_on = _inds[i].is_checked;     //save this one
                        }
                    }
                }
            } // next i
            
            //Echo the indicators for an Audit (no classification will be saved)
            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++) {
                if (_inds[i].is_checked) ind_list += "," + i;
            } // next i

            Program.Audit("Final list = " + ind_list.Substring(1));
            Program.Audit("---------------");
        }


        private void CheckProcs()
        {
            //CheckProc_1();
            //CheckProc_2();
            //CheckProc_3();
            //CheckProc_4();
            //CheckProc_5();
            //CheckProc_6();
            //CheckProc_7();
            //CheckProc_8();
            //CheckProc_9();
            //CheckProc_10();
            //CheckProc_11();

        }

        private void DoProc(int pnum, string code)
        {
            double mins = 0;
            string found_what;
            DateTime evdt;
            DateTime enddt = DateTime.MinValue;

            if (GetResultAndEVDT("", code, "", "", out found_what, out evdt))
            {
                mins = 60.0 * found_what.ToDouble();
                enddt = evdt.AddMinutes(mins);

                if (ProcExistsInDB(pnum, evdt, enddt))
                {
                    Program.Audit("Procedure " + pnum+ ": already exists");
                }
                else
                {
                    if (!QueuedProcOverlaps(pnum, evdt, enddt))
                    {
                        var proc = new proc_data();
                        proc.procedure_number = pnum;
                        proc.start = evdt;
                        proc.finish = enddt;
                        _procs.Add(proc);
                        Program.Audit("Procedure " + pnum + ": Found " + code + " between " + evdt + " and " + enddt);
                    }
                }

            }

        }

        private bool OnlyHasED()
        {
            var db = PFSDBUtility.NewPfsDataContext();
            var query = from el in db.ENCOUNTER_LOCATIONs
                        join u in db.UNITs on el.UNIT_ID equals u.UNIT_ID
                        where (el.ENCOUNTER_ID == _pat.encounter_id)
                        where (u.IS_ED == null || u.IS_ED.ToString().ToUpper() == "N")
                        select new { u.NAME };
            return (query.Count() == 0);
        }

        private bool ProcExistsInDB(int pnum, DateTime startdt, DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && (proc.PROCEDURE_DATETIME == startdt)
                            && (proc.DEPARTURE_DATETIME == enddt)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new { proc.PROCEDURE_EVENT_ID };
            return (query.Count() > 0);
        }

        private bool QueuedProcOverlaps(int pnum, DateTime startdt, DateTime enddt)
        {
            bool overlap = false;

            proc_data[] pary = _procs.ToArray();
            for (int i = 0; i <= pary.GetUpperBound(0); i++)
            {
                if (pary[i].procedure_number == pnum)
                {
                    //overlap if   p.start between stardt and enddt
                    //             p.finish between startdt and enddt
                    //             startdt >= p.start and enddt <= p.finish
                    if ((pary[i].start >= startdt) && (pary[i].start < enddt) ||
                        (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
                        (startdt >= pary[i].start) && (enddt <= pary[i].finish))
                    {
                        overlap = true;
                    }

                    if ((pary[i].start >= startdt) && (pary[i].start < enddt) && (pary[i].finish > startdt) && (pary[i].finish <= enddt) ||
                        (pary[i].start < startdt) && (pary[i].finish > startdt) && (pary[i].finish < enddt)
                        )
                    {
                        // the proc in the list is completely contained in startdt/endt pair
                        // change this proc to be the startdt/enddt
                        // overlap is still true, but the list item will be updated with the encompassing times
                        //Program.Audit("Procedure " + pnum + ": " + startdt + " - " + enddt + " will trump " + pary[i].start + " - " + pary[i].finish);
                        pary[i].start = startdt;
                        pary[i].finish = enddt;
                    }
                }
            }
            _procs = pary.ToList();
            return overlap;
        }


        private void CheckProc_1()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P1. 1-1 safety observation by RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_2()
        {
            string nowstr;
            string toddtstr;
            string yesdtstr;
            string timea ="";
            DateTime timea_startdt, timea_enddt;
            DateTime timeb_startdt, timeb_enddt;
            DateTime nowdt = _pat.pull_finish;              // "now" is pull time
            DateTime yesdt;
            
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P2. 1-1 safety observation by non-RN");
            Program.VerboseAudit("---------------");

            nowstr = nowdt.ToString("yyyyMMddHHmm");
            yesdt = nowdt.AddDays(-1);
            toddtstr = nowdt.ToString("yyyyMMdd");
            yesdtstr = yesdt.ToString("yyyyMMdd");
            
            //when is now? (yesterday/today)
            //yes 7am -- yes 7p  -- tod 7a -- tod 7p
            //                                   A                   B
            //if nowdt >= tod7pm then check  tod 7am-tod 7pm and tod 7pm-tom 7a
            //if nowdt >= tod7am then check  yes 7p - tod 7a     tod 7a-tod 7p
            //if nowdt >= yes7pm then check  yes 7a-yes 7p       yes 7p-tod 7a
            if (nowstr.CompareTo(toddtstr + "1900") >= 0) {
                timea = toddtstr + "0700";
            } else if (nowstr.CompareTo(toddtstr + "0700") >= 0) {
                timea = yesdtstr + "1900";
            } else if (nowstr.CompareTo(yesdtstr + "1900") >= 0) {
                timea = yesdtstr + "0700";
            }
            
            timea_startdt = PFSUtility.ISOToDateTime(timea);
            timea_enddt = timea_startdt.AddHours(12);
            MaybeAddSitter(timea_startdt, timea_enddt);

            timeb_startdt = timea_enddt;
            timeb_enddt = timeb_startdt.AddHours(12);
            MaybeAddSitter(timeb_startdt, timeb_enddt);
        }

        private void MaybeAddSitter(DateTime startdt, DateTime enddt)
        {
            var query = StartNewQuery(SearchDepth.SearchSinceAdmission);    // add custom time range below
            query = query.Where(e => (e.EVENT_DATETIME >= startdt) && (e.EVENT_DATETIME < enddt));
            query = AndCodeInList(query, "Sitter");
            query = AndResultInList(query,"continued, initiated");
            query = AndResultNotInList(query, "discontinued");

            if (query.Count() > 0) {
                if (ProcExists(2, startdt, enddt)) {
                    Program.Audit("Procedure 2: already exists");
                } else {
                    var proc = new proc_data();
                    proc.procedure_number = 2;
                    proc.start = startdt;
                    proc.finish = enddt;
                    _procs.Add(proc);
                    Program.Audit("Procedure 2: Found Sitter between " + startdt + " and " + enddt);
                }
            }
            
        }

        private bool ProcExists(int pnum, DateTime startdt, DateTime enddt)
        {
            // Yes, this does do a database query, but only if we are about to add a procedure.
            // It only loads once and it never loads if we don't try to add a procedure.
            LoadPatientProceduresIfNeeded();
            var query = from proc in _procedure_events
                        from ans in proc.PROCEDURE_ANSWERs
                        where (proc.ENCOUNTER_ID == _pat.encounter_id)
                            && (proc.PROCEDURE_DATETIME == startdt)
                            && (proc.DEPARTURE_DATETIME == enddt)
                            && (ans.PROCEDURE_NUMBER == pnum)
                        select new {proc.PROCEDURE_EVENT_ID};
            return (query.Count() > 0);
        }


        private void CheckProc_3()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P3. Off unit accompanied by RN");
            Program.VerboseAudit("---------------");
            DoProc(3, "A_MHAcuOffUnit");
        }

        private void CheckProc_4()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P4. Off unit accompanied by non-RN");
            Program.VerboseAudit("---------------");
            DoProc(4, "A_MHAcuOffUNonRN");
        }

        private void CheckProc_5()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P5. Patient/family education by RN");
            Program.VerboseAudit("---------------");
            DoProc(5, "A_MHAcuPtFamEduc");
        }

        private void CheckProc_6()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P6. Extensive wound management by RN");
            Program.VerboseAudit("---------------");
            DoProc(6, "A_MHAcuExtensive");
        }

        private void CheckProc_7()
        {
            //Program.VerboseAudit("---------------");
            //Program.VerboseAudit("P7. Extensive wound management by non-RN");
            //Program.VerboseAudit("---------------");
        }

        private void CheckProc_8()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P8. Coordination of care by RN");
            Program.VerboseAudit("---------------");
            DoProc(8, "A_MHAcuCoordinat");
        }

        private void CheckProc_9()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P9 1:1 RN at bedside");
            Program.VerboseAudit("---------------");
            DoProc(9, "A_MHAcu1:1byURN");
        }

        private void CheckProc_10()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P10. 1:1 non-RN at bedside");
            Program.VerboseAudit("---------------");
            DoProc(10, "A_MhAcu1:1UNonRN");
        }

        private void CheckProc_11()
        {
            Program.VerboseAudit("---------------");
            Program.VerboseAudit("P11. 2:1 by RN at bedside");
            Program.VerboseAudit("---------------");
            DoProc(11, "A_MHAcu2:1by URN");
        }

        private void CheckOutcomes()
        {
            var query = StartNewQuery(SearchDepth.SearchDefault);
            query = AndItemFilter(query, "", "A_MHPostFalltime", "", "", "");
            foreach (var ch in query)
            {
                var outc = new proc_data();
                outc.procedure_number = 1;
                outc.start = ch.EVENT_DATETIME;
                _outcomes.Add(outc);
                Program.Audit("Outcomes 1: Found A_MHPostFalltime at " + outc.start);
            }
        }

        private string GetEDChildName()
        {

            EDchild edchildrec = new EDchild();
            Program.VerboseAudit("Looking for EDchild with parent unit=" + _pat.unit_id + " and meth=" + _pat.meth_id);
            edchildrec = Program.EDchildren.Find(e => e.parent_unitid == _pat.unit_id && e.meth_id == _pat.meth_id);
            return edchildrec.child_name;
        }
        

        private const string DATETIME_FORMAT = "yyyyMMddHHmm";              // ISO Date/Time w/o seconds

        private void OutputClass()
        {
            string outstr, ind_list, desc, str_pull_dt;
            int i, tc_event_id;

            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         212 242 304
            //         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1         1
            //1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901 3456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
            //1       |10 5            |                |                |        |10023121779         |ALEXANDER                       |GLORIA                          |                                |0508    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289530  |           |201403030400                                                                      |NNYNNNNYYNNYNNNYNNNYYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//1       |10 5            |                |                |        |10024544908         |ARMSTRONG                       |AARON                           |                                |0535    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289531  |           |201403030400                                                                      |NYNNYNNNNNNYNNNYNNNNYNYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//|0547    |A   |201403030330|                |201403030330    |18  |C|    |3040|1440|18289532  |           |201403030400                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN
//|0547    |A   |201403030700|                |201403030700    |18  |C|    |3040|1440|18289532  |           |201403030700                                                                      |NNYNYNNNYNNYNYNYNNNYNYYNNNNNNNNNNNNNNNNNNNNNNNNNNN

            if (Program.g_is_test)
                tc_event_id = 9999;
            else
                tc_event_id = PFSDBUtility.NextGID();                         //get a unique id for this class
//            str_pull_dt = _pat.pull_finish.ToString(DATETIME_FORMAT);
            str_pull_dt = _pat.unit_arrival.AddHours(1).ToString(DATETIME_FORMAT);
            string unitname = GetEDChildName();
            outstr = _pat.facilty_code.FixedWidth(8);                       //(facility code)
            //outstr += "|" + "ED Visit Subunit".FixedWidth(16); // _pat.unit_name.FixedWidth(16);
            outstr += "|" + unitname.FixedWidth(16); // _pat.unit_name.FixedWidth(16);
            outstr += "|" + "".FixedWidth(16);                               //(unit code)
            outstr += "|" + "".FixedWidth(16);                               //(area code)
            outstr += "|" + "".FixedWidth(8);                                //(class date - give datetime instead)
            outstr += "|" + _pat.acct.FixedWidth(20);
            outstr += "|" + _pat.last_name.FixedWidth(32);
            outstr += "|" + _pat.first_name.FixedWidth(32);
            outstr += "|" + _pat.middle_name.FixedWidth(32);
            outstr += "|" + _pat.room.FixedWidth(8);
            outstr += "|" + _pat.bed.FixedWidth(4);
          outstr += "|" + str_pull_dt.FixedWidth(12);                      //class datetime (could change)
            outstr += "|" + "".FixedWidth(16);                               //(login)
          outstr += "|" + str_pull_dt.FixedWidth(16);                      //(employee)/(pull datetime)
            outstr += "|" + _pat.meth_id.ToString().FixedWidth(4);
            outstr += "|" + "C".FixedWidth(1);                               //record type = class
            outstr += "|" + "".FixedWidth(4);                                //(stage)
            outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);      //TC source ID - (TCP port)
            outstr += "|" + _pat.range.ToString().FixedWidth(4);             //TC pull range
            outstr += "|" + tc_event_id.ToString().FixedWidth(10);           //TC event ID
            outstr += "|";
            outstr = outstr.FixedWidth(294);
          outstr += "|" + _pat.unit_arrival.AddHours(1).ToString(DATETIME_FORMAT);        //IN
            outstr = outstr.FixedWidth(377);
            outstr += "|";
            
            ind_list = "";
            for (i = 1; (i <= MAX_INDS); i++) {
                if (_inds[i].is_checked) {
                    outstr += "Y";
                    ind_list += "," + i;
                } else {
                    outstr += "N";
                }
            } // next i
            ind_list = ind_list.Substring(1);                           //strip leading comma
  
            Program.outfile.WriteLine(outstr);                          //output to transparent.txt

            Program.Audit("");
            desc = "Classified: " + ind_list;
            if (Program.g_is_test) {
                Program.Audit(desc);
            } else {
                //Save the selected indicators plus both Program.Audits in the event log; link with tc_event_id
                PFSEventLog.AddTransparentMappingEventLogEntry(
                    desc, Program.gLogUnitID, Program.gLogEncounterID,
                    tc_event_id, Program.gLogMapperVersion,
                    Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
            }
        }

        //private void OutputProcs()
        //{
        //    int i;
        //    string outstr, proc_list, desc;
        //    int tc_event_id;

        //    foreach(var proc in _procs) {
        //        if (Program.g_is_test)
        //            tc_event_id = 9999;
        //        else
        //            tc_event_id = PFSDBUtility.NextGID();                         //get a unique id for this proc

        //        outstr = _pat.facilty_code.FixedWidth(8);
        //        outstr += "|" + _pat.unit_name;                                 //10
        //        outstr = outstr.FixedWidth(68);
        //        outstr += "|" + _pat.acct.FixedWidth(20);                       //90
        //        outstr += "|" + _pat.last_name.FixedWidth(32);
        //        outstr += "|" + _pat.first_name.FixedWidth(32);
        //        outstr += "|" + _pat.middle_name.FixedWidth(32);
        //        outstr = outstr.FixedWidth(202);
        //        outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //204 proc dt
        //        outstr = outstr.FixedWidth(254);
        //        outstr += "|P";                                                 //256 procedure type record
        //        outstr += "|" + "".FixedWidth(4);                               //(stage)
        //        outstr += "|" + _pat.TC_source_id.ToString().FixedWidth(4);     //TC source ID
        //        outstr += "|" + _pat.range.ToString().FixedWidth(4);            //TC pull range
        //        outstr += "|" + tc_event_id.ToString().FixedWidth(10);          //TC event ID
        //        outstr += "|";
        //        outstr = outstr.FixedWidth(294);
        //        outstr += "|" + proc.start.ToString(DATETIME_FORMAT);           //296 procdt in
        //        outstr = outstr.FixedWidth(346);
        //        outstr += "|" + proc.finish.ToString(DATETIME_FORMAT);          //348 procdt out
        //        outstr = outstr.FixedWidth(377);
        //        outstr += "|";
                
        //        proc_list = "";
        //        for (i = 1; (i < MAX_PROCS); i++) {
        //            if (proc.procedure_number == i) {
        //                outstr += "Y";
        //                proc_list += "," + i;
        //            } else {
        //                outstr += "N";
        //            }
        //        } // next i
        //        proc_list = proc_list.Substring(1);                             //strip leading comma

        //        Program.outfile.WriteLine(outstr);                              //output to transparent.txt

        //        desc = "Procedures: " + proc_list;
        //        if (Program.g_is_test) {
        //            Program.Audit(desc);
        //        } else {
        //            //Save the selected procedures plus both Program.Audits in the event log; link with tc_event_id
        //            //(this assumes the procedure Program.Audit is mixed in with the class Program.Audit)
        //            PFSEventLog.AddTransparentMappingEventLogEntry(desc, Program.gLogUnitID, Program.gLogEncounterID,
        //                tc_event_id, Program.gLogMapperVersion,
        //                Program.gBriefAudit.ToString(), Program.gVerboseAudit.ToString());
        //        }
        //    } // next proc
        //}

    }

}
